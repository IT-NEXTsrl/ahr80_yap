* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_mco                                                        *
*              Costi - attivit�                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_14]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-02                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgspc_mco")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgspc_mco")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgspc_mco")
  return

* --- Class definition
define class tgspc_mco as StdPCForm
  Width  = 666
  Height = 169
  Top    = 48
  Left   = 61
  cComment = "Costi - attivit�"
  cPrg = "gspc_mco"
  HelpContextID=171288471
  add object cnt as tcgspc_mco
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgspc_mco as PCContext
  w_CSCODCOM = space(15)
  w_CSTIPSTR = space(1)
  w_CSCODMAT = space(15)
  w_CSCODCOS = space(5)
  w_CSPREVEN = 0
  w_DESCRI = space(30)
  w_CSCONSUN = 0
  w_CSORDIN = 0
  w_TOTPREV = 0
  w_TOTCONS = 0
  w_TOTORD = 0
  w_VALUTA = space(3)
  w_DECIMALI = 0
  w_CALCPICP = 0
  proc Save(i_oFrom)
    this.w_CSCODCOM = i_oFrom.w_CSCODCOM
    this.w_CSTIPSTR = i_oFrom.w_CSTIPSTR
    this.w_CSCODMAT = i_oFrom.w_CSCODMAT
    this.w_CSCODCOS = i_oFrom.w_CSCODCOS
    this.w_CSPREVEN = i_oFrom.w_CSPREVEN
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_CSCONSUN = i_oFrom.w_CSCONSUN
    this.w_CSORDIN = i_oFrom.w_CSORDIN
    this.w_TOTPREV = i_oFrom.w_TOTPREV
    this.w_TOTCONS = i_oFrom.w_TOTCONS
    this.w_TOTORD = i_oFrom.w_TOTORD
    this.w_VALUTA = i_oFrom.w_VALUTA
    this.w_DECIMALI = i_oFrom.w_DECIMALI
    this.w_CALCPICP = i_oFrom.w_CALCPICP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CSCODCOM = this.w_CSCODCOM
    i_oTo.w_CSTIPSTR = this.w_CSTIPSTR
    i_oTo.w_CSCODMAT = this.w_CSCODMAT
    i_oTo.w_CSCODCOS = this.w_CSCODCOS
    i_oTo.w_CSPREVEN = this.w_CSPREVEN
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_CSCONSUN = this.w_CSCONSUN
    i_oTo.w_CSORDIN = this.w_CSORDIN
    i_oTo.w_TOTPREV = this.w_TOTPREV
    i_oTo.w_TOTCONS = this.w_TOTCONS
    i_oTo.w_TOTORD = this.w_TOTORD
    i_oTo.w_VALUTA = this.w_VALUTA
    i_oTo.w_DECIMALI = this.w_DECIMALI
    i_oTo.w_CALCPICP = this.w_CALCPICP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgspc_mco as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 666
  Height = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=171288471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MA_COSTI_IDX = 0
  TIPCOSTO_IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  cFile = "MA_COSTI"
  cKeySelect = "CSCODCOM,CSTIPSTR,CSCODMAT"
  cKeyWhere  = "CSCODCOM=this.w_CSCODCOM and CSTIPSTR=this.w_CSTIPSTR and CSCODMAT=this.w_CSCODMAT"
  cKeyDetail  = "CSCODCOM=this.w_CSCODCOM and CSTIPSTR=this.w_CSTIPSTR and CSCODMAT=this.w_CSCODMAT and CSCODCOS=this.w_CSCODCOS"
  cKeyWhereODBC = '"CSCODCOM="+cp_ToStrODBC(this.w_CSCODCOM)';
      +'+" and CSTIPSTR="+cp_ToStrODBC(this.w_CSTIPSTR)';
      +'+" and CSCODMAT="+cp_ToStrODBC(this.w_CSCODMAT)';

  cKeyDetailWhereODBC = '"CSCODCOM="+cp_ToStrODBC(this.w_CSCODCOM)';
      +'+" and CSTIPSTR="+cp_ToStrODBC(this.w_CSTIPSTR)';
      +'+" and CSCODMAT="+cp_ToStrODBC(this.w_CSCODMAT)';
      +'+" and CSCODCOS="+cp_ToStrODBC(this.w_CSCODCOS)';

  cKeyWhereODBCqualified = '"MA_COSTI.CSCODCOM="+cp_ToStrODBC(this.w_CSCODCOM)';
      +'+" and MA_COSTI.CSTIPSTR="+cp_ToStrODBC(this.w_CSTIPSTR)';
      +'+" and MA_COSTI.CSCODMAT="+cp_ToStrODBC(this.w_CSCODMAT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gspc_mco"
  cComment = "Costi - attivit�"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CSCODCOM = space(15)
  w_CSTIPSTR = space(1)
  w_CSCODMAT = space(15)
  w_CSCODCOS = space(5)
  w_CSPREVEN = 0
  w_DESCRI = space(30)
  w_CSCONSUN = 0
  w_CSORDIN = 0
  w_TOTPREV = 0
  w_TOTCONS = 0
  w_TOTORD = 0
  w_VALUTA = space(3)
  w_DECIMALI = 0
  w_CALCPICP = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_mcoPag1","gspc_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIPCOSTO'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='MA_COSTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MA_COSTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MA_COSTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgspc_mco'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MA_COSTI where CSCODCOM=KeySet.CSCODCOM
    *                            and CSTIPSTR=KeySet.CSTIPSTR
    *                            and CSCODMAT=KeySet.CSCODMAT
    *                            and CSCODCOS=KeySet.CSCODCOS
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2],this.bLoadRecFilter,this.MA_COSTI_IDX,"gspc_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MA_COSTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MA_COSTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MA_COSTI '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CSCODCOM',this.w_CSCODCOM  ,'CSTIPSTR',this.w_CSTIPSTR  ,'CSCODMAT',this.w_CSCODMAT  )
      select * from (i_cTable) MA_COSTI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTPREV = 0
        .w_TOTCONS = 0
        .w_TOTORD = 0
        .w_VALUTA = space(3)
        .w_DECIMALI = 0
        .w_CSCODCOM = NVL(CSCODCOM,space(15))
          if link_1_1_joined
            this.w_CSCODCOM = NVL(CNCODCAN101,NVL(this.w_CSCODCOM,space(15)))
            this.w_VALUTA = NVL(CNCODVAL101,space(3))
          else
          .link_1_1('Load')
          endif
        .w_CSTIPSTR = NVL(CSTIPSTR,space(1))
        .w_CSCODMAT = NVL(CSCODMAT,space(15))
          .link_1_4('Load')
        .w_CALCPICP = DEFPIP(.w_DECIMALI)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MA_COSTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPREV = 0
      this.w_TOTCONS = 0
      this.w_TOTORD = 0
      scan
        with this
          .w_DESCRI = space(30)
          .w_CSCODCOS = NVL(CSCODCOS,space(5))
          if link_2_1_joined
            this.w_CSCODCOS = NVL(TCCODICE201,NVL(this.w_CSCODCOS,space(5)))
            this.w_DESCRI = NVL(TCDESCRI201,space(30))
          else
          .link_2_1('Load')
          endif
          .w_CSPREVEN = NVL(CSPREVEN,0)
          .w_CSCONSUN = NVL(CSCONSUN,0)
          .w_CSORDIN = NVL(CSORDIN,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPREV = .w_TOTPREV+.w_CSPREVEN
          .w_TOTCONS = .w_TOTCONS+.w_CSCONSUN
          .w_TOTORD = .w_TOTORD+.w_CSORDIN
          replace CSCODCOS with .w_CSCODCOS
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CALCPICP = DEFPIP(.w_DECIMALI)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CSCODCOM=space(15)
      .w_CSTIPSTR=space(1)
      .w_CSCODMAT=space(15)
      .w_CSCODCOS=space(5)
      .w_CSPREVEN=0
      .w_DESCRI=space(30)
      .w_CSCONSUN=0
      .w_CSORDIN=0
      .w_TOTPREV=0
      .w_TOTCONS=0
      .w_TOTORD=0
      .w_VALUTA=space(3)
      .w_DECIMALI=0
      .w_CALCPICP=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CSCODCOM))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_CSCODCOS))
         .link_2_1('Full')
        endif
        .DoRTCalc(5,12,.f.)
        if not(empty(.w_VALUTA))
         .link_1_4('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_CALCPICP = DEFPIP(.w_DECIMALI)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MA_COSTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MA_COSTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSCODCOM,"CSCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSTIPSTR,"CSTIPSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSCODMAT,"CSCODMAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CSCODCOS C(5);
      ,t_CSPREVEN N(18,4);
      ,t_DESCRI C(30);
      ,t_CSCONSUN N(18,4);
      ,t_CSORDIN N(18,4);
      ,CSCODCOS C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgspc_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODCOS_2_1.controlsource=this.cTrsName+'.t_CSCODCOS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSPREVEN_2_2.controlsource=this.cTrsName+'.t_CSPREVEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_3.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSCONSUN_2_4.controlsource=this.cTrsName+'.t_CSCONSUN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSORDIN_2_5.controlsource=this.cTrsName+'.t_CSORDIN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(217)
    this.AddVLine(357)
    this.AddVLine(497)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODCOS_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
      *
      * insert into MA_COSTI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MA_COSTI')
        i_extval=cp_InsertValODBCExtFlds(this,'MA_COSTI')
        i_cFldBody=" "+;
                  "(CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS,CSPREVEN"+;
                  ",CSCONSUN,CSORDIN,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_CSCODCOM)+","+cp_ToStrODBC(this.w_CSTIPSTR)+","+cp_ToStrODBC(this.w_CSCODMAT)+","+cp_ToStrODBCNull(this.w_CSCODCOS)+","+cp_ToStrODBC(this.w_CSPREVEN)+;
             ","+cp_ToStrODBC(this.w_CSCONSUN)+","+cp_ToStrODBC(this.w_CSORDIN)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MA_COSTI')
        i_extval=cp_InsertValVFPExtFlds(this,'MA_COSTI')
        cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_CSCODCOM,'CSTIPSTR',this.w_CSTIPSTR,'CSCODMAT',this.w_CSCODMAT,'CSCODCOS',this.w_CSCODCOS)
        INSERT INTO (i_cTable) (;
                   CSCODCOM;
                  ,CSTIPSTR;
                  ,CSCODMAT;
                  ,CSCODCOS;
                  ,CSPREVEN;
                  ,CSCONSUN;
                  ,CSORDIN;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CSCODCOM;
                  ,this.w_CSTIPSTR;
                  ,this.w_CSCODMAT;
                  ,this.w_CSCODCOS;
                  ,this.w_CSPREVEN;
                  ,this.w_CSCONSUN;
                  ,this.w_CSORDIN;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CSCODCOS<>' ') and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MA_COSTI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CSCODCOS="+cp_ToStrODBC(&i_TN.->CSCODCOS)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MA_COSTI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CSCODCOS=&i_TN.->CSCODCOS;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CSCODCOS<>' ') and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CSCODCOS="+cp_ToStrODBC(&i_TN.->CSCODCOS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CSCODCOS=&i_TN.->CSCODCOS;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CSCODCOS with this.w_CSCODCOS
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MA_COSTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MA_COSTI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CSPREVEN="+cp_ToStrODBC(this.w_CSPREVEN)+;
                     ",CSCONSUN="+cp_ToStrODBC(this.w_CSCONSUN)+;
                     ",CSORDIN="+cp_ToStrODBC(this.w_CSORDIN)+;
                     ",CSCODCOS="+cp_ToStrODBC(this.w_CSCODCOS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CSCODCOS="+cp_ToStrODBC(CSCODCOS)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MA_COSTI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CSPREVEN=this.w_CSPREVEN;
                     ,CSCONSUN=this.w_CSCONSUN;
                     ,CSORDIN=this.w_CSORDIN;
                     ,CSCODCOS=this.w_CSCODCOS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CSCODCOS=&i_TN.->CSCODCOS;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CSCODCOS<>' ') and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MA_COSTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CSCODCOS="+cp_ToStrODBC(&i_TN.->CSCODCOS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CSCODCOS=&i_TN.->CSCODCOS;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CSCODCOS<>' ') and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
          .link_1_4('Full')
        .DoRTCalc(13,13,.t.)
          .w_CALCPICP = DEFPIP(.w_DECIMALI)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CSCODCOM
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CSCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CSCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CSCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CSCODCOM)
            select CNCODCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CSCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_VALUTA = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CSCODCOM = space(15)
      endif
      this.w_VALUTA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CSCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.CNCODCAN as CNCODCAN101"+ ",link_1_1.CNCODVAL as CNCODVAL101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on MA_COSTI.CSCODCOM=link_1_1.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and MA_COSTI.CSCODCOM=link_1_1.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CSCODCOS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CSCODCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_ATC',True,'TIPCOSTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CSCODCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CSCODCOS))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CSCODCOS)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CSCODCOS) and !this.bDontReportError
            deferred_cp_zoom('TIPCOSTO','*','TCCODICE',cp_AbsName(oSource.parent,'oCSCODCOS_2_1'),i_cWhere,'GSPC_ATC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CSCODCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CSCODCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CSCODCOS)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CSCODCOS = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCRI = NVL(_Link_.TCDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CSCODCOS = space(5)
      endif
      this.w_DESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CSCODCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPCOSTO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TCCODICE as TCCODICE201"+ ",link_2_1.TCDESCRI as TCDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MA_COSTI.CSCODCOS=link_2_1.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MA_COSTI.CSCODCOS=link_2_1.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMALI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECIMALI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTPREV_3_1.value==this.w_TOTPREV)
      this.oPgFrm.Page1.oPag.oTOTPREV_3_1.value=this.w_TOTPREV
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCONS_3_2.value==this.w_TOTCONS)
      this.oPgFrm.Page1.oPag.oTOTCONS_3_2.value=this.w_TOTCONS
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTORD_3_4.value==this.w_TOTORD)
      this.oPgFrm.Page1.oPag.oTOTORD_3_4.value=this.w_TOTORD
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODCOS_2_1.value==this.w_CSCODCOS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODCOS_2_1.value=this.w_CSCODCOS
      replace t_CSCODCOS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODCOS_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSPREVEN_2_2.value==this.w_CSPREVEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSPREVEN_2_2.value=this.w_CSPREVEN
      replace t_CSPREVEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSPREVEN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_3.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_3.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCONSUN_2_4.value==this.w_CSCONSUN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCONSUN_2_4.value=this.w_CSCONSUN
      replace t_CSCONSUN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCONSUN_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSORDIN_2_5.value==this.w_CSORDIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSORDIN_2_5.value=this.w_CSORDIN
      replace t_CSORDIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSORDIN_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'MA_COSTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_CSCODCOS<>' '
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CSCODCOS<>' ')
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CSCODCOS=space(5)
      .w_CSPREVEN=0
      .w_DESCRI=space(30)
      .w_CSCONSUN=0
      .w_CSORDIN=0
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_CSCODCOS))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(5,14,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CSCODCOS = t_CSCODCOS
    this.w_CSPREVEN = t_CSPREVEN
    this.w_DESCRI = t_DESCRI
    this.w_CSCONSUN = t_CSCONSUN
    this.w_CSORDIN = t_CSORDIN
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CSCODCOS with this.w_CSCODCOS
    replace t_CSPREVEN with this.w_CSPREVEN
    replace t_DESCRI with this.w_DESCRI
    replace t_CSCONSUN with this.w_CSCONSUN
    replace t_CSORDIN with this.w_CSORDIN
    if i_srv='A'
      replace CSCODCOS with this.w_CSCODCOS
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPREV = .w_TOTPREV-.w_cspreven
        .w_TOTCONS = .w_TOTCONS-.w_csconsun
        .w_TOTORD = .w_TOTORD-.w_csordin
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgspc_mcoPag1 as StdContainer
  Width  = 662
  height = 169
  stdWidth  = 662
  stdheight = 169
  resizeXpos=163
  resizeYpos=106
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=-2, width=644,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CSCODCOS",Label1="Codice",Field2="DESCRI",Label2="Descrizione",Field3="CSPREVEN",Label3="Preventivi",Field4="CSCONSUN",Label4="Consuntivi",Field5="CSORDIN",Label5="Impegni finanziari",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49491590

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=21,;
    width=640+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=22,width=639+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPCOSTO|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPCOSTO'
        oDropInto=this.oBodyCol.oRow.oCSCODCOS_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPREV_3_1 as StdField with uid="VFSHDYSOSB",rtseq=9,rtrep=.f.,;
    cFormVar="w_TOTPREV",value=0,enabled=.f.,;
    ToolTipText = "Totale",;
    HelpContextID = 78330934,;
    cQueryName = "TOTPREV",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=219, Top=142, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oTOTCONS_3_2 as StdField with uid="XMKCGDNIPL",rtseq=10,rtrep=.f.,;
    cFormVar="w_TOTCONS",value=0,enabled=.f.,;
    ToolTipText = "Totale",;
    HelpContextID = 43107274,;
    cQueryName = "TOTCONS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=358, Top=142, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oTOTORD_3_4 as StdField with uid="JYPHQKGWTT",rtseq=11,rtrep=.f.,;
    cFormVar="w_TOTORD",value=0,enabled=.f.,;
    ToolTipText = "Totale",;
    HelpContextID = 206947274,;
    cQueryName = "TOTORD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=499, Top=142, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oStr_3_3 as StdString with uid="EFOPIYVRDE",Visible=.t., Left=106, Top=143,;
    Alignment=1, Width=107, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgspc_mcoBodyRow as CPBodyRowCnt
  Width=630
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCSCODCOS_2_1 as StdTrsField with uid="TKYTLEETXX",rtseq=4,rtrep=.t.,;
    cFormVar="w_CSCODCOS",value=space(5),isprimarykey=.t.,;
    HelpContextID = 238473351,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIPCOSTO", cZoomOnZoom="GSPC_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CSCODCOS"

  func oCSCODCOS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCSCODCOS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCSCODCOS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCSCODCOS_2_1.readonly and this.parent.oCSCODCOS_2_1.isprimarykey)
    do cp_zoom with 'TIPCOSTO','*','TCCODICE',cp_AbsName(this.parent,'oCSCODCOS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_ATC',"",'',this.parent.oContained
   endif
  endproc
  proc oCSCODCOS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSPC_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CSCODCOS
    i_obj.ecpSave()
  endproc

  add object oCSPREVEN_2_2 as StdTrsField with uid="GBNWLZUZTZ",rtseq=5,rtrep=.t.,;
    cFormVar="w_CSPREVEN",value=0,enabled=.f.,;
    HelpContextID = 81592180,;
    cTotal = "this.Parent.oContained.w_totprev", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=210, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oDESCRI_2_3 as StdTrsField with uid="OYRAMTXEVO",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(30),enabled=.f.,;
    HelpContextID = 123854538,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=51, Top=0, InputMask=replicate('X',30)

  add object oCSCONSUN_2_4 as StdTrsField with uid="BZONLOULGO",rtseq=7,rtrep=.t.,;
    cFormVar="w_CSCONSUN",value=0,enabled=.f.,;
    ToolTipText = "Totale a consuntivo",;
    HelpContextID = 40447860,;
    cTotal = "this.Parent.oContained.w_totcons", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=349, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oCSORDIN_2_5 as StdTrsField with uid="BTGPZXFJCS",rtseq=8,rtrep=.t.,;
    cFormVar="w_CSORDIN",value=0,enabled=.f.,;
    HelpContextID = 137564378,;
    cTotal = "this.Parent.oContained.w_totord", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=490, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]
  add object oLast as LastKeyMover
  * ---
  func oCSCODCOS_2_1.When()
    return(.t.)
  proc oCSCODCOS_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCSCODCOS_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_mco','MA_COSTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CSCODCOM=MA_COSTI.CSCODCOM";
  +" and "+i_cAliasName2+".CSTIPSTR=MA_COSTI.CSTIPSTR";
  +" and "+i_cAliasName2+".CSCODMAT=MA_COSTI.CSCODMAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
