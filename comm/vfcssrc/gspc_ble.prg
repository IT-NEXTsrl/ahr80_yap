* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_ble                                                        *
*              Gestione log-errori                                             *
*                                                                              *
*      Author: Zucchetti SpA (CF)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-31                                                      *
* Last revis.: 2005-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_ble",oParentObject)
return(i_retval)

define class tgspc_ble as StdBatch
  * --- Local variables
  * --- WorkFile variables
  COMM_ERR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Log Errori - Esegue aggiornamento flag stampato
    * --- (T)utte - (U)ltima - (N)on stampate
    * --- Inizializza Variabili Locali
    private l_Titolo,l_DettTec
    vq_Exec("..\COMM\EXE\QUERY\GSPC_KLE.VQR",this,"__Tmp__")
    if used("__Tmp__")
      do case
        case this.oParentObject.w_LOperaz = "GM"
          l_Titolo = "Generazione movimenti di commessa da dichiarazioni di manodopera"
      endcase
      * --- Lancia Stampa
      l_DettTec = this.oParentObject.w_LDettTec
      CP_CHPRN("..\COMM\EXE\QUERY\GSPC_KLE","",this.oParentObject)
      if used("__tmp__")
        * --- Marca i record stampati
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_0387A360
        bErr_0387A360=bTrsErr
        this.Try_0387A360()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0387A360
        * --- End
      endif
    endif
  endproc
  proc Try_0387A360()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    select __tmp__
    scan
    * --- Write into COMM_ERR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COMM_ERR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COMM_ERR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COMM_ERR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LESTAMPA ="+cp_NullLink(cp_ToStrODBC("S"),'COMM_ERR','LESTAMPA');
          +i_ccchkf ;
      +" where ";
          +"LESERIAL = "+cp_ToStrODBC(__tmp__.LESERIAL);
          +" and LEROWNUM = "+cp_ToStrODBC(__tmp__.LEROWNUM);
             )
    else
      update (i_cTable) set;
          LESTAMPA = "S";
          &i_ccchkf. ;
       where;
          LESERIAL = __tmp__.LESERIAL;
          and LEROWNUM = __tmp__.LEROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COMM_ERR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
