* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bdo                                                        *
*              Legge dati mov, in gsve_bmk                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_8]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-20                                                      *
* Last revis.: 2006-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIALE,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bdo",oParentObject,m.pSERIALE,m.pOPER)
return(i_retval)

define class tgspc_bdo as StdBatch
  * --- Local variables
  pSERIALE = space(10)
  pOPER = space(1)
  w_SERIAL = space(10)
  * --- WorkFile variables
  MAT_MAST_idx=0
  RAT_COMM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi dalla Gestione Documenti - Lanciato da GSVE_BMK
    this.w_SERIAL = this.pSERIALE
    if this.pOPER="S"
      * --- Elimina le rate Scadenze di Commessa associate al Documento (in cancellazione)
      * --- Delete from RAT_COMM
      i_nConn=i_TableProp[this.RAT_COMM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RAT_COMM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              SCSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Legge la commessa e l'attitivt� dal movimento di commessa
      * --- Read from MAT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MACODCOM,MACODATT"+;
          " from "+i_cTable+" MAT_MAST where ";
              +"MASERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MACODCOM,MACODATT;
          from (i_cTable) where;
              MASERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_CODCOM = NVL(cp_ToDate(_read_.MACODCOM),cp_NullValue(_read_.MACODCOM))
        this.oParentObject.w_CODATT = NVL(cp_ToDate(_read_.MACODATT),cp_NullValue(_read_.MACODATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  proc Init(oParentObject,pSERIALE,pOPER)
    this.pSERIALE=pSERIALE
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MAT_MAST'
    this.cWorkTables[2]='RAT_COMM'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIALE,pOPER"
endproc
