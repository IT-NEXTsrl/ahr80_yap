* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc1bcc                                                        *
*              Cruscotto commessa                                              *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-22                                                      *
* Last revis.: 2018-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc1bcc",oParentObject,m.pPARAM)
return(i_retval)

define class tgspc1bcc as StdBatch
  * --- Local variables
  pPARAM = space(5)
  w_STAMPA = .f.
  w_nLenght = 0
  w_nDecimal = 0
  w_CAMBIOCOMMESER = 0
  w_TAB = space(1)
  w_AVFOGLIA = 0
  w_CRLF = space(1)
  w_CEXPTABLE = space(100)
  w_CODART = space(41)
  w_CONS = 0
  w_CRIFTABLE = space(100)
  w_DECCOM = 0
  w_EXPFIELD = space(100)
  w_EXPKEY = space(100)
  w_FILTRO = space(20)
  w_FOGLIA = space(1)
  w_FORMATO = space(20)
  w_INPCURS = space(100)
  w_LIVELLO = space(100)
  w_LUNGH = 0
  w_MESS = space(250)
  w_MOLTIP = 0
  w_NUMPOINT = 0
  w_OPERAT = space(1)
  w_OTHERFLD = space(100)
  w_PADRE = space(15)
  w_PERCOMP = 0
  w_PREVEN = 0
  w_POSREC = 0
  w_PREV = 0
  w_PREVMA = 0
  w_PREVMD = 0
  w_PREVAP = 0
  w_PREVAL = 0
  w_RICERCA = space(100)
  w_RIFKEY = space(100)
  w_REPKEY = space(100)
  w_STOP = .f.
  w_TIPPRE = space(1)
  w_TIPPRE1 = space(1)
  w_TIPSTR = space(1)
  w_TOTCONS = 0
  w_TOTCOST = 0
  w_TOTORDI = 0
  w_TOTPREV = 0
  w_TOTALE = 0
  w_TIPCOM = space(1)
  w_UMDFLT = space(3)
  w_UNIQUE = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_RICART = space(1)
  w_CONS_MA = 0
  w_CONS_MD = 0
  w_CONS_AP = 0
  w_CONS_AL = 0
  w_DTRIECO1 = ctod("  /  /  ")
  w_DTRIECO2 = ctod("  /  /  ")
  w_IMPORTO_MA = 0
  w_IMPORTO_MD = 0
  w_IMPORTO_AP = 0
  w_IMPORTO_AL = 0
  w_LIVELLO = space(100)
  w_LUNGH = 0
  w_NUMPOINT = 0
  w_ORDI_MA = 0
  w_ORDI_MD = 0
  w_ORDI_AP = 0
  w_ORDI_AL = 0
  w_PADRE = space(15)
  w_POSREC = 0
  w_RICERCA = space(100)
  w_TIPO = space(1)
  w_DOCVAL = space(3)
  w_CAOVAL = 0
  w_NUMREC = .f.
  w_DATDOC = ctod("  /  /  ")
  w_CAOCOMM = 0
  w_IMP_MA = 0
  w_ORD_MA = 0
  w_IMP_MD = 0
  w_ORD_MD = 0
  w_IMP_AP = 0
  w_ORD_AP = 0
  w_IMP_AL = 0
  w_ORD_AL = 0
  * --- WorkFile variables
  CPAR_DEF_idx=0
  ATTIVITA_idx=0
  ART_ICOL_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Grafici di avanzamento
    * --- Variabili
    * --- --
    *     EXIT_ Esce da maschera di visualizzazione
    *     VISUA Visualizza
    *     PRINT Stampa da visualizzazione
    *     PRIN2 Stampa senza visualizzazione
    * --- Utilizzata per decidere se disabilitare o meno i bottoni di stampa 
    *     dopo aver elaborato i dati.
    this.w_STAMPA = VarType( this.oParentObject.w_FLSTAMP1 )="L"
    * --- Decimali della valuta di commessa w_DECCOM
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_nLenght = 18
    this.w_nDecimal = this.w_DECCOM
    this.w_FORMATO = "99,999,999,999,999"
    if this.w_DECCOM > 0
      this.w_FORMATO = RIGHT(this.w_FORMATO,18-1-this.w_DECCOM)+"."+REPL("9",this.w_DECCOM)
    endif
    L_CODCOM=this.oParentObject.w_CODCOM 
 L_CODVAL=this.oParentObject.w_CODVAL 
 L_DESCOM=this.oParentObject.w_DESCOM 
 L_DATAINI=this.oParentObject.w_DATAINI 
 L_TIPOCOST=this.oParentObject.w_TIPOCOST 
 L_TIPOGRA=this.oParentObject.w_TIPOGRA 
 L_DECIMALICOMMESSA = this.w_DECCOM 
 L_FORMATO = this.w_FORMATO
    * --- Caratteri TAB e A CAPO-RITORNO CARRELLO
    this.w_TAB = chr(9)
    this.w_CRLF = chr(13)+chr(10)
    * --- COSTRUISCE IL CURSORE DI STAMPA ED ESEGUE LA STAMPA
    if this.pPARAM<>"EXIT_" and this.pPARAM<>"PRINT"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Crea cursore di stampa
      Create Cursor STAMPA (PAGINA N(2,0), RIGA N(2,0), DESCRIZIONE C(30), stampa_A C(18), stampa_B C(18), ; 
 stampa_C C(18), stampa_D C(18),n_A N(18,4), n_B N(18,4), n_C N(18,4), n_D N(18,4)) 
 =WrCursor("STAMPA")
      * --- Crea cursore con i grafici da utilizzare nella stampa, da TamGraph, ed esegue la numerazione
      *     delle pagine
       
 Select *, 9 as page from TamGrap2 where nome = "Avanzamento" ; 
 or nome = "Costi Tipol." or nome= "Importi Costi" ; 
 or nome= "Costi Ricavi" or nome= "StM. Attuale" ; 
 or nome= "StM. Residuo" or nome= "StM. Atteso" ; 
 or nome= "StPerc. Margine" Into Cursor CursoriGrafici NOFILTER 
 =WrCursor ("CursoriGrafici") 
 GO TOP 
 LOCATE for nome = "Avanzamento" 
 REPLACE page with 1 
 GO TOP 
 LOCATE for nome = "Costi Tipol." 
 REPLACE page with 2 
 GO TOP 
 LOCATE for nome = "Importi Costi" 
 REPLACE page with 3 
 GO TOP 
 LOCATE for nome = "Costi Ricavi" 
 REPLACE page with 4 
 GO TOP 
 LOCATE for nome = "StM. Attuale" 
 REPLACE page with 5 
 GO TOP 
 LOCATE for nome = "StM. Residuo" 
 REPLACE page with 6 
 GO TOP 
 LOCATE for nome = "StM. Atteso" 
 REPLACE page with 7 
 GO TOP 
 LOCATE for nome = "StPerc. Margine" 
 REPLACE page with 8
      * --- Calcola i dati per Pag1 mettendoli nel cursore STAMPA
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
       
 select 9999999999999.99-9999999999999.99+n_D, ; 
 9999999999999.99-9999999999999.99+iif(n_A>n_D,n_A-n_D,0), ; 
 9999999999999.99-9999999999999.99+n_A, ; 
 9999999999999.99-9999999999999.99+n_B, ; 
 9999999999999.99-9999999999999.99+n_C ; 
 from STAMPA where pagina=1 and inlist(riga,2,4) order by RIGA into array VettorePag4 
 VettorePag4(2,3)=iif(VettorePag4(2,1)>VettorePag4(2,3),VettorePag4(2,1),VettorePag4(2,3))
       
 Dimension Vett_Mar(4,4)
      if this.oParentObject.w_CODVAL=g_CODLIR
        * --- Se la valuta � lire, nei grafici del margine si riportano le cifre in milioni di lire
         
 L_ML = 1000000
      else
         
 L_ML = 1
      endif
      if this.oParentObject.w_TIPOGRA="I"
        * --- Mostra Importi
      else
         
 Vett_Mar(1,1) = AH_MSGFORMAT("ATTUALI") 
 Vett_Mar(1,2) = str(100,18,2) 
 If Val(L_FatturatoAttuale) <> 0 
 L_percCostiPag5 = str(val(L_ConsuntivoPag4)*100/val(L_FatturatoAttuale),18,2) 
 L_percImpePag5 = str(val(L_ImpegniFinanziariPag4)*100/val(L_FatturatoAttuale),18,2) 
 else 
 L_percCostiPag5 = "0,00" 
 L_percImpePag5 = "0,00" 
 endif 
 Vett_Mar(1,3) = L_PercCostiPag5 
 Vett_Mar(1,4) = str(100-val(Vett_Mar(1,3)),18,2) 
 Vett_Mar(2,1) = AH_MSGFORMAT("RESIDUI") 
 Vett_Mar(2,2) = str(100,18,2) 
 Vett_Mar(2,3) = str(VettorePag4(2,2)*100/VettorePag4(1,2),18,2) 
 Vett_Mar(2,4) = str(100-val(Vett_Mar(2,3)),18,2) 
 Vett_Mar(3,1) = AH_MSGFORMAT("ATTESI") 
 Vett_Mar(3,2) = str(100,18,2) 
 Vett_Mar(3,3) = str(VettorePag4(2,3)*100/VettorePag4(1,3),18,2) 
 Vett_Mar(3,4) = str(100-val(Vett_Mar(3,3)),18,2)
      endif
       
 Dimension VettRicavi(3,2) 
 * Ricavi Attuali 
 VettRicavi(1,1)=VettorePag4(1,4) 
 *Ricavi Residui 
 VettRicavi(2,1)=VettorePag4(1,5) 
 * Ricavi Attesi 
 VettRicavi(3,1)=VettorePag4(1,4)+VettorePag4(1,5) 
 IF VettRicavi(3,1)<>0 
 VettRicavi(1,2)=cp_round(100*VettRicavi(1,1)/VettRicavi(3,1),2) 
 VettRicavi(2,2)=cp_round(100*VettRicavi(2,1)/VettRicavi(3,1),2) 
 VettRicavi(3,2)=cp_round(100*VettRicavi(3,1)/VettRicavi(3,1),2) 
 ELSE 
 VettRicavi(1,2)=0 
 VettRicavi(2,2)=0 
 VettRicavi(3,2)=100 
 ENDIF
       
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTUALI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(1,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(VettRicavi(1,2),18,2) 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "RESIDUI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(VettRicavi(2,2),18,2) 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(3,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(VettRicavi(3,2),18,2) 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 4 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTUALI" ) 
 REPLACE STAMPA_A WITH STR(cp_round(Vettorepag4(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 IF VettorePag4(2,3)<>0 
 REPLACE STAMPA_B WITH STR(VettorePag4(2,1)*100/VettorePag4(2,3),18,2) 
 ELSE 
 REPLACE STAMPA_B WITH STR(0,18,2) 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 5 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "RESIDUI" ) 
 IF VettorePag4(2,3) > VettorePag4(2,1) 
 REPLACE STAMPA_A WITH STR(cp_round(Vettorepag4(2,2),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 ELSE 
 REPLACE STAMPA_A WITH STR(cp_round(0,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 ENDIF 
 IF VettorePag4(2,3)<>0 AND VettorePag4(2,3) > VettorePag4(2,1) 
 REPLACE STAMPA_B WITH STR(VettorePag4(2,2)*100/VettorePag4(2,3),18,2) 
 ELSE 
 REPLACE STAMPA_B WITH STR(0,18,2) 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 4 
 REPLACE RIGA WITH 6 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE STAMPA_A WITH STR(cp_round(Vettorepag4(2,3),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE STAMPA_B WITH STR(100,18,2)
       
 Vett_Mar(1,1) = AH_MSGFORMAT("ATTUALI") 
 Vett_Mar(1,2) = STR(cp_round(VettRicavi(1,1)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(1,3) = STR(cp_round(VettorePag4(2,1)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(1,4) = STR(cp_round((VettRicavi(1,1)-VettorePag4(2,1))/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(2,1) = AH_MSGFORMAT("RESIDUI") 
 Vett_Mar(2,2) = STR(cp_round(VettRicavi(2,1)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(2,3) = STR(cp_round(VettorePag4(2,2)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(2,4) = STR(cp_round((VettRicavi(2,1)-VettorePag4(2,2))/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(3,1) = AH_MSGFORMAT("ATTESI") 
 Vett_Mar(3,2) = STR(cp_round(VettRicavi(3,1)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(3,3) = STR(cp_round(VettorePag4(2,3)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(3,4) = STR(cp_round((VettRicavi(3,1)-VettorePag4(2,3))/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_Mar(4,1) = AH_MSGFORMAT("MARGINE") 
 Vett_Mar(4,2) = IIF(VettRicavi(1,1)=0,"-",STR(cp_round(100*(VettRicavi(1,1)-VettorePag4(2,1))/VettRicavi(1,1),2),10,2)) 
 Vett_Mar(4,3) = IIF(VettRicavi(2,1)=0,"-",STR(cp_round(100*(VettRicavi(2,1)-VettorePag4(2,2))/VettRicavi(2,1),2),10,2)) 
 Vett_Mar(4,4) = IIF(VettRicavi(3,1)=0,"-",STR(cp_round(100*(VettRicavi(3,1)-VettorePag4(2,3))/VettRicavi(3,1),2),10,2))
       
 APPEND BLANK 
 REPLACE PAGINA WITH 5 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTUALI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(1,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(1,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(1,2),18,2) 
 REPLACE n_B WITH VettRicavi(1,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,1),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,1)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,1)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 5 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "RESIDUI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(2,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(2,2),18,2) 
 REPLACE n_B WITH VettRicavi(2,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,2),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,2),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,2)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,2)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 5 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(3,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(3,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(100,18,2) 
 REPLACE n_B WITH 100 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,3),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,3),this.w_DECCOM) 
 REPLACE stampa_D WITH STR(100,18,2) 
 REPLACE n_D WITH 100 
 
 APPEND BLANK 
 REPLACE PAGINA WITH 6 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTUALI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(1,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(1,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(1,2),18,2) 
 REPLACE n_B WITH VettRicavi(1,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,1),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,1)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,1)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 6 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "RESIDUI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(2,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(2,2),18,2) 
 REPLACE n_B WITH VettRicavi(2,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,2),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,2),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,2)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,2)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 6 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(3,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(3,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(100,18,2) 
 REPLACE n_B WITH 100 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,3),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,3),this.w_DECCOM) 
 REPLACE stampa_D WITH STR(100,18,2) 
 REPLACE n_D WITH 100 
 
 APPEND BLANK 
 REPLACE PAGINA WITH 7 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTUALI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(1,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(1,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(1,2),18,2) 
 REPLACE n_B WITH VettRicavi(1,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,1),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,1)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,1)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 7 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "RESIDUI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(2,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(VettRicavi(2,2),18,2) 
 REPLACE n_B WITH VettRicavi(2,2) 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,2),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,2),this.w_DECCOM) 
 IF VettorePag4(2,3)<>0 
 REPLACE stampa_D WITH STR(VettorePag4(2,2)*100/VettorePag4(2,3),18,2) 
 REPLACE n_D WITH VettorePag4(2,2)*100/VettorePag4(2,3) 
 ELSE 
 REPLACE stampa_D WITH STR(0,18,2) 
 REPLACE n_D WITH 0 
 ENDIF 
 APPEND BLANK 
 REPLACE PAGINA WITH 7 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ATTESI" ) 
 REPLACE stampa_A WITH STR(cp_round(VettRicavi(3,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH cp_round(VettRicavi(3,1),this.w_DECCOM) 
 REPLACE stampa_B WITH STR(100,18,2) 
 REPLACE n_B WITH 100 
 REPLACE stampa_C WITH STR(cp_round(Vettorepag4(2,3),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_C WITH cp_round(Vettorepag4(2,3),this.w_DECCOM) 
 REPLACE stampa_D WITH STR(100,18,2) 
 REPLACE n_D WITH 100
       
 APPEND BLANK 
 REPLACE PAGINA WITH 8 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MARGINE" ) 
 REPLACE stampa_A WITH Vett_Mar(4,2) 
 REPLACE stampa_B WITH Vett_Mar(4,3) 
 REPLACE stampa_C WITH Vett_Mar(4,4)
      * --- Vett_Avanz_ serve per valorizzare il cursore _Avanz_
      DIMENSION Vett_Avanz_(4,3)
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("CONSUNTIVO")+this.w_TAB+AH_MSGFORMAT("IMPEGNATO")+this.w_CRLF 
 L_AvanTecnNonDisp="" 
 Select STAMPA 
 GO TOP 
 SCAN FOR PAGINA=1
      if riga=1
        Vett_Avanz_(1,1) = AH_MSGFORMAT("Tecnico") 
 Vett_Avanz_(1,2) = STAMPA_A 
 Vett_Avanz_(1,3) = SPACE(6) 
 L_AvanTecnNonDisp="DISP"
      else
        L_AvanTecnNonDisp=IIF(Empty(L_AvanTecnNonDisp),"NONDISP",L_AvanTecnNonDisp)
      endif
      if inlist(riga,2,4) and n_A <> 0 
        Vett_Avanz_(riga,1) = IIF(Riga=2,AH_MSGFORMAT("Fatt./Fatt.da em"),AH_MSGFORMAT("Cons/Imp.fin."))
        if RIGA=2 
          if L_AvanTecnNonDisp="DISP" 
            this.oParentObject.w_TXT1 = AH_MSGFORMAT("Totale ricavi")+" "+str(100*(n_B+n_C)/n_A,5,2)+" %"
            this.oParentObject.w_TXT3 = ""
          else
            this.oParentObject.w_TXT1 = ""
            this.oParentObject.w_TXT3 = AH_MSGFORMAT("Totale ricavi")+" "+str(100*(n_B+n_C)/n_A,5,2)+" %"
          endif
        endif
        if RIGA=4
          this.oParentObject.w_TXT2 = AH_MSGFORMAT("Totale costi")+" "+str(100*(n_B+n_C)/n_A,5,2)+" %"
        endif
         
 Vett_Avanz_(riga,2) = str(100*n_B/n_A,6,2) 
 Vett_Avanz_(riga,3) = str(100*n_C/n_A,6,2)
      endif
      if inlist(riga,2,4) and n_A = 0 
        Vett_Avanz_(riga,1) = IIF(Riga=2,AH_MSGFORMAT("Fatt./Fatt.da em"),AH_MSGFORMAT("Cons/Imp.fin.")) 
        if RIGA=2
          if L_AvanTecnNonDisp="DISP" 
            this.oParentObject.w_TXT1 = AH_MSGFORMAT("Totale ricavi")+" "+str(0,5,2)+" %"
            this.oParentObject.w_TXT3 = "" 
          else
            this.oParentObject.w_TXT1 = ""
            this.oParentObject.w_TXT3 = AH_MSGFORMAT("Totale ricavi")+" "+str(0,5,2)+" %" 
          endif
        endif
        if RIGA=4
          this.oParentObject.w_TXT2 = AH_MSGFORMAT("Totale costi")+" "+str(0,5,2)+" %" 
        endif
         
 Vett_Avanz_(riga,2) = STAMPA_B 
 Vett_Avanz_(riga,3) = STAMPA_C
      endif
      if riga=3 and n_A <> 0 
         
 Vett_Avanz_(riga,1) = AH_MSGFORMAT("Manodopera") 
 Vett_Avanz_(riga,2) = str(100*n_B/n_A,6,2) 
 Vett_Avanz_(riga,3) = SPACE(6)
      endif
      if riga=3 and val(STAMPA_A) = 0 
         
 Vett_Avanz_(riga,1) = AH_MSGFORMAT("Manodopera") 
 Vett_Avanz_(riga,2) = str(0,6,2) 
 Vett_Avanz_(riga,3) = SPACE(6)
      endif
      L_DatiGrafico=L_DatiGrafico+Vett_Avanz_(riga,1)+this.w_TAB+Vett_Avanz_(riga,2)+this.w_TAB+Vett_Avanz_(riga,3)+this.w_CRLF
      EndScan
      L_TXT1 = this.oParentObject.w_TXT1 
 L_TXT2 = this.oParentObject.w_TXT2 
 L_TXT3 = this.oParentObject.w_TXT3 
      * --- IF VISUALIZZAZIONE
      *     Allo stesso modo costruisce il cursore _Avanz_ da utilizzare per la visualizzazione
       
 Create Cursor _Avanz_ (tipo C (15) , CONSUNTIVO C(18),IMPEGNATO C(18) ) 
 Cur =WrCursor ("_Avanz_")
      if this.oParentObject.w_DATAINI >= i_DATSYS
         
 Append Blank 
 Replace Tipo With Vett_Avanz_(1,1), CONSUNTIVO With Vett_Avanz_(1,2)
      endif
      Append Blank 
 Replace Tipo With Vett_Avanz_(2,1), CONSUNTIVO With Vett_Avanz_(2,2) , IMPEGNATO With Vett_Avanz_(2,3) 
 Append Blank 
 Replace Tipo With Vett_Avanz_(3,1), CONSUNTIVO With Vett_Avanz_(3,2) 
 Append Blank 
 Replace Tipo With Vett_Avanz_(4,1), CONSUNTIVO With Vett_Avanz_(4,2), IMPEGNATO With Vett_Avanz_(4,3)
      SELECT TIPO , NVL(VAL(CONSUNTIVO), 0) AS CONSUNTIVO , NVL(VAL(IMPEGNATO), 0)AS IMPEGNATO FROM _AVANZ_ INTO CURSOR _AVANZ1_
      USE IN SELECT("_AVANZ_")
      SELECT * FROM _AVANZ1_ INTO CURSOR _AVANZ_ READWRITE
      USE IN SELECT("_AVANZ1_")
      * --- Controllo abilitazione pulsante di stampa 
      if this.w_STAMPA
        this.oParentObject.w_FLSTAMP1 = reccount("_Avanz_")>0
      endif
      L_ISTRUZIONE="SELECT 'Previste' as TIPO, SUM(PREV) AS "+AH_MSGFORMAT("IMPORTO")+", 'P' as TIPMOV FROM _ORE_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
 L_ISTRUZIONE=L_ISTRUZIONE+"SELECT 'Consuntive' as TIPO, SUM(CONS) AS "+AH_MSGFORMAT("IMPORTO")+", 'C' as TIPMOV FROM _ORE_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
 L_ISTRUZIONE=L_ISTRUZIONE+"SELECT 'Residue' as TIPO, SUM(PREV)-SUM(CONS) AS "+AH_MSGFORMAT("IMPORTO")+", 'E' as TIPMOV FROM _ORE_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" INTO CURSOR _GraphManod_ "
      &L_ISTRUZIONE
       
 DIMENSION Vett_Costi_(4,2) 
 DIMENSION Vett_ImpCos_(4,3)
       
 Select bsc_curs 
 L_PREVENTIVO_MA=NVL(PREVENTIVO_MA,0) 
 L_PREVENTIVO_MD=NVL(PREVENTIVO_MD,0) 
 L_PREVENTIVO_AP=NVL(PREVENTIVO_AP,0) 
 L_PREVENTIVO_AL=NVL(PREVENTIVO_AL,0) 
 L_CONS_MA=NVL(CONS_MA,0) 
 L_CONS_MD=NVL(CONS_MD,0) 
 L_CONS_AP=NVL(CONS_AP,0) 
 L_CONS_AL=NVL(CONS_AL,0) 
 L_ORDI_MA=NVL(ORDI_MA,0) 
 L_ORDI_MD=NVL(ORDI_MD,0) 
 L_ORDI_AP=NVL(ORDI_AP,0) 
 L_ORDI_AL=NVL(ORDI_AL,0) 
 L_CONSORDI_MA=NVL(ORDI_MA,0)+NVL(CONS_MA,0) 
 L_CONSORDI_MD=NVL(ORDI_MD,0)+NVL(CONS_MD,0) 
 L_CONSORDI_AP=NVL(ORDI_AP,0)+NVL(CONS_AP,0) 
 L_CONSORDI_AL=NVL(ORDI_AL,0)+NVL(CONS_AL,0)
      L_PREVENTIVO = L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL
      L_PREVENTIVO = STR(cp_round(L_PREVENTIVO,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
      if L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL <> 0
         
 L_PERCPREVENTIVO_MA=alltrim(str(cp_round(L_PREVENTIVO_MA*100/(L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL),2),6,2)) 
 L_PERCPREVENTIVO_MD=alltrim(str(cp_round(L_PREVENTIVO_MD*100/(L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL),2),6,2)) 
 L_PERCPREVENTIVO_AP=alltrim(str(cp_round(L_PREVENTIVO_AP*100/(L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL),2),6,2)) 
 L_PERCPREVENTIVO_AL=alltrim(str(cp_round(L_PREVENTIVO_AL*100/(L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL),2),6,2))
      else
         
 L_PERCPREVENTIVO_MA="0,00" 
 L_PERCPREVENTIVO_MD="0,00" 
 L_PERCPREVENTIVO_AP="0,00" 
 L_PERCPREVENTIVO_AL="0,00"
        L_PREVENTIVO="0,00"
      endif
      if L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL <> 0
         
 L_PERCCONS_MA=alltrim(str(cp_round(L_CONS_MA*100/(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL),2),6,2)) 
 L_PERCCONS_MD=alltrim(str(cp_round(L_CONS_MD*100/(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL),2),6,2)) 
 L_PERCCONS_AP=alltrim(str(cp_round(L_CONS_AP*100/(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL),2),6,2)) 
 L_PERCCONS_AL=alltrim(str(cp_round(L_CONS_AL*100/(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL),2),6,2))
      else
         
 L_PERCCONS_MA="0,00" 
 L_PERCCONS_MD="0,00" 
 L_PERCCONS_AP="0,00" 
 L_PERCCONS_AL="0,00"
      endif
      if L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL <> 0
         
 L_PERCORDI_MA=alltrim(str(cp_round(L_ORDI_MA*100/(L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL),2),6,2)) 
 L_PERCORDI_MD=alltrim(str(cp_round(L_ORDI_MD*100/(L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL),2),6,2)) 
 L_PERCORDI_AP=alltrim(str(cp_round(L_ORDI_AP*100/(L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL),2),6,2)) 
 L_PERCORDI_AL=alltrim(str(cp_round(L_ORDI_AL*100/(L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL),2),6,2))
      else
         
 L_PERCORDI_MA="0,00" 
 L_PERCORDI_MD="0,00" 
 L_PERCORDI_AP="0,00" 
 L_PERCORDI_AL="0,00"
      endif
      if L_CONSORDI_MA+L_CONSORDI_MD+L_CONSORDI_AP+L_CONSORDI_AL <> 0
         
 L_PERCCONSORDI_MA=alltrim(str(cp_round(L_CONSORDI_MA*100/(L_CONSORDI_MA+L_CONSORDI_MD+L_CONSORDI_AP+L_CONSORDI_AL),2),6,2)) 
 L_PERCCONSORDI_MD=alltrim(str(cp_round(L_CONSORDI_MD*100/(L_CONSORDI_MA+L_CONSORDI_MD+L_CONSORDI_AP+L_CONSORDI_AL),2),6,2)) 
 L_PERCCONSORDI_AP=alltrim(str(cp_round(L_CONSORDI_AP*100/(L_CONSORDI_MA+L_CONSORDI_MD+L_CONSORDI_AP+L_CONSORDI_AL),2),6,2)) 
 L_PERCCONSORDI_AL=alltrim(str(cp_round(L_CONSORDI_AL*100/(L_CONSORDI_MA+L_CONSORDI_MD+L_CONSORDI_AP+L_CONSORDI_AL),2),6,2))
      else
         
 L_PERCCONSORDI_MA="0,00" 
 L_PERCCONSORDI_MD="0,00" 
 L_PERCCONSORDI_AP="0,00" 
 L_PERCCONSORDI_AL="0,00"
      endif
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("PERCENTUALE")+this.w_CRLF 
 Vett_Costi_(1,1) = AH_MSGFORMAT("MATERIALI") 
 Vett_Costi_(1,2) = iif(this.oParentObject.w_TIPOCOST="C",L_PERCCONS_MA,L_PERCCONSORDI_MA) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("MATERIALI")+this.w_TAB+Vett_Costi_(1,2)+this.w_CRLF 
 Vett_Costi_(2,1) = AH_MSGFORMAT("MANODOPERA") 
 Vett_Costi_(2,2) = iif(this.oParentObject.w_TIPOCOST="C",L_PERCCONS_MD,L_PERCCONSORDI_MD) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("MANODOPERA")+this.w_TAB+Vett_Costi_(2,2)+this.w_CRLF 
 Vett_Costi_(3,1) = AH_MSGFORMAT("APPALTI") 
 Vett_Costi_(3,2) = iif(this.oParentObject.w_TIPOCOST="C",L_PERCCONS_AP,L_PERCCONSORDI_AP) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("APPALTI")+this.w_TAB+Vett_Costi_(3,2)+this.w_CRLF 
 Vett_Costi_(4,1) = AH_MSGFORMAT("ALTRO") 
 Vett_Costi_(4,2) = iif(this.oParentObject.w_TIPOCOST="C",L_PERCCONS_AL,L_PERCCONSORDI_AL) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ALTRO")+this.w_TAB+Vett_Costi_(4,2)+this.w_CRLF 
 
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("PREVENTIVO")+this.w_TAB+AH_MSGFORMAT("CONSUNTIVO")+this.w_CRLF 
 Vett_ImpCos_(1,1) = AH_MSGFORMAT("MATERIALI") 
 Vett_ImpCos_(1,2) = str(L_PREVENTIVO_MA,18,2) 
 Vett_ImpCos_(1,3) = STR(iif(this.oParentObject.w_TIPOCOST="C",L_CONS_MA,L_CONSORDI_MA),18,2) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("MATERIALI")+this.w_TAB+Vett_ImpCos_(1,2)+this.w_TAB+Vett_ImpCos_(1,3)+this.w_CRLF 
 Vett_ImpCos_(2,1) = AH_MSGFORMAT("MANODOPERA") 
 Vett_ImpCos_(2,2) = str(L_PREVENTIVO_MD,18,2) 
 Vett_ImpCos_(2,3) = STR(iif(this.oParentObject.w_TIPOCOST="C",L_CONS_MD,L_CONSORDI_MD),18,2) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("MANODOPERA")+this.w_TAB+Vett_ImpCos_(2,2)+this.w_TAB+Vett_ImpCos_(2,3)+this.w_CRLF 
 Vett_ImpCos_(3,1) = AH_MSGFORMAT("APPALTI") 
 Vett_ImpCos_(3,2) = str(L_PREVENTIVO_AP,18,2) 
 Vett_ImpCos_(3,3) = STR(iif(this.oParentObject.w_TIPOCOST="C",L_CONS_AP,L_CONSORDI_AP),18,2) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("APPALTI")+this.w_TAB+Vett_ImpCos_(3,2)+this.w_TAB+Vett_ImpCos_(3,3)+this.w_CRLF 
 Vett_ImpCos_(4,1) = AH_MSGFORMAT("ALTRO") 
 Vett_ImpCos_(4,2) = str(L_PREVENTIVO_AL,18,2) 
 Vett_ImpCos_(4,3) = STR(iif(this.oParentObject.w_TIPOCOST="C",L_CONS_AL,L_CONSORDI_AL),18,2) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ALTRO")+this.w_TAB+Vett_ImpCos_(4,2)+this.w_TAB+Vett_ImpCos_(4,3)+this.w_CRLF
       
 Create cursor _Costi_ (Tipo C(15), Importo N(18,4)) 
 =WrCursor("_Costi_") 
 Append Blank 
 Replace Tipo With Vett_Costi_(1,1), IMPORTO With VAL(Vett_Costi_(1,2)) 
 Append Blank 
 Replace Tipo With Vett_Costi_(2,1), IMPORTO With VAL(Vett_Costi_(2,2)) 
 Append Blank 
 Replace Tipo With Vett_Costi_(3,1), IMPORTO With VAL(Vett_Costi_(3,2)) 
 Append Blank 
 Replace Tipo With Vett_Costi_(4,1), IMPORTO With VAL(Vett_Costi_(4,2)) 
 Create cursor _ImpCos_ (Tipo C(15), Preventivo N(18,4), Consuntivo N(18,4), TipoCosto C(2)) 
 =WrCursor("_ImpCos_") 
 Append Blank 
 Replace Tipo With Vett_ImpCos_(1,1) 
 Replace PREVENTIVO With VAL(Vett_ImpCos_(1,2)) 
 Replace CONSUNTIVO With VAL(Vett_ImpCos_(1,3)) 
 Replace TIPOCOSTO With "MA" 
 Append Blank 
 Replace Tipo With Vett_ImpCos_(2,1) 
 Replace PREVENTIVO With VAL(Vett_ImpCos_(2,2)) 
 Replace CONSUNTIVO With VAL(Vett_ImpCos_(2,3)) 
 Replace TIPOCOSTO With "MD" 
 Append Blank 
 Replace Tipo With Vett_ImpCos_(3,1) 
 Replace PREVENTIVO With VAL(Vett_ImpCos_(3,2)) 
 Replace CONSUNTIVO With VAL(Vett_ImpCos_(3,3)) 
 Replace TIPOCOSTO With "AP" 
 Append Blank 
 Replace Tipo With Vett_ImpCos_(4,1) 
 Replace PREVENTIVO With VAL(Vett_ImpCos_(4,2)) 
 Replace CONSUNTIVO With VAL(Vett_ImpCos_(4,3)) 
 Replace TIPOCOSTO With "AL"
      if Used("bsc_curs")
        Use in bsc_curs
      endif
      * --- Esegue gli assegnamenti per le variabili di pag4
      *     La terza colonna del vettore VettorePag4 contiene val(STAMPA_A),
      *     la quarta colonna contiene val(STAMPA_B) e la quinta val(STAMPA_C)
      L_FatturatoAttuale = STR(cp_round(VettorePag4(1,4),this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
      if VettorePag4(1,3) <> 0
        L_percFatturatoAttuale = str(100*VettorePag4(1,4)/VettRicavi(1,3),18,2)
      else
        L_percFatturatoAttuale = "0,00"
      endif
      L_MaturatoNonFatturato = STR(cp_round(VettorePag4(1,5),this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
      if VettorePag4(1,3) <> 0
        L_percMaturatoNonFatturato = str(100*VettorePag4(1,5)/VettRicavi(1,3),18,2)
      else
        L_percMaturatoNonFatturato = "0,00"
      endif
      L_ConsuntivoPag4 = STR(cp_round(VettorePag4(2,4),this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
      if VettorePag4(2,3) <> 0
        L_percConsuntivoPag4 = str(100*VettorePag4(2,4)/VettorePag4(2,3),18,2)
      else
        L_percConsuntivoPag4 = "0,00"
      endif
      L_ImpegniFinanziariPag4 = STR(cp_round(VettorePag4(2,5),this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
      if VettorePag4(2,5) <> 0
        L_percImpegniFinanziariPag4 = str(100*VettorePag4(2,5)/VettorePag4(2,3),18,2)
      else
        L_percImpegniFinanziariPag4 = "0,00"
      endif
       
 Dimension Vett_CosRic_(3,3)
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("RICAVI")+this.w_TAB+AH_MSGFORMAT("COSTI")+this.w_CRLF 
 Vett_CosRic_(1,1) = AH_MSGFORMAT("ATTUALI") 
 Vett_CosRic_(1,2) = STR(cp_round(VettRicavi(1,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_CosRic_(1,3) = STR(cp_round(VettorePag4(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ATTUALI")+this.w_TAB+Vett_CosRic_(1,2)+this.w_TAB+Vett_CosRic_(1,3)+this.w_CRLF 
 Vett_CosRic_(2,1) = AH_MSGFORMAT("RESIDUI") 
 Vett_CosRic_(2,2) = STR(cp_round(VettRicavi(2,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 IF VettorePag4(2,1) > VettorePag4(2,3) 
 Vett_CosRic_(2,3) = STR(cp_round(0,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 ENDIF 
 IF VettorePag4(2,1) <= VettorePag4(2,3) 
 Vett_CosRic_(2,3) = STR(cp_round(VettorePag4(2,2),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 ENDIF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("RESIDUI")+this.w_TAB+Vett_CosRic_(2,2)+this.w_TAB+Vett_CosRic_(2,3)+this.w_CRLF 
 Vett_CosRic_(3,1) = AH_MSGFORMAT("ATTESI") 
 Vett_CosRic_(3,2) = STR(cp_round(VettRicavi(3,1),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 Vett_CosRic_(3,3) = STR(cp_round(VettorePag4(2,3),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ATTESI")+this.w_TAB+Vett_CosRic_(3,2)+this.w_TAB+Vett_CosRic_(3,3)+this.w_CRLF
       
 Create Cursor _CosRic_ (Tipo C (15), Ricavi N(18,4) , Costi N(18,4)) 
 =WrCursor("_CosRic_") 
 Append Blank 
 Replace Tipo with Vett_CosRic_(1,1), Ricavi with VAL(Vett_CosRic_(1,2)), Costi with VAL(Vett_CosRic_(1,3)) 
 Append Blank 
 Replace Tipo with Vett_CosRic_(2,1), Ricavi with VAL(Vett_CosRic_(2,2)), Costi with VAL(Vett_CosRic_(2,3)) 
 Append Blank 
 Replace Tipo with Vett_CosRic_(3,1), Ricavi with VAL(Vett_CosRic_(3,2)), Costi with VAL(Vett_CosRic_(3,3))
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("RICAVI")+this.w_TAB+AH_MSGFORMAT("COSTI")+this.w_TAB+AH_MSGFORMAT("DIFFERENZA")+this.w_CRLF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ATTUALI")+this.w_TAB+Vett_Mar(1,2)+this.w_TAB+STR(cp_round(VettorePag4(2,4)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)+this.w_TAB+Vett_Mar(1,4)+this.w_CRLF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("IMPEGNI")+this.w_TAB+""+this.w_TAB+STR(cp_round(VettorePag4(2,5)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)+this.w_TAB+""+this.w_CRLF 
 L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("RICAVI")+this.w_TAB+AH_MSGFORMAT("COSTI")+this.w_TAB+AH_MSGFORMAT("DIFFERENZA")+this.w_CRLF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("RESIDUI")+this.w_TAB+Vett_Mar(2,2)+this.w_TAB+Vett_Mar(2,3)+this.w_TAB+Vett_Mar(2,4)+this.w_CRLF 
 
 L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("RICAVI")+this.w_TAB+AH_MSGFORMAT("COSTI")+this.w_TAB+AH_MSGFORMAT("DIFFERENZA")+this.w_CRLF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("ATTESI")+this.w_TAB+Vett_Mar(3,2)+this.w_TAB+Vett_Mar(3,3)+this.w_TAB+Vett_Mar(3,4)+this.w_CRLF 
 
      Create Cursor _MarAgg_ (Tipo C (15), Impegni N(18,4), Attuali N(18,4)) 
 =WrCursor("_MarAgg_") 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Ricavi") 
 Replace Attuali with VAL(Vett_Mar(1,2)), Impegni with nvl(VAL(""),0) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Costi") 
 Replace Attuali with VAL(STR(cp_round(VettorePag4(2,4)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)) 
 if this.oParentObject.w_TIPOGRA="I" 
 Replace Impegni with VAL(STR(cp_round(VettorePag4(2,5)/L_ML,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)) 
 else 
 Replace Impegni with L_PercImpePag5 
 endif 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Margine") 
 Replace Attuali with nvl(VAL(Vett_Mar(1,4)),0), Impegni with nvl(VAL(""),0)
      Create Cursor _MarFin_ (Tipo C (15), Residui N (20,5)) 
 =WrCursor("_MarFin_") 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Ricavi") 
 Replace Residui with VAL(Vett_Mar(2,2)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Costi") 
 Replace Residui with VAL(Vett_Mar(2,3)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Margine") 
 Replace Residui with VAL(Vett_Mar(2,4))
      Create Cursor _MarAtt_ (Tipo C (15), Attesi N (20,5)) 
 =WrCursor("_MarAtt_") 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Ricavi") 
 Replace Attesi with VAL(Vett_Mar(3,2)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Costi") 
 Replace Attesi with VAL(Vett_Mar(3,3)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Margine") 
 Replace Attesi with VAL(Vett_Mar(3,4))
      L_DatiGrafico=" "+this.w_TAB+AH_MSGFORMAT("ATTUALE")+this.w_TAB+AH_MSGFORMAT("RESIDUO")+this.w_TAB+AH_MSGFORMAT("ATTESO")+this.w_CRLF 
 L_DatiGrafico=L_DatiGrafico+AH_MSGFORMAT("MARGINE")+this.w_TAB 
 L_DatiGrafico=L_DatiGrafico+Vett_Mar(4,2)+this.w_TAB 
 L_DatiGrafico=L_DatiGrafico+Vett_Mar(4,3)+this.w_TAB 
 L_DatiGrafico=L_DatiGrafico+Vett_Mar(4,4)+this.w_CRLF 
 
      this.oParentObject.w_VALORE_PER_MARAT = Val(Vett_Mar(4,2))
      this.oParentObject.w_VALORE_PER_MARRE = Val(Vett_Mar(4,3))
      this.oParentObject.w_VALORE_PER_MARTO = Val(Vett_Mar(4,4))
      Create Cursor _MarPer_ (Tipo C (15), Margine N (20,5)) 
 =WrCursor("_MarPer_") 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Attuale") 
 Replace Margine with VAL(Vett_Mar(4,2)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Residuo") 
 Replace Margine with VAL(Vett_Mar(4,3)) 
 Append Blank 
 Replace Tipo with AH_MSGFORMAT("Atteso") 
 Replace Margine with VAL(Vett_Mar(4,4))
      this.oParentObject.w_M_PERCCONS_MA = L_PERCCONS_MA
      this.oParentObject.w_M_PERCCONS_MD = L_PERCCONS_MD
      this.oParentObject.w_M_PERCCONS_AP = L_PERCCONS_AP
      this.oParentObject.w_M_PERCCONS_AL = L_PERCCONS_AL
      this.oParentObject.w_M_PERCCONSORDI_MA = L_PERCCONSORDI_MA
      this.oParentObject.w_M_PERCCONSORDI_MD = L_PERCCONSORDI_MD
      this.oParentObject.w_M_PERCCONSORDI_AP = L_PERCCONSORDI_AP
      this.oParentObject.w_M_PERCCONSORDI_AL = L_PERCCONSORDI_AL
      this.oParentObject.w_M_PERCPREVENTIVO_MA = L_PERCPREVENTIVO_MA
      this.oParentObject.w_M_PERCPREVENTIVO_MD = L_PERCPREVENTIVO_MD
      this.oParentObject.w_M_PERCPREVENTIVO_AP = L_PERCPREVENTIVO_AP
      this.oParentObject.w_M_PERCPREVENTIVO_AL = L_PERCPREVENTIVO_AL
      this.oParentObject.w_M_PERCORDI_MA = L_PERCORDI_MA
      this.oParentObject.w_M_PERCORDI_MD = L_PERCORDI_MD
      this.oParentObject.w_M_PERCORDI_AP = L_PERCORDI_AP
      this.oParentObject.w_M_PERCORDI_AL = L_PERCORDI_AL
      this.oParentObject.w_M_FATTURATOATTUALE = L_FATTURATOATTUALE
      this.oParentObject.w_M_MATURATONONFATTURATO = L_MATURATONONFATTURATO
      this.oParentObject.w_M_PERCFATTURATOATTUALE = L_PERCFATTURATOATTUALE
      this.oParentObject.w_M_PERCMATURATONONFATTURATO = L_PERCMATURATONONFATTURATO
      this.oParentObject.w_M_AVANTECNNONDISP = L_AVANTECNNONDISP
      this.oParentObject.w_M_CONSUNTIVOPAG4 = L_CONSUNTIVOPAG4
      this.oParentObject.w_M_IMPEGNIFINANZIARIPAG4 = L_IMPEGNIFINANZIARIPAG4
      this.oParentObject.w_M_PERCCONSUNTIVOPAG4 = L_PERCCONSUNTIVOPAG4
      this.oParentObject.w_M_PERCIMPEGNIFINANZIARIPAG4 = L_PERCIMPEGNIFINANZIARIPAG4
      this.oParentObject.w_M_PREVENTIVO = L_PREVENTIVO
    endif
    if this.pPARAM="VISUA"
      * --- ALCUNI GRAFICI VISUALIZZANO I NOMI DELLE COLONNE, PER QUESTO I CURSORI VANNO TRADOTTI
      * --- _IMPCOS_
      L_ISTRUZIONE="SELECT TIPO, PREVENTIVO AS "+AH_MSGFORMAT("PREVENTIVO")+", CONSUNTIVO AS "+AH_MSGFORMAT("CONSUNTIVO") ; 
 +", TIPOCOSTO AS "+AH_MSGFORMAT("TIPOCOSTO")+" FROM _IMPCOS_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _IMPCOS_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _IMPCOS_
      * --- _COSRIC_
      L_ISTRUZIONE="SELECT TIPO, COSTI AS "+AH_MSGFORMAT("COSTI")+", RICAVI AS "+AH_MSGFORMAT("RICAVI")+" FROM _COSRIC_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _COSRIC_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _COSRIC_
      * --- _MARAGG_
      L_ISTRUZIONE="SELECT TIPO, ATTUALI AS "+AH_MSGFORMAT("ATTUALI")+", IMPEGNI AS "+AH_MSGFORMAT("IMPEGNI")+" FROM _MARAGG_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _MARAGG_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _MARAGG_
      * --- _MARFIN_
      L_ISTRUZIONE="SELECT TIPO, RESIDUI AS "+AH_MSGFORMAT("RESIDUI") + " FROM _MARFIN_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _MARFIN_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _MARFIN_
      * --- _MARATT_
      L_ISTRUZIONE="SELECT TIPO, ATTESI AS "+AH_MSGFORMAT("ATTESI")+" FROM _MARATT_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _MARATT_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _MARATT_
      * --- _MARPER_
      L_ISTRUZIONE="SELECT TIPO, MARGINE AS "+AH_MSGFORMAT("MARGINE")+" FROM _MARPER_ INTO CURSOR CURSORETRADOTTO"
      &L_ISTRUZIONE
      SELECT _MARPER_
      USE
      SELECT * FROM CURSORETRADOTTO INTO CURSOR _MARPER_
      * --- elimina cursore tradotto
      SELECT CURSORETRADOTTO
      USE
      L_ISTRUZIONE="SELECT 'Attuale' as TIPO, ATTUALI AS "+AH_MSGFORMAT("IMPORTO")+" FROM _MARAGG_ Where TIPO='Margine' "
      L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
      L_ISTRUZIONE=L_ISTRUZIONE+ "SELECT 'Residuo' as TIPO, RESIDUI AS "+AH_MSGFORMAT("IMPORTO") + " FROM _MARFIN_  Where TIPO='Margine'  "
      L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
      L_ISTRUZIONE=L_ISTRUZIONE+ "SELECT 'Atteso' as TIPO , ATTESI AS IMPORTO FROM _MARATT_  WHERE TIPO='Margine'  INTO CURSOR _GraphMargine_ "
      &L_ISTRUZIONE
      select _GraphMargine_
      LOCATE FOR TIPO ="Attuale"
      this.oParentObject.w_VALORE_MARAT = str( _GraphMargine_.Importo, this.w_nLenght, this.w_nDecimal)
      select _GraphMargine_
      LOCATE FOR TIPO ="Residuo"
      this.oParentObject.w_VALORE_MARRE = str( _GraphMargine_.Importo, this.w_nLenght, this.w_nDecimal)
      select _GraphMargine_
      LOCATE FOR TIPO ="Atteso"
      this.oParentObject.w_VALORE_MARTO = str( _GraphMargine_.Importo, this.w_nLenght, this.w_nDecimal)
      This.oParentobject.Notifyevent("GraphPag1") 
 This.oParentobject.Notifyevent("GraphPag2") 
 This.oParentobject.Notifyevent("GraphPag3") 
 This.oParentobject.Notifyevent("GraphPag4") 
 This.oParentobject.Notifyevent("GraphPag5") 
 This.oParentobject.Notifyevent("GraphPag6") 
 This.oParentobject.Notifyevent("GraphPag7")
      This.oParentobject.mCalc(.T.)
      This.oParentobject.Notifyevent("AggiornaGauge")
      This.bUpdateParentObject=.f.
    endif
    if this.pPARAM="PRINT"
      L_FORMATO = This.oParentObject.w_FORMATO
      * --- Variabili report relative a indicatori %
      L_VALORE_PER_ATEC = This.oParentObject.w_VALORE_PER_ATEC
      L_VALORE_PER_MDO = This.oParentObject.w_VALORE_PER_MDO
      L_VALORE_PER_COSTI = This.oParentObject.w_VALORE_PER_COSTI
      L_AVANZ_RICAVI = This.oParentObject.w_AVANZ_RICAVI
      L_VALORE_PER_MARAT = This.oParentObject.w_VALORE_PER_MARAT
      L_VALORE_PER_MARRE = This.oParentObject.w_VALORE_PER_MARRE
      L_VALORE_PER_MARTO = This.oParentObject.w_VALORE_PER_MARTO
      L_VALORE_OBIETTIVO = This.oParentObject.w_VALORE_OBIETTIVO
      * --- Variabili report relative ai costi
      L_M_PREVENTIVO = val(this.oParentObject.w_M_PREVENTIVO)
      L_M_CONSUNTIVOPAG4 = VAL(this.oParentObject.w_M_CONSUNTIVOPAG4)
      L_M_IMPEGNIFINANZIARIPAG4 = VAL(this.oParentObject.w_M_IMPEGNIFINANZIARIPAG4)
      L_VALORE_AFINIRE = VAL(This.oParentObject.w_VALORE_AFINIRE)
      * --- Variabili report relative ai ricavi
      L_M_RICAVIPREVISTI = VAL(This.oParentObject.w_M_RICAVIPREVISTI)
      L_M_RICAVICONSEGUITI = VAL(This.oParentObject.w_M_RICAVICONSEGUITI)
      L_M_RICAVIRESIDUI = VAL(This.oParentObject.w_M_RICAVIRESIDUI)
      L_M_RICAVISAL = VAL(This.oParentObject.w_M_RICAVISAL)
      * --- Variabili report relative al margine
      L_VALORE_PER_MARAT = This.oParentObject.w_VALORE_PER_MARAT
      L_VALORE_PER_MARRE = This.oParentObject.w_VALORE_PER_MARRE
      L_VALORE_PER_MARTO = This.oParentObject.w_VALORE_PER_MARTO
      * --- Variabili report relative alle ore di mdo
      L_TOTOREPREV = This.oParentObject.w_TOTOREPREV
      L_TOTORECONS = This.oParentObject.w_TOTORECONS
      L_TOTORERES = This.oParentObject.w_TOTORERES
      * --- Salvo le immagini delle GaugeBar
      this.oParentObject.w_GAUGETEC.SaveBitmap()     
      this.oParentObject.w_GAUGEMDO.SaveBitmap()     
      this.oParentObject.w_GAUGECOSTI.SaveBitmap()     
      this.oParentObject.w_GAUGERICAVI.SaveBitmap()     
      this.oParentObject.w_GAUGEMARAT.SaveBitmap()     
      this.oParentObject.w_GAUGEMARRE.SaveBitmap()     
      this.oParentObject.w_GAUGEMARTO.SaveBitmap()     
      this.oParentObject.w_GAUGEOB.SaveBitmap()     
      L_PicTec = this.oParentObject.w_GAUGETEC.cBitmapFile 
 L_PicMDO = this.oParentObject.w_GAUGEMDO.cBitmapFile 
 L_PicCosti = this.oParentObject.w_GAUGECOSTI.cBitmapFile 
 L_PicRicavi = this.oParentObject.w_GAUGERICAVI.cBitmapFile 
 L_PicMarat = this.oParentObject.w_GAUGEMARAT.cBitmapFile 
 L_PicMarre = this.oParentObject.w_GAUGEMARRE.cBitmapFile 
 L_PicMarto = this.oParentObject.w_GAUGEMARTO.cBitmapFile 
 L_PicOb = this.oParentObject.w_GAUGEOB.cBitmapFile
      * --- Crea cursore con i grafici da utilizzare nella stampa, da TamGraph, ed esegue la numerazione
      *     delle pagine
      l_MemoryStream13 = ""
      l_MemoryStream14 = ""
      l_MemoryStream15 = ""
      l_MemoryStream16 = ""
      USE IN SELECT("cursorigrafici") 
 CREATE CURSOR cursorigrafici (olegraph M, nome C(30), ; 
 nVal1 N(18,5) , nVal2 N(18,5) , nVal3 N(18,5) , nVal4 N(18,5) , ; 
 cCap1 C(20) , cCap2 C(20) , cCap3 C(20) , cCap4 C(20) , pagina N(2), page N(2) ) 
 =WrCursor ("CursoriGrafici")
      l_MemoryStream13 = This.oParentobject.w_Graph13.oChart.DrawReport(600 , 300)
      l_MemoryStream14 = This.oParentobject.w_Graph14.oChart.DrawReport(600 , 300)
      l_MemoryStream15 = This.oParentobject.w_Graph15.oChart.DrawReport(600 , 300)
      l_MemoryStream16 = This.oParentobject.w_Graph16.oChart.DrawReport(600 , 300)
      APPEND BLANK
      REPLACE nome with "Costi per tipologia", pagina with 1, page with 1,olegraph with l_MemoryStream13, ; 
 nVal1 with L_M_PREVENTIVO, nVal2 with L_M_CONSUNTIVOPAG4, nVal3 with L_M_IMPEGNIFINANZIARIPAG4, nVal4 with L_VALORE_AFINIRE , ; 
 cCap1 with "Preventivo" , cCap2 with "consuntivo" , cCap3 with "Imp. finanziari" , cCap4 with "A finire"
      APPEND BLANK
      REPLACE nome with "Ricavi", pagina with 1, page with 2, olegraph with l_MemoryStream14, ; 
 nVal1 with L_M_RICAVIPREVISTI, nVal2 with L_M_RICAVICONSEGUITI, nVal3 with L_M_RICAVIRESIDUI, nVal4 with L_M_RICAVISAL, ; 
 cCap1 with "Previsti" , cCap2 with "Conseguiti" , cCap3 with "Residui" , cCap4 with "SAL"
      APPEND BLANK
      REPLACE nome with "Margine", pagina with 1, page with 3, olegraph with l_MemoryStream15 ,; 
 nVal1 with L_VALORE_PER_MARAT, nVal2 with L_VALORE_PER_MARRE, nVal3 with L_VALORE_PER_MARTO, nVal4 with 0, ; 
 cCap1 with "Attuale" , cCap2 with "Residuo" , cCap3 with "Atteso" , cCap4 with ""
      APPEND BLANK
      REPLACE nome with "Manodopera", pagina with 1, page with 4, olegraph with l_MemoryStream16 ,; 
 nVal1 with L_TOTOREPREV, nVal2 with L_TOTORECONS, nVal3 with L_TOTORERES, nVal4 with 0, ; 
 cCap1 with "Previste" , cCap2 with "Consuntive" , cCap3 with "Residue" , cCap4 with ""
      select * from cursorigrafici into cursor __tmp__ 
 CP_CHPRN("..\COMM\EXE\QUERY\GSPC1BCC.FRX")
    endif
    if this.pPARAM="PRIN2"
      * --- SCEGLIE LE PAGINE DA STAMPARE
      this.w_FILTRO = ""
      this.w_FILTRO = this.w_FILTRO+"-"+IIF(this.oParentObject.w_PRNTGR_AV="S","1","")
      this.w_FILTRO = this.w_FILTRO+"-"+IIF(this.oParentObject.w_PRNTGR_CT="S","2-3","")
      this.w_FILTRO = this.w_FILTRO+"-"+IIF(this.oParentObject.w_PRNTGR_RC="S","4","")
      this.w_FILTRO = this.w_FILTRO+"-"+IIF(this.oParentObject.w_PRNTGR_MA="S","5-6-7-8","")
      select * from cursorigrafici into cursor __tmp__ 
 CP_CHPRN("..\COMM\EXE\QUERY\GSPC1BCC.FRX")
    endif
    if this.pPARAM="EXIT_"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione parametri da passare alla CP_EXPDB
    this.oParentObject.w_COUTCURS = Sys(2015)
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "STMILLES"
    * --- Altri campi
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDUNIQUE,PDUMCOMM,PDRICART"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC("TAM");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDUNIQUE,PDUMCOMM,PDRICART;
        from (i_cTable) where;
            PDCHIAVE = "TAM";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNIQUE = NVL(cp_ToDate(_read_.PDUNIQUE),cp_NullValue(_read_.PDUNIQUE))
      this.w_UMDFLT = NVL(cp_ToDate(_read_.PDUMCOMM),cp_NullValue(_read_.PDUMCOMM))
      this.w_RICART = NVL(cp_ToDate(_read_.PDRICART),cp_NullValue(_read_.PDRICART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TIPSTR = iif(nvl(this.w_UNIQUE," ")="S","P","T")
    * --- Nel caso di ad hoc Revolution bisogna sempre leggere dal progetto amministrativo
    if g_APPLICATION="ADHOC REVOLUTION"
      this.w_TIPSTR = "P"
    endif
    this.w_TIPCOM = "P"
    * --- Verifica dell'esistenza (del capoprogetto) della struttura. Se la struttura non esiste l'export non viene effettuato.
    * --- La struttura pu� non esistere per i seguenti motivi:
    * --- 1. la struttura gestionale non � stata creata dopo essere passati da una a tre strutture nella gestione del progetto
    * --- 2. la commessa � stata creata dall'analitica e non sono stati creati i capiprogetto
    this.w_STOP = .T.
    * --- Select from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
          +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOM)+" and ATTIPATT="+cp_ToStrODBC(this.w_TIPSTR)+" and ATTIPCOM="+cp_ToStrODBC(this.w_TIPCOM)+"";
           ,"_Curs_ATTIVITA")
    else
      select * from (i_cTable);
       where ATCODCOM=this.oParentObject.w_CODCOM and ATTIPATT=this.w_TIPSTR and ATTIPCOM=this.w_TIPCOM;
        into cursor _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      select _Curs_ATTIVITA
      locate for 1=1
      do while not(eof())
      * --- Se esiste il capoprogetto permetto al batch di continuare
      this.w_STOP = .F.
        select _Curs_ATTIVITA
        continue
      enddo
      use
    endif
    if this.w_STOP
      * --- Nel caso in cui la struttura sia vuota l'export non viene eseguito
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_TIPSTR="T" 
        this.w_MESS = "Attenzione: la struttura tecnica non esiste%0Impossibile visualizzare o stampare gli avanzamenti di commessa"
      else
        this.w_MESS = "Attenzione: la struttura amministrativa non esiste%0Impossibile visualizzare o stampare gli avanzamenti di commessa"
      endif
      ah_ErrorMsg(this.w_MESS,48)
      if this.w_STAMPA
        this.oParentObject.w_FLSTAMP1 = .f.
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Se la struttura esiste proseguo con la visualizzazione degli avanzamenti di commessa
    * --- Costruisco con una query contentente la base per l'esplosione
    VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.oParentObject.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      AH_Msg( "Nessun dato",.f.)
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select *, 1000*0 As AvanTec from ( this.oParentObject.w_COUTCURS ) Into Cursor _Tecn_ Order By LvlKey Desc NoFilter
    Cur =WrCursor ("_Tecn_")
    * --- Calcolo la percentuale di avanzamento del Tecnico
    * --- Eseguo il solito algoritmo (con qualche variante) per calcolare il totale applicato alle percentuali
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- CURSORE DI STAMPA
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 1 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "TECNICO" ) 
 REPLACE stampa_A WITH STR(this.w_PERCOMP,18,2) 
 REPLACE stampa_B WITH SPACE(18) 
 REPLACE stampa_C WITH SPACE(18) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH this.w_PERCOMP 
 REPLACE n_B WITH 0 
 REPLACE n_C WITH 0 
 REPLACE n_D WITH 0
    * --- RICAVI
    * --- GSPC_BAC
    * --- Calcolo i Ricavi
    * --- Leggo gli ordini a cliente aventi sulla riga la commessa (e non l'attivit�)
    * --- Il 100 sar� dato dal totale dei documenti di questo tipo
    * --- Mentre la percentuale sar� data dal totale con data eff. evasione inferiore o uguale alla data nella maschera
    * --- Calcolo i Ricavi
    * --- Leggo gli ordini a cliente aventi sulla riga la commessa (e non l'attivit�)
    * --- Il 100 sar� dato dal totale dei documenti di questo tipo
    * --- Mentre la percentuale sar� data dal totale con data eff. evasione inferiore o uguale alla data nella maschera
    Vq_Exec ("..\COMM\EXE\QUERY\GSPC_BGR.VQR",This,"_Ricavi2_")
    =wrcursor("_Ricavi2_")
    scan for mvcodval<>this.oParentObject.w_CODVAL
    * --- Gli importi vengono convertiti nella valuta di commessa
    * --- CALCOLA IL CAMBIO ATTUALE TRA LA VALUTA DI COMMESSA E LA VAUTA DELL'ESERCIZIO CORRENTE
    this.w_CAMBIOCOMMESER = GETCAM( this.oParentObject.w_CODVAL, i_DATSYS )
    * --- CONVERTE GLI IMPORTI IN VALUTA DI COMMESSA
    REPLACE PERCOMM WITH VAL2VAL( PERCOMM, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    REPLACE TOTCOMM WITH VAL2VAL( TOTCOMM, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    REPLACE FATCOMM WITH VAL2VAL( FATCOMM, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    REPLACE PERCOOR WITH VAL2VAL( PERCOOR, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    REPLACE PERCODT WITH VAL2VAL( PERCODT, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    REPLACE PERCODT WITH VAL2VAL( RESCOMM, MVCAOVAL, CP_TODATE( MVDATDOC ), i_DATSYS, this.w_CAMBIOCOMMESER, this.w_DECCOM )
    endscan
    Select Sum(TotComm) As TotComm, Sum(PerComm) As PerComm,Sum(FatComm) As FatComm,; 
 Sum(PerCoor) As PerCoor, Sum(PerCodt) As PerCodt, sum(rescomm) as rescomm, sum(SalComm) as SalComm; 
 from _Ricavi2_ Into Cursor _Ricavi_ NoFilter
    L_ISTRUZIONE="SELECT 'Previsti' as TIPO, TOTCOMM AS "+AH_MSGFORMAT("IMPORTO")+", 'O' as TIPMOV FROM _Ricavi_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
 L_ISTRUZIONE=L_ISTRUZIONE+"SELECT 'Conseguiti' as TIPO, FATCOMM AS "+AH_MSGFORMAT("IMPORTO")+", 'F' as TIPMOV FROM _Ricavi_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
 L_ISTRUZIONE=L_ISTRUZIONE+"SELECT 'Residui' as TIPO, RESCOMM AS "+AH_MSGFORMAT("IMPORTO")+", 'R' as TIPMOV FROM _Ricavi_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" UNION ALL " 
 L_ISTRUZIONE=L_ISTRUZIONE+"SELECT 'SAL' as TIPO, SALCOMM AS "+AH_MSGFORMAT("IMPORTO")+", 'A' as TIPMOV FROM _Ricavi_ " 
 L_ISTRUZIONE=L_ISTRUZIONE+" INTO CURSOR _GraphRicavi_ "
    &L_ISTRUZIONE
    use in SELECT("_Ricavi2_")
    L_RicPrevisto=_Ricavi_.TotComm 
 L_RicAvanzamento=_Ricavi_.PerComm 
 L_RicFatturato=_Ricavi_.FatComm 
 L_RicAvaOr=_Ricavi_.PerCoor 
 L_RicAvaDt=_Ricavi_.PerCodt
    L_RicResiduo =_Ricavi_.ResComm
    L_RicSAL =_Ricavi_.SalComm
    this.w_PERCOMP = 100 * ( Nvl(_Ricavi_.PerComm,0) / Nvl(_Ricavi_.TotComm,0) )
    this.oParentObject.w_M_RICAVIPREVISTI = STR(cp_round( L_RicPrevisto ,this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
    this.oParentObject.w_M_RICAVICONSEGUITI = STR(cp_round( L_RicFatturato, this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
    this.oParentObject.w_M_RICAVIRESIDUI = STR(cp_round( L_RicResiduo , this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
    this.oParentObject.w_M_RICAVISAL = STR(cp_round( L_RicSAL, this.w_DECCOM),this.w_nLenght, this.w_nDecimal)
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 1 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH "RICAVI" 
 REPLACE stampa_A WITH STR(cp_round(L_RicPrevisto,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_RicFatturato,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_RicAvanzamento,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH STR(cp_round(L_RicFatturato+L_RicAvaDt,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH L_RicPrevisto 
 REPLACE n_B WITH L_RicFatturato 
 REPLACE n_C WITH L_RicAvanzamento 
 REPLACE n_D WITH L_RicFatturato+L_RicAvaDt
    * --- Calcolo la Manodopera
    * --- Recupero i dati esclusivamente dai movimenti preventivi e consuntivi di commessa con Costo di tipo manodopera
    this.w_TOTPREV = 0
    * --- Contatore per totale preventivato (tempi!!!)
    this.w_TOTCONS = 0
    * --- Contatore per totale consuntivato (tempi!!!)
    Vq_Exec ("..\Comm\Exe\Query\Gspc3BGR.Vqr",This,"_Ore_")
    SELECT "_Ore_"
    GO TOP
    SCAN
    if MAUNIMIS=this.w_UMDFLT
      * --- L'UM � gi� quella giusta: incremento direttamente i contatori
      this.w_TOTPREV = this.w_TOTPREV+PREV
      this.w_TOTCONS = this.w_TOTCONS+CONS
    else
      * --- Faccio le verifiche sull'articolo (una delle due unit� di misura deve essere quella di default
      * --- a seconda dei casi si usa il corretto fattore di conversione
      this.w_CODART = MACODART
      if .F.
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = 'impossibile leggere da ART_ICOL'
          return
        endif
        select (i_nOldArea)
      endif
      this.w_UNMIS1 = ARUNMIS1
      this.w_OPERAT = NVL(AROPERAT, "*")
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
      * --- leggo le UM e i dati per la conversione sull'articolo
      * --- N.B.: UM movimento <> UM default
      if nvl(this.w_UNMIS1,"")=MAUNIMIS and nvl(this.w_UNMIS2,"")=this.w_UMDFLT
        * --- l'UM del movimento � la prima UM (UNIMIS1)
        this.w_PREV = IIF(this.w_OPERAT="*",PREV*this.w_MOLTIP,IIF(this.w_OPERAT="/",PREV/this.w_MOLTIP,0))
        this.w_CONS = IIF(this.w_OPERAT="*",CONS*this.w_MOLTIP,IIF(this.w_OPERAT="/",CONS/this.w_MOLTIP,0))
      else
        if nvl(this.w_UNMIS2,"")=MAUNIMIS and nvl(this.w_UNMIS1,"")=this.w_UMDFLT
          * --- l'UM del movimento � la seconda UM (UNIMIS2)
          this.w_PREV = IIF(this.w_OPERAT="*",PREV/this.w_MOLTIP,IIF(this.w_OPERAT="/",PREV*this.w_MOLTIP,0))
          this.w_CONS = IIF(this.w_OPERAT="*",CONS/this.w_MOLTIP,IIF(this.w_OPERAT="/",CONS*this.w_MOLTIP,0))
        else
          this.w_PREV = 0
          this.w_CONS = 0
        endif
      endif
      this.w_TOTPREV = this.w_TOTPREV+this.w_PREV
      this.w_TOTCONS = this.w_TOTCONS+this.w_CONS
      replace prev with this.w_PREV, cons with this.w_CONS
    endif
    SELECT "_Ore_"
    ENDSCAN
    * --- SU _Ore_
    if this.w_TOTPREV <>0 AND this.w_TOTCONS <>0
      this.w_PERCOMP = (this.w_TOTCONS / this.w_TOTPREV)*100
    else
      this.w_PERCOMP = 0
    endif
    this.oParentObject.w_TOTOREPREV = this.w_TOTPREV
    this.oParentObject.w_TOTORECONS = this.w_TOTCONS
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 1 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MANODOPERA" ) 
 REPLACE stampa_A WITH STR(this.w_TotPrev,6,2) 
 REPLACE stampa_B WITH STR(this.w_TotCons,6,2) 
 REPLACE stampa_C WITH SPACE(18) 
 REPLACE stampa_D WITH space(18) 
 REPLACE n_A WITH this.w_TotPrev 
 REPLACE n_B WITH this.w_TotCons 
 REPLACE n_C WITH 0 
 REPLACE n_D WITH 0
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo i cursori
    if Used("Stampa")
      Use in Stampa
    endif
    if Used("CursoriGrafici")
      Use in CursoriGrafici
    endif
    if Used("Pag1Grafico")
      Use in Pag1Grafico
    endif
    if Used("Pag2Grafico")
      Use in Pag2Grafico
    endif
    if Used("Forfait")
      Use in Forfait
    endif
    if Used("bsc_curs")
      Use in bsc_curs
    endif
    if Used("__TMP__")
      Use in __TMP__
    endif
    if Used("_Tecn_")
      Use in _Tecn_
    endif
    if Used("Query")
      Use in Query
    endif
    if Used("Importi")
      Use in Importi
    endif
    if Used("Importi2")
      Use in Importi2
    endif
    if Used("Importi3")
      Use in Importi3
    endif
    if Used("Importi4")
      Use in Importi4
    endif
    if Used("Importi5")
      Use in Importi5
    endif
    if Used("Importi6")
      Use in Importi6
    endif
    if Used("Importi8")
      Use in Importi8
    endif
    if Used("_ore_")
      Use in _ore_
    endif
    if Used(this.oParentObject.w_COUTCURS)
      Use in ( this.oParentObject.w_COUTCURS )
    endif
    if Used("_Ricavi_")
      Use in _Ricavi_
    endif
    if Used("_Costi1_")
      Use in _Costi1_
    endif
    if Used("__PAG1__")
      Use in __PAG1__
    endif
    if Used("__PAG2__")
      Use in __PAG2__
    endif
    if Used("__PAG3__")
      Use in __PAG3__
    endif
    if Used("Curs_Val")
      Select ("Curs_Val")
      Use
    endif
    if Used("Curs_Val7")
      Select ("Curs_Val7")
      Use
    endif
    if Used("Curs_Val11")
      Select ("Curs_Val11")
      Use
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina cursori grafici
    if Used("_Avanz_")
      Use in _Avanz_
    endif
    if Used("_Costi_")
      Use in _Costi_
    endif
    if Used("_ImpCos_")
      Use in _ImpCos_
    endif
    if Used("_CosRic_")
      Use in _CosRic_
    endif
    if Used("_MarPer_")
      Use in _MarPer_
    endif
    if Used("_MarAgg_")
      Use in _MarAgg_
    endif
    if Used("_MarFin_")
      Use in _MarFin_
    endif
    if Used("_MarAtt_")
      Use in _MarAtt_
    endif
    if Used("Cur_Temp")
      Use in Cur_Temp
    endif
    if Used("Tamgraph")
      Use in Tamgraph
    endif
    if Used("Tamgrap2")
      Use in Tamgrap2
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Calcolo percentuale tecnico
    * --- La procedura Scorre riga per riga e per ognuna
    * --- somma la percentuale sul padre
    * --- Il cursore deve essere ordinato in modo da consultare prima i figli dei padri (lvlkey DESC)
    Select _Tecn_
    Scan
    this.w_LIVELLO = RTrim(_Tecn_.LvlKey)
    this.w_LUNGH = Len(this.w_LIVELLO)
    this.w_POSREC = RecNo()
    this.w_NUMPOINT = Occurs(".",this.w_LIVELLO)
    AH_Msg( "Calcolo avanzamento tecnico...: %1",.t.,.f.,.f.,this.w_PADRE)
    * --- Per ogni record aggiungo nel padre i vari importi
    if this.w_NUMPOINT>0
      * --- Questa variabile contiene il LEVELKEY del padre
      this.w_RICERCA = Padr( Left ( this.w_LIVELLO , AT ( "." , this.w_LIVELLO , this.w_NUMPOINT ) -1 ),200)
      * --- w_PERCOMP contiene la percentuale di completamento dell'attivit� o la frazione del conto in millesimi
      if _Tecn_.ATTIPCOM="A"
        this.w_PERCOMP = Nvl( _Tecn_.ATPERCOM , 0 )*Nvl ( _Tecn_.STMILLES , 0 )/100
      else
        this.w_PERCOMP = Nvl ( _Tecn_.STMILLES , 0 )*Nvl ( _Tecn_.AvanTec , 0 )/1000
      endif
      Select _Tecn_
      Go Top
      Locate For LvlKey = this.w_RICERCA
      if Found()
        this.w_PADRE = _Tecn_.AtCodAtt
        * --- Read from ATTIVITA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AT_PRJVW,ATPERCOM"+;
            " from "+i_cTable+" ATTIVITA where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPSTR);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_PADRE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AT_PRJVW,ATPERCOM;
            from (i_cTable) where;
                ATCODCOM = this.oParentObject.w_CODCOM;
                and ATTIPATT = this.w_TIPSTR;
                and ATCODATT = this.w_PADRE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FOGLIA = NVL(cp_ToDate(_read_.AT_PRJVW),cp_NullValue(_read_.AT_PRJVW))
          this.w_AVFOGLIA = NVL(cp_ToDate(_read_.ATPERCOM),cp_NullValue(_read_.ATPERCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if nvl(this.w_FOGLIA,"X") <> "F"
          * --- Solo se il nodo non � foglia
          Replace AvanTec With Nvl(AvanTec,0)+this.w_PERCOMP
        else
          Replace AvanTec With 10*Nvl(this.w_AVFOGLIA,0)
        endif
        Go this.w_POSREC
      endif
    endif
    Select _Tecn_
    EndScan
    Select _Tecn_
    Go Bottom
    * --- Leggo il totale sul padre
    this.w_PERCOMP = Nvl ( _Tecn_.AvanTec , 0 )/10
    this.oParentObject.w_VALORE_PER_ATEC = this.w_PERCOMP
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Costi
    * --- Calcolo il totale delle attivit� (della Commessa)
    * --- Calcolo il preventivo (Iniziale) per farlo utilizzo la solita Query del calcolo preventivo leggermente modificata (raggruppo)
    * --- Poi metto a 100 il preventivo e proporziono le altre voci (Impegnato e ordinato)
    * --- Preventivo Iniziale (se messo a 'N' preventivo iniziale )
    this.w_TIPPRE = IIF(this.oParentObject.w_TPREVEN="I","N"," ")
    this.w_TIPPRE1 = IIF(this.oParentObject.w_TPREVEN="A","S"," ")
    Vq_Exec ("..\Comm\Exe\Query\Gspc6bgr.Vqr",This,"Importi")
    * --- Raggruppo i vari Costi relativi alle Attivit� e ai sottoconti
    Select ; 
 Sum(Prev_Ma) As Preventivo_Ma , Sum(Prev_Md) As Preventivo_Md , ; 
 Sum(Prev_Ap) As Preventivo_Ap , Sum(Prev_Al) As Preventivo_Al, ; 
 Sum(Cons_Ma) As Cons_Ma , Sum(Cons_Md) As Cons_Md , ; 
 Sum(Cons_Ap) As Cons_Ap , Sum(Cons_Al) As Cons_Al, ; 
 Sum(Ordi_Ma) As Ordi_Ma , Sum(Ordi_Md) As Ordi_Md , ; 
 Sum(Ordi_AP) As Ordi_AP , Sum(Ordi_AL) As Ordi_AL ; 
 from Importi Into Cursor Importi NoFilter 
 Sum Preventivo_MA+Preventivo_MD+Preventivo_AP+Preventivo_AL TO this.w_PREVEN 
 Sum Cons_MA+Cons_MD+Cons_AP+Cons_AL TO this.w_TOTCOST 
 Sum Ordi_MA+Ordi_MD+Ordi_AP+Ordi_AL TO this.w_TOTORDI
    * --- Calcolo i totali del preventivo divisi per Tipo
    Sum Preventivo_MA TO this.w_PREVMA
    Sum Preventivo_MD TO this.w_PREVMD
    Sum Preventivo_AP TO this.w_PREVAP
    Sum Preventivo_AL TO this.w_PREVAL
    * --- Riporto tutto in percentuale
    this.w_TOTCONS = ( this.w_TOTCOST / this.w_PREVEN )*100
    this.w_TOTORDI = (this.w_TOTORDI / this.w_PREVEN)*100
    this.w_TOTPREV = 100
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi (campo per ordinamento)
    this.w_TIPSTR = "P"
    * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
    VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    Select "QUERY"
    * --- Costruisco il cursore dei dati della Tree View
    PC_EXPDB (this.oParentObject.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
    if used("__tmp__")
      use in __tmp__
    endif
    this.w_DTRIECO1 = cp_CharToDate("  -  -  ")
    this.w_DTRIECO2 = this.oParentObject.w_DATAINI
    * --- Metto la struttura nel cursore TEMP
    Select * From ( this.oParentObject.w_COUTCURS ) Order By LvlKey Into Cursor TempPag2 NoFilter
    * --- Se preventivo iniziale prendo tutte le attivit� (anche quelle con padre forfettario)
    this.w_TIPPRE = IIF(this.oParentObject.w_TPREVEN="I","N"," ")
    * --- Calcolo gli Importi da stampare a preventivo
    *     QUERY DA RINOMINARE
    Vq_Exec ("..\Comm\Exe\Query\Gspc11GR.Vqr",This,"Forfait")
    if .f.
      * --- Caso per TOTALE COMMESSA
      Vq_Exec ("..\Comm\Exe\Query\Gspc12gr.Vqr",This,"Importi")
    else
      * --- Caso per selezione da data a data
      this.w_TIPO = "A"
      * --- Valorizzo comunque il tipo di stampa ad Aggiornato
      Vq_Exec ("..\Comm\Exe\Query\Gspc13gr.Vqr",This,"Importi2") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc14gr.Vqr",This,"Importi3") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc15gr.Vqr",This,"Importi4") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc16gr.Vqr",This,"Importi5") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc18gr.Vqr",This,"Importi6") 
 Vq_Exec ("..\Comm\Exe\Query\Gspc19gr.Vqr",This,"Importi8") 
 Select * from Importi2 ; 
 union all select * from Importi3 ; 
 union all select * from Importi4 ; 
 union all select * from Importi5 ; 
 union all select * from Importi6 ; 
 union all select * from Importi8 ; 
 into cursor Importi nofilter
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Entra con w_FILTRO='D'
      SELECT distinct atcodatt, atpercom FROM _tecn_ into CURSOR _tecn1_ where attipatt="A"
      select distinct B.*, NVL(C.ATPERCOM,0) as atpercom from importi B left outer join _tecn1_ c on b.codatt=c.atcodatt and c.atpercom < 100 into cursor importi
      USE IN SELECT("_TECN1_")
    endif
    * --- Raggruppo i vari Costi relativi alle Attivit� e ai sottoconti
     
 Select CodAtt,TipAtt, min(atpercom) as atpercom, Min( FlPrev ) As FlPrev, Sum(Forf_MA) As Forf_MA, Sum(Costi_MA) As Costi_MA, ; 
 Sum(Forf_MD) As Forf_MD, Sum(Costi_MD) As Costi_MD, Sum(Forf_AP) As Forf_AP, Sum(Costi_AP) As Costi_AP, ; 
 Sum(Forf_AL) As Forf_AL, Sum(Costi_AL) As Costi_AL, ; 
 Sum(Cons_MA) As Cons_MA, Sum(Cons_MD) As Cons_MD, Sum(Cons_AP) As Cons_AP, Sum(Cons_AL) As Cons_AL, ; 
 Sum(Ordi_MA) As Ordi_MA, Sum(Ordi_MD) As Ordi_MD, Sum(Ordi_AP) As Ordi_AP, Sum(Ordi_AL) As Ordi_AL ; 
 from Importi Group By CodAtt,TipAtt Into Cursor Importi NoFilter
    * --- Costruisco il Cursore di Stampa - Solo Codice Attivit� e Tipo (La commessa � uguale)
    * --- All'interno dei Campi Preventivo_xx verranno posti gli Importi (Sub Totali)
    * --- Il campo CODPAD viene riempito nel calcolo dei totali (mi dice chi � il padre del nodo)
    Select TempPag2.*, Importi.FLPrev, AtCodAtt As CodPad, Occurs(".",LvlKey) As NumPoint, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_MA,Importi.Costi_MA) As Preventivo_MA, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_MD,Importi.Costi_MD) As Preventivo_MD, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_AP,Importi.Costi_AP) As Preventivo_AP, ; 
 IIF(Importi.FLPrev="S",Importi.Forf_AL,Importi.Costi_AL) As Preventivo_AL, ; 
 Importi.Cons_Ma ,Importi.Cons_Md , Importi.Cons_Ap , Importi.Cons_Al, ; 
 Importi.Ordi_Ma , Importi.Ordi_Md , Importi.Ordi_Ap , Importi.Ordi_Al ; 
 from TempPag2 Left Outer Join Importi On Importi.TipAtt=TempPag2.AtTipAtt And Importi.CodAtt=TempPag2.AtCodAtt ; 
 Order By LvlKey Desc Into Cursor TempPag2 NoFilter 
 Cur = WrCursor ("TempPag2")
    select atcodcom, ; 
 sum(NVL( preventivo_ma * (MAX(atpercom, 100) - atpercom) / MAX(atpercom, 100) , 0) ; 
 + NVL( preventivo_md * (MAX(atpercom, 100) - atpercom) / MAX(atpercom, 100) , 0) + ; 
 NVL( preventivo_ap * (MAX(atpercom, 100) - atpercom) / MAX(atpercom, 100), 0) ; 
 + NVL( preventivo_al * (MAX(atpercom, 100) - atpercom) / MAX(atpercom, 100), 0)) as preventivo ; 
 from TempPag2 into cursor _prevrip_ where attipatt="A" group by atcodcom
    this.oParentObject.w_VALORE_AFINIRE = str(nvl(_prevrip_.preventivo, 0), 18,2)
    use in select("_prevrip_")
    Select TEMPPag2.LVLKEY, Forfait.* from TEMPPag2 inner join FORFAIT ; 
 on TEMPPag2.ATCODATT=FORFAIT.CODATT and ; 
 TEMPPag2.ATTIPATT=FORFAIT.TIPATT into cursor FORFAIT
     
 Select FORFAIT 
 GO TOP 
 SCAN
    if this.oParentObject.w_TPREVEN="I" or (this.oParentObject.w_TPREVEN="A" and FLPREV="S")
       
 update TEMPPag2 set ; 
 TEMPPag2.PREVENTIVO_MA=FORFAIT.FORF_MA, ; 
 TEMPPag2.PREVENTIVO_MD=FORFAIT.FORF_MD, ; 
 TEMPPag2.PREVENTIVO_AP=FORFAIT.FORF_AP, ; 
 TEMPPag2.PREVENTIVO_AL=FORFAIT.FORF_AL ; 
 where TEMPPag2.ATCODATT=FORFAIT.CODATT and ; 
 TEMPPag2.ATTIPATT=FORFAIT.TIPATT
      L_CHIAVE=alltrim(LvlKey ) 
 L_LUNGH=len(L_CHIAVE)
       
 update TEMPPag2 set ; 
 TEMPPag2.PREVENTIVO_MA=0, ; 
 TEMPPag2.PREVENTIVO_MD=0, ; 
 TEMPPag2.PREVENTIVO_AP=0, ; 
 TEMPPag2.PREVENTIVO_AL=0 ; 
 where left(alltrim(TEMPPag2.LVLKEY),L_LUNGH)=L_CHIAVE ; 
 and ATTIPCOM="A"
    endif
    EndScan
     
 Select TempPag2 
 GO TOP 
 SCAN
    * --- Calcolo Gli Importi. La procedura Scorre riga per riga e per ognuna
    * --- Vado anche a riempire CodPad con il conto padre della riga
    * --- Il cursore deve essere ordinato in modo da consultare prima i figli dei padri (lvlkey DESC)
    * --- Ogni figlio infatti cerca il padre e in esso somma il suo importo
    this.w_LIVELLO = RTrim(TempPag2.LvlKey)
    this.w_LUNGH = Len(this.w_LIVELLO)
    this.w_POSREC = RecNo()
    this.w_NUMPOINT = Occurs(".",this.w_LIVELLO)
    AH_Msg( "Calcolo totali di stampa...:%1",.t.,.f.,.f.,this.w_PADRE )
    if this.w_NUMPOINT>0
      * --- Questa variabile contiene il LEVELKEY del padre
      this.w_RICERCA = Left( Left ( this.w_LIVELLO , AT ( "." , this.w_LIVELLO , this.w_NUMPOINT ) -1 )+space(200),200)
      this.w_CONS_MA = Nvl(Cons_MA,0)
      this.w_ORDI_MA = Nvl(Ordi_MA,0)
      this.w_IMPORTO_MA = Nvl(Preventivo_MA,0)
      this.w_CONS_MD = Nvl(Cons_MD,0)
      this.w_ORDI_MD = Nvl(Ordi_MD,0)
      this.w_IMPORTO_MD = Nvl(Preventivo_MD,0)
      this.w_CONS_AP = Nvl(Cons_AP,0)
      this.w_ORDI_AP = Nvl(Ordi_AP,0)
      this.w_IMPORTO_AP = Nvl(Preventivo_AP,0)
      this.w_CONS_AL = Nvl(Cons_AL,0)
      this.w_ORDI_AL = Nvl(Ordi_AL,0)
      this.w_IMPORTO_AL = Nvl(Preventivo_AL,0)
       
 Select TempPag2 
 GO TOP 
 Locate For LvlKey = this.w_RICERCA
      if Found()
        this.w_PADRE = TempPag2.AtCodAtt
        Replace Preventivo_MA With Nvl(Preventivo_MA,0)+Nvl(this.w_IMPORTO_MA,0)
        Replace Preventivo_MD With Nvl(Preventivo_MD,0)+Nvl(this.w_IMPORTO_MD,0)
        Replace Preventivo_AP With Nvl(Preventivo_AP,0)+Nvl(this.w_IMPORTO_AP,0)
        Replace Preventivo_AL With Nvl(Preventivo_AL,0)+Nvl(this.w_IMPORTO_AL,0)
        Replace Cons_MA With Nvl(Cons_MA,0)+Nvl(this.w_CONS_MA,0)
        Replace Cons_MD With Nvl(Cons_MD,0)+Nvl(this.w_CONS_MD,0)
        Replace Cons_AP With Nvl(Cons_AP,0)+Nvl(this.w_CONS_AP,0)
        Replace Cons_AL With Nvl(Cons_AL,0)+Nvl(this.w_CONS_AL,0)
        Replace Ordi_MA With Nvl(Ordi_MA,0)+Nvl(this.w_ORDI_MA,0)
        Replace Ordi_MD With Nvl(Ordi_MD,0)+Nvl(this.w_ORDI_MD,0)
        Replace Ordi_AP With Nvl(Ordi_AP,0)+Nvl(this.w_ORDI_AP,0)
        Replace Ordi_AL With Nvl(Ordi_AL,0)+Nvl(this.w_ORDI_AL,0)
        Go this.w_POSREC
        Select TempPag2
        Replace CodPad With this.w_PADRE
      endif
    endif
    EndScan
    * --- Solo primo livello
     
 Select *,Occurs(".",LvlKey) As Ordine From TempPag2 Where len(alltrim(LvlKey))<=1 ; 
 Order By Ordine,LvlKey Into Cursor bsc_curs NoFilter
     
 L_PREVENTIVO_MA=nvl(PREVENTIVO_MA,0) 
 L_PREVENTIVO_MD=nvl(PREVENTIVO_MD,0) 
 L_PREVENTIVO_AP=nvl(PREVENTIVO_AP,0) 
 L_PREVENTIVO_AL=nvl(PREVENTIVO_AL,0) 
 L_CONS_MA=nvl(CONS_MA,0) 
 L_CONS_MD=nvl(CONS_MD,0) 
 L_CONS_AP=nvl(CONS_AP,0) 
 L_CONS_AL=nvl(CONS_AL,0) 
 L_ORDI_MA=nvl(ORDI_MA,0) 
 L_ORDI_MD=nvl(ORDI_MD,0) 
 L_ORDI_AP=nvl(ORDI_AP,0) 
 L_ORDI_AL=nvl(ORDI_AL,0)
    if Used("TempPag2")
      Use in TEMPPag2
    endif
    * --- RIGA DEI COSTI DEL GRAFICO DI PAG1
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 1 
 REPLACE RIGA WITH 4 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "COSTI" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH STR(cp_round(L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL+L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE n_A WITH L_PREVENTIVO_MA+L_PREVENTIVO_MD+L_PREVENTIVO_AP+L_PREVENTIVO_AL 
 REPLACE n_B WITH L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL 
 REPLACE n_C WITH L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL 
 REPLACE n_D WITH L_CONS_MA+L_CONS_MD+L_CONS_AP+L_CONS_AL+L_ORDI_MA+L_ORDI_MD+L_ORDI_AP+L_ORDI_AL
    * --- PAG2
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 2 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MATERIALI" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_MA,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_MA,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_MA+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MA),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_MA 
 REPLACE n_B WITH L_CONS_MA 
 REPLACE n_C WITH L_ORDI_MA+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MA) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 2 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MANODOPERA" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_MD,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_MD,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_MD+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MD),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_MD 
 REPLACE n_B WITH L_CONS_MD 
 REPLACE n_C WITH L_ORDI_MD+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MD) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 2 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "APPALTI" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_AP,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_AP,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_AP+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AP),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_AP 
 REPLACE n_B WITH L_CONS_AP 
 REPLACE n_C WITH L_ORDI_AP+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AP) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 2 
 REPLACE RIGA WITH 4 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ALTRO" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_AL+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AL),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_AL 
 REPLACE n_B WITH L_CONS_AL 
 REPLACE n_C WITH L_ORDI_AL+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AL) 
 REPLACE n_D WITH 0
    * --- Inerisce RIGHE DUPLICATE DA PAG2 per far stampare il grafico di pag3
     
 SELECT STAMPA 
 APPEND BLANK 
 REPLACE PAGINA WITH 3 
 REPLACE RIGA WITH 1 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MATERIALI" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_MA,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_MA,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_MA+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MA),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_MA 
 REPLACE n_B WITH L_CONS_MA 
 REPLACE n_C WITH L_ORDI_MA+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MA) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 3 
 REPLACE RIGA WITH 2 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "MANODOPERA" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_MD,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_MD,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_MD+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MD),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_MD 
 REPLACE n_B WITH L_CONS_MD 
 REPLACE n_C WITH L_ORDI_MD+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_MD) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 3 
 REPLACE RIGA WITH 3 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "APPALTI" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_AP,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_AP,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_AP+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AP),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_AP 
 REPLACE n_B WITH L_CONS_AP 
 REPLACE n_C WITH L_ORDI_AP+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AP) 
 REPLACE n_D WITH 0 
 APPEND BLANK 
 REPLACE PAGINA WITH 3 
 REPLACE RIGA WITH 4 
 REPLACE DESCRIZIONE WITH AH_MSGFORMAT( "ALTRO" ) 
 REPLACE stampa_A WITH STR(cp_round(L_PREVENTIVO_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_B WITH STR(cp_round(L_CONS_AL,this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_C WITH STR(cp_round(L_ORDI_AL+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AL),this.w_DECCOM),this.w_nLenght, this.w_nDecimal) 
 REPLACE stampa_D WITH SPACE(18) 
 REPLACE n_A WITH L_PREVENTIVO_AL 
 REPLACE n_B WITH L_CONS_AL 
 REPLACE n_C WITH L_ORDI_AL+IIF(this.oParentObject.w_TIPOCOST="C",0,L_CONS_AL) 
 REPLACE n_D WITH 0
    * --- PER CONVERTIRE GLi IMPORTI vedi gspc_bsc
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Traduzione Doc. Valuta diversa e parzialmente importati a valore (Simil Pag2 GSPC_BRS)
    * --- Leggo il tasso di conversione della valuta (Se letto nella maschera a volte non lo prende)
    if this.oParentObject.w_CODVAL<>g_PERVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VACAOVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VACAOVAL;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_CODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAOCOMM = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_CAOCOMM = g_CAOVAL
    endif
    * --- Recupero i documenti evasi parzialmente a valore con valuta diversa dalla valurta di commessa
    Vq_Exec ( "..\Comm\Exe\Query\Gspc20gr.Vqr" , This , "Curs_Val7" )
    Vq_Exec ( "..\Comm\Exe\Query\Gspc21gr.Vqr" , This , "Curs_Val11" )
    select * from Curs_Val7 ;
    union all ;
    select * from Curs_Val11 ;
    into cursor Curs_Val nofilter
    Select "Curs_Val"
    Cur = WrCursor ("Curs_Val")
    Go Top
    this.w_NUMREC = RECCOUNT()>0
    Scan
    this.w_CAOVAL = Nvl ( CAOVAL , 0 )
    this.w_DATDOC = Cp_ToDate(DATDOC)
    * --- Se Valuta del documento inesistente non applico la conversione (Dovrebbe esserci sempre)
    this.w_DOCVAL = Nvl ( CODVAL , this.oParentObject.w_CODVAL )
    this.w_IMP_MA = Nvl ( IMP_MA , 0 )
    this.w_IMP_MD = Nvl ( IMP_MD , 0 )
    this.w_IMP_AP = Nvl ( IMP_AP , 0 )
    this.w_IMP_AL = Nvl ( IMP_AL , 0 )
    * --- Eseguo la conversione della Riga
    this.w_ORD_MA = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_MA , VAL2MON(this.w_IMP_MA,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL,this.w_DECCOM) )
    this.w_ORD_MD = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_MD , VAL2MON(this.w_IMP_MD,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL,this.w_DECCOM) )
    this.w_ORD_AP = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_AP , VAL2MON(this.w_IMP_AP,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL,this.w_DECCOM) )
    this.w_ORD_AL = IIF ( this.oParentObject.w_CODVAL=this.w_DOCVAL,this.w_IMP_AL , VAL2MON(this.w_IMP_AL,this.w_CAOVAL,this.w_CAOCOMM,this.w_DATDOC,this.oParentObject.w_CODVAL,this.w_DECCOM) )
    Select "Curs_Val"
    Replace ORD_MA With this.w_ORD_MA
    Replace ORD_MD With this.w_ORD_MD
    Replace ORD_AP With this.w_ORD_AP
    Replace ORD_AL With this.w_ORD_AL
    EndScan
    if this.w_NUMREC
      * --- Metto tutto Assieme
      Select CodAtt, TipAtt, "N" As FlPrev, ;
      Ord_MA*0 As Costi_MA, Ord_MD*0 As Costi_MD, Ord_AP*0 As Costi_AP, Ord_AL*0 As Costi_AL, ;
      Ord_MA*0 As Forf_MA, Ord_MD*0 As Forf_MD, Ord_AP*0 As Forf_AP, Ord_AL*0 As Forf_AL, ;
      CONS_MA*0 As Cons_MA, CONS_MD*0 As Cons_MD, CONS_AP*0 As Cons_AP, CONS_AL*0 As Cons_AL, ;
      Sum(Ord_MA) As Ordi_MA, Sum(Ord_MD) As Ordi_MD, Sum(Ord_AP) As Ordi_AP, Sum(Ord_AL) As Ordi_AL ;
      From Curs_Val Group By CodAtt ;
      Union all ;
      Select Importi.* From Importi into Cursor Cur_Temp NoFilter
      * --- Raggruppo Ulteriormente
      Select CodAtt, TipAtt, FlPrev, ;
      Sum (Costi_MA) As Costi_MA, Sum(Costi_MD) As Costi_MD, Sum(Costi_AP) As Costi_AP, Sum(Costi_AL) As Costi_AL, ;
      Sum(Forf_MA) As Forf_MA, Sum(Forf_MD) As Forf_MD, Sum(Forf_AP) As Forf_AP, Sum(Forf_AL) As Forf_AL, ;
      Sum(Cons_MA) As Cons_MA, Sum(Cons_MD) As Cons_MD, Sum(Cons_AP) As Cons_AP, Sum(Cons_AL) As Cons_AL, ;
      Sum(Ordi_MA) As Ordi_MA, Sum(Ordi_MD) As Ordi_MD, Sum(Ordi_AP) As Ordi_AP, Sum(Ordi_AL) As Ordi_AL ;
      From Cur_Temp Group By CodAtt Into Cursor Importi NoFilter
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CPAR_DEF'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='VALUTE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
