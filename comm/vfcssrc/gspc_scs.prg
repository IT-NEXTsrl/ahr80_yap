* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_scs                                                        *
*              Caricamento rapido                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_17]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-29                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_scs",oParentObject))

* --- Class definition
define class tgspc_scs as StdForm
  Top    = 2
  Left   = 39

  * --- Standard Properties
  Width  = 565
  Height = 420
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=90855529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gspc_scs"
  cComment = "Caricamento rapido"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOCARI = space(10)
  w_STCODCOM = space(15)
  w_STTIPSTR = space(1)
  w_STATTPAD = space(15)
  w_MESSAGGIO = space(0)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_scsPag1","gspc_scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSPC_BCD(this,.w_TIPOCARI)
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOCARI=space(10)
      .w_STCODCOM=space(15)
      .w_STTIPSTR=space(1)
      .w_STATTPAD=space(15)
      .w_MESSAGGIO=space(0)
      .w_TIPOCARI=oParentObject.w_TIPOCARI
      .w_STCODCOM=oParentObject.w_STCODCOM
      .w_STTIPSTR=oParentObject.w_STTIPSTR
      .w_STATTPAD=oParentObject.w_STATTPAD
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(1,4,.f.)
        .w_MESSAGGIO = AH_MSGFORMAT("Seleziona i conti da aggiungere come figli al nodo selezionato")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPOCARI=.w_TIPOCARI
      .oParentObject.w_STCODCOM=.w_STCODCOM
      .oParentObject.w_STTIPSTR=.w_STTIPSTR
      .oParentObject.w_STATTPAD=.w_STATTPAD
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTCODCOM_1_4.value==this.w_STCODCOM)
      this.oPgFrm.Page1.oPag.oSTCODCOM_1_4.value=this.w_STCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATTPAD_1_7.value==this.w_STATTPAD)
      this.oPgFrm.Page1.oPag.oSTATTPAD_1_7.value=this.w_STATTPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oMESSAGGIO_1_12.value==this.w_MESSAGGIO)
      this.oPgFrm.Page1.oPag.oMESSAGGIO_1_12.value=this.w_MESSAGGIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_scsPag1 as StdContainer
  Width  = 561
  height = 420
  stdWidth  = 561
  stdheight = 420
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTCODCOM_1_4 as StdField with uid="HXRMSHJTDJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_STCODCOM", cQueryName = "STCODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa del legame",;
    HelpContextID = 232181389,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=426, Top=49, InputMask=replicate('X',15)


  add object ZOOM as cp_szoombox with uid="GHXWBSVNYB",left=-5, top=21, width=429,height=364,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ATTIVITA",cZoomFile="GSPC_SCS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,QueryonLoad=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSPC_AAT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 87142118

  add object oSTATTPAD_1_7 as StdField with uid="BFWCYCVMOF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STATTPAD", cQueryName = "STATTPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto, o sottoconto padre del legame",;
    HelpContextID = 3019114,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=426, Top=97, InputMask=replicate('X',15)


  add object oBtn_1_10 as StdButton with uid="HPRENYTYFX",left=454, top=368, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma caricamento rapido";
    , HelpContextID = 90826778;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSPC_BCD(this.Parent.oContained,.w_TIPOCARI)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="KOTWJAFPMC",left=505, top=368, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83538106;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESSAGGIO_1_12 as StdMemo with uid="DCGPQPPRYU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MESSAGGIO", cQueryName = "MESSAGGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 100542207,;
   bGlobalFont=.t.,;
    Height=111, Width=127, Left=427, Top=131

  add object oStr_1_2 as StdString with uid="CEZHVEUIJD",Visible=.t., Left=70, Top=4,;
    Alignment=0, Width=425, Height=15,;
    Caption="Questa funzionalitą prevede la cancellazione delle righe presenti nel dettaglio."  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOCARI='GSPC_BGS')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="HQPVLPPZHA",Visible=.t., Left=426, Top=30,;
    Alignment=0, Width=98, Height=15,;
    Caption="Commessa"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SYJJIJOJKW",Visible=.t., Left=3, Top=4,;
    Alignment=1, Width=63, Height=15,;
    Caption="Attenzione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOCARI='GSPC_BGS')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="SWRRFPJHMT",Visible=.t., Left=426, Top=79,;
    Alignment=0, Width=98, Height=15,;
    Caption="Padre"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
