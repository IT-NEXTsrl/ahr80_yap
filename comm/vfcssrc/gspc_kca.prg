* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kca                                                        *
*              Copia schede tecniche                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_109]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kca",oParentObject))

* --- Class definition
define class tgspc_kca as StdForm
  Top    = 3
  Left   = 48

  * --- Standard Properties
  Width  = 520
  Height = 262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=169191319
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gspc_kca"
  cComment = "Copia schede tecniche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_A = space(1)
  w_CODCOMS = space(15)
  w_CODATTS = space(15)
  o_CODATTS = space(15)
  w_CODATT2 = space(15)
  w_CODCOMD = space(15)
  w_RADIOVAL = space(10)
  w_DESATTS = space(30)
  w_DESATT2 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_kcaPag1","gspc_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOMS_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='CAN_TIER'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_A=space(1)
      .w_CODCOMS=space(15)
      .w_CODATTS=space(15)
      .w_CODATT2=space(15)
      .w_CODCOMD=space(15)
      .w_RADIOVAL=space(10)
      .w_DESATTS=space(30)
      .w_DESATT2=space(30)
        .w_A = "A"
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOMS))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODATTS))
          .link_1_3('Full')
        endif
        .w_CODATT2 = .w_CODATTS
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODATT2))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCOMD))
          .link_1_5('Full')
        endif
        .w_RADIOVAL = "A"
    endwith
    this.DoRTCalc(7,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CODATTS<>.w_CODATTS
            .w_CODATT2 = .w_CODATTS
          .link_1_4('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOMS
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMS)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMS))
          select CNCODCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMS)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMS) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMS_1_2'),i_cWhere,'',"Commessa di origine",'GSPC_CS.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMS)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMS = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOMS<>.w_CODCOMD
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endif
        this.w_CODCOMS = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATTS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATTS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATTS)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODCOM,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_A;
                     ,'ATCODCOM',this.w_CODCOMS;
                     ,'ATCODATT',trim(this.w_CODATTS))
          select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODCOM,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATTS)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATTS) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODCOM,ATCODATT',cp_AbsName(oSource.parent,'oCODATTS_1_3'),i_cWhere,'',"Attivit� iniziale",'GSPC_ATT.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_A<>oSource.xKey(1);
           .or. this.w_CODCOMS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODCOM',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATTS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATTS);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_A;
                       ,'ATCODCOM',this.w_CODCOMS;
                       ,'ATCODATT',this.w_CODATTS)
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATTS = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTS = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATTS = space(15)
      endif
      this.w_DESATTS = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT empty(.w_CODCOMS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATTS = space(15)
        this.w_DESATTS = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATTS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT2)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODCOM,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_A;
                     ,'ATCODCOM',this.w_CODCOMS;
                     ,'ATCODATT',trim(this.w_CODATT2))
          select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODCOM,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT2)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT2) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODCOM,ATCODATT',cp_AbsName(oSource.parent,'oCODATT2_1_4'),i_cWhere,'',"Attivit� finale",'GSPC_ATT.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_A<>oSource.xKey(1);
           .or. this.w_CODCOMS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODCOM',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT2);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_A;
                       ,'ATCODCOM',this.w_CODCOMS;
                       ,'ATCODATT',this.w_CODATT2)
            select ATTIPATT,ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT2 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT2 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT2 = space(15)
      endif
      this.w_DESATT2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT empty(.w_CODCOMS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATT2 = space(15)
        this.w_DESATT2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMD
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMD)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMD))
          select CNCODCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMD)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMD) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMD_1_5'),i_cWhere,'',"Commessa di destinazione",'GSPC_CS.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMD)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMD = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMD = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOMS<>.w_CODCOMD
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endif
        this.w_CODCOMD = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOMS_1_2.value==this.w_CODCOMS)
      this.oPgFrm.Page1.oPag.oCODCOMS_1_2.value=this.w_CODCOMS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATTS_1_3.value==this.w_CODATTS)
      this.oPgFrm.Page1.oPag.oCODATTS_1_3.value=this.w_CODATTS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT2_1_4.value==this.w_CODATT2)
      this.oPgFrm.Page1.oPag.oCODATT2_1_4.value=this.w_CODATT2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOMD_1_5.value==this.w_CODCOMD)
      this.oPgFrm.Page1.oPag.oCODCOMD_1_5.value=this.w_CODCOMD
    endif
    if not(this.oPgFrm.Page1.oPag.oRADIOVAL_1_6.RadioValue()==this.w_RADIOVAL)
      this.oPgFrm.Page1.oPag.oRADIOVAL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATTS_1_8.value==this.w_DESATTS)
      this.oPgFrm.Page1.oPag.oDESATTS_1_8.value=this.w_DESATTS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT2_1_12.value==this.w_DESATT2)
      this.oPgFrm.Page1.oPag.oDESATT2_1_12.value=this.w_DESATT2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODCOMS)) or not(.w_CODCOMS<>.w_CODCOMD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOMS_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODCOMS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
          case   not(NOT empty(.w_CODCOMS))  and not(empty(.w_CODATTS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATTS_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(NOT empty(.w_CODCOMS))  and not(empty(.w_CODATT2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODCOMD)) or not(.w_CODCOMS<>.w_CODCOMD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOMD_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODCOMD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODATTS = this.w_CODATTS
    return

enddefine

* --- Define pages as container
define class tgspc_kcaPag1 as StdContainer
  Width  = 516
  height = 262
  stdWidth  = 516
  stdheight = 262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOMS_1_2 as StdField with uid="UAQXIVVCDP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOMS", cQueryName = "CODCOMS",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile copiare una struttura di commessa su s� stessa",;
    ToolTipText = "Codice commessa sorgente",;
    HelpContextID = 62047450,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=138, Top=62, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMS"

  func oCODCOMS_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODATTS)
        bRes2=.link_1_3('Full')
      endif
      if .not. empty(.w_CODATT2)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOMS_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMS_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMS_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commessa di origine",'GSPC_CS.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oCODATTS_1_3 as StdField with uid="PKLOJSLLYU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODATTS", cQueryName = "CODATTS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� iniziale da copiare",;
    HelpContextID = 60504870,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=138, Top=91, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_A", oKey_2_1="ATCODCOM", oKey_2_2="this.w_CODCOMS", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATTS"

  func oCODATTS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATTS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATTS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_A)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOMS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_A)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOMS)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODCOM,ATCODATT',cp_AbsName(this.parent,'oCODATTS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit� iniziale",'GSPC_ATT.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODATT2_1_4 as StdField with uid="XYCFROPWPC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODATT2", cQueryName = "CODATT2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa sorgente",;
    HelpContextID = 207930586,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=138, Top=120, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_A", oKey_2_1="ATCODCOM", oKey_2_2="this.w_CODCOMS", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT2"

  func oCODATT2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_A)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOMS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_A)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOMS)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODCOM,ATCODATT',cp_AbsName(this.parent,'oCODATT2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit� finale",'GSPC_ATT.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODCOMD_1_5 as StdField with uid="QXYUTPKDUO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCOMD", cQueryName = "CODCOMD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile copiare una struttura di commessa su s� stessa",;
    ToolTipText = "Codice commessa sorgente",;
    HelpContextID = 206388006,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=138, Top=149, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMD"

  func oCODCOMD_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMD_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMD_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMD_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commessa di destinazione",'GSPC_CS.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oRADIOVAL_1_6 as StdRadio with uid="YAMNHYNRUD",rtseq=6,rtrep=.f.,left=138, top=178, width=293,height=32;
    , cFormVar="w_RADIOVAL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADIOVAL_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Mantieni valuta del movimento di origine"
      this.Buttons(1).HelpContextID = 179098014
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Utilizza valuta della commessa di destinazione"
      this.Buttons(2).HelpContextID = 179098014
      this.Buttons(2).Top=15
      this.SetAll("Width",291)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oRADIOVAL_1_6.RadioValue()
    return(iif(this.value =1,"A",;
    iif(this.value =2,"B",;
    space(10))))
  endfunc
  func oRADIOVAL_1_6.GetRadio()
    this.Parent.oContained.w_RADIOVAL = this.RadioValue()
    return .t.
  endfunc

  func oRADIOVAL_1_6.SetRadio()
    this.Parent.oContained.w_RADIOVAL=trim(this.Parent.oContained.w_RADIOVAL)
    this.value = ;
      iif(this.Parent.oContained.w_RADIOVAL=="A",1,;
      iif(this.Parent.oContained.w_RADIOVAL=="B",2,;
      0))
  endfunc

  add object oDESATTS_1_8 as StdField with uid="GHDUBUUNPK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESATTS", cQueryName = "DESATTS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 60563766,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=258, Top=91, InputMask=replicate('X',30)


  add object oBtn_1_11 as StdButton with uid="ZPUNMVNNJL",left=409, top=211, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la copia delle attivit�";
    , HelpContextID = 169220070;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do GSPC_BAV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CODATTS<=.w_CODATT2 and NOT empty(.w_CODCOMS) and NOT empty(.w_CODCOMD) and NOT empty(.w_CODATTS) and NOT empty(.w_CODATT2))
      endwith
    endif
  endfunc

  add object oDESATT2_1_12 as StdField with uid="DZUXCAKQOD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 207871690,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=258, Top=120, InputMask=replicate('X',30)


  add object oBtn_1_17 as StdButton with uid="KOTWJAFPMC",left=460, top=211, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 176508742;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="ZGXGGBIPJL",Visible=.t., Left=7, Top=62,;
    Alignment=1, Width=128, Height=18,;
    Caption="Commessa sorgente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ONHJLTYHIX",Visible=.t., Left=13, Top=149,;
    Alignment=1, Width=122, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="ZBLYGLDFQD",Visible=.t., Left=7, Top=6,;
    Alignment=0, Width=496, Height=18,;
    Caption="La procedura copia le schede tecniche delle attivit� selezionate,"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="CPMPZLAIQW",Visible=.t., Left=13, Top=178,;
    Alignment=1, Width=122, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KPAOIDIUGM",Visible=.t., Left=7, Top=26,;
    Alignment=0, Width=318, Height=18,;
    Caption="dalla commessa sorgente alla commessa destinazione."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="IDTKJUTEDH",Visible=.t., Left=13, Top=91,;
    Alignment=1, Width=122, Height=15,;
    Caption="Attivit� iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LRTNZNHCCS",Visible=.t., Left=13, Top=120,;
    Alignment=1, Width=122, Height=15,;
    Caption="Attivit� finale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
