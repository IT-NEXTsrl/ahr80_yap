* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bg3                                                        *
*              Copia progetto - elimina legami                                 *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-11                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCOMMESSA,pTIPO,pTIPSTR,pNODO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bg3",oParentObject,m.pCOMMESSA,m.pTIPO,m.pTIPSTR,m.pNODO)
return(i_retval)

define class tgspc_bg3 as StdBatch
  * --- Local variables
  pCOMMESSA = space(15)
  pTIPO = space(1)
  pTIPSTR = space(1)
  pNODO = space(15)
  w_TIPO = space(1)
  w_NODO = space(15)
  w_CursoreStruttura = space(10)
  * --- WorkFile variables
  STRUTTUR_idx=0
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Elimina legami
    * --- cursore AttivitaDaCanc creato in GSPC_BG4
    SELECT AttivitaDaCanc
    INSERT INTO AttivitaDaCanc (CODICE, TIPO) VALUES (this.pNODO, this.pTIPO)
    this.w_CursoreStruttura = SYS(2015)
    VQ_EXEC("..\COMM\EXE\QUERY\GSPC_BG3.VQR",this,this.w_CursoreStruttura)
    * --- Delete from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"STCODCOM = "+cp_ToStrODBC(this.pCOMMESSA);
            +" and STTIPSTR = "+cp_ToStrODBC(this.pTIPO);
            +" and STATTPAD = "+cp_ToStrODBC(this.pNODO);
             )
    else
      delete from (i_cTable) where;
            STCODCOM = this.pCOMMESSA;
            and STTIPSTR = this.pTIPO;
            and STATTPAD = this.pNODO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    SELECT (this.w_CursoreStruttura)
     
 GO TOP 
 SCAN
    this.w_TIPO = STTIPFIG
    this.w_NODO = STATTFIG
    SELECT (this.w_CursoreStruttura)
    GSPC_BG3 (this, this.pCOMMESSA, this.w_TIPO, this.pTIPSTR, this.w_NODO)
    SELECT (this.w_CursoreStruttura)
    ENDSCAN
    * --- Delete from STRUTTUR
    i_nConn=i_TableProp[this.STRUTTUR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"STCODCOM = "+cp_ToStrODBC(this.pCOMMESSA);
            +" and STTIPFIG = "+cp_ToStrODBC(this.pTIPO);
            +" and STATTFIG = "+cp_ToStrODBC(this.pNODO);
            +" and STTIPSTR = "+cp_ToStrODBC(this.pTIPSTR);
             )
    else
      delete from (i_cTable) where;
            STCODCOM = this.pCOMMESSA;
            and STTIPFIG = this.pTIPO;
            and STATTFIG = this.pNODO;
            and STTIPSTR = this.pTIPSTR;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    SELECT (this.w_CursoreStruttura)
    USE
  endproc


  proc Init(oParentObject,pCOMMESSA,pTIPO,pTIPSTR,pNODO)
    this.pCOMMESSA=pCOMMESSA
    this.pTIPO=pTIPO
    this.pTIPSTR=pTIPSTR
    this.pNODO=pNODO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STRUTTUR'
    this.cWorkTables[2]='ATTIVITA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCOMMESSA,pTIPO,pTIPSTR,pNODO"
endproc
