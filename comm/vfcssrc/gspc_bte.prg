* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bte                                                        *
*              Tempificazione attivita di commessa                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-29                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODCOM,pDATINI,pDATFIN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bte",oParentObject,m.pCODCOM,m.pDATINI,m.pDATFIN)
return(i_retval)

define class tgspc_bte as StdBatch
  * --- Local variables
  pCODCOM = space(15)
  pDATINI = ctod("  /  /  ")
  pDATFIN = ctod("  /  /  ")
  w_EXITLOOP = .f.
  w_LIVELLO = 0
  w_CURLIV = 0
  w_MPCODCOM = space(15)
  w_MPTIPATT = space(1)
  w_MPCODATT = space(15)
  w_MPATTPRE = space(15)
  w_MPTIPPRE = space(2)
  w_MTRITAR = 0
  w_D_DATINI = ctod("  /  /  ")
  w_D_DATFIN = ctod("  /  /  ")
  w_D_DURGIO = 0
  w_O_DATINI = ctod("  /  /  ")
  w_O_DATFIN = ctod("  /  /  ")
  w_O_DURGIO = 0
  w_TIPREC = space(1)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PROGR = space(1)
  w_MESS = space(200)
  w_ERRNO = 0
  * --- WorkFile variables
  TMPATTPREC_idx=0
  ATTIVITA_idx=0
  CAN_TIER_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ERRNO = 0
    * --- Try
    local bErr_03723A90
    bErr_03723A90=bTrsErr
    this.Try_03723A90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_ERRNO = 2
    endif
    bTrsErr=bTrsErr or bErr_03723A90
    * --- End
    * --- Drop temporary table TMPATTPREC
    i_nIdx=cp_GetTableDefIdx('TMPATTPREC')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPATTPREC')
    endif
    i_retcode = 'stop'
    i_retval = this.w_ERRNO
    return
  endproc
  proc Try_03723A90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDPROGR"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC("TAM");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDPROGR;
        from (i_cTable) where;
            PDCHIAVE = "TAM";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PROGR = NVL(cp_ToDate(_read_.PDPROGR),cp_NullValue(_read_.PDPROGR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MPCODCOM = NVL(this.pCODCOM,"")
    this.w_DATINI = NVL(this.pDATINI, cp_CharToDate("  -  -    "))
    this.w_DATFIN = NVL(this.pDATFIN, cp_CharToDate("  -  -    "))
    if UPPER(this.oParentObject.Class) <> "TGSPC_BG4"
      if this.w_DATINI=cp_CharToDate("  -  -    ") or this.w_DATFIN=cp_CharToDate("  -  -    ")
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDATINI,CNDATFIN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_MPCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDATINI,CNDATFIN;
            from (i_cTable) where;
                CNCODCAN = this.w_MPCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATINI = NVL(cp_ToDate(_read_.CNDATINI),cp_NullValue(_read_.CNDATINI))
          this.w_DATFIN = NVL(cp_ToDate(_read_.CNDATFIN),cp_NullValue(_read_.CNDATFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_PROGR="A"
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
          do vq_exec with 'GSPC_BTE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDATINI = _t2.ATDATINI";
              +",ATDATFIN = _t2.ATDATFIN";
              +i_ccchkf;
              +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
              +"ATTIVITA.ATDATINI = _t2.ATDATINI";
              +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
              +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
              +"ATDATINI,";
              +"ATDATFIN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.ATDATINI,";
              +"t2.ATDATFIN";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
              +"ATDATINI = _t2.ATDATINI";
              +",ATDATFIN = _t2.ATDATFIN";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                  +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                  +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
              +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
          do vq_exec with 'GSPC6BG9',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDATINI = _t2.ATDATINI";
              +",ATDATFIN = _t2.ATDATFIN";
              +i_ccchkf;
              +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
              +"ATTIVITA.ATDATINI = _t2.ATDATINI";
              +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
              +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
              +"ATDATINI,";
              +"ATDATFIN";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.ATDATINI,";
              +"t2.ATDATFIN";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                  +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                  +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
              +"ATDATINI = _t2.ATDATINI";
              +",ATDATFIN = _t2.ATDATFIN";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                  +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                  +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
              +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Popolo la tabella TMPATTPREC con le attivit� che non sono precedenti di altre attivit�
    * --- Create temporary table TMPATTPREC
    i_nIdx=cp_AddTableDef('TMPATTPREC') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSPC5BG1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPATTPREC_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ciclo sui records finch� trovo attivita.....
    this.w_EXITLOOP = .f.
    this.w_LIVELLO = 1
    * --- Ciclo su tutte le attivit�...
    do while !this.w_EXITLOOP
      if this.w_LIVELLO >= 100
        this.w_EXITLOOP = .t.
      else
        this.w_LIVELLO = this.w_LIVELLO + 1
        * --- Select from TMPATTPREC
        i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2],.t.,this.TMPATTPREC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPATTPREC ";
              +" where LIVELLO="+cp_ToStrODBC(this.w_LIVELLO)+"-1";
               ,"_Curs_TMPATTPREC")
        else
          select * from (i_cTable);
           where LIVELLO=this.w_LIVELLO-1;
            into cursor _Curs_TMPATTPREC
        endif
        if used('_Curs_TMPATTPREC')
          select _Curs_TMPATTPREC
          locate for 1=1
          do while not(eof())
          this.w_MPTIPATT = NVL(_Curs_TMPATTPREC.MPTIPATT,"")
          this.w_MPCODATT = NVL(_Curs_TMPATTPREC.MPCODATT,"")
          * --- Insert into TMPATTPREC
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSPC5BG2",this.TMPATTPREC_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if i_ROWS <= 0
            this.w_EXITLOOP = .t.
          endif
            select _Curs_TMPATTPREC
            continue
          enddo
          use
        endif
      endif
    enddo
    if this.w_LIVELLO>=100
      this.w_ERRNO = 1
    else
      * --- Elimino i records che hanno livello 1, non dovr� aggiornare nulla...
      * --- Delete from TMPATTPREC
      i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"LIVELLO = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              LIVELLO = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Aggiorno le attivit�...
      do while this.w_CURLIV <= this.w_LIVELLO
        this.w_CURLIV = this.w_CURLIV + 1
        if this.w_PROGR="A"
          * --- Tipo precedenza 'FI'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Tipo precedenza 'IF'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Tipo precedenza 'FF'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG5',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG5',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG5',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Tipo precedenza 'IF'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG6',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG6',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG6',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Tipo precedenza 'FI'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Tipo precedenza 'FF'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ATCODCOM,ATTIPATT,ATCODATT"
            do vq_exec with 'GSPC5BG8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA, "+i_cQueryTable+" _t2 set ";
                +"ATTIVITA.ATDATINI = _t2.ATDATINI";
                +",ATTIVITA.ATDATFIN = _t2.ATDATFIN";
                +Iif(Empty(i_ccchkf),"",",ATTIVITA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ATTIVITA.ATCODCOM = t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = t2.ATCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set (";
                +"ATDATINI,";
                +"ATDATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ATDATINI,";
                +"t2.ATDATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ATTIVITA.ATCODCOM = _t2.ATCODCOM";
                    +" and "+"ATTIVITA.ATTIPATT = _t2.ATTIPATT";
                    +" and "+"ATTIVITA.ATCODATT = _t2.ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ATTIVITA set ";
                +"ATDATINI = _t2.ATDATINI";
                +",ATDATFIN = _t2.ATDATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ATCODCOM = "+i_cQueryTable+".ATCODCOM";
                    +" and "+i_cTable+".ATTIPATT = "+i_cQueryTable+".ATTIPATT";
                    +" and "+i_cTable+".ATCODATT = "+i_cQueryTable+".ATCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI = (select ATDATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",ATDATFIN = (select ATDATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPATTPRE"
            do vq_exec with 'GSPC6BG8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.D_DATINI = _t2.D_DATINI";
                +",TMPATTPREC.D_DATFIN = _t2.D_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = t2.MPATTPRE";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"D_DATINI,";
                +"D_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.D_DATINI,";
                +"t2.D_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPATTPRE = _t2.MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"D_DATINI = _t2.D_DATINI";
                +",D_DATFIN = _t2.D_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPATTPRE = "+i_cQueryTable+".MPATTPRE";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"D_DATINI = (select D_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",D_DATFIN = (select D_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMPATTPREC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPATTPREC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPATTPREC_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MPCODCOM,MPTIPATT,MPCODATT"
            do vq_exec with 'GSPC7BG8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPATTPREC_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC, "+i_cQueryTable+" _t2 set ";
                +"TMPATTPREC.O_DATINI = _t2.O_DATINI";
                +",TMPATTPREC.O_DATFIN = _t2.O_DATFIN";
                +Iif(Empty(i_ccchkf),"",",TMPATTPREC.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPATTPREC.MPCODCOM = t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = t2.MPCODATT";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set (";
                +"O_DATINI,";
                +"O_DATFIN";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.O_DATINI,";
                +"t2.O_DATFIN";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPATTPREC.MPCODCOM = _t2.MPCODCOM";
                    +" and "+"TMPATTPREC.MPTIPATT = _t2.MPTIPATT";
                    +" and "+"TMPATTPREC.MPCODATT = _t2.MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPATTPREC set ";
                +"O_DATINI = _t2.O_DATINI";
                +",O_DATFIN = _t2.O_DATFIN";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MPCODCOM = "+i_cQueryTable+".MPCODCOM";
                    +" and "+i_cTable+".MPTIPATT = "+i_cQueryTable+".MPTIPATT";
                    +" and "+i_cTable+".MPCODATT = "+i_cQueryTable+".MPCODATT";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"O_DATINI = (select O_DATINI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",O_DATFIN = (select O_DATFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      enddo
    endif
    return


  proc Init(oParentObject,pCODCOM,pDATINI,pDATFIN)
    this.pCODCOM=pCODCOM
    this.pDATINI=pDATINI
    this.pDATFIN=pDATFIN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*TMPATTPREC'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='CPAR_DEF'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_TMPATTPREC')
      use in _Curs_TMPATTPREC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODCOM,pDATINI,pDATFIN"
endproc
