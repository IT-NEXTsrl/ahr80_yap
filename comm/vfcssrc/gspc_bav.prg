* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bav                                                        *
*              Copia attivita                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_90]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2008-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bav",oParentObject)
return(i_retval)

define class tgspc_bav as StdBatch
  * --- Local variables
  w_CODCOM = space(15)
  w_CODVAL = space(3)
  w_NOMESS = space(1)
  w_TIPCOM = space(1)
  w_DESCRI = space(30)
  w_DESSUP = space(0)
  w_CENCOS = space(15)
  w_TOTCOS = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DURGIO = 0
  w_VINCOL = space(3)
  w_DATVIN = ctod("  /  /  ")
  w_CARDIN = space(1)
  w___RIGA = 0
  w_FLPREV = space(1)
  w__STATO = space(1)
  w__OKTEC = space(1)
  w__OKAMM = space(1)
  w__OKGES = space(1)
  w_GRPMAN = 0
  w_CODFAM = space(5)
  w_PERCOM = 0
  w__PRJVW = space(1)
  w_ATTIVITA = space(15)
  w_MASERIAL = space(10)
  w_MANUMREG = 0
  w_MAALFREG = 0
  w_MADATREG = ctod("  /  /  ")
  w_MA__ANNO = space(4)
  w_MATIPMOV = space(1)
  w_MACODVAL = space(3)
  w_MACAOVAL = 0
  w_MACODLIS = space(5)
  w_UTCV = 0
  w_UTCC = 0
  w_UTDV = ctod("  /  /  ")
  w_UTDC = ctod("  /  /  ")
  w_NEWSERIAL = space(10)
  w_NEWCODVAL = space(3)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MACODKEY = space(20)
  w_MACODART = space(20)
  w_MAUNIMIS = space(3)
  w_MAQTAMOV = 0
  w_MAPREZZO = 0
  w_MADATANN = ctod("  /  /  ")
  w_MADESCRI = space(40)
  w_MA__NOTE = space(0)
  w_MACODCOS = space(5)
  w_MAOPPREV = space(1)
  w_MAOPCONS = space(1)
  w_MAQTA1UM = 0
  w_MAVALRIG = 0
  w_MACOATTS = space(15)
  w_MATIPATT = space(1)
  w_MACOCOMS = space(15)
  w_MACODFAM = space(5)
  CopiaMovimento = .f.
  ValutaCommessa = space(3)
  w_CAOCOM = 0
  w_DECCOM = 0
  ValutaCommeOld = space(3)
  * --- WorkFile variables
  ATTIVITA_idx=0
  CAN_TIER_idx=0
  MAT_DETT_idx=0
  MAT_MAST_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSPC_KCA
    * --- Attivit� da copiare
    * --- Commessa dell'attivit� da copiare
    * --- Commessa dell'attivit� copiata
    * --- Variabili per i movimenti di commessa (master)
    * --- Variabili per i movimenti di commessa (detail)
    * --- Pulsante Radio della maschera GSPC_KCA.
    * --- "A"= Mantieni valuta della scheda tecnica originaria
    * --- "B"= Prendi valuta della commessa di destinazione
    * --- Controlla se esiste gi� un movimento preventivo per CODCOMD e ATTIVITA, usato a pag 2
    * --- Valuta della commessa di destinazione
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNCODVAL"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CODCOMD);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNCODVAL;
        from (i_cTable) where;
            CNCODCAN = this.oParentObject.w_CODCOMD;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.ValutaCommessa = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Assegnate per la Ricostruzione Saldi Costi
    this.w_CODCOM = this.oParentObject.w_CODCOMD
    this.w_CODVAL = this.ValutaCommessa
    * --- Cambio valuta e decimali di commessa di destinazione
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.ValutaCommessa);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.ValutaCommessa;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOCOM = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Valuta della commessa di Partenza
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNCODVAL"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CODCOMS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNCODVAL;
        from (i_cTable) where;
            CNCODCAN = this.oParentObject.w_CODCOMS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.ValutaCommeOld = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Stato Provvisorio
    this.w__STATO = "P"
    * --- Data Inizio Attivit� = Oggi
    this.w_DATINI = i_datsys
    * --- Try
    local bErr_033C9E58
    bErr_033C9E58=bTrsErr
    this.Try_033C9E58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
    endif
    bTrsErr=bTrsErr or bErr_033C9E58
    * --- End
    ah_ErrorMsg("Esecuzione terminata","","")
  endproc
  proc Try_033C9E58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" ATTIVITA ";
          +" where ATTIPATT='A' and ATCODATT>="+cp_ToStrODBC(this.oParentObject.w_CODATTS)+" and ATCODATT<="+cp_ToStrODBC(this.oParentObject.w_CODATT2)+" and ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+"";
           ,"_Curs_ATTIVITA")
    else
      select * from (i_cTable);
       where ATTIPATT="A" and ATCODATT>=this.oParentObject.w_CODATTS and ATCODATT<=this.oParentObject.w_CODATT2 and ATCODCOM=this.oParentObject.w_CODCOMS;
        into cursor _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      select _Curs_ATTIVITA
      locate for 1=1
      do while not(eof())
      * --- Copiati dal DataBase
      this.w_ATTIVITA = _Curs_ATTIVITA.ATCODATT
      this.w_TIPCOM = _Curs_ATTIVITA.ATTIPCOM
      this.w_DESCRI = _Curs_ATTIVITA.ATDESCRI
      this.w_DESSUP = _Curs_ATTIVITA.ATDESSUP
      this.w_CENCOS = _Curs_ATTIVITA.ATCENCOS
      this.w_TOTCOS = _Curs_ATTIVITA.ATTOTCOS
      this.w_DURGIO = _Curs_ATTIVITA.ATDURGIO
      this.w_VINCOL = _Curs_ATTIVITA.ATVINCOL
      this.w_DATVIN = _Curs_ATTIVITA.ATDATVIN
      this.w_CARDIN = _Curs_ATTIVITA.ATCARDIN
      this.w___RIGA = _Curs_ATTIVITA.AT__RIGA
      this.w_FLPREV = _Curs_ATTIVITA.ATFLPREV
      this.w__OKTEC = _Curs_ATTIVITA.AT_OKTEC
      this.w__OKAMM = _Curs_ATTIVITA.AT_OKAMM
      this.w__OKGES = _Curs_ATTIVITA.AT_OKGES
      this.w_GRPMAN = _Curs_ATTIVITA.ATGRPMAN
      this.w_CODFAM = _Curs_ATTIVITA.ATCODFAM
      this.w_PERCOM = _Curs_ATTIVITA.ATPERCOM
      this.w__PRJVW = _Curs_ATTIVITA.AT_PRJVW
      * --- Data Fine Attivit� = Data Corrente + Durata Attivit�
      this.w_DATFIN = i_datsys+this.w_DURGIO
      * --- Se non presente, l'attivit� viene inserita nella commessa di destinazione
      * --- Try
      local bErr_033C32C0
      bErr_033C32C0=bTrsErr
      this.Try_033C32C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_Msg("Attivit� %1 gi� presente nella commessa %2",.f.,.f.,2,alltrim(this.w_ATTIVITA),alltrim(this.oParentObject.w_CODCOMD))
      endif
      bTrsErr=bTrsErr or bErr_033C32C0
      * --- End
      * --- Copia i movimenti preventivi
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_ATTIVITA
        continue
      enddo
      use
    endif
    * --- Ricostruzione Saldi
    this.w_NOMESS = "N"
    do GSPC_BRS with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_033C32C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ATCODCOM"+",ATTIPATT"+",ATCODATT"+",ATTIPCOM"+",ATDESCRI"+",ATDESSUP"+",ATCENCOS"+",ATTOTCOS"+",ATDATINI"+",ATDATFIN"+",ATDURGIO"+",ATVINCOL"+",ATDATVIN"+",ATCARDIN"+",AT__RIGA"+",ATFLPREV"+",AT_STATO"+",AT_OKTEC"+",AT_OKAMM"+",AT_OKGES"+",ATGRPMAN"+",ATCODFAM"+",ATPERCOM"+",AT_PRJVW"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'ATTIVITA','ATCODCOM');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ATTIVITA','ATTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATTIVITA),'ATTIVITA','ATCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCOM),'ATTIVITA','ATTIPCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'ATTIVITA','ATDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'ATTIVITA','ATDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOS),'ATTIVITA','ATCENCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTCOS),'ATTIVITA','ATTOTCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'ATTIVITA','ATDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'ATTIVITA','ATDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DURGIO),'ATTIVITA','ATDURGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VINCOL),'ATTIVITA','ATVINCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATVIN),'ATTIVITA','ATDATVIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CARDIN),'ATTIVITA','ATCARDIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w___RIGA),'ATTIVITA','AT__RIGA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLPREV),'ATTIVITA','ATFLPREV');
      +","+cp_NullLink(cp_ToStrODBC(this.w__STATO),'ATTIVITA','AT_STATO');
      +","+cp_NullLink(cp_ToStrODBC(this.w__OKTEC),'ATTIVITA','AT_OKTEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w__OKAMM),'ATTIVITA','AT_OKAMM');
      +","+cp_NullLink(cp_ToStrODBC(this.w__OKGES),'ATTIVITA','AT_OKGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRPMAN),'ATTIVITA','ATGRPMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODFAM),'ATTIVITA','ATCODFAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERCOM),'ATTIVITA','ATPERCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w__PRJVW),'ATTIVITA','AT_PRJVW');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.oParentObject.w_CODCOMD,'ATTIPATT',"A",'ATCODATT',this.w_ATTIVITA,'ATTIPCOM',this.w_TIPCOM,'ATDESCRI',this.w_DESCRI,'ATDESSUP',this.w_DESSUP,'ATCENCOS',this.w_CENCOS,'ATTOTCOS',this.w_TOTCOS,'ATDATINI',this.w_DATINI,'ATDATFIN',this.w_DATFIN,'ATDURGIO',this.w_DURGIO,'ATVINCOL',this.w_VINCOL)
      insert into (i_cTable) (ATCODCOM,ATTIPATT,ATCODATT,ATTIPCOM,ATDESCRI,ATDESSUP,ATCENCOS,ATTOTCOS,ATDATINI,ATDATFIN,ATDURGIO,ATVINCOL,ATDATVIN,ATCARDIN,AT__RIGA,ATFLPREV,AT_STATO,AT_OKTEC,AT_OKAMM,AT_OKGES,ATGRPMAN,ATCODFAM,ATPERCOM,AT_PRJVW &i_ccchkf. );
         values (;
           this.oParentObject.w_CODCOMD;
           ,"A";
           ,this.w_ATTIVITA;
           ,this.w_TIPCOM;
           ,this.w_DESCRI;
           ,this.w_DESSUP;
           ,this.w_CENCOS;
           ,this.w_TOTCOS;
           ,this.w_DATINI;
           ,this.w_DATFIN;
           ,this.w_DURGIO;
           ,this.w_VINCOL;
           ,this.w_DATVIN;
           ,this.w_CARDIN;
           ,this.w___RIGA;
           ,this.w_FLPREV;
           ,this.w__STATO;
           ,this.w__OKTEC;
           ,this.w__OKAMM;
           ,this.w__OKGES;
           ,this.w_GRPMAN;
           ,this.w_CODFAM;
           ,this.w_PERCOM;
           ,this.w__PRJVW;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Attivit� %1 copiata nella commessa %2",.f.,.f.,2,alltrim(this.w_ATTIVITA),alltrim(this.oParentObject.w_CODCOMD))
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Data di registrazione = Oggi
    this.w_MADATREG = i_datsys
    this.w_MA__ANNO = ALLTRIM(STR(YEAR(i_DATSYS)))
    * --- Tipo Movimento = Preventivo
    this.w_MATIPMOV = "P"
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    * --- Inserito e variato dall'utente corrente
    * --- Movimenti preventivi associati alla commessa e all'attivit� da copiare
    * --- Select from MAT_MAST
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_MAST ";
          +" where MACODCOM="+cp_ToStrODBC(this.oParentObject.w_CODCOMS)+" and MACODATT="+cp_ToStrODBC(this.w_ATTIVITA)+" and MATIPMOV='P'";
           ,"_Curs_MAT_MAST")
    else
      select * from (i_cTable);
       where MACODCOM=this.oParentObject.w_CODCOMS and MACODATT=this.w_ATTIVITA and MATIPMOV="P";
        into cursor _Curs_MAT_MAST
    endif
    if used('_Curs_MAT_MAST')
      select _Curs_MAT_MAST
      locate for 1=1
      do while not(eof())
      this.w_MASERIAL = _Curs_MAT_MAST.MASERIAL
      this.w_MACODVAL = _Curs_MAT_MAST.MACODVAL
      * --- Pulsante Radio in GSPC_KCA
      if this.oParentObject.w_RADIOVAL="A"
        * --- Prende la valuta della scheda tecnica di partenza
        this.w_NEWCODVAL = this.w_MACODVAL
      else
        * --- Prende la valuta della commessa della scheda tecnica di destinazione
        this.w_NEWCODVAL = this.ValutaCommessa
      endif
      * --- Legge il cambio della valuta nella variabile w_MACAOVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VACAOVAL"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_NEWCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VACAOVAL;
          from (i_cTable) where;
              VACODVAL = this.w_NEWCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MACAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Assegnamento eseguito come in GSPC_MAT
      this.w_MACAOVAL = IIF(this.w_MACAOVAL=0,GETCAM(this.w_MACODVAL, this.w_MADATREG, 0),this.w_MACAOVAL)
      if this.ValutaCommessa<>this.ValutaCommeOld
        this.w_MACODLIS = SPACE(5)
      else
        this.w_MACODLIS = _Curs_MAT_MAST.MACODLIS
      endif
      * --- Contolla se esiste gi� un movimento preventivo
      vq_exec("..\COMM\EXE\QUERY\GSPC_BAV.VQR",this,"ResultSet")
      select Quanti from ResultSet into array MovimentiPreventivi
      this.CopiaMovimento = (MovimentiPreventivi = 0)
      if this.CopiaMovimento
        this.w_NEWSERIAL = cp_GetProg("MAT_MAST","SEMAT",this.w_NEWSERIAL,i_codazi)
        this.w_MANUMREG = cp_GetProg("MAT_MAST","NUMAT",this.w_MANUMREG,i_codazi,this.w_MA__ANNO,this.w_MAALFREG)
        if this.w_MACODVAL <> this.w_NEWCODVAL
          * --- Se � cambiata la valuta del movimento, non viene copiato il listino
          * --- Insert into MAT_MAST
          i_nConn=i_TableProp[this.MAT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MASERIAL"+",MACODCOM"+",MACODATT"+",MANUMREG"+",MADATREG"+",MA__ANNO"+",MATIPMOV"+",MACODVAL"+",MACAOVAL"+",MAALFREG"+",MA__ANNO"+",UTCV"+",UTCC"+",UTDV"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_MAST','MASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'MAT_MAST','MACODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ATTIVITA),'MAT_MAST','MACODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MANUMREG),'MAT_MAST','MANUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADATREG),'MAT_MAST','MADATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPMOV),'MAT_MAST','MATIPMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAALFREG),'MAT_MAST','MAALFREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'MAT_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'MAT_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'MAT_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'MAT_MAST','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'MACODCOM',this.oParentObject.w_CODCOMD,'MACODATT',this.w_ATTIVITA,'MANUMREG',this.w_MANUMREG,'MADATREG',this.w_MADATREG,'MA__ANNO',this.w_MA__ANNO,'MATIPMOV',this.w_MATIPMOV,'MACODVAL',this.w_NEWCODVAL,'MACAOVAL',this.w_MACAOVAL,'MAALFREG',this.w_MAALFREG,'MA__ANNO',this.w_MA__ANNO,'UTCV',this.w_UTCV)
            insert into (i_cTable) (MASERIAL,MACODCOM,MACODATT,MANUMREG,MADATREG,MA__ANNO,MATIPMOV,MACODVAL,MACAOVAL,MAALFREG,MA__ANNO,UTCV,UTCC,UTDV,UTDC &i_ccchkf. );
               values (;
                 this.w_NEWSERIAL;
                 ,this.oParentObject.w_CODCOMD;
                 ,this.w_ATTIVITA;
                 ,this.w_MANUMREG;
                 ,this.w_MADATREG;
                 ,this.w_MA__ANNO;
                 ,this.w_MATIPMOV;
                 ,this.w_NEWCODVAL;
                 ,this.w_MACAOVAL;
                 ,this.w_MAALFREG;
                 ,this.w_MA__ANNO;
                 ,this.w_UTCV;
                 ,this.w_UTCC;
                 ,this.w_UTDV;
                 ,this.w_UTDC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Altrimenti viene copiato anche il listino
          * --- Insert into MAT_MAST
          i_nConn=i_TableProp[this.MAT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MASERIAL"+",MACODCOM"+",MACODATT"+",MANUMREG"+",MADATREG"+",MA__ANNO"+",MATIPMOV"+",MACODVAL"+",MACAOVAL"+",MAALFREG"+",UTCV"+",UTCC"+",UTDV"+",UTDC"+",MACODLIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_MAST','MASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOMD),'MAT_MAST','MACODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ATTIVITA),'MAT_MAST','MACODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MANUMREG),'MAT_MAST','MANUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MADATREG),'MAT_MAST','MADATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPMOV),'MAT_MAST','MATIPMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MAALFREG),'MAT_MAST','MAALFREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'MAT_MAST','UTCV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'MAT_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'MAT_MAST','UTDV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'MAT_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MACODLIS),'MAT_MAST','MACODLIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'MACODCOM',this.oParentObject.w_CODCOMD,'MACODATT',this.w_ATTIVITA,'MANUMREG',this.w_MANUMREG,'MADATREG',this.w_MADATREG,'MA__ANNO',this.w_MA__ANNO,'MATIPMOV',this.w_MATIPMOV,'MACODVAL',this.w_NEWCODVAL,'MACAOVAL',this.w_MACAOVAL,'MAALFREG',this.w_MAALFREG,'UTCV',this.w_UTCV,'UTCC',this.w_UTCC)
            insert into (i_cTable) (MASERIAL,MACODCOM,MACODATT,MANUMREG,MADATREG,MA__ANNO,MATIPMOV,MACODVAL,MACAOVAL,MAALFREG,UTCV,UTCC,UTDV,UTDC,MACODLIS &i_ccchkf. );
               values (;
                 this.w_NEWSERIAL;
                 ,this.oParentObject.w_CODCOMD;
                 ,this.w_ATTIVITA;
                 ,this.w_MANUMREG;
                 ,this.w_MADATREG;
                 ,this.w_MA__ANNO;
                 ,this.w_MATIPMOV;
                 ,this.w_NEWCODVAL;
                 ,this.w_MACAOVAL;
                 ,this.w_MAALFREG;
                 ,this.w_UTCV;
                 ,this.w_UTCC;
                 ,this.w_UTDV;
                 ,this.w_UTDC;
                 ,this.w_MACODLIS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if ah_YesNo("Esiste gi� un movimento preventivo per l'attivit� %1%0si desidera sostituirlo?","",this.w_ATTIVITA)
          * --- Legge il seriale della scheda tecnica nella variabile w_MASERIAL
          * --- Read from MAT_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MASERIAL"+;
              " from "+i_cTable+" MAT_MAST where ";
                  +"MACODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOMD);
                  +" and MACODATT = "+cp_ToStrODBC(this.w_ATTIVITA);
                  +" and MATIPMOV = "+cp_ToStrODBC("P");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MASERIAL;
              from (i_cTable) where;
                  MACODCOM = this.oParentObject.w_CODCOMD;
                  and MACODATT = this.w_ATTIVITA;
                  and MATIPMOV = "P";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NEWSERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_MACODVAL <> this.w_NEWCODVAL
            * --- Aggiorna valuta e cambio
            * --- Write into MAT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MAT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MACODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
              +",MACAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
                  +i_ccchkf ;
              +" where ";
                  +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
                     )
            else
              update (i_cTable) set;
                  MACODVAL = this.w_NEWCODVAL;
                  ,MACAOVAL = this.w_MACAOVAL;
                  &i_ccchkf. ;
               where;
                  MASERIAL = this.w_NEWSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Aggiorna valuta, cambio e listino
            * --- Write into MAT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MAT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MAT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MACODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCODVAL),'MAT_MAST','MACODVAL');
              +",MACAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
              +",MACODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MACODLIS),'MAT_MAST','MACODLIS');
                  +i_ccchkf ;
              +" where ";
                  +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
                     )
            else
              update (i_cTable) set;
                  MACODVAL = this.w_NEWCODVAL;
                  ,MACAOVAL = this.w_MACAOVAL;
                  ,MACODLIS = this.w_MACODLIS;
                  &i_ccchkf. ;
               where;
                  MASERIAL = this.w_NEWSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Cancella il dettaglio della scheda tecnica di destinazione
          * --- Delete from MAT_DETT
          i_nConn=i_TableProp[this.MAT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MASERIAL = "+cp_ToStrODBC(this.w_NEWSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  MASERIAL = this.w_NEWSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
        select _Curs_MAT_MAST
        continue
      enddo
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" MAT_DETT ";
          +" where MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)+"";
          +" order by CPROWNUM";
           ,"_Curs_MAT_DETT")
    else
      select * from (i_cTable);
       where MASERIAL=this.w_MASERIAL;
       order by CPROWNUM;
        into cursor _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      select _Curs_MAT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = _Curs_MAT_DETT.CPROWNUM
      this.w_CPROWORD = _Curs_MAT_DETT.CPROWORD
      this.w_MACODKEY = _Curs_MAT_DETT.MACODKEY
      this.w_MACODART = _Curs_MAT_DETT.MACODART
      this.w_MAUNIMIS = _Curs_MAT_DETT.MAUNIMIS
      this.w_MAQTAMOV = _Curs_MAT_DETT.MAQTAMOV
      this.w_MAPREZZO = _Curs_MAT_DETT.MAPREZZO
      if this.w_MACODVAL<>this.w_NEWCODVAL
        this.w_MAPREZZO = valcam(this.w_MAPREZZO, this.w_MACODVAL, this.w_NEWCODVAL, i_datsys, 0)
      endif
      this.w_MADATANN = _Curs_MAT_DETT.MADATANN
      this.w_MADESCRI = _Curs_MAT_DETT.MADESCRI
      this.w_MA__NOTE = _Curs_MAT_DETT.MA__NOTE
      this.w_MACODCOS = _Curs_MAT_DETT.MACODCOS
      this.w_MAOPPREV = _Curs_MAT_DETT.MAOPPREV
      this.w_MAOPCONS = _Curs_MAT_DETT.MAOPCONS
      this.w_MAQTA1UM = _Curs_MAT_DETT.MAQTA1UM
      this.w_MACOATTS = _Curs_MAT_DETT.MACOATTS
      this.w_MATIPATT = _Curs_MAT_DETT.MATIPATT
      this.w_MACODFAM = _Curs_MAT_DETT.MACODFAM
      this.w_MACOCOMS = this.oParentObject.w_CODCOMD
      this.w_MAVALRIG = IIF(NOT EMPTY(NVL(this.w_MADATANN,cp_CharToDate("  -  -  "))),0,VAL2MON(this.w_MAPREZZO,this.w_MACAOVAL,this.w_CAOCOM,g_DATEUR)*this.w_MAQTAMOV)
      * --- Inserisce il movimento di dettaglio con chiave w_NEWSERIAL
      if CP_TODATE(_Curs_MAT_DETT.MADATANN)<i_datsys
        * --- Insert into MAT_DETT
        i_nConn=i_TableProp[this.MAT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MASERIAL"+",CPROWNUM"+",CPROWORD"+",MACODKEY"+",MACODART"+",MAUNIMIS"+",MAQTAMOV"+",MAPREZZO"+",MADATANN"+",MADESCRI"+",MA__NOTE"+",MACODCOS"+",MAOPPREV"+",MAOPCONS"+",MAQTA1UM"+",MAVALRIG"+",MACOATTS"+",MATIPATT"+",MACOCOMS"+",MACODFAM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'MAT_DETT','MASERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MAT_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MAT_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACODKEY),'MAT_DETT','MACODKEY');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACODART),'MAT_DETT','MACODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAUNIMIS),'MAT_DETT','MAUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTAMOV),'MAT_DETT','MAQTAMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAPREZZO),'MAT_DETT','MAPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MADATANN),'MAT_DETT','MADATANN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'MAT_DETT','MADESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MA__NOTE),'MAT_DETT','MA__NOTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MAT_DETT','MACODCOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPPREV),'MAT_DETT','MAOPPREV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPCONS),'MAT_DETT','MAOPCONS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTA1UM),'MAT_DETT','MAQTA1UM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MAVALRIG),'MAT_DETT','MAVALRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MAT_DETT','MACOATTS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MAT_DETT','MATIPATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MAT_DETT','MACOCOMS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_MAT_DETT.MACODFAM),'MAT_DETT','MACODFAM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_NEWSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MACODKEY',this.w_MACODKEY,'MACODART',this.w_MACODART,'MAUNIMIS',this.w_MAUNIMIS,'MAQTAMOV',this.w_MAQTAMOV,'MAPREZZO',this.w_MAPREZZO,'MADATANN',this.w_MADATANN,'MADESCRI',this.w_MADESCRI,'MA__NOTE',this.w_MA__NOTE,'MACODCOS',this.w_MACODCOS)
          insert into (i_cTable) (MASERIAL,CPROWNUM,CPROWORD,MACODKEY,MACODART,MAUNIMIS,MAQTAMOV,MAPREZZO,MADATANN,MADESCRI,MA__NOTE,MACODCOS,MAOPPREV,MAOPCONS,MAQTA1UM,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MACODFAM &i_ccchkf. );
             values (;
               this.w_NEWSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_MACODKEY;
               ,this.w_MACODART;
               ,this.w_MAUNIMIS;
               ,this.w_MAQTAMOV;
               ,this.w_MAPREZZO;
               ,this.w_MADATANN;
               ,this.w_MADESCRI;
               ,this.w_MA__NOTE;
               ,this.w_MACODCOS;
               ,this.w_MAOPPREV;
               ,this.w_MAOPCONS;
               ,this.w_MAQTA1UM;
               ,this.w_MAVALRIG;
               ,this.w_MACOATTS;
               ,this.w_MATIPATT;
               ,this.w_MACOCOMS;
               ,_Curs_MAT_DETT.MACODFAM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_MAT_DETT
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='MAT_DETT'
    this.cWorkTables[4]='MAT_MAST'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_MAT_DETT')
      use in _Curs_MAT_DETT
    endif
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_MAT_MAST')
      use in _Curs_MAT_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
