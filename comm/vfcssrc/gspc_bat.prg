* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bat                                                        *
*              Legge dati padre (attivita)                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-15                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bat",oParentObject,m.pTIPO)
return(i_retval)

define class tgspc_bat as StdBatch
  * --- Local variables
  pTIPO = space(10)
  w_OLDPIANIF = space(1)
  w_SERIAL = space(10)
  w_MESS = space(200)
  w_NUMREG = 0
  w_VEAC = space(1)
  w_DATREG = ctod("  /  /  ")
  * --- WorkFile variables
  MAT_MAST_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTIPO="SON"
        * --- Se figlio non instanziato lo instanzio
        if Upper( This.OparentObject.GSPC_MAP.class)="STDDYNAMICCHILD"
          This.OparentObject.oPgFrm.Pages[2].opag.uienable(.T.)
        endif
        * --- Passa i valori della chiave dal Padre (Attivit�) al figlio (Att. Precedenti)
        this.oParentObject.GSPC_MAP.w_CODCOM=this.oParentObject.w_ATCODCOM
        this.oParentObject.GSPC_MAP.w_CODATT=this.oParentObject.w_ATCODATT
        this.oParentObject.GSPC_MAP.w_TIPATT=this.oParentObject.w_ATTIPATT
      case this.pTIPO="CHECK"
        * --- Se Spianifico una Attivit� spianificata
        Select (This.oParentObject.cCursor)
        this.w_OLDPIANIF = nvl(AT_STATO,"0")
        if this.w_OLDPIANIF<>this.oParentObject.w_AT_STATO
          * --- L'utente ha modificato lo stato dell'Attivit�
          if this.oParentObject.w_AT_STATO $ "PC" and not this.w_OLDPIANIF $ "PC"
            * --- L'attivit� � stata "Spianificata"
            this.w_SERIAL = Space(10)
            * --- Leggo il seriale del movimento preventivo
            * --- Read from MAT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MASERIAL"+;
                " from "+i_cTable+" MAT_MAST where ";
                    +"MACODATT = "+cp_ToStrODBC(this.oParentObject.w_ATCODATT);
                    +" and MACODCOM = "+cp_ToStrODBC(this.oParentObject.w_ATCODCOM);
                    +" and MATIPMOV = "+cp_ToStrODBC("P");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MASERIAL;
                from (i_cTable) where;
                    MACODATT = this.oParentObject.w_ATCODATT;
                    and MACODCOM = this.oParentObject.w_ATCODCOM;
                    and MATIPMOV = "P";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty(nvl(this.w_SERIAL,""))
              * --- Leggo i dati del documento generato dalla Pianificazione
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVNUMREG,MVDATREG,MVFLVEAC"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVMOVCOM = "+cp_ToStrODBC(this.w_SERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVNUMREG,MVDATREG,MVFLVEAC;
                  from (i_cTable) where;
                      MVMOVCOM = this.w_SERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_NUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
                this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
                this.w_VEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if ! Empty(NVL(this.w_DATREG,cp_CharToDate("  -  -  ")))
                if this.w_VEAC = "V"
                  this.w_MESS = AH_MSGFORMAT( "Occorre cancellare il documento generato dalla pianificazione.%0Tipo: documento interno ciclo attivo%0Doc. n: %1 del %2" , ALLTRIM( STR( this.w_NUMREG ) ) , DTOC( this.w_DATREG ) )
                else
                  this.w_MESS = AH_MSGFORMAT( "Occorre cancellare il documento generato dalla pianificazione.%0Tipo: documento interno ciclo passivo%0Doc. n: %1 del %2" , ALLTRIM( STR( this.w_NUMREG ) ) , DTOC( this.w_DATREG ) )
                endif
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.w_MESS
              endif
            endif
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MAT_MAST'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
