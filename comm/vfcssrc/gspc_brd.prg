* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_brd                                                        *
*              Generazione mov.comm.                                           *
*                                                                              *
*      Author: Zucchetti Spa CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_200]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-21                                                      *
* Last revis.: 2008-06-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_brd",oParentObject,m.pAzione)
return(i_retval)

define class tgspc_brd as StdBatch
  * --- Local variables
  w_SCOLIS = space(1)
  pAzione = space(10)
  w_TIME1 = ctod("  /  /  ")
  w_TIME2 = ctod("  /  /  ")
  w_NUMSER = space(15)
  w_RIGA = 0
  w_COMM = space(15)
  w_CODDIP = space(5)
  w_DESCOMM = space(30)
  w_DESDIP = space(40)
  w_SERVIZIO = space(41)
  w_UT = 0
  NC = space(10)
  w_OGGI = ctod("  /  /  ")
  w_DATA = ctod("  /  /  ")
  w_UMDEFA = space(3)
  w_RDNUMRIL = space(15)
  w_MESSAGGIO = space(250)
  w_DATALIS2 = ctod("  /  /  ")
  w_DATALIS1 = ctod("  /  /  ")
  w_DECTOT = 0
  w_MASERIAL = space(10)
  w_MANUMREG = 0
  w_MATIPMOV = space(1)
  w_MACODVAL = space(3)
  w_MACAOVAL = 0
  w_MACODLIS = space(5)
  w_MAALFREG = 0
  w_MADATREG = ctod("  /  /  ")
  w_MACODATT = space(15)
  w_MACODCOM = space(15)
  w_VALCOM = space(3)
  w_CAOCOM = 0
  w_DECCOM = space(1)
  w_ROWNUM = 0
  w_CPROWORD = 0
  w_MACODKEY = space(41)
  w_MACODART = space(20)
  w_MACODVAR = space(20)
  w_MAUNIMIS = space(3)
  w_MAQTAMOV = 0
  w_MAPREZZO = 0
  w_MADESCRI = space(40)
  w_MAOPPREV = space(1)
  w_MACOATTS = space(15)
  w_MACOCOMS = space(15)
  w_MATIPATT = space(1)
  w_MAOPCONS = space(1)
  w_MANOTE = space(0)
  w_MACODCOS = space(5)
  w_MAVALRIG = 0
  w_MADATANN = ctod("  /  /  ")
  w_MACODFAM = space(5)
  w_COSTOH = 0
  w_ERRORE = .f.
  w_ESCO = .f.
  TmpC = space(100)
  w_CPROWNUM = 0
  w_CONTARIG = 0
  GSPC_MRD = .NULL.
  ROWORD = 0
  w_CODATT = space(15)
  w_RIGAMOV = 0
  w_PROG = .NULL.
  w_UNIMISLI = space(3)
  w_MA__ANNO = space(4)
  w_CODART = space(20)
  w_DATREG = ctod("  /  /  ")
  w_QTAUM1 = 0
  w_CODLIS = space(5)
  w_IVALIS = space(1)
  w_MAQTA1UM = 0
  w_CODVAL = space(3)
  w_LIPREZZO = 0
  w_DTOBS1 = ctod("  /  /  ")
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_TIPCOD = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_UNMIS1 = space(3)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_CATANAL = space(5)
  w_VOCCOST = space(15)
  w_UMCAL = space(10)
  w_CODVAL = space(3)
  w_CODCOS = space(5)
  w_TOTRIGA = 0
  w_CODCOSDE = space(5)
  w_CODFAM = space(5)
  w_PIANIF = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_CALPRZ = 0
  w_UMLIPREZZO = space(3)
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTALIS = 0
  w_LSerial = space(10)
  w_LNumErr = 0
  w_LOperaz = space(20)
  w_LOggErr = space(41)
  w_LErrore = space(80)
  w_LOra = space(8)
  w_LMessage = space(0)
  w_LSelOpe = space(1)
  w_LDettTec = space(1)
  w_LDatini = ctod("  /  /  ")
  w_LDatfin = ctod("  /  /  ")
  * --- WorkFile variables
  DICH_MDO_idx=0
  CAN_TIER_idx=0
  DIPENDEN_idx=0
  LISTINI_idx=0
  VALUTE_idx=0
  ATTIVITA_idx=0
  MAT_MAST_idx=0
  KEY_ARTI_idx=0
  LIS_TINI_idx=0
  ART_ICOL_idx=0
  VOCIIVA_idx=0
  CAANARTI_idx=0
  VOC_COST_idx=0
  TIPCOSTO_idx=0
  MAT_DETT_idx=0
  MA_COSTI_idx=0
  CPAR_DEF_idx=0
  COMM_ERR_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Anagrafica Dichiarazioni (GSPC_ARD) e Generazione Movimenti di Commessa (GSPC_KGM)
    * --- Caller
    * --- Locali
    do case
      case this.pAzione="DELTAT"
        * --- Calcola DeltaT = (data fine - data inizio)
        this.oParentObject.w_RDORAINI = chktime(this.oParentObject.w_RDORAINI)
        this.oParentObject.w_RDORAINI = iif(empty(CTOT(this.oParentObject.w_RDORAINI)), space(8), this.oParentObject.w_RDORAINI)
        this.oParentObject.w_RDORAFIN = chktime(this.oParentObject.w_RDORAFIN)
        this.oParentObject.w_RDORAFIN = iif(empty(CTOT(this.oParentObject.w_RDORAFIN)), space(8), this.oParentObject.w_RDORAFIN)
        if not (empty(this.oParentObject.w_RDORAINI) or empty(this.oParentObject.w_RDORAFIN) or empty(this.oParentObject.w_RDDATINI) or empty(this.oParentObject.w_RDDATFIN))
          this.w_TIME1 = cp_CharToDatetime(DTOC(this.oParentObject.w_RDDATINI) +" "+ this.oParentObject.w_RDORAINI)
          this.w_TIME2 = cp_CharToDatetime(DTOC(this.oParentObject.w_RDDATFIN) +" "+ this.oParentObject.w_RDORAFIN)
          if this.w_TIME2 < this.w_TIME1
            this.oParentObject.w_RDDATFIN = this.oParentObject.w_RDDATINI
            this.oParentObject.w_RDORAFIN = this.oParentObject.w_RDORAINI
          endif
          this.oParentObject.w_RDSECON = max(0, this.w_TIME2 - this.w_TIME1)
          this.oParentObject.w_RDTEMPOR = cp_round(this.oParentObject.w_RDSECON/3600,3)
        else
          this.oParentObject.w_RDSECON = 0
          this.oParentObject.w_RDTEMPOR = 0
        endif
      case this.pAzione="TEMPOCH"
        if not (empty(this.oParentObject.w_RDORAINI) or empty(this.oParentObject.w_RDORAFIN) or empty(this.oParentObject.w_RDDATINI) or empty(this.oParentObject.w_RDDATFIN))
          this.w_TIME2 = cp_CharToDatetime(DTOC(this.oParentObject.w_RDDATFIN) +" "+ this.oParentObject.w_RDORAFIN)
          this.w_TIME1 = this.w_TIME2 - this.oParentObject.w_RDTEMPOR*3600
          this.oParentObject.w_RDDATINI = ttod(this.w_TIME1)
          this.oParentObject.w_RDORAINI = chktime(this.w_TIME1)
        endif
        this.oParentObject.w_RDSECON = cp_round(this.oParentObject.w_RDTEMPOR*3600,3)
      case this.pAzione="NEWRCD"
        * --- Legge il massimo seriale
        * --- Select from DICH_MDO
        i_nConn=i_TableProp[this.DICH_MDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2],.t.,this.DICH_MDO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(RDNUMRIL) AS NUMSER  from "+i_cTable+" DICH_MDO ";
               ,"_Curs_DICH_MDO")
        else
          select MAX(RDNUMRIL) AS NUMSER from (i_cTable);
            into cursor _Curs_DICH_MDO
        endif
        if used('_Curs_DICH_MDO')
          select _Curs_DICH_MDO
          locate for 1=1
          do while not(eof())
          this.w_NUMSER = NVL(NUMSER,SPACE(15))
            select _Curs_DICH_MDO
            continue
          enddo
          use
        endif
        * --- Legge la riga max del seriale max
        * --- Select from DICH_MDO
        i_nConn=i_TableProp[this.DICH_MDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2],.t.,this.DICH_MDO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) as RIGA  from "+i_cTable+" DICH_MDO ";
              +" where RDNUMRIL="+cp_ToStrODBC(this.w_NUMSER)+"";
               ,"_Curs_DICH_MDO")
        else
          select MAX(CPROWNUM) as RIGA from (i_cTable);
           where RDNUMRIL=this.w_NUMSER;
            into cursor _Curs_DICH_MDO
        endif
        if used('_Curs_DICH_MDO')
          select _Curs_DICH_MDO
          locate for 1=1
          do while not(eof())
          this.w_RIGA = NVL(RIGA,0)
            select _Curs_DICH_MDO
            continue
          enddo
          use
        endif
        * --- Leggo Commessa e Dipendente
        * --- Read from DICH_MDO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DICH_MDO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2],.t.,this.DICH_MDO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RDCODCOM,RDDIPEND,RDCODATT"+;
            " from "+i_cTable+" DICH_MDO where ";
                +"RDNUMRIL = "+cp_ToStrODBC(this.w_NUMSER);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RDCODCOM,RDDIPEND,RDCODATT;
            from (i_cTable) where;
                RDNUMRIL = this.w_NUMSER;
                and CPROWNUM = this.w_RIGA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COMM = NVL(cp_ToDate(_read_.RDCODCOM),cp_NullValue(_read_.RDCODCOM))
          this.w_CODDIP = NVL(cp_ToDate(_read_.RDDIPEND),cp_NullValue(_read_.RDDIPEND))
          this.w_CODATT = NVL(cp_ToDate(_read_.RDCODATT),cp_NullValue(_read_.RDCODATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oparentobject.w_RDCODCOM=this.w_COMM
        this.oparentobject.w_RDDIPEND=this.w_CODDIP
        this.oparentobject.w_RDCODATT=this.w_CODATT
        * --- Leggo lo Stato dell'Attivit�
        * --- Read from ATTIVITA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AT_STATO"+;
            " from "+i_cTable+" ATTIVITA where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_COMM);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                +" and ATTIPATT = "+cp_ToStrODBC("A");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AT_STATO;
            from (i_cTable) where;
                ATCODCOM = this.w_COMM;
                and ATCODATT = this.w_CODATT;
                and ATTIPATT = "A";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PIANIF = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oparentobject.w_PIANIF= this.w_PIANIF
        * --- Leggo le descrizioni
        * --- Commessa
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_COMM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN;
            from (i_cTable) where;
                CNCODCAN = this.w_COMM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCOMM = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oparentobject.w_RDDESCAN=this.w_DESCOMM
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCOGNOM,DPCODUTE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODDIP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCOGNOM,DPCODUTE;
            from (i_cTable) where;
                DPCODICE = this.w_CODDIP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESDIP = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
          this.oParentObject.w_UTENTE = NVL(cp_ToDate(_read_.DPCODUTE),cp_NullValue(_read_.DPCODUTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Descrizione addetto alla rilevazione
        this.oparentobject.w_RDCOGNOM=this.w_DESDIP
      case this.pAzione = "SEL_DESEL"
        * --- Seleziona/Deseleziona tutte le righe dello zoom
        * --- Nome cursore collegato allo zoom
        this.NC = this.oParentObject.w_ZoomSel.cCursor
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "GEN_MOV" or this.pAzione = "IMM_MOV" and this.oParentObject.w_RDAGGIMM="A"
        this.w_ERRORE = .F.
        this.w_ESCO = .F.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if used("Temp")
          use in Temp
        endif
        if this.w_ERRORE=.T.
          if ah_YesNo("Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?","", alltrim(str(this.w_LNumErr,5,0)) )
            * --- Lancia Stampa
            do GSPC_BLE with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.pAzione = "DEL_MOV"
        this.w_ERRORE = .F.
        this.NC = this.oParentObject.w_ZoomSel.cCursor
        select RDNUMRIL,CPROWNUM from (this.NC) where xchk=1 into cursor ElabCur 
 go top 
 scan
        this.w_RDNUMRIL = ElabCur.RDNUMRIL
        this.w_CPROWNUM = ElabCur.CPROWNUM
        this.w_RIGA = recno()
        * --- Leggo i dati che mi servono per lo storno di MA_COSTI e l'eliminazione della testata
        * --- Read from MAT_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MACODCOS,MAOPPREV,MAOPCONS,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MASERIAL"+;
            " from "+i_cTable+" MAT_DETT where ";
                +"MADICRIF = "+cp_ToStrODBC(this.w_RDNUMRIL);
                +" and MAROWRIF = "+cp_ToStrODBC(this.w_CPROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MACODCOS,MAOPPREV,MAOPCONS,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MASERIAL;
            from (i_cTable) where;
                MADICRIF = this.w_RDNUMRIL;
                and MAROWRIF = this.w_CPROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MACODCOS = NVL(cp_ToDate(_read_.MACODCOS),cp_NullValue(_read_.MACODCOS))
          this.w_MAOPPREV = NVL(cp_ToDate(_read_.MAOPPREV),cp_NullValue(_read_.MAOPPREV))
          this.w_MAOPCONS = NVL(cp_ToDate(_read_.MAOPCONS),cp_NullValue(_read_.MAOPCONS))
          this.w_MAVALRIG = NVL(cp_ToDate(_read_.MAVALRIG),cp_NullValue(_read_.MAVALRIG))
          this.w_MACOATTS = NVL(cp_ToDate(_read_.MACOATTS),cp_NullValue(_read_.MACOATTS))
          this.w_MATIPATT = NVL(cp_ToDate(_read_.MATIPATT),cp_NullValue(_read_.MATIPATT))
          this.w_MACOCOMS = NVL(cp_ToDate(_read_.MACOCOMS),cp_NullValue(_read_.MACOCOMS))
          this.w_MASERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MAOPCONS = iif(this.w_MAOPCONS="-","+",iif(this.w_MAOPCONS="+","-",this.w_MAOPCONS))
        this.w_MAOPPREV = iif(this.w_MAOPPREV="-","+",iif(this.w_MAOPPREV="+","-",this.w_MAOPPREV))
        * --- Read from MAT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2],.t.,this.MAT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MANUMREG,MAALFREG"+;
            " from "+i_cTable+" MAT_MAST where ";
                +"MASERIAL = "+cp_ToStrODBC(this.w_MASERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MANUMREG,MAALFREG;
            from (i_cTable) where;
                MASERIAL = this.w_MASERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MANUMREG = NVL(cp_ToDate(_read_.MANUMREG),cp_NullValue(_read_.MANUMREG))
          this.w_MAALFREG = NVL(cp_ToDate(_read_.MAALFREG),cp_NullValue(_read_.MAALFREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        ah_Msg("Eliminazione riga %1 movimento di commessa n. %2/%3",.T.,.F.,.F., alltrim(str(this.w_CPROWNUM)), alltrim(str(this.w_MANUMREG)), alltrim(str(this.w_MAALFREG))) 
        * --- Elimino la riga del movimento di commessa relativa alla Dichiarazione Selezionata
        * --- Try
        local bErr_033CBE98
        bErr_033CBE98=bTrsErr
        this.Try_033CBE98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_033CBE98
        * --- End
        select ElabCur 
 go this.w_RIGA
        endscan
        if used("ElabCur")
          use in ElabCur
        endif
        * --- Eseguo il refresh dello Zoom
        this.oParentobject.notifyevent("Interroga")
      case this.pAzione = "CARICA_DET"
        this.oParentObject.w_DETCARI = "S"
        this.GSPC_MRD = this.oParentObject
        vq_exec ("..\COMM\exe\query\GSPC_MRD", this, "_DETT_")
        if used("_DETT_")
          this.ROWORD = 0
          select _DETT_
          go top
          SCAN
          * --- Creazione DETTAGLIO
          * --- Nuova Riga del Temporaneo
          if this.ROWORD<>0
            this.GSPC_MRD.InitRow()     
          endif
          * --- Valorizza Variabili di Riga ...
          this.ROWORD = this.ROWORD+1
          this.GSPC_MRD.w_CPROWORD = this.ROWORD*10
          this.GSPC_MRD.w_RDCODATT = NVL(_DETT_.SUCODATT," ")
          this.GSPC_MRD.w_PIANIF = NVL(_DETT_.AT_STATO," ")
          * --- Carica il Temporaneo dei Dati e skippa al record successivo
          this.GSPC_MRD.TrsFromWork()     
          ENDSCAN
          use in _DETT_
          * --- Rinfrescamenti vari
          Private tc 
 tc = this.GSPC_MRD.cTrsName
          SELECT (tc) 
 GO TOP 
 With this.GSPC_MRD 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
        endif
      case this.pAzione="APREMOV"
        if this.oParentObject.w_RDPROCES="S"
          this.w_RDNUMRIL = this.oparentobject.w_RDNUMRIL
          this.w_RIGAMOV = this.oparentobject.w_cprownum
          * --- Read from MAT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MASERIAL"+;
              " from "+i_cTable+" MAT_DETT where ";
                  +"MADICRIF = "+cp_ToStrODBC(this.w_RDNUMRIL);
                  +" and MAROWRIF = "+cp_ToStrODBC(this.w_RIGAMOV);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MASERIAL;
              from (i_cTable) where;
                  MADICRIF = this.w_RDNUMRIL;
                  and MAROWRIF = this.w_RIGAMOV;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MASERIAL = NVL(cp_ToDate(_read_.MASERIAL),cp_NullValue(_read_.MASERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Dettaglio Documenti
          this.w_PROG = GSPC_MAT("C")
          if !(this.w_PROG.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_MASERIAL = this.w_MASERIAL
          this.w_PROG.QueryKeySet("MASERIAL='"+ this.w_MASERIAL +"'")     
          this.w_PROG.LoadRecWarn()     
        else
          ah_ErrorMsg("Dichiarazione non processata","","")
        endif
    endcase
  endproc
  proc Try_033CBE98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MADICRIF = "+cp_ToStrODBC(this.w_RDNUMRIL);
            +" and MAROWRIF = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      delete from (i_cTable) where;
            MADICRIF = this.w_RDNUMRIL;
            and MAROWRIF = this.w_CPROWNUM;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Storno MA_COSTI
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSPREVEN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSPREVEN');
      +",CSCONSUN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSCONSUN');
          +i_ccchkf ;
      +" where ";
          +"CSCODCOM = "+cp_ToStrODBC(this.w_MACOCOMS);
          +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MATIPATT);
          +" and CSCODMAT = "+cp_ToStrODBC(this.w_MACOATTS);
          +" and CSCODCOS = "+cp_ToStrODBC(this.w_MACODCOS);
             )
    else
      update (i_cTable) set;
          CSPREVEN = &i_cOp1.;
          ,CSCONSUN = &i_cOp2.;
          &i_ccchkf. ;
       where;
          CSCODCOM = this.w_MACOCOMS;
          and CSTIPSTR = this.w_MATIPATT;
          and CSCODMAT = this.w_MACOATTS;
          and CSCODCOS = this.w_MACODCOS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Metto la dichiarazione da processare
    * --- Write into DICH_MDO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DICH_MDO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DICH_MDO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RDPROCES ="+cp_NullLink(cp_ToStrODBC("N"),'DICH_MDO','RDPROCES');
          +i_ccchkf ;
      +" where ";
          +"RDNUMRIL = "+cp_ToStrODBC(this.w_RDNUMRIL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          RDPROCES = "N";
          &i_ccchkf. ;
       where;
          RDNUMRIL = this.w_RDNUMRIL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Verifico se dopo la cancellazione della riga rimane almeno una riga sul dettaglio
    * --- Select from MAT_DETT
    i_nConn=i_TableProp[this.MAT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2],.t.,this.MAT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count (*) as TOTRIGHE  from "+i_cTable+" MAT_DETT ";
          +" where MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)+"";
           ,"_Curs_MAT_DETT")
    else
      select count (*) as TOTRIGHE from (i_cTable);
       where MASERIAL=this.w_MASERIAL;
        into cursor _Curs_MAT_DETT
    endif
    if used('_Curs_MAT_DETT')
      select _Curs_MAT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CONTARIG = _Curs_MAT_DETT.TOTRIGHE
        select _Curs_MAT_DETT
        continue
      enddo
      use
    endif
    * --- Se il dettaglio � vuoto cancello anche la testata
    if nvl(this.w_CONTARIG,0)=0
      ah_Msg("Eliminazione movimento di commessa n. %1/%2",.T.,.F.,.F., alltrim(str(this.w_MANUMREG)), alltrim(str(this.w_MAALFREG)) )
      * --- Delete from MAT_MAST
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MASERIAL = "+cp_ToStrODBC(this.w_MASERIAL);
               )
      else
        delete from (i_cTable) where;
              MASERIAL = this.w_MASERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- variabili utilizzate in GSPC_BCL
    * --- Pulizia LOG
    this.w_LOperaz = "GM"
    * --- Try
    local bErr_035BF580
    bErr_035BF580=bTrsErr
    this.Try_035BF580()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035BF580
    * --- End
    if this.pAzione = "GEN_MOV" 
      this.NC = this.oParentObject.w_ZoomSel.cCursor
      select RDNUMRIL,RDDATRIL,RDCODCOM,RDCODATT,RDRILAUT,RDTEMPOR,RDDIPEND,RDDATINI,RDORAINI,; 
 RDDATFIN,RDORAFIN,CPROWNUM,space(41) as SERVIZIO, space(5) as LISTINO from (this.NC) where xchk=1 into cursor ElabCur
    else
      if (empty(nvl(this.oParentObject.w_RDCODATT,"")) or empty(nvl(this.oParentObject.w_RDTEMPOR,0))) and this.pAzione=="IMM_MOV"
        i_retcode = 'stop'
        return
      endif
      create cursor ElabCur (RDNUMRIL C(15),RDDATRIL D, RDCODCOM C(15), RDCODATT C(15), RDRILAUT C(1), RDTEMPOR N(12,3),; 
 RDDIPEND C(5), RDDATINI D, RDORAINI C(8), RDDATFIN D, RDORAFIN C(8), CPROWNUM N(4), SERVIZIO C(41), LISTINO C(5))
      if this.pAzione = "IMM_MOV1" 
        * --- Da detail (GSPC_MRD) - carico le righe del transitorio
        TrsCur=this.oparentobject.ctrsname
        select (TrsCur) 
 go top 
 scan for not(Empty(t_CPROWORD)) and not(Empty(t_RDCODATT)) and not(Empty(t_RDTEMPOR))
        insert into ElabCur values (this.oParentObject.w_RDNUMRIL,this.oParentObject.w_RDDATRIL,this.oParentObject.w_RDCODCOM,&TrsCur..t_RDCODATT,; 
 this.oParentObject.w_RDRILAUT,&TrsCur..t_RDTEMPOR,this.oParentObject.w_RDDIPEND,&TrsCur..t_RDDATINI,&TrsCur..t_RDORAINI,; 
 &TrsCur..t_RDDATFIN,&TrsCur..t_RDORAFIN,&TrsCur..CPROWNUM,space(41),space(5))
        endscan 
 select (TrsCur) 
 go bottom
      else
        * --- Da anagrafica (GSPC_ARD) - Ho solo una riga
        insert into ElabCur values (this.oParentObject.w_RDNUMRIL,this.oParentObject.w_RDDATRIL,this.oParentObject.w_RDCODCOM,this.oParentObject.w_RDCODATT,; 
 this.oParentObject.w_RDRILAUT,this.oParentObject.w_RDTEMPOR,this.oParentObject.w_RDDIPEND,this.oParentObject.w_RDDATINI,this.oParentObject.w_RDORAINI,; 
 this.oParentObject.w_RDDATFIN,this.oParentObject.w_RDORAFIN,this.oParentObject.w_CPROWNUM,space(41),space(5))
      endif
    endif
    * --- Rendo Scrivibile ElabCur
    Cur=WRCURSOR("ElabCur") 
 select ElabCur
    if reccount() > 0
      scan
      this.w_CODDIP = ElabCur.RDDIPEND
      this.w_COMM = ElabCur.RDCODCOM
      this.w_RIGA = recno()
      * --- Verifica la Presenza del Servizio
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPSERVIZ"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_CODDIP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPSERVIZ;
          from (i_cTable) where;
              DPCODICE = this.w_CODDIP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERVIZIO = NVL(cp_ToDate(_read_.DPSERVIZ),cp_NullValue(_read_.DPSERVIZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(nvl(this.w_SERVIZIO,""))
        * --- Se non � associato alcun servizio al Dipendente legato alla rilevazione verifico se c'� sulla commessa.
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNSERVIZ"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_COMM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNSERVIZ;
            from (i_cTable) where;
                CNCODCAN = this.w_COMM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERVIZIO = NVL(cp_ToDate(_read_.CNSERVIZ),cp_NullValue(_read_.CNSERVIZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      select ElabCur 
 update ElabCur set SERVIZIO=this.w_SERVIZIO where recno()=this.w_RIGA 
 go this.w_riga
      if this.pAzione = "GEN_MOV" 
        this.w_MACODLIS = this.oParentObject.w_CMCODLIS
      endif
      if this.pAzione = "GEN_MOV" and empty(nvl(this.w_MACODLIS,"")) or this.pAzione="IMM_MOV"
        * --- Sull'anagrafica GSPC_ARD il listino non � selezionabile, lo prendo dalla commessa.
        *     Se in GSDB_KGM non seleziono alcun listino (magari perch� ho diverse commesse con diverso listino associato) allora lo leggo dalla commessa.
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODLIS"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_COMM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODLIS;
            from (i_cTable) where;
                CNCODCAN = this.w_COMM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MACODLIS = NVL(cp_ToDate(_read_.CNCODLIS),cp_NullValue(_read_.CNCODLIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.pAzione = "GEN_MOV" or this.pAzione="IMM_MOV"
        this.w_CODLIS = this.w_MACODLIS
      endif
      select ElabCur 
 update ElabCur set LISTINO=this.w_MACODLIS where recno()=this.w_RIGA 
 go this.w_riga
      EndScan
      Select * from ElabCur into cursor temp order by 3,4,14,2,7
      if used("ElabCur")
        use in ElabCur
      endif
      Select Temp 
 scan
      this.w_DATA = Temp.RDDATRIL
      this.w_COMM = Temp.RDCODCOM
      this.w_CODDIP = Temp.RDDIPEND
      * --- Leggo l'utente per pAzione=GEN_MOV
      if this.pAzione = "GEN_MOV" 
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPCODUTE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODDIP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPCODUTE;
            from (i_cTable) where;
                DPCODICE = this.w_CODDIP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UT = NVL(cp_ToDate(_read_.DPCODUTE),cp_NullValue(_read_.DPCODUTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_UTENTE = this.w_UT
      endif
      * --- Leggo l'unit� di misura delle ore dai Parametri di Default
      * --- Read from CPAR_DEF
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PDUMCOMM"+;
          " from "+i_cTable+" CPAR_DEF where ";
              +"PDCHIAVE = "+cp_ToStrODBC("TAM");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PDUMCOMM;
          from (i_cTable) where;
              PDCHIAVE = "TAM";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UMDEFA = NVL(cp_ToDate(_read_.PDUMCOMM),cp_NullValue(_read_.PDUMCOMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MACODLIS = Temp.LISTINO
      * --- Try
      local bErr_035D8410
      bErr_035D8410=bTrsErr
      this.Try_035D8410()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.oParentObject.w_RDAGGIMM = "N"
        * --- log errori
        this.w_ERRORE = .T.
        this.w_LErrore = message()
        this.w_LOggErr = ah_Msgformat("Generico")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_035D8410
      * --- End
      EndScan
    else
      ah_ErrorMsg("Non sono state selezionate dichiarazioni da processare","!","")
      if used("ElabCur")
        use in ElabCur
      endif
    endif
    if this.pAzione = "GEN_MOV" 
      * --- Eseguo il refresh dello Zoom
      this.oParentobject.notifyevent("Interroga")
    endif
  endproc
  proc Try_035BF580()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into COMM_ERR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COMM_ERR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COMM_ERR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COMM_ERR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LESTORIC ="+cp_NullLink(cp_ToStrODBC("N"),'COMM_ERR','LESTORIC');
          +i_ccchkf ;
      +" where ";
          +"LECODUTE = "+cp_ToStrODBC(i_CODUTE);
          +" and LEOPERAZ = "+cp_ToStrODBC(this.w_LOperaz);
          +" and LESTORIC = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          LESTORIC = "N";
          &i_ccchkf. ;
       where;
          LECODUTE = i_CODUTE;
          and LEOPERAZ = this.w_LOperaz;
          and LESTORIC = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_035D8410()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scrive testata
    * --- Read from LISTINI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LISTINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LSVALLIS,LSIVALIS"+;
        " from "+i_cTable+" LISTINI where ";
            +"LSCODLIS = "+cp_ToStrODBC(this.w_MACODLIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LSVALLIS,LSIVALIS;
        from (i_cTable) where;
            LSCODLIS = this.w_MACODLIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODVAL = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
      this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MACODVAL = this.w_CODVAL
    this.w_MATIPMOV = "C"
    this.w_MADATREG = this.w_DATA
    this.w_MA__ANNO = ALLTRIM(STR(YEAR(this.w_DATA)))
    this.w_MAALFREG = IIF(g_MAGUTE="S", 0, this.oParentObject.w_UTENTE)
    * --- Calcola MASERIAL e MANUMREG
    this.w_MASERIAL = cp_GetProg("MAT_MAST","SEMAT",this.w_MASERIAL,i_codazi)
    this.w_MANUMREG = cp_GetProg("MAT_MAST","NUMAT",this.w_MANUMREG,i_codazi,this.w_MA__ANNO,this.w_MAALFREG)
    this.w_OGGI = cp_todate(date())
    this.w_MACODCOM = this.w_COMM
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNCODVAL"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_MACODCOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNCODVAL;
        from (i_cTable) where;
            CNCODCAN = this.w_MACODCOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge w_CAOCOM e w_DECCOM (cambio e decimali di w_VALCOM)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALCOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOCOM = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_MACODVAL)
      this.w_MACODVAL = g_CODEUR
    endif
    * --- Legge w_MACAOVAL (cambio di w_MACODVAL)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MACODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_MACODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MACAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MACAOVAL = IIF(this.w_MACAOVAL=0,GETCAM(this.w_MACODVAL, this.w_MADATREG, 7),this.w_MACAOVAL)
    this.w_MACODATT = Temp.RDCODATT
    * --- Read from ATTIVITA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATCODFAM,AT_STATO"+;
        " from "+i_cTable+" ATTIVITA where ";
            +"ATCODATT = "+cp_ToStrODBC(this.w_MACODATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATCODFAM,AT_STATO;
        from (i_cTable) where;
            ATCODATT = this.w_MACODATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODFAM = NVL(cp_ToDate(_read_.ATCODFAM),cp_NullValue(_read_.ATCODFAM))
      this.w_PIANIF = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Insert into MAT_MAST
    i_nConn=i_TableProp[this.MAT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MASERIAL"+",MACODCOM"+",MACODATT"+",MANUMREG"+",MAALFREG"+",MADATREG"+",MA__ANNO"+",MATIPMOV"+",MACODVAL"+",MACAOVAL"+",MACODLIS"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MASERIAL),'MAT_MAST','MASERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOM),'MAT_MAST','MACODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODATT),'MAT_MAST','MACODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MANUMREG),'MAT_MAST','MANUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAALFREG),'MAT_MAST','MAALFREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MADATREG),'MAT_MAST','MADATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MA__ANNO),'MAT_MAST','MA__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPMOV),'MAT_MAST','MATIPMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODVAL),'MAT_MAST','MACODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACAOVAL),'MAT_MAST','MACAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODLIS),'MAT_MAST','MACODLIS');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MAT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MAT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'MAT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MAT_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_MASERIAL,'MACODCOM',this.w_MACODCOM,'MACODATT',this.w_MACODATT,'MANUMREG',this.w_MANUMREG,'MAALFREG',this.w_MAALFREG,'MADATREG',this.w_MADATREG,'MA__ANNO',this.w_MA__ANNO,'MATIPMOV',this.w_MATIPMOV,'MACODVAL',this.w_MACODVAL,'MACAOVAL',this.w_MACAOVAL,'MACODLIS',this.w_MACODLIS,'UTCC',i_CODUTE)
      insert into (i_cTable) (MASERIAL,MACODCOM,MACODATT,MANUMREG,MAALFREG,MADATREG,MA__ANNO,MATIPMOV,MACODVAL,MACAOVAL,MACODLIS,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_MASERIAL;
           ,this.w_MACODCOM;
           ,this.w_MACODATT;
           ,this.w_MANUMREG;
           ,this.w_MAALFREG;
           ,this.w_MADATREG;
           ,this.w_MA__ANNO;
           ,this.w_MATIPMOV;
           ,this.w_MACODVAL;
           ,this.w_MACAOVAL;
           ,this.w_MACODLIS;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pAzione = "GEN_MOV" and this.w_ESCO=.F.
      ah_Msg("Creato movimento di commessa n. %1/%2",.T.,,2, alltrim(str(this.w_MANUMREG)), alltrim(str(this.w_MAALFREG)) )
    else
      if this.w_ESCO=.F.
        this.w_MESSAGGIO = "Creato movimento di commessa n. %1/%2 del %3"
      endif
    endif
    if this.pAzione = "IMM_MOV" and this.w_ESCO=.F.
      ah_ErrorMsg(this.w_MESSAGGIO,"","", alltrim(str(this.w_MANUMREG)), alltrim(str(this.w_MAALFREG)), dtoc(this.w_MADATREG) )
    endif
    if this.w_ESCO=.T.
      * --- Elimino la testata del movimento (se w_ESCO � .T. il dettaglio � vuoto)
      * --- Delete from MAT_MAST
      i_nConn=i_TableProp[this.MAT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MASERIAL = "+cp_ToStrODBC(this.w_MASERIAL);
               )
      else
        delete from (i_cTable) where;
              MASERIAL = this.w_MASERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Errore gestito da log (pag.5)
      this.w_ESCO = .F.
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzo le varibili identificative della riga
    this.w_ROWNUM = 0
    this.w_CPROWORD = 0
    select Temp
    scan
    if Temp.RDCODCOM=this.w_MACODCOM and Temp.RDCODATT=this.w_MACODATT and Temp.LISTINO=this.w_MACODLIS and Temp.RDDATRIL=this.w_MADATREG and Temp.RDDIPEND=this.w_CODDIP
      this.w_ESCO = .F.
      * --- Faccio un solo Movimento se Commessa,Attivit�,Listino e Data Registrazione non cambiano, in caso alternativo torno a pag.2 e 
      *     creo un nuovo Movimento di Commessa.
      this.w_RDNUMRIL = Temp.RDNUMRIL
      this.w_CPROWNUM = Temp.CPROWNUM
      * --- Verifica la Presenza del Servizio
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPSERVIZ,DPCOSORA"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_CODDIP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPSERVIZ,DPCOSORA;
          from (i_cTable) where;
              DPCODICE = this.w_CODDIP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERVIZIO = NVL(cp_ToDate(_read_.DPSERVIZ),cp_NullValue(_read_.DPSERVIZ))
        this.w_COSTOH = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(nvl(this.w_SERVIZIO,""))
        * --- Se non � associato alcun servizio al Dipendente legato alla rilevazione verifico se c'� sulla commessa.
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNSERVIZ"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_COMM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNSERVIZ;
            from (i_cTable) where;
                CNCODCAN = this.w_COMM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERVIZIO = NVL(cp_ToDate(_read_.CNSERVIZ),cp_NullValue(_read_.CNSERVIZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Scrive dettaglio
      if empty(nvl(this.w_SERVIZIO,""))
        this.w_ERRORE = .T.
        this.w_LErrore = ah_Msgformat("Non � indicato il servizio n� sull'anagrafica dipendenti n� sulla commessa")
        this.w_LOggErr = this.w_RDNUMRIL
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ESCO = .T.
      else
        this.w_ROWNUM = this.w_ROWNUM+1
        this.w_CPROWORD = this.w_ROWNUM*10
        this.w_MACODKEY = this.w_SERVIZIO
        * --- legge w_MACODART e w_MACODVAR da KEY_ARTI
        msg=" "
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART,CACODVAR,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_MACODKEY);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART,CACODVAR,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO;
            from (i_cTable) where;
                CACODICE = this.w_MACODKEY;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MACODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_MACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
          this.w_MADESCRI = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
          this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
          this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
          this.w_DTOBS1 = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
          this.w_TIPCOD = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN,ARFLUSEP,ARPREZUM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_MACODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN,ARFLUSEP,ARPREZUM;
            from (i_cTable) where;
                ARCODART = this.w_MACODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
          this.w_VOCCOST = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
          this.w_UNIMISLI = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ,UMMODUM2"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_UNMIS1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ,UMMODUM2;
            from (i_cTable) where;
                UMCODICE = this.w_UNMIS1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(nvl(this.w_MACODLIS,""))
          * --- Caso Particolare: il listino � vuoto, quindi il controllo passa sulla riga (dove si trova 
          *     il costo orario del dipendente)
        else
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_MAUNIMIS = this.w_UMDEFA
            this.w_MAQTAMOV = IIF(this.w_TIPCOD="F", 1, RDTEMPOR)
            this.w_UMCAL = this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_MAUNIMIS+this.w_OPERAT+this.w_OPERA3
            this.w_QTAUM1 = CALQTA(this.w_MAQTAMOV,this.w_MAUNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg, this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
            if not empty(msg)
              this.w_ERRORE = .T.
              this.w_LErrore = ah_Msgformat(msg)
              this.w_LOggErr = this.w_RDNUMRIL
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_CODART = this.w_MACODART
            this.w_DATREG = this.w_MADATREG
            this.w_MAQTA1UM = this.w_QTAUM1
            * --- Select from GSMA_QLL
            do vq_exec with 'GSMA_QLL',this,'_Curs_GSMA_QLL','',.f.,.t.
            if used('_Curs_GSMA_QLL')
              select _Curs_GSMA_QLL
              locate for 1=1
              do while not(eof())
              this.w_DATALIS1 = _Curs_GSMA_QLL.LIDATATT
              this.w_DATALIS2 = _Curs_GSMA_QLL.LIDATDIS
              exit
                select _Curs_GSMA_QLL
                continue
              enddo
              use
            endif
          else
            if EMPTY(NVL(this.w_MACODVAR,""))
              * --- Read from LIS_TINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LIDATATT,LIDATDIS,LIUNIMIS"+;
                  " from "+i_cTable+" LIS_TINI where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_MACODART);
                      +" and LICODLIS = "+cp_ToStrODBC(this.w_MACODLIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LIDATATT,LIDATDIS,LIUNIMIS;
                  from (i_cTable) where;
                      LICODART = this.w_MACODART;
                      and LICODLIS = this.w_MACODLIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DATALIS1 = NVL(cp_ToDate(_read_.LIDATATT),cp_NullValue(_read_.LIDATATT))
                this.w_DATALIS2 = NVL(cp_ToDate(_read_.LIDATDIS),cp_NullValue(_read_.LIDATDIS))
                this.w_UNIMISLI = NVL(cp_ToDate(_read_.LIUNIMIS),cp_NullValue(_read_.LIUNIMIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Read from LIS_TINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LIDATATT,LIDATDIS,LIUNIMIS"+;
                  " from "+i_cTable+" LIS_TINI where ";
                      +"LICODART = "+cp_ToStrODBC(this.w_MACODART);
                      +" and LICODVAR = "+cp_ToStrODBC(this.w_MACODVAR);
                      +" and LICODLIS = "+cp_ToStrODBC(this.w_MACODLIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LIDATATT,LIDATDIS,LIUNIMIS;
                  from (i_cTable) where;
                      LICODART = this.w_MACODART;
                      and LICODVAR = this.w_MACODVAR;
                      and LICODLIS = this.w_MACODLIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DATALIS1 = NVL(cp_ToDate(_read_.LIDATATT),cp_NullValue(_read_.LIDATATT))
                this.w_DATALIS2 = NVL(cp_ToDate(_read_.LIDATDIS),cp_NullValue(_read_.LIDATDIS))
                this.w_UNIMISLI = NVL(cp_ToDate(_read_.LIUNIMIS),cp_NullValue(_read_.LIUNIMIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          if this.w_DATALIS1>this.w_MADATREG or this.w_DATALIS2<this.w_MADATREG
            this.w_ERRORE = .T.
            this.w_LErrore = ah_Msgformat("Listino non valido alla data %1", dtoc(cp_todate(this.w_MADATREG)) )
            this.w_LOggErr = this.w_RDNUMRIL
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_ESCO = .T.
          endif
          * --- --07 luglio 2002
          if this.w_CODVAL <> g_CODEUR
            this.w_ERRORE = .T.
            this.w_LErrore = ah_Msgformat("La valuta di listino deve essere Euro")
            this.w_LOggErr = this.w_RDNUMRIL
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MACODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN;
              from (i_cTable) where;
                  ARCODART = this.w_MACODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
            this.w_VOCCOST = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARCODCAA"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_MACODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARCODCAA;
              from (i_cTable) where;
                  ARCODART = this.w_MACODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
            this.w_CATANAL = NVL(cp_ToDate(_read_.ARCODCAA),cp_NullValue(_read_.ARCODCAA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- L'unit� di misura di partenza (alla quale fa riferimento la q.t� dichiarata sull'anagrafica Dichiarazioni GSPC_ARD) � SEMPRE ore, le conversioni 
        *     di U.M. partono sempre dal presupposto che sui parametri di default sia indicata l'unita di misura corrispondente alle ore, in caso ci� non
        *     accada la conversione di U.M. verr� eseguita correttamente usando i fattori di conversioni forniti sul servizio, ma risulter� comunque sbagliata 
        *     perch� su GSDB_ARD si inseriscono sempre e solo q.t� in ore.
        if this.w_ESCO=.F.
          this.w_UNMIS1 = left(nvl(this.w_UNMIS1,"")+space(3),3)
          this.w_UNMIS2 = left(nvl(this.w_UNMIS2,"")+space(3),3)
          this.w_UNMIS3 = left(nvl(this.w_UNMIS3,"")+space(3),3)
          this.w_UMDEFA = left(nvl(this.w_UMDEFA,"")+space(3),3)
          this.w_MAUNIMIS = this.w_UMDEFA
          if empty(nvl(this.w_MACODLIS,""))
            this.w_LIPREZZO = this.w_COSTOH
            this.w_UMLIPREZZO = this.w_UNMIS1
            if this.w_LIPREZZO=0
              this.w_ERRORE = .T.
              this.w_LErrore = ah_Msgformat("Il costo orario indicato sull'anagrafica dipendente � 0")
              this.w_LOggErr = this.w_RDNUMRIL
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_LIPREZZO=0
              this.w_ERRORE = .T.
              this.w_LErrore = ah_Msgformat("Il prezzo indicato sul listino � 0")
              this.w_LOggErr = this.w_RDNUMRIL
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Se l'Unit� di Misura del Listino/Contratto 
          *     coincide con Unit� di Misura movimentata non effettuo conversioni
          if this.w_UMLIPREZZO=this.w_MAUNIMIS 
            this.w_MAPREZZO = IIF(this.w_IVALIS="L",CALNET( this.w_LIPREZZO , this.w_PERIVA , this.w_DECTOT , "" , this.w_PERIVA), this.w_LIPREZZO)
          else
            do case
              case this.w_UMLIPREZZO=this.w_UNMIS1 and (this.w_MAUNIMIS=this.w_UNMIS2 or this.w_MAUNIMIS=this.w_UNMIS3)
                * --- U.M. Listini/Contratti = UM1 e U.M. movimentata = UM2
                *     (Converto in UM2)
                this.w_UMCAL = this.w_UNMIS1+this.w_UNMIS2+this.w_UNMIS3+this.w_MAUNIMIS+this.w_OPERAT+this.w_OPERA3
                if g_APPLICATION = "ADHOC REVOLUTION"
                  if empty(this.w_CLUNIMIS)
                    this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",this.w_MOLTIP,this.w_MOLTI3,this.w_PERIVA)
                  else
                    this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",1,1,this.w_PERIVA)
                  endif
                else
                  this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",this.w_MOLTIP,this.w_MOLTI3,this.w_PERIVA, this.w_DECTOT)
                endif
              case this.w_UMLIPREZZO=this.w_UNMIS2 and !Empty(this.w_UMLIPREZZO) and this.w_MAUNIMIS=this.w_UNMIS1
                * --- U.M. Listini/Contratti = UM2 e U.M. movimentata = UM1
                *     (Converto in UM1)
                this.w_UMCAL = this.w_UNMIS2+this.w_UNMIS1+this.w_UNMIS3+this.w_MAUNIMIS+IIF(this.w_OPERAT="/","*","/")+this.w_OPERA3
                if g_APPLICATION = "ADHOC REVOLUTION"
                  if empty(this.w_CLUNIMIS)
                    this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",this.w_MOLTIP,this.w_MOLTI3,this.w_PERIVA)
                  else
                    this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",1,1,this.w_PERIVA)
                  endif
                else
                  this.w_MAPREZZO = CALMMLIS(this.w_LIPREZZO, this.w_UMCAL+this.w_IVALIS+"P",this.w_MOLTIP,this.w_MOLTI3,this.w_PERIVA, this.w_DECTOT)
                endif
              otherwise
                * --- Listino/ Contratto  Non valido
                * --- w_LIPREZZO - w_MAPREZZO
                this.w_LIPREZZO = 0
                this.w_MAPREZZO = 0
            endcase
          endif
          this.w_MAQTAMOV = IIF(this.w_TIPCOD="F", 1, TEMP.RDTEMPOR)
          if g_APPLICATION = "ADHOC REVOLUTION"
            * --- Read from VOC_COST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOC_COST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VCTIPCOS"+;
                " from "+i_cTable+" VOC_COST where ";
                    +"VCCODICE = "+cp_ToStrODBC(this.w_VOCCOST);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VCTIPCOS;
                from (i_cTable) where;
                    VCCODICE = this.w_VOCCOST;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Read from CAANARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAANARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAANARTI_idx,2],.t.,this.CAANARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CATIPCOS"+;
                " from "+i_cTable+" CAANARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CATANAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CATIPCOS;
                from (i_cTable) where;
                    CACODICE = this.w_CATANAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCOS = NVL(cp_ToDate(_read_.CATIPCOS),cp_NullValue(_read_.CATIPCOS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Read from TIPCOSTO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIPCOSTO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPCOSTO_idx,2],.t.,this.TIPCOSTO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCDESBRE"+;
              " from "+i_cTable+" TIPCOSTO where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.w_CODCOS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCDESBRE;
              from (i_cTable) where;
                  TCCODICE = this.w_CODCOS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCOSDE = NVL(cp_ToDate(_read_.TCDESBRE),cp_NullValue(_read_.TCDESBRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MAOPPREV = IIF(this.w_MATIPMOV="P" and this.w_MAPREZZO<>0 ,"+"," ")
          this.w_MACOCOMS = this.w_MACODCOM
          this.w_MACOATTS = this.w_MACODATT
          this.w_MATIPATT = "A"
          this.w_MAOPCONS = IIF(this.w_MATIPMOV="C" and this.w_MAPREZZO<>0,"+"," ")
          this.w_MANOTE = ah_Msgformat("Ore lavoro inserite da utente %1", alltrim(str(this.w_UT)) )
          this.w_MACODCOS = this.w_CODCOS
          this.w_MAQTA1UM = CALQTA(this.w_MAQTAMOV,this.w_MAUNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg, this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
          if not empty(msg)
            this.w_ERRORE = .T.
            this.w_LErrore = ah_Msgformat(msg)
            this.w_LOggErr = this.w_RDNUMRIL
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_MADATANN = cp_CharToDate("  -  -  ")
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_MAVALRIG = IIF(NOT EMPTY(NVL(this.w_MADATANN,cp_CharToDate("  -  -  "))),0,VAL2MON(this.w_MAPREZZO,this.w_MACAOVAL,this.w_CAOCOM,this.w_MADATREG,this.w_VALCOM)*this.w_MAQTAMOV)
          else
            this.w_MAVALRIG = IIF(NOT EMPTY(NVL(this.w_MADATANN,cp_CharToDate("  -  -  "))),0,VAL2MON(this.w_MAPREZZO,this.w_MACAOVAL,this.w_CAOCOM,this.w_MADATREG,this.w_VALCOM,this.w_DECCOM)*this.w_MAQTAMOV)
          endif
          this.w_TOTRIGA = this.w_MAPREZZO*this.w_MAQTAMOV
          this.w_MACODFAM = this.w_CODFAM
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_MACODKEY = LEFT(this.w_MACODKEY + SPACE(20), 20)
            * --- Insert into MAT_DETT
            i_nConn=i_TableProp[this.MAT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MASERIAL"+",CPROWNUM"+",CPROWORD"+",MACODKEY"+",MACODART"+",MAUNIMIS"+",MAQTAMOV"+",MAPREZZO"+",MADATANN"+",MADESCRI"+",MA__NOTE"+",MACODCOS"+",MAOPPREV"+",MAOPCONS"+",MAQTA1UM"+",MAVALRIG"+",MACOATTS"+",MATIPATT"+",MACOCOMS"+",MACODFAM"+",MADICRIF"+",MAROWRIF"+",MARIFKIT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MASERIAL),'MAT_DETT','MASERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MAT_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MAT_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODKEY),'MAT_DETT','MACODKEY');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODART),'MAT_DETT','MACODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAUNIMIS),'MAT_DETT','MAUNIMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTAMOV),'MAT_DETT','MAQTAMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAPREZZO),'MAT_DETT','MAPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MADATANN),'MAT_DETT','MADATANN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'MAT_DETT','MADESCRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MANOTE),'MAT_DETT','MA__NOTE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MAT_DETT','MACODCOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPPREV),'MAT_DETT','MAOPPREV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPCONS),'MAT_DETT','MAOPCONS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTA1UM),'MAT_DETT','MAQTA1UM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAVALRIG),'MAT_DETT','MAVALRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MAT_DETT','MACOATTS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MAT_DETT','MATIPATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MAT_DETT','MACOCOMS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODFAM),'MAT_DETT','MACODFAM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RDNUMRIL),'MAT_DETT','MADICRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MAT_DETT','MAROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(0),'MAT_DETT','MARIFKIT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_MASERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_CPROWORD,'MACODKEY',this.w_MACODKEY,'MACODART',this.w_MACODART,'MAUNIMIS',this.w_MAUNIMIS,'MAQTAMOV',this.w_MAQTAMOV,'MAPREZZO',this.w_MAPREZZO,'MADATANN',this.w_MADATANN,'MADESCRI',this.w_MADESCRI,'MA__NOTE',this.w_MANOTE,'MACODCOS',this.w_MACODCOS)
              insert into (i_cTable) (MASERIAL,CPROWNUM,CPROWORD,MACODKEY,MACODART,MAUNIMIS,MAQTAMOV,MAPREZZO,MADATANN,MADESCRI,MA__NOTE,MACODCOS,MAOPPREV,MAOPCONS,MAQTA1UM,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MACODFAM,MADICRIF,MAROWRIF,MARIFKIT &i_ccchkf. );
                 values (;
                   this.w_MASERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_CPROWORD;
                   ,this.w_MACODKEY;
                   ,this.w_MACODART;
                   ,this.w_MAUNIMIS;
                   ,this.w_MAQTAMOV;
                   ,this.w_MAPREZZO;
                   ,this.w_MADATANN;
                   ,this.w_MADESCRI;
                   ,this.w_MANOTE;
                   ,this.w_MACODCOS;
                   ,this.w_MAOPPREV;
                   ,this.w_MAOPCONS;
                   ,this.w_MAQTA1UM;
                   ,this.w_MAVALRIG;
                   ,this.w_MACOATTS;
                   ,this.w_MATIPATT;
                   ,this.w_MACOCOMS;
                   ,this.w_MACODFAM;
                   ,this.w_RDNUMRIL;
                   ,this.w_CPROWNUM;
                   ,0;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into MAT_DETT
            i_nConn=i_TableProp[this.MAT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAT_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MASERIAL"+",CPROWNUM"+",CPROWORD"+",MACODKEY"+",MACODART"+",MACODVAR"+",MAUNIMIS"+",MAQTAMOV"+",MAPREZZO"+",MADATANN"+",MADESCRI"+",MA__NOTE"+",MACODCOS"+",MAOPPREV"+",MAOPCONS"+",MAQTA1UM"+",MAVALRIG"+",MACOATTS"+",MATIPATT"+",MACOCOMS"+",MACODFAM"+",MADICRIF"+",MAROWRIF"+",MARIFKIT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_MASERIAL),'MAT_DETT','MASERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MAT_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MAT_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODKEY),'MAT_DETT','MACODKEY');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODART),'MAT_DETT','MACODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODVAR),'MAT_DETT','MACODVAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAUNIMIS),'MAT_DETT','MAUNIMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTAMOV),'MAT_DETT','MAQTAMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAPREZZO),'MAT_DETT','MAPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MADATANN),'MAT_DETT','MADATANN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MADESCRI),'MAT_DETT','MADESCRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MANOTE),'MAT_DETT','MA__NOTE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MAT_DETT','MACODCOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPPREV),'MAT_DETT','MAOPPREV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAOPCONS),'MAT_DETT','MAOPCONS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAQTA1UM),'MAT_DETT','MAQTA1UM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MAVALRIG),'MAT_DETT','MAVALRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MAT_DETT','MACOATTS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MAT_DETT','MATIPATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MAT_DETT','MACOCOMS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACODFAM),'MAT_DETT','MACODFAM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RDNUMRIL),'MAT_DETT','MADICRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MAT_DETT','MAROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(0),'MAT_DETT','MARIFKIT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_MASERIAL,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_CPROWORD,'MACODKEY',this.w_MACODKEY,'MACODART',this.w_MACODART,'MACODVAR',this.w_MACODVAR,'MAUNIMIS',this.w_MAUNIMIS,'MAQTAMOV',this.w_MAQTAMOV,'MAPREZZO',this.w_MAPREZZO,'MADATANN',this.w_MADATANN,'MADESCRI',this.w_MADESCRI,'MA__NOTE',this.w_MANOTE)
              insert into (i_cTable) (MASERIAL,CPROWNUM,CPROWORD,MACODKEY,MACODART,MACODVAR,MAUNIMIS,MAQTAMOV,MAPREZZO,MADATANN,MADESCRI,MA__NOTE,MACODCOS,MAOPPREV,MAOPCONS,MAQTA1UM,MAVALRIG,MACOATTS,MATIPATT,MACOCOMS,MACODFAM,MADICRIF,MAROWRIF,MARIFKIT &i_ccchkf. );
                 values (;
                   this.w_MASERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_CPROWORD;
                   ,this.w_MACODKEY;
                   ,this.w_MACODART;
                   ,this.w_MACODVAR;
                   ,this.w_MAUNIMIS;
                   ,this.w_MAQTAMOV;
                   ,this.w_MAPREZZO;
                   ,this.w_MADATANN;
                   ,this.w_MADESCRI;
                   ,this.w_MANOTE;
                   ,this.w_MACODCOS;
                   ,this.w_MAOPPREV;
                   ,this.w_MAOPCONS;
                   ,this.w_MAQTA1UM;
                   ,this.w_MAVALRIG;
                   ,this.w_MACOATTS;
                   ,this.w_MATIPATT;
                   ,this.w_MACOCOMS;
                   ,this.w_MACODFAM;
                   ,this.w_RDNUMRIL;
                   ,this.w_CPROWNUM;
                   ,0;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Valorizza il flag Processato sulla Dichiarazione
          * --- Write into DICH_MDO
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DICH_MDO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DICH_MDO_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DICH_MDO_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RDPROCES ="+cp_NullLink(cp_ToStrODBC("S"),'DICH_MDO','RDPROCES');
                +i_ccchkf ;
            +" where ";
                +"RDNUMRIL = "+cp_ToStrODBC(this.w_RDNUMRIL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                RDPROCES = "S";
                &i_ccchkf. ;
             where;
                RDNUMRIL = this.w_RDNUMRIL;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- AGGIORNA SALDI ATTIVITA
          * --- Try
          local bErr_0344A238
          bErr_0344A238=bTrsErr
          this.Try_0344A238()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0344A238
          * --- End
          * --- Write into MA_COSTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSPREVEN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSPREVEN');
            +",CSCONSUN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSCONSUN');
                +i_ccchkf ;
            +" where ";
                +"CSCODCOM = "+cp_ToStrODBC(this.w_MACOCOMS);
                +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MATIPATT);
                +" and CSCODMAT = "+cp_ToStrODBC(this.w_MACOATTS);
                +" and CSCODCOS = "+cp_ToStrODBC(this.w_MACODCOS);
                   )
          else
            update (i_cTable) set;
                CSPREVEN = &i_cOp1.;
                ,CSCONSUN = &i_cOp2.;
                &i_ccchkf. ;
             where;
                CSCODCOM = this.w_MACOCOMS;
                and CSTIPSTR = this.w_MATIPATT;
                and CSCODMAT = this.w_MACOATTS;
                and CSCODCOS = this.w_MACODCOS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      select Temp
      this.w_RIGA = recno()
    endif
    endscan
    select Temp 
 go this.w_RIGA
  endproc
  proc Try_0344A238()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MA_COSTI
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODCOM"+",CSTIPSTR"+",CSCODMAT"+",CSCODCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MACOCOMS),'MA_COSTI','CSCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATIPATT),'MA_COSTI','CSTIPSTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACOATTS),'MA_COSTI','CSCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MACODCOS),'MA_COSTI','CSCODCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MACOCOMS,'CSTIPSTR',this.w_MATIPATT,'CSCODMAT',this.w_MACOATTS,'CSCODCOS',this.w_MACODCOS)
      insert into (i_cTable) (CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS &i_ccchkf. );
         values (;
           this.w_MACOCOMS;
           ,this.w_MATIPATT;
           ,this.w_MACOATTS;
           ,this.w_MACODCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LIPREZZO = 0
    * --- Calcola Prezzo e Sconti
    * --- Azzero l'Array che verr� riempito dalla Funzione
    if g_APPLICATION = "ADHOC REVOLUTION"
      DECLARE ARRCALC (16)
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA( this.w_MAQTA1UM,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
      this.w_QTAUM2 = CALQTA( this.w_MAQTA1UM,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MAUNIMIS 
 pArrUm [3] = this.w_MAQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MAQTA1UM 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.w_MACODLIS , this.w_MACODART , " " , this.w_MAQTA1UM , this.w_MACODVAL , this.w_MACAOVAL , this.w_MADATREG , " " , " ", this.w_MACODVAL , " ", " ", " ", this.w_SCOLIS, 0,"M", @ARRCALC, " ", " ","N",@pArrUm )
      this.w_CLUNIMIS = ARRCALC(16)
      this.w_LIPREZZO = ARRCALC(5)
      this.w_UMLIPREZZO = this.w_UNIMISLI
      if this.w_MAUNIMIS<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
        this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_MAQTA1UM, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
        this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_MAQTAMOV, this.w_QTALIS, "N", 0, this.w_DECTOT), this.w_DECTOT)
      endif
    else
      this.w_CALPRZ = CalPrzli( Space(15) , "N" , this.w_MACODLIS , this.w_MACODART , this.w_MACODVAR ,Space(5) , this.w_MAQTA1UM , Space(5) , this.w_MACODVAL , this.w_MACAOVAL , this.w_MADATREG , Space(5) , Space(5),@ARRCALC,Space(3),Space(1) )
      this.w_LIPREZZO = ARRCALC(5)
      this.w_UMLIPREZZO = ARRCALC(9)
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Determina progressivo log
    if empty(this.w_LSerial)
      i_Conn=i_TableProp[this.COMM_ERR_IDX, 3] 
 cp_NextTableProg(this, i_Conn, "PRERC", "i_codazi,w_LSerial")
    endif
    * --- Scrive LOG
    if not empty(this.w_LSerial)
      * --- Scrive LOG
      this.w_LOra = time()
      this.w_LMessage = "Message()= "+message()
      * --- Try
      local bErr_034B0D00
      bErr_034B0D00=bTrsErr
      this.Try_034B0D00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_034B0D00
      * --- End
    endif
  endproc
  proc Try_034B0D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COMM_ERR
    i_nConn=i_TableProp[this.COMM_ERR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COMM_ERR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COMM_ERR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LESERIAL"+",LEROWNUM"+",LEOPERAZ"+",LEDATOPE"+",LEORAOPE"+",LECODUTE"+",LECODICE"+",LEERRORE"+",LEMESSAG"+",LESTORIC"+",LESTAMPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSerial),'COMM_ERR','LESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNumErr),'COMM_ERR','LEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOperaz),'COMM_ERR','LEOPERAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'COMM_ERR','LEDATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOra),'COMM_ERR','LEORAOPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'COMM_ERR','LECODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOggErr),'COMM_ERR','LECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LErrore),'COMM_ERR','LEERRORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMessage),'COMM_ERR','LEMESSAG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'COMM_ERR','LESTORIC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'COMM_ERR','LESTAMPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LESERIAL',this.w_LSerial,'LEROWNUM',this.w_LNumErr,'LEOPERAZ',this.w_LOperaz,'LEDATOPE',i_DATSYS,'LEORAOPE',this.w_LOra,'LECODUTE',i_CODUTE,'LECODICE',this.w_LOggErr,'LEERRORE',this.w_LErrore,'LEMESSAG',this.w_LMessage,'LESTORIC',"S",'LESTAMPA',"N")
      insert into (i_cTable) (LESERIAL,LEROWNUM,LEOPERAZ,LEDATOPE,LEORAOPE,LECODUTE,LECODICE,LEERRORE,LEMESSAG,LESTORIC,LESTAMPA &i_ccchkf. );
         values (;
           this.w_LSerial;
           ,this.w_LNumErr;
           ,this.w_LOperaz;
           ,i_DATSYS;
           ,this.w_LOra;
           ,i_CODUTE;
           ,this.w_LOggErr;
           ,this.w_LErrore;
           ,this.w_LMessage;
           ,"S";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,19)]
    this.cWorkTables[1]='DICH_MDO'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='MAT_MAST'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='LIS_TINI'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='VOCIIVA'
    this.cWorkTables[12]='CAANARTI'
    this.cWorkTables[13]='VOC_COST'
    this.cWorkTables[14]='TIPCOSTO'
    this.cWorkTables[15]='MAT_DETT'
    this.cWorkTables[16]='MA_COSTI'
    this.cWorkTables[17]='CPAR_DEF'
    this.cWorkTables[18]='COMM_ERR'
    this.cWorkTables[19]='UNIMIS'
    return(this.OpenAllTables(19))

  proc CloseCursors()
    if used('_Curs_DICH_MDO')
      use in _Curs_DICH_MDO
    endif
    if used('_Curs_DICH_MDO')
      use in _Curs_DICH_MDO
    endif
    if used('_Curs_MAT_DETT')
      use in _Curs_MAT_DETT
    endif
    if used('_Curs_GSMA_QLL')
      use in _Curs_GSMA_QLL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
