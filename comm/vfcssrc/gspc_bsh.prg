* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bsh                                                        *
*              Stampa scheda tecnica                                           *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-02                                                      *
* Last revis.: 2005-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bsh",oParentObject)
return(i_retval)

define class tgspc_bsh as StdBatch
  * --- Local variables
  w_OQRY = space(50)
  w_CODCOM = space(15)
  w_DATAINI = ctod("  /  /  ")
  w_DECTOT1 = 0
  w_OREP = space(50)
  w_CODATT1 = space(15)
  w_DATAFIN = ctod("  /  /  ")
  w_ODES = space(50)
  w_CODATT2 = space(15)
  w_CODICE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa il Movimento di commessa dalla gestione
    * --- Utilizza come report l'output utente associato a GSPC_SSS (stampa schede tecniche) di Default
    if this.oParentObject.w_MATIPMOV="P"
      * --- Leggo Nell'output utente la query ed il report
      LookOut(this,"1","GSPC_SSS")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_OQRY = "..\comm\exe\query\gspc_smc.vqr"
      this.w_OREP = "..\comm\exe\query\gspc_smc.frx"
    endif
    * --- Lancio la Stampa
    if Not Empty (this.w_OQRY) And Not Empty (this.w_OREP)
      * --- non utilizzo la Vx_Exec (o oggetto analogo) perch� fa casino con i ..
      this.w_DECTOT1 = this.oParentObject.w_DECTOT
      * --- Utilizzato dal Report per w_DECTOT1
      L_oParentObject=This
      Vq_Exec ( Alltrim( this.w_OQRY ) ,This, "__Tmp__")
      Cp_Chprn ( Alltrim( this.w_OREP ),"",this.oParentObject )
    else
      Ah_ErrorMsg("Report o query inesistenti. Verificare output utente (GSPC_SSS)",48)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
