* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc1btp                                                        *
*              Eventi da pianificazione con MS-PROJECT                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-27                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc1btp",oParentObject,m.pOPER)
return(i_retval)

define class tgspc1btp as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_COLENABLED = .f.
  w_nCURROW = 0
  w_nROW = 0
  w_Zoom = .NULL.
  GSPC_KTP = .NULL.
  w_cCursor = space(10)
  w_RISPOSTA = 0
  w_RECNO = 0
  w_nCol = 0
  w_cCol = 0
  w_Esci = .f.
  w_Column = space(10)
  w_OLDLOCK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.GSPC_KTP = this.oParentObject
    this.w_Zoom = this.GSPC_KTP.w_Zoom
    this.w_cCursor = this.oParentObject.w_Zoom.cCursor
    SELECT (this.w_cCursor)
    this.w_RECNO = RECNO()
    do case
      case this.pOPER="SELEZI"
        if this.oParentObject.w_SELEZI="S"
          UPDATE (this.w_cCursor) Set xchk=1, cnselezi=0, cntipope=1
        else
          UPDATE (this.w_cCursor) Set xchk=0, cnselezi=2, cntipope=3
        endif
        this.oParentObject.w_Zoom.grd.refresh()     
      case this.pOPER="SELTIP"
        if this.oParentObject.w_SELTIP="S"
          UPDATE (this.w_cCursor) Set cnselezi=1 where xchk=1
        else
          UPDATE (this.w_cCursor) Set cnselezi=0 where cnselezi=1
        endif
        this.oParentObject.w_Zoom.grd.refresh()     
      case this.pOPER="CAMBIOCOM"
        if this.oParentObject.w_PRJREL="2003"
          if this.oParentObject.w_SELEZMUL="S"
            this.oParentObject.w_CODCOM = SPACE(15)
            this.oParentObject.w_DESCOM = SPACE(30)
          else
            this.oParentObject.w_CODCOM = g_CODCOM
          endif
          * --- Abilito/Disabilito pagina 2
          this.GSPC_KTP.oPgFrm.Pages(2).Enabled = Empty(this.oParentObject.w_CODCOM) And this.oParentObject.w_SELEZMUL="S"
          if Empty(this.oParentObject.w_CODCOM) And this.oParentObject.w_SELEZMUL="S"
            * --- Se � vuota la commessa attivo pagina 2
            this.GSPC_KTP.NotifyEvent("Aggiorna")     
            this.GSPC_KTP.oPgFrm.ActivePage = 2
            this.GSPC_KTP.NotifyEvent("ActivatePage 2")     
            this.GSPC_KTP.mCalc(.t.)     
            if !Empty(g_CODCOM)
              * --- Deseleziono tutte le commesse
              this.oParentObject.w_SELEZI = "D"
              this.GSPC_KTP.NotifyEvent("w_SELEZI Changed")     
              * --- Seleziono solo la commessa indicata nei parametri di default
              SELECT (this.w_cCursor) 
 LOCATE FOR CNCODCAN = g_CODCOM
              if Found()
                SELECT (this.w_cCursor)
                Replace xchk with 1, cnselezi with 0, cntipope with 1
              endif
            else
              * --- Seleziono tutte le commesse
              this.oParentObject.w_SELEZI = "S"
              this.GSPC_KTP.NotifyEvent("w_SELEZI Changed")     
            endif
            SELECT (this.w_cCursor) 
 GO TOP
            this.w_nCURROW = RECNO()
            this.w_nROW = this.w_nCURROW
            this.oParentObject.w_Zoom.grd.refresh()     
          endif
        endif
        this.GSPC_KTP.mCalc(.t.)     
      case this.pOPER="AggOpe"
        this.w_RISPOSTA = 0
        * --- Trascodifico i valori della combo in risposte
        do case
          case this.oParentObject.w_OPERAZ="S"
            this.w_RISPOSTA = 1
          case this.oParentObject.w_OPERAZ="T"
            this.w_RISPOSTA = 2
          case this.oParentObject.w_OPERAZ="N"
            this.w_RISPOSTA = 3
        endcase
        if this.w_RISPOSTA > 0 
          UPDATE (this.w_cCursor) Set cntipope=this.w_RISPOSTA where cnselezi=1 and xchk=1
          UPDATE (this.w_cCursor) Set cnselezi=0 where xchk=1
          this.oParentObject.w_Zoom.grd.refresh()     
          this.oParentObject.w_SELTIP = "D"
        endif
      case this.pOPER="ZRC"
        replace cnselezi with 0, cntipope with 1
      case this.pOPER="ZRU"
        replace cnselezi with 2, cntipope with 3
      case this.pOPER="TRC"
      case this.pOPER="TRC"
    endcase
    SELECT (this.w_cCursor)
     LOCAL l_olderr, berr 
 l_olderr=ON("error") 
 berr=.f. 
 ON ERROR berr=.t.
    GO this.w_RECNO
     on error &l_olderr
    this.oParentObject.w_Zoom.grd.refresh()     
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OLDLOCK = this.GSPC_KTP.LockScreen
    if Not this.w_OLDLOCK
      this.GSPC_KTP.LockScreen = .T.
    endif
    this.w_Esci = .f.
    this.w_nCol = this.GSPC_KTP.w_zoom.grd.columncount
    this.w_cCol = 1
    Local n
    n = "1"
    do while this.w_cCol <= this.w_nCol And !this.w_Esci
      n = alltrim(str(this.w_cCol))
      this.w_Column = this.GSPC_KTP.w_zoom.grd.Column&n..ControlSource
      if Upper(Alltrim(this.w_Column)) = "CNTIPOPE"
        this.w_Esci = .t.
      else
        this.w_cCol = this.w_cCol + 1
      endif
    enddo
    * --- Se vale True all'uscita della while significa che ho trovato il campo nello zoom
    if this.w_Esci
      this.oParentObject.w_CHECK = this.oParentObject.w_Zoom.GetVar("XCHK")
      this.GSPC_KTP.w_zoom.grd.Column&n..enabled = this.oParentObject.w_CHECK
      this.oParentObject.w_CBOSEL = this.oParentObject.w_CHECK=1
    endif
    if Not this.w_OLDLOCK
      this.GSPC_KTP.LockScreen = .f.
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
