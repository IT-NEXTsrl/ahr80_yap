* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kcx                                                        *
*              Copia progetto                                                  *
*                                                                              *
*      Author: Paolo Saitti                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_76]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-28                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kcx",oParentObject))

* --- Class definition
define class tgspc_kcx as StdForm
  Top    = 17
  Left   = 57

  * --- Standard Properties
  Width  = 460
  Height = 218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=99244137
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_kcx"
  cComment = "Copia progetto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(3)
  w_UNIQUE = space(1)
  w_CODCOMS = space(15)
  w_DESCOMS = space(30)
  w_CODCOMD = space(15)
  w_DESCOMD = space(30)
  w_CHKAMM = space(1)
  w_CHKGEST = space(1)
  w_CHKTECN = space(1)
  w_CHKATT = space(1)
  w_DATINIS = ctod('  /  /  ')
  w_DATINID = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_kcxPag1","gspc_kcx",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOMS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CPAR_DEF'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSPC_BCX with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_kcx
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(3)
      .w_UNIQUE=space(1)
      .w_CODCOMS=space(15)
      .w_DESCOMS=space(30)
      .w_CODCOMD=space(15)
      .w_DESCOMD=space(30)
      .w_CHKAMM=space(1)
      .w_CHKGEST=space(1)
      .w_CHKTECN=space(1)
      .w_CHKATT=space(1)
      .w_DATINIS=ctod("  /  /  ")
      .w_DATINID=ctod("  /  /  ")
        .w_CODICE = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODICE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_UNIQUE))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCOMS))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODCOMD))
          .link_1_6('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CHKAMM = 'P'
        .w_CHKGEST = iif(nvl(.w_UNIQUE,' ')='S' or g_APPLICATION <> "ad hoc ENTERPRISE",' ','G')
        .w_CHKTECN = iif(nvl(.w_UNIQUE,' ')='S' or g_APPLICATION <> "ad hoc ENTERPRISE",' ','T')
        .w_CHKATT = 'A'
    endwith
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gspc_kcx
    this.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCHKAMM_1_10.visible=!this.oPgFrm.Page1.oPag.oCHKAMM_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCHKGEST_1_11.visible=!this.oPgFrm.Page1.oPag.oCHKGEST_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCHKTECN_1_12.visible=!this.oPgFrm.Page1.oPag.oCHKTECN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCHKATT_1_13.visible=!this.oPgFrm.Page1.oPag.oCHKATT_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDUNIQUE";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_CODICE);
                   +" and PDCHIAVE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_CODICE;
                       ,'PDCHIAVE',this.w_CODICE)
            select PDCHIAVE,PDUNIQUE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.PDCHIAVE,space(3))
      this.w_UNIQUE = NVL(_Link_.PDUNIQUE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(3)
      endif
      this.w_UNIQUE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNIQUE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNIQUE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNIQUE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDUNIQUE";
                   +" from "+i_cTable+" "+i_lTable+" where PDUNIQUE="+cp_ToStrODBC(this.w_UNIQUE);
                   +" and PDCHIAVE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_CODICE;
                       ,'PDUNIQUE',this.w_UNIQUE)
            select PDCHIAVE,PDUNIQUE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNIQUE = NVL(_Link_.PDUNIQUE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNIQUE = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)+'\'+cp_ToStr(_Link_.PDUNIQUE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNIQUE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMS)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMS))
          select CNCODCAN,CNDESCAN,CNDATINI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMS)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMS) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMS_1_3'),i_cWhere,'',"",'GSPC_CS.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMS)
            select CNCODCAN,CNDESCAN,CNDATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMS = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMS = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATINIS = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMS = space(15)
      endif
      this.w_DESCOMS = space(30)
      this.w_DATINIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOMS<>.w_CODCOMD
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endif
        this.w_CODCOMS = space(15)
        this.w_DESCOMS = space(30)
        this.w_DATINIS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMD
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMD)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMD))
          select CNCODCAN,CNDESCAN,CNDATINI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMD)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMD) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMD_1_6'),i_cWhere,'',"",'GSPC_CS.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDATINI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMD)
            select CNCODCAN,CNDESCAN,CNDATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMD = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMD = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATINID = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMD = space(15)
      endif
      this.w_DESCOMD = space(30)
      this.w_DATINID = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOMD<>.w_CODCOMS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endif
        this.w_CODCOMD = space(15)
        this.w_DESCOMD = space(30)
        this.w_DATINID = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOMS_1_3.value==this.w_CODCOMS)
      this.oPgFrm.Page1.oPag.oCODCOMS_1_3.value=this.w_CODCOMS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMS_1_5.value==this.w_DESCOMS)
      this.oPgFrm.Page1.oPag.oDESCOMS_1_5.value=this.w_DESCOMS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOMD_1_6.value==this.w_CODCOMD)
      this.oPgFrm.Page1.oPag.oCODCOMD_1_6.value=this.w_CODCOMD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMD_1_8.value==this.w_DESCOMD)
      this.oPgFrm.Page1.oPag.oDESCOMD_1_8.value=this.w_DESCOMD
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKAMM_1_10.RadioValue()==this.w_CHKAMM)
      this.oPgFrm.Page1.oPag.oCHKAMM_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKGEST_1_11.RadioValue()==this.w_CHKGEST)
      this.oPgFrm.Page1.oPag.oCHKGEST_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKTECN_1_12.RadioValue()==this.w_CHKTECN)
      this.oPgFrm.Page1.oPag.oCHKTECN_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKATT_1_13.RadioValue()==this.w_CHKATT)
      this.oPgFrm.Page1.oPag.oCHKATT_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODCOMS)) or not(.w_CODCOMS<>.w_CODCOMD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOMS_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCOMS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
          case   ((empty(.w_CODCOMD)) or not(.w_CODCOMD<>.w_CODCOMS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOMD_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODCOMD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non � possibile copiare una struttura di commessa su s� stessa")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_kcxPag1 as StdContainer
  Width  = 456
  height = 218
  stdWidth  = 456
  stdheight = 218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOMS_1_3 as StdField with uid="VVJZBHNEER",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCOMS", cQueryName = "CODCOMS",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile copiare una struttura di commessa su s� stessa",;
    ToolTipText = "Codice commessa sorgente",;
    HelpContextID = 206388006,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=104, Top=57, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMS"

  func oCODCOMS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSPC_CS.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oDESCOMS_1_5 as StdField with uid="HOKXOXQELF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCOMS", cQueryName = "DESCOMS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 206446902,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=226, Top=57, InputMask=replicate('X',30)

  add object oCODCOMD_1_6 as StdField with uid="MLUMFGAWLY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCOMD", cQueryName = "CODCOMD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Non � possibile copiare una struttura di commessa su s� stessa",;
    ToolTipText = "Codice commessa sorgente",;
    HelpContextID = 206388006,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=104, Top=86, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMD"

  func oCODCOMD_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMD_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMD_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMD_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSPC_CS.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oDESCOMD_1_8 as StdField with uid="OBHSUXJDID",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCOMD", cQueryName = "DESCOMD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 206446902,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=226, Top=86, InputMask=replicate('X',30)

  add object oCHKAMM_1_10 as StdCheck with uid="KPSIOJSIDO",rtseq=7,rtrep=.f.,left=42, top=147, caption="Str. amministrativa",;
    ToolTipText = "Seleziona le strutture amministrative per la copia",;
    HelpContextID = 204186662,;
    cFormVar="w_CHKAMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKAMM_1_10.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func oCHKAMM_1_10.GetRadio()
    this.Parent.oContained.w_CHKAMM = this.RadioValue()
    return .t.
  endfunc

  func oCHKAMM_1_10.SetRadio()
    this.Parent.oContained.w_CHKAMM=trim(this.Parent.oContained.w_CHKAMM)
    this.value = ;
      iif(this.Parent.oContained.w_CHKAMM=='P',1,;
      0)
  endfunc

  func oCHKAMM_1_10.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCHKGEST_1_11 as StdCheck with uid="NCQJSXUGXB",rtseq=8,rtrep=.f.,left=42, top=173, caption="Struttura gestionale",;
    ToolTipText = "Seleziona le strutture gestionali per la copia",;
    HelpContextID = 240016346,;
    cFormVar="w_CHKGEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKGEST_1_11.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oCHKGEST_1_11.GetRadio()
    this.Parent.oContained.w_CHKGEST = this.RadioValue()
    return .t.
  endfunc

  func oCHKGEST_1_11.SetRadio()
    this.Parent.oContained.w_CHKGEST=trim(this.Parent.oContained.w_CHKGEST)
    this.value = ;
      iif(this.Parent.oContained.w_CHKGEST=='G',1,;
      0)
  endfunc

  func oCHKGEST_1_11.mHide()
    with this.Parent.oContained
      return (nvl(.w_UNIQUE,' ')='S' or g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCHKTECN_1_12 as StdCheck with uid="JOUJQUUENY",rtseq=9,rtrep=.f.,left=42, top=199, caption="Struttura tecnica",;
    ToolTipText = "Seleziona le strutture tecniche per la copia",;
    HelpContextID = 29271078,;
    cFormVar="w_CHKTECN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKTECN_1_12.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oCHKTECN_1_12.GetRadio()
    this.Parent.oContained.w_CHKTECN = this.RadioValue()
    return .t.
  endfunc

  func oCHKTECN_1_12.SetRadio()
    this.Parent.oContained.w_CHKTECN=trim(this.Parent.oContained.w_CHKTECN)
    this.value = ;
      iif(this.Parent.oContained.w_CHKTECN=='T',1,;
      0)
  endfunc

  func oCHKTECN_1_12.mHide()
    with this.Parent.oContained
      return (nvl(.w_UNIQUE,' ')='S' or g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCHKATT_1_13 as StdCheck with uid="HNRGCNOEKB",rtseq=10,rtrep=.f.,left=228, top=147, caption="Anche attivit�",;
    ToolTipText = "Ricopia anche le attivit�",;
    HelpContextID = 60531750,;
    cFormVar="w_CHKATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKATT_1_13.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oCHKATT_1_13.GetRadio()
    this.Parent.oContained.w_CHKATT = this.RadioValue()
    return .t.
  endfunc

  func oCHKATT_1_13.SetRadio()
    this.Parent.oContained.w_CHKATT=trim(this.Parent.oContained.w_CHKATT)
    this.value = ;
      iif(this.Parent.oContained.w_CHKATT=='A',1,;
      0)
  endfunc

  func oCHKATT_1_13.mHide()
    with this.Parent.oContained
      return (.w_CHKTECN+.w_CHKGEST+.w_CHKAMM='TGP')
    endwith
  endfunc


  add object oBtn_1_20 as StdButton with uid="MTHUHZKMQI",left=349, top=166, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la copia delle strutture selezionate";
    , HelpContextID = 99215386;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSPC_BCX with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not EMPTY (.w_CODCOMS) and not EMPTY(.w_CODCOMD))
      endwith
    endif
  endfunc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="KOTWJAFPMC",left=399, top=166, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 91926714;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="GOPZKQXLUH",left=349, top=166, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esegue la copia delle strutture selezionate";
    , HelpContextID = 99215386;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do GSPC_BCX with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not EMPTY (.w_CODCOMS) and not EMPTY(.w_CODCOMD) and (.w_CHKTECN='T' or .w_CHKGEST='G' or .w_CHKAMM='P'))
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="QRGRDTRPKH",left=399, top=166, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 260137238;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
     endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="YFPSLZYTDI",Visible=.t., Left=46, Top=58,;
    Alignment=1, Width=54, Height=15,;
    Caption="Sorgente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YXNGHUOUHI",Visible=.t., Left=7, Top=87,;
    Alignment=1, Width=93, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="PDYTPJIXUF",Visible=.t., Left=5, Top=124,;
    Alignment=1, Width=49, Height=15,;
    Caption="Copia:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="YBMTJTMXAZ",Visible=.t., Left=8, Top=6,;
    Alignment=0, Width=379, Height=15,;
    Caption="La procedura copia le strutture selezionate tra due commesse."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="ZXOEQVNYGT",Visible=.t., Left=8, Top=35,;
    Alignment=1, Width=68, Height=15,;
    Caption="Commesse:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_18 as StdBox with uid="JSNKRUWSAL",left=8, top=50, width=440,height=0

  add object oBox_1_19 as StdBox with uid="XRHEGMRXNR",left=8, top=141, width=438,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kcx','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
