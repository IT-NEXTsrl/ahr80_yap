* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_msp                                                        *
*              Scadenze commessa                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_46]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-25                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_msp"))

* --- Class definition
define class tgspc_msp as StdTrsForm
  Top    = 57
  Left   = 61

  * --- Standard Properties
  Width  = 626
  Height = 344+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=171288471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RAT_COMM_IDX = 0
  CAN_TIER_IDX = 0
  DOC_MAST_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "RAT_COMM"
  cKeySelect = "SCSERIAL"
  cKeyWhere  = "SCSERIAL=this.w_SCSERIAL"
  cKeyDetail  = "SCSERIAL=this.w_SCSERIAL and SCCODCOM=this.w_SCCODCOM"
  cKeyWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cKeyDetailWhereODBC = '"SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';
      +'+" and SCCODCOM="+cp_ToStrODBC(this.w_SCCODCOM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RAT_COMM.SCSERIAL="+cp_ToStrODBC(this.w_SCSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RAT_COMM.CPROWNUM '
  cPrg = "gspc_msp"
  cComment = "Scadenze commessa"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SCSERIAL = space(10)
  w_SERIAL = space(10)
  w_SCCONFER = space(1)
  w_FIRST = space(10)
  w_SCIMPCOM = 0
  w_SCCODCOM = space(15)
  w_SCDATSCA = ctod('  /  /  ')
  w_TOTIMP = 0
  o_TOTIMP = 0
  w_SBILA = 0
  w_DESCOM = space(30)
  w_DATDOC = ctod('  /  /  ')
  w_TIPDOC = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DESDOC = space(35)
  w_FLEVAC = space(1)
  o_FLEVAC = space(1)
  w_CLADOC = space(2)
  o_CLADOC = space(2)
  w_PARAME = space(3)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gspc_msp
  * totale al termine della Loadrec
  w_TOTINIT=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RAT_COMM','gspc_msp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_mspPag1","gspc_msp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Scadenze commessa")
      .Pages(1).HelpContextID = 187989006
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='RAT_COMM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RAT_COMM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RAT_COMM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SCSERIAL = NVL(SCSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RAT_COMM where SCSERIAL=KeySet.SCSERIAL
    *                            and SCCODCOM=KeySet.SCCODCOM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2],this.bLoadRecFilter,this.RAT_COMM_IDX,"gspc_msp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RAT_COMM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RAT_COMM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RAT_COMM '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCSERIAL',this.w_SCSERIAL  )
      select * from (i_cTable) RAT_COMM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SERIAL = .w_SCSERIAL
        .w_TOTIMP = 0
        .w_DATDOC = ctod("  /  /  ")
        .w_TIPDOC = space(5)
        .w_NUMDOC = 0
        .w_ALFDOC = space(10)
        .w_DESDOC = space(35)
        .w_FLEVAC = space(1)
        .w_CLADOC = space(2)
        .w_SCSERIAL = NVL(SCSERIAL,space(10))
          .link_1_3('Load')
        .w_SCCONFER = NVL(SCCONFER,space(1))
        .w_SBILA = .w_TOTIMP  - .w_TOTINIT
          .link_1_6('Load')
        .w_PARAME = .w_FLEVAC + .w_CLADOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'RAT_COMM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTIMP = 0
      scan
        with this
          .w_FIRST = space(10)
          .w_DESCOM = space(30)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_SCIMPCOM = NVL(SCIMPCOM,0)
          .w_SCCODCOM = NVL(SCCODCOM,space(15))
          if link_2_3_joined
            this.w_SCCODCOM = NVL(CNCODCAN203,NVL(this.w_SCCODCOM,space(15)))
            this.w_DESCOM = NVL(CNDESCAN203,space(30))
          else
          .link_2_3('Load')
          endif
          .w_SCDATSCA = NVL(cp_ToDate(SCDATSCA),ctod("  /  /  "))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMP = .w_TOTIMP+.w_SCIMPCOM
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_SBILA = .w_TOTIMP  - .w_TOTINIT
        .w_PARAME = .w_FLEVAC + .w_CLADOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_16.enabled = .oPgFrm.Page1.oPag.oBtn_1_16.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gspc_msp
        * w_TOTINIT totale iniziale
        * non riesco a calcolarlo all'inizio perch� al
        * caricamento della prima registrazione la prima
        * riga non mi esegue la Init su una variabile
        this.w_TOTINIT=This.w_TOTIMP
        THIS.w_SBILA = THIS.w_TOTIMP  - THIS.w_TOTINIT
        THIS.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_SCSERIAL=space(10)
      .w_SERIAL=space(10)
      .w_SCCONFER=space(1)
      .w_FIRST=space(10)
      .w_SCIMPCOM=0
      .w_SCCODCOM=space(15)
      .w_SCDATSCA=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_SBILA=0
      .w_DESCOM=space(30)
      .w_DATDOC=ctod("  /  /  ")
      .w_TIPDOC=space(5)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DESDOC=space(35)
      .w_FLEVAC=space(1)
      .w_CLADOC=space(2)
      .w_PARAME=space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_SERIAL = .w_SCSERIAL
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SERIAL))
         .link_1_3('Full')
        endif
        .DoRTCalc(3,6,.f.)
        if not(empty(.w_SCCODCOM))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,8,.f.)
        .w_SBILA = .w_TOTIMP  - .w_TOTINIT
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_TIPDOC))
         .link_1_6('Full')
        endif
        .DoRTCalc(13,17,.f.)
        .w_PARAME = .w_FLEVAC + .w_CLADOC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RAT_COMM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCCONFER_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RAT_COMM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCSERIAL,"SCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCONFER,"SCCONFER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
    i_lTable = "RAT_COMM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RAT_COMM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPC_SSC with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FIRST C(10);
      ,t_SCIMPCOM N(18,4);
      ,t_SCCODCOM C(15);
      ,t_SCDATSCA D(8);
      ,t_DESCOM C(30);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgspc_mspbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFIRST_2_1.controlsource=this.cTrsName+'.t_FIRST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCIMPCOM_2_2.controlsource=this.cTrsName+'.t_SCIMPCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODCOM_2_3.controlsource=this.cTrsName+'.t_SCCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCDATSCA_2_4.controlsource=this.cTrsName+'.t_SCDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_5.controlsource=this.cTrsName+'.t_DESCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(22)
    this.AddVLine(146)
    this.AddVLine(372)
    this.AddVLine(453)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIRST_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
      *
      * insert into RAT_COMM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RAT_COMM')
        i_extval=cp_InsertValODBCExtFlds(this,'RAT_COMM')
        i_cFldBody=" "+;
                  "(SCSERIAL,SCCONFER,SCIMPCOM,SCCODCOM,SCDATSCA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SCSERIAL)+","+cp_ToStrODBC(this.w_SCCONFER)+","+cp_ToStrODBC(this.w_SCIMPCOM)+","+cp_ToStrODBCNull(this.w_SCCODCOM)+","+cp_ToStrODBC(this.w_SCDATSCA)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RAT_COMM')
        i_extval=cp_InsertValVFPExtFlds(this,'RAT_COMM')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SCSERIAL',this.w_SCSERIAL,'SCCODCOM',this.w_SCCODCOM)
        INSERT INTO (i_cTable) (;
                   SCSERIAL;
                  ,SCCONFER;
                  ,SCIMPCOM;
                  ,SCCODCOM;
                  ,SCDATSCA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SCSERIAL;
                  ,this.w_SCCONFER;
                  ,this.w_SCIMPCOM;
                  ,this.w_SCCODCOM;
                  ,this.w_SCDATSCA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_SCCODCOM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RAT_COMM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " SCCONFER="+cp_ToStrODBC(this.w_SCCONFER)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RAT_COMM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  SCCONFER=this.w_SCCONFER;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_SCCODCOM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RAT_COMM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RAT_COMM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " SCCONFER="+cp_ToStrODBC(this.w_SCCONFER)+;
                     ",SCIMPCOM="+cp_ToStrODBC(this.w_SCIMPCOM)+;
                     ",SCDATSCA="+cp_ToStrODBC(this.w_SCDATSCA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RAT_COMM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      SCCONFER=this.w_SCCONFER;
                     ,SCIMPCOM=this.w_SCIMPCOM;
                     ,SCDATSCA=this.w_SCDATSCA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_SCCODCOM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RAT_COMM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_SCCODCOM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RAT_COMM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAT_COMM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_3('Full')
        .DoRTCalc(3,5,.t.)
          .link_2_3('Full')
        .DoRTCalc(7,8,.t.)
        if .o_TOTIMP<>.w_TOTIMP
          .w_SBILA = .w_TOTIMP  - .w_TOTINIT
        endif
        .DoRTCalc(10,11,.t.)
          .link_1_6('Full')
        .DoRTCalc(13,17,.t.)
        if .o_FLEVAC<>.w_FLEVAC.or. .o_CLADOC<>.w_CLADOC
          .w_PARAME = .w_FLEVAC + .w_CLADOC
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCIMPCOM_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSCIMPCOM_2_2.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_1.visible=!this.oPgFrm.Page1.oPag.oStr_1_1.mHide()
    this.oPgFrm.Page1.oPag.oDATDOC_1_5.visible=!this.oPgFrm.Page1.oPag.oDATDOC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTIPDOC_1_6.visible=!this.oPgFrm.Page1.oPag.oTIPDOC_1_6.mHide()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_7.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_7.mHide()
    this.oPgFrm.Page1.oPag.oALFDOC_1_8.visible=!this.oPgFrm.Page1.oPag.oALFDOC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESDOC_1_9.visible=!this.oPgFrm.Page1.oPag.oDESDOC_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SERIAL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVFLVEAC,MVCLADOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_SERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_SERIAL)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVFLVEAC,MVCLADOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIAL = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
      this.w_TIPDOC = NVL(_Link_.MVTIPDOC,space(5))
      this.w_FLEVAC = NVL(_Link_.MVFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.MVCLADOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SERIAL = space(10)
      endif
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
      this.w_TIPDOC = space(5)
      this.w_FLEVAC = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_SCCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_SCCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CNCODCAN as CNCODCAN203"+ ",link_2_3.CNDESCAN as CNDESCAN203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on RAT_COMM.SCCODCOM=link_2_3.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and RAT_COMM.SCCODCOM=link_2_3.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSCCONFER_1_4.RadioValue()==this.w_SCCONFER)
      this.oPgFrm.Page1.oPag.oSCCONFER_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oSBILA_3_2.value==this.w_SBILA)
      this.oPgFrm.Page1.oPag.oSBILA_3_2.value=this.w_SBILA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_1_5.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_1_5.value=this.w_DATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_6.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_6.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_7.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_7.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_8.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_8.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_9.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_9.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIRST_2_1.value==this.w_FIRST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIRST_2_1.value=this.w_FIRST
      replace t_FIRST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFIRST_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCIMPCOM_2_2.value==this.w_SCIMPCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCIMPCOM_2_2.value=this.w_SCIMPCOM
      replace t_SCIMPCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCIMPCOM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODCOM_2_3.value==this.w_SCCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODCOM_2_3.value=this.w_SCCODCOM
      replace t_SCCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODCOM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCDATSCA_2_4.value==this.w_SCDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCDATSCA_2_4.value=this.w_SCDATSCA
      replace t_SCDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCDATSCA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_5.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_5.value=this.w_DESCOM
      replace t_DESCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'RAT_COMM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_SBILA=0)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Totale scadenze diverso da totale iniziale"))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_SCCODCOM)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_SCCODCOM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TOTIMP = this.w_TOTIMP
    this.o_FLEVAC = this.w_FLEVAC
    this.o_CLADOC = this.w_CLADOC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_SCCODCOM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=.f.
    if !i_bRes
      cp_ErrorMsg("Impossibile cancellare la scadenza utilizzare la funzionalit� di calcolo scadenze commesse","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FIRST=space(10)
      .w_SCIMPCOM=0
      .w_SCCODCOM=space(15)
      .w_SCDATSCA=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_SCCODCOM))
        .link_2_3('Full')
      endif
    endwith
    this.DoRTCalc(7,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FIRST = t_FIRST
    this.w_SCIMPCOM = t_SCIMPCOM
    this.w_SCCODCOM = t_SCCODCOM
    this.w_SCDATSCA = t_SCDATSCA
    this.w_DESCOM = t_DESCOM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FIRST with this.w_FIRST
    replace t_SCIMPCOM with this.w_SCIMPCOM
    replace t_SCCODCOM with this.w_SCCODCOM
    replace t_SCDATSCA with this.w_SCDATSCA
    replace t_DESCOM with this.w_DESCOM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMP = .w_TOTIMP-.w_scimpcom
        .SetControlsValue()
      endwith
  EndProc
  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile inserire scadenze utilizzare la funzionalit� di calcolo scadenze commesse"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare il documento utilizzare la funzionalit� di calcolo scadenze commesse"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgspc_mspPag1 as StdContainer
  Width  = 622
  height = 344
  stdWidth  = 622
  stdheight = 344
  resizeXpos=178
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCONFER_1_4 as StdCheck with uid="BLSVZVVEQN",rtseq=3,rtrep=.f.,left=492, top=63, caption="Non ricalcolare",;
    ToolTipText = "Se attivo il documento non viene ricalcolato durante l'elaborazione scadenze",;
    HelpContextID = 90775672,;
    cFormVar="w_SCCONFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCCONFER_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SCCONFER,&i_cF..t_SCCONFER),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oSCCONFER_1_4.GetRadio()
    this.Parent.oContained.w_SCCONFER = this.RadioValue()
    return .t.
  endfunc

  func oSCCONFER_1_4.ToRadio()
    this.Parent.oContained.w_SCCONFER=trim(this.Parent.oContained.w_SCCONFER)
    return(;
      iif(this.Parent.oContained.w_SCCONFER=='S',1,;
      0))
  endfunc

  func oSCCONFER_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDATDOC_1_5 as StdField with uid="AHKLXFGNJG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATDOC", cQueryName = "DATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 227594954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=357, Top=63

  func oDATDOC_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
    endif
  endfunc

  add object oTIPDOC_1_6 as StdField with uid="NWYMZPLHVO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documento",;
    HelpContextID = 227609034,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=37, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
    endif
  endfunc

  func oTIPDOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNUMDOC_1_7 as StdField with uid="RDDGKVYNOB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 227618346,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=104, Top=63

  func oNUMDOC_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
    endif
  endfunc

  add object oALFDOC_1_8 as StdField with uid="IHBOIJNPZW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento",;
    HelpContextID = 227649530,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=231, Top=63, InputMask=replicate('X',10)

  func oALFDOC_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
    endif
  endfunc

  add object oDESDOC_1_9 as StdField with uid="SAWPGNEHMU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 227598026,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=168, Top=37, InputMask=replicate('X',35)

  func oDESDOC_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="AKXNDLRXZH",left=438, top=40, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento";
    , HelpContextID = 233985338;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SCSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    with this.Parent.oContained
      return (Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ))
    endwith
  endfunc

  func oBtn_1_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) )))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=14, top=96, width=597,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="FIRST",Label1="' '",Field2="SCCODCOM",Label2="Commessa",Field3="DESCOM",Label3="Descrizione",Field4="SCDATSCA",Label4="Scadenza",Field5="SCIMPCOM",Label5="Importo (Euro)",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218943866

  add object oStr_1_1 as StdString with uid="RVWLVKBRTO",Visible=.t., Left=81, Top=49,;
    Alignment=0, Width=349, Height=19,;
    Caption="Scadenze riferite a documento eliminato"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_1.mHide()
    with this.Parent.oContained
      return (Empty(.w_SCSERIAL) Or Not Empty(.w_TIPDOC))
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="IUDHYAUZEC",Visible=.t., Left=35, Top=37,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="AICVSDPFEA",Visible=.t., Left=19, Top=64,;
    Alignment=1, Width=83, Height=18,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="IKTDAMJZMF",Visible=.t., Left=220, Top=65,;
    Alignment=2, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="PFHSGWEZDO",Visible=.t., Left=324, Top=64,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (Not ( Empty( .w_SCSERIAL )  Or Not Empty( .w_TIPDOC ) ))
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="YYOSMIXQBO",Visible=.t., Left=15, Top=9,;
    Alignment=0, Width=328, Height=19,;
    Caption="Estremi documento"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_14 as StdBox with uid="JQQIFSPBOP",left=14, top=25, width=603,height=66

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=5,top=115,;
    width=593+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=6,top=116,width=592+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMP_3_1 as StdField with uid="FEUNHGRSHX",rtseq=8,rtrep=.f.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    HelpContextID = 11256778,;
    cQueryName = "TOTIMP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=456, Top=320, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oSBILA_3_2 as StdField with uid="AJIJSIYRIS",rtseq=9,rtrep=.f.,;
    cFormVar="w_SBILA",value=0,enabled=.f.,;
    ToolTipText = "Sbilancio tra importi inseriti e totale iniziale",;
    HelpContextID = 244743974,;
    cQueryName = "SBILA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=261, Top=320, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oStr_3_3 as StdString with uid="PEFYOAQGBO",Visible=.t., Left=151, Top=320,;
    Alignment=1, Width=106, Height=18,;
    Caption="Sbilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="EVGPCKLYJQ",Visible=.t., Left=400, Top=320,;
    Alignment=1, Width=53, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgspc_mspBodyRow as CPBodyRowCnt
  Width=583
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFIRST_2_1 as StdTrsField with uid="SNFXZYUIRC",rtseq=4,rtrep=.t.,;
    cFormVar="w_FIRST",value=space(10),;
    HelpContextID = 265164118,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=2, Left=-2, Top=0, InputMask=replicate('X',10)

  add object oSCIMPCOM_2_2 as StdTrsField with uid="NWJMFWSHHH",rtseq=5,rtrep=.t.,;
    cFormVar="w_SCIMPCOM",value=0,;
    HelpContextID = 226000781,;
    cTotal = "this.Parent.oContained.w_totimp", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=439, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oSCIMPCOM_2_2.mCond()
    with this.Parent.oContained
      return (Not Empty ( .w_SCCODCOM ))
    endwith
  endfunc

  add object oSCCODCOM_2_3 as StdTrsField with uid="JNNZMUMMNH",rtseq=6,rtrep=.t.,;
    cFormVar="w_SCCODCOM",value=space(15),enabled=.f.,isprimarykey=.t.,;
    HelpContextID = 238477197,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=10, Top=0, InputMask=replicate('X',15), cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_SCCODCOM"

  func oSCCODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oSCDATSCA_2_4 as StdTrsField with uid="HMMOUYEWWZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_SCDATSCA",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 45822055,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=357, Top=0

  add object oDESCOM_2_5 as StdTrsField with uid="HAFLNHUMZC",rtseq=10,rtrep=.t.,;
    cFormVar="w_DESCOM",value=space(30),enabled=.f.,;
    HelpContextID = 59891402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=130, Top=0, InputMask=replicate('X',30)
  add object oLast as LastKeyMover
  * ---
  func oFIRST_2_1.When()
    return(.t.)
  proc oFIRST_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFIRST_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_msp','RAT_COMM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCSERIAL=RAT_COMM.SCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
