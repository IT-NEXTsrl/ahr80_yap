* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bmp                                                        *
*              Maschera pianificazione                                         *
*                                                                              *
*      Author:                                                                 *
*      Client: Nicola Gorlandi                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_9]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-04                                                      *
* Last revis.: 2006-02-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bmp",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bmp as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_OBJMOV = .NULL.
  w_PARMOV = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Maschera Pianificazione
    * --- "LANCIA" Lancia Movimento Provvisorio
    * --- "SELECTALL" Seleziona de seleziona gli elementi dello Zoom
    do case
      case this.pTipo="LANCIA"
        * --- Movimento Provvisorio con visualizzati i prezzi
        this.w_PARMOV = "NP"
        this.w_OBJMOV = GSPC_MAT( this.w_PARMOV )
        * --- Testo subito se l'utente pu� aprire la gestione
        if !(this.w_OBJMOV.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Lancio l'Oggetto in Interrogazione
        this.w_OBJMOV.EcpFilter()     
        this.w_OBJMOV.w_MASERIAL = this.oParentObject.w_SERIAL
        this.w_OBJMOV.EcpSave()     
      case this.pTipo="SELECTALL"
        * --- Seleziona Deseleziona Tutti
        Update (This.oParentObject.w_ZOOM.cCursor) Set Xchk= this.oParentObject.w_SELALL
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
