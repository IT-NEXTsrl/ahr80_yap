* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_scf                                                        *
*              Cash flow di commessa                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_32]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-03                                                      *
* Last revis.: 2012-06-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_scf",oParentObject))

* --- Class definition
define class tgspc_scf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 710
  Height = 435
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-14"
  HelpContextID=90855529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  ATTIVITA_IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_scf"
  cComment = "Cash flow di commessa"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(10)
  w_DEFCOM = space(15)
  w_CODCOM = space(15)
  w_COMFIN = ctod('  /  /  ')
  o_COMFIN = ctod('  /  /  ')
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_PERIODO = space(1)
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_VALCOM = space(3)
  w_CLICOM = space(15)
  w_CAOVAL = 0
  w_GRAPH = .F.
  w_DECTOT = 0
  w_GrafhCash = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_scfPag1","gspc_scf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOM_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_GrafhCash = this.oPgFrm.Pages(1).oPag.GrafhCash
    DoDefault()
    proc Destroy()
      this.w_GrafhCash = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='CPAR_DEF'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(10)
      .w_DEFCOM=space(15)
      .w_CODCOM=space(15)
      .w_COMFIN=ctod("  /  /  ")
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_PERIODO=space(1)
      .w_DESCOM=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_VALCOM=space(3)
      .w_CLICOM=space(15)
      .w_CAOVAL=0
      .w_GRAPH=.f.
      .w_DECTOT=0
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODCOM = .w_DEFCOM
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCOM))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_DATAFIN = .w_COMFIN+7
        .w_PERIODO = 'S'
          .DoRTCalc(8,8,.f.)
        .w_OBTEST = i_DATSYS
        .w_VALCOM = g_CODEUR
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_VALCOM))
          .link_1_15('Full')
        endif
      .oPgFrm.Page1.oPag.GrafhCash.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(11,12,.f.)
        .w_GRAPH = .F.
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
        if .o_COMFIN<>.w_COMFIN
            .w_DATAFIN = .w_COMFIN+7
        endif
        .DoRTCalc(7,9,.t.)
            .w_VALCOM = g_CODEUR
          .link_1_15('Full')
        .oPgFrm.Page1.oPag.GrafhCash.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.GrafhCash.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.GrafhCash.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_3'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNCODCON,CNDATINI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_CLICOM = NVL(_Link_.CNCODCON,space(15))
      this.w_DATAINI = NVL(cp_ToDate(_Link_.CNDATINI),ctod("  /  /  "))
      this.w_COMFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_CLICOM = space(15)
      this.w_DATAINI = ctod("  /  /  ")
      this.w_COMFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALCOM
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALCOM)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALCOM = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALCOM = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_3.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_3.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_5.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_5.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_6.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_8.RadioValue()==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_11.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_11.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATAINI)) or not(.w_DATAINI<=.w_DATAFIN OR EMPTY(.w_DATAFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non significativo")
          case   ((empty(.w_DATAFIN)) or not(.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non significativo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COMFIN = this.w_COMFIN
    return

enddefine

* --- Define pages as container
define class tgspc_scfPag1 as StdContainer
  Width  = 706
  height = 435
  stdWidth  = 706
  stdheight = 435
  resizeXpos=359
  resizeYpos=231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOM_1_3 as StdField with uid="IHZLXPMVWS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 214776614,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=79, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDATAINI_1_5 as StdField with uid="YZRGONYNAA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non significativo",;
    ToolTipText = "Data inizio cash flow",;
    HelpContextID = 43242186,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=445, Top=11

  func oDATAINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINI<=.w_DATAFIN OR EMPTY(.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_6 as StdField with uid="BVUNLYAPGI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non significativo",;
    ToolTipText = "Data fine cash flow",;
    HelpContextID = 130273994,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=546, Top=11

  func oDATAFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATAINI<=.w_DATAFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_7 as StdButton with uid="HWIJLNYWTB",left=650, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare";
    , HelpContextID = 69361917;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSPC_BFL(this.Parent.oContained,"GRAPH")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODCOM))
      endwith
    endif
  endfunc


  add object oPERIODO_1_8 as StdCombo with uid="UOBJKITBYD",rtseq=7,rtrep=.f.,left=79,top=384,width=106,height=21;
    , ToolTipText = "Periodo di visualizzazione grafico";
    , HelpContextID = 64229878;
    , cFormVar="w_PERIODO",RowSource=""+"Settimanale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPERIODO_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oPERIODO_1_8.GetRadio()
    this.Parent.oContained.w_PERIODO = this.RadioValue()
    return .t.
  endfunc

  func oPERIODO_1_8.SetRadio()
    this.Parent.oContained.w_PERIODO=trim(this.Parent.oContained.w_PERIODO)
    this.value = ;
      iif(this.Parent.oContained.w_PERIODO=='S',1,;
      iif(this.Parent.oContained.w_PERIODO=='M',2,;
      0))
  endfunc


  add object oBtn_1_9 as StdButton with uid="IWBHDQZJEJ",left=600, top=384, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare gli importi";
    , HelpContextID = 50934054;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSPC_BFL(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_GRAPH)
      endwith
    endif
  endfunc

  add object oDESCOM_1_11 as StdField with uid="LEYLUAWALB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 214835510,;
   bGlobalFont=.t.,;
    Height=21, Width=201, Left=205, Top=11, InputMask=replicate('X',30)


  add object GrafhCash as cp_FoxCharts with uid="IWOFCVICJG",left=5, top=59, width=696,height=318,;
    caption='Object',;
   bGlobalFont=.t.,;
    cFileCfg="..\COMM\exe\query\gspc_scf_l",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",cSource="_Cash_",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 87142118


  add object oObj_1_20 as cp_runprogram with uid="ZLQXSTZSZE",left=565, top=446, width=132,height=19,;
    caption='GSPC_BFL',;
   bGlobalFont=.t.,;
    prg='GSPC_BFL("DONE")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 47054770


  add object oBtn_1_24 as StdButton with uid="KOTWJAFPMC",left=650, top=384, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83538106;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="EHUTOWLIQL",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=72, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RLNGHMBSWZ",Visible=.t., Left=408, Top=11,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="LNNQBDFEEA",Visible=.t., Left=524, Top=11,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YJFLGWQHSC",Visible=.t., Left=17, Top=384,;
    Alignment=1, Width=60, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GPFFUTAEEY",Visible=.t., Left=558, Top=335,;
    Alignment=0, Width=138, Height=17,;
    Caption="Importi espressi in Euro"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_GRAPH=.F.)
    endwith
  endfunc

  add object oBox_1_22 as StdBox with uid="OVXHYONBYW",left=4, top=59, width=698,height=320
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_scf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
