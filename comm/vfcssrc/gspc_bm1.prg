* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bm1                                                        *
*              Cambia prezzi al variare del listino                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_389]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2006-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bm1",oParentObject)
return(i_retval)

define class tgspc_bm1 as StdBatch
  * --- Local variables
  w_SCOLIS = space(1)
  w_MESS = space(10)
  w_RECO = 0
  w_AGGRIG = .f.
  w_NUREC = 0
  w_OK = .f.
  w_NabsRow = 0
  w_NRelRow = 0
  * --- WorkFile variables
  TIP_DOCU_idx=0
  TAB_SCON_idx=0
  LISTINI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Documento al Variare del Listino  e della data (da GSPC_MAT)
    if NOT EMPTY(this.oParentObject.w_MACODLIS)
      this.w_RECO = RECCOUNT(this.oParentObject.cTrsName)
      this.w_NUREC = RECNO(this.oParentObject.cTrsName)
      if this.w_RECO>1 OR (this.w_RECO=1 AND NOT EMPTY(this.oParentObject.w_MACODKEY))
        this.w_OK = ah_YesNo("Aggiorno i prezzi sulle righe in base al listino impostato?")
        if this.w_OK=.T.
          this.oParentObject.mCalc(.t.)
          this.w_NabsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
          this.w_NRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
          SELECT (this.oParentObject.cTrsName)
          GO TOP
          SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_MACODKEY) AND NOT DELETED()
          * --- Legge i Dati del Temporaneo
          this.oParentObject.WorkFromTrs()
          this.oParentObject.SaveDependsOn()
          this.oParentObject.w_MACOTLIS = this.oParentObject.w_MACODLIS
          SELECT (this.oParentObject.cTrsName)
          this.oParentObject.NotifyEvent("Ricalcola")
          SELECT (this.oParentObject.cTrsName)
          * --- Carica il Temporaneo dei Dati
          this.oParentObject.TrsFromWork()
          * --- Flag Notifica Riga Variata
          if i_SRV<>"A"
            replace i_SRV with "U"
          endif
          ENDSCAN
          SELECT (this.oParentObject.cTrsName)
          if this.w_NUREC>0 AND this.w_NUREC<=RECCOUNT()
            GOTO this.w_NUREC
          else
            GO TOP
          endif
          With this.oParentObject
          .WorkFromTrs()
          .SaveDependsOn()
          .SetControlsValue()
          .ChildrenChangeRow()
          .oPgFrm.Page1.oPag.oBody.Refresh()
          .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NabsRow
          .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRelRow
          EndWith
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='TAB_SCON'
    this.cWorkTables[3]='LISTINI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
