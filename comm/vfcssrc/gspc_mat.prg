* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_mat                                                        *
*              Movimenti di commessa                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_443]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-27                                                      *
* Last revis.: 2016-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gspc_mat
* passa un parametro per visualizzare o meno i prezzi (PRIMO CARATTERE) 'S/N'
* il secondo per sapere se movimento preventivo o consuntivo (SECONDO CARATERE) 'P/C'
Parameters pTipo
* Se .f. visualizza anche i prezzi e il listino
* --- Fine Area Manuale
return(createobject("tgspc_mat"))

* --- Class definition
define class tgspc_mat as StdTrsForm
  Top    = 4
  Left   = 12

  * --- Standard Properties
  Width  = 760
  Height = 374+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-14"
  HelpContextID=130701417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=97

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MAT_MAST_IDX = 0
  MAT_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  LISTINI_IDX = 0
  ART_ICOL_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  VALUTE_IDX = 0
  UNIMIS_IDX = 0
  MA_COSTI_IDX = 0
  TIPCOSTO_IDX = 0
  VOCIIVA_IDX = 0
  PC_FAMIG_IDX = 0
  VOC_COST_IDX = 0
  CPAR_DEF_IDX = 0
  cFile = "MAT_MAST"
  cFileDetail = "MAT_DETT"
  cKeySelect = "MASERIAL"
  cKeyWhere  = "MASERIAL=this.w_MASERIAL"
  cKeyDetail  = "MASERIAL=this.w_MASERIAL"
  cKeyWhereODBC = '"MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)';

  cKeyDetailWhereODBC = '"MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MAT_DETT.MASERIAL="+cp_ToStrODBC(this.w_MASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAT_DETT.CPROWORD '
  cPrg = "gspc_mat"
  cComment = "Movimenti di commessa"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  cAutoZoom = 'GSPC_MAT'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- gspc_mat
  w_TIPMOV=''
  * caricamento automatico
  * metto le dichiarazioni qui per evitare che
  * siano sbiancate nella BlankRec
  w_CODCOM=Space ( 15 )
  w_CODLIS = Space ( 5 )
  * --- Fine Area Manuale

  * --- Local Variables
  w_MADATREG = ctod('  /  /  ')
  o_MADATREG = ctod('  /  /  ')
  w_MA__ANNO = space(4)
  w_MAALFREG = 0
  w_MANUMREG = 0
  w_READPAR = space(10)
  w_MADATREG = ctod('  /  /  ')
  w_DEFCOM = space(15)
  w_TIPATT = space(1)
  w_MA__ANNO = space(4)
  w_MASERIAL = space(10)
  w_MATIPMOV = space(1)
  o_MATIPMOV = space(1)
  w_MACODCOM = space(15)
  o_MACODCOM = space(15)
  w_MACODATT = space(15)
  o_MACODATT = space(15)
  w_VALCOM = space(3)
  o_VALCOM = space(3)
  w_MACODVAL = space(3)
  o_MACODVAL = space(3)
  w_COMDES = space(30)
  w_ATTDES = space(30)
  w_LISDES = space(40)
  w_SIMVAL = space(5)
  w_MACAOVAL = 0
  w_MACODLIS = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_CENCOS = space(15)
  w_DECTOT = 0
  w_CALPICTU = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_PIANIF = space(1)
  w_CODFAM = space(5)
  o_CODFAM = space(5)
  w_CAOCOM = 0
  w_DECCOM = 0
  w_CALPICT = 0
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_IVALIS = space(1)
  w_HASEVCOP = space(10)
  w_HASEVENT = .F.
  w_DECUNI = 0
  w_CALCPICU = 0
  w_MACOTLIS = space(5)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_CPROWORD = 0
  w_MACODKEY = space(20)
  o_MACODKEY = space(20)
  w_MADESCRI = space(40)
  w_MACODART = space(20)
  o_MACODART = space(20)
  w_CODIVA = space(5)
  w_VOCCEN = space(15)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_CODCOS = space(5)
  o_CODCOS = space(5)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_PERIVA = 0
  w_MAUNIMIS = space(3)
  o_MAUNIMIS = space(3)
  w_MAQTAMOV = 0
  o_MAQTAMOV = 0
  w_MAPREZZO = 0
  o_MAPREZZO = 0
  w_UMCAL = space(10)
  w_DTOBS1 = ctod('  /  /  ')
  w_MAOPPREV = space(1)
  w_MACOATTS = space(15)
  w_MACOCOMS = space(15)
  w_MATIPATT = space(1)
  w_MAOPCONS = space(1)
  w_MA__NOTE = space(0)
  w_MACODCOS = space(5)
  w_MAQTA1UM = 0
  w_MAVALRIG = 0
  w_LIPREZZO = 0
  o_LIPREZZO = 0
  w_CODCOSDE = space(5)
  w_MADATANN = ctod('  /  /  ')
  w_TOTRIGA = 0
  w_TIPCOD = space(1)
  w_MACODFAM = space(5)
  w_FADESC = space(50)
  w_TESTUM = .F.
  w_ROW = 0
  w_RED = 0
  w_GREEN = 0
  w_BLUE = 0
  w_FLSERG = space(1)
  w_MADICRIF = space(15)
  w_FLFRAZ = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_QTALIS = 0
  w_TOTMOV = 0
  w_TOTDOC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MASERIAL = this.W_MASERIAL
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MA__ANNO = this.W_MA__ANNO
  op_MAALFREG = this.W_MAALFREG
  op_MANUMREG = this.W_MANUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gspc_mat
  * GESTIONE DELLA CANCELLAZIONE DELLA RIGA
  w_OLDTIPRIG = ''
  w_OLDROWNUM = 0
  
  * --- Gestisce la modifica e la cancellazione
  func ah_HasCPEvents(i_cOp)
    if this.cFunction="Query" and inlist(Upper(i_cop),"ECPEDIT","ECPDELETE")
        if (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
        ENDIF
       This.w_HASEVCOP=i_cop
       This.NotifyEvent("HasEvent")
       Return ( This.w_HASEVENT )
    Else
       * --- default
       Return(.t.)
    Endif
  EndFunc
  
  PROC ecpF6()
  * - RICORDA I VALORI DELLA RIGA CANCELLATA
     if inlist(this.cFunction,'Edit','Load')
         Local L_cTrsName
         L_cTrsName=This.cTrsname
         This.w_OLDTIPRIG = Nvl( &L_cTrsName..t_TIPCOD,'' )
         This.w_OLDROWNUM = Nvl( &L_cTrsName..CPROWNUM , 0 )
          if empty(nvl( &L_cTrsName..t_MADICRIF,''))
            dodefault()
           else
            AH_ERRORMSG("Impossibile eliminare. Riga generata da dichiarazione di MDO",'!',"")
          endif
     endif
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAT_MAST','gspc_mat')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_matPag1","gspc_mat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti di commessa")
      .Pages(1).HelpContextID = 7372551
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gspc_mat
     This.Parent.w_TIPMOV=RIGHT(ALLTRIM(pTipo),1)
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='ATTIVITA'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='MA_COSTI'
    this.cWorkTables[9]='TIPCOSTO'
    this.cWorkTables[10]='VOCIIVA'
    this.cWorkTables[11]='PC_FAMIG'
    this.cWorkTables[12]='VOC_COST'
    this.cWorkTables[13]='CPAR_DEF'
    this.cWorkTables[14]='MAT_MAST'
    this.cWorkTables[15]='MAT_DETT'
    * --- Area Manuale = Open Work Table
    * --- gspc_mat
    * colora le righe obsolete
         This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
         "rgb(nvl(t_red,0),nvl(t_green,0),nvl(t_blue,0))"
    
    
    * --- Fine Area Manuale
  return(this.OpenAllTables(15))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAT_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAT_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MASERIAL = NVL(MASERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_36_joined
    link_2_36_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MAT_MAST where MASERIAL=KeySet.MASERIAL
    *
    i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2],this.bLoadRecFilter,this.MAT_MAST_IDX,"gspc_mat")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAT_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAT_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MAT_DETT.","MAT_MAST.")
      i_cTable = i_cTable+' MAT_MAST '
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MASERIAL',this.w_MASERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_READPAR = 'TAM'
        .w_DEFCOM = space(15)
        .w_TIPATT = 'A'
        .w_VALCOM = space(3)
        .w_COMDES = space(30)
        .w_ATTDES = space(30)
        .w_LISDES = space(40)
        .w_SIMVAL = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_CENCOS = space(15)
        .w_DECTOT = 0
        .w_PIANIF = space(1)
        .w_CODFAM = space(5)
        .w_CAOCOM = 0
        .w_DECCOM = 0
        .w_CODVAL = space(3)
        .w_CAOVAL = 0
        .w_IVALIS = space(1)
        .w_HASEVCOP = space(10)
        .w_HASEVENT = TRUE
        .w_DECUNI = 0
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_TOTMOV = 0
        .w_TOTDOC = 0
        .w_MADATREG = NVL(cp_ToDate(MADATREG),ctod("  /  /  "))
        .w_MA__ANNO = NVL(MA__ANNO,space(4))
        .op_MA__ANNO = .w_MA__ANNO
        .w_MAALFREG = NVL(MAALFREG,0)
        .op_MAALFREG = .w_MAALFREG
        .w_MANUMREG = NVL(MANUMREG,0)
        .op_MANUMREG = .w_MANUMREG
          .link_1_5('Load')
        .w_MADATREG = NVL(cp_ToDate(MADATREG),ctod("  /  /  "))
        .w_MA__ANNO = CALCESER(.w_MADATREG, g_CODESE)
        .w_MASERIAL = NVL(MASERIAL,space(10))
        .op_MASERIAL = .w_MASERIAL
        .w_MATIPMOV = NVL(MATIPMOV,space(1))
        .w_MACODCOM = NVL(MACODCOM,space(15))
          if link_1_12_joined
            this.w_MACODCOM = NVL(CNCODCAN112,NVL(this.w_MACODCOM,space(15)))
            this.w_COMDES = NVL(CNDESCAN112,space(30))
            this.w_DTOBSO = NVL(cp_ToDate(CNDTOBSO112),ctod("  /  /  "))
            this.w_VALCOM = NVL(CNCODVAL112,space(3))
          else
          .link_1_12('Load')
          endif
        .w_MACODATT = NVL(MACODATT,space(15))
          .link_1_13('Load')
          .link_1_14('Load')
        .w_MACODVAL = NVL(MACODVAL,space(3))
          if link_1_15_joined
            this.w_MACODVAL = NVL(VACODVAL115,NVL(this.w_MACODVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL115,space(5))
            this.w_DECTOT = NVL(VADECTOT115,0)
            this.w_CAOVAL = NVL(VACAOVAL115,0)
            this.w_DECUNI = NVL(VADECUNI115,0)
          else
          .link_1_15('Load')
          endif
        .w_MACAOVAL = NVL(MACAOVAL,0)
        .w_MACODLIS = NVL(MACODLIS,space(5))
          .link_1_29('Load')
        .w_OBTEST = .w_MADATREG
        .w_CALPICTU = DEFPIP(.w_DECTOT)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .w_CALPICT = DEFPIC(.w_DECCOM)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .w_UMCAL = .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MAUNIMIS+.w_OPERAT+.w_OPERA3
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate(AH_MsgFormat("Totale in (%1):",.w_VALCOM))
        .oPgFrm.Page1.oPag.oObj_3_4.Calculate(AH_MsgFormat("Totale in (%1):",.w_MACODVAL) )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MAT_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MAT_DETT where MASERIAL=KeySet.MASERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MAT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MAT_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MAT_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MAT_DETT"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_36_joined=this.AddJoinedLink_2_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MASERIAL',this.w_MASERIAL  )
        select * from (i_cTable) MAT_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTMOV = 0
      this.w_TOTDOC = 0
      scan
        with this
          .w_CODIVA = space(5)
          .w_VOCCEN = space(15)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_CODCOS = space(5)
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_OPERA3 = space(1)
          .w_PERIVA = 0
          .w_DTOBS1 = ctod("  /  /  ")
          .w_LIPREZZO = 0
          .w_CODCOSDE = space(5)
          .w_TIPCOD = space(1)
          .w_FADESC = space(50)
        .w_TESTUM = .T.
          .w_FLSERG = space(1)
          .w_FLFRAZ = space(1)
          .w_NOFRAZ = space(1)
          .w_MODUM2 = space(1)
          .w_FLUSEP = space(1)
          .w_PREZUM = space(1)
          .w_CLUNIMIS = space(3)
          .w_QTALIS = 0
          .w_CPROWNUM = CPROWNUM
        .w_MACOTLIS = .w_MACODLIS
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MACODKEY = NVL(MACODKEY,space(20))
          if link_2_2_joined
            this.w_MACODKEY = NVL(CACODICE202,NVL(this.w_MACODKEY,space(20)))
            this.w_MADESCRI = NVL(CADESART202,space(40))
            this.w_MACODART = NVL(CACODART202,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
            this.w_TIPCOD = NVL(CA__TIPO202,space(1))
            this.w_MA__NOTE = NVL(CADESSUP202,space(0))
          else
          .link_2_2('Load')
          endif
          .w_MADESCRI = NVL(MADESCRI,space(40))
          .w_MACODART = NVL(MACODART,space(20))
          if link_2_4_joined
            this.w_MACODART = NVL(ARCODART204,NVL(this.w_MACODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_MOLTIP = NVL(ARMOLTIP204,0)
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_OPERAT = NVL(AROPERAT204,space(1))
            this.w_CODIVA = NVL(ARCODIVA204,space(5))
            this.w_VOCCEN = NVL(ARVOCCEN204,space(15))
            this.w_FLSERG = NVL(ARFLSERG204,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP204,space(1))
            this.w_PREZUM = NVL(ARPREZUM204,space(1))
          else
          .link_2_4('Load')
          endif
          .link_2_5('Load')
          .link_2_6('Load')
          .link_2_7('Load')
          .link_2_11('Load')
          .w_MAUNIMIS = NVL(MAUNIMIS,space(3))
          if link_2_16_joined
            this.w_MAUNIMIS = NVL(UMCODICE216,NVL(this.w_MAUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ216,space(1))
          else
          .link_2_16('Load')
          endif
          .w_MAQTAMOV = NVL(MAQTAMOV,0)
          .w_MAPREZZO = NVL(MAPREZZO,0)
          .w_MAOPPREV = NVL(MAOPPREV,space(1))
          .w_MACOATTS = NVL(MACOATTS,space(15))
          .w_MACOCOMS = NVL(MACOCOMS,space(15))
          .w_MATIPATT = NVL(MATIPATT,space(1))
          .w_MAOPCONS = NVL(MAOPCONS,space(1))
          .w_MA__NOTE = NVL(MA__NOTE,space(0))
          .w_MACODCOS = NVL(MACODCOS,space(5))
          * evitabile
          *.link_2_27('Load')
          .w_MAQTA1UM = NVL(MAQTA1UM,0)
          .w_MAVALRIG = NVL(MAVALRIG,0)
          .w_MADATANN = NVL(cp_ToDate(MADATANN),ctod("  /  /  "))
        .w_TOTRIGA = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0 , cp_Round(  .w_MAPREZZO*.w_MAQTAMOV , .w_DECTOT))
          .w_MACODFAM = NVL(MACODFAM,space(5))
          if link_2_36_joined
            this.w_MACODFAM = NVL(FACODICE236,NVL(this.w_MACODFAM,space(5)))
            this.w_FADESC = NVL(FADESCRI236,space(50))
          else
          .link_2_36('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .w_ROW = .w_CPROWORD
        .w_RED = IIF(EMPTY(.w_MADATANN), 255, 200)
        .w_GREEN = iif(empty(.w_MADATANN), 255, 63)
        .w_BLUE = IIF(EMPTY(.w_MADATANN), 255, 63)
          .w_MADICRIF = NVL(MADICRIF,space(15))
          select (this.cTrsName)
          append blank
          replace MAOPPREV with .w_MAOPPREV
          replace MACOATTS with .w_MACOATTS
          replace MACOCOMS with .w_MACOCOMS
          replace MATIPATT with .w_MATIPATT
          replace MAOPCONS with .w_MAOPCONS
          replace MACODCOS with .w_MACODCOS
          replace MAVALRIG with .w_MAVALRIG
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTMOV = .w_TOTMOV+.w_TOTRIGA
          .w_TOTDOC = .w_TOTDOC+.w_MAVALRIG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_MA__ANNO = CALCESER(.w_MADATREG, g_CODESE)
        .w_OBTEST = .w_MADATREG
        .w_CALPICTU = DEFPIP(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .w_CALPICT = DEFPIC(.w_DECCOM)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_MACOTLIS = .w_MACODLIS
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate(AH_MsgFormat("Totale in (%1):",.w_VALCOM))
        .oPgFrm.Page1.oPag.oObj_3_4.Calculate(AH_MsgFormat("Totale in (%1):",.w_MACODVAL) )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_mat
    * Per riproporre in caricamento i dati appena immessi
    If This.cFunction='Load'
       This.w_CODLIS=This.w_MACODLIS
       This.w_CODCOM=This.w_MACODCOM
    EndIf
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MADATREG=ctod("  /  /  ")
      .w_MA__ANNO=space(4)
      .w_MAALFREG=0
      .w_MANUMREG=0
      .w_READPAR=space(10)
      .w_MADATREG=ctod("  /  /  ")
      .w_DEFCOM=space(15)
      .w_TIPATT=space(1)
      .w_MA__ANNO=space(4)
      .w_MASERIAL=space(10)
      .w_MATIPMOV=space(1)
      .w_MACODCOM=space(15)
      .w_MACODATT=space(15)
      .w_VALCOM=space(3)
      .w_MACODVAL=space(3)
      .w_COMDES=space(30)
      .w_ATTDES=space(30)
      .w_LISDES=space(40)
      .w_SIMVAL=space(5)
      .w_MACAOVAL=0
      .w_MACODLIS=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_CENCOS=space(15)
      .w_DECTOT=0
      .w_CALPICTU=0
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_PIANIF=space(1)
      .w_CODFAM=space(5)
      .w_CAOCOM=0
      .w_DECCOM=0
      .w_CALPICT=0
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_IVALIS=space(1)
      .w_HASEVCOP=space(10)
      .w_HASEVENT=.f.
      .w_DECUNI=0
      .w_CALCPICU=0
      .w_MACOTLIS=space(5)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_CPROWORD=10
      .w_MACODKEY=space(20)
      .w_MADESCRI=space(40)
      .w_MACODART=space(20)
      .w_CODIVA=space(5)
      .w_VOCCEN=space(15)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_CODCOS=space(5)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_PERIVA=0
      .w_MAUNIMIS=space(3)
      .w_MAQTAMOV=0
      .w_MAPREZZO=0
      .w_UMCAL=space(10)
      .w_DTOBS1=ctod("  /  /  ")
      .w_MAOPPREV=space(1)
      .w_MACOATTS=space(15)
      .w_MACOCOMS=space(15)
      .w_MATIPATT=space(1)
      .w_MAOPCONS=space(1)
      .w_MA__NOTE=space(0)
      .w_MACODCOS=space(5)
      .w_MAQTA1UM=0
      .w_MAVALRIG=0
      .w_LIPREZZO=0
      .w_CODCOSDE=space(5)
      .w_MADATANN=ctod("  /  /  ")
      .w_TOTRIGA=0
      .w_TIPCOD=space(1)
      .w_MACODFAM=space(5)
      .w_FADESC=space(50)
      .w_TESTUM=.f.
      .w_ROW=0
      .w_RED=0
      .w_GREEN=0
      .w_BLUE=0
      .w_FLSERG=space(1)
      .w_MADICRIF=space(15)
      .w_FLFRAZ=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(3)
      .w_QTALIS=0
      .w_TOTMOV=0
      .w_TOTDOC=0
      if .cFunction<>"Filter"
        .w_MADATREG = i_DATSYS
        .w_MA__ANNO = alltrim(str(YEAR(.w_MADATREG)))
        .w_MAALFREG = IIF(.cFunction="Load", IIF(g_MAGUTE="S", 0, i_CODUTE), .w_MAALFREG)
        .DoRTCalc(4,4,.f.)
        .w_READPAR = 'TAM'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READPAR))
         .link_1_5('Full')
        endif
        .w_MADATREG = i_DATSYS
        .DoRTCalc(7,7,.f.)
        .w_TIPATT = 'A'
        .w_MA__ANNO = CALCESER(.w_MADATREG, g_CODESE)
        .DoRTCalc(10,10,.f.)
        .w_MATIPMOV = 'C'
        .w_MACODCOM = .w_DEFCOM
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MACODCOM))
         .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MACODATT))
         .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_VALCOM))
         .link_1_14('Full')
        endif
        .w_MACODVAL = .w_VALCOM
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MACODVAL))
         .link_1_15('Full')
        endif
        .DoRTCalc(16,19,.f.)
        .w_MACAOVAL = GETCAM(.w_MACODVAL, .w_MADATREG, 7)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_MACODLIS))
         .link_1_29('Full')
        endif
        .w_OBTEST = .w_MADATREG
        .DoRTCalc(23,25,.f.)
        .w_CALPICTU = DEFPIP(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(27,34,.f.)
        .w_CALPICT = DEFPIC(.w_DECCOM)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(36,39,.f.)
        .w_HASEVENT = TRUE
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(41,41,.f.)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .w_MACOTLIS = .w_MACODLIS
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .DoRTCalc(44,47,.f.)
        if not(empty(.w_MACODKEY))
         .link_2_2('Full')
        endif
        .DoRTCalc(48,49,.f.)
        if not(empty(.w_MACODART))
         .link_2_4('Full')
        endif
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_CODIVA))
         .link_2_5('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_VOCCEN))
         .link_2_6('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_7('Full')
        endif
        .DoRTCalc(53,56,.f.)
        if not(empty(.w_CODCOS))
         .link_2_11('Full')
        endif
        .DoRTCalc(57,60,.f.)
        .w_MAUNIMIS = .w_UNMIS1
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_MAUNIMIS))
         .link_2_16('Full')
        endif
        .w_MAQTAMOV = IIF(.w_TIPCOD='F', 1, .w_MAQTAMOV)
        .w_MAPREZZO = .w_LIPREZZO
        .w_UMCAL = .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MAUNIMIS+.w_OPERAT+.w_OPERA3
        .DoRTCalc(65,65,.f.)
        .w_MAOPPREV = IIF(.w_MATIPMOV='P' and .w_MAPREZZO<>0 ,'+',' ')
        .w_MACOATTS = .w_MACODATT
        .w_MACOCOMS = .w_MACODCOM
        .w_MATIPATT = 'A'
        .w_MAOPCONS = IIF(.w_MATIPMOV='C' and .w_MAPREZZO<>0,'+',' ')
        .DoRTCalc(71,71,.f.)
        .w_MACODCOS = .w_CODCOS
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_MACODCOS))
         .link_2_27('Full')
        endif
        .w_MAQTA1UM = IIF(.w_TIPCOD='F', 1, CALQTAADV(.w_MAQTAMOV,.w_MAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MAQTAMOV"))
        .w_MAVALRIG = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0,VAL2VAL(.w_MAPREZZO*.w_MAQTAMOV,.w_MACAOVAL,.w_MADATREG,.w_MADATREG,GETCAM(.w_VALCOM,.w_MADATREG),.w_DECCOM))
        .DoRTCalc(75,77,.f.)
        .w_TOTRIGA = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0 , cp_Round(  .w_MAPREZZO*.w_MAQTAMOV , .w_DECTOT))
        .DoRTCalc(79,79,.f.)
        .w_MACODFAM = .w_CODFAM
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_MACODFAM))
         .link_2_36('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .DoRTCalc(81,81,.f.)
        .w_TESTUM = .T.
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .w_ROW = .w_CPROWORD
        .w_RED = IIF(EMPTY(.w_MADATANN), 255, 200)
        .w_GREEN = iif(empty(.w_MADATANN), 255, 63)
        .w_BLUE = IIF(EMPTY(.w_MADATANN), 255, 63)
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate(AH_MsgFormat("Totale in (%1):",.w_VALCOM))
        .oPgFrm.Page1.oPag.oObj_3_4.Calculate(AH_MsgFormat("Totale in (%1):",.w_MACODVAL) )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAT_MAST')
    this.DoRTCalc(87,97,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gspc_mat
    
    If This.cFunction='Load'
       * Per riproporre in caricamento i dati appena immessi
       This.w_MACODLIS=iif ( Empty(This.w_CODLIS),This.w_MACODLIS,This.w_CODLIS)
       * leggo la descrizione del listino
       Obj_Lis=This.GetcTRL('w_MACODLIS')
       Obj_Lis.Check
       This.w_MACODCOM=iif ( Empty(This.w_CODCOM),This.w_MACODCOM,This.w_CODCOM)
       * rieseguo il link sulla commessa (per riproporre la valuta corretta) - (P)
       Obj_Lis=This.GetcTRL('w_MACODCOM')
       Obj_Lis.Check
       * per evitare inconsistenze tra testata e righe - (P)
       This.w_MACOCOMS=This.w_MACODCOM
       * Ricalcolo i dati correlati
       this.mcalc(.t.)
    EndIf
    
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMANUMREG_1_4.enabled = i_bVal
      .Page1.oPag.oMADATREG_1_6.enabled = i_bVal
      .Page1.oPag.oMATIPMOV_1_11.enabled = i_bVal
      .Page1.oPag.oMACODCOM_1_12.enabled = i_bVal
      .Page1.oPag.oMACODATT_1_13.enabled = i_bVal
      .Page1.oPag.oMACODVAL_1_15.enabled = i_bVal
      .Page1.oPag.oMACAOVAL_1_28.enabled = i_bVal
      .Page1.oPag.oMACODLIS_1_29.enabled = i_bVal
      .Page1.oPag.oMADATANN_2_32.enabled = i_bVal
      .Page1.oPag.oMACODFAM_2_36.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_2_40.enabled = i_bVal
      .Page1.oPag.oObj_2_42.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMANUMREG_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MAT_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMAT","i_CODAZI,w_MASERIAL")
      cp_AskTableProg(this,i_nConn,"NUMAT","i_CODAZI,w_MA__ANNO,w_MAALFREG,w_MANUMREG")
      .op_CODAZI = .w_CODAZI
      .op_MASERIAL = .w_MASERIAL
      .op_CODAZI = .w_CODAZI
      .op_MA__ANNO = .w_MA__ANNO
      .op_MAALFREG = .w_MAALFREG
      .op_MANUMREG = .w_MANUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATREG,"MADATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MA__ANNO,"MA__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MAALFREG,"MAALFREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MANUMREG,"MANUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MADATREG,"MADATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MASERIAL,"MASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MATIPMOV,"MATIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODCOM,"MACODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODATT,"MACODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODVAL,"MACODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACAOVAL,"MACAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MACODLIS,"MACODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
    i_lTable = "MAT_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAT_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSPC_BSH with this
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_MACODKEY C(20);
      ,t_MADESCRI C(40);
      ,t_MACODART C(20);
      ,t_MAUNIMIS C(3);
      ,t_MAQTAMOV N(12,3);
      ,t_MAPREZZO N(18,5);
      ,t_MAVALRIG N(18,4);
      ,t_CODCOSDE C(5);
      ,t_MADATANN D(8);
      ,t_MACODFAM C(5);
      ,t_FADESC C(50);
      ,t_ROW N(5);
      ,t_MADICRIF C(15);
      ,MAOPPREV C(1);
      ,MACOATTS C(15);
      ,MACOCOMS C(15);
      ,MATIPATT C(1);
      ,MAOPCONS C(1);
      ,MACODCOS C(5);
      ,MAVALRIG N(18,4);
      ,CPROWNUM N(10);
      ,t_MACOTLIS C(5);
      ,t_CODIVA C(5);
      ,t_VOCCEN C(15);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_MOLTIP N(10,4);
      ,t_OPERAT C(1);
      ,t_CODCOS C(5);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_OPERA3 C(1);
      ,t_PERIVA N(5,2);
      ,t_DTOBS1 D(8);
      ,t_MAOPPREV C(1);
      ,t_MACOATTS C(15);
      ,t_MACOCOMS C(15);
      ,t_MATIPATT C(1);
      ,t_MAOPCONS C(1);
      ,t_MA__NOTE M(10);
      ,t_MACODCOS C(5);
      ,t_MAQTA1UM N(12,3);
      ,t_LIPREZZO N(18,4);
      ,t_TOTRIGA N(18,4);
      ,t_TIPCOD C(1);
      ,t_TESTUM L(1);
      ,t_RED N(3);
      ,t_GREEN N(3);
      ,t_BLUE N(3);
      ,t_FLSERG C(1);
      ,t_FLFRAZ C(1);
      ,t_NOFRAZ C(1);
      ,t_MODUM2 C(1);
      ,t_FLUSEP C(1);
      ,t_PREZUM C(1);
      ,t_CLUNIMIS C(3);
      ,t_QTALIS N(12,3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgspc_matbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMACODKEY_2_2.controlsource=this.cTrsName+'.t_MACODKEY'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMADESCRI_2_3.controlsource=this.cTrsName+'.t_MADESCRI'
    this.oPgFRm.Page1.oPag.oMACODART_2_4.controlsource=this.cTrsName+'.t_MACODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMAUNIMIS_2_16.controlsource=this.cTrsName+'.t_MAUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMAQTAMOV_2_17.controlsource=this.cTrsName+'.t_MAQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMAPREZZO_2_18.controlsource=this.cTrsName+'.t_MAPREZZO'
    this.oPgFRm.Page1.oPag.oMAVALRIG_2_29.controlsource=this.cTrsName+'.t_MAVALRIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCODCOSDE_2_31.controlsource=this.cTrsName+'.t_CODCOSDE'
    this.oPgFRm.Page1.oPag.oMADATANN_2_32.controlsource=this.cTrsName+'.t_MADATANN'
    this.oPgFRm.Page1.oPag.oMACODFAM_2_36.controlsource=this.cTrsName+'.t_MACODFAM'
    this.oPgFRm.Page1.oPag.oFADESC_2_37.controlsource=this.cTrsName+'.t_FADESC'
    this.oPgFRm.Page1.oPag.oROW_2_43.controlsource=this.cTrsName+'.t_ROW'
    this.oPgFRm.Page1.oPag.oMADICRIF_2_49.controlsource=this.cTrsName+'.t_MADICRIF'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(42)
    this.AddVLine(199)
    this.AddVLine(436)
    this.AddVLine(478)
    this.AddVLine(514)
    this.AddVLine(605)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEMAT","i_CODAZI,w_MASERIAL")
          cp_NextTableProg(this,i_nConn,"NUMAT","i_CODAZI,w_MA__ANNO,w_MAALFREG,w_MANUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MAT_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAT_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'MAT_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(MADATREG,MA__ANNO,MAALFREG,MANUMREG,MASERIAL"+;
                  ",MATIPMOV,MACODCOM,MACODATT,MACODVAL,MACAOVAL"+;
                  ",MACODLIS,UTCC,UTCV,UTDC,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MADATREG)+;
                    ","+cp_ToStrODBC(this.w_MA__ANNO)+;
                    ","+cp_ToStrODBC(this.w_MAALFREG)+;
                    ","+cp_ToStrODBC(this.w_MANUMREG)+;
                    ","+cp_ToStrODBC(this.w_MASERIAL)+;
                    ","+cp_ToStrODBC(this.w_MATIPMOV)+;
                    ","+cp_ToStrODBCNull(this.w_MACODCOM)+;
                    ","+cp_ToStrODBCNull(this.w_MACODATT)+;
                    ","+cp_ToStrODBCNull(this.w_MACODVAL)+;
                    ","+cp_ToStrODBC(this.w_MACAOVAL)+;
                    ","+cp_ToStrODBCNull(this.w_MACODLIS)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAT_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'MAT_MAST')
        cp_CheckDeletedKey(i_cTable,0,'MASERIAL',this.w_MASERIAL)
        INSERT INTO (i_cTable);
              (MADATREG,MA__ANNO,MAALFREG,MANUMREG,MASERIAL,MATIPMOV,MACODCOM,MACODATT,MACODVAL,MACAOVAL,MACODLIS,UTCC,UTCV,UTDC,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MADATREG;
                  ,this.w_MA__ANNO;
                  ,this.w_MAALFREG;
                  ,this.w_MANUMREG;
                  ,this.w_MASERIAL;
                  ,this.w_MATIPMOV;
                  ,this.w_MACODCOM;
                  ,this.w_MACODATT;
                  ,this.w_MACODVAL;
                  ,this.w_MACAOVAL;
                  ,this.w_MACODLIS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_DETT_IDX,2])
      *
      * insert into MAT_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MASERIAL,CPROWORD,MACODKEY,MADESCRI,MACODART"+;
                  ",MAUNIMIS,MAQTAMOV,MAPREZZO,MAOPPREV,MACOATTS"+;
                  ",MACOCOMS,MATIPATT,MAOPCONS,MA__NOTE,MACODCOS"+;
                  ",MAQTA1UM,MAVALRIG,MADATANN,MACODFAM,MADICRIF,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MASERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MACODKEY)+","+cp_ToStrODBC(this.w_MADESCRI)+","+cp_ToStrODBCNull(this.w_MACODART)+;
             ","+cp_ToStrODBCNull(this.w_MAUNIMIS)+","+cp_ToStrODBC(this.w_MAQTAMOV)+","+cp_ToStrODBC(this.w_MAPREZZO)+","+cp_ToStrODBC(this.w_MAOPPREV)+","+cp_ToStrODBC(this.w_MACOATTS)+;
             ","+cp_ToStrODBC(this.w_MACOCOMS)+","+cp_ToStrODBC(this.w_MATIPATT)+","+cp_ToStrODBC(this.w_MAOPCONS)+","+cp_ToStrODBC(this.w_MA__NOTE)+","+cp_ToStrODBCNull(this.w_MACODCOS)+;
             ","+cp_ToStrODBC(this.w_MAQTA1UM)+","+cp_ToStrODBC(this.w_MAVALRIG)+","+cp_ToStrODBC(this.w_MADATANN)+","+cp_ToStrODBCNull(this.w_MACODFAM)+","+cp_ToStrODBC(this.w_MADICRIF)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MASERIAL',this.w_MASERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MASERIAL,this.w_CPROWORD,this.w_MACODKEY,this.w_MADESCRI,this.w_MACODART"+;
                ",this.w_MAUNIMIS,this.w_MAQTAMOV,this.w_MAPREZZO,this.w_MAOPPREV,this.w_MACOATTS"+;
                ",this.w_MACOCOMS,this.w_MATIPATT,this.w_MAOPCONS,this.w_MA__NOTE,this.w_MACODCOS"+;
                ",this.w_MAQTA1UM,this.w_MAVALRIG,this.w_MADATANN,this.w_MACODFAM,this.w_MADICRIF,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gspc_mat
    this.bHeaderUpdated = .t.
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MAT_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MAT_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MADATREG="+cp_ToStrODBC(this.w_MADATREG)+;
             ",MA__ANNO="+cp_ToStrODBC(this.w_MA__ANNO)+;
             ",MAALFREG="+cp_ToStrODBC(this.w_MAALFREG)+;
             ",MANUMREG="+cp_ToStrODBC(this.w_MANUMREG)+;
             ",MATIPMOV="+cp_ToStrODBC(this.w_MATIPMOV)+;
             ",MACODCOM="+cp_ToStrODBCNull(this.w_MACODCOM)+;
             ",MACODATT="+cp_ToStrODBCNull(this.w_MACODATT)+;
             ",MACODVAL="+cp_ToStrODBCNull(this.w_MACODVAL)+;
             ",MACAOVAL="+cp_ToStrODBC(this.w_MACAOVAL)+;
             ",MACODLIS="+cp_ToStrODBCNull(this.w_MACODLIS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MAT_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'MASERIAL',this.w_MASERIAL  )
          UPDATE (i_cTable) SET;
              MADATREG=this.w_MADATREG;
             ,MA__ANNO=this.w_MA__ANNO;
             ,MAALFREG=this.w_MAALFREG;
             ,MANUMREG=this.w_MANUMREG;
             ,MATIPMOV=this.w_MATIPMOV;
             ,MACODCOM=this.w_MACODCOM;
             ,MACODATT=this.w_MACODATT;
             ,MACODVAL=this.w_MACODVAL;
             ,MACAOVAL=this.w_MACAOVAL;
             ,MACODLIS=this.w_MACODLIS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 And Not Empty( t_MACODKEY )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MAT_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MAT_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MAT_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAT_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MACODKEY="+cp_ToStrODBCNull(this.w_MACODKEY)+;
                     ",MADESCRI="+cp_ToStrODBC(this.w_MADESCRI)+;
                     ",MACODART="+cp_ToStrODBCNull(this.w_MACODART)+;
                     ",MAUNIMIS="+cp_ToStrODBCNull(this.w_MAUNIMIS)+;
                     ",MAQTAMOV="+cp_ToStrODBC(this.w_MAQTAMOV)+;
                     ",MAPREZZO="+cp_ToStrODBC(this.w_MAPREZZO)+;
                     ",MAOPPREV="+cp_ToStrODBC(this.w_MAOPPREV)+;
                     ",MACOATTS="+cp_ToStrODBC(this.w_MACOATTS)+;
                     ",MACOCOMS="+cp_ToStrODBC(this.w_MACOCOMS)+;
                     ",MATIPATT="+cp_ToStrODBC(this.w_MATIPATT)+;
                     ",MAOPCONS="+cp_ToStrODBC(this.w_MAOPCONS)+;
                     ",MA__NOTE="+cp_ToStrODBC(this.w_MA__NOTE)+;
                     ",MACODCOS="+cp_ToStrODBCNull(this.w_MACODCOS)+;
                     ",MAQTA1UM="+cp_ToStrODBC(this.w_MAQTA1UM)+;
                     ",MAVALRIG="+cp_ToStrODBC(this.w_MAVALRIG)+;
                     ",MADATANN="+cp_ToStrODBC(this.w_MADATANN)+;
                     ",MACODFAM="+cp_ToStrODBCNull(this.w_MACODFAM)+;
                     ",MADICRIF="+cp_ToStrODBC(this.w_MADICRIF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MACODKEY=this.w_MACODKEY;
                     ,MADESCRI=this.w_MADESCRI;
                     ,MACODART=this.w_MACODART;
                     ,MAUNIMIS=this.w_MAUNIMIS;
                     ,MAQTAMOV=this.w_MAQTAMOV;
                     ,MAPREZZO=this.w_MAPREZZO;
                     ,MAOPPREV=this.w_MAOPPREV;
                     ,MACOATTS=this.w_MACOATTS;
                     ,MACOCOMS=this.w_MACOCOMS;
                     ,MATIPATT=this.w_MATIPATT;
                     ,MAOPCONS=this.w_MAOPCONS;
                     ,MA__NOTE=this.w_MA__NOTE;
                     ,MACODCOS=this.w_MACODCOS;
                     ,MAQTA1UM=this.w_MAQTA1UM;
                     ,MAVALRIG=this.w_MAVALRIG;
                     ,MADATANN=this.w_MADATANN;
                     ,MACODFAM=this.w_MACODFAM;
                     ,MADICRIF=this.w_MADICRIF;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MAOPPREV,space(1))==this.w_MAOPPREV;
              and NVL(&i_cF..MAVALRIG,0)==this.w_MAVALRIG;
              and NVL(&i_cF..MAOPCONS,space(1))==this.w_MAOPCONS;
              and NVL(&i_cF..MAVALRIG,0)==this.w_MAVALRIG;
              and NVL(&i_cF..MACODCOS,space(5))==this.w_MACODCOS;
              and NVL(&i_cF..MACOCOMS,space(15))==this.w_MACOCOMS;
              and NVL(&i_cF..MATIPATT,space(1))==this.w_MATIPATT;
              and NVL(&i_cF..MACOATTS,space(15))==this.w_MACOATTS;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MAOPPREV,space(1)),'CSPREVEN','',NVL(&i_cF..MAVALRIG,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MAOPCONS,space(1)),'CSCONSUN','',NVL(&i_cF..MAVALRIG,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MACODCOS,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" CSPREVEN="+i_cOp1+","           +" CSCONSUN="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CSCODCOS="+cp_ToStrODBC(NVL(&i_cF..MACODCOS,space(5)));
             +" AND CSCODCOM="+cp_ToStrODBC(NVL(&i_cF..MACOCOMS,space(15)));
             +" AND CSTIPSTR="+cp_ToStrODBC(NVL(&i_cF..MATIPATT,space(1)));
             +" AND CSCODMAT="+cp_ToStrODBC(NVL(&i_cF..MACOATTS,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MAOPPREV,'CSPREVEN',i_cF+'.MAVALRIG',&i_cF..MAVALRIG,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MAOPCONS,'CSCONSUN',i_cF+'.MAVALRIG',&i_cF..MAVALRIG,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CSCODCOM',&i_cF..MACOCOMS;
                 ,'CSTIPSTR',&i_cF..MATIPATT;
                 ,'CSCODMAT',&i_cF..MACOATTS;
                 ,'CSCODCOS',&i_cF..MACODCOS)
      UPDATE (i_cTable) SET ;
           CSPREVEN=&i_cOp1.  ,;
           CSCONSUN=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MAOPPREV,space(1))==this.w_MAOPPREV;
              and NVL(&i_cF..MAVALRIG,0)==this.w_MAVALRIG;
              and NVL(&i_cF..MAOPCONS,space(1))==this.w_MAOPCONS;
              and NVL(&i_cF..MAVALRIG,0)==this.w_MAVALRIG;
              and NVL(&i_cF..MACODCOS,space(5))==this.w_MACODCOS;
              and NVL(&i_cF..MACOCOMS,space(15))==this.w_MACOCOMS;
              and NVL(&i_cF..MATIPATT,space(1))==this.w_MATIPATT;
              and NVL(&i_cF..MACOATTS,space(15))==this.w_MACOATTS;

      i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MACODCOS)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" CSPREVEN="+i_cOp1  +",";
         +" CSCONSUN="+i_cOp2  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CSCODCOS="+cp_ToStrODBC(this.w_MACODCOS);
           +" AND CSCODCOM="+cp_ToStrODBC(this.w_MACOCOMS);
           +" AND CSTIPSTR="+cp_ToStrODBC(this.w_MATIPATT);
           +" AND CSCODMAT="+cp_ToStrODBC(this.w_MACOATTS);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MACODCOS)
        i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (CSCODCOS,CSCODCOM,CSTIPSTR,CSCODMAT  ;
             ,CSPREVEN"+",CSCONSUN,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MACODCOS)+","+cp_ToStrODBC(this.w_MACOCOMS)+","+cp_ToStrODBC(this.w_MATIPATT)+","+cp_ToStrODBC(this.w_MACOATTS)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CSCODCOM',this.w_MACOCOMS;
                 ,'CSTIPSTR',this.w_MATIPATT;
                 ,'CSCODMAT',this.w_MACOATTS;
                 ,'CSCODCOS',this.w_MACODCOS)
      UPDATE (i_cTable) SET;
           CSPREVEN=&i_cOp1.  ,;
           CSCONSUN=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MACODCOS)
        i_cOp1=cp_SetTrsOp(this.w_MAOPPREV,'CSPREVEN','this.w_MAVALRIG',this.w_MAVALRIG,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MAOPCONS,'CSCONSUN','this.w_MAVALRIG',this.w_MAVALRIG,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'CSCODCOM',this.w_MACOCOMS,'CSTIPSTR',this.w_MATIPATT,'CSCODMAT',this.w_MACOATTS,'CSCODCOS',this.w_MACODCOS)
        INSERT INTO (i_cTable) (CSCODCOS,CSCODCOM,CSTIPSTR,CSCODMAT  ;
         ,CSPREVEN,CSCONSUN,CPCCCHK) VALUES (this.w_MACODCOS,this.w_MACOCOMS,this.w_MATIPATT,this.w_MACOATTS  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 And Not Empty( t_MACODKEY )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MAT_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAT_DETT_IDX,2])
        *
        * delete MAT_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
        *
        * delete MAT_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 And Not Empty( t_MACODKEY )) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAT_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_MA__ANNO = alltrim(str(YEAR(.w_MADATREG)))
          .w_MAALFREG = IIF(.cFunction="Load", IIF(g_MAGUTE="S", 0, i_CODUTE), .w_MAALFREG)
        .DoRTCalc(4,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,8,.t.)
          .w_MA__ANNO = CALCESER(.w_MADATREG, g_CODESE)
        .DoRTCalc(10,12,.t.)
        if .o_MACODCOM<>.w_MACODCOM
          .link_1_13('Full')
        endif
          .link_1_14('Full')
        if .o_VALCOM<>.w_VALCOM
          .w_MACODVAL = .w_VALCOM
          .link_1_15('Full')
        endif
        .DoRTCalc(16,19,.t.)
        if .o_MACODVAL<>.w_MACODVAL.or. .o_MADATREG<>.w_MADATREG
          .w_MACAOVAL = GETCAM(.w_MACODVAL, .w_MADATREG, 7)
        endif
        if .o_MACODCOM<>.w_MACODCOM
          .link_1_29('Full')
        endif
        if .o_MADATREG<>.w_MADATREG
          .w_OBTEST = .w_MADATREG
        endif
        .DoRTCalc(23,25,.t.)
          .w_CALPICTU = DEFPIP(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(27,34,.t.)
          .w_CALPICT = DEFPIC(.w_DECCOM)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(36,41,.t.)
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        if .o_MACODKEY<>.w_MACODKEY
          .w_MACOTLIS = .w_MACODLIS
        endif
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .DoRTCalc(44,48,.t.)
          .link_2_4('Full')
          .link_2_5('Full')
        if .o_MACODART<>.w_MACODART
          .link_2_6('Full')
        endif
          .link_2_7('Full')
        .DoRTCalc(53,55,.t.)
          .link_2_11('Full')
        .DoRTCalc(57,60,.t.)
        if .o_MACODART<>.w_MACODART
          .w_MAUNIMIS = .w_UNMIS1
          .link_2_16('Full')
        endif
        if .o_MACODKEY<>.w_MACODKEY
          .w_MAQTAMOV = IIF(.w_TIPCOD='F', 1, .w_MAQTAMOV)
        endif
        if .o_MACODART<>.w_MACODART.or. .o_MAUNIMIS<>.w_MAUNIMIS.or. .o_LIPREZZO<>.w_LIPREZZO
          .w_MAPREZZO = .w_LIPREZZO
        endif
          .w_UMCAL = .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MAUNIMIS+.w_OPERAT+.w_OPERA3
        .DoRTCalc(65,65,.t.)
        if .o_MATIPMOV<>.w_MATIPMOV.or. .o_MAPREZZO<>.w_MAPREZZO
          .w_MAOPPREV = IIF(.w_MATIPMOV='P' and .w_MAPREZZO<>0 ,'+',' ')
        endif
        if .o_MACODATT<>.w_MACODATT
          .w_MACOATTS = .w_MACODATT
        endif
        if .o_MACODCOM<>.w_MACODCOM
          .w_MACOCOMS = .w_MACODCOM
        endif
        .DoRTCalc(69,69,.t.)
        if .o_MATIPMOV<>.w_MATIPMOV.or. .o_MAPREZZO<>.w_MAPREZZO
          .w_MAOPCONS = IIF(.w_MATIPMOV='C' and .w_MAPREZZO<>0,'+',' ')
        endif
        .DoRTCalc(71,71,.t.)
        if .o_CODCOS<>.w_CODCOS
          .w_MACODCOS = .w_CODCOS
          .link_2_27('Full')
        endif
        if .o_MAQTAMOV<>.w_MAQTAMOV.or. .o_MAUNIMIS<>.w_MAUNIMIS.or. .o_MACODKEY<>.w_MACODKEY
          .w_MAQTA1UM = IIF(.w_TIPCOD='F', 1, CALQTAADV(.w_MAQTAMOV,.w_MAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MAQTAMOV"))
        endif
          .w_TOTDOC = .w_TOTDOC-.w_mavalrig
          .w_MAVALRIG = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0,VAL2VAL(.w_MAPREZZO*.w_MAQTAMOV,.w_MACAOVAL,.w_MADATREG,.w_MADATREG,GETCAM(.w_VALCOM,.w_MADATREG),.w_DECCOM))
          .w_TOTDOC = .w_TOTDOC+.w_mavalrig
        .DoRTCalc(75,77,.t.)
          .w_TOTMOV = .w_TOTMOV-.w_totriga
          .w_TOTRIGA = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0 , cp_Round(  .w_MAPREZZO*.w_MAQTAMOV , .w_DECTOT))
          .w_TOTMOV = .w_TOTMOV+.w_totriga
        .DoRTCalc(79,79,.t.)
        if .o_CODFAM<>.w_CODFAM
          .w_MACODFAM = .w_CODFAM
          .link_2_36('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .DoRTCalc(81,82,.t.)
          .w_ROW = .w_CPROWORD
          .w_RED = IIF(EMPTY(.w_MADATANN), 255, 200)
          .w_GREEN = iif(empty(.w_MADATANN), 255, 63)
          .w_BLUE = IIF(EMPTY(.w_MADATANN), 255, 63)
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate(AH_MsgFormat("Totale in (%1):",.w_VALCOM))
        .oPgFrm.Page1.oPag.oObj_3_4.Calculate(AH_MsgFormat("Totale in (%1):",.w_MACODVAL) )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEMAT","i_CODAZI,w_MASERIAL")
          .op_MASERIAL = .w_MASERIAL
        endif
        if .op_CODAZI<>.w_CODAZI .or. .op_MA__ANNO<>.w_MA__ANNO .or. .op_MAALFREG<>.w_MAALFREG
           cp_AskTableProg(this,i_nConn,"NUMAT","i_CODAZI,w_MA__ANNO,w_MAALFREG,w_MANUMREG")
          .op_MANUMREG = .w_MANUMREG
        endif
        .op_CODAZI = .w_CODAZI
        .op_CODAZI = .w_CODAZI
        .op_MA__ANNO = .w_MA__ANNO
        .op_MAALFREG = .w_MAALFREG
      endwith
      this.DoRTCalc(87,97,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MACOTLIS with this.w_MACOTLIS
      replace t_CODIVA with this.w_CODIVA
      replace t_VOCCEN with this.w_VOCCEN
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_CODCOS with this.w_CODCOS
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_OPERA3 with this.w_OPERA3
      replace t_PERIVA with this.w_PERIVA
      replace t_DTOBS1 with this.w_DTOBS1
      replace t_MAOPPREV with this.w_MAOPPREV
      replace t_MACOATTS with this.w_MACOATTS
      replace t_MACOCOMS with this.w_MACOCOMS
      replace t_MATIPATT with this.w_MATIPATT
      replace t_MAOPCONS with this.w_MAOPCONS
      replace t_MA__NOTE with this.w_MA__NOTE
      replace t_MACODCOS with this.w_MACODCOS
      replace t_MAQTA1UM with this.w_MAQTA1UM
      replace t_LIPREZZO with this.w_LIPREZZO
      replace t_TOTRIGA with this.w_TOTRIGA
      replace t_TIPCOD with this.w_TIPCOD
      replace t_TESTUM with this.w_TESTUM
      replace t_RED with this.w_RED
      replace t_GREEN with this.w_GREEN
      replace t_BLUE with this.w_BLUE
      replace t_FLSERG with this.w_FLSERG
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_NOFRAZ with this.w_NOFRAZ
      replace t_MODUM2 with this.w_MODUM2
      replace t_FLUSEP with this.w_FLUSEP
      replace t_PREZUM with this.w_PREZUM
      replace t_CLUNIMIS with this.w_CLUNIMIS
      replace t_QTALIS with this.w_QTALIS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate(AH_MsgFormat("Totale in (%1):",.w_VALCOM))
        .oPgFrm.Page1.oPag.oObj_3_4.Calculate(AH_MsgFormat("Totale in (%1):",.w_MACODVAL) )
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
    endwith
  return
  proc Calculate_FPVOFJUDHY()
    with this
          * --- CAMBIA COMMESSA O ATTIVITA' 
          GSPC_BMK(this;
              ,'CHGCOMM';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMATIPMOV_1_11.enabled = this.oPgFrm.Page1.oPag.oMATIPMOV_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMACODCOM_1_12.enabled = this.oPgFrm.Page1.oPag.oMACODCOM_1_12.mCond()
    this.oPgFrm.Page1.oPag.oMACODATT_1_13.enabled = this.oPgFrm.Page1.oPag.oMACODATT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMACODVAL_1_15.enabled = this.oPgFrm.Page1.oPag.oMACODVAL_1_15.mCond()
    this.oPgFrm.Page1.oPag.oMACAOVAL_1_28.enabled = this.oPgFrm.Page1.oPag.oMACAOVAL_1_28.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMACODKEY_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMACODKEY_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAUNIMIS_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAUNIMIS_2_16.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAQTAMOV_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAQTAMOV_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAPREZZO_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMAPREZZO_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oMACODART_2_4.visible=!this.oPgFrm.Page1.oPag.oMACODART_2_4.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_3_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("w_MACODATT Changed") or lower(cEvent)==lower("w_MACODCOM Changed")
          .Calculate_FPVOFJUDHY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(10))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACODCOM
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MACODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MACODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMACODCOM_1_12'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MACODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MACODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_COMDES = NVL(_Link_.CNDESCAN,space(30))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_VALCOM = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MACODCOM = space(15)
      endif
      this.w_COMDES = space(30)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_VALCOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MACODCOM = space(15)
        this.w_COMDES = space(30)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_VALCOM = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.CNCODCAN as CNCODCAN112"+ ",link_1_12.CNDESCAN as CNDESCAN112"+ ",link_1_12.CNDTOBSO as CNDTOBSO112"+ ",link_1_12.CNCODVAL as CNCODVAL112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on MAT_MAST.MACODCOM=link_1_12.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and MAT_MAST.MACODCOM=link_1_12.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MACODATT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_MACODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_MACODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_MACODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_MACODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_MACODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_MACODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MACODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oMACODATT_1_13'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'GSPC_MAT.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MACODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o attivit� inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_MACODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_MACODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_MACODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_MACODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,ATCENCOS,AT_STATO,ATCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_ATTDES = NVL(_Link_.ATDESCRI,space(30))
      this.w_CENCOS = NVL(_Link_.ATCENCOS,space(15))
      this.w_PIANIF = NVL(_Link_.AT_STATO,space(1))
      this.w_CODFAM = NVL(_Link_.ATCODFAM,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MACODATT = space(15)
      endif
      this.w_ATTDES = space(30)
      this.w_CENCOS = space(15)
      this.w_PIANIF = space(1)
      this.w_CODFAM = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MATIPMOV='P' and .w_PIANIF$'C|P') Or (.w_MATIPMOV='C' and .w_PIANIF$'C|Z|L') Or .cFunction='Query'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o attivit� inesistente")
        endif
        this.w_MACODATT = space(15)
        this.w_ATTDES = space(30)
        this.w_CENCOS = space(15)
        this.w_PIANIF = space(1)
        this.w_CODFAM = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALCOM
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALCOM)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALCOM = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOCOM = NVL(_Link_.VACAOVAL,0)
      this.w_DECCOM = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALCOM = space(3)
      endif
      this.w_CAOCOM = 0
      this.w_DECCOM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACODVAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVA',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MACODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MACODVAL))
          select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMACODVAL_1_15'),i_cWhere,'GSAR_AVA',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MACODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MACODVAL)
            select VACODVAL,VASIMVAL,VADECTOT,VACAOVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_MACODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.VACODVAL as VACODVAL115"+ ",link_1_15.VASIMVAL as VASIMVAL115"+ ",link_1_15.VADECTOT as VADECTOT115"+ ",link_1_15.VACAOVAL as VACAOVAL115"+ ",link_1_15.VADECUNI as VADECUNI115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on MAT_MAST.MACODVAL=link_1_15.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and MAT_MAST.MACODVAL=link_1_15.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MACODLIS
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MACODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_MACODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_MACODVAL;
                     ,'LSCODLIS',trim(this.w_MACODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oMACODLIS_1_29'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'gsma1mvm.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MACODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Listino inesistente o non valido o valuta listino incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_MACODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MACODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_MACODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_MACODVAL;
                       ,'LSCODLIS',this.w_MACODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSIVALIS,LSDTINVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_LISDES = NVL(_Link_.LSDESLIS,space(40))
      this.w_CODVAL = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MACODLIS = space(5)
      endif
      this.w_LISDES = space(40)
      this.w_CODVAL = space(3)
      this.w_IVALIS = space(1)
      this.w_INILIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODVAL=.w_MACODVAL AND .w_INILIS<=.w_MADATREG AND (.w_FINLIS>=.w_MADATREG OR EMPTY(.w_FINLIS))) OR EMPTY(.w_MACODLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino inesistente o non valido o valuta listino incongruente")
        endif
        this.w_MACODLIS = space(5)
        this.w_LISDES = space(40)
        this.w_CODVAL = space(3)
        this.w_IVALIS = space(1)
        this.w_INILIS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACODKEY
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODKEY) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MACODKEY)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MACODKEY))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODKEY)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACODKEY) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMACODKEY_2_2'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSPC2MAT.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODKEY)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MACODKEY);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MACODKEY)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO,CA__TIPO,CADESSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODKEY = NVL(_Link_.CACODICE,space(20))
      this.w_MADESCRI = NVL(_Link_.CADESART,space(40))
      this.w_MACODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_TIPCOD = NVL(_Link_.CA__TIPO,space(1))
      this.w_MA__NOTE = NVL(_Link_.CADESSUP,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_MACODKEY = space(20)
      endif
      this.w_MADESCRI = space(40)
      this.w_MACODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_TIPCOD = space(1)
      this.w_MA__NOTE = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DTOBS1>.w_MADATREG OR EMPTY(.w_DTOBS1)) And (.w_TIPCOD $ 'FM' or .w_MATIPMOV='P')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente o inesistente o obsoleto")
        endif
        this.w_MACODKEY = space(20)
        this.w_MADESCRI = space(40)
        this.w_MACODART = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_TIPCOD = space(1)
        this.w_MA__NOTE = space(0)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODKEY Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CADTOBSO as CADTOBSO202"+ ",link_2_2.CA__TIPO as CA__TIPO202"+ ",link_2_2.CADESSUP as CADESSUP202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MAT_DETT.MACODKEY=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MAT_DETT.MACODKEY=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MACODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN,ARFLSERG,ARFLUSEP,ARPREZUM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MACODART)
            select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCODIVA,ARVOCCEN,ARFLSERG,ARFLUSEP,ARPREZUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_CODIVA = NVL(_Link_.ARCODIVA,space(5))
      this.w_VOCCEN = NVL(_Link_.ARVOCCEN,space(15))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MACODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_CODIVA = space(5)
      this.w_VOCCEN = space(15)
      this.w_FLSERG = space(1)
      this.w_FLUSEP = space(1)
      this.w_PREZUM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARMOLTIP as ARMOLTIP204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.AROPERAT as AROPERAT204"+ ",link_2_4.ARCODIVA as ARCODIVA204"+ ",link_2_4.ARVOCCEN as ARVOCCEN204"+ ",link_2_4.ARFLSERG as ARFLSERG204"+ ",link_2_4.ARFLUSEP as ARFLUSEP204"+ ",link_2_4.ARPREZUM as ARPREZUM204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on MAT_DETT.MACODART=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and MAT_DETT.MACODART=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCCEN
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCCEN)
            select VCCODICE,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VOCCEN = space(15)
      endif
      this.w_CODCOS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_lTable = "TIPCOSTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2], .t., this.TIPCOSTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESBRE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCOS)
            select TCCODICE,TCDESBRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOS = NVL(_Link_.TCCODICE,space(5))
      this.w_CODCOSDE = NVL(_Link_.TCDESBRE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOS = space(5)
      endif
      this.w_CODCOSDE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCOSTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAUNIMIS
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_MAUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_MAUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oMAUNIMIS_2_16'),i_cWhere,'',"UNIT� DI MISURA",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MAUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MAUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_MAUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_MAUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_MAUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.UMCODICE as UMCODICE216"+ ",link_2_16.UMFLFRAZ as UMFLFRAZ216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on MAT_DETT.MAUNIMIS=link_2_16.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and MAT_DETT.MAUNIMIS=link_2_16.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MACODCOS
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_lTable = "MA_COSTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2], .t., this.MA_COSTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCOS="+cp_ToStrODBC(this.w_MACODCOS);
                   +" and CSCODCOM="+cp_ToStrODBC(this.w_MACOCOMS);
                   +" and CSTIPSTR="+cp_ToStrODBC(this.w_MATIPATT);
                   +" and CSCODMAT="+cp_ToStrODBC(this.w_MACOATTS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCOM',this.w_MACOCOMS;
                       ,'CSTIPSTR',this.w_MATIPATT;
                       ,'CSCODMAT',this.w_MACOATTS;
                       ,'CSCODCOS',this.w_MACODCOS)
            select CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCOM,1)+'\'+cp_ToStr(_Link_.CSTIPSTR,1)+'\'+cp_ToStr(_Link_.CSCODMAT,1)+'\'+cp_ToStr(_Link_.CSCODCOS,1)
      cp_ShowWarn(i_cKey,this.MA_COSTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACODFAM
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PC_FAMIG_IDX,3]
    i_lTable = "PC_FAMIG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2], .t., this.PC_FAMIG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_AFA',True,'PC_FAMIG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_MACODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_MACODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACODFAM) and !this.bDontReportError
            deferred_cp_zoom('PC_FAMIG','*','FACODICE',cp_AbsName(oSource.parent,'oMACODFAM_2_36'),i_cWhere,'GSPC_AFA',"Elenco famiglie",'GSPC_FAM.PC_FAMIG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_MACODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_MACODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_FADESC = NVL(_Link_.FADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_MACODFAM = space(5)
      endif
      this.w_FADESC = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.PC_FAMIG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PC_FAMIG_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PC_FAMIG_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_36.FACODICE as FACODICE236"+ ",link_2_36.FADESCRI as FADESCRI236"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_36 on MAT_DETT.MACODFAM=link_2_36.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_36"
          i_cKey=i_cKey+'+" and MAT_DETT.MACODFAM=link_2_36.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMAALFREG_1_3.value==this.w_MAALFREG)
      this.oPgFrm.Page1.oPag.oMAALFREG_1_3.value=this.w_MAALFREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMANUMREG_1_4.value==this.w_MANUMREG)
      this.oPgFrm.Page1.oPag.oMANUMREG_1_4.value=this.w_MANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATREG_1_6.value==this.w_MADATREG)
      this.oPgFrm.Page1.oPag.oMADATREG_1_6.value=this.w_MADATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMATIPMOV_1_11.RadioValue()==this.w_MATIPMOV)
      this.oPgFrm.Page1.oPag.oMATIPMOV_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODCOM_1_12.value==this.w_MACODCOM)
      this.oPgFrm.Page1.oPag.oMACODCOM_1_12.value=this.w_MACODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODATT_1_13.value==this.w_MACODATT)
      this.oPgFrm.Page1.oPag.oMACODATT_1_13.value=this.w_MACODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODVAL_1_15.value==this.w_MACODVAL)
      this.oPgFrm.Page1.oPag.oMACODVAL_1_15.value=this.w_MACODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMDES_1_22.value==this.w_COMDES)
      this.oPgFrm.Page1.oPag.oCOMDES_1_22.value=this.w_COMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oATTDES_1_23.value==this.w_ATTDES)
      this.oPgFrm.Page1.oPag.oATTDES_1_23.value=this.w_ATTDES
    endif
    if not(this.oPgFrm.Page1.oPag.oLISDES_1_24.value==this.w_LISDES)
      this.oPgFrm.Page1.oPag.oLISDES_1_24.value=this.w_LISDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMACAOVAL_1_28.value==this.w_MACAOVAL)
      this.oPgFrm.Page1.oPag.oMACAOVAL_1_28.value=this.w_MACAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODLIS_1_29.value==this.w_MACODLIS)
      this.oPgFrm.Page1.oPag.oMACODLIS_1_29.value=this.w_MACODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCENCOS_1_32.value==this.w_CENCOS)
      this.oPgFrm.Page1.oPag.oCENCOS_1_32.value=this.w_CENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODART_2_4.value==this.w_MACODART)
      this.oPgFrm.Page1.oPag.oMACODART_2_4.value=this.w_MACODART
      replace t_MACODART with this.oPgFrm.Page1.oPag.oMACODART_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMAVALRIG_2_29.value==this.w_MAVALRIG)
      this.oPgFrm.Page1.oPag.oMAVALRIG_2_29.value=this.w_MAVALRIG
      replace t_MAVALRIG with this.oPgFrm.Page1.oPag.oMAVALRIG_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMADATANN_2_32.value==this.w_MADATANN)
      this.oPgFrm.Page1.oPag.oMADATANN_2_32.value=this.w_MADATANN
      replace t_MADATANN with this.oPgFrm.Page1.oPag.oMADATANN_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMACODFAM_2_36.value==this.w_MACODFAM)
      this.oPgFrm.Page1.oPag.oMACODFAM_2_36.value=this.w_MACODFAM
      replace t_MACODFAM with this.oPgFrm.Page1.oPag.oMACODFAM_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFADESC_2_37.value==this.w_FADESC)
      this.oPgFrm.Page1.oPag.oFADESC_2_37.value=this.w_FADESC
      replace t_FADESC with this.oPgFrm.Page1.oPag.oFADESC_2_37.value
    endif
    if not(this.oPgFrm.Page1.oPag.oROW_2_43.value==this.w_ROW)
      this.oPgFrm.Page1.oPag.oROW_2_43.value=this.w_ROW
      replace t_ROW with this.oPgFrm.Page1.oPag.oROW_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMADICRIF_2_49.value==this.w_MADICRIF)
      this.oPgFrm.Page1.oPag.oMADICRIF_2_49.value=this.w_MADICRIF
      replace t_MADICRIF with this.oPgFrm.Page1.oPag.oMADICRIF_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTMOV_3_1.value==this.w_TOTMOV)
      this.oPgFrm.Page1.oPag.oTOTMOV_3_1.value=this.w_TOTMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDOC_3_2.value==this.w_TOTDOC)
      this.oPgFrm.Page1.oPag.oTOTDOC_3_2.value=this.w_TOTDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMACODKEY_2_2.value==this.w_MACODKEY)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMACODKEY_2_2.value=this.w_MACODKEY
      replace t_MACODKEY with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMACODKEY_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMADESCRI_2_3.value==this.w_MADESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMADESCRI_2_3.value=this.w_MADESCRI
      replace t_MADESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMADESCRI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAUNIMIS_2_16.value==this.w_MAUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAUNIMIS_2_16.value=this.w_MAUNIMIS
      replace t_MAUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAUNIMIS_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAQTAMOV_2_17.value==this.w_MAQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAQTAMOV_2_17.value=this.w_MAQTAMOV
      replace t_MAQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAQTAMOV_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAPREZZO_2_18.value==this.w_MAPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAPREZZO_2_18.value=this.w_MAPREZZO
      replace t_MAPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAPREZZO_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCOSDE_2_31.value==this.w_CODCOSDE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCOSDE_2_31.value=this.w_CODCOSDE
      replace t_CODCOSDE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCODCOSDE_2_31.value
    endif
    cp_SetControlsValueExtFlds(this,'MAT_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MADATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMADATREG_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MADATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MANUMREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMANUMREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_MANUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MADATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMADATREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MADATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MACODCOM) or not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMACODCOM_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MACODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MACODATT) or not((.w_MATIPMOV='P' and .w_PIANIF$'C|P') Or (.w_MATIPMOV='C' and .w_PIANIF$'C|Z|L') Or .cFunction='Query'))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMACODATT_1_13.SetFocus()
            i_bnoObbl = !empty(.w_MACODATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o attivit� inesistente")
          case   (empty(.w_MACODVAL))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMACODVAL_1_15.SetFocus()
            i_bnoObbl = !empty(.w_MACODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MACAOVAL))  and (.w_CAOVAL=0 And .cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMACAOVAL_1_28.SetFocus()
            i_bnoObbl = !empty(.w_MACAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CODVAL=.w_MACODVAL AND .w_INILIS<=.w_MADATREG AND (.w_FINLIS>=.w_MADATREG OR EMPTY(.w_FINLIS))) OR EMPTY(.w_MACODLIS))  and not(empty(.w_MACODLIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMACODLIS_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino inesistente o non valido o valuta listino incongruente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (t_CPROWORD<>0 And Not Empty( t_MACODKEY ));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_DTOBS1>.w_MADATREG OR EMPTY(.w_DTOBS1)) And (.w_TIPCOD $ 'FM' or .w_MATIPMOV='P')) and (empty(nvl(.w_MADICRIF,''))) and not(empty(.w_MACODKEY)) and (.w_CPROWORD<>0 And Not Empty( .w_MACODKEY ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMACODKEY_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice incongruente o inesistente o obsoleto")
        case   (empty(.w_MAUNIMIS) or not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_MAUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_MAUNIMIS))) and (.w_TIPCOD $ 'RM' And Not Empty(.w_MACODKEY) and empty(nvl(.w_MADICRIF,''))) and (.w_CPROWORD<>0 And Not Empty( .w_MACODKEY ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAUNIMIS_2_16
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_MAQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MAQTAMOV=INT(.w_MAQTAMOV))) and (.w_TIPCOD $ 'RM' And Not Empty(.w_MACODKEY) and empty(nvl(.w_MADICRIF,''))) and (.w_CPROWORD<>0 And Not Empty( .w_MACODKEY ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMAQTAMOV_2_17
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
      endcase
      if .w_CPROWORD<>0 And Not Empty( .w_MACODKEY )
        * --- Area Manuale = Check Row
        * --- gspc_mat
                IF Empty(.w_CODCOSDE) and.w_TIPCOD<>"A"
                 i_bRes = .f.
                 i_bnoChk = .f.
                 i_cErrorMsg =Ah_MsgFormat("Articolo senza tipologia di costo. Inserire nelle voci di costo")
                EndIF
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MADATREG = this.w_MADATREG
    this.o_MATIPMOV = this.w_MATIPMOV
    this.o_MACODCOM = this.w_MACODCOM
    this.o_MACODATT = this.w_MACODATT
    this.o_VALCOM = this.w_VALCOM
    this.o_MACODVAL = this.w_MACODVAL
    this.o_CODFAM = this.w_CODFAM
    this.o_MACODKEY = this.w_MACODKEY
    this.o_MACODART = this.w_MACODART
    this.o_CODCOS = this.w_CODCOS
    this.o_MAUNIMIS = this.w_MAUNIMIS
    this.o_MAQTAMOV = this.w_MAQTAMOV
    this.o_MAPREZZO = this.w_MAPREZZO
    this.o_LIPREZZO = this.w_LIPREZZO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 And Not Empty( t_MACODKEY ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MACOTLIS=space(5)
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_MACODKEY=space(20)
      .w_MADESCRI=space(40)
      .w_MACODART=space(20)
      .w_CODIVA=space(5)
      .w_VOCCEN=space(15)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_CODCOS=space(5)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_OPERA3=space(1)
      .w_PERIVA=0
      .w_MAUNIMIS=space(3)
      .w_MAQTAMOV=0
      .w_MAPREZZO=0
      .w_DTOBS1=ctod("  /  /  ")
      .w_MAOPPREV=space(1)
      .w_MACOATTS=space(15)
      .w_MACOCOMS=space(15)
      .w_MATIPATT=space(1)
      .w_MAOPCONS=space(1)
      .w_MA__NOTE=space(0)
      .w_MACODCOS=space(5)
      .w_MAQTA1UM=0
      .w_MAVALRIG=0
      .w_LIPREZZO=0
      .w_CODCOSDE=space(5)
      .w_MADATANN=ctod("  /  /  ")
      .w_TOTRIGA=0
      .w_TIPCOD=space(1)
      .w_MACODFAM=space(5)
      .w_FADESC=space(50)
      .w_TESTUM=.f.
      .w_ROW=0
      .w_RED=0
      .w_GREEN=0
      .w_BLUE=0
      .w_FLSERG=space(1)
      .w_MADICRIF=space(15)
      .w_FLFRAZ=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(3)
      .w_QTALIS=0
      .DoRTCalc(1,42,.f.)
        .w_MACOTLIS = .w_MACODLIS
      .DoRTCalc(44,47,.f.)
      if not(empty(.w_MACODKEY))
        .link_2_2('Full')
      endif
      .DoRTCalc(48,49,.f.)
      if not(empty(.w_MACODART))
        .link_2_4('Full')
      endif
      .DoRTCalc(50,50,.f.)
      if not(empty(.w_CODIVA))
        .link_2_5('Full')
      endif
      .DoRTCalc(51,51,.f.)
      if not(empty(.w_VOCCEN))
        .link_2_6('Full')
      endif
      .DoRTCalc(52,52,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_7('Full')
      endif
      .DoRTCalc(53,56,.f.)
      if not(empty(.w_CODCOS))
        .link_2_11('Full')
      endif
      .DoRTCalc(57,60,.f.)
        .w_MAUNIMIS = .w_UNMIS1
      .DoRTCalc(61,61,.f.)
      if not(empty(.w_MAUNIMIS))
        .link_2_16('Full')
      endif
        .w_MAQTAMOV = IIF(.w_TIPCOD='F', 1, .w_MAQTAMOV)
        .w_MAPREZZO = .w_LIPREZZO
      .DoRTCalc(64,65,.f.)
        .w_MAOPPREV = IIF(.w_MATIPMOV='P' and .w_MAPREZZO<>0 ,'+',' ')
        .w_MACOATTS = .w_MACODATT
        .w_MACOCOMS = .w_MACODCOM
        .w_MATIPATT = 'A'
        .w_MAOPCONS = IIF(.w_MATIPMOV='C' and .w_MAPREZZO<>0,'+',' ')
      .DoRTCalc(71,71,.f.)
        .w_MACODCOS = .w_CODCOS
      .DoRTCalc(72,72,.f.)
      if not(empty(.w_MACODCOS))
        .link_2_27('Full')
      endif
        .w_MAQTA1UM = IIF(.w_TIPCOD='F', 1, CALQTAADV(.w_MAQTAMOV,.w_MAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "MAQTAMOV"))
        .w_MAVALRIG = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0,VAL2VAL(.w_MAPREZZO*.w_MAQTAMOV,.w_MACAOVAL,.w_MADATREG,.w_MADATREG,GETCAM(.w_VALCOM,.w_MADATREG),.w_DECCOM))
      .DoRTCalc(75,77,.f.)
        .w_TOTRIGA = IIF(NOT EMPTY(NVL(.w_MADATANN,cp_CharToDate('  -  -  '))),0 , cp_Round(  .w_MAPREZZO*.w_MAQTAMOV , .w_DECTOT))
      .DoRTCalc(79,79,.f.)
        .w_MACODFAM = .w_CODFAM
      .DoRTCalc(80,80,.f.)
      if not(empty(.w_MACODFAM))
        .link_2_36('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(AH_MsgFormat("Imp. riga in (%1):",.w_VALCOM) )
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate()
      .DoRTCalc(81,81,.f.)
        .w_TESTUM = .T.
        .oPgFrm.Page1.oPag.oObj_2_42.Calculate()
        .w_ROW = .w_CPROWORD
        .w_RED = IIF(EMPTY(.w_MADATANN), 255, 200)
        .w_GREEN = iif(empty(.w_MADATANN), 255, 63)
        .w_BLUE = IIF(EMPTY(.w_MADATANN), 255, 63)
    endwith
    this.DoRTCalc(87,97,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MACOTLIS = t_MACOTLIS
    this.w_CPROWORD = t_CPROWORD
    this.w_MACODKEY = t_MACODKEY
    this.w_MADESCRI = t_MADESCRI
    this.w_MACODART = t_MACODART
    this.w_CODIVA = t_CODIVA
    this.w_VOCCEN = t_VOCCEN
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_CODCOS = t_CODCOS
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_OPERA3 = t_OPERA3
    this.w_PERIVA = t_PERIVA
    this.w_MAUNIMIS = t_MAUNIMIS
    this.w_MAQTAMOV = t_MAQTAMOV
    this.w_MAPREZZO = t_MAPREZZO
    this.w_DTOBS1 = t_DTOBS1
    this.w_MAOPPREV = t_MAOPPREV
    this.w_MACOATTS = t_MACOATTS
    this.w_MACOCOMS = t_MACOCOMS
    this.w_MATIPATT = t_MATIPATT
    this.w_MAOPCONS = t_MAOPCONS
    this.w_MA__NOTE = t_MA__NOTE
    this.w_MACODCOS = t_MACODCOS
    this.w_MAQTA1UM = t_MAQTA1UM
    this.w_MAVALRIG = t_MAVALRIG
    this.w_LIPREZZO = t_LIPREZZO
    this.w_CODCOSDE = t_CODCOSDE
    this.w_MADATANN = t_MADATANN
    this.w_TOTRIGA = t_TOTRIGA
    this.w_TIPCOD = t_TIPCOD
    this.w_MACODFAM = t_MACODFAM
    this.w_FADESC = t_FADESC
    this.w_TESTUM = t_TESTUM
    this.w_ROW = t_ROW
    this.w_RED = t_RED
    this.w_GREEN = t_GREEN
    this.w_BLUE = t_BLUE
    this.w_FLSERG = t_FLSERG
    this.w_MADICRIF = t_MADICRIF
    this.w_FLFRAZ = t_FLFRAZ
    this.w_NOFRAZ = t_NOFRAZ
    this.w_MODUM2 = t_MODUM2
    this.w_FLUSEP = t_FLUSEP
    this.w_PREZUM = t_PREZUM
    this.w_CLUNIMIS = t_CLUNIMIS
    this.w_QTALIS = t_QTALIS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MACOTLIS with this.w_MACOTLIS
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MACODKEY with this.w_MACODKEY
    replace t_MADESCRI with this.w_MADESCRI
    replace t_MACODART with this.w_MACODART
    replace t_CODIVA with this.w_CODIVA
    replace t_VOCCEN with this.w_VOCCEN
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_CODCOS with this.w_CODCOS
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_OPERA3 with this.w_OPERA3
    replace t_PERIVA with this.w_PERIVA
    replace t_MAUNIMIS with this.w_MAUNIMIS
    replace t_MAQTAMOV with this.w_MAQTAMOV
    replace t_MAPREZZO with this.w_MAPREZZO
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_MAOPPREV with this.w_MAOPPREV
    replace t_MACOATTS with this.w_MACOATTS
    replace t_MACOCOMS with this.w_MACOCOMS
    replace t_MATIPATT with this.w_MATIPATT
    replace t_MAOPCONS with this.w_MAOPCONS
    replace t_MA__NOTE with this.w_MA__NOTE
    replace t_MACODCOS with this.w_MACODCOS
    replace t_MAQTA1UM with this.w_MAQTA1UM
    replace t_MAVALRIG with this.w_MAVALRIG
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_CODCOSDE with this.w_CODCOSDE
    replace t_MADATANN with this.w_MADATANN
    replace t_TOTRIGA with this.w_TOTRIGA
    replace t_TIPCOD with this.w_TIPCOD
    replace t_MACODFAM with this.w_MACODFAM
    replace t_FADESC with this.w_FADESC
    replace t_TESTUM with this.w_TESTUM
    replace t_ROW with this.w_ROW
    replace t_RED with this.w_RED
    replace t_GREEN with this.w_GREEN
    replace t_BLUE with this.w_BLUE
    replace t_FLSERG with this.w_FLSERG
    replace t_MADICRIF with this.w_MADICRIF
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_NOFRAZ with this.w_NOFRAZ
    replace t_MODUM2 with this.w_MODUM2
    replace t_FLUSEP with this.w_FLUSEP
    replace t_PREZUM with this.w_PREZUM
    replace t_CLUNIMIS with this.w_CLUNIMIS
    replace t_QTALIS with this.w_QTALIS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTDOC = .w_TOTDOC-.w_mavalrig
        .w_TOTMOV = .w_TOTMOV-.w_totriga
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgspc_matPag1 as StdContainer
  Width  = 756
  height = 375
  stdWidth  = 756
  stdheight = 375
  resizeXpos=388
  resizeYpos=251
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMAALFREG_1_3 as StdField with uid="PLDWKTNZYA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MAALFREG", cQueryName = "MAALFREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di serie (dipende dall'utente)",;
    HelpContextID = 249953805,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=169, Top=12

  add object oMANUMREG_1_4 as StdField with uid="BNLHGATAHY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MANUMREG", cQueryName = "MANUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 257936909,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=65, Left=85, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oMADATREG_1_6 as StdField with uid="RDHNLXYCSA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MADATREG", cQueryName = "MADATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 263925261,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=284, Top=12


  add object oMATIPMOV_1_11 as StdCombo with uid="CINDQNRAWM",rtseq=11,rtrep=.f.,left=628,top=12,width=119,height=21;
    , ToolTipText = "Tipo movimento (preventivo consuntivo parametro)";
    , HelpContextID = 92000740;
    , cFormVar="w_MATIPMOV",RowSource=""+"Preventivo,"+"Consuntivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMATIPMOV_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MATIPMOV,&i_cF..t_MATIPMOV),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oMATIPMOV_1_11.GetRadio()
    this.Parent.oContained.w_MATIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oMATIPMOV_1_11.ToRadio()
    this.Parent.oContained.w_MATIPMOV=trim(this.Parent.oContained.w_MATIPMOV)
    return(;
      iif(this.Parent.oContained.w_MATIPMOV=='P',1,;
      iif(this.Parent.oContained.w_MATIPMOV=='C',2,;
      0)))
  endfunc

  func oMATIPMOV_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMATIPMOV_1_11.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oMACODCOM_1_12 as StdField with uid="KJDDIDRGRT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MACODCOM", cQueryName = "MACODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa",;
    HelpContextID = 3596781,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=85, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MACODCOM"

  func oMACODCOM_1_12.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oMACODCOM_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_MACODATT)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMACODCOM_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACODCOM_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMACODCOM_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oMACODCOM_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MACODCOM
    i_obj.ecpSave()
  endproc

  add object oMACODATT_1_13 as StdField with uid="QOGYRKRCFR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MACODATT", cQueryName = "MACODATT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Stato attivit� non conforme al tipo di movimento o attivit� inesistente",;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 231284250,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=85, Top=68, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_MACODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_MACODATT"

  func oMACODATT_1_13.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oMACODATT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACODATT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACODATT_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_MACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_MACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oMACODATT_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'GSPC_MAT.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oMACODATT_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_MACODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_MACODATT
    i_obj.ecpSave()
  endproc

  add object oMACODVAL_1_15 as StdField with uid="APPXKBPWYL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MACODVAL", cQueryName = "MACODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta movimento",;
    HelpContextID = 46734866,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=85, Top=95, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVA", oKey_1_1="VACODVAL", oKey_1_2="this.w_MACODVAL"

  func oMACODVAL_1_15.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oMACODVAL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
      if .not. empty(.w_MACODLIS)
        bRes2=.link_1_29('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMACODVAL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACODVAL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMACODVAL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVA',"Elenco valute",'',this.parent.oContained
  endproc
  proc oMACODVAL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_MACODVAL
    i_obj.ecpSave()
  endproc

  add object oCOMDES_1_22 as StdField with uid="NRLLBPLLVO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COMDES", cQueryName = "COMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 3224794,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=218, Top=40, InputMask=replicate('X',30)

  add object oATTDES_1_23 as StdField with uid="ENCDLHNEHL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ATTDES", cQueryName = "ATTDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 3194874,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=218, Top=68, InputMask=replicate('X',30)

  add object oLISDES_1_24 as StdField with uid="KATIGXCRPY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_LISDES", cQueryName = "LISDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 3201610,;
   bGlobalFont=.t.,;
    Height=21, Width=228, Left=519, Top=95, InputMask=replicate('X',40)

  add object oSIMVAL_1_27 as StdField with uid="VSNAOHOAMP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 123681242,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=136, Top=95, InputMask=replicate('X',5)

  add object oMACAOVAL_1_28 as StdField with uid="XDDKPUWRUK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MACAOVAL", cQueryName = "MACAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio valuta movimento rispetto all'Euro",;
    HelpContextID = 57351698,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=268, Top=95, cSayPict='"99999.9999999"', cGetPict='"99999.9999999"'

  func oMACAOVAL_1_28.mCond()
    with this.Parent.oContained
      return (.w_CAOVAL=0 And .cFunction='Load')
    endwith
  endfunc

  add object oMACODLIS_1_29 as StdField with uid="FYSONCDCND",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MACODLIS", cQueryName = "MACODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino inesistente o non valido o valuta listino incongruente",;
    ToolTipText = "Codice del listino",;
    HelpContextID = 121037287,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=457, Top=95, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_MACODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_MACODLIS"

  func oMACODLIS_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACODLIS_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACODLIS_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_MACODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_MACODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oMACODLIS_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'gsma1mvm.LISTINI_VZM',this.parent.oContained
  endproc
  proc oMACODLIS_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LSVALLIS=w_MACODVAL
     i_obj.w_LSCODLIS=this.parent.oContained.w_MACODLIS
    i_obj.ecpSave()
  endproc

  add object oCENCOS_1_32 as StdField with uid="APCSTYYBQF",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CENCOS", cQueryName = "CENCOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo associato all'attivit�",;
    HelpContextID = 7196966,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=619, Top=68, InputMask=replicate('X',15)


  add object oObj_1_42 as cp_runprogram with uid="RLNTFVHLFD",left=-5, top=392, width=242,height=19,;
    caption='GSPC_BCL',;
   bGlobalFont=.t.,;
    prg="GSPC_BCL('A')",;
    cEvent = "w_MAQTAMOV Changed,w_MACODKEY Changed",;
    nPag=1;
    , ToolTipText = "Calcolo prezzo da listini";
    , HelpContextID = 7208882


  add object oObj_1_43 as cp_runprogram with uid="FLHRVNKUFY",left=266, top=388, width=242,height=19,;
    caption='GSPC_BMK',;
   bGlobalFont=.t.,;
    prg='GSPC_BMK("CHECK")',;
    cEvent = "Insert start,Update start,Delete start",;
    nPag=1;
    , ToolTipText = "Controlli generali al salvataggio dei movimenti";
    , HelpContextID = 261226575


  add object oObj_1_50 as cp_runprogram with uid="OHCZFMTMWE",left=266, top=430, width=229,height=19,;
    caption='GSPC_BMK',;
   bGlobalFont=.t.,;
    prg='GSPC_BMK("CHGCAO")',;
    cEvent = "w_MACAOVAL Changed",;
    nPag=1;
    , ToolTipText = "Aggiorna importi nella valuta di commessa se modifico il cambio";
    , HelpContextID = 261226575


  add object oObj_1_51 as cp_runprogram with uid="OYTGHCBZQB",left=266, top=453, width=230,height=19,;
    caption='GSPC_BMK',;
   bGlobalFont=.t.,;
    prg='GSPC_BMK("CHGVAL")',;
    cEvent = "w_MACODVAL Changed",;
    nPag=1;
    , ToolTipText = "Aggiorna importi nella valuta di commessa se modifico il cambio";
    , HelpContextID = 261226575


  add object oObj_1_57 as cp_runprogram with uid="UEHIKUHWFL",left=505, top=431, width=158,height=21,;
    caption='GSPC_BMK',;
   bGlobalFont=.t.,;
    prg="GSPC_BMK('HasEvent')",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 261226575


  add object oObj_1_60 as cp_runprogram with uid="EZLZHFPEAZ",left=-6, top=415, width=242,height=19,;
    caption='GSPC_BCL',;
   bGlobalFont=.t.,;
    prg="GSPC_BCL('R')",;
    cEvent = "Ricalcola",;
    nPag=1;
    , ToolTipText = "Ricalcolo prezzo da listini";
    , HelpContextID = 7208882


  add object oObj_1_64 as cp_runprogram with uid="OGJIUCSWZM",left=-7, top=464, width=242,height=19,;
    caption='GSPC_BM1',;
   bGlobalFont=.t.,;
    prg="GSPC_BM1",;
    cEvent = "w_MACODLIS Changed,w_MADATREG Changed",;
    nPag=1;
    , ToolTipText = "Calcolo prezzo da listini";
    , HelpContextID = 261226601


  add object oObj_1_65 as cp_runprogram with uid="XQHAMOIDCE",left=-7, top=490, width=242,height=19,;
    caption='GSPC_BCL',;
   bGlobalFont=.t.,;
    prg="GSPC_BCL('U')",;
    cEvent = "w_MAUNIMIS Changed",;
    nPag=1;
    , ToolTipText = "Calcolo prezzo da listini";
    , HelpContextID = 7208882


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=128, width=748,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="MACODKEY",Label2="Articolo",Field3="MADESCRI",Label3="Descrizione",Field4="CODCOSDE",Label4="Tipo",Field5="MAUNIMIS",Label5="U.M.",Field6="MAQTAMOV",Label6="Quantit�",Field7="MAPREZZO",Label7="Prezzo unitario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185389434

  add object oStr_1_16 as StdString with uid="SWBFJVOASG",Visible=.t., Left=4, Top=40,;
    Alignment=1, Width=79, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="ZFKJRENUXN",Visible=.t., Left=4, Top=68,;
    Alignment=1, Width=79, Height=15,;
    Caption="Attivit�:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="TQJKALJEXA",Visible=.t., Left=4, Top=12,;
    Alignment=1, Width=79, Height=15,;
    Caption="Reg. n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="GOWOZQWTOW",Visible=.t., Left=377, Top=95,;
    Alignment=1, Width=77, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JJNVLBNLTO",Visible=.t., Left=158, Top=12,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ENQFTXUNYO",Visible=.t., Left=225, Top=12,;
    Alignment=1, Width=54, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="VTYBHURXUB",Visible=.t., Left=4, Top=95,;
    Alignment=1, Width=79, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="YDKRMBEWVK",Visible=.t., Left=201, Top=95,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="FCWFCVIKEW",Visible=.t., Left=532, Top=68,;
    Alignment=1, Width=83, Height=15,;
    Caption="C/Costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="LEMTUTRZTD",Visible=.t., Left=549, Top=12,;
    Alignment=1, Width=73, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="XXNMQOHVWJ",Visible=.t., Left=9, Top=350,;
    Alignment=1, Width=36, Height=16,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.

  add object oBox_1_53 as StdBox with uid="AGYEPLQHNV",left=2, top=345, width=97,height=30

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=148,;
    width=744+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=149,width=743+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMACODART_2_4.Refresh()
      this.Parent.oMAVALRIG_2_29.Refresh()
      this.Parent.oMADATANN_2_32.Refresh()
      this.Parent.oMACODFAM_2_36.Refresh()
      this.Parent.oFADESC_2_37.Refresh()
      this.Parent.oROW_2_43.Refresh()
      this.Parent.oMADICRIF_2_49.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMACODKEY_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oMAUNIMIS_2_16
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMACODART_2_4 as StdTrsField with uid="TQTZIZPQJR",rtseq=49,rtrep=.t.,;
    cFormVar="w_MACODART",value=space(20),enabled=.f.,;
    HelpContextID = 231284250,;
    cTotal="", bFixedPos=.t., cQueryName = "MACODART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=939, Top=-43, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MACODART"

  func oMACODART_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  func oMACODART_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMAVALRIG_2_29 as StdTrsField with uid="LTSUNRTGCO",rtseq=74,rtrep=.t.,;
    cFormVar="w_MAVALRIG",value=0,enabled=.f.,;
    ToolTipText = "Valore riga in valuta di commessa",;
    HelpContextID = 12825075,;
    cTotal="this.Parent.oContained.w_totdoc", bFixedPos=.t., cQueryName = "MAVALRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=274, Top=351, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oMADATANN_2_32 as StdTrsField with uid="TBEDEQIMLK",rtseq=77,rtrep=.t.,;
    cFormVar="w_MADATANN",value=ctod("  /  /  "),;
    ToolTipText = "Data annullamento riga",;
    HelpContextID = 21287404,;
    cTotal="", bFixedPos=.t., cQueryName = "MADATANN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=129, Top=326

  add object oMACODFAM_2_36 as StdTrsField with uid="CSRSHSAUPJ",rtseq=80,rtrep=.t.,;
    cFormVar="w_MACODFAM",value=space(5),;
    ToolTipText = "Codice famiglia",;
    HelpContextID = 46734867,;
    cTotal="", bFixedPos=.t., cQueryName = "MACODFAM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=274, Top=326, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PC_FAMIG", cZoomOnZoom="GSPC_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_MACODFAM"

  func oMACODFAM_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACODFAM_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMACODFAM_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PC_FAMIG','*','FACODICE',cp_AbsName(this.parent,'oMACODFAM_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_AFA',"Elenco famiglie",'GSPC_FAM.PC_FAMIG_VZM',this.parent.oContained
  endproc
  proc oMACODFAM_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSPC_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_MACODFAM
    i_obj.ecpSave()
  endproc

  add object oFADESC_2_37 as StdTrsField with uid="VBNEELUAMI",rtseq=81,rtrep=.t.,;
    cFormVar="w_FADESC",value=space(50),enabled=.f.,;
    HelpContextID = 256955050,;
    cTotal="", bFixedPos=.t., cQueryName = "FADESC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=333, Top=326, InputMask=replicate('X',50)

  add object oObj_2_39 as cp_calclbl with uid="CBKCDBRRIT",width=125,height=14,;
   left=142, top=355,;
    caption='Object',;
   bGlobalFont=.t.,;
    Alignment=1,caption="label text",;
    nPag=2;
    , HelpContextID = 47296230

  add object oObj_2_40 as cp_runprogram with uid="XCTDODHCZK",width=354,height=17,;
   left=266, top=479,;
    caption='GSPC_BVU',;
   bGlobalFont=.t.,;
    prg="GSPC_BVU('change')",;
    cEvent = "w_MAUNIMIS Changed,w_MACODKEY Changed",;
    nPag=2;
    , HelpContextID = 7208891

  add object oObj_2_42 as cp_runprogram with uid="HECNKCWCAI",width=263,height=17,;
   left=266, top=501,;
    caption='GSPC_BVU',;
   bGlobalFont=.t.,;
    prg="GSPC_BVU('F10')",;
    cEvent = "Insert start,Update start",;
    nPag=2;
    , HelpContextID = 7208891

  add object oROW_2_43 as StdTrsField with uid="YURPGQRNUW",rtseq=83,rtrep=.t.,;
    cFormVar="w_ROW",value=0,enabled=.f.,;
    ToolTipText = "Indicatore di riga attualmente selezionata",;
    HelpContextID = 130323434,;
    cTotal="", bFixedPos=.t., cQueryName = "ROW",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=48, Top=349

  add object oMADICRIF_2_49 as StdTrsField with uid="KJRAUULABA",rtseq=88,rtrep=.t.,;
    cFormVar="w_MADICRIF",value=space(15),enabled=.f.,;
    ToolTipText = "Dichiarazione di MDO di riferimento",;
    HelpContextID = 21811700,;
    cTotal="", bFixedPos=.t., cQueryName = "MADICRIF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=619, Top=40, InputMask=replicate('X',15)

  add object oStr_2_35 as StdString with uid="BMRAVGQZTK",Visible=.t., Left=4, Top=326,;
    Alignment=1, Width=121, Height=15,;
    Caption="Data annullam. riga:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="UVPVESLHKM",Visible=.t., Left=216, Top=326,;
    Alignment=1, Width=51, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="YTMQLPFOBB",Visible=.t., Left=547, Top=43,;
    Alignment=1, Width=68, Height=15,;
    Caption="Dich. MDO:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTMOV_3_1 as StdField with uid="PFRTVCOVJL",rtseq=96,rtrep=.f.,;
    cFormVar="w_TOTMOV",value=0,enabled=.f.,;
    ToolTipText = "Totale movimento",;
    HelpContextID = 58211382,;
    cQueryName = "TOTMOV",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=620, Top=326, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oTOTDOC_3_2 as StdField with uid="JOGDHBNRKK",rtseq=97,rtrep=.f.,;
    cFormVar="w_TOTDOC",value=0,enabled=.f.,;
    ToolTipText = "Totale movimento in valuta di commessa",;
    HelpContextID = 261145546,;
    cQueryName = "TOTDOC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=620, Top=350, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oObj_3_3 as cp_calclbl with uid="FKSTKRCUSD",width=125,height=14,;
   left=491, top=352,;
    caption='Object',;
   bGlobalFont=.t.,;
    Alignment=1,caption="label text",;
    nPag=3;
    , HelpContextID = 47296230

  add object oObj_3_4 as cp_calclbl with uid="VITJWHHMDW",width=125,height=13,;
   left=491, top=328,;
    caption='Object',;
   bGlobalFont=.t.,;
    Alignment=1,caption="label text",;
    nPag=3;
    , HelpContextID = 47296230
enddefine

* --- Defining Body row
define class tgspc_matBodyRow as CPBodyRowCnt
  Width=734
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="WLJMEUVTAA",rtseq=46,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 217717866,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], BackStyle=0

  add object oMACODKEY_2_2 as StdTrsField with uid="YXBCNKBEBJ",rtseq=47,rtrep=.t.,;
    cFormVar="w_MACODKEY",value=space(20),;
    HelpContextID = 130620959,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente o inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=38, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , BackStyle=0, cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MACODKEY"

  func oMACODKEY_2_2.mCond()
    with this.Parent.oContained
      return (empty(nvl(.w_MADICRIF,'')))
    endwith
  endfunc

  func oMACODKEY_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACODKEY_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMACODKEY_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMACODKEY_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSPC2MAT.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMACODKEY_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MACODKEY
    i_obj.ecpSave()
  endproc

  add object oMADESCRI_2_3 as StdTrsField with uid="FTYIJLHDTI",rtseq=48,rtrep=.t.,;
    cFormVar="w_MADESCRI",value=space(40),;
    HelpContextID = 11480591,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=233, Left=195, Top=0, InputMask=replicate('X',40), bHasZoom = .t. , BackStyle=0

  proc oMADESCRI_2_3.mZoom
    do GSPC_KDS with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMAUNIMIS_2_16 as StdTrsField with uid="SXYVWUMABH",rtseq=61,rtrep=.t.,;
    cFormVar="w_MAUNIMIS",value=space(3),;
    HelpContextID = 99008999,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=474, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , BackStyle=0, cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_MAUNIMIS"

  func oMAUNIMIS_2_16.mCond()
    with this.Parent.oContained
      return (.w_TIPCOD $ 'RM' And Not Empty(.w_MACODKEY) and empty(nvl(.w_MADICRIF,'')))
    endwith
  endfunc

  func oMAUNIMIS_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAUNIMIS_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMAUNIMIS_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oMAUNIMIS_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"UNIT� DI MISURA",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oMAQTAMOV_2_17 as StdTrsField with uid="FYCZNWRNRV",rtseq=62,rtrep=.t.,;
    cFormVar="w_MAQTAMOV",value=0,;
    HelpContextID = 107020772,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=511, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  func oMAQTAMOV_2_17.mCond()
    with this.Parent.oContained
      return (.w_TIPCOD $ 'RM' And Not Empty(.w_MACODKEY) and empty(nvl(.w_MADICRIF,'')))
    endwith
  endfunc

  func oMAQTAMOV_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MAQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MAQTAMOV=INT(.w_MAQTAMOV)))
    endwith
    return bRes
  endfunc

  add object oMAPREZZO_2_18 as StdTrsField with uid="SUAQZZULZY",rtseq=63,rtrep=.t.,;
    cFormVar="w_MAPREZZO",value=0,;
    ToolTipText = "Prezzo di listino",;
    HelpContextID = 153293291,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=601, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oMAPREZZO_2_18.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MACODKEY))
    endwith
  endfunc

  add object oCODCOSDE_2_31 as StdTrsField with uid="LXUZTWSAPF",rtseq=76,rtrep=.t.,;
    cFormVar="w_CODCOSDE",value=space(5),enabled=.f.,;
    HelpContextID = 7158635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=433, Top=0, InputMask=replicate('X',5), BackStyle=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_mat','MAT_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MASERIAL=MAT_MAST.MASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
