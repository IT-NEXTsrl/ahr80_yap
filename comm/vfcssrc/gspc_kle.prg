* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kle                                                        *
*              Visualizza log messaggi/errori                                  *
*                                                                              *
*      Author: Zucchetti Spa (CF)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_58]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-31                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kle",oParentObject))

* --- Class definition
define class tgspc_kle as StdForm
  Top    = 7
  Left   = 5

  * --- Standard Properties
  Width  = 770
  Height = 457
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-02"
  HelpContextID=216684649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  CPAR_DEF_IDX = 0
  cPrg = "gspc_kle"
  cComment = "Visualizza log messaggi/errori"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LOperaz = space(2)
  w_LSELOPE = space(1)
  w_ERRTEC = space(0)
  w_lDATINI = ctod('  /  /  ')
  w_lDATFIN = ctod('  /  /  ')
  w_LSerial = space(10)
  w_ReadPAR = space(2)
  w_DettTec = space(1)
  w_LDettTec = space(1)
  w_ZoomLOG = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_klePag1","gspc_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Lista errori")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLOperaz_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomLOG = this.oPgFrm.Pages(1).oPag.ZoomLOG
    DoDefault()
    proc Destroy()
      this.w_ZoomLOG = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CPAR_DEF'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSPC_BLE with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LOperaz=space(2)
      .w_LSELOPE=space(1)
      .w_ERRTEC=space(0)
      .w_lDATINI=ctod("  /  /  ")
      .w_lDATFIN=ctod("  /  /  ")
      .w_LSerial=space(10)
      .w_ReadPAR=space(2)
      .w_DettTec=space(1)
      .w_LDettTec=space(1)
        .w_LOperaz = 'GM'
        .w_LSELOPE = "U"
      .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
        .w_ERRTEC = iif(.w_LDettTec="S",.w_ZoomLOG.GetVar("LEMESSAG"),"Dettaglio Tecnico non disponibile")
          .DoRTCalc(4,6,.f.)
        .w_ReadPAR = 'TAM'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ReadPAR))
          .link_1_15('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_LDettTec = iif(.w_LOperaz="MR", "N", .w_DettTec)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_ERRTEC = iif(.w_LDettTec="S",.w_ZoomLOG.GetVar("LEMESSAG"),"Dettaglio Tecnico non disponibile")
        .DoRTCalc(4,6,.t.)
          .link_1_15('Full')
        .DoRTCalc(8,8,.t.)
            .w_LDettTec = iif(.w_LOperaz="MR", "N", .w_DettTec)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomLOG.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomLOG.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadPAR
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDLOGTEC";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_ReadPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_ReadPAR)
            select PDCHIAVE,PDLOGTEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPAR = NVL(_Link_.PDCHIAVE,space(2))
      this.w_DettTec = NVL(_Link_.PDLOGTEC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPAR = space(2)
      endif
      this.w_DettTec = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLOperaz_1_1.RadioValue()==this.w_LOperaz)
      this.oPgFrm.Page1.oPag.oLOperaz_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLSELOPE_1_2.RadioValue()==this.w_LSELOPE)
      this.oPgFrm.Page1.oPag.oLSELOPE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oERRTEC_1_7.value==this.w_ERRTEC)
      this.oPgFrm.Page1.oPag.oERRTEC_1_7.value=this.w_ERRTEC
    endif
    if not(this.oPgFrm.Page1.oPag.olDATINI_1_9.value==this.w_lDATINI)
      this.oPgFrm.Page1.oPag.olDATINI_1_9.value=this.w_lDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.olDATFIN_1_10.value==this.w_lDATFIN)
      this.oPgFrm.Page1.oPag.olDATFIN_1_10.value=this.w_lDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLDettTec_1_17.RadioValue()==this.w_LDettTec)
      this.oPgFrm.Page1.oPag.oLDettTec_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_lDATINI<=.w_lDATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.olDATFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_klePag1 as StdContainer
  Width  = 766
  height = 457
  stdWidth  = 766
  stdheight = 457
  resizeXpos=464
  resizeYpos=279
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLOperaz_1_1 as StdCombo with uid="SEQOEGROEI",rtseq=1,rtrep=.f.,left=96,top=13,width=152,height=21;
    , HelpContextID = 73270346;
    , cFormVar="w_LOperaz",RowSource=""+"Generazione mov.comm.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLOperaz_1_1.RadioValue()
    return(iif(this.value =1,'GM',;
    space(2)))
  endfunc
  func oLOperaz_1_1.GetRadio()
    this.Parent.oContained.w_LOperaz = this.RadioValue()
    return .t.
  endfunc

  func oLOperaz_1_1.SetRadio()
    this.Parent.oContained.w_LOperaz=trim(this.Parent.oContained.w_LOperaz)
    this.value = ;
      iif(this.Parent.oContained.w_LOperaz=='GM',1,;
      0)
  endfunc


  add object oLSELOPE_1_2 as StdCombo with uid="UZYUJNAEAX",rtseq=2,rtrep=.f.,left=342,top=13,width=138,height=21;
    , HelpContextID = 139874230;
    , cFormVar="w_LSELOPE",RowSource=""+"Solo ultima,"+"Solo non stampate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLSELOPE_1_2.RadioValue()
    return(iif(this.value =1,"U",;
    iif(this.value =2,"N",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oLSELOPE_1_2.GetRadio()
    this.Parent.oContained.w_LSELOPE = this.RadioValue()
    return .t.
  endfunc

  func oLSELOPE_1_2.SetRadio()
    this.Parent.oContained.w_LSELOPE=trim(this.Parent.oContained.w_LSELOPE)
    this.value = ;
      iif(this.Parent.oContained.w_LSELOPE=="U",1,;
      iif(this.Parent.oContained.w_LSELOPE=="N",2,;
      iif(this.Parent.oContained.w_LSELOPE=="T",3,;
      0)))
  endfunc


  add object ZoomLOG as cp_zoombox with uid="NOMGJKDEGU",left=1, top=48, width=764,height=331,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="COMM_ERR",cZoomFile="GSPC_KLE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 229748454


  add object oBtn_1_5 as StdButton with uid="OJIJKJBYEZ",left=711, top=3, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'interrogazione in base ai parametri di selezione";
    , HelpContextID = 228673046;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oERRTEC_1_7 as StdMemo with uid="SINLTFLYSM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ERRTEC", cQueryName = "ERRTEC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 180297286,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=48, Width=491, Left=134, Top=385

  add object olDATINI_1_9 as StdField with uid="RGQJUDRGMY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_lDATINI", cQueryName = "lDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 167902538,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=602, Top=6

  add object olDATFIN_1_10 as StdField with uid="PCNZOPXKPL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_lDATFIN", cQueryName = "lDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 254934346,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=602, Top=26

  func olDATFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_lDATINI<=.w_lDATFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_13 as StdButton with uid="TPSLDZDQFO",left=661, top=408, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare il log degli errori";
    , HelpContextID = 193540390;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSPC_BLE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLDettTec_1_17 as StdCheck with uid="CHOBJUUMTG",rtseq=9,rtrep=.f.,left=630, top=385, caption="Dettaglio tecnico", enabled=.f.,;
    ToolTipText = "Se attivo, visualizza dettaglio tecnico errori",;
    HelpContextID = 248529177,;
    cFormVar="w_LDettTec", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLDettTec_1_17.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oLDettTec_1_17.GetRadio()
    this.Parent.oContained.w_LDettTec = this.RadioValue()
    return .t.
  endfunc

  func oLDettTec_1_17.SetRadio()
    this.Parent.oContained.w_LDettTec=trim(this.Parent.oContained.w_LDettTec)
    this.value = ;
      iif(this.Parent.oContained.w_LDettTec=="S",1,;
      0)
  endfunc


  add object oBtn_1_18 as StdButton with uid="LMDZINRNZE",left=711, top=408, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142696726;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="WEYJDRWLST",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=86, Height=15,;
    Caption="Operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XDSDVLYWBK",Visible=.t., Left=252, Top=13,;
    Alignment=1, Width=86, Height=15,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PAHLFTAQUW",Visible=.t., Left=3, Top=385,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione errore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BIRKHXHVYP",Visible=.t., Left=496, Top=6,;
    Alignment=1, Width=102, Height=15,;
    Caption="Data iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="KXELLGYXKX",Visible=.t., Left=496, Top=26,;
    Alignment=1, Width=102, Height=15,;
    Caption="Data finale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
