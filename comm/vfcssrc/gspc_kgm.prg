* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_kgm                                                        *
*              Generazione movimenti di MDO                                    *
*                                                                              *
*      Author: Zucchetti SpA CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_41]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-21                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_kgm",oParentObject))

* --- Class definition
define class tgspc_kgm as StdForm
  Top    = 9
  Left   = 9

  * --- Standard Properties
  Width  = 790
  Height = 476
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-08"
  HelpContextID=236300183
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  DICH_MDO_IDX = 0
  DIPENDEN_IDX = 0
  LISTINI_IDX = 0
  cPrg = "gspc_kgm"
  cComment = "Generazione movimenti di MDO"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PROCESS = space(1)
  w_CODCOMM = space(15)
  w_ATTINI = space(15)
  w_ATTFIN = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = space(15)
  w_NUMFIN = space(15)
  w_DIPINI = space(5)
  w_DIPFIN = space(5)
  w_DESCAN = space(30)
  w_DESATT1 = space(30)
  w_DESATT2 = space(30)
  w_SELEZI = space(1)
  w_CMCODLIS = space(5)
  w_DESLIS = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_UTENTE = 0
  w_RDAGGIMM = space(1)
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_kgmPag1","gspc_kgm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCOMM_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='DICH_MDO'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='LISTINI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROCESS=space(1)
      .w_CODCOMM=space(15)
      .w_ATTINI=space(15)
      .w_ATTFIN=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=space(15)
      .w_NUMFIN=space(15)
      .w_DIPINI=space(5)
      .w_DIPFIN=space(5)
      .w_DESCAN=space(30)
      .w_DESATT1=space(30)
      .w_DESATT2=space(30)
      .w_SELEZI=space(1)
      .w_CMCODLIS=space(5)
      .w_DESLIS=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_UTENTE=0
      .w_RDAGGIMM=space(1)
        .w_PROCESS = 'N'
        .w_CODCOMM = g_CODCOM
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCOMM))
          .link_1_4('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ATTINI))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ATTFIN))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,7,.f.)
        if not(empty(.w_NUMINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_NUMFIN))
          .link_1_10('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DIPINI))
          .link_1_12('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_DIPFIN))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
          .DoRTCalc(11,13,.f.)
        .w_SELEZI = "D"
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CMCODLIS))
          .link_1_28('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
          .DoRTCalc(16,16,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(18,18,.f.)
        .w_RDAGGIMM = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCOMM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOMM))
          select CNCODCAN,CNDESCAN,CNCODLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOMM_1_4'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODLIS";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOMM)
            select CNCODCAN,CNDESCAN,CNCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_CMCODLIS = NVL(_Link_.CNCODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_CMCODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTINI)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOMM;
                     ,'ATCODATT',trim(this.w_ATTINI))
          select ATCODCOM,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTINI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTINI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATCODATT',cp_AbsName(oSource.parent,'oATTINI_1_5'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOMM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTINI);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOMM;
                       ,'ATCODATT',this.w_ATTINI)
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTINI = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT1 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTINI = space(15)
      endif
      this.w_DESATT1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATTFIN) OR (.w_ATTINI<= .w_ATTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTINI = space(15)
        this.w_DESATT1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTFIN)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOMM;
                     ,'ATCODATT',trim(this.w_ATTFIN))
          select ATCODCOM,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTFIN)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTFIN) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATCODATT',cp_AbsName(oSource.parent,'oATTFIN_1_6'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOMM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTFIN);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOMM;
                       ,'ATCODATT',this.w_ATTFIN)
            select ATCODCOM,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTFIN = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT2 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTFIN = space(15)
      endif
      this.w_DESATT2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATTFIN) OR (.w_ATTINI<= .w_ATTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTFIN = space(15)
        this.w_DESATT2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_lTable = "DICH_MDO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2], .t., this.DICH_MDO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DICH_MDO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RDNUMRIL like "+cp_ToStrODBC(trim(this.w_NUMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RDNUMRIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RDNUMRIL',trim(this.w_NUMINI))
          select RDNUMRIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RDNUMRIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINI)==trim(_Link_.RDNUMRIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINI) and !this.bDontReportError
            deferred_cp_zoom('DICH_MDO','*','RDNUMRIL',cp_AbsName(oSource.parent,'oNUMINI_1_9'),i_cWhere,'',"Dichiarazioni di MDO",'gspc_zdi.DICH_MDO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                     +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',oSource.xKey(1))
            select RDNUMRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                   +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(this.w_NUMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',this.w_NUMINI)
            select RDNUMRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINI = NVL(_Link_.RDNUMRIL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_NUMFIN) OR (.w_NUMINI<= .w_NUMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NUMINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])+'\'+cp_ToStr(_Link_.RDNUMRIL,1)
      cp_ShowWarn(i_cKey,this.DICH_MDO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_lTable = "DICH_MDO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2], .t., this.DICH_MDO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DICH_MDO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RDNUMRIL like "+cp_ToStrODBC(trim(this.w_NUMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RDNUMRIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RDNUMRIL',trim(this.w_NUMFIN))
          select RDNUMRIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RDNUMRIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMFIN)==trim(_Link_.RDNUMRIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMFIN) and !this.bDontReportError
            deferred_cp_zoom('DICH_MDO','*','RDNUMRIL',cp_AbsName(oSource.parent,'oNUMFIN_1_10'),i_cWhere,'',"Dichiarazioni di MDO",'gspc_zdi.DICH_MDO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                     +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',oSource.xKey(1))
            select RDNUMRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RDNUMRIL";
                   +" from "+i_cTable+" "+i_lTable+" where RDNUMRIL="+cp_ToStrODBC(this.w_NUMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RDNUMRIL',this.w_NUMFIN)
            select RDNUMRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMFIN = NVL(_Link_.RDNUMRIL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NUMFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_NUMFIN) OR (.w_NUMINI<= .w_NUMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NUMFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])+'\'+cp_ToStr(_Link_.RDNUMRIL,1)
      cp_ShowWarn(i_cKey,this.DICH_MDO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIPINI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIPINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DIPINI)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DIPINI))
          select DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIPINI)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIPINI) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDIPINI_1_12'),i_cWhere,'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIPINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DIPINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DIPINI)
            select DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIPINI = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DIPINI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DIPFIN) OR (.w_DIPINI<= .w_DIPFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DIPINI = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIPINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIPFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIPFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DIPFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DIPFIN))
          select DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIPFIN)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIPFIN) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDIPFIN_1_13'),i_cWhere,'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIPFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DIPFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DIPFIN)
            select DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIPFIN = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DIPFIN = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_DIPFIN) OR (.w_DIPINI<= .w_DIPFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DIPFIN = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIPFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CMCODLIS
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CMCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CMCODLIS))
          select LSCODLIS,LSDESLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CMCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_CMCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_CMCODLIS)+"%");

            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CMCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCMCODLIS_1_28'),i_cWhere,'',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CMCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CMCODLIS)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CMCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCOMM_1_4.value==this.w_CODCOMM)
      this.oPgFrm.Page1.oPag.oCODCOMM_1_4.value=this.w_CODCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oATTINI_1_5.value==this.w_ATTINI)
      this.oPgFrm.Page1.oPag.oATTINI_1_5.value=this.w_ATTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTFIN_1_6.value==this.w_ATTFIN)
      this.oPgFrm.Page1.oPag.oATTFIN_1_6.value=this.w_ATTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_7.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_7.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_8.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_8.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_9.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_9.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_10.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_10.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPINI_1_12.value==this.w_DIPINI)
      this.oPgFrm.Page1.oPag.oDIPINI_1_12.value=this.w_DIPINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPFIN_1_13.value==this.w_DIPFIN)
      this.oPgFrm.Page1.oPag.oDIPFIN_1_13.value=this.w_DIPFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_23.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_23.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT1_1_24.value==this.w_DESATT1)
      this.oPgFrm.Page1.oPag.oDESATT1_1_24.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT2_1_25.value==this.w_DESATT2)
      this.oPgFrm.Page1.oPag.oDESATT2_1_25.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_27.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMCODLIS_1_28.value==this.w_CMCODLIS)
      this.oPgFrm.Page1.oPag.oCMCODLIS_1_28.value=this.w_CMCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_32.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_32.value=this.w_DESLIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_ATTFIN) OR (.w_ATTINI<= .w_ATTFIN))  and not(empty(.w_ATTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_ATTFIN) OR (.w_ATTINI<= .w_ATTFIN))  and not(empty(.w_ATTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DATFIN) OR (.w_DATINI<= .w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DATFIN) OR (.w_DATINI<= .w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_NUMFIN) OR (.w_NUMINI<= .w_NUMFIN))  and not(empty(.w_NUMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_NUMFIN) OR (.w_NUMINI<= .w_NUMFIN))  and not(empty(.w_NUMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DIPFIN) OR (.w_DIPINI<= .w_DIPFIN))  and not(empty(.w_DIPINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIPINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_DIPFIN) OR (.w_DIPINI<= .w_DIPFIN))  and not(empty(.w_DIPFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIPFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgspc_kgmPag1 as StdContainer
  Width  = 786
  height = 476
  stdWidth  = 786
  stdheight = 476
  resizeXpos=423
  resizeYpos=272
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCOMM_1_4 as StdField with uid="GYPUSGUEBW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCOMM", cQueryName = "CODCOMM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa di selezione",;
    HelpContextID = 5061414,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=89, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOMM"

  func oCODCOMM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_ATTINI)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_ATTFIN)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOMM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOMM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oATTINI_1_5 as StdField with uid="VBAGWPMHPU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ATTINI", cQueryName = "ATTINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Attivit� di inizio selezione",;
    HelpContextID = 62636026,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=89, Top=54, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOMM", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTINI"

  func oATTINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOMM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOMM)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATCODATT',cp_AbsName(this.parent,'oATTINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oATTFIN_1_6 as StdField with uid="DGGJTTXXOD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ATTFIN", cQueryName = "ATTFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Attivit� di fine selezione",;
    HelpContextID = 252624890,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=89, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOMM", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTFIN"

  proc oATTFIN_1_6.mDefault
    with this.Parent.oContained
      if empty(.w_ATTFIN)
        .w_ATTFIN = .w_ATTINI
      endif
    endwith
  endproc

  func oATTFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOMM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOMM)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATCODATT',cp_AbsName(this.parent,'oATTFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oDATINI_1_7 as StdField with uid="NTMBHBUCIX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 62640842,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=507, Top=33

  func oDATINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFIN) OR (.w_DATINI<= .w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_8 as StdField with uid="LDDAMUBNPA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 252629706,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=507, Top=55

  proc oDATFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_DATFIN)
        .w_DATFIN = .w_DATINI
      endif
    endwith
  endproc

  func oDATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFIN) OR (.w_DATINI<= .w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_9 as StdField with uid="NTMZJZQLMN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero rilevazione di inizio selezione",;
    HelpContextID = 62664234,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=652, Top=33, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="DICH_MDO", oKey_1_1="RDNUMRIL", oKey_1_2="this.w_NUMINI"

  func oNUMINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DICH_MDO','*','RDNUMRIL',cp_AbsName(this.parent,'oNUMINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni di MDO",'gspc_zdi.DICH_MDO_VZM',this.parent.oContained
  endproc

  add object oNUMFIN_1_10 as StdField with uid="CCEAOYVCZG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero rilevazione di fine selezione",;
    HelpContextID = 252653098,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=652, Top=55, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="DICH_MDO", oKey_1_1="RDNUMRIL", oKey_1_2="this.w_NUMFIN"

  proc oNUMFIN_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_NUMFIN)
        .w_NUMFIN = .w_NUMINI
      endif
    endwith
  endproc

  func oNUMFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DICH_MDO','*','RDNUMRIL',cp_AbsName(this.parent,'oNUMFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dichiarazioni di MDO",'gspc_zdi.DICH_MDO_VZM',this.parent.oContained
  endproc

  add object oDIPINI_1_12 as StdField with uid="XRBCPVJDFS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DIPINI", cQueryName = "DIPINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Dipendente di inizio selezione",;
    HelpContextID = 62655178,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=507, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_DIPINI"

  func oDIPINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIPINI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIPINI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oDIPINI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDIPFIN_1_13 as StdField with uid="AKNMETPGWB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DIPFIN", cQueryName = "DIPFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Dipendente di fine selezione",;
    HelpContextID = 252644042,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=652, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_DIPFIN"

  proc oDIPFIN_1_13.mDefault
    with this.Parent.oContained
      if empty(.w_DIPFIN)
        .w_DIPFIN = .w_DIPINI
      endif
    endwith
  endproc

  func oDIPFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIPFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIPFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oDIPFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oBtn_1_22 as StdButton with uid="BKLVOWOAPK",left=733, top=83, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'interrogazione in base ai parametri di selezione";
    , HelpContextID = 123648490;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAN_1_23 as StdField with uid="RFHLUZHQUQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 261217994,;
   bGlobalFont=.t.,;
    Height=21, Width=220, Left=207, Top=32, InputMask=replicate('X',30)

  add object oDESATT1_1_24 as StdField with uid="YOZYMTHPZD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 140762826,;
   bGlobalFont=.t.,;
    Height=21, Width=220, Left=207, Top=54, InputMask=replicate('X',30)

  add object oDESATT2_1_25 as StdField with uid="ZKNKIUMZUX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 140762826,;
   bGlobalFont=.t.,;
    Height=21, Width=220, Left=207, Top=76, InputMask=replicate('X',30)


  add object ZoomSel as cp_szoombox with uid="CDOKLGPPMD",left=18, top=134, width=773,height=291,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DICH_MDO",cZoomFile="GSPC_KGM",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSPC_MRD",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 122573082

  add object oSELEZI_1_27 as StdRadio with uid="RNTTHLXPHT",rtseq=14,rtrep=.f.,left=10, top=434, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 50351578
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 50351578
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_27.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_27.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_27.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  add object oCMCODLIS_1_28 as StdField with uid="NATNPDMWXP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CMCODLIS", cQueryName = "CMCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino",;
    HelpContextID = 245967225,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=229, Top=434, InputMask=replicate('X',5), bHasZoom = .t. , TABSTOP=.F., cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CMCODLIS"

  func oCMCODLIS_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCMCODLIS_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCMCODLIS_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCMCODLIS_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'',this.parent.oContained
  endproc


  add object oBtn_1_29 as StdButton with uid="UPNCADPLOK",left=679, top=426, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il movimento di commessa";
    , HelpContextID = 236328934;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSPC_BRD(this.Parent.oContained,"GEN_MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_30 as StdButton with uid="XDTGPXBYKC",left=730, top=426, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 209624810;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESLIS_1_32 as StdField with uid="GCAAYAVGUX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168353482,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=434, InputMask=replicate('X',40)


  add object oObj_1_33 as cp_runprogram with uid="NGDEDRAXRA",left=1, top=480, width=137,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('SEL_DESEL')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 122573082

  add object oStr_1_3 as StdString with uid="VTLFVMLPOT",Visible=.t., Left=7, Top=5,;
    Alignment=0, Width=241, Height=15,;
    Caption="Selezioni dichiarazioni MDO"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="YKKLHFGAXI",Visible=.t., Left=429, Top=33,;
    Alignment=1, Width=78, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KBJCTEHKWF",Visible=.t., Left=429, Top=55,;
    Alignment=1, Width=78, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OXOHASGZGM",Visible=.t., Left=6, Top=32,;
    Alignment=1, Width=83, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="HOFDTUQOEH",Visible=.t., Left=6, Top=54,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DVUSQNGZNR",Visible=.t., Left=6, Top=76,;
    Alignment=1, Width=83, Height=15,;
    Caption="Ad attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EHBZMAXWFP",Visible=.t., Left=590, Top=33,;
    Alignment=1, Width=62, Height=15,;
    Caption="Da num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LEOOGGASTI",Visible=.t., Left=590, Top=55,;
    Alignment=1, Width=62, Height=15,;
    Caption="A num.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PQAVWJDKMU",Visible=.t., Left=429, Top=77,;
    Alignment=1, Width=78, Height=15,;
    Caption="Da dip.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QLIZLLTJPI",Visible=.t., Left=590, Top=77,;
    Alignment=1, Width=62, Height=15,;
    Caption="A dip.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="USXVQDSSDA",Visible=.t., Left=157, Top=434,;
    Alignment=1, Width=70, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oBox_1_2 as StdBox with uid="LPULQXTDJK",left=4, top=21, width=771,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_kgm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
