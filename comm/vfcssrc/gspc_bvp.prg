* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bvp                                                        *
*              Aggiorna at_prjvw (foglia)                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
* Interfaccia AHE-PROJECT                                                      *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ATCODCOM,w_ATCODATT,w_ATTIPATT,w_AT_PRJVW,w_UNIQUE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bvp",oParentObject,m.w_ATCODCOM,m.w_ATCODATT,m.w_ATTIPATT,m.w_AT_PRJVW,m.w_UNIQUE)
return(i_retval)

define class tgspc_bvp as StdBatch
  * --- Local variables
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_CURSORNA = space(100)
  w_ATCODCOM = space(15)
  w_ATCODATT = space(15)
  w_ATTIPATT = space(1)
  w_AT_PRJVW = space(1)
  w_UNIQUE = space(1)
  w_TIPATTSVC = space(1)
  w_CODCOM = space(15)
  w_TIPSTR = space(1)
  w_CODCONTO = space(15)
  w_TIPATT = space(1)
  * --- WorkFile variables
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per aggiornare il campo AT_PRJVW nel caso un antenato diventa o non sia pi� nodo foglia.
    *     Il batch va a scrivere 'N' nel campo oppure mette 'S' (Considera normalmente)
    if empty(this.w_UNIQUE) or g_APPLICATION="ADHOC REVOLUTION"
      this.w_UNIQUE = "S"
    endif
    if (this.w_ATTIPATT<>"G" and this.w_UNIQUE<>"S") or (this.w_ATTIPATT<>"P" and this.w_UNIQUE="S")
      * --- Se la macro-attivit� non � di tipo gestionale a Project non interessa
      i_retcode = 'stop'
      return
    else
      * --- si aggiorna opportunamente tutto il sottoalbero relativo a quella attivit�
      * --- per quanto riguarda il flag AT_PRJVW:
      * --- - se la macroattivit� viene flaggata con 'S'=> tutto il sottoalbero viene flaggato a 'S'
      * --- - se la macroattivit� viene flaggata con 'F' => tutto il sottoalbero viene flaggato a 'N'
      this.w_CODCOM = this.w_ATCODCOM
      this.w_TIPSTR = this.w_ATTIPATT
      this.w_CODCONTO = this.w_ATCODATT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- A questo punto i dati sono nel cursore w_COUTCURS
      select (this.w_COUTCURS)
      go top
      scan
      this.w_CODCOM = ATCODCOM
      this.w_TIPATT = ATTIPATT
      this.w_CODCONTO = ATCODATT
      if this.w_CODCONTO<>this.w_ATCODATT
        * --- (la macro attivit� radice del sotto albero viene lasciata flaggata a 'F')
        if this.w_AT_PRJVW="F"
          * --- - se la macroattivit� viene flaggata con 'F' => tutto il sottoalbero viene flaggato a 'N'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AT_PRJVW ="+cp_NullLink(cp_ToStrODBC("N"),'ATTIVITA','AT_PRJVW');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPATT);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_CODCONTO);
                   )
          else
            update (i_cTable) set;
                AT_PRJVW = "N";
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CODCOM;
                and ATTIPATT = this.w_TIPATT;
                and ATCODATT = this.w_CODCONTO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Impossibile aggiornare i flag in ATTIVITA'
            return
          endif
        else
          * --- - se la macroattivit� viene flaggata con 'S'=> tutto il sottoalbero viene flaggato a 'S'
          * --- Write into ATTIVITA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AT_PRJVW ="+cp_NullLink(cp_ToStrODBC("S"),'ATTIVITA','AT_PRJVW');
                +i_ccchkf ;
            +" where ";
                +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                +" and ATTIPATT = "+cp_ToStrODBC(this.w_TIPATT);
                +" and ATCODATT = "+cp_ToStrODBC(this.w_CODCONTO);
                   )
          else
            update (i_cTable) set;
                AT_PRJVW = "S";
                &i_ccchkf. ;
             where;
                ATCODCOM = this.w_CODCOM;
                and ATTIPATT = this.w_TIPATT;
                and ATCODATT = this.w_CODCONTO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Impossibile aggiornare i flag in ATTIVITA'
            return
          endif
        endif
      endif
      endscan
      * --- Rimuovo il cursore in uscita
      Use in (this.w_COUTCURS) 
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione della treeview
    * --- Crea Cursore Preventivo
    * --- Assegno al cursore da usare un nome di cursore libero
    this.w_CURSORNA = SYS(2015)
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = ""
    * --- Altri campi
    this.w_TIPSTR = iif(nvl(this.w_UNIQUE," ")="S","P","G")
    * --- Parametro per la visual query: solo attivit� gestionali
    AH_Msg( "Caricamento progetto..." )
    * --- Recupero solo la parte di struttura identificata dal conto
    VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    Select "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        SELECT "__tmp__" 
 USE
      endif
    else
      * --- Rimuovo il cursore in uscita
      if used (this.w_COUTCURS)
        Use in (this.w_COUTCURS) 
      endif
      * --- Non c'� nulla da controllare
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,w_ATCODCOM,w_ATCODATT,w_ATTIPATT,w_AT_PRJVW,w_UNIQUE)
    this.w_ATCODCOM=w_ATCODCOM
    this.w_ATCODATT=w_ATCODATT
    this.w_ATTIPATT=w_ATTIPATT
    this.w_AT_PRJVW=w_AT_PRJVW
    this.w_UNIQUE=w_UNIQUE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIVITA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ATCODCOM,w_ATCODATT,w_ATTIPATT,w_AT_PRJVW,w_UNIQUE"
endproc
