* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bs1                                                        *
*              Chiude form legami                                              *
*                                                                              *
*      Author: NICOLA GORLANDI                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-19                                                      *
* Last revis.: 2008-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bs1",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bs1 as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_CONTA = 0
  w_MESS = space(100)
  * --- WorkFile variables
  STRUTTUR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTipo="Treeview"
        * --- Aggiorna la Treeview dalla gestione legami se questa � lanciata dalla maschera Tree
        Obj_form=this.oParentObject.w_TREEFORM
        if type("Obj_form.ClassName")="C"
          * --- Faccio il refresh
          this.oParentObject.w_TREEFORM.NotifyEvent("Reload")     
        endif
      case this.pTipo="CodPad"
        * --- Controllo che nessun altro abbia messo un legame con quel padre
        * --- Lanciato nella Replace Init
        this.w_CONTA = 0
        * --- Select from STRUTTUR
        i_nConn=i_TableProp[this.STRUTTUR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRUTTUR_idx,2],.t.,this.STRUTTUR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select STTIPSTR  from "+i_cTable+" STRUTTUR ";
              +" where STCODCOM="+cp_ToStrODBC(this.oParentObject.w_STCODCOM)+" And STTIPSTR="+cp_ToStrODBC(this.oParentObject.w_STTIPSTR)+" And STATTPAD="+cp_ToStrODBC(this.oParentObject.w_STATTPAD)+"";
               ,"_Curs_STRUTTUR")
        else
          select STTIPSTR from (i_cTable);
           where STCODCOM=this.oParentObject.w_STCODCOM And STTIPSTR=this.oParentObject.w_STTIPSTR And STATTPAD=this.oParentObject.w_STATTPAD;
            into cursor _Curs_STRUTTUR
        endif
        if used('_Curs_STRUTTUR')
          select _Curs_STRUTTUR
          locate for 1=1
          do while not(eof())
          this.w_CONTA = this.w_CONTA+1
          Exit
            select _Curs_STRUTTUR
            continue
          enddo
          use
        endif
        if this.w_CONTA<>0
          this.w_MESS = AH_MSGFORMAT( "Esiste gia un legame con padre %1. Impossibile salvare" , ALLTRIM( this.oParentObject.w_STATTPAD ) ) 
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRUTTUR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_STRUTTUR')
      use in _Curs_STRUTTUR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
