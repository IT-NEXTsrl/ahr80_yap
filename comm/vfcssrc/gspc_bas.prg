* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bas                                                        *
*              Gestione stato attivit�                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-07                                                      *
* Last revis.: 2007-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bas",oParentObject,m.pAzione)
return(i_retval)

define class tgspc_bas as StdBatch
  * --- Local variables
  pAzione = space(10)
  NC = space(10)
  GSPC_KCM = .NULL.
  I = 0
  w_ZOOM = .NULL.
  w_CHKDETT = 0
  w_Riga_Cur = 0
  w_CHKOK = .f.
  w_NR = 0
  w_ATPERCOM = 0
  w_UTPERCOM = 0
  w_CODCONTO = space(15)
  zCursor = space(10)
  tCursor = space(10)
  w_STTIPSTR = space(1)
  w_MESS = space(200)
  w_STR_CONF = space(10)
  w_STR_LANC = space(10)
  w_STR_COMP = space(10)
  w_STR_PIAN = space(10)
  w_STR_PROV = space(10)
  w_MESS = space(200)
  f_CODCOM = space(15)
  f_CODATT = space(0)
  f_STATO = space(0)
  f_PERCOM = space(0)
  w_CONTA = 0
  zColor = space(200)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_LASTPOINT = 0
  w_CODATTCUR = space(15)
  w_RELAZ = .NULL.
  w_TIPSTRLOC = space(1)
  * --- WorkFile variables
  ATTIVITA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamata da GSPC_KAS
    * --- Parametro operazione da eseguire
    * --- --
    * --- Puntatore al padre
    this.GSPC_KCM = this.oParentObject
    this.w_ZOOM = this.GSPC_KCM.w_ZoomDett
    this.NC = this.w_ZOOM.cCursor
    * --- Inizializzazioni
    this.w_CODCONTO = this.oParentObject.w_CODSTC
    this.w_STR_PROV = AH_MsgFormat("Provvisoria")
    this.w_STR_CONF = AH_MsgFormat("Confermata")
    this.w_STR_PIAN = AH_MsgFormat("Pianificata")
    this.w_STR_LANC = AH_MsgFormat("Lanciata")
    this.w_STR_COMP = AH_MsgFormat("Completata")
    * --- Casi
    do case
      case this.pAzione="SEL_DESEL"
        * --- Seleziona/Deseleziona tutte le righe dello zoom
        * --- Nome cursore collegato allo zoom
        if used(this.NC)
          * --- Gestisco l'errore nel caso in cui non riesca a riposizionarmi sul currsore
          *     Caso in cui alla requery ho meno righe
          w_olderr=ON("ERROR")
          w_err =.f.
          ON ERROR w_err=.t.
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
            do case
              case this.oParentObject.w_AT_STATO = "P"
                UPDATE (this.NC) SET AT_STATO=this.w_STR_CONF
              case this.oParentObject.w_AT_STATO = "Z"
                UPDATE (this.NC) SET UTPERCOM= iif(UTPERCOM=0, 1, UTPERCOM)
                UPDATE (this.NC) SET AT_STATO= iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
              case this.oParentObject.w_AT_STATO $ "L-F"
                UPDATE (this.NC) SET UTPERCOM=100
                UPDATE (this.NC) SET AT_STATO=iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
              case this.oParentObject.w_AT_STATO = "C"
                UPDATE (this.NC) SET AT_STATO=this.w_STR_PIAN
            endcase
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=0
            do case
              case this.oParentObject.w_AT_STATO = "P"
                UPDATE (this.NC) SET AT_STATO=this.w_STR_PROV
              case this.oParentObject.w_AT_STATO = "Z"
                UPDATE (this.NC) SET UTPERCOM= ATPERCOM
                UPDATE (this.NC) SET AT_STATO= iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, this.w_STR_PIAN))
              case this.oParentObject.w_AT_STATO $ "L-F"
                UPDATE (this.NC) SET UTPERCOM=ATPERCOM
                UPDATE (this.NC) SET AT_STATO=iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
              case this.oParentObject.w_AT_STATO = "C"
                UPDATE (this.NC) SET AT_STATO=this.w_STR_CONF
            endcase
          endif
          ON ERROR &w_olderr
        endif
      case this.pAzione="TITLE"
        do case
          case this.oParentObject.w_TIPSTR="P"
            this.GSPC_KCM.Caption = this.GSPC_KCM.cComment + chr(32) + AH_MsgFormat("(amministrativo)")
          case this.oParentObject.w_TIPSTR="T"
            this.GSPC_KCM.Caption = this.GSPC_KCM.cComment + chr(32) + AH_MsgFormat("(tecnico)")
          case this.oParentObject.w_TIPSTR="G"
            this.GSPC_KCM.Caption = this.GSPC_KCM.cComment + chr(32) + AH_MsgFormat("(gestionale)")
          otherwise
            this.GSPC_KCM.Caption = this.GSPC_KCM.cComment + chr(32) + AH_MsgFormat("(nessuna struttura)")
        endcase
        if this.oParentObject.w_TIPSTR="X"
          this.w_MESS = "Attenzione!%0L'utente non appartiene ai gruppi impostati nei parametri di default della commessa%0%0Per effettuare le modifiche l'utente deve appartenere ad almeno uno di questi gruppi."
          ah_ErrorMsg(this.w_MESS, 48, "")
        endif
      case this.pAzione="RQY" and !this.oParentObject.w_FIRST
        this.oParentObject.w_CALCOLA = .f.
        if USED (this.NC)
          SELECT (this.NC)
          ZAP
        endif
        this.w_ZOOM.grd.refresh()     
      case this.pAzione="RQF"
        this.w_STTIPSTR = this.oParentObject.w_TIPSTR
        this.oParentObject.w_CALCOLA = .f.
        this.GSPC_KCM.Notifyevent("Interroga")     
        if not empty(this.w_CODCONTO)
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        VQ_EXEC("..\COMM\EXE\QUERY\GSPC1KAS.VQR",this,"Curs_Rel")
        if used ("Curs_Rel")
          = wrcursor("Curs_Rel")
          if not empty(this.w_CODCONTO)
            delete from Curs_Rel where atcodatt not in (select atcodatt from (this.oParentObject.w_CURSORNA) where ATCODCOM=this.oParentObject.w_CODCOM and attipatt = "A")
          endif
          select Curs_Rel
          go top
          SCAN
          scatter memvar
          select (this.NC)
          append blank
          gather memvar
          select Curs_Rel
          ENDSCAN
        endif
        if used (this.oParentObject.w_CURSORNA)
          use in (this.oParentObject.w_CURSORNA)
        endif
        if used ("Curs_Rel")
          use in Curs_Rel
        endif
        select (this.NC)
        go top
        this.w_ZOOM.grd.refresh()     
        this.w_CHKDETT = 0
        this.oParentObject.w_FIRST = .f.
        this.oParentObject.w_CALCOLA = .t.
      case this.pAzione="DRC"
        * --- Eseguito il Check sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(this.NC)
          * --- Aggiorna quantita' su cursore di dettaglio
          SELECT (this.NC)
          do case
            case this.oParentObject.w_AT_STATO = "P"
              replace AT_STATO with this.w_STR_CONF
            case this.oParentObject.w_AT_STATO = "Z"
              replace UTPERCOM with iif(UTPERCOM>=1, UTPERCOM, iif(UTPERCOM=0, 1, 0))
              replace AT_STATO with iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
            case this.oParentObject.w_AT_STATO $ "L-F"
              replace UTPERCOM with 100
              replace AT_STATO with iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
            case this.oParentObject.w_AT_STATO = "C"
              replace AT_STATO with this.w_STR_PIAN
          endcase
        endif
      case this.pAzione="DRU"
        * --- Eseguito il Check sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(this.NC)
          * --- Aggiorna quantita' su cursore di dettaglio
          SELECT (this.NC)
          do case
            case this.oParentObject.w_AT_STATO = "P"
              replace AT_STATO with this.w_STR_PROV
            case this.oParentObject.w_AT_STATO = "Z"
              replace UTPERCOM with ATPERCOM
              replace AT_STATO with this.w_STR_PIAN, ATPERCOM with 0
            case this.oParentObject.w_AT_STATO $ "L-F"
              replace UTPERCOM with ATPERCOM
              replace AT_STATO with iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, ""))
            case this.oParentObject.w_AT_STATO = "C"
              replace AT_STATO with this.w_STR_CONF
          endcase
        endif
      case this.pAzione="AGST"
        if this.oParentObject.w_CALCOLA
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAzione="APP"
        UPDATE (this.NC) SET UTPERCOM=this.oParentObject.w_NPCOMP WHERE XCHK=1
        UPDATE (this.NC) SET AT_STATO=iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, this.w_STR_PIAN))
        do case
          case this.oParentObject.w_AT_STATO="Z"
            UPDATE (this.NC) SET XCHK= IIF(UTPERCOM>0, 1, 0)
          case this.oParentObject.w_AT_STATO="L"
            UPDATE (this.NC) SET XCHK= IIF(UTPERCOM<>ATPERCOM, 1, 0)
        endcase
      case this.pAzione="UPD"
        f_TRSTATO = "iif(alltrim(AT_STATO)="+chr(39)+this.w_STR_CONF+chr(39)+", 'C', iif(alltrim(AT_STATO)="+chr(39)+this.w_STR_LANC+chr(39)+", 'L', iif(alltrim(AT_STATO)="+chr(39)+this.w_STR_COMP+chr(39)+",  'F', IIF(alltrim(AT_STATO)="+chr(39)+this.w_STR_PIAN+chr(39)+ ", 'Z', ' '))))"
        if USED(this.NC)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CHKOK
            Select ATCODATT, AT_STATO, UTPERCOM from (this.NC) into cursor Appo where XCHK=1
            select Appo
            if reccount()>0
              go top
              * --- Try
              local bErr_036EA950
              bErr_036EA950=bTrsErr
              this.Try_036EA950()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
              endif
              bTrsErr=bTrsErr or bErr_036EA950
              * --- End
            endif
            if used("Appo")
              use in Appo
            endif
          endif
        endif
      case this.pAzione="DON"
        if used (this.oParentObject.w_CURSORNA)
          use in (this.oParentObject.w_CURSORNA)
        endif
        this.w_CHKDETT = -1
    endcase
    if this.w_CHKDETT <> -1
      do case
        case this.oParentObject.w_AT_STATO = "P"
          this.zColor = "IIF(XCHK=1, RGB(160,255,255), RGB(255,255,160))"
        case this.oParentObject.w_AT_STATO = "Z"
          this.zColor = "IIF(XCHK=1 and (UTPERCOM>=1 and UTPERCOM<=99), RGB(174,192,255), IIF(XCHK=1 and UTPERCOM=100, RGB(254,80,111), RGB(160,255,160)))"
        case this.oParentObject.w_AT_STATO = "L"
          this.zColor = "IIF(XCHK=1 and (UTPERCOM>=1 and UTPERCOM<=99), RGB(174,192,255), IIF(XCHK=1 and UTPERCOM=100, RGB(254,80,111), IIF(UTPERCOM=0, RGB(160,255,160), RGB(174,192,255))))"
        case this.oParentObject.w_AT_STATO = "C"
          this.zColor = "IIF(XCHK=1, RGB(160,255,160), RGB(160,255,255))"
        case this.oParentObject.w_AT_STATO = "F"
          this.zColor = "IIF(XCHK=1 and (UTPERCOM>=1 and UTPERCOM<=99), RGB(174,192,255), IIF(XCHK=1 and UTPERCOM=100, RGB(254,80,111), IIF(UTPERCOM=0, RGB(160,255,160), RGB(254,80,111))))"
      endcase
      if USED(this.NC)
        SELECT (this.NC)
        FOR this.I=1 TO this.w_Zoom.grd.ColumnCount
        ND = ALLTRIM(STR(this.I))
        if UPPER(this.w_Zoom.grd.Column&ND..ControlSource)<>"XCHK"
          do case
            case this.oParentObject.w_AT_STATO = "P-C"
              if UPPER(this.w_Zoom.grd.Column&ND..ControlSource)="UTPERCOM"
                this.w_ZOOM.grd.Column&ND..ReadOnly = .t.
              endif
            case this.oParentObject.w_AT_STATO $ "Z-L-F"
              if UPPER(this.w_Zoom.grd.Column&ND..ControlSource)="UTPERCOM"
                this.w_ZOOM.grd.Column&ND..ReadOnly = .f.
              endif
          endcase
        endif
        if inlist(UPPER(this.w_Zoom.grd.Column&ND..ControlSource), "ATCODATT", "AT_STATO", "UTPERCOM")
          this.w_ZOOM.grd.Column&ND..DynamicBackColor = this.zColor
        endif
        ENDFOR
      endif
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.bUpdateParentObject = .F.
  endproc
  proc Try_036EA950()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    scan
    this.f_CODCOM = NVL(this.oParentObject.w_CODCOM, SPACE(15))
    this.f_CODATT = NVL(ATCODATT, SPACE(15))
    this.f_STATO = &f_TRSTATO
    this.f_PERCOM = NVL(UTPERCOM, 0)
    ah_msg("Aggiornamento attivit� %1", .t., .f., .f., alltrim(this.f_CODATT))
    if not empty(this.f_STATO)
      if this.f_STATO $ "C-P"
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.f_STATO),'ATTIVITA','AT_STATO');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.f_CODCOM);
              +" and ATCODATT = "+cp_ToStrODBC(this.f_CODATT);
              +" and ATTIPATT = "+cp_ToStrODBC("A");
                 )
        else
          update (i_cTable) set;
              AT_STATO = this.f_STATO;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.f_CODCOM;
              and ATCODATT = this.f_CODATT;
              and ATTIPATT = "A";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into ATTIVITA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ATTIVITA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AT_STATO ="+cp_NullLink(cp_ToStrODBC(this.f_STATO),'ATTIVITA','AT_STATO');
          +",ATPERCOM ="+cp_NullLink(cp_ToStrODBC(this.f_PERCOM),'ATTIVITA','ATPERCOM');
              +i_ccchkf ;
          +" where ";
              +"ATCODCOM = "+cp_ToStrODBC(this.f_CODCOM);
              +" and ATCODATT = "+cp_ToStrODBC(this.f_CODATT);
              +" and ATTIPATT = "+cp_ToStrODBC("A");
                 )
        else
          update (i_cTable) set;
              AT_STATO = this.f_STATO;
              ,ATPERCOM = this.f_PERCOM;
              &i_ccchkf. ;
           where;
              ATCODCOM = this.f_CODCOM;
              and ATCODATT = this.f_CODATT;
              and ATTIPATT = "A";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_CONTA = this.w_CONTA + 1
    endif
    select Appo
    endscan
    wait Clear
    this.w_MESS = "Confermi aggiornamento?"
    if AH_YesNo(this.w_MESS)
      * --- commit
      cp_EndTrs(.t.)
      AH_ErrorMsg("Operazione completata con successo%0Sono state aggiornate %1 attivit�", 64, "" , alltrim(str(this.w_CONTA,6,0)))
      this.oParentObject.w_CALCOLA = .f.
      this.GSPC_KCM.NotifyEvent("Reload")     
    else
      * --- Raise
      i_Error="Elaborazione annullata"
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CHKOK = .t.
    if this.oParentObject.w_AT_STATO <> "P"
      if USED(this.NC)
        this.w_NR = reccount()
        * --- Aggiorna quantita' su cursore di dettaglio
        SELECT (this.NC)
        if this.w_NR>0
          this.w_Riga_Cur = Recno()
          * --- Gestisco l'errore nel caso in cui non riesca a riposizionarmi sul currsore
          *     Caso in cui alla requery ho meno righe
          w_olderr=ON("ERROR")
          w_err =.f.
          ON ERROR w_err=.t.
          goto this.oParentObject.w_Riga
          this.oParentObject.w_Riga = this.w_Riga_Cur
          this.w_ATPERCOM = NVL(ATPERCOM, 0)
          this.w_UTPERCOM = NVL(UTPERCOM, 0)
          do case
            case this.oParentObject.w_AT_STATO $ "L-Z-F"
              if this.w_UTPERCOM>100 or this.w_UTPERCOM<0
                ah_ErrorMsg("La percentuale di completamento deve essere compresa tra 0 e 100 ","STOP", "")
                replace UTPERCOM with this.w_ATPERCOM
                replace AT_STATO with iif(UTPERCOM>=1 and UTPERCOM<=99, this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, IIF(UTPERCOM=0 AND ATPERCOM>0 , this.w_STR_LANC,this.w_STR_PIAN)))
                replace XCHK with 0
                this.w_CHKOK = .f.
              else
                do case
                  case this.oParentObject.w_AT_STATO="Z"
                    replace AT_STATO with iif((UTPERCOM>=1 and UTPERCOM<=99), this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, this.w_STR_PIAN))
                    if this.w_UTPERCOM=0 or this.w_UTPERCOM=this.w_ATPERCOM 
                      replace XCHK with 0
                    else
                      replace XCHK with 1
                    endif
                  case this.oParentObject.w_AT_STATO $ "L-F"
                    replace AT_STATO with iif((UTPERCOM>=1 and UTPERCOM<=99) , this.w_STR_LANC, iif(UTPERCOM=100, this.w_STR_COMP, IIF(UTPERCOM=0 , this.w_STR_PIAN," ")))
                    if this.w_UTPERCOM<>this.w_ATPERCOM
                      replace XCHK with 1
                    else
                      replace XCHK with 0
                    endif
                endcase
              endif
          endcase
          SELECT (this.NC)
          go this.w_Riga_Cur
          this.oParentObject.w_Riga = this.w_Riga_Cur
          ON ERROR &w_olderr
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna la Treeview
    if EMPTY(this.oParentObject.w_CURSORNA)
      this.oParentObject.w_CURSORNA = SYS(2015)
    endif
    * --- Definizione parametri da passare alla CP_EXPDB
    this.w_COUTCURS = this.oParentObject.w_CURSORNA
    * --- Cursore utilizzato dalla Tree View
    this.w_INPCURS = "QUERY"
    * --- Cursore di partenza
    this.w_CRIFTABLE = "ATTIVITA"
    * --- Tabella di riferimento
    this.w_RIFKEY = "ATCODCOM,ATCODATT,ATTIPATT"
    * --- Chiave anagrafica componenti
    this.w_CEXPTABLE = "STRUTTUR"
    * --- Tabella di esplosione
    this.w_EXPKEY = "STCODCOM,STATTPAD,STTIPSTR,STATTFIG,STTIPFIG"
    * --- Chiave della movimentazione contenente la struttura
    this.w_REPKEY = "STCODCOM,STATTFIG,STTIPFIG"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_EXPFIELD = ""
    * --- Campi per espolosione
    this.w_OTHERFLD = "CPROWORD"
    * --- Altri campi (ordinamento)
    if Empty ( this.w_CODCONTO )
      * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
      VQ_EXEC("..\COMM\EXE\QUERY\TREEVIEW.VQR",this,"query")
    else
      * --- Recupero solo la parte di struttura identificata dal conto
      VQ_EXEC("..\COMM\EXE\QUERY\GSPC0BSC.VQR",this,"query")
    endif
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      PC_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD)
      if USED("__tmp__")
        use in __tmp__
      endif
      if used("query")
        use in query
      endif
    else
      * --- Se la prima commessa specificata non ha elementi
      if Not Used ( this.oParentObject.w_CURSORNA )
        AH_ErrorMsg("La commessa non ha nessun elemento.",,"Attenzione")
        i_retcode = 'stop'
        return
      endif
      Select ( this.oParentObject.w_CURSORNA )
      Zap
      AH_ErrorMsg("La commessa non ha nessun elemento.",48, "")
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIVITA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
