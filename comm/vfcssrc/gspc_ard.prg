* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_ard                                                        *
*              Dichiarazioni di MDO                                            *
*                                                                              *
*      Author: Zucchetti Spa CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_95]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-17                                                      *
* Last revis.: 2013-07-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_ard"))

* --- Class definition
define class tgspc_ard as StdForm
  Top    = 3
  Left   = 15

  * --- Standard Properties
  Width  = 577
  Height = 206+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-22"
  HelpContextID=126507113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  DICH_MDO_IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cFile = "DICH_MDO"
  cKeySelect = "RDNUMRIL,CPROWNUM"
  cKeyWhere  = "RDNUMRIL=this.w_RDNUMRIL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"RDNUMRIL="+cp_ToStrODBC(this.w_RDNUMRIL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';

  cKeyWhereODBCqualified = '"DICH_MDO.RDNUMRIL="+cp_ToStrODBC(this.w_RDNUMRIL)';
      +'+" and DICH_MDO.CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';

  cPrg = "gspc_ard"
  cComment = "Dichiarazioni di MDO"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_STATOANAL = space(10)
  w_RDNUMRIL = space(15)
  o_RDNUMRIL = space(15)
  w_GLOBALCM = space(15)
  o_GLOBALCM = space(15)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_RDTIPATT = space(1)
  w_RDOPERIL = 0
  w_RDDATRIL = ctod('  /  /  ')
  o_RDDATRIL = ctod('  /  /  ')
  w_RDCODESE = space(4)
  w_RDORARIL = space(8)
  w_RDDIPEND = space(5)
  w_RDPROCES = space(1)
  w_RDCODCOM = space(15)
  w_RDAGGIMM = space(1)
  w_RDCODATT = space(15)
  w_RDDATINI = ctod('  /  /  ')
  w_RDORAINI = space(8)
  w_RDDATFIN = ctod('  /  /  ')
  w_RDORAFIN = space(8)
  w_RDTEMPOR = 0
  w_RDSECON = 0
  w_RDCOGNOM = space(40)
  w_RDDESCAN = space(30)
  w_RDDESATT = space(30)
  w_RDRILAUT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_UTENTE = 0
  w_PIANIF = space(1)
  w_COMMOBS = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_RDNUMRIL = this.W_RDNUMRIL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DICH_MDO','gspc_ard')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_ardPag1","gspc_ard",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dichiarazioni di MDO")
      .Pages(1).HelpContextID = 137546336
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRDNUMRIL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='DICH_MDO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DICH_MDO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DICH_MDO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RDNUMRIL = NVL(RDNUMRIL,space(15))
      .w_CPROWNUM = NVL(CPROWNUM,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_13_joined
    link_1_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DICH_MDO where RDNUMRIL=KeySet.RDNUMRIL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DICH_MDO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DICH_MDO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DICH_MDO '
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RDNUMRIL',this.w_RDNUMRIL  ,'CPROWNUM',this.w_CPROWNUM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RDCOGNOM = space(40)
        .w_RDDESCAN = space(30)
        .w_RDDESATT = space(30)
        .w_OBTEST = i_datsys
        .w_UTENTE = 0
        .w_PIANIF = space(1)
        .w_COMMOBS = ctod("  /  /  ")
        .w_STATOANAL = upper(this.cFunction)
        .w_RDNUMRIL = NVL(RDNUMRIL,space(15))
        .op_RDNUMRIL = .w_RDNUMRIL
        .w_GLOBALCM = iif(!empty(.w_RDNUMRIL),g_CODCOM,'')
        .w_CPROWNUM = NVL(CPROWNUM,0)
        .w_CPROWORD = NVL(CPROWORD,0)
        .w_RDTIPATT = NVL(RDTIPATT,space(1))
        .w_RDOPERIL = NVL(RDOPERIL,0)
        .w_RDDATRIL = NVL(cp_ToDate(RDDATRIL),ctod("  /  /  "))
        .w_RDCODESE = NVL(RDCODESE,space(4))
        .w_RDORARIL = NVL(RDORARIL,space(8))
        .w_RDDIPEND = NVL(RDDIPEND,space(5))
          .link_1_11('Load')
        .w_RDPROCES = NVL(RDPROCES,space(1))
        .w_RDCODCOM = NVL(RDCODCOM,space(15))
          if link_1_13_joined
            this.w_RDCODCOM = NVL(CNCODCAN113,NVL(this.w_RDCODCOM,space(15)))
            this.w_RDDESCAN = NVL(CNDESCAN113,space(30))
            this.w_RDAGGIMM = NVL(CNFLAGIM113,space(1))
            this.w_COMMOBS = NVL(cp_ToDate(CNDTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
        .w_RDAGGIMM = NVL(RDAGGIMM,space(1))
        .w_RDCODATT = NVL(RDCODATT,space(15))
          .link_1_15('Load')
        .w_RDDATINI = NVL(cp_ToDate(RDDATINI),ctod("  /  /  "))
        .w_RDORAINI = NVL(RDORAINI,space(8))
        .w_RDDATFIN = NVL(cp_ToDate(RDDATFIN),ctod("  /  /  "))
        .w_RDORAFIN = NVL(RDORAFIN,space(8))
        .w_RDTEMPOR = NVL(RDTEMPOR,0)
        .w_RDSECON = NVL(RDSECON,0)
        .w_RDRILAUT = NVL(RDRILAUT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DICH_MDO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STATOANAL = space(10)
      .w_RDNUMRIL = space(15)
      .w_GLOBALCM = space(15)
      .w_CPROWNUM = 0
      .w_CPROWORD = 0
      .w_RDTIPATT = space(1)
      .w_RDOPERIL = 0
      .w_RDDATRIL = ctod("  /  /  ")
      .w_RDCODESE = space(4)
      .w_RDORARIL = space(8)
      .w_RDDIPEND = space(5)
      .w_RDPROCES = space(1)
      .w_RDCODCOM = space(15)
      .w_RDAGGIMM = space(1)
      .w_RDCODATT = space(15)
      .w_RDDATINI = ctod("  /  /  ")
      .w_RDORAINI = space(8)
      .w_RDDATFIN = ctod("  /  /  ")
      .w_RDORAFIN = space(8)
      .w_RDTEMPOR = 0
      .w_RDSECON = 0
      .w_RDCOGNOM = space(40)
      .w_RDDESCAN = space(30)
      .w_RDDESATT = space(30)
      .w_RDRILAUT = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_UTENTE = 0
      .w_PIANIF = space(1)
      .w_COMMOBS = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_STATOANAL = upper(this.cFunction)
          .DoRTCalc(2,2,.f.)
        .w_GLOBALCM = iif(!empty(.w_RDNUMRIL),g_CODCOM,'')
        .w_CPROWNUM = iif(.w_STATOANAL<>'LOAD',.w_CPROWNUM,1)
        .w_CPROWORD = iif(.w_STATOANAL<>'LOAD',.w_CPROWORD,10)
        .w_RDTIPATT = 'A'
        .w_RDOPERIL = i_CODUTE
        .w_RDDATRIL = i_DATSYS
        .w_RDCODESE = CALCESER(.w_RDDATRIL, g_CODESE)
        .w_RDORARIL = time()
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_RDDIPEND))
          .link_1_11('Full')
          endif
        .w_RDPROCES = 'N'
        .w_RDCODCOM = iif(empty(.w_RDCODCOM),.w_GLOBALCM,.w_RDCODCOM)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_RDCODCOM))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_RDCODATT))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,24,.f.)
        .w_RDRILAUT = 'M'
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DICH_MDO')
    this.DoRTCalc(27,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
      .op_CODAZI = .w_CODAZI
      .op_RDNUMRIL = .w_RDNUMRIL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRDNUMRIL_1_2.enabled = i_bVal
      .Page1.oPag.oRDDATRIL_1_8.enabled = i_bVal
      .Page1.oPag.oRDORARIL_1_10.enabled = i_bVal
      .Page1.oPag.oRDDIPEND_1_11.enabled = i_bVal
      .Page1.oPag.oRDCODCOM_1_13.enabled = i_bVal
      .Page1.oPag.oRDAGGIMM_1_14.enabled = i_bVal
      .Page1.oPag.oRDCODATT_1_15.enabled = i_bVal
      .Page1.oPag.oRDDATINI_1_16.enabled = i_bVal
      .Page1.oPag.oRDORAINI_1_17.enabled = i_bVal
      .Page1.oPag.oRDDATFIN_1_18.enabled = i_bVal
      .Page1.oPag.oRDORAFIN_1_19.enabled = i_bVal
      .Page1.oPag.oRDTEMPOR_1_20.enabled = i_bVal
      .Page1.oPag.oBtn_1_45.enabled = .Page1.oPag.oBtn_1_45.mCond()
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRDNUMRIL_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRDNUMRIL_1_2.enabled = .t.
        .Page1.oPag.oRDDATRIL_1_8.enabled = .t.
        .Page1.oPag.oRDCODCOM_1_13.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DICH_MDO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDNUMRIL,"RDNUMRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPROWNUM,"CPROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPROWORD,"CPROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDTIPATT,"RDTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDOPERIL,"RDOPERIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDATRIL,"RDDATRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDCODESE,"RDCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDORARIL,"RDORARIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDIPEND,"RDDIPEND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDPROCES,"RDPROCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDCODCOM,"RDCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDAGGIMM,"RDAGGIMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDCODATT,"RDCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDATINI,"RDDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDORAINI,"RDORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDDATFIN,"RDDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDORAFIN,"RDORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDTEMPOR,"RDTEMPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDSECON,"RDSECON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDRILAUT,"RDRILAUT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    i_lTable = "DICH_MDO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DICH_MDO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DICH_MDO_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DICH_MDO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DICH_MDO')
        i_extval=cp_InsertValODBCExtFlds(this,'DICH_MDO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RDNUMRIL,CPROWNUM,CPROWORD,RDTIPATT,RDOPERIL"+;
                  ",RDDATRIL,RDCODESE,RDORARIL,RDDIPEND,RDPROCES"+;
                  ",RDCODCOM,RDAGGIMM,RDCODATT,RDDATINI,RDORAINI"+;
                  ",RDDATFIN,RDORAFIN,RDTEMPOR,RDSECON,RDRILAUT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RDNUMRIL)+;
                  ","+cp_ToStrODBC(this.w_CPROWNUM)+;
                  ","+cp_ToStrODBC(this.w_CPROWORD)+;
                  ","+cp_ToStrODBC(this.w_RDTIPATT)+;
                  ","+cp_ToStrODBC(this.w_RDOPERIL)+;
                  ","+cp_ToStrODBC(this.w_RDDATRIL)+;
                  ","+cp_ToStrODBC(this.w_RDCODESE)+;
                  ","+cp_ToStrODBC(this.w_RDORARIL)+;
                  ","+cp_ToStrODBCNull(this.w_RDDIPEND)+;
                  ","+cp_ToStrODBC(this.w_RDPROCES)+;
                  ","+cp_ToStrODBCNull(this.w_RDCODCOM)+;
                  ","+cp_ToStrODBC(this.w_RDAGGIMM)+;
                  ","+cp_ToStrODBCNull(this.w_RDCODATT)+;
                  ","+cp_ToStrODBC(this.w_RDDATINI)+;
                  ","+cp_ToStrODBC(this.w_RDORAINI)+;
                  ","+cp_ToStrODBC(this.w_RDDATFIN)+;
                  ","+cp_ToStrODBC(this.w_RDORAFIN)+;
                  ","+cp_ToStrODBC(this.w_RDTEMPOR)+;
                  ","+cp_ToStrODBC(this.w_RDSECON)+;
                  ","+cp_ToStrODBC(this.w_RDRILAUT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DICH_MDO')
        i_extval=cp_InsertValVFPExtFlds(this,'DICH_MDO')
        cp_CheckDeletedKey(i_cTable,0,'RDNUMRIL',this.w_RDNUMRIL,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable);
              (RDNUMRIL,CPROWNUM,CPROWORD,RDTIPATT,RDOPERIL,RDDATRIL,RDCODESE,RDORARIL,RDDIPEND,RDPROCES,RDCODCOM,RDAGGIMM,RDCODATT,RDDATINI,RDORAINI,RDDATFIN,RDORAFIN,RDTEMPOR,RDSECON,RDRILAUT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RDNUMRIL;
                  ,this.w_CPROWNUM;
                  ,this.w_CPROWORD;
                  ,this.w_RDTIPATT;
                  ,this.w_RDOPERIL;
                  ,this.w_RDDATRIL;
                  ,this.w_RDCODESE;
                  ,this.w_RDORARIL;
                  ,this.w_RDDIPEND;
                  ,this.w_RDPROCES;
                  ,this.w_RDCODCOM;
                  ,this.w_RDAGGIMM;
                  ,this.w_RDCODATT;
                  ,this.w_RDDATINI;
                  ,this.w_RDORAINI;
                  ,this.w_RDDATFIN;
                  ,this.w_RDORAFIN;
                  ,this.w_RDTEMPOR;
                  ,this.w_RDSECON;
                  ,this.w_RDRILAUT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DICH_MDO_IDX,i_nConn)
      *
      * update DICH_MDO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DICH_MDO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
             ",RDTIPATT="+cp_ToStrODBC(this.w_RDTIPATT)+;
             ",RDOPERIL="+cp_ToStrODBC(this.w_RDOPERIL)+;
             ",RDDATRIL="+cp_ToStrODBC(this.w_RDDATRIL)+;
             ",RDCODESE="+cp_ToStrODBC(this.w_RDCODESE)+;
             ",RDORARIL="+cp_ToStrODBC(this.w_RDORARIL)+;
             ",RDDIPEND="+cp_ToStrODBCNull(this.w_RDDIPEND)+;
             ",RDPROCES="+cp_ToStrODBC(this.w_RDPROCES)+;
             ",RDCODCOM="+cp_ToStrODBCNull(this.w_RDCODCOM)+;
             ",RDAGGIMM="+cp_ToStrODBC(this.w_RDAGGIMM)+;
             ",RDCODATT="+cp_ToStrODBCNull(this.w_RDCODATT)+;
             ",RDDATINI="+cp_ToStrODBC(this.w_RDDATINI)+;
             ",RDORAINI="+cp_ToStrODBC(this.w_RDORAINI)+;
             ",RDDATFIN="+cp_ToStrODBC(this.w_RDDATFIN)+;
             ",RDORAFIN="+cp_ToStrODBC(this.w_RDORAFIN)+;
             ",RDTEMPOR="+cp_ToStrODBC(this.w_RDTEMPOR)+;
             ",RDSECON="+cp_ToStrODBC(this.w_RDSECON)+;
             ",RDRILAUT="+cp_ToStrODBC(this.w_RDRILAUT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DICH_MDO')
        i_cWhere = cp_PKFox(i_cTable  ,'RDNUMRIL',this.w_RDNUMRIL  ,'CPROWNUM',this.w_CPROWNUM  )
        UPDATE (i_cTable) SET;
              CPROWORD=this.w_CPROWORD;
             ,RDTIPATT=this.w_RDTIPATT;
             ,RDOPERIL=this.w_RDOPERIL;
             ,RDDATRIL=this.w_RDDATRIL;
             ,RDCODESE=this.w_RDCODESE;
             ,RDORARIL=this.w_RDORARIL;
             ,RDDIPEND=this.w_RDDIPEND;
             ,RDPROCES=this.w_RDPROCES;
             ,RDCODCOM=this.w_RDCODCOM;
             ,RDAGGIMM=this.w_RDAGGIMM;
             ,RDCODATT=this.w_RDCODATT;
             ,RDDATINI=this.w_RDDATINI;
             ,RDORAINI=this.w_RDORAINI;
             ,RDDATFIN=this.w_RDDATFIN;
             ,RDORAFIN=this.w_RDORAFIN;
             ,RDTEMPOR=this.w_RDTEMPOR;
             ,RDSECON=this.w_RDSECON;
             ,RDRILAUT=this.w_RDRILAUT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DICH_MDO_IDX,i_nConn)
      *
      * delete DICH_MDO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RDNUMRIL',this.w_RDNUMRIL  ,'CPROWNUM',this.w_CPROWNUM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DICH_MDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICH_MDO_IDX,2])
    if i_bUpd
      with this
            .w_STATOANAL = upper(this.cFunction)
        .DoRTCalc(2,2,.t.)
        if .o_RDNUMRIL<>.w_RDNUMRIL
            .w_GLOBALCM = iif(!empty(.w_RDNUMRIL),g_CODCOM,'')
        endif
        .DoRTCalc(4,8,.t.)
        if .o_RDDATRIL<>.w_RDDATRIL
            .w_RDCODESE = CALCESER(.w_RDDATRIL, g_CODESE)
        endif
        .DoRTCalc(10,12,.t.)
        if .o_GLOBALCM<>.w_GLOBALCM
            .w_RDCODCOM = iif(empty(.w_RDCODCOM),.w_GLOBALCM,.w_RDCODCOM)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"DICCOM","i_CODAZI,w_RDNUMRIL")
          .op_RDNUMRIL = .w_RDNUMRIL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(14,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRDAGGIMM_1_14.enabled = this.oPgFrm.Page1.oPag.oRDAGGIMM_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RDDIPEND
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDDIPEND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RDDIPEND)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_RDDIPEND))
          select DPCODICE,DPCOGNOM,DPCODUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDDIPEND)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDDIPEND) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oRDDIPEND_1_11'),i_cWhere,'GSAR_BDZ',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDDIPEND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RDDIPEND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_RDDIPEND)
            select DPCODICE,DPCOGNOM,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDDIPEND = NVL(_Link_.DPCODICE,space(5))
      this.w_RDCOGNOM = NVL(_Link_.DPCOGNOM,space(40))
      this.w_UTENTE = NVL(_Link_.DPCODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_RDDIPEND = space(5)
      endif
      this.w_RDCOGNOM = space(40)
      this.w_UTENTE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RDOPERIL=.w_UTENTE OR nvl(.w_UTENTE,0)=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Utente non autorizzato")
        endif
        this.w_RDDIPEND = space(5)
        this.w_RDCOGNOM = space(40)
        this.w_UTENTE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDDIPEND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RDCODCOM
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_acn',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_RDCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_RDCODCOM))
          select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oRDCODCOM_1_13'),i_cWhere,'gsar_acn',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_RDCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_RDCODCOM)
            select CNCODCAN,CNDESCAN,CNFLAGIM,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_RDDESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_RDAGGIMM = NVL(_Link_.CNFLAGIM,space(1))
      this.w_COMMOBS = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RDCODCOM = space(15)
      endif
      this.w_RDDESCAN = space(30)
      this.w_RDAGGIMM = space(1)
      this.w_COMMOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !(empty (.w_COMMOBS) or .w_COMMOBS>i_datsys)
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione, la commessa selezionata � obsoleta")))
          if not(i_bRes)
            this.w_RDCODCOM = space(15)
            this.w_RDDESCAN = space(30)
            this.w_RDAGGIMM = space(1)
            this.w_COMMOBS = ctod("  /  /  ")
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.CNCODCAN as CNCODCAN113"+ ",link_1_13.CNDESCAN as CNDESCAN113"+ ",link_1_13.CNFLAGIM as CNFLAGIM113"+ ",link_1_13.CNDTOBSO as CNDTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on DICH_MDO.RDCODCOM=link_1_13.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and DICH_MDO.RDCODCOM=link_1_13.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RDCODATT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_RDCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_RDCODCOM;
                     ,'ATTIPATT',this.w_RDTIPATT;
                     ,'ATCODATT',trim(this.w_RDCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oRDCODATT_1_15'),i_cWhere,'GSPC_BZZ',"Attivit�",'gspc_zrd.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RDCODCOM<>oSource.xKey(1);
           .or. this.w_RDTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_RDCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_RDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_RDTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_RDCODCOM;
                       ,'ATTIPATT',this.w_RDTIPATT;
                       ,'ATCODATT',this.w_RDCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI,AT_STATO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_RDDESATT = NVL(_Link_.ATDESCRI,space(30))
      this.w_PIANIF = NVL(_Link_.AT_STATO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RDCODATT = space(15)
      endif
      this.w_RDDESATT = space(30)
      this.w_PIANIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=chkutatt(.w_RDCODCOM,.w_RDCODATT,.w_RDOPERIL) and (.w_PIANIF$'C|Z|L' Or .cFunction='Query')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
        endif
        this.w_RDCODATT = space(15)
        this.w_RDDESATT = space(30)
        this.w_PIANIF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRDNUMRIL_1_2.value==this.w_RDNUMRIL)
      this.oPgFrm.Page1.oPag.oRDNUMRIL_1_2.value=this.w_RDNUMRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDOPERIL_1_7.value==this.w_RDOPERIL)
      this.oPgFrm.Page1.oPag.oRDOPERIL_1_7.value=this.w_RDOPERIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDATRIL_1_8.value==this.w_RDDATRIL)
      this.oPgFrm.Page1.oPag.oRDDATRIL_1_8.value=this.w_RDDATRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDORARIL_1_10.value==this.w_RDORARIL)
      this.oPgFrm.Page1.oPag.oRDORARIL_1_10.value=this.w_RDORARIL
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDIPEND_1_11.value==this.w_RDDIPEND)
      this.oPgFrm.Page1.oPag.oRDDIPEND_1_11.value=this.w_RDDIPEND
    endif
    if not(this.oPgFrm.Page1.oPag.oRDPROCES_1_12.RadioValue()==this.w_RDPROCES)
      this.oPgFrm.Page1.oPag.oRDPROCES_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRDCODCOM_1_13.value==this.w_RDCODCOM)
      this.oPgFrm.Page1.oPag.oRDCODCOM_1_13.value=this.w_RDCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRDAGGIMM_1_14.RadioValue()==this.w_RDAGGIMM)
      this.oPgFrm.Page1.oPag.oRDAGGIMM_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRDCODATT_1_15.value==this.w_RDCODATT)
      this.oPgFrm.Page1.oPag.oRDCODATT_1_15.value=this.w_RDCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDATINI_1_16.value==this.w_RDDATINI)
      this.oPgFrm.Page1.oPag.oRDDATINI_1_16.value=this.w_RDDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRDORAINI_1_17.value==this.w_RDORAINI)
      this.oPgFrm.Page1.oPag.oRDORAINI_1_17.value=this.w_RDORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDATFIN_1_18.value==this.w_RDDATFIN)
      this.oPgFrm.Page1.oPag.oRDDATFIN_1_18.value=this.w_RDDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRDORAFIN_1_19.value==this.w_RDORAFIN)
      this.oPgFrm.Page1.oPag.oRDORAFIN_1_19.value=this.w_RDORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRDTEMPOR_1_20.value==this.w_RDTEMPOR)
      this.oPgFrm.Page1.oPag.oRDTEMPOR_1_20.value=this.w_RDTEMPOR
    endif
    if not(this.oPgFrm.Page1.oPag.oRDCOGNOM_1_27.value==this.w_RDCOGNOM)
      this.oPgFrm.Page1.oPag.oRDCOGNOM_1_27.value=this.w_RDCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDESCAN_1_28.value==this.w_RDDESCAN)
      this.oPgFrm.Page1.oPag.oRDDESCAN_1_28.value=this.w_RDDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oRDDESATT_1_29.value==this.w_RDDESATT)
      this.oPgFrm.Page1.oPag.oRDDESATT_1_29.value=this.w_RDDESATT
    endif
    cp_SetControlsValueExtFlds(this,'DICH_MDO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RDNUMRIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDNUMRIL_1_2.SetFocus()
            i_bnoObbl = !empty(.w_RDNUMRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RDDIPEND)) or not(.w_RDOPERIL=.w_UTENTE OR nvl(.w_UTENTE,0)=0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDDIPEND_1_11.SetFocus()
            i_bnoObbl = !empty(.w_RDDIPEND)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Utente non autorizzato")
          case   (empty(.w_RDCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDCODCOM_1_13.SetFocus()
            i_bnoObbl = !empty(.w_RDCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RDCODATT)) or not(chkutatt(.w_RDCODCOM,.w_RDCODATT,.w_RDOPERIL) and (.w_PIANIF$'C|Z|L' Or .cFunction='Query')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDCODATT_1_15.SetFocus()
            i_bnoObbl = !empty(.w_RDCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Stato attivit� non conforme al tipo di movimento o non autorizzata")
          case   not(.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDDATINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RDORAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDORAINI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_RDORAINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDDATFIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RDTEMPOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRDTEMPOR_1_20.SetFocus()
            i_bnoObbl = !empty(.w_RDTEMPOR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RDNUMRIL = this.w_RDNUMRIL
    this.o_GLOBALCM = this.w_GLOBALCM
    this.o_RDDATRIL = this.w_RDDATRIL
    return

  func CanEdit()
    local i_res
    i_res=this.w_RDPROCES<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare. Dichiarazione gi� processata"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_RDPROCES<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare. Dichiarazione gi� processata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgspc_ardPag1 as StdContainer
  Width  = 573
  height = 206
  stdWidth  = 573
  stdheight = 206
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRDNUMRIL_1_2 as StdField with uid="PBTGXEOBTC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RDNUMRIL", cQueryName = "RDNUMRIL",nZero=15,;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero dichiarazione",;
    HelpContextID = 6303390,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=83, Top=15, cSayPict="'999999999999999'", cGetPict="'999999999999999'", InputMask=replicate('X',15)

  add object oRDOPERIL_1_7 as StdField with uid="QCRWJCNHFO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RDOPERIL", cQueryName = "RDOPERIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operatore che ha registrato la dichiarazione",;
    HelpContextID = 15015582,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=521, Top=15, cSayPict='"9999"', cGetPict='"9999"'

  add object oRDDATRIL_1_8 as StdField with uid="XNXUFECXUW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RDDATRIL", cQueryName = "RDDATRIL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dichiarazione",;
    HelpContextID = 315038,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=247, Top=15

  add object oRDORARIL_1_10 as StdField with uid="FTWFNXPDWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RDORARIL", cQueryName = "RDORARIL",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora dichiarazione",;
    HelpContextID = 19078814,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=382, Top=15, cSayPict='"99:99:99"', cGetPict='"99:99:99"', InputMask=replicate('X',8)

  add object oRDDIPEND_1_11 as StdField with uid="RHJXTRWNGH",rtseq=11,rtrep=.t.,;
    cFormVar = "w_RDDIPEND", cQueryName = "RDDIPEND",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Utente non autorizzato",;
    ToolTipText = "Codice dipendente",;
    HelpContextID = 222088870,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=83, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_RDDIPEND"

  func oRDDIPEND_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oRDDIPEND_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRDDIPEND_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oRDDIPEND_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Dipendenti",'GSPC_UTE.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oRDDIPEND_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_RDDIPEND
     i_obj.ecpSave()
  endproc

  add object oRDPROCES_1_12 as StdCheck with uid="PUTWTZPSAM",rtseq=12,rtrep=.f.,left=457, top=42, caption="Processata", enabled=.f.,;
    ToolTipText = "Se attivo: dichiarazione gi� processata",;
    HelpContextID = 12382569,;
    cFormVar="w_RDPROCES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRDPROCES_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRDPROCES_1_12.GetRadio()
    this.Parent.oContained.w_RDPROCES = this.RadioValue()
    return .t.
  endfunc

  func oRDPROCES_1_12.SetRadio()
    this.Parent.oContained.w_RDPROCES=trim(this.Parent.oContained.w_RDPROCES)
    this.value = ;
      iif(this.Parent.oContained.w_RDPROCES=='S',1,;
      0)
  endfunc

  add object oRDCODCOM_1_13 as StdField with uid="HNVVTXNUWJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RDCODCOM", cQueryName = "RDCODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 267837085,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=83, Top=69, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_acn", oKey_1_1="CNCODCAN", oKey_1_2="this.w_RDCODCOM"

  func oRDCODCOM_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
      if .not. empty(.w_RDCODATT)
        bRes2=.link_1_15('Full')
      endif
      if bRes and !(empty (.w_COMMOBS) or .w_COMMOBS>i_datsys)
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione, la commessa selezionata � obsoleta")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oRDCODCOM_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRDCODCOM_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oRDCODCOM_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_acn',"Commesse",'',this.parent.oContained
  endproc
  proc oRDCODCOM_1_13.mZoomOnZoom
    local i_obj
    i_obj=gsar_acn()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_RDCODCOM
     i_obj.ecpSave()
  endproc

  add object oRDAGGIMM_1_14 as StdCheck with uid="UYKFUCUPIF",rtseq=14,rtrep=.f.,left=457, top=69, caption="Agg. immediato",;
    ToolTipText = "Genera immediatamente il movimento di commessa associato",;
    HelpContextID = 164560541,;
    cFormVar="w_RDAGGIMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRDAGGIMM_1_14.RadioValue()
    return(iif(this.value =1,'A',;
    'N'))
  endfunc
  func oRDAGGIMM_1_14.GetRadio()
    this.Parent.oContained.w_RDAGGIMM = this.RadioValue()
    return .t.
  endfunc

  func oRDAGGIMM_1_14.SetRadio()
    this.Parent.oContained.w_RDAGGIMM=trim(this.Parent.oContained.w_RDAGGIMM)
    this.value = ;
      iif(this.Parent.oContained.w_RDAGGIMM=='A',1,;
      0)
  endfunc

  func oRDAGGIMM_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(.cFunction)<>'EDIT')
    endwith
   endif
  endfunc

  add object oRDCODATT_1_15 as StdField with uid="JLWZFRPMZR",rtseq=15,rtrep=.t.,;
    cFormVar = "w_RDCODATT", cQueryName = "RDCODATT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Stato attivit� non conforme al tipo di movimento o non autorizzata",;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 235479402,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=83, Top=96, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_RDCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_RDTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_RDCODATT"

  func oRDCODATT_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oRDCODATT_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRDCODATT_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_RDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_RDTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_RDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_RDTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oRDCODATT_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'gspc_zrd.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oRDCODATT_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_RDCODCOM
    i_obj.ATTIPATT=w_RDTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_RDCODATT
     i_obj.ecpSave()
  endproc

  add object oRDDATINI_1_16 as StdField with uid="HLDXTNKODD",rtseq=16,rtrep=.t.,;
    cFormVar = "w_RDDATINI", cQueryName = "RDDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 151309985,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=86, Top=156

  proc oRDDATINI_1_16.mDefault
    with this.Parent.oContained
      if empty(.w_RDDATINI)
        .w_RDDATINI = i_DATSYS
      endif
    endwith
  endproc

  func oRDDATINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATFIN))
    endwith
    return bRes
  endfunc

  add object oRDORAINI_1_17 as StdField with uid="WOHSTCDPVS",rtseq=17,rtrep=.t.,;
    cFormVar = "w_RDORAINI", cQueryName = "RDORAINI",;
    bObbl = .t. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora inizio",;
    HelpContextID = 170073761,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=165, Top=156, cSayPict='"99:99:99"', cGetPict='"99:99:99"', InputMask=replicate('X',8)

  proc oRDORAINI_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_RDORAINI)
        .w_RDORAINI = time()
      endif
    endwith
  endproc

  add object oRDDATFIN_1_18 as StdField with uid="XCPYFBRUBI",rtseq=18,rtrep=.t.,;
    cFormVar = "w_RDDATFIN", cQueryName = "RDDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine",;
    HelpContextID = 201641628,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=308, Top=156

  proc oRDDATFIN_1_18.mDefault
    with this.Parent.oContained
      if empty(.w_RDDATFIN)
        .w_RDDATFIN = i_DATSYS
      endif
    endwith
  endproc

  func oRDDATFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RDDATINI<=.w_RDDATFIN or empty(.w_RDDATINI))
    endwith
    return bRes
  endfunc

  add object oRDORAFIN_1_19 as StdField with uid="DEAJWBGGPC",rtseq=19,rtrep=.t.,;
    cFormVar = "w_RDORAFIN", cQueryName = "RDORAFIN",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Ora fine",;
    HelpContextID = 220405404,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=387, Top=156, cSayPict='"99:99:99"', cGetPict='"99:99:99"', InputMask=replicate('X',8)

  proc oRDORAFIN_1_19.mDefault
    with this.Parent.oContained
      if empty(.w_RDORAFIN)
        .w_RDORAFIN = time()
      endif
    endwith
  endproc

  add object oRDTEMPOR_1_20 as StdField with uid="VMGPZOYMEV",rtseq=20,rtrep=.t.,;
    cFormVar = "w_RDTEMPOR", cQueryName = "RDTEMPOR",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Tempo da consuntivare",;
    HelpContextID = 40881816,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=86, Top=184, cSayPict='"@Z 999999999.999"', cGetPict='"999999999.999"'

  add object oRDCOGNOM_1_27 as StdField with uid="BTCRUNNXDJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_RDCOGNOM", cQueryName = "RDCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 80141981,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=150, Top=42, InputMask=replicate('X',40)

  add object oRDDESCAN_1_28 as StdField with uid="GFXDYPOCWE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_RDDESCAN", cQueryName = "RDDESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 15675748,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=220, Top=69, InputMask=replicate('X',30)

  add object oRDDESATT_1_29 as StdField with uid="SAYOZTSDQA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RDDESATT", cQueryName = "RDDESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 250556778,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=220, Top=96, InputMask=replicate('X',30)


  add object oObj_1_35 as cp_runprogram with uid="NFWSQXMXFT",left=5, top=247, width=160,height=18,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('DELTAT')",;
    cEvent = "w_RDDATFIN Changed,w_RDDATINI Changed,w_RDORAFIN Changed,w_RDORAINI Changed",;
    nPag=1;
    , HelpContextID = 11403178


  add object oObj_1_36 as cp_runprogram with uid="MWGGAMLDOO",left=5, top=271, width=160,height=18,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('TEMPOCH')",;
    cEvent = "w_RDTEMPOR Changed",;
    nPag=1;
    , HelpContextID = 11403178


  add object oObj_1_41 as cp_runprogram with uid="BLJFFJUTOZ",left=5, top=295, width=160,height=18,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('NEWRCD')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 11403178


  add object oObj_1_43 as cp_runprogram with uid="ZRBAMTAOVC",left=5, top=319, width=160,height=18,;
    caption='GSPC_BRD',;
   bGlobalFont=.t.,;
    prg="GSPC_BRD('IMM_MOV')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 11403178


  add object oBtn_1_45 as StdButton with uid="ZYSAWYWWHP",left=475, top=94, width=50,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il movimento di commessa associato";
    , HelpContextID = 32993546;
    , tabstop=.f.,Caption='\<Commessa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSPC_BRD(this.Parent.oContained,"APREMOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_RDPROCES='S')
      endwith
    endif
  endfunc

  add object oStr_1_22 as StdString with uid="OEYTDCFSGX",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=76, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LDOJEBIAOE",Visible=.t., Left=215, Top=15,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="HSTSAHNZQX",Visible=.t., Left=460, Top=15,;
    Alignment=1, Width=58, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TCCRFRYMLW",Visible=.t., Left=326, Top=15,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IYDGLWNCRL",Visible=.t., Left=3, Top=42,;
    Alignment=1, Width=78, Height=18,;
    Caption="Dipendente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YQNJFPAIXU",Visible=.t., Left=10, Top=184,;
    Alignment=1, Width=72, Height=18,;
    Caption="Tempo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="KFHUOTSHSV",Visible=.t., Left=2, Top=69,;
    Alignment=1, Width=79, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="LXGYDZFZNE",Visible=.t., Left=2, Top=96,;
    Alignment=1, Width=79, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IRJDYQPZDJ",Visible=.t., Left=201, Top=184,;
    Alignment=0, Width=65, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VGGUMBVFDT",Visible=.t., Left=2, Top=131,;
    Alignment=0, Width=170, Height=15,;
    Caption="Rilevazione tempi"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="SOANPWNTTN",Visible=.t., Left=11, Top=156,;
    Alignment=1, Width=71, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="PUZDHDRQWL",Visible=.t., Left=239, Top=156,;
    Alignment=1, Width=65, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oBox_1_40 as StdBox with uid="ONBSXFOCNZ",left=4, top=147, width=559,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_ard','DICH_MDO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RDNUMRIL=DICH_MDO.RDNUMRIL";
  +" and "+i_cAliasName2+".CPROWNUM=DICH_MDO.CPROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
