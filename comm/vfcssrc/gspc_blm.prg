* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_blm                                                        *
*              Lancia movimento da mastrino                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_209]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-28                                                      *
* Last revis.: 2011-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_blm",oParentObject,m.pTIPO)
return(i_retval)

define class tgspc_blm as StdBatch
  * --- Local variables
  pTIPO = space(10)
  w_OBJMOV = .NULL.
  w_PARDOC = space(3)
  w_PARMOV = .f.
  w_GRD = .NULL.
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia il Movimento da Mastrini Movimenti (da GSPC_SMM)
    do case
      case this.pTIPO="Titolo"
        w_ColTitle = IIF(this.oParentObject.w_TIPMOV $ "P-C-I-E-T", AH_MSGFORMAT("Impegnato") , AH_MSGFORMAT("Residuo") )
        i_Col = "7"
        if this.oParentObject.w_ZOOM.GRD.Column&i_Col..hdr.Class="Stdzheader"
          this.oParentObject.w_ZOOM.GRD.Column&i_Col..hdr.Caption = w_ColTitle
        endif
        this.oParentObject.Notifyevent("Reload")
        this.oParentObject.w_TOTPREV = 0
        this.oParentObject.w_TOTCONS = 0
        this.oParentObject.w_TOTIMP = 0
      case this.pTIPO="Lancio"
        * --- Bottone Dettagli - Lancia Documenti ...
        do case
          case this.oParentObject.w_TIPDOC2 = "MOVICONS" Or this.oParentObject.w_TIPDOC2 = "MOVIPREV"
            * --- Era IIF(w_TIPOSTRU<>'P','S','N')+w_TIPMOV
            this.w_PARMOV = "N"+this.oParentObject.w_TIPMOV
            this.w_OBJMOV = GSPC_MAT( this.w_PARMOV )
            * --- Testo subito se l'utente pu� aprire la gestione
            if !( this.w_OBJMOV.bSec1 )
              i_retcode = 'stop'
              return
            endif
            * --- Lancio l'Oggetto in Interrogazione
            this.w_OBJMOV.EcpFilter()     
            this.w_OBJMOV.w_MASERIAL = this.oParentObject.w_SERIAL
            this.w_OBJMOV.EcpSave()     
            oCpToolBar.SetQuery()
          case this.oParentObject.w_TIPDOC2 = "MOVIMAGA"
            GSAR_BZM (this,this.oParentObject.w_SERIAL, -10)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPDOC2 = "DOCUMXXX"
            GSAR_BZM (this,this.oParentObject.w_SERIAL, -20)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case this.pTipo="Chiudi"
        if used("__MOV__")
          use in __MOV__
        endif
      otherwise
        if this.pTipo<>"Totali"
          if used("__MOV__")
            use in __MOV__
          endif
          do GSPC_BSL with this.oParentObject
          if used("__MoV__")
            SELECT MANUMREG,MAALFREG,MADATREG,MATIPMOV,PREV,CONS,IMP,MASERIAL,MACODKEY,; 
 MADESCRI,CPROWNUM,ATCODATT AS MACOATTS,TCDESBRE,SECTION, MATIPDOCF AS MVTIPDOCF,; 
 MACLADOCF AS MVCLADOCF, MAFLVEACF AS MVFLVEACF,MACOSRIC, SPACE( 10 ) AS MATIPMO2 FROM __MoV__ INTO CURSOR __MoT__
          endif
        endif
        * --- A questo punto il cursore con i dati di riga � pronto
        * --- Verifico se il batch viene chiamato dalla visualizza schede (par='V') o dalla visualizza totali (par='T')
        if this.pTIPO="Requery"
          * --- Il batch viene lanciato dalla visualizza
          * --- Metto il Cursore nello Zoom
          this.w_GRD = this.oParentObject.w_ZOOM.Grd
          Select ( this.oParentObject.w_ZOOM.cCursor )
          Zap
          Cur = WrCursor ("__MOT__")
          * --- Inserisco il risultato del cursore all'interno dello zoom
          Select __MOT__
          Go Top
          Scan
          Scatter Memvar
          MATIPMO2 = SPACE( 8 )
          if MATIPMOV ="Mov. Consuntivo"
            MATIPMO2 = "MOVICONS"
          endif
          if MATIPMOV ="Mov. Preventivo"
            MATIPMO2 = "MOVIPREV"
          endif
          if MATIPMOV ="Mov. Magazzino  "
            MATIPMO2 = "MOVIMAGA"
          endif
          if Left ( MATIPMOV ,3 ) = "Doc"
            MATIPMO2 = "DOCUMXXX"
          endif
          Select (this.oParentObject.w_ZOOM.cCursor)
          Append blank
          Gather memvar
          Endscan
          * --- Calcolo i totali
          Select "__MOT__"
          Sum Prev To this.oParentObject.w_TOTPREV
          Sum Cons To this.oParentObject.w_TOTCONS
          Sum Imp To this.oParentObject.w_TOTIMP
          if used("__MOT__")
            use in __MOT__
          endif
          SELECT ( this.oParentObject.w_ZOOM.cCursor )
          Go Top
        else
          * --- Il batch � stato lanciato dalla visualizza totali per tipo di costo
          select TCDESBRE,sum(PREV) as PREV, sum(CONS) as CONS, sum(IMP) as IMP ; 
 from __MOV__ where !empty(nvl(TCDESBRE,"")) group by TCDESBRE ; 
 order by 1 into cursor totali nofilter
          * --- Metto il Cursore nello Zoom
          this.w_GRD = this.oParentObject.w_ZOOMTOT.Grd
          Select ( this.oParentObject.w_ZOOMTOT.cCursor )
          Zap
          Cur = WrCursor ("totali")
          Select Totali
          Go Top
          Scan
          Scatter Memvar
          Select (this.oParentObject.w_ZOOMTOT.cCursor)
          Append blank
          Gather memvar
          Endscan
          if used("totali")
            use in totali
          endif
          SELECT ( this.oParentObject.w_ZOOMTOT.cCursor )
          Go Top
        endif
        this.w_GRD.Refresh()     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("__MOV__")
      use in __MOV__
    endif
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
