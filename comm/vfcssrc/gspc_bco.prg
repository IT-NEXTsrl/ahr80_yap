* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bco                                                        *
*              Configurazione costi                                            *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_106]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-03                                                      *
* Last revis.: 2013-07-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bco",oParentObject,m.pTipo)
return(i_retval)

define class tgspc_bco as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_CODCOS = space(5)
  w_MAT = .f.
  w_ALTPREV = 0
  w_TOTPREV = 0
  w_GENPREV = 0
  w_FATTCOMM = 0
  w_DESCRI = space(30)
  w_MAN = .f.
  w_ALTCONS = 0
  w_TOTCONS = 0
  w_GENCONS = 0
  w_RECNO = 0
  w_APP = .f.
  w_ALTFINE = 0
  w_TOTFINE = 0
  w_GENFINE = 0
  w_MDI = .f.
  w_COLPRI = 0
  w_COLIND = 0
  w_COLCOM = 0
  w_COLMAR = 0
  w_COLDEF = 0
  w_TESDEF = 0
  w_READPAR = space(10)
  w_RICART = space(1)
  GSPC_KCC = .NULL.
  * --- WorkFile variables
  TIPCOSTO_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Configurazione Costi
    * --- Colori di Sfondo
    * --- Colore Costo Primo
    * --- Colore Costo Industriale
    * --- Colore Costo Complessivo
    * --- Colore Margine
    * --- Sfondo di Default (Bianco)
    * --- Colore Testo di Default (Bianco)
    this.w_COLPRI = RGB(191,247,210)
    this.w_COLIND = RGB ( 31,224,164 )
    this.w_COLCOM = RGB ( 55,198,91 )
    this.w_COLMAR = RGB ( 251,243,179 )
    this.w_COLDEF = RGB ( 255,255,255 )
    this.w_TESDEF = RGB(0,0,0)
    this.w_MAT = .F.
    this.w_ALTPREV = 0
    this.w_TOTPREV = 0
    this.w_APP = .F.
    this.w_ALTCONS = 0
    this.w_TOTCONS = 0
    this.w_MAN = .F.
    this.w_ALTFINE = 0
    this.w_TOTFINE = 0
    do case
      case this.pTipo="ZOOM"
        this.oParentObject.w_CALC = .T.
        * --- Recupero i Dati dai Saldi Attivit�. La prima recupera i costi Altri la seconda raggruppa per le altrre 3 categorie
        Vq_Exec ( "..\Comm\Exe\Query\GSPCA1CO.VQR", This , "_Saldi_" )
        * --- Union per costi 'AL'
        Vq_Exec ( "..\Comm\Exe\Query\GSPCABCO.VQR", This , "_SecSal_" )
        * --- Union per costi diversi da 'AL'
        Select Max(Space(30)) As Descrizione, max(100) As Ordine, ;
        Left(_Saldi_.COSTO+Space(5),5) As Costo, ;
        Sum(_Saldi_.PREVEN) as PREVEN, Sum(_Saldi_.CONSUN) as CONSUN, Sum(_Saldi_.FINIRE) as FINIRE, ;
        max(16777215) As ColSfo,max(16777215) As COLTES ;
        From _Saldi_ Group by _Saldi_.COSTO Union All ;
        Select max(Space(30)) As Descrizione, max(100) As Ordine,;
        Left(_SecSal_.COSTO+Space(5),5) As Costo, ;
        sum(_SecSal_.PREVEN) as PREVEN, sum(_SecSal_.CONSUN) as CONSUN, sum(_SecSal_.FINIRE) as FINIRE, ;
        max(16777215) As ColSfo,max(16777215) As COLTES ;
        From _SecSal_ Group By _SecSal_.COSTO Into Cursor _Saldi_ NoFilter
        if Used ("_SecSal_")
          Select _SecSal_
          Use
        endif
        * --- Aggiungo le descrizioni ed evenutali voci a 0 Mancanti
        Cur = WrCursor ( "_Saldi_" )
        Select _Saldi_
        Scan
        do case
          case _Saldi_.Costo="MA"
            * --- Materiali
            this.w_TOTPREV = this.w_TOTPREV+Nvl(_saldi_.PREVEN,0)
            this.w_TOTCONS = this.w_TOTCONS+Nvl(_saldi_.CONSUN,0)
            this.w_TOTFINE = this.w_TOTFINE+Nvl(_saldi_.FINIRE,0)
            this.w_MAT = .T.
            Replace Descrizione With LEFT(ah_MsgFormat("Materiali")+SPACE(30),30)
            Replace Ordine With 1
          case _Saldi_.Costo="MD"
            this.w_TOTPREV = this.w_TOTPREV+Nvl(_saldi_.PREVEN,0)
            this.w_TOTCONS = this.w_TOTCONS+Nvl(_saldi_.CONSUN,0)
            this.w_TOTFINE = this.w_TOTFINE+Nvl(_saldi_.FINIRE,0)
            this.w_MAN = .T.
            Replace Descrizione With LEFT(ah_MsgFormat("Manodopera")+SPACE(30),30)
            Replace Ordine With 2
          case _Saldi_.Costo="AP"
            this.w_TOTPREV = this.w_TOTPREV+Nvl(_saldi_.PREVEN,0)
            this.w_TOTCONS = this.w_TOTCONS+Nvl(_saldi_.CONSUN,0)
            this.w_TOTFINE = this.w_TOTFINE+Nvl(_saldi_.FINIRE,0)
            this.w_APP = .T.
            Replace Descrizione With LEFT(ah_MsgFormat("Appalti")+SPACE(30),30)
            Replace Ordine With 3
          case _Saldi_.Costo="MI"
            this.w_TOTPREV = this.w_TOTPREV+Nvl(_saldi_.PREVEN,0)
            this.w_TOTCONS = this.w_TOTCONS+Nvl(_saldi_.CONSUN,0)
            this.w_TOTFINE = this.w_TOTFINE+Nvl(_saldi_.FINIRE,0)
            this.w_MDI = .T.
            Replace Descrizione With LEFT(ah_MsgFormat("Manodopera ind.")+SPACE(30),30)
            Replace Ordine With 4
          case .T.
            this.w_ALTPREV = this.w_ALTPREV+Nvl( _Saldi_.PREVEN , 0 )
            this.w_ALTCONS = this.w_ALTCONS+Nvl( _Saldi_.CONSUN , 0 )
            this.w_ALTFINE = this.w_ALTFINE+Nvl( _Saldi_.FINIRE , 0 )
            this.w_CODCOS = _Saldi_.COSTO
            * --- Leggo la descrizione del Costo di tipo altro
            this.w_DESCRI = LEFT(ah_MsgFormat("Altri costi")+SPACE(30),30)
            Select _Saldi_
            Replace Descrizione With this.w_DESCRI
            Replace Ordine With 5+RecNo()
        endcase
        Replace ColSfo With this.w_COLDEF
        Replace COLTES With this.w_TESDEF
        Select _Saldi_
        EndScan
        * --- Se Manca qualche voce lo aggiungo a Zero
        if not this.w_MAT
          Append Blank
          Replace Descrizione With LEFT(ah_MsgFormat("Materiali")+SPACE(30),30)
          Replace Ordine With 1
          Replace Costo With "MA   "
          Replace Preven With 0
          Replace Consun With 0
          Replace Finire With 0
          Replace ColSfo With this.w_COLDEF
          Replace COLTES With RGB ( 0,0,0 )
        endif
        if Not this.w_MAN
          Append Blank
          Replace Descrizione With LEFT(ah_MsgFormat("Manodopera")+SPACE(30),30)
          Replace Ordine With 2
          Replace Costo With "MD   "
          Replace Preven With 0
          Replace Consun With 0
          Replace Finire With 0
          Replace ColSfo With this.w_COLDEF
          Replace COLTES With RGB ( 0,0,0 )
        endif
        if Not this.w_APP
          Append Blank
          Replace Descrizione With LEFT(ah_MsgFormat("Appalti")+SPACE(30),30)
          Replace Ordine With 3
          Replace Costo With "AP    "
          Replace Preven With 0
          Replace Consun With 0
          Replace Finire With 0
          Replace ColSfo With this.w_COLDEF
          Replace COLTES With RGB ( 0,0,0 )
        endif
        if Not this.w_MDI
          Append Blank
          Replace Descrizione With LEFT(ah_MsgFormat("Manodopera ind.")+SPACE(30),30)
          Replace Ordine With 2
          Replace Costo With "MI   "
          Replace Preven With 0
          Replace Consun With 0
          Replace Finire With 0
          Replace ColSfo With this.w_COLDEF
          Replace COLTES With RGB ( 0,0,0 )
        endif
        * --- Aggiungo la riga con i totali (Normali e Costo Industriale )
        Select _Saldi_
        Append Blank
        Replace Descrizione With LEFT(ah_MsgFormat("Costo primo")+SPACE(30),30)
        Replace Ordine With 4
        Replace Costo With Space(5)
        Replace Preven With this.w_TOTPREV
        Replace Consun With this.w_TOTCONS
        Replace Finire With this.w_TOTFINE
        * --- Colore per somma Costo primo
        Replace ColSfo With this.w_COLPRI
        Replace COLTES With RGB ( 0,0,0 )
        * --- Aggiungo la riga con i totali (Normali e Costo Industriale )
        Select _Saldi_
        Append Blank
        Replace Descrizione With LEFT(ah_MsgFormat("Costo industriale")+SPACE(30),30)
        Replace Ordine With RecNo()
        Replace Costo With Space(5)
        Replace Preven With this.w_TOTPREV+this.w_ALTPREV
        Replace Consun With this.w_TOTCONS+this.w_ALTCONS
        Replace Finire With this.w_TOTFINE+this.w_ALTFINE
        * --- Colore per somma Costo indutriale
        Replace ColSfo With this.w_COLIND
        Replace COLTES With RGB ( 0,0,0 )
        Select _Saldi_
        this.w_RECNO = RecCount()+1
        * --- Recupero i dati dalla Contabilit� Generale (Cursore _ConGen_)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Analizzo il cursore per veirficare eventuali valori a Zero
        Cur = WrCursor ( "_ConGen_" )
        Select "_ConGen_"
        if RecCount()=0
          Append Blank
          Replace Descrizione With LEFT(ah_MsgFormat("Costo generale")+SPACE(30),30)
          Replace Ordine With this.w_RecNo
          Replace Costo With "PNOTA"
          Replace Preven With 0
          Replace Consun With 0
          Replace Finire With 0
        endif
        * --- Comunque imposto il colore
        Replace ColSfo With this.w_COLDEF
        Replace COLTES With this.w_TESDEF
        this.w_GENPREV = Nvl ( _ConGen_.Preven , 0 )
        this.w_GENCONS = Nvl ( _ConGen_.Consun , 0 )
        this.w_GENFINE = Nvl ( _ConGen_.Finire , 0 )
        * --- Aggiungo Costo complessivo
        Append Blank
        Replace Descrizione With LEFT(ah_MsgFormat("Costo complessivo")+SPACE(30),30)
        Replace Ordine With this.w_RecNo+1
        Replace Costo With Space(5)
        Replace Preven With this.w_TOTPREV+this.w_ALTPREV+this.w_GENPREV
        Replace Consun With this.w_TOTCONS+this.w_ALTCONS+this.w_GENCONS
        Replace Finire With this.w_TOTFINE+this.w_ALTFINE + this.w_GENFINE
        Replace ColSfo With this.w_COLCOM
        Replace COLTES With this.w_TESDEF
        * --- CAlcolo w_FATTCOMM (fatturato della commessa)
        this.w_READPAR = "TAM"
        * --- Read from CPAR_DEF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDRICART"+;
            " from "+i_cTable+" CPAR_DEF where ";
                +"PDCHIAVE = "+cp_ToStrODBC(this.w_READPAR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDRICART;
            from (i_cTable) where;
                PDCHIAVE = this.w_READPAR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RICART = NVL(cp_ToDate(_read_.PDRICART),cp_NullValue(_read_.PDRICART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        Vq_Exec ( "..\Comm\Exe\Query\GSPC_BCO.VQR", This , "_FattComm_" )
        if g_APPLICATION<>"ADHOC REVOLUTION"
           
 SELECT _FattComm_ 
 SUM IIF(MVCODVAL = this.oParentObject.w_VALCOM, IMPORTO,VAL2CAM(IMPORTO,MVCODVAL,this.oParentObject.w_VALCOM,MVCAOVAL,MVDATDOC,this.oParentObject.w_DECTOT)) to L_FATTCOMM
        else
           
 SELECT _FattComm_ 
 SUM IIF(MVCODVAL = this.oParentObject.w_VALCOM, IMPORTO,cp_ROUND(VAL2CAM(IMPORTO,MVCODVAL,this.oParentObject.w_VALCOM,MVCAOVAL,MVDATDOC),this.oParentObject.w_DECTOT)) to L_FATTCOMM
        endif
        this.w_FATTCOMM = Nvl(L_FATTCOMM, 0 )
        * --- Rimuovo i Cursori
        if Used ("_FattComm_")
          Select _FattComm_
          Use
        endif
        Select "_ConGen_"
        * --- Aggiungo margine
        Append Blank
        Replace Descrizione With LEFT(ah_MsgFormat("Margine")+SPACE(30),30)
        Replace Ordine With this.w_RecNo+2
        Replace Costo With Space(5)
        Replace Preven With this.oParentObject.w_IMPCOM - ( this.w_TOTPREV+this.w_ALTPREV+this.w_GENPREV )
        Replace Consun With this.w_FATTCOMM - (this.w_TOTCONS+this.w_ALTCONS+this.w_GENCONS)
        Replace Finire With this.oParentObject.w_IMPCOM - ( this.w_TOTFINE+this.w_ALTFINE + this.w_GENFINE )
        Replace ColSfo With this.w_COLMAR
        Replace COLTES With this.w_TESDEF
        * --- Aggiungo Importo iniziale commessa
        Append Blank
        Replace Descrizione With LEFT(ah_MsgFormat("Valore commessa")+SPACE(30),30)
        Replace Ordine With this.w_RecNo+3
        Replace Costo With Space(5)
        Replace Preven With this.oParentObject.w_IMPCOM
        Replace Consun With this.w_FATTCOMM
        Replace Finire With this.oParentObject.w_IMPCOM
        Replace ColSfo With this.w_COLDEF
        Replace COLTES With this.w_TESDEF
        this.GSPC_KCC = this.oparentobject
        b = this.GSPC_KCC.__dummy__.enabled
        this.GSPC_KCC.__dummy__.enabled = .t.
        this.GSPC_KCC.__dummy__.SetFocus()     
        * --- Posizionamento sul cursore dello zoom
        Select (this.oParentObject.w_ZOOM.cCursor)
        * --- Cancello il contenuto dello Zoom
        Zap
        * --- Metto tutto assieme nel cursore da visualizzare
        Select * From _ConGen_ Union All ;
        Select * From _Saldi_ Order By 2 Into cursor Visu
        * --- Inserisco il risultato del cursore Visu nello zoom
        Select Visu
        Go Top
        Scan
        Scatter Memvar
        Select (this.oParentObject.w_ZOOM.cCursor)
        Append blank
        Gather memvar
        Endscan
        Select (this.oParentObject.w_ZOOM.cCursor)
        Go Top
        * --- Refresho lo Zoom
        this.oParentObject.w_ZOOM.Refresh()     
        this.GSPC_KCC.__dummy__.enabled=.f.
        this.GSPC_KCC.__dummy__.enabled = b
        * --- Rimuovo i Cursori
        if Used ("_ConGen_")
          Select _ConGen_
          Use
        endif
        if Used ("_Saldi_")
          Select _Saldi_
          Use
        endif
        if Used ("Visu")
          Select Visu
          Use
        endif
      case this.pTipo="STAMPA"
        * --- Lancio la stampa
        Select * From ( this.oParentObject.w_ZOOM.cCursor ) Order By Ordine Into Cursor __Tmp__ NoFilter
        L_Format = 40 + VVP 
 L_Commessa = this.oParentObject.w_CODCOM 
 L_CommDesc = this.oParentObject.w_DESCOM 
 L_DatReg = this.oParentObject.w_DATREG 
 L_VALUTA = this.oParentObject.w_VALCOM
        Cp_Chprn ( "..\COMM\EXE\QUERY\GSPC_BCO.FRX","",this.oParentObject )
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Traduco nell'importo della commessa eventuali movimenti in valuta diversa
    Vq_Exec ( "..\Comm\Exe\Query\GSPC2BCO.VQR", This , "Generale" )
    * --- Raggruppo il risultato (Proviene da query in union)
    Select Sum (Nvl(Preven,0)) As Preven , Sum (Nvl(Consun,0)) As Consun, Sum(Nvl(Finire,0)) As Finire, ;
    Codval From Generale group By CodVal Into Cursor Generale NoFilter
    Cur = WrCursor ( "Generale" )
    Scan For CodVal<>this.oParentObject.w_VALCOM
    * --- Se Movimento in valuta diversa lo converto nella valuta della commessa
    * --- L'altra possibile valuta � comunque di conto (Se valuta Commessa LIre allora Euro o viceversa)
    if Nvl(Preven,0)<>0
      Replace Preven With VAL2CAM( Preven , CodVal , this.oParentObject.w_VALCOM , 0 , i_DATSYS )
    endif
    if Nvl(Consun,0)<>0
      Replace Consun With VAL2CAM( Consun , CodVal , this.oParentObject.w_VALCOM , 0 , i_DATSYS )
    endif
    if Nvl(Finire,0)<>0
      Replace Finire With VAL2CAM( Finire , CodVal , this.oParentObject.w_VALCOM , 0 , i_DATSYS )
    endif
    Select Generale
    EndScan
    * --- Raggruppo i dati dalla Contabilit� generale nel cursore risultato
    Select Left(AH_MSGFORMAT("Costo Generale")+Space(30),30) As Descrizione, this.w_RECNO As ordine, "PNOTA" As Costo,;
    Sum (Nvl(Preven,0)) As Preven , Sum (Nvl(Consun,0)) As Consun, Sum(Nvl(Finire,0)) As Finire, ;
    16777215 As ColSfo,16777215 As COLTES From Generale Into Cursor _ConGen_ NoFilter
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TIPCOSTO'
    this.cWorkTables[2]='CPAR_DEF'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
