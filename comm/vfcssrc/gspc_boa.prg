* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_boa                                                        *
*              Stampa documenti non evasi                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_209]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-02                                                      *
* Last revis.: 2006-02-17                                                      *
*                                                                              *
* Batch per la stampa dei documenti OR e DT non evasi                          *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_boa",oParentObject)
return(i_retval)

define class tgspc_boa as StdBatch
  * --- Local variables
  w_TIPCOS = space(2)
  w_CLADOC = space(2)
  w_FLCOCO = space(1)
  w_TIPSORT = space(1)
  w_RESVORIG = 0
  w_RESVCONTO = 0
  w_CAOVAL = 0
  w_DATDOC = ctod("  /  /  ")
  w_DECTOT = 0
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- (lanciato dalla mask GSPC_SOA)
    * --- Variabili globali
    * --- commessa inizio selezione
    * --- commessa fine selezione
    * --- Selezione fornitore
    * --- Data di inizio selezione
    * --- Data di fine selezione
    * --- Tipo di stampa da eseguire
    * --- valuta di conto
    * --- decimali valuta di conto
    * --- Cambio associato al conto
    * --- variabili locali
    * --- selezione codice costo
    * --- Tipo documento (ORdine o DdT)
    * --- Flag aggiorna consuntivo (solo per stampa avanzamenti lavori - #5)
    * --- Tipo di ordinamento per le stampe ordini materiali
    * --- Importo in valuta originale
    * --- Importo convertito in valuta di conto
    * --- Cambio associato al documento
    * --- Data documento
    * --- Decimali valuta di commessa
    * --- Serial per ricerca padre import
    * --- Rownum per ricerca padre import
    * --- NUMDOC per ricerca padre import
    * --- ALFDOC per ricerca padre import
    * --- Preparazione parametri per i diversi tipi di stampe
    * --- Stampa ordini di appalto non evasi
    do case
      case this.oParentObject.w_TIPPRINT=1
        this.w_CLADOC = "OR"
        this.w_FLCOCO = " "
        this.w_TIPCOS = "AP"
        this.w_TIPSORT = "C"
      case this.oParentObject.w_TIPPRINT=2
        * --- Stampa ordini materiali non evasi, ordinati per commessa
        this.w_CLADOC = "OR"
        this.w_FLCOCO = " "
        this.w_TIPCOS = "MA"
        this.w_TIPSORT = "C"
        * --- Stampa ordini materiali non evasi, ordinati per data di evasione
      case this.oParentObject.w_TIPPRINT=3
        this.w_CLADOC = "OR"
        this.w_FLCOCO = " "
        this.w_TIPCOS = "MA"
        this.w_TIPSORT = "D"
        * --- Stampa DDT di acquisto non fatturati
      case this.oParentObject.w_TIPPRINT=4
        this.w_CLADOC = "DT"
        this.w_FLCOCO = " "
        this.w_TIPCOS = " "
        this.w_TIPSORT = "C"
      case this.oParentObject.w_TIPPRINT=5
        * --- Stampa fatture da ricevere stati di avanzamento non fatturati
        this.w_CLADOC = "DI"
        this.w_FLCOCO = "+"
        this.w_TIPCOS = "AP "
        this.w_TIPSORT = "C"
    endcase
    * --- Eseguo la query relativa ai documenti in valuta di conto parzialmente evasi
    Vq_Exec("..\comm\exe\query\GSPC_SOA.vqr",this,"curs0")
    * --- Eseguo la query relativa ai documenti in valuta di conto completamente NON evasi
    Vq_Exec("..\comm\exe\query\GSPC2SOA.vqr",this,"curs2")
    * --- Eseguo la query relativa ai documenti in valuta DIVERSA da quella di conto, parzialmente evasi
    Vq_Exec("..\comm\exe\query\GSPC1SOA.vqr",this,"curs1")
    * --- Eseguo la query relativa ai documenti in valuta DIVERSA da quella di conto, completamente NON evasi
    Vq_Exec("..\comm\exe\query\GSPC3SOA.vqr",this,"curs3")
    * --- Aggiorno il campo residuo ai documenti in valuta di conto
    res=WrCursor("curs0")
    Select "curs0"
    Go Top
    Scan
    this.w_RESVCONTO = RESVORIG
    replace RESVCONT with this.w_RESVCONTO
    EndScan
    res=WrCursor("curs2")
    Select "curs2"
    Go Top
    Scan
    this.w_RESVCONTO = RESVORIG
    replace RESVCONT with this.w_RESVCONTO
    EndScan
    * --- Applico i cambi ai cursori relativi alle valute diverse
    res=WrCursor("curs1")
    Select "curs1"
    Go Top
    Scan
    * --- Preparo i parametri per la conversione della valuta (VAL2MON)
    this.w_RESVORIG = RESVORIG
    this.w_CAOVAL = Nvl(MVCAOVAL,0)
    this.w_DATDOC = Cp_ToDate(MVDATDOC)
    * --- Eseguo la conversione del residuo di riga in valuta di conto
    this.w_RESVCONTO = VAL2MON(this.w_RESVORIG, this.w_CAOVAL, this.oParentObject.w_CAOCONTO, this.w_DATDOC, this.oParentObject.w_VCONTO)
    Replace RESVCONT With this.w_RESVCONTO
    EndScan
    res=WrCursor("curs3")
    Select "curs3"
    Go Top
    Scan
    * --- Preparo i parametri per la conversione della valuta (VAL2MON)
    this.w_RESVORIG = RESVORIG
    this.w_CAOVAL = Nvl(MVCAOVAL,0)
    this.w_DATDOC = Cp_ToDate(MVDATDOC)
    * --- Eseguo la conversione del residuo di riga in valuta di conto
    this.w_RESVCONTO = VAL2MON(this.w_RESVORIG, this.w_CAOVAL, this.oParentObject.w_CAOCONTO, this.w_DATDOC, this.oParentObject.w_VCONTO)
    Replace RESVCONT With this.w_RESVCONTO
    EndScan
    * --- Preparazione macro per parametrizzare order by e group by nelle query
    if this.w_TIPSORT<>"D"
      * --- Ramo THEN: stampe 1,2,4,5 (lanciano il report GSPC_SOA)
      ordertemp="13,1,4,12"
      * --- ordina temp per valuta, commessa, attivit�, data documento
      grouptemp1="MVCODCOM,MVCODVAL"
      * --- Raggruppa temp1 per commessa e per valuta
      orderTMP="1,26,4,2,3,13"
      * --- Ordina __TMP__ per commessa, sezione,attivit�, num doc, alfa doc e data doc
      if this.oParentObject.w_TIPPRINT<>2
        launchreport="CP_CHPRN('..\Comm\Exe\Query\Gspc_soa.Frx', ' ', this)"
      else
        launchreport="CP_CHPRN('..\Comm\Exe\Query\Gspc2soa.Frx', ' ', this)"
      endif
      * --- Report da lanciare
    else
      * --- Ramo ELSE: stampa 3 (materiali ord. per data evasione), lanciano il report GSPC3SOA
      ordertemp="19"
      * --- ordina temp per data di evasione prevista
      grouptemp1="MVCODVAL"
      * --- Raggruppa temp1 per valuta
      orderTMP="26,19"
      * --- Ordina __TMP__ per sezione e data di evasione prevista
      launchreport="CP_CHPRN('..\Comm\Exe\Query\Gspc3soa.Frx', ' ', this)"
      * --- Report da lanciare
    endif
    * --- Metto assieme i 4 cursori (preparazione sezione dettagli report)
    * --- ordinati per valuta,commessa,data
    Select curs0.* from curs0 ;
    union all ;
    Select curs2.* from curs2 ;
    union all ;
    Select curs1.* from curs1 ;
    union all ;
    Select curs3.* from curs3 Order By &ordertemp ;
    into cursor temp nofilter
    * --- Aggiornamento dei riferimenti al padre origine di import
    * --- N.B.: non pu� essere fatto direttamente nelle query iniziali, perch� occorre inserire due nuove tabelle documenti
    * --- e verrebbero dei problemi di lunghezza nei campi che calcolano gli importi per evitare errori di campi ambigui in SQL
    select "temp"
    cur=wrcursor("temp")
    go top
    scan
    this.w_SERIAL = MVSERIAL
    this.w_ROWORD = CPROWORD1
    Vq_Exec("..\comm\exe\query\GSPC4SOA.vqr",this,"curs4")
    select "curs4"
    this.w_NUMDOC = MVNUMDOC1
    this.w_ALFDOC = MVALFDOC1
    this.w_ROWORD = CPROWORD1
    select "temp"
    replace MVNUMDOC1 with this.w_NUMDOC
    replace CPROWORD1 with this.w_ROWORD
    replace MVALFDOC1 with this.w_ALFDOC
    replace prezzo with cp_round(prezzo,vadectot)
    replace evaso with cp_round(evaso,vadectot)
    replace resvcont with cp_round(resvcont,g_perpvl)
    endscan
    * --- Preparazione della sezione totali del report
    * --- Il campo MVSERIAL � stato inserito per evitare di ripetere i dati di testata nel report
    * --- (cfr. grouping su MVSERIAL nel .frx)
    Select min(MVCODCOM) as MVCODCOM, min(MVNUMDOC) as MVNUMDOC, min(MVALFDOC) as MVALFDOC, ;
    min(MVCODATT) as MVCODATT, min(ANDESCRI) as ANDESCRI, min(MVCODART) as MVCODART, ;
    sum(PREZZO) as PREZZO,sum(EVASO) as EVASO,sum(RESVORIG) as RESVORIG,sum(RESVCONT) as RESVCONT,;
    min(MVTIPRIG) as MVTIPRIG, min(MVCAOVAL) as MVCAOVAL, min(MVDATDOC) as MVDATDOC, ;
    MVCODVAL, min(VADECTOT) as VADECTOT, min(VASIMVAL) as VASIMVAL, ;
    "XXXXXXXXXX" as MVSERIAL, min(QTAEVAD) as QTAEVAD, min(MVDATEVA) as MVDATEVA, ;
    min(MVDESART) as MVDESART,min(MVQTAUM1) as MVQTAUM1, min(MVQTAEV1) as MVQTAEV1, ;
    min (MVNUMDOC1) as MVNUMDOC1, min(MVALFDOC1) as MVALFDOC1, min(CPROWORD1) as CPROWORD1 ;
    From temp Group By &grouptemp1 Into Cursor temp1 NoFilter
    * --- Preparazione del cursore da passare al report (ordinati per commessa,sezione D o T)
    Select temp.*,"D" as SEZIONE From temp ;
    Union all;
    Select temp1.*,"T" as SEZIONE From temp1 ;
    Order By &orderTMP Into Cursor __TMP__ NoFilter
    * --- lancio la stampa
    l_CODCOM1=this.oParentObject.w_CODCOM1
    l_CODCOM2=this.oParentObject.w_CODCOM2
    l_FORNITORE=this.oParentObject.w_FORNITORE
    l_FDESCRI=this.oParentObject.w_FDESCRI
    l_DATAINI=this.oParentObject.w_DATAINI
    l_DATAFIN=this.oParentObject.w_DATAFIN
    l_TIPCOS=this.w_TIPCOS
    l_CLADOC=this.w_CLADOC
    l_TIPPRINT=this.oParentObject.w_TIPPRINT
    l_caoconto=this.oParentObject.w_CAOCONTO
    l_vconto=this.oParentObject.w_VCONTO
    &launchreport
    * --- rilascio i cursori
    if used("curs")
      select("curs")
      Use
    endif
    if used("curs1")
      select("curs1")
      Use
    endif
    if used("curs2")
      select("curs2")
      Use
    endif
    if used("curs3")
      select("curs3")
      Use
    endif
    if used("curs4")
      select("curs4")
      Use
    endif
    if used("temp")
      select("temp")
      Use
    endif
    if used("temp1")
      select("temp1")
      Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
