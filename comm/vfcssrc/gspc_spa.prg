* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_spa                                                        *
*              Stampa autorizzazioni                                           *
*                                                                              *
*      Author: Zucchetti Spa CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_72]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-16                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_spa",oParentObject))

* --- Class definition
define class tgspc_spa as StdForm
  Top    = 16
  Left   = 15

  * --- Standard Properties
  Width  = 491
  Height = 252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=141187177
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cpgroups_IDX = 0
  cpusers_IDX = 0
  cPrg = "gspc_spa"
  cComment = "Stampa autorizzazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TAATTIVI = space(1)
  w_COMMINI = space(15)
  o_COMMINI = space(15)
  w_COMMFIN = space(15)
  o_COMMFIN = space(15)
  w_DESCOMF = space(30)
  w_ATTINI = space(15)
  w_ATTFIN = space(15)
  w_CODGRP = 0
  w_CODUTE = 0
  w_DESATTI = space(30)
  w_DESCOMI = space(30)
  w_DESATTF = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DESUTE = space(20)
  w_DESGRP = space(20)
  w_DIRITTI = space(10)
  o_DIRITTI = space(10)
  w_FREQUENTE = space(1)
  w_AUTORIZZATA = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_spaPag1","gspc_spa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOMMINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='cpgroups'
    this.cWorkTables[4]='cpusers'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TAATTIVI=space(1)
      .w_COMMINI=space(15)
      .w_COMMFIN=space(15)
      .w_DESCOMF=space(30)
      .w_ATTINI=space(15)
      .w_ATTFIN=space(15)
      .w_CODGRP=0
      .w_CODUTE=0
      .w_DESATTI=space(30)
      .w_DESCOMI=space(30)
      .w_DESATTF=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESUTE=space(20)
      .w_DESGRP=space(20)
      .w_DIRITTI=space(10)
      .w_FREQUENTE=space(1)
      .w_AUTORIZZATA=space(1)
        .w_TAATTIVI = 'A'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_COMMINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_COMMFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_ATTINI = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ATTINI))
          .link_1_6('Full')
        endif
        .w_ATTFIN = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ATTFIN))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODGRP))
          .link_1_8('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODUTE))
          .link_1_9('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
          .DoRTCalc(9,11,.f.)
        .w_OBTEST = I_DATSYS
          .DoRTCalc(13,14,.f.)
        .w_DIRITTI = 'T'
        .w_FREQUENTE = IIF(.w_DIRITTI$'F','S','N')
        .w_AUTORIZZATA = IIF(.w_DIRITTI$'FA','S',IIF(.w_DIRITTI='N','N',' '))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_COMMINI<>.w_COMMINI.or. .o_COMMFIN<>.w_COMMFIN
            .w_ATTINI = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
          .link_1_6('Full')
        endif
        if .o_COMMFIN<>.w_COMMFIN.or. .o_COMMINI<>.w_COMMINI
            .w_ATTFIN = IIF(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN),'               ',.w_ATTINI)
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(7,15,.t.)
        if .o_DIRITTI<>.w_DIRITTI
            .w_FREQUENTE = IIF(.w_DIRITTI$'F','S','N')
        endif
        if .o_DIRITTI<>.w_DIRITTI
            .w_AUTORIZZATA = IIF(.w_DIRITTI$'FA','S',IIF(.w_DIRITTI='N','N',' '))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTINI_1_6.enabled = this.oPgFrm.Page1.oPag.oATTINI_1_6.mCond()
    this.oPgFrm.Page1.oPag.oATTFIN_1_7.enabled = this.oPgFrm.Page1.oPag.oATTFIN_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODGRP_1_8.enabled = this.oPgFrm.Page1.oPag.oCODGRP_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODUTE_1_9.enabled = this.oPgFrm.Page1.oPag.oCODUTE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COMMINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMINI)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMINI_1_2'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMMINI = space(15)
      endif
      this.w_DESCOMI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_COMMFIN)or .w_COMMINI<=.w_COMMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMMINI = space(15)
        this.w_DESCOMI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMFIN)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMFIN_1_3'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMMFIN = space(15)
      endif
      this.w_DESCOMF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_COMMINI)or .w_COMMINI<=.w_COMMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMMFIN = space(15)
        this.w_DESCOMF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTINI)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_TAATTIVI;
                     ,'ATCODATT',trim(this.w_ATTINI))
          select ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTINI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTINI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTINI_1_6'),i_cWhere,'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TAATTIVI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTINI);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_TAATTIVI;
                       ,'ATCODATT',this.w_ATTINI)
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTINI = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTI = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTINI = space(15)
      endif
      this.w_DESATTI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATTFIN) or .w_ATTINI<=.w_ATTFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTINI = space(15)
        this.w_DESATTI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTFIN)+"%");
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);

          i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATTIPATT',this.w_TAATTIVI;
                     ,'ATCODATT',trim(this.w_ATTFIN))
          select ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTFIN)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTFIN) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTFIN_1_7'),i_cWhere,'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TAATTIVI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',oSource.xKey(1);
                       ,'ATCODATT',oSource.xKey(2))
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTFIN);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TAATTIVI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATTIPATT',this.w_TAATTIVI;
                       ,'ATCODATT',this.w_ATTFIN)
            select ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTFIN = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTF = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATTFIN = space(15)
      endif
      this.w_DESATTF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ATTINI)or .w_ATTINI<=.w_ATTFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTFIN = space(15)
        this.w_DESATTF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRP
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','CODE',cp_AbsName(oSource.parent,'oCODGRP_1_8'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRP = NVL(_Link_.CODE,0)
      this.w_DESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRP = 0
      endif
      this.w_DESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_9'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOMMINI_1_2.value==this.w_COMMINI)
      this.oPgFrm.Page1.oPag.oCOMMINI_1_2.value=this.w_COMMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMFIN_1_3.value==this.w_COMMFIN)
      this.oPgFrm.Page1.oPag.oCOMMFIN_1_3.value=this.w_COMMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMF_1_4.value==this.w_DESCOMF)
      this.oPgFrm.Page1.oPag.oDESCOMF_1_4.value=this.w_DESCOMF
    endif
    if not(this.oPgFrm.Page1.oPag.oATTINI_1_6.value==this.w_ATTINI)
      this.oPgFrm.Page1.oPag.oATTINI_1_6.value=this.w_ATTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTFIN_1_7.value==this.w_ATTFIN)
      this.oPgFrm.Page1.oPag.oATTFIN_1_7.value=this.w_ATTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRP_1_8.value==this.w_CODGRP)
      this.oPgFrm.Page1.oPag.oCODGRP_1_8.value=this.w_CODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_9.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_9.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATTI_1_13.value==this.w_DESATTI)
      this.oPgFrm.Page1.oPag.oDESATTI_1_13.value=this.w_DESATTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMI_1_16.value==this.w_DESCOMI)
      this.oPgFrm.Page1.oPag.oDESCOMI_1_16.value=this.w_DESCOMI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATTF_1_18.value==this.w_DESATTF)
      this.oPgFrm.Page1.oPag.oDESATTF_1_18.value=this.w_DESATTF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_23.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_23.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_1_24.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_1_24.value=this.w_DESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRITTI_1_26.RadioValue()==this.w_DIRITTI)
      this.oPgFrm.Page1.oPag.oDIRITTI_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_COMMFIN)or .w_COMMINI<=.w_COMMFIN))  and not(empty(.w_COMMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMMINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_COMMINI)or .w_COMMINI<=.w_COMMFIN))  and not(empty(.w_COMMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMMFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_ATTFIN) or .w_ATTINI<=.w_ATTFIN)  and (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))  and not(empty(.w_ATTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_ATTINI)or .w_ATTINI<=.w_ATTFIN)  and (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))  and not(empty(.w_ATTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COMMINI = this.w_COMMINI
    this.o_COMMFIN = this.w_COMMFIN
    this.o_DIRITTI = this.w_DIRITTI
    return

enddefine

* --- Define pages as container
define class tgspc_spaPag1 as StdContainer
  Width  = 487
  height = 252
  stdWidth  = 487
  stdheight = 252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOMMINI_1_2 as StdField with uid="AEWJVMZWMY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COMMINI", cQueryName = "COMMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 175622950,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=128, Top=6, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMINI"

  func oCOMMINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCOMMFIN_1_3 as StdField with uid="XIGYTOJLVH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COMMFIN", cQueryName = "COMMFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 179844314,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=128, Top=29, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMFIN"

  proc oCOMMFIN_1_3.mDefault
    with this.Parent.oContained
      if empty(.w_COMMFIN)
        .w_COMMFIN = .w_COMMINI
      endif
    endwith
  endproc

  func oCOMMFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDESCOMF_1_4 as StdField with uid="LCFOJESBCV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCOMF", cQueryName = "DESCOMF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 164503862,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=249, Top=29, InputMask=replicate('X',30)

  add object oATTINI_1_6 as StdField with uid="VOGDDXFEHR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ATTINI", cQueryName = "ATTINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di inizio selezione",;
    HelpContextID = 171687930,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=128, Top=52, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_TAATTIVI", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTINI"

  func oATTINI_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))
    endwith
   endif
  endfunc

  func oATTINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TAATTIVI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TAATTIVI)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oATTFIN_1_7 as StdField with uid="TPINPKCIJU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATTFIN", cQueryName = "ATTFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di fine selezione",;
    HelpContextID = 93241338,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=128, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATTIPATT", oKey_1_2="this.w_TAATTIVI", oKey_2_1="ATCODATT", oKey_2_2="this.w_ATTFIN"

  func oATTFIN_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_COMMINI<>.w_COMMFIN OR empty(.w_COMMINI) OR empty(.w_COMMFIN)))
    endwith
   endif
  endfunc

  func oATTFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TAATTIVI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TAATTIVI)
    endif
    do cp_zoom with 'ATTIVITA','*','ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC1AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oCODGRP_1_8 as StdField with uid="RFQLSTNWGG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODGRP", cQueryName = "CODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 50250970,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=128, Top=98, bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="CODE", oKey_1_2="this.w_CODGRP"

  func oCODGRP_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODUTE = 0)
    endwith
   endif
  endfunc

  func oCODGRP_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRP_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRP_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','CODE',cp_AbsName(this.parent,'oCODGRP_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc

  add object oCODUTE_1_9 as StdField with uid="TVLQUIQYIO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 231785690,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=128, Top=124, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODGRP = 0)
    endwith
   endif
  endfunc

  func oCODUTE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=128, top=177, width=350,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 36810470


  add object oBtn_1_11 as StdButton with uid="IDQLKXKZHR",left=377, top=203, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 267833050;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="UKWSGSSBLX",left=428, top=203, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 50241258;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESATTI_1_13 as StdField with uid="AELSWDKMAZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESATTI", cQueryName = "DESATTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 18620726,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=249, Top=52, InputMask=replicate('X',30)

  add object oDESCOMI_1_16 as StdField with uid="WPUVWCJTXN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCOMI", cQueryName = "DESCOMI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 164503862,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=249, Top=6, InputMask=replicate('X',30)

  add object oDESATTF_1_18 as StdField with uid="ICAQHTUJID",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESATTF", cQueryName = "DESATTF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 18620726,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=249, Top=75, InputMask=replicate('X',30)

  add object oDESUTE_1_23 as StdField with uid="PJRGCYTKLS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 231726794,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=170, Top=124, InputMask=replicate('X',20)

  add object oDESGRP_1_24 as StdField with uid="DLRUBJHIJJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 50192074,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=170, Top=98, InputMask=replicate('X',20)


  add object oDIRITTI_1_26 as StdCombo with uid="CWVDUOCRPJ",rtseq=15,rtrep=.f.,left=128,top=151,width=120,height=22;
    , ToolTipText = "Definizione diritti";
    , HelpContextID = 19141942;
    , cFormVar="w_DIRITTI",RowSource=""+"Tutte,"+"Frequente,"+"Autorizzata,"+"Non autorizzata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIRITTI_1_26.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    space(10))))))
  endfunc
  func oDIRITTI_1_26.GetRadio()
    this.Parent.oContained.w_DIRITTI = this.RadioValue()
    return .t.
  endfunc

  func oDIRITTI_1_26.SetRadio()
    this.Parent.oContained.w_DIRITTI=trim(this.Parent.oContained.w_DIRITTI)
    this.value = ;
      iif(this.Parent.oContained.w_DIRITTI=='T',1,;
      iif(this.Parent.oContained.w_DIRITTI=='F',2,;
      iif(this.Parent.oContained.w_DIRITTI=='A',3,;
      iif(this.Parent.oContained.w_DIRITTI=='N',4,;
      0))))
  endfunc

  add object oStr_1_5 as StdString with uid="PDIZSVTEPC",Visible=.t., Left=41, Top=30,;
    Alignment=1, Width=79, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BPUHPITAMJ",Visible=.t., Left=40, Top=54,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PGZKNKALVF",Visible=.t., Left=34, Top=178,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FYTEGOOWAF",Visible=.t., Left=32, Top=6,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="SEICYKOMFL",Visible=.t., Left=57, Top=76,;
    Alignment=1, Width=63, Height=15,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="URGBQORPKN",Visible=.t., Left=68, Top=125,;
    Alignment=1, Width=52, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BLURCDTHHL",Visible=.t., Left=76, Top=100,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="CQXSMJMFJI",Visible=.t., Left=5, Top=154,;
    Alignment=1, Width=115, Height=18,;
    Caption="Definizione diritti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gspc_spa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
