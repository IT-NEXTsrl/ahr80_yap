* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_acm                                                        *
*              Conti                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_112]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-29                                                      *
* Last revis.: 2018-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgspc_acm"))

* --- Class definition
define class tgspc_acm as StdForm
  Top    = 14
  Left   = 19

  * --- Standard Properties
  Width  = 647
  Height = 202+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-11-05"
  HelpContextID=158705559
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  CPAR_DEF_IDX = 0
  ATT_PREC_IDX = 0
  cFile = "ATTIVITA"
  cKeySelect = "ATCODCOM,ATTIPATT,ATCODATT"
  cQueryFilter="ATTIPATT='P' And ATTIPCOM In  ('P','C')"
  cKeyWhere  = "ATCODCOM=this.w_ATCODCOM and ATTIPATT=this.w_ATTIPATT and ATCODATT=this.w_ATCODATT"
  cKeyWhereODBC = '"ATCODCOM="+cp_ToStrODBC(this.w_ATCODCOM)';
      +'+" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT)';
      +'+" and ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cKeyWhereODBCqualified = '"ATTIVITA.ATCODCOM="+cp_ToStrODBC(this.w_ATCODCOM)';
      +'+" and ATTIVITA.ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT)';
      +'+" and ATTIVITA.ATCODATT="+cp_ToStrODBC(this.w_ATCODATT)';

  cPrg = "gspc_acm"
  cComment = "Conti"
  icon = "anag.ico"
  cAutoZoom = 'GSPCPACM'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPAR = space(1)
  w_DEFCOM = space(15)
  w_USEPRJ = space(1)
  w_ATCODCOM = space(15)
  o_ATCODCOM = space(15)
  w_ATTIPATT = space(1)
  w_ATTIPCOM = space(1)
  o_ATTIPCOM = space(1)
  w_ATCODATT = space(15)
  w_ATDESCRI = space(30)
  w_AT_PRJVW = space(1)
  w_PRJVWSVC = space(1)
  w_ATDESSUP = space(0)
  w_DESCOM = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_ATFLPREV = space(1)
  w_ATDURGIO = 0
  o_ATDURGIO = 0
  w_ATVINCOL = space(3)
  w_ATDATVIN = ctod('  /  /  ')
  o_ATDATVIN = ctod('  /  /  ')
  w_ATDATINI = ctod('  /  /  ')
  o_ATDATINI = ctod('  /  /  ')
  w_ATDATFIN = ctod('  /  /  ')
  o_ATDATFIN = ctod('  /  /  ')
  w_ATPERCOM = 0
  w_ATCARDIN = space(1)
  w_MSPROJ = space(1)
  w_PROGR = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_UNIQUE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ATTIVITA','gspc_acm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgspc_acmPag1","gspc_acm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati generali")
      .Pages(1).HelpContextID = 102181388
      .Pages(2).addobject("oPag","tgspc_acmPag2","gspc_acm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tempificazione")
      .Pages(2).HelpContextID = 221056343
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATCODCOM_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CPAR_DEF'
    this.cWorkTables[3]='ATT_PREC'
    this.cWorkTables[4]='ATTIVITA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ATTIVITA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ATTIVITA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATCODCOM = NVL(ATCODCOM,space(15))
      .w_ATTIPATT = NVL(ATTIPATT,space(1))
      .w_ATCODATT = NVL(ATCODATT,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ATTIVITA where ATCODCOM=KeySet.ATCODCOM
    *                            and ATTIPATT=KeySet.ATTIPATT
    *                            and ATCODATT=KeySet.ATCODATT
    *
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ATTIVITA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ATTIVITA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ATTIVITA '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR = 'TAM'
        .w_DEFCOM = space(15)
        .w_USEPRJ = space(1)
        .w_PRJVWSVC = 'N'
        .w_DESCOM = space(30)
        .w_OBTEST = i_DATSYS
        .w_PROGR = space(1)
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_UNIQUE = "S"
          .link_1_1('Load')
        .w_ATCODCOM = NVL(ATCODCOM,space(15))
          if link_1_4_joined
            this.w_ATCODCOM = NVL(CNCODCAN104,NVL(this.w_ATCODCOM,space(15)))
            this.w_DESCOM = NVL(CNDESCAN104,space(30))
          else
          .link_1_4('Load')
          endif
        .w_ATTIPATT = NVL(ATTIPATT,space(1))
        .w_ATTIPCOM = NVL(ATTIPCOM,space(1))
        .w_ATCODATT = NVL(ATCODATT,space(15))
        .w_ATDESCRI = NVL(ATDESCRI,space(30))
        .w_AT_PRJVW = NVL(AT_PRJVW,space(1))
        .w_ATDESSUP = NVL(ATDESSUP,space(0))
        .w_ATFLPREV = NVL(ATFLPREV,space(1))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_ATDURGIO = NVL(ATDURGIO,0)
        .w_ATVINCOL = NVL(ATVINCOL,space(3))
        .w_ATDATVIN = NVL(cp_ToDate(ATDATVIN),ctod("  /  /  "))
        .w_ATDATINI = NVL(cp_ToDate(ATDATINI),ctod("  /  /  "))
        .w_ATDATFIN = NVL(cp_ToDate(ATDATFIN),ctod("  /  /  "))
        .w_ATPERCOM = NVL(ATPERCOM,0)
        .w_ATCARDIN = NVL(ATCARDIN,space(1))
        .w_MSPROJ = .w_USEPRJ
        cp_LoadRecExtFlds(this,'ATTIVITA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gspc_acm
    * --- Propone l'ultima commessa Inserita
    p_OLDCOMM=space(15)
    if p_OLDCOMM<>this.w_ATCODCOM
       p_OLDCOMM=this.w_ATCODCOM
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR = space(1)
      .w_DEFCOM = space(15)
      .w_USEPRJ = space(1)
      .w_ATCODCOM = space(15)
      .w_ATTIPATT = space(1)
      .w_ATTIPCOM = space(1)
      .w_ATCODATT = space(15)
      .w_ATDESCRI = space(30)
      .w_AT_PRJVW = space(1)
      .w_PRJVWSVC = space(1)
      .w_ATDESSUP = space(0)
      .w_DESCOM = space(30)
      .w_OBTEST = ctod("  /  /  ")
      .w_ATFLPREV = space(1)
      .w_ATDURGIO = 0
      .w_ATVINCOL = space(3)
      .w_ATDATVIN = ctod("  /  /  ")
      .w_ATDATINI = ctod("  /  /  ")
      .w_ATDATFIN = ctod("  /  /  ")
      .w_ATPERCOM = 0
      .w_ATCARDIN = space(1)
      .w_MSPROJ = space(1)
      .w_PROGR = space(1)
      .w_DATINI = ctod("  /  /  ")
      .w_DATFIN = ctod("  /  /  ")
      .w_UNIQUE = space(1)
      if .cFunction<>"Filter"
        .w_READPAR = 'TAM'
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_READPAR))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_ATCODCOM = IIF(p_OLDCOMM<>space(15), p_OLDCOMM, .w_DEFCOM)
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_ATCODCOM))
          .link_1_4('Full')
          endif
        .w_ATTIPATT = 'P'
        .w_ATTIPCOM = 'C'
        .w_ATCODATT = IIF(.w_ATTIPCOM='P', .w_ATCODCOM, .w_ATCODATT)
          .DoRTCalc(8,8,.f.)
        .w_AT_PRJVW = 'S'
        .w_PRJVWSVC = 'N'
          .DoRTCalc(11,12,.f.)
        .w_OBTEST = i_DATSYS
        .w_ATFLPREV = 'N'
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_ATDURGIO = 1
        .w_ATVINCOL = IIF(EMPTY(.w_ATDATVIN), IIF(.w_PROGR='A', 'PPP', 'PTP'), .w_ATVINCOL)
          .DoRTCalc(17,17,.f.)
        .w_ATDATINI = iif(.w_PROGR='I',iif(empty(.w_ATDATFIN),nvl(.w_DATFIN,i_DATSYS),.w_ATDATFIN)-(.w_ATDURGIO-1),max(NVL(.w_DATINI,i_DATSYS),i_DATSYS))
        .w_ATDATFIN = iif(.w_PROGR='A',.w_ATDATINI+(.w_ATDURGIO-1),IIF(EMPTY(.w_ATDATFIN),NVL(.w_DATFIN,i_DATSYS),.w_ATDATFIN))
          .DoRTCalc(20,20,.f.)
        .w_ATCARDIN = 'N'
        .w_MSPROJ = .w_USEPRJ
          .DoRTCalc(23,25,.f.)
        .w_UNIQUE = "S"
      endif
    endwith
    cp_BlankRecExtFlds(this,'ATTIVITA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATCODCOM_1_4.enabled = i_bVal
      .Page1.oPag.oATTIPCOM_1_6.enabled = i_bVal
      .Page1.oPag.oATCODATT_1_8.enabled = i_bVal
      .Page1.oPag.oATDESCRI_1_10.enabled = i_bVal
      .Page1.oPag.oAT_PRJVW_1_11.enabled = i_bVal
      .Page1.oPag.oATDESSUP_1_14.enabled = i_bVal
      .Page2.oPag.oATDURGIO_2_1.enabled = i_bVal
      .Page2.oPag.oATVINCOL_2_2.enabled = i_bVal
      .Page2.oPag.oATDATVIN_2_3.enabled = i_bVal
      .Page2.oPag.oATDATINI_2_4.enabled = i_bVal
      .Page2.oPag.oATDATFIN_2_5.enabled = i_bVal
      .Page2.oPag.oATPERCOM_2_6.enabled = i_bVal
      .Page2.oPag.oATCARDIN_2_7.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oATCODCOM_1_4.enabled = .f.
        .Page1.oPag.oATCODATT_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oATCODCOM_1_4.enabled = .t.
        .Page1.oPag.oATCODATT_1_8.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ATTIVITA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODCOM,"ATCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPATT,"ATTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPCOM,"ATTIPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESCRI,"ATDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_PRJVW,"AT_PRJVW",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESSUP,"ATDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLPREV,"ATFLPREV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDURGIO,"ATDURGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATVINCOL,"ATVINCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATVIN,"ATDATVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATINI,"ATDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFIN,"ATDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERCOM,"ATPERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCARDIN,"ATCARDIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    i_lTable = "ATTIVITA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ATTIVITA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSPC_SES with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ATTIVITA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ATTIVITA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ATTIVITA')
        i_extval=cp_InsertValODBCExtFlds(this,'ATTIVITA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATCODCOM,ATTIPATT,ATTIPCOM,ATCODATT,ATDESCRI"+;
                  ",AT_PRJVW,ATDESSUP,ATFLPREV,ATDURGIO,ATVINCOL"+;
                  ",ATDATVIN,ATDATINI,ATDATFIN,ATPERCOM,ATCARDIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ATCODCOM)+;
                  ","+cp_ToStrODBC(this.w_ATTIPATT)+;
                  ","+cp_ToStrODBC(this.w_ATTIPCOM)+;
                  ","+cp_ToStrODBC(this.w_ATCODATT)+;
                  ","+cp_ToStrODBC(this.w_ATDESCRI)+;
                  ","+cp_ToStrODBC(this.w_AT_PRJVW)+;
                  ","+cp_ToStrODBC(this.w_ATDESSUP)+;
                  ","+cp_ToStrODBC(this.w_ATFLPREV)+;
                  ","+cp_ToStrODBC(this.w_ATDURGIO)+;
                  ","+cp_ToStrODBC(this.w_ATVINCOL)+;
                  ","+cp_ToStrODBC(this.w_ATDATVIN)+;
                  ","+cp_ToStrODBC(this.w_ATDATINI)+;
                  ","+cp_ToStrODBC(this.w_ATDATFIN)+;
                  ","+cp_ToStrODBC(this.w_ATPERCOM)+;
                  ","+cp_ToStrODBC(this.w_ATCARDIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ATTIVITA')
        i_extval=cp_InsertValVFPExtFlds(this,'ATTIVITA')
        cp_CheckDeletedKey(i_cTable,0,'ATCODCOM',this.w_ATCODCOM,'ATTIPATT',this.w_ATTIPATT,'ATCODATT',this.w_ATCODATT)
        INSERT INTO (i_cTable);
              (ATCODCOM,ATTIPATT,ATTIPCOM,ATCODATT,ATDESCRI,AT_PRJVW,ATDESSUP,ATFLPREV,ATDURGIO,ATVINCOL,ATDATVIN,ATDATINI,ATDATFIN,ATPERCOM,ATCARDIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATCODCOM;
                  ,this.w_ATTIPATT;
                  ,this.w_ATTIPCOM;
                  ,this.w_ATCODATT;
                  ,this.w_ATDESCRI;
                  ,this.w_AT_PRJVW;
                  ,this.w_ATDESSUP;
                  ,this.w_ATFLPREV;
                  ,this.w_ATDURGIO;
                  ,this.w_ATVINCOL;
                  ,this.w_ATDATVIN;
                  ,this.w_ATDATINI;
                  ,this.w_ATDATFIN;
                  ,this.w_ATPERCOM;
                  ,this.w_ATCARDIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ATTIVITA_IDX,i_nConn)
      *
      * update ATTIVITA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ATTIVITA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATTIPCOM="+cp_ToStrODBC(this.w_ATTIPCOM)+;
             ",ATDESCRI="+cp_ToStrODBC(this.w_ATDESCRI)+;
             ",AT_PRJVW="+cp_ToStrODBC(this.w_AT_PRJVW)+;
             ",ATDESSUP="+cp_ToStrODBC(this.w_ATDESSUP)+;
             ",ATFLPREV="+cp_ToStrODBC(this.w_ATFLPREV)+;
             ",ATDURGIO="+cp_ToStrODBC(this.w_ATDURGIO)+;
             ",ATVINCOL="+cp_ToStrODBC(this.w_ATVINCOL)+;
             ",ATDATVIN="+cp_ToStrODBC(this.w_ATDATVIN)+;
             ",ATDATINI="+cp_ToStrODBC(this.w_ATDATINI)+;
             ",ATDATFIN="+cp_ToStrODBC(this.w_ATDATFIN)+;
             ",ATPERCOM="+cp_ToStrODBC(this.w_ATPERCOM)+;
             ",ATCARDIN="+cp_ToStrODBC(this.w_ATCARDIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ATTIVITA')
        i_cWhere = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
        UPDATE (i_cTable) SET;
              ATTIPCOM=this.w_ATTIPCOM;
             ,ATDESCRI=this.w_ATDESCRI;
             ,AT_PRJVW=this.w_AT_PRJVW;
             ,ATDESSUP=this.w_ATDESSUP;
             ,ATFLPREV=this.w_ATFLPREV;
             ,ATDURGIO=this.w_ATDURGIO;
             ,ATVINCOL=this.w_ATVINCOL;
             ,ATDATVIN=this.w_ATDATVIN;
             ,ATDATINI=this.w_ATDATINI;
             ,ATDATFIN=this.w_ATDATFIN;
             ,ATPERCOM=this.w_ATPERCOM;
             ,ATCARDIN=this.w_ATCARDIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ATTIVITA_IDX,i_nConn)
      *
      * delete ATTIVITA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATCODCOM',this.w_ATCODCOM  ,'ATTIPATT',this.w_ATTIPATT  ,'ATCODATT',this.w_ATCODATT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_ATTIPCOM<>.w_ATTIPCOM
            .w_ATCODATT = IIF(.w_ATTIPCOM='P', .w_ATCODCOM, .w_ATCODATT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(8,15,.t.)
        if .o_ATDATVIN<>.w_ATDATVIN
            .w_ATVINCOL = IIF(EMPTY(.w_ATDATVIN), IIF(.w_PROGR='A', 'PPP', 'PTP'), .w_ATVINCOL)
        endif
        .DoRTCalc(17,17,.t.)
        if .o_ATDURGIO<>.w_ATDURGIO.or. .o_ATDATFIN<>.w_ATDATFIN.or. .o_DATINI<>.w_DATINI
            .w_ATDATINI = iif(.w_PROGR='I',iif(empty(.w_ATDATFIN),nvl(.w_DATFIN,i_DATSYS),.w_ATDATFIN)-(.w_ATDURGIO-1),max(NVL(.w_DATINI,i_DATSYS),i_DATSYS))
        endif
        if .o_ATDURGIO<>.w_ATDURGIO.or. .o_ATDATINI<>.w_ATDATINI.or. .o_DATFIN<>.w_DATFIN.or. .o_ATCODCOM<>.w_ATCODCOM
            .w_ATDATFIN = iif(.w_PROGR='A',.w_ATDATINI+(.w_ATDURGIO-1),IIF(EMPTY(.w_ATDATFIN),NVL(.w_DATFIN,i_DATSYS),.w_ATDATFIN))
        endif
        .DoRTCalc(20,21,.t.)
            .w_MSPROJ = .w_USEPRJ
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTIPCOM_1_6.enabled = this.oPgFrm.Page1.oPag.oATTIPCOM_1_6.mCond()
    this.oPgFrm.Page1.oPag.oATCODATT_1_8.enabled = this.oPgFrm.Page1.oPag.oATCODATT_1_8.mCond()
    this.oPgFrm.Page1.oPag.oAT_PRJVW_1_11.enabled = this.oPgFrm.Page1.oPag.oAT_PRJVW_1_11.mCond()
    this.oPgFrm.Page2.oPag.oATDURGIO_2_1.enabled = this.oPgFrm.Page2.oPag.oATDURGIO_2_1.mCond()
    this.oPgFrm.Page2.oPag.oATDATINI_2_4.enabled = this.oPgFrm.Page2.oPag.oATDATINI_2_4.mCond()
    this.oPgFrm.Page2.oPag.oATDATFIN_2_5.enabled = this.oPgFrm.Page2.oPag.oATDATFIN_2_5.mCond()
    this.oPgFrm.Page2.oPag.oATPERCOM_2_6.enabled = this.oPgFrm.Page2.oPag.oATPERCOM_2_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPRJVWSVC_1_12.visible=!this.oPgFrm.Page1.oPag.oPRJVWSVC_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page2.oPag.oATVINCOL_2_2.visible=!this.oPgFrm.Page2.oPag.oATVINCOL_2_2.mHide()
    this.oPgFrm.Page2.oPag.oATDATVIN_2_3.visible=!this.oPgFrm.Page2.oPag.oATDATVIN_2_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDMSPROJ,PDPROGR,PDCODCOM";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_READPAR)
            select PDCHIAVE,PDMSPROJ,PDPROGR,PDCODCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCHIAVE,space(1))
      this.w_USEPRJ = NVL(_Link_.PDMSPROJ,space(1))
      this.w_PROGR = NVL(_Link_.PDPROGR,space(1))
      this.w_DEFCOM = NVL(_Link_.PDCODCOM,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(1)
      endif
      this.w_USEPRJ = space(1)
      this.w_PROGR = space(1)
      this.w_DEFCOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODCOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODCOM_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CNCODCAN as CNCODCAN104"+ ",link_1_4.CNDESCAN as CNDESCAN104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on ATTIVITA.ATCODCOM=link_1_4.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and ATTIVITA.ATCODCOM=link_1_4.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATCODCOM_1_4.value==this.w_ATCODCOM)
      this.oPgFrm.Page1.oPag.oATCODCOM_1_4.value=this.w_ATCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPCOM_1_6.RadioValue()==this.w_ATTIPCOM)
      this.oPgFrm.Page1.oPag.oATTIPCOM_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODATT_1_8.value==this.w_ATCODATT)
      this.oPgFrm.Page1.oPag.oATCODATT_1_8.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATDESCRI_1_10.value==this.w_ATDESCRI)
      this.oPgFrm.Page1.oPag.oATDESCRI_1_10.value=this.w_ATDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_PRJVW_1_11.RadioValue()==this.w_AT_PRJVW)
      this.oPgFrm.Page1.oPag.oAT_PRJVW_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRJVWSVC_1_12.RadioValue()==this.w_PRJVWSVC)
      this.oPgFrm.Page1.oPag.oPRJVWSVC_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATDESSUP_1_14.value==this.w_ATDESSUP)
      this.oPgFrm.Page1.oPag.oATDESSUP_1_14.value=this.w_ATDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_18.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_18.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oATDURGIO_2_1.value==this.w_ATDURGIO)
      this.oPgFrm.Page2.oPag.oATDURGIO_2_1.value=this.w_ATDURGIO
    endif
    if not(this.oPgFrm.Page2.oPag.oATVINCOL_2_2.RadioValue()==this.w_ATVINCOL)
      this.oPgFrm.Page2.oPag.oATVINCOL_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATVIN_2_3.value==this.w_ATDATVIN)
      this.oPgFrm.Page2.oPag.oATDATVIN_2_3.value=this.w_ATDATVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATINI_2_4.value==this.w_ATDATINI)
      this.oPgFrm.Page2.oPag.oATDATINI_2_4.value=this.w_ATDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oATDATFIN_2_5.value==this.w_ATDATFIN)
      this.oPgFrm.Page2.oPag.oATDATFIN_2_5.value=this.w_ATDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATPERCOM_2_6.value==this.w_ATPERCOM)
      this.oPgFrm.Page2.oPag.oATPERCOM_2_6.value=this.w_ATPERCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oATCARDIN_2_7.RadioValue()==this.w_ATCARDIN)
      this.oPgFrm.Page2.oPag.oATCARDIN_2_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ATTIVITA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ATCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODCOM_1_4.SetFocus()
            i_bnoObbl = !empty(.w_ATCODCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ATCODATT)) or not(.w_ATTIPCOM='C' OR .w_ATCODATT=.w_ATCODCOM))  and (.w_ATTIPCOM='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODATT_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ATCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto o capoprogetto incongruente/inesistente")
          case   (empty(.w_AT_PRJVW))  and (.w_ATTIPATT='P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAT_PRJVW_1_11.SetFocus()
            i_bnoObbl = !empty(.w_AT_PRJVW)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ATDURGIO>0)  and (.w_AT_PRJVW='F')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDURGIO_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La durata deve essere espressa con valori stretttamente positivi")
          case   not(NOT EMPTY(.w_ATDATVIN) OR .w_ATVINCOL $ 'PPP-PTP' OR .w_MSPROJ='N')  and not(.w_MSPROJ='N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATDATVIN_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la data vincolo")
          case   not(.w_ATPERCOM>=0 And .w_ATPERCOM<=100)  and (.w_AT_PRJVW='F')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATPERCOM_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La percentuale � espressa in centesimi (0..100)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATCODCOM = this.w_ATCODCOM
    this.o_ATTIPCOM = this.w_ATTIPCOM
    this.o_ATDURGIO = this.w_ATDURGIO
    this.o_ATDATVIN = this.w_ATDATVIN
    this.o_ATDATINI = this.w_ATDATINI
    this.o_ATDATFIN = this.w_ATDATFIN
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgspc_acmPag1 as StdContainer
  Width  = 643
  height = 202
  stdWidth  = 643
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCODCOM_1_4 as StdField with uid="ZRIRPOAIRA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ATCODCOM", cQueryName = "ATCODCOM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 17379411,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=128, Left=127, Top=10, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODCOM"

  func oATCODCOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODCOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODCOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODCOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oATTIPCOM_1_6 as StdCombo with uid="MMVWLNFEMK",rtseq=6,rtrep=.f.,left=521,top=10,width=119,height=21;
    , ToolTipText = "Tipo voce";
    , HelpContextID = 29638739;
    , cFormVar="w_ATTIPCOM",RowSource=""+"Conto,"+"Capo progetto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATTIPCOM_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    ' ')))
  endfunc
  func oATTIPCOM_1_6.GetRadio()
    this.Parent.oContained.w_ATTIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oATTIPCOM_1_6.SetRadio()
    this.Parent.oContained.w_ATTIPCOM=trim(this.Parent.oContained.w_ATTIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_ATTIPCOM=='C',1,;
      iif(this.Parent.oContained.w_ATTIPCOM=='P',2,;
      0))
  endfunc

  func oATTIPCOM_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oATCODATT_1_8 as StdField with uid="GNDNMQXEMT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODCOM,ATTIPATT,ATCODATT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto o capoprogetto incongruente/inesistente",;
    ToolTipText = "Codice voce",;
    HelpContextID = 16175014,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=128, Left=127, Top=37, InputMask=replicate('X',15)

  func oATCODATT_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPCOM='C')
    endwith
   endif
  endfunc

  func oATCODATT_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATTIPCOM='C' OR .w_ATCODATT=.w_ATCODCOM)
    endwith
    return bRes
  endfunc

  add object oATDESCRI_1_10 as StdField with uid="BMNOHEONGQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ATDESCRI", cQueryName = "ATDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione per ricerche",;
    HelpContextID = 235978673,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=127, Top=66, InputMask=replicate('X',30)


  add object oAT_PRJVW_1_11 as StdCombo with uid="MGOGJAXRTP",rtseq=9,rtrep=.f.,left=127,top=96,width=251,height=21;
    , HelpContextID = 118755235;
    , cFormVar="w_AT_PRJVW",RowSource=""+"Considera normalmente,"+"Considera come attivit� foglia", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oAT_PRJVW_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAT_PRJVW_1_11.GetRadio()
    this.Parent.oContained.w_AT_PRJVW = this.RadioValue()
    return .t.
  endfunc

  func oAT_PRJVW_1_11.SetRadio()
    this.Parent.oContained.w_AT_PRJVW=trim(this.Parent.oContained.w_AT_PRJVW)
    this.value = ;
      iif(this.Parent.oContained.w_AT_PRJVW=='S',1,;
      iif(this.Parent.oContained.w_AT_PRJVW=='F',2,;
      0))
  endfunc

  func oAT_PRJVW_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTIPATT='P')
    endwith
   endif
  endfunc


  add object oPRJVWSVC_1_12 as StdCombo with uid="KLXGSSTRVZ",rtseq=10,rtrep=.f.,left=127,top=96,width=251,height=21, enabled=.f.;
    , HelpContextID = 230645959;
    , cFormVar="w_PRJVWSVC",RowSource=""+"Non considerare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRJVWSVC_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    space(1)))
  endfunc
  func oPRJVWSVC_1_12.GetRadio()
    this.Parent.oContained.w_PRJVWSVC = this.RadioValue()
    return .t.
  endfunc

  func oPRJVWSVC_1_12.SetRadio()
    this.Parent.oContained.w_PRJVWSVC=trim(this.Parent.oContained.w_PRJVWSVC)
    this.value = ;
      iif(this.Parent.oContained.w_PRJVWSVC=='N',1,;
      0)
  endfunc

  func oPRJVWSVC_1_12.mHide()
    with this.Parent.oContained
      return (.w_USEPRJ<>'S' or .w_AT_PRJVW<>'N' or .w_ATTIPATT<>'P')
    endwith
  endfunc

  add object oATDESSUP_1_14 as StdMemo with uid="RIXHNDQRHL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ATDESSUP", cQueryName = "ATDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 235978666,;
   bGlobalFont=.t.,;
    Height=65, Width=510, Left=127, Top=131

  add object oDESCOM_1_18 as StdField with uid="PLPOIRZGRE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 72474314,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=258, Top=10, InputMask=replicate('X',30)


  add object oObj_1_22 as cp_runprogram with uid="KQKOWGAZWN",left=-4, top=221, width=217,height=20,;
    caption='GSPC_BVP',;
   bGlobalFont=.t.,;
    prg="GSPC_BVP(w_ATCODCOM,w_ATCODATT,w_ATTIPATT,w_AT_PRJVW,w_UNIQUE)",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 240255050

  add object oStr_1_7 as StdString with uid="QQFTPIZUKA",Visible=.t., Left=4, Top=10,;
    Alignment=1, Width=119, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="CGRAQBNQOH",Visible=.t., Left=4, Top=37,;
    Alignment=1, Width=119, Height=18,;
    Caption="Conto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_ATTIPCOM='P')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="WCPGQCBQSF",Visible=.t., Left=4, Top=66,;
    Alignment=1, Width=119, Height=15,;
    Caption="Descrizione breve:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WSDEIPSQUL",Visible=.t., Left=4, Top=131,;
    Alignment=1, Width=119, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="XXIPHJQOWM",Visible=.t., Left=478, Top=10,;
    Alignment=1, Width=39, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="WVDOHRGYQB",Visible=.t., Left=4, Top=37,;
    Alignment=1, Width=119, Height=18,;
    Caption="Capo progetto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_ATTIPCOM='C')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="LMBXNVOYYP",Visible=.t., Left=4, Top=96,;
    Alignment=1, Width=119, Height=15,;
    Caption="Export in MS-Project:"  ;
  , bGlobalFont=.t.
enddefine
define class tgspc_acmPag2 as StdContainer
  Width  = 643
  height = 202
  stdWidth  = 643
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATDURGIO_2_1 as StdField with uid="RVUYAOHVLC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ATDURGIO", cQueryName = "ATDURGIO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La durata deve essere espressa con valori stretttamente positivi",;
    ToolTipText = "Durata in giorni dell'attivit�",;
    HelpContextID = 99565653,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=98, Top=34, cSayPict='"999999"', cGetPict='"999999"'

  func oATDURGIO_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_PRJVW='F')
    endwith
   endif
  endfunc

  func oATDURGIO_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATDURGIO>0)
    endwith
    return bRes
  endfunc


  add object oATVINCOL_2_2 as StdCombo with uid="UMXYKWDJTU",rtseq=16,rtrep=.f.,left=362,top=34,width=139,height=21;
    , ToolTipText = "Tipo di vincoli";
    , HelpContextID = 27549778;
    , cFormVar="w_ATVINCOL",RowSource=""+"Pi� presto possibile,"+"Finire non oltre il,"+"Iniziare non prima", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATVINCOL_2_2.RadioValue()
    return(iif(this.value =1,'PPP',;
    iif(this.value =2,'FNO',;
    iif(this.value =3,'INP',;
    space(3)))))
  endfunc
  func oATVINCOL_2_2.GetRadio()
    this.Parent.oContained.w_ATVINCOL = this.RadioValue()
    return .t.
  endfunc

  func oATVINCOL_2_2.SetRadio()
    this.Parent.oContained.w_ATVINCOL=trim(this.Parent.oContained.w_ATVINCOL)
    this.value = ;
      iif(this.Parent.oContained.w_ATVINCOL=='PPP',1,;
      iif(this.Parent.oContained.w_ATVINCOL=='FNO',2,;
      iif(this.Parent.oContained.w_ATVINCOL=='INP',3,;
      0)))
  endfunc

  func oATVINCOL_2_2.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oATDATVIN_2_3 as StdField with uid="FIGYVQJNUE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ATDATVIN", cQueryName = "ATDATVIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la data vincolo",;
    ToolTipText = "Data vincolo",;
    HelpContextID = 83574868,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=510, Top=34

  func oATDATVIN_2_3.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  func oATDATVIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT EMPTY(.w_ATDATVIN) OR .w_ATVINCOL $ 'PPP-PTP' OR .w_MSPROJ='N')
    endwith
    return bRes
  endfunc

  add object oATDATINI_2_4 as StdField with uid="PJFETLOOSP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATDATINI", cQueryName = "ATDATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 133906511,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=98, Top=67

  func oATDATINI_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_PRJVW='F' and .w_PROGR='A')
    endwith
   endif
  endfunc

  add object oATDATFIN_2_5 as StdField with uid="LURQTZSFSO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ATDATFIN", cQueryName = "ATDATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�, calcolata come somma della data inizio e della durata",;
    HelpContextID = 83574868,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=98, Top=100

  func oATDATFIN_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_PRJVW='F' and .w_PROGR='I')
    endwith
   endif
  endfunc

  add object oATPERCOM_2_6 as StdField with uid="ZHFLWOTGYM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ATPERCOM", cQueryName = "ATPERCOM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La percentuale � espressa in centesimi (0..100)",;
    ToolTipText = "Percentuale completamento",;
    HelpContextID = 31457363,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=362, Top=67, cSayPict='"999"', cGetPict='"999"'

  func oATPERCOM_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AT_PRJVW='F')
    endwith
   endif
  endfunc

  func oATPERCOM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATPERCOM>=0 And .w_ATPERCOM<=100)
    endwith
    return bRes
  endfunc

  add object oATCARDIN_2_7 as StdCheck with uid="ORFZOKBEPW",rtseq=21,rtrep=.f.,left=424, top=67, caption="Attivit� cardine",;
    ToolTipText = "Attivit� cardine (MS Project)",;
    HelpContextID = 47919188,;
    cFormVar="w_ATCARDIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATCARDIN_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATCARDIN_2_7.GetRadio()
    this.Parent.oContained.w_ATCARDIN = this.RadioValue()
    return .t.
  endfunc

  func oATCARDIN_2_7.SetRadio()
    this.Parent.oContained.w_ATCARDIN=trim(this.Parent.oContained.w_ATCARDIN)
    this.value = ;
      iif(this.Parent.oContained.w_ATCARDIN=='S',1,;
      0)
  endfunc

  add object oStr_2_8 as StdString with uid="VFPTVLNYFG",Visible=.t., Left=8, Top=67,;
    Alignment=1, Width=86, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="FGREEAIWDO",Visible=.t., Left=8, Top=100,;
    Alignment=1, Width=86, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="QGMGLRSXHQ",Visible=.t., Left=8, Top=34,;
    Alignment=1, Width=86, Height=15,;
    Caption="Durata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="DJTGQABAPN",Visible=.t., Left=277, Top=34,;
    Alignment=1, Width=79, Height=15,;
    Caption="Vincoli:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (.w_MSPROJ='N')
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="NFYGYONZZO",Visible=.t., Left=200, Top=67,;
    Alignment=1, Width=156, Height=18,;
    Caption="% Completamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ATTIPATT='P' And ATTIPCOM In  ('P','C')"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ATCODCOM=ATTIVITA.ATCODCOM";
  +" and "+i_cAliasName+".ATTIPATT=ATTIVITA.ATTIPATT";
  +" and "+i_cAliasName+".ATCODATT=ATTIVITA.ATCODATT";
  +")"
  endif
  i_res=cp_AppQueryFilter('gspc_acm','ATTIVITA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATCODCOM=ATTIVITA.ATCODCOM";
  +" and "+i_cAliasName2+".ATTIPATT=ATTIVITA.ATTIPATT";
  +" and "+i_cAliasName2+".ATCODATT=ATTIVITA.ATCODATT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
