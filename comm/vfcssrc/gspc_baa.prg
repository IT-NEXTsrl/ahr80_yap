* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_baa                                                        *
*              Definizione autorizzazioni                                      *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_80]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-02                                                      *
* Last revis.: 2009-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_baa",oParentObject,m.pPARAM)
return(i_retval)

define class tgspc_baa as StdBatch
  * --- Local variables
  w_AttivitaAutorizzata = space(1)
  w_AttivitaFrequente = space(1)
  w_CODATT = space(15)
  w_Gruppo = 0
  w_OldWorkArea = 0
  w_PosizioneMemorizzata = 0
  w_SUKEY = 0
  w_Utente = 0
  pPARAM = space(4)
  * --- WorkFile variables
  ATTIVITA_idx=0
  SERVUTGR_idx=0
  CPUSERS_idx=0
  CPGROUPS_idx=0
  CPUSRGRP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPARAM = "Iniz"
        * --- --Bottone Inizializza
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPARAM = "Freq"
        * --- w_FREQUENTE Changed
        Select (this.oParentObject.w_ZoomAtt.cCursor)
        REPLACE SUATFREQ WITH IIF(this.oParentObject.w_FREQUENTE="S",ah_MsgFormat("S�"),ah_MsgFormat("No"))
        this.oParentObject.w_ZoomAtt.Refresh()     
      case this.pPARAM = "Auto"
        Select (this.oParentObject.w_ZoomAtt.cCursor)
        REPLACE SUATTAUT WITH IIF(this.oParentObject.w_AUTORIZZATA="S",ah_MsgFormat("S�"),ah_MsgFormat("No"))
        if SUATTAUT=ah_MsgFormat("No") and SUATFREQ=ah_MsgFormat("S�")
          REPLACE SUATFREQ WITH ah_MsgFormat("No")
        endif
        this.oParentObject.w_ZoomAtt.Refresh()     
      case this.pPARAM = "Conf"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pPARAM = "All_"
        Select (this.oParentObject.w_ZoomAtt.cCursor)
        if reccount(this.oParentObject.w_ZoomAtt.cCursor)>0
          this.w_PosizioneMemorizzata = RECNO()
          UPDATE (this.oParentObject.w_ZoomAtt.cCursor) SET xChk=val(this.oParentObject.w_SelAll)
          GO this.w_PosizioneMemorizzata
          this.oParentObject.w_ZoomAtt.Refresh()     
        endif
      case this.pPARAM ="Canc"
        * --- --Assegnamenti su w_FREQUENTE e w_AUTORIZZATA
        Select (this.oParentObject.w_ZoomAtt.cCursor)
        this.w_PosizioneMemorizzata = RECNO()
        GO TOP
        SCAN FOR xChk = 1
        this.w_CODATT = ATCODATT
        this.w_Gruppo = SUCODGRP
        this.w_Utente = SUCODUTE
        this.w_SUKEY = this.w_Utente + 10000 * this.w_Gruppo
        * --- Delete from SERVUTGR
        i_nConn=i_TableProp[this.SERVUTGR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SUKEY = "+cp_ToStrODBC(this.w_SUKEY);
                +" and SUCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
                +" and SUCODATT = "+cp_ToStrODBC(this.w_CODATT);
                 )
        else
          delete from (i_cTable) where;
                SUKEY = this.w_SUKEY;
                and SUCODCOM = this.oParentObject.w_CODCOM;
                and SUCODATT = this.w_CODATT;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        ENDSCAN
        if RECCOUNT(this.oParentObject.w_ZoomAtt.cCursor) > 0
          GO this.w_PosizioneMemorizzata
          ah_ErrorMsg("Aggiornamento completato","","")
          this.oparentobject.NotifyEvent("EsegueQuery")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(this.oParentObject.w_CODCOM)
      ah_ErrorMsg("Occorre specificare una commessa","!","")
    endif
    this.w_AttivitaAutorizzata = "N"
    this.w_AttivitaFrequente = "N"
    * --- Try
    local bErr_03698BF0
    bErr_03698BF0=bTrsErr
    this.Try_03698BF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_03698BF0
    * --- End
    ah_ErrorMsg("Elaborazione terminata","","")
  endproc
  proc Try_03698BF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from gspc3baa
    do vq_exec with 'gspc3baa',this,'_Curs_gspc3baa','',.f.,.t.
    if used('_Curs_gspc3baa')
      select _Curs_gspc3baa
      locate for 1=1
      do while not(eof())
      this.w_CODATT = _Curs_gspc3baa.ATCODATT
      this.w_OldWorkArea = SELECT()
      do case
        case this.oParentObject.w_CODUTE> 0 AND this.oParentObject.w_CODGRP = 0
          * --- Select from CPUSERS
          i_nConn=i_TableProp[this.CPUSERS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CODE  from "+i_cTable+" CPUSERS ";
                +" where CODE = "+cp_ToStrODBC(this.oParentObject.w_CODUTE)+"";
                 ,"_Curs_CPUSERS")
          else
            select CODE from (i_cTable);
             where CODE = this.oParentObject.w_CODUTE;
              into cursor _Curs_CPUSERS
          endif
          if used('_Curs_CPUSERS')
            select _Curs_CPUSERS
            locate for 1=1
            do while not(eof())
            this.w_Utente = _Curs_CPUSERS.CODE
            this.w_Gruppo = 0
            this.w_SUKEY = this.w_Utente + 10000 * this.w_Gruppo
            * --- Try
            local bErr_03699CD0
            bErr_03699CD0=bTrsErr
            this.Try_03699CD0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03699CD0
            * --- End
              select _Curs_CPUSERS
              continue
            enddo
            use
          endif
        case this.oParentObject.w_CODUTE = 0 AND this.oParentObject.w_CODGRP > 0
          * --- Select from CPGROUPS
          i_nConn=i_TableProp[this.CPGROUPS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPGROUPS_idx,2],.t.,this.CPGROUPS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CODE  from "+i_cTable+" CPGROUPS ";
                +" where CODE = "+cp_ToStrODBC(this.oParentObject.w_CODGRP)+"";
                 ,"_Curs_CPGROUPS")
          else
            select CODE from (i_cTable);
             where CODE = this.oParentObject.w_CODGRP;
              into cursor _Curs_CPGROUPS
          endif
          if used('_Curs_CPGROUPS')
            select _Curs_CPGROUPS
            locate for 1=1
            do while not(eof())
            this.w_Utente = 0
            this.w_Gruppo = _Curs_CPGROUPS.CODE
            this.w_SUKEY = this.w_Utente + 10000 * this.w_Gruppo
            * --- Try
            local bErr_03698CB0
            bErr_03698CB0=bTrsErr
            this.Try_03698CB0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03698CB0
            * --- End
              select _Curs_CPGROUPS
              continue
            enddo
            use
          endif
        case this.oParentObject.w_CODUTE= 0 AND this.oParentObject.w_CODGRP= 0
          * --- Select from CPUSERS
          i_nConn=i_TableProp[this.CPUSERS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CODE  from "+i_cTable+" CPUSERS ";
                 ,"_Curs_CPUSERS")
          else
            select CODE from (i_cTable);
              into cursor _Curs_CPUSERS
          endif
          if used('_Curs_CPUSERS')
            select _Curs_CPUSERS
            locate for 1=1
            do while not(eof())
            this.w_Utente = _Curs_CPUSERS.CODE
            this.w_Gruppo = 0
            this.w_SUKEY = this.w_Utente + 10000 * this.w_Gruppo
            * --- Try
            local bErr_036B8B80
            bErr_036B8B80=bTrsErr
            this.Try_036B8B80()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_036B8B80
            * --- End
              select _Curs_CPUSERS
              continue
            enddo
            use
          endif
          * --- Select from CPGROUPS
          i_nConn=i_TableProp[this.CPGROUPS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CPGROUPS_idx,2],.t.,this.CPGROUPS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CODE  from "+i_cTable+" CPGROUPS ";
                 ,"_Curs_CPGROUPS")
          else
            select CODE from (i_cTable);
              into cursor _Curs_CPGROUPS
          endif
          if used('_Curs_CPGROUPS')
            select _Curs_CPGROUPS
            locate for 1=1
            do while not(eof())
            this.w_Utente = 0
            this.w_Gruppo = _Curs_CPGROUPS.CODE
            this.w_SUKEY = this.w_Utente + 10000 * this.w_Gruppo
            * --- Try
            local bErr_0276F168
            bErr_0276F168=bTrsErr
            this.Try_0276F168()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_0276F168
            * --- End
              select _Curs_CPGROUPS
              continue
            enddo
            use
          endif
      endcase
      SELECT(this.w_OldWorkArea)
        select _Curs_gspc3baa
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03699CD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SERVUTGR
    i_nConn=i_TableProp[this.SERVUTGR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SERVUTGR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SUCODUTE"+",SUCODGRP"+",SUKEY"+",SUCODCOM"+",SUTIPATT"+",SUCODATT"+",SUATFREQ"+",SUATTAUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_Utente),'SERVUTGR','SUCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Gruppo),'SERVUTGR','SUCODGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUKEY),'SERVUTGR','SUKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'SERVUTGR','SUCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPATT),'SERVUTGR','SUTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'SERVUTGR','SUCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SUCODUTE',this.w_Utente,'SUCODGRP',this.w_Gruppo,'SUKEY',this.w_SUKEY,'SUCODCOM',this.oParentObject.w_CODCOM,'SUTIPATT',this.oParentObject.w_TIPATT,'SUCODATT',this.w_CODATT,'SUATFREQ',this.w_AttivitaFrequente,'SUATTAUT',this.w_AttivitaAutorizzata)
      insert into (i_cTable) (SUCODUTE,SUCODGRP,SUKEY,SUCODCOM,SUTIPATT,SUCODATT,SUATFREQ,SUATTAUT &i_ccchkf. );
         values (;
           this.w_Utente;
           ,this.w_Gruppo;
           ,this.w_SUKEY;
           ,this.oParentObject.w_CODCOM;
           ,this.oParentObject.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_AttivitaFrequente;
           ,this.w_AttivitaAutorizzata;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if UPPER(This.oParentObject.cPrg)="GSPC_KA2"
      ah_Msg("Inizializzazione attivit� %1 per l'utente %2",.t.,.f.,.t.,alltrim(this.w_CODATT),alltrim(str(this.w_Utente)))
    endif
    return
  proc Try_03698CB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SERVUTGR
    i_nConn=i_TableProp[this.SERVUTGR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SERVUTGR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SUCODUTE"+",SUCODGRP"+",SUKEY"+",SUCODCOM"+",SUTIPATT"+",SUCODATT"+",SUATFREQ"+",SUATTAUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_Utente),'SERVUTGR','SUCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Gruppo),'SERVUTGR','SUCODGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUKEY),'SERVUTGR','SUKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'SERVUTGR','SUCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPATT),'SERVUTGR','SUTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'SERVUTGR','SUCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SUCODUTE',this.w_Utente,'SUCODGRP',this.w_Gruppo,'SUKEY',this.w_SUKEY,'SUCODCOM',this.oParentObject.w_CODCOM,'SUTIPATT',this.oParentObject.w_TIPATT,'SUCODATT',this.w_CODATT,'SUATFREQ',this.w_AttivitaFrequente,'SUATTAUT',this.w_AttivitaAutorizzata)
      insert into (i_cTable) (SUCODUTE,SUCODGRP,SUKEY,SUCODCOM,SUTIPATT,SUCODATT,SUATFREQ,SUATTAUT &i_ccchkf. );
         values (;
           this.w_Utente;
           ,this.w_Gruppo;
           ,this.w_SUKEY;
           ,this.oParentObject.w_CODCOM;
           ,this.oParentObject.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_AttivitaFrequente;
           ,this.w_AttivitaAutorizzata;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if UPPER(This.oParentObject.cPrg)="GSPC_KA2"
      ah_Msg("Inizializzazione attivit� %1 per il gruppo %2",.t.,.f.,.t.,alltrim(this.w_CODATT),alltrim(str(this.w_Gruppo)))
    endif
    return
  proc Try_036B8B80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SERVUTGR
    i_nConn=i_TableProp[this.SERVUTGR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SERVUTGR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SUCODUTE"+",SUCODGRP"+",SUKEY"+",SUCODCOM"+",SUTIPATT"+",SUCODATT"+",SUATFREQ"+",SUATTAUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_Utente),'SERVUTGR','SUCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Gruppo),'SERVUTGR','SUCODGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUKEY),'SERVUTGR','SUKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'SERVUTGR','SUCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPATT),'SERVUTGR','SUTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'SERVUTGR','SUCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SUCODUTE',this.w_Utente,'SUCODGRP',this.w_Gruppo,'SUKEY',this.w_SUKEY,'SUCODCOM',this.oParentObject.w_CODCOM,'SUTIPATT',this.oParentObject.w_TIPATT,'SUCODATT',this.w_CODATT,'SUATFREQ',this.w_AttivitaFrequente,'SUATTAUT',this.w_AttivitaAutorizzata)
      insert into (i_cTable) (SUCODUTE,SUCODGRP,SUKEY,SUCODCOM,SUTIPATT,SUCODATT,SUATFREQ,SUATTAUT &i_ccchkf. );
         values (;
           this.w_Utente;
           ,this.w_Gruppo;
           ,this.w_SUKEY;
           ,this.oParentObject.w_CODCOM;
           ,this.oParentObject.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_AttivitaFrequente;
           ,this.w_AttivitaAutorizzata;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if UPPER(This.oParentObject.cPrg)="GSPC_KA2"
      ah_Msg("Inizializzazione attivit� %1 per l'utente %2",.t.,.f.,.t.,alltrim(this.w_CODATT),alltrim(str(this.w_Utente)))
    endif
    return
  proc Try_0276F168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SERVUTGR
    i_nConn=i_TableProp[this.SERVUTGR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SERVUTGR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SUCODUTE"+",SUCODGRP"+",SUKEY"+",SUCODCOM"+",SUTIPATT"+",SUCODATT"+",SUATFREQ"+",SUATTAUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_Utente),'SERVUTGR','SUCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Gruppo),'SERVUTGR','SUCODGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SUKEY),'SERVUTGR','SUKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCOM),'SERVUTGR','SUCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPATT),'SERVUTGR','SUTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'SERVUTGR','SUCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SUCODUTE',this.w_Utente,'SUCODGRP',this.w_Gruppo,'SUKEY',this.w_SUKEY,'SUCODCOM',this.oParentObject.w_CODCOM,'SUTIPATT',this.oParentObject.w_TIPATT,'SUCODATT',this.w_CODATT,'SUATFREQ',this.w_AttivitaFrequente,'SUATTAUT',this.w_AttivitaAutorizzata)
      insert into (i_cTable) (SUCODUTE,SUCODGRP,SUKEY,SUCODCOM,SUTIPATT,SUCODATT,SUATFREQ,SUATTAUT &i_ccchkf. );
         values (;
           this.w_Utente;
           ,this.w_Gruppo;
           ,this.w_SUKEY;
           ,this.oParentObject.w_CODCOM;
           ,this.oParentObject.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_AttivitaFrequente;
           ,this.w_AttivitaAutorizzata;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if UPPER(This.oParentObject.cPrg)="GSPC_KA2"
      ah_Msg("Inizializzazione attivit� %1 per il gruppo %2",.t.,.f.,.t.,alltrim(this.w_CODATT),alltrim(str(this.w_Gruppo)))
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Assegnamenti su w_FREQUENTE e w_AUTORIZZATA
    Select (this.oParentObject.w_ZoomAtt.cCursor)
    this.w_PosizioneMemorizzata = RECNO()
    GO TOP
    SCAN FOR xChk = 1
    this.w_CODATT = ATCODATT
    this.w_Gruppo = SUCODGRP
    this.w_Utente = SUCODUTE
    this.w_AttivitaAutorizzata = this.oParentObject.w_AUTORIZZATA
    this.w_AttivitaFrequente = this.oParentObject.w_FREQUENTE
    if this.w_Utente <> 0
      * --- Write into SERVUTGR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SERVUTGR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SERVUTGR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SUATFREQ ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
        +",SUATTAUT ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
            +i_ccchkf ;
        +" where ";
            +"SUCODUTE = "+cp_ToStrODBC(this.w_Utente);
            +" and SUCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
            +" and SUCODATT = "+cp_ToStrODBC(this.w_CODATT);
               )
      else
        update (i_cTable) set;
            SUATFREQ = this.w_AttivitaFrequente;
            ,SUATTAUT = this.w_AttivitaAutorizzata;
            &i_ccchkf. ;
         where;
            SUCODUTE = this.w_Utente;
            and SUCODCOM = this.oParentObject.w_CODCOM;
            and SUCODATT = this.w_CODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_Gruppo <> 0
      * --- Write into SERVUTGR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SERVUTGR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SERVUTGR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SUATFREQ ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
        +",SUATTAUT ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
            +i_ccchkf ;
        +" where ";
            +"SUCODGRP = "+cp_ToStrODBC(this.w_Gruppo);
            +" and SUCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
            +" and SUCODATT = "+cp_ToStrODBC(this.w_CODATT);
               )
      else
        update (i_cTable) set;
            SUATFREQ = this.w_AttivitaFrequente;
            ,SUATTAUT = this.w_AttivitaAutorizzata;
            &i_ccchkf. ;
         where;
            SUCODGRP = this.w_Gruppo;
            and SUCODCOM = this.oParentObject.w_CODCOM;
            and SUCODATT = this.w_CODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Select from CPUSRGRP
      i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select USERCODE  from "+i_cTable+" CPUSRGRP ";
            +" where GROUPCODE="+cp_ToStrODBC(this.w_Gruppo)+"";
             ,"_Curs_CPUSRGRP")
      else
        select USERCODE from (i_cTable);
         where GROUPCODE=this.w_Gruppo;
          into cursor _Curs_CPUSRGRP
      endif
      if used('_Curs_CPUSRGRP')
        select _Curs_CPUSRGRP
        locate for 1=1
        do while not(eof())
        * --- Write into SERVUTGR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SERVUTGR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SERVUTGR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SERVUTGR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUATFREQ ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaFrequente),'SERVUTGR','SUATFREQ');
          +",SUATTAUT ="+cp_NullLink(cp_ToStrODBC(this.w_AttivitaAutorizzata),'SERVUTGR','SUATTAUT');
              +i_ccchkf ;
          +" where ";
              +"SUCODUTE = "+cp_ToStrODBC(_Curs_CPUSRGRP.USERCODE);
              +" and SUCODCOM = "+cp_ToStrODBC(this.oParentObject.w_CODCOM);
              +" and SUCODATT = "+cp_ToStrODBC(this.w_CODATT);
                 )
        else
          update (i_cTable) set;
              SUATFREQ = this.w_AttivitaFrequente;
              ,SUATTAUT = this.w_AttivitaAutorizzata;
              &i_ccchkf. ;
           where;
              SUCODUTE = _Curs_CPUSRGRP.USERCODE;
              and SUCODCOM = this.oParentObject.w_CODCOM;
              and SUCODATT = this.w_CODATT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_CPUSRGRP
          continue
        enddo
        use
      endif
    endif
    ENDSCAN
    if RECCOUNT(this.oParentObject.w_ZoomAtt.cCursor) > 0
      GO this.w_PosizioneMemorizzata
      ah_ErrorMsg("Aggiornamento completato","","")
    endif
    this.oparentobject.NotifyEvent("EsegueQuery")
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ATTIVITA'
    this.cWorkTables[2]='SERVUTGR'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='CPGROUPS'
    this.cWorkTables[5]='CPUSRGRP'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gspc3baa')
      use in _Curs_gspc3baa
    endif
    if used('_Curs_CPUSERS')
      use in _Curs_CPUSERS
    endif
    if used('_Curs_CPGROUPS')
      use in _Curs_CPGROUPS
    endif
    if used('_Curs_CPUSERS')
      use in _Curs_CPUSERS
    endif
    if used('_Curs_CPGROUPS')
      use in _Curs_CPGROUPS
    endif
    if used('_Curs_CPUSRGRP')
      use in _Curs_CPUSRGRP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
