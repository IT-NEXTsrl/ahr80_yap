* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gspc_bsl                                                        *
*              Stampa schede movimenti                                         *
*                                                                              *
*      Author: Paolo Saitti                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_402]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-28                                                      *
* Last revis.: 2016-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgspc_bsl",oParentObject)
return(i_retval)

define class tgspc_bsl as StdBatch
  * --- Local variables
  w_TECNICO = .f.
  w_CODCOM1 = space(15)
  w_CODCOM2 = space(15)
  w_DESCOS1 = space(30)
  w_CODATT1 = space(15)
  w_DESCOS2 = space(30)
  w_CODATT2 = space(15)
  w_DESMAG1 = space(30)
  w_DATAINI = ctod("  /  /  ")
  w_DESMAG2 = space(30)
  w_DATAFIN = ctod("  /  /  ")
  w_TIPMOV = space(1)
  w_CODVAL = space(3)
  w_CODVAL1 = space(3)
  w_CODVAL2 = space(3)
  w_ValStampa = space(3)
  w_DecStampa = 0
  w_CODMAG1 = space(5)
  w_CODMAG2 = space(5)
  w_CODCOS1 = space(5)
  w_CODCOS2 = space(5)
  w_ANACOSTI = space(1)
  w_ANARICAV = space(1)
  w_TIPRIC = space(1)
  w_ClsName = space(10)
  w_QUERY1 = space(200)
  w_QUERY7 = space(200)
  w_READPAR = space(10)
  w_RICART = space(1)
  w_NUMRIG = 0
  w_CICLA = .f.
  w_RIGHEINS = 0
  w_MESS = space(200)
  w_MVTIPMOV = space(19)
  w_ATT = space(15)
  w_DECCOM = 0
  pPadre = .NULL.
  w_STIDRIGA = space(1)
  w_MESS = space(200)
  w_SIMVAL = space(5)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_CODAZI = space(5)
  w_FLIMPFIN = space(1)
  w_DOCVAL = space(3)
  w_CAOVAL = 0
  w_MAIDEMOV = space(1)
  * --- WorkFile variables
  VALUTE_idx=0
  MATMPMOV_idx=0
  STTMPMOV_idx=0
  TMPMOVIDETT_idx=0
  CPAR_DEF_idx=0
  TMPDOCUM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa schede movimenti (lanciato dalla mask GSPC_SSM)
    this.pPadre = this.oParentObject
    this.w_ClsName = UPPER(ALLTRIM(this.pPadre.Class))
    do case
      case type("this.pPadre")="O"
        this.w_ClsName = UPPER(ALLTRIM(this.pPadre.Class))
      otherwise
        i_retcode = 'stop'
        return
    endcase
    this.w_CODAZI = i_CODAZI
    this.w_CODATT1 = SPACE(15)
    this.w_CODATT2 = SPACE(15)
    this.w_CODMAG1 = SPACE(5)
    this.w_CODMAG2 = SPACE(5)
    this.w_CODCOS1 = SPACE(5)
    this.w_CODCOS2 = SPACE(5)
    this.w_FLIMPFIN = "S"
    this.w_READPAR = "TAM"
    * --- Read from CPAR_DEF
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CPAR_DEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPAR_DEF_idx,2],.t.,this.CPAR_DEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDRICART"+;
        " from "+i_cTable+" CPAR_DEF where ";
            +"PDCHIAVE = "+cp_ToStrODBC(this.w_READPAR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDRICART;
        from (i_cTable) where;
            PDCHIAVE = this.w_READPAR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RICART = NVL(cp_ToDate(_read_.PDRICART),cp_NullValue(_read_.PDRICART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_ClsName="TGSPC_SSM"
        this.w_CODCOM1 = this.pPadre.w_CODCOM1
        this.w_CODCOM2 = this.pPadre.w_CODCOM2
        this.w_CODATT1 = this.pPadre.w_CODATT1
        this.w_CODATT2 = this.pPadre.w_CODATT2
        this.w_DESCOS1 = this.pPadre.w_DESCOS1
        this.w_DESCOS2 = this.pPadre.w_DESCOS2
        this.w_DESMAG1 = this.pPadre.w_DESMAG1
        this.w_DESMAG2 = this.pPadre.w_DESMAG2
        this.w_DATAINI = this.pPadre.w_DATAINI
        this.w_DATAFIN = this.pPadre.w_DATAFIN
        this.w_TECNICO = this.pPadre.w_TECNICO
        this.w_TIPMOV = this.pPadre.w_TIPMOV
        this.w_TIPRIC = this.pPadre.w_TIPRIC
        this.w_ValStampa = this.pPadre.w_ValStampa
        this.w_DecStampa = this.pPadre.w_DecStampa
        this.w_CODCOS1 = this.pPadre.w_CODCOS1
        this.w_CODCOS2 = this.pPadre.w_CODCOS2
        this.w_CODMAG1 = this.pPadre.w_CODMAG1
        this.w_CODMAG2 = this.pPadre.w_CODMAG2
        this.w_OQRY = this.pPadre.w_OQRY
        this.w_OREP = this.pPadre.w_OREP
        this.w_CODVAL1 = this.pPadre.w_CODVAL1
        this.w_CODVAL2 = this.pPadre.w_CODVAL2
        this.w_CODVAL = SPACE(3)
        if (this.w_CODVAL1<>g_CODEUR and this.w_CODVAL2<>g_CODEUR and this.w_CODCOM1=this.w_CODCOM2) and !empty(nvl(this.w_CODCOM1,"")) and !empty(nvl(this.w_CODCOM2,""))
          this.w_ValStampa = this.pPadre.w_ValStampa
        else
          this.w_ValStampa = g_PERVAL
        endif
        this.w_ANACOSTI = this.pPadre.w_ANACOSTI
        this.w_ANARICAV = this.pPadre.w_ANARICAV
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VASIMVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_ValStampa);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VASIMVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_ValStampa;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
          this.w_DecStampa = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Decimali della valuta di commessa
      case this.w_ClsName="TGSPC_SMM"
        this.w_CODCOM1 = this.pPadre.w_CODCOM
        this.w_CODCOM2 = this.pPadre.w_CODCOM
        this.w_CODATT1 = this.pPadre.w_CODATT
        this.w_CODATT2 = this.pPadre.w_CODATT
        this.w_TIPMOV = this.pPadre.w_TIPMOV
        this.w_TIPRIC = this.pPadre.w_TIPMOV
        this.w_ANACOSTI = IIF(this.w_TIPMOV $ "P-C-I-E-T", "S", "N")
        this.w_ANARICAV = IIF(this.w_TIPRIC $ "O-F-A-R", "S", "N")
        this.w_DATAINI = this.pPadre.w_DATAINI
        this.w_DATAFIN = this.pPadre.w_DATAFIN
        this.w_ValStampa = g_CODEUR
        this.w_DecStampa = 5
      case this.w_ClsName="TGSPC_SRC"
        this.w_CODCOM1 = this.pPadre.w_CODCOM3
        this.w_CODCOM2 = this.pPadre.w_CODCOM2
        this.w_TIPMOV = "T"
        this.w_TIPRIC = "R"
        this.w_ANACOSTI = "N"
        this.w_ANARICAV = "S"
        if this.pPadre.w_FILTRO="N"
          this.w_DATAINI = cp_CharToDate("  -  -  ")
          this.w_DATAFIN = cp_CharToDate("  -  -  ")
        else
          this.w_DATAINI = this.pPadre.w_DATAINI
          this.w_DATAFIN = this.pPadre.w_DATAFIN
        endif
        this.w_ValStampa = g_CODEUR
        this.w_DecStampa = 5
      case this.w_ClsName="TGSIN_BIM"
        this.w_CODCOM1 = SPACE(15)
        this.w_CODCOM2 = SPACE(15)
        this.w_DESCOS1 = SPACE(30)
        this.w_DESCOS2 = SPACE(30)
        this.w_DESMAG1 = SPACE(30)
        this.w_DESMAG2 = SPACE(30)
        this.w_DATAINI = this.pPadre.oParentObject.w_DATAINI
        this.w_DATAFIN = iif (empty(this.pPadre.oParentObject.w_DATAFIN), i_datsys, this.pPadre.oParentObject.w_DATAFIN)
        this.pPadre.oParentObject.w_DATAINI = this.w_DATAINI
        this.pPadre.oParentObject.w_DATAFIN = this.w_DATAFIN
        this.w_TIPMOV = "T"
        this.w_TIPRIC = "R"
        this.w_ANACOSTI = "S"
        this.w_ANARICAV = "S"
        this.w_CODVAL = SPACE(3)
        this.w_ValStampa = g_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_ValStampa);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_ValStampa;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DecStampa = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OQRY = alltrim(NVL(this.pPadre.w_QUERY, ""))
        this.w_OREP = SPACE(50)
    endcase
    * --- Drop temporary table MATMPMOV
    i_nIdx=cp_GetTableDefIdx('MATMPMOV')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('MATMPMOV')
    endif
    * --- Drop temporary table STTMPMOV
    i_nIdx=cp_GetTableDefIdx('STTMPMOV')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('STTMPMOV')
    endif
    this.w_STIDRIGA = SPACE(1)
    * --- Variabile Passata Come Parametro Alla Query
    *     GSPCSRSSM per selezionare alcuni documenti
    do case
      case this.w_ClsName="TGSIN_BIM"
        AddmsgNL ("%1----------------------------------------------------------------------------------------------------------- ", this.pPadre, space(11))
        AddMsgNL("%1< ELABORAZIONE COSTI >%0", this.pPadre, space(11))
        AddMsgNL("%1Estrazione movimenti", this.pPadre, space(11))
      otherwise
        ah_Msg("Estrazione movimenti in corso...",.T.)
    endcase
    if this.w_ANACOSTI="S"
      if this.w_TIPMOV<>"E"
        * --- Inserisco Movimenti di Magazzino, Documenti e Movimenti di Commessa 
        *     da GSPC_SSM
        * --- Create temporary table MATMPMOV
        i_nIdx=cp_AddTableDef('MATMPMOV') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_OSCHEQVTXG[4]
        indexes_OSCHEQVTXG[1]='MASERIAL'
        indexes_OSCHEQVTXG[2]='CPROWNUM'
        indexes_OSCHEQVTXG[3]='MAIDRIGA'
        indexes_OSCHEQVTXG[4]='MAIDEMOV'
        vq_exec('..\COMM\EXE\QUERY\GSPC_SSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_OSCHEQVTXG,.f.)
        this.MATMPMOV_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- -- Gestione storico --
        do case
          case this.w_ClsName<>"TGSIN_BIM" or this.w_FLIMPFIN="S"
            * --- Create temporary table STTMPMOV
            i_nIdx=cp_AddTableDef('STTMPMOV') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            declare indexes_WRFJIHVRUM[2]
            indexes_WRFJIHVRUM[1]='MVSERRIF'
            indexes_WRFJIHVRUM[2]='MVROWRIF'
            vq_exec('..\COMM\EXE\QUERY\GSPC8SSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_WRFJIHVRUM,.f.)
            this.STTMPMOV_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endcase
        do case
          case this.w_ClsName="TGSIN_BIM" and this.w_FLIMPFIN="S"
            AddMsgNL("%1Inserimento storico documenti in corso...", this.pPadre, space( 11 ) )
          case this.w_ClsName="TGSPC_SSM"
            ah_Msg("Inserimento storico documenti in corso...",.T.)
        endcase
        * --- In base allo strorico aggiorno (Query GSPCURSSM.VQR)
        *     oppure inserisco nuove righe (Query GSPCIRSSM.VQR)
        *     nella MATMPMOV
        *     Insert e write di seguito
        * --- Write into MATMPMOV
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MATMPMOV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="MASERIAL,CPROWNUM,MAIDRIGA,MAIDEMOV"
          do vq_exec with 'GSPCURSSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MATMPMOV_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                  +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                  +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MATIPMOV = _t2.MATIPMOV";
              +",MATIPDOCF = _t2.MATIPDOCF";
              +",MARIFKIT = _t2.MARIFKIT";
              +",MAFLVEAC = _t2.MAFLVEAC";
              +",MACLADOC = _t2.MACLADOC";
              +",MACATIPO = _t2.MACATIPO";
              +",MAIMPFIN = _t2.MAIMPFIN";
              +",MACOPREV = _t2.MACOPREV";
              +",MACOCONS = _t2.MACOCONS";
              +i_ccchkf;
              +" from "+i_cTable+" MATMPMOV, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                  +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                  +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV, "+i_cQueryTable+" _t2 set ";
              +"MATMPMOV.MATIPMOV = _t2.MATIPMOV";
              +",MATMPMOV.MATIPDOCF = _t2.MATIPDOCF";
              +",MATMPMOV.MARIFKIT = _t2.MARIFKIT";
              +",MATMPMOV.MAFLVEAC = _t2.MAFLVEAC";
              +",MATMPMOV.MACLADOC = _t2.MACLADOC";
              +",MATMPMOV.MACATIPO = _t2.MACATIPO";
              +",MATMPMOV.MAIMPFIN = _t2.MAIMPFIN";
              +",MATMPMOV.MACOPREV = _t2.MACOPREV";
              +",MATMPMOV.MACOCONS = _t2.MACOCONS";
              +Iif(Empty(i_ccchkf),"",",MATMPMOV.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="MATMPMOV.MASERIAL = t2.MASERIAL";
                  +" and "+"MATMPMOV.CPROWNUM = t2.CPROWNUM";
                  +" and "+"MATMPMOV.MAIDRIGA = t2.MAIDRIGA";
                  +" and "+"MATMPMOV.MAIDEMOV = t2.MAIDEMOV";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV set (";
              +"MATIPMOV,";
              +"MATIPDOCF,";
              +"MARIFKIT,";
              +"MAFLVEAC,";
              +"MACLADOC,";
              +"MACATIPO,";
              +"MAIMPFIN,";
              +"MACOPREV,";
              +"MACOCONS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.MATIPMOV,";
              +"t2.MATIPDOCF,";
              +"t2.MARIFKIT,";
              +"t2.MAFLVEAC,";
              +"t2.MACLADOC,";
              +"t2.MACATIPO,";
              +"t2.MAIMPFIN,";
              +"t2.MACOPREV,";
              +"t2.MACOCONS";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                  +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                  +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV set ";
              +"MATIPMOV = _t2.MATIPMOV";
              +",MATIPDOCF = _t2.MATIPDOCF";
              +",MARIFKIT = _t2.MARIFKIT";
              +",MAFLVEAC = _t2.MAFLVEAC";
              +",MACLADOC = _t2.MACLADOC";
              +",MACATIPO = _t2.MACATIPO";
              +",MAIMPFIN = _t2.MAIMPFIN";
              +",MACOPREV = _t2.MACOPREV";
              +",MACOCONS = _t2.MACOCONS";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MASERIAL = "+i_cQueryTable+".MASERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MAIDRIGA = "+i_cQueryTable+".MAIDRIGA";
                  +" and "+i_cTable+".MAIDEMOV = "+i_cQueryTable+".MAIDEMOV";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MATIPMOV = (select MATIPMOV from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MATIPDOCF = (select MATIPDOCF from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MARIFKIT = (select MARIFKIT from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MAFLVEAC = (select MAFLVEAC from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MACLADOC = (select MACLADOC from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MACATIPO = (select MACATIPO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MAIMPFIN = (select MAIMPFIN from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MACOPREV = (select MACOPREV from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MACOCONS = (select MACOCONS from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Insert into MATMPMOV
        i_nConn=i_TableProp[this.MATMPMOV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPCIRSSM",this.MATMPMOV_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Solo Documenti che movimentano gli impegni completamente evasi - Due Query
        * --- Una recupera i documenti evasi da altri documenti e una quelli evasi semplicemente fleggando la riga
        * --- In questo Caso viene messo a 0 MVIMPCOM per cui occorre prendere MVVALMAG o la sua tradizione nella valuta di commessa (PAG2)
        * --- Create temporary table MATMPMOV
        i_nIdx=cp_AddTableDef('MATMPMOV') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_JQWYZICFNM[4]
        indexes_JQWYZICFNM[1]='MASERIAL'
        indexes_JQWYZICFNM[2]='CPROWNUM'
        indexes_JQWYZICFNM[3]='MAIDRIGA'
        indexes_JQWYZICFNM[4]='MAIDEMOV'
        vq_exec('..\COMM\EXE\QUERY\GSPC5SSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JQWYZICFNM,.f.)
        this.MATMPMOV_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    if this.w_ANARICAV="S"
      * --- Inserisco Movimenti di Magazzino, Documenti e Movimenti di Commessa 
      *     da GSPC_SSM
      do case
        case this.w_ClsName="TGSIN_BIM"
          AddmsgNL ("%1----------------------------------------------------------------------------------------------------------- ", this.pPadre, space(11))
          AddMsgNL("%1< ELABORAZIONE RICAVI >%0", this.pPadre, space(11))
          AddMsgNL("%1Estrazione movimenti", this.pPadre, space(11))
        otherwise
          ah_Msg("Estrazione movimenti in corso...",.T.)
      endcase
      if !cp_ExistTableDef("MATMPMOV")
        * --- Se Estraggo tutti i Movimenti Analizzo le Fatture Da Emettere
        *     Lancio La tracciabilit� di Riga (batch GSPC1BSL)
        * --- Se la Tabella Non � ancora stata Creata (caso in cui non seleziono i Costi) La Creo
        if this.w_RICART<>"S"
          * --- Create temporary table MATMPMOV
          i_nIdx=cp_AddTableDef('MATMPMOV') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\COMM\EXE\QUERY\GSPCRPSSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.MATMPMOV_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table MATMPMOV
          i_nIdx=cp_AddTableDef('MATMPMOV') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\COMM\EXE\QUERY\GSPCRPSSMR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.MATMPMOV_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
      else
        * --- Inserisco i Ricavi Previsti
        if this.w_RICART<>"S"
          * --- Insert into MATMPMOV
          i_nConn=i_TableProp[this.MATMPMOV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPCRPSSM",this.MATMPMOV_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into MATMPMOV
          i_nConn=i_TableProp[this.MATMPMOV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPCRPSSMR",this.MATMPMOV_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      * --- -- Gestione storico --
      if this.w_TIPRIC="R" or this.w_TIPRIC="A"
        do case
          case this.w_ClsName="TGSIN_BIM"
            AddMsgNL("%1Elaborazione ricavi in corso...", this.pPadre,space( 11 ))
        endcase
        * --- Cerco di Tracciare le Righe dei documenti di Vendita
        *     Utilizzando le tabelle temporanee per Velocizzare
        this.w_RIGHEINS = 0
        this.w_CICLA = True
        this.w_NUMRIG = 1
        * --- Prima Query du estrazione della catena dei documenti Da Tracciare
        do case
          case this.w_ClsName="TGSIN_BIM"
            AddMsgNL("%1Estrazione dati livello %2 in corso..." , this.pPadre, space(11), alltrim(str(this.w_NUMRIG)) )
          otherwise
            ah_Msg("Estrazione dati livello %1 in corso...",.T.,.F.,.F., alltrim(str(this.w_NUMRIG)) )
        endcase
        * --- -- Gestione storico --
        if this.w_TIPRIC="R" or this.w_TIPRIC="A"
          * --- Cerco di Tracciare le Righe dei documenti di Vendita
          *     Utilizzando le tabelle temporanee per Velocizzare
          this.w_RIGHEINS = 0
          this.w_CICLA = True
          this.w_NUMRIG = 1
          * --- Prima Query du estrazione della catena dei documenti Da Tracciare
          do case
            case inlist(this.w_ClsName,"TGSPC_SSM", "TGSPC_SMM")
              ah_Msg("Estrazione documenti origine di fattura in corso...", .t.,.f.,.f., alltrim(str(this.w_NUMRIG)))
            case this.w_ClsName="TGSIM_BIN"
              AddMsgNL("Estrazione documenti origine di fattura in corso..." , this.pPadre, alltrim(str(this.w_NUMRIG)))
          endcase
          * --- Righe Ragguppate
          * --- Create temporary table TMPMOVIDETT
          i_nIdx=cp_AddTableDef('TMPMOVIDETT') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_CZNPXCYAIM[1]
          indexes_CZNPXCYAIM[1]='MASERIAL'
          vq_exec('..\COMM\EXE\QUERY\GSPCFFDSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_CZNPXCYAIM,.f.)
          this.TMPMOVIDETT_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Prendiamo in considerazione tutti i documenti origine di una fattura
          if this.w_RICART<>"S"
            * --- Create temporary table STTMPMOV
            i_nIdx=cp_AddTableDef('STTMPMOV') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\COMM\EXE\QUERY\GSPC22SM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.STTMPMOV_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            * --- Create temporary table STTMPMOV
            i_nIdx=cp_AddTableDef('STTMPMOV') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\COMM\EXE\QUERY\GSPC22SMR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.STTMPMOV_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          do case
            case inlist(this.w_ClsName,"TGSPC_SSM", "TGSPC_SMM")
              ah_Msg("Elaborazione dati in corso...", .t.,.f.,.f., alltrim(str(this.w_NUMRIG)))
            case this.w_ClsName="TGSIM_BIN"
              AddMsgNL("Elaborazione dati in corso..." , this.pPadre, alltrim(str(this.w_NUMRIG)))
          endcase
          * --- Estrazione documenti che evadono
          this.w_NUMRIG = 2
          if this.w_RICART<>"S"
            * --- Create temporary table TMPDOCUM
            i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\COMM\EXE\QUERY\GSPC20SM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDOCUM_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            * --- Create temporary table TMPDOCUM
            i_nIdx=cp_AddTableDef('TMPDOCUM') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\COMM\EXE\QUERY\GSPC20SMR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDOCUM_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          * --- Aggiorno i Padri Con La Parte Evasa
          * --- Write into STTMPMOV
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STTMPMOV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STTMPMOV_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MASERIAL,CPROWNUM,MAIDRIGA,MAIDEMOV"
            do vq_exec with 'GSPC31SM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.STTMPMOV_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="STTMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"STTMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"STTMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"STTMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVQTAEVA = STTMPMOV.MVQTAEVA+_t2.MVQTAEVA";
                +",MVIMPEVA = STTMPMOV.MVIMPEVA+_t2.MVIMPEVA";
                +",MAIMPFIN = STTMPMOV.MAIMPFIN-_t2.MVIMPFIN";
                +",MVFLEVAS = _t2.MVFLEVAS";
                +i_ccchkf;
                +" from "+i_cTable+" STTMPMOV, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="STTMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"STTMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"STTMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"STTMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STTMPMOV, "+i_cQueryTable+" _t2 set ";
                +"STTMPMOV.MVQTAEVA = STTMPMOV.MVQTAEVA+_t2.MVQTAEVA";
                +",STTMPMOV.MVIMPEVA = STTMPMOV.MVIMPEVA+_t2.MVIMPEVA";
                +",STTMPMOV.MAIMPFIN = STTMPMOV.MAIMPFIN-_t2.MVIMPFIN";
                +",STTMPMOV.MVFLEVAS = _t2.MVFLEVAS";
                +Iif(Empty(i_ccchkf),"",",STTMPMOV.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="STTMPMOV.MASERIAL = t2.MASERIAL";
                    +" and "+"STTMPMOV.CPROWNUM = t2.CPROWNUM";
                    +" and "+"STTMPMOV.MAIDRIGA = t2.MAIDRIGA";
                    +" and "+"STTMPMOV.MAIDEMOV = t2.MAIDEMOV";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STTMPMOV set (";
                +"MVQTAEVA,";
                +"MVIMPEVA,";
                +"MAIMPFIN,";
                +"MVFLEVAS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"MVQTAEVA+t2.MVQTAEVA,";
                +"MVIMPEVA+t2.MVIMPEVA,";
                +"MAIMPFIN-t2.MVIMPFIN,";
                +"t2.MVFLEVAS";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="STTMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"STTMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"STTMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"STTMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" STTMPMOV set ";
                +"MVQTAEVA = STTMPMOV.MVQTAEVA+_t2.MVQTAEVA";
                +",MVIMPEVA = STTMPMOV.MVIMPEVA+_t2.MVIMPEVA";
                +",MAIMPFIN = STTMPMOV.MAIMPFIN-_t2.MVIMPFIN";
                +",MVFLEVAS = _t2.MVFLEVAS";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MASERIAL = "+i_cQueryTable+".MASERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                    +" and "+i_cTable+".MAIDRIGA = "+i_cQueryTable+".MAIDRIGA";
                    +" and "+i_cTable+".MAIDEMOV = "+i_cQueryTable+".MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVQTAEVA = (select "+i_cTable+".MVQTAEVA+MVQTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVIMPEVA = (select "+i_cTable+".MVIMPEVA+MVIMPEVA from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MAIMPFIN = (select "+i_cTable+".MAIMPFIN-MVIMPFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Inserisco i documenti da visualizzare
          if inlist(this.w_ClsName,"TGSPC_SSM", "TGSPC_SMM")
            * --- Insert into MATMPMOV
            i_nConn=i_TableProp[this.MATMPMOV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPC28SMB",this.MATMPMOV_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into MATMPMOV
            i_nConn=i_TableProp[this.MATMPMOV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COMM\EXE\QUERY\GSPC28SM",this.MATMPMOV_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Drop temporary table TMPMOVIDETT
      i_nIdx=cp_GetTableDefIdx('TMPMOVIDETT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMOVIDETT')
      endif
    endif
    do case
      case this.w_ClsName<>"TGSPC_SRC"
        * --- Esegue la traduzione degli importi nella valuta della stampa se richiesto
        if this.w_VAlStampa=g_CODEUR
          do case
            case this.w_ClsName="TGSIN_BIM"
              AddMsgNL("%1Traduzione importi in valuta di conto in corso..." , this.pPadre, space(11))
            otherwise
              ah_Msg("Traduzione importi in valuta di conto in corso...",.T.)
          endcase
          * --- (w_VAlStampa, w_DecStampa) vengono passate come parametro alla Query
          *     GSPCCVSSM.VQR per effettuare la conversione dei documenti in valuta diversa
          * --- Write into MATMPMOV
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MATMPMOV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATMPMOV_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MASERIAL,CPROWNUM,MAIDRIGA,MAIDEMOV"
            do vq_exec with 'GSPCCVSSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MATMPMOV_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MAIMPFIN = _t2.MAIMPFIN";
                +",MACOPREV = _t2.MACOPREV";
                +",MACOCONS = _t2.MACOCONS";
                +i_ccchkf;
                +" from "+i_cTable+" MATMPMOV, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV, "+i_cQueryTable+" _t2 set ";
                +"MATMPMOV.MAIMPFIN = _t2.MAIMPFIN";
                +",MATMPMOV.MACOPREV = _t2.MACOPREV";
                +",MATMPMOV.MACOCONS = _t2.MACOCONS";
                +Iif(Empty(i_ccchkf),"",",MATMPMOV.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="MATMPMOV.MASERIAL = t2.MASERIAL";
                    +" and "+"MATMPMOV.CPROWNUM = t2.CPROWNUM";
                    +" and "+"MATMPMOV.MAIDRIGA = t2.MAIDRIGA";
                    +" and "+"MATMPMOV.MAIDEMOV = t2.MAIDEMOV";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV set (";
                +"MAIMPFIN,";
                +"MACOPREV,";
                +"MACOCONS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.MAIMPFIN,";
                +"t2.MACOPREV,";
                +"t2.MACOCONS";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="MATMPMOV.MASERIAL = _t2.MASERIAL";
                    +" and "+"MATMPMOV.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"MATMPMOV.MAIDRIGA = _t2.MAIDRIGA";
                    +" and "+"MATMPMOV.MAIDEMOV = _t2.MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MATMPMOV set ";
                +"MAIMPFIN = _t2.MAIMPFIN";
                +",MACOPREV = _t2.MACOPREV";
                +",MACOCONS = _t2.MACOCONS";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MASERIAL = "+i_cQueryTable+".MASERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                    +" and "+i_cTable+".MAIDRIGA = "+i_cQueryTable+".MAIDRIGA";
                    +" and "+i_cTable+".MAIDEMOV = "+i_cQueryTable+".MAIDEMOV";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MAIMPFIN = (select MAIMPFIN from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MACOPREV = (select MACOPREV from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MACOCONS = (select MACOCONS from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
    endcase
    do case
      case this.w_ClsName="TGSPC_SSM"
        * --- Variabili Report
        L_VALSTAMPA=this.w_SIMVAL && this.w_VAlStampa
        this.w_SIMVAL = Ah_Msgformat("Importi espressi in %1" , alltrim(this.w_SIMVAL) )
        * --- Variabili Report
        L_DECIMALI = this.w_DecStampa
        l_CODCOM1=this.w_CODCOM1
        l_CODCOM2=this.w_CODCOM2
        l_CODATT1=this.w_CODATT1
        l_CODATT2=this.w_CODATT2
        l_DATAINI=this.w_DATAINI
        l_DATAFIN=this.w_DATAFIN
        l_TIPMOV = IIF(this.w_ANACOSTI="S", this.w_TIPMOV, " ")
        l_TIPRIC = IIF(this.w_ANARICAV="S", this.w_TIPRIC, " ")
        l_CODCOSTO1=this.w_DESCOS1
        l_CODCOSTO2=this.w_DESCOS2
        l_MAGA1=this.w_DESMAG1
        l_MAGA2=this.w_DESMAG2
        L_TECNICO=.f.
        L_FLCOSTI=this.w_ANACOSTI
        L_FLRICAVI=this.w_ANARICAV
        vq_exec(alltrim(this.w_OQRY),this,"__TMP__")
        if used("__TMP__")
          CP_CHPRN( ALLTRIM(this.w_OREP), " ", this )
        endif
        if used("__tmp__")
          use in __tmp__
        endif
      case this.w_ClsName="TGSPC_SMM"
        do case
          case this.w_ANACOSTI="S"
            Vq_Exec("..\COMM\EXE\QUERY\GSPC12SM.VQR",This,"__MoV__")
          case this.w_ANARICAV="S"
            Vq_Exec("..\COMM\EXE\QUERY\GSPC14SM.VQR",This,"__MoV__")
        endcase
      case this.w_ClsName="TGSPC_SRC"
        Vq_Exec("..\COMM\EXE\QUERY\GSPC13SM.VQR",This,"__MR__")
    endcase
    if this.w_ClsName<>"TGSIN_BIM"
      * --- Drop temporary table MATMPMOV
      i_nIdx=cp_GetTableDefIdx('MATMPMOV')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('MATMPMOV')
      endif
    endif
    * --- Drop temporary table STTMPMOV
    i_nIdx=cp_GetTableDefIdx('STTMPMOV')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('STTMPMOV')
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Var. per gestione dello storico
    * --- Decimali della valuta di commessa
    * --- --
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='*MATMPMOV'
    this.cWorkTables[3]='*STTMPMOV'
    this.cWorkTables[4]='*TMPMOVIDETT'
    this.cWorkTables[5]='CPAR_DEF'
    this.cWorkTables[6]='*TMPDOCUM'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
