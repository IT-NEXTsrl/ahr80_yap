PUBLIC loghnd, ANS
ANS=6

loghnd = FCREATE("ROUND.LOG")
DO apply_round_proc 
FCLOSE(loghnd)


PROCEDURE apply_round_proc()
    LOCAL filenumber,i,j,namearray,dirnumber, updfile, occorr
    filenumber = ADIR(alistfile , "*.*def")
    PRGR=0
    oldPRGR=-1
    for i=1 to filenumber
	    namefile = alistfile[i,1]

	    ***************************
	    *** elenco file esclusi ***
	    ***************************

	    IF NOT INLIST(UPPER(ALLTRIM(namefile)),"GSCE_BCG.BTCDEF","GSMR_BCR.BTCDEF","GSMR_BGP.BTCDEF","GSUT_BEE.BTCDEF","GSAG_BSP.BTCDEF","GSUT_GCP.MSKDEF","GSTE_BLD.BTCDEF","GSCR_BC4.BTCDEF")
		    gcString = filetostr(namefile)
			updfile = .f.
			occorr=1
			possearch=ATC("round", gcString)
			PRGR=INT(i*100/filenumber)
			IF PRGR<>oldPRGR
				WAIT WINDOW SYS(2003)+" -> "+STR(PRGR,3,0)+"%" nowait	
				oldPRGR=PRGR
			endif
		    DO while possearch>0 
			      lchar=LOWER(SUBSTRC(gcString, possearch-1, 1))
			      rchar=LOWER(SUBSTRC(gcString, possearch+5, 1))
			      r2char=LOWER(SUBSTRC(gcString, possearch+6, 1))
			      IF lchar $ "_1234567890[" OR BETWEEN(lchar ,"a", "z") OR ;
			         rchar $ "_1234567890" OR BETWEEN(rchar ,"a", "z") 
			      	*non � la funzione round: nessuna sostituzione!!!!
			      ELSE
			      	* prefisso
			      	IF lchar $ "'(,/*-+=<> " OR INLIST(ASC(lchar),10,13)
			      		IF rchar="(" OR rchar=" " AND r2char="("
			      			IF ANS=6
								ANS=MESSAGEBOX("Trovato " + lchar + "round" + rchar + r2char + " - Continuo controllo?", 3 ,namefile)
							endif
							IF ANS=2
								return
							ENDIF
			      			gcString=LEFTC(gcString, possearch-1)+"cp_"+SUBSTRC(gcString, possearch)
			      			updfile = .t.
			      		ELSE
			      			*MESSAGEBOX("Suffisso <"+rchar+r2char+"> non gestito!",0,namefile)
			      			*return
			      		ENDIF
			      	ELSE
			      	   MESSAGEBOX("Prefisso "+lchar+" non gestito!",0,namefile)
			      	   SUSPEND
			      	   
			      	ENDIF
			      ENDIF
			      occorr = occorr+1
			      possearch=ATC("round", gcString, occorr)
		    ENDDO
		    IF updfile then
		    	  IF FILE(namefile+".bak")
		    	  	DELETE FILE (namefile+".bak")
		    	  endif
		    	  COPY FILE (namefile) TO (namefile+".bak")
			      w_Handle = fopen(namefile, 2)
			      = Fwrite(w_Handle, gcstring)
			      FCLOSE(w_Handle)
			      FPUTS(loghnd, FULLPATH(namefile))
		    ENDIF
		ENDIF
    ENDFOR
	* chiamata ricorsiva alle sottocartelle	
    namearray=SYS(2015)
    dirnumber = ADIR(&namearray , "", "D")
    for j=1 to dirnumber
    NewDir=UPPER(&namearray[j,1])
        IF LEFT(NewDir,1)<>'.' AND ! NewDir=='STATESTO' AND ! NewDir=='PCON'
		        CD (NewDir)
		        apply_round_proc()
		        CD ..\
				IF ANS=2
					return
				ENDIF
		endif
    endfor
endproc

