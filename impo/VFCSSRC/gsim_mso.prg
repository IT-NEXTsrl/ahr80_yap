* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mso                                                        *
*              Sorgente dati ODBC                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_70]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-17                                                      *
* Last revis.: 2013-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_mso"))

* --- Class definition
define class tgsim_mso as StdTrsForm
  Top    = 13
  Left   = 15

  * --- Standard Properties
  Width  = 622
  Height = 478+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-21"
  HelpContextID=171327639
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  SRCMODBC_IDX = 0
  SRC_ODBC_IDX = 0
  cFile = "SRCMODBC"
  cFileDetail = "SRC_ODBC"
  cKeySelect = "SOCODICE"
  cKeyWhere  = "SOCODICE=this.w_SOCODICE"
  cKeyDetail  = "SOCODICE=this.w_SOCODICE and FLNAME=this.w_FLNAME"
  cKeyWhereODBC = '"SOCODICE="+cp_ToStrODBC(this.w_SOCODICE)';

  cKeyDetailWhereODBC = '"SOCODICE="+cp_ToStrODBC(this.w_SOCODICE)';
      +'+" and FLNAME="+cp_ToStrODBC(this.w_FLNAME)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"SRC_ODBC.SOCODICE="+cp_ToStrODBC(this.w_SOCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'SRC_ODBC.CPROWNUM '
  cPrg = "gsim_mso"
  cComment = "Sorgente dati ODBC"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SOCODICE = space(20)
  w_SODESCR = space(0)
  w_FLNAME = space(30)
  w_FLTYPE = space(1)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_FLCOMMEN = space(80)
  w_SOSQLSRC = space(0)
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_ODBCCURSOR = space(8)
  w_ODBCSQL = 0
  w_ODBCCONN = 0
  w_SOROUTRA = space(30)
  w_FOGLEXC = space(30)
  w_FLGFILE = space(1)
  w_FILEEXC = space(150)
  w_RECNOEXC = 0
  w_FLGTITOLO = space(1)
  w_oHeaderDetail = .NULL.
  w_FILESEL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SRCMODBC','gsim_mso')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_msoPag1","gsim_mso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Struttura")
      .Pages(1).HelpContextID = 99803304
      .Pages(2).addobject("oPag","tgsim_msoPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Interrogazione SQL")
      .Pages(2).HelpContextID = 1660256
      .Pages(3).addobject("oPag","tgsim_msoPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Cursore")
      .Pages(3).HelpContextID = 60873254
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSOCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    this.w_FILESEL = this.oPgFrm.Pages(3).oPag.FILESEL
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      this.w_FILESEL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='SRCMODBC'
    this.cWorkTables[2]='SRC_ODBC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SRCMODBC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SRCMODBC_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SOCODICE = NVL(SOCODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsim_mso
    if this.w_ODBCCONN > 0
       do gsim_bso with this,"Chiudi"
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from SRCMODBC where SOCODICE=KeySet.SOCODICE
    *
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2],this.bLoadRecFilter,this.SRCMODBC_IDX,"gsim_mso")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SRCMODBC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SRCMODBC.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"SRC_ODBC.","SRCMODBC.")
      i_cTable = i_cTable+' SRCMODBC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_ODBCDSN = space(30)
        .w_ODBCPATH = SPACE(200)
        .w_ODBCUSER = space(30)
        .w_ODBCPASSW = space(30)
        .w_ODBCCURSOR = sys(2015)
        .w_ODBCSQL = -1
        .w_ODBCCONN = -1
        .w_RECNOEXC = 2
        .w_FLGTITOLO = 'N'
        .w_SOCODICE = NVL(SOCODICE,space(20))
        .w_SODESCR = NVL(SODESCR,space(0))
        .w_SOSQLSRC = NVL(SOSQLSRC,space(0))
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .w_SOROUTRA = NVL(SOROUTRA,space(30))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page3.oPag.FILESEL.Calculate()
        .w_FOGLEXC = NVL(FOGLEXC,space(30))
        .w_FLGFILE = NVL(FLGFILE,space(1))
        .w_FILEEXC = NVL(FILEEXC,space(150))
        cp_LoadRecExtFlds(this,'SRCMODBC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from SRC_ODBC where SOCODICE=KeySet.SOCODICE
      *                            and FLNAME=KeySet.FLNAME
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.SRC_ODBC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('SRC_ODBC')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "SRC_ODBC.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" SRC_ODBC"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
        select * from (i_cTable) SRC_ODBC where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_FLNAME = NVL(FLNAME,space(30))
          .w_FLTYPE = NVL(FLTYPE,space(1))
          .w_FLLENGHT = NVL(FLLENGHT,0)
          .w_FLDECIMA = NVL(FLDECIMA,0)
          .w_FLCOMMEN = NVL(FLCOMMEN,space(80))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page3.oPag.FILESEL.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page3.oPag.oBtn_5_6.enabled = .oPgFrm.Page3.oPag.oBtn_5_6.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_SOCODICE=space(20)
      .w_SODESCR=space(0)
      .w_FLNAME=space(30)
      .w_FLTYPE=space(1)
      .w_FLLENGHT=0
      .w_FLDECIMA=0
      .w_FLCOMMEN=space(80)
      .w_SOSQLSRC=space(0)
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_ODBCCURSOR=space(8)
      .w_ODBCSQL=0
      .w_ODBCCONN=0
      .w_SOROUTRA=space(30)
      .w_FOGLEXC=space(30)
      .w_FLGFILE=space(1)
      .w_FILEEXC=space(150)
      .w_RECNOEXC=0
      .w_FLGTITOLO=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .DoRTCalc(1,9,.f.)
        .w_ODBCPATH = SPACE(200)
        .DoRTCalc(11,12,.f.)
        .w_ODBCCURSOR = sys(2015)
        .w_ODBCSQL = -1
        .w_ODBCCONN = -1
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page3.oPag.FILESEL.Calculate()
        .DoRTCalc(16,16,.f.)
        .w_FOGLEXC = ""
        .w_FLGFILE = "N"
        .DoRTCalc(19,19,.f.)
        .w_RECNOEXC = 2
        .w_FLGTITOLO = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SRCMODBC')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page3.oPag.oBtn_5_6.enabled = this.oPgFrm.Page3.oPag.oBtn_5_6.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSOCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSODESCR_1_3.enabled = i_bVal
      .Page2.oPag.oSOSQLSRC_4_1.enabled = i_bVal
      .Page3.oPag.oODBCDSN_5_3.enabled = i_bVal
      .Page3.oPag.oODBCPATH_5_5.enabled = i_bVal
      .Page3.oPag.oODBCUSER_5_8.enabled = i_bVal
      .Page3.oPag.oODBCPASSW_5_10.enabled = i_bVal
      .Page2.oPag.oSOROUTRA_4_2.enabled = i_bVal
      .Page3.oPag.oFOGLEXC_5_21.enabled = i_bVal
      .Page3.oPag.oFLGFILE_5_22.enabled = i_bVal
      .Page3.oPag.oFILEEXC_5_23.enabled = i_bVal
      .Page3.oPag.oRECNOEXC_5_27.enabled = i_bVal
      .Page3.oPag.oFLGTITOLO_5_29.enabled = i_bVal
      .Page3.oPag.oBtn_5_2.enabled = i_bVal
      .Page3.oPag.oBtn_5_6.enabled = .Page3.oPag.oBtn_5_6.mCond()
      .Page3.oPag.oBtn_5_15.enabled = i_bVal
      .Page3.oPag.oBtn_5_17.enabled = i_bVal
      .Page3.oPag.oObj_5_1.enabled = i_bVal
      .Page3.oPag.FILESEL.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSOCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSOCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SRCMODBC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOCODICE,"SOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SODESCR,"SODESCR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOSQLSRC,"SOSQLSRC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOROUTRA,"SOROUTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FOGLEXC,"FOGLEXC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLGFILE,"FLGFILE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FILEEXC,"FILEEXC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
    i_lTable = "SRCMODBC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SRCMODBC_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FLNAME C(30);
      ,t_FLTYPE C(1);
      ,t_FLLENGHT N(3);
      ,t_FLDECIMA N(2);
      ,t_FLCOMMEN C(80);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_msobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.controlsource=this.cTrsName+'.t_FLNAME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.controlsource=this.cTrsName+'.t_FLTYPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.controlsource=this.cTrsName+'.t_FLLENGHT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.controlsource=this.cTrsName+'.t_FLDECIMA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.controlsource=this.cTrsName+'.t_FLCOMMEN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(197)
    this.AddVLine(225)
    this.AddVLine(274)
    this.AddVLine(319)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SRCMODBC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SRCMODBC')
        i_extval=cp_InsertValODBCExtFlds(this,'SRCMODBC')
        local i_cFld
        i_cFld=" "+;
                  "(SOCODICE,SODESCR,SOSQLSRC,SOROUTRA,FOGLEXC"+;
                  ",FLGFILE,FILEEXC"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_SOCODICE)+;
                    ","+cp_ToStrODBC(this.w_SODESCR)+;
                    ","+cp_ToStrODBC(this.w_SOSQLSRC)+;
                    ","+cp_ToStrODBC(this.w_SOROUTRA)+;
                    ","+cp_ToStrODBC(this.w_FOGLEXC)+;
                    ","+cp_ToStrODBC(this.w_FLGFILE)+;
                    ","+cp_ToStrODBC(this.w_FILEEXC)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SRCMODBC')
        i_extval=cp_InsertValVFPExtFlds(this,'SRCMODBC')
        cp_CheckDeletedKey(i_cTable,0,'SOCODICE',this.w_SOCODICE)
        INSERT INTO (i_cTable);
              (SOCODICE,SODESCR,SOSQLSRC,SOROUTRA,FOGLEXC,FLGFILE,FILEEXC &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_SOCODICE;
                  ,this.w_SODESCR;
                  ,this.w_SOSQLSRC;
                  ,this.w_SOROUTRA;
                  ,this.w_FOGLEXC;
                  ,this.w_FLGFILE;
                  ,this.w_FILEEXC;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SRC_ODBC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])
      *
      * insert into SRC_ODBC
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(SOCODICE,FLNAME,FLTYPE,FLLENGHT,FLDECIMA"+;
                  ",FLCOMMEN,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SOCODICE)+","+cp_ToStrODBC(this.w_FLNAME)+","+cp_ToStrODBC(this.w_FLTYPE)+","+cp_ToStrODBC(this.w_FLLENGHT)+","+cp_ToStrODBC(this.w_FLDECIMA)+;
             ","+cp_ToStrODBC(this.w_FLCOMMEN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SOCODICE',this.w_SOCODICE,'FLNAME',this.w_FLNAME)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_SOCODICE,this.w_FLNAME,this.w_FLTYPE,this.w_FLLENGHT,this.w_FLDECIMA"+;
                ",this.w_FLCOMMEN,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update SRCMODBC
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'SRCMODBC')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " SODESCR="+cp_ToStrODBC(this.w_SODESCR)+;
             ",SOSQLSRC="+cp_ToStrODBC(this.w_SOSQLSRC)+;
             ",SOROUTRA="+cp_ToStrODBC(this.w_SOROUTRA)+;
             ",FOGLEXC="+cp_ToStrODBC(this.w_FOGLEXC)+;
             ",FLGFILE="+cp_ToStrODBC(this.w_FLGFILE)+;
             ",FILEEXC="+cp_ToStrODBC(this.w_FILEEXC)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'SRCMODBC')
          i_cWhere = cp_PKFox(i_cTable  ,'SOCODICE',this.w_SOCODICE  )
          UPDATE (i_cTable) SET;
              SODESCR=this.w_SODESCR;
             ,SOSQLSRC=this.w_SOSQLSRC;
             ,SOROUTRA=this.w_SOROUTRA;
             ,FOGLEXC=this.w_FOGLEXC;
             ,FLGFILE=this.w_FLGFILE;
             ,FILEEXC=this.w_FILEEXC;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_FLNAME))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.SRC_ODBC_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from SRC_ODBC
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update SRC_ODBC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " FLTYPE="+cp_ToStrODBC(this.w_FLTYPE)+;
                     ",FLLENGHT="+cp_ToStrODBC(this.w_FLLENGHT)+;
                     ",FLDECIMA="+cp_ToStrODBC(this.w_FLDECIMA)+;
                     ",FLCOMMEN="+cp_ToStrODBC(this.w_FLCOMMEN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      FLTYPE=this.w_FLTYPE;
                     ,FLLENGHT=this.w_FLLENGHT;
                     ,FLDECIMA=this.w_FLDECIMA;
                     ,FLCOMMEN=this.w_FLCOMMEN;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_FLNAME))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.SRC_ODBC_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])
        *
        * delete SRC_ODBC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
        *
        * delete SRCMODBC
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_FLNAME))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page3.oPag.FILESEL.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page3.oPag.FILESEL.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_WSFKTSEMKX()
    with this
          * --- chiusura cursori e connessioni
          gsim_bso(this;
              ,'Chiudi';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oODBCDSN_5_3.enabled = this.oPgFrm.Page3.oPag.oODBCDSN_5_3.mCond()
    this.oPgFrm.Page3.oPag.oFILEEXC_5_23.enabled = this.oPgFrm.Page3.oPag.oFILEEXC_5_23.mCond()
    this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.enabled = this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_2.enabled = this.oPgFrm.Page3.oPag.oBtn_5_2.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_15.enabled = this.oPgFrm.Page3.oPag.oBtn_5_15.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_17.enabled = this.oPgFrm.Page3.oPag.oBtn_5_17.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oODBCDSN_5_3.visible=!this.oPgFrm.Page3.oPag.oODBCDSN_5_3.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_4.visible=!this.oPgFrm.Page3.oPag.oStr_5_4.mHide()
    this.oPgFrm.Page3.oPag.oODBCPATH_5_5.visible=!this.oPgFrm.Page3.oPag.oODBCPATH_5_5.mHide()
    this.oPgFrm.Page3.oPag.oBtn_5_6.visible=!this.oPgFrm.Page3.oPag.oBtn_5_6.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_7.visible=!this.oPgFrm.Page3.oPag.oStr_5_7.mHide()
    this.oPgFrm.Page3.oPag.oODBCUSER_5_8.visible=!this.oPgFrm.Page3.oPag.oODBCUSER_5_8.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_9.visible=!this.oPgFrm.Page3.oPag.oStr_5_9.mHide()
    this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.visible=!this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_11.visible=!this.oPgFrm.Page3.oPag.oStr_5_11.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_20.visible=!this.oPgFrm.Page3.oPag.oStr_5_20.mHide()
    this.oPgFrm.Page3.oPag.oFOGLEXC_5_21.visible=!this.oPgFrm.Page3.oPag.oFOGLEXC_5_21.mHide()
    this.oPgFrm.Page3.oPag.oFILEEXC_5_23.visible=!this.oPgFrm.Page3.oPag.oFILEEXC_5_23.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_26.visible=!this.oPgFrm.Page3.oPag.oStr_5_26.mHide()
    this.oPgFrm.Page3.oPag.oRECNOEXC_5_27.visible=!this.oPgFrm.Page3.oPag.oRECNOEXC_5_27.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_28.visible=!this.oPgFrm.Page3.oPag.oStr_5_28.mHide()
    this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.visible=!this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.oObj_5_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page3.oPag.FILESEL.Event(cEvent)
        if lower(cEvent)==lower("Edit Aborted") or lower(cEvent)==lower("Insert row end") or lower(cEvent)==lower("Update end")
          .Calculate_WSFKTSEMKX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSOCODICE_1_1.value==this.w_SOCODICE)
      this.oPgFrm.Page1.oPag.oSOCODICE_1_1.value=this.w_SOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSODESCR_1_3.value==this.w_SODESCR)
      this.oPgFrm.Page1.oPag.oSODESCR_1_3.value=this.w_SODESCR
    endif
    if not(this.oPgFrm.Page2.oPag.oSOSQLSRC_4_1.value==this.w_SOSQLSRC)
      this.oPgFrm.Page2.oPag.oSOSQLSRC_4_1.value=this.w_SOSQLSRC
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCDSN_5_3.value==this.w_ODBCDSN)
      this.oPgFrm.Page3.oPag.oODBCDSN_5_3.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCPATH_5_5.value==this.w_ODBCPATH)
      this.oPgFrm.Page3.oPag.oODBCPATH_5_5.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCUSER_5_8.value==this.w_ODBCUSER)
      this.oPgFrm.Page3.oPag.oODBCUSER_5_8.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.value==this.w_ODBCPASSW)
      this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page2.oPag.oSOROUTRA_4_2.value==this.w_SOROUTRA)
      this.oPgFrm.Page2.oPag.oSOROUTRA_4_2.value=this.w_SOROUTRA
    endif
    if not(this.oPgFrm.Page3.oPag.oFOGLEXC_5_21.value==this.w_FOGLEXC)
      this.oPgFrm.Page3.oPag.oFOGLEXC_5_21.value=this.w_FOGLEXC
    endif
    if not(this.oPgFrm.Page3.oPag.oFLGFILE_5_22.RadioValue()==this.w_FLGFILE)
      this.oPgFrm.Page3.oPag.oFLGFILE_5_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFILEEXC_5_23.value==this.w_FILEEXC)
      this.oPgFrm.Page3.oPag.oFILEEXC_5_23.value=this.w_FILEEXC
    endif
    if not(this.oPgFrm.Page3.oPag.oRECNOEXC_5_27.value==this.w_RECNOEXC)
      this.oPgFrm.Page3.oPag.oRECNOEXC_5_27.value=this.w_RECNOEXC
    endif
    if not(this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.RadioValue()==this.w_FLGTITOLO)
      this.oPgFrm.Page3.oPag.oFLGTITOLO_5_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value==this.w_FLNAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value=this.w_FLNAME
      replace t_FLNAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value==this.w_FLTYPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value=this.w_FLTYPE
      replace t_FLTYPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value==this.w_FLLENGHT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value=this.w_FLLENGHT
      replace t_FLLENGHT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value==this.w_FLDECIMA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value=this.w_FLDECIMA
      replace t_FLDECIMA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value=this.w_FLCOMMEN
      replace t_FLCOMMEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'SRCMODBC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_RECNOEXC>0)  and not(.w_FLGFILE <> 'S' OR EMPTY(.w_FILEEXC))
            .oPgFrm.ActivePage=3
            .oPgFrm.Page3.oPag.oRECNOEXC_5_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_FLNAME))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_FLNAME)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FLNAME=space(30)
      .w_FLTYPE=space(1)
      .w_FLLENGHT=0
      .w_FLDECIMA=0
      .w_FLCOMMEN=space(80)
    endwith
    this.DoRTCalc(1,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FLNAME = t_FLNAME
    this.w_FLTYPE = t_FLTYPE
    this.w_FLLENGHT = t_FLLENGHT
    this.w_FLDECIMA = t_FLDECIMA
    this.w_FLCOMMEN = t_FLCOMMEN
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FLNAME with this.w_FLNAME
    replace t_FLTYPE with this.w_FLTYPE
    replace t_FLLENGHT with this.w_FLLENGHT
    replace t_FLDECIMA with this.w_FLDECIMA
    replace t_FLCOMMEN with this.w_FLCOMMEN
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_msoPag1 as StdContainer
  Width  = 618
  height = 478
  stdWidth  = 618
  stdheight = 478
  resizeXpos=464
  resizeYpos=375
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOCODICE_1_1 as StdField with uid="QFFXJVPWXO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SOCODICE", cQueryName = "SOCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 137771669,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=69, Top=9, InputMask=replicate('X',20)

  add object oSODESCR_1_3 as StdMemo with uid="VAADGLDVEK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SODESCR", cQueryName = "SODESCR",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 223357658,;
   bGlobalFont=.t.,;
    Height=48, Width=544, Left=69, Top=36


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=88, width=600,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="FLNAME",Label1="Campo",Field2="FLTYPE",Label2="Tipo",Field3="FLLENGHT",Label3="Lung.",Field4="FLDECIMA",Label4="Decim.",Field5="FLCOMMEN",Label5="Nota",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49452422

  add object oStr_1_2 as StdString with uid="DDJVULARJE",Visible=.t., Left=0, Top=12,;
    Alignment=1, Width=67, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=107,;
    width=594+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=108,width=593+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsim_msoPag2 as StdContainer
    Width  = 618
    height = 478
    stdWidth  = 618
    stdheight = 478
  resizeXpos=440
  resizeYpos=337
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOSQLSRC_4_1 as StdMemo with uid="FRCBJHPJMO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SOSQLSRC", cQueryName = "SOSQLSRC",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 229849751,;
    FontName = "Fixedsys", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=363, Width=601, Left=8, Top=14

  add object oSOROUTRA_4_2 as StdField with uid="BLWJNCNPUV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SOROUTRA", cQueryName = "SOROUTRA",;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare una routine per l'elaborazione del cursore dopo l'interrogazione",;
    HelpContextID = 203770521,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=229, Top=388, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oSOROUTRA_4_2.mZoom
    this.parent.oContained.w_SOROUTRA=getfile("FXP")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_4_3 as StdString with uid="ZBZRREMVAS",Visible=.t., Left=6, Top=392,;
    Alignment=1, Width=220, Height=18,;
    Caption="Routine elaborazione cursore:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsim_msoPag3 as StdContainer
    Width  = 618
    height = 478
    stdWidth  = 618
    stdheight = 478
  resizeXpos=376
  resizeYpos=274
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_5_1 as ah_CBrowse with uid="LBPJECTCYI",left=9, top=14, width=598,height=297,;
    caption='Browse',;
   bGlobalFont=.t.,;
    nPag=5;
    , HelpContextID = 152787178


  add object oBtn_5_2 as StdButton with uid="EUZJYLNNIJ",left=375, top=341, width=48,height=45,;
    CpPicture="BMP/Auto.bmp", caption="", nPag=5;
    , HelpContextID = 84649559;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_2.Click()
      with this.Parent.oContained
        do gsim_bso with this.Parent.oContained,"Esegui"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_2.mCond()
    with this.Parent.oContained
      return (!empty(.w_ODBCDSN) or (!empty(.w_FILEEXC) and .w_FLGFILE = 'S'))
    endwith
  endfunc

  add object oODBCDSN_5_3 as StdField with uid="XEHLZVFDPO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 239228442,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=84, Top=341, InputMask=replicate('X',30)

  func oODBCDSN_5_3.mCond()
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S')
    endwith
  endfunc

  func oODBCDSN_5_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE = 'S')
    endwith
    endif
  endfunc

  add object oODBCPATH_5_5 as StdField with uid="WWNCSLLCQU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 5, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 260199890,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=84, Top=364, InputMask=replicate('X',200)

  func oODBCPATH_5_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
    endif
  endfunc


  add object oBtn_5_6 as StdButton with uid="CEHYJDNFNL",left=286, top=364, width=21,height=21,;
    caption="...", nPag=5;
    , HelpContextID = 171528662;
  , bGlobalFont=.t.

    proc oBtn_5_6.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
   endif
  endfunc

  add object oODBCUSER_5_8 as StdField with uid="EEJDSJIWXT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 47032888,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=84, Top=387, InputMask=replicate('X',30)

  func oODBCUSER_5_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
    endif
  endfunc

  add object oODBCPASSW_5_10 as StdField with uid="RLZKBTNPUM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 260198487,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=84, Top=410, InputMask=replicate('X',30), PasswordChar='*'

  func oODBCPASSW_5_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
    endif
  endfunc


  add object oBtn_5_15 as StdButton with uid="CAERRCHIWU",left=430, top=341, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=5;
    , HelpContextID = 176320698;
    , caption='\<Copia Stru.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_15.Click()
      with this.Parent.oContained
        do gsim_bso with this.Parent.oContained,"Copia"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_15.mCond()
    with this.Parent.oContained
      return ((!empty(.w_FILEEXC) or !empty(.w_ODBCDSN)) and .w_ODBCSQL>0)
    endwith
  endfunc


  add object oBtn_5_17 as StdButton with uid="ZCJJBWCFOO",left=487, top=341, width=48,height=45,;
    CpPicture="BMP\LANCIATO.BMP", caption="", nPag=5;
    , HelpContextID = 105457705;
    , caption='C\<hiudi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_17.Click()
      with this.Parent.oContained
        do gsim_bso with this.Parent.oContained,"Chiudi"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_17.mCond()
    with this.Parent.oContained
      return (.w_ODBCCONN > 0)
    endwith
  endfunc


  add object FILESEL as cp_askfile with uid="CHJDLSUFIJ",left=312, top=341, width=21,height=21,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FILEEXC",;
    nPag=5;
    , ToolTipText = "Selezione file Excel";
    , HelpContextID = 171528662

  add object oFOGLEXC_5_21 as StdField with uid="ROMIVRQHMA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FOGLEXC", cQueryName = "FOGLEXC",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome foglio per importazione file Excel",;
    HelpContextID = 153680810,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=84, Top=364, InputMask=replicate('X',30)

  func oFOGLEXC_5_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE<>"S")
    endwith
    endif
  endfunc

  add object oFLGFILE_5_22 as StdCheck with uid="DQGFQLBZCW",rtseq=18,rtrep=.f.,left=339, top=341, caption="",;
    ToolTipText = "Flag importazione file Excel",;
    HelpContextID = 185663830,;
    cFormVar="w_FLGFILE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLGFILE_5_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLGFILE,&i_cF..t_FLGFILE),this.value)
    return(iif(xVal =1,"S",;
    "N"))
  endfunc
  func oFLGFILE_5_22.GetRadio()
    this.Parent.oContained.w_FLGFILE = this.RadioValue()
    return .t.
  endfunc

  func oFLGFILE_5_22.ToRadio()
    this.Parent.oContained.w_FLGFILE=trim(this.Parent.oContained.w_FLGFILE)
    return(;
      iif(this.Parent.oContained.w_FLGFILE=="S",1,;
      0))
  endfunc

  func oFLGFILE_5_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFILEEXC_5_23 as StdField with uid="GMHMDJAQWE",rtseq=19,rtrep=.f.,;
    cFormVar = "w_FILEEXC", cQueryName = "FILEEXC",;
    bObbl = .f. , nPag = 5, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il percorso+nome del file da importare",;
    HelpContextID = 154120618,;
   bGlobalFont=.t.,;
    Height=21, Width=217, Left=84, Top=341, InputMask=replicate('X',150)

  func oFILEEXC_5_23.mCond()
    with this.Parent.oContained
      return (.w_FLGFILE = 'S')
    endwith
  endfunc

  func oFILEEXC_5_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S')
    endwith
    endif
  endfunc

  add object oRECNOEXC_5_27 as StdField with uid="SHVCEGREPC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RECNOEXC", cQueryName = "RECNOEXC",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Il file viene importato dalla riga inserita",;
    HelpContextID = 75021145,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=84, Top=387, cSayPict='"9999"', cGetPict='"9999"'

  func oRECNOEXC_5_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S' OR EMPTY(.w_FILEEXC))
    endwith
    endif
  endfunc

  func oRECNOEXC_5_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RECNOEXC>0)
    endwith
    return bRes
  endfunc

  add object oFLGTITOLO_5_29 as StdCheck with uid="QKUXAKEURQ",rtseq=21,rtrep=.f.,left=140, top=390, caption="Nomi colonne nella riga precedente",;
    ToolTipText = "Flag titoli nella riga precedente",;
    HelpContextID = 216070510,;
    cFormVar="w_FLGTITOLO", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oFLGTITOLO_5_29.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLGTITOLO,&i_cF..t_FLGTITOLO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oFLGTITOLO_5_29.GetRadio()
    this.Parent.oContained.w_FLGTITOLO = this.RadioValue()
    return .t.
  endfunc

  func oFLGTITOLO_5_29.ToRadio()
    this.Parent.oContained.w_FLGTITOLO=trim(this.Parent.oContained.w_FLGTITOLO)
    return(;
      iif(this.Parent.oContained.w_FLGTITOLO=='S',1,;
      0))
  endfunc

  func oFLGTITOLO_5_29.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLGTITOLO_5_29.mCond()
    with this.Parent.oContained
      return (.w_RECNOEXC >0 AND EMPTY(.w_SOSQLSRC))
    endwith
  endfunc

  func oFLGTITOLO_5_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S' OR EMPTY(.w_FILEEXC))
    endwith
    endif
  endfunc

  add object oStr_5_4 as StdString with uid="PZRNRZAVLX",Visible=.t., Left=26, Top=368,;
    Alignment=1, Width=54, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_5_4.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
  endfunc

  add object oStr_5_7 as StdString with uid="MDNRFJNEDP",Visible=.t., Left=11, Top=345,;
    Alignment=1, Width=69, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  func oStr_5_7.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE = 'S')
    endwith
  endfunc

  add object oStr_5_9 as StdString with uid="WTUPAALUNU",Visible=.t., Left=39, Top=391,;
    Alignment=1, Width=41, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  func oStr_5_9.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
  endfunc

  add object oStr_5_11 as StdString with uid="PBPBMZTTLN",Visible=.t., Left=14, Top=414,;
    Alignment=1, Width=66, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_5_11.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE="S")
    endwith
  endfunc

  add object oStr_5_13 as StdString with uid="VJTQYMWNNE",Visible=.t., Left=11, Top=322,;
    Alignment=0, Width=335, Height=18,;
    Caption="Solo per esecuzione interrogazione"  ;
  , bGlobalFont=.t.

  add object oStr_5_20 as StdString with uid="OGSPCJDLQR",Visible=.t., Left=43, Top=368,;
    Alignment=1, Width=37, Height=18,;
    Caption="Foglio:"  ;
  , bGlobalFont=.t.

  func oStr_5_20.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE<>"S")
    endwith
  endfunc

  add object oStr_5_26 as StdString with uid="LMYLYGDRRS",Visible=.t., Left=24, Top=345,;
    Alignment=1, Width=56, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_5_26.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S')
    endwith
  endfunc

  add object oStr_5_28 as StdString with uid="QNOBIMPXSP",Visible=.t., Left=10, Top=391,;
    Alignment=1, Width=70, Height=18,;
    Caption="Riga iniziale:"  ;
  , bGlobalFont=.t.

  func oStr_5_28.mHide()
    with this.Parent.oContained
      return (.w_FLGFILE <> 'S' OR EMPTY(.w_FILEEXC))
    endwith
  endfunc

  add object oBox_5_12 as StdBox with uid="CACZPQPLOB",left=9, top=337, width=356,height=138
enddefine

* --- Defining Body row
define class tgsim_msoBodyRow as CPBodyRowCnt
  Width=584
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFLNAME_2_1 as StdTrsField with uid="MZSKTKVLXE",rtseq=3,rtrep=.t.,;
    cFormVar="w_FLNAME",value=space(30),isprimarykey=.t.,;
    HelpContextID = 196316842,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=186, Left=-2, Top=0, InputMask=replicate('X',30)

  add object oFLTYPE_2_2 as StdTrsField with uid="WQPXMSUELG",rtseq=4,rtrep=.t.,;
    cFormVar="w_FLTYPE",value=space(1),;
    HelpContextID = 191573674,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=189, Top=0, InputMask=replicate('X',1)

  add object oFLLENGHT_2_3 as StdTrsField with uid="UXKTBYPJZJ",rtseq=5,rtrep=.t.,;
    cFormVar="w_FLLENGHT",value=0,;
    HelpContextID = 106975658,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=216, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oFLDECIMA_2_4 as StdTrsField with uid="XSYPAOHRJE",rtseq=6,rtrep=.t.,;
    cFormVar="w_FLDECIMA",value=0,;
    HelpContextID = 139472489,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=266, Top=0, cSayPict=["99"], cGetPict=["99"]

  add object oFLCOMMEN_2_5 as StdTrsField with uid="TLRGJILEWE",rtseq=7,rtrep=.t.,;
    cFormVar="w_FLCOMMEN",value=space(80),;
    HelpContextID = 207208868,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=266, Left=313, Top=0, InputMask=replicate('X',80)
  add object oLast as LastKeyMover
  * ---
  func oFLNAME_2_1.When()
    return(.t.)
  proc oFLNAME_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFLNAME_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mso','SRCMODBC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SOCODICE=SRCMODBC.SOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsim_mso
DEFINE CLASS ah_CBrowse AS Grid

  cCursor=""

  * Standard
  RecordSource=""
  RecordSourceType=1   && ALIAS
  ColumnCount=0
  ReadOnly=.t.
  DeleteMark=.f.

  proc Calculate(xValue)
  endproc

  proc Event(cEvent)
    if cEvent="Browse"
       this.Browse()
    endif
  endproc

  proc Browse
    local nI,cI,oCol
    with this
       .RecordSource=''
       .ColumnCount=0
       if !empty(.cCursor)
          .ColumnCount=fcount(.cCursor)
          .recordsource=.cCursor
          for nI=1 to .ColumnCount
	    cI=alltrim(str(nI))
	    oCol=.Columns(nI)
            oCol.Header1.Caption=field(nI,.cCursor)
   	    oCol.Bound=.f.
            oCol.ReadOnly=.ReadOnly
   	    oCol.ControlSource=alltrim(.cCursor)+'.'+field(nI,.cCursor)
   	    oCol.SelectOnEntry=.f.   	
	   next
       endif
    endwith
    Grid::Refresh()
  endproc

ENDDEFINE
* --- Fine Area Manuale
