* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kma                                                        *
*              Import magazzino e documenti                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2013-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kma",oParentObject))

* --- Class definition
define class tgsim_kma as StdForm
  Top    = 24
  Left   = 88

  * --- Standard Properties
  Width  = 636
  Height = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-16"
  HelpContextID=199868265
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  IMPORTAZ_IDX = 0
  cPrg = "gsim_kma"
  cComment = "Import magazzino e documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_INCORSO = .F.
  w_TIPSRC = space(1)
  o_TIPSRC = space(1)
  w_CODIMP = space(20)
  w_DESIMP = space(60)
  w_PATH = space(200)
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_SELELS = space(10)
  w_SELEAR = space(10)
  w_SELEAA = space(10)
  w_SELEAB = space(10)
  w_SELELA = space(10)
  w_SELEAN = space(10)
  w_SELECI = space(10)
  w_SELECG = space(10)
  w_SELECP = space(10)
  w_SELECD = space(10)
  w_SELECM = space(10)
  w_SELESE = space(10)
  w_SELEKA = space(10)
  w_SELECN = space(10)
  w_SELECX = space(10)
  w_SELEDB = space(10)
  w_SELEGM = space(10)
  w_SELEMA = space(10)
  w_SELENO = space(10)
  w_SELEIN = space(10)
  w_SELEIF = space(10)
  w_SELEIC = space(10)
  w_SELEIS = space(10)
  w_SELEPR = space(10)
  w_SELESM = space(10)
  w_SELERI = space(10)
  w_SELETC = space(10)
  w_SELETT = space(10)
  w_SELETO = space(10)
  w_SELETR = space(10)
  w_SELEUM = space(10)
  w_SELEDT = space(10)
  w_SELEDR = space(10)
  w_SELEDO = space(10)
  o_SELEDO = space(10)
  w_SELEMM = space(10)
  w_SELETA = space(10)
  w_SELETM = space(10)
  w_SELETP = space(10)
  w_RADSELEZ = space(10)
  w_SELECONT = space(10)
  w_SELERESO = space(10)
  w_IMPARIVA = space(10)
  w_IMAZZERA = space(10)
  w_IMTRANSA = space(1)
  w_IMCONFER = space(1)
  w_ArchParz = space(45)
  w_ArchPar2 = space(45)
  w_Gestiti = space(45)
  w_SELECS = space(10)
  w_TIPDBF = space(1)
  w_SELECF = space(10)
  * --- Area Manuale = Declare Variables
  * --- gsim_kma
  Autocenter=.T.
  
  Function GetControlObject(cVar,nPage)
    local oObj
    local nIndex
    oObj = .NULL.
    cVar=upper(cVar)
    with this.oPgFrm.Pages(nPage).oPag
      for nIndex=1 to .ControlCount
          if type('this.oPgFrm.Pages(nPage).oPag.Controls(nIndex).cFormVar') = 'C'
             if upper(.Controls(nIndex).cFormVar) == cVar
                oObj= .Controls(nIndex)
                nIndex = .ControlCount + 1
             endif
          endif
      next
    endwith
    return (oObj)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kmaPag1","gsim_kma",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODIMP_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='IMPORTAZ'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gsim_bac with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_INCORSO=.f.
      .w_TIPSRC=space(1)
      .w_CODIMP=space(20)
      .w_DESIMP=space(60)
      .w_PATH=space(200)
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_SELELS=space(10)
      .w_SELEAR=space(10)
      .w_SELEAA=space(10)
      .w_SELEAB=space(10)
      .w_SELELA=space(10)
      .w_SELEAN=space(10)
      .w_SELECI=space(10)
      .w_SELECG=space(10)
      .w_SELECP=space(10)
      .w_SELECD=space(10)
      .w_SELECM=space(10)
      .w_SELESE=space(10)
      .w_SELEKA=space(10)
      .w_SELECN=space(10)
      .w_SELECX=space(10)
      .w_SELEDB=space(10)
      .w_SELEGM=space(10)
      .w_SELEMA=space(10)
      .w_SELENO=space(10)
      .w_SELEIN=space(10)
      .w_SELEIF=space(10)
      .w_SELEIC=space(10)
      .w_SELEIS=space(10)
      .w_SELEPR=space(10)
      .w_SELESM=space(10)
      .w_SELERI=space(10)
      .w_SELETC=space(10)
      .w_SELETT=space(10)
      .w_SELETO=space(10)
      .w_SELETR=space(10)
      .w_SELEUM=space(10)
      .w_SELEDT=space(10)
      .w_SELEDR=space(10)
      .w_SELEDO=space(10)
      .w_SELEMM=space(10)
      .w_SELETA=space(10)
      .w_SELETM=space(10)
      .w_SELETP=space(10)
      .w_RADSELEZ=space(10)
      .w_SELECONT=space(10)
      .w_SELERESO=space(10)
      .w_IMPARIVA=space(10)
      .w_IMAZZERA=space(10)
      .w_IMTRANSA=space(1)
      .w_IMCONFER=space(1)
      .w_ArchParz=space(45)
      .w_ArchPar2=space(45)
      .w_Gestiti=space(45)
      .w_SELECS=space(10)
      .w_TIPDBF=space(1)
      .w_SELECF=space(10)
        .w_INCORSO = .F.
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODIMP))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
          .DoRTCalc(6,6,.f.)
        .w_ODBCPATH = SPACE(200)
          .DoRTCalc(8,9,.f.)
        .w_SELELS = 'LS'
        .w_SELEAR = 'AR'
        .w_SELEAA = 'AA'
        .w_SELEAB = 'AB'
        .w_SELELA = 'LA'
        .w_SELEAN = 'AN'
        .w_SELECI = 'CI'
        .w_SELECG = 'CG'
        .w_SELECP = 'CP'
        .w_SELECD = 'CD'
        .w_SELECM = 'CM'
        .w_SELESE = 'SE'
        .w_SELEKA = 'KA'
        .w_SELECN = 'CN'
        .w_SELECX = 'CX'
        .w_SELEDB = 'DB'
        .w_SELEGM = 'GM'
        .w_SELEMA = 'MA'
        .w_SELENO = 'NO'
        .w_SELEIN = 'IN'
        .w_SELEIF = 'IF'
        .w_SELEIC = 'IC'
        .w_SELEIS = 'IS'
        .w_SELEPR = 'PR'
        .w_SELESM = 'SM'
        .w_SELERI = 'RI'
        .w_SELETC = 'TC'
        .w_SELETT = 'TT'
        .w_SELETO = 'TO'
        .w_SELETR = 'TR'
        .w_SELEUM = 'UM'
        .w_SELEDT = 'DT'
        .w_SELEDR = 'DR'
        .w_SELEDO = 'DO'
        .w_SELEMM = 'MM'
        .w_SELETA = 'TA'
        .w_SELETM = 'TM'
        .w_SELETP = 'TP'
        .w_RADSELEZ = 'D'
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .w_SELECONT = 'N'
        .w_SELERESO = 'S'
        .w_IMPARIVA = 'S'
        .w_IMAZZERA = 'N'
        .w_IMTRANSA = IIF( .w_SELEDO<>"DO" , "S" , "N" )
        .w_IMCONFER = 'N'
      .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .w_ArchParz = .w_SELEAR+"|"+.w_SELECM+"|"+.w_SELECD+"|"+.w_SELEMA+"|"+.w_SELEKA+"|"+.w_SELELS+"|"+.w_SELECN+"|"+.w_SELEUM+"|"+.w_SELECP+"|"
        .w_ArchPar2 = .w_SELESM+"|"+.w_SELELA+"|"+.w_SELEMM+"|"+.w_SELEDT+"|"+.w_SELEDR+"|"+.w_SELEGM+"|"+.w_SELECG+"|"+.w_SELETT+"|"+.w_SELENO+"|"+.w_SELETC+"|"+.w_SELEAN+"|"+.w_SELEIN+"|"+.w_SELEIC+"|"+.w_SELEIS+"|"+.w_SELEIF+"|"+.w_SELECI+"|"+.w_SELECX+IIF(.w_SELECX='CX',"|XX","")+IIF(.w_SELECD='CD',"|DC","")+"|"+.w_SELETR+"|"+.w_SELETO+"|"+.w_SELEDB+"|"+.w_SELERI+"|"+.w_SELESE+"|"+.w_SELEAB+"|"+.w_SELEPR+"|"+.w_SELETA+"|"+.w_SELETM+"|"+.w_SELETP+"|"+.w_SELEAA+"I"+.w_SELECF+"I"+.w_SELEDO
        .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
        .w_SELECS = '  '
          .DoRTCalc(59,59,.f.)
        .w_SELECF = 'CF'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPSRC<>.w_TIPSRC
            .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
        endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(6,52,.t.)
        if .o_SELEDO<>.w_SELEDO
            .w_IMTRANSA = IIF( .w_SELEDO<>"DO" , "S" , "N" )
        endif
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .DoRTCalc(54,54,.t.)
            .w_ArchParz = .w_SELEAR+"|"+.w_SELECM+"|"+.w_SELECD+"|"+.w_SELEMA+"|"+.w_SELEKA+"|"+.w_SELELS+"|"+.w_SELECN+"|"+.w_SELEUM+"|"+.w_SELECP+"|"
            .w_ArchPar2 = .w_SELESM+"|"+.w_SELELA+"|"+.w_SELEMM+"|"+.w_SELEDT+"|"+.w_SELEDR+"|"+.w_SELEGM+"|"+.w_SELECG+"|"+.w_SELETT+"|"+.w_SELENO+"|"+.w_SELETC+"|"+.w_SELEAN+"|"+.w_SELEIN+"|"+.w_SELEIC+"|"+.w_SELEIS+"|"+.w_SELEIF+"|"+.w_SELECI+"|"+.w_SELECX+IIF(.w_SELECX='CX',"|XX","")+IIF(.w_SELECD='CD',"|DC","")+"|"+.w_SELETR+"|"+.w_SELETO+"|"+.w_SELEDB+"|"+.w_SELERI+"|"+.w_SELESE+"|"+.w_SELEAB+"|"+.w_SELEPR+"|"+.w_SELETA+"|"+.w_SELETM+"|"+.w_SELETP+"|"+.w_SELEAA+"I"+.w_SELECF+"I"+.w_SELEDO
            .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
            .w_SELECS = '  '
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(59,60,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODIMP_1_3.enabled = this.oPgFrm.Page1.oPag.oCODIMP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oPATH_1_5.enabled = this.oPgFrm.Page1.oPag.oPATH_1_5.mCond()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_6.enabled = this.oPgFrm.Page1.oPag.oODBCDSN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_8.enabled = this.oPgFrm.Page1.oPag.oODBCPATH_1_8.mCond()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_10.enabled = this.oPgFrm.Page1.oPag.oODBCUSER_1_10.mCond()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.enabled = this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSELELS_1_15.enabled = this.oPgFrm.Page1.oPag.oSELELS_1_15.mCond()
    this.oPgFrm.Page1.oPag.oSELEAR_1_16.enabled = this.oPgFrm.Page1.oPag.oSELEAR_1_16.mCond()
    this.oPgFrm.Page1.oPag.oSELEAA_1_17.enabled = this.oPgFrm.Page1.oPag.oSELEAA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oSELEAB_1_18.enabled = this.oPgFrm.Page1.oPag.oSELEAB_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSELELA_1_19.enabled = this.oPgFrm.Page1.oPag.oSELELA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oSELEAN_1_20.enabled = this.oPgFrm.Page1.oPag.oSELEAN_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSELECI_1_21.enabled = this.oPgFrm.Page1.oPag.oSELECI_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSELECG_1_22.enabled = this.oPgFrm.Page1.oPag.oSELECG_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSELECP_1_23.enabled = this.oPgFrm.Page1.oPag.oSELECP_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSELECD_1_24.enabled = this.oPgFrm.Page1.oPag.oSELECD_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSELECM_1_25.enabled = this.oPgFrm.Page1.oPag.oSELECM_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSELESE_1_26.enabled = this.oPgFrm.Page1.oPag.oSELESE_1_26.mCond()
    this.oPgFrm.Page1.oPag.oSELEKA_1_27.enabled = this.oPgFrm.Page1.oPag.oSELEKA_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSELECN_1_28.enabled = this.oPgFrm.Page1.oPag.oSELECN_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSELECX_1_29.enabled = this.oPgFrm.Page1.oPag.oSELECX_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSELEDB_1_30.enabled = this.oPgFrm.Page1.oPag.oSELEDB_1_30.mCond()
    this.oPgFrm.Page1.oPag.oSELEGM_1_31.enabled = this.oPgFrm.Page1.oPag.oSELEGM_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSELEMA_1_32.enabled = this.oPgFrm.Page1.oPag.oSELEMA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSELENO_1_33.enabled = this.oPgFrm.Page1.oPag.oSELENO_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSELEIN_1_34.enabled = this.oPgFrm.Page1.oPag.oSELEIN_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSELEIF_1_35.enabled = this.oPgFrm.Page1.oPag.oSELEIF_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSELEIC_1_36.enabled = this.oPgFrm.Page1.oPag.oSELEIC_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSELEIS_1_37.enabled = this.oPgFrm.Page1.oPag.oSELEIS_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSELEPR_1_38.enabled = this.oPgFrm.Page1.oPag.oSELEPR_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSELESM_1_40.enabled = this.oPgFrm.Page1.oPag.oSELESM_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSELERI_1_41.enabled = this.oPgFrm.Page1.oPag.oSELERI_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSELETC_1_42.enabled = this.oPgFrm.Page1.oPag.oSELETC_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSELETT_1_43.enabled = this.oPgFrm.Page1.oPag.oSELETT_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSELETO_1_44.enabled = this.oPgFrm.Page1.oPag.oSELETO_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSELETR_1_45.enabled = this.oPgFrm.Page1.oPag.oSELETR_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSELEUM_1_46.enabled = this.oPgFrm.Page1.oPag.oSELEUM_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSELEDT_1_48.enabled = this.oPgFrm.Page1.oPag.oSELEDT_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSELEDR_1_49.enabled = this.oPgFrm.Page1.oPag.oSELEDR_1_49.mCond()
    this.oPgFrm.Page1.oPag.oSELEDO_1_50.enabled = this.oPgFrm.Page1.oPag.oSELEDO_1_50.mCond()
    this.oPgFrm.Page1.oPag.oSELEMM_1_51.enabled = this.oPgFrm.Page1.oPag.oSELEMM_1_51.mCond()
    this.oPgFrm.Page1.oPag.oSELETA_1_52.enabled = this.oPgFrm.Page1.oPag.oSELETA_1_52.mCond()
    this.oPgFrm.Page1.oPag.oSELETM_1_53.enabled = this.oPgFrm.Page1.oPag.oSELETM_1_53.mCond()
    this.oPgFrm.Page1.oPag.oSELETP_1_54.enabled = this.oPgFrm.Page1.oPag.oSELETP_1_54.mCond()
    this.oPgFrm.Page1.oPag.oRADSELEZ_1_55.enabled_(this.oPgFrm.Page1.oPag.oRADSELEZ_1_55.mCond())
    this.oPgFrm.Page1.oPag.oSELECONT_1_60.enabled = this.oPgFrm.Page1.oPag.oSELECONT_1_60.mCond()
    this.oPgFrm.Page1.oPag.oSELERESO_1_61.enabled = this.oPgFrm.Page1.oPag.oSELERESO_1_61.mCond()
    this.oPgFrm.Page1.oPag.oIMPARIVA_1_62.enabled = this.oPgFrm.Page1.oPag.oIMPARIVA_1_62.mCond()
    this.oPgFrm.Page1.oPag.oIMAZZERA_1_63.enabled = this.oPgFrm.Page1.oPag.oIMAZZERA_1_63.mCond()
    this.oPgFrm.Page1.oPag.oIMTRANSA_1_64.enabled = this.oPgFrm.Page1.oPag.oIMTRANSA_1_64.mCond()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_65.enabled = this.oPgFrm.Page1.oPag.oIMCONFER_1_65.mCond()
    this.oPgFrm.Page1.oPag.oSELECF_1_83.enabled = this.oPgFrm.Page1.oPag.oSELECF_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPATH_1_5.visible=!this.oPgFrm.Page1.oPag.oPATH_1_5.mHide()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_6.visible=!this.oPgFrm.Page1.oPag.oODBCDSN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_8.visible=!this.oPgFrm.Page1.oPag.oODBCPATH_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_10.visible=!this.oPgFrm.Page1.oPag.oODBCUSER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.visible=!this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_65.visible=!this.oPgFrm.Page1.oPag.oIMCONFER_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_3'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(60))
      this.w_IMPARIVA = NVL(_Link_.IMPARIVA,space(10))
      this.w_IMAZZERA = NVL(_Link_.IMAZZERA,space(10))
      this.w_SELERESO = NVL(_Link_.IMRESOCO,space(10))
      this.w_SELECONT = NVL(_Link_.IMCONTRO,space(10))
      this.w_TIPSRC = NVL(_Link_.IMTIPSRC,space(1))
      this.w_TIPDBF = NVL(_Link_.IMTIPDBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(60)
      this.w_IMPARIVA = space(10)
      this.w_IMAZZERA = space(10)
      this.w_SELERESO = space(10)
      this.w_SELECONT = space(10)
      this.w_TIPSRC = space(1)
      this.w_TIPDBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_3.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_3.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_4.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_4.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_5.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_5.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCDSN_1_6.value==this.w_ODBCDSN)
      this.oPgFrm.Page1.oPag.oODBCDSN_1_6.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPATH_1_8.value==this.w_ODBCPATH)
      this.oPgFrm.Page1.oPag.oODBCPATH_1_8.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCUSER_1_10.value==this.w_ODBCUSER)
      this.oPgFrm.Page1.oPag.oODBCUSER_1_10.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.value==this.w_ODBCPASSW)
      this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page1.oPag.oSELELS_1_15.RadioValue()==this.w_SELELS)
      this.oPgFrm.Page1.oPag.oSELELS_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAR_1_16.RadioValue()==this.w_SELEAR)
      this.oPgFrm.Page1.oPag.oSELEAR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAA_1_17.RadioValue()==this.w_SELEAA)
      this.oPgFrm.Page1.oPag.oSELEAA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAB_1_18.RadioValue()==this.w_SELEAB)
      this.oPgFrm.Page1.oPag.oSELEAB_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELELA_1_19.RadioValue()==this.w_SELELA)
      this.oPgFrm.Page1.oPag.oSELELA_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAN_1_20.RadioValue()==this.w_SELEAN)
      this.oPgFrm.Page1.oPag.oSELEAN_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECI_1_21.RadioValue()==this.w_SELECI)
      this.oPgFrm.Page1.oPag.oSELECI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECG_1_22.RadioValue()==this.w_SELECG)
      this.oPgFrm.Page1.oPag.oSELECG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECP_1_23.RadioValue()==this.w_SELECP)
      this.oPgFrm.Page1.oPag.oSELECP_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECD_1_24.RadioValue()==this.w_SELECD)
      this.oPgFrm.Page1.oPag.oSELECD_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECM_1_25.RadioValue()==this.w_SELECM)
      this.oPgFrm.Page1.oPag.oSELECM_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESE_1_26.RadioValue()==this.w_SELESE)
      this.oPgFrm.Page1.oPag.oSELESE_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEKA_1_27.RadioValue()==this.w_SELEKA)
      this.oPgFrm.Page1.oPag.oSELEKA_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECN_1_28.RadioValue()==this.w_SELECN)
      this.oPgFrm.Page1.oPag.oSELECN_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECX_1_29.RadioValue()==this.w_SELECX)
      this.oPgFrm.Page1.oPag.oSELECX_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDB_1_30.RadioValue()==this.w_SELEDB)
      this.oPgFrm.Page1.oPag.oSELEDB_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEGM_1_31.RadioValue()==this.w_SELEGM)
      this.oPgFrm.Page1.oPag.oSELEGM_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEMA_1_32.RadioValue()==this.w_SELEMA)
      this.oPgFrm.Page1.oPag.oSELEMA_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENO_1_33.RadioValue()==this.w_SELENO)
      this.oPgFrm.Page1.oPag.oSELENO_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIN_1_34.RadioValue()==this.w_SELEIN)
      this.oPgFrm.Page1.oPag.oSELEIN_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIF_1_35.RadioValue()==this.w_SELEIF)
      this.oPgFrm.Page1.oPag.oSELEIF_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIC_1_36.RadioValue()==this.w_SELEIC)
      this.oPgFrm.Page1.oPag.oSELEIC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIS_1_37.RadioValue()==this.w_SELEIS)
      this.oPgFrm.Page1.oPag.oSELEIS_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPR_1_38.RadioValue()==this.w_SELEPR)
      this.oPgFrm.Page1.oPag.oSELEPR_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESM_1_40.RadioValue()==this.w_SELESM)
      this.oPgFrm.Page1.oPag.oSELESM_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERI_1_41.RadioValue()==this.w_SELERI)
      this.oPgFrm.Page1.oPag.oSELERI_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETC_1_42.RadioValue()==this.w_SELETC)
      this.oPgFrm.Page1.oPag.oSELETC_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETT_1_43.RadioValue()==this.w_SELETT)
      this.oPgFrm.Page1.oPag.oSELETT_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETO_1_44.RadioValue()==this.w_SELETO)
      this.oPgFrm.Page1.oPag.oSELETO_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETR_1_45.RadioValue()==this.w_SELETR)
      this.oPgFrm.Page1.oPag.oSELETR_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEUM_1_46.RadioValue()==this.w_SELEUM)
      this.oPgFrm.Page1.oPag.oSELEUM_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDT_1_48.RadioValue()==this.w_SELEDT)
      this.oPgFrm.Page1.oPag.oSELEDT_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDR_1_49.RadioValue()==this.w_SELEDR)
      this.oPgFrm.Page1.oPag.oSELEDR_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDO_1_50.RadioValue()==this.w_SELEDO)
      this.oPgFrm.Page1.oPag.oSELEDO_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEMM_1_51.RadioValue()==this.w_SELEMM)
      this.oPgFrm.Page1.oPag.oSELEMM_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETA_1_52.RadioValue()==this.w_SELETA)
      this.oPgFrm.Page1.oPag.oSELETA_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETM_1_53.RadioValue()==this.w_SELETM)
      this.oPgFrm.Page1.oPag.oSELETM_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETP_1_54.RadioValue()==this.w_SELETP)
      this.oPgFrm.Page1.oPag.oSELETP_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRADSELEZ_1_55.RadioValue()==this.w_RADSELEZ)
      this.oPgFrm.Page1.oPag.oRADSELEZ_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECONT_1_60.RadioValue()==this.w_SELECONT)
      this.oPgFrm.Page1.oPag.oSELECONT_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERESO_1_61.RadioValue()==this.w_SELERESO)
      this.oPgFrm.Page1.oPag.oSELERESO_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPARIVA_1_62.RadioValue()==this.w_IMPARIVA)
      this.oPgFrm.Page1.oPag.oIMPARIVA_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAZZERA_1_63.RadioValue()==this.w_IMAZZERA)
      this.oPgFrm.Page1.oPag.oIMAZZERA_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTRANSA_1_64.RadioValue()==this.w_IMTRANSA)
      this.oPgFrm.Page1.oPag.oIMTRANSA_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCONFER_1_65.RadioValue()==this.w_IMCONFER)
      this.oPgFrm.Page1.oPag.oIMCONFER_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECF_1_83.RadioValue()==this.w_SELECF)
      this.oPgFrm.Page1.oPag.oSELECF_1_83.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODIMP))  and (!.w_INCORSO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMP_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPSRC = this.w_TIPSRC
    this.o_SELEDO = this.w_SELEDO
    return

enddefine

* --- Define pages as container
define class tgsim_kmaPag1 as StdContainer
  Width  = 632
  height = 497
  stdWidth  = 632
  stdheight = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODIMP_1_3 as StdField with uid="JVRUITIVQR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 114043866,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=104, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  func oCODIMP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_3.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oDESIMP_1_4 as StdField with uid="OMDLTUIPBE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 113984970,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=263, Top=13, InputMask=replicate('X',60)

  add object oPATH_1_5 as StdField with uid="TEKEDCGFLL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 194787594,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=104, Top=42, InputMask=replicate('X',200)

  func oPATH_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC<>"O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oPATH_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oODBCDSN_1_6 as StdField with uid="CCNSWIOJJL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 73553434,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=105, Top=42, InputMask=replicate('X',30)

  func oODBCDSN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCDSN_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPATH_1_8 as StdField with uid="OTCZZZYUDO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 173910574,;
   bGlobalFont=.t.,;
    Height=21, Width=194, Left=105, Top=65, InputMask=replicate('X',200)

  func oODBCPATH_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDBF='S')
    endwith
   endif
  endfunc

  func oODBCPATH_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="JKKEBCSAFN",left=299, top=67, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 199667242;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDBF='S')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDBF<>'S')
     endwith
    endif
  endfunc

  add object oODBCUSER_1_10 as StdField with uid="KUQMZDJHWU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente per connessione ODBC",;
    HelpContextID = 212707896,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=393, Top=42, InputMask=replicate('X',30)

  func oODBCUSER_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCUSER_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPASSW_1_11 as StdField with uid="DLZRZHNCSZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Password per connessione ODBC",;
    HelpContextID = 94523479,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=393, Top=65, InputMask=replicate('X',30), PasswordChar='*'

  func oODBCPASSW_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCPASSW_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc


  add object oBtn_1_12 as StdButton with uid="SJQLZNXFRO",left=583, top=42, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 199667242;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPSRC<>"O")
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPSRC="O")
     endwith
    endif
  endfunc

  add object oSELELS_1_15 as StdCheck with uid="ILDBWXOABO",rtseq=10,rtrep=.f.,left=30, top=112, caption="Anagrafica listini",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 64992474,;
    cFormVar="w_SELELS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELELS_1_15.RadioValue()
    return(iif(this.value =1,'LS',;
    ' '))
  endfunc
  func oSELELS_1_15.GetRadio()
    this.Parent.oContained.w_SELELS = this.RadioValue()
    return .t.
  endfunc

  func oSELELS_1_15.SetRadio()
    this.Parent.oContained.w_SELELS=trim(this.Parent.oContained.w_SELELS)
    this.value = ;
      iif(this.Parent.oContained.w_SELELS=='LS',1,;
      0)
  endfunc

  func oSELELS_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELELS<>"|")
    endwith
   endif
  endfunc

  add object oSELEAR_1_16 as StdCheck with uid="QUOKTCPHUC",rtseq=11,rtrep=.f.,left=30, top=130, caption="Articoli",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 93304026,;
    cFormVar="w_SELEAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAR_1_16.RadioValue()
    return(iif(this.value =1,'AR',;
    ' '))
  endfunc
  func oSELEAR_1_16.GetRadio()
    this.Parent.oContained.w_SELEAR = this.RadioValue()
    return .t.
  endfunc

  func oSELEAR_1_16.SetRadio()
    this.Parent.oContained.w_SELEAR=trim(this.Parent.oContained.w_SELEAR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAR=='AR',1,;
      0)
  endfunc

  func oSELEAR_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAR<>"|")
    endwith
   endif
  endfunc

  add object oSELEAA_1_17 as StdCheck with uid="CITWHYBTON",rtseq=12,rtrep=.f.,left=30, top=148, caption="Articoli alternativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 110081242,;
    cFormVar="w_SELEAA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAA_1_17.RadioValue()
    return(iif(this.value =1,'AA',;
    ' '))
  endfunc
  func oSELEAA_1_17.GetRadio()
    this.Parent.oContained.w_SELEAA = this.RadioValue()
    return .t.
  endfunc

  func oSELEAA_1_17.SetRadio()
    this.Parent.oContained.w_SELEAA=trim(this.Parent.oContained.w_SELEAA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAA=='AA',1,;
      0)
  endfunc

  func oSELEAA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAA<>"|")
    endwith
   endif
  endfunc

  add object oSELEAB_1_18 as StdCheck with uid="VSIOJCZNKB",rtseq=13,rtrep=.f.,left=30, top=166, caption="Articoli distinta base",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 93304026,;
    cFormVar="w_SELEAB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAB_1_18.RadioValue()
    return(iif(this.value =1,'AB',;
    ' '))
  endfunc
  func oSELEAB_1_18.GetRadio()
    this.Parent.oContained.w_SELEAB = this.RadioValue()
    return .t.
  endfunc

  func oSELEAB_1_18.SetRadio()
    this.Parent.oContained.w_SELEAB=trim(this.Parent.oContained.w_SELEAB)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAB=='AB',1,;
      0)
  endfunc

  func oSELEAB_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAB<>"|")
    endwith
   endif
  endfunc

  add object oSELELA_1_19 as StdCheck with uid="MXIWGDPJDB",rtseq=14,rtrep=.f.,left=30, top=184, caption="Articoli listini",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 98546906,;
    cFormVar="w_SELELA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELELA_1_19.RadioValue()
    return(iif(this.value =1,'LA',;
    ' '))
  endfunc
  func oSELELA_1_19.GetRadio()
    this.Parent.oContained.w_SELELA = this.RadioValue()
    return .t.
  endfunc

  func oSELELA_1_19.SetRadio()
    this.Parent.oContained.w_SELELA=trim(this.Parent.oContained.w_SELELA)
    this.value = ;
      iif(this.Parent.oContained.w_SELELA=='LA',1,;
      0)
  endfunc

  func oSELELA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELELA<>"|")
    endwith
   endif
  endfunc

  add object oSELEAN_1_20 as StdCheck with uid="YUBWUWSZAH",rtseq=15,rtrep=.f.,left=30, top=202, caption="Articoli note",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 160412890,;
    cFormVar="w_SELEAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAN_1_20.RadioValue()
    return(iif(this.value =1,'AN',;
    ' '))
  endfunc
  func oSELEAN_1_20.GetRadio()
    this.Parent.oContained.w_SELEAN = this.RadioValue()
    return .t.
  endfunc

  func oSELEAN_1_20.SetRadio()
    this.Parent.oContained.w_SELEAN=trim(this.Parent.oContained.w_SELEAN)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAN=='AN',1,;
      0)
  endfunc

  func oSELEAN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAN<>"|")
    endwith
   endif
  endfunc

  add object oSELECI_1_21 as StdCheck with uid="WXFVQGCHQI",rtseq=16,rtrep=.f.,left=30, top=220, caption="Categorie contabili articoli",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 242201818,;
    cFormVar="w_SELECI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECI_1_21.RadioValue()
    return(iif(this.value =1,'CI',;
    ' '))
  endfunc
  func oSELECI_1_21.GetRadio()
    this.Parent.oContained.w_SELECI = this.RadioValue()
    return .t.
  endfunc

  func oSELECI_1_21.SetRadio()
    this.Parent.oContained.w_SELECI=trim(this.Parent.oContained.w_SELECI)
    this.value = ;
      iif(this.Parent.oContained.w_SELECI=='CI',1,;
      0)
  endfunc

  func oSELECI_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECI<>"|")
    endwith
   endif
  endfunc

  add object oSELECG_1_22 as StdCheck with uid="XMYZMFXTYD",rtseq=17,rtrep=.f.,left=30, top=238, caption="Categorie omogenee",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7320794,;
    cFormVar="w_SELECG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECG_1_22.RadioValue()
    return(iif(this.value =1,'CG',;
    ' '))
  endfunc
  func oSELECG_1_22.GetRadio()
    this.Parent.oContained.w_SELECG = this.RadioValue()
    return .t.
  endfunc

  func oSELECG_1_22.SetRadio()
    this.Parent.oContained.w_SELECG=trim(this.Parent.oContained.w_SELECG)
    this.value = ;
      iif(this.Parent.oContained.w_SELECG=='CG',1,;
      0)
  endfunc

  func oSELECG_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECG<>"|")
    endwith
   endif
  endfunc

  add object oSELECP_1_23 as StdCheck with uid="OVMELIAGZX",rtseq=18,rtrep=.f.,left=30, top=256, caption="Categorie provvigioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 124761306,;
    cFormVar="w_SELECP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECP_1_23.RadioValue()
    return(iif(this.value =1,'CP',;
    ' '))
  endfunc
  func oSELECP_1_23.GetRadio()
    this.Parent.oContained.w_SELECP = this.RadioValue()
    return .t.
  endfunc

  func oSELECP_1_23.SetRadio()
    this.Parent.oContained.w_SELECP=trim(this.Parent.oContained.w_SELECP)
    this.value = ;
      iif(this.Parent.oContained.w_SELECP=='CP',1,;
      0)
  endfunc

  func oSELECP_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECP<>"|")
    endwith
   endif
  endfunc

  add object oSELECD_1_24 as StdCheck with uid="HOKNJUYPON",rtseq=19,rtrep=.f.,left=30, top=274, caption="Causali documenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 57652442,;
    cFormVar="w_SELECD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECD_1_24.RadioValue()
    return(iif(this.value =1,'CD',;
    ' '))
  endfunc
  func oSELECD_1_24.GetRadio()
    this.Parent.oContained.w_SELECD = this.RadioValue()
    return .t.
  endfunc

  func oSELECD_1_24.SetRadio()
    this.Parent.oContained.w_SELECD=trim(this.Parent.oContained.w_SELECD)
    this.value = ;
      iif(this.Parent.oContained.w_SELECD=='CD',1,;
      0)
  endfunc

  func oSELECD_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECD<>"|")
    endwith
   endif
  endfunc

  add object oSELECM_1_25 as StdCheck with uid="BKBWIAORLL",rtseq=20,rtrep=.f.,left=30, top=292, caption="Causali magazzino",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 175092954,;
    cFormVar="w_SELECM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECM_1_25.RadioValue()
    return(iif(this.value =1,'CM',;
    ' '))
  endfunc
  func oSELECM_1_25.GetRadio()
    this.Parent.oContained.w_SELECM = this.RadioValue()
    return .t.
  endfunc

  func oSELECM_1_25.SetRadio()
    this.Parent.oContained.w_SELECM=trim(this.Parent.oContained.w_SELECM)
    this.value = ;
      iif(this.Parent.oContained.w_SELECM=='CM',1,;
      0)
  endfunc

  func oSELECM_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECM<>"|")
    endwith
   endif
  endfunc

  add object oSELESE_1_26 as StdCheck with uid="PJOJGKCJTY",rtseq=21,rtrep=.f.,left=30, top=310, caption="Cicli semplificati",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 24098010,;
    cFormVar="w_SELESE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESE_1_26.RadioValue()
    return(iif(this.value =1,'SE',;
    ' '))
  endfunc
  func oSELESE_1_26.GetRadio()
    this.Parent.oContained.w_SELESE = this.RadioValue()
    return .t.
  endfunc

  func oSELESE_1_26.SetRadio()
    this.Parent.oContained.w_SELESE=trim(this.Parent.oContained.w_SELESE)
    this.value = ;
      iif(this.Parent.oContained.w_SELESE=='SE',1,;
      0)
  endfunc

  func oSELESE_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESE<>"|")
    endwith
   endif
  endfunc

  add object oSELEKA_1_27 as StdCheck with uid="WOTHKIKIOK",rtseq=22,rtrep=.f.,left=30, top=328, caption="Codici articoli / servizi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 99595482,;
    cFormVar="w_SELEKA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEKA_1_27.RadioValue()
    return(iif(this.value =1,'KA',;
    ' '))
  endfunc
  func oSELEKA_1_27.GetRadio()
    this.Parent.oContained.w_SELEKA = this.RadioValue()
    return .t.
  endfunc

  func oSELEKA_1_27.SetRadio()
    this.Parent.oContained.w_SELEKA=trim(this.Parent.oContained.w_SELEKA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEKA=='KA',1,;
      0)
  endfunc

  func oSELEKA_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEKA<>"|")
    endwith
   endif
  endfunc

  add object oSELECN_1_28 as StdCheck with uid="EWWZNIKBBB",rtseq=23,rtrep=.f.,left=30, top=346, caption="Contratti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 158315738,;
    cFormVar="w_SELECN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECN_1_28.RadioValue()
    return(iif(this.value =1,'CN',;
    ' '))
  endfunc
  func oSELECN_1_28.GetRadio()
    this.Parent.oContained.w_SELECN = this.RadioValue()
    return .t.
  endfunc

  func oSELECN_1_28.SetRadio()
    this.Parent.oContained.w_SELECN=trim(this.Parent.oContained.w_SELECN)
    this.value = ;
      iif(this.Parent.oContained.w_SELECN=='CN',1,;
      0)
  endfunc

  func oSELECN_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECN<>"|")
    endwith
   endif
  endfunc

  add object oSELECX_1_29 as StdCheck with uid="WUUNPOEZXJ",rtseq=24,rtrep=.f.,left=30, top=364, caption="Contropartite ven./acq.",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 258979034,;
    cFormVar="w_SELECX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECX_1_29.RadioValue()
    return(iif(this.value =1,'CX',;
    ' '))
  endfunc
  func oSELECX_1_29.GetRadio()
    this.Parent.oContained.w_SELECX = this.RadioValue()
    return .t.
  endfunc

  func oSELECX_1_29.SetRadio()
    this.Parent.oContained.w_SELECX=trim(this.Parent.oContained.w_SELECX)
    this.value = ;
      iif(this.Parent.oContained.w_SELECX=='CX',1,;
      0)
  endfunc

  func oSELECX_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECX<>"|")
    endwith
   endif
  endfunc

  add object oSELEDB_1_30 as StdCheck with uid="ECYRZOPIJW",rtseq=25,rtrep=.f.,left=30, top=382, caption="Distinta base",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 90158298,;
    cFormVar="w_SELEDB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDB_1_30.RadioValue()
    return(iif(this.value =1,'DB',;
    ' '))
  endfunc
  func oSELEDB_1_30.GetRadio()
    this.Parent.oContained.w_SELEDB = this.RadioValue()
    return .t.
  endfunc

  func oSELEDB_1_30.SetRadio()
    this.Parent.oContained.w_SELEDB=trim(this.Parent.oContained.w_SELEDB)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDB=='DB',1,;
      0)
  endfunc

  func oSELEDB_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDB<>"|")
    endwith
   endif
  endfunc

  add object oSELEGM_1_31 as StdCheck with uid="JPPHRMEGIF",rtseq=26,rtrep=.f.,left=215, top=112, caption="Gruppi merceologici",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 170898650,;
    cFormVar="w_SELEGM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEGM_1_31.RadioValue()
    return(iif(this.value =1,'GM',;
    ' '))
  endfunc
  func oSELEGM_1_31.GetRadio()
    this.Parent.oContained.w_SELEGM = this.RadioValue()
    return .t.
  endfunc

  func oSELEGM_1_31.SetRadio()
    this.Parent.oContained.w_SELEGM=trim(this.Parent.oContained.w_SELEGM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEGM=='GM',1,;
      0)
  endfunc

  func oSELEGM_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEGM<>"|")
    endwith
   endif
  endfunc

  add object oSELEMA_1_32 as StdCheck with uid="TIDKGZCRKH",rtseq=27,rtrep=.f.,left=215, top=130, caption="Magazzini",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 97498330,;
    cFormVar="w_SELEMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEMA_1_32.RadioValue()
    return(iif(this.value =1,'MA',;
    ' '))
  endfunc
  func oSELEMA_1_32.GetRadio()
    this.Parent.oContained.w_SELEMA = this.RadioValue()
    return .t.
  endfunc

  func oSELEMA_1_32.SetRadio()
    this.Parent.oContained.w_SELEMA=trim(this.Parent.oContained.w_SELEMA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEMA=='MA',1,;
      0)
  endfunc

  func oSELEMA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEMA<>"|")
    endwith
   endif
  endfunc

  add object oSELENO_1_33 as StdCheck with uid="SBMQEQBWRT",rtseq=28,rtrep=.f.,left=215, top=148, caption="Nomenclature",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 130004186,;
    cFormVar="w_SELENO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENO_1_33.RadioValue()
    return(iif(this.value =1,'NO',;
    ' '))
  endfunc
  func oSELENO_1_33.GetRadio()
    this.Parent.oContained.w_SELENO = this.RadioValue()
    return .t.
  endfunc

  func oSELENO_1_33.SetRadio()
    this.Parent.oContained.w_SELENO=trim(this.Parent.oContained.w_SELENO)
    this.value = ;
      iif(this.Parent.oContained.w_SELENO=='NO',1,;
      0)
  endfunc

  func oSELENO_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENO<>"|")
    endwith
   endif
  endfunc

  add object oSELEIN_1_34 as StdCheck with uid="GERRZDMVHA",rtseq=29,rtrep=.f.,left=215, top=166, caption="Inventari",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 152024282,;
    cFormVar="w_SELEIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIN_1_34.RadioValue()
    return(iif(this.value =1,'IN',;
    ' '))
  endfunc
  func oSELEIN_1_34.GetRadio()
    this.Parent.oContained.w_SELEIN = this.RadioValue()
    return .t.
  endfunc

  func oSELEIN_1_34.SetRadio()
    this.Parent.oContained.w_SELEIN=trim(this.Parent.oContained.w_SELEIN)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIN=='IN',1,;
      0)
  endfunc

  func oSELEIN_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIN<>"|")
    endwith
   endif
  endfunc

  add object oSELEIF_1_35 as StdCheck with uid="EJQOFFQFHQ",rtseq=30,rtrep=.f.,left=215, top=184, caption="- valorizzazione FIFO cont.",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 17806554,;
    cFormVar="w_SELEIF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIF_1_35.RadioValue()
    return(iif(this.value =1,'IF',;
    ' '))
  endfunc
  func oSELEIF_1_35.GetRadio()
    this.Parent.oContained.w_SELEIF = this.RadioValue()
    return .t.
  endfunc

  func oSELEIF_1_35.SetRadio()
    this.Parent.oContained.w_SELEIF=trim(this.Parent.oContained.w_SELEIF)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIF=='IF',1,;
      0)
  endfunc

  func oSELEIF_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIF<>"|")
    endwith
   endif
  endfunc

  add object oSELEIC_1_36 as StdCheck with uid="XTHSLYGNTP",rtseq=31,rtrep=.f.,left=215, top=202, caption="- valorizzazione LIFO cont.",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 68138202,;
    cFormVar="w_SELEIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIC_1_36.RadioValue()
    return(iif(this.value =1,'IC',;
    ' '))
  endfunc
  func oSELEIC_1_36.GetRadio()
    this.Parent.oContained.w_SELEIC = this.RadioValue()
    return .t.
  endfunc

  func oSELEIC_1_36.SetRadio()
    this.Parent.oContained.w_SELEIC=trim(this.Parent.oContained.w_SELEIC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIC=='IC',1,;
      0)
  endfunc

  func oSELEIC_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIC<>"|")
    endwith
   endif
  endfunc

  add object oSELEIS_1_37 as StdCheck with uid="DUMCJKGETT",rtseq=32,rtrep=.f.,left=215, top=220, caption="- valorizzazione LIFO scatti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 68138202,;
    cFormVar="w_SELEIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIS_1_37.RadioValue()
    return(iif(this.value =1,'IS',;
    ' '))
  endfunc
  func oSELEIS_1_37.GetRadio()
    this.Parent.oContained.w_SELEIS = this.RadioValue()
    return .t.
  endfunc

  func oSELEIS_1_37.SetRadio()
    this.Parent.oContained.w_SELEIS=trim(this.Parent.oContained.w_SELEIS)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIS=='IS',1,;
      0)
  endfunc

  func oSELEIS_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIS<>"|")
    endwith
   endif
  endfunc

  add object oSELEPR_1_38 as StdCheck with uid="QCIGLGFVZH",rtseq=33,rtrep=.f.,left=215, top=238, caption="Preventivi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 77575386,;
    cFormVar="w_SELEPR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPR_1_38.RadioValue()
    return(iif(this.value =1,'PR',;
    ' '))
  endfunc
  func oSELEPR_1_38.GetRadio()
    this.Parent.oContained.w_SELEPR = this.RadioValue()
    return .t.
  endfunc

  func oSELEPR_1_38.SetRadio()
    this.Parent.oContained.w_SELEPR=trim(this.Parent.oContained.w_SELEPR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPR=='PR',1,;
      0)
  endfunc

  func oSELEPR_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPR<>"|")
    endwith
   endif
  endfunc

  add object oSELESM_1_40 as StdCheck with uid="EYHCNTFPUK",rtseq=34,rtrep=.f.,left=215, top=256, caption="Sconti e maggiorazioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 158315738,;
    cFormVar="w_SELESM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESM_1_40.RadioValue()
    return(iif(this.value =1,'SM',;
    ' '))
  endfunc
  func oSELESM_1_40.GetRadio()
    this.Parent.oContained.w_SELESM = this.RadioValue()
    return .t.
  endfunc

  func oSELESM_1_40.SetRadio()
    this.Parent.oContained.w_SELESM=trim(this.Parent.oContained.w_SELESM)
    this.value = ;
      iif(this.Parent.oContained.w_SELESM=='SM',1,;
      0)
  endfunc

  func oSELESM_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESM<>"|")
    endwith
   endif
  endfunc

  add object oSELERI_1_41 as StdCheck with uid="LKXMIRKYED",rtseq=35,rtrep=.f.,left=215, top=274, caption="Risorse",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 226473178,;
    cFormVar="w_SELERI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERI_1_41.RadioValue()
    return(iif(this.value =1,'RI',;
    ' '))
  endfunc
  func oSELERI_1_41.GetRadio()
    this.Parent.oContained.w_SELERI = this.RadioValue()
    return .t.
  endfunc

  func oSELERI_1_41.SetRadio()
    this.Parent.oContained.w_SELERI=trim(this.Parent.oContained.w_SELERI)
    this.value = ;
      iif(this.Parent.oContained.w_SELERI=='RI',1,;
      0)
  endfunc

  func oSELERI_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERI<>"|")
    endwith
   endif
  endfunc

  add object oSELETC_1_42 as StdCheck with uid="TLQSWROQUX",rtseq=36,rtrep=.f.,left=215, top=292, caption="Tipi confezione",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 56603866,;
    cFormVar="w_SELETC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETC_1_42.RadioValue()
    return(iif(this.value =1,'TC',;
    ' '))
  endfunc
  func oSELETC_1_42.GetRadio()
    this.Parent.oContained.w_SELETC = this.RadioValue()
    return .t.
  endfunc

  func oSELETC_1_42.SetRadio()
    this.Parent.oContained.w_SELETC=trim(this.Parent.oContained.w_SELETC)
    this.value = ;
      iif(this.Parent.oContained.w_SELETC=='TC',1,;
      0)
  endfunc

  func oSELETC_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETC<>"|")
    endwith
   endif
  endfunc

  add object oSELETT_1_43 as StdCheck with uid="DKYBPYLOPQ",rtseq=37,rtrep=.f.,left=215, top=310, caption="Tipi transazioni (INTRA)",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 39826650,;
    cFormVar="w_SELETT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETT_1_43.RadioValue()
    return(iif(this.value =1,'TT',;
    ' '))
  endfunc
  func oSELETT_1_43.GetRadio()
    this.Parent.oContained.w_SELETT = this.RadioValue()
    return .t.
  endfunc

  func oSELETT_1_43.SetRadio()
    this.Parent.oContained.w_SELETT=trim(this.Parent.oContained.w_SELETT)
    this.value = ;
      iif(this.Parent.oContained.w_SELETT=='TT',1,;
      0)
  endfunc

  func oSELETT_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETT<>"|")
    endwith
   endif
  endfunc

  add object oSELETO_1_44 as StdCheck with uid="LZFZJRBCMG",rtseq=38,rtrep=.f.,left=215, top=328, caption="Tipologie colli",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 123712730,;
    cFormVar="w_SELETO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETO_1_44.RadioValue()
    return(iif(this.value =1,'TO',;
    ' '))
  endfunc
  func oSELETO_1_44.GetRadio()
    this.Parent.oContained.w_SELETO = this.RadioValue()
    return .t.
  endfunc

  func oSELETO_1_44.SetRadio()
    this.Parent.oContained.w_SELETO=trim(this.Parent.oContained.w_SELETO)
    this.value = ;
      iif(this.Parent.oContained.w_SELETO=='TO',1,;
      0)
  endfunc

  func oSELETO_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETO<>"|")
    endwith
   endif
  endfunc

  add object oSELETR_1_45 as StdCheck with uid="JKJSVAVACX",rtseq=39,rtrep=.f.,left=215, top=346, caption="Trascodifiche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 73381082,;
    cFormVar="w_SELETR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETR_1_45.RadioValue()
    return(iif(this.value =1,'TR',;
    ' '))
  endfunc
  func oSELETR_1_45.GetRadio()
    this.Parent.oContained.w_SELETR = this.RadioValue()
    return .t.
  endfunc

  func oSELETR_1_45.SetRadio()
    this.Parent.oContained.w_SELETR=trim(this.Parent.oContained.w_SELETR)
    this.value = ;
      iif(this.Parent.oContained.w_SELETR=='TR',1,;
      0)
  endfunc

  func oSELETR_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETR<>"|")
    endwith
   endif
  endfunc

  add object oSELEUM_1_46 as StdCheck with uid="DKPVXJHPTH",rtseq=40,rtrep=.f.,left=215, top=364, caption="Unit� di misura",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 156218586,;
    cFormVar="w_SELEUM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEUM_1_46.RadioValue()
    return(iif(this.value =1,'UM',;
    ' '))
  endfunc
  func oSELEUM_1_46.GetRadio()
    this.Parent.oContained.w_SELEUM = this.RadioValue()
    return .t.
  endfunc

  func oSELEUM_1_46.SetRadio()
    this.Parent.oContained.w_SELEUM=trim(this.Parent.oContained.w_SELEUM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEUM=='UM',1,;
      0)
  endfunc

  func oSELEUM_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEUM<>"|")
    endwith
   endif
  endfunc

  add object oSELEDT_1_48 as StdCheck with uid="HJAEBPEZGA",rtseq=41,rtrep=.f.,left=417, top=112, caption="Documenti testate",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 56603866,;
    cFormVar="w_SELEDT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDT_1_48.RadioValue()
    return(iif(this.value =1,'DT',;
    ' '))
  endfunc
  func oSELEDT_1_48.GetRadio()
    this.Parent.oContained.w_SELEDT = this.RadioValue()
    return .t.
  endfunc

  func oSELEDT_1_48.SetRadio()
    this.Parent.oContained.w_SELEDT=trim(this.Parent.oContained.w_SELEDT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDT=='DT',1,;
      0)
  endfunc

  func oSELEDT_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDT<>"|")
    endwith
   endif
  endfunc

  add object oSELEDR_1_49 as StdCheck with uid="WWFOKPOYVL",rtseq=42,rtrep=.f.,left=417, top=130, caption="Documenti righe",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 90158298,;
    cFormVar="w_SELEDR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDR_1_49.RadioValue()
    return(iif(this.value =1,'DR',;
    ' '))
  endfunc
  func oSELEDR_1_49.GetRadio()
    this.Parent.oContained.w_SELEDR = this.RadioValue()
    return .t.
  endfunc

  func oSELEDR_1_49.SetRadio()
    this.Parent.oContained.w_SELEDR=trim(this.Parent.oContained.w_SELEDR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDR=='DR',1,;
      0)
  endfunc

  func oSELEDR_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDR<>"|")
    endwith
   endif
  endfunc

  add object oSELEDO_1_50 as StdCheck with uid="OUCQVGNSXX",rtseq=43,rtrep=.f.,left=417, top=148, caption="Documenti (testate e righe)",;
    ToolTipText = "Importa i documenti se previsto dai tracciati records e gestisce la transazione a livello di documento",;
    HelpContextID = 140489946,;
    cFormVar="w_SELEDO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDO_1_50.RadioValue()
    return(iif(this.value =1,'DO',;
    ' '))
  endfunc
  func oSELEDO_1_50.GetRadio()
    this.Parent.oContained.w_SELEDO = this.RadioValue()
    return .t.
  endfunc

  func oSELEDO_1_50.SetRadio()
    this.Parent.oContained.w_SELEDO=trim(this.Parent.oContained.w_SELEDO)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDO=='DO',1,;
      0)
  endfunc

  func oSELEDO_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDO<>"|")
    endwith
   endif
  endfunc

  add object oSELEMM_1_51 as StdCheck with uid="XNUVULEUKO",rtseq=44,rtrep=.f.,left=417, top=166, caption="Movimenti magazzino",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 164607194,;
    cFormVar="w_SELEMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEMM_1_51.RadioValue()
    return(iif(this.value =1,'MM',;
    ' '))
  endfunc
  func oSELEMM_1_51.GetRadio()
    this.Parent.oContained.w_SELEMM = this.RadioValue()
    return .t.
  endfunc

  func oSELEMM_1_51.SetRadio()
    this.Parent.oContained.w_SELEMM=trim(this.Parent.oContained.w_SELEMM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEMM=='MM',1,;
      0)
  endfunc

  func oSELEMM_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEMM<>"|")
    endwith
   endif
  endfunc

  add object oSELETA_1_52 as StdCheck with uid="CLMSKLTUUD",rtseq=45,rtrep=.f.,left=417, top=328, caption="Articoli",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 90158298,;
    cFormVar="w_SELETA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETA_1_52.RadioValue()
    return(iif(this.value =1,'TA',;
    ' '))
  endfunc
  func oSELETA_1_52.GetRadio()
    this.Parent.oContained.w_SELETA = this.RadioValue()
    return .t.
  endfunc

  func oSELETA_1_52.SetRadio()
    this.Parent.oContained.w_SELETA=trim(this.Parent.oContained.w_SELETA)
    this.value = ;
      iif(this.Parent.oContained.w_SELETA=='TA',1,;
      0)
  endfunc

  func oSELETA_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETA<>"|")
    endwith
   endif
  endfunc

  add object oSELETM_1_53 as StdCheck with uid="AAXZYGEKZH",rtseq=46,rtrep=.f.,left=417, top=346, caption="Causali magazzino",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 157267162,;
    cFormVar="w_SELETM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETM_1_53.RadioValue()
    return(iif(this.value =1,'TM',;
    ' '))
  endfunc
  func oSELETM_1_53.GetRadio()
    this.Parent.oContained.w_SELETM = this.RadioValue()
    return .t.
  endfunc

  func oSELETM_1_53.SetRadio()
    this.Parent.oContained.w_SELETM=trim(this.Parent.oContained.w_SELETM)
    this.value = ;
      iif(this.Parent.oContained.w_SELETM=='TM',1,;
      0)
  endfunc

  func oSELETM_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETM<>"|")
    endwith
   endif
  endfunc

  add object oSELETP_1_54 as StdCheck with uid="DOLRQAJIKA",rtseq=47,rtrep=.f.,left=417, top=364, caption="Pagamenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 106935514,;
    cFormVar="w_SELETP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETP_1_54.RadioValue()
    return(iif(this.value =1,'TP',;
    ' '))
  endfunc
  func oSELETP_1_54.GetRadio()
    this.Parent.oContained.w_SELETP = this.RadioValue()
    return .t.
  endfunc

  func oSELETP_1_54.SetRadio()
    this.Parent.oContained.w_SELETP=trim(this.Parent.oContained.w_SELETP)
    this.value = ;
      iif(this.Parent.oContained.w_SELETP=='TP',1,;
      0)
  endfunc

  func oSELETP_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETP<>"|")
    endwith
   endif
  endfunc

  add object oRADSELEZ_1_55 as StdRadio with uid="JTFLDCVNJL",rtseq=48,rtrep=.f.,left=365, top=408, width=232,height=23;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELEZ_1_55.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 79546224
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 79546224
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ_1_55.GetRadio()
    this.Parent.oContained.w_RADSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ_1_55.SetRadio()
    this.Parent.oContained.w_RADSELEZ=trim(this.Parent.oContained.w_RADSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ=='D',2,;
      0))
  endfunc

  func oRADSELEZ_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODIMP) and !.w_INCORSO)
    endwith
   endif
  endfunc


  add object oObj_1_56 as cp_runprogram with uid="IXGAHCLBMS",left=11, top=513, width=264,height=19,;
    caption='GSIM_BSM(0)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSM(0)",;
    cEvent = "w_CODIMP Changed,Blank",;
    nPag=1;
    , HelpContextID = 61150413

  add object oSELECONT_1_60 as StdCheck with uid="HJVITWNLMM",rtseq=49,rtrep=.f.,left=26, top=428, caption="Controllo valori",;
    ToolTipText = "Se attivato consente di controllare i valori assegnati ai singoli campi",;
    HelpContextID = 141538438,;
    cFormVar="w_SELECONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECONT_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELECONT_1_60.GetRadio()
    this.Parent.oContained.w_SELECONT = this.RadioValue()
    return .t.
  endfunc

  func oSELECONT_1_60.SetRadio()
    this.Parent.oContained.w_SELECONT=trim(this.Parent.oContained.w_SELECONT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECONT=='S',1,;
      0)
  endfunc

  func oSELECONT_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc


  add object oSELERESO_1_61 as StdCombo with uid="VYMIVBYKOS",rtseq=50,rtrep=.f.,left=26,top=452,width=131,height=21;
    , ToolTipText = "Se attivato genera il resoconto delle operazioni di import";
    , HelpContextID = 25146507;
    , cFormVar="w_SELERESO",RowSource=""+"Errori+segnalazioni,"+"Errori+stop dopo 10,"+"Errori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELERESO_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    'N')))))
  endfunc
  func oSELERESO_1_61.GetRadio()
    this.Parent.oContained.w_SELERESO = this.RadioValue()
    return .t.
  endfunc

  func oSELERESO_1_61.SetRadio()
    this.Parent.oContained.w_SELERESO=trim(this.Parent.oContained.w_SELERESO)
    this.value = ;
      iif(this.Parent.oContained.w_SELERESO=='S',1,;
      iif(this.Parent.oContained.w_SELERESO=='D',2,;
      iif(this.Parent.oContained.w_SELERESO=='E',3,;
      iif(this.Parent.oContained.w_SELERESO=='N',4,;
      0))))
  endfunc

  func oSELERESO_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMPARIVA_1_62 as StdCheck with uid="AWRYUUTVDF",rtseq=51,rtrep=.f.,left=184, top=428, caption="Controllo partita IVA",;
    ToolTipText = "Se attivato consente il riconoscimento dei clienti/fornitori per partita IVA",;
    HelpContextID = 41718471,;
    cFormVar="w_IMPARIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMPARIVA_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMPARIVA_1_62.GetRadio()
    this.Parent.oContained.w_IMPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oIMPARIVA_1_62.SetRadio()
    this.Parent.oContained.w_IMPARIVA=trim(this.Parent.oContained.w_IMPARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IMPARIVA=='S',1,;
      0)
  endfunc

  func oIMPARIVA_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMAZZERA_1_63 as StdCheck with uid="HMAFHBTQIQ",rtseq=52,rtrep=.f.,left=184, top=444, caption="Azzera movimenti",;
    ToolTipText = "Se attivato la procedura elimina le movimentazioni prima di importarle",;
    HelpContextID = 15424825,;
    cFormVar="w_IMAZZERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAZZERA_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAZZERA_1_63.GetRadio()
    this.Parent.oContained.w_IMAZZERA = this.RadioValue()
    return .t.
  endfunc

  func oIMAZZERA_1_63.SetRadio()
    this.Parent.oContained.w_IMAZZERA=trim(this.Parent.oContained.w_IMAZZERA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAZZERA=='S',1,;
      0)
  endfunc

  func oIMAZZERA_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMTRANSA_1_64 as StdCheck with uid="GPDSMVITBT",rtseq=53,rtrep=.f.,left=184, top=460, caption="Sotto transazione",;
    ToolTipText = "Se attivato la procedura viene eseguita sotto un'unica transazione",;
    HelpContextID = 108909255,;
    cFormVar="w_IMTRANSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMTRANSA_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMTRANSA_1_64.GetRadio()
    this.Parent.oContained.w_IMTRANSA = this.RadioValue()
    return .t.
  endfunc

  func oIMTRANSA_1_64.SetRadio()
    this.Parent.oContained.w_IMTRANSA=trim(this.Parent.oContained.w_IMTRANSA)
    this.value = ;
      iif(this.Parent.oContained.w_IMTRANSA=='S',1,;
      0)
  endfunc

  func oIMTRANSA_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO AND .w_SELEDO<>"DO")
    endwith
   endif
  endfunc

  add object oIMCONFER_1_65 as StdCheck with uid="RVMTDVMTAB",rtseq=54,rtrep=.f.,left=184, top=476, caption="Richiesta conferma",;
    ToolTipText = "Se attivato viene richiesto la conferma prima di aggiornare il database (valido solo se attivo sotto transazione)",;
    HelpContextID = 256492248,;
    cFormVar="w_IMCONFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMCONFER_1_65.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMCONFER_1_65.GetRadio()
    this.Parent.oContained.w_IMCONFER = this.RadioValue()
    return .t.
  endfunc

  func oIMCONFER_1_65.SetRadio()
    this.Parent.oContained.w_IMCONFER=trim(this.Parent.oContained.w_IMCONFER)
    this.value = ;
      iif(this.Parent.oContained.w_IMCONFER=='S',1,;
      0)
  endfunc

  func oIMCONFER_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTRANSA='S' and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oIMCONFER_1_65.mHide()
    with this.Parent.oContained
      return (.w_IMTRANSA<>'S')
    endwith
  endfunc


  add object oBtn_1_67 as StdButton with uid="VQERAGEWET",left=518, top=443, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 199839514;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      with this.Parent.oContained
        do GSIM_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO and (not empty(.w_CODIMP)) and (.w_TIPSRC<>'O' or !empty(.w_ODBCDSN))                                                       and g_IMPOMAGA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_68 as StdButton with uid="MRPTSAHHIT",left=575, top=443, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 192550842;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_68.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oBtn_1_72 as StdButton with uid="IXBUJQRYLM",left=369, top=443, width=48,height=45,;
    CpPicture="BMP\info.bmp", caption="", nPag=1;
    , HelpContextID = 76321706;
    , caption='\<Info';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_72.Click()
      do gsim_kai with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_72.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oObj_1_75 as cp_runprogram with uid="TMLZAGARPU",left=305, top=513, width=346,height=19,;
    caption='GSIM_BSM(1)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSM(1)",;
    cEvent = "w_RADSELEZ Changed, w_CODIMP Changed",;
    nPag=1;
    , HelpContextID = 61150157

  add object oSELECF_1_83 as StdCheck with uid="PDNDFOGEJJ",rtseq=60,rtrep=.f.,left=215, top=382, caption="Clienti e fornitori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 24098010,;
    cFormVar="w_SELECF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECF_1_83.RadioValue()
    return(iif(this.value =1,'CF',;
    ' '))
  endfunc
  func oSELECF_1_83.GetRadio()
    this.Parent.oContained.w_SELECF = this.RadioValue()
    return .t.
  endfunc

  func oSELECF_1_83.SetRadio()
    this.Parent.oContained.w_SELECF=trim(this.Parent.oContained.w_SELECF)
    this.value = ;
      iif(this.Parent.oContained.w_SELECF=='CF',1,;
      0)
  endfunc

  func oSELECF_1_83.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECF<>"|")
    endwith
   endif
  endfunc

  add object oStr_1_7 as StdString with uid="TULOXBETIP",Visible=.t., Left=3, Top=67,;
    Alignment=1, Width=100, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="HNFSNYQVVY",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tracciati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="TXLTEBBQSG",Visible=.t., Left=22, Top=408,;
    Alignment=0, Width=141, Height=15,;
    Caption="Verifiche e resoconti"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QUCHHTNQRF",Visible=.t., Left=29, Top=93,;
    Alignment=0, Width=214, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="SQXRTSXXPT",Visible=.t., Left=182, Top=408,;
    Alignment=0, Width=116, Height=15,;
    Caption="Opzioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="FTEJVJHPFP",Visible=.t., Left=3, Top=44,;
    Alignment=1, Width=100, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="UXUMQQXWYB",Visible=.t., Left=3, Top=44,;
    Alignment=1, Width=100, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="QQMMTLMXER",Visible=.t., Left=336, Top=44,;
    Alignment=1, Width=53, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="KUWPABNYKM",Visible=.t., Left=323, Top=68,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_74 as StdString with uid="BRDYMEXVNR",Visible=.t., Left=417, Top=93,;
    Alignment=0, Width=200, Height=15,;
    Caption="Selezione movimenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="GQVGEZQABP",Visible=.t., Left=417, Top=310,;
    Alignment=0, Width=152, Height=18,;
    Caption="Traduzioni"  ;
  , bGlobalFont=.t.

  add object oBox_1_47 as StdBox with uid="LZRSZSRDKV",left=19, top=425, width=146,height=1

  add object oBox_1_57 as StdBox with uid="VNBLQXCGAX",left=18, top=107, width=394,height=297

  add object oBox_1_59 as StdBox with uid="UECDJXNURS",left=178, top=425, width=127,height=1

  add object oBox_1_73 as StdBox with uid="CMOBWQCVBO",left=409, top=107, width=216,height=297

  add object oBox_1_81 as StdBox with uid="BXVASFZUMR",left=413, top=325, width=211,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kma','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
