* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bas                                                        *
*              Import archivi spedizione                                       *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_183]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2014-10-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsim_bas
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tamGSIM_BAS",oParentObject)
return(i_retval)

define class tamGSIM_BAS as tGSIM_BAS

  proc Done()
    if !empty(i_Error)
      ah_ErrorMsg(i_Error,,'')
    endif
    if !isnull(this.oParentObject) and this.bUpdateParentObject
      this.oParentObject.mCalc(.t.)
    endif
  return

enddefine

Proc tamGSIM_BAS
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bas",oParentObject)
return(i_retval)

define class tgsim_bas as StdBatch
  * --- Local variables
  * --- WorkFile variables
  PORTI_idx=0
  VETTORI_idx=0
  MODASPED_idx=0
  ASPETTO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT ARCHIVI SPEDIZIONE
    * --- Modello di segnalazione
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Aggiornamento righe IVA S/N
    * --- Aggiornamento partite S/N
    * --- Aggiornamento analitica S/N
    * --- Aggiornamento saldi S/N
    * --- Ricalcolo numeri registrazioni
    * --- Conto di costo per IVA indetraibile
    * --- File ascii da importare
    * --- Variabili per gestione rottura registrazioni
    * --- Chiave corrente movimentazione
    * --- Ultima chiave di rottura movimentazione
    * --- Ultimo serial
    * --- Ultimo numero registrazione
    * --- Ultimo numero protocollo
    * --- Ultimo tipo cliente/fornitore
    * --- Ultimo Articolo
    * --- Ultimo numero di riga
    * --- Ultimo Codice < 50
    * --- Aggiornamento archivi
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_025482E8
        bErr_025482E8=bTrsErr
        this.Try_025482E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_025482E8
        * --- End
        * --- Aggiornamento records
        i_rows = 0
        * --- Try
        local bErr_02543D30
        bErr_02543D30=bTrsErr
        this.Try_02543D30()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_02543D30
        * --- End
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
        if this.oParentObject.w_Corretto .and. this.oParentObject.w_ResoDett <> AH_MSGFORMAT( "<SENZA RESOCONTO>" )
          * --- Scrittura resoconto
          this.oParentObject.w_ResoMode = "SCRITTURA"
          * --- Aggiornamento archivio resoconti
          this.oParentObject.Pag4()
        endif
      case .T.
        * --- Scrittura resoconto per archivi non supportati
        this.oParentObject.w_ResoMode = "NONSUPP"
        * --- Aggiornamento archivio resoconti
        this.oParentObject.Pag4()
    endcase
  endproc
  proc Try_025482E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "PO"
        * --- Porti
        w_POCODPOR = left( ltrim(w_POCODPOR)+space(1), 1)
        * --- Insert into PORTI
        i_nConn=i_TableProp[this.PORTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PORTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PORTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"POCODPOR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_POCODPOR),'PORTI','POCODPOR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'POCODPOR',w_POCODPOR)
          insert into (i_cTable) (POCODPOR &i_ccchkf. );
             values (;
               w_POCODPOR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SP"
        * --- Spedizioni
        w_SPCODSPE = left( ltrim(w_SPCODSPE)+space(3), 3)
        * --- Insert into MODASPED
        i_nConn=i_TableProp[this.MODASPED_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODASPED_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MODASPED_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SPCODSPE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_SPCODSPE),'MODASPED','SPCODSPE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SPCODSPE',w_SPCODSPE)
          insert into (i_cTable) (SPCODSPE &i_ccchkf. );
             values (;
               w_SPCODSPE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "VE"
        * --- Vettori
        w_VTCODVET = left( ltrim(w_VTCODVET)+space(5), 5)
        * --- Insert into VETTORI
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VETTORI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"VTCODVET"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_VTCODVET),'VETTORI','VTCODVET');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'VTCODVET',w_VTCODVET)
          insert into (i_cTable) (VTCODVET &i_ccchkf. );
             values (;
               w_VTCODVET;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AS"
        * --- Aspetto Esteriore dei Beni
        w_ASCODASP = left( ltrim(w_ASCODASP)+space(3), 3)
        * --- Insert into ASPETTO
        i_nConn=i_TableProp[this.ASPETTO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASPETTO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASPETTO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ASCODASP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ASCODASP),'ASPETTO','ASCODASP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ASCODASP',w_ASCODASP)
          insert into (i_cTable) (ASCODASP &i_ccchkf. );
             values (;
               w_ASCODASP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
    endcase
    return
  proc Try_02543D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "PO"
        * --- Porti
        this.oParentObject.w_ResoDett = trim( w_POCODPOR )
        * --- Write into PORTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PORTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PORTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PORTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PODESPOR ="+cp_NullLink(cp_ToStrODBC(w_PODESPOR),'PORTI','PODESPOR');
              +i_ccchkf ;
          +" where ";
              +"POCODPOR = "+cp_ToStrODBC(w_POCODPOR);
                 )
        else
          update (i_cTable) set;
              PODESPOR = w_PODESPOR;
              &i_ccchkf. ;
           where;
              POCODPOR = w_POCODPOR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SP"
        * --- Spedizioni
        this.oParentObject.w_ResoDett = trim( w_SPCODSPE )
        * --- Write into MODASPED
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MODASPED_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODASPED_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MODASPED_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SPDESSPE ="+cp_NullLink(cp_ToStrODBC(w_SPDESSPE),'MODASPED','SPDESSPE');
          +",SPMODSPE ="+cp_NullLink(cp_ToStrODBC(w_SPMODSPE),'MODASPED','SPMODSPE');
              +i_ccchkf ;
          +" where ";
              +"SPCODSPE = "+cp_ToStrODBC(w_SPCODSPE);
                 )
        else
          update (i_cTable) set;
              SPDESSPE = w_SPDESSPE;
              ,SPMODSPE = w_SPMODSPE;
              &i_ccchkf. ;
           where;
              SPCODSPE = w_SPCODSPE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "VE"
        * --- Vettori
        this.oParentObject.w_ResoDett = trim( w_VTCODVET )
        * --- Write into VETTORI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.VETTORI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"VTDESVET ="+cp_NullLink(cp_ToStrODBC(w_VTDESVET),'VETTORI','VTDESVET');
          +",VTINDVET ="+cp_NullLink(cp_ToStrODBC(w_VTINDVET),'VETTORI','VTINDVET');
          +",VTVETCAP ="+cp_NullLink(cp_ToStrODBC(w_VTVETCAP),'VETTORI','VTVETCAP');
          +",VTLOCVET ="+cp_NullLink(cp_ToStrODBC(w_VTLOCVET),'VETTORI','VTLOCVET');
          +",VTPROVET ="+cp_NullLink(cp_ToStrODBC(w_VTPROVET),'VETTORI','VTPROVET');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'VETTORI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'VETTORI','UTDV');
          +",VTDTINVA ="+cp_NullLink(cp_ToStrODBC(w_VTDTINVA),'VETTORI','VTDTINVA');
          +",VTDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_VTDTOBSO),'VETTORI','VTDTOBSO');
          +",VTTIPCON ="+cp_NullLink(cp_ToStrODBC(w_VTTIPCON),'VETTORI','VTTIPCON');
          +",VTCODCON ="+cp_NullLink(cp_ToStrODBC(w_VTCODCON),'VETTORI','VTCODCON');
          +",VTNUMISC ="+cp_NullLink(cp_ToStrODBC(w_VTNUMISC),'VETTORI','VTNUMISC');
              +i_ccchkf ;
          +" where ";
              +"VTCODVET = "+cp_ToStrODBC(w_VTCODVET);
                 )
        else
          update (i_cTable) set;
              VTDESVET = w_VTDESVET;
              ,VTINDVET = w_VTINDVET;
              ,VTVETCAP = w_VTVETCAP;
              ,VTLOCVET = w_VTLOCVET;
              ,VTPROVET = w_VTPROVET;
              ,UTCV = this.oParentObject.w_CreVarUte;
              ,UTDV = this.oParentObject.w_CreVarDat;
              ,VTDTINVA = w_VTDTINVA;
              ,VTDTOBSO = w_VTDTOBSO;
              ,VTTIPCON = w_VTTIPCON;
              ,VTCODCON = w_VTCODCON;
              ,VTNUMISC = w_VTNUMISC;
              &i_ccchkf. ;
           where;
              VTCODVET = w_VTCODVET;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AS"
        * --- Aspetto Esteriore dei Beni
        this.oParentObject.w_ResoDett = trim( w_ASCODASP )
        * --- Write into ASPETTO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ASPETTO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASPETTO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ASPETTO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ASDESASP ="+cp_NullLink(cp_ToStrODBC(w_ASDESASP),'ASPETTO','ASDESASP');
              +i_ccchkf ;
          +" where ";
              +"ASCODASP = "+cp_ToStrODBC(w_ASCODASP);
                 )
        else
          update (i_cTable) set;
              ASDESASP = w_ASDESASP;
              &i_ccchkf. ;
           where;
              ASCODASP = w_ASCODASP;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PORTI'
    this.cWorkTables[2]='VETTORI'
    this.cWorkTables[3]='MODASPED'
    this.cWorkTables[4]='ASPETTO'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
