* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_aar                                                        *
*              Archivi di destinazione                                         *
*                                                                              *
*      Author: TAM SOFTWARE & CODE LAB                                         *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_6]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-30                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_aar"))

* --- Class definition
define class tgsim_aar as StdForm
  Top    = 86
  Left   = 136

  * --- Standard Properties
  Width  = 357
  Height = 158+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=143245161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  IMPOARCH_IDX = 0
  cFile = "IMPOARCH"
  cKeySelect = "ARCODICE"
  cKeyWhere  = "ARCODICE=this.w_ARCODICE"
  cKeyWhereODBC = '"ARCODICE="+cp_ToStrODBC(this.w_ARCODICE)';

  cKeyWhereODBCqualified = '"IMPOARCH.ARCODICE="+cp_ToStrODBC(this.w_ARCODICE)';

  cPrg = "gsim_aar"
  cComment = "Archivi di destinazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ARCODICE = space(2)
  w_ARDESCRI = space(20)
  w_ARTABELL = space(15)
  w_ARTABDET = space(15)
  w_ARROUTIN = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IMPOARCH','gsim_aar')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_aarPag1","gsim_aar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivio di destinazione")
      .Pages(1).HelpContextID = 169057570
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='IMPOARCH'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IMPOARCH_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IMPOARCH_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ARCODICE = NVL(ARCODICE,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from IMPOARCH where ARCODICE=KeySet.ARCODICE
    *
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IMPOARCH')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IMPOARCH.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IMPOARCH '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ARCODICE = NVL(ARCODICE,space(2))
        .w_ARDESCRI = NVL(ARDESCRI,space(20))
        .w_ARTABELL = NVL(ARTABELL,space(15))
        .w_ARTABDET = NVL(ARTABDET,space(15))
        .w_ARROUTIN = NVL(ARROUTIN,space(30))
        cp_LoadRecExtFlds(this,'IMPOARCH')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ARCODICE = space(2)
      .w_ARDESCRI = space(20)
      .w_ARTABELL = space(15)
      .w_ARTABDET = space(15)
      .w_ARROUTIN = space(30)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'IMPOARCH')
    this.DoRTCalc(1,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oARCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oARDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oARTABELL_1_6.enabled = i_bVal
      .Page1.oPag.oARTABDET_1_7.enabled = i_bVal
      .Page1.oPag.oARROUTIN_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oARCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oARCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'IMPOARCH',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODICE,"ARCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESCRI,"ARDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTABELL,"ARTABELL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTABDET,"ARTABDET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARROUTIN,"ARROUTIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    i_lTable = "IMPOARCH"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IMPOARCH_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.IMPOARCH_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IMPOARCH
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IMPOARCH')
        i_extval=cp_InsertValODBCExtFlds(this,'IMPOARCH')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ARCODICE,ARDESCRI,ARTABELL,ARTABDET,ARROUTIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ARCODICE)+;
                  ","+cp_ToStrODBC(this.w_ARDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ARTABELL)+;
                  ","+cp_ToStrODBC(this.w_ARTABDET)+;
                  ","+cp_ToStrODBC(this.w_ARROUTIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IMPOARCH')
        i_extval=cp_InsertValVFPExtFlds(this,'IMPOARCH')
        cp_CheckDeletedKey(i_cTable,0,'ARCODICE',this.w_ARCODICE)
        INSERT INTO (i_cTable);
              (ARCODICE,ARDESCRI,ARTABELL,ARTABDET,ARROUTIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ARCODICE;
                  ,this.w_ARDESCRI;
                  ,this.w_ARTABELL;
                  ,this.w_ARTABDET;
                  ,this.w_ARROUTIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.IMPOARCH_IDX,i_nConn)
      *
      * update IMPOARCH
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'IMPOARCH')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ARDESCRI="+cp_ToStrODBC(this.w_ARDESCRI)+;
             ",ARTABELL="+cp_ToStrODBC(this.w_ARTABELL)+;
             ",ARTABDET="+cp_ToStrODBC(this.w_ARTABDET)+;
             ",ARROUTIN="+cp_ToStrODBC(this.w_ARROUTIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'IMPOARCH')
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
        UPDATE (i_cTable) SET;
              ARDESCRI=this.w_ARDESCRI;
             ,ARTABELL=this.w_ARTABELL;
             ,ARTABDET=this.w_ARTABDET;
             ,ARROUTIN=this.w_ARROUTIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.IMPOARCH_IDX,i_nConn)
      *
      * delete IMPOARCH
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODICE',this.w_ARCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODICE_1_1.value==this.w_ARCODICE)
      this.oPgFrm.Page1.oPag.oARCODICE_1_1.value=this.w_ARCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value==this.w_ARDESCRI)
      this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value=this.w_ARDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABELL_1_6.value==this.w_ARTABELL)
      this.oPgFrm.Page1.oPag.oARTABELL_1_6.value=this.w_ARTABELL
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABDET_1_7.value==this.w_ARTABDET)
      this.oPgFrm.Page1.oPag.oARTABDET_1_7.value=this.w_ARTABDET
    endif
    if not(this.oPgFrm.Page1.oPag.oARROUTIN_1_8.value==this.w_ARROUTIN)
      this.oPgFrm.Page1.oPag.oARROUTIN_1_8.value=this.w_ARROUTIN
    endif
    cp_SetControlsValueExtFlds(this,'IMPOARCH')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ARTABELL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTABELL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ARTABELL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsim_aarPag1 as StdContainer
  Width  = 353
  height = 158
  stdWidth  = 353
  stdheight = 158
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODICE_1_1 as StdField with uid="XIHOMSYHLS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ARCODICE", cQueryName = "ARCODICE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 84526923,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=109, Top=20, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oARDESCRI_1_3 as StdField with uid="MLUGQGIZJL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ARDESCRI", cQueryName = "ARDESCRI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 1058993,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=109, Top=49, InputMask=replicate('X',20)

  add object oARTABELL_1_6 as StdField with uid="AFZWDVQAHV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ARTABELL", cQueryName = "ARTABELL",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tabella principale",;
    HelpContextID = 253962414,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=109, Top=78, InputMask=replicate('X',15)

  add object oARTABDET_1_7 as StdField with uid="ICMBFNSGGQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ARTABDET", cQueryName = "ARTABDET",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tabella dettaglio",;
    HelpContextID = 266131290,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=109, Top=101, InputMask=replicate('X',15)

  add object oARROUTIN_1_8 as StdField with uid="HFFCDQWUWW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARROUTIN", cQueryName = "ARROUTIN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Routine per l'importazione dati negli archivi. Se non specificato utilizza routine standard.",;
    HelpContextID = 18528084,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=109, Top=131, cSayPict='repl("!",30)', cGetPict='repl("!",30)', InputMask=replicate('X',30)

  add object oStr_1_2 as StdString with uid="MZYPXSAWEL",Visible=.t., Left=2, Top=20,;
    Alignment=1, Width=106, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="RGIUAOMDEI",Visible=.t., Left=2, Top=50,;
    Alignment=1, Width=106, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EHONISXRSY",Visible=.t., Left=2, Top=80,;
    Alignment=1, Width=106, Height=15,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="MONEYYTSRR",Visible=.t., Left=2, Top=134,;
    Alignment=1, Width=106, Height=18,;
    Caption="Routine:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_aar','IMPOARCH','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ARCODICE=IMPOARCH.ARCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
