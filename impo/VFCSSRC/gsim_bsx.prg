* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bsx                                                        *
*              Sel/des archivi import/export                                   *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_5]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bsx",oParentObject)
return(i_retval)

define class tgsim_bsx as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione e Deselezione Archivi Import/Export
    * --- Archivi
    this.oParentObject.w_SELETR = iif( this.oParentObject.w_RADSELEZ="D" , "", "TR" )
    this.oParentObject.w_SELEAD = iif( this.oParentObject.w_RADSELEZ="D" , "", "AD" )
    this.oParentObject.w_SELEVP = iif( this.oParentObject.w_RADSELEZ="D" , "", "VP" )
    this.oParentObject.w_SELETS = iif( this.oParentObject.w_RADSELEZ="D" , "", "TS" )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
