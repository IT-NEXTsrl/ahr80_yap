* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mim                                                        *
*              Tracciati importazione                                          *
*                                                                              *
*      Author: TAM SOFTWARE & CODE LAB                                         *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_88]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2013-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_mim"))

* --- Class definition
define class tgsim_mim as StdTrsForm
  Top    = 3
  Left   = 3

  * --- Standard Properties
  Width  = 788
  Height = 445+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-15"
  HelpContextID=3555479
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  IMPORTAZ_IDX = 0
  tracciat_IDX = 0
  IMPOARCH_IDX = 0
  TRACCIAT_IDX = 0
  SRC_ODBC_IDX = 0
  SRCMODBC_IDX = 0
  cFile = "IMPORTAZ"
  cKeySelect = "IMCODICE,IMTIPSRC"
  cKeyWhere  = "IMCODICE=this.w_IMCODICE and IMTIPSRC=this.w_IMTIPSRC"
  cKeyDetail  = "IMCODICE=this.w_IMCODICE and IMTIPSRC=this.w_IMTIPSRC and IM_ASCII=this.w_IM_ASCII and IMDESTIN=this.w_IMDESTIN"
  cKeyWhereODBC = '"IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';
      +'+" and IMTIPSRC="+cp_ToStrODBC(this.w_IMTIPSRC)';

  cKeyDetailWhereODBC = '"IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';
      +'+" and IMTIPSRC="+cp_ToStrODBC(this.w_IMTIPSRC)';
      +'+" and IM_ASCII="+cp_ToStrODBC(this.w_IM_ASCII)';
      +'+" and IMDESTIN="+cp_ToStrODBC(this.w_IMDESTIN)';

  cKeyWhereODBCqualified = '"IMPORTAZ.IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';
      +'+" and IMPORTAZ.IMTIPSRC="+cp_ToStrODBC(this.w_IMTIPSRC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsim_mim"
  cComment = "Tracciati importazione"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IMCODICE = space(20)
  w_IMANNOTA = space(0)
  w_IMTIPSRC = space(1)
  o_IMTIPSRC = space(1)
  w_IMSEQUEN = 0
  w_IM_ASCII = space(200)
  w_IM_ODBC = space(20)
  o_IM_ODBC = space(20)
  w_IM_EXCEL = space(150)
  o_IM_EXCEL = space(150)
  w_IM_ASCII = space(200)
  w_IMDESTIN = space(2)
  w_DESARC = space(20)
  w_IMTRASCO = space(1)
  w_IMTRAMUL = space(1)
  w_IMFILTRO = space(70)
  w_IMCHIMOV = space(70)
  w_IMAGGSAL = space(1)
  w_IMAGGIVA = space(1)
  w_IMAGGPAR = space(1)
  w_IMAGGANA = space(1)
  w_IMCONIND = space(15)
  w_IMAGGNUR = space(1)
  w_IMPARIVA = space(1)
  w_IMAZZERA = space(1)
  w_IMCONTRO = space(1)
  w_IMRESOCO = space(1)
  w_IMTIPDBF = space(1)
  w_IMROUTIN = space(30)
  w_FLGFILE = space(10)

  * --- Children pointers
  gsim_mtr = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IMPORTAZ','gsim_mim')
    stdPageFrame::Init()
    *set procedure to gsim_mtr additive
    with this
      .Pages(1).addobject("oPag","tgsim_mimPag1","gsim_mim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tracciati")
      .Pages(1).HelpContextID = 3158214
      .Pages(2).addobject("oPag","tgsim_mimPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Impostazioni")
      .Pages(2).HelpContextID = 62060400
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsim_mtr
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='tracciat'
    this.cWorkTables[2]='IMPOARCH'
    this.cWorkTables[3]='TRACCIAT'
    this.cWorkTables[4]='SRC_ODBC'
    this.cWorkTables[5]='SRCMODBC'
    this.cWorkTables[6]='IMPORTAZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IMPORTAZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IMPORTAZ_IDX,3]
  return

  function CreateChildren()
    this.gsim_mtr = CREATEOBJECT('stdLazyChild',this,'gsim_mtr')
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsim_mtr)
      this.gsim_mtr.DestroyChildrenChain()
      this.gsim_mtr=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsim_mtr.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsim_mtr.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsim_mtr.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .gsim_mtr.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_IMCODICE,"TRCODICE";
             ,.w_IMTIPSRC,"TRTIPSRC";
             ,.w_IM_ASCII,"TR_ASCII";
             ,.w_IMDESTIN,"TRDESTIN";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_IMCODICE = NVL(IMCODICE,space(20))
      .w_IMTIPSRC = NVL(IMTIPSRC,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from IMPORTAZ where IMCODICE=KeySet.IMCODICE
    *                            and IMTIPSRC=KeySet.IMTIPSRC
    *                            and IM_ASCII=KeySet.IM_ASCII
    *                            and IMDESTIN=KeySet.IMDESTIN
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsim_mim
      * --- Setta Ordine per Sequenza
      i_cOrder = 'order by IMSEQUEN'
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2],this.bLoadRecFilter,this.IMPORTAZ_IDX,"gsim_mim")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IMPORTAZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IMPORTAZ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' IMPORTAZ '
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IMCODICE',this.w_IMCODICE  ,'IMTIPSRC',this.w_IMTIPSRC  )
      select * from (i_cTable) IMPORTAZ where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_IMCODICE = NVL(IMCODICE,space(20))
        .w_IMANNOTA = NVL(IMANNOTA,space(0))
        .w_IMTIPSRC = NVL(IMTIPSRC,space(1))
        .w_IMPARIVA = NVL(IMPARIVA,space(1))
        .w_IMAZZERA = NVL(IMAZZERA,space(1))
        .w_IMCONTRO = NVL(IMCONTRO,space(1))
        .w_IMRESOCO = NVL(IMRESOCO,space(1))
        .w_IMTIPDBF = NVL(IMTIPDBF,space(1))
        .w_IMROUTIN = NVL(IMROUTIN,space(30))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ICASE(.w_IMTIPSRC='A',Ah_MsgFormat("Nome file ASCII"),.w_IMTIPSRC='O',Ah_MsgFormat("Sorgente dati ODBC"),Ah_MsgFormat("Sorgente dati EXCEL")))
        cp_LoadRecExtFlds(this,'IMPORTAZ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESARC = space(20)
        .w_FLGFILE = 'S'
          .w_IMSEQUEN = NVL(IMSEQUEN,0)
          .w_IM_ASCII = NVL(IM_ASCII,space(200))
          .w_IM_ODBC = NVL(IM_ODBC,space(20))
          * evitabile
          *.link_2_3('Load')
          .w_IM_EXCEL = NVL(IM_EXCEL,space(150))
          * evitabile
          *.link_2_4('Load')
        .w_IM_ASCII = ICASE(.w_IMTIPSRC='O',.w_IM_ODBC,.w_IMTIPSRC='A',.w_IM_ASCII,.w_IM_EXCEL)
          .w_IMDESTIN = NVL(IMDESTIN,space(2))
          if link_2_6_joined
            this.w_IMDESTIN = NVL(ARCODICE206,NVL(this.w_IMDESTIN,space(2)))
            this.w_DESARC = NVL(ARDESCRI206,space(20))
          else
          .link_2_6('Load')
          endif
          .w_IMTRASCO = NVL(IMTRASCO,space(1))
          .w_IMTRAMUL = NVL(IMTRAMUL,space(1))
          .w_IMFILTRO = NVL(IMFILTRO,space(70))
          .w_IMCHIMOV = NVL(IMCHIMOV,space(70))
          .w_IMAGGSAL = NVL(IMAGGSAL,space(1))
          .w_IMAGGIVA = NVL(IMAGGIVA,space(1))
          .w_IMAGGPAR = NVL(IMAGGPAR,space(1))
          .w_IMAGGANA = NVL(IMAGGANA,space(1))
          .w_IMCONIND = NVL(IMCONIND,space(15))
          .w_IMAGGNUR = NVL(IMAGGNUR,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace IM_ASCII with .w_IM_ASCII
          replace IMDESTIN with .w_IMDESTIN
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ICASE(.w_IMTIPSRC='A',Ah_MsgFormat("Nome file ASCII"),.w_IMTIPSRC='O',Ah_MsgFormat("Sorgente dati ODBC"),Ah_MsgFormat("Sorgente dati EXCEL")))
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_19.enabled = .oPgFrm.Page1.oPag.oBtn_2_19.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_IMCODICE=space(20)
      .w_IMANNOTA=space(0)
      .w_IMTIPSRC=space(1)
      .w_IMSEQUEN=0
      .w_IM_ASCII=space(200)
      .w_IM_ODBC=space(20)
      .w_IM_EXCEL=space(150)
      .w_IM_ASCII=space(200)
      .w_IMDESTIN=space(2)
      .w_DESARC=space(20)
      .w_IMTRASCO=space(1)
      .w_IMTRAMUL=space(1)
      .w_IMFILTRO=space(70)
      .w_IMCHIMOV=space(70)
      .w_IMAGGSAL=space(1)
      .w_IMAGGIVA=space(1)
      .w_IMAGGPAR=space(1)
      .w_IMAGGANA=space(1)
      .w_IMCONIND=space(15)
      .w_IMAGGNUR=space(1)
      .w_IMPARIVA=space(1)
      .w_IMAZZERA=space(1)
      .w_IMCONTRO=space(1)
      .w_IMRESOCO=space(1)
      .w_IMTIPDBF=space(1)
      .w_IMROUTIN=space(30)
      .w_FLGFILE=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_IMTIPSRC = 'A'
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_IM_ODBC))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_IM_EXCEL))
         .link_2_4('Full')
        endif
        .w_IM_ASCII = ICASE(.w_IMTIPSRC='O',.w_IM_ODBC,.w_IMTIPSRC='A',.w_IM_ASCII,.w_IM_EXCEL)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_IMDESTIN))
         .link_2_6('Full')
        endif
        .DoRTCalc(10,10,.f.)
        .w_IMTRASCO = 'N'
        .w_IMTRAMUL = 'N'
        .DoRTCalc(13,14,.f.)
        .w_IMAGGSAL = 'N'
        .w_IMAGGIVA = 'S'
        .w_IMAGGPAR = 'S'
        .w_IMAGGANA = 'N'
        .DoRTCalc(19,19,.f.)
        .w_IMAGGNUR = 'S'
        .w_IMPARIVA = 'S'
        .w_IMAZZERA = 'S'
        .w_IMCONTRO = 'N'
        .w_IMRESOCO = 'S'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ICASE(.w_IMTIPSRC='A',Ah_MsgFormat("Nome file ASCII"),.w_IMTIPSRC='O',Ah_MsgFormat("Sorgente dati ODBC"),Ah_MsgFormat("Sorgente dati EXCEL")))
        .DoRTCalc(25,26,.f.)
        .w_FLGFILE = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'IMPORTAZ')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled = this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIMCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oIMANNOTA_1_3.enabled = i_bVal
      .Page1.oPag.oIMTIPSRC_1_4.enabled_(i_bVal)
      .Page2.oPag.oIMPARIVA_4_1.enabled = i_bVal
      .Page2.oPag.oIMAZZERA_4_2.enabled = i_bVal
      .Page2.oPag.oIMCONTRO_4_5.enabled = i_bVal
      .Page2.oPag.oIMRESOCO_4_6.enabled = i_bVal
      .Page1.oPag.oIMTIPDBF_1_6.enabled = i_bVal
      .Page1.oPag.oBtn_2_19.enabled = .Page1.oPag.oBtn_2_19.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIMCODICE_1_1.enabled = .f.
        .Page1.oPag.oIMTIPSRC_1_4.enabled_(.f.)
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIMCODICE_1_1.enabled = .t.
        .Page1.oPag.oIMTIPSRC_1_4.enabled_(.t.)
      endif
    endwith
    this.gsim_mtr.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'IMPORTAZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsim_mtr.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMCODICE,"IMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMANNOTA,"IMANNOTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMTIPSRC,"IMTIPSRC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMPARIVA,"IMPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMAZZERA,"IMAZZERA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMCONTRO,"IMCONTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMRESOCO,"IMRESOCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMTIPDBF,"IMTIPDBF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMROUTIN,"IMROUTIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    i_lTable = "IMPORTAZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IMPORTAZ_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_IMSEQUEN N(5);
      ,t_IM_ASCII C(200);
      ,t_IM_ODBC C(20);
      ,t_IM_EXCEL C(150);
      ,t_IMDESTIN C(2);
      ,t_DESARC C(20);
      ,t_IMTRASCO N(3);
      ,t_IMTRAMUL N(3);
      ,IM_ASCII C(200);
      ,IMDESTIN C(2);
      ,t_IMFILTRO C(70);
      ,t_IMCHIMOV C(70);
      ,t_IMAGGSAL C(1);
      ,t_IMAGGIVA C(1);
      ,t_IMAGGPAR C(1);
      ,t_IMAGGANA C(1);
      ,t_IMCONIND C(15);
      ,t_IMAGGNUR C(1);
      ,t_FLGFILE C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_mimbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1.controlsource=this.cTrsName+'.t_IMSEQUEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.controlsource=this.cTrsName+'.t_IM_ASCII'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.controlsource=this.cTrsName+'.t_IM_ODBC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.controlsource=this.cTrsName+'.t_IM_EXCEL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESTIN_2_6.controlsource=this.cTrsName+'.t_IMDESTIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESARC_2_7.controlsource=this.cTrsName+'.t_DESARC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.controlsource=this.cTrsName+'.t_IMTRASCO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.controlsource=this.cTrsName+'.t_IMTRAMUL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(396)
    this.AddVLine(441)
    this.AddVLine(576)
    this.AddVLine(704)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
      *
      * insert into IMPORTAZ
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IMPORTAZ')
        i_extval=cp_InsertValODBCExtFlds(this,'IMPORTAZ')
        i_cFldBody=" "+;
                  "(IMCODICE,IMANNOTA,IMTIPSRC,IMSEQUEN,IM_ASCII"+;
                  ",IM_ODBC,IM_EXCEL,IMDESTIN,IMTRASCO,IMTRAMUL"+;
                  ",IMFILTRO,IMCHIMOV,IMAGGSAL,IMAGGIVA,IMAGGPAR"+;
                  ",IMAGGANA,IMCONIND,IMAGGNUR,IMPARIVA,IMAZZERA"+;
                  ",IMCONTRO,IMRESOCO,IMTIPDBF,IMROUTIN,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IMCODICE)+","+cp_ToStrODBC(this.w_IMANNOTA)+","+cp_ToStrODBC(this.w_IMTIPSRC)+","+cp_ToStrODBC(this.w_IMSEQUEN)+","+cp_ToStrODBC(this.w_IM_ASCII)+;
             ","+cp_ToStrODBCNull(this.w_IM_ODBC)+","+cp_ToStrODBCNull(this.w_IM_EXCEL)+","+cp_ToStrODBCNull(this.w_IMDESTIN)+","+cp_ToStrODBC(this.w_IMTRASCO)+","+cp_ToStrODBC(this.w_IMTRAMUL)+;
             ","+cp_ToStrODBC(this.w_IMFILTRO)+","+cp_ToStrODBC(this.w_IMCHIMOV)+","+cp_ToStrODBC(this.w_IMAGGSAL)+","+cp_ToStrODBC(this.w_IMAGGIVA)+","+cp_ToStrODBC(this.w_IMAGGPAR)+;
             ","+cp_ToStrODBC(this.w_IMAGGANA)+","+cp_ToStrODBC(this.w_IMCONIND)+","+cp_ToStrODBC(this.w_IMAGGNUR)+","+cp_ToStrODBC(this.w_IMPARIVA)+","+cp_ToStrODBC(this.w_IMAZZERA)+;
             ","+cp_ToStrODBC(this.w_IMCONTRO)+","+cp_ToStrODBC(this.w_IMRESOCO)+","+cp_ToStrODBC(this.w_IMTIPDBF)+","+cp_ToStrODBC(this.w_IMROUTIN)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IMPORTAZ')
        i_extval=cp_InsertValVFPExtFlds(this,'IMPORTAZ')
        cp_CheckDeletedKey(i_cTable,0,'IMCODICE',this.w_IMCODICE,'IMTIPSRC',this.w_IMTIPSRC,'IM_ASCII',this.w_IM_ASCII,'IMDESTIN',this.w_IMDESTIN)
        INSERT INTO (i_cTable) (;
                   IMCODICE;
                  ,IMANNOTA;
                  ,IMTIPSRC;
                  ,IMSEQUEN;
                  ,IM_ASCII;
                  ,IM_ODBC;
                  ,IM_EXCEL;
                  ,IMDESTIN;
                  ,IMTRASCO;
                  ,IMTRAMUL;
                  ,IMFILTRO;
                  ,IMCHIMOV;
                  ,IMAGGSAL;
                  ,IMAGGIVA;
                  ,IMAGGPAR;
                  ,IMAGGANA;
                  ,IMCONIND;
                  ,IMAGGNUR;
                  ,IMPARIVA;
                  ,IMAZZERA;
                  ,IMCONTRO;
                  ,IMRESOCO;
                  ,IMTIPDBF;
                  ,IMROUTIN;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IMCODICE;
                  ,this.w_IMANNOTA;
                  ,this.w_IMTIPSRC;
                  ,this.w_IMSEQUEN;
                  ,this.w_IM_ASCII;
                  ,this.w_IM_ODBC;
                  ,this.w_IM_EXCEL;
                  ,this.w_IMDESTIN;
                  ,this.w_IMTRASCO;
                  ,this.w_IMTRAMUL;
                  ,this.w_IMFILTRO;
                  ,this.w_IMCHIMOV;
                  ,this.w_IMAGGSAL;
                  ,this.w_IMAGGIVA;
                  ,this.w_IMAGGPAR;
                  ,this.w_IMAGGANA;
                  ,this.w_IMCONIND;
                  ,this.w_IMAGGNUR;
                  ,this.w_IMPARIVA;
                  ,this.w_IMAZZERA;
                  ,this.w_IMCONTRO;
                  ,this.w_IMRESOCO;
                  ,this.w_IMTIPDBF;
                  ,this.w_IMROUTIN;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (!empty(t_IMSEQUEN)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'IMPORTAZ')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " IMANNOTA="+cp_ToStrODBC(this.w_IMANNOTA)+;
                 ",IMPARIVA="+cp_ToStrODBC(this.w_IMPARIVA)+;
                 ",IMAZZERA="+cp_ToStrODBC(this.w_IMAZZERA)+;
                 ",IMCONTRO="+cp_ToStrODBC(this.w_IMCONTRO)+;
                 ",IMRESOCO="+cp_ToStrODBC(this.w_IMRESOCO)+;
                 ",IMTIPDBF="+cp_ToStrODBC(this.w_IMTIPDBF)+;
                 ",IMROUTIN="+cp_ToStrODBC(this.w_IMROUTIN)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and IM_ASCII="+cp_ToStrODBC(&i_TN.->IM_ASCII)+;
                 " and IMDESTIN="+cp_ToStrODBC(&i_TN.->IMDESTIN)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'IMPORTAZ')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  IMANNOTA=this.w_IMANNOTA;
                 ,IMPARIVA=this.w_IMPARIVA;
                 ,IMAZZERA=this.w_IMAZZERA;
                 ,IMCONTRO=this.w_IMCONTRO;
                 ,IMRESOCO=this.w_IMRESOCO;
                 ,IMTIPDBF=this.w_IMTIPDBF;
                 ,IMROUTIN=this.w_IMROUTIN;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and IM_ASCII=&i_TN.->IM_ASCII;
                      and IMDESTIN=&i_TN.->IMDESTIN;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (!empty(t_IMSEQUEN)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.gsim_mtr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_IMCODICE,"TRCODICE";
                     ,this.w_IMTIPSRC,"TRTIPSRC";
                     ,this.w_IM_ASCII,"TR_ASCII";
                     ,this.w_IMDESTIN,"TRDESTIN";
                     )
              this.gsim_mtr.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and IM_ASCII="+cp_ToStrODBC(&i_TN.->IM_ASCII)+;
                            " and IMDESTIN="+cp_ToStrODBC(&i_TN.->IMDESTIN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and IM_ASCII=&i_TN.->IM_ASCII;
                            and IMDESTIN=&i_TN.->IMDESTIN;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace IM_ASCII with this.w_IM_ASCII
              replace IMDESTIN with this.w_IMDESTIN
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update IMPORTAZ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'IMPORTAZ')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IMANNOTA="+cp_ToStrODBC(this.w_IMANNOTA)+;
                     ",IMSEQUEN="+cp_ToStrODBC(this.w_IMSEQUEN)+;
                     ",IM_ODBC="+cp_ToStrODBCNull(this.w_IM_ODBC)+;
                     ",IM_EXCEL="+cp_ToStrODBCNull(this.w_IM_EXCEL)+;
                     ",IMTRASCO="+cp_ToStrODBC(this.w_IMTRASCO)+;
                     ",IMTRAMUL="+cp_ToStrODBC(this.w_IMTRAMUL)+;
                     ",IMFILTRO="+cp_ToStrODBC(this.w_IMFILTRO)+;
                     ",IMCHIMOV="+cp_ToStrODBC(this.w_IMCHIMOV)+;
                     ",IMAGGSAL="+cp_ToStrODBC(this.w_IMAGGSAL)+;
                     ",IMAGGIVA="+cp_ToStrODBC(this.w_IMAGGIVA)+;
                     ",IMAGGPAR="+cp_ToStrODBC(this.w_IMAGGPAR)+;
                     ",IMAGGANA="+cp_ToStrODBC(this.w_IMAGGANA)+;
                     ",IMCONIND="+cp_ToStrODBC(this.w_IMCONIND)+;
                     ",IMAGGNUR="+cp_ToStrODBC(this.w_IMAGGNUR)+;
                     ",IMPARIVA="+cp_ToStrODBC(this.w_IMPARIVA)+;
                     ",IMAZZERA="+cp_ToStrODBC(this.w_IMAZZERA)+;
                     ",IMCONTRO="+cp_ToStrODBC(this.w_IMCONTRO)+;
                     ",IMRESOCO="+cp_ToStrODBC(this.w_IMRESOCO)+;
                     ",IMTIPDBF="+cp_ToStrODBC(this.w_IMTIPDBF)+;
                     ",IMROUTIN="+cp_ToStrODBC(this.w_IMROUTIN)+;
                     ",IM_ASCII="+cp_ToStrODBC(this.w_IM_ASCII)+;
                     ",IMDESTIN="+cp_ToStrODBC(this.w_IMDESTIN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and IM_ASCII="+cp_ToStrODBC(IM_ASCII)+;
                             " and IMDESTIN="+cp_ToStrODBC(IMDESTIN)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'IMPORTAZ')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IMANNOTA=this.w_IMANNOTA;
                     ,IMSEQUEN=this.w_IMSEQUEN;
                     ,IM_ODBC=this.w_IM_ODBC;
                     ,IM_EXCEL=this.w_IM_EXCEL;
                     ,IMTRASCO=this.w_IMTRASCO;
                     ,IMTRAMUL=this.w_IMTRAMUL;
                     ,IMFILTRO=this.w_IMFILTRO;
                     ,IMCHIMOV=this.w_IMCHIMOV;
                     ,IMAGGSAL=this.w_IMAGGSAL;
                     ,IMAGGIVA=this.w_IMAGGIVA;
                     ,IMAGGPAR=this.w_IMAGGPAR;
                     ,IMAGGANA=this.w_IMAGGANA;
                     ,IMCONIND=this.w_IMCONIND;
                     ,IMAGGNUR=this.w_IMAGGNUR;
                     ,IMPARIVA=this.w_IMPARIVA;
                     ,IMAZZERA=this.w_IMAZZERA;
                     ,IMCONTRO=this.w_IMCONTRO;
                     ,IMRESOCO=this.w_IMRESOCO;
                     ,IMTIPDBF=this.w_IMTIPDBF;
                     ,IMROUTIN=this.w_IMROUTIN;
                     ,IM_ASCII=this.w_IM_ASCII;
                     ,IMDESTIN=this.w_IMDESTIN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and IM_ASCII=&i_TN.->IM_ASCII;
                                      and IMDESTIN=&i_TN.->IMDESTIN;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (!empty(t_IMSEQUEN))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.gsim_mtr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_IMCODICE,"TRCODICE";
               ,this.w_IMTIPSRC,"TRTIPSRC";
               ,this.w_IM_ASCII,"TR_ASCII";
               ,this.w_IMDESTIN,"TRDESTIN";
               )
          this.gsim_mtr.mReplace()
          this.gsim_mtr.bSaveContext=.f.
        endif
      endscan
     this.gsim_mtr.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (!empty(t_IMSEQUEN)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- gsim_mtr : Deleting
        this.gsim_mtr.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_IMCODICE,"TRCODICE";
               ,this.w_IMTIPSRC,"TRTIPSRC";
               ,this.w_IM_ASCII,"TR_ASCII";
               ,this.w_IMDESTIN,"TRDESTIN";
               )
        this.gsim_mtr.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete IMPORTAZ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and IM_ASCII="+cp_ToStrODBC(&i_TN.->IM_ASCII)+;
                            " and IMDESTIN="+cp_ToStrODBC(&i_TN.->IMDESTIN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and IM_ASCII=&i_TN.->IM_ASCII;
                              and IMDESTIN=&i_TN.->IMDESTIN;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (!empty(t_IMSEQUEN)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_IM_ODBC<>.w_IM_ODBC.or. .o_IMTIPSRC<>.w_IMTIPSRC.or. .o_IM_EXCEL<>.w_IM_EXCEL
          .w_IM_ASCII = ICASE(.w_IMTIPSRC='O',.w_IM_ODBC,.w_IMTIPSRC='A',.w_IM_ASCII,.w_IM_EXCEL)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ICASE(.w_IMTIPSRC='A',Ah_MsgFormat("Nome file ASCII"),.w_IMTIPSRC='O',Ah_MsgFormat("Sorgente dati ODBC"),Ah_MsgFormat("Sorgente dati EXCEL")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_IM_ASCII with this.w_IM_ASCII
      replace t_IMFILTRO with this.w_IMFILTRO
      replace t_IMCHIMOV with this.w_IMCHIMOV
      replace t_IMAGGSAL with this.w_IMAGGSAL
      replace t_IMAGGIVA with this.w_IMAGGIVA
      replace t_IMAGGPAR with this.w_IMAGGPAR
      replace t_IMAGGANA with this.w_IMAGGANA
      replace t_IMCONIND with this.w_IMCONIND
      replace t_IMAGGNUR with this.w_IMAGGNUR
      replace t_FLGFILE with this.w_FLGFILE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ICASE(.w_IMTIPSRC='A',Ah_MsgFormat("Nome file ASCII"),.w_IMTIPSRC='O',Ah_MsgFormat("Sorgente dati ODBC"),Ah_MsgFormat("Sorgente dati EXCEL")))
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.enabled = this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_ASCII_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_ASCII_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_ODBC_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_ODBC_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_EXCEL_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIM_EXCEL_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIMTRAMUL_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIMTRAMUL_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.visible=!this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IM_ODBC
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_lTable = "SRCMODBC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2], .t., this.SRCMODBC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IM_ODBC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mso',True,'SRCMODBC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_IM_ODBC)+"%");

          i_ret=cp_SQL(i_nConn,"select SOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',trim(this.w_IM_ODBC))
          select SOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IM_ODBC)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IM_ODBC) and !this.bDontReportError
            deferred_cp_zoom('SRCMODBC','*','SOCODICE',cp_AbsName(oSource.parent,'oIM_ODBC_2_3'),i_cWhere,'gsim_mso',"Sorgente dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1))
            select SOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IM_ODBC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_IM_ODBC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_IM_ODBC)
            select SOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IM_ODBC = NVL(_Link_.SOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IM_ODBC = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SRCMODBC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IM_ODBC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IM_EXCEL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SRCMODBC_IDX,3]
    i_lTable = "SRCMODBC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2], .t., this.SRCMODBC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IM_EXCEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mso',True,'SRCMODBC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SOCODICE like "+cp_ToStrODBC(trim(this.w_IM_EXCEL)+"%");
                   +" and FLGFILE="+cp_ToStrODBC(this.w_FLGFILE);

          i_ret=cp_SQL(i_nConn,"select FLGFILE,SOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLGFILE,SOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLGFILE',this.w_FLGFILE;
                     ,'SOCODICE',trim(this.w_IM_EXCEL))
          select FLGFILE,SOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLGFILE,SOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IM_EXCEL)==trim(_Link_.SOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IM_EXCEL) and !this.bDontReportError
            deferred_cp_zoom('SRCMODBC','*','FLGFILE,SOCODICE',cp_AbsName(oSource.parent,'oIM_EXCEL_2_4'),i_cWhere,'gsim_mso',"Sorgente dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLGFILE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLGFILE,SOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FLGFILE,SOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLGFILE,SOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FLGFILE="+cp_ToStrODBC(this.w_FLGFILE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLGFILE',oSource.xKey(1);
                       ,'SOCODICE',oSource.xKey(2))
            select FLGFILE,SOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IM_EXCEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLGFILE,SOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SOCODICE="+cp_ToStrODBC(this.w_IM_EXCEL);
                   +" and FLGFILE="+cp_ToStrODBC(this.w_FLGFILE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLGFILE',this.w_FLGFILE;
                       ,'SOCODICE',this.w_IM_EXCEL)
            select FLGFILE,SOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IM_EXCEL = NVL(_Link_.SOCODICE,space(150))
    else
      if i_cCtrl<>'Load'
        this.w_IM_EXCEL = space(150)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SRCMODBC_IDX,2])+'\'+cp_ToStr(_Link_.FLGFILE,1)+'\'+cp_ToStr(_Link_.SOCODICE,1)
      cp_ShowWarn(i_cKey,this.SRCMODBC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IM_EXCEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMDESTIN
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMDESTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_IMDESTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_IMDESTIN))
          select ARCODICE,ARDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMDESTIN)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMDESTIN) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oIMDESTIN_2_6'),i_cWhere,'GSIM_AAR',"Archivi di destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMDESTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_IMDESTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_IMDESTIN)
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMDESTIN = NVL(_Link_.ARCODICE,space(2))
      this.w_DESARC = NVL(_Link_.ARDESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IMDESTIN = space(2)
      endif
      this.w_DESARC = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMDESTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMPOARCH_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.ARCODICE as ARCODICE206"+ ",link_2_6.ARDESCRI as ARDESCRI206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on IMPORTAZ.IMDESTIN=link_2_6.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and IMPORTAZ.IMDESTIN=link_2_6.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oIMCODICE_1_1.value==this.w_IMCODICE)
      this.oPgFrm.Page1.oPag.oIMCODICE_1_1.value=this.w_IMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oIMANNOTA_1_3.value==this.w_IMANNOTA)
      this.oPgFrm.Page1.oPag.oIMANNOTA_1_3.value=this.w_IMANNOTA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTIPSRC_1_4.RadioValue()==this.w_IMTIPSRC)
      this.oPgFrm.Page1.oPag.oIMTIPSRC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPARIVA_4_1.RadioValue()==this.w_IMPARIVA)
      this.oPgFrm.Page2.oPag.oIMPARIVA_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMAZZERA_4_2.RadioValue()==this.w_IMAZZERA)
      this.oPgFrm.Page2.oPag.oIMAZZERA_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMCONTRO_4_5.RadioValue()==this.w_IMCONTRO)
      this.oPgFrm.Page2.oPag.oIMCONTRO_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMRESOCO_4_6.RadioValue()==this.w_IMRESOCO)
      this.oPgFrm.Page2.oPag.oIMRESOCO_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.RadioValue()==this.w_IMTIPDBF)
      this.oPgFrm.Page1.oPag.oIMTIPDBF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1.value==this.w_IMSEQUEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1.value=this.w_IMSEQUEN
      replace t_IMSEQUEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.value==this.w_IM_ASCII)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.value=this.w_IM_ASCII
      replace t_IM_ASCII with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ASCII_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.value==this.w_IM_ODBC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.value=this.w_IM_ODBC
      replace t_IM_ODBC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_ODBC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.value==this.w_IM_EXCEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.value=this.w_IM_EXCEL
      replace t_IM_EXCEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIM_EXCEL_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESTIN_2_6.value==this.w_IMDESTIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESTIN_2_6.value=this.w_IMDESTIN
      replace t_IMDESTIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESTIN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESARC_2_7.value==this.w_DESARC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESARC_2_7.value=this.w_DESARC
      replace t_DESARC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESARC_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.RadioValue()==this.w_IMTRASCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.SetRadio()
      replace t_IMTRASCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.RadioValue()==this.w_IMTRAMUL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.SetRadio()
      replace t_IMTRAMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'IMPORTAZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (!empty(t_IMSEQUEN));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_IMSEQUEN) and (!empty(.w_IMSEQUEN))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMSEQUEN_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_IMDESTIN) and (!empty(.w_IMSEQUEN))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESTIN_2_6
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .gsim_mtr.CheckForm()
      if !empty(.w_IMSEQUEN)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMTIPSRC = this.w_IMTIPSRC
    this.o_IM_ODBC = this.w_IM_ODBC
    this.o_IM_EXCEL = this.w_IM_EXCEL
    * --- gsim_mtr : Depends On
    this.gsim_mtr.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(!empty(t_IMSEQUEN))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_IMSEQUEN=0
      .w_IM_ASCII=space(200)
      .w_IM_ODBC=space(20)
      .w_IM_EXCEL=space(150)
      .w_IM_ASCII=space(200)
      .w_IMDESTIN=space(2)
      .w_DESARC=space(20)
      .w_IMTRASCO=space(1)
      .w_IMTRAMUL=space(1)
      .w_IMFILTRO=space(70)
      .w_IMCHIMOV=space(70)
      .w_IMAGGSAL=space(1)
      .w_IMAGGIVA=space(1)
      .w_IMAGGPAR=space(1)
      .w_IMAGGANA=space(1)
      .w_IMCONIND=space(15)
      .w_IMAGGNUR=space(1)
      .w_FLGFILE=space(10)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_IM_ODBC))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_IM_EXCEL))
        .link_2_4('Full')
      endif
        .w_IM_ASCII = ICASE(.w_IMTIPSRC='O',.w_IM_ODBC,.w_IMTIPSRC='A',.w_IM_ASCII,.w_IM_EXCEL)
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_IMDESTIN))
        .link_2_6('Full')
      endif
      .DoRTCalc(10,10,.f.)
        .w_IMTRASCO = 'N'
        .w_IMTRAMUL = 'N'
      .DoRTCalc(13,14,.f.)
        .w_IMAGGSAL = 'N'
        .w_IMAGGIVA = 'S'
        .w_IMAGGPAR = 'S'
        .w_IMAGGANA = 'N'
      .DoRTCalc(19,19,.f.)
        .w_IMAGGNUR = 'S'
      .DoRTCalc(21,26,.f.)
        .w_FLGFILE = 'S'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_IMSEQUEN = t_IMSEQUEN
    this.w_IM_ASCII = t_IM_ASCII
    this.w_IM_ODBC = t_IM_ODBC
    this.w_IM_EXCEL = t_IM_EXCEL
    this.w_IM_ASCII = t_IM_ASCII
    this.w_IMDESTIN = t_IMDESTIN
    this.w_DESARC = t_DESARC
    this.w_IMTRASCO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.RadioValue(.t.)
    this.w_IMTRAMUL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.RadioValue(.t.)
    this.w_IMFILTRO = t_IMFILTRO
    this.w_IMCHIMOV = t_IMCHIMOV
    this.w_IMAGGSAL = t_IMAGGSAL
    this.w_IMAGGIVA = t_IMAGGIVA
    this.w_IMAGGPAR = t_IMAGGPAR
    this.w_IMAGGANA = t_IMAGGANA
    this.w_IMCONIND = t_IMCONIND
    this.w_IMAGGNUR = t_IMAGGNUR
    this.w_FLGFILE = t_FLGFILE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_IMSEQUEN with this.w_IMSEQUEN
    replace t_IM_ASCII with this.w_IM_ASCII
    replace t_IM_ODBC with this.w_IM_ODBC
    replace t_IM_EXCEL with this.w_IM_EXCEL
    replace t_IM_ASCII with this.w_IM_ASCII
    replace t_IMDESTIN with this.w_IMDESTIN
    replace t_DESARC with this.w_DESARC
    replace t_IMTRASCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRASCO_2_8.ToRadio()
    replace t_IMTRAMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMTRAMUL_2_9.ToRadio()
    replace t_IMFILTRO with this.w_IMFILTRO
    replace t_IMCHIMOV with this.w_IMCHIMOV
    replace t_IMAGGSAL with this.w_IMAGGSAL
    replace t_IMAGGIVA with this.w_IMAGGIVA
    replace t_IMAGGPAR with this.w_IMAGGPAR
    replace t_IMAGGANA with this.w_IMAGGANA
    replace t_IMCONIND with this.w_IMCONIND
    replace t_IMAGGNUR with this.w_IMAGGNUR
    replace t_FLGFILE with this.w_FLGFILE
    if i_srv='A'
      replace IM_ASCII with this.w_IM_ASCII
      replace IMDESTIN with this.w_IMDESTIN
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_mimPag1 as StdContainer
  Width  = 784
  height = 445
  stdWidth  = 784
  stdheight = 445
  resizeXpos=324
  resizeYpos=371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMCODICE_1_1 as StdField with uid="BXEVWNRGJK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IMCODICE", cQueryName = "IMCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della configurazione dei tracciati di importazione",;
    HelpContextID = 37109045,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=138, Top=12, InputMask=replicate('X',20)

  add object oIMANNOTA_1_3 as StdMemo with uid="XTIFRZZUGL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IMANNOTA", cQueryName = "IMANNOTA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note generiche relative alla configurazione dei tracciati di importazione",;
    HelpContextID = 194469177,;
   bGlobalFont=.t.,;
    Height=53, Width=448, Left=323, Top=12

  add object oIMTIPSRC_1_4 as StdRadio with uid="FULUGUONQX",rtseq=3,rtrep=.f.,left=127, top=36, width=193,height=17;
    , cFormVar="w_IMTIPSRC", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oIMTIPSRC_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="ASCII"
      this.Buttons(1).HelpContextID = 125513015
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("ASCII","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="ODBC"
      this.Buttons(2).HelpContextID = 125513015
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("ODBC","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="EXCEL"
      this.Buttons(3).HelpContextID = 125513015
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("EXCEL","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oIMTIPSRC_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMTIPSRC,&i_cF..t_IMTIPSRC),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'O',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oIMTIPSRC_1_4.GetRadio()
    this.Parent.oContained.w_IMTIPSRC = this.RadioValue()
    return .t.
  endfunc

  func oIMTIPSRC_1_4.ToRadio()
    this.Parent.oContained.w_IMTIPSRC=trim(this.Parent.oContained.w_IMTIPSRC)
    return(;
      iif(this.Parent.oContained.w_IMTIPSRC=='A',1,;
      iif(this.Parent.oContained.w_IMTIPSRC=='O',2,;
      iif(this.Parent.oContained.w_IMTIPSRC=='E',3,;
      0))))
  endfunc

  func oIMTIPSRC_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIMTIPDBF_1_6 as StdCheck with uid="YCFPHOZTWK",rtseq=25,rtrep=.f.,left=150, top=51, caption="ODBC per file DBF",;
    HelpContextID = 108735796,;
    cFormVar="w_IMTIPDBF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMTIPDBF_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMTIPDBF,&i_cF..t_IMTIPDBF),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oIMTIPDBF_1_6.GetRadio()
    this.Parent.oContained.w_IMTIPDBF = this.RadioValue()
    return .t.
  endfunc

  func oIMTIPDBF_1_6.ToRadio()
    this.Parent.oContained.w_IMTIPDBF=trim(this.Parent.oContained.w_IMTIPDBF)
    return(;
      iif(this.Parent.oContained.w_IMTIPDBF=='S',1,;
      0))
  endfunc

  func oIMTIPDBF_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIMTIPDBF_1_6.mCond()
    with this.Parent.oContained
      return (.w_IMTIPSRC='O')
    endwith
  endfunc

  func oIMTIPDBF_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'O')
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=75, width=763,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="IMSEQUEN",Label1="Sequenza",Field2="IM_ASCII",Label2="",Field3="IM_ODBC",Label3="",Field4="IMDESTIN",Label4="Archivio",Field5="DESARC",Label5="Destinazione",Field6="IMTRASCO",Label6="Trascodifica",Field7="IMTRAMUL",Label7="Multiplo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 217224582


  add object oObj_1_8 as cp_calclbl with uid="ZVJCFQBFEU",left=86, top=78, width=229,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=2,;
    nPag=1;
    , HelpContextID = 86882330

  add object oStr_1_2 as StdString with uid="IFGFORNQPK",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice importazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="TUWCQCTRKZ",Visible=.t., Left=17, Top=35,;
    Alignment=1, Width=102, Height=18,;
    Caption="Sorgente dati:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=94,;
    width=759+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.7000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=95,width=758+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.7000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='SRCMODBC|IMPOARCH|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='SRCMODBC'
        oDropInto=this.oBodyCol.oRow.oIM_ODBC_2_3
      case cFile='SRCMODBC'
        oDropInto=this.oBodyCol.oRow.oIM_EXCEL_2_4
      case cFile='IMPOARCH'
        oDropInto=this.oBodyCol.oRow.oIMDESTIN_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_10 as StdButton with uid="EVGUBDDZHV",width=48,height=45,;
   left=61, top=400,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , HelpContextID = 230814095;
    , Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_10.Click()
      this.Parent.oContained.gsim_mtr.LinkPCClick()
    endproc

  add object oBtn_2_19 as StdButton with uid="TUMFNGBFDJ",width=48,height=45,;
   left=6, top=400,;
    CpPicture="BMP\AVANZATE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle opzioni avanzate di importazione";
    , HelpContextID = 112538773;
    , Caption='\<Avanzate';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      do gsim_kav with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsim_mimPag2 as StdContainer
    Width  = 784
    height = 445
    stdWidth  = 784
    stdheight = 445
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMPARIVA_4_1 as StdCheck with uid="RWXESCHPYL",rtseq=21,rtrep=.f.,left=34, top=66, caption="Controllo p.IVA",;
    ToolTipText = "Se attivato consente il riconoscimento dei clienti/fornitori per partita IVA",;
    HelpContextID = 23293241,;
    cFormVar="w_IMPARIVA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oIMPARIVA_4_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMPARIVA,&i_cF..t_IMPARIVA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oIMPARIVA_4_1.GetRadio()
    this.Parent.oContained.w_IMPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oIMPARIVA_4_1.ToRadio()
    this.Parent.oContained.w_IMPARIVA=trim(this.Parent.oContained.w_IMPARIVA)
    return(;
      iif(this.Parent.oContained.w_IMPARIVA=='S',1,;
      0))
  endfunc

  func oIMPARIVA_4_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIMAZZERA_4_2 as StdCheck with uid="WXAQTPSCQV",rtseq=22,rtrep=.f.,left=34, top=88, caption="Azzera movimenti",;
    ToolTipText = "Se attivato la procedura elimina le movimentazioni prima di importarle",;
    HelpContextID = 80436537,;
    cFormVar="w_IMAZZERA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oIMAZZERA_4_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMAZZERA,&i_cF..t_IMAZZERA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oIMAZZERA_4_2.GetRadio()
    this.Parent.oContained.w_IMAZZERA = this.RadioValue()
    return .t.
  endfunc

  func oIMAZZERA_4_2.ToRadio()
    this.Parent.oContained.w_IMAZZERA=trim(this.Parent.oContained.w_IMAZZERA)
    return(;
      iif(this.Parent.oContained.w_IMAZZERA=='S',1,;
      0))
  endfunc

  func oIMAZZERA_4_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIMCONTRO_4_5 as StdCheck with uid="HWMWYGDJTY",rtseq=23,rtrep=.f.,left=34, top=110, caption="Controllo valori",;
    ToolTipText = "Se attivato consente di controllare i valori assegnati ai singoli campi",;
    HelpContextID = 110509355,;
    cFormVar="w_IMCONTRO", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oIMCONTRO_4_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMCONTRO,&i_cF..t_IMCONTRO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oIMCONTRO_4_5.GetRadio()
    this.Parent.oContained.w_IMCONTRO = this.RadioValue()
    return .t.
  endfunc

  func oIMCONTRO_4_5.ToRadio()
    this.Parent.oContained.w_IMCONTRO=trim(this.Parent.oContained.w_IMCONTRO)
    return(;
      iif(this.Parent.oContained.w_IMCONTRO=='S',1,;
      0))
  endfunc

  func oIMCONTRO_4_5.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oIMRESOCO_4_6 as StdCombo with uid="PVDUGLSRIR",rtseq=24,rtrep=.f.,left=260,top=66,width=151,height=21;
    , ToolTipText = "Consente di selezionare il tipo di resoconto desiderato";
    , HelpContextID = 189746475;
    , cFormVar="w_IMRESOCO",RowSource=""+"Errori+segnalazioni,"+"Errori,"+"Nessuno", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oIMRESOCO_4_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMRESOCO,&i_cF..t_IMRESOCO),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'E',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oIMRESOCO_4_6.GetRadio()
    this.Parent.oContained.w_IMRESOCO = this.RadioValue()
    return .t.
  endfunc

  func oIMRESOCO_4_6.ToRadio()
    this.Parent.oContained.w_IMRESOCO=trim(this.Parent.oContained.w_IMRESOCO)
    return(;
      iif(this.Parent.oContained.w_IMRESOCO=='S',1,;
      iif(this.Parent.oContained.w_IMRESOCO=='E',2,;
      iif(this.Parent.oContained.w_IMRESOCO=='N',3,;
      0))))
  endfunc

  func oIMRESOCO_4_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oStr_4_4 as StdString with uid="NCZBVQBCXC",Visible=.t., Left=27, Top=38,;
    Alignment=0, Width=181, Height=15,;
    Caption="Opzioni di importazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="QITLBBKYUU",Visible=.t., Left=258, Top=38,;
    Alignment=0, Width=190, Height=15,;
    Caption="Opzioni di scrittura dei resoconti"  ;
  , bGlobalFont=.t.

  add object oBox_4_3 as StdBox with uid="PVMUSQLIDX",left=23, top=54, width=186,height=1

  add object oBox_4_7 as StdBox with uid="BYNIYQFPLZ",left=254, top=54, width=201,height=1
enddefine

* --- Defining Body row
define class tgsim_mimBodyRow as CPBodyRowCnt
  Width=749
  Height=int(fontmetric(1,"Arial",9,"")*1*1.7000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oIMSEQUEN_2_1 as StdTrsField with uid="LQYHECDZNL",rtseq=4,rtrep=.t.,;
    cFormVar="w_IMSEQUEN",value=0,;
    ToolTipText = "Sequenza importazione",;
    HelpContextID = 91176236,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=50, Left=-2, Top=0, cSayPict=[v_ZR+"99999"], cGetPict=[v_ZR+"99999"]

  add object oIM_ASCII_2_2 as StdTrsField with uid="LOZOFNIDYG",rtseq=5,rtrep=.t.,;
    cFormVar="w_IM_ASCII",value=space(200),isprimarykey=.t.,;
    HelpContextID = 145588943,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=324, Left=59, Top=0, cSayPict=[repl("!",200)], cGetPict=[repl("!",200)], InputMask=replicate('X',200), bHasZoom = .t. 

  func oIM_ASCII_2_2.mCond()
    with this.Parent.oContained
      return (.w_IMTIPSRC='A')
    endwith
  endfunc

  func oIM_ASCII_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'A')
    endwith
    endif
  endfunc

  proc oIM_ASCII_2_2.mZoom
    this.parent.oContained.w_IM_ASCII=left(getfile()+space(254),254)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIM_ODBC_2_3 as StdTrsField with uid="INKEEMRYAQ",rtseq=6,rtrep=.t.,;
    cFormVar="w_IM_ODBC",value=space(20),;
    HelpContextID = 154434938,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=188, Left=51, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="SRCMODBC", cZoomOnZoom="gsim_mso", oKey_1_1="SOCODICE", oKey_1_2="this.w_IM_ODBC"

  func oIM_ODBC_2_3.mCond()
    with this.Parent.oContained
      return (.w_IMTIPSRC='O')
    endwith
  endfunc

  func oIM_ODBC_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'O')
    endwith
    endif
  endfunc

  func oIM_ODBC_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oIM_ODBC_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIM_ODBC_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SRCMODBC','*','SOCODICE',cp_AbsName(this.parent,'oIM_ODBC_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mso',"Sorgente dati",'',this.parent.oContained
  endproc
  proc oIM_ODBC_2_3.mZoomOnZoom
    local i_obj
    i_obj=gsim_mso()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SOCODICE=this.parent.oContained.w_IM_ODBC
    i_obj.ecpSave()
  endproc

  add object oIM_EXCEL_2_4 as StdTrsField with uid="XFDSKSXNOX",rtseq=7,rtrep=.t.,;
    cFormVar="w_IM_EXCEL",value=space(150),;
    HelpContextID = 117341486,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=51, Top=0, InputMask=replicate('X',150), bHasZoom = .t. , cLinkFile="SRCMODBC", cZoomOnZoom="gsim_mso", oKey_1_1="FLGFILE", oKey_1_2="this.w_FLGFILE", oKey_2_1="SOCODICE", oKey_2_2="this.w_IM_EXCEL"

  func oIM_EXCEL_2_4.mCond()
    with this.Parent.oContained
      return (.w_IMTIPSRC='E')
    endwith
  endfunc

  func oIM_EXCEL_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'E')
    endwith
    endif
  endfunc

  func oIM_EXCEL_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oIM_EXCEL_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIM_EXCEL_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SRCMODBC_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FLGFILE="+cp_ToStrODBC(this.Parent.oContained.w_FLGFILE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FLGFILE="+cp_ToStr(this.Parent.oContained.w_FLGFILE)
    endif
    do cp_zoom with 'SRCMODBC','*','FLGFILE,SOCODICE',cp_AbsName(this.parent,'oIM_EXCEL_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mso',"Sorgente dati",'',this.parent.oContained
  endproc
  proc oIM_EXCEL_2_4.mZoomOnZoom
    local i_obj
    i_obj=gsim_mso()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FLGFILE=w_FLGFILE
     i_obj.w_SOCODICE=this.parent.oContained.w_IM_EXCEL
    i_obj.ecpSave()
  endproc

  add object oIMDESTIN_2_6 as StdTrsField with uid="JUDVJLMMIH",rtseq=9,rtrep=.t.,;
    cFormVar="w_IMDESTIN",value=space(2),isprimarykey=.t.,;
    ToolTipText = "Archivio di destinazione in Enterprise",;
    HelpContextID = 162517716,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=390, Top=0, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_IMDESTIN"

  func oIMDESTIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oIMDESTIN_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIMDESTIN_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oIMDESTIN_2_6.readonly and this.parent.oIMDESTIN_2_6.isprimarykey)
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oIMDESTIN_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"Archivi di destinazione",'',this.parent.oContained
   endif
  endproc
  proc oIMDESTIN_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_IMDESTIN
    i_obj.ecpSave()
  endproc

  add object oDESARC_2_7 as StdTrsField with uid="SAZEOYUYWS",rtseq=10,rtrep=.t.,;
    cFormVar="w_DESARC",value=space(20),enabled=.f.,;
    HelpContextID = 123946442,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=432, Top=0, InputMask=replicate('X',20)

  add object oIMTRASCO_2_8 as StdTrsCombo with uid="KXHQMPPENI",rtrep=.t.,;
    cFormVar="w_IMTRASCO", RowSource=""+"Con trascodifica,"+"Senza trascodifica" , ;
    HelpContextID = 140651819,;
    Height=21, Width=121, Left=568, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIMTRASCO_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMTRASCO,&i_cF..t_IMTRASCO),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oIMTRASCO_2_8.GetRadio()
    this.Parent.oContained.w_IMTRASCO = this.RadioValue()
    return .t.
  endfunc

  func oIMTRASCO_2_8.ToRadio()
    this.Parent.oContained.w_IMTRASCO=trim(this.Parent.oContained.w_IMTRASCO)
    return(;
      iif(this.Parent.oContained.w_IMTRASCO=='S',1,;
      iif(this.Parent.oContained.w_IMTRASCO=='N',2,;
      0)))
  endfunc

  func oIMTRASCO_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIMTRAMUL_2_9 as StdTrsCombo with uid="AKKNBOQGPA",rtrep=.t.,;
    cFormVar="w_IMTRAMUL", RowSource=""+"Si,"+"No" , ;
    ToolTipText = "Indica se i dati sono compresi in un file a tracciato multiplo",;
    HelpContextID = 241315118,;
    Height=21, Width=45, Left=699, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIMTRAMUL_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IMTRAMUL,&i_cF..t_IMTRAMUL),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oIMTRAMUL_2_9.GetRadio()
    this.Parent.oContained.w_IMTRAMUL = this.RadioValue()
    return .t.
  endfunc

  func oIMTRAMUL_2_9.ToRadio()
    this.Parent.oContained.w_IMTRAMUL=trim(this.Parent.oContained.w_IMTRAMUL)
    return(;
      iif(this.Parent.oContained.w_IMTRAMUL=='S',1,;
      iif(this.Parent.oContained.w_IMTRAMUL=='N',2,;
      0)))
  endfunc

  func oIMTRAMUL_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIMTRAMUL_2_9.mCond()
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'O' AND .w_IMTIPSRC<>'E')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oIMSEQUEN_2_1.When()
    return(.t.)
  proc oIMSEQUEN_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oIMSEQUEN_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mim','IMPORTAZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IMCODICE=IMPORTAZ.IMCODICE";
  +" and "+i_cAliasName2+".IMTIPSRC=IMPORTAZ.IMTIPSRC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
