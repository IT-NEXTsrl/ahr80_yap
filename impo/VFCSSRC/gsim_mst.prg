* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mst                                                        *
*              Struttura tabelle                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-12                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsim_mst")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsim_mst")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsim_mst")
  return

* --- Class definition
define class tgsim_mst as StdPCForm
  Width  = 629
  Height = 277
  Top    = 10
  Left   = 10
  cComment = "Struttura tabelle"
  cPrg = "gsim_mst"
  HelpContextID=97107817
  add object cnt as tcgsim_mst
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsim_mst as PCContext
  w_TBNAME = space(30)
  w_FLNAME = space(30)
  w_FLTYPE = space(1)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_FLCOMMEN = space(80)
  w_FLCHECK = space(4)
  proc Save(i_oFrom)
    this.w_TBNAME = i_oFrom.w_TBNAME
    this.w_FLNAME = i_oFrom.w_FLNAME
    this.w_FLTYPE = i_oFrom.w_FLTYPE
    this.w_FLLENGHT = i_oFrom.w_FLLENGHT
    this.w_FLDECIMA = i_oFrom.w_FLDECIMA
    this.w_FLCOMMEN = i_oFrom.w_FLCOMMEN
    this.w_FLCHECK = i_oFrom.w_FLCHECK
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TBNAME = this.w_TBNAME
    i_oTo.w_FLNAME = this.w_FLNAME
    i_oTo.w_FLTYPE = this.w_FLTYPE
    i_oTo.w_FLLENGHT = this.w_FLLENGHT
    i_oTo.w_FLDECIMA = this.w_FLDECIMA
    i_oTo.w_FLCOMMEN = this.w_FLCOMMEN
    i_oTo.w_FLCHECK = this.w_FLCHECK
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsim_mst as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 629
  Height = 277
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=97107817
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  XDC_FIELDS_IDX = 0
  cFile = "XDC_FIELDS"
  cKeySelect = "TBNAME"
  cKeyWhere  = "TBNAME=this.w_TBNAME"
  cKeyDetail  = "TBNAME=this.w_TBNAME and FLNAME=this.w_FLNAME"
  cKeyWhereODBC = '"TBNAME="+cp_ToStrODBC(this.w_TBNAME)';

  cKeyDetailWhereODBC = '"TBNAME="+cp_ToStrODBC(this.w_TBNAME)';
      +'+" and FLNAME="+cp_ToStrODBC(this.w_FLNAME)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"XDC_FIELDS.TBNAME="+cp_ToStrODBC(this.w_TBNAME)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'XDC_FIELDS.CPROWNUM '
  cPrg = "gsim_mst"
  cComment = "Struttura tabelle"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 3
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TBNAME = space(30)
  w_FLNAME = space(30)
  w_FLTYPE = space(1)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_FLCOMMEN = space(80)
  w_FLCHECK = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_mstPag1","gsim_mst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTBNAME_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='XDC_FIELDS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.XDC_FIELDS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.XDC_FIELDS_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsim_mst'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from XDC_FIELDS where TBNAME=KeySet.TBNAME
    *                            and FLNAME=KeySet.FLNAME
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2],this.bLoadRecFilter,this.XDC_FIELDS_IDX,"gsim_mst")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('XDC_FIELDS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "XDC_FIELDS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' XDC_FIELDS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TBNAME',this.w_TBNAME  )
      select * from (i_cTable) XDC_FIELDS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TBNAME = NVL(TBNAME,space(30))
        cp_LoadRecExtFlds(this,'XDC_FIELDS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_FLNAME = NVL(FLNAME,space(30))
          .w_FLTYPE = NVL(FLTYPE,space(1))
          .w_FLLENGHT = NVL(FLLENGHT,0)
          .w_FLDECIMA = NVL(FLDECIMA,0)
          .w_FLCOMMEN = NVL(FLCOMMEN,space(80))
          .w_FLCHECK = NVL(FLCHECK,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TBNAME=space(30)
      .w_FLNAME=space(30)
      .w_FLTYPE=space(1)
      .w_FLLENGHT=0
      .w_FLDECIMA=0
      .w_FLCOMMEN=space(80)
      .w_FLCHECK=space(0)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'XDC_FIELDS')
    this.DoRTCalc(1,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTBNAME_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTBNAME_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTBNAME_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'XDC_FIELDS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBNAME,"TBNAME",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FLNAME C(30);
      ,t_FLTYPE C(1);
      ,t_FLLENGHT N(5);
      ,t_FLDECIMA N(5);
      ,t_FLCOMMEN C(80);
      ,t_FLCHECK M(4);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_mstbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.controlsource=this.cTrsName+'.t_FLNAME'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.controlsource=this.cTrsName+'.t_FLTYPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.controlsource=this.cTrsName+'.t_FLLENGHT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.controlsource=this.cTrsName+'.t_FLDECIMA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.controlsource=this.cTrsName+'.t_FLCOMMEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLCHECK_2_6.controlsource=this.cTrsName+'.t_FLCHECK'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
      *
      * insert into XDC_FIELDS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'XDC_FIELDS')
        i_extval=cp_InsertValODBCExtFlds(this,'XDC_FIELDS')
        i_cFldBody=" "+;
                  "(TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA"+;
                  ",FLCOMMEN,FLCHECK,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TBNAME)+","+cp_ToStrODBC(this.w_FLNAME)+","+cp_ToStrODBC(this.w_FLTYPE)+","+cp_ToStrODBC(this.w_FLLENGHT)+","+cp_ToStrODBC(this.w_FLDECIMA)+;
             ","+cp_ToStrODBC(this.w_FLCOMMEN)+","+cp_ToStrODBC(this.w_FLCHECK)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'XDC_FIELDS')
        i_extval=cp_InsertValVFPExtFlds(this,'XDC_FIELDS')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TBNAME',this.w_TBNAME,'FLNAME',this.w_FLNAME)
        INSERT INTO (i_cTable) (;
                   TBNAME;
                  ,FLNAME;
                  ,FLTYPE;
                  ,FLLENGHT;
                  ,FLDECIMA;
                  ,FLCOMMEN;
                  ,FLCHECK;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TBNAME;
                  ,this.w_FLNAME;
                  ,this.w_FLTYPE;
                  ,this.w_FLLENGHT;
                  ,this.w_FLDECIMA;
                  ,this.w_FLCOMMEN;
                  ,this.w_FLCHECK;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_FLNAME))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'XDC_FIELDS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'XDC_FIELDS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_FLNAME))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update XDC_FIELDS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'XDC_FIELDS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " FLTYPE="+cp_ToStrODBC(this.w_FLTYPE)+;
                     ",FLLENGHT="+cp_ToStrODBC(this.w_FLLENGHT)+;
                     ",FLDECIMA="+cp_ToStrODBC(this.w_FLDECIMA)+;
                     ",FLCOMMEN="+cp_ToStrODBC(this.w_FLCOMMEN)+;
                     ",FLCHECK="+cp_ToStrODBC(this.w_FLCHECK)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'XDC_FIELDS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      FLTYPE=this.w_FLTYPE;
                     ,FLLENGHT=this.w_FLLENGHT;
                     ,FLDECIMA=this.w_FLDECIMA;
                     ,FLCOMMEN=this.w_FLCOMMEN;
                     ,FLCHECK=this.w_FLCHECK;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_FLNAME))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete XDC_FIELDS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_FLNAME))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTBNAME_1_1.value==this.w_TBNAME)
      this.oPgFrm.Page1.oPag.oTBNAME_1_1.value=this.w_TBNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value==this.w_FLNAME)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value=this.w_FLNAME
      replace t_FLNAME with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNAME_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value==this.w_FLTYPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value=this.w_FLTYPE
      replace t_FLTYPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLTYPE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value==this.w_FLLENGHT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value=this.w_FLLENGHT
      replace t_FLLENGHT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLLENGHT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value==this.w_FLDECIMA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value=this.w_FLDECIMA
      replace t_FLDECIMA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDECIMA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value=this.w_FLCOMMEN
      replace t_FLCOMMEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMMEN_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCHECK_2_6.value==this.w_FLCHECK)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCHECK_2_6.value=this.w_FLCHECK
      replace t_FLCHECK with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCHECK_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'XDC_FIELDS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_FLNAME))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_FLNAME)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FLNAME=space(30)
      .w_FLTYPE=space(1)
      .w_FLLENGHT=0
      .w_FLDECIMA=0
      .w_FLCOMMEN=space(80)
      .w_FLCHECK=space(0)
    endwith
    this.DoRTCalc(1,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FLNAME = t_FLNAME
    this.w_FLTYPE = t_FLTYPE
    this.w_FLLENGHT = t_FLLENGHT
    this.w_FLDECIMA = t_FLDECIMA
    this.w_FLCOMMEN = t_FLCOMMEN
    this.w_FLCHECK = t_FLCHECK
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FLNAME with this.w_FLNAME
    replace t_FLTYPE with this.w_FLTYPE
    replace t_FLLENGHT with this.w_FLLENGHT
    replace t_FLDECIMA with this.w_FLDECIMA
    replace t_FLCOMMEN with this.w_FLCOMMEN
    replace t_FLCHECK with this.w_FLCHECK
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_mstPag1 as StdContainer
  Width  = 625
  height = 277
  stdWidth  = 625
  stdheight = 277
  resizeXpos=467
  resizeYpos=190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTBNAME_1_1 as StdField with uid="PGAJFSGHCB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TBNAME", cQueryName = "TBNAME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 72116278,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=280, Top=10, InputMask=replicate('X',30)

  add object oStr_1_2 as StdString with uid="CENHDSEEQC",Visible=.t., Left=157, Top=14,;
    Alignment=1, Width=122, Height=18,;
    Caption="Nome tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="RBVTJGGJKM",Visible=.t., Left=20, Top=39,;
    Alignment=2, Width=223, Height=18,;
    Caption="Nome campo"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="ZOFTPYAQNM",Visible=.t., Left=247, Top=58,;
    Alignment=2, Width=28, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EGNCVRACVG",Visible=.t., Left=286, Top=58,;
    Alignment=2, Width=69, Height=18,;
    Caption="Lunghezza"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="OHCJHIEQYG",Visible=.t., Left=359, Top=58,;
    Alignment=2, Width=69, Height=18,;
    Caption="Decimali"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RRUMZMEJQW",Visible=.t., Left=20, Top=58,;
    Alignment=2, Width=223, Height=18,;
    Caption="Commento"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=73,;
    width=586+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=74,width=585+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsim_mstBodyRow as CPBodyRowCnt
  Width=576
  Height=int(fontmetric(1,"Arial",9,"")*3*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFLNAME_2_1 as StdTrsField with uid="BZHCNIUOVY",rtseq=2,rtrep=.t.,;
    cFormVar="w_FLNAME",value=space(30),isprimarykey=.t.,;
    HelpContextID = 72118614,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=-2, Top=0, InputMask=replicate('X',30)

  add object oFLTYPE_2_2 as StdTrsField with uid="SVHEPKTQHZ",rtseq=3,rtrep=.t.,;
    cFormVar="w_FLTYPE",value=space(1),;
    HelpContextID = 76861782,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=226, Top=0, InputMask=replicate('X',1)

  add object oFLLENGHT_2_3 as StdTrsField with uid="MCATWPIVSR",rtseq=4,rtrep=.t.,;
    cFormVar="w_FLLENGHT",value=0,;
    HelpContextID = 161459798,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=275, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oFLDECIMA_2_4 as StdTrsField with uid="CHJEMPKYVL",rtseq=5,rtrep=.t.,;
    cFormVar="w_FLDECIMA",value=0,;
    HelpContextID = 139472489,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=346, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oFLCOMMEN_2_5 as StdTrsField with uid="TQRHBTVTME",rtseq=6,rtrep=.t.,;
    cFormVar="w_FLCOMMEN",value=space(80),;
    HelpContextID = 207208868,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=573, Left=-2, Top=19, InputMask=replicate('X',80)

  add object oFLCHECK_2_6 as StdTrsMemo with uid="QOWZDONOMM",rtseq=7,rtrep=.t.,;
    cFormVar="w_FLCHECK",value=space(0),;
    HelpContextID = 237846186,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=38
  add object oLast as LastKeyMover
  * ---
  func oFLNAME_2_1.When()
    return(.t.)
  proc oFLNAME_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFLNAME_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=2
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mst','XDC_FIELDS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TBNAME=XDC_FIELDS.TBNAME";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
