* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bta                                                        *
*              Traduzione codici di ricerca                                    *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_178]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bta",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bta as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_ODBC = space(30)
  w_NOMARC = space(2)
  w_CODICE = space(15)
  w_CODLIN = space(3)
  w_DESCOD = space(30)
  w_DESSUP = space(30)
  w_UNMIS1 = space(2)
  w_UNMIS2 = space(2)
  w_CODART = space(15)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TRASFORMA CURSORE CON LETTURA CODICI DI RICERCA ASSOCIATI ALL'ARTICOLO DA IMPORTARE
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      CREATE CURSOR CODRIC (LGNOMARC C(2), LGCODICE C(15), LGCODLIN C(3), LGDESCOD C(30), LGDESSUP C(30), LGUNMIS1 C(2), LGUNMIS2 C(2))
      SELECT (this.w_CURSOR)
      GO TOP
      SCAN
      this.w_NOMARC = LGNOMARC
      this.w_CODART = LGCODICE
      this.w_CODLIN = LGCODLIN
      this.w_DESCOD = LGDESCOD
      this.w_DESSUP = LGDESSUP
      this.w_UNMIS1 = LGUNMIS1
      this.w_UNMIS2 = LGUNMIS2
      * --- Select from KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CACODICE  from "+i_cTable+" KEY_ARTI ";
            +" where CACODART="+cp_ToStrODBC(this.w_CODART)+"";
            +" order by CACODICE";
             ,"_Curs_KEY_ARTI")
      else
        select CACODICE from (i_cTable);
         where CACODART=this.w_CODART;
         order by CACODICE;
          into cursor _Curs_KEY_ARTI
      endif
      if used('_Curs_KEY_ARTI')
        select _Curs_KEY_ARTI
        locate for 1=1
        do while not(eof())
        this.w_CODICE = _Curs_KEY_ARTI.CACODICE
        INSERT INTO CODRIC (LGNOMARC, LGCODICE, LGCODLIN, LGDESCOD, LGDESSUP, LGUNMIS1, LGUNMIS2) ;
        VALUES (this.w_NOMARC, this.w_CODICE, this.w_CODLIN, this.w_DESCOD, this.w_DESSUP, this.w_UNMIS1, this.w_UNMIS2)
          select _Curs_KEY_ARTI
          continue
        enddo
        use
      endif
      SELECT (this.w_CURSOR)
      ENDSCAN
      SELECT (this.w_CURSOR)
      use
      SELECT * FROM CODRIC INTO CURSOR (this.w_CURSOR) 
    endif
    if used("CODRIC")
      select CODRIC
      use
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
