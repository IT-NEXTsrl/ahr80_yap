* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bxx                                                        *
*              Trasf. note sam                                                 *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_117]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bxx",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bxx as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_IDNOTA = 0
  w_NOTA = space(0)
  w_ODBC = space(30)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TRASFORMA CURSORE CON STRUTTURA RECORD NOTE SAM
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      this.NC = SQLCONNECT(this.w_ODBC)
      SQLPREPARE(this.NC,"SELECT * FROM NOTE", "CURSNOTE")
      SQLEXEC(this.NC)
      wrcursor(this.w_CURSOR)
      select (this.w_CURSOR)
      go top
      if reccount() > 0
        SCAN 
        this.w_IDNOTA = IDNOTE
        this.w_NOTA = ""
        SELECT * FROM CURSNOTE WHERE ID=this.w_IDNOTA OR IDPADRE=this.w_IDNOTA ORDER BY RIGA INTO CURSOR CAMPNOTA
        SELECT CAMPNOTA
        GO TOP
        SCAN 
        this.w_NOTA = this.w_NOTA+CAMPNOTA.NOTA
        ENDSCAN
        select (this.w_CURSOR)
        REPLACE NOTA with this.w_NOTA
        ENDSCAN
      endif
      if used("CAMPNOTA")
        select CAMPNOTA
        use
      endif
      if used("CURSNOTE")
        select CURSNOTE
        use
      endif
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
