* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bpp                                                        *
*              Aggiorna cursore prestazioni                                    *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_198]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2012-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bpp",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bpp as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_ODBC = space(30)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CURSORI UTILIZZATI:
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      Select (this.w_CURSOR)
      GO TOP
       
 Select parcella as serparc,sum(de_importo_euro) as totparc from (this.w_CURSOR) group by parcella into cursor TMP_PARC 
 Select * from ( (this.w_CURSOR) inner join TMP_PARC on parcella=TMP_PARC.serparc) into cursor (this.w_CURSOR) 
 Use in TMP_PARC
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
