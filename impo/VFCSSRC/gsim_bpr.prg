* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bpr                                                        *
*              Imposta o aggiorna progressivo                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_40]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-11                                                      *
* Last revis.: 2010-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,p_PROG,p_KEY,p_VAL1,p_VAL2,p_VAL3,p_VAL4,p_VAL5
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bpr",oParentObject,m.p_PROG,m.p_KEY,m.p_VAL1,m.p_VAL2,m.p_VAL3,m.p_VAL4,m.p_VAL5)
return(i_retval)

define class tgsim_bpr as StdBatch
  * --- Local variables
  p_PROG = space(5)
  p_KEY = space(50)
  p_VAL1 = 0
  p_VAL2 = 0
  p_VAL3 = 0
  p_VAL4 = 0
  p_VAL5 = 0
  w_i = 0
  w_n = 0
  w_KEY = space(50)
  w_RESULT = 0
  w_PROGRE = 0
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVPRD = space(2)
  w_MVPRP = space(2)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_PNCODESE = space(4)
  w_PNANNDOC = space(4)
  w_PNANNPRO = space(4)
  w_PNCODUTE = 0
  w_PNPRG = space(10)
  w_PNPRD = space(10)
  w_PNPRP = space(10)
  w_PNNUMRER = 0
  w_PNALFDOC = space(10)
  w_PNALFPRO = space(10)
  w_PNNUMDOC = 0
  w_PNNUMPRO = 0
  w_MMNUMREG = 0
  w_MMCODESE = space(4)
  w_MMCODUTE = 0
  w_INCODESE = space(4)
  w_INNUMINV = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  MVM_MAST_idx=0
  PNT_MAST_idx=0
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Impostazione o aggiornamento progressivo
    *     
    *     - Utilizza la funzione standard cp_NextTableProg;
    *     - Permette di utilizzare variabili semplici w_XXXXXX non definite;
    *     - Ritorna il numero di progressivo assegnato;
    *     - Aggiorna il progressivo solo se il valore del progressivo corrente � maggiore del progressivo contenuto nella tabella progressivi;
    *     
    *     
    * --- Parametri
    *     ---------------
    * --- Il tipi dei valori pu� essere differente
    * --- Documenti di Vendita
    *     ----------------------------------
    *     i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC  - PRDOC
    * --- Documenti di Acquisto
    *     ----------------------------------
    *     i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC  - PRDOC  - w_MVPRD='DV' OR w_MVPRD='IV'
    *     i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST   - PRPRO -  w_MVANNPRO<>'    '
    * --- Ordini e Impegni
    *     ----------------------------------
    *     i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC   - PRDOC
    * --- Primanota
    *     ----------------------------------
    *     i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER  - PRPNT
    *     i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC  - PRDOC  - w_PNANNDOC<>'    '
    *     i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO - PRPRO - w_PNANNPRO<>'    '
    * --- Movimenti di Magazzino
    *     --------------------------------------
    *     i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG -  PRMVM
    * --- Inventari
    *     --------------
    *     i_codazi,w_INCODESE,w_INNUMINV - SEINV
    * --- Per progressivi documenti e ordini
    * --- Per progressivi primanota
    * --- Per progressivi magazzino
    * --- Per progressivi inventari
    this.w_n = 1
    this.w_KEY = this.p_KEY
    w_VAR = ""
    do while !empty(this.w_KEY)
      * --- Estrae nome variabile
      this.w_i = at(",",this.w_KEY)
      if this.w_i > 0
        w_VAR = left(this.w_KEY, this.w_i - 1)
        this.w_KEY = substr(this.w_KEY, this.w_i + 1)
      else
        w_VAR = this.w_KEY
        this.w_KEY = ""
      endif
      * --- Valorizza su variabile locale
      if lower(left(w_VAR,2))=="w_"
        w_cn = alltrim(str(this.w_n))
        this.&w_VAR = this.p_VAL&w_cn
        this.w_n = this.w_n + 1
      endif
    enddo
    * --- Valorizzo le variabile associate alla serie per Primanota e Documenti in modo che rispettino la nuova dimensione Char (10).
    this.w_MVALFDOC = Left(this.w_MVALFDOC+space(10),10)
    this.w_MVALFEST = Left(this.w_MVALFEST+space(10),10)
    this.w_PNALFPRO = Left(this.w_PNALFPRO+space(10),10)
    this.w_PNALFDOC = Left(this.w_PNALFDOC+space(10),10)
    if !empty(w_VAR)
      this.w_PROGRE = this.&w_VAR
      * --- Connessione
      do case
        case this.p_PROG = "PRDOC"
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        case this.p_PROG = "PRMVM"
          i_Conn=i_TableProp[this.MVM_MAST_IDX, 3]
        case this.p_PROG = "SEINV"
          i_Conn=i_TableProp[this.INVENTAR_IDX, 3]
        otherwise
          i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      endcase
      * --- Aggiorna progressivo
      cp_NextTableProg(this, i_Conn, this.p_PROG, this.p_KEY)
      wait clear
      * --- Ritorna:
      *     - se il progressivo era nullo il nuovo progressivo
      *     - se il progressivo era non nullo lo stesso progressivo
      this.w_RESULT = iif(empty(this.w_PROGRE),this.&w_VAR,this.w_PROGRE)
    endif
    i_retcode = 'stop'
    i_retval = (this.w_RESULT)
    return
  endproc


  proc Init(oParentObject,p_PROG,p_KEY,p_VAL1,p_VAL2,p_VAL3,p_VAL4,p_VAL5)
    this.p_PROG=p_PROG
    this.p_KEY=p_KEY
    this.p_VAL1=p_VAL1
    this.p_VAL2=p_VAL2
    this.p_VAL3=p_VAL3
    this.p_VAL4=p_VAL4
    this.p_VAL5=p_VAL5
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MVM_MAST'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='INVENTAR'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="p_PROG,p_KEY,p_VAL1,p_VAL2,p_VAL3,p_VAL4,p_VAL5"
endproc
