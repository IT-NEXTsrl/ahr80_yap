* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bqp                                                        *
*              Lettura prestazioni da Quick                                    *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_117]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-12                                                      *
* Last revis.: 2009-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bqp",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bqp as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_NOMTAB = space(8)
  w_NUM_FAT = space(5)
  w_DATA_FAT = ctod("  /  /  ")
  w_LINK_DBF = 0
  w_CONTROLLO = space(3)
  w_SPESE_GEN = space(1)
  w_RITENUTA = space(1)
  w_CASSA = space(1)
  w_CHECK = space(2)
  w_IVA = space(1)
  w_PERCENT_IVA = space(3)
  w_PERCENT_RIT = space(3)
  w_PERCENT_CA = space(3)
  w_NUM_NOM = space(10)
  w_TIPO = space(10)
  w_DATAPAG1 = ctod("  /  /  ")
  w_DATAPAG2 = ctod("  /  /  ")
  w_DATAPAG3 = ctod("  /  /  ")
  w_DATAPAG4 = ctod("  /  /  ")
  w_NUMEROPRATICA = 0
  w_NOTE_INCASSO = space(0)
  w_COMBO1 = 0
  w_COMBO2 = 0
  w_COMBO3 = 0
  w_COMBO4 = 0
  w_COMBO5 = 0
  w_IMPORTANZA = .f.
  w_LIRE = .f.
  w_PAGAM1 = 0
  w_PAGAM2 = 0
  w_PAGAM3 = 0
  w_PAGAM4 = 0
  w_TOT_SPE_IMP = 0
  w_TOT_SPE_ESE = 0
  w_TOT_DIR_ONO = 0
  w_TOTALE_FAT = 0
  w_ACCONTI_DIRITTI_ONORARI = 0
  w_ACCONTI_ESCLUSI = 0
  w_TIPO_ACCONTO = 0
  w_ACCONTI_SPESE_IMPONIB = 0
  w_ACCONTI_SG = .f.
  w_PERCENT_SPG = space(5)
  w_DESCRIZIO = space(0)
  w_SPE_IMP = 0
  w_SPE_ESE = 0
  w_DIRITTI = 0
  w_ONORARI = 0
  w_CODATT = space(4)
  w_Z_IMP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA PRESTAZIONI DA QUICK ORGANIZER
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      * --- Connessione a FATTURE.MDB (tramite odbc avente nome: 'QuickFat')
      this.NC = SQLCONNECT("QuickFat")
      CREATE CURSOR FATTUREQ (NUM_FAT C(5), DATA_FAT D(8), LINK_DBF N(4,0), CONTROLLO C(3), SPESE_GEN C(1), RITENUTA C(1), ;
      CASSA C(1), CHECKA C(2), IVA C(1), PERCENT_IVA C(3), PERCENT_RIT C(3), PERCENT_CA C(3), NUM_NOM C(10), TIPO C(10), ;
      DATAPAG1 D(8), DATAPAG2 D(8), DATAPAG3 D(8), DATAPAG4 D(8), NUMEROPRATICA N(4,0), NOTE_INCASSO M(10), COMBO1 N(4,0), COMBO2 N(4,0), ;
      COMBO3 N(4,0), COMBO4 N(4,0), COMBO5 N(4,0), IMPORTANZA L(1), LIRE L(1), PAGAM1 N(8,4), PAGAM2 N(8,4), PAGAM3 N(8,4), ;
      PAGAM4 N(8,4), TOT_SPE_IMP N(8,4), TOT_SPE_ESE N(8,4), TOT_DIR_ONO N(8,4), TOTALE_FAT N(8,4), ACCONTI_DIRITTI_ONORARI N(8,4), ACCONTI_ESCLUSI N(8,4), TIPO_ACCONTO N(4,0), ;
      ACCONTI_SPESE_IMPONIB N(8,4), ACCONTI_SG L(1), PERCENT_SPG C(5), DESCRIZIO M(10), CODATT C(4), Z_IMP N(8,4))
      select (this.w_CURSOR)
      go top
      if reccount() > 0 
        SCAN 
        * --- Per ogni fattura
        this.w_NUM_FAT = NVL(NUM_FAT,"")
        this.w_DATA_FAT = NVL(DATA_FAT,CTOD("  -  -  "))
        this.w_LINK_DBF = NVL(LINK_DBF,0)
        this.w_CONTROLLO = NVL(CONTROLLO,"")
        this.w_SPESE_GEN = NVL(SPESE_GEN,"")
        this.w_RITENUTA = NVL(RITENUTA,"")
        this.w_CASSA = NVL(CASSA,"")
        this.w_CHECK = NVL(CHECK,"")
        this.w_IVA = NVL(IVA,"")
        this.w_PERCENT_IVA = NVL(PERCENT_IVA,"")
        this.w_PERCENT_RIT = NVL(PERCENT_RIT,"")
        this.w_PERCENT_CA = NVL(PERCENT_CA,"")
        this.w_NUM_NOM = NVL(NUM_NOM,"")
        this.w_TIPO = NVL(TIPO,"")
        this.w_DATAPAG1 = NVL(DATAPAG1,CTOD("  -  -  "))
        this.w_DATAPAG2 = NVL(DATAPAG2,CTOD("  -  -  "))
        this.w_DATAPAG3 = NVL(DATAPAG3,CTOD("  -  -  "))
        this.w_DATAPAG4 = NVL(DATAPAG4,CTOD("  -  -  "))
        this.w_NUMEROPRATICA = NVL(NUMEROPRATICA,0)
        this.w_NOTE_INCASSO = NVL(NOTE_INCASSO,"")
        this.w_COMBO1 = NVL(COMBO1,0)
        this.w_COMBO2 = NVL(COMBO2,0)
        this.w_COMBO3 = NVL(COMBO3,0)
        this.w_COMBO4 = NVL(COMBO4,0)
        this.w_COMBO5 = NVL(COMBO5,0)
        this.w_IMPORTANZA = NVL(IMPORTANZA,.F.)
        this.w_LIRE = NVL(LIRE,.F.)
        this.w_PAGAM1 = NVL(PAGAM1,0)
        this.w_PAGAM2 = NVL(PAGAM2,0)
        this.w_PAGAM3 = NVL(PAGAM3,0)
        this.w_PAGAM4 = NVL(PAGAM4,0)
        this.w_TOT_SPE_IMP = NVL(TOT_SPE_IMP,0)
        this.w_TOT_SPE_ESE = NVL(TOT_SPE_ESE,0)
        this.w_TOT_DIR_ONO = NVL(TOT_DIR_ONO,0)
        this.w_TOTALE_FAT = NVL(TOTALE_FAT,0)
        this.w_ACCONTI_DIRITTI_ONORARI = NVL(ACCONTI_DIRITTI_ONORARI,0)
        this.w_ACCONTI_ESCLUSI = NVL(ACCONTI_ESCLUSI,0)
        this.w_TIPO_ACCONTO = NVL(TIPO_ACCONTO,0)
        this.w_ACCONTI_SPESE_IMPONIB = NVL(ACCONTI_SPESE_IMPONIB,0)
        this.w_ACCONTI_SG = NVL(ACCONTI_SG,.F.)
        this.w_PERCENT_SPG = NVL(PERCENT_SPG,"")
        this.w_NOMTAB = "TL"+ALLTRIM(STR(this.w_LINK_DBF))
        SQLPREPARE(this.NC,"SELECT *, 'ZSI' AS CODATT FROM ["+this.w_NOMTAB+"] WHERE SPE_IMP>0 UNION SELECT *, 'ZSA' AS CODATT FROM ["+this.w_NOMTAB+"] WHERE SPE_ESE>0 UNION SELECT *, 'ZDIR' AS CODATT FROM ["+this.w_NOMTAB+"] WHERE DIRITTI>0 OR (SPE_IMP=0 AND SPE_ESE=0 AND ONORARI=0) UNION SELECT *, 'ZONO' AS CODATT FROM ["+this.w_NOMTAB+"] WHERE ONORARI>0", "CURSFATT")
        SQLEXEC(this.NC)
        if used("CURSFATT")
          select CURSFATT
          go top
          if reccount() > 0 
            SCAN 
            * --- Per ogni riga di fattura
            this.w_DESCRIZIO = NVL(DESCRIZIO,"")
            this.w_SPE_IMP = NVL(SPE_IMP,0)
            this.w_SPE_ESE = NVL(SPE_ESE,0)
            this.w_DIRITTI = NVL(DIRITTI,0)
            this.w_ONORARI = NVL(ONORARI,0)
            this.w_CODATT = NVL(CODATT,"ZDIR")
            this.w_Z_IMP = IIF(this.w_CODATT="ZSI",this.w_SPE_IMP,IIF(this.w_CODATT="ZSA",this.w_SPE_ESE,IIF(this.w_CODATT="ZONO",this.w_ONORARI,this.w_DIRITTI)))
            INSERT INTO FATTUREQ (NUM_FAT, DATA_FAT, LINK_DBF, CONTROLLO, SPESE_GEN, RITENUTA, ;
            CASSA, CHECKA, IVA, PERCENT_IVA, PERCENT_RIT, PERCENT_CA, NUM_NOM, TIPO, ;
            DATAPAG1, DATAPAG2, DATAPAG3, DATAPAG4, NUMEROPRATICA, NOTE_INCASSO, COMBO1, COMBO2, ;
            COMBO3, COMBO4, COMBO5, IMPORTANZA, LIRE, PAGAM1, PAGAM2, PAGAM3, ;
            PAGAM4, TOT_SPE_IMP, TOT_SPE_ESE, TOT_DIR_ONO, TOTALE_FAT, ACCONTI_DIRITTI_ONORARI, ACCONTI_ESCLUSI, TIPO_ACCONTO, ;
            ACCONTI_SPESE_IMPONIB, ACCONTI_SG, PERCENT_SPG, DESCRIZIO, CODATT, Z_IMP) ;
            VALUES (this.w_NUM_FAT, this.w_DATA_FAT, this.w_LINK_DBF, this.w_CONTROLLO, this.w_SPESE_GEN, this.w_RITENUTA, ;
            this.w_CASSA, this.w_CHECK, this.w_IVA, this.w_PERCENT_IVA, this.w_PERCENT_RIT, this.w_PERCENT_CA, this.w_NUM_NOM, this.w_TIPO, ;
            this.w_DATAPAG1, this.w_DATAPAG2, this.w_DATAPAG3, this.w_DATAPAG4, this.w_NUMEROPRATICA, this.w_NOTE_INCASSO, this.w_COMBO1, this.w_COMBO2, ;
            this.w_COMBO3, this.w_COMBO4, this.w_COMBO5, this.w_IMPORTANZA, this.w_LIRE, this.w_PAGAM1, this.w_PAGAM2, this.w_PAGAM3, ;
            this.w_PAGAM4, this.w_TOT_SPE_IMP, this.w_TOT_SPE_ESE, this.w_TOT_DIR_ONO, this.w_TOTALE_FAT, this.w_ACCONTI_DIRITTI_ONORARI, this.w_ACCONTI_ESCLUSI, this.w_TIPO_ACCONTO, ;
            this.w_ACCONTI_SPESE_IMPONIB, this.w_ACCONTI_SG, this.w_PERCENT_SPG, this.w_DESCRIZIO, this.w_CODATT, this.w_Z_IMP)
            select CURSFATT
            ENDSCAN
          endif
          select CURSFATT
          use
        endif
        select (this.w_CURSOR)
        ENDSCAN
      endif
      SELECT (this.w_CURSOR)
      use
      * --- Risultato in w_CURSOR
      SELECT * FROM FATTUREQ INTO CURSOR (this.w_CURSOR) ORDER BY 2, 3
      if used("FATTUREQ")
        select FATTUREQ
        use
      endif
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
