* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bxc                                                        *
*              Import Dati F24                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-20                                                      *
* Last revis.: 2013-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bxc",oParentObject)
return(i_retval)

define class tgsim_bxc as StdBatch
  * --- Local variables
  w_CAUSAINPS = space(5)
  w_INDICEINPS = space(2)
  * --- WorkFile variables
  COD_TRIB_idx=0
  CAU_AEN_idx=0
  CAU_INPS_idx=0
  COD_PREV_idx=0
  ENTI_COM_idx=0
  SE_INAIL_idx=0
  SED_INPS_idx=0
  ENTI_LOC_idx=0
  REG_PROV_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla routine GSIM_BAC per l'importazione delle tabelle F24
    * --- Modello di segnalazione
    * --- Dettaglio segnalazione
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Aggiornamento archivi
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_036B1E30
    bErr_036B1E30=bTrsErr
    this.Try_036B1E30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_036B1E30
    * --- End
    i_rows = 0
    * --- Try
    local bErr_036B1CB0
    bErr_036B1CB0=bTrsErr
    this.Try_036B1CB0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      this.oParentObject.w_Corretto = .F.
    endif
    bTrsErr=bTrsErr or bErr_036B1CB0
    * --- End
    if i_rows=0
      this.oParentObject.w_Corretto = .F.
    endif
    if this.oParentObject.w_Corretto .and. this.oParentObject.w_ResoDett <> ah_Msgformat("<SENZA RESOCONTO>")
      * --- Scrittura resoconto
      this.oParentObject.w_ResoMode = "SCRITTURA"
      * --- Aggiornamento archivio resoconti
      this.oParentObject.Pag4()
    endif
  endproc
  proc Try_036B1E30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "CU"
        i_rows = 0
        * --- Codici Tributo
        w_TRCODICE = left( ltrim(w_TRCODICE), 5)
        * --- Insert into COD_TRIB
        i_nConn=i_TableProp[this.COD_TRIB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_TRIB_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TRCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TRCODICE),'COD_TRIB','TRCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TRCODICE',w_TRCODICE)
          insert into (i_cTable) (TRCODICE &i_ccchkf. );
             values (;
               w_TRCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CK"
        i_rows = 0
        * --- Causali Altri Enti
        w_AECODENT = left( ltrim(w_AECODENT), 5)
        w_AECAUSEN = left( ltrim(w_AECAUSEN), 5)
        * --- Insert into CAU_AEN
        i_nConn=i_TableProp[this.CAU_AEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_AEN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_AEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"AECAUSEN"+",AECODENT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_AECAUSEN),'CAU_AEN','AECAUSEN');
          +","+cp_NullLink(cp_ToStrODBC(w_AECODENT),'CAU_AEN','AECODENT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'AECAUSEN',w_AECAUSEN,'AECODENT',w_AECODENT)
          insert into (i_cTable) (AECAUSEN,AECODENT &i_ccchkf. );
             values (;
               w_AECAUSEN;
               ,w_AECODENT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CJ"
        i_rows = 0
        * --- Causali INPS
        * --- Dato che nelle Causali INPS ci possono essere pi� codici nello stesso record
        *     (perch� hanno i restanti dati uguali), li dobbiamo dividere
        w_CS_CAUSA = w_CS_CAUSA
        this.w_INDICEINPS = 1
        this.w_CAUSAINPS = ALLTRIM(GETWORDNUM(ALLTRIM(w_CS_CAUSA),this.w_INDICEINPS,"-"))
        do while not Empty(this.w_CAUSAINPS)
          * --- Try
          local bErr_036B28E0
          bErr_036B28E0=bTrsErr
          this.Try_036B28E0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_036B28E0
          * --- End
          this.w_INDICEINPS = this.w_INDICEINPS + 1
          this.w_CAUSAINPS = ALLTRIM(GETWORDNUM(ALLTRIM(w_CS_CAUSA),this.w_INDICEINPS,"-"))
        enddo
      case this.oParentObject.w_Destinaz = "CV"
        i_rows = 0
        * --- Codice Enti Previdenziali
        w_CPCODICE = left( ltrim(w_CPCODICE), 5)
        * --- Insert into COD_PREV
        i_nConn=i_TableProp[this.COD_PREV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_PREV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_PREV_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CPCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CPCODICE),'COD_PREV','CPCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CPCODICE',w_CPCODICE)
          insert into (i_cTable) (CPCODICE &i_ccchkf. );
             values (;
               w_CPCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "NC"
        i_rows = 0
        * --- Codici Enti/Comuni
        w_ECCODICE = left( ltrim(w_ECCODICE), 5)
        * --- Insert into ENTI_COM
        i_nConn=i_TableProp[this.ENTI_COM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ENTI_COM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ENTI_COM_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ECCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ECCODICE),'ENTI_COM','ECCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ECCODICE',w_ECCODICE)
          insert into (i_cTable) (ECCODICE &i_ccchkf. );
             values (;
               w_ECCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SI"
        i_rows = 0
        * --- Sedi INAIL
        w_SICODICE = left( ltrim(w_SICODICE), 5)
        * --- Insert into SE_INAIL
        i_nConn=i_TableProp[this.SE_INAIL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SE_INAIL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SICODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_SICODICE),'SE_INAIL','SICODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SICODICE',w_SICODICE)
          insert into (i_cTable) (SICODICE &i_ccchkf. );
             values (;
               w_SICODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SN"
        i_rows = 0
        * --- Sedi INPS
        w_PSCODICE = left( ltrim(w_PSCODICE), 5)
        * --- Insert into SED_INPS
        i_nConn=i_TableProp[this.SED_INPS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SED_INPS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SED_INPS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PSCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_PSCODICE),'SED_INPS','PSCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PSCODICE',w_PSCODICE)
          insert into (i_cTable) (PSCODICE &i_ccchkf. );
             values (;
               w_PSCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="EL"
        i_rows = 0
        * --- Codici Enti Locali
        w_ELCODICE = left( ltrim(w_ELCODICE), 2)
        * --- Insert into ENTI_LOC
        i_nConn=i_TableProp[this.ENTI_LOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ENTI_LOC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ENTI_LOC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ELCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ELCODICE),'ENTI_LOC','ELCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ELCODICE',w_ELCODICE)
          insert into (i_cTable) (ELCODICE &i_ccchkf. );
             values (;
               w_ELCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="RG"
        i_rows = 0
        * --- Codici Regioni e Province Autonome
        w_RPCODICE = left( ltrim(w_RPCODICE), 2)
        * --- Insert into REG_PROV
        i_nConn=i_TableProp[this.REG_PROV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REG_PROV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_PROV_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RPCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_RPCODICE),'REG_PROV','RPCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RPCODICE',w_RPCODICE)
          insert into (i_cTable) (RPCODICE &i_ccchkf. );
             values (;
               w_RPCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
    endcase
    return
  proc Try_036B1CB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "CU"
        * --- Codici Tributo
        this.oParentObject.w_ResoDett = trim(w_TRCODICE)
        * --- Write into COD_TRIB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COD_TRIB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRDESCRI ="+cp_NullLink(cp_ToStrODBC(w_TRDESCRI),'COD_TRIB','TRDESCRI');
          +",TRTIPTRI ="+cp_NullLink(cp_ToStrODBC(w_TRTIPTRI),'COD_TRIB','TRTIPTRI');
          +",TRFLMERF ="+cp_NullLink(cp_ToStrODBC(w_TRFLMERF),'COD_TRIB','TRFLMERF');
          +",TRFLCODA ="+cp_NullLink(cp_ToStrODBC(w_TRFLCODA),'COD_TRIB','TRFLCODA');
              +i_ccchkf ;
          +" where ";
              +"TRCODICE = "+cp_ToStrODBC(w_TRCODICE);
                 )
        else
          update (i_cTable) set;
              TRDESCRI = w_TRDESCRI;
              ,TRTIPTRI = w_TRTIPTRI;
              ,TRFLMERF = w_TRFLMERF;
              ,TRFLCODA = w_TRFLCODA;
              &i_ccchkf. ;
           where;
              TRCODICE = w_TRCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CK"
        * --- Causali Altri Enti
        this.oParentObject.w_ResoDett = trim(w_AECODENT+w_AECAUSEN)
        * --- Write into CAU_AEN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAU_AEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_AEN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_AEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AEDESENT ="+cp_NullLink(cp_ToStrODBC(w_AEDESENT),'CAU_AEN','AEDESENT');
          +",AEPERINI ="+cp_NullLink(cp_ToStrODBC(w_AEPERINI),'CAU_AEN','AEPERINI');
          +",AEPERFIN ="+cp_NullLink(cp_ToStrODBC(w_AEPERFIN),'CAU_AEN','AEPERFIN');
              +i_ccchkf ;
          +" where ";
              +"AECODENT = "+cp_ToStrODBC(w_AECODENT);
              +" and AECAUSEN = "+cp_ToStrODBC(w_AECAUSEN);
                 )
        else
          update (i_cTable) set;
              AEDESENT = w_AEDESENT;
              ,AEPERINI = w_AEPERINI;
              ,AEPERFIN = w_AEPERFIN;
              &i_ccchkf. ;
           where;
              AECODENT = w_AECODENT;
              and AECAUSEN = w_AECAUSEN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CJ"
        * --- Causali INPS
        this.oParentObject.w_ResoDett = trim(w_CS_CAUSA)
        this.w_INDICEINPS = 1
        this.w_CAUSAINPS = ALLTRIM(GETWORDNUM(ALLTRIM(w_CS_CAUSA),this.w_INDICEINPS,"-"))
        do while not empty(this.w_CAUSAINPS)
          * --- Write into CAU_INPS
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CAU_INPS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_INPS_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_INPS_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CSDESCRI),'CAU_INPS','CSDESCRI');
            +",CSPERINI ="+cp_NullLink(cp_ToStrODBC(w_CSPERINI),'CAU_INPS','CSPERINI');
            +",CSPERFIN ="+cp_NullLink(cp_ToStrODBC(w_CSPERFIN),'CAU_INPS','CSPERFIN');
                +i_ccchkf ;
            +" where ";
                +"CS_CAUSA = "+cp_ToStrODBC(this.w_CAUSAINPS);
                   )
          else
            update (i_cTable) set;
                CSDESCRI = w_CSDESCRI;
                ,CSPERINI = w_CSPERINI;
                ,CSPERFIN = w_CSPERFIN;
                &i_ccchkf. ;
             where;
                CS_CAUSA = this.w_CAUSAINPS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_INDICEINPS = this.w_INDICEINPS + 1
          this.w_CAUSAINPS = ALLTRIM(GETWORDNUM(ALLTRIM(w_CS_CAUSA),this.w_INDICEINPS,"-"))
        enddo
      case this.oParentObject.w_Destinaz = "CV"
        * --- Codice Enti Previdenziali
        this.oParentObject.w_ResoDett = trim(w_CPCODICE)
        * --- Write into COD_PREV
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COD_PREV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_PREV_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_PREV_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CPDESCRI),'COD_PREV','CPDESCRI');
              +i_ccchkf ;
          +" where ";
              +"CPCODICE = "+cp_ToStrODBC(w_CPCODICE);
                 )
        else
          update (i_cTable) set;
              CPDESCRI = w_CPDESCRI;
              &i_ccchkf. ;
           where;
              CPCODICE = w_CPCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "NC"
        * --- Codici Enti/Comuni
        this.oParentObject.w_ResoDett = trim(w_ECCODICE)
        * --- Write into ENTI_COM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ENTI_COM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ENTI_COM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ENTI_COM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ECTERRIT ="+cp_NullLink(cp_ToStrODBC(w_ECTERRIT),'ENTI_COM','ECTERRIT');
              +i_ccchkf ;
          +" where ";
              +"ECCODICE = "+cp_ToStrODBC(w_ECCODICE);
                 )
        else
          update (i_cTable) set;
              ECTERRIT = w_ECTERRIT;
              &i_ccchkf. ;
           where;
              ECCODICE = w_ECCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SI"
        * --- Sedi INAIL
        this.oParentObject.w_ResoDett = trim(w_SICODICE)
        * --- Write into SE_INAIL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SE_INAIL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SE_INAIL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SI__NOME ="+cp_NullLink(cp_ToStrODBC(w_SI__NOME),'SE_INAIL','SI__NOME');
          +",SI_INDIR ="+cp_NullLink(cp_ToStrODBC(w_SI_INDIR),'SE_INAIL','SI_INDIR');
          +",SI__CAP ="+cp_NullLink(cp_ToStrODBC(w_SI__CAP),'SE_INAIL','SI__CAP');
          +",SI_LOCAL ="+cp_NullLink(cp_ToStrODBC(w_SI_LOCAL),'SE_INAIL','SI_LOCAL');
          +",SI__PROV ="+cp_NullLink(cp_ToStrODBC(w_SI__PROV),'SE_INAIL','SI__PROV');
              +i_ccchkf ;
          +" where ";
              +"SICODICE = "+cp_ToStrODBC(w_SICODICE);
                 )
        else
          update (i_cTable) set;
              SI__NOME = w_SI__NOME;
              ,SI_INDIR = w_SI_INDIR;
              ,SI__CAP = w_SI__CAP;
              ,SI_LOCAL = w_SI_LOCAL;
              ,SI__PROV = w_SI__PROV;
              &i_ccchkf. ;
           where;
              SICODICE = w_SICODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SN"
        * --- Sedi INPS
        this.oParentObject.w_ResoDett = trim(w_PSCODICE)
        * --- Write into SED_INPS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SED_INPS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SED_INPS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SED_INPS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PSDESCRI ="+cp_NullLink(cp_ToStrODBC(w_PSDESCRI),'SED_INPS','PSDESCRI');
              +i_ccchkf ;
          +" where ";
              +"PSCODICE = "+cp_ToStrODBC(w_PSCODICE);
                 )
        else
          update (i_cTable) set;
              PSDESCRI = w_PSDESCRI;
              &i_ccchkf. ;
           where;
              PSCODICE = w_PSCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="EL"
        * --- Codici Enti Locali
        this.oParentObject.w_ResoDett = trim(w_ELCODICE)
        * --- Write into ENTI_LOC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ENTI_LOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ENTI_LOC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ENTI_LOC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELTERRIT ="+cp_NullLink(cp_ToStrODBC(w_ELTERRIT),'ENTI_LOC','ELTERRIT');
              +i_ccchkf ;
          +" where ";
              +"ELCODICE = "+cp_ToStrODBC(w_ELCODICE);
                 )
        else
          update (i_cTable) set;
              ELTERRIT = w_ELTERRIT;
              &i_ccchkf. ;
           where;
              ELCODICE = w_ELCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="RG"
        * --- Codici Regioni e Province Autonome
        this.oParentObject.w_ResoDett = trim(w_RPCODICE)
        * --- Write into REG_PROV
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.REG_PROV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.REG_PROV_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_PROV_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RPDESCRI ="+cp_NullLink(cp_ToStrODBC(w_RPDESCRI),'REG_PROV','RPDESCRI');
              +i_ccchkf ;
          +" where ";
              +"RPCODICE = "+cp_ToStrODBC(w_RPCODICE);
                 )
        else
          update (i_cTable) set;
              RPDESCRI = w_RPDESCRI;
              &i_ccchkf. ;
           where;
              RPCODICE = w_RPCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return
  proc Try_036B28E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_INPS
    i_nConn=i_TableProp[this.CAU_INPS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_INPS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_INPS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CS_CAUSA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CAUSAINPS),'CAU_INPS','CS_CAUSA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CS_CAUSA',this.w_CAUSAINPS)
      insert into (i_cTable) (CS_CAUSA &i_ccchkf. );
         values (;
           this.w_CAUSAINPS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Impossibile inserire!'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='COD_TRIB'
    this.cWorkTables[2]='CAU_AEN'
    this.cWorkTables[3]='CAU_INPS'
    this.cWorkTables[4]='COD_PREV'
    this.cWorkTables[5]='ENTI_COM'
    this.cWorkTables[6]='SE_INAIL'
    this.cWorkTables[7]='SED_INPS'
    this.cWorkTables[8]='ENTI_LOC'
    this.cWorkTables[9]='REG_PROV'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
