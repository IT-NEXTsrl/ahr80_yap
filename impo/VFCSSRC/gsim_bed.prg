* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bed                                                        *
*              Exp/imp tabelle import DBF                                      *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_379]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bed",oParentObject)
return(i_retval)

define class tgsim_bed as StdBatch
  * --- Local variables
  Messaggio = space(254)
  w_SCELTA = 0
  response = space(1)
  w_nt = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableName = space(50)
  FraseSQLPresente = .f.
  FraseSQL = space(1)
  w_nField = 0
  w_ARTABDET = space(15)
  * --- WorkFile variables
  IMPOARCH_idx=0
  IMPORTAZ_idx=0
  PREDEFIN_idx=0
  PREDEIMP_idx=0
  SRCMODBC_idx=0
  SRC_ODBC_idx=0
  TRACCIAT_idx=0
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export/Import Dati Tracciati, Destinazioni Valori Predefiniti e Trascodifiche DBF
    * --- Variabili maschera
    * --- Tipo operazione da effettuare (I=Import; E=Export)
    * --- Nomi e path dei files DBF
    * --- Campi di attivazione Import/Export
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    this.w_nt = 0
    * --- Numero connessione
    this.w_nConn = 0
    * --- Nome fisico tabella
    this.w_cTable = ""
    * --- Nome Logico Tabella
    this.w_cTableName = ""
    if this.oParentObject.w_RADSELIE1 = "E"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    wait clear
    if this.oParentObject.w_RADSELIE1="I"
      ah_ErrorMsg("Procedura di importazione terminata con successo","i","")
    else
      ah_ErrorMsg("Procedura di esportazione terminata con successo","i","")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Scrittura files dbf
    this.w_SCELTA = 1
    * --- Database IMPOARCH - Archivi di destinazione
    if this.oParentObject.w_SELEAD1 = "AD"
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      ah_Msg("Export archivi di destinazione...",.T.)
      this.w_cTableName = "IMPOARCH"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export Destinazioni (IMPOARCH.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Databases SRC_ODBC,SRCMODBC Sorgenti dati ODBC
    if this.oParentObject.w_SELESOR = "SO"
      Filedbf = alltrim(this.oParentObject.w_DBF5)
      ah_Msg("Sorgenti dati ODBC...",.T.)
      this.w_cTableName = "SRC_ODBC"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export trascodifiche (SRC_ODBC.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Fine esportazione di SRC_ODBC
      Filedbf = alltrim(this.oParentObject.w_DBF51)
      ah_Msg("Sorgenti dati ODBC...",.T.)
      this.w_cTableName = "SRCMODBC"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export trascodifiche SRCMODBC.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Databases IMPORT,TRACCIAT - Archivi dei tracciati records
    if this.oParentObject.w_SELETR1 = "TR"
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      ah_Msg("Export tracciati record...",.T.)
      this.w_cTableName = "IMPORTAZ"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export tracciati (IMPORT.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Fine esportazione di IMPORTAZ
      Filedbf = alltrim(this.oParentObject.w_DBF11) 
      ah_Msg("Export tracciati record...",.T.)
      this.w_cTableName = "TRACCIAT"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export tracciati (TRACCIAT.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Databases PREDEFIN,PREDEIMP - Valori predefiniti
    if this.oParentObject.w_SELEVP1 = "VP"
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      ah_Msg("Export valori predefiniti...",.T.)
      this.w_cTableName = "PREDEFIN"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export Valori predefiniti (PREDEFIN.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Fine esportazione di PREDEFIN
      Filedbf = alltrim(this.oParentObject.w_DBF31)
      ah_Msg("Export valori predefiniti...",.T.)
      this.w_cTableName = "PREDEIMP"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export Valori predefiniti (PREDEIMP.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- Databases TRASCODI,TRASCIMP - Trascodifiche
    if this.oParentObject.w_SELETS1 = "TS"
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      ah_Msg("Export trascodifiche...",.T.)
      this.w_cTableName = "TRASCODI"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("CursDBF")
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Export trascodifiche (TRASCODI.DBF)
        if this.response="S"
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Fine esportazione di TRASCODI
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- Lettura files DBF
    * --- ARCHIVI DI DESTINAZIONE
    if this.oParentObject.w_SELEAD1 = "AD"
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      ah_Msg("Import archivi di destinazione...",.T.)
      * --- Import Destinazioni in tabella IMPOARCH
      if file(Filedbf)
        use (Filedbf) alias CursDBF
        if reccount("CursDBF")<>0
          go top
          scan for 1=1
          * --- Try
          local bErr_047F7318
          bErr_047F7318=bTrsErr
          this.Try_047F7318()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_047F7318
          * --- End
          * --- Per importazione da tracciati precedenti alla release 1.2
          this.w_ARTABDET = iif(type("CursDBF.ARTABDET")="C",CursDBF.ARTABDET,"")
          * --- Scrittura
          * --- Write into IMPOARCH
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.IMPOARCH_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.IMPOARCH_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARDESCRI ="+cp_NullLink(cp_ToStrODBC(CursDBF.ARDESCRI),'IMPOARCH','ARDESCRI');
            +",ARTABELL ="+cp_NullLink(cp_ToStrODBC(CursDBF.ARTABELL),'IMPOARCH','ARTABELL');
            +",ARROUTIN ="+cp_NullLink(cp_ToStrODBC(CursDBF.ARROUTIN),'IMPOARCH','ARROUTIN');
            +",ARTABDET ="+cp_NullLink(cp_ToStrODBC(this.w_ARTABDET),'IMPOARCH','ARTABDET');
                +i_ccchkf ;
            +" where ";
                +"ARCODICE = "+cp_ToStrODBC(CursDBF.ARCODICE);
                   )
          else
            update (i_cTable) set;
                ARDESCRI = CursDBF.ARDESCRI;
                ,ARTABELL = CursDBF.ARTABELL;
                ,ARROUTIN = CursDBF.ARROUTIN;
                ,ARTABDET = this.w_ARTABDET;
                &i_ccchkf. ;
             where;
                ARCODICE = CursDBF.ARCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          endscan
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    * --- SORGENTI ODBC
    if this.oParentObject.w_SELESOR = "SO"
      Filedbf = alltrim(this.oParentObject.w_DBF51)
      ah_Msg("Import sorgenti ODBC...",.T.)
      * --- Import sorgenti ODBC in tabella SRCMODBC
      if file(Filedbf)
        use (Filedbf) alias CursDBF
        if reccount("CursDBF")<>0
          go top
          scan for 1=1
          if Type("CursDBF.FLGFILE")= "C" and CursDBF.FLGFILE = "S"
            * --- Try
            local bErr_047EF368
            bErr_047EF368=bTrsErr
            this.Try_047EF368()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Write into SRCMODBC
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SRCMODBC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SRCMODBC_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SODESCR ="+cp_NullLink(cp_ToStrODBC(CursDBF.SODESCR),'SRCMODBC','SODESCR');
                +",SOSQLSRC ="+cp_NullLink(cp_ToStrODBC(CursDBF.SOSQLSRC),'SRCMODBC','SOSQLSRC');
                +",SOROUTRA ="+cp_NullLink(cp_ToStrODBC(CursDBF.SOROUTRA),'SRCMODBC','SOROUTRA');
                +",FLGFILE ="+cp_NullLink(cp_ToStrODBC(CursDBF.FLGFILE),'SRCMODBC','FLGFILE');
                +",FILEEXC ="+cp_NullLink(cp_ToStrODBC(CursDBF.FILEEXC),'SRCMODBC','FILEEXC');
                +",FOGLEXC ="+cp_NullLink(cp_ToStrODBC(CursDBF.FOGLEXC),'SRCMODBC','FOGLEXC');
                    +i_ccchkf ;
                +" where ";
                    +"SOCODICE = "+cp_ToStrODBC(CursDBF.SOCODICE);
                       )
              else
                update (i_cTable) set;
                    SODESCR = CursDBF.SODESCR;
                    ,SOSQLSRC = CursDBF.SOSQLSRC;
                    ,SOROUTRA = CursDBF.SOROUTRA;
                    ,FLGFILE = CursDBF.FLGFILE;
                    ,FILEEXC = CursDBF.FILEEXC;
                    ,FOGLEXC = CursDBF.FOGLEXC;
                    &i_ccchkf. ;
                 where;
                    SOCODICE = CursDBF.SOCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_047EF368
            * --- End
          else
            * --- Try
            local bErr_047F15B8
            bErr_047F15B8=bTrsErr
            this.Try_047F15B8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Write into SRCMODBC
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SRCMODBC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SRCMODBC_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SODESCR ="+cp_NullLink(cp_ToStrODBC(CursDBF.SODESCR),'SRCMODBC','SODESCR');
                +",SOSQLSRC ="+cp_NullLink(cp_ToStrODBC(CursDBF.SOSQLSRC),'SRCMODBC','SOSQLSRC');
                +",SOROUTRA ="+cp_NullLink(cp_ToStrODBC(CursDBF.SOROUTRA),'SRCMODBC','SOROUTRA');
                    +i_ccchkf ;
                +" where ";
                    +"SOCODICE = "+cp_ToStrODBC(CursDBF.SOCODICE);
                       )
              else
                update (i_cTable) set;
                    SODESCR = CursDBF.SODESCR;
                    ,SOSQLSRC = CursDBF.SOSQLSRC;
                    ,SOROUTRA = CursDBF.SOROUTRA;
                    &i_ccchkf. ;
                 where;
                    SOCODICE = CursDBF.SOCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            bTrsErr=bTrsErr or bErr_047F15B8
            * --- End
          endif
          endscan
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Fine import sorgenti ODBC in tabella SRCMODBC
          Filedbf = alltrim(this.oParentObject.w_DBF5)
          ah_Msg("Import sorgenti ODBC...",.T.)
          * --- Import sorgenti ODBC in tabella SRC_ODBC
          if file(Filedbf)
            use (Filedbf) alias CursDBF
            if reccount("CursDBF")<>0
              go top
              scan for 1=1
              * --- Try
              local bErr_047CE7E0
              bErr_047CE7E0=bTrsErr
              this.Try_047CE7E0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                * --- Write into SRC_ODBC
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SRC_ODBC_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SRC_ODBC_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SRC_ODBC_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CPROWNUM ="+cp_NullLink(cp_ToStrODBC(CursDBF.CPROWNUM),'SRC_ODBC','CPROWNUM');
                  +",FLTYPE ="+cp_NullLink(cp_ToStrODBC(CursDBF.FLTYPE),'SRC_ODBC','FLTYPE');
                  +",FLLENGHT ="+cp_NullLink(cp_ToStrODBC(CursDBF.FLLENGHT),'SRC_ODBC','FLLENGHT');
                  +",FLDECIMA ="+cp_NullLink(cp_ToStrODBC(CursDBF.FLDECIMA),'SRC_ODBC','FLDECIMA');
                  +",FLCOMMEN ="+cp_NullLink(cp_ToStrODBC(CursDBF.FLCOMMEN),'SRC_ODBC','FLCOMMEN');
                      +i_ccchkf ;
                  +" where ";
                      +"SOCODICE = "+cp_ToStrODBC(CursDBF.SOCODICE);
                      +" and FLNAME = "+cp_ToStrODBC(CursDBF.FLNAME);
                         )
                else
                  update (i_cTable) set;
                      CPROWNUM = CursDBF.CPROWNUM;
                      ,FLTYPE = CursDBF.FLTYPE;
                      ,FLLENGHT = CursDBF.FLLENGHT;
                      ,FLDECIMA = CursDBF.FLDECIMA;
                      ,FLCOMMEN = CursDBF.FLCOMMEN;
                      &i_ccchkf. ;
                   where;
                      SOCODICE = CursDBF.SOCODICE;
                      and FLNAME = CursDBF.FLNAME;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              bTrsErr=bTrsErr or bErr_047CE7E0
              * --- End
              endscan
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      endif
    endif
    * --- TRACCIATI RECORDS
    if this.oParentObject.w_SELETR1 = "TR"
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      ah_Msg("Import tracciati record...",.T.)
      * --- Import in tabella IMPORTAZ
      if file(Filedbf)
        use (Filedbf) alias CursDBF
        if reccount("CursDBF")<>0
          go top
          scan for 1=1
          * --- Try
          local bErr_047AB218
          bErr_047AB218=bTrsErr
          this.Try_047AB218()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if type("CursDBF.IM_EXCEL")<>"U"
              * --- Write into IMPORTAZ
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.IMPORTAZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IMSEQUEN ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
                +",IMTRASCO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRASCO),'IMPORTAZ','IMTRASCO');
                +",IMTRAMUL ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
                +",IMAGGPAR ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
                +",IMAGGANA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGANA),'IMPORTAZ','IMAGGANA');
                +",IMAGGIVA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
                +",IMAGGSAL ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
                +",IMAGGNUR ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
                +",IMCONIND ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONIND),'IMPORTAZ','IMCONIND');
                +",IMFILTRO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMFILTRO),'IMPORTAZ','IMFILTRO');
                +",IMCHIMOV ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
                +",IMANNOTA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMANNOTA),'IMPORTAZ','IMANNOTA');
                +",IMPARIVA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMPARIVA),'IMPORTAZ','IMPARIVA');
                +",IMAZZERA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAZZERA),'IMPORTAZ','IMAZZERA');
                +",IMRESOCO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMRESOCO),'IMPORTAZ','IMRESOCO');
                +",IMCONTRO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONTRO),'IMPORTAZ','IMCONTRO');
                +",IM_EXCEL ="+cp_NullLink(cp_ToStrODBC(CursDBF.IM_EXCEL),'IMPORTAZ','IM_EXCEL');
                +",IMTIPDBF ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPDBF),'IMPORTAZ','IMTIPDBF');
                +",IMROUTIN ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMROUTIN),'IMPORTAZ','IMROUTIN');
                +",IM_ODBC ="+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ODBC),'IMPORTAZ','IM_ODBC');
                    +i_ccchkf ;
                +" where ";
                    +"IMCODICE = "+cp_ToStrODBC(CursDBF.IMCODICE);
                    +" and IM_ASCII = "+cp_ToStrODBC(CursDBF.IM_ASCII);
                    +" and IMDESTIN = "+cp_ToStrODBC(CursDBF.IMDESTIN);
                    +" and IMTIPSRC = "+cp_ToStrODBC(CursDBF.IMTIPSRC);
                       )
              else
                update (i_cTable) set;
                    IMSEQUEN = CursDBF.IMSEQUEN;
                    ,IMTRASCO = CursDBF.IMTRASCO;
                    ,IMTRAMUL = CursDBF.IMTRAMUL;
                    ,IMAGGPAR = CursDBF.IMAGGPAR;
                    ,IMAGGANA = CursDBF.IMAGGANA;
                    ,IMAGGIVA = CursDBF.IMAGGIVA;
                    ,IMAGGSAL = CursDBF.IMAGGSAL;
                    ,IMAGGNUR = CursDBF.IMAGGNUR;
                    ,IMCONIND = CursDBF.IMCONIND;
                    ,IMFILTRO = CursDBF.IMFILTRO;
                    ,IMCHIMOV = CursDBF.IMCHIMOV;
                    ,IMANNOTA = CursDBF.IMANNOTA;
                    ,IMPARIVA = CursDBF.IMPARIVA;
                    ,IMAZZERA = CursDBF.IMAZZERA;
                    ,IMRESOCO = CursDBF.IMRESOCO;
                    ,IMCONTRO = CursDBF.IMCONTRO;
                    ,IM_EXCEL = CursDBF.IM_EXCEL;
                    ,IMTIPDBF = CursDBF.IMTIPDBF;
                    ,IMROUTIN = CursDBF.IMROUTIN;
                    ,IM_ODBC = CursDBF.IM_ODBC;
                    &i_ccchkf. ;
                 where;
                    IMCODICE = CursDBF.IMCODICE;
                    and IM_ASCII = CursDBF.IM_ASCII;
                    and IMDESTIN = CursDBF.IMDESTIN;
                    and IMTIPSRC = CursDBF.IMTIPSRC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Write into IMPORTAZ
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.IMPORTAZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IMSEQUEN ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
                +",IMTRASCO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRASCO),'IMPORTAZ','IMTRASCO');
                +",IMTRAMUL ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
                +",IMAGGPAR ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
                +",IMAGGANA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGANA),'IMPORTAZ','IMAGGANA');
                +",IMAGGIVA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
                +",IMAGGSAL ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
                +",IMAGGNUR ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
                +",IMCONIND ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONIND),'IMPORTAZ','IMCONIND');
                +",IMFILTRO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMFILTRO),'IMPORTAZ','IMFILTRO');
                +",IMCHIMOV ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
                +",IMANNOTA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMANNOTA),'IMPORTAZ','IMANNOTA');
                +",IMPARIVA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMPARIVA),'IMPORTAZ','IMPARIVA');
                +",IMAZZERA ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMAZZERA),'IMPORTAZ','IMAZZERA');
                +",IMRESOCO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMRESOCO),'IMPORTAZ','IMRESOCO');
                +",IMCONTRO ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONTRO),'IMPORTAZ','IMCONTRO');
                +",IM_ODBC ="+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ODBC),'IMPORTAZ','IM_ODBC');
                +",IMTIPDBF ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPDBF),'IMPORTAZ','IMTIPDBF');
                +",IMROUTIN ="+cp_NullLink(cp_ToStrODBC(CursDBF.IMROUTIN),'IMPORTAZ','IMROUTIN');
                    +i_ccchkf ;
                +" where ";
                    +"IMCODICE = "+cp_ToStrODBC(CursDBF.IMCODICE);
                    +" and IM_ASCII = "+cp_ToStrODBC(CursDBF.IM_ASCII);
                    +" and IMDESTIN = "+cp_ToStrODBC(CursDBF.IMDESTIN);
                    +" and IMTIPSRC = "+cp_ToStrODBC(CursDBF.IMTIPSRC);
                       )
              else
                update (i_cTable) set;
                    IMSEQUEN = CursDBF.IMSEQUEN;
                    ,IMTRASCO = CursDBF.IMTRASCO;
                    ,IMTRAMUL = CursDBF.IMTRAMUL;
                    ,IMAGGPAR = CursDBF.IMAGGPAR;
                    ,IMAGGANA = CursDBF.IMAGGANA;
                    ,IMAGGIVA = CursDBF.IMAGGIVA;
                    ,IMAGGSAL = CursDBF.IMAGGSAL;
                    ,IMAGGNUR = CursDBF.IMAGGNUR;
                    ,IMCONIND = CursDBF.IMCONIND;
                    ,IMFILTRO = CursDBF.IMFILTRO;
                    ,IMCHIMOV = CursDBF.IMCHIMOV;
                    ,IMANNOTA = CursDBF.IMANNOTA;
                    ,IMPARIVA = CursDBF.IMPARIVA;
                    ,IMAZZERA = CursDBF.IMAZZERA;
                    ,IMRESOCO = CursDBF.IMRESOCO;
                    ,IMCONTRO = CursDBF.IMCONTRO;
                    ,IM_ODBC = CursDBF.IM_ODBC;
                    ,IMTIPDBF = CursDBF.IMTIPDBF;
                    ,IMROUTIN = CursDBF.IMROUTIN;
                    &i_ccchkf. ;
                 where;
                    IMCODICE = CursDBF.IMCODICE;
                    and IM_ASCII = CursDBF.IM_ASCII;
                    and IMDESTIN = CursDBF.IMDESTIN;
                    and IMTIPSRC = CursDBF.IMTIPSRC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_047AB218
          * --- End
          endscan
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Filedbf = alltrim(this.oParentObject.w_DBF11)
          ah_Msg("Import tracciati record...",.T.)
          * --- Import in tabella TRACCIAT
          if file(Filedbf)
            use (Filedbf) alias CursDBF
            if reccount("CursDBF")<>0
              go top
              scan for 1=1
              * --- Try
              local bErr_047AEFF8
              bErr_047AEFF8=bTrsErr
              this.Try_047AEFF8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                * --- Write into TRACCIAT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.TRACCIAT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TRACCIAT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"TRCAMDES ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDES),'TRACCIAT','TRCAMDES');
                  +",TRCAMTIP ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMTIP),'TRACCIAT','TRCAMTIP');
                  +",TRCAMLUN ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMLUN),'TRACCIAT','TRCAMLUN');
                  +",TRCAMDEC ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDEC),'TRACCIAT','TRCAMDEC');
                  +",TRCAMDST ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDST),'TRACCIAT','TRCAMDST');
                  +",TRCAMOBB ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMOBB),'TRACCIAT','TRCAMOBB');
                  +",TRANNOTA ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRANNOTA),'TRACCIAT','TRANNOTA');
                  +",TRTRASCO ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRTRASCO),'TRACCIAT','TRTRASCO');
                  +",TRTIPDES ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPDES),'TRACCIAT','TRTIPDES');
                  +",TRTIPSRG ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPSRG),'TRACCIAT','TRTIPSRG');
                  +",TREXPSRG ="+cp_NullLink(cp_ToStrODBC(CursDBF.TREXPSRG),'TRACCIAT','TREXPSRG');
                  +",TRCAMSRC ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMSRC),'TRACCIAT','TRCAMSRC');
                  +",TRCAMDLE ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDLE),'TRACCIAT','TRCAMDLE');
                  +",TRCAMDDE ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDDE),'TRACCIAT','TRCAMDDE');
                      +i_ccchkf ;
                  +" where ";
                      +"TRCODICE = "+cp_ToStrODBC(CursDBF.TRCODICE);
                      +" and TR_ASCII = "+cp_ToStrODBC(CursDBF.TR_ASCII);
                      +" and TRDESTIN = "+cp_ToStrODBC(CursDBF.TRDESTIN);
                      +" and TRCAMNUM = "+cp_ToStrODBC(CursDBF.TRCAMNUM);
                      +" and TRTIPSRC = "+cp_ToStrODBC(CursDBF.TRTIPSRC);
                         )
                else
                  update (i_cTable) set;
                      TRCAMDES = CursDBF.TRCAMDES;
                      ,TRCAMTIP = CursDBF.TRCAMTIP;
                      ,TRCAMLUN = CursDBF.TRCAMLUN;
                      ,TRCAMDEC = CursDBF.TRCAMDEC;
                      ,TRCAMDST = CursDBF.TRCAMDST;
                      ,TRCAMOBB = CursDBF.TRCAMOBB;
                      ,TRANNOTA = CursDBF.TRANNOTA;
                      ,TRTRASCO = CursDBF.TRTRASCO;
                      ,TRTIPDES = CursDBF.TRTIPDES;
                      ,TRTIPSRG = CursDBF.TRTIPSRG;
                      ,TREXPSRG = CursDBF.TREXPSRG;
                      ,TRCAMSRC = CursDBF.TRCAMSRC;
                      ,TRCAMDLE = CursDBF.TRCAMDLE;
                      ,TRCAMDDE = CursDBF.TRCAMDDE;
                      &i_ccchkf. ;
                   where;
                      TRCODICE = CursDBF.TRCODICE;
                      and TR_ASCII = CursDBF.TR_ASCII;
                      and TRDESTIN = CursDBF.TRDESTIN;
                      and TRCAMNUM = CursDBF.TRCAMNUM;
                      and TRTIPSRC = CursDBF.TRTIPSRC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              bTrsErr=bTrsErr or bErr_047AEFF8
              * --- End
              endscan
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      endif
    endif
    * --- VALORI PREDEFINITI
    if this.oParentObject.w_SELEVP1 = "VP"
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      ah_Msg("Import valori predefiniti...",.T.)
      * --- Import Valori predefiniti in tabella PREDEFIN
      if file(Filedbf)
        use (Filedbf) alias CursDBF
        if reccount("CursDBF")<>0
          go top
          scan for 1=1
          * --- Try
          local bErr_0479A168
          bErr_0479A168=bTrsErr
          this.Try_0479A168()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into PREDEFIN
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PREDEFIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PREDEFIN_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRTIPCAM ="+cp_NullLink(cp_ToStrODBC(CursDBF.PRTIPCAM),'PREDEFIN','PRTIPCAM');
              +",PRLUNCAM ="+cp_NullLink(cp_ToStrODBC(CursDBF.PRLUNCAM),'PREDEFIN','PRLUNCAM');
              +",PRTIPVAL ="+cp_NullLink(cp_ToStrODBC(CursDBF.PRTIPVAL),'PREDEFIN','PRTIPVAL');
              +",PRVALPRE ="+cp_NullLink(cp_ToStrODBC(CursDBF.PRVALPRE),'PREDEFIN','PRVALPRE');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODFIL = "+cp_ToStrODBC(CursDBF.PRCODFIL);
                  +" and PRNOMCAM = "+cp_ToStrODBC(CursDBF.PRNOMCAM);
                     )
            else
              update (i_cTable) set;
                  PRTIPCAM = CursDBF.PRTIPCAM;
                  ,PRLUNCAM = CursDBF.PRLUNCAM;
                  ,PRTIPVAL = CursDBF.PRTIPVAL;
                  ,PRVALPRE = CursDBF.PRVALPRE;
                  &i_ccchkf. ;
               where;
                  PRCODFIL = CursDBF.PRCODFIL;
                  and PRNOMCAM = CursDBF.PRNOMCAM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0479A168
          * --- End
          endscan
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Filedbf = alltrim(this.oParentObject.w_DBF31)
          ah_Msg("Import valori predefiniti...",.T.)
          * --- Import valori predefiniti in tabella PREDEIMP
          if file(Filedbf)
            use (Filedbf) alias CursDBF
            if reccount("CursDBF")<>0
              go top
              scan for 1=1
              * --- Try
              local bErr_047B36A8
              bErr_047B36A8=bTrsErr
              this.Try_047B36A8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                * --- Write into PREDEIMP
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREDEIMP_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREDEIMP_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PITIPVAL ="+cp_NullLink(cp_ToStrODBC(CursDBF.PITIPVAL),'PREDEIMP','PITIPVAL');
                  +",PIVALPRE ="+cp_NullLink(cp_ToStrODBC(CursDBF.PIVALPRE),'PREDEIMP','PIVALPRE');
                      +i_ccchkf ;
                  +" where ";
                      +"PICODFIL = "+cp_ToStrODBC(CursDBF.PICODFIL);
                      +" and PINOMCAM = "+cp_ToStrODBC(CursDBF.PINOMCAM);
                      +" and PICODIMP = "+cp_ToStrODBC(CursDBF.PICODIMP);
                         )
                else
                  update (i_cTable) set;
                      PITIPVAL = CursDBF.PITIPVAL;
                      ,PIVALPRE = CursDBF.PIVALPRE;
                      &i_ccchkf. ;
                   where;
                      PICODFIL = CursDBF.PICODFIL;
                      and PINOMCAM = CursDBF.PINOMCAM;
                      and PICODIMP = CursDBF.PICODIMP;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              bTrsErr=bTrsErr or bErr_047B36A8
              * --- End
              endscan
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      endif
    endif
    * --- TRASCODIFICHE
    if this.oParentObject.w_SELETS1 = "TS"
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      ah_Msg("Import trascodifiche...",.T.)
      * --- Import trascodifiche in tabella TRASCODI
      if file(Filedbf)
        use (Filedbf) alias CursDBF
        if reccount("CursDBF")<>0
          go top
          * --- Gestiamo il caso in cui nel DBF da cui importiamo non sia presente il campo SOSQLSRC, relativo alla frase SQL per importare le trascodifiche mancanti
          Dimension aStruct(1)
          this.w_nField = 0
          this.FraseSQLPresente = .F.
          this.FraseSQL = ""
          this.w_nField = AFIELDS(aStruct,"CursDBF")
          if ASCAN(aStruct,"SOSQLSRC",1,this.w_nField,1) > 0
            this.FraseSQLPresente = .T.
          endif
          * --- Se FraseSQLPresente vale .t. significa che il campo � presente nella struttura di import
          scan for 1=1
          * --- Getiamo il valore del campo da inserire nella tabella
          if this.FraseSQLPresente
            this.FraseSQL = ALLTRIM(NVL(CursDBF.SOSQLSRC,""))
          endif
          * --- Try
          local bErr_04792D88
          bErr_04792D88=bTrsErr
          this.Try_04792D88()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into TRASCODI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRCODENT),'TRASCODI','TRCODENT');
              +",TRTRAPAR ="+cp_NullLink(cp_ToStrODBC(CursDBF.TRTRAPAR),'TRASCODI','TRTRAPAR');
              +",SOSQLSRC ="+cp_NullLink(cp_ToStrODBC(this.FraseSQL),'TRASCODI','SOSQLSRC');
                  +i_ccchkf ;
              +" where ";
                  +"TRCODFIL = "+cp_ToStrODBC(CursDBF.TRCODFIL);
                  +" and TRNOMCAM = "+cp_ToStrODBC(CursDBF.TRNOMCAM);
                  +" and TRCODEXT = "+cp_ToStrODBC(CursDBF.TRCODEXT);
                  +" and TRTIPTRA = "+cp_ToStrODBC(CursDBF.TRTIPTRA);
                  +" and TRCODIMP = "+cp_ToStrODBC(CursDBF.TRCODIMP);
                     )
            else
              update (i_cTable) set;
                  TRCODENT = CursDBF.TRCODENT;
                  ,TRTRAPAR = CursDBF.TRTRAPAR;
                  ,SOSQLSRC = this.FraseSQL;
                  &i_ccchkf. ;
               where;
                  TRCODFIL = CursDBF.TRCODFIL;
                  and TRNOMCAM = CursDBF.TRNOMCAM;
                  and TRCODEXT = CursDBF.TRCODEXT;
                  and TRTIPTRA = CursDBF.TRTIPTRA;
                  and TRCODIMP = CursDBF.TRCODIMP;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04792D88
          * --- End
          endscan
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_047F7318()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into IMPOARCH
    i_nConn=i_TableProp[this.IMPOARCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPOARCH_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ARCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.ARCODICE),'IMPOARCH','ARCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ARCODICE',CursDBF.ARCODICE)
      insert into (i_cTable) (ARCODICE &i_ccchkf. );
         values (;
           CursDBF.ARCODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047EF368()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SRCMODBC
    i_nConn=i_TableProp[this.SRCMODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SRCMODBC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SOCODICE"+",SODESCR"+",SOSQLSRC"+",SOROUTRA"+",FLGFILE"+",FILEEXC"+",FOGLEXC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.SOCODICE),'SRCMODBC','SOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SODESCR),'SRCMODBC','SODESCR');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SOSQLSRC),'SRCMODBC','SOSQLSRC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SOROUTRA),'SRCMODBC','SOROUTRA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLGFILE),'SRCMODBC','FLGFILE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FILEEXC),'SRCMODBC','FILEEXC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FOGLEXC),'SRCMODBC','FOGLEXC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SOCODICE',CursDBF.SOCODICE,'SODESCR',CursDBF.SODESCR,'SOSQLSRC',CursDBF.SOSQLSRC,'SOROUTRA',CursDBF.SOROUTRA,'FLGFILE',CursDBF.FLGFILE,'FILEEXC',CursDBF.FILEEXC,'FOGLEXC',CursDBF.FOGLEXC)
      insert into (i_cTable) (SOCODICE,SODESCR,SOSQLSRC,SOROUTRA,FLGFILE,FILEEXC,FOGLEXC &i_ccchkf. );
         values (;
           CursDBF.SOCODICE;
           ,CursDBF.SODESCR;
           ,CursDBF.SOSQLSRC;
           ,CursDBF.SOROUTRA;
           ,CursDBF.FLGFILE;
           ,CursDBF.FILEEXC;
           ,CursDBF.FOGLEXC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047F15B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SRCMODBC
    i_nConn=i_TableProp[this.SRCMODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SRCMODBC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SOCODICE"+",SODESCR"+",SOSQLSRC"+",SOROUTRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.SOCODICE),'SRCMODBC','SOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SODESCR),'SRCMODBC','SODESCR');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SOSQLSRC),'SRCMODBC','SOSQLSRC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.SOROUTRA),'SRCMODBC','SOROUTRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SOCODICE',CursDBF.SOCODICE,'SODESCR',CursDBF.SODESCR,'SOSQLSRC',CursDBF.SOSQLSRC,'SOROUTRA',CursDBF.SOROUTRA)
      insert into (i_cTable) (SOCODICE,SODESCR,SOSQLSRC,SOROUTRA &i_ccchkf. );
         values (;
           CursDBF.SOCODICE;
           ,CursDBF.SODESCR;
           ,CursDBF.SOSQLSRC;
           ,CursDBF.SOROUTRA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047CE7E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SRC_ODBC
    i_nConn=i_TableProp[this.SRC_ODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRC_ODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SRC_ODBC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SOCODICE"+",FLNAME"+",FLTYPE"+",FLLENGHT"+",FLDECIMA"+",FLCOMMEN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.SOCODICE),'SRC_ODBC','SOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLNAME),'SRC_ODBC','FLNAME');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLTYPE),'SRC_ODBC','FLTYPE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLLENGHT),'SRC_ODBC','FLLENGHT');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLDECIMA),'SRC_ODBC','FLDECIMA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.FLCOMMEN),'SRC_ODBC','FLCOMMEN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SOCODICE',CursDBF.SOCODICE,'FLNAME',CursDBF.FLNAME,'FLTYPE',CursDBF.FLTYPE,'FLLENGHT',CursDBF.FLLENGHT,'FLDECIMA',CursDBF.FLDECIMA,'FLCOMMEN',CursDBF.FLCOMMEN)
      insert into (i_cTable) (SOCODICE,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN &i_ccchkf. );
         values (;
           CursDBF.SOCODICE;
           ,CursDBF.FLNAME;
           ,CursDBF.FLTYPE;
           ,CursDBF.FLLENGHT;
           ,CursDBF.FLDECIMA;
           ,CursDBF.FLCOMMEN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047AB218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if ! empty(CursDBF.IM_ODBC)
      * --- Try
      local bErr_047C3110
      bErr_047C3110=bTrsErr
      this.Try_047C3110()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_047C3110
      * --- End
    endif
    if type("CursDBF.IM_EXCEL")<>"U"
      * --- Insert into IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPORTAZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IMCODICE"+",IMSEQUEN"+",IM_ASCII"+",IMDESTIN"+",IMTRASCO"+",IMTRAMUL"+",IMAGGPAR"+",IMAGGANA"+",IMAGGIVA"+",IMAGGSAL"+",IMAGGNUR"+",IMCONIND"+",IMFILTRO"+",IMCHIMOV"+",IMANNOTA"+",IMPARIVA"+",IMAZZERA"+",IMRESOCO"+",IMCONTRO"+",IMTIPSRC"+",IMTIPDBF"+",IMROUTIN"+",IM_EXCEL"+",IM_ODBC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(CursDBF.IMCODICE),'IMPORTAZ','IMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ASCII),'IMPORTAZ','IM_ASCII');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMDESTIN),'IMPORTAZ','IMDESTIN');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRASCO),'IMPORTAZ','IMTRASCO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGANA),'IMPORTAZ','IMAGGANA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONIND),'IMPORTAZ','IMCONIND');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMFILTRO),'IMPORTAZ','IMFILTRO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMANNOTA),'IMPORTAZ','IMANNOTA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMPARIVA),'IMPORTAZ','IMPARIVA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAZZERA),'IMPORTAZ','IMAZZERA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMRESOCO),'IMPORTAZ','IMRESOCO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONTRO),'IMPORTAZ','IMCONTRO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPSRC),'IMPORTAZ','IMTIPSRC');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPDBF),'IMPORTAZ','IMTIPDBF');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMROUTIN),'IMPORTAZ','IMROUTIN');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IM_EXCEL),'IMPORTAZ','IM_EXCEL');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ODBC),'IMPORTAZ','IM_ODBC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IMCODICE',CursDBF.IMCODICE,'IMSEQUEN',CursDBF.IMSEQUEN,'IM_ASCII',CursDBF.IM_ASCII,'IMDESTIN',CursDBF.IMDESTIN,'IMTRASCO',CursDBF.IMTRASCO,'IMTRAMUL',CursDBF.IMTRAMUL,'IMAGGPAR',CursDBF.IMAGGPAR,'IMAGGANA',CursDBF.IMAGGANA,'IMAGGIVA',CursDBF.IMAGGIVA,'IMAGGSAL',CursDBF.IMAGGSAL,'IMAGGNUR',CursDBF.IMAGGNUR,'IMCONIND',CursDBF.IMCONIND)
        insert into (i_cTable) (IMCODICE,IMSEQUEN,IM_ASCII,IMDESTIN,IMTRASCO,IMTRAMUL,IMAGGPAR,IMAGGANA,IMAGGIVA,IMAGGSAL,IMAGGNUR,IMCONIND,IMFILTRO,IMCHIMOV,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF,IMROUTIN,IM_EXCEL,IM_ODBC &i_ccchkf. );
           values (;
             CursDBF.IMCODICE;
             ,CursDBF.IMSEQUEN;
             ,CursDBF.IM_ASCII;
             ,CursDBF.IMDESTIN;
             ,CursDBF.IMTRASCO;
             ,CursDBF.IMTRAMUL;
             ,CursDBF.IMAGGPAR;
             ,CursDBF.IMAGGANA;
             ,CursDBF.IMAGGIVA;
             ,CursDBF.IMAGGSAL;
             ,CursDBF.IMAGGNUR;
             ,CursDBF.IMCONIND;
             ,CursDBF.IMFILTRO;
             ,CursDBF.IMCHIMOV;
             ,CursDBF.IMANNOTA;
             ,CursDBF.IMPARIVA;
             ,CursDBF.IMAZZERA;
             ,CursDBF.IMRESOCO;
             ,CursDBF.IMCONTRO;
             ,CursDBF.IMTIPSRC;
             ,CursDBF.IMTIPDBF;
             ,CursDBF.IMROUTIN;
             ,CursDBF.IM_EXCEL;
             ,CursDBF.IM_ODBC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPORTAZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IMCODICE"+",IMSEQUEN"+",IM_ASCII"+",IMDESTIN"+",IMTRASCO"+",IMTRAMUL"+",IMAGGPAR"+",IMAGGANA"+",IMAGGIVA"+",IMAGGSAL"+",IMAGGNUR"+",IMCONIND"+",IMFILTRO"+",IMCHIMOV"+",IMANNOTA"+",IMPARIVA"+",IMAZZERA"+",IMRESOCO"+",IMCONTRO"+",IMTIPSRC"+",IM_ODBC"+",IMTIPDBF"+",IMROUTIN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(CursDBF.IMCODICE),'IMPORTAZ','IMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ASCII),'IMPORTAZ','IM_ASCII');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMDESTIN),'IMPORTAZ','IMDESTIN');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRASCO),'IMPORTAZ','IMTRASCO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGANA),'IMPORTAZ','IMAGGANA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONIND),'IMPORTAZ','IMCONIND');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMFILTRO),'IMPORTAZ','IMFILTRO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMANNOTA),'IMPORTAZ','IMANNOTA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMPARIVA),'IMPORTAZ','IMPARIVA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMAZZERA),'IMPORTAZ','IMAZZERA');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMRESOCO),'IMPORTAZ','IMRESOCO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMCONTRO),'IMPORTAZ','IMCONTRO');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPSRC),'IMPORTAZ','IMTIPSRC');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IM_ODBC),'IMPORTAZ','IM_ODBC');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMTIPDBF),'IMPORTAZ','IMTIPDBF');
        +","+cp_NullLink(cp_ToStrODBC(CursDBF.IMROUTIN),'IMPORTAZ','IMROUTIN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IMCODICE',CursDBF.IMCODICE,'IMSEQUEN',CursDBF.IMSEQUEN,'IM_ASCII',CursDBF.IM_ASCII,'IMDESTIN',CursDBF.IMDESTIN,'IMTRASCO',CursDBF.IMTRASCO,'IMTRAMUL',CursDBF.IMTRAMUL,'IMAGGPAR',CursDBF.IMAGGPAR,'IMAGGANA',CursDBF.IMAGGANA,'IMAGGIVA',CursDBF.IMAGGIVA,'IMAGGSAL',CursDBF.IMAGGSAL,'IMAGGNUR',CursDBF.IMAGGNUR,'IMCONIND',CursDBF.IMCONIND)
        insert into (i_cTable) (IMCODICE,IMSEQUEN,IM_ASCII,IMDESTIN,IMTRASCO,IMTRAMUL,IMAGGPAR,IMAGGANA,IMAGGIVA,IMAGGSAL,IMAGGNUR,IMCONIND,IMFILTRO,IMCHIMOV,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IM_ODBC,IMTIPDBF,IMROUTIN &i_ccchkf. );
           values (;
             CursDBF.IMCODICE;
             ,CursDBF.IMSEQUEN;
             ,CursDBF.IM_ASCII;
             ,CursDBF.IMDESTIN;
             ,CursDBF.IMTRASCO;
             ,CursDBF.IMTRAMUL;
             ,CursDBF.IMAGGPAR;
             ,CursDBF.IMAGGANA;
             ,CursDBF.IMAGGIVA;
             ,CursDBF.IMAGGSAL;
             ,CursDBF.IMAGGNUR;
             ,CursDBF.IMCONIND;
             ,CursDBF.IMFILTRO;
             ,CursDBF.IMCHIMOV;
             ,CursDBF.IMANNOTA;
             ,CursDBF.IMPARIVA;
             ,CursDBF.IMAZZERA;
             ,CursDBF.IMRESOCO;
             ,CursDBF.IMCONTRO;
             ,CursDBF.IMTIPSRC;
             ,CursDBF.IM_ODBC;
             ,CursDBF.IMTIPDBF;
             ,CursDBF.IMROUTIN;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_047AEFF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRACCIAT
    i_nConn=i_TableProp[this.TRACCIAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRACCIAT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TR_ASCII"+",TRDESTIN"+",TRCAMNUM"+",TRCAMDES"+",TRCAMTIP"+",TRCAMLUN"+",TRCAMDEC"+",TRCAMDST"+",TRCAMOBB"+",TRANNOTA"+",TRTRASCO"+",TRTIPSRC"+",TRTIPDES"+",TRTIPSRG"+",TREXPSRG"+",TRCAMSRC"+",TRCAMDLE"+",TRCAMDDE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.TRCODICE),'TRACCIAT','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TR_ASCII),'TRACCIAT','TR_ASCII');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRDESTIN),'TRACCIAT','TRDESTIN');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMNUM),'TRACCIAT','TRCAMNUM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDES),'TRACCIAT','TRCAMDES');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMTIP),'TRACCIAT','TRCAMTIP');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMLUN),'TRACCIAT','TRCAMLUN');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDEC),'TRACCIAT','TRCAMDEC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDST),'TRACCIAT','TRCAMDST');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMOBB),'TRACCIAT','TRCAMOBB');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRANNOTA),'TRACCIAT','TRANNOTA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTRASCO),'TRACCIAT','TRTRASCO');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPSRC),'TRACCIAT','TRTIPSRC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPDES),'TRACCIAT','TRTIPDES');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPSRG),'TRACCIAT','TRTIPSRG');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TREXPSRG),'TRACCIAT','TREXPSRG');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMSRC),'TRACCIAT','TRCAMSRC');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDLE),'TRACCIAT','TRCAMDLE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCAMDDE),'TRACCIAT','TRCAMDDE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',CursDBF.TRCODICE,'TR_ASCII',CursDBF.TR_ASCII,'TRDESTIN',CursDBF.TRDESTIN,'TRCAMNUM',CursDBF.TRCAMNUM,'TRCAMDES',CursDBF.TRCAMDES,'TRCAMTIP',CursDBF.TRCAMTIP,'TRCAMLUN',CursDBF.TRCAMLUN,'TRCAMDEC',CursDBF.TRCAMDEC,'TRCAMDST',CursDBF.TRCAMDST,'TRCAMOBB',CursDBF.TRCAMOBB,'TRANNOTA',CursDBF.TRANNOTA,'TRTRASCO',CursDBF.TRTRASCO)
      insert into (i_cTable) (TRCODICE,TR_ASCII,TRDESTIN,TRCAMNUM,TRCAMDES,TRCAMTIP,TRCAMLUN,TRCAMDEC,TRCAMDST,TRCAMOBB,TRANNOTA,TRTRASCO,TRTIPSRC,TRTIPDES,TRTIPSRG,TREXPSRG,TRCAMSRC,TRCAMDLE,TRCAMDDE &i_ccchkf. );
         values (;
           CursDBF.TRCODICE;
           ,CursDBF.TR_ASCII;
           ,CursDBF.TRDESTIN;
           ,CursDBF.TRCAMNUM;
           ,CursDBF.TRCAMDES;
           ,CursDBF.TRCAMTIP;
           ,CursDBF.TRCAMLUN;
           ,CursDBF.TRCAMDEC;
           ,CursDBF.TRCAMDST;
           ,CursDBF.TRCAMOBB;
           ,CursDBF.TRANNOTA;
           ,CursDBF.TRTRASCO;
           ,CursDBF.TRTIPSRC;
           ,CursDBF.TRTIPDES;
           ,CursDBF.TRTIPSRG;
           ,CursDBF.TREXPSRG;
           ,CursDBF.TRCAMSRC;
           ,CursDBF.TRCAMDLE;
           ,CursDBF.TRCAMDDE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0479A168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PREDEFIN
    i_nConn=i_TableProp[this.PREDEFIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PREDEFIN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODFIL"+",PRNOMCAM"+",PRTIPCAM"+",PRLUNCAM"+",PRTIPVAL"+",PRVALPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.PRCODFIL),'PREDEFIN','PRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PRNOMCAM),'PREDEFIN','PRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PRTIPCAM),'PREDEFIN','PRTIPCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PRLUNCAM),'PREDEFIN','PRLUNCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PRTIPVAL),'PREDEFIN','PRTIPVAL');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PRVALPRE),'PREDEFIN','PRVALPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODFIL',CursDBF.PRCODFIL,'PRNOMCAM',CursDBF.PRNOMCAM,'PRTIPCAM',CursDBF.PRTIPCAM,'PRLUNCAM',CursDBF.PRLUNCAM,'PRTIPVAL',CursDBF.PRTIPVAL,'PRVALPRE',CursDBF.PRVALPRE)
      insert into (i_cTable) (PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM,PRTIPVAL,PRVALPRE &i_ccchkf. );
         values (;
           CursDBF.PRCODFIL;
           ,CursDBF.PRNOMCAM;
           ,CursDBF.PRTIPCAM;
           ,CursDBF.PRLUNCAM;
           ,CursDBF.PRTIPVAL;
           ,CursDBF.PRVALPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047B36A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PREDEIMP
    i_nConn=i_TableProp[this.PREDEIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PREDEIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PICODFIL"+",PINOMCAM"+",PICODIMP"+",PITIPVAL"+",PIVALPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.PICODFIL),'PREDEIMP','PICODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PINOMCAM),'PREDEIMP','PINOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PICODIMP),'PREDEIMP','PICODIMP');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PITIPVAL),'PREDEIMP','PITIPVAL');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.PIVALPRE),'PREDEIMP','PIVALPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PICODFIL',CursDBF.PICODFIL,'PINOMCAM',CursDBF.PINOMCAM,'PICODIMP',CursDBF.PICODIMP,'PITIPVAL',CursDBF.PITIPVAL,'PIVALPRE',CursDBF.PIVALPRE)
      insert into (i_cTable) (PICODFIL,PINOMCAM,PICODIMP,PITIPVAL,PIVALPRE &i_ccchkf. );
         values (;
           CursDBF.PICODFIL;
           ,CursDBF.PINOMCAM;
           ,CursDBF.PICODIMP;
           ,CursDBF.PITIPVAL;
           ,CursDBF.PIVALPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04792D88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODEXT"+",TRCODFIL"+",TRNOMCAM"+",TRTIPTRA"+",TRCODENT"+",TRCODIMP"+",TRTRAPAR"+",SOSQLSRC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.TRCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCODENT),'TRASCODI','TRCODENT');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.TRTRAPAR),'TRASCODI','TRTRAPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.FraseSQL),'TRASCODI','SOSQLSRC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODEXT',CursDBF.TRCODEXT,'TRCODFIL',CursDBF.TRCODFIL,'TRNOMCAM',CursDBF.TRNOMCAM,'TRTIPTRA',CursDBF.TRTIPTRA,'TRCODENT',CursDBF.TRCODENT,'TRCODIMP',CursDBF.TRCODIMP,'TRTRAPAR',CursDBF.TRTRAPAR,'SOSQLSRC',this.FraseSQL)
      insert into (i_cTable) (TRCODEXT,TRCODFIL,TRNOMCAM,TRTIPTRA,TRCODENT,TRCODIMP,TRTRAPAR,SOSQLSRC &i_ccchkf. );
         values (;
           CursDBF.TRCODEXT;
           ,CursDBF.TRCODFIL;
           ,CursDBF.TRNOMCAM;
           ,CursDBF.TRTIPTRA;
           ,CursDBF.TRCODENT;
           ,CursDBF.TRCODIMP;
           ,CursDBF.TRTRAPAR;
           ,this.FraseSQL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_047C3110()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SRCMODBC
    i_nConn=i_TableProp[this.SRCMODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SRCMODBC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SOCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.IM_ODBC),'SRCMODBC','SOCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SOCODICE',CursDBF.IM_ODBC)
      insert into (i_cTable) (SOCODICE &i_ccchkf. );
         values (;
           CursDBF.IM_ODBC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica esistenza file da esportare
    this.response = "S"
    if file( Filedbf )
      if not ah_YesNo("E' gi� presente il file:%1. Si desidera sovrascrivere?","",FileDBF)
        this.response = "N"
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursore generico
    USE IN SELECT("CursDBF")
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura su file DBF
    if used("CursDBF")
      select CursDBF
      go top
      copy to (Filedbf) TYPE FOX2X
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore "CursDBF" dalla tabella (w_cTableName)
    *     
    *     Ritorno:
    *     - CursDBF
    *     - i_Rows con numero record tabella
    * --- --
    USE IN SELECT("CursDBF")
    * --- Cerca area di lavoro tabella aperta in Work Table
    private cIDX
    cIDX = alltrim(this.w_cTableName)+"_idx"
    this.w_nT = this.&cIDX
    this.w_nConn = i_TableProp[ this.w_nT ,3]
    this.w_cTable = cp_SetAzi(i_TableProp[ this.w_nT ,2])
    if this.w_nConn<>0
      * --- SQL Server
      i_Rows = SqlExec( this.w_nConn,"select * from " + this.w_cTable,"CursDBF")
      i_Rows = iif(used("CursDBF"),reccount(),0)
    else
      * --- Fox Pro
      cTable = this.w_cTable
      select * from (cTable) into cursor CursDBF
      i_Rows = _TALLY
    endif
    if not used("CursDBF")
      AH_ERRORMSG("Impossibile creare il cursore di esportazione per la tabella %1" ,"!","",this.w_cTableName)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='PREDEFIN'
    this.cWorkTables[4]='PREDEIMP'
    this.cWorkTables[5]='SRCMODBC'
    this.cWorkTables[6]='SRC_ODBC'
    this.cWorkTables[7]='TRACCIAT'
    this.cWorkTables[8]='TRASCODI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
