* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bex                                                        *
*              Exp/imp tabelle import ASCII                                    *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_150]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2006-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bex",oParentObject)
return(i_retval)

define class tgsim_bex as StdBatch
  * --- Local variables
  w_nHandle = 0
  Messaggio = space(254)
  w_SCELTA = 0
  * --- WorkFile variables
  IMPOARCH_idx=0
  IMPORTAZ_idx=0
  PREDEFIN_idx=0
  PREDEIMP_idx=0
  TRACCIAT_idx=0
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export/Import Dati Tracciati, Destinazioni Valori Predefiniti e Trascodifiche
    * --- Variabili maschera
    * --- Tipo operazione da effettuare (I=Import; E=Export)
    * --- Nomi e path dei files ASCII
    * --- Campi di attivazione Import/Export
    * --- cursore per tabella IMPORTAZ
    create cursor CursAscii1 ( KEYCON C(8),IMCODICE C(20), IMSEQUEN N(5), IM_ASCII C(200), IMDESTIN C(2), ;
    IMTRASCO C(1), IMTRAMUL C(1), IMAGGPAR C(1), IMAGGANA C(1), IMAGGIVA C(1), IMAGGSAL C(1), ;
    IMAGGNUR C(1), IMCONIND C(15), IMFILTRO C(70), IMCHIMOV C(70), IMANNOTA C(200), ;
    IMPARIVA C(1), IMAZZERA C(1), IMRESOCO C(1), IMCONTRO C(1) )
    a=wrcursor("CursAscii1")
    * --- cursore per tabella TRACCIAT
    create cursor CursAscii3 ( KEYCON C(8),TRCODICE C(20), TR_ASCII C(200), TRDESTIN C(2), TRCAMNUM N(5), ;
    TRCAMDES C(30), TRCAMTIP C(1), TRCAMLUN N(5), TRCAMDEC N(2), TRCAMDST C(15) NULL, ;
    TRCAMOBB C(1), TRANNOTA C(200), TRTRASCO C(1) )
    b=wrcursor("CursAscii3")
    * --- cursore per tabella PREDEFIN
    create cursor CursAscii4 ( KEYCON C(8),PRCODFIL C(2), PRNOMCAM C(15), PRTIPCAM C(1), PRLUNCAM N(3), ;
    PRTIPVAL C(1), PRVALPRE C(200) )
    b=wrcursor("CursAscii4")
    * --- cursore per tabella PREDEFIN
    create cursor CursAscii5 ( KEYCON C(8),PICODFIL C(2), PINOMCAM C(15), PICODIMP C(20), ;
    PITIPVAL C(1), PIVALPRE C(200) )
    b=wrcursor("CursAscii5")
    * --- cursore per tabella TRASCODI
    create cursor CursAscii6 ( KEYCON C(8),TRCODFIL C(2), TRNOMCAM C(15), TRCODEXT C(50), ;
    TRCODENT C(200) )
    b=wrcursor("CursAscii6")
    * --- cursore per tabella TRASCIMP
    create cursor CursAscii7 ( KEYCON C(8),TICODFIL C(2), TINOMCAM C(15), TICODEXT C(50), ;
    TICODIMP C(20), TICODENT C(200) )
    b=wrcursor("CursAscii7")
    if this.oParentObject.w_RADSELIE = "E"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    wait clear
    if this.oParentObject.w_RADSELIE="I"
      ah_ErrorMsg("Procedura di importazione terminata con successo","i","")
    else
      ah_ErrorMsg("Procedura di esportazione terminata con successo","i","")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Scrittura files ascii
    this.w_SCELTA = 1
    if this.oParentObject.w_SELEAD = "AD"
      FileAscii = alltrim(this.oParentObject.w_ASCII2)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export archivi di destinazione...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim2bex.VQR",this,"CursAscii")
      * --- Export Destinazioni
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELETR = "TR"
      FileAscii = alltrim(this.oParentObject.w_ASCII1)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export tracciati record...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim1bex.VQR",this,"CursAscii")
      * --- Export Importaz
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii1 (KEYCON,IMCODICE,IMSEQUEN,IM_ASCII,IMDESTIN, IMTRASCO, ;
      IMTRAMUL,IMAGGPAR,IMAGGANA,IMAGGIVA,IMAGGSAL,IMAGGNUR,IMCONIND, ;
      IMFILTRO,IMCHIMOV,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO) ;
      VALUES ( CursAscii.KEYCON,CursAscii.IMCODICE,CursAscii.IMSEQUEN,CursAscii.IM_ASCII,CursAscii.IMDESTIN, ;
      NVL(CursAscii.IMTRASCO, " "),NVL(CursAscii.IMTRAMUL," "), ;
      NVL(CursAscii.IMAGGPAR," "),NVL(CursAscii.IMAGGANA," "),NVL(CursAscii.IMAGGIVA," "), ;
      NVL(CursAscii.IMAGGSAL," "),NVL(CursAscii.IMAGGNUR," "),NVL(CursAscii.IMCONIND,space(15)), ;
      NVL(CursAscii.IMFILTRO,space(70)),NVL(CursAscii.IMCHIMOV,space(70)), ;
      NVL(substr(strtran(CursAscii.IMANNOTA,chr(13)+chr(10),"\n"),1,200),space(200)),NVL(CursAscii.IMPARIVA," "), ;
      NVL(CursAscii.IMAZZERA," "),NVL(CursAscii.IMRESOCO," "),NVL(CursAscii.IMCONTRO," "))
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii1 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FileAscii = alltrim(this.oParentObject.w_ASCII1) + "D"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export tracciati record...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim3bex.VQR",this,"CursAscii")
      * --- Export Tracciati Records
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii3 (KEYCON,TRCODICE,TR_ASCII,TRDESTIN,TRCAMNUM,TRCAMDES, ;
      TRCAMTIP,TRCAMLUN,TRCAMDEC,TRCAMDST,TRCAMOBB,TRANNOTA,TRTRASCO ) ;
      VALUES ( CursAscii.KEYCON,CursAscii.TRCODICE,CursAscii.TR_ASCII,CursAscii.TRDESTIN,CursAscii.TRCAMNUM, ;
      CursAscii.TRCAMDES,CursAscii.TRCAMTIP,CursAscii.TRCAMLUN,CursAscii.TRCAMDEC, ;
      CursAscii.TRCAMDST,CursAscii.TRCAMOBB,substr(strtran(CursAscii.TRANNOTA,chr(13)+chr(10),"\n"),1,200),CursAscii.TRTRASCO )
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii3 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELEVP = "VP"
      FileAscii = alltrim(this.oParentObject.w_ASCII3)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export valori predefiniti...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim4bex.VQR",this,"CursAscii")
      * --- Export Valori predefiniti
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii4 (KEYCON,PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM, ;
      PRTIPVAL,PRVALPRE ) ;
      VALUES ( CursAscii.KEYCON,CursAscii.PRCODFIL,CursAscii.PRNOMCAM,CursAscii.PRTIPCAM,CursAscii.PRLUNCAM, ;
      CursAscii.PRTIPVAL,substr(strtran(CursAscii.PRVALPRE,chr(13)+chr(10),"\n"),1,200)) 
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii4 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FileAscii = alltrim(this.oParentObject.w_ASCII3) + "D"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export valori predefiniti...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim5bex.VQR",this,"CursAscii")
      * --- Export valori predefiniti per import
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii5 (KEYCON,PICODFIL,PINOMCAM,PICODIMP, ;
      PITIPVAL,PIVALPRE ) ;
      VALUES ( CursAscii.KEYCON,CursAscii.PICODFIL,CursAscii.PINOMCAM,CursAscii.PICODIMP, ;
      CursAscii.PITIPVAL,substr(strtran(CursAscii.PIVALPRE,chr(13)+chr(10),"\n"),1,200))
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii5 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELETS = "TS"
      FileAscii = alltrim(this.oParentObject.w_ASCII4)
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export trascodifiche...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim6bex.VQR",this,"CursAscii")
      * --- Export trascodifiche
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii6 (KEYCON,TRCODFIL,TRNOMCAM,TRCODEXT, ;
      TRCODENT ) ;
      VALUES ( CursAscii.KEYCON,CursAscii.TRCODFIL,CursAscii.TRNOMCAM,CursAscii.TRCODEXT, ;
      substr(strtran(CursAscii.TRCODENT,chr(13)+chr(10),"\n"),1,200))
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii6 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FileAscii = alltrim(this.oParentObject.w_ASCII4) + "D"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ah_Msg("Export trascodifiche...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim7bex.VQR",this,"CursAscii")
      * --- Export trascodifiche per import
      select CursAscii
      go top
      * --- Lo scrivo in un cursore per prendere i primi 200 caratteri dei campi MEMO
      scan
      INSERT INTO CursAscii7 (KEYCON,TICODFIL,TINOMCAM,TICODEXT, ;
      TICODIMP,TICODENT ) ;
      VALUES ( CursAscii.KEYCON,CursAscii.TICODFIL,CursAscii.TINOMCAM,CursAscii.TICODEXT, ;
      CursAscii.TICODIMP,substr(strtran(CursAscii.TICODENT,chr(13)+chr(10),"\n"),1,200))
      endscan
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select * from CursAscii7 into cursor CursAscii
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- Lettura files ascii
    this.w_SCELTA = 2
    if this.oParentObject.w_SELEAD = "AD"
      FileAscii = alltrim(this.oParentObject.w_ASCII2)
      ah_Msg("Import archivi di destinazione...",.T.)
      VQ_EXEC("..\impo\exe\query\gsim2bex.VQR",this,"CursAscii")
      * --- Import Destinazioni
      if file(FileAscii)
        a=wrcursor("CursAscii")
        select CursAscii
        append from (FileAscii) type SDF
        go top
        scan while NOT EMPTY(alltrim(CursAscii.KEYCON))
        if alltrim(CursAscii.KEYCON) <> "IMPOARCH"
          * --- Se non riconosco l'archivio segnalo l'errore
          AH_ErrorMsg("Il file %1 non � del tipo destinazioni (IMPOARCH)","!","" , alltrim(FileAscii))
          EXIT
        endif
        * --- Try
        local bErr_036B2A30
        bErr_036B2A30=bTrsErr
        this.Try_036B2A30()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into IMPOARCH
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.IMPOARCH_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.IMPOARCH_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARDESCRI ="+cp_NullLink(cp_ToStrODBC(CursAscii.ARDESCRI),'IMPOARCH','ARDESCRI');
            +",ARTABELL ="+cp_NullLink(cp_ToStrODBC(CursAscii.ARTABELL),'IMPOARCH','ARTABELL');
                +i_ccchkf ;
            +" where ";
                +"ARCODICE = "+cp_ToStrODBC(CursAscii.ARCODICE);
                   )
          else
            update (i_cTable) set;
                ARDESCRI = CursAscii.ARDESCRI;
                ,ARTABELL = CursAscii.ARTABELL;
                &i_ccchkf. ;
             where;
                ARCODICE = CursAscii.ARCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_036B2A30
        * --- End
        endscan
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_SELETR = "TR"
      FileAscii = alltrim(this.oParentObject.w_ASCII1)
      ah_Msg("Import tracciati record...",.T.)
      if file(FileAscii)
        * --- Import Importaz
        a=wrcursor("CursAscii1")
        select CursAscii1
        append from (FileAscii) type SDF
        go top
        scan while NOT EMPTY(alltrim(CursAscii1.KEYCON))
        if alltrim(CursAscii1.KEYCON) <> "IMPORTAZ"
          * --- Se non riconosco l'archivio segnalo l'errore
          AH_ErrorMsg("Il file %1 non � del tipo importazioni (IMPORTAZ)","!","" , alltrim(FileAscii))
          EXIT
        endif
        replace IMANNOTA with strtran(IMANNOTA,"\n",chr(13)+chr(10))
        * --- Try
        local bErr_038074B8
        bErr_038074B8=bTrsErr
        this.Try_038074B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into IMPORTAZ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.IMPORTAZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMSEQUEN ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
            +",IMTRASCO ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMTRASCO),'IMPORTAZ','IMTRASCO');
            +",IMTRAMUL ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
            +",IMAGGPAR ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
            +",IMAGGANA ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGANA),'IMPORTAZ','IMAGGANA');
            +",IMAGGIVA ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
            +",IMAGGSAL ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
            +",IMAGGNUR ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
            +",IMCONIND ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCONIND),'IMPORTAZ','IMCONIND');
            +",IMFILTRO ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMFILTRO),'IMPORTAZ','IMFILTRO');
            +",IMCHIMOV ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
            +",IMANNOTA ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMANNOTA),'IMPORTAZ','IMANNOTA');
            +",IMPARIVA ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMPARIVA),'IMPORTAZ','IMPARIVA');
            +",IMAZZERA ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAZZERA),'IMPORTAZ','IMAZZERA');
            +",IMRESOCO ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMRESOCO),'IMPORTAZ','IMRESOCO');
            +",IMCONTRO ="+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCONTRO),'IMPORTAZ','IMCONTRO');
                +i_ccchkf ;
            +" where ";
                +"IMCODICE = "+cp_ToStrODBC(CursAscii1.IMCODICE);
                +" and IM_ASCII = "+cp_ToStrODBC(CursAscii1.IM_ASCII);
                +" and IMDESTIN = "+cp_ToStrODBC(CursAscii1.IMDESTIN);
                +" and IMTIPSRC = "+cp_ToStrODBC("A");
                   )
          else
            update (i_cTable) set;
                IMSEQUEN = CursAscii1.IMSEQUEN;
                ,IMTRASCO = CursAscii1.IMTRASCO;
                ,IMTRAMUL = CursAscii1.IMTRAMUL;
                ,IMAGGPAR = CursAscii1.IMAGGPAR;
                ,IMAGGANA = CursAscii1.IMAGGANA;
                ,IMAGGIVA = CursAscii1.IMAGGIVA;
                ,IMAGGSAL = CursAscii1.IMAGGSAL;
                ,IMAGGNUR = CursAscii1.IMAGGNUR;
                ,IMCONIND = CursAscii1.IMCONIND;
                ,IMFILTRO = CursAscii1.IMFILTRO;
                ,IMCHIMOV = CursAscii1.IMCHIMOV;
                ,IMANNOTA = CursAscii1.IMANNOTA;
                ,IMPARIVA = CursAscii1.IMPARIVA;
                ,IMAZZERA = CursAscii1.IMAZZERA;
                ,IMRESOCO = CursAscii1.IMRESOCO;
                ,IMCONTRO = CursAscii1.IMCONTRO;
                &i_ccchkf. ;
             where;
                IMCODICE = CursAscii1.IMCODICE;
                and IM_ASCII = CursAscii1.IM_ASCII;
                and IMDESTIN = CursAscii1.IMDESTIN;
                and IMTIPSRC = "A";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_038074B8
        * --- End
        endscan
        FileAscii = alltrim(this.oParentObject.w_ASCII1) + "D"
        ah_Msg("Import tracciati record...",.T.)
        if file(FileAscii)
          a=wrcursor("CursAscii3")
          * --- Import tracciati
          select CursAscii3
          append from (FileAscii) type SDF
          go top
          scan while NOT EMPTY(alltrim(CursAscii3.KEYCON))
          if alltrim(CursAscii3.KEYCON) <> "TRACCIAT"
            * --- Se non riconosco l'archivio segnalo l'errore
            AH_ErrorMsg("Il file %1 non � del tipo tracciati record (TRACCIAT)","!","" , alltrim(FileAscii))
            EXIT
          endif
          replace TRANNOTA with strtran(TRANNOTA,"\n",chr(13)+chr(10))
          * --- Try
          local bErr_0380C4C8
          bErr_0380C4C8=bTrsErr
          this.Try_0380C4C8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into TRACCIAT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRACCIAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRACCIAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCAMDES ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDES),'TRACCIAT','TRCAMDES');
              +",TRCAMTIP ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMTIP),'TRACCIAT','TRCAMTIP');
              +",TRCAMLUN ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMLUN),'TRACCIAT','TRCAMLUN');
              +",TRCAMDEC ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDEC),'TRACCIAT','TRCAMDEC');
              +",TRCAMDST ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDST),'TRACCIAT','TRCAMDST');
              +",TRCAMOBB ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMOBB),'TRACCIAT','TRCAMOBB');
              +",TRANNOTA ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRANNOTA),'TRACCIAT','TRANNOTA');
              +",TRTRASCO ="+cp_NullLink(cp_ToStrODBC(CursAscii3.TRTRASCO),'TRACCIAT','TRTRASCO');
              +",TRTIPDES ="+cp_NullLink(cp_ToStrODBC("V"),'TRACCIAT','TRTIPDES');
                  +i_ccchkf ;
              +" where ";
                  +"TRCODICE = "+cp_ToStrODBC(CursAscii3.TRCODICE);
                  +" and TR_ASCII = "+cp_ToStrODBC(CursAscii3.TR_ASCII);
                  +" and TRDESTIN = "+cp_ToStrODBC(CursAscii3.TRDESTIN);
                  +" and TRCAMNUM = "+cp_ToStrODBC(CursAscii3.TRCAMNUM);
                  +" and TRTIPSRC = "+cp_ToStrODBC("A");
                     )
            else
              update (i_cTable) set;
                  TRCAMDES = CursAscii3.TRCAMDES;
                  ,TRCAMTIP = CursAscii3.TRCAMTIP;
                  ,TRCAMLUN = CursAscii3.TRCAMLUN;
                  ,TRCAMDEC = CursAscii3.TRCAMDEC;
                  ,TRCAMDST = CursAscii3.TRCAMDST;
                  ,TRCAMOBB = CursAscii3.TRCAMOBB;
                  ,TRANNOTA = CursAscii3.TRANNOTA;
                  ,TRTRASCO = CursAscii3.TRTRASCO;
                  ,TRTIPDES = "V";
                  &i_ccchkf. ;
               where;
                  TRCODICE = CursAscii3.TRCODICE;
                  and TR_ASCII = CursAscii3.TR_ASCII;
                  and TRDESTIN = CursAscii3.TRDESTIN;
                  and TRCAMNUM = CursAscii3.TRCAMNUM;
                  and TRTIPSRC = "A";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0380C4C8
          * --- End
          endscan
        endif
      endif
    endif
    if this.oParentObject.w_SELEVP = "VP"
      FileAscii = alltrim(this.oParentObject.w_ASCII3)
      ah_Msg("Import valori predefiniti...",.T.)
      if file(FileAscii)
        * --- Import Valori predefiniti
        a=wrcursor("CursAscii4")
        select CursAscii4
        append from (FileAscii) type SDF
        go top
        scan while NOT EMPTY(alltrim(CursAscii4.KEYCON))
        if alltrim(CursAscii4.KEYCON) <> "PREDEFIN"
          * --- Se non riconosco l'archivio segnalo l'errore
          AH_ErrorMsg("Il file %1 non � del tipo valori predefiniti (PREDEFIN)","!","" , alltrim(FileAscii))
          EXIT
        endif
        replace PRVALPRE with strtran(PRVALPRE,"\n",chr(13)+chr(10))
        * --- Try
        local bErr_037F1EA8
        bErr_037F1EA8=bTrsErr
        this.Try_037F1EA8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PREDEFIN
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PREDEFIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PREDEFIN_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRTIPCAM ="+cp_NullLink(cp_ToStrODBC(CursAscii4.PRTIPCAM),'PREDEFIN','PRTIPCAM');
            +",PRLUNCAM ="+cp_NullLink(cp_ToStrODBC(CursAscii4.PRLUNCAM),'PREDEFIN','PRLUNCAM');
            +",PRTIPVAL ="+cp_NullLink(cp_ToStrODBC(CursAscii4.PRTIPVAL),'PREDEFIN','PRTIPVAL');
            +",PRVALPRE ="+cp_NullLink(cp_ToStrODBC(CursAscii4.PRVALPRE),'PREDEFIN','PRVALPRE');
                +i_ccchkf ;
            +" where ";
                +"PRCODFIL = "+cp_ToStrODBC(CursAscii4.PRCODFIL);
                +" and PRNOMCAM = "+cp_ToStrODBC(CursAscii4.PRNOMCAM);
                   )
          else
            update (i_cTable) set;
                PRTIPCAM = CursAscii4.PRTIPCAM;
                ,PRLUNCAM = CursAscii4.PRLUNCAM;
                ,PRTIPVAL = CursAscii4.PRTIPVAL;
                ,PRVALPRE = CursAscii4.PRVALPRE;
                &i_ccchkf. ;
             where;
                PRCODFIL = CursAscii4.PRCODFIL;
                and PRNOMCAM = CursAscii4.PRNOMCAM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_037F1EA8
        * --- End
        endscan
        FileAscii = alltrim(this.oParentObject.w_ASCII3) + "D"
        ah_Msg("Import valori predefiniti...",.T.)
        * --- Import valori predefiniti per import
        if file(FileAscii)
          a=wrcursor("CursAscii5")
          select CursAscii5
          append from (FileAscii) type SDF
          go top
          scan while NOT EMPTY(alltrim(CursAscii5.KEYCON))
          if alltrim(CursAscii5.KEYCON) <> "PREDEIMP"
            * --- Se non riconosco l'archivio segnalo l'errore
            AH_ErrorMsg("Il file %1 non � del tipo valori predefiniti (PREDEIMP)","!","" , alltrim(FileAscii))
            EXIT
          endif
          replace PIVALPRE with strtran(PIVALPRE,"\n",chr(13)+chr(10))
          * --- Try
          local bErr_037F6468
          bErr_037F6468=bTrsErr
          this.Try_037F6468()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into PREDEIMP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PREDEIMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PREDEIMP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PITIPVAL ="+cp_NullLink(cp_ToStrODBC(CursAscii5.PITIPVAL),'PREDEIMP','PITIPVAL');
              +",PIVALPRE ="+cp_NullLink(cp_ToStrODBC(CursAscii5.PIVALPRE),'PREDEIMP','PIVALPRE');
                  +i_ccchkf ;
              +" where ";
                  +"PICODFIL = "+cp_ToStrODBC(CursAscii5.PICODFIL);
                  +" and PINOMCAM = "+cp_ToStrODBC(CursAscii5.PINOMCAM);
                  +" and PICODIMP = "+cp_ToStrODBC(CursAscii5.PICODIMP);
                     )
            else
              update (i_cTable) set;
                  PITIPVAL = CursAscii5.PITIPVAL;
                  ,PIVALPRE = CursAscii5.PIVALPRE;
                  &i_ccchkf. ;
               where;
                  PICODFIL = CursAscii5.PICODFIL;
                  and PINOMCAM = CursAscii5.PINOMCAM;
                  and PICODIMP = CursAscii5.PICODIMP;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_037F6468
          * --- End
          endscan
        endif
      endif
    endif
    if this.oParentObject.w_SELETS = "TS"
      FileAscii = alltrim(this.oParentObject.w_ASCII4)
      ah_Msg("Import trascodifiche...",.T.)
      if file(FileAscii)
        * --- Import trascodifiche
        a=wrcursor("CursAscii6")
        select CursAscii6
        append from (FileAscii) type SDF
        go top
        scan while NOT EMPTY(alltrim(CursAscii6.KEYCON))
        if alltrim(CursAscii6.KEYCON) <> "TRASCODI"
          * --- Se non riconosco l'archivio segnalo l'errore
          AH_ErrorMsg("Il file %1 non � del tipo trascodifiche (TRASCODI)","!","" , alltrim(FileAscii))
          EXIT
        endif
        replace TRCODENT with strtran(TRCODENT,"\n",chr(13)+chr(10))
        * --- Try
        local bErr_037D4150
        bErr_037D4150=bTrsErr
        this.Try_037D4150()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into TRASCODI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TRASCODI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(CursAscii6.TRCODENT),'TRASCODI','TRCODENT');
                +i_ccchkf ;
            +" where ";
                +"TRCODFIL = "+cp_ToStrODBC(CursAscii6.TRCODFIL);
                +" and TRNOMCAM = "+cp_ToStrODBC(CursAscii6.TRNOMCAM);
                +" and TRCODEXT = "+cp_ToStrODBC(CursAscii6.TRCODEXT);
                +" and TRTIPTRA = "+cp_ToStrODBC("G");
                +" and TRCODIMP = "+cp_ToStrODBC("Tutti");
                   )
          else
            update (i_cTable) set;
                TRCODENT = CursAscii6.TRCODENT;
                &i_ccchkf. ;
             where;
                TRCODFIL = CursAscii6.TRCODFIL;
                and TRNOMCAM = CursAscii6.TRNOMCAM;
                and TRCODEXT = CursAscii6.TRCODEXT;
                and TRTIPTRA = "G";
                and TRCODIMP = "Tutti";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_037D4150
        * --- End
        endscan
        FileAscii = alltrim(this.oParentObject.w_ASCII4) + "D"
        ah_Msg("Import trascodifiche...",.T.)
        if file(FileAscii)
          * --- Import trascodifiche per import
          a=wrcursor("CursAscii7")
          select CursAscii7
          append from (FileAscii) type SDF
          go top
          scan while NOT EMPTY(alltrim(CursAscii7.KEYCON))
          if alltrim(CursAscii7.KEYCON) <> "TRASCIMP"
            * --- Se non riconosco l'archivio segnalo l'errore
            AH_ErrorMsg("Il file %1 non � del tipo trascodifiche (TRASCIMP)","!","" , alltrim(FileAscii))
            EXIT
          endif
          replace TICODENT with strtran(TICODENT,"\n",chr(13)+chr(10))
          * --- Try
          local bErr_037D89E0
          bErr_037D89E0=bTrsErr
          this.Try_037D89E0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into TRASCODI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(CursAscii7.TICODENT),'TRASCODI','TRCODENT');
                  +i_ccchkf ;
              +" where ";
                  +"TRCODFIL = "+cp_ToStrODBC(CursAscii7.TICODFIL);
                  +" and TRNOMCAM = "+cp_ToStrODBC(CursAscii7.TINOMCAM);
                  +" and TRCODEXT = "+cp_ToStrODBC(CursAscii7.TICODEXT);
                  +" and TRCODIMP = "+cp_ToStrODBC(CursAscii7.TICODIMP);
                  +" and TRTIPTRA = "+cp_ToStrODBC("S");
                     )
            else
              update (i_cTable) set;
                  TRCODENT = CursAscii7.TICODENT;
                  &i_ccchkf. ;
               where;
                  TRCODFIL = CursAscii7.TICODFIL;
                  and TRNOMCAM = CursAscii7.TINOMCAM;
                  and TRCODEXT = CursAscii7.TICODEXT;
                  and TRCODIMP = CursAscii7.TICODIMP;
                  and TRTIPTRA = "S";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_037D89E0
          * --- End
          endscan
        endif
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_036B2A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into IMPOARCH
    i_nConn=i_TableProp[this.IMPOARCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPOARCH_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ARCODICE"+",ARDESCRI"+",ARTABELL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii.ARCODICE),'IMPOARCH','ARCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii.ARDESCRI),'IMPOARCH','ARDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii.ARTABELL),'IMPOARCH','ARTABELL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ARCODICE',CursAscii.ARCODICE,'ARDESCRI',CursAscii.ARDESCRI,'ARTABELL',CursAscii.ARTABELL)
      insert into (i_cTable) (ARCODICE,ARDESCRI,ARTABELL &i_ccchkf. );
         values (;
           CursAscii.ARCODICE;
           ,CursAscii.ARDESCRI;
           ,CursAscii.ARTABELL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_038074B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into IMPORTAZ
    i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPORTAZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IMCODICE"+",IMSEQUEN"+",IM_ASCII"+",IMDESTIN"+",IMTRASCO"+",IMTRAMUL"+",IMAGGPAR"+",IMAGGANA"+",IMAGGIVA"+",IMAGGSAL"+",IMAGGNUR"+",IMCONIND"+",IMFILTRO"+",IMCHIMOV"+",IMANNOTA"+",IMPARIVA"+",IMAZZERA"+",IMRESOCO"+",IMCONTRO"+",IMTIPSRC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii1.IMCODICE),'IMPORTAZ','IMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IM_ASCII),'IMPORTAZ','IM_ASCII');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMDESTIN),'IMPORTAZ','IMDESTIN');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMTRASCO),'IMPORTAZ','IMTRASCO');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGANA),'IMPORTAZ','IMAGGANA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCONIND),'IMPORTAZ','IMCONIND');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMFILTRO),'IMPORTAZ','IMFILTRO');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMANNOTA),'IMPORTAZ','IMANNOTA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMPARIVA),'IMPORTAZ','IMPARIVA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMAZZERA),'IMPORTAZ','IMAZZERA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMRESOCO),'IMPORTAZ','IMRESOCO');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii1.IMCONTRO),'IMPORTAZ','IMCONTRO');
      +","+cp_NullLink(cp_ToStrODBC("A"),'IMPORTAZ','IMTIPSRC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IMCODICE',CursAscii1.IMCODICE,'IMSEQUEN',CursAscii1.IMSEQUEN,'IM_ASCII',CursAscii1.IM_ASCII,'IMDESTIN',CursAscii1.IMDESTIN,'IMTRASCO',CursAscii1.IMTRASCO,'IMTRAMUL',CursAscii1.IMTRAMUL,'IMAGGPAR',CursAscii1.IMAGGPAR,'IMAGGANA',CursAscii1.IMAGGANA,'IMAGGIVA',CursAscii1.IMAGGIVA,'IMAGGSAL',CursAscii1.IMAGGSAL,'IMAGGNUR',CursAscii1.IMAGGNUR,'IMCONIND',CursAscii1.IMCONIND)
      insert into (i_cTable) (IMCODICE,IMSEQUEN,IM_ASCII,IMDESTIN,IMTRASCO,IMTRAMUL,IMAGGPAR,IMAGGANA,IMAGGIVA,IMAGGSAL,IMAGGNUR,IMCONIND,IMFILTRO,IMCHIMOV,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC &i_ccchkf. );
         values (;
           CursAscii1.IMCODICE;
           ,CursAscii1.IMSEQUEN;
           ,CursAscii1.IM_ASCII;
           ,CursAscii1.IMDESTIN;
           ,CursAscii1.IMTRASCO;
           ,CursAscii1.IMTRAMUL;
           ,CursAscii1.IMAGGPAR;
           ,CursAscii1.IMAGGANA;
           ,CursAscii1.IMAGGIVA;
           ,CursAscii1.IMAGGSAL;
           ,CursAscii1.IMAGGNUR;
           ,CursAscii1.IMCONIND;
           ,CursAscii1.IMFILTRO;
           ,CursAscii1.IMCHIMOV;
           ,CursAscii1.IMANNOTA;
           ,CursAscii1.IMPARIVA;
           ,CursAscii1.IMAZZERA;
           ,CursAscii1.IMRESOCO;
           ,CursAscii1.IMCONTRO;
           ,"A";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0380C4C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRACCIAT
    i_nConn=i_TableProp[this.TRACCIAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRACCIAT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TR_ASCII"+",TRDESTIN"+",TRCAMNUM"+",TRCAMDES"+",TRCAMTIP"+",TRCAMLUN"+",TRCAMDEC"+",TRCAMDST"+",TRCAMOBB"+",TRANNOTA"+",TRTRASCO"+",TRTIPSRC"+",TRTIPDES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii3.TRCODICE),'TRACCIAT','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TR_ASCII),'TRACCIAT','TR_ASCII');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRDESTIN),'TRACCIAT','TRDESTIN');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMNUM),'TRACCIAT','TRCAMNUM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDES),'TRACCIAT','TRCAMDES');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMTIP),'TRACCIAT','TRCAMTIP');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMLUN),'TRACCIAT','TRCAMLUN');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDEC),'TRACCIAT','TRCAMDEC');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMDST),'TRACCIAT','TRCAMDST');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRCAMOBB),'TRACCIAT','TRCAMOBB');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRANNOTA),'TRACCIAT','TRANNOTA');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii3.TRTRASCO),'TRACCIAT','TRTRASCO');
      +","+cp_NullLink(cp_ToStrODBC("A"),'TRACCIAT','TRTIPSRC');
      +","+cp_NullLink(cp_ToStrODBC("V"),'TRACCIAT','TRTIPDES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',CursAscii3.TRCODICE,'TR_ASCII',CursAscii3.TR_ASCII,'TRDESTIN',CursAscii3.TRDESTIN,'TRCAMNUM',CursAscii3.TRCAMNUM,'TRCAMDES',CursAscii3.TRCAMDES,'TRCAMTIP',CursAscii3.TRCAMTIP,'TRCAMLUN',CursAscii3.TRCAMLUN,'TRCAMDEC',CursAscii3.TRCAMDEC,'TRCAMDST',CursAscii3.TRCAMDST,'TRCAMOBB',CursAscii3.TRCAMOBB,'TRANNOTA',CursAscii3.TRANNOTA,'TRTRASCO',CursAscii3.TRTRASCO)
      insert into (i_cTable) (TRCODICE,TR_ASCII,TRDESTIN,TRCAMNUM,TRCAMDES,TRCAMTIP,TRCAMLUN,TRCAMDEC,TRCAMDST,TRCAMOBB,TRANNOTA,TRTRASCO,TRTIPSRC,TRTIPDES &i_ccchkf. );
         values (;
           CursAscii3.TRCODICE;
           ,CursAscii3.TR_ASCII;
           ,CursAscii3.TRDESTIN;
           ,CursAscii3.TRCAMNUM;
           ,CursAscii3.TRCAMDES;
           ,CursAscii3.TRCAMTIP;
           ,CursAscii3.TRCAMLUN;
           ,CursAscii3.TRCAMDEC;
           ,CursAscii3.TRCAMDST;
           ,CursAscii3.TRCAMOBB;
           ,CursAscii3.TRANNOTA;
           ,CursAscii3.TRTRASCO;
           ,"A";
           ,"V";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037F1EA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PREDEFIN
    i_nConn=i_TableProp[this.PREDEFIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PREDEFIN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODFIL"+",PRNOMCAM"+",PRTIPCAM"+",PRLUNCAM"+",PRTIPVAL"+",PRVALPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii4.PRCODFIL),'PREDEFIN','PRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii4.PRNOMCAM),'PREDEFIN','PRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii4.PRTIPCAM),'PREDEFIN','PRTIPCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii4.PRLUNCAM),'PREDEFIN','PRLUNCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii4.PRTIPVAL),'PREDEFIN','PRTIPVAL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii4.PRVALPRE),'PREDEFIN','PRVALPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODFIL',CursAscii4.PRCODFIL,'PRNOMCAM',CursAscii4.PRNOMCAM,'PRTIPCAM',CursAscii4.PRTIPCAM,'PRLUNCAM',CursAscii4.PRLUNCAM,'PRTIPVAL',CursAscii4.PRTIPVAL,'PRVALPRE',CursAscii4.PRVALPRE)
      insert into (i_cTable) (PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM,PRTIPVAL,PRVALPRE &i_ccchkf. );
         values (;
           CursAscii4.PRCODFIL;
           ,CursAscii4.PRNOMCAM;
           ,CursAscii4.PRTIPCAM;
           ,CursAscii4.PRLUNCAM;
           ,CursAscii4.PRTIPVAL;
           ,CursAscii4.PRVALPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037F6468()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PREDEIMP
    i_nConn=i_TableProp[this.PREDEIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PREDEIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PICODFIL"+",PINOMCAM"+",PICODIMP"+",PITIPVAL"+",PIVALPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii5.PICODFIL),'PREDEIMP','PICODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii5.PINOMCAM),'PREDEIMP','PINOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii5.PICODIMP),'PREDEIMP','PICODIMP');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii5.PITIPVAL),'PREDEIMP','PITIPVAL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii5.PIVALPRE),'PREDEIMP','PIVALPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PICODFIL',CursAscii5.PICODFIL,'PINOMCAM',CursAscii5.PINOMCAM,'PICODIMP',CursAscii5.PICODIMP,'PITIPVAL',CursAscii5.PITIPVAL,'PIVALPRE',CursAscii5.PIVALPRE)
      insert into (i_cTable) (PICODFIL,PINOMCAM,PICODIMP,PITIPVAL,PIVALPRE &i_ccchkf. );
         values (;
           CursAscii5.PICODFIL;
           ,CursAscii5.PINOMCAM;
           ,CursAscii5.PICODIMP;
           ,CursAscii5.PITIPVAL;
           ,CursAscii5.PIVALPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037D4150()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRCODENT"+",TRTIPTRA"+",TRCODIMP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii6.TRCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii6.TRNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii6.TRCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii6.TRCODENT),'TRASCODI','TRCODENT');
      +","+cp_NullLink(cp_ToStrODBC("G"),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC("Tutti"),'TRASCODI','TRCODIMP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODFIL',CursAscii6.TRCODFIL,'TRNOMCAM',CursAscii6.TRNOMCAM,'TRCODEXT',CursAscii6.TRCODEXT,'TRCODENT',CursAscii6.TRCODENT,'TRTIPTRA',"G",'TRCODIMP',"Tutti")
      insert into (i_cTable) (TRCODFIL,TRNOMCAM,TRCODEXT,TRCODENT,TRTIPTRA,TRCODIMP &i_ccchkf. );
         values (;
           CursAscii6.TRCODFIL;
           ,CursAscii6.TRNOMCAM;
           ,CursAscii6.TRCODEXT;
           ,CursAscii6.TRCODENT;
           ,"G";
           ,"Tutti";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037D89E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRCODIMP"+",TRCODENT"+",TRTIPTRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursAscii7.TICODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii7.TINOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii7.TICODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii7.TICODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(CursAscii7.TICODENT),'TRASCODI','TRCODENT');
      +","+cp_NullLink(cp_ToStrODBC("S"),'TRASCODI','TRTIPTRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODFIL',CursAscii7.TICODFIL,'TRNOMCAM',CursAscii7.TINOMCAM,'TRCODEXT',CursAscii7.TICODEXT,'TRCODIMP',CursAscii7.TICODIMP,'TRCODENT',CursAscii7.TICODENT,'TRTIPTRA',"S")
      insert into (i_cTable) (TRCODFIL,TRNOMCAM,TRCODEXT,TRCODIMP,TRCODENT,TRTIPTRA &i_ccchkf. );
         values (;
           CursAscii7.TICODFIL;
           ,CursAscii7.TINOMCAM;
           ,CursAscii7.TICODEXT;
           ,CursAscii7.TICODIMP;
           ,CursAscii7.TICODENT;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripulisco il file ascii prima di cominciare l'operazione
    * --- Verifica esistenza file da importare
    if file( FileAscii )
      this.w_nHandle = fcreate( FileAscii )
      if this.w_nHandle < 0
        AH_ERRORMSG( "Non � possibile eliminare il file ASCII %1", "!", "" , FileAscii )
      endif
      fclose( this.w_nHandle )
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursore generico
    if used("CursAscii")
      select CursAscii
      use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura su fil ASCII
    select CursAscii
    go top
    COPY TO &FileAscii TYPE SDF
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Cursori
    if used("CursAscii1")
      select CursAscii1
      use
    endif
    if used("CursAscii3")
      select CursAscii3
      use
    endif
    if used("CursAscii4")
      select CursAscii4
      use
    endif
    if used("CursAscii5")
      select CursAscii5
      use
    endif
    if used("CursAscii6")
      select CursAscii6
      use
    endif
    if used("CursAscii7")
      select CursAscii7
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='PREDEFIN'
    this.cWorkTables[4]='PREDEIMP'
    this.cWorkTables[5]='TRACCIAT'
    this.cWorkTables[6]='TRASCODI'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
