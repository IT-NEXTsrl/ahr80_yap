* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bso                                                        *
*              Sorgenti dati ODBC                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_54]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-17                                                      *
* Last revis.: 2013-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bso",oParentObject,m.pDO)
return(i_retval)

define class tgsim_bso as StdBatch
  * --- Local variables
  w_TITOLO = space(100)
  pDO = space(10)
  w_n = 0
  w_nField = 0
  w_ODBCIMPO = space(30)
  w_ContaInseriti = 0
  w_ResMsg = .NULL.
  w_MsgInizio = space(100)
  w_MsgFine = space(100)
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_ANNULLA = .f.
  w_CURSOR = space(50)
  w_INDEX1 = space(3)
  w_INDEX2 = space(3)
  w_OBJECT = .NULL.
  w_DAIM = .f.
  w_ValoreCampo = space(1)
  w_DAIM = .f.
  w_ITER = 0
  w_COUNT = space(2)
  w_COLONNA = space(10)
  * --- WorkFile variables
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SORGENTI DATI ODBC E IMPORTAZIONE FILE EXCEL
    * --- Gestione trascodifiche
    * --- GSIM_KAE
    * --- Importazione da File Excel
    this.w_ODBCIMPO = this.oParentObject.w_ODBCDSN
    * --- Importazione da file Excel
    do case
      case this.pDO="Esegui" OR this.pDO="Trascodifica" OR this.pDO="Carica"
        * --- pDO="Trascodifica" se eseguiamo la frase SQL in GSIM_MTD
        *     pDO="Carica" se stiamo caricando i codici mancanti da GSIM_MTD
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pDO="Copia"
        if this.oParentObject.w_ODBCSQL >= 0
          * --- Inizializza Transitorio
          SELECT (this.oParentObject.cTrsName)
          * --- Lettura Struttura
          Dimension aStruct(1)
          this.w_nField = AFIELDS(aStruct,this.oParentObject.w_ODBCCURSOR)
          FOR this.w_n=1 to this.w_nField
          SELECT (this.oParentObject.cTrsName)
          delete all for empty(t_FLNAME)
          GO TOP
          locate for t_FLNAME=aStruct(this.w_n,1)
          if found()
            this.oParentObject.w_FLCOMMEN = t_FLCOMMEN
          else
            this.oParentObject.InitRow()
          endif
          this.oParentObject.w_FLNAME = aStruct(this.w_n,1)
          this.oParentObject.w_FLTYPE = aStruct(this.w_n,2)
          this.oParentObject.w_FLLENGHT = aStruct(this.w_n,3)
          this.oParentObject.w_FLDECIMA = aStruct(this.w_n,4)
          this.oParentObject.TrsFromWork()
          next
          * --- Questa Parte derivata dal Metodo LoadRec
          SELECT (this.oParentObject.cTrsName)
          GO TOP
          With this.oParentObject
          .WorkFromTrs()
          .SetControlsValue()
          .ChildrenChangeRow()
          .oPgFrm.Page1.oPag.oBody.Refresh()
          .oPgFrm.Page1.oPag.oBody.nAbsRow=1
          .oPgFrm.Page1.oPag.oBody.nRelRow=1
          EndWith
        endif
      case this.pDO="Chiudi"
        * --- Chiude cursore e connessione
        USE IN SELECT (this.oParentObject.w_ODBCCURSOR)
        if this.oParentObject.w_ODBCCONN > 0 AND this.oParentObject.w_FLGFILE<>"S"
          =sqldisconnect(this.oParentObject.w_ODBCCONN)
          this.oParentObject.w_ODBCCONN = -1
          this.oParentObject.w_ODBCSQL = -1
        endif
      case this.pDO="Salva"
        * --- Chiudiamo l'anagrafica delle trascodifiche
        this.w_OBJECT = this.oParentObject
        this.w_OBJECT.ECPSAVE()     
      case this.pDO="Import"
        * --- GSIM_KAE: Carica in automatico tutte le trascodifiche per il tracciato in oggetto
        if Ah_YesNo("Procedere con l'inserimento delle trascodifiche mancanti?")
          * --- Nella query GSIM_BSO estraiamo tutte le odbc del tracciato in esame in cui � necessario effettuare una trascodifica.
          * --- Select from ..\IMPO\EXE\QUERY\GSIM_BSO.VQR
          do vq_exec with '..\IMPO\EXE\QUERY\GSIM_BSO.VQR',this,'_Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR','',.f.,.t.
          if used('_Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR')
            select _Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR
            locate for 1=1
            do while not(eof())
            * --- Dobbiamo leggere da TRASCODI la frase SQL da eseguire. Non � presente nel cursore poich� altrimenti la distinct non funzionerebbe
            this.oParentObject.w_TRCODFIL = TRCODFIL
            this.oParentObject.w_TRTIPTRA = TRTIPTRA
            this.oParentObject.w_TRNOMCAM = TRNOMCAM
            this.oParentObject.w_TRCODIMP = TRCODIMP
            this.oParentObject.w_TRTRAPAR = TRTRAPAR
            * --- Read from TRASCODI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SOSQLSRC"+;
                " from "+i_cTable+" TRASCODI where ";
                    +"TRCODFIL = "+cp_ToStrODBC(this.oParentObject.w_TRCODFIL);
                    +" and TRTIPTRA = "+cp_ToStrODBC(this.oParentObject.w_TRTIPTRA);
                    +" and TRNOMCAM = "+cp_ToStrODBC(this.oParentObject.w_TRNOMCAM);
                    +" and TRCODIMP = "+cp_ToStrODBC(this.oParentObject.w_TRCODIMP);
                    +" and TRTRAPAR = "+cp_ToStrODBC(this.oParentObject.w_TRTRAPAR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SOSQLSRC;
                from (i_cTable) where;
                    TRCODFIL = this.oParentObject.w_TRCODFIL;
                    and TRTIPTRA = this.oParentObject.w_TRTIPTRA;
                    and TRNOMCAM = this.oParentObject.w_TRNOMCAM;
                    and TRCODIMP = this.oParentObject.w_TRCODIMP;
                    and TRTRAPAR = this.oParentObject.w_TRTRAPAR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_SOSQLSRC = NVL(cp_ToDate(_read_.SOSQLSRC),cp_NullValue(_read_.SOSQLSRC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Adesso procediamo con l'esecuzione della frase SQL e con l'analisi del cursore ottenuto
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR
              continue
            enddo
            use
          endif
          if Ah_YesNo("Visualizzare il log di elaborazione?")
            * --- Per sapere se ci sono errori controlliamo w_ResMsg.ah_MsgParts.Count> 0 
            this.w_MsgInizio = ah_MsgFormat("Elenco codici inseriti %0")
            this.w_RESOCON1 = this.w_MsgInizio + this.w_ResMsg.ComposeMessage() + this.w_MsgFine
            this.w_ANNULLA = .F.
            this.w_DAIM = .T.
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if !EMPTY(this.oParentObject.w_ODBCCURSOR)
            USE IN SELECT(this.oParentObject.w_ODBCCURSOR)
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pDO="Trascodifica" se eseguiamo la frase SQL in GSIM_MTD
    *     pDO="Carica" se stiamo caricando i codici mancanti da GSIM_MTD
    USE IN SELECT (this.oParentObject.w_ODBCCURSOR)
    if NOT EMPTY(this.oParentObject.w_ODBCPATH)
      this.oParentObject.w_ODBCPATH = ALLTRIM(this.oParentObject.w_ODBCPATH)
      this.oParentObject.w_ODBCPATH = IIF(RIGHT(this.oParentObject.w_ODBCPATH,1)="\", LEFT(this.oParentObject.w_ODBCPATH, LEN(this.oParentObject.w_ODBCPATH)-1), this.oParentObject.w_ODBCPATH)
    endif
    * --- Verifica se � attiva una connessione ODBC con la sorgente dati. Se non � attiva la crea
    * --- Se il flag di importazione Excel � attivo eseguo la import del file specificato
    if this.oParentObject.w_FLGFILE="S"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if empty(this.oParentObject.w_SOSQLSRC) OR this.oParentObject.w_SOSQLSRC = " "
        this.oParentObject.w_SOSQLSRC = "SELECT * FROM [FOGLIO]"
      else
        if UPPER(this.oParentObject.w_SOSQLSRC) <> "SELECT * FROM [FOGLIO]"
          this.w_INDEX1 = AT("from [", LOWER(this.oParentObject.w_SOSQLSRC))+6
          this.w_INDEX2 = AT("]", LOWER(this.oParentObject.w_SOSQLSRC)) +1
          this.w_INDEX2 = this.w_INDEX2 - this.w_INDEX1
          this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1,this.w_INDEX2,"FOGLIO]")
        endif
      endif
      if this.oParentObject.w_FLGTITOLO<>"S" and UPPER(this.oParentObject.w_SOSQLSRC) = "SELECT * FROM [FOGLIO]"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_INDEX1 = AT("recno()>"+ALLTRIM(STR(this.oParentObject.w_RECNOEXC-1)), LOWER(this.oParentObject.w_SOSQLSRC))
      if this.oParentObject.w_RECNOEXC>0 AND this.w_INDEX1=0
        * --- Controllo se c'� gi� la clausola WHERE
        this.w_INDEX1 = AT("where", LOWER(this.oParentObject.w_SOSQLSRC))
        if this.w_INDEX1 <> 0
          * --- Inserisco RECNO()> e metto ci� che c'era gi� nella WHERE tra parentesi
          this.w_INDEX1 = this.w_INDEX1+5
          this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1,1," RECNO()>"+ALLTRIM(STR(this.oParentObject.w_RECNOEXC-1))+" AND ( " )+" )"
        else
          this.oParentObject.w_SOSQLSRC = this.oParentObject.w_SOSQLSRC+" WHERE RECNO()>"+ALLTRIM(STR(this.oParentObject.w_RECNOEXC-1))
        endif
      endif
      if this.oParentObject.w_FLGTITOLO = "S"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.oParentObject.w_ODBCCONN = 1
    else
      if this.oParentObject.w_ODBCCONN<=0
        this.oParentObject.w_ODBCCONN = sqlconnect(alltrim(this.oParentObject.w_ODBCDSN),alltrim(this.oParentObject.w_ODBCUSER),alltrim(this.oParentObject.w_ODBCPASSW))
        if this.oParentObject.w_ODBCCONN <=0
          AH_ERRORMSG("Impossibile aprire connessione alla sorgente dati %1" , "!" , "" , this.oParentObject.w_ODBCDSN)
        endif
      endif
    endif
    if this.oParentObject.w_ODBCCONN>0
      if EMPTY(this.oParentObject.w_ODBCPATH) and this.oParentObject.w_FLGFILE<>"S"
        this.oParentObject.w_ODBCSQL = sqlprepare(this.oParentObject.w_ODBCCONN,this.oParentObject.w_SOSQLSRC,this.oParentObject.w_ODBCCURSOR)
      else
        if this.oParentObject.w_FLGFILE<>"S"
          OLDP=SYS(5)+SYS(2003)
          NEWP=this.oParentObject.w_ODBCPATH
          CD (NEWP)
        endif
        this.oParentObject.w_ODBCSQL = 1
        NOMECUR=this.oParentObject.w_ODBCCURSOR
      endif
      if this.oParentObject.w_ODBCSQL > 0
        AH_MSG( "Interrogazione in corso..." , .T. )
        this.oParentObject.MousePointer = 11
        if EMPTY(this.oParentObject.w_ODBCPATH) AND this.oParentObject.w_FLGFILE<>"S"
          this.oParentObject.w_ODBCSQL = sqlexec(this.oParentObject.w_ODBCCONN)
        else
          L_Err=.f. 
 L_OLDERROR=On("Error") 
 on error L_err=.t.
          COMMODBC=STRTRAN(this.oParentObject.w_SOSQLSRC, CHR(13)+CHR(10), " ")
          &COMMODBC INTO CURSOR &NOMECUR nofilter
          on error &L_OldError 
          if l_err=.t. and this.oParentObject.w_FLGTITOLO="S"
            ah_errormsg("Errore nell'esecuzione foglio excel, non sono presenti alcuni nomi colonna oppure ci sono nomi colonna che iniziano per caratteri numerici","!","")
            this.oParentObject.w_SOSQLSRC = " "
            this.oParentObject.w_FLGTITOLO = "N"
            use in SELECT("FOGLIO")
            this.bUpdateParentObject = .f.
            i_retcode = 'stop'
            return
          endif
          if this.oParentObject.w_FLGFILE<>"S"
            CD (OLDP)
          else
            use in SELECT("FOGLIO")
          endif
          this.oParentObject.w_FLGTITOLO = "N"
        endif
        if this.pDO="Esegui" AND !empty(this.oParentObject.w_SOROUTRA)
          * --- Eventuale routine di elaborazione cursore, proveniamo da GSIM_MSO
          do (this.oParentObject.w_SOROUTRA) with this, this.oParentObject.w_ODBCCURSOR
        endif
      endif
      if this.oParentObject.w_ODBCSQL >= 0
        if this.pDO<>"Import"
          * --- Nel caso di import proveniamo da GSIM_KAE e NON abbiamo il cursore
          this.oParentObject.oPgFrm.Page3.oPag.oObj_5_1.cCursor = this.oParentObject.w_ODBCCURSOR
          this.oParentObject.oPgFrm.Page3.oPag.oObj_5_1.Browse()
        endif
        * --- Nel caso in cui il parametro sia "Carica" deve procede con l'inserimento in TRASCODI dei codici mancanti
        if this.pDO="Carica" OR this.pDO="Import"
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Gestione import trascodifiche per codice %1, campo %2", this.oParentObject.w_TRCODFIL, this.oParentObject.w_TRNOMCAM))     
          this.w_ContaInseriti = 0
          SELECT (this.oParentObject.w_ODBCCURSOR)
          Dimension aStruct(1)
          this.w_nField = AFIELDS(aStruct,this.oParentObject.w_ODBCCURSOR)
          w_NomeCampo = ALLTRIM( aStruct(1) )
          SCAN
          * --- Presumiamo che nel cursore ci sia un solo campo, o comunque che il primo campo inserito sia il valore da inserire
          this.w_ValoreCampo = &w_NomeCampo
          * --- Try
          local bErr_037BBB00
          bErr_037BBB00=bTrsErr
          this.Try_037BBB00()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Non deve fare nulla, il codice era gi� presente con la relativa trascodifica e quindi non � necessaria alcuna azione
          endif
          bTrsErr=bTrsErr or bErr_037BBB00
          * --- End
          ENDSCAN
          if this.pDO="Carica" AND this.w_ContaInseriti >0 AND Ah_YesNo("Visualizzare il log di elaborazione?")
            * --- Per sapere se ci sono errori controlliamo w_ResMsg.ah_MsgParts.Count> 0 
            this.w_MsgInizio = ah_MsgFormat("Elenco codici inseriti %0")
            this.w_RESOCON1 = this.w_MsgInizio + this.w_ResMsg.ComposeMessage() + this.w_MsgFine
            this.w_ANNULLA = .F.
            this.w_DAIM = .T.
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      else
        AH_ERRORMSG("Errore durante l'esecuzione dell'interrogazione %1%0%0Dettaglio:%0%2","!", "",this.oParentObject.w_ODBCDSN,MESSAGE())
      endif
      wait clear
      this.oParentObject.MousePointer = 0
    endif
  endproc
  proc Try_037BBB00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+",SOSQLSRC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TRTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TRCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TRCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TRNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ValoreCampo),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TRTRAPAR),'TRASCODI','TRTRAPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SOSQLSRC),'TRASCODI','SOSQLSRC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.oParentObject.w_TRTIPTRA,'TRCODIMP',this.oParentObject.w_TRCODIMP,'TRCODFIL',this.oParentObject.w_TRCODFIL,'TRNOMCAM',this.oParentObject.w_TRNOMCAM,'TRCODEXT',this.w_ValoreCampo,'TRTRAPAR',this.oParentObject.w_TRTRAPAR,'SOSQLSRC',this.oParentObject.w_SOSQLSRC)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR,SOSQLSRC &i_ccchkf. );
         values (;
           this.oParentObject.w_TRTIPTRA;
           ,this.oParentObject.w_TRCODIMP;
           ,this.oParentObject.w_TRCODFIL;
           ,this.oParentObject.w_TRNOMCAM;
           ,this.w_ValoreCampo;
           ,this.oParentObject.w_TRTRAPAR;
           ,this.oParentObject.w_SOSQLSRC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ContaInseriti = this.w_ContaInseriti + 1
    this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Inserito codice %1", this.w_ValoreCampo))     
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CURSOR = STRTRAN(JUSTSTEM(this.oParentObject.w_FILEEXC), " ", "_")
    USE IN SELECT (this.w_CURSOR)
    CREATE CURSOR pippo (pluto C(10))
    this.oParentObject.w_FOGLEXC = ALLTRIM(NVL(this.oParentObject.w_FOGLEXC," "))
    L_Err=.f. 
 L_OLDERROR=On("Error") 
 on error L_err=.t.
    * --- I dbf dei file importati andranno nella cartella temporanei di adhoc
    p='"'+TEMPADHOC()+'"'
    CD &p
    if NOT EMPTY(this.oParentObject.w_FOGLEXC)
      IMPORT FROM ALLTRIM(this.oParentObject.w_FILEEXC) XL5 SHEET (this.oParentObject.w_FOGLEXC)
    else
      IMPORT FROM ALLTRIM(this.oParentObject.w_FILEEXC) XLS
    endif
    * --- Torno alla cartella principale
    p='"'+cHomeDir+'"'
    CD &p
    on error &L_OldError 
    if l_err=.t.
      ah_errormsg("Errore di importazione, file o foglio inesistente o gi� in uso o formato excel errato","!","")
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_ODBCCURSOR = this.w_CURSOR
    SELECT * FROM (this.w_CURSOR) INTO CURSOR "FOGLIO"
    USE IN SELECT (this.w_CURSOR)
    USE IN SELECT ("pippo")
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Spostamento nella riga dove ci sono i nomi delle colonne
    SELECT FOGLIO
    For this.w_ITER=1 to this.oParentObject.w_RECNOEXC-2
    skip
    next
    * --- Recupero i nomi di tutte le colonne del file Excel
    this.w_INDEX1 = AT("*",this.oParentObject.w_SOSQLSRC)
    AFIELDS(w_aTABELLA, "FOGLIO")
    this.w_COUNT = ALEN(w_aTABELLA,1)
    this.w_ITER = 1
    do while this.w_ITER<=this.w_COUNT
      * --- Qualsiasi tipo di dato ci sia nel cursore, viene trasformato in stringa e vengono tolti invii, spazi e parentesi
      w_VALORE = "FOGLIO."+ALLTRIM(w_aTABELLA(this.w_ITER,1))
      * --- Utile per non dare errore di argomento non valido alla ALLTRIM
      this.w_TITOLO = IIF(TYPE("&w_VALORE") <> "C", ALLTRIM(TRANSFORM(&w_VALORE)), ALLTRIM(&w_VALORE))
      this.w_TITOLO = STRTRAN( this.w_TITOLO, CHR(10), "_" )
      this.w_TITOLO = STRTRAN( this.w_TITOLO, " " , "_" )
      this.w_TITOLO = STRTRAN( this.w_TITOLO, "(" , "" )
      this.w_TITOLO = STRTRAN( this.w_TITOLO, ")" , "" )
      if this.w_ITER=1
        this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1, 1, w_aTABELLA(this.w_ITER,1)+" AS "+this.w_TITOLO+" ")
      else
        this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1, 0,", "+w_aTABELLA(this.w_ITER,1)+" AS "+this.w_TITOLO+" ")
      endif
      this.w_INDEX1 = AT("from [", LOWER(this.oParentObject.w_SOSQLSRC))
      this.w_ITER = this.w_ITER+1
    enddo
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rinomino le colonne se non � stata specificata nessuna ridenominazione (altrimenti pu� provocare eventuali errori con i nomi delle variabili delle altre procedure)
    * --- Anzich� usare come colonne predefinite A, B, C... raddoppio l'ultima lettera
    this.w_INDEX1 = AT("*",this.oParentObject.w_SOSQLSRC)
    AFIELDS(w_aTABELLA, "FOGLIO")
    this.w_COUNT = ALEN(w_aTABELLA,1)
    this.w_ITER = 1
    do while this.w_ITER<=this.w_COUNT
      this.w_COLONNA = w_aTABELLA(this.w_ITER,1)
      this.w_TITOLO = STUFFC(this.w_COLONNA,LEN(this.w_COLONNA),1,REPLICATE(RIGHT(this.w_COLONNA,1),2))
      if this.w_ITER=1
        this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1, 1, w_aTABELLA(this.w_ITER,1)+" AS "+this.w_TITOLO+" ")
      else
        this.oParentObject.w_SOSQLSRC = STUFFC(this.oParentObject.w_SOSQLSRC,this.w_INDEX1, 0,", "+w_aTABELLA(this.w_ITER,1)+" AS "+this.w_TITOLO+" ")
      endif
      this.w_INDEX1 = AT("from [", LOWER(this.oParentObject.w_SOSQLSRC))
      this.w_ITER = this.w_ITER+1
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pDO)
    this.pDO=pDO
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TRASCODI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR')
      use in _Curs__d__d__IMPO_EXE_QUERY_GSIM_BSO_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDO"
endproc
