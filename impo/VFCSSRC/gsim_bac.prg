* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bac                                                        *
*              Import dati                                                     *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_551]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2018-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsim_bac
* Clean Trasnsaction error
i_TrsMsg=""
bTrsErr=.F.
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bac",oParentObject)
return(i_retval)

define class tgsim_bac as StdBatch
  * --- Local variables
  w_NoFile = .f.
  w_PATH1 = space(200)
  w_ODBCCONN = 0
  w_ODBCSQL = 0
  w_SOSQLSRC = space(0)
  w_SOROUTRA = space(10)
  w_INDEX1 = space(3)
  w_INDEX2 = space(3)
  w_FOGLEXC = space(30)
  w_FILEEXC = space(150)
  w_BATCNAME = space(8)
  w_MSGINFO = space(10)
  w_DATCON = space(4)
  w_VALUT = .f.
  w_DecTot = 0
  w_NzCaoVal = 0
  w_nt = 0
  w_ntd = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableD = space(50)
  w_ODBCIMPO = space(30)
  w_DestErr = 0
  w_Control = .NULL.
  w_CODAZI = space(5)
  w_CODLIT = space(3)
  w_INIESE = ctod("  /  /  ")
  ULT_CHIMOV = space(10)
  ULT_ATT = space(20)
  ULT_SERA = space(20)
  ULT_CHIRIG = space(10)
  ULT_SERI = space(10)
  ULT_NREG = 0
  ULT_PROT = 0
  ULT_TCLF = space(1)
  ULT_CLFO = space(15)
  ULT_RNUM = 0
  ULT_RORD = 0
  ULT_RNUMAN = 0
  ULT_COD1 = 0
  MESS_CTR = 0
  MESS_INT = 0
  w_CHCLIFOR = space(15)
  ULT_INTES = space(16)
  ULT_CONT = space(15)
  ULT_PROG = 0
  w_CFTIPTRA = space(1)
  w_CFCODIMP = space(20)
  w_CFCODFIL = space(2)
  w_CFNOMCAM = space(30)
  w_CFCODEXT = space(50)
  w_CFCODENT = space(0)
  w_CFTRAPAR = space(1)
  w_UNIMESS = .f.
  ULT_CSMOV = space(15)
  w_CPCHIAVE = 0
  w_CPORDINE = 0
  ULT_DBMOV = space(20)
  w_CECOUNT = 0
  w_ULTTIPCON = space(1)
  w_ULTCODCON = space(15)
  w_IMROUTIN = space(10)
  w_ERRDETT = .f.
  ULT_ATTDOCINT = space(20)
  ULT_PREFATT = 0
  SER_PREFATT = 0
  ULT_PREPROF = 0
  SER_PREPROF = 0
  ULT_ACCFATT = 0
  ULT_ACCPROF = 0
  w_tipoimp = space(1)
  w_PLADOC = space(1)
  w_READAZI = space(5)
  w_PAPREANT = space(20)
  w_PAPRESPE = space(20)
  w_DESANT = space(40)
  w_DESSPE = space(40)
  w_XXCONOMA = space(7)
  w_XXCONIOM = space(7)
  w_CLIESI = space(1)
  w_APPONOTE = space(0)
  w___NOME = space(50)
  w_COGNOM = space(50)
  w_nPres = space(10)
  w_CAMPO = space(10)
  w_righe = 0
  w_XXCONOMA = space(7)
  w_XXCONIOM = space(7)
  w_NomeFile = space(20)
  w_NomeFRow = space(20)
  w_QRYASCII = space(20)
  w_QRYDEST = space(2)
  w_DestFile = space(15)
  w_DestFileD = space(15)
  w_DestRow = space(2)
  w_TrascRow = space(1)
  w_Record = space(2)
  w_TrasParz = space(1)
  w_Valore = space(10)
  w_DesCampo = space(8)
  w_Puntatore = 0
  w_Corretto = .f.
  w_Archivio = space(20)
  w_ObblCont = 0
  w_ObblCoRow = 0
  w_Contatore = 0
  w_Messaggio = space(8)
  w_DettaRow = 0
  w_ChiaveRow = space(10)
  w_TipRecAsc = space(1)
  w_TempVar = space(1)
  w_TempVar2 = space(1)
  w_CmFlaVal = space(1)
  w_ContValo = space(1)
  w_PerIva = 0
  w_IvaLis = space(1)
  w_CreVarUte = space(10)
  w_CreVarDat = space(10)
  PreNomCam = space(1)
  ChiavePrec = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_UMCAL = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_ResoMode = space(1)
  w_ResoMess = space(8)
  w_ResoRiga = 0
  w_ResoTipo = space(8)
  w_ResoDett = space(8)
  w_ResoNErr = 0
  NumReco = 0
  NumTotReco = 0
  w_IMAGGSAL = space(1)
  w_MultiTrac = space(1)
  w_IMAGGIVA = space(1)
  w_Destinaz = space(2)
  w_IMAGGPAR = space(1)
  w_Trascodi = space(1)
  w_IMAGGANA = space(1)
  w_Sequenza = 0
  w_IMAGGNUR = space(1)
  w_IM_ASCII = space(20)
  w_IMCONIND = space(15)
  w_IM_ASROW = space(20)
  PreCodFil = space(1)
  PreTipVal = space(1)
  PreCamTip = space(1)
  PreCamLun = space(1)
  PreValPre = space(1)
  PreCurCam = space(1)
  w_FLAVEN = space(1)
  w_FLACOSTO = space(1)
  w_CURSOR = space(50)
  * --- WorkFile variables
  AGENTI_idx=0
  BAN_CHE_idx=0
  CACOCLFO_idx=0
  CAM_AGAZ_idx=0
  CATECOMM_idx=0
  CAUIVA_idx=0
  CAUIVA1_idx=0
  CAUPRI_idx=0
  CAUPRI1_idx=0
  CAU_CONT_idx=0
  COC_MAST_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  IMPOARCH_idx=0
  LINGUE_idx=0
  LISTINI_idx=0
  MASTRI_idx=0
  MOD_CONT_idx=0
  MOD_PAGA_idx=0
  NAZIONI_idx=0
  PAG_2AME_idx=0
  PAG_AMEN_idx=0
  PREDEFIN_idx=0
  PREDEIMP_idx=0
  RESOCONT_idx=0
  SALDICON_idx=0
  SRCMODBC_idx=0
  TRASCODI_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  ZONE_idx=0
  COD_ABI_idx=0
  COD_CAB_idx=0
  CAM_BI_idx=0
  ESERCIZI_idx=0
  CONTATTI_idx=0
  IMPORTAZ_idx=0
  OFF_NOMI_idx=0
  NOM_ATTR_idx=0
  CAT_ATTR_idx=0
  SALDIART_idx=0
  LISTART_idx=0
  TMPSALDI1_idx=0
  ART_ICOL_idx=0
  AZIENDA_idx=0
  PAR_ALTE_idx=0
  COLCRONO_idx=0
  PAR_CAUS_idx=0
  PRE_STAZ_idx=0
  BAN_CONTI_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT DATI
    * --- Fasi di lavoro
    * --- 1) Caricamento dei tracciati di importazione in CursFiles
    * --- 2) Caricamento, per ogni file ascii oppure sorgente dati ODBC, del tracciato record in CursTracc
    * --- 3) Per ogni sorgente dati ODBC viene eseguita l'interrogazione SQL e creato il cursore CursAscii con i dati estratti.
    * --- 3) Per ogni file ascii presente nel tracciato di importazione viene caricato il cursore CursAscii
    * --- 4) Caricamento, in caso di master/detail, di CursAsRow e CursTrRow per le righe di dettaglio
    * --- 5) Scrittura del file di destinazione in adhoc Revolution/Enterprise e continuazione dal punto 2
    * --- Limiti della versione attuale
    * --- Sono gestiti soltanto i campi compresi nel tracciato standard.
    * --- Disabilita form chiamante
    this.oParentObject.Closable = .F.
    this.oParentObject.w_INCORSO = .T.
    this.oParentObject.mEnableControls()
    * --- Disabilita gestione toolbar
    public i_stopFK
    i_stopFK = .T.
    * --- Salva gestione tasti funzionali
    push key clear
    w_FlagESC = .F.
    on key label ESC w_FlagESC=.T.
    * --- Inizio elaborazione
    wait wind "Lettura configurazioni import ..." nowait
    * --- Flag Elaborazione in corso
    * --- Codice tracciati import
    * --- Path relativa per modifica
    * --- Considera personalizzazioni rivenditori
    * --- Abilita compilazione resoconto
    * --- Abilita controllo valori
    * --- Variabile per il riconoscimento degli archivi gestiti
    * --- Ricerca per Partita IVA
    * --- Azzeramento movimenti prima dell'import
    * --- Richiesta conferma transazione
    * --- Scelta se effettuare IMPORT sotto transazione (per archivi grandi ci possono essere probl. di mem.)
    * --- Inizializzazione Variabili locali (La definizione � a pagina 8)
    * --- Nome del file ascii da leggere su disco
    this.w_NomeFile = ""
    * --- Booleana per verificare la presenza del file ASCII
    * --- Nome del file ascii righe da leggere su disco
    this.w_NomeFRow = ""
    * --- Percorso del file ASCII
    this.w_PATH1 = this.oParentObject.w_PATH
    * --- Nome del file ascii utilizzato nella query della lettura del tracciato
    this.w_QRYASCII = ""
    * --- Codice file destinazione utilizzato nella query della lettura del tracciato
    this.w_QRYDEST = ""
    * --- Nome archivio di destinazione
    this.w_DestFile = ""
    * --- Nome archivio di dettaglio di destinazione
    this.w_DestFileD = ""
    * --- Archivio di destinazione per righe dettaglio
    this.w_DestRow = ""
    * --- Utilizzo trascodifiche su righe dettaglio S/N
    this.w_TrascRow = ""
    * --- Record ascii
    * --- Valore estratto dal record Ascii e convertito in stringa da sorgente dati ODBC. Il valore con il tipo origine � in w_ValoreSrc
    this.w_Record = ""
    this.w_Valore = ""
    * --- Valore e descrizione di un campo
    this.w_DesCampo = ""
    * --- Posizione all'interno del record
    this.w_Puntatore = 0
    * --- Record corretto S/N
    this.w_Corretto = .F.
    * --- Nome archivio
    this.w_Archivio = ""
    * --- Contatore campi obbligatori
    this.w_ObblCont = 0
    * --- Contatore campi obbligatori per righe dettaglio
    this.w_ObblCoRow = 0
    * --- Contatore per calcoli temporanei
    this.w_Contatore = 0
    * --- Variabile per testo messaggi di elaborazione
    this.w_Messaggio = ""
    * --- Contatore delle righe scritte nell'archivio di dettaglio
    this.w_DettaRow = 0
    * --- Chiave di ricerca delle righe di dettaglio
    this.w_ChiaveRow = 0
    * --- Tipo record ascii ( 'T' = Normale o testata, 'R' = Riga dettaglio )
    this.w_TipRecAsc = ""
    * --- Variabile di comodo per usi temporanei
    this.w_TempVar = ""
    * --- Variabile di comodo per usi temporanei
    this.w_TempVar2 = ""
    * --- Flag aggiornamento saldi letto da causale magaz. (A, V, ecc.)
    this.w_CmFlaVal = ""
    this.w_ContValo = this.oParentObject.w_SeleCont
    * --- Controllo valori
    this.w_PerIva = 0
    * --- Aliquota iva
    this.w_IvaLis = ""
    * --- Indicatore listino IVA Lordo/Netto
    this.w_CreVarUte = i_codute
    * --- Utente che ha effettuato l'import
    this.w_CreVarDat = SetInfoDate( g_CALUTD )
    * --- Data nella quale � stato effettuato l'import
    this.PreNomCam = ""
    * --- Nome campo per ricerca valori predefiniti
    this.ChiavePrec = "  "
    * --- Tipo Archivio + Chiave registrazione precedente
    * --- Read file e foglio Excel da importare
    * --- Nome batch elaborazione archivio destinazione
    * --- Messaggio wait window
    this.w_MSGINFO = ""
    * --- Esercizio di Consolidamento Saldi Cespiti
    if this.oParentObject.w_TIPDBF="S" 
      if NOT EMPTY(this.oParentObject.w_ODBCPATH)
        this.oParentObject.w_ODBCPATH = ALLTRIM(this.oParentObject.w_ODBCPATH)
        this.oParentObject.w_ODBCPATH = IIF(RIGHT(this.oParentObject.w_ODBCPATH,1)="\", LEFT(this.oParentObject.w_ODBCPATH, LEN(this.oParentObject.w_ODBCPATH)-1), this.oParentObject.w_ODBCPATH)
      endif
    endif
    if g_CESP="S" AND "CESP" $ UPPER(i_cModules) AND this.oParentObject.w_SELECS="CS"
      * --- Lettura dell'Esercizio di Consolidamento dai Parametri Cespiti
      GSCE_BIV(this,"R")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Variabili per gestione valute
    this.w_DecTot = 0
    * --- Numero di decimali nei totali della valuta
    this.w_NzCaoVal = 0
    * --- Cambio valuta nazionale
    * --- Variabili per gestione unita' di misura (la definizione � a pagina 8)
    this.w_UNMIS1 = space(3)
    * --- Unita' di Misura
    this.w_UNMIS2 = space(3)
    this.w_UNMIS3 = space(3)
    this.w_UMCAL = space(1)
    * --- U.M. per calcoli
    this.w_MOLTIP = 0
    * --- Moltiplicatore
    this.w_OPERAT = space(1)
    * --- Operatore
    this.w_OPERA3 = space(1)
    * --- Operatore 3^ UM
    this.w_MOLTI3 = 0
    * --- Moltiplicatore 3^ UM
    * --- Variabili per gestione resoconti (la definizione � a pagina 8)
    * --- Modello di segnalazione
    this.w_ResoMode = ""
    * --- Segnalazione
    this.w_ResoMess = ""
    * --- Contatore delle righe scritte
    this.w_ResoRiga = 0
    * --- Tipo segnalazione
    this.w_ResoTipo = ""
    * --- Dettaglio segnalazione
    this.w_ResoDett = ""
    * --- Numero errori riscontrati
    this.w_ResoNErr = 0
    * --- Numero record per il messaggio di errore sul campo obbligatorio
    this.NumReco = 0
    this.NumTotReco = 0
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    this.w_nt = 0
    this.w_ntd = 0
    * --- Numero connessione
    this.w_nConn = 0
    * --- Nome fisico tabella
    this.w_cTable = ""
    * --- Nome fisico tabella dettaglio
    this.w_cTableD = ""
    * --- ODBC
    this.w_ODBCIMPO = this.oParentObject.w_ODBCDSN
    * --- Gestione messagistica...
     
 Public g_IMPOFLD 
 g_IMPOFLD=""
    * --- Vettori
    dimension ObblCampi[10]
    * --- Campi obbligatori testata e campi obbligatori corpo
    dimension ObblCaRow[10]
    * --- Variabili associate ai campi della configurazione dei tracciati records (la definizione � a pagina 8)
    * --- Creazione del cursore per il resoconto da tenere in memoria (lo rendo scrivibile)
    create cursor CursReso ( CODICE C(20), RIGA N(5), DESCRI M(10), DESTIP C(1), DESDET M(10) )
    a=wrcursor("CursReso")
    if this.oParentObject.w_TIPSRC="O"
      * --- Apre connessione ODBC
      this.w_ODBCCONN = sqlconnect(alltrim(this.oParentObject.w_ODBCDSN),alltrim(this.oParentObject.w_ODBCUSER),alltrim(this.oParentObject.w_ODBCPASSW))
      if this.w_ODBCCONN <=0
        AH_ERRORMSG("Impossibile aprire la connessione alla origine dati " + upper(alltrim(this.oParentObject.w_ODBCDSN)), 48, "Attenzione")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Comincia la transazione per dare la possibilit� all'utente di controllare solo i valori
    if this.oParentObject.w_SeleReso $ "SED"
      * --- Azzeramento resoconto precedente
      * --- Delete from RESOCONT
      i_nConn=i_TableProp[this.RESOCONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RESOCONT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_CODIMP);
               )
      else
        delete from (i_cTable) where;
              RECODICE = this.oParentObject.w_CODIMP;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if this.oParentObject.w_IMTRANSA = "S"
        * --- begin transaction
        cp_BeginTrs()
      endif
      this.w_ResoMode = "SOLOMESS"
      this.w_ResoTipo = "S"
      this.w_ResoMess = AH_MSGFORMAT( "Import archivi iniziato alle ore %1 del giorno %2" , time() , dtoc(i_DatSys) )
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if this.oParentObject.w_IMTRANSA = "S"
        * --- begin transaction
        cp_BeginTrs()
      endif
    endif
    * --- Creazione del cursore per il caricamento dei files ascii
    create cursor CursAscii ( Dati01 C(254) , Dati02 C(254), Dati03 C(254), Dati04 C(254), Dati05 C(254), Dati06 C(254), ;
    Dati07 C(254) , Dati08 C(254), Dati09 C(254), Dati10 C(254) )
    * --- Creazione del cursore dell'elenco dei files ascii
    do case
      case this.oParentObject.w_TIPSRC="O"
        AH_MSG( "Lettura elenco delle sorgenti dati ODBC" , .T. )
      case this.oParentObject.w_TIPSRC = "E"
        AH_MSG( "Lettura elenco dei file EXCEL" , .T. )
      otherwise
        AH_MSG( "Lettura elenco dei file ASCII" , .T. )
    endcase
    * --- Create temporary table LISTART
    i_nIdx=cp_AddTableDef('LISTART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ART_ICOL_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"ARCODART AS KEYSAL, ARPREZUM AS ACQVEN "," from "+i_cTable;
          +" where 1=0";
          )
    this.LISTART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    VQ_EXEC("..\impo\exe\query\gsim_bim.VQR",this,"CursFiles")
    * --- Lettura delle informazioni dei files ascii
    do while .not. eof() and this.w_ResoMode<>"STOP"
      * --- Lettura dati file corrente
      this.w_Sequenza = CursFiles.IMSEQUEN
      this.w_NomeFile = upper( CursFiles.IM_ASCII )
      this.w_NomeFile = strtran( this.w_NomeFile , "XXXXX", i_codazi )
      this.w_IM_ASCII = trim( CursFiles.IM_ASCII )
      this.w_Destinaz = CursFiles.IMDESTIN
      this.w_Trascodi = CursFiles.IMTRASCO
      this.w_MultiTrac = CursFiles.IMTRAMUL
      w_FILTRO = iif( empty( CursFiles.IMFILTRO ), "1=1" , CursFiles.IMFILTRO )
      w_IMCHIMOV = CursFiles.IMCHIMOV
      this.w_IMAGGSAL = CursFiles.IMAGGSAL
      this.w_IMAGGIVA = CursFiles.IMAGGIVA
      this.w_IMAGGPAR = CursFiles.IMAGGPAR
      this.w_IMAGGANA = CursFiles.IMAGGANA
      this.w_IMAGGNUR = CursFiles.IMAGGNUR
      this.w_IMCONIND = CursFiles.IMCONIND
      * --- Non elabora i tracciati delle altre importazioni 
      if this.w_Destinaz $ this.oParentObject.w_Gestiti
        * --- Numero di errori per archivio di destinazione
        this.w_DestErr = this.w_ResoNErr
        * --- Evidenza archivio in fase di importazione
        this.w_Control = this.oParentObject.GetControlObject(alltrim("w_SELE"+this.w_Destinaz),1)
        if type("this.w_Control")="O"
          this.w_Control.BackStyle = 1
          this.w_Control.BackColor = rgb(255,255,0)
          this.w_Control.FontStrikeThru = .F.
          doevent
        endif
        * --- Verifica esistenza file da importare
        if this.oParentObject.w_TIPSRC="O" or this.oParentObject.w_TIPSRC="E" or file( ALLTRIM(this.oParentObject.w_PATH)+this.w_NomeFile )
          * --- Elaborazione del file ascii o sorgente dati odbc e ricalcolo totali ultimo documento importato
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT empty( this.ULT_CHIMOV )
            do case
              case this.w_Destinaz="DR"
                do gsar_brd with this, this.ULT_SERI
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Write into DOC_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                if i_nConn<>0
                  local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                  declare i_aIndex[1]
                  i_cQueryTable=cp_getTempTableName(i_nConn)
                  i_aIndex(1)="MVSERIAL"
                  do vq_exec with 'gsim0bmm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
                  i_cDB=cp_GetDatabaseType(i_nConn)
                  do case
                  case i_cDB="SQLServer"
                    i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"MVCODAG2 = _t2.MVCODAG2";
                      +",MVCODAGE = _t2.MVCODAGE";
                      +",MVCODBA2 = _t2.MVCODBA2";
                      +",MVCODBAN = _t2.MVCODBAN";
                      +",MVCODDES = _t2.MVCODDES";
                      +",MVCODIVE = _t2.MVCODIVE";
                      +",MVCODPAG = _t2.MVCODPAG";
                      +",MVCODPOR = _t2.MVCODPOR";
                      +",MVCODSED = _t2.MVCODSED";
                      +",MVCODSPE = _t2.MVCODSPE";
                      +",MVCODVAL = _t2.MVCODVAL";
                      +",MVCODVE2 = _t2.MVCODVE2";
                      +",MVCODVET = _t2.MVCODVET";
                      +",MVFLSCOR = _t2.MVFLSCOR";
                      +",MVIVAIMB = _t2.MVIVAIMB";
                      +",MVIVAINC = _t2.MVIVAINC";
                      +",MVIVATRA = _t2.MVIVATRA";
                      +",MVNUMCOR = _t2.MVNUMCOR";
                      +",MVRIFDIC = _t2.MVRIFDIC";
                      +",MVCODORN = _t2.MVCODORN";
                      +i_ccchkf;
                      +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
                  case i_cDB="MySQL"
                    i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
                      +"DOC_MAST.MVCODAG2 = _t2.MVCODAG2";
                      +",DOC_MAST.MVCODAGE = _t2.MVCODAGE";
                      +",DOC_MAST.MVCODBA2 = _t2.MVCODBA2";
                      +",DOC_MAST.MVCODBAN = _t2.MVCODBAN";
                      +",DOC_MAST.MVCODDES = _t2.MVCODDES";
                      +",DOC_MAST.MVCODIVE = _t2.MVCODIVE";
                      +",DOC_MAST.MVCODPAG = _t2.MVCODPAG";
                      +",DOC_MAST.MVCODPOR = _t2.MVCODPOR";
                      +",DOC_MAST.MVCODSED = _t2.MVCODSED";
                      +",DOC_MAST.MVCODSPE = _t2.MVCODSPE";
                      +",DOC_MAST.MVCODVAL = _t2.MVCODVAL";
                      +",DOC_MAST.MVCODVE2 = _t2.MVCODVE2";
                      +",DOC_MAST.MVCODVET = _t2.MVCODVET";
                      +",DOC_MAST.MVFLSCOR = _t2.MVFLSCOR";
                      +",DOC_MAST.MVIVAIMB = _t2.MVIVAIMB";
                      +",DOC_MAST.MVIVAINC = _t2.MVIVAINC";
                      +",DOC_MAST.MVIVATRA = _t2.MVIVATRA";
                      +",DOC_MAST.MVNUMCOR = _t2.MVNUMCOR";
                      +",DOC_MAST.MVRIFDIC = _t2.MVRIFDIC";
                      +",DOC_MAST.MVCODORN = _t2.MVCODORN";
                      +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                      +" where "+i_cWhere)
                  case i_cDB="Oracle"
                    i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
                    
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                      +"MVCODAG2,";
                      +"MVCODAGE,";
                      +"MVCODBA2,";
                      +"MVCODBAN,";
                      +"MVCODDES,";
                      +"MVCODIVE,";
                      +"MVCODPAG,";
                      +"MVCODPOR,";
                      +"MVCODSED,";
                      +"MVCODSPE,";
                      +"MVCODVAL,";
                      +"MVCODVE2,";
                      +"MVCODVET,";
                      +"MVFLSCOR,";
                      +"MVIVAIMB,";
                      +"MVIVAINC,";
                      +"MVIVATRA,";
                      +"MVNUMCOR,";
                      +"MVRIFDIC,";
                      +"MVCODORN";
                      +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                      +"t2.MVCODAG2,";
                      +"t2.MVCODAGE,";
                      +"t2.MVCODBA2,";
                      +"t2.MVCODBAN,";
                      +"t2.MVCODDES,";
                      +"t2.MVCODIVE,";
                      +"t2.MVCODPAG,";
                      +"t2.MVCODPOR,";
                      +"t2.MVCODSED,";
                      +"t2.MVCODSPE,";
                      +"t2.MVCODVAL,";
                      +"t2.MVCODVE2,";
                      +"t2.MVCODVET,";
                      +"t2.MVFLSCOR,";
                      +"t2.MVIVAIMB,";
                      +"t2.MVIVAINC,";
                      +"t2.MVIVATRA,";
                      +"t2.MVNUMCOR,";
                      +"t2.MVRIFDIC,";
                      +"t2.MVCODORN";
                      +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                      +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                  case i_cDB="PostgreSQL"
                    i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
                      +"MVCODAG2 = _t2.MVCODAG2";
                      +",MVCODAGE = _t2.MVCODAGE";
                      +",MVCODBA2 = _t2.MVCODBA2";
                      +",MVCODBAN = _t2.MVCODBAN";
                      +",MVCODDES = _t2.MVCODDES";
                      +",MVCODIVE = _t2.MVCODIVE";
                      +",MVCODPAG = _t2.MVCODPAG";
                      +",MVCODPOR = _t2.MVCODPOR";
                      +",MVCODSED = _t2.MVCODSED";
                      +",MVCODSPE = _t2.MVCODSPE";
                      +",MVCODVAL = _t2.MVCODVAL";
                      +",MVCODVE2 = _t2.MVCODVE2";
                      +",MVCODVET = _t2.MVCODVET";
                      +",MVFLSCOR = _t2.MVFLSCOR";
                      +",MVIVAIMB = _t2.MVIVAIMB";
                      +",MVIVAINC = _t2.MVIVAINC";
                      +",MVIVATRA = _t2.MVIVATRA";
                      +",MVNUMCOR = _t2.MVNUMCOR";
                      +",MVRIFDIC = _t2.MVRIFDIC";
                      +",MVCODORN = _t2.MVCODORN";
                      +i_ccchkf;
                      +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                  otherwise
                    i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"MVCODAG2 = (select MVCODAG2 from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODAGE = (select MVCODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODBA2 = (select MVCODBA2 from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODBAN = (select MVCODBAN from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODDES = (select MVCODDES from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODIVE = (select MVCODIVE from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODPAG = (select MVCODPAG from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODPOR = (select MVCODPOR from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODSED = (select MVCODSED from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODSPE = (select MVCODSPE from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODVAL = (select MVCODVAL from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODVE2 = (select MVCODVE2 from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODVET = (select MVCODVET from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVFLSCOR = (select MVFLSCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVIVAIMB = (select MVIVAIMB from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVIVAINC = (select MVIVAINC from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVIVATRA = (select MVIVATRA from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVNUMCOR = (select MVNUMCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
                      +",MVCODORN = (select MVCODORN from "+i_cQueryTable+" where "+i_cWhere+")";
                      +i_ccchkf;
                      +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                  endcase
                  cp_DropTempTable(i_nConn,i_cQueryTable)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_Destinaz="DO"
                * --- Nel caso w_DESTINAZ = "DO" la ransazione � gestita a livello di documento
                if this.w_ERRDETT
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                else
                  do gsar_brd with this, this.ULT_SERI
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Write into DOC_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                  if i_nConn<>0
                    local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                    declare i_aIndex[1]
                    i_cQueryTable=cp_getTempTableName(i_nConn)
                    i_aIndex(1)="MVSERIAL"
                    do vq_exec with 'gsim0bmm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
                    i_cDB=cp_GetDatabaseType(i_nConn)
                    do case
                    case i_cDB="SQLServer"
                      i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVCODAG2 = _t2.MVCODAG2";
                        +",MVCODAGE = _t2.MVCODAGE";
                        +",MVCODBA2 = _t2.MVCODBA2";
                        +",MVCODBAN = _t2.MVCODBAN";
                        +",MVCODDES = _t2.MVCODDES";
                        +",MVCODIVE = _t2.MVCODIVE";
                        +",MVCODPAG = _t2.MVCODPAG";
                        +",MVCODPOR = _t2.MVCODPOR";
                        +",MVCODSED = _t2.MVCODSED";
                        +",MVCODSPE = _t2.MVCODSPE";
                        +",MVCODVAL = _t2.MVCODVAL";
                        +",MVCODVE2 = _t2.MVCODVE2";
                        +",MVCODVET = _t2.MVCODVET";
                        +",MVFLSCOR = _t2.MVFLSCOR";
                        +",MVIVAIMB = _t2.MVIVAIMB";
                        +",MVIVAINC = _t2.MVIVAINC";
                        +",MVIVATRA = _t2.MVIVATRA";
                        +",MVNUMCOR = _t2.MVNUMCOR";
                        +",MVRIFDIC = _t2.MVRIFDIC";
                        +",MVCODORN = _t2.MVCODORN";
                        +i_ccchkf;
                        +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
                    case i_cDB="MySQL"
                      i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
                        +"DOC_MAST.MVCODAG2 = _t2.MVCODAG2";
                        +",DOC_MAST.MVCODAGE = _t2.MVCODAGE";
                        +",DOC_MAST.MVCODBA2 = _t2.MVCODBA2";
                        +",DOC_MAST.MVCODBAN = _t2.MVCODBAN";
                        +",DOC_MAST.MVCODDES = _t2.MVCODDES";
                        +",DOC_MAST.MVCODIVE = _t2.MVCODIVE";
                        +",DOC_MAST.MVCODPAG = _t2.MVCODPAG";
                        +",DOC_MAST.MVCODPOR = _t2.MVCODPOR";
                        +",DOC_MAST.MVCODSED = _t2.MVCODSED";
                        +",DOC_MAST.MVCODSPE = _t2.MVCODSPE";
                        +",DOC_MAST.MVCODVAL = _t2.MVCODVAL";
                        +",DOC_MAST.MVCODVE2 = _t2.MVCODVE2";
                        +",DOC_MAST.MVCODVET = _t2.MVCODVET";
                        +",DOC_MAST.MVFLSCOR = _t2.MVFLSCOR";
                        +",DOC_MAST.MVIVAIMB = _t2.MVIVAIMB";
                        +",DOC_MAST.MVIVAINC = _t2.MVIVAINC";
                        +",DOC_MAST.MVIVATRA = _t2.MVIVATRA";
                        +",DOC_MAST.MVNUMCOR = _t2.MVNUMCOR";
                        +",DOC_MAST.MVRIFDIC = _t2.MVRIFDIC";
                        +",DOC_MAST.MVCODORN = _t2.MVCODORN";
                        +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                        +" where "+i_cWhere)
                    case i_cDB="Oracle"
                      i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
                      
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                        +"MVCODAG2,";
                        +"MVCODAGE,";
                        +"MVCODBA2,";
                        +"MVCODBAN,";
                        +"MVCODDES,";
                        +"MVCODIVE,";
                        +"MVCODPAG,";
                        +"MVCODPOR,";
                        +"MVCODSED,";
                        +"MVCODSPE,";
                        +"MVCODVAL,";
                        +"MVCODVE2,";
                        +"MVCODVET,";
                        +"MVFLSCOR,";
                        +"MVIVAIMB,";
                        +"MVIVAINC,";
                        +"MVIVATRA,";
                        +"MVNUMCOR,";
                        +"MVRIFDIC,";
                        +"MVCODORN";
                        +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                        +"t2.MVCODAG2,";
                        +"t2.MVCODAGE,";
                        +"t2.MVCODBA2,";
                        +"t2.MVCODBAN,";
                        +"t2.MVCODDES,";
                        +"t2.MVCODIVE,";
                        +"t2.MVCODPAG,";
                        +"t2.MVCODPOR,";
                        +"t2.MVCODSED,";
                        +"t2.MVCODSPE,";
                        +"t2.MVCODVAL,";
                        +"t2.MVCODVE2,";
                        +"t2.MVCODVET,";
                        +"t2.MVFLSCOR,";
                        +"t2.MVIVAIMB,";
                        +"t2.MVIVAINC,";
                        +"t2.MVIVATRA,";
                        +"t2.MVNUMCOR,";
                        +"t2.MVRIFDIC,";
                        +"t2.MVCODORN";
                        +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                        +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                    case i_cDB="PostgreSQL"
                      i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
                        +"MVCODAG2 = _t2.MVCODAG2";
                        +",MVCODAGE = _t2.MVCODAGE";
                        +",MVCODBA2 = _t2.MVCODBA2";
                        +",MVCODBAN = _t2.MVCODBAN";
                        +",MVCODDES = _t2.MVCODDES";
                        +",MVCODIVE = _t2.MVCODIVE";
                        +",MVCODPAG = _t2.MVCODPAG";
                        +",MVCODPOR = _t2.MVCODPOR";
                        +",MVCODSED = _t2.MVCODSED";
                        +",MVCODSPE = _t2.MVCODSPE";
                        +",MVCODVAL = _t2.MVCODVAL";
                        +",MVCODVE2 = _t2.MVCODVE2";
                        +",MVCODVET = _t2.MVCODVET";
                        +",MVFLSCOR = _t2.MVFLSCOR";
                        +",MVIVAIMB = _t2.MVIVAIMB";
                        +",MVIVAINC = _t2.MVIVAINC";
                        +",MVIVATRA = _t2.MVIVATRA";
                        +",MVNUMCOR = _t2.MVNUMCOR";
                        +",MVRIFDIC = _t2.MVRIFDIC";
                        +",MVCODORN = _t2.MVCODORN";
                        +i_ccchkf;
                        +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                    otherwise
                      i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVCODAG2 = (select MVCODAG2 from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODAGE = (select MVCODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODBA2 = (select MVCODBA2 from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODBAN = (select MVCODBAN from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODDES = (select MVCODDES from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODIVE = (select MVCODIVE from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODPAG = (select MVCODPAG from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODPOR = (select MVCODPOR from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODSED = (select MVCODSED from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODSPE = (select MVCODSPE from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODVAL = (select MVCODVAL from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODVE2 = (select MVCODVE2 from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODVET = (select MVCODVET from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVFLSCOR = (select MVFLSCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVIVAIMB = (select MVIVAIMB from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVIVAINC = (select MVIVAINC from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVIVATRA = (select MVIVATRA from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVNUMCOR = (select MVNUMCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVCODORN = (select MVCODORN from "+i_cQueryTable+" where "+i_cWhere+")";
                        +i_ccchkf;
                        +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                    endcase
                    cp_DropTempTable(i_nConn,i_cQueryTable)
                  else
                    error "not yet implemented!"
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- commit
                  cp_EndTrs(.t.)
                endif
            endcase
            * --- Si riposizione sul cursore dei tracciati
            select CursFiles
          endif
          * --- Rimuove messaggi a video
          wait clear
        else
          * --- Scrittura resoconto
          this.w_NoFile = .T.
          this.w_ResoMode = "SOLOMESS"
          this.w_ResoTipo = "S"
          this.w_ResoMess = AH_MSGFORMAT( "File ascii %1 non trovato" , ALLTRIM(this.oParentObject.w_PATH)+trim(this.w_NomeFile) )
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if AH_yesno( "Il file ASCII non esiste!%0%0Si desidera interrompere la procedura di import?" , "!" , ALLTRIM(this.oParentObject.w_PATH)+trim(this.w_NomeFile) )
            this.w_ResoMode = "STOP"
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            exit
          endif
        endif
        * --- Rimuove evidenza e segna archivio come eseguito 
        if type("this.w_Control")="O"
          this.w_Control.BackStyle = 0
          this.w_Control.FontStrikeThru = .T.
          doevent
        endif
        * --- Gestione ESC
        if w_FlagESC
          if ah_yesno( "Si desidera interrompere la procedura di importazione ?" )
            this.w_ResoMode = "STOP"
            this.w_ResoMess = AH_MSGFORMAT( "Procedura di import interrotta su richiesta dell'utente, premendo il tasto ESC" )
          else
            w_FlagESC = .F.
          endif
        endif
      endif
      * --- Ricerca del tracciato successivo
      skip
    enddo
    * --- Se selezionato import saldi cespiti lancio la ricostruzione saldi in lire...
    if this.oParentObject.w_SELECS="CS" And g_CESP="S" AND "CESP" $ UPPER(i_CMODULES)
      * --- Lancio la ricostruzione dei saldi lire compressi in Euro
      this.w_CODAZI = i_CODAZI
      this.w_CODLIT = g_CODLIR
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESFINESE  from "+i_cTable+" ESERCIZI ";
            +" where ESCODAZI= "+cp_ToStrODBC(this.w_CODAZI)+" And ESVALNAZ= "+cp_ToStrODBC(this.w_CODLIT)+"";
            +" order by ESFINESE";
             ,"_Curs_ESERCIZI")
      else
        select ESFINESE from (i_cTable);
         where ESCODAZI= this.w_CODAZI And ESVALNAZ= this.w_CODLIT;
         order by ESFINESE;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_INIESE = _Curs_ESERCIZI.ESFINESE
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      GSCE_BRE(this, this.w_INIESE ) 
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (this.w_ResoNErr > 0) or w_FlagESC or (this.oParentObject.w_IMCONFER="S")
      if (this.w_ResoNErr > 0)
        * --- Caso N Errori
        if this.w_ResoMode="STOP"
          AH_ERRORMSG ("Import archivi bloccato dopo n. %1 errori.%0%0Verificare resoconto importazione" , "!" , "" , alltrim(str(this.w_ResoNErr,4)) )
        else
          AH_ERRORMSG ("Import archivi terminato con n. %1 errori.%0%0Verificare resoconto importazione" , "!" , "" , alltrim(str(this.w_ResoNErr,4)) )
        endif
        * --- Riporto il resoconto calcolato in memoria sulla tabella dei resoconti
        if used("__tmp__")
          select __tmp__
          use
        endif
        select CursReso
        go top
        select codice as RECODICE, riga as CPROWNUM, destip as REDESTIP, desdet as REDESDET, descri as REDESCRI from CursReso into cursor __tmp__ NOFILTER
        if used("__tmp__")
          * --- Necessario popkey e i_StopFK per riabilitare tasti funzionali
          pop key
          i_stopFK = .F.
          cp_chprn("..\EXE\QUERY\GSIM_SRE.FRX", " ", this)
          if used("__tmp__")
            select __tmp__
            use
          endif
          push key clear
        endif
      else
        if w_FlagESC
          * --- Caso ESC senza errori
          ah_errormsg( "Import archivi interrotto dall'utente - nessun errore rilevato", "i" ,"" )
        else
          * --- Caso OK
          ah_errormsg( "Import archivi terminato con successo", "i" ,"" )
        endif
      endif
      * --- Transazione terminata con errori o interrota dall'utente (richiesta di conferma modifiche)
      if this.oParentObject.w_IMTRANSA = "S"
        if ah_YesNo("Si vuole confermare l'aggiornamento del database?")
          * --- Transazione non eseguita con successo ma viene convalidato ugualmente l'aggiornamento al DB
          ah_msg( "Aggiornamento database in corso... " , .t. )
          * --- commit
          cp_EndTrs(.t.)
          if this.oParentObject.w_TIPSRC<>"O" AND this.oParentObject.w_TIPSRC<>"E"
            do GSIM_BAZ with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_IMAGGSAL="S"
            this.Pag11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          * --- Eseguo ROLLBACK e scrivo messaggio su resoconti
          this.w_ResoMode = "SOLOMESS"
          this.w_ResoTipo = "S"
          this.w_ResoMess = ah_msgformat( "Eseguito rollback sull'intera importazione" )
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
      else
        if this.w_VALUT
          this.w_ResoMode = "SOLOMESS"
          this.w_ResoTipo = "S"
          this.w_ResoMess = ah_msgformat( "In seguito all'importazione delle valute, � necessario verificare la correttezza delle relative impostazioni in Dati azienda, Esercizi e anagrafica valute, nonch� rilanciare la procedura di conversione 5.0-M5040" )
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_ResoMode = "SOLOMESS"
        this.w_ResoTipo = "S"
        this.w_ResoMess = ah_msgformat( "� stata eseguita una importazione dei dati parziale" )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_TIPSRC<>"O" AND this.oParentObject.w_TIPSRC<>"E"
          do GSIM_BAZ with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_IMAGGSAL="S"
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    else
      if this.oParentObject.w_TIPSRC<>"O" AND this.oParentObject.w_TIPSRC<>"E"
        do GSIM_BAZ with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Transazione eseguita con successo (convalida operazioni)
      if this.oParentObject.w_IMTRANSA = "S"
        ah_msg( "Aggiornamento database in corso ... " , .t. )
        * --- commit
        cp_EndTrs(.t.)
      endif
      if this.w_IMAGGSAL="S"
        this.Pag11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      wait clear
      if NOT this.w_NoFile
        * --- Caso OK
        ah_ERRORMSG ( "Import archivi terminato con successo", "i" , "" )
      else
        * --- Non � stato trovato almeno un file ASCII.
        ah_ERRORMSG ( "Import archivi terminato con successo%0Non � stato possibile importare uno o pi� files ASCII", "i" , "" )
      endif
    endif
    * --- Azzeramento resoconto precedente
    if this.oParentObject.w_SeleReso $ "SED"
      if this.w_VALUT
        this.w_ResoMode = "SOLOMESS"
        this.w_ResoTipo = "S"
        this.w_ResoMess = ah_msgformat( "In seguito all'importazione delle valute, � necessario verificare la correttezza delle relative impostazioni in Dati azienda, Esercizi e anagrafica valute, nonch� rilanciare la procedura di conversione 5.0-M5040" )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_ResoMode = "SOLOMESS"
      this.w_ResoTipo = "S"
      this.w_ResoMess = ah_msgformat( "Import archivi terminato alle ore %1 del giorno %2" , time() , dtoc(i_DatSys) )
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Riporto il resoconto calcolato in memoria sulla tabella dei resoconti
    select CursReso
    go top
    scan
    * --- Try
    local bErr_03030610
    bErr_03030610=bTrsErr
    this.Try_03030610()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03030610
    * --- End
    endscan
    if this.oParentObject.w_TIPSRC="O"
      * --- Chiude connessione ODBC
      if this.w_ODBCCONN > 0
        sqldisconnect(this.w_ODBCCONN)
        this.w_ODBCCONN = 0
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_03030610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RESOCONT
    i_nConn=i_TableProp[this.RESOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RESOCONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",REDESTIP"+",REDESDET"+",REDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursReso.CODICE),'RESOCONT','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursReso.RIGA),'RESOCONT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(CursReso.DESTIP),'RESOCONT','REDESTIP');
      +","+cp_NullLink(cp_ToStrODBC(CursReso.DESDET),'RESOCONT','REDESDET');
      +","+cp_NullLink(cp_ToStrODBC(CursReso.DESCRI),'RESOCONT','REDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',CursReso.CODICE,'CPROWNUM',CursReso.RIGA,'REDESTIP',CursReso.DESTIP,'REDESDET',CursReso.DESDET,'REDESCRI',CursReso.DESCRI)
      insert into (i_cTable) (RECODICE,CPROWNUM,REDESTIP,REDESDET,REDESCRI &i_ccchkf. );
         values (;
           CursReso.CODICE;
           ,CursReso.RIGA;
           ,CursReso.DESTIP;
           ,CursReso.DESDET;
           ,CursReso.DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione dei records letti dal file ascii
    * --- Variabili per gestione rottura movimentazioni
    this.ULT_CHIMOV = space(10)
    * --- Variabili per gestione rottura attivit�
    this.ULT_ATT = space(20)
    * --- Ultima chiave rottura riga attivit�
    this.ULT_SERA = space(20)
    * --- Ultima chiave rottura movimentazione
    this.ULT_CHIRIG = space(10)
    * --- Ultima chiave rottura riga movimentazione
    this.ULT_SERI = space(10)
    * --- Ultimo serial
    this.ULT_NREG = 0
    * --- Ultimo numero registrazione
    this.ULT_PROT = 0
    * --- Ultimo numero protocollo
    this.ULT_TCLF = " "
    * --- Ultimo tipo cliente/fornitore
    this.ULT_CLFO = space(15)
    * --- Ultimo cliente/fornitore
    this.ULT_RNUM = 0
    * --- Ultimo numero di riga
    this.ULT_RORD = 0
    * --- Ultimo cproword
    this.ULT_RNUMAN = 0
    * --- Ultimo numero di riga analitica da primanota
    this.ULT_COD1 = space(50)
    * --- Ultimo COD1 utilizzato per contenere un codice < 50 per rottura
    * --- Variabili per gestione messaggi a video records elaborati
    this.MESS_CTR = 0
    * --- Contatore records elaborati
    this.MESS_INT = 0
    * --- Contatore intervallo di segnalazione
    * --- Chiave Numerica Cliente/Fornitore
    * --- Ultimo Intestatatio Contratto + tipo
    this.ULT_INTES = space(16)
    * --- Ultimo Codice Contratto Originario
    this.ULT_CONT = space(15)
    * --- Ultimo Progressivo contratti
    this.ULT_PROG = 0
    * --- Variabili per Trascodifiche Clienti/Fornitori
    * --- Variabili per gestione rottura Distinta Base
    this.w_UNIMESS = .F.
    * --- Cicli Semplificati
    this.ULT_CSMOV = space(15)
    this.w_CPCHIAVE = 0
    this.w_CPORDINE = 0
    * --- Distinta Base
    this.ULT_DBMOV = space(20)
    * --- Contatore per saldi cespiti
    this.w_CECOUNT = 0
    * --- Variabili per import contatti Cliente/Fornitore
    * --- Variabile per archiviazione eventuale routine di linearizzazione file ASCII
    * --- Variabile per gestione w_DESTINAZ = 'DO'.
    *     Indica se durante l'importazione di un documento si � verificato un errore su almeno una riga
    this.w_ERRDETT = .F.
    * --- Variabile per gestione attivit� documento interno
    this.ULT_ATTDOCINT = space(20)
    * --- Variabile per gestione prestazioni fatturate
    this.ULT_PREFATT = 0
    * --- Variabile per gestione prestazioni fatturate - seriale
    this.SER_PREFATT = 1
    * --- Variabile per gestione prestazioni proformate
    this.ULT_PREPROF = 0
    * --- Variabile per gestione prestazioni proformate - seriale
    this.SER_PREPROF = 1
    * --- Variabile per gestione acconti fatturati
    this.ULT_ACCFATT = 0
    * --- Variabile per gestione acconti proformati
    this.ULT_ACCPROF = 0
    * --- Lettura nome archivio di destinazione e lettura routine di importazione
    this.w_Archivio = ""
    this.w_DestFile = ""
    this.w_BatcName = ""
    * --- Read from IMPOARCH
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.IMPOARCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2],.t.,this.IMPOARCH_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARTABELL,ARDESCRI,ARTABDET,ARROUTIN"+;
        " from "+i_cTable+" IMPOARCH where ";
            +"ARCODICE = "+cp_ToStrODBC(this.w_Destinaz);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARTABELL,ARDESCRI,ARTABDET,ARROUTIN;
        from (i_cTable) where;
            ARCODICE = this.w_Destinaz;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DestFile = NVL(cp_ToDate(_read_.ARTABELL),cp_NullValue(_read_.ARTABELL))
      this.w_Archivio = NVL(cp_ToDate(_read_.ARDESCRI),cp_NullValue(_read_.ARDESCRI))
      this.w_DestFileD = NVL(cp_ToDate(_read_.ARTABDET),cp_NullValue(_read_.ARTABDET))
      this.w_BatcName = NVL(cp_ToDate(_read_.ARROUTIN),cp_NullValue(_read_.ARROUTIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_Archivio = trim( this.w_Archivio )
    this.w_DestFile = trim( this.w_DestFile )
    this.w_DestFileD = trim( this.w_DestFileD )
    this.w_tipoimp = this.oParentObject.w_TIPSRC
    * --- Routine di importazione
    if empty(this.w_BATCNAME)
      do case
        case this.w_DESTINAZ $ "PN|CE|VC|CO|PT|MC"
          * --- Movimenti contabili
          this.w_BATCNAME = "GSIM_BPN"
        case this.w_DESTINAZ $ "CF|ZO|NA|LI|VA|IV|AG|DE|BA|CB|CC|PA|PD|PC|AC|SC|CA|CL|GI|XX|TR|MS|CH|RF|NM|TU|KR|EC"
          * --- Archivi contabili
          this.w_BATCNAME = "GSIM_BAC"
        case this.w_DESTINAZ $ "AR|AN|CM|MA|KA|CD|DC|LS|LA|CN|UM|CP|SM|GM|CG|TT|NO|TC|IN|IC|IS|IF|CI|CX|TO|TA|TM|TP|AA|T2"
          * --- Archivi magazzino
          this.w_BATCNAME = "GSIM_BMA"
        case this.w_DESTINAZ $ "MM|DT|DR|DO"
          * --- Movimenti magazzino e documenti
          this.w_BATCNAME = "GSIM_BMM"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZPLADOC"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZPLADOC;
              from (i_cTable) where;
                  AZCODAZI = this.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PLADOC = NVL(cp_ToDate(_read_.AZPLADOC),cp_NullValue(_read_.AZPLADOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case this.w_DESTINAZ $ "PO|SP|VE|AS" 
          * --- Archivi Spedizione
          this.w_BATCNAME = "GSIM_BAS"
        case this.w_DESTINAZ $ "CR|CT|CS" AND g_CESP="S" AND "CESP" $ UPPER(i_CMODULES)
          * --- Archivi Cespiti
          this.w_BATCNAME = "GSCE_BIM"
        case this.w_DESTINAZ $ "DB|RI|SE|AB|PR" AND g_DISB="S" AND "DISB" $ UPPER(i_CMODULES)
          * --- Distinta Base
          this.w_BATCNAME = "GSDS_BID"
        case this.w_DESTINAZ $ "OG|ST|UB|TI|MP|UP|EP|A1|A2|A3|A4|SD|DP|C1|SO|RS|PI|PE|TL|CU|IP|DG|AT|TF|DI|RU|FP|PS|F1|F2|F3|F4|FT|A5|GU|RA|GN|T1|PM|A6|A7|A8|A9|FR|EV|SN|ON|FY|C2|PZ|IA|TZ|PP" AND IsAlt()
          * --- Alter Ego
          this.w_BATCNAME = "GSAL_BIA"
          if this.w_DESTINAZ="AT"
            * --- Import prestazioni, inseriamo la fattura e la proforma fittizia.
            *     Il batch provvede ad avvalorare ULT_PREFATT e ULT_PREPROF
            do GSAL_BI3 with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_DESTINAZ="GU"
            * --- Select from OFF_NOMI
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select NOCODICE  from "+i_cTable+" OFF_NOMI ";
                  +" order by NOCODICE desc";
                   ,"_Curs_OFF_NOMI")
            else
              select NOCODICE from (i_cTable);
               order by NOCODICE desc;
                into cursor _Curs_OFF_NOMI
            endif
            if used('_Curs_OFF_NOMI')
              select _Curs_OFF_NOMI
              locate for 1=1
              do while not(eof())
              this.ULT_CSMOV = _Curs_OFF_NOMI.NOCODICE
              exit
                select _Curs_OFF_NOMI
                continue
              enddo
              use
            endif
          endif
        case this.w_DESTINAZ $ "BB|BE|BM|BR|BT|BU|BS|BC" AND g_BIBL="S" AND "BIBL" $ UPPER(i_CMODULES)
          * --- Biblioteca
          this.w_BATCNAME = "GSBB_BIM"
        case this.w_DESTINAZ $ "CU|CK|CJ|CV|NC|SI|SN|EL|RG"
          * --- Tabelle F24
          this.w_BATCNAME = "GSIM_BXC"
      endcase
    endif
    * --- Azzeramento contatore righe di dettaglio (per movimentazioni e m/detail)
    this.w_DettaRow = 0
    * --- Definizione campi obbligatori
    this.w_ObblCont = 0
    ObblCampi = ""
    do case
      case this.w_Destinaz = "CF"
        this.w_ObblCont = 6
        ObblCampi[1] = "w_ANTIPCON"
        ObblCampi[2] = "w_ANDESCRI"
        ObblCampi[3] = "w_ANCONSUP"
        ObblCampi[4] = "w_ANCODLIN"
        ObblCampi[5] = "w_ANCATCON"
        ObblCampi[6] = "w_ANCODICE"
      case this.w_Destinaz = "DE"
      case this.w_Destinaz = "PT"
        this.w_ObblCont = 2
        ObblCampi[1] = "w_PTDATSCA"
        ObblCampi[2] = "w_PT_SEGNO"
      case this.w_Destinaz = "RF"
        this.w_ObblCont = 2
        ObblCampi[1] = "w_COTIPCON"
        ObblCampi[2] = "w_COCODCON"
    endcase
    * --- Controllo esistenza record nei Parametri Alterego (PAR_ALTE)
    if this.w_Destinaz = "EC"
      * --- Nel caso di importazione della Causali escluse viene garantita la presenza del codice studio nei Parametri Alterego
      * --- Try
      local bErr_04A7F638
      bErr_04A7F638=bTrsErr
      this.Try_04A7F638()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04A7F638
      * --- End
    endif
    if this.oParentObject.w_TIPSRC="A"
      * --- Caricamento del cursore con i records del file ascii
      select CursAscii
      zap
      FileAscii = ALLTRIM(this.oParentObject.w_PATH)+this.w_NomeFile
      * --- Verifico se � stata inserita una routine di linearizzazione del file
      * --- Read from IMPORTAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMROUTIN"+;
          " from "+i_cTable+" IMPORTAZ where ";
              +"IMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODIMP);
              +" and IMTIPSRC = "+cp_ToStrODBC(this.oParentObject.w_TIPSRC);
              +" and IM_ASCII = "+cp_ToStrODBC(this.w_IM_ASCII);
              +" and IMDESTIN = "+cp_ToStrODBC(this.w_DESTINAZ);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMROUTIN;
          from (i_cTable) where;
              IMCODICE = this.oParentObject.w_CODIMP;
              and IMTIPSRC = this.oParentObject.w_TIPSRC;
              and IM_ASCII = this.w_IM_ASCII;
              and IMDESTIN = this.w_DESTINAZ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMROUTIN = NVL(cp_ToDate(_read_.IMROUTIN),cp_NullValue(_read_.IMROUTIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if ! empty(this.w_IMROUTIN)
        local l_program 
 l_program=substr(this.w_IMROUTIN,1,at("(",this.w_IMROUTIN)-1)+" with this,"+alltrim(substr(this.w_IMROUTIN,at("(",this.w_IMROUTIN)+1,(at(")",this.w_IMROUTIN)-at("(",this.w_IMROUTIN))-1))+",'"+ALLTRIM(this.oParentObject.w_PATH)+"','"+ALLTRIM(FileAscii)+"'" 
 do &l_program
      endif
      append from (FileAscii) type SDF
      go top
    else
      * --- Crea cursore da fonte ODBC o file EXCEL
      select CursAscii
      * --- per svuotare cursore
      select * from CursAscii where 1=2 into cursor CursAscii 
      this.w_SOSQLSRC = ""
      this.w_SOROUTRA = ""
      * --- Read from SRCMODBC
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SRCMODBC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2],.t.,this.SRCMODBC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SOSQLSRC,SOROUTRA,FILEEXC,FOGLEXC"+;
          " from "+i_cTable+" SRCMODBC where ";
              +"SOCODICE = "+cp_ToStrODBC(LEFT(this.w_IM_ASCII,20));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SOSQLSRC,SOROUTRA,FILEEXC,FOGLEXC;
          from (i_cTable) where;
              SOCODICE = LEFT(this.w_IM_ASCII,20);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SOSQLSRC = NVL(cp_ToDate(_read_.SOSQLSRC),cp_NullValue(_read_.SOSQLSRC))
        this.w_SOROUTRA = NVL(cp_ToDate(_read_.SOROUTRA),cp_NullValue(_read_.SOROUTRA))
        this.w_FILEEXC = NVL(cp_ToDate(_read_.FILEEXC),cp_NullValue(_read_.FILEEXC))
        this.w_FOGLEXC = NVL(cp_ToDate(_read_.FOGLEXC),cp_NullValue(_read_.FOGLEXC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_TIPSRC = "E"
        this.Pag12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if !empty(this.w_SOSQLSRC)
        if EMPTY(this.oParentObject.w_ODBCPATH) and this.oParentObject.w_TIPSRC <> "E"
          this.w_ODBCSQL = sqlprepare(this.w_ODBCCONN,this.w_SOSQLSRC,"CursAscii")
        else
          if this.oParentObject.w_TIPSRC <> "E"
            OLDP=SYS(5)+SYS(2003)
            NEWP=this.oParentObject.w_ODBCPATH
            CD (NEWP)
          endif
          this.w_ODBCSQL = 1
        endif
        if this.w_ODBCSQL > 0
          ah_msg( "Interrogazione ODBC in corso..." , .t. )
          this.oParentObject.MousePointer = 11
          if EMPTY(this.oParentObject.w_ODBCPATH) AND this.oParentObject.w_TIPSRC <> "E"
            this.w_ODBCSQL = sqlexec(this.w_ODBCCONN)
          else
            if this.oParentObject.w_TIPSRC = "E"
              SELECT * FROM "FOGLIO" INTO CURSOR CursAscii
            endif
            COMMODBC=STRTRAN(this.w_SOSQLSRC, CHR(13)+CHR(10), " ")
            &COMMODBC INTO CURSOR CursAscii nofilter
            if this.oParentObject.w_TIPSRC <> "E"
              CD (OLDP)
            else
              use in FOGLIO
            endif
          endif
          if !empty(this.w_SOROUTRA)
            do (this.w_SOROUTRA) with this, "CursAscii"
          endif
          if this.w_ODBCSQL < 0
            * --- Messaggio avvertimento
            this.w_Messaggio = AH_msgformat( "Si � verificato un errore durante l'esecuzione dell'interrogazione%0della sorgenti dati ODBC: %1", LEFT(this.w_IM_ASCII,20) )
            AH_ERRORMSG( "Si � verificato un errore durante l'esecuzione dell'interrogazione%0della sorgenti dati ODBC: %1", "!" , "" , LEFT(this.w_IM_ASCII,20) )
            * --- Scrittura resoconto
            this.w_ResoMode = "SOLOMESS"
            this.w_ResoTipo = "E"
            this.w_ResoMess = this.w_Messaggio
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.oParentObject.MousePointer = 0
        wait clear
      endif
    endif
    * --- Creazione del cursore della struttura del files ascii
    AH_MSG( "Lettura tracciato record..." , .T. )
    this.w_QRYASCII = this.w_IM_ASCII
    this.w_QRYDEST = this.w_Destinaz
    VQ_EXEC("..\impo\exe\query\gsim1bim.VQR",this,"CursTracc")
    select CursAscii
    if reccount("CursTracc") > 0
      * --- Calcola ogni quanti records deve essere emesso un messaggio di elaborazione
      this.MESS_INT = reccount()
      this.MESS_INT = iif( this.MESS_INT > 10000, 10, iif( this.MESS_INT > 100, 5, 1 ))
      * --- Apertura file di destinazione per lettura campi tracciato record
      this.w_nT = cp_OpenTable(this.w_DestFile)
      if !empty(this.w_DestFileD)
        this.w_nTD = cp_OpenTable(this.w_DestFileD)
      endif
      if this.w_DESTINAZ = "PZ" and Isalt()
        this.w_READAZI = i_CODAZI
        * --- Read from PAR_ALTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAPRESPE,PAPREANT"+;
            " from "+i_cTable+" PAR_ALTE where ";
                +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAPRESPE,PAPREANT;
            from (i_cTable) where;
                PACODAZI = this.w_READAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PAPRESPE = NVL(cp_ToDate(_read_.PAPRESPE),cp_NullValue(_read_.PAPRESPE))
          this.w_PAPREANT = NVL(cp_ToDate(_read_.PAPREANT),cp_NullValue(_read_.PAPREANT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty(this.w_PAPREANT)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_PAPREANT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = this.w_PAPREANT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESANT = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(this.w_PAPRESPE)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_PAPRESPE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = this.w_PAPRESPE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESSPE = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        w_OBJPRZ=CREATEOBJECT("PrestWriter",this,"PRE_STAZ") 
 w_OBJPRZ.cCODANT=this.w_PAPREANT 
 w_OBJPRZ.cCODSPE=this.w_PAPRESPE 
 w_OBJPRZ.cDESANT=this.w_DESANT 
 w_OBJPRZ.cDESSPE=this.w_DESSPE
      endif
      * --- Import dei record ascii
      select CursAscii
      go bottom
      go top
      this.NumTotReco = reccount()
      this.NumReco = 0
      do while .not. eof() and this.w_ResoMode<>"STOP"
        * --- Mi copio il numero di record elaborato per rilasciarlo nell'errore di chiave obbligatoria
        this.NumReco = this.NumReco + 1
        * --- Messaggio di elaborazione in corso
        if this.NumReco=1 .or. this.MESS_CTR > this.MESS_INT
          this.MESS_CTR = 0
          this.w_MSGINFO = "Elaborazione %1 - record %2 di %3"
          ah_msg( this.w_MSGINFO , .T. , , , trim(this.w_Archivio) , alltrim(str(this.NumReco,10)) , alltrim(str(this.NumTotReco,10)) )
          doevent
        endif
        this.MESS_CTR = this.MESS_CTR + 1
        select CursAscii
        if this.oParentObject.w_TIPSRC<>"A"
          * --- ODBC
          this.w_Record = ""
          this.w_TipRecAsc = "T"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- ASCII
          if .not. empty(Dati01 + Dati02 + Dati03 + Dati04 + Dati05 + Dati06 + Dati07 + Dati08 + Dati09 + Dati10)
            * --- Gestione file ascii con tracciato multiplo (i primi due caratteri devono corrispondere al codice dell'archivio di destinazione)
            if this.w_MultiTrac="N" .or. left( CursAscii.Dati01, 2) = this.w_Destinaz
              * --- Interpretazione record ascii e scrittura in Enterprise
              this.w_Record = CursAscii.Dati01 + CursAscii.Dati02 + CursAscii.Dati03 + CursAscii.Dati04 + CursAscii.Dati05
              this.w_Record = this.w_Record + CursAscii.Dati06 + CursAscii.Dati07 + CursAscii.Dati08 + CursAscii.Dati09
              this.w_Record = this.w_Record + CursAscii.Dati10
              this.w_TipRecAsc = "T"
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        select CursAscii
        skip
      enddo
      if this.w_DESTINAZ = "PZ" AND Isalt()
        w_OBJPRZ=.null.
      endif
      this.w_nT = cp_CloseTable(this.w_DestFile)
      if !empty(this.w_DestFileD)
        this.w_nTD = cp_CloseTable(this.w_DestFileD)
      endif
      if g_CESP="S" AND "CESP" $ UPPER(i_cModules) AND this.oParentObject.w_SELECS="CS"
        * --- Scrittura dell'Esercizio di Consolidamento nei Parametri Cespiti 
        GSCE_BIV(this,"W")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Segnalazione per definizioni del tracciato non trovate
      this.w_TipRecAsc = "T"
      this.w_ResoMode = "NOTRACC"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Si riposizione sul cursore dei tracciati
    select CursFiles
  endproc
  proc Try_04A7F638()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_ALTE
    i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_ALTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PACODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_CODAZI),'PAR_ALTE','PACODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PACODAZI',i_CODAZI)
      insert into (i_cTable) (PACODAZI &i_ccchkf. );
         values (;
           i_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Interpretazione dati record corrente
    * --- Questa pagina del batch viene utilizzata sia per i records di testata che per quelli di dettaglio
    * --- Prestare particolare attenzione nelle personalizzazioni e assicurarsi che la procedura funzioni correttamente in entrambi i casi
    * --- Azzeramento delle variabili non previste dai tracciati standard
    store space(1) to w_EXTIPSOT, w_EXSEZBIL, w_EXCODIVA, w_EXCLFTIP, w_EXTIPREG, w_EXFLPART, w_EXDESCRI, w_EXTIPCON, w_EXTIPCOP
    store space(1) to w_EXMASCON, w_EXCLFNOV, w_EXFLDAVE, w_EX_SEGNO, w_EX__TIPO, w_EXALFDOC, w_EXCODESE
    store space(15) to w_EXCONDET, w_EXCONIND, w_EXCODCLI, w_EXCODFOR, w_EXCLFCON, w_EXCLIFOR, w_EXCODICE
    store space(15) to w_EXCODVOC, w_EXCODCEN, w_EXCODCON, w_EXCONTRO
    store space(14) to w_EXNUMPAR,w_EXKEYINC
    store space(3) to w_EXCODVAL, w_EXCODPAG, w_EXCODBUN
    store space(40) to w_EXRAGSOC
    store space(35) to w_EXINDIRI
    store space(8) to w_EX___CAP
    store space(30) to w_EXLOCALI
    store space(2) to w_EXPROVIN
    store 0 to w_EXNUMREG, w_MCNUMLIV, w_EXDARPRO, w_EXAVEPRO, w_EXDARPER, w_EXAVEPER, w_EXNUMRIG, w_EXPARAME
    store 0 to w_EXVALORE, w_EXVALVAL, w_EXIMPONI, w_EXIMPIVA, w_EXNUMDOC
    store 0 to w_EXIMPON1, w_EXIMPON2, w_EXIMPON3, w_EXIMPON4, w_EXIMPON5, w_EXIMPON6, w_EXIMPON7, w_EXIMPON8, w_EXIMPON9
    store 0 to w_EXIMPIV1, w_EXIMPIV2, w_EXIMPIV3, w_EXIMPIV4, w_EXIMPIV5, w_EXIMPIV6, w_EXIMPIV7, w_EXIMPIV8, w_EXIMPIV9, w_EXIVARAT
    store " " to w_EXCODIV1, w_EXCODIV2, w_EXCODIV3, w_EXCODIV4, w_EXCODIV5, w_EXCODIV6, w_EXCODIV7, w_EXCODIV8, w_EXCODIV9
    store " " to w_CVCONOMV, w_CVCONACQ, w_CVCONOMA, w_CVIVAOMA, w_CVIVAOMC, w_CVCONNCF
    store cp_CharToDate("  -  -  ") to w_EXDATSCA, w_EXDATREG, w_EXDATDOC, w_EXINICOM, w_EXFINCOM
    store space(1) to w_TrasCF
    store "W" to w_EXTIPORI
    store space(15) to w_IDCODATD
    store space(100) to w_IDVALATD
    store space(40) to w_IDDESATD
    STORE " " TO w_PTFLSOSP
    * --- Variabili di Appoggio per le Contropartite Ven./Acq. relative agli Omaggi
    * --- Variabili corpo primanota. Non vengono inizializzate automaticamente perch� scorre solo PNT_MAST.
    * --- Possono essere definite anche aggiungendole in fondo al tracciato. Qui sono state aggiunte per comodita'.
    store 0 to w_PNIMPDAR, w_PNIMPAVE, w_PNLIBGIO, w_PNIMPIND
    store cp_CharToDate("  -  -  ") to w_PNINICOM, w_PNFINCOM
    store " " to w_PNFLPART, w_PNFLZERO, w_PNDESRIG, w_PNCAURIG, w_PNCODPAG
    store " " to w_PNFLSALD, w_PNFLSALI, w_PNFLSALF, w_PNFLABAN, w_PNCODBUN, w_PNCODCON
    * --- Variabili corpo movimenti magazzino. Non vengono inizializzate automaticamente perch� scorre solo MVM_MAST.
    * --- Possono essere definite anche aggiungendole in fondo al tracciato. Qui sono state aggiunte per comodita'.
    store 0 to w_MMQTAMOV, w_MMQTAUM1, w_MMPREZZO, w_MMSCONT1, w_MMSCONT2, w_MMSCONT3, w_MMSCONT4, w_MMVALMAG
    store 0 to w_MMIMPNAZ, w_MMIMPCOM, w_MMVALULT
    store cp_CharToDate("  -  -  ") to w_MMINICOM, w_MMFINCOM
    store " " to w_MMCAUMAG, w_MMCAUCOL, w_MMCODMAG, w_MMCODMAT, w_MMCODLIS, w_MMCODICE, w_MMCODART, w_MMCODVAR
    store " " to w_MMUNIMIS, w_MMFLELGM, w_MMFLELAN, w_MMFLCASC, w_MMF2CASC, w_MMFLORDI, w_MMF2ORDI, w_MMFLIMPE
    store " " to w_MMF2IMPE, w_MMFLRISE, w_MMF2RISE, w_MMFLOMAG,w_MMFLRIPA
    store " " to w_MMCODBUN, w_MMFLLOTT, w_MMF2LOTT,w_FLUBIC,w_F2UBIC,w_FLLOTT
    store " " to w_MMTCOMAG, w_MMTCOMAT, w_MMCODATT, w_MMCODCOS, w_MMCODCOM, w_MMFLORCO, w_MMFLCOCO
    store " " to w_MMFLULPV, w_MMFLULCA, w_MMTIPATT, w_MMCODATT
    store space(20) to w_MMCODUBI,w_MMCODUB2,w_MMCODLOT,w_MMCODCEN,w_MMVOCCEN,w_MMCODCOM
    * --- Variabili per gestione Listino Articoli e Scaglioni
    store 0 to w_LAPREZZO, w_LAQUANTI, w_LASCONT1, w_LASCONT2, w_LASCONT3
    store " " to w_PRCODPRO, w_PRCODFOR
    store "F" to w_PRTIPCON
    * --- Variabili per Articoli
    store 0 to w_PRSCOMIN, w_PRSCOMAX,w_PRDISMIN,w_PRLOTRIO,w_PRGIOINV,w_PRGIOAPP,w_PRCOSSTA
    * --- Variabili per Nominativi
    store " " to w_ANCODCAT, w_ANDESCAT
    * --- Variabili per Giudici
    store " " to w_GIUDICE
    store 1 to w_NUMERO
    * --- Variabile per scrittura dei soli record nuovi (non vengono sovrascritti record gi� esistenti)
    *     Utilizzata sotto la condizione IsAlt() per la scrittura di:
    *     - Codici IVA;
    *     - Piano dei Conti;
    *     - Causali contabili;
    *     - Automatismi.
    store "V" to w_EXSOLNUO
    store space(10) to w_EXCODNUO
    * --- Variabili per la gestione della sovrascrittura dei clienti (non vengono sovrascritti alcuni campi) sotto la condizione IsAlt()
    * --- Azzeramento delle variabili previste dai tracciati standard di adhoc
    * --- Creazione cursore vuoto per lettura tracciato record
    if .not. empty( this.w_DestFile )
      this.w_nConn = i_TableProp[ this.w_nT ,3]
      this.w_cTable = cp_SetAzi(i_TableProp[ this.w_nT ,2])
      * --- SQL Server
      if empty(this.w_DestFileD)
        * --- Solo tabella principale
        SqlExec( this.w_nConn,"select * from " + this.w_cTable + " where 1=2","__trac__")
      else
        * --- Tabella principale e dettaglio
        *     Prima Eseguo la dichiarazione della Principale 
        *     e poi del dettaglio per evitare superamento Limite massimo campi 
        *     Cursori Fox
        SqlExec( this.w_nConn,"select * from " + this.w_cTable + " where 1=2","__trac__")
        if used( "__trac__" )
          select __trac__
          For this.w_TempVar=1 to fcount()
          w_TmpCampo = field( this.w_TempVar )
          w_TmpVaria = "w_"+w_TmpCampo
          &w_TmpVaria = &w_TmpCampo
          next
          select __trac__
          use
        endif
        this.w_cTableD = cp_SetAzi(i_TableProp[ this.w_nTD ,2])
        SqlExec( this.w_nConn,"select * from " + this.w_cTableD + " where 1=2","__trac__")
      endif
      * --- Inizializzazione di una variabile per ogni campo
      if used( "__trac__" )
        select __trac__
        For this.w_TempVar=1 to fcount()
        w_TmpCampo = field( this.w_TempVar )
        w_TmpVaria = "w_"+w_TmpCampo
        &w_TmpVaria = &w_TmpCampo
        next
        select __trac__
        use
      endif
      if this.w_DESTINAZ="DO" AND IsAlt()
        this.w_nPres = cp_OpenTable("PRE_STAZ")
        this.w_nConn = i_TableProp[ this.w_nPres ,3]
        this.w_cTable = cp_SetAzi(i_TableProp[ this.w_nPres ,2])
        SqlExec( this.w_nConn,"select * from " + this.w_cTable + " where 1=2","__trac__")
        if used( "__trac__" )
          select __trac__
          For this.w_TempVar=1 to fcount()
          w_TmpCampo = field( this.w_TempVar )
          w_TmpVaria = "w_"+w_TmpCampo
          &w_TmpVaria = &w_TmpCampo
          next
          select __trac__
          use
        endif
      endif
    endif
    * --- Lettura cursore tracciato record
    this.w_Corretto = .T.
    this.w_Puntatore = 1
    select CursTracc
    go top
    * --- Salva on error standard
    private onErrSave
    onErrSave=on("ERROR")
    do while .not. eof()
      * --- Disattiva on error standard
      on error
      * --- Lettura dati campo corrente
      w_TRCAMDST = NVL(TRCAMDST,"")
      this.w_DesCampo = TRCAMDES
      * --- Calcolo valore campo
      if TRTIPSRG="E"
        * --- EXPRESSIONE
        this.w_Valore = iif(empty(TREXPSRG),"",evaluate(STRTRAN(TREXPSRG,CHR(13)+CHR(10),"")))
      else
        * --- CAMPO oppure RECORD ASCII
        if this.oParentObject.w_TIPSRC<>"A"
          * --- ODBC o EXCEL
          * --- In CursAscii sono presenti i valori estratti via ODBC o Excel
          if empty(nvl(TRCAMSRC,""))
            this.w_Valore = ""
          else
            tCamSrc="CursAscii."+TRCAMSRC
            this.w_Valore = &tCamSrc
            if type("this.w_Valore")="D"
              * --- Patch per driver Visual FoxPro ODBC 
              *     Se la data � vuota viene assegnato il valore 30/12/1899. Riferimento Microsoft Q150433
              if this.w_VALORE=cp_CharToDate("30-12-1899")
                this.w_Valore = cp_CharToDate("  -  -  ")
              endif
            endif
            if type("this.w_Valore")="C" and IsAlt()
              this.w_Valore = STRTRAN(this.w_Valore, CHR(0), "")
            endif
          endif
        else
          * --- ASCII
          this.w_Valore = alltrim(substr( this.w_Record , this.w_Puntatore, TRCAMLUN ))
          if this.w_Puntatore=1
            this.w_ChiaveRow = substr( this.w_Record , this.w_Puntatore, TRCAMLUN )
          endif
        endif
      endif
      * --- Riabilita on error standard
      on error &onErrSave
      * --- Verifica del valore attribuito ad ogni campo
      if this.w_ContValo = "S"
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Incremento puntatore posizione campi
      this.w_Puntatore = this.w_Puntatore + iif(TRTIPSRG="E",0,TRCAMLUN)
      * --- Inizializzazione variabili con valori procedura esterna
      w_EXCLIFOR = iif( alltrim(upper(w_TRCAMDST))="ANCODICE", this.w_Valore, w_EXCLIFOR )
      w_EXTIPSOT = iif( alltrim(upper(w_TRCAMDST))="ANTIPSOT", this.w_Valore , w_EXTIPSOT )
      * --- Trascodifica
      if !empty(this.w_VALORE)
        if !empty(w_TRCAMDST) .and. TRTRASCO="S".and. this.w_Trascodi ="S"
          * --- Se si tratta di un cliente/fornitore nuovo non esegue la trascodifica ed usa il codice esterno per caricare la nuova anagrafica
          if .not. (this.w_Destinaz="CF" .and. alltrim(upper(w_TRCAMDST))="ANCODICE" .and. w_EXCLFNOV="N")
            this.Pag10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      * --- Conversione nel formato previsto dal campo
      if TRCAMDLE=0
        * --- Utilizza impostazioni sorgente dati
        this.w_Valore = this.ToType(this.w_Valore,TRCAMTIP,TRCAMLUN,TRCAMDEC)
      else
        * --- Utilizza impostazioni destinazione
        this.w_Valore = this.ToType(this.w_Valore,TRCAMTIP,TRCAMDLE,TRCAMDDE)
      endif
      * --- Decodifica informazioni da tracciato ascii
      if .not. empty(w_TRCAMDST)
        w_TRCAMDST = "w_"+w_TRCAMDST
        &w_TRCAMDST = this.w_Valore
        * --- Valori predefiniti per campo se � un campo obbligatorio ed � vuoto
        if trcamobb="S" .and. empty( this.w_Valore )
          this.PreNomCam = substr(w_TRCAMDST,3)
          this.Pag9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Verifica campi obbligatori definiti sul tracciato
      if trcamobb="S" .and. empty( this.w_Valore )
        * --- Compilazione messaggio
        * --- Scrittura resoconto
        this.w_ResoMode = "SOLOMESS"
        this.w_ResoTipo = "S"
        this.w_ResoMess = ah_msgformat( "Campo obbligatorio %1 non presente nel tracciato dell'archivio%0%1 nel record numero %2" , trim(trcamdst) , alltrim(str(this.NumRecO,10)) )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Richiesta interruzione import
        this.w_Messaggio = "Campo obbligatorio %1 non presente nel tracciato dell'archivio%0%1 nel record numero %2%0Totale errori riscontrati - %3%0%0Si desidera interrompere la procedura di import?"
        if ah_yesno( this.w_Messaggio , "" , trim(trcamdst) , alltrim(str(this.NumRecO,10)) , alltrim(str(this.w_ResoNErr+1,10)) )
          this.w_ResoMode = "STOP"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_ResoMode = "SOLOMESS"
          this.w_ResoTipo = "S"
          this.w_ResoMess = AH_MSGFORMAT( "Eseguito rollback sull'intera importazione" )
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_IMTRANSA = "S"
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_Corretto = .F.
        this.w_ResoDett = alltrim(str(this.NumRecO,10)) 
        i_TrsMsg = ah_msgformat( "Campo obbligatorio %1 non presente nel tracciato dell'archivio%0%1 nel record numero %2" , trim(trcamdst) , alltrim(str(this.NumRecO,10)) )
      endif
      select CursTracc
      skip
    enddo
    * --- Ripristino area di lavoro
    select CursAscii
    if this.w_Corretto = .T.
      * --- Lettura dei valori predefiniti
      this.PreNomCam = space(1)
      this.Pag9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Gestione records clienti e fornitori
      if this.w_Destinaz="CF"
        do case
          case this.oParentObject.w_IMPARIVA="S" .and. w_TrasCF="S"
            * --- Se � abilitato il controllo della Partita IVA e la trascodifica � parziale (un valore non presente nella trascodifica viene restituitito inalterato)
            w_EXCLIFOR = w_ANCODICE
            w_ANCODICE = space(15)
            * --- Ricerca per Partita IVA
            if .not. empty( w_ANPARIVA )
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                      +" and ANPARIVA = "+cp_ToStrODBC(w_ANPARIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = w_ANTIPCON;
                      and ANPARIVA = w_ANPARIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Ricerca per Codice Fiscale
            if empty( w_ANCODICE) .and. (.not. empty( w_ANCODFIS ))
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                      +" and ANCODFIS = "+cp_ToStrODBC(w_ANCODFIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = w_ANTIPCON;
                      and ANCODFIS = w_ANCODFIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if empty(w_ANCODICE)
              if g_CFNUME="S"
                this.w_CHCLIFOR = SPACE(15)
                * --- Se la ricerca non e' andata a buon fine e la codifica Cliente/Fornitore � Numerica, viene ricercato il primo progressivo libero
                i_Conn=i_TableProp[this.CONTI_IDX, 3]
                * --- Aggiorna progressivo Codice Cliente/Fornitore
                if w_ANTIPCON="C"
                  cp_NextTableProg(this, i_Conn, "PRNUCL", "i_codazi,w_CHCLIFOR")
                else
                  cp_NextTableProg(this, i_Conn, "PRNUFO", "i_codazi,w_CHCLIFOR")
                endif
                w_ANCODICE = this.w_CHCLIFOR
                * --- Aggiusta il Codice Cliente/Fornitore calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica p_CLF
                if LEN(ALLTRIM(p_CLF))<>0
                  w_ANCODICE = RIGHT(w_ANCODICE, LEN(ALLTRIM(p_CLF)))
                endif
              else
                * --- Controlla se il codice alfanumerico � gi� presente
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANCODICE"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                        +" and ANCODICE = "+cp_ToStrODBC(w_EXCLIFOR);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANCODICE;
                    from (i_cTable) where;
                        ANTIPCON = w_ANTIPCON;
                        and ANCODICE = w_EXCLIFOR;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if not empty(w_ANCODICE) and w_EXNUOCLF="N"
                  * --- Se il Codice del Nuovo Cliente/Fornitore � gia presente, viene inserito l'errore nel resoconto
                  * --- Scrittura resoconto
                  this.w_ResoMode = "SCARTO"
                  if w_ANTIPCON="C"
                    this.w_ResoDett = AH_MSGFORMAT( "%1%0Cliente con codifica alfanumerica gi� presente. Impostare una trascodifica" , w_ANCODICE )
                  else
                    this.w_ResoDett = AH_MSGFORMAT( "%1%0Fornitore con codifica alfanumerica gi� presente. Impostare una trascodifica" , w_ANCODICE )
                  endif
                  this.Pag4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  * --- Se il Codice del Nuovo Cliente/Fornitore non � presente o se il Codice � relativo ad un Cliente/Fornitore Variato
                  * --- Aggiorno il Cliente/Fornitore con il Codice originario
                  w_ANCODICE = w_EXCLIFOR
                endif
              endif
            else
              * --- Se il Nuovo Cliente/Fornitore � gi� presente viene inserita una segnalazione nel resoconto
              if w_EXNUOCLF="N"
                * --- Se il Nuovo Cliente/Fornitore � gia presente, viene inserito l'errore nel resoconto
                * --- Scrittura resoconto
                this.w_ResoMode = "SCARTO"
                if w_ANTIPCON="C"
                  this.w_ResoDett = AH_MSGFORMAT( "%1%0Cliente gi� presente. Impostare flag 'Nuovo/Variato' uguale a 'V' per eseguire l'aggiornamento dell'anagrafica" , w_ANCODICE )
                else
                  this.w_ResoDett = AH_MSGFORMAT( "%1%0Fornitore gi� presente. Impostare flag 'Nuovo/Variato' uguale a 'V' per eseguire l'aggiornamento dell'anagrafica" , w_ANCODICE )
                endif
                this.Pag4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            * --- Carica Trascodifica per campo PNCODCLF (archivio di destinazione PN)
            * --- Variabili per Trascodifiche Clienti/Fornitori
            this.w_CFTIPTRA = "S"
            this.w_CFCODIMP = ALLTRIM(this.oParentObject.w_CODIMP)
            this.w_CFCODFIL = "PN"
            this.w_CFNOMCAM = "PNCODCLF"
            this.w_CFCODEXT = ALLTRIM(w_ANTIPCON)+ALLTRIM(w_EXCLIFOR)
            this.w_CFCODENT = ALLTRIM(w_ANCODICE)
            this.w_CFTRAPAR = "S"
            * --- Try
            local bErr_04B5D7C0
            bErr_04B5D7C0=bTrsErr
            this.Try_04B5D7C0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04B5D7C0
            * --- End
            * --- Write into TRASCODI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_CFCODENT),'TRASCODI','TRCODENT');
                  +i_ccchkf ;
              +" where ";
                  +"TRTIPTRA = "+cp_ToStrODBC(this.w_CFTIPTRA);
                  +" and TRCODIMP = "+cp_ToStrODBC(this.w_CFCODIMP);
                  +" and TRCODFIL = "+cp_ToStrODBC(this.w_CFCODFIL);
                  +" and TRNOMCAM = "+cp_ToStrODBC(this.w_CFNOMCAM);
                  +" and TRCODEXT = "+cp_ToStrODBC(this.w_CFCODEXT);
                     )
            else
              update (i_cTable) set;
                  TRCODENT = this.w_CFCODENT;
                  &i_ccchkf. ;
               where;
                  TRTIPTRA = this.w_CFTIPTRA;
                  and TRCODIMP = this.w_CFCODIMP;
                  and TRCODFIL = this.w_CFCODFIL;
                  and TRNOMCAM = this.w_CFNOMCAM;
                  and TRCODEXT = this.w_CFCODEXT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Carica Trascodifica per campo PNCODCON (archivio di destinazione PN)
            * --- Variabili per Trascodifiche Clienti/Fornitori
            this.w_CFNOMCAM = "PNCODCON"
            * --- Try
            local bErr_04B5FB00
            bErr_04B5FB00=bTrsErr
            this.Try_04B5FB00()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04B5FB00
            * --- End
            * --- Write into TRASCODI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_CFCODENT),'TRASCODI','TRCODENT');
                  +i_ccchkf ;
              +" where ";
                  +"TRTIPTRA = "+cp_ToStrODBC(this.w_CFTIPTRA);
                  +" and TRCODIMP = "+cp_ToStrODBC(this.w_CFCODIMP);
                  +" and TRCODFIL = "+cp_ToStrODBC(this.w_CFCODFIL);
                  +" and TRNOMCAM = "+cp_ToStrODBC(this.w_CFNOMCAM);
                  +" and TRCODEXT = "+cp_ToStrODBC(this.w_CFCODEXT);
                     )
            else
              update (i_cTable) set;
                  TRCODENT = this.w_CFCODENT;
                  &i_ccchkf. ;
               where;
                  TRTIPTRA = this.w_CFTIPTRA;
                  and TRCODIMP = this.w_CFCODIMP;
                  and TRCODFIL = this.w_CFCODFIL;
                  and TRNOMCAM = this.w_CFNOMCAM;
                  and TRCODEXT = this.w_CFCODEXT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if IsAlt()
              * --- Carica Trascodifica per campo PTCODCON (archivio di destinazione PT)
              * --- Variabili per Trascodifiche Clienti/Fornitori
              this.w_CFTIPTRA = "S"
              this.w_CFCODIMP = ALLTRIM(this.oParentObject.w_CODIMP)
              this.w_CFCODFIL = "PT"
              this.w_CFNOMCAM = "PTCODCON"
              this.w_CFCODEXT = ALLTRIM(w_ANTIPCON)+ALLTRIM(w_EXCLIFOR)
              this.w_CFCODENT = ALLTRIM(w_ANCODICE)
              this.w_CFTRAPAR = "S"
              * --- Try
              local bErr_04B77A30
              bErr_04B77A30=bTrsErr
              this.Try_04B77A30()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04B77A30
              * --- End
              * --- Write into TRASCODI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TRASCODI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_CFCODENT),'TRASCODI','TRCODENT');
                    +i_ccchkf ;
                +" where ";
                    +"TRTIPTRA = "+cp_ToStrODBC(this.w_CFTIPTRA);
                    +" and TRCODIMP = "+cp_ToStrODBC(this.w_CFCODIMP);
                    +" and TRCODFIL = "+cp_ToStrODBC(this.w_CFCODFIL);
                    +" and TRNOMCAM = "+cp_ToStrODBC(this.w_CFNOMCAM);
                    +" and TRCODEXT = "+cp_ToStrODBC(this.w_CFCODEXT);
                       )
              else
                update (i_cTable) set;
                    TRCODENT = this.w_CFCODENT;
                    &i_ccchkf. ;
                 where;
                    TRTIPTRA = this.w_CFTIPTRA;
                    and TRCODIMP = this.w_CFCODIMP;
                    and TRCODFIL = this.w_CFCODFIL;
                    and TRNOMCAM = this.w_CFNOMCAM;
                    and TRCODEXT = this.w_CFCODEXT;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Carica Trascodifica per campo ANCODICE (archivio di destinazione TU)
              * --- Variabili per Trascodifiche Clienti/Fornitori
              this.w_CFTIPTRA = "S"
              this.w_CFCODIMP = ALLTRIM(this.oParentObject.w_CODIMP)
              this.w_CFCODFIL = "TU"
              this.w_CFNOMCAM = "ANCODICE"
              this.w_CFCODEXT = ALLTRIM(w_ANTIPCON)+ALLTRIM(w_EXCLIFOR)
              this.w_CFCODENT = ALLTRIM(w_ANCODICE)
              this.w_CFTRAPAR = "S"
              * --- Try
              local bErr_04B7CEC0
              bErr_04B7CEC0=bTrsErr
              this.Try_04B7CEC0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04B7CEC0
              * --- End
              * --- Write into TRASCODI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TRASCODI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_CFCODENT),'TRASCODI','TRCODENT');
                    +i_ccchkf ;
                +" where ";
                    +"TRTIPTRA = "+cp_ToStrODBC(this.w_CFTIPTRA);
                    +" and TRCODIMP = "+cp_ToStrODBC(this.w_CFCODIMP);
                    +" and TRCODFIL = "+cp_ToStrODBC(this.w_CFCODFIL);
                    +" and TRNOMCAM = "+cp_ToStrODBC(this.w_CFNOMCAM);
                    +" and TRCODEXT = "+cp_ToStrODBC(this.w_CFCODEXT);
                       )
              else
                update (i_cTable) set;
                    TRCODENT = this.w_CFCODENT;
                    &i_ccchkf. ;
                 where;
                    TRTIPTRA = this.w_CFTIPTRA;
                    and TRCODIMP = this.w_CFCODIMP;
                    and TRCODFIL = this.w_CFCODFIL;
                    and TRNOMCAM = this.w_CFNOMCAM;
                    and TRCODEXT = this.w_CFCODEXT;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Carica Trascodifica per campo ANCODICE (archivio di destinazione CF)
            * --- Variabili per Trascodifiche Clienti/Fornitori
            this.w_CFCODFIL = "CF"
            this.w_CFNOMCAM = "ANCODICE"
            * --- Try
            local bErr_04B62C20
            bErr_04B62C20=bTrsErr
            this.Try_04B62C20()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04B62C20
            * --- End
            * --- Write into TRASCODI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TRASCODI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_CFCODENT),'TRASCODI','TRCODENT');
                  +i_ccchkf ;
              +" where ";
                  +"TRTIPTRA = "+cp_ToStrODBC(this.w_CFTIPTRA);
                  +" and TRCODIMP = "+cp_ToStrODBC(this.w_CFCODIMP);
                  +" and TRCODFIL = "+cp_ToStrODBC(this.w_CFCODFIL);
                  +" and TRNOMCAM = "+cp_ToStrODBC(this.w_CFNOMCAM);
                  +" and TRCODEXT = "+cp_ToStrODBC(this.w_CFCODEXT);
                     )
            else
              update (i_cTable) set;
                  TRCODENT = this.w_CFCODENT;
                  &i_ccchkf. ;
               where;
                  TRTIPTRA = this.w_CFTIPTRA;
                  and TRCODIMP = this.w_CFCODIMP;
                  and TRCODFIL = this.w_CFCODFIL;
                  and TRNOMCAM = this.w_CFNOMCAM;
                  and TRCODEXT = this.w_CFCODEXT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            w_ANCODICE = ALLTRIM(w_ANCODICE)
          case this.oParentObject.w_IMPARIVA="S" .and. this.w_Trascodi="S" .and. empty(w_ANCODICE)
            * --- Se � abilitata la trascodifica e per il codice corrente non e' stata trovata nessuna trascodifica (w_ANCODICE vuoto)
            * --- la procedura prova a cercare il nominativo per partita iva o per codice fiscale.
            if .not. empty( w_ANPARIVA )
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                      +" and ANPARIVA = "+cp_ToStrODBC(w_ANPARIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = w_ANTIPCON;
                      and ANPARIVA = w_ANPARIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if empty( w_ANCODICE) .and. (.not. empty( w_ANCODFIS ))
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                      +" and ANCODFIS = "+cp_ToStrODBC(w_ANCODFIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = w_ANTIPCON;
                      and ANCODFIS = w_ANCODFIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_ANCODICE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Se la ricerca e' andata a buon fine viene caricata automaticamente la trascodifica altrimenti viene segnalato un errore.
            if .not. empty( w_ANCODICE )
              * --- Try
              local bErr_04B912B0
              bErr_04B912B0=bTrsErr
              this.Try_04B912B0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04B912B0
              * --- End
            else
              this.w_ResoMode = "NONTRAS"
              w_TRCAMDST = "ANCODICE"
              w_ValoMess = w_EXCLIFOR
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Corretto = .F.
            endif
          case (.not. empty(w_ANCODICE)) .and. w_EXCLFNOV="N" .and. (.not. empty(w_ANTIPCON))
            * --- Se il cliente/fornitore e' stato passato come nuovo non deve esistere. Se lo trova viene segnalato un errore.
            this.w_TempVar = space(15)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODICE"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODICE;
                from (i_cTable) where;
                    ANTIPCON = w_ANTIPCON;
                    and ANCODICE = w_ANCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TempVar = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if .not. empty( this.w_TempVar )
              this.w_ResoMode = "NONNUOVO"
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_Corretto = .F.
            endif
        endcase
      endif
    endif
    * --- Aggiornamento base dati
    if this.w_Corretto and evaluate(w_Filtro)
      * --- Controllo campi obbligatori che non dipendono dal tracciato del file ascii (campi sempre richiesti da adhoc)
      this.w_TempVar = this.w_ObblCont
      this.w_Contatore = 0
      For this.w_Contatore = 1 to this.w_TempVar
      * --- Calcolo valore campo obbligatorio
      this.w_TempVar2 = eval( ObblCampi[ this.w_Contatore ] )
      * --- Messaggio per campo vuoto
      if empty( this.w_TempVar2 )
        * --- Compilazione messaggio
        * --- Scrittura resoconto
        this.w_ResoMode = "SOLOMESS"
        this.w_ResoTipo = "S"
        this.w_ResoMess = AH_MSGFORMAT( "Campo obbligatorio %1 non presente nel tracciato dell'archivio%0%2 nel record numero %3" , ObblCampi[ this.w_Contatore ] , this.w_Archivio , alltrim(str(this.NumRecO,10)) )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Richiesta interruzione import
        this.w_Messaggio = "Campo obbligatorio %1 non presente nel tracciato dell'archivio%0%2 nel record numero %3%0Totale errori riscontrati - %4%0%0Si desidera interrompere la procedura di import?"
        if ah_yesno( this.w_Messaggio , "" , ObblCampi[ this.w_Contatore ] , this.w_Archivio , alltrim(str(this.NumRecO,10)) ,alltrim(str(this.w_ResoNErr+1,10)) )
          this.w_ResoMode = "STOP"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_ResoMode = "SOLOMESS"
          this.w_ResoTipo = "S"
          this.w_ResoMess = AH_MSGFORMAT( "Eseguito rollback sull'intera importazione" )
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_IMTRANSA = "S"
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_Corretto = .F.
      endif
      next
    endif
    if this.w_Corretto and evaluate(w_Filtro)
      * --- Gestione chiave rottura per righe movimentazioni
      if .not. empty( w_IMCHIMOV )
        this.w_ChiaveRow = &w_IMCHIMOV
      endif
      * --- Aggiornamento archivi
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_Corretto = .F.
      * --- Aggiornamento non riuscito (I dati non erano corretti o non e' stato possibile aggiornare il database di Enterprise)
      * --- Non puo' essere messa una else alla if precedente perche' w_Corretto puo' cambiare anche nelle pagine 6 e 7
      this.w_ResoMode = "SCARTO"
      if evaluate(w_Filtro)
        if .not. empty( w_IMCHIMOV )
          this.w_ChiaveRow = &w_IMCHIMOV
        else
          this.w_ChiaveRow = ""
        endif
      endif
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_04B5D7C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CFTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFTRAPAR),'TRASCODI','TRTRAPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.w_CFTIPTRA,'TRCODIMP',this.w_CFCODIMP,'TRCODFIL',this.w_CFCODFIL,'TRNOMCAM',this.w_CFNOMCAM,'TRCODEXT',this.w_CFCODEXT,'TRTRAPAR',this.w_CFTRAPAR)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
         values (;
           this.w_CFTIPTRA;
           ,this.w_CFCODIMP;
           ,this.w_CFCODFIL;
           ,this.w_CFNOMCAM;
           ,this.w_CFCODEXT;
           ,this.w_CFTRAPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B5FB00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CFTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFTRAPAR),'TRASCODI','TRTRAPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.w_CFTIPTRA,'TRCODIMP',this.w_CFCODIMP,'TRCODFIL',this.w_CFCODFIL,'TRNOMCAM',this.w_CFNOMCAM,'TRCODEXT',this.w_CFCODEXT,'TRTRAPAR',this.w_CFTRAPAR)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
         values (;
           this.w_CFTIPTRA;
           ,this.w_CFCODIMP;
           ,this.w_CFCODFIL;
           ,this.w_CFNOMCAM;
           ,this.w_CFCODEXT;
           ,this.w_CFTRAPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B77A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CFTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFTRAPAR),'TRASCODI','TRTRAPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.w_CFTIPTRA,'TRCODIMP',this.w_CFCODIMP,'TRCODFIL',this.w_CFCODFIL,'TRNOMCAM',this.w_CFNOMCAM,'TRCODEXT',this.w_CFCODEXT,'TRTRAPAR',this.w_CFTRAPAR)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
         values (;
           this.w_CFTIPTRA;
           ,this.w_CFCODIMP;
           ,this.w_CFCODFIL;
           ,this.w_CFNOMCAM;
           ,this.w_CFCODEXT;
           ,this.w_CFTRAPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B7CEC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CFTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFTRAPAR),'TRASCODI','TRTRAPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.w_CFTIPTRA,'TRCODIMP',this.w_CFCODIMP,'TRCODFIL',this.w_CFCODFIL,'TRNOMCAM',this.w_CFNOMCAM,'TRCODEXT',this.w_CFCODEXT,'TRTRAPAR',this.w_CFTRAPAR)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
         values (;
           this.w_CFTIPTRA;
           ,this.w_CFCODIMP;
           ,this.w_CFCODFIL;
           ,this.w_CFNOMCAM;
           ,this.w_CFCODEXT;
           ,this.w_CFTRAPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B62C20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CFTIPTRA),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODIMP),'TRASCODI','TRCODIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODFIL),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFNOMCAM),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFCODEXT),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CFTRAPAR),'TRASCODI','TRTRAPAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',this.w_CFTIPTRA,'TRCODIMP',this.w_CFCODIMP,'TRCODFIL',this.w_CFCODFIL,'TRNOMCAM',this.w_CFNOMCAM,'TRCODEXT',this.w_CFCODEXT,'TRTRAPAR',this.w_CFTRAPAR)
      insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
         values (;
           this.w_CFTIPTRA;
           ,this.w_CFCODIMP;
           ,this.w_CFCODFIL;
           ,this.w_CFNOMCAM;
           ,this.w_CFCODEXT;
           ,this.w_CFTRAPAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B912B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRCODENT"+",TRTIPTRA"+",TRCODIMP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_Destinaz),'TRASCODI','TRCODFIL');
      +","+cp_NullLink(cp_ToStrODBC("ANCODICE"),'TRASCODI','TRNOMCAM');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCLIFOR),'TRASCODI','TRCODEXT');
      +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'TRASCODI','TRCODENT');
      +","+cp_NullLink(cp_ToStrODBC("S"),'TRASCODI','TRTIPTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMP),'TRASCODI','TRCODIMP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODFIL',this.w_Destinaz,'TRNOMCAM',"ANCODICE",'TRCODEXT',w_EXCLIFOR,'TRCODENT',w_ANCODICE,'TRTIPTRA',"S",'TRCODIMP',this.oParentObject.w_CODIMP)
      insert into (i_cTable) (TRCODFIL,TRNOMCAM,TRCODEXT,TRCODENT,TRTIPTRA,TRCODIMP &i_ccchkf. );
         values (;
           this.w_Destinaz;
           ,"ANCODICE";
           ,w_EXCLIFOR;
           ,w_ANCODICE;
           ,"S";
           ,this.oParentObject.w_CODIMP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento archivio resoconti
    * --- Gestione tipo messaggio
    * --- Se il tipo non viene specificato devono essere inizializzate le variabili prima della chiamata a questa pagina
    do case
      case this.w_ResoMode = "STOP"
        * --- Interruzione procedura import
        this.w_ResoTipo = "E"
        this.w_ResoDett = ""
        this.w_ResoMess = AH_MSGFORMAT( "Procedura di import interrotta dall'operatore" )
      case this.w_ResoMode = "SOLOMESS"
        * --- Solo messaggio (compilato prima della chiamata)
        this.w_ResoDett = ""
      case this.w_ResoMode = "SCRITTURA"
        * --- Resoconto scrittura record
        this.w_ResoTipo = "S"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Scritto il record numero %1 della sorgente dati %2 in %3" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Scritto il record numero %1 del file %2 in %3" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Scritto il record numero %1 con chiave %2%0 della sorgente dati %3 in %4" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Scritto il record numero %1 con chiave%2%0 del file %3 in %4" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio )
          endif
        endif
        this.w_ResoDett = ah_msgformat( "Chiave %1" , NVL(this.w_ResoDett,"<NULL>") )
      case this.w_ResoMode = "SCARTO"
        * --- Resoconto scarto record
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Scartato il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Scartato il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Scartato il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Scartato il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = NVL(this.w_ResoDett,"<NULL>")
        this.w_ResoDett = ah_msgformat( "Errore di scrittura o dati non corretti per record %1" , iif(empty(this.w_ResoDett),"",chr(13)+this.w_ResoDett) + iif( type("i_trsmsg")="C" , chr(13)+cp_translateDBMessage(i_trsmsg, .T. ) ,"") )
        if VarType( g_IMPOFLD )="C" And Not Empty( g_IMPOFLD )
          * --- Il meccanismo di traduazione automatica ha individuato uno o pi� campi
          *     separati da virgole in cui si trova il dato che ha generato il problema sul
          *     database
          g_IMPOFLD= g_IMPOFLD + ","
          do while At(",", g_IMPOFLD )>0
            this.w_CAMPO = Left( g_IMPOFLD , At(",", g_IMPOFLD ) -1)
            L_Macro= "w_"+this.w_CAMPO
            olderror = on ("ERROR") 
 on error valcampo = " "
            valcampo = &L_Macro
            on error &olderror
            if vartype(L_Macro) # "U" and not empty(valcampo)
              this.w_ResoDett = this.w_ResoDett + ah_msgformat( "%0%1=%2" , this.w_CAMPO , valcampo )
            endif
            g_IMPOFLD= Substr( g_IMPOFLD , At(",", g_IMPOFLD )+1)
          enddo
        endif
      case this.w_ResoMode = "NONSUPP"
        * --- Resoconto archivi non supportati
        this.w_ResoTipo = "E"
        this.w_ResoMess = AH_MSGFORMAT( "Import archivio %1 (tabella %2) non supportato" , trim(this.w_Archivio) , this.w_DestFile )
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoDett = AH_MSGFORMAT( "Record numero %1 con chiave %2 della sorgente dati %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        else
          this.w_ResoDett = AH_MSGFORMAT( "Record numero %1 con chiave %2 del file %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        endif
      case this.w_ResoMode = "NONTRAS"
        * --- Resoconto campo senza trascodifica
        this.w_ResoTipo = "E"
        this.w_ResoMess = ah_msgformat( "Trascodifica mancante per il campo %1 dell'archivio %2 (tabella %3)" , trim(w_TRCAMDST) , trim(this.w_Archivio) , this.w_DestFile )
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 della sorgente dati %3%0Valore %4" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) , iif( empty(w_ValoMess), "vuoto", w_ValoMess) )
        else
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 del file %3%0Valore %4" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) , iif( empty(w_ValoMess), "vuoto", w_ValoMess) )
        endif
      case this.w_ResoMode = "CONTIRAGG"
        * --- Resoconto per errore in raggruppamento conti o mastri
        this.w_ResoTipo = "E"
        if right( alltrim( w_ANCODICE ) , 3) # "000"
          this.w_ResoMess = ah_msgformat( "Mastro di raggruppamento errato per il conto %1" , trim(w_ANCODICE) )
        else
          this.w_ResoMess = ah_msgformat( "Mastro di raggruppamento errato per il mastro %1" , trim(w_ANCODICE) )
        endif
        if empty( w_ANCONSUP )
          this.w_ResoMess = AH_ERRORMSG( "%1%0Il record � stato caricato senza raggruppamento" ,48,"", this.w_ResoMess )
        else
          this.w_ResoMess = AH_ERRORMSG( "%1%0Il record � stato caricato ricalcolando il codice di raggruppamento come %2" ,48,"", this.w_ResoMess , trim(w_ANCONSUP) )
        endif
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 della sorgente dati %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        else
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 del file %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        endif
      case this.w_ResoMode = "CONTITIPO"
        * --- Conto senza tipo sottoconto
        this.w_ResoTipo = "S"
        this.w_ResoMess = AH_MSGFORMAT( "Conto %1 senza tipo conto%0Il record � stato caricato assegnando il tipo generico" , trim(w_ANCODICE) )
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 della sorgente dati %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        else
          this.w_ResoDett = ah_msgformat( "Record numero %1 con chiave %2 del file %3" , alltrim(str(this.NumReco,10)) , NVL(this.w_ResoDett,"<NULL>") , alltrim(this.w_IM_ASCII) )
        endif
      case this.w_ResoMode = "NOTRACC"
        * --- Resoconto per definizioni di tracciato non trovate
        this.w_ResoTipo = "E"
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoMess = ah_msgformat( "Non esiste la definizione del tracciato record della sorgente dati %1%0Archivio di destinazione %2 (tabella %3)" , alltrim(this.w_IM_ASCII) , trim(this.w_Archivio) , this.w_DestFile )
        else
          this.w_ResoMess = ah_msgformat( "Non esiste la definizione del tracciato record del file %1%0Archivio di destinazione %2 (tabella %3)" , alltrim(this.w_IM_ASCII) , trim(this.w_Archivio) , this.w_DestFile )
        endif
        this.w_ResoDett = AH_MSGFORMAT( "La tabella dei tracciati records non � stata configurata correttamente" )
      case this.w_ResoMode = "NONNUOVO"
        * --- Cliente/fornitore passato come nuovo ma gia' presente in archivio
        this.w_ResoTipo = "E"
        if this.oParentObject.w_TIPSRC="O"
          this.w_ResoMess = ah_msgformat( "Scartato il record numero %1della sorgente dati %2" , alltrim(str(this.NumReco,10)) , alltrim(this.w_IM_ASCII) )
        else
          this.w_ResoMess = ah_msgformat( "Scartato il record numero %1del file %2" , alltrim(str(this.NumReco,10)) , alltrim(this.w_IM_ASCII) )
        endif
        if w_ANTIPCON="C"
          this.w_ResoDett = ah_msgformat( "Nuovo cliente con codice %1 gi� presente in archivio" , trim(w_ANCODICE) )
        else
          this.w_ResoDett = ah_msgformat( "Nuovo fornitore con codice %1 gi� presente in archivio" , trim(w_ANCODICE) )
        endif
      case this.w_ResoMode = "NOESEANA"
        this.w_ResoTipo = "E"
        this.w_ResoMess = ah_msgformat( "Esercizio di primo utilizzo del cespite %1 non presente nella tabella degli esercizi" , trim(w_CECODICE) )
      case this.w_ResoMode = "NOESESAL"
        this.w_ResoTipo = "E"
        this.w_ResoMess = ah_msgformat( "Esercizio %1 dei saldi cespiti %2 non presente nella tabella degli esercizi" , trim(w_SCCODESE) , trim(w_SCCODCES) )
      case this.w_ResoMode = "NOCMANUT"
        this.w_ResoTipo = "E"
        this.w_ResoMess = ah_msgformat( "Conto Manutenzione %1 delle categorie cespiti %2 non presente nella tabella dei conti" , trim(w_CCCONMAN) , trim(w_CCCODICE) )
      case this.w_ResoMode = "NOUNIMIS"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Il servizio FUORI MAGAZZINO � utilizzato senza l'unit� di misura o con un'unit� di misura incongruente" )
      case this.w_ResoMode = "NOCONTRO"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Le contropartite sono valorizzate con uno spazio perch� almeno una non � presente nel piano dei conti" )
      case this.w_ResoMode="NOCODPAG"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Manca il codice del pagamento nell'archivio dei clienti/fornitori" )
      case this.w_ResoMode="NOCODCON"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Manca il codice del conto in primanota" )
      case this.w_ResoMode="NOCAUCON"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Manca la causale contabile in primanota" )
      case this.w_ResoMode="NOMODPAG" AND this.w_UNIMESS=.F.
        * --- Partita senza tipo pagamento (Segnalare solo una volta)
        this.w_UNIMESS = .T.
        this.w_ResoTipo = "E"
        this.w_ResoMess = AH_MSGFORMAT( "Esistono partite senza tipo del pagamento%0Verificare in manutenzione partite/scadenze" )
        this.w_ResoDett = AH_MSGFORMAT( "Tipo del pagamento non definito nelle partite" )
      case this.w_ResoMode="NOANACOM"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Codice della commessa o codice della voce di costo non valorizzato" )
      case this.w_ResoMode="NOANALIT"
        this.w_ResoTipo = "E"
        if empty(this.w_ChiaveRow)
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 della sorgente dati %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 del file %2%0Archivio di destinazione %3 (tabella %4)" , alltrim(str(this.NumReco,10)) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        else
          if this.oParentObject.w_TIPSRC="O"
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave %2%0 della sorgente dati %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          else
            this.w_ResoMess = AH_MSGFORMAT( "Controllare il record numero %1 con chiave%2%0 del file %3%0Archivio di destinazione %4 (tabella %5)" , alltrim(str(this.NumReco,10)) , alltrim(this.w_ChiaveRow) , this.w_IM_ASCII , this.w_Archivio , this.w_DestFile )
          endif
        endif
        this.w_ResoDett = AH_MSGFORMAT( "Codice del centro di costo o codice della voce di costo non valorizzato" )
    endcase
    * --- Incremento contatore righe scritte
    this.w_ResoRiga = this.w_ResoRiga + 1
    * --- Incremento contatore errori riscontrati
    if this.w_ResoTipo = "E"
      this.w_ResoNErr = this.w_ResoNErr + 1
    endif
    * --- Gestione Stop dopo 10 errori
    if this.oParentObject.w_SeleReso="D" and this.w_ResoNErr >= 10
      * --- Interruzione procedura import
      this.w_ResoMode = "STOP"
      this.w_ResoMess = AH_MSGFORMAT( "Procedura di import interrotta dopo 10 errori" )
    endif
    * --- Verifica se si tratta di una segnalazione o di un errore da riportare nel resoconto
    TmpOkRes = .F.
    if this.oParentObject.w_SeleReso = "S"
      * --- Resoconto per segnalazioni ed errori
      TmpOkRes = .T.
    else
      * --- Resoconto per errori
      if this.oParentObject.w_SeleReso $ "ED"
        if this.w_ResoTipo = "E"
          * --- Considera tutti gli errori
          TmpOkRes = .T.
        else
          * --- Riporta anche le righe di inizio e fine import e le segnalazioni piu' importanti
          do case
            case left(upper( this.w_ResoMess ),23) = "IMPORT ARCHIVI INIZIATO"
              TmpOkRes = .T.
            case left(upper( this.w_ResoMess ),24) = "IMPORT ARCHIVI TERMINATO"
              TmpOkRes = .T.
            case left(upper( this.w_ResoMess ),35) = "MANCA IL TOTALE DOCUMENTO IN VALUTA"
              TmpOkRes = .T.
            case left(upper( this.w_ResoMess ),17) = "ESEGUITO ROLLBACK"
              TmpOkRes = .T.
          endcase
        endif
      endif
    endif
    * --- Gestione del messaggio 'cpccchk non modificato'
    if upper(this.w_ResoDett)="CPCCCHK NON MODIFICATO"
      this.w_ResoDett = AH_MSGFORMAT( "%1%0Probabilmente e' stato impostato in modo non corretto il tipo di un campo definito nell'archivio dei tracciati records. Il caso tipico e' quello di un campo numerico definito come carattere" , this.w_ResoDett )
    endif
    * --- Aggiornamento resoconto
    if TmpOkRes
      * --- Try
      local bErr_04C0E508
      bErr_04C0E508=bTrsErr
      this.Try_04C0E508()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04C0E508
      * --- End
    endif
    this.w_ResoDett = ""
    this.w_ResoMode = iif(this.w_ResoMode=="STOP",this.w_ResoMode," ")
  endproc
  proc Try_04C0E508()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    INSERT INTO CursReso (CODICE, RIGA, DESCRI, DESTIP, DESDET) ;
    VALUES (this.oParentObject.w_CODIMP,this.w_ResoRiga,this.w_ResoMess,this.w_ResoTipo,this.w_ResoDett)
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Fine
    wait clear
    * --- Ripristina gestione tasti
    pop key
    * --- Chiusura cursori (Funzione definita nell'area manuale)
    this.CloseCurs ()
    * --- Abilita form chiamante
    this.oParentObject.Closable = .T.
    this.oParentObject.w_INCORSO = .F.
    this.oParentObject.mEnableControls()
    * --- Abilita gestione toolbar
    i_stopFK = .F.
    this.w_Control = .null.
    * --- Rimuovo la variabile utilizzata..
    Release all like g_IMPOFLD
    i_retcode = 'stop'
    return
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    if this.w_Destinaz == "RF" and (this.w_ULTTIPCON <> w_COTIPCON OR this.w_ULTCODCON <> w_COCODCON)
      this.w_DettaRow = 1
    else
      this.w_DettaRow = this.w_DettaRow + 1
    endif
    do case
      case this.w_Destinaz $ this.oParentObject.w_Gestiti
        if this.w_BATCNAME<>"GSIM_BAC"
          * --- ARCHIVI ELABORATI DA ROUTINE ESTERNE
          if this.w_BATCNAME="GSCE_BIM"
            this.w_CECOUNT = this.w_CECOUNT + 1
          endif
          * --- Try
          local bErr_04CAAA08
          bErr_04CAAA08=bTrsErr
          this.Try_04CAAA08()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            this.w_Corretto = .F.
          endif
          bTrsErr=bTrsErr or bErr_04CAAA08
          * --- End
        else
          * --- ARCHIVI ELABORATI DA ROUTINE GSIM_BAC
          if this.w_Destinaz <> "XX"
            * --- Caso particolare w_Destinaz='XX': non avviene nessuna scrittura
            * --- Try
            local bErr_04CCE9E8
            bErr_04CCE9E8=bTrsErr
            this.Try_04CCE9E8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              if this.w_Destinaz = "CF"
                * --- se w_CLIESI='S' allora il cliente esisteva gi�
                this.w_CLIESI = "S"
              endif
            endif
            bTrsErr=bTrsErr or bErr_04CCE9E8
            * --- End
            i_rows = 0
            * --- Try
            local bErr_04CCF798
            bErr_04CCF798=bTrsErr
            this.Try_04CCF798()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_Corretto = .F.
            endif
            bTrsErr=bTrsErr or bErr_04CCF798
            * --- End
          else
            * --- Caso particolare w_Destinaz='XX': vengono valorizzate le Variabili di Appoggio per le Contropartite Ven./Acq. relative agli Omaggi
            i_rows = 1
            this.w_XXCONOMA = w_EXCONOMA
            this.w_XXCONIOM = w_EXCONIOM
          endif
        endif
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.w_Corretto = .F.
        endif
        if this.w_Corretto .and. this.w_ResoDett <> "<SENZA RESOCONTO>"
          * --- Scrittura resoconto
          this.w_ResoMode = "SCRITTURA"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case .T.
        * --- Scrittura resoconto per archivi non supportati
        this.w_ResoMode = "NONSUPP"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc
  proc Try_04CAAA08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_DESTINAZ = "DO" AND this.ULT_CHIMOV <> this.w_ChiaveRow
      * --- Solo per il caso w_DESTINAZ='DO', in cui la transazione � gestita a livello di riga
      if ! this.w_ERRDETT
        if (.not. empty( this.ULT_CHIMOV ))
          * --- Ricalcolo i totali del documento precedente. Vedi anche GSIM_BAC Pag.1
          if (.not. empty( this.ULT_SERI ))
            do gsar_brd with this, this.ULT_SERI
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL"
              do vq_exec with 'gsim0bmm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVCODAG2 = _t2.MVCODAG2";
                  +",MVCODAGE = _t2.MVCODAGE";
                  +",MVCODBA2 = _t2.MVCODBA2";
                  +",MVCODBAN = _t2.MVCODBAN";
                  +",MVCODDES = _t2.MVCODDES";
                  +",MVCODIVE = _t2.MVCODIVE";
                  +",MVCODPAG = _t2.MVCODPAG";
                  +",MVCODPOR = _t2.MVCODPOR";
                  +",MVCODSED = _t2.MVCODSED";
                  +",MVCODSPE = _t2.MVCODSPE";
                  +",MVCODVAL = _t2.MVCODVAL";
                  +",MVCODVE2 = _t2.MVCODVE2";
                  +",MVCODVET = _t2.MVCODVET";
                  +",MVFLSCOR = _t2.MVFLSCOR";
                  +",MVIVAIMB = _t2.MVIVAIMB";
                  +",MVIVAINC = _t2.MVIVAINC";
                  +",MVIVATRA = _t2.MVIVATRA";
                  +",MVNUMCOR = _t2.MVNUMCOR";
                  +",MVRIFDIC = _t2.MVRIFDIC";
                  +",MVCODORN = _t2.MVCODORN";
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
                  +"DOC_MAST.MVCODAG2 = _t2.MVCODAG2";
                  +",DOC_MAST.MVCODAGE = _t2.MVCODAGE";
                  +",DOC_MAST.MVCODBA2 = _t2.MVCODBA2";
                  +",DOC_MAST.MVCODBAN = _t2.MVCODBAN";
                  +",DOC_MAST.MVCODDES = _t2.MVCODDES";
                  +",DOC_MAST.MVCODIVE = _t2.MVCODIVE";
                  +",DOC_MAST.MVCODPAG = _t2.MVCODPAG";
                  +",DOC_MAST.MVCODPOR = _t2.MVCODPOR";
                  +",DOC_MAST.MVCODSED = _t2.MVCODSED";
                  +",DOC_MAST.MVCODSPE = _t2.MVCODSPE";
                  +",DOC_MAST.MVCODVAL = _t2.MVCODVAL";
                  +",DOC_MAST.MVCODVE2 = _t2.MVCODVE2";
                  +",DOC_MAST.MVCODVET = _t2.MVCODVET";
                  +",DOC_MAST.MVFLSCOR = _t2.MVFLSCOR";
                  +",DOC_MAST.MVIVAIMB = _t2.MVIVAIMB";
                  +",DOC_MAST.MVIVAINC = _t2.MVIVAINC";
                  +",DOC_MAST.MVIVATRA = _t2.MVIVATRA";
                  +",DOC_MAST.MVNUMCOR = _t2.MVNUMCOR";
                  +",DOC_MAST.MVRIFDIC = _t2.MVRIFDIC";
                  +",DOC_MAST.MVCODORN = _t2.MVCODORN";
                  +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                  +"MVCODAG2,";
                  +"MVCODAGE,";
                  +"MVCODBA2,";
                  +"MVCODBAN,";
                  +"MVCODDES,";
                  +"MVCODIVE,";
                  +"MVCODPAG,";
                  +"MVCODPOR,";
                  +"MVCODSED,";
                  +"MVCODSPE,";
                  +"MVCODVAL,";
                  +"MVCODVE2,";
                  +"MVCODVET,";
                  +"MVFLSCOR,";
                  +"MVIVAIMB,";
                  +"MVIVAINC,";
                  +"MVIVATRA,";
                  +"MVNUMCOR,";
                  +"MVRIFDIC,";
                  +"MVCODORN";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.MVCODAG2,";
                  +"t2.MVCODAGE,";
                  +"t2.MVCODBA2,";
                  +"t2.MVCODBAN,";
                  +"t2.MVCODDES,";
                  +"t2.MVCODIVE,";
                  +"t2.MVCODPAG,";
                  +"t2.MVCODPOR,";
                  +"t2.MVCODSED,";
                  +"t2.MVCODSPE,";
                  +"t2.MVCODVAL,";
                  +"t2.MVCODVE2,";
                  +"t2.MVCODVET,";
                  +"t2.MVFLSCOR,";
                  +"t2.MVIVAIMB,";
                  +"t2.MVIVAINC,";
                  +"t2.MVIVATRA,";
                  +"t2.MVNUMCOR,";
                  +"t2.MVRIFDIC,";
                  +"t2.MVCODORN";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
                  +"MVCODAG2 = _t2.MVCODAG2";
                  +",MVCODAGE = _t2.MVCODAGE";
                  +",MVCODBA2 = _t2.MVCODBA2";
                  +",MVCODBAN = _t2.MVCODBAN";
                  +",MVCODDES = _t2.MVCODDES";
                  +",MVCODIVE = _t2.MVCODIVE";
                  +",MVCODPAG = _t2.MVCODPAG";
                  +",MVCODPOR = _t2.MVCODPOR";
                  +",MVCODSED = _t2.MVCODSED";
                  +",MVCODSPE = _t2.MVCODSPE";
                  +",MVCODVAL = _t2.MVCODVAL";
                  +",MVCODVE2 = _t2.MVCODVE2";
                  +",MVCODVET = _t2.MVCODVET";
                  +",MVFLSCOR = _t2.MVFLSCOR";
                  +",MVIVAIMB = _t2.MVIVAIMB";
                  +",MVIVAINC = _t2.MVIVAINC";
                  +",MVIVATRA = _t2.MVIVATRA";
                  +",MVNUMCOR = _t2.MVNUMCOR";
                  +",MVRIFDIC = _t2.MVRIFDIC";
                  +",MVCODORN = _t2.MVCODORN";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVCODAG2 = (select MVCODAG2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODAGE = (select MVCODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODBA2 = (select MVCODBA2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODBAN = (select MVCODBAN from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODDES = (select MVCODDES from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODIVE = (select MVCODIVE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODPAG = (select MVCODPAG from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODPOR = (select MVCODPOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODSED = (select MVCODSED from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODSPE = (select MVCODSPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVAL = (select MVCODVAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVE2 = (select MVCODVE2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVET = (select MVCODVET from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLSCOR = (select MVFLSCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVAIMB = (select MVIVAIMB from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVAINC = (select MVIVAINC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVATRA = (select MVIVATRA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVNUMCOR = (select MVNUMCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODORN = (select MVCODORN from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- commit
          cp_EndTrs(.t.)
        endif
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      * --- begin transaction
      cp_BeginTrs()
    endif
    do ( this.w_BatcName ) with this
    i_rows = 1
    return
  proc Try_04CCE9E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_Destinaz = "CF"
        this.w_CLIESI = ""
        * --- Clienti e fornitori
        if IsAlt()
          * --- Utente che ha effettuato l'import (a meno che nel tracciato non venga valorizzato il campo UTCC)
          this.w_CreVarUte = IIF(VarType(w_UTCC)<>"U" And Not Empty(w_UTCC),w_UTCC, this.w_CreVarUte)
          * --- Data nella quale � stato effettuato l'import (a meno che nel tracciato non venga valorizzato il campo UTDC)
          this.w_CreVarDat = IIF(VarType(w_UTDC)<>"U" And Not Empty(w_UTDC),w_UTDC, this.w_CreVarDat)
        endif
        * --- Insert into CONTI
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANPARIVA"+",ANCONSUP"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ANTIPCON),'CONTI','ANTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'CONTI','ANCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'CONTI','ANDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(w_ANPARIVA),'CONTI','ANPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'CONTI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'CONTI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',w_ANTIPCON,'ANCODICE',w_ANCODICE,'ANDESCRI',w_ANDESCRI,'ANPARIVA',w_ANPARIVA,'ANCONSUP',w_ANCONSUP,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCONSUP,UTCC,UTDC &i_ccchkf. );
             values (;
               w_ANTIPCON;
               ,w_ANCODICE;
               ,w_ANDESCRI;
               ,w_ANPARIVA;
               ,w_ANCONSUP;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Conti Correnti
        if VARTYPE(w_CCCODBAN)<>"U" AND !EMPTY(w_CCCODBAN) AND VARTYPE(w_CCCONCOR) <>"U" AND ! EMPTY(w_CCCONCOR)
          * --- Try
          local bErr_04DA3DF8
          bErr_04DA3DF8=bTrsErr
          this.Try_04DA3DF8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DA3DF8
          * --- End
        endif
        * --- se w_CLIESI='N' allora il cliente non esisteva (� stato creato in questo momento)
        this.w_CLIESI = "N"
      case this.w_Destinaz = "ZO"
        * --- Zone
        if type( "w_ZOCODZON" ) = "C"
          w_ZOCODZON = left( ltrim(w_ZOCODZON), 3 )
        endif
        * --- Insert into ZONE
        i_nConn=i_TableProp[this.ZONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZONE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZONE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ZOCODZON"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ZOCODZON),'ZONE','ZOCODZON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'ZONE','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'ZONE','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ZOCODZON',w_ZOCODZON,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (ZOCODZON,UTCC,UTDC &i_ccchkf. );
             values (;
               w_ZOCODZON;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "NA"
        * --- Nazioni
        if type( "w_NACODNAZ" ) = "C"
          w_NACODNAZ = left( ltrim(w_NACODNAZ), 3 )
        endif
        * --- Insert into NAZIONI
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NAZIONI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NACODNAZ"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_NACODNAZ),'NAZIONI','NACODNAZ');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NACODNAZ',w_NACODNAZ)
          insert into (i_cTable) (NACODNAZ &i_ccchkf. );
             values (;
               w_NACODNAZ;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "LI"
        * --- Lingue
        if type( "w_LUCODICE" ) = "C"
          w_LUCODICE = left( ltrim(w_LUCODICE), 3 )
        endif
        * --- Insert into LINGUE
        i_nConn=i_TableProp[this.LINGUE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LINGUE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LINGUE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LUCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LUCODICE),'LINGUE','LUCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LUCODICE',w_LUCODICE)
          insert into (i_cTable) (LUCODICE &i_ccchkf. );
             values (;
               w_LUCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "VA"
        * --- Valute
        if type( "w_VACODVAL" ) = "C"
          w_VACODVAL = left( ltrim(w_VACODVAL), 3 )
        endif
        * --- Insert into VALUTE
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VALUTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"VACODVAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_VACODVAL),'VALUTE','VACODVAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'VACODVAL',w_VACODVAL)
          insert into (i_cTable) (VACODVAL &i_ccchkf. );
             values (;
               w_VACODVAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "IV"
        * --- Codici iva
        if type( "w_IVCODIVA" ) = "C"
          w_IVCODIVA = left( ltrim(w_IVCODIVA), 5 )
        endif
        * --- Insert into VOCIIVA
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOCIIVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IVCODIVA"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_IVCODIVA),'VOCIIVA','IVCODIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'VOCIIVA','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'VOCIIVA','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IVCODIVA',w_IVCODIVA,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (IVCODIVA,UTCC,UTDC &i_ccchkf. );
             values (;
               w_IVCODIVA;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        w_EXCODNUO = w_IVCODIVA
      case this.w_Destinaz = "AG"
        * --- Agenti
        if type( "w_AGCODAGE" ) = "C"
          w_AGCODAGE = left( ltrim(w_AGCODAGE), 5 )
        endif
        * --- Insert into AGENTI
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AGENTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"AGCODAGE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_AGCODAGE),'AGENTI','AGCODAGE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'AGCODAGE',w_AGCODAGE)
          insert into (i_cTable) (AGCODAGE &i_ccchkf. );
             values (;
               w_AGCODAGE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "DE"
        * --- Destinazioni diverse
        if NVL(w_DDTIPCON, " ")=" " AND NVL(w_DDCODICE, " ")=" " AND Empty(NVL(w_DDCODDES, " ")) AND (NOT EMPTY(Nvl(w_DDCODVET," ")) OR NOT EMPTY(Nvl(w_DDCODPOR," ")) OR (NOT EMPTY(Nvl(w_DDCODSPE," ")) AND w_EXTIPCON="C"))
          * --- Caso in cui la destinazione diversa non esiste, viene valorizzata con i dati del cliente/fornitore
          * --- Insert into DES_DIVE
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DES_DIVE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DDTIPCON"+",DDCODICE"+",DDCODDES"+",DDNOMDES"+",DDINDIRI"+",DD___CAP"+",DDLOCALI"+",DDPROVIN"+",DDCODVET"+",DDCODPOR"+",DDCODSPE"+",DDTIPRIF"+",DDCODNAZ"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_EXTIPCON),'DES_DIVE','DDTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(w_EXCODICE),'DES_DIVE','DDCODICE');
            +","+cp_NullLink(cp_ToStrODBC("99999"),'DES_DIVE','DDCODDES');
            +","+cp_NullLink(cp_ToStrODBC(w_EXRAGSOC),'DES_DIVE','DDNOMDES');
            +","+cp_NullLink(cp_ToStrODBC(w_EXINDIRI),'DES_DIVE','DDINDIRI');
            +","+cp_NullLink(cp_ToStrODBC(w_EX___CAP),'DES_DIVE','DD___CAP');
            +","+cp_NullLink(cp_ToStrODBC(w_EXLOCALI),'DES_DIVE','DDLOCALI');
            +","+cp_NullLink(cp_ToStrODBC(w_EXPROVIN),'DES_DIVE','DDPROVIN');
            +","+cp_NullLink(cp_ToStrODBC(w_DDCODVET),'DES_DIVE','DDCODVET');
            +","+cp_NullLink(cp_ToStrODBC(w_DDCODPOR),'DES_DIVE','DDCODPOR');
            +","+cp_NullLink(cp_ToStrODBC(w_DDCODSPE),'DES_DIVE','DDCODSPE');
            +","+cp_NullLink(cp_ToStrODBC(w_DDTIPRIF),'DES_DIVE','DDTIPRIF');
            +","+cp_NullLink(cp_ToStrODBC(w_DDCODNAZ),'DES_DIVE','DDCODNAZ');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DDTIPCON',w_EXTIPCON,'DDCODICE',w_EXCODICE,'DDCODDES',"99999",'DDNOMDES',w_EXRAGSOC,'DDINDIRI',w_EXINDIRI,'DD___CAP',w_EX___CAP,'DDLOCALI',w_EXLOCALI,'DDPROVIN',w_EXPROVIN,'DDCODVET',w_DDCODVET,'DDCODPOR',w_DDCODPOR,'DDCODSPE',w_DDCODSPE,'DDTIPRIF',w_DDTIPRIF)
            insert into (i_cTable) (DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODVET,DDCODPOR,DDCODSPE,DDTIPRIF,DDCODNAZ &i_ccchkf. );
               values (;
                 w_EXTIPCON;
                 ,w_EXCODICE;
                 ,"99999";
                 ,w_EXRAGSOC;
                 ,w_EXINDIRI;
                 ,w_EX___CAP;
                 ,w_EXLOCALI;
                 ,w_EXPROVIN;
                 ,w_DDCODVET;
                 ,w_DDCODPOR;
                 ,w_DDCODSPE;
                 ,w_DDTIPRIF;
                 ,w_DDCODNAZ;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          if NVL(w_DDTIPCON, " ")<>" " AND NVL(w_DDCODICE, " ")<>" " AND NVL(w_DDCODDES, " ")<>" "
            * --- Insert into DES_DIVE
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DES_DIVE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DDTIPCON"+",DDCODICE"+",DDCODDES"+",DDNOMDES"+",DDINDIRI"+",DD___CAP"+",DDLOCALI"+",DDPROVIN"+",DDPERSON"+",DD__NOTE"+",DDTELEFO"+",DD_EMAIL"+",DDCODVET"+",DDCODPOR"+",DDCODSPE"+",DDTIPRIF"+",DD_EMPEC"+",DDCODNAZ"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(w_DDTIPCON),'DES_DIVE','DDTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODICE),'DES_DIVE','DDCODICE');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODDES),'DES_DIVE','DDCODDES');
              +","+cp_NullLink(cp_ToStrODBC(w_DDNOMDES),'DES_DIVE','DDNOMDES');
              +","+cp_NullLink(cp_ToStrODBC(w_DDINDIRI),'DES_DIVE','DDINDIRI');
              +","+cp_NullLink(cp_ToStrODBC(w_DD___CAP),'DES_DIVE','DD___CAP');
              +","+cp_NullLink(cp_ToStrODBC(w_DDLOCALI),'DES_DIVE','DDLOCALI');
              +","+cp_NullLink(cp_ToStrODBC(w_DDPROVIN),'DES_DIVE','DDPROVIN');
              +","+cp_NullLink(cp_ToStrODBC(w_DDPERSON),'DES_DIVE','DDPERSON');
              +","+cp_NullLink(cp_ToStrODBC(w_DD__NOTE),'DES_DIVE','DD__NOTE');
              +","+cp_NullLink(cp_ToStrODBC(w_DDTELEFO),'DES_DIVE','DDTELEFO');
              +","+cp_NullLink(cp_ToStrODBC(w_DD_EMAIL),'DES_DIVE','DD_EMAIL');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODVET),'DES_DIVE','DDCODVET');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODPOR),'DES_DIVE','DDCODPOR');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODSPE),'DES_DIVE','DDCODSPE');
              +","+cp_NullLink(cp_ToStrODBC(w_DDTIPRIF),'DES_DIVE','DDTIPRIF');
              +","+cp_NullLink(cp_ToStrODBC(w_DD_EMPEC),'DES_DIVE','DD_EMPEC');
              +","+cp_NullLink(cp_ToStrODBC(w_DDCODNAZ),'DES_DIVE','DDCODNAZ');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DDTIPCON',w_DDTIPCON,'DDCODICE',w_DDCODICE,'DDCODDES',w_DDCODDES,'DDNOMDES',w_DDNOMDES,'DDINDIRI',w_DDINDIRI,'DD___CAP',w_DD___CAP,'DDLOCALI',w_DDLOCALI,'DDPROVIN',w_DDPROVIN,'DDPERSON',w_DDPERSON,'DD__NOTE',w_DD__NOTE,'DDTELEFO',w_DDTELEFO,'DD_EMAIL',w_DD_EMAIL)
              insert into (i_cTable) (DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDPERSON,DD__NOTE,DDTELEFO,DD_EMAIL,DDCODVET,DDCODPOR,DDCODSPE,DDTIPRIF,DD_EMPEC,DDCODNAZ &i_ccchkf. );
                 values (;
                   w_DDTIPCON;
                   ,w_DDCODICE;
                   ,w_DDCODDES;
                   ,w_DDNOMDES;
                   ,w_DDINDIRI;
                   ,w_DD___CAP;
                   ,w_DDLOCALI;
                   ,w_DDPROVIN;
                   ,w_DDPERSON;
                   ,w_DD__NOTE;
                   ,w_DDTELEFO;
                   ,w_DD_EMAIL;
                   ,w_DDCODVET;
                   ,w_DDCODPOR;
                   ,w_DDCODSPE;
                   ,w_DDTIPRIF;
                   ,w_DD_EMPEC;
                   ,w_DDCODNAZ;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      case this.w_Destinaz = "BA"
        * --- Banche
        if type( "w_BACODBAN" ) = "C"
          w_BACODBAN= left( ltrim(w_BACODBAN), 10 )
        endif
        * --- Insert into BAN_CHE
        i_nConn=i_TableProp[this.BAN_CHE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.BAN_CHE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"BACODBAN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_BACODBAN),'BAN_CHE','BACODBAN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'BACODBAN',w_BACODBAN)
          insert into (i_cTable) (BACODBAN &i_ccchkf. );
             values (;
               w_BACODBAN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="CB"
        * --- Conti Banche
        if type( "w_BACODBAN" ) = "C"
          w_BACODBAN= left( ltrim(w_BACODBAN), 15 )
        endif
        * --- Insert into COC_MAST
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"BACODBAN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_BACODBAN),'COC_MAST','BACODBAN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'BACODBAN',w_BACODBAN)
          insert into (i_cTable) (BACODBAN &i_ccchkf. );
             values (;
               w_BACODBAN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "CC"
        * --- Causali contabili
        if type( "w_CCCODICE" ) = "C"
          w_CCCODICE = left( ltrim(w_CCCODICE), 5 )
        endif
        * --- Insert into CAU_CONT
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_CONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CCCODICE),'CAU_CONT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'CAU_CONT','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'CAU_CONT','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCCODICE',w_CCCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (CCCODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_CCCODICE;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        w_EXCODNUO = w_CCCODICE
      case this.w_Destinaz = "PA"
        * --- Pagamenti
        if type( "w_PACODICE" ) = "C"
          w_PACODICE = left( ltrim(w_PACODICE), 5 )
        endif
        * --- Insert into PAG_AMEN
        i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAG_AMEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PACODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_PACODICE),'PAG_AMEN','PACODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'PAG_AMEN','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'PAG_AMEN','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PACODICE',w_PACODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (PACODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_PACODICE;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "PD"
        * --- Dettaglio Pagamenti
        if w_EXTIPORI="W"
          if type( "w_P2CODICE" ) = "C"
            w_P2CODICE = left( ltrim(w_P2CODICE), 5 )
          endif
          do GSIM_BAM with this, this.oParentObject.w_IMAZZERA, this.w_Destinaz, this.ChiavePrec, w_P2CODICE, "", ""
          w_P2NUMRAT = right( "000" + alltrim(w_P2NUMRAT), 3)
          w_P2MODPAG = left( w_P2MODPAG+space(10), 10 )
          if left(ALLTRIM(w_P2MODPAG)+space(10), 2 ) $ "BO-MA-RD-RI-CA-CC-RB"
            * --- Se rientra nelle tipologie previste da Revolution  altrimenti
            *     assegno pagamento di Default
            w_MPTIPPAG = left(ALLTRIM(w_P2MODPAG)+space(10), 2 )
          else
            w_MPTIPPAG ="RD"
          endif
          * --- Try
          local bErr_04DC1078
          bErr_04DC1078=bTrsErr
          this.Try_04DC1078()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DC1078
          * --- End
        endif
        * --- Insert into PAG_2AME
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAG_2AME_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"P2CODICE"+",P2NUMRAT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_P2CODICE),'PAG_2AME','P2CODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_P2NUMRAT),'PAG_2AME','P2NUMRAT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'P2CODICE',w_P2CODICE,'P2NUMRAT',w_P2NUMRAT)
          insert into (i_cTable) (P2CODICE,P2NUMRAT &i_ccchkf. );
             values (;
               w_P2CODICE;
               ,w_P2NUMRAT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz = "PC"
        * --- Piano dei conti
        w_ANTIPCON = iif( empty(w_ANTIPCON), "G", w_ANTIPCON )
        w_ANCODICE = left( w_ANCODICE + space(15), 15)
        if empty(w_EXMASCON)
          w_EXMASCON = "M"
          * --- Import da procedure tipo Ad Hoc che non specificano il tipo (mastro/conto o sottoconto)
          w_EXMASCON = iif( right(alltrim(w_ANCODICE),3)="000" .and. right(alltrim(w_ANCODICE),5)#"00000" , "C", w_EXMASCON )
          w_EXMASCON = iif( right(alltrim(w_ANCODICE),3)#"000" , "S", w_EXMASCON )
          w_EXMASCON = iif( w_EXTIPSOT $ "CF" , "C", w_EXMASCON )
          if w_EXMASCON="M"
            w_MCNUMLIV = iif(g_MaxLiv=0,3,g_MaxLiv)
          else
            w_MCNUMLIV = iif( w_EXMASCON="C" .and. w_EXTIPSOT$"CF" .and. right(alltrim(w_ANCODICE),3)="000" ,2,1)
          endif
          * --- Alza livello per mastro clienti o fornitori
          if w_EXTIPSOT$"CF" and right(alltrim(w_ANCODICE),3)<>"000"
            w_EXMASCLFO=left(alltrim(w_ANCODICE),4)+"000"
            * --- Write into MASTRI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MCNUMLIV ="+cp_NullLink(cp_ToStrODBC(w_MCNUMLIV+1),'MASTRI','MCNUMLIV');
                  +i_ccchkf ;
              +" where ";
                  +"MCCODICE = "+cp_ToStrODBC(w_EXMASCLFO);
                     )
            else
              update (i_cTable) set;
                  MCNUMLIV = w_MCNUMLIV+1;
                  &i_ccchkf. ;
               where;
                  MCCODICE = w_EXMASCLFO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            w_EXCODNUO = w_EXMASCLFO
          endif
        else
          if empty(w_MCNUMLIV)
            w_MCNUMLIV = iif( w_EXMASCON="M" , iif(g_MaxLiv=0,3,g_MaxLiv), 1)
          endif
        endif
        if w_EXMASCON="S"
          * --- Insert into CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"ANTIPCON"+",ANCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_ANTIPCON),'CONTI','ANTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'CONTI','ANCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'CONTI','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'CONTI','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',w_ANTIPCON,'ANCODICE',w_ANCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
            insert into (i_cTable) (ANTIPCON,ANCODICE,UTCC,UTDC &i_ccchkf. );
               values (;
                 w_ANTIPCON;
                 ,w_ANCODICE;
                 ,this.w_CreVarUte;
                 ,this.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into MASTRI
          i_nConn=i_TableProp[this.MASTRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MASTRI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MCCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_ANCODICE),'MASTRI','MCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'MASTRI','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'MASTRI','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MCCODICE',w_ANCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
            insert into (i_cTable) (MCCODICE,UTCC,UTDC &i_ccchkf. );
               values (;
                 w_ANCODICE;
                 ,this.w_CreVarUte;
                 ,this.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        w_EXCODNUO = w_ANCODICE
      case this.w_Destinaz = "AC"
        * --- Modelli e automatismi contabili
        w_APCODCAU = left( ltrim(w_APCODCAU), 5 )
        w_APCODCON = left( ltrim(w_APCODCON)+space(15), 15 )
        w_EXCODCLI = left( ltrim(w_EXCODCLI)+space(15), 15 )
        w_EXCODFOR = left( ltrim(w_EXCODFOR)+space(15), 15 )
        w_EXCLFTIP = iif( .not. empty(w_EXCODCLI), "C", iif( .not. empty(w_EXCODFOR) ,"F", " ") )
        w_EXCLFCON = iif( w_EXCLFTIP="C", w_EXCODCLI, iif( w_EXCLFTIP="F" , w_EXCODFOR, space(len(w_EXCODFOR)) ))
        w_EXCODIVA = iif( alltrim(w_EXCODIVA)+" " = "0 " , space(len(w_EXCODIVA)) , w_EXCODIVA )
        do GSIM_BAM with this, this.oParentObject.w_IMAZZERA, this.w_Destinaz, this.ChiavePrec, w_APCODCAU, w_EXCLFTIP, w_EXCLFCON
        if empty(w_APTIPCON)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANTIPCON"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANCODICE = "+cp_ToStrODBC(w_APCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANTIPCON;
              from (i_cTable) where;
                  ANCODICE = w_APCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_APTIPCON = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty( w_APTIPCON )
            * --- Read from MASTRI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MCTIPMAS"+;
                " from "+i_cTable+" MASTRI where ";
                    +"MCCODICE = "+cp_ToStrODBC(w_APCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MCTIPMAS;
                from (i_cTable) where;
                    MCCODICE = w_APCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_APTIPCON = NVL(cp_ToDate(_read_.MCTIPMAS),cp_NullValue(_read_.MCTIPMAS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se si tratta di un cliente o di un fornitore deve cercare nei mastri
          endif
          w_APTIPCON = iif(.not. w_APTIPCON$"CF","G",w_APTIPCON)
        endif
        w_APCODCON = iif( w_APTIPCON="C", w_EXCODCLI, iif( w_APTIPCON="F" , w_EXCODFOR, w_APCODCON) )
        if w_EXCLFTIP $ "CF"
          w_TestMode = " "
          * --- Read from MOD_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOD_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2],.t.,this.MOD_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCCODICE"+;
              " from "+i_cTable+" MOD_CONT where ";
                  +"CCCODICE = "+cp_ToStrODBC(w_APCODCAU);
                  +" and CCTIPCON = "+cp_ToStrODBC(w_EXCLFTIP);
                  +" and CCCODCON = "+cp_ToStrODBC(w_EXCLFCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCCODICE;
              from (i_cTable) where;
                  CCCODICE = w_APCODCAU;
                  and CCTIPCON = w_EXCLFTIP;
                  and CCCODCON = w_EXCLFCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_TestMode = NVL(cp_ToDate(_read_.CCCODICE),cp_NullValue(_read_.CCCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty( w_TestMode )
            * --- Try
            local bErr_04DD0C28
            bErr_04DD0C28=bTrsErr
            this.Try_04DD0C28()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04DD0C28
            * --- End
          endif
          * --- Insert into CAUPRI
          i_nConn=i_TableProp[this.CAUPRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUPRI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"APCODCAU"+",APTIPCLF"+",APCODCLF"+",CPROWNUM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_APCODCAU),'CAUPRI','APCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(w_EXCLFTIP),'CAUPRI','APTIPCLF');
            +","+cp_NullLink(cp_ToStrODBC(w_EXCLFCON),'CAUPRI','APCODCLF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DettaRow),'CAUPRI','CPROWNUM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'APCODCAU',w_APCODCAU,'APTIPCLF',w_EXCLFTIP,'APCODCLF',w_EXCLFCON,'CPROWNUM',this.w_DettaRow)
            insert into (i_cTable) (APCODCAU,APTIPCLF,APCODCLF,CPROWNUM &i_ccchkf. );
               values (;
                 w_APCODCAU;
                 ,w_EXCLFTIP;
                 ,w_EXCLFCON;
                 ,this.w_DettaRow;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if .not. empty( w_EXCODIVA )
            * --- Insert into CAUIVA
            i_nConn=i_TableProp[this.CAUIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUIVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"AICODCAU"+",AITIPCLF"+",AICODCLF"+",CPROWNUM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(w_APCODCAU),'CAUIVA','AICODCAU');
              +","+cp_NullLink(cp_ToStrODBC(w_EXCLFTIP),'CAUIVA','AITIPCLF');
              +","+cp_NullLink(cp_ToStrODBC(w_EXCLFCON),'CAUIVA','AICODCLF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DettaRow),'CAUIVA','CPROWNUM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'AICODCAU',w_APCODCAU,'AITIPCLF',w_EXCLFTIP,'AICODCLF',w_EXCLFCON,'CPROWNUM',this.w_DettaRow)
              insert into (i_cTable) (AICODCAU,AITIPCLF,AICODCLF,CPROWNUM &i_ccchkf. );
                 values (;
                   w_APCODCAU;
                   ,w_EXCLFTIP;
                   ,w_EXCLFCON;
                   ,this.w_DettaRow;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            w_EXCODNUO = w_APCODCAU
          endif
        else
          * --- Insert into CAUPRI1
          i_nConn=i_TableProp[this.CAUPRI1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUPRI1_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"APCODCAU"+",CPROWNUM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_APCODCAU),'CAUPRI1','APCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DettaRow),'CAUPRI1','CPROWNUM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'APCODCAU',w_APCODCAU,'CPROWNUM',this.w_DettaRow)
            insert into (i_cTable) (APCODCAU,CPROWNUM &i_ccchkf. );
               values (;
                 w_APCODCAU;
                 ,this.w_DettaRow;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if .not. empty( w_EXCODIVA )
            * --- Insert into CAUIVA1
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUIVA1_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"AICODCAU"+",CPROWNUM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(w_APCODCAU),'CAUIVA1','AICODCAU');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DettaRow),'CAUIVA1','CPROWNUM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'AICODCAU',w_APCODCAU,'CPROWNUM',this.w_DettaRow)
              insert into (i_cTable) (AICODCAU,CPROWNUM &i_ccchkf. );
                 values (;
                   w_APCODCAU;
                   ,this.w_DettaRow;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            w_EXCODNUO = w_APCODCAU
          endif
        endif
      case this.w_Destinaz ="SC".and.(w_EXMASCON="S".or.w_SLTIPCON$"CF".or.(empty(w_EXMASCON).and.imconmas(w_SLCODICE)="C"))
        * --- Saldi contabili
        w_SLTIPCON = iif( empty(w_SLTIPCON), "G", left( ltrim(w_SLTIPCON), 1) )
        w_SLCODICE =left( w_SLCODICE + space(15), 15)
        w_SLCODESE = iif( empty(w_SLCODESE), g_CODESE, w_SLCODESE )
        * --- Insert into SALDICON
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_SLTIPCON),'SALDICON','SLTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(w_SLCODICE),'SALDICON','SLCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_SLCODESE),'SALDICON','SLCODESE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',w_SLTIPCON,'SLCODICE',w_SLCODICE,'SLCODESE',w_SLCODESE)
          insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE &i_ccchkf. );
             values (;
               w_SLTIPCON;
               ,w_SLCODICE;
               ,w_SLCODESE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="CA"
        * --- Categorie Commerciali
        w_CTCODICE = left( ltrim(w_CTCODICE), 3 )
        * --- Insert into CATECOMM
        i_nConn=i_TableProp[this.CATECOMM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CATECOMM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CATECOMM_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CTCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CTCODICE),'CATECOMM','CTCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CTCODICE',w_CTCODICE)
          insert into (i_cTable) (CTCODICE &i_ccchkf. );
             values (;
               w_CTCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="CL"
        * --- Categorie Contabili Cli/For
        w_C2CODICE = left( ltrim(w_C2CODICE), 5 )
        * --- Insert into CACOCLFO
        i_nConn=i_TableProp[this.CACOCLFO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOCLFO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOCLFO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"C2CODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_C2CODICE),'CACOCLFO','C2CODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'C2CODICE',w_C2CODICE)
          insert into (i_cTable) (C2CODICE &i_ccchkf. );
             values (;
               w_C2CODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="GI"
        * --- Cambi Giornalieri
        w_CGCODVAL=left(ltrim(w_CGCODVAL),3)
        * --- Insert into CAM_BI
        i_nConn=i_TableProp[this.CAM_BI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAM_BI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CGDATCAM"+",CGCODVAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CGDATCAM),'CAM_BI','CGDATCAM');
          +","+cp_NullLink(cp_ToStrODBC(w_CGCODVAL),'CAM_BI','CGCODVAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CGDATCAM',w_CGDATCAM,'CGCODVAL',w_CGCODVAL)
          insert into (i_cTable) (CGDATCAM,CGCODVAL &i_ccchkf. );
             values (;
               w_CGDATCAM;
               ,w_CGCODVAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="TR"
        * --- Trascodifiche
        * --- Insert into TRASCODI
        i_nConn=i_TableProp[this.TRASCODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TRTIPTRA"+",TRCODIMP"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRTRAPAR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TRTIPTRA),'TRASCODI','TRTIPTRA');
          +","+cp_NullLink(cp_ToStrODBC(w_TRCODIMP),'TRASCODI','TRCODIMP');
          +","+cp_NullLink(cp_ToStrODBC(w_TRCODFIL),'TRASCODI','TRCODFIL');
          +","+cp_NullLink(cp_ToStrODBC(w_TRNOMCAM),'TRASCODI','TRNOMCAM');
          +","+cp_NullLink(cp_ToStrODBC(w_TRCODEXT),'TRASCODI','TRCODEXT');
          +","+cp_NullLink(cp_ToStrODBC(w_TRTRAPAR),'TRASCODI','TRTRAPAR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TRTIPTRA',w_TRTIPTRA,'TRCODIMP',w_TRCODIMP,'TRCODFIL',w_TRCODFIL,'TRNOMCAM',w_TRNOMCAM,'TRCODEXT',w_TRCODEXT,'TRTRAPAR',w_TRTRAPAR)
          insert into (i_cTable) (TRTIPTRA,TRCODIMP,TRCODFIL,TRNOMCAM,TRCODEXT,TRTRAPAR &i_ccchkf. );
             values (;
               w_TRTIPTRA;
               ,w_TRCODIMP;
               ,w_TRCODFIL;
               ,w_TRNOMCAM;
               ,w_TRCODEXT;
               ,w_TRTRAPAR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="MS"
        * --- Mastri
        w_MCCODICE = left( w_MCCODICE + space(15), 15)
        * --- Insert into MASTRI
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MASTRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MCCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_MCCODICE),'MASTRI','MCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'MASTRI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'MASTRI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MCCODICE',w_MCCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (MCCODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_MCCODICE;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="CH"
        * --- Conti
        w_ANTIPCON = iif( empty(w_ANTIPCON), "G", w_ANTIPCON )
        w_ANCODICE = left( w_ANCODICE + space(15), 15)
        * --- Insert into CONTI
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ANTIPCON"+",ANCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ANTIPCON),'CONTI','ANTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'CONTI','ANCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'CONTI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'CONTI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',w_ANTIPCON,'ANCODICE',w_ANCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (ANTIPCON,ANCODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_ANTIPCON;
               ,w_ANCODICE;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="RF"
        if not empty(w_COTIPCON) and not empty(w_COCODCON) 
          this.w_ULTTIPCON = w_COTIPCON
          this.w_ULTCODCON = w_COCODCON
          * --- Insert into CONTATTI
          i_nConn=i_TableProp[this.CONTATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTATTI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"COTIPCON"+",COCODCON"+",CPROWNUM"+",CPROWORD"+",COINDMAI"+",CONUMTEL"+",CODESDIV"+",CORIFPER"+",CONUMCEL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_COTIPCON),'CONTATTI','COTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(w_COCODCON),'CONTATTI','COCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DETTAROW),'CONTATTI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DETTAROW*10),'CONTATTI','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(w_COINDMAI),'CONTATTI','COINDMAI');
            +","+cp_NullLink(cp_ToStrODBC(w_CONUMTEL),'CONTATTI','CONUMTEL');
            +","+cp_NullLink(cp_ToStrODBC(w_CODESDIV),'CONTATTI','CODESDIV');
            +","+cp_NullLink(cp_ToStrODBC(w_CORIFPER),'CONTATTI','CORIFPER');
            +","+cp_NullLink(cp_ToStrODBC(w_CONUMCEL),'CONTATTI','CONUMCEL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'COTIPCON',w_COTIPCON,'COCODCON',w_COCODCON,'CPROWNUM',this.w_DETTAROW,'CPROWORD',this.w_DETTAROW*10,'COINDMAI',w_COINDMAI,'CONUMTEL',w_CONUMTEL,'CODESDIV',w_CODESDIV,'CORIFPER',w_CORIFPER,'CONUMCEL',w_CONUMCEL)
            insert into (i_cTable) (COTIPCON,COCODCON,CPROWNUM,CPROWORD,COINDMAI,CONUMTEL,CODESDIV,CORIFPER,CONUMCEL &i_ccchkf. );
               values (;
                 w_COTIPCON;
                 ,w_COCODCON;
                 ,this.w_DETTAROW;
                 ,this.w_DETTAROW*10;
                 ,w_COINDMAI;
                 ,w_CONUMTEL;
                 ,w_CODESDIV;
                 ,w_CORIFPER;
                 ,w_CONUMCEL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.w_Destinaz="NM"
        * --- Nominativi
        if IsAlt()
          * --- Utente che ha effettuato l'import (a meno che nel tracciato non venga valorizzato il campo UTCC)
          this.w_CreVarUte = IIF(VarType(w_UTCC)<>"U" And Not Empty(w_UTCC),w_UTCC, this.w_CreVarUte)
          * --- Data nella quale � stato effettuato l'import (a meno che nel tracciato non venga valorizzato il campo UTDC)
          this.w_CreVarDat = IIF(VarType(w_UTDC)<>"U" And Not Empty(w_UTDC),w_UTDC, this.w_CreVarDat)
        endif
        * --- Insert into OFF_NOMI
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NOCODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_NOCODICE),'OFF_NOMI','NOCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarUte),'OFF_NOMI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CreVarDat),'OFF_NOMI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NOCODICE',w_NOCODICE,'UTCC',this.w_CreVarUte,'UTDC',this.w_CreVarDat)
          insert into (i_cTable) (NOCODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_NOCODICE;
               ,this.w_CreVarUte;
               ,this.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if NOT EMPTY(w_ANCODCAT)
          * --- Insert into NOM_ATTR
          i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOM_ATTR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"ANCODICE"+",ANCODCAT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_NOCODICE),'NOM_ATTR','ANCODICE');
            +","+cp_NullLink(cp_ToStrODBC(w_ANCODCAT),'NOM_ATTR','ANCODCAT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'ANCODICE',w_NOCODICE,'ANCODCAT',w_ANCODCAT)
            insert into (i_cTable) (ANCODICE,ANCODCAT &i_ccchkf. );
               values (;
                 w_NOCODICE;
                 ,w_ANCODCAT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.w_Destinaz="TU"
        * --- Codice tributo nei fornitori
        if type( "w_ANCODICE" ) = "C"
          w_ANCODICE = left( ltrim(w_ANCODICE), 15 )
        endif
        * --- Insert into CONTI
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ANTIPCON"+",ANCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ANTIPCON),'CONTI','ANTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'CONTI','ANCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',w_ANTIPCON,'ANCODICE',w_ANCODICE)
          insert into (i_cTable) (ANTIPCON,ANCODICE &i_ccchkf. );
             values (;
               w_ANTIPCON;
               ,w_ANCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="KR"
        * --- Cronologico
        w_CCCODICE = left( ltrim(w_CCCODICE), 15)
        w_CCTIPCON = left( ltrim(w_CCTIPCON), 1)
        w_CCCODCON = left( ltrim(w_CCCODCON), 15)
        * --- Insert into COLCRONO
        i_nConn=i_TableProp[this.COLCRONO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COLCRONO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLCRONO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCCODICE"+",CCTIPCON"+",CCCODCON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CCCODICE),'COLCRONO','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_CCTIPCON),'COLCRONO','CCTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(w_CCCODCON),'COLCRONO','CCCODCON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCCODICE',w_CCCODICE,'CCTIPCON',w_CCTIPCON,'CCCODCON',w_CCCODCON)
          insert into (i_cTable) (CCCODICE,CCTIPCON,CCCODCON &i_ccchkf. );
             values (;
               w_CCCODICE;
               ,w_CCTIPCON;
               ,w_CCCODCON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.w_Destinaz="EC"
        * --- Causali escluse
        w_PACODAZI = left( ltrim(w_PACODAZI), 5)
        w_PACODCAU = left( ltrim(w_PACODCAU), 5)
        * --- Insert into PAR_CAUS
        i_nConn=i_TableProp[this.PAR_CAUS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CAUS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_CAUS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PACODAZI"+",PACODCAU"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_PACODAZI),'PAR_CAUS','PACODAZI');
          +","+cp_NullLink(cp_ToStrODBC(w_PACODCAU),'PAR_CAUS','PACODCAU');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PACODAZI',w_PACODAZI,'PACODCAU',w_PACODCAU)
          insert into (i_cTable) (PACODAZI,PACODCAU &i_ccchkf. );
             values (;
               w_PACODAZI;
               ,w_PACODCAU;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
    endcase
    return
  proc Try_04CCF798()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_Destinaz = "CF"
        * --- Clienti e fornitori
        w_ANFLESIG = iif(empty(w_ANFLESIG),"N", w_ANFLESIG)
        this.w_ResoDett = trim(w_ANTIPCON+w_ANCODICE)
        if IsAlt() AND VarType( w_EXNUOCLF )="C" AND w_EXNUOCLF="V"
          * --- Solo per Alterego e solo se EXNUOCLF='V'
          * --- E' gi� stato eseguito l'import da AlterII, l'import da AHW Contabilit� non deve sovrascrivere tutti i campi
          *     Elenco campi esclusi dalla scrittura:
          *     - ANFLRITE
          *     - ANCODIRP
          *     - ANCAURIT
          *     - ANTIPOCL
          this.w_APPONOTE = ""
          this.w___NOME = ""
          this.w_COGNOM = ""
          * --- Lettura del campo note per fare l'append con quanto arriva dal tracciato
          *     lettura nome e cognome per sovrascrittura solo se quanto arriva dal tracciato � pieno (ADHOC_CONTABILITA)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AN__NOTE,AN__NOME,ANCOGNOM"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AN__NOTE,AN__NOME,ANCOGNOM;
              from (i_cTable) where;
                  ANTIPCON = w_ANTIPCON;
                  and ANCODICE = w_ANCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPONOTE = NVL(cp_ToDate(_read_.AN__NOTE),cp_NullValue(_read_.AN__NOTE))
            this.w___NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
            this.w_COGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          w_AN__NOTE = iif(empty(this.w_APPONOTE),"",this.w_APPONOTE+CHR(13)+CHR(10))+w_AN__NOTE
          * --- Nome e cognome vengono sovrascritti solo se non � vuoto quantoarriva dal tracciato (ADHOC_CONTABILITA)
          w_AN__NOME = iif(empty(w_AN__NOME),this.w___NOME,w_AN__NOME)
          w_ANCOGNOM = iif(empty(w_ANCOGNOM),this.w_COGNOM,w_ANCOGNOM)
          if this.w_CLIESI="S"
            * --- se w_CLIESI='S' allora il cliente esisteva gi�, segue elenco dei campi che non vengono sovrascritti:
            *     - ANDESCRI
            *     - ANINDIRI
            *     - ANINDIR2
            *     - AN___CAP
            *     - ANLOCALI
            *     - ANPROVIN
            *     - ANTELEFO
            *     - ANTELFAX
            *     - ANNUMCEL
            *     - ANPERFIS
            *     - AN_SESSO
            *     - ANDATNAS
            *     - ANLOCNAS
            *     - ANPRONAS
            *     - ANCODFIS
            *     - ANPARIVA
            *     - ANINDWEB
            *     - AN_EMAIL
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AFFLINTR ="+cp_NullLink(cp_ToStrODBC(w_AFFLINTR),'CONTI','AFFLINTR');
              +",AN1MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN1MESCL),'CONTI','AN1MESCL');
              +",AN1SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN1SCONT),'CONTI','AN1SCONT');
              +",AN2MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN2MESCL),'CONTI','AN2MESCL');
              +",AN2SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN2SCONT),'CONTI','AN2SCONT');
              +",AN__NOME ="+cp_NullLink(cp_ToStrODBC(w_AN__NOME),'CONTI','AN__NOME');
              +",AN__NOTE ="+cp_NullLink(cp_ToStrODBC(w_AN__NOTE),'CONTI','AN__NOTE');
              +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC(w_ANBOLFAT),'CONTI','ANBOLFAT');
              +",ANCATCOM ="+cp_NullLink(cp_ToStrODBC(w_ANCATCOM),'CONTI','ANCATCOM');
              +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(w_ANCATCON),'CONTI','ANCATCON');
              +",ANCATSCM ="+cp_NullLink(cp_ToStrODBC(w_ANCATSCM),'CONTI','ANCATSCM');
              +",ANCCNOTE ="+cp_NullLink(cp_ToStrODBC(w_ANCCNOTE),'CONTI','ANCCNOTE');
              +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(w_ANCCTAGG),'CONTI','ANCCTAGG');
              +",ANCODAG1 ="+cp_NullLink(cp_ToStrODBC(w_ANCODAG1),'CONTI','ANCODAG1');
              +",ANCODATT ="+cp_NullLink(cp_ToStrODBC(w_ANCODATT),'CONTI','ANCODATT');
              +",ANCODBA2 ="+cp_NullLink(cp_ToStrODBC(w_ANCODBA2),'CONTI','ANCODBA2');
              +",ANCODBAN ="+cp_NullLink(cp_ToStrODBC(w_ANCODBAN),'CONTI','ANCODBAN');
              +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(w_ANCODIVA),'CONTI','ANCODIVA');
              +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(w_ANCODLIN),'CONTI','ANCODLIN');
              +",ANCODPAG ="+cp_NullLink(cp_ToStrODBC(w_ANCODPAG),'CONTI','ANCODPAG');
              +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(w_ANCODSTU),'CONTI','ANCODSTU');
              +",ANCODVAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODVAL),'CONTI','ANCODVAL');
              +",ANCODZON ="+cp_NullLink(cp_ToStrODBC(w_ANCODZON),'CONTI','ANCODZON');
              +",ANCOGNOM ="+cp_NullLink(cp_ToStrODBC(w_ANCOGNOM),'CONTI','ANCOGNOM');
              +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(w_ANCONCON),'CONTI','ANCONCON');
              +",ANCONRIF ="+cp_NullLink(cp_ToStrODBC(w_ANCONRIF),'CONTI','ANCONRIF');
              +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
              +",ANDATAVV ="+cp_NullLink(cp_ToStrODBC(w_ANDATAVV),'CONTI','ANDATAVV');
              +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(w_ANDATMOR),'CONTI','ANDATMOR');
              +",ANDESCR2 ="+cp_NullLink(cp_ToStrODBC(w_ANDESCR2),'CONTI','ANDESCR2');
              +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ANDTINVA),'CONTI','ANDTINVA');
              +",ANDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_ANDTOBSO),'CONTI','ANDTOBSO');
              +",ANFLBLVE ="+cp_NullLink(cp_ToStrODBC(w_ANFLBLVE),'CONTI','ANFLBLVE');
              +",ANFLCODI ="+cp_NullLink(cp_ToStrODBC(w_ANFLCODI),'CONTI','ANFLCODI');
              +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC(w_ANFLCONA),'CONTI','ANFLCONA');
              +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC(w_ANFLESIG),'CONTI','ANFLESIG');
              +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(w_ANFLFIDO),'CONTI','ANFLFIDO');
              +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC(w_ANFLGAVV),'CONTI','ANFLGAVV');
              +",ANFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_ANFLRAGG),'CONTI','ANFLRAGG');
              +",ANGIOFIS ="+cp_NullLink(cp_ToStrODBC(w_ANGIOFIS),'CONTI','ANGIOFIS');
              +",ANGIOSC1 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC1),'CONTI','ANGIOSC1');
              +",ANGIOSC2 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC2),'CONTI','ANGIOSC2');
              +",ANGRUPRO ="+cp_NullLink(cp_ToStrODBC(w_ANGRUPRO),'CONTI','ANGRUPRO');
              +",ANMAXORD ="+cp_NullLink(cp_ToStrODBC(w_ANMAXORD),'CONTI','ANMAXORD');
              +",ANNAZION ="+cp_NullLink(cp_ToStrODBC(w_ANNAZION),'CONTI','ANNAZION');
              +",ANNOTAIN ="+cp_NullLink(cp_ToStrODBC(w_ANNOTAIN),'CONTI','ANNOTAIN');
              +",ANNUMCOR ="+cp_NullLink(cp_ToStrODBC(w_ANNUMCOR),'CONTI','ANNUMCOR');
              +",ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(w_ANNUMLIS),'CONTI','ANNUMLIS');
              +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(w_ANPARTSN),'CONTI','ANPARTSN');
              +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC(w_ANPREBOL),'CONTI','ANPREBOL');
              +",ANRITENU ="+cp_NullLink(cp_ToStrODBC(w_ANRITENU),'CONTI','ANRITENU');
              +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC(w_ANSCORPO),'CONTI','ANSCORPO');
              +",ANTIPCLF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPCLF),'CONTI','ANTIPCLF');
              +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPFAT),'CONTI','ANTIPFAT');
              +",ANTIPRIF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPRIF),'CONTI','ANTIPRIF');
              +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPSOT),'CONTI','ANTIPSOT');
              +",ANVALFID ="+cp_NullLink(cp_ToStrODBC(w_ANVALFID),'CONTI','ANVALFID');
              +",ANTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPOPE),'CONTI','ANTIPOPE');
              +",ANCATOPE ="+cp_NullLink(cp_ToStrODBC("CF"),'CONTI','ANCATOPE');
              +",ANGESCON ="+cp_NullLink(cp_ToStrODBC(w_ANGESCON),'CONTI','ANGESCON');
              +",ANCODSAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODSAL),'CONTI','ANCODSAL');
              +",ANMCALST ="+cp_NullLink(cp_ToStrODBC(w_ANMCALST),'CONTI','ANMCALST');
              +",ANOPETRE ="+cp_NullLink(cp_ToStrODBC(w_ANOPETRE),'CONTI','ANOPETRE');
              +",ANTIPPRE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPPRE),'CONTI','ANTIPPRE');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                     )
            else
              update (i_cTable) set;
                  AFFLINTR = w_AFFLINTR;
                  ,AN1MESCL = w_AN1MESCL;
                  ,AN1SCONT = w_AN1SCONT;
                  ,AN2MESCL = w_AN2MESCL;
                  ,AN2SCONT = w_AN2SCONT;
                  ,AN__NOME = w_AN__NOME;
                  ,AN__NOTE = w_AN__NOTE;
                  ,ANBOLFAT = w_ANBOLFAT;
                  ,ANCATCOM = w_ANCATCOM;
                  ,ANCATCON = w_ANCATCON;
                  ,ANCATSCM = w_ANCATSCM;
                  ,ANCCNOTE = w_ANCCNOTE;
                  ,ANCCTAGG = w_ANCCTAGG;
                  ,ANCODAG1 = w_ANCODAG1;
                  ,ANCODATT = w_ANCODATT;
                  ,ANCODBA2 = w_ANCODBA2;
                  ,ANCODBAN = w_ANCODBAN;
                  ,ANCODIVA = w_ANCODIVA;
                  ,ANCODLIN = w_ANCODLIN;
                  ,ANCODPAG = w_ANCODPAG;
                  ,ANCODSTU = w_ANCODSTU;
                  ,ANCODVAL = w_ANCODVAL;
                  ,ANCODZON = w_ANCODZON;
                  ,ANCOGNOM = w_ANCOGNOM;
                  ,ANCONCON = w_ANCONCON;
                  ,ANCONRIF = w_ANCONRIF;
                  ,ANCONSUP = w_ANCONSUP;
                  ,ANDATAVV = w_ANDATAVV;
                  ,ANDATMOR = w_ANDATMOR;
                  ,ANDESCR2 = w_ANDESCR2;
                  ,ANDTINVA = w_ANDTINVA;
                  ,ANDTOBSO = w_ANDTOBSO;
                  ,ANFLBLVE = w_ANFLBLVE;
                  ,ANFLCODI = w_ANFLCODI;
                  ,ANFLCONA = w_ANFLCONA;
                  ,ANFLESIG = w_ANFLESIG;
                  ,ANFLFIDO = w_ANFLFIDO;
                  ,ANFLGAVV = w_ANFLGAVV;
                  ,ANFLRAGG = w_ANFLRAGG;
                  ,ANGIOFIS = w_ANGIOFIS;
                  ,ANGIOSC1 = w_ANGIOSC1;
                  ,ANGIOSC2 = w_ANGIOSC2;
                  ,ANGRUPRO = w_ANGRUPRO;
                  ,ANMAXORD = w_ANMAXORD;
                  ,ANNAZION = w_ANNAZION;
                  ,ANNOTAIN = w_ANNOTAIN;
                  ,ANNUMCOR = w_ANNUMCOR;
                  ,ANNUMLIS = w_ANNUMLIS;
                  ,ANPARTSN = w_ANPARTSN;
                  ,ANPREBOL = w_ANPREBOL;
                  ,ANRITENU = w_ANRITENU;
                  ,ANSCORPO = w_ANSCORPO;
                  ,ANTIPCLF = w_ANTIPCLF;
                  ,ANTIPFAT = w_ANTIPFAT;
                  ,ANTIPRIF = w_ANTIPRIF;
                  ,ANTIPSOT = w_ANTIPSOT;
                  ,ANVALFID = w_ANVALFID;
                  ,ANTIPOPE = w_ANTIPOPE;
                  ,ANCATOPE = "CF";
                  ,ANGESCON = w_ANGESCON;
                  ,ANCODSAL = w_ANCODSAL;
                  ,ANMCALST = w_ANMCALST;
                  ,ANOPETRE = w_ANOPETRE;
                  ,ANTIPPRE = w_ANTIPPRE;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = w_ANTIPCON;
                  and ANCODICE = w_ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- se w_CLIESI='N' allora il cliente non esisteva
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AFFLINTR ="+cp_NullLink(cp_ToStrODBC(w_AFFLINTR),'CONTI','AFFLINTR');
              +",AN1MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN1MESCL),'CONTI','AN1MESCL');
              +",AN1SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN1SCONT),'CONTI','AN1SCONT');
              +",AN2MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN2MESCL),'CONTI','AN2MESCL');
              +",AN2SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN2SCONT),'CONTI','AN2SCONT');
              +",AN___CAP ="+cp_NullLink(cp_ToStrODBC(w_AN___CAP),'CONTI','AN___CAP');
              +",AN__NOME ="+cp_NullLink(cp_ToStrODBC(w_AN__NOME),'CONTI','AN__NOME');
              +",AN__NOTE ="+cp_NullLink(cp_ToStrODBC(w_AN__NOTE),'CONTI','AN__NOTE');
              +",AN_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_AN_EMAIL),'CONTI','AN_EMAIL');
              +",AN_SESSO ="+cp_NullLink(cp_ToStrODBC(w_AN_SESSO),'CONTI','AN_SESSO');
              +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC(w_ANBOLFAT),'CONTI','ANBOLFAT');
              +",ANCATCOM ="+cp_NullLink(cp_ToStrODBC(w_ANCATCOM),'CONTI','ANCATCOM');
              +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(w_ANCATCON),'CONTI','ANCATCON');
              +",ANCATSCM ="+cp_NullLink(cp_ToStrODBC(w_ANCATSCM),'CONTI','ANCATSCM');
              +",ANCCNOTE ="+cp_NullLink(cp_ToStrODBC(w_ANCCNOTE),'CONTI','ANCCNOTE');
              +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(w_ANCCTAGG),'CONTI','ANCCTAGG');
              +",ANCODAG1 ="+cp_NullLink(cp_ToStrODBC(w_ANCODAG1),'CONTI','ANCODAG1');
              +",ANCODATT ="+cp_NullLink(cp_ToStrODBC(w_ANCODATT),'CONTI','ANCODATT');
              +",ANCODBA2 ="+cp_NullLink(cp_ToStrODBC(w_ANCODBA2),'CONTI','ANCODBA2');
              +",ANCODBAN ="+cp_NullLink(cp_ToStrODBC(w_ANCODBAN),'CONTI','ANCODBAN');
              +",ANCODFIS ="+cp_NullLink(cp_ToStrODBC(w_ANCODFIS),'CONTI','ANCODFIS');
              +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(w_ANCODIVA),'CONTI','ANCODIVA');
              +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(w_ANCODLIN),'CONTI','ANCODLIN');
              +",ANCODPAG ="+cp_NullLink(cp_ToStrODBC(w_ANCODPAG),'CONTI','ANCODPAG');
              +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(w_ANCODSTU),'CONTI','ANCODSTU');
              +",ANCODVAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODVAL),'CONTI','ANCODVAL');
              +",ANCODZON ="+cp_NullLink(cp_ToStrODBC(w_ANCODZON),'CONTI','ANCODZON');
              +",ANCOGNOM ="+cp_NullLink(cp_ToStrODBC(w_ANCOGNOM),'CONTI','ANCOGNOM');
              +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(w_ANCONCON),'CONTI','ANCONCON');
              +",ANCONRIF ="+cp_NullLink(cp_ToStrODBC(w_ANCONRIF),'CONTI','ANCONRIF');
              +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
              +",ANDATAVV ="+cp_NullLink(cp_ToStrODBC(w_ANDATAVV),'CONTI','ANDATAVV');
              +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(w_ANDATMOR),'CONTI','ANDATMOR');
              +",ANDATNAS ="+cp_NullLink(cp_ToStrODBC(w_ANDATNAS),'CONTI','ANDATNAS');
              +",ANDESCR2 ="+cp_NullLink(cp_ToStrODBC(w_ANDESCR2),'CONTI','ANDESCR2');
              +",ANDESCRI ="+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'CONTI','ANDESCRI');
              +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ANDTINVA),'CONTI','ANDTINVA');
              +",ANDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_ANDTOBSO),'CONTI','ANDTOBSO');
              +",ANFLBLVE ="+cp_NullLink(cp_ToStrODBC(w_ANFLBLVE),'CONTI','ANFLBLVE');
              +",ANFLCODI ="+cp_NullLink(cp_ToStrODBC(w_ANFLCODI),'CONTI','ANFLCODI');
              +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC(w_ANFLCONA),'CONTI','ANFLCONA');
              +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC(w_ANFLESIG),'CONTI','ANFLESIG');
              +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(w_ANFLFIDO),'CONTI','ANFLFIDO');
              +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC(w_ANFLGAVV),'CONTI','ANFLGAVV');
              +",ANFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_ANFLRAGG),'CONTI','ANFLRAGG');
              +",ANGIOFIS ="+cp_NullLink(cp_ToStrODBC(w_ANGIOFIS),'CONTI','ANGIOFIS');
              +",ANGIOSC1 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC1),'CONTI','ANGIOSC1');
              +",ANGIOSC2 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC2),'CONTI','ANGIOSC2');
              +",ANGRUPRO ="+cp_NullLink(cp_ToStrODBC(w_ANGRUPRO),'CONTI','ANGRUPRO');
              +",ANINDIR2 ="+cp_NullLink(cp_ToStrODBC(w_ANINDIR2),'CONTI','ANINDIR2');
              +",ANINDIRI ="+cp_NullLink(cp_ToStrODBC(w_ANINDIRI),'CONTI','ANINDIRI');
              +",ANINDWEB ="+cp_NullLink(cp_ToStrODBC(w_ANINDWEB),'CONTI','ANINDWEB');
              +",ANLOCALI ="+cp_NullLink(cp_ToStrODBC(w_ANLOCALI),'CONTI','ANLOCALI');
              +",ANLOCNAS ="+cp_NullLink(cp_ToStrODBC(w_ANLOCNAS),'CONTI','ANLOCNAS');
              +",ANMAXORD ="+cp_NullLink(cp_ToStrODBC(w_ANMAXORD),'CONTI','ANMAXORD');
              +",ANNAZION ="+cp_NullLink(cp_ToStrODBC(w_ANNAZION),'CONTI','ANNAZION');
              +",ANNOTAIN ="+cp_NullLink(cp_ToStrODBC(w_ANNOTAIN),'CONTI','ANNOTAIN');
              +",ANNUMCEL ="+cp_NullLink(cp_ToStrODBC(w_ANNUMCEL),'CONTI','ANNUMCEL');
              +",ANNUMCOR ="+cp_NullLink(cp_ToStrODBC(w_ANNUMCOR),'CONTI','ANNUMCOR');
              +",ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(w_ANNUMLIS),'CONTI','ANNUMLIS');
              +",ANPARIVA ="+cp_NullLink(cp_ToStrODBC(w_ANPARIVA),'CONTI','ANPARIVA');
              +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(w_ANPARTSN),'CONTI','ANPARTSN');
              +",ANPERFIS ="+cp_NullLink(cp_ToStrODBC(w_ANPERFIS),'CONTI','ANPERFIS');
              +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC(w_ANPREBOL),'CONTI','ANPREBOL');
              +",ANPRONAS ="+cp_NullLink(cp_ToStrODBC(w_ANPRONAS),'CONTI','ANPRONAS');
              +",ANPROVIN ="+cp_NullLink(cp_ToStrODBC(w_ANPROVIN),'CONTI','ANPROVIN');
              +",ANRITENU ="+cp_NullLink(cp_ToStrODBC(w_ANRITENU),'CONTI','ANRITENU');
              +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC(w_ANSCORPO),'CONTI','ANSCORPO');
              +",ANTELEFO ="+cp_NullLink(cp_ToStrODBC(w_ANTELEFO),'CONTI','ANTELEFO');
              +",ANTELFAX ="+cp_NullLink(cp_ToStrODBC(w_ANTELFAX),'CONTI','ANTELFAX');
              +",ANTIPCLF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPCLF),'CONTI','ANTIPCLF');
              +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPFAT),'CONTI','ANTIPFAT');
              +",ANTIPRIF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPRIF),'CONTI','ANTIPRIF');
              +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPSOT),'CONTI','ANTIPSOT');
              +",ANVALFID ="+cp_NullLink(cp_ToStrODBC(w_ANVALFID),'CONTI','ANVALFID');
              +",ANTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPOPE),'CONTI','ANTIPOPE');
              +",ANCATOPE ="+cp_NullLink(cp_ToStrODBC("CF"),'CONTI','ANCATOPE');
              +",ANGESCON ="+cp_NullLink(cp_ToStrODBC(w_ANGESCON),'CONTI','ANGESCON');
              +",ANCODSAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODSAL),'CONTI','ANCODSAL');
              +",ANMCALST ="+cp_NullLink(cp_ToStrODBC(w_ANMCALST),'CONTI','ANMCALST');
              +",ANOPETRE ="+cp_NullLink(cp_ToStrODBC(w_ANOPETRE),'CONTI','ANOPETRE');
              +",ANTIPPRE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPPRE),'CONTI','ANTIPPRE');
              +",ANMAGTER ="+cp_NullLink(cp_ToStrODBC(w_ANMAGTER),'CONTI','ANMAGTER');
              +",AN_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_AN_EMPEC),'CONTI','AN_EMPEC');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                     )
            else
              update (i_cTable) set;
                  AFFLINTR = w_AFFLINTR;
                  ,AN1MESCL = w_AN1MESCL;
                  ,AN1SCONT = w_AN1SCONT;
                  ,AN2MESCL = w_AN2MESCL;
                  ,AN2SCONT = w_AN2SCONT;
                  ,AN___CAP = w_AN___CAP;
                  ,AN__NOME = w_AN__NOME;
                  ,AN__NOTE = w_AN__NOTE;
                  ,AN_EMAIL = w_AN_EMAIL;
                  ,AN_SESSO = w_AN_SESSO;
                  ,ANBOLFAT = w_ANBOLFAT;
                  ,ANCATCOM = w_ANCATCOM;
                  ,ANCATCON = w_ANCATCON;
                  ,ANCATSCM = w_ANCATSCM;
                  ,ANCCNOTE = w_ANCCNOTE;
                  ,ANCCTAGG = w_ANCCTAGG;
                  ,ANCODAG1 = w_ANCODAG1;
                  ,ANCODATT = w_ANCODATT;
                  ,ANCODBA2 = w_ANCODBA2;
                  ,ANCODBAN = w_ANCODBAN;
                  ,ANCODFIS = w_ANCODFIS;
                  ,ANCODIVA = w_ANCODIVA;
                  ,ANCODLIN = w_ANCODLIN;
                  ,ANCODPAG = w_ANCODPAG;
                  ,ANCODSTU = w_ANCODSTU;
                  ,ANCODVAL = w_ANCODVAL;
                  ,ANCODZON = w_ANCODZON;
                  ,ANCOGNOM = w_ANCOGNOM;
                  ,ANCONCON = w_ANCONCON;
                  ,ANCONRIF = w_ANCONRIF;
                  ,ANCONSUP = w_ANCONSUP;
                  ,ANDATAVV = w_ANDATAVV;
                  ,ANDATMOR = w_ANDATMOR;
                  ,ANDATNAS = w_ANDATNAS;
                  ,ANDESCR2 = w_ANDESCR2;
                  ,ANDESCRI = w_ANDESCRI;
                  ,ANDTINVA = w_ANDTINVA;
                  ,ANDTOBSO = w_ANDTOBSO;
                  ,ANFLBLVE = w_ANFLBLVE;
                  ,ANFLCODI = w_ANFLCODI;
                  ,ANFLCONA = w_ANFLCONA;
                  ,ANFLESIG = w_ANFLESIG;
                  ,ANFLFIDO = w_ANFLFIDO;
                  ,ANFLGAVV = w_ANFLGAVV;
                  ,ANFLRAGG = w_ANFLRAGG;
                  ,ANGIOFIS = w_ANGIOFIS;
                  ,ANGIOSC1 = w_ANGIOSC1;
                  ,ANGIOSC2 = w_ANGIOSC2;
                  ,ANGRUPRO = w_ANGRUPRO;
                  ,ANINDIR2 = w_ANINDIR2;
                  ,ANINDIRI = w_ANINDIRI;
                  ,ANINDWEB = w_ANINDWEB;
                  ,ANLOCALI = w_ANLOCALI;
                  ,ANLOCNAS = w_ANLOCNAS;
                  ,ANMAXORD = w_ANMAXORD;
                  ,ANNAZION = w_ANNAZION;
                  ,ANNOTAIN = w_ANNOTAIN;
                  ,ANNUMCEL = w_ANNUMCEL;
                  ,ANNUMCOR = w_ANNUMCOR;
                  ,ANNUMLIS = w_ANNUMLIS;
                  ,ANPARIVA = w_ANPARIVA;
                  ,ANPARTSN = w_ANPARTSN;
                  ,ANPERFIS = w_ANPERFIS;
                  ,ANPREBOL = w_ANPREBOL;
                  ,ANPRONAS = w_ANPRONAS;
                  ,ANPROVIN = w_ANPROVIN;
                  ,ANRITENU = w_ANRITENU;
                  ,ANSCORPO = w_ANSCORPO;
                  ,ANTELEFO = w_ANTELEFO;
                  ,ANTELFAX = w_ANTELFAX;
                  ,ANTIPCLF = w_ANTIPCLF;
                  ,ANTIPFAT = w_ANTIPFAT;
                  ,ANTIPRIF = w_ANTIPRIF;
                  ,ANTIPSOT = w_ANTIPSOT;
                  ,ANVALFID = w_ANVALFID;
                  ,ANTIPOPE = w_ANTIPOPE;
                  ,ANCATOPE = "CF";
                  ,ANGESCON = w_ANGESCON;
                  ,ANCODSAL = w_ANCODSAL;
                  ,ANMCALST = w_ANMCALST;
                  ,ANOPETRE = w_ANOPETRE;
                  ,ANTIPPRE = w_ANTIPPRE;
                  ,ANMAGTER = w_ANMAGTER;
                  ,AN_EMPEC = w_AN_EMPEC;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = w_ANTIPCON;
                  and ANCODICE = w_ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          * --- Write into CONTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AFFLINTR ="+cp_NullLink(cp_ToStrODBC(w_AFFLINTR),'CONTI','AFFLINTR');
            +",AN1MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN1MESCL),'CONTI','AN1MESCL');
            +",AN1SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN1SCONT),'CONTI','AN1SCONT');
            +",AN2MESCL ="+cp_NullLink(cp_ToStrODBC(w_AN2MESCL),'CONTI','AN2MESCL');
            +",AN2SCONT ="+cp_NullLink(cp_ToStrODBC(w_AN2SCONT),'CONTI','AN2SCONT');
            +",AN___CAP ="+cp_NullLink(cp_ToStrODBC(w_AN___CAP),'CONTI','AN___CAP');
            +",AN__NOME ="+cp_NullLink(cp_ToStrODBC(w_AN__NOME),'CONTI','AN__NOME');
            +",AN__NOTE ="+cp_NullLink(cp_ToStrODBC(w_AN__NOTE),'CONTI','AN__NOTE');
            +",AN_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_AN_EMAIL),'CONTI','AN_EMAIL');
            +",AN_SESSO ="+cp_NullLink(cp_ToStrODBC(w_AN_SESSO),'CONTI','AN_SESSO');
            +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC(w_ANBOLFAT),'CONTI','ANBOLFAT');
            +",ANCATCOM ="+cp_NullLink(cp_ToStrODBC(w_ANCATCOM),'CONTI','ANCATCOM');
            +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(w_ANCATCON),'CONTI','ANCATCON');
            +",ANCATSCM ="+cp_NullLink(cp_ToStrODBC(w_ANCATSCM),'CONTI','ANCATSCM');
            +",ANCCNOTE ="+cp_NullLink(cp_ToStrODBC(w_ANCCNOTE),'CONTI','ANCCNOTE');
            +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(w_ANCCTAGG),'CONTI','ANCCTAGG');
            +",ANCODAG1 ="+cp_NullLink(cp_ToStrODBC(w_ANCODAG1),'CONTI','ANCODAG1');
            +",ANCODATT ="+cp_NullLink(cp_ToStrODBC(w_ANCODATT),'CONTI','ANCODATT');
            +",ANCODBA2 ="+cp_NullLink(cp_ToStrODBC(w_ANCODBA2),'CONTI','ANCODBA2');
            +",ANCODBAN ="+cp_NullLink(cp_ToStrODBC(w_ANCODBAN),'CONTI','ANCODBAN');
            +",ANCODFIS ="+cp_NullLink(cp_ToStrODBC(w_ANCODFIS),'CONTI','ANCODFIS');
            +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(w_ANCODIVA),'CONTI','ANCODIVA');
            +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(w_ANCODLIN),'CONTI','ANCODLIN');
            +",ANCODPAG ="+cp_NullLink(cp_ToStrODBC(w_ANCODPAG),'CONTI','ANCODPAG');
            +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(w_ANCODSTU),'CONTI','ANCODSTU');
            +",ANCODVAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODVAL),'CONTI','ANCODVAL');
            +",ANCODZON ="+cp_NullLink(cp_ToStrODBC(w_ANCODZON),'CONTI','ANCODZON');
            +",ANCOGNOM ="+cp_NullLink(cp_ToStrODBC(w_ANCOGNOM),'CONTI','ANCOGNOM');
            +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(w_ANCONCON),'CONTI','ANCONCON');
            +",ANCONRIF ="+cp_NullLink(cp_ToStrODBC(w_ANCONRIF),'CONTI','ANCONRIF');
            +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
            +",ANDATAVV ="+cp_NullLink(cp_ToStrODBC(w_ANDATAVV),'CONTI','ANDATAVV');
            +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(w_ANDATMOR),'CONTI','ANDATMOR');
            +",ANDATNAS ="+cp_NullLink(cp_ToStrODBC(w_ANDATNAS),'CONTI','ANDATNAS');
            +",ANDESCR2 ="+cp_NullLink(cp_ToStrODBC(w_ANDESCR2),'CONTI','ANDESCR2');
            +",ANDESCRI ="+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'CONTI','ANDESCRI');
            +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ANDTINVA),'CONTI','ANDTINVA');
            +",ANDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_ANDTOBSO),'CONTI','ANDTOBSO');
            +",ANFLBLVE ="+cp_NullLink(cp_ToStrODBC(w_ANFLBLVE),'CONTI','ANFLBLVE');
            +",ANFLCODI ="+cp_NullLink(cp_ToStrODBC(w_ANFLCODI),'CONTI','ANFLCODI');
            +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC(w_ANFLCONA),'CONTI','ANFLCONA');
            +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC(w_ANFLESIG),'CONTI','ANFLESIG');
            +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(w_ANFLFIDO),'CONTI','ANFLFIDO');
            +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC(w_ANFLGAVV),'CONTI','ANFLGAVV');
            +",ANFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_ANFLRAGG),'CONTI','ANFLRAGG');
            +",ANGIOFIS ="+cp_NullLink(cp_ToStrODBC(w_ANGIOFIS),'CONTI','ANGIOFIS');
            +",ANGIOSC1 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC1),'CONTI','ANGIOSC1');
            +",ANGIOSC2 ="+cp_NullLink(cp_ToStrODBC(w_ANGIOSC2),'CONTI','ANGIOSC2');
            +",ANGRUPRO ="+cp_NullLink(cp_ToStrODBC(w_ANGRUPRO),'CONTI','ANGRUPRO');
            +",ANINDIR2 ="+cp_NullLink(cp_ToStrODBC(w_ANINDIR2),'CONTI','ANINDIR2');
            +",ANINDIRI ="+cp_NullLink(cp_ToStrODBC(w_ANINDIRI),'CONTI','ANINDIRI');
            +",ANINDWEB ="+cp_NullLink(cp_ToStrODBC(w_ANINDWEB),'CONTI','ANINDWEB');
            +",ANLOCALI ="+cp_NullLink(cp_ToStrODBC(w_ANLOCALI),'CONTI','ANLOCALI');
            +",ANLOCNAS ="+cp_NullLink(cp_ToStrODBC(w_ANLOCNAS),'CONTI','ANLOCNAS');
            +",ANMAXORD ="+cp_NullLink(cp_ToStrODBC(w_ANMAXORD),'CONTI','ANMAXORD');
            +",ANNAZION ="+cp_NullLink(cp_ToStrODBC(w_ANNAZION),'CONTI','ANNAZION');
            +",ANNOTAIN ="+cp_NullLink(cp_ToStrODBC(w_ANNOTAIN),'CONTI','ANNOTAIN');
            +",ANNUMCEL ="+cp_NullLink(cp_ToStrODBC(w_ANNUMCEL),'CONTI','ANNUMCEL');
            +",ANNUMCOR ="+cp_NullLink(cp_ToStrODBC(w_ANNUMCOR),'CONTI','ANNUMCOR');
            +",ANNUMLIS ="+cp_NullLink(cp_ToStrODBC(w_ANNUMLIS),'CONTI','ANNUMLIS');
            +",ANPARIVA ="+cp_NullLink(cp_ToStrODBC(w_ANPARIVA),'CONTI','ANPARIVA');
            +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(w_ANPARTSN),'CONTI','ANPARTSN');
            +",ANPERFIS ="+cp_NullLink(cp_ToStrODBC(w_ANPERFIS),'CONTI','ANPERFIS');
            +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC(w_ANPREBOL),'CONTI','ANPREBOL');
            +",ANPRONAS ="+cp_NullLink(cp_ToStrODBC(w_ANPRONAS),'CONTI','ANPRONAS');
            +",ANPROVIN ="+cp_NullLink(cp_ToStrODBC(w_ANPROVIN),'CONTI','ANPROVIN');
            +",ANRITENU ="+cp_NullLink(cp_ToStrODBC(w_ANRITENU),'CONTI','ANRITENU');
            +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC(w_ANSCORPO),'CONTI','ANSCORPO');
            +",ANTELEFO ="+cp_NullLink(cp_ToStrODBC(w_ANTELEFO),'CONTI','ANTELEFO');
            +",ANTELFAX ="+cp_NullLink(cp_ToStrODBC(w_ANTELFAX),'CONTI','ANTELFAX');
            +",ANTIPCLF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPCLF),'CONTI','ANTIPCLF');
            +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPFAT),'CONTI','ANTIPFAT');
            +",ANTIPRIF ="+cp_NullLink(cp_ToStrODBC(w_ANTIPRIF),'CONTI','ANTIPRIF');
            +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPSOT),'CONTI','ANTIPSOT');
            +",ANVALFID ="+cp_NullLink(cp_ToStrODBC(w_ANVALFID),'CONTI','ANVALFID');
            +",ANTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPOPE),'CONTI','ANTIPOPE');
            +",ANCATOPE ="+cp_NullLink(cp_ToStrODBC("CF"),'CONTI','ANCATOPE');
            +",ANGESCON ="+cp_NullLink(cp_ToStrODBC(w_ANGESCON),'CONTI','ANGESCON');
            +",ANFLRITE ="+cp_NullLink(cp_ToStrODBC(w_ANFLRITE),'CONTI','ANFLRITE');
            +",ANCODIRP ="+cp_NullLink(cp_ToStrODBC(w_ANCODIRP),'CONTI','ANCODIRP');
            +",ANCAURIT ="+cp_NullLink(cp_ToStrODBC(w_ANCAURIT),'CONTI','ANCAURIT');
            +",ANCODSAL ="+cp_NullLink(cp_ToStrODBC(w_ANCODSAL),'CONTI','ANCODSAL');
            +",ANMCALST ="+cp_NullLink(cp_ToStrODBC(w_ANMCALST),'CONTI','ANMCALST');
            +",ANTIPOCL ="+cp_NullLink(cp_ToStrODBC(w_ANTIPOCL),'CONTI','ANTIPOCL');
            +",UTCV ="+cp_NullLink(cp_ToStrODBC(w_UTCV),'CONTI','UTCV');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(w_UTDV),'CONTI','UTDV');
            +",ANOPETRE ="+cp_NullLink(cp_ToStrODBC(w_ANOPETRE),'CONTI','ANOPETRE');
            +",ANTIPPRE ="+cp_NullLink(cp_ToStrODBC(w_ANTIPPRE),'CONTI','ANTIPPRE');
            +",ANFLBLLS ="+cp_NullLink(cp_ToStrODBC(w_ANFLBLLS),'CONTI','ANFLBLLS');
            +",ANCHKMAI ="+cp_NullLink(cp_ToStrODBC(w_ANCHKMAI),'CONTI','ANCHKMAI');
            +",ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC(w_ANFLGCPZ ),'CONTI','ANFLGCPZ');
            +",ANFLPRIV ="+cp_NullLink(cp_ToStrODBC(w_ANFLPRIV),'CONTI','ANFLPRIV');
            +",ANCHKFAX ="+cp_NullLink(cp_ToStrODBC(w_ANCHKFAX),'CONTI','ANCHKFAX');
            +",ANFLSOAL ="+cp_NullLink(cp_ToStrODBC(w_ANFLSOAL),'CONTI','ANFLSOAL');
            +",ANCHKSTA ="+cp_NullLink(cp_ToStrODBC(w_ANCHKSTA),'CONTI','ANCHKSTA');
            +",ANCODCUC ="+cp_NullLink(cp_ToStrODBC(w_ANCODCUC),'CONTI','ANCODCUC');
            +",ANMAGTER ="+cp_NullLink(cp_ToStrODBC(w_ANMAGTER),'CONTI','ANMAGTER');
            +",AN_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_AN_EMPEC),'CONTI','AN_EMPEC');
            +",ANCHKPEC ="+cp_NullLink(cp_ToStrODBC(w_ANCHKPEC),'CONTI','ANCHKPEC');
                +i_ccchkf ;
            +" where ";
                +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                   )
          else
            update (i_cTable) set;
                AFFLINTR = w_AFFLINTR;
                ,AN1MESCL = w_AN1MESCL;
                ,AN1SCONT = w_AN1SCONT;
                ,AN2MESCL = w_AN2MESCL;
                ,AN2SCONT = w_AN2SCONT;
                ,AN___CAP = w_AN___CAP;
                ,AN__NOME = w_AN__NOME;
                ,AN__NOTE = w_AN__NOTE;
                ,AN_EMAIL = w_AN_EMAIL;
                ,AN_SESSO = w_AN_SESSO;
                ,ANBOLFAT = w_ANBOLFAT;
                ,ANCATCOM = w_ANCATCOM;
                ,ANCATCON = w_ANCATCON;
                ,ANCATSCM = w_ANCATSCM;
                ,ANCCNOTE = w_ANCCNOTE;
                ,ANCCTAGG = w_ANCCTAGG;
                ,ANCODAG1 = w_ANCODAG1;
                ,ANCODATT = w_ANCODATT;
                ,ANCODBA2 = w_ANCODBA2;
                ,ANCODBAN = w_ANCODBAN;
                ,ANCODFIS = w_ANCODFIS;
                ,ANCODIVA = w_ANCODIVA;
                ,ANCODLIN = w_ANCODLIN;
                ,ANCODPAG = w_ANCODPAG;
                ,ANCODSTU = w_ANCODSTU;
                ,ANCODVAL = w_ANCODVAL;
                ,ANCODZON = w_ANCODZON;
                ,ANCOGNOM = w_ANCOGNOM;
                ,ANCONCON = w_ANCONCON;
                ,ANCONRIF = w_ANCONRIF;
                ,ANCONSUP = w_ANCONSUP;
                ,ANDATAVV = w_ANDATAVV;
                ,ANDATMOR = w_ANDATMOR;
                ,ANDATNAS = w_ANDATNAS;
                ,ANDESCR2 = w_ANDESCR2;
                ,ANDESCRI = w_ANDESCRI;
                ,ANDTINVA = w_ANDTINVA;
                ,ANDTOBSO = w_ANDTOBSO;
                ,ANFLBLVE = w_ANFLBLVE;
                ,ANFLCODI = w_ANFLCODI;
                ,ANFLCONA = w_ANFLCONA;
                ,ANFLESIG = w_ANFLESIG;
                ,ANFLFIDO = w_ANFLFIDO;
                ,ANFLGAVV = w_ANFLGAVV;
                ,ANFLRAGG = w_ANFLRAGG;
                ,ANGIOFIS = w_ANGIOFIS;
                ,ANGIOSC1 = w_ANGIOSC1;
                ,ANGIOSC2 = w_ANGIOSC2;
                ,ANGRUPRO = w_ANGRUPRO;
                ,ANINDIR2 = w_ANINDIR2;
                ,ANINDIRI = w_ANINDIRI;
                ,ANINDWEB = w_ANINDWEB;
                ,ANLOCALI = w_ANLOCALI;
                ,ANLOCNAS = w_ANLOCNAS;
                ,ANMAXORD = w_ANMAXORD;
                ,ANNAZION = w_ANNAZION;
                ,ANNOTAIN = w_ANNOTAIN;
                ,ANNUMCEL = w_ANNUMCEL;
                ,ANNUMCOR = w_ANNUMCOR;
                ,ANNUMLIS = w_ANNUMLIS;
                ,ANPARIVA = w_ANPARIVA;
                ,ANPARTSN = w_ANPARTSN;
                ,ANPERFIS = w_ANPERFIS;
                ,ANPREBOL = w_ANPREBOL;
                ,ANPRONAS = w_ANPRONAS;
                ,ANPROVIN = w_ANPROVIN;
                ,ANRITENU = w_ANRITENU;
                ,ANSCORPO = w_ANSCORPO;
                ,ANTELEFO = w_ANTELEFO;
                ,ANTELFAX = w_ANTELFAX;
                ,ANTIPCLF = w_ANTIPCLF;
                ,ANTIPFAT = w_ANTIPFAT;
                ,ANTIPRIF = w_ANTIPRIF;
                ,ANTIPSOT = w_ANTIPSOT;
                ,ANVALFID = w_ANVALFID;
                ,ANTIPOPE = w_ANTIPOPE;
                ,ANCATOPE = "CF";
                ,ANGESCON = w_ANGESCON;
                ,ANFLRITE = w_ANFLRITE;
                ,ANCODIRP = w_ANCODIRP;
                ,ANCAURIT = w_ANCAURIT;
                ,ANCODSAL = w_ANCODSAL;
                ,ANMCALST = w_ANMCALST;
                ,ANTIPOCL = w_ANTIPOCL;
                ,UTCV = w_UTCV;
                ,UTDV = w_UTDV;
                ,ANOPETRE = w_ANOPETRE;
                ,ANTIPPRE = w_ANTIPPRE;
                ,ANFLBLLS = w_ANFLBLLS;
                ,ANCHKMAI = w_ANCHKMAI;
                ,ANFLGCPZ = w_ANFLGCPZ ;
                ,ANFLPRIV = w_ANFLPRIV;
                ,ANCHKFAX = w_ANCHKFAX;
                ,ANFLSOAL = w_ANFLSOAL;
                ,ANCHKSTA = w_ANCHKSTA;
                ,ANCODCUC = w_ANCODCUC;
                ,ANMAGTER = w_ANMAGTER;
                ,AN_EMPEC = w_AN_EMPEC;
                ,ANCHKPEC = w_ANCHKPEC;
                &i_ccchkf. ;
             where;
                ANTIPCON = w_ANTIPCON;
                and ANCODICE = w_ANCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_righe = i_ROWS
          if VARTYPE(w_CCCODBAN)<>"U" AND !EMPTY(w_CCCODBAN) AND VARTYPE(w_CCCONCOR) <>"U" AND ! EMPTY(w_CCCONCOR)
            w_CC_EMAIL=iif(vartype(w_CC_EMAIL)="U",space(50),w_CC_EMAIL) 
 w_CCCINABI=iif(vartype(w_CCCINABI)="U",space(1),w_CCCINABI) 
 w_CCCINCAB=iif(vartype(w_CCCINCAB)="U",space(2),w_CCCINCAB) 
 w_CCCOBBAN=iif(vartype(w_CCCOBBAN)="U",space(23),w_CCCOBBAN) 
 w_CCCODBIC=iif(vartype(w_CCCODBIC)="U",space(11),w_CCCODBIC) 
 w_CCCODVAL=iif(vartype(w_CCCODVAL)="U",NULL,w_CCCODVAL) 
 w_CCCOIBAN=iif(vartype(w_CCCOIBAN)="U",space(34),w_CCCOIBAN) 
 w_CCDESBAN=iif(vartype(w_CCDESBAN)="U",space(50),w_CCDESBAN) 
 w_CCNUMTEL=iif(vartype(w_CCNUMTEL)="U",space(18),w_CCNUMTEL) 
 w_CCOBSOLE=iif(vartype(w_CCOBSOLE)="U",space(1),w_CCOBSOLE) 
 w_CC_EMPEC=iif(vartype(w_CC_EMPEC)="U",space(50),w_CC_EMPEC) 
            * --- Write into BAN_CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.BAN_CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CC_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_CC_EMAIL),'BAN_CONTI','CC_EMAIL');
              +",CCCINABI ="+cp_NullLink(cp_ToStrODBC(w_CCCINABI),'BAN_CONTI','CCCINABI');
              +",CCCINCAB ="+cp_NullLink(cp_ToStrODBC(w_CCCINCAB),'BAN_CONTI','CCCINCAB');
              +",CCCOBBAN ="+cp_NullLink(cp_ToStrODBC(w_CCCOBBAN),'BAN_CONTI','CCCOBBAN');
              +",CCCODBIC ="+cp_NullLink(cp_ToStrODBC(w_CCCODBIC ),'BAN_CONTI','CCCODBIC');
              +",CCCODVAL ="+cp_NullLink(cp_ToStrODBC(w_CCCODVAL),'BAN_CONTI','CCCODVAL');
              +",CCCOIBAN ="+cp_NullLink(cp_ToStrODBC(w_CCCOIBAN),'BAN_CONTI','CCCOIBAN');
              +",CCDESBAN ="+cp_NullLink(cp_ToStrODBC(w_CCDESBAN),'BAN_CONTI','CCDESBAN');
              +",CCNUMTEL ="+cp_NullLink(cp_ToStrODBC(w_CCNUMTEL),'BAN_CONTI','CCNUMTEL');
              +",CCOBSOLE ="+cp_NullLink(cp_ToStrODBC(w_CCOBSOLE),'BAN_CONTI','CCOBSOLE');
              +",CC_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_CC_EMPEC),'BAN_CONTI','CC_EMPEC');
                  +i_ccchkf ;
              +" where ";
                  +"CCTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                  +" and CCCODCON = "+cp_ToStrODBC(w_ANCODICE);
                  +" and CCCODBAN = "+cp_ToStrODBC(w_CCCODBAN);
                  +" and CCCONCOR = "+cp_ToStrODBC(w_CCCONCOR);
                     )
            else
              update (i_cTable) set;
                  CC_EMAIL = w_CC_EMAIL;
                  ,CCCINABI = w_CCCINABI;
                  ,CCCINCAB = w_CCCINCAB;
                  ,CCCOBBAN = w_CCCOBBAN;
                  ,CCCODBIC = w_CCCODBIC ;
                  ,CCCODVAL = w_CCCODVAL;
                  ,CCCOIBAN = w_CCCOIBAN;
                  ,CCDESBAN = w_CCDESBAN;
                  ,CCNUMTEL = w_CCNUMTEL;
                  ,CCOBSOLE = w_CCOBSOLE;
                  ,CC_EMPEC = w_CC_EMPEC;
                  &i_ccchkf. ;
               where;
                  CCTIPCON = w_ANTIPCON;
                  and CCCODCON = w_ANCODICE;
                  and CCCODBAN = w_CCCODBAN;
                  and CCCONCOR = w_CCCONCOR;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if i_ROWS=0
            i_rows=this.w_righe
          endif
        endif
      case this.w_Destinaz = "ZO"
        * --- Zone
        this.w_ResoDett = trim(w_ZOCODZON)
        * --- Write into ZONE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ZONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ZONE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ZONE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ZODESZON ="+cp_NullLink(cp_ToStrODBC(w_ZODESZON),'ZONE','ZODESZON');
          +",ZODTINVA ="+cp_NullLink(cp_ToStrODBC(w_ZODTINVA),'ZONE','ZODTINVA');
          +",ZODTOBSO ="+cp_NullLink(cp_ToStrODBC(w_ZODTOBSO),'ZONE','ZODTOBSO');
              +i_ccchkf ;
          +" where ";
              +"ZOCODZON = "+cp_ToStrODBC(w_ZOCODZON);
                 )
        else
          update (i_cTable) set;
              ZODESZON = w_ZODESZON;
              ,ZODTINVA = w_ZODTINVA;
              ,ZODTOBSO = w_ZODTOBSO;
              &i_ccchkf. ;
           where;
              ZOCODZON = w_ZOCODZON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "NA"
        * --- Nazioni
        this.w_ResoDett = trim(w_NACODNAZ)
        * --- Write into NAZIONI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NAZIONI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NADESNAZ ="+cp_NullLink(cp_ToStrODBC(w_NADESNAZ),'NAZIONI','NADESNAZ');
          +",NACODISO ="+cp_NullLink(cp_ToStrODBC(w_NACODISO),'NAZIONI','NACODISO');
          +",NACODVAL ="+cp_NullLink(cp_ToStrODBC(w_NACODVAL),'NAZIONI','NACODVAL');
          +",NACODEST ="+cp_NullLink(cp_ToStrODBC(w_NACODEST),'NAZIONI','NACODEST');
          +",NAPAGISO ="+cp_NullLink(cp_ToStrODBC(w_NAPAGISO),'NAZIONI','NAPAGISO');
          +",NACODUIC ="+cp_NullLink(cp_ToStrODBC(w_NACODUIC),'NAZIONI','NACODUIC');
              +i_ccchkf ;
          +" where ";
              +"NACODNAZ = "+cp_ToStrODBC(w_NACODNAZ);
                 )
        else
          update (i_cTable) set;
              NADESNAZ = w_NADESNAZ;
              ,NACODISO = w_NACODISO;
              ,NACODVAL = w_NACODVAL;
              ,NACODEST = w_NACODEST;
              ,NAPAGISO = w_NAPAGISO;
              ,NACODUIC = w_NACODUIC;
              &i_ccchkf. ;
           where;
              NACODNAZ = w_NACODNAZ;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "LI"
        * --- Lingue
        this.w_ResoDett = trim(w_LUCODICE)
        * --- Write into LINGUE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LINGUE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LINGUE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LINGUE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LUDESCRI ="+cp_NullLink(cp_ToStrODBC(w_LUDESCRI),'LINGUE','LUDESCRI');
              +i_ccchkf ;
          +" where ";
              +"LUCODICE = "+cp_ToStrODBC(w_LUCODICE);
                 )
        else
          update (i_cTable) set;
              LUDESCRI = w_LUDESCRI;
              &i_ccchkf. ;
           where;
              LUCODICE = w_LUCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "VA"
        * --- Valute
        w_VADATEUR = iif(type("w_VADATEUR")="U",cp_CharToDate("  -  -    "), w_VADATEUR) 
 w_VAUNIVAL = iif(type("w_VAUNIVAL")="U", SPACE(3), w_VAUNIVAL) 
 w_VAOPECAM = iif(type("w_VAOPECAM")="U", SPACE(1), w_VAOPECAM)
        this.w_ResoDett = trim(w_VACODVAL)
        * --- Write into VALUTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.VALUTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"VADESVAL ="+cp_NullLink(cp_ToStrODBC(w_VADESVAL),'VALUTE','VADESVAL');
          +",VASIMVAL ="+cp_NullLink(cp_ToStrODBC(w_VASIMVAL),'VALUTE','VASIMVAL');
          +",VACAOVAL ="+cp_NullLink(cp_ToStrODBC(w_VACAOVAL),'VALUTE','VACAOVAL');
          +",VALINVAL ="+cp_NullLink(cp_ToStrODBC(w_VALINVAL),'VALUTE','VALINVAL');
          +",VADECUNI ="+cp_NullLink(cp_ToStrODBC(w_VADECUNI),'VALUTE','VADECUNI');
          +",VADECTOT ="+cp_NullLink(cp_ToStrODBC(w_VADECTOT),'VALUTE','VADECTOT');
          +",VABOLESE ="+cp_NullLink(cp_ToStrODBC(w_VABOLESE),'VALUTE','VABOLESE');
          +",VABOLSUP ="+cp_NullLink(cp_ToStrODBC(w_VABOLSUP),'VALUTE','VABOLSUP');
          +",VABOLCAM ="+cp_NullLink(cp_ToStrODBC(w_VABOLCAM),'VALUTE','VABOLCAM');
          +",VABOLARR ="+cp_NullLink(cp_ToStrODBC(w_VABOLARR),'VALUTE','VABOLARR');
          +",VABOLMIM ="+cp_NullLink(cp_ToStrODBC(w_VABOLMIM),'VALUTE','VABOLMIM');
          +",VADTOBSO ="+cp_NullLink(cp_ToStrODBC(w_VADTOBSO),'VALUTE','VADTOBSO');
          +",VADATEUR ="+cp_NullLink(cp_ToStrODBC(w_VADATEUR),'VALUTE','VADATEUR');
          +",VAOPECAM ="+cp_NullLink(cp_ToStrODBC(w_VAOPECAM),'VALUTE','VAOPECAM');
          +",VAUNIVAL ="+cp_NullLink(cp_ToStrODBC(w_VAUNIVAL),'VALUTE','VAUNIVAL');
          +",VACODISO ="+cp_NullLink(cp_ToStrODBC(w_VACODISO),'VALUTE','VACODISO');
              +i_ccchkf ;
          +" where ";
              +"VACODVAL = "+cp_ToStrODBC(w_VACODVAL);
                 )
        else
          update (i_cTable) set;
              VADESVAL = w_VADESVAL;
              ,VASIMVAL = w_VASIMVAL;
              ,VACAOVAL = w_VACAOVAL;
              ,VALINVAL = w_VALINVAL;
              ,VADECUNI = w_VADECUNI;
              ,VADECTOT = w_VADECTOT;
              ,VABOLESE = w_VABOLESE;
              ,VABOLSUP = w_VABOLSUP;
              ,VABOLCAM = w_VABOLCAM;
              ,VABOLARR = w_VABOLARR;
              ,VABOLMIM = w_VABOLMIM;
              ,VADTOBSO = w_VADTOBSO;
              ,VADATEUR = w_VADATEUR;
              ,VAOPECAM = w_VAOPECAM;
              ,VAUNIVAL = w_VAUNIVAL;
              ,VACODISO = w_VACODISO;
              &i_ccchkf. ;
           where;
              VACODVAL = w_VACODVAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_VALUT = .T.
      case this.w_Destinaz = "IV"
        * --- Codici Iva
        this.w_ResoDett = trim(w_IVCODIVA)
        w_IVFLGSER=iif(empty(nvl(w_IVFLGSER,"")),"E",w_IVFLGSER)
        if NOT IsAlt() OR NOT w_EXSOLNUO="N" OR NOT EMPTY(w_EXCODNUO)
          * --- Write into VOCIIVA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VOCIIVA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IVACQINT ="+cp_NullLink(cp_ToStrODBC(w_IVACQINT),'VOCIIVA','IVACQINT');
            +",IVBOLIVA ="+cp_NullLink(cp_ToStrODBC(w_IVBOLIVA),'VOCIIVA','IVBOLIVA');
            +",IVCODINT ="+cp_NullLink(cp_ToStrODBC(w_IVCODINT),'VOCIIVA','IVCODINT');
            +",IVDESIVA ="+cp_NullLink(cp_ToStrODBC(w_IVDESIVA),'VOCIIVA','IVDESIVA');
            +",IVDTINVA ="+cp_NullLink(cp_ToStrODBC(w_IVDTINVA),'VOCIIVA','IVDTINVA');
            +",IVDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_IVDTOBSO),'VOCIIVA','IVDTOBSO');
            +",IVFLGBSA ="+cp_NullLink(cp_ToStrODBC(w_IVFLGBSA),'VOCIIVA','IVFLGBSA');
            +",IVFLGBSV ="+cp_NullLink(cp_ToStrODBC(w_IVFLGBSV),'VOCIIVA','IVFLGBSV');
            +",IVFLGSER ="+cp_NullLink(cp_ToStrODBC(w_IVFLGSER),'VOCIIVA','IVFLGSER');
            +",IVFLVAFF ="+cp_NullLink(cp_ToStrODBC(w_IVFLVAFF),'VOCIIVA','IVFLVAFF');
            +",IVIMPEXP ="+cp_NullLink(cp_ToStrODBC(w_IVIMPEXP),'VOCIIVA','IVIMPEXP');
            +",IVMACIVA ="+cp_NullLink(cp_ToStrODBC(w_IVMACIVA),'VOCIIVA','IVMACIVA');
            +",IVPERCOM ="+cp_NullLink(cp_ToStrODBC(w_IVPERCOM),'VOCIIVA','IVPERCOM');
            +",IVPERIND ="+cp_NullLink(cp_ToStrODBC(w_IVPERIND),'VOCIIVA','IVPERIND');
            +",IVPERIVA ="+cp_NullLink(cp_ToStrODBC(w_IVPERIVA),'VOCIIVA','IVPERIVA');
            +",IVPLAIVA ="+cp_NullLink(cp_ToStrODBC(w_IVPLAIVA),'VOCIIVA','IVPLAIVA');
            +",IVPROIVA ="+cp_NullLink(cp_ToStrODBC(w_IVPROIVA),'VOCIIVA','IVPROIVA');
            +",IVRIGQUE ="+cp_NullLink(cp_ToStrODBC(w_IVRIGQUE),'VOCIIVA','IVRIGQUE');
            +",IVRIGQUF ="+cp_NullLink(cp_ToStrODBC(w_IVRIGQUF),'VOCIIVA','IVRIGQUF');
            +",IVTIPAGR ="+cp_NullLink(cp_ToStrODBC(w_IVTIPAGR),'VOCIIVA','IVTIPAGR');
            +",IVTIPBEN ="+cp_NullLink(cp_ToStrODBC(w_IVTIPBEN),'VOCIIVA','IVTIPBEN');
                +i_ccchkf ;
            +" where ";
                +"IVCODIVA = "+cp_ToStrODBC(w_IVCODIVA);
                   )
          else
            update (i_cTable) set;
                IVACQINT = w_IVACQINT;
                ,IVBOLIVA = w_IVBOLIVA;
                ,IVCODINT = w_IVCODINT;
                ,IVDESIVA = w_IVDESIVA;
                ,IVDTINVA = w_IVDTINVA;
                ,IVDTOBSO = w_IVDTOBSO;
                ,IVFLGBSA = w_IVFLGBSA;
                ,IVFLGBSV = w_IVFLGBSV;
                ,IVFLGSER = w_IVFLGSER;
                ,IVFLVAFF = w_IVFLVAFF;
                ,IVIMPEXP = w_IVIMPEXP;
                ,IVMACIVA = w_IVMACIVA;
                ,IVPERCOM = w_IVPERCOM;
                ,IVPERIND = w_IVPERIND;
                ,IVPERIVA = w_IVPERIVA;
                ,IVPLAIVA = w_IVPLAIVA;
                ,IVPROIVA = w_IVPROIVA;
                ,IVRIGQUE = w_IVRIGQUE;
                ,IVRIGQUF = w_IVRIGQUF;
                ,IVTIPAGR = w_IVTIPAGR;
                ,IVTIPBEN = w_IVTIPBEN;
                &i_ccchkf. ;
             where;
                IVCODIVA = w_IVCODIVA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.w_Destinaz = "AG"
        * --- Agenti
        w_AGTIPAGE = iif( empty(w_AGTIPAGE), iif(empty(w_AGCZOAGE), "C", "A"), w_AGTIPAGE)
        w_AGFLAZIE = iif( empty(w_AGFLAZIE), "I", w_AGFLAZIE)
        w_AGFLESCL = iif( empty(w_AGFLESCL), "M", w_AGFLESCL)
        w_AGFLFARI = iif( empty(w_AGFLFARI), "F", w_AGFLFARI)
        this.w_ResoDett = trim(w_AGCODAGE)
        * --- Write into AGENTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AGAGECAP ="+cp_NullLink(cp_ToStrODBC(w_AGCAPAGE),'AGENTI','AGAGECAP');
          +",AGCATPRO ="+cp_NullLink(cp_ToStrODBC(w_AGCATPRO),'AGENTI','AGCATPRO');
          +",AGCITAGE ="+cp_NullLink(cp_ToStrODBC(w_AGCITAGE),'AGENTI','AGCITAGE');
          +",AGCODENA ="+cp_NullLink(cp_ToStrODBC(w_AGCODENA),'AGENTI','AGCODENA');
          +",AGCODFOR ="+cp_NullLink(cp_ToStrODBC(w_AGCODFOR),'AGENTI','AGCODFOR');
          +",AGCZOAGE ="+cp_NullLink(cp_ToStrODBC(w_AGCZOAGE),'AGENTI','AGCZOAGE');
          +",AGDESAGE ="+cp_NullLink(cp_ToStrODBC(w_AGDESAGE),'AGENTI','AGDESAGE');
          +",AGDTINIT ="+cp_NullLink(cp_ToStrODBC(w_AGDTINIT),'AGENTI','AGDTINIT');
          +",AGDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_AGDTOBSO),'AGENTI','AGDTOBSO');
          +",AGFISAGE ="+cp_NullLink(cp_ToStrODBC(w_AGFISAGE),'AGENTI','AGFISAGE');
          +",AGFLAZIE ="+cp_NullLink(cp_ToStrODBC(w_AGFLAZIE),'AGENTI','AGFLAZIE');
          +",AGFLESCL ="+cp_NullLink(cp_ToStrODBC(w_AGFLESCL),'AGENTI','AGFLESCL');
          +",AGFLFARI ="+cp_NullLink(cp_ToStrODBC(w_AGFLFARI),'AGENTI','AGFLFARI');
          +",AGFLFIRR ="+cp_NullLink(cp_ToStrODBC(w_AGFLFIRR),'AGENTI','AGFLFIRR');
          +",AGINDAGE ="+cp_NullLink(cp_ToStrODBC(w_AGINDAGE),'AGENTI','AGINDAGE');
          +",AGPERCRE ="+cp_NullLink(cp_ToStrODBC(w_AGPERCRE),'AGENTI','AGPERCRE');
          +",AGPERIMP ="+cp_NullLink(cp_ToStrODBC(w_AGPERIMP),'AGENTI','AGPERIMP');
          +",AGPROAGE ="+cp_NullLink(cp_ToStrODBC(w_AGPROAGE),'AGENTI','AGPROAGE');
          +",AGRITIRP ="+cp_NullLink(cp_ToStrODBC(w_AGRITIRP),'AGENTI','AGRITIRP');
          +",AGTELEFO ="+cp_NullLink(cp_ToStrODBC(w_AGTELEFO),'AGENTI','AGTELEFO');
          +",AGTIPAGE ="+cp_NullLink(cp_ToStrODBC(w_AGTIPAGE),'AGENTI','AGTIPAGE');
          +",AGFLGCPZ ="+cp_NullLink(cp_ToStrODBC(w_AGFLGCPZ),'AGENTI','AGFLGCPZ');
              +i_ccchkf ;
          +" where ";
              +"AGCODAGE = "+cp_ToStrODBC(w_AGCODAGE);
                 )
        else
          update (i_cTable) set;
              AGAGECAP = w_AGCAPAGE;
              ,AGCATPRO = w_AGCATPRO;
              ,AGCITAGE = w_AGCITAGE;
              ,AGCODENA = w_AGCODENA;
              ,AGCODFOR = w_AGCODFOR;
              ,AGCZOAGE = w_AGCZOAGE;
              ,AGDESAGE = w_AGDESAGE;
              ,AGDTINIT = w_AGDTINIT;
              ,AGDTOBSO = w_AGDTOBSO;
              ,AGFISAGE = w_AGFISAGE;
              ,AGFLAZIE = w_AGFLAZIE;
              ,AGFLESCL = w_AGFLESCL;
              ,AGFLFARI = w_AGFLFARI;
              ,AGFLFIRR = w_AGFLFIRR;
              ,AGINDAGE = w_AGINDAGE;
              ,AGPERCRE = w_AGPERCRE;
              ,AGPERIMP = w_AGPERIMP;
              ,AGPROAGE = w_AGPROAGE;
              ,AGRITIRP = w_AGRITIRP;
              ,AGTELEFO = w_AGTELEFO;
              ,AGTIPAGE = w_AGTIPAGE;
              ,AGFLGCPZ = w_AGFLGCPZ;
              &i_ccchkf. ;
           where;
              AGCODAGE = w_AGCODAGE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into AGENTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AGENTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AG_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_AG_EMAIL),'AGENTI','AG_EMAIL');
          +",AG_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_AG_EMPEC),'AGENTI','AG_EMPEC');
          +",AGCHKMAI ="+cp_NullLink(cp_ToStrODBC(w_AGCHKMAI),'AGENTI','AGCHKMAI');
          +",AGCHKPEC ="+cp_NullLink(cp_ToStrODBC(w_AGCHKPEC),'AGENTI','AGCHKPEC');
          +",AGCHKSTA ="+cp_NullLink(cp_ToStrODBC(w_AGCHKSTA),'AGENTI','AGCHKSTA');
          +",AGCHKFAX ="+cp_NullLink(cp_ToStrODBC(w_AGCHKFAX),'AGENTI','AGCHKFAX');
          +",AGCHKCPZ ="+cp_NullLink(cp_ToStrODBC(w_AGCHKCPZ),'AGENTI','AGCHKCPZ');
          +",AGCHKPTL ="+cp_NullLink(cp_ToStrODBC(w_AGCHKPTL),'AGENTI','AGCHKPTL');
              +i_ccchkf ;
          +" where ";
              +"AGCODAGE = "+cp_ToStrODBC(w_AGCODAGE);
                 )
        else
          update (i_cTable) set;
              AG_EMAIL = w_AG_EMAIL;
              ,AG_EMPEC = w_AG_EMPEC;
              ,AGCHKMAI = w_AGCHKMAI;
              ,AGCHKPEC = w_AGCHKPEC;
              ,AGCHKSTA = w_AGCHKSTA;
              ,AGCHKFAX = w_AGCHKFAX;
              ,AGCHKCPZ = w_AGCHKCPZ;
              ,AGCHKPTL = w_AGCHKPTL;
              &i_ccchkf. ;
           where;
              AGCODAGE = w_AGCODAGE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "DE"
        * --- Destinazioni diverse
        if NVL(w_DDTIPCON, " ")=" " AND NVL(w_DDCODICE, " ")=" " AND NVL(w_DDCODDES, " ")=" " AND (NOT EMPTY(w_DDCODVET) OR NOT EMPTY(w_DDCODPOR) OR (NOT EMPTY(w_DDCODSPE) AND w_EXTIPCON="C"))
          * --- Caso in cui la destinazione diversa non esiste, viene valorizzata con i dati del cliente/fornitore
          this.w_ResoDett = trim(w_EXTIPCON+w_EXCODICE+"1")
          * --- Write into DES_DIVE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DES_DIVE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DDNOMDES ="+cp_NullLink(cp_ToStrODBC(w_EXRAGSOC),'DES_DIVE','DDNOMDES');
            +",DDINDIRI ="+cp_NullLink(cp_ToStrODBC(w_EXINDIRI),'DES_DIVE','DDINDIRI');
            +",DD___CAP ="+cp_NullLink(cp_ToStrODBC(w_EX___CAP),'DES_DIVE','DD___CAP');
            +",DDLOCALI ="+cp_NullLink(cp_ToStrODBC(w_EXLOCALI),'DES_DIVE','DDLOCALI');
            +",DDPROVIN ="+cp_NullLink(cp_ToStrODBC(w_EXPROVIN),'DES_DIVE','DDPROVIN');
            +",DDCODVET ="+cp_NullLink(cp_ToStrODBC(w_DDCODVET),'DES_DIVE','DDCODVET');
            +",DDCODPOR ="+cp_NullLink(cp_ToStrODBC(w_DDCODPOR),'DES_DIVE','DDCODPOR');
            +",DDCODSPE ="+cp_NullLink(cp_ToStrODBC(w_DDCODSPE),'DES_DIVE','DDCODSPE');
            +",DDTIPRIF ="+cp_NullLink(cp_ToStrODBC(w_DDTIPRIF),'DES_DIVE','DDTIPRIF');
            +",DDCATOPE ="+cp_NullLink(cp_ToStrODBC("OP"),'DES_DIVE','DDCATOPE');
            +",DDTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_DDTIPOPE),'DES_DIVE','DDTIPOPE');
                +i_ccchkf ;
            +" where ";
                +"DDTIPCON = "+cp_ToStrODBC(w_EXTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(w_EXCODICE);
                +" and DDCODDES = "+cp_ToStrODBC("99999");
                   )
          else
            update (i_cTable) set;
                DDNOMDES = w_EXRAGSOC;
                ,DDINDIRI = w_EXINDIRI;
                ,DD___CAP = w_EX___CAP;
                ,DDLOCALI = w_EXLOCALI;
                ,DDPROVIN = w_EXPROVIN;
                ,DDCODVET = w_DDCODVET;
                ,DDCODPOR = w_DDCODPOR;
                ,DDCODSPE = w_DDCODSPE;
                ,DDTIPRIF = w_DDTIPRIF;
                ,DDCATOPE = "OP";
                ,DDTIPOPE = w_DDTIPOPE;
                &i_ccchkf. ;
             where;
                DDTIPCON = w_EXTIPCON;
                and DDCODICE = w_EXCODICE;
                and DDCODDES = "99999";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_ResoDett = trim(w_DDTIPCON+w_DDCODICE+w_DDCODDES)
          if NVL(w_DDTIPCON, " ")<>" " AND NVL(w_DDCODICE, " ")<>" " AND NVL(w_DDCODDES, " ")<>" "
            * --- Write into DES_DIVE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DES_DIVE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DDNOMDES ="+cp_NullLink(cp_ToStrODBC(w_DDNOMDES),'DES_DIVE','DDNOMDES');
              +",DDINDIRI ="+cp_NullLink(cp_ToStrODBC(w_DDINDIRI),'DES_DIVE','DDINDIRI');
              +",DD___CAP ="+cp_NullLink(cp_ToStrODBC(w_DD___CAP),'DES_DIVE','DD___CAP');
              +",DDLOCALI ="+cp_NullLink(cp_ToStrODBC(w_DDLOCALI),'DES_DIVE','DDLOCALI');
              +",DDPROVIN ="+cp_NullLink(cp_ToStrODBC(w_DDPROVIN),'DES_DIVE','DDPROVIN');
              +",DDPERSON ="+cp_NullLink(cp_ToStrODBC(w_DDPERSON),'DES_DIVE','DDPERSON');
              +",DD__NOTE ="+cp_NullLink(cp_ToStrODBC(w_DD__NOTE),'DES_DIVE','DD__NOTE');
              +",DDTELEFO ="+cp_NullLink(cp_ToStrODBC(w_DDTELEFO),'DES_DIVE','DDTELEFO');
              +",DD_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_DD_EMAIL),'DES_DIVE','DD_EMAIL');
              +",DDCODVET ="+cp_NullLink(cp_ToStrODBC(w_DDCODVET),'DES_DIVE','DDCODVET');
              +",DDCODPOR ="+cp_NullLink(cp_ToStrODBC(w_DDCODPOR),'DES_DIVE','DDCODPOR');
              +",DDCODSPE ="+cp_NullLink(cp_ToStrODBC(w_DDCODSPE),'DES_DIVE','DDCODSPE');
              +",DDTIPRIF ="+cp_NullLink(cp_ToStrODBC(w_DDTIPRIF),'DES_DIVE','DDTIPRIF');
              +",DDCATOPE ="+cp_NullLink(cp_ToStrODBC("OP"),'DES_DIVE','DDCATOPE');
              +",DDTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_DDTIPOPE),'DES_DIVE','DDTIPOPE');
              +",DD_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_DD_EMPEC),'DES_DIVE','DD_EMPEC');
                  +i_ccchkf ;
              +" where ";
                  +"DDTIPCON = "+cp_ToStrODBC(w_DDTIPCON);
                  +" and DDCODICE = "+cp_ToStrODBC(w_DDCODICE);
                  +" and DDCODDES = "+cp_ToStrODBC(w_DDCODDES);
                     )
            else
              update (i_cTable) set;
                  DDNOMDES = w_DDNOMDES;
                  ,DDINDIRI = w_DDINDIRI;
                  ,DD___CAP = w_DD___CAP;
                  ,DDLOCALI = w_DDLOCALI;
                  ,DDPROVIN = w_DDPROVIN;
                  ,DDPERSON = w_DDPERSON;
                  ,DD__NOTE = w_DD__NOTE;
                  ,DDTELEFO = w_DDTELEFO;
                  ,DD_EMAIL = w_DD_EMAIL;
                  ,DDCODVET = w_DDCODVET;
                  ,DDCODPOR = w_DDCODPOR;
                  ,DDCODSPE = w_DDCODSPE;
                  ,DDTIPRIF = w_DDTIPRIF;
                  ,DDCATOPE = "OP";
                  ,DDTIPOPE = w_DDTIPOPE;
                  ,DD_EMPEC = w_DD_EMPEC;
                  &i_ccchkf. ;
               where;
                  DDTIPCON = w_DDTIPCON;
                  and DDCODICE = w_DDCODICE;
                  and DDCODDES = w_DDCODDES;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- In questo caso � giusto non scrivere niente (evita la segnalazione di errore)
            i_rows = 1
          endif
        endif
      case this.w_Destinaz = "BA"
        * --- Banche
        this.w_ResoDett = trim(w_BACODBAN)
        if EMPTY(w_BADESABI) AND NOT EMPTY(w_BACODABI)
          * --- Read from COD_ABI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_ABI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2],.t.,this.COD_ABI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ABDESABI"+;
              " from "+i_cTable+" COD_ABI where ";
                  +"ABCODABI = "+cp_ToStrODBC(w_BACODABI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ABDESABI;
              from (i_cTable) where;
                  ABCODABI = w_BACODABI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_BADESABI = NVL(cp_ToDate(_read_.ABDESABI),cp_NullValue(_read_.ABDESABI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if EMPTY(w_BADESFIL) AND NOT EMPTY(w_BACODABI) AND NOT EMPTY(w_BACODCAB)
          * --- Read from COD_CAB
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_CAB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2],.t.,this.COD_CAB_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FIDESFIL"+;
              " from "+i_cTable+" COD_CAB where ";
                  +"FICODABI = "+cp_ToStrODBC(w_BACODABI);
                  +" and FICODCAB = "+cp_ToStrODBC(w_BACODCAB);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FIDESFIL;
              from (i_cTable) where;
                  FICODABI = w_BACODABI;
                  and FICODCAB = w_BACODCAB;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_BADESFIL = NVL(cp_ToDate(_read_.FIDESFIL),cp_NullValue(_read_.FIDESFIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Write into BAN_CHE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.BAN_CHE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.BAN_CHE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"BACODABI ="+cp_NullLink(cp_ToStrODBC(w_BACODABI),'BAN_CHE','BACODABI');
          +",BACODCAB ="+cp_NullLink(cp_ToStrODBC(w_BACODCAB),'BAN_CHE','BACODCAB');
          +",BACINABI ="+cp_NullLink(cp_ToStrODBC(w_BACINABI),'BAN_CHE','BACINABI');
          +",BACINCAB ="+cp_NullLink(cp_ToStrODBC(w_BACINCAB),'BAN_CHE','BACINCAB');
          +",BADESBAN ="+cp_NullLink(cp_ToStrODBC(w_BADESBAN),'BAN_CHE','BADESBAN');
          +",BAPROVIN ="+cp_NullLink(cp_ToStrODBC(w_BAPROVIN),'BAN_CHE','BAPROVIN');
          +",BAINDIRI ="+cp_NullLink(cp_ToStrODBC(w_BAINDIRI),'BAN_CHE','BAINDIRI');
          +",BA___CAP ="+cp_NullLink(cp_ToStrODBC(w_BA___CAP),'BAN_CHE','BA___CAP');
          +",BALOCALI ="+cp_NullLink(cp_ToStrODBC(w_BALOCALI),'BAN_CHE','BALOCALI');
          +",BADESABI ="+cp_NullLink(cp_ToStrODBC(w_BADESABI),'BAN_CHE','BADESABI');
          +",BADESFIL ="+cp_NullLink(cp_ToStrODBC(w_BADESFIL),'BAN_CHE','BADESFIL');
              +i_ccchkf ;
          +" where ";
              +"BACODBAN = "+cp_ToStrODBC(w_BACODBAN);
                 )
        else
          update (i_cTable) set;
              BACODABI = w_BACODABI;
              ,BACODCAB = w_BACODCAB;
              ,BACINABI = w_BACINABI;
              ,BACINCAB = w_BACINCAB;
              ,BADESBAN = w_BADESBAN;
              ,BAPROVIN = w_BAPROVIN;
              ,BAINDIRI = w_BAINDIRI;
              ,BA___CAP = w_BA___CAP;
              ,BALOCALI = w_BALOCALI;
              ,BADESABI = w_BADESABI;
              ,BADESFIL = w_BADESFIL;
              &i_ccchkf. ;
           where;
              BACODBAN = w_BACODBAN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz="CB"
        * --- Conti Banche
        this.w_ResoDett = trim(w_BACODBAN)
        * --- Write into COC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"BACODABI ="+cp_NullLink(cp_ToStrODBC(w_BACODABI),'COC_MAST','BACODABI');
          +",BACODCAB ="+cp_NullLink(cp_ToStrODBC(w_BACODCAB),'COC_MAST','BACODCAB');
          +",BACINABI ="+cp_NullLink(cp_ToStrODBC(w_BACINABI),'COC_MAST','BACINABI');
          +",BACINEUR ="+cp_NullLink(cp_ToStrODBC(w_BACINCAB),'COC_MAST','BACINEUR');
          +",BADESCRI ="+cp_NullLink(cp_ToStrODBC(w_BADESCRI),'COC_MAST','BADESCRI');
          +",BATIPCOL ="+cp_NullLink(cp_ToStrODBC(w_BATIPCOL),'COC_MAST','BATIPCOL');
          +",BACONCOL ="+cp_NullLink(cp_ToStrODBC(w_BACONCOL),'COC_MAST','BACONCOL');
          +",BACONCOR ="+cp_NullLink(cp_ToStrODBC(w_BACONCOR),'COC_MAST','BACONCOR');
          +",BACODSIA ="+cp_NullLink(cp_ToStrODBC(w_BACODSIA),'COC_MAST','BACODSIA');
          +",BAPAESEU ="+cp_NullLink(cp_ToStrODBC(w_BAPAESEU),'COC_MAST','BAPAESEU');
          +",BA__BBAN ="+cp_NullLink(cp_ToStrODBC(w_BA__BBAN),'COC_MAST','BA__BBAN');
          +",BA__IBAN ="+cp_NullLink(cp_ToStrODBC(w_BA__IBAN),'COC_MAST','BA__IBAN');
              +i_ccchkf ;
          +" where ";
              +"BACODBAN = "+cp_ToStrODBC(w_BACODBAN);
                 )
        else
          update (i_cTable) set;
              BACODABI = w_BACODABI;
              ,BACODCAB = w_BACODCAB;
              ,BACINABI = w_BACINABI;
              ,BACINEUR = w_BACINCAB;
              ,BADESCRI = w_BADESCRI;
              ,BATIPCOL = w_BATIPCOL;
              ,BACONCOL = w_BACONCOL;
              ,BACONCOR = w_BACONCOR;
              ,BACODSIA = w_BACODSIA;
              ,BAPAESEU = w_BAPAESEU;
              ,BA__BBAN = w_BA__BBAN;
              ,BA__IBAN = w_BA__IBAN;
              &i_ccchkf. ;
           where;
              BACODBAN = w_BACODBAN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "CC"
        * --- Causali contabili
        w_CCFLRIFE = iif( empty(w_CCFLRIFE), "N", w_CCFLRIFE)
        w_CCTIPDOC = iif( empty(w_CCTIPDOC), "NO", w_CCTIPDOC)
        w_CCTIPREG = iif( empty(w_CCTIPREG), "N", w_CCTIPREG)
        w_CCCALDOC = iif( empty(w_CCCALDOC), "N", w_CCCALDOC)
        w_CCCFDAVE= iif( empty(w_CCCFDAVE), "D", w_CCCFDAVE)
        w_CCFLPDOC = iif( empty(w_CCFLPDOC), "N", w_CCFLPDOC)
        w_CCFLPPRO = iif( empty(w_CCFLPPRO), "N", w_CCFLPPRO)
        this.w_ResoDett = trim(w_CCCODICE)
        if NOT IsAlt() OR NOT w_EXSOLNUO="N" OR NOT EMPTY(w_EXCODNUO)
          * --- Write into CAU_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CAU_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CCDESCRI),'CAU_CONT','CCDESCRI');
            +",CCFLPART ="+cp_NullLink(cp_ToStrODBC(w_CCFLPART),'CAU_CONT','CCFLPART');
            +",CCFLANAL ="+cp_NullLink(cp_ToStrODBC(w_CCFLANAL),'CAU_CONT','CCFLANAL');
            +",CCFLSALI ="+cp_NullLink(cp_ToStrODBC(w_CCFLSALI),'CAU_CONT','CCFLSALI');
            +",CCFLSALF ="+cp_NullLink(cp_ToStrODBC(w_CCFLSALF),'CAU_CONT','CCFLSALF');
            +",CCFLCOMP ="+cp_NullLink(cp_ToStrODBC(w_CCFLCOMP),'CAU_CONT','CCFLCOMP');
            +",CCFLRIFE ="+cp_NullLink(cp_ToStrODBC(w_CCFLRIFE),'CAU_CONT','CCFLRIFE');
            +",CCTIPDOC ="+cp_NullLink(cp_ToStrODBC(w_CCTIPDOC),'CAU_CONT','CCTIPDOC');
            +",CCTIPREG ="+cp_NullLink(cp_ToStrODBC(w_CCTIPREG),'CAU_CONT','CCTIPREG');
            +",CCNUMREG ="+cp_NullLink(cp_ToStrODBC(w_CCNUMREG),'CAU_CONT','CCNUMREG');
            +",CCNUMDOC ="+cp_NullLink(cp_ToStrODBC(w_CCNUMDOC),'CAU_CONT','CCNUMDOC');
            +",CCFLIVDF ="+cp_NullLink(cp_ToStrODBC(w_CCFLIVDF),'CAU_CONT','CCFLIVDF');
            +",CCFLSTDA ="+cp_NullLink(cp_ToStrODBC(w_CCFLSTDA),'CAU_CONT','CCFLSTDA');
            +",CCCALDOC ="+cp_NullLink(cp_ToStrODBC(w_CCCALDOC),'CAU_CONT','CCCALDOC');
            +",CCTESDOC ="+cp_NullLink(cp_ToStrODBC(w_CCTESDOC),'CAU_CONT','CCTESDOC');
            +",CCDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CCDTINVA),'CAU_CONT','CCDTINVA');
            +",CCDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CCDTOBSO),'CAU_CONT','CCDTOBSO');
            +",CCSERDOC ="+cp_NullLink(cp_ToStrODBC(w_CCSERDOC),'CAU_CONT','CCSERDOC');
            +",CCSERPRO ="+cp_NullLink(cp_ToStrODBC(w_CCSERPRO),'CAU_CONT','CCSERPRO');
            +",CCCFDAVE ="+cp_NullLink(cp_ToStrODBC(w_CCCFDAVE),'CAU_CONT','CCCFDAVE');
            +",CCCONIVA ="+cp_NullLink(cp_ToStrODBC(w_CCCONIVA),'CAU_CONT','CCCONIVA');
            +",CCFLPDOC ="+cp_NullLink(cp_ToStrODBC(w_CCFLPDOC),'CAU_CONT','CCFLPDOC');
            +",CCFLPPRO ="+cp_NullLink(cp_ToStrODBC(w_CCFLPPRO),'CAU_CONT','CCFLPPRO');
            +",CCFLPDIF ="+cp_NullLink(cp_ToStrODBC(w_CCFLPDIF),'CAU_CONT','CCFLPDIF');
            +",CCSCIPAG ="+cp_NullLink(cp_ToStrODBC(w_CCSCIPAG),'CAU_CONT','CCSCIPAG');
            +",CCFLCOSE ="+cp_NullLink(cp_ToStrODBC(w_CCFLCOSE),'CAU_CONT','CCFLCOSE');
            +",CCFLINSO ="+cp_NullLink(cp_ToStrODBC(w_CCFLINSO),'CAU_CONT','CCFLINSO');
                +i_ccchkf ;
            +" where ";
                +"CCCODICE = "+cp_ToStrODBC(w_CCCODICE);
                   )
          else
            update (i_cTable) set;
                CCDESCRI = w_CCDESCRI;
                ,CCFLPART = w_CCFLPART;
                ,CCFLANAL = w_CCFLANAL;
                ,CCFLSALI = w_CCFLSALI;
                ,CCFLSALF = w_CCFLSALF;
                ,CCFLCOMP = w_CCFLCOMP;
                ,CCFLRIFE = w_CCFLRIFE;
                ,CCTIPDOC = w_CCTIPDOC;
                ,CCTIPREG = w_CCTIPREG;
                ,CCNUMREG = w_CCNUMREG;
                ,CCNUMDOC = w_CCNUMDOC;
                ,CCFLIVDF = w_CCFLIVDF;
                ,CCFLSTDA = w_CCFLSTDA;
                ,CCCALDOC = w_CCCALDOC;
                ,CCTESDOC = w_CCTESDOC;
                ,CCDTINVA = w_CCDTINVA;
                ,CCDTOBSO = w_CCDTOBSO;
                ,CCSERDOC = w_CCSERDOC;
                ,CCSERPRO = w_CCSERPRO;
                ,CCCFDAVE = w_CCCFDAVE;
                ,CCCONIVA = w_CCCONIVA;
                ,CCFLPDOC = w_CCFLPDOC;
                ,CCFLPPRO = w_CCFLPPRO;
                ,CCFLPDIF = w_CCFLPDIF;
                ,CCSCIPAG = w_CCSCIPAG;
                ,CCFLCOSE = w_CCFLCOSE;
                ,CCFLINSO = w_CCFLINSO;
                &i_ccchkf. ;
             where;
                CCCODICE = w_CCCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.w_Destinaz = "PA"
        * --- Pagamenti
        if w_EXTIPORI="W"
          w_PAVALINC = iif( empty(w_PAVALINC), g_PERVAL, w_PAVALINC)
          w_PASCONTO = w_PASCONTO * -1
        endif
        this.w_ResoDett = trim(w_PACODICE)
        * --- Write into PAG_AMEN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAG_AMEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PADESCRI ="+cp_NullLink(cp_ToStrODBC(w_PADESCRI),'PAG_AMEN','PADESCRI');
          +",PAVALINC ="+cp_NullLink(cp_ToStrODBC(w_PAVALINC),'PAG_AMEN','PAVALINC');
          +",PASPEINC ="+cp_NullLink(cp_ToStrODBC(w_PASPEINC),'PAG_AMEN','PASPEINC');
          +",PAVALIN2 ="+cp_NullLink(cp_ToStrODBC(w_PAVALIN2),'PAG_AMEN','PAVALIN2');
          +",PASPEIN2 ="+cp_NullLink(cp_ToStrODBC(w_PASPEIN2),'PAG_AMEN','PASPEIN2');
          +",PASCONTO ="+cp_NullLink(cp_ToStrODBC(w_PASCONTO),'PAG_AMEN','PASCONTO');
          +",PADTINVA ="+cp_NullLink(cp_ToStrODBC(w_PADTINVA),'PAG_AMEN','PADTINVA');
          +",PADTOBSO ="+cp_NullLink(cp_ToStrODBC(w_PADTOBSO),'PAG_AMEN','PADTOBSO');
              +i_ccchkf ;
          +" where ";
              +"PACODICE = "+cp_ToStrODBC(w_PACODICE);
                 )
        else
          update (i_cTable) set;
              PADESCRI = w_PADESCRI;
              ,PAVALINC = w_PAVALINC;
              ,PASPEINC = w_PASPEINC;
              ,PAVALIN2 = w_PAVALIN2;
              ,PASPEIN2 = w_PASPEIN2;
              ,PASCONTO = w_PASCONTO;
              ,PADTINVA = w_PADTINVA;
              ,PADTOBSO = w_PADTOBSO;
              &i_ccchkf. ;
           where;
              PACODICE = w_PACODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "PD"
        * --- Dettaglio pagamenti
        if empty(w_P2FLNETT) .and. empty(w_P2FL_IVA) .and. empty(w_P2FLSPES)
          store 1 to w_P2FLNETT , w_P2FL_IVA , w_P2FLSPES
        endif
        this.w_ResoDett = trim(w_P2CODICE)
        * --- Write into PAG_2AME
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAG_2AME_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"P2MODPAG ="+cp_NullLink(cp_ToStrODBC(w_P2MODPAG),'PAG_2AME','P2MODPAG');
          +",P2GIOSCA ="+cp_NullLink(cp_ToStrODBC(w_P2GIOSCA),'PAG_2AME','P2GIOSCA');
          +",P2SCADEN ="+cp_NullLink(cp_ToStrODBC(w_P2SCADEN),'PAG_2AME','P2SCADEN');
          +",P2GIOFIS ="+cp_NullLink(cp_ToStrODBC(w_P2GIOFIS),'PAG_2AME','P2GIOFIS');
          +",P2FLNETT ="+cp_NullLink(cp_ToStrODBC(w_P2FLNETT),'PAG_2AME','P2FLNETT');
          +",P2FL_IVA ="+cp_NullLink(cp_ToStrODBC(w_P2FL_IVA),'PAG_2AME','P2FL_IVA');
          +",P2FLSPES ="+cp_NullLink(cp_ToStrODBC(w_P2FLSPES),'PAG_2AME','P2FLSPES');
              +i_ccchkf ;
          +" where ";
              +"P2CODICE = "+cp_ToStrODBC(w_P2CODICE);
              +" and P2NUMRAT = "+cp_ToStrODBC(w_P2NUMRAT);
                 )
        else
          update (i_cTable) set;
              P2MODPAG = w_P2MODPAG;
              ,P2GIOSCA = w_P2GIOSCA;
              ,P2SCADEN = w_P2SCADEN;
              ,P2GIOFIS = w_P2GIOFIS;
              ,P2FLNETT = w_P2FLNETT;
              ,P2FL_IVA = w_P2FL_IVA;
              ,P2FLSPES = w_P2FLSPES;
              &i_ccchkf. ;
           where;
              P2CODICE = w_P2CODICE;
              and P2NUMRAT = w_P2NUMRAT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.ChiavePrec = this.w_Destinaz + w_P2CODICE
      case this.w_Destinaz = "PC"
        * --- Conti e mastri
        w_EXSEZBIL = iif( empty(w_EXSEZBIL), "A", w_EXSEZBIL)
        w_ANCCTAGG = iif( empty(w_ANCCTAGG), iif(w_EXSEZBIL$"CR".and.g_PERCCR="S","M","E"), w_ANCCTAGG)
        w_MCTIPMAS = iif( w_EXTIPSOT$"CF" , w_EXTIPSOT, "G")
        if empty(w_EXTIPSOT) .and. w_EXMASCON="S"
          this.w_ResoMode = "CONTITIPO"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        w_ANCONSUP = iif( w_MCNUMLIV=iif(g_MaxLiv=0,3,g_MaxLiv) and w_EXMASCON="M" , space(15), w_ANCONSUP)
        w_MCSEQSTA = 999
        if w_EXMASCON="M" .and. (.not. empty( w_EXSEZBIL))
          w_MCSEQSTA = iif(w_EXSEZBIL="A",999,iif(w_EXSEZBIL="P",888,iif(w_EXSEZBIL="C",777,666)))
          w_MCSEQSTA = iif(w_EXSEZBIL="T",555,iif(w_EXSEZBIL="O",444,w_MCSEQSTA))
        endif
        if trim(w_ANCODICE) == trim(w_ANCONSUP) .and. (.not. empty(w_ANCONSUP))
          w_ANCONSUP = iif( val(left(w_ANCODICE,2))#0, left(w_ANCODICE,2)+"00000","")
          this.w_ResoMode = "CONTIRAGG"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_ResoDett = trim(w_ANCODICE)+iif( w_EXMASCON="S" , "", " (Mastri)")
        if NOT IsAlt() OR NOT w_EXSOLNUO="N" OR NOT EMPTY(w_EXCODNUO)
          if w_EXMASCON="S"
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ANDESCRI ="+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'CONTI','ANDESCRI');
              +",ANNOTAIN ="+cp_NullLink(cp_ToStrODBC(w_ANNOTAIN),'CONTI','ANNOTAIN');
              +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
              +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPSOT),'CONTI','ANTIPSOT');
              +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(w_ANPARTSN),'CONTI','ANPARTSN');
              +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ANDTINVA),'CONTI','ANDTINVA');
              +",ANCCNOTE ="+cp_NullLink(cp_ToStrODBC(w_ANCCNOTE),'CONTI','ANCCNOTE');
              +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(w_ANCCTAGG),'CONTI','ANCCTAGG');
              +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(w_ANCODSTU),'CONTI','ANCODSTU');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                     )
            else
              update (i_cTable) set;
                  ANDESCRI = w_ANDESCRI;
                  ,ANNOTAIN = w_ANNOTAIN;
                  ,ANCONSUP = w_ANCONSUP;
                  ,ANTIPSOT = w_ANTIPSOT;
                  ,ANPARTSN = w_ANPARTSN;
                  ,ANDTINVA = w_ANDTINVA;
                  ,ANCCNOTE = w_ANCCNOTE;
                  ,ANCCTAGG = w_ANCCTAGG;
                  ,ANCODSTU = w_ANCODSTU;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = w_ANTIPCON;
                  and ANCODICE = w_ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into MASTRI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'MASTRI','MCDESCRI');
              +",MCCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'MASTRI','MCCONSUP');
              +",MCSEZBIL ="+cp_NullLink(cp_ToStrODBC(w_EXSEZBIL),'MASTRI','MCSEZBIL');
              +",MCTIPMAS ="+cp_NullLink(cp_ToStrODBC(w_MCTIPMAS),'MASTRI','MCTIPMAS');
              +",MCNUMLIV ="+cp_NullLink(cp_ToStrODBC(w_MCNUMLIV),'MASTRI','MCNUMLIV');
              +",MCSEQSTA ="+cp_NullLink(cp_ToStrODBC(w_MCSEQSTA),'MASTRI','MCSEQSTA');
              +",MCCODSTU ="+cp_NullLink(cp_ToStrODBC(w_MCCODSTU),'MASTRI','MCCODSTU');
              +",MCPROSTU ="+cp_NullLink(cp_ToStrODBC(w_MCPROSTU),'MASTRI','MCPROSTU');
                  +i_ccchkf ;
              +" where ";
                  +"MCCODICE = "+cp_ToStrODBC(w_ANCODICE);
                     )
            else
              update (i_cTable) set;
                  MCDESCRI = w_ANDESCRI;
                  ,MCCONSUP = w_ANCONSUP;
                  ,MCSEZBIL = w_EXSEZBIL;
                  ,MCTIPMAS = w_MCTIPMAS;
                  ,MCNUMLIV = w_MCNUMLIV;
                  ,MCSEQSTA = w_MCSEQSTA;
                  ,MCCODSTU = w_MCCODSTU;
                  ,MCPROSTU = w_MCPROSTU;
                  &i_ccchkf. ;
               where;
                  MCCODICE = w_ANCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      case this.w_Destinaz = "AC"
        * --- Modelli e automatismi contabili
        w_APFLAUTO = iif(empty(w_APFLAUTO),"X",w_APFLAUTO)
        w_EXCONDET = iif(empty( w_EXCONDET ) , left( w_APCODCON + space(15), 15) , w_EXCONDET )
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLPART,CCTIPREG,CCNUMREG"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(w_APCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLPART,CCTIPREG,CCNUMREG;
            from (i_cTable) where;
                CCCODICE = w_APCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_EXFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
          w_EXTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
          w_EXNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_rows = 0
        w_APFLPART = iif(empty(w_APFLPART), iif( (! empty(w_EXFLPART)).and.w_APTIPCON$"CF", w_EXFLPART, "N"), w_APFLPART)
        this.w_ResoDett = trim(w_APCODCAU+w_EXCLFTIP+w_EXCLFCON)
        if NOT IsAlt() OR NOT w_EXSOLNUO="N" OR NOT EMPTY(w_EXCODNUO)
          if w_EXCLFTIP $ "CF"
            * --- Write into CAUPRI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CAUPRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUPRI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"APTIPCON ="+cp_NullLink(cp_ToStrODBC(w_APTIPCON),'CAUPRI','APTIPCON');
              +",APCODCON ="+cp_NullLink(cp_ToStrODBC(w_APCODCON),'CAUPRI','APCODCON');
              +",APFLDAVE ="+cp_NullLink(cp_ToStrODBC(w_APFLDAVE),'CAUPRI','APFLDAVE');
              +",APFLAUTO ="+cp_NullLink(cp_ToStrODBC(w_APFLAUTO),'CAUPRI','APFLAUTO');
              +",APFLPART ="+cp_NullLink(cp_ToStrODBC(w_APFLPART),'CAUPRI','APFLPART');
              +",APCAURIG ="+cp_NullLink(cp_ToStrODBC(w_APCAURIG),'CAUPRI','APCAURIG');
                  +i_ccchkf ;
              +" where ";
                  +"APCODCAU = "+cp_ToStrODBC(w_APCODCAU);
                  +" and APTIPCLF = "+cp_ToStrODBC(w_EXCLFTIP);
                  +" and APCODCLF = "+cp_ToStrODBC(w_EXCLFCON);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_DettaRow);
                     )
            else
              update (i_cTable) set;
                  APTIPCON = w_APTIPCON;
                  ,APCODCON = w_APCODCON;
                  ,APFLDAVE = w_APFLDAVE;
                  ,APFLAUTO = w_APFLAUTO;
                  ,APFLPART = w_APFLPART;
                  ,APCAURIG = w_APCAURIG;
                  &i_ccchkf. ;
               where;
                  APCODCAU = w_APCODCAU;
                  and APTIPCLF = w_EXCLFTIP;
                  and APCODCLF = w_EXCLFCON;
                  and CPROWNUM = this.w_DettaRow;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_righe = i_rows
            if .not. empty( w_EXCODIVA )
              * --- Write into CAUIVA
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CAUIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUIVA_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"AICODIVA ="+cp_NullLink(cp_ToStrODBC(w_EXCODIVA),'CAUIVA','AICODIVA');
                +",AITIPCON ="+cp_NullLink(cp_ToStrODBC(w_APTIPCON),'CAUIVA','AITIPCON');
                +",AICONDET ="+cp_NullLink(cp_ToStrODBC(w_EXCONDET),'CAUIVA','AICONDET');
                +",AICONIND ="+cp_NullLink(cp_ToStrODBC(w_EXCONIND),'CAUIVA','AICONIND');
                +",AITIPREG ="+cp_NullLink(cp_ToStrODBC(w_EXTIPREG),'CAUIVA','AITIPREG');
                +",AINUMREG ="+cp_NullLink(cp_ToStrODBC(w_EXNUMREG),'CAUIVA','AINUMREG');
                    +i_ccchkf ;
                +" where ";
                    +"AICODCAU = "+cp_ToStrODBC(w_APCODCAU);
                    +" and AITIPCLF = "+cp_ToStrODBC(w_EXCLFTIP);
                    +" and AICODCLF = "+cp_ToStrODBC(w_EXCLFCON);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_DettaRow);
                       )
              else
                update (i_cTable) set;
                    AICODIVA = w_EXCODIVA;
                    ,AITIPCON = w_APTIPCON;
                    ,AICONDET = w_EXCONDET;
                    ,AICONIND = w_EXCONIND;
                    ,AITIPREG = w_EXTIPREG;
                    ,AINUMREG = w_EXNUMREG;
                    &i_ccchkf. ;
                 where;
                    AICODCAU = w_APCODCAU;
                    and AITIPCLF = w_EXCLFTIP;
                    and AICODCLF = w_EXCLFCON;
                    and CPROWNUM = this.w_DettaRow;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            * --- Write into CAUPRI1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CAUPRI1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUPRI1_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"APTIPCON ="+cp_NullLink(cp_ToStrODBC(w_APTIPCON),'CAUPRI1','APTIPCON');
              +",APCODCON ="+cp_NullLink(cp_ToStrODBC(w_APCODCON),'CAUPRI1','APCODCON');
              +",APFLDAVE ="+cp_NullLink(cp_ToStrODBC(w_APFLDAVE),'CAUPRI1','APFLDAVE');
              +",APFLAUTO ="+cp_NullLink(cp_ToStrODBC(w_APFLAUTO),'CAUPRI1','APFLAUTO');
              +",APFLPART ="+cp_NullLink(cp_ToStrODBC(w_APFLPART),'CAUPRI1','APFLPART');
              +",APCAURIG ="+cp_NullLink(cp_ToStrODBC(w_APCAURIG),'CAUPRI1','APCAURIG');
                  +i_ccchkf ;
              +" where ";
                  +"APCODCAU = "+cp_ToStrODBC(w_APCODCAU);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_DettaRow);
                     )
            else
              update (i_cTable) set;
                  APTIPCON = w_APTIPCON;
                  ,APCODCON = w_APCODCON;
                  ,APFLDAVE = w_APFLDAVE;
                  ,APFLAUTO = w_APFLAUTO;
                  ,APFLPART = w_APFLPART;
                  ,APCAURIG = w_APCAURIG;
                  &i_ccchkf. ;
               where;
                  APCODCAU = w_APCODCAU;
                  and CPROWNUM = this.w_DettaRow;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_righe = i_rows
            if .not. empty( w_EXCODIVA )
              * --- Write into CAUIVA1
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CAUIVA1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUIVA1_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"AICODIVA ="+cp_NullLink(cp_ToStrODBC(w_EXCODIVA),'CAUIVA1','AICODIVA');
                +",AITIPCON ="+cp_NullLink(cp_ToStrODBC(w_APTIPCON),'CAUIVA1','AITIPCON');
                +",AICONDET ="+cp_NullLink(cp_ToStrODBC(w_EXCONDET),'CAUIVA1','AICONDET');
                +",AICONIND ="+cp_NullLink(cp_ToStrODBC(w_EXCONIND),'CAUIVA1','AICONIND');
                +",AITIPREG ="+cp_NullLink(cp_ToStrODBC(w_EXTIPREG),'CAUIVA1','AITIPREG');
                +",AINUMREG ="+cp_NullLink(cp_ToStrODBC(w_EXNUMREG),'CAUIVA1','AINUMREG');
                    +i_ccchkf ;
                +" where ";
                    +"AICODCAU = "+cp_ToStrODBC(w_APCODCAU);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_DettaRow);
                       )
              else
                update (i_cTable) set;
                    AICODIVA = w_EXCODIVA;
                    ,AITIPCON = w_APTIPCON;
                    ,AICONDET = w_EXCONDET;
                    ,AICONIND = w_EXCONIND;
                    ,AITIPREG = w_EXTIPREG;
                    ,AINUMREG = w_EXNUMREG;
                    &i_ccchkf. ;
                 where;
                    AICODCAU = w_APCODCAU;
                    and CPROWNUM = this.w_DettaRow;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        if i_rows=0
          i_rows = this.w_righe
        endif
        this.ChiavePrec = this.w_Destinaz + w_APCODCAU + w_EXCLFTIP + w_EXCLFCON
      case this.w_Destinaz = "SC"
        * --- Saldi contabili
        if w_EXMASCON="S".or.w_SLTIPCON$"CF".or.(empty(w_EXMASCON) .and. imconmas(w_SLCODICE)="C" )
          this.w_ResoDett = trim(w_SLTIPCON+w_SLCODICE+w_SLCODESE)
          * --- Write into SALDICON
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARPRO ="+cp_NullLink(cp_ToStrODBC(w_SLDARPRO),'SALDICON','SLDARPRO');
            +",SLAVEPRO ="+cp_NullLink(cp_ToStrODBC(w_SLAVEPRO),'SALDICON','SLAVEPRO');
            +",SLDARPER ="+cp_NullLink(cp_ToStrODBC(w_SLDARPER),'SALDICON','SLDARPER');
            +",SLAVEPER ="+cp_NullLink(cp_ToStrODBC(w_SLAVEPER),'SALDICON','SLAVEPER');
            +",SLDARINI ="+cp_NullLink(cp_ToStrODBC(w_SLDARINI),'SALDICON','SLDARINI');
            +",SLAVEINI ="+cp_NullLink(cp_ToStrODBC(w_SLAVEINI),'SALDICON','SLAVEINI');
            +",SLDARFIN ="+cp_NullLink(cp_ToStrODBC(w_SLDARFIN),'SALDICON','SLDARFIN');
            +",SLAVEFIN ="+cp_NullLink(cp_ToStrODBC(w_SLAVEFIN),'SALDICON','SLAVEFIN');
                +i_ccchkf ;
            +" where ";
                +"SLTIPCON = "+cp_ToStrODBC(w_SLTIPCON);
                +" and SLCODICE = "+cp_ToStrODBC(w_SLCODICE);
                +" and SLCODESE = "+cp_ToStrODBC(w_SLCODESE);
                   )
          else
            update (i_cTable) set;
                SLDARPRO = w_SLDARPRO;
                ,SLAVEPRO = w_SLAVEPRO;
                ,SLDARPER = w_SLDARPER;
                ,SLAVEPER = w_SLAVEPER;
                ,SLDARINI = w_SLDARINI;
                ,SLAVEINI = w_SLAVEINI;
                ,SLDARFIN = w_SLDARFIN;
                ,SLAVEFIN = w_SLAVEFIN;
                &i_ccchkf. ;
             where;
                SLTIPCON = w_SLTIPCON;
                and SLCODICE = w_SLCODICE;
                and SLCODESE = w_SLCODESE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Evita la generazione di un errore
          i_rows = 1
          this.w_ResoDett = "<SENZA RESOCONTO>"
        endif
      case this.w_Destinaz="CA"
        * --- Categorie Commerciali
        this.w_ResoDett = trim(w_CTCODICE)
        * --- Write into CATECOMM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CATECOMM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CATECOMM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CATECOMM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CTDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CTDESCRI),'CATECOMM','CTDESCRI');
              +i_ccchkf ;
          +" where ";
              +"CTCODICE = "+cp_ToStrODBC(w_CTCODICE);
                 )
        else
          update (i_cTable) set;
              CTDESCRI = w_CTDESCRI;
              &i_ccchkf. ;
           where;
              CTCODICE = w_CTCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz="CL"
        * --- Categorie Contabili Cli/For
        this.w_ResoDett = trim(w_C2CODICE)
        * --- Write into CACOCLFO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CACOCLFO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOCLFO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CACOCLFO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"C2DESCRI ="+cp_NullLink(cp_ToStrODBC(w_C2DESCRI),'CACOCLFO','C2DESCRI');
              +i_ccchkf ;
          +" where ";
              +"C2CODICE = "+cp_ToStrODBC(w_C2CODICE);
                 )
        else
          update (i_cTable) set;
              C2DESCRI = w_C2DESCRI;
              &i_ccchkf. ;
           where;
              C2CODICE = w_C2CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "GI"
        * --- Cambi Giornalieri
        this.w_ResoDett = DTOC(w_CGDATCAM) + " " + trim(w_CGCODVAL)
        * --- Write into CAM_BI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAM_BI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAM_BI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CGCAMBIO ="+cp_NullLink(cp_ToStrODBC(w_CGCAMBIO),'CAM_BI','CGCAMBIO');
              +i_ccchkf ;
          +" where ";
              +"CGDATCAM = "+cp_ToStrODBC(w_CGDATCAM);
              +" and CGCODVAL = "+cp_ToStrODBC(w_CGCODVAL);
                 )
        else
          update (i_cTable) set;
              CGCAMBIO = w_CGCAMBIO;
              &i_ccchkf. ;
           where;
              CGDATCAM = w_CGDATCAM;
              and CGCODVAL = w_CGCODVAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "TR"
        * --- Trascodifiche
        * --- Write into TRASCODI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TRASCODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TRASCODI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRCODENT ="+cp_NullLink(cp_ToStrODBC(w_TRCODENT),'TRASCODI','TRCODENT');
              +i_ccchkf ;
          +" where ";
              +"TRTIPTRA = "+cp_ToStrODBC(w_TRTIPTRA);
              +" and TRCODIMP = "+cp_ToStrODBC(w_TRCODIMP);
              +" and TRCODFIL = "+cp_ToStrODBC(w_TRCODFIL);
              +" and TRNOMCAM = "+cp_ToStrODBC(w_TRNOMCAM);
              +" and TRCODEXT = "+cp_ToStrODBC(w_TRCODEXT);
                 )
        else
          update (i_cTable) set;
              TRCODENT = w_TRCODENT;
              &i_ccchkf. ;
           where;
              TRTIPTRA = w_TRTIPTRA;
              and TRCODIMP = w_TRCODIMP;
              and TRCODFIL = w_TRCODFIL;
              and TRNOMCAM = w_TRNOMCAM;
              and TRCODEXT = w_TRCODEXT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "MS"
        * --- Mastri
        * --- Write into MASTRI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_MCDESCRI),'MASTRI','MCDESCRI');
          +",MCCONSUP ="+cp_NullLink(cp_ToStrODBC(w_MCCONSUP),'MASTRI','MCCONSUP');
          +",MCSEZBIL ="+cp_NullLink(cp_ToStrODBC(w_MCSEZBIL),'MASTRI','MCSEZBIL');
          +",MCTIPMAS ="+cp_NullLink(cp_ToStrODBC(w_MCTIPMAS),'MASTRI','MCTIPMAS');
          +",MCNUMLIV ="+cp_NullLink(cp_ToStrODBC(w_MCNUMLIV),'MASTRI','MCNUMLIV');
          +",MCSEQSTA ="+cp_NullLink(cp_ToStrODBC(w_MCSEQSTA),'MASTRI','MCSEQSTA');
          +",MCCODSTU ="+cp_NullLink(cp_ToStrODBC(w_MCCODSTU),'MASTRI','MCCODSTU');
          +",MCPROSTU ="+cp_NullLink(cp_ToStrODBC(w_MCPROSTU),'MASTRI','MCPROSTU');
              +i_ccchkf ;
          +" where ";
              +"MCCODICE = "+cp_ToStrODBC(w_MCCODICE);
                 )
        else
          update (i_cTable) set;
              MCDESCRI = w_MCDESCRI;
              ,MCCONSUP = w_MCCONSUP;
              ,MCSEZBIL = w_MCSEZBIL;
              ,MCTIPMAS = w_MCTIPMAS;
              ,MCNUMLIV = w_MCNUMLIV;
              ,MCSEQSTA = w_MCSEQSTA;
              ,MCCODSTU = w_MCCODSTU;
              ,MCPROSTU = w_MCPROSTU;
              &i_ccchkf. ;
           where;
              MCCODICE = w_MCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "CH"
        * --- Conti
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANDESCRI ="+cp_NullLink(cp_ToStrODBC(w_ANDESCRI),'CONTI','ANDESCRI');
          +",ANNOTAIN ="+cp_NullLink(cp_ToStrODBC(w_ANNOTAIN),'CONTI','ANNOTAIN');
          +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(w_ANCONSUP),'CONTI','ANCONSUP');
          +",ANTIPSOT ="+cp_NullLink(cp_ToStrODBC(w_ANTIPSOT),'CONTI','ANTIPSOT');
          +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(w_ANPARTSN),'CONTI','ANPARTSN');
          +",ANDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ANDTINVA),'CONTI','ANDTINVA');
          +",ANCCNOTE ="+cp_NullLink(cp_ToStrODBC(w_ANCCNOTE),'CONTI','ANCCNOTE');
          +",ANCCTAGG ="+cp_NullLink(cp_ToStrODBC(w_ANCCTAGG),'CONTI','ANCCTAGG');
          +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(w_ANCODSTU),'CONTI','ANCODSTU');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANDESCRI = w_ANDESCRI;
              ,ANNOTAIN = w_ANNOTAIN;
              ,ANCONSUP = w_ANCONSUP;
              ,ANTIPSOT = w_ANTIPSOT;
              ,ANPARTSN = w_ANPARTSN;
              ,ANDTINVA = w_ANDTINVA;
              ,ANCCNOTE = w_ANCCNOTE;
              ,ANCCTAGG = w_ANCCTAGG;
              ,ANCODSTU = w_ANCODSTU;
              &i_ccchkf. ;
           where;
              ANTIPCON = w_ANTIPCON;
              and ANCODICE = w_ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "RF"
        if not empty(w_COTIPCON) and not empty(w_COCODCON) 
          * --- Write into CONTATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTATTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_DETTAROW*10),'CONTATTI','CPROWORD');
            +",COINDMAI ="+cp_NullLink(cp_ToStrODBC(w_COINDMAI),'CONTATTI','COINDMAI');
            +",CONUMTEL ="+cp_NullLink(cp_ToStrODBC(w_CONUMTEL),'CONTATTI','CONUMTEL');
            +",CODESDIV ="+cp_NullLink(cp_ToStrODBC(w_CODESDIV),'CONTATTI','CODESDIV');
            +",CORIFPER ="+cp_NullLink(cp_ToStrODBC(w_CORIFPER),'CONTATTI','CORIFPER');
            +",CONUMCEL ="+cp_NullLink(cp_ToStrODBC(w_CONUMCEL),'CONTATTI','CONUMCEL');
                +i_ccchkf ;
            +" where ";
                +"COTIPCON = "+cp_ToStrODBC(w_COTIPCON);
                +" and COCODCON = "+cp_ToStrODBC(w_COCODCON);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_DETTAROW);
                   )
          else
            update (i_cTable) set;
                CPROWORD = this.w_DETTAROW*10;
                ,COINDMAI = w_COINDMAI;
                ,CONUMTEL = w_CONUMTEL;
                ,CODESDIV = w_CODESDIV;
                ,CORIFPER = w_CORIFPER;
                ,CONUMCEL = w_CONUMCEL;
                &i_ccchkf. ;
             where;
                COTIPCON = w_COTIPCON;
                and COCODCON = w_COCODCON;
                and CPROWNUM = this.w_DETTAROW;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.w_Destinaz = "NM"
        * --- Nominativi
        this.w_ResoDett = trim(w_NOCODICE)
        * --- Write into OFF_NOMI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
          +",NOTIPPER ="+cp_NullLink(cp_ToStrODBC(w_NOTIPPER),'OFF_NOMI','NOTIPPER');
          +",NOCOD_RU ="+cp_NullLink(cp_ToStrODBC(w_NOCOD_RU),'OFF_NOMI','NOCOD_RU');
          +",NOBADGE ="+cp_NullLink(cp_ToStrODBC(w_NOBADGE),'OFF_NOMI','NOBADGE');
          +",NOCOGNOM ="+cp_NullLink(cp_ToStrODBC(w_NOCOGNOM),'OFF_NOMI','NOCOGNOM');
          +",NO__NOME ="+cp_NullLink(cp_ToStrODBC(w_NO__NOME),'OFF_NOMI','NO__NOME');
          +",NO_SESSO ="+cp_NullLink(cp_ToStrODBC(w_NO_SESSO),'OFF_NOMI','NO_SESSO');
          +",NODATNAS ="+cp_NullLink(cp_ToStrODBC(w_NODATNAS),'OFF_NOMI','NODATNAS');
          +",NOLOCNAS ="+cp_NullLink(cp_ToStrODBC(w_NOLOCNAS),'OFF_NOMI','NOLOCNAS');
          +",NOPRONAS ="+cp_NullLink(cp_ToStrODBC(w_NOPRONAS),'OFF_NOMI','NOPRONAS');
          +",NOTIPCLI ="+cp_NullLink(cp_ToStrODBC(w_NOTIPCLI),'OFF_NOMI','NOTIPCLI');
          +",NOCODCLI ="+cp_NullLink(cp_ToStrODBC(w_NOCODCLI),'OFF_NOMI','NOCODCLI');
          +",NODESCRI ="+cp_NullLink(cp_ToStrODBC(w_NODESCRI),'OFF_NOMI','NODESCRI');
          +",NODESCR2 ="+cp_NullLink(cp_ToStrODBC(w_NODESCR2),'OFF_NOMI','NODESCR2');
          +",NOINDIRI ="+cp_NullLink(cp_ToStrODBC(w_NOINDIRI),'OFF_NOMI','NOINDIRI');
          +",NOINDIR2 ="+cp_NullLink(cp_ToStrODBC(w_NOINDIR2),'OFF_NOMI','NOINDIR2');
          +",NO___CAP ="+cp_NullLink(cp_ToStrODBC(w_NO___CAP),'OFF_NOMI','NO___CAP');
          +",NOLOCALI ="+cp_NullLink(cp_ToStrODBC(w_NOLOCALI),'OFF_NOMI','NOLOCALI');
          +",NOPROVIN ="+cp_NullLink(cp_ToStrODBC(w_NOPROVIN),'OFF_NOMI','NOPROVIN');
          +",NONAZION ="+cp_NullLink(cp_ToStrODBC(w_NONAZION),'OFF_NOMI','NONAZION');
          +",NOPARIVA ="+cp_NullLink(cp_ToStrODBC(w_NOPARIVA),'OFF_NOMI','NOPARIVA');
          +",NOCODFIS ="+cp_NullLink(cp_ToStrODBC(w_NOCODFIS),'OFF_NOMI','NOCODFIS');
          +",NONAZNAS ="+cp_NullLink(cp_ToStrODBC(w_NONAZNAS),'OFF_NOMI','NONAZNAS');
          +",NONUMCAR ="+cp_NullLink(cp_ToStrODBC(w_NONUMCAR),'OFF_NOMI','NONUMCAR');
          +",NO___CAD ="+cp_NullLink(cp_ToStrODBC(w_NO___CAD),'OFF_NOMI','NO___CAD');
          +",NOLOCALD ="+cp_NullLink(cp_ToStrODBC(w_NOLOCALD),'OFF_NOMI','NOLOCALD');
          +",NOPROVID ="+cp_NullLink(cp_ToStrODBC(w_NOPROVID),'OFF_NOMI','NOPROVID');
          +",NONAZIOD ="+cp_NullLink(cp_ToStrODBC(w_NONAZIOD),'OFF_NOMI','NONAZIOD');
          +",NOTELEFO ="+cp_NullLink(cp_ToStrODBC(w_NOTELEFO),'OFF_NOMI','NOTELEFO');
          +",NOTELFAX ="+cp_NullLink(cp_ToStrODBC(w_NOTELFAX),'OFF_NOMI','NOTELFAX');
          +",NONUMCEL ="+cp_NullLink(cp_ToStrODBC(w_NONUMCEL),'OFF_NOMI','NONUMCEL');
          +",NOINDWEB ="+cp_NullLink(cp_ToStrODBC(w_NOINDWEB),'OFF_NOMI','NOINDWEB');
          +",NO_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_NO_EMAIL),'OFF_NOMI','NO_EMAIL');
          +",NOCHKMAI ="+cp_NullLink(cp_ToStrODBC(w_NOCHKSTA),'OFF_NOMI','NOCHKMAI');
          +",NOCHKFAX ="+cp_NullLink(cp_ToStrODBC(w_NOCHKFAX),'OFF_NOMI','NOCHKFAX');
          +",NOCHKWWP ="+cp_NullLink(cp_ToStrODBC(w_NOCHKWWP),'OFF_NOMI','NOCHKWWP');
          +",NOCHKPTL ="+cp_NullLink(cp_ToStrODBC(w_NOCHKPTL),'OFF_NOMI','NOCHKPTL');
          +",NOCHKCPZ ="+cp_NullLink(cp_ToStrODBC(w_NOCHKCPZ),'OFF_NOMI','NOCHKCPZ');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(w_UTCV),'OFF_NOMI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(w_UTDV),'OFF_NOMI','UTDV');
          +",NODTINVA ="+cp_NullLink(cp_ToStrODBC(w_NODTINVA),'OFF_NOMI','NODTINVA');
          +",NODTOBSO ="+cp_NullLink(cp_ToStrODBC(w_NODTOBSO),'OFF_NOMI','NODTOBSO');
          +",NOTIPCON ="+cp_NullLink(cp_ToStrODBC(w_NOTIPCON),'OFF_NOMI','NOTIPCON');
          +",NOCODCON ="+cp_NullLink(cp_ToStrODBC(w_NOCODCON),'OFF_NOMI','NOCODCON');
          +",NOCODPAG ="+cp_NullLink(cp_ToStrODBC(w_NOCODPAG),'OFF_NOMI','NOCODPAG');
          +",NOGIOFIS ="+cp_NullLink(cp_ToStrODBC(w_NOGIOFIS),'OFF_NOMI','NOGIOFIS');
          +",NO1MESCL ="+cp_NullLink(cp_ToStrODBC(w_NO1MESCL),'OFF_NOMI','NO1MESCL');
          +",NOGIOSC1 ="+cp_NullLink(cp_ToStrODBC(w_NOGIOSC1),'OFF_NOMI','NOGIOSC1');
          +",NO2MESCL ="+cp_NullLink(cp_ToStrODBC(w_NO2MESCL),'OFF_NOMI','NO2MESCL');
          +",NOGIOSC2 ="+cp_NullLink(cp_ToStrODBC(w_NOGIOSC2),'OFF_NOMI','NOGIOSC2');
          +",NOCODBAN ="+cp_NullLink(cp_ToStrODBC(w_NOCODBAN),'OFF_NOMI','NOCODBAN');
          +",NONUMCOR ="+cp_NullLink(cp_ToStrODBC(w_NONUMCOR),'OFF_NOMI','NONUMCOR');
          +",NOCODBA2 ="+cp_NullLink(cp_ToStrODBC(w_NOCODBA2),'OFF_NOMI','NOCODBA2');
          +",NOFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_NOFLRAGG),'OFF_NOMI','NOFLRAGG');
          +",NOFLGAVV ="+cp_NullLink(cp_ToStrODBC(w_NOFLGAVV),'OFF_NOMI','NOFLGAVV');
          +",NODATAVV ="+cp_NullLink(cp_ToStrODBC(w_NODATAVV),'OFF_NOMI','NODATAVV');
          +",NOINTMOR ="+cp_NullLink(cp_ToStrODBC(w_NOINTMOR),'OFF_NOMI','NOINTMOR');
          +",NOMORTAS ="+cp_NullLink(cp_ToStrODBC(w_NOMORTAS),'OFF_NOMI','NOMORTAS');
          +",NOCODPRI ="+cp_NullLink(cp_ToStrODBC(w_NOCODPRI),'OFF_NOMI','NOCODPRI');
          +",NOCODGRU ="+cp_NullLink(cp_ToStrODBC(w_NOCODGRU),'OFF_NOMI','NOCODGRU');
          +",NOCODORI ="+cp_NullLink(cp_ToStrODBC(w_NOCODORI),'OFF_NOMI','NOCODORI');
          +",NOCODOPE ="+cp_NullLink(cp_ToStrODBC(w_NOCODOPE),'OFF_NOMI','NOCODOPE');
          +",NOCODZON ="+cp_NullLink(cp_ToStrODBC(w_NOCODZON),'OFF_NOMI','NOCODZON');
          +",NOCODLIN ="+cp_NullLink(cp_ToStrODBC(w_NOCODLIN),'OFF_NOMI','NOCODLIN');
          +",NOCODVAL ="+cp_NullLink(cp_ToStrODBC(w_NOCODVAL),'OFF_NOMI','NOCODVAL');
          +",NOCODAGE ="+cp_NullLink(cp_ToStrODBC(w_NOCODAGE),'OFF_NOMI','NOCODAGE');
          +",NO__NOTE ="+cp_NullLink(cp_ToStrODBC(w_NO__NOTE),'OFF_NOMI','NO__NOTE');
          +",NOSOGGET ="+cp_NullLink(cp_ToStrODBC(w_NOSOGGET),'OFF_NOMI','NOSOGGET');
          +",NOASSOPE ="+cp_NullLink(cp_ToStrODBC(w_NOASSOPE),'OFF_NOMI','NOASSOPE');
          +",NOFLGBEN ="+cp_NullLink(cp_ToStrODBC(w_NOFLGBEN),'OFF_NOMI','NOFLGBEN');
          +",NOIDRIDY ="+cp_NullLink(cp_ToStrODBC(w_NOIDRIDY),'OFF_NOMI','NOIDRIDY');
          +",NOTIIDRI ="+cp_NullLink(cp_ToStrODBC(w_NOTIIDRI),'OFF_NOMI','NOTIIDRI');
          +",NOCOIDRI ="+cp_NullLink(cp_ToStrODBC(w_NOCOIDRI),'OFF_NOMI','NOCOIDRI');
          +",NOINDI_2 ="+cp_NullLink(cp_ToStrODBC(w_NOINDI_2),'OFF_NOMI','NOINDI_2');
          +",NOPRIVCY ="+cp_NullLink(cp_ToStrODBC(w_NOPRIVCY),'OFF_NOMI','NOPRIVCY');
          +",NOCODSAL ="+cp_NullLink(cp_ToStrODBC(w_NOCODSAL),'OFF_NOMI','NOCODSAL');
          +",NORIFINT ="+cp_NullLink(cp_ToStrODBC(w_NORIFINT),'OFF_NOMI','NORIFINT');
          +",NOFLANTI ="+cp_NullLink(cp_ToStrODBC(w_NOFLANTI),'OFF_NOMI','NOFLANTI');
          +",NO_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_NO_EMPEC),'OFF_NOMI','NO_EMPEC');
          +",NOCHKPEC ="+cp_NullLink(cp_ToStrODBC(w_NOCHKPEC),'OFF_NOMI','NOCHKPEC');
              +i_ccchkf ;
          +" where ";
              +"NOCODICE = "+cp_ToStrODBC(w_NOCODICE);
                 )
        else
          update (i_cTable) set;
              NOTIPNOM = w_NOTIPNOM;
              ,NOTIPPER = w_NOTIPPER;
              ,NOCOD_RU = w_NOCOD_RU;
              ,NOBADGE = w_NOBADGE;
              ,NOCOGNOM = w_NOCOGNOM;
              ,NO__NOME = w_NO__NOME;
              ,NO_SESSO = w_NO_SESSO;
              ,NODATNAS = w_NODATNAS;
              ,NOLOCNAS = w_NOLOCNAS;
              ,NOPRONAS = w_NOPRONAS;
              ,NOTIPCLI = w_NOTIPCLI;
              ,NOCODCLI = w_NOCODCLI;
              ,NODESCRI = w_NODESCRI;
              ,NODESCR2 = w_NODESCR2;
              ,NOINDIRI = w_NOINDIRI;
              ,NOINDIR2 = w_NOINDIR2;
              ,NO___CAP = w_NO___CAP;
              ,NOLOCALI = w_NOLOCALI;
              ,NOPROVIN = w_NOPROVIN;
              ,NONAZION = w_NONAZION;
              ,NOPARIVA = w_NOPARIVA;
              ,NOCODFIS = w_NOCODFIS;
              ,NONAZNAS = w_NONAZNAS;
              ,NONUMCAR = w_NONUMCAR;
              ,NO___CAD = w_NO___CAD;
              ,NOLOCALD = w_NOLOCALD;
              ,NOPROVID = w_NOPROVID;
              ,NONAZIOD = w_NONAZIOD;
              ,NOTELEFO = w_NOTELEFO;
              ,NOTELFAX = w_NOTELFAX;
              ,NONUMCEL = w_NONUMCEL;
              ,NOINDWEB = w_NOINDWEB;
              ,NO_EMAIL = w_NO_EMAIL;
              ,NOCHKMAI = w_NOCHKSTA;
              ,NOCHKFAX = w_NOCHKFAX;
              ,NOCHKWWP = w_NOCHKWWP;
              ,NOCHKPTL = w_NOCHKPTL;
              ,NOCHKCPZ = w_NOCHKCPZ;
              ,UTCV = w_UTCV;
              ,UTDV = w_UTDV;
              ,NODTINVA = w_NODTINVA;
              ,NODTOBSO = w_NODTOBSO;
              ,NOTIPCON = w_NOTIPCON;
              ,NOCODCON = w_NOCODCON;
              ,NOCODPAG = w_NOCODPAG;
              ,NOGIOFIS = w_NOGIOFIS;
              ,NO1MESCL = w_NO1MESCL;
              ,NOGIOSC1 = w_NOGIOSC1;
              ,NO2MESCL = w_NO2MESCL;
              ,NOGIOSC2 = w_NOGIOSC2;
              ,NOCODBAN = w_NOCODBAN;
              ,NONUMCOR = w_NONUMCOR;
              ,NOCODBA2 = w_NOCODBA2;
              ,NOFLRAGG = w_NOFLRAGG;
              ,NOFLGAVV = w_NOFLGAVV;
              ,NODATAVV = w_NODATAVV;
              ,NOINTMOR = w_NOINTMOR;
              ,NOMORTAS = w_NOMORTAS;
              ,NOCODPRI = w_NOCODPRI;
              ,NOCODGRU = w_NOCODGRU;
              ,NOCODORI = w_NOCODORI;
              ,NOCODOPE = w_NOCODOPE;
              ,NOCODZON = w_NOCODZON;
              ,NOCODLIN = w_NOCODLIN;
              ,NOCODVAL = w_NOCODVAL;
              ,NOCODAGE = w_NOCODAGE;
              ,NO__NOTE = w_NO__NOTE;
              ,NOSOGGET = w_NOSOGGET;
              ,NOASSOPE = w_NOASSOPE;
              ,NOFLGBEN = w_NOFLGBEN;
              ,NOIDRIDY = w_NOIDRIDY;
              ,NOTIIDRI = w_NOTIIDRI;
              ,NOCOIDRI = w_NOCOIDRI;
              ,NOINDI_2 = w_NOINDI_2;
              ,NOPRIVCY = w_NOPRIVCY;
              ,NOCODSAL = w_NOCODSAL;
              ,NORIFINT = w_NORIFINT;
              ,NOFLANTI = w_NOFLANTI;
              ,NO_EMPEC = w_NO_EMPEC;
              ,NOCHKPEC = w_NOCHKPEC;
              &i_ccchkf. ;
           where;
              NOCODICE = w_NOCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NOT EMPTY(w_ANCODCAT)
          * --- Read from CAT_ATTR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAT_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAT_ATTR_idx,2],.t.,this.CAT_ATTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CTDESCRI"+;
              " from "+i_cTable+" CAT_ATTR where ";
                  +"CTCODICE = "+cp_ToStrODBC(w_ANCODCAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CTDESCRI;
              from (i_cTable) where;
                  CTCODICE = w_ANCODCAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_ANDESCAT = NVL(cp_ToDate(_read_.CTDESCRI),cp_NullValue(_read_.CTDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Write into NOM_ATTR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.NOM_ATTR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANDESCAT ="+cp_NullLink(cp_ToStrODBC(w_ANDESCAT),'NOM_ATTR','ANDESCAT');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(w_NOCODICE);
                +" and ANCODCAT = "+cp_ToStrODBC(w_ANCODCAT);
                   )
          else
            update (i_cTable) set;
                ANDESCAT = w_ANDESCAT;
                &i_ccchkf. ;
             where;
                ANCODICE = w_NOCODICE;
                and ANCODCAT = w_ANCODCAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.w_Destinaz = "TU"
        * --- Codice tributo nei fornitori
        this.w_ResoDett = trim(w_ANTIPCON+w_ANCODICE)
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODIRP ="+cp_NullLink(cp_ToStrODBC(""),'CONTI','ANCODIRP');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(w_ANCODICE);
                 )
        else
          update (i_cTable) set;
              ANCODIRP = "";
              &i_ccchkf. ;
           where;
              ANTIPCON = w_ANTIPCON;
              and ANCODICE = w_ANCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "KR"
        * --- Cronologico
        this.w_ResoDett = trim(w_CCCODICE+w_CCTIPCON+w_CCCODCON)
        * --- Write into COLCRONO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COLCRONO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COLCRONO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COLCRONO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CCDESCRI),'COLCRONO','CCDESCRI');
              +i_ccchkf ;
          +" where ";
              +"CCCODICE = "+cp_ToStrODBC(w_CCCODICE);
              +" and CCTIPCON = "+cp_ToStrODBC(w_CCTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(w_CCCODCON);
                 )
        else
          update (i_cTable) set;
              CCDESCRI = w_CCDESCRI;
              &i_ccchkf. ;
           where;
              CCCODICE = w_CCCODICE;
              and CCTIPCON = w_CCTIPCON;
              and CCCODCON = w_CCCODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_Destinaz = "EC"
        * --- Causali escluse
        * --- Gestito nell'insert (sono solo campi chiave)
        * --- L'assegnamento i_rows = 1 serve per evitare che impostato w_Corretto = .F. => ERRORE SENZA RESOCONTO
        i_rows = 1
    endcase
    return
  proc Try_04DA3DF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into BAN_CONTI
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.BAN_CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCTIPCON"+",CCCODCON"+",CCCONCOR"+",CCCODBAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ANTIPCON),'BAN_CONTI','CCTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_ANCODICE),'BAN_CONTI','CCCODCON');
      +","+cp_NullLink(cp_ToStrODBC(w_CCCONCOR),'BAN_CONTI','CCCONCOR');
      +","+cp_NullLink(cp_ToStrODBC(w_CCCODBAN),'BAN_CONTI','CCCODBAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCTIPCON',w_ANTIPCON,'CCCODCON',w_ANCODICE,'CCCONCOR',w_CCCONCOR,'CCCODBAN',w_CCCODBAN)
      insert into (i_cTable) (CCTIPCON,CCCODCON,CCCONCOR,CCCODBAN &i_ccchkf. );
         values (;
           w_ANTIPCON;
           ,w_ANCODICE;
           ,w_CCCONCOR;
           ,w_CCCODBAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04DC1078()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_PAGA
    i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MPCODICE"+",MPDESCRI"+",MPTIPPAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_P2MODPAG),'MOD_PAGA','MPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_P2MODPAG),'MOD_PAGA','MPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(w_MPTIPPAG),'MOD_PAGA','MPTIPPAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MPCODICE',w_P2MODPAG,'MPDESCRI',w_P2MODPAG,'MPTIPPAG',w_MPTIPPAG)
      insert into (i_cTable) (MPCODICE,MPDESCRI,MPTIPPAG &i_ccchkf. );
         values (;
           w_P2MODPAG;
           ,w_P2MODPAG;
           ,w_MPTIPPAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04DD0C28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_CONT
    i_nConn=i_TableProp[this.MOD_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCTIPCON"+",CCCODCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_APCODCAU),'MOD_CONT','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCLFTIP),'MOD_CONT','CCTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCLFCON),'MOD_CONT','CCCODCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',w_APCODCAU,'CCTIPCON',w_EXCLFTIP,'CCCODCON',w_EXCLFCON)
      insert into (i_cTable) (CCCODICE,CCTIPCON,CCCODCON &i_ccchkf. );
         values (;
           w_APCODCAU;
           ,w_EXCLFTIP;
           ,w_EXCLFCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- DEFINIZIONE DELLE VARIABILI
    * --- Variabili locali
    * --- Nome del file ascii da leggere su disco
    * --- Nome del file ascii righe da leggere su disco
    * --- Nome del file ascii utilizzato nella query della lettura del tracciato
    * --- Codice file destinazione utilizzato nella query della lettura del tracciato
    * --- Nome archivio di destinazione
    * --- Archivio di destinazione per righe dettaglio
    * --- Utilizzo trascodifiche su righe dettaglio S/N
    * --- Record ascii
    * --- Utilizzo trascodifiche S/N
    * --- Trascodifica Parziale: un valore non presente nella trascodifica viene restituitito inalterato
    * --- Valore e descrizione di un campo
    * --- Posizione all'interno del record
    * --- Record corretto S/N
    * --- Nome archivio
    * --- Contatore campi obbligatori
    * --- Contatore campi obbligatori per righe dettaglio
    * --- Contatore per calcoli temporanei
    * --- Variabile per testo messaggi di elaborazione
    * --- Contatore delle righe scritte nell'archivio di dettaglio
    * --- Chiave di ricerca delle righe di dettaglio
    * --- Tipo record ascii ( 'T' = Normale o testata, 'R' = Riga dettaglio )
    * --- Variabile di comodo per usi temporanei
    * --- Variabile di comodo per usi temporanei
    * --- Flag aggiornamento saldi letto da causale magaz. (A, V, ecc.)
    * --- Controllo valori
    * --- Aliquota iva
    * --- Indicatore listino IVA Lordo/Netto
    * --- Utente che ha effettuato l'import
    * --- Data nella quale � stato effettuato l'import
    * --- Nome campo per ricerca valori predefiniti
    * --- Tipo Archivio + Chiave registrazione precedente
    * --- Variabili per gestione unita' di misura
    * --- Unita' di Misura
    * --- U.M. per calcoli
    * --- Moltiplicatore
    * --- Operatore
    * --- Operatore 3^ UM
    * --- Moltiplicatore 3^ UM
    * --- Variabili per gestione resoconti
    * --- Modello di segnalazione
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Numero record per il messaggio di errore sul campo obbligatorio
    * --- Variabili associate ai campi della configurazione dei tracciati records
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo manuale elaborazione
    * --- Controllo valori letti
    * --- Composizione messaggio
    if this.oParentObject.w_TIPSRC<>"O"
      this.w_Messaggio = "Archivio: %1%0Campo: %2%0Valore: %3%0Posizione: %4%0Si desidera proseguire nella verifica?"
      * --- Richiesta
      if .not. AH_yesno( this.w_Messaggio , "" , this.w_Archivio , w_TRCAMDST , this.ToType(NVL(this.w_Valore,"<NULL>"),"C",iif(TRCAMDLE=0,TRCAMLUN,TRCAMDLE),iif(TRCAMDLE=0,TRCAMDEC,TRCAMDDE)) , alltrim(str(this.w_Puntatore,6,0)) )
        this.w_ContValo = "N"
      endif
    else
      this.w_Messaggio = "Archivio: %1%0Campo: %2%0Valore: %3%0Si desidera proseguire nella verifica?"
      * --- Richiesta
      if .not. AH_yesno( this.w_Messaggio , "" , this.w_Archivio , w_TRCAMDST , this.ToType(NVL(this.w_Valore,"<NULL>"),"C",iif(TRCAMDLE=0,TRCAMLUN,TRCAMDLE),iif(TRCAMDLE=0,TRCAMDEC,TRCAMDDE)) )
        this.w_ContValo = "N"
      endif
    endif
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione valori predefiniti
    * --- La pagina corrente pu� essere chiamata in due modi:
    * --- 1) Con PreNomCam vuoto.
    * --- Cerca il valore predefinito per tutti i campi del file corrente ed inizializza variabili che si chiamano "W_" + NomeCampo
    * --- 2) Con PreNomCam pieno.
    * --- Cerca il valore predefinito solo per il campo specificato e lo assegna a w_Valore.
    this.PreCodFil = ""
    this.PreTipVal = ""
    this.PreCamTip = ""
    this.PreCamLun = ""
    this.PreValPre = ""
    this.PreCurCam = ""
    * --- Calcola il codice del file da utilizzare
    this.PreCodFil = this.w_Destinaz
    * --- Ricerca i valori predefiniti per i campi dell'archivio corrente
    * --- Select from PREDEFIN
    i_nConn=i_TableProp[this.PREDEFIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2],.t.,this.PREDEFIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREDEFIN ";
          +" where PRCODFIL = "+cp_ToStrODBC(this.PreCodFil)+" and ( "+cp_ToStrODBC(this.PreNomCam)+"=' ' or PRNOMCAM = "+cp_ToStrODBC(this.PreNomCam)+" )";
           ,"_Curs_PREDEFIN")
    else
      select * from (i_cTable);
       where PRCODFIL = this.PreCodFil and ( this.PreNomCam=" " or PRNOMCAM = this.PreNomCam );
        into cursor _Curs_PREDEFIN
    endif
    if used('_Curs_PREDEFIN')
      select _Curs_PREDEFIN
      locate for 1=1
      do while not(eof())
      * --- Lettura valori record corrente
      this.PreCurCam = alltrim(PRNOMCAM)
      this.PreCamTip = PRTIPCAM
      this.PreCamLun = PRLUNCAM
      * --- Composizione del nome della variabile associata al campo da valorizzare
      w_TRCAMDST = "w_"+this.PreCurCam
      if type( w_TRCAMDST ) = "U"
        &w_TRCAMDST = ""
      endif
      * --- Se la variabile non � stata inizializzata cerca il valore predefinito
      if empty( evaluate(w_TRCAMDST ))
        * --- Verifica se esiste un valore predefinito specifico per l'import correntemente usato
        this.PreTipVal = ""
        this.PreValPre = ""
        w_SaveFile = alias()
        * --- Read from PREDEIMP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PREDEIMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2],.t.,this.PREDEIMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PITIPVAL,PIVALPRE"+;
            " from "+i_cTable+" PREDEIMP where ";
                +"PICODFIL = "+cp_ToStrODBC(this.PreCodFil);
                +" and PINOMCAM = "+cp_ToStrODBC(this.PreCurCam);
                +" and PICODIMP = "+cp_ToStrODBC(this.oParentObject.w_CodImp);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PITIPVAL,PIVALPRE;
            from (i_cTable) where;
                PICODFIL = this.PreCodFil;
                and PINOMCAM = this.PreCurCam;
                and PICODIMP = this.oParentObject.w_CodImp;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.PreTipVal = NVL(cp_ToDate(_read_.PITIPVAL),cp_NullValue(_read_.PITIPVAL))
          this.PreValPre = NVL(cp_ToDate(_read_.PIVALPRE),cp_NullValue(_read_.PIVALPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        select &w_SaveFile
        * --- Altrimenti utilizza quello generico per tutti gli import
        if empty( this.PreTipVal )
          this.PreTipVal = PRTIPVAL
          this.PreValPre = PRVALPRE
        endif
        * --- Assegnazione del valore predefinito
        if this.PreTipVal = "P" .and. (.not. empty(this.PreValPre))
          * --- Conversione nel formato previsto dal campo
          this.PreValPre = alltrim( this.PreValPre )
          do case
            case this.PreCamTip = "N"
              * --- Numerico
              this.PreValPre = strtran( this.PreValPre , "." , "," )
              &w_TRCAMDST = val( this.PreValPre )
            case this.PreCamTip = "D"
              * --- Data
              &w_TRCAMDST = cp_CharToDate( right( this.PreValPre,2 ) + "-" + substr( this.PreValPre, 5, 2) + "-" + left( this.PreValPre, 4) )
            case this.PreCamTip = "L"
              * --- Logico
              this.PreValPre = upper( this.PreValPre )
              &w_TRCAMDST = iif( this.PreValPre = ".T." , .T., .F.)
            case this.PreCamTip = "E"
              * --- Espressione
              TmpExpr = this.PreValPre
              &w_TRCAMDST = &TmpExpr
            case .T.
              * --- Carattere o altro
              this.PreValPre = left( this.PreValPre + space( this.PreCamLun ), this.PreCamLun)
              &w_TRCAMDST = this.PreValPre
          endcase
          this.w_Valore = this.PreValPre
        endif
      endif
        select _Curs_PREDEFIN
        continue
      enddo
      use
    endif
    * --- Ripristino area di lavoro
    select CursTracc
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione trascodifiche
    * --- Calcola il codice del file da utilizzare
    this.PreCodFil = this.w_Destinaz
    * --- Memorizza il valore da trascodificare in tipo carattere
    Private w_ValoLett, w_CodExt ,w_ValoMess
    w_ValoMess = this.ToType(this.w_Valore,"C",iif(TRCAMDLE=0,TRCAMLUN,TRCAMDLE),iif(TRCAMDLE=0,TRCAMDEC,TRCAMDDE))
    w_ValoLett = left(w_ValoMess+space(50),50)
    * --- Verifica se esiste una trascodifica specifica per l'import correntemente usato
    w_SaveFile = alias()
    w_TestFil = ""
    this.w_Valore = ""
    * --- Ricerca utilizzando CODICE IMPORTAZIONE
    * --- Read from TRASCODI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TRCODENT,TRCODFIL"+;
        " from "+i_cTable+" TRASCODI where ";
            +"TRCODFIL = "+cp_ToStrODBC(this.PreCodFil);
            +" and TRNOMCAM = "+cp_ToStrODBC(w_TRCAMDST);
            +" and TRCODEXT = "+cp_ToStrODBC(w_ValoLett);
            +" and TRCODIMP = "+cp_ToStrODBC(this.oParentObject.w_CodImp);
            +" and TRTIPTRA = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TRCODENT,TRCODFIL;
        from (i_cTable) where;
            TRCODFIL = this.PreCodFil;
            and TRNOMCAM = w_TRCAMDST;
            and TRCODEXT = w_ValoLett;
            and TRCODIMP = this.oParentObject.w_CodImp;
            and TRTIPTRA = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_Valore = NVL(cp_ToDate(_read_.TRCODENT),cp_NullValue(_read_.TRCODENT))
      w_TestFil = NVL(cp_ToDate(_read_.TRCODFIL),cp_NullValue(_read_.TRCODFIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    select &w_SaveFile
    * --- Altrimenti utilizza quella generica per tutti gli import (TRTIPTRA=G e TRCODIMP=Tutti)
    if empty( w_TestFil )
      * --- Read from TRASCODI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TRASCODI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRCODENT,TRCODFIL"+;
          " from "+i_cTable+" TRASCODI where ";
              +"TRCODFIL = "+cp_ToStrODBC(this.PreCodFil);
              +" and TRNOMCAM = "+cp_ToStrODBC(w_TRCAMDST);
              +" and TRCODEXT = "+cp_ToStrODBC(w_ValoLett);
              +" and TRCODIMP = "+cp_ToStrODBC("Tutti");
              +" and TRTIPTRA = "+cp_ToStrODBC("G");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRCODENT,TRCODFIL;
          from (i_cTable) where;
              TRCODFIL = this.PreCodFil;
              and TRNOMCAM = w_TRCAMDST;
              and TRCODEXT = w_ValoLett;
              and TRCODIMP = "Tutti";
              and TRTIPTRA = "G";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_Valore = NVL(cp_ToDate(_read_.TRCODENT),cp_NullValue(_read_.TRCODENT))
        w_TestFil = NVL(cp_ToDate(_read_.TRCODFIL),cp_NullValue(_read_.TRCODFIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_Valore = alltrim(this.w_Valore)
    * --- La trascodifica non � stata trovata. La procedura genera una segnalazione nei resoconti.
    * --- Prosegue provando a caricare il record con il campo in questione vuoto.
    if empty( w_TestFil )
      * --- Verifica se esiste una trascodifica parziale specifica per l'import correntemente usato
      this.w_TrasParz = " "
      store space(1) to w_TrasCF
      * --- Read from TRASCODI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TRASCODI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRTRAPAR"+;
          " from "+i_cTable+" TRASCODI where ";
              +"TRCODFIL = "+cp_ToStrODBC(this.PreCodFil);
              +" and TRNOMCAM = "+cp_ToStrODBC(w_TRCAMDST);
              +" and TRCODIMP = "+cp_ToStrODBC(this.oParentObject.w_CodImp);
              +" and TRTIPTRA = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRTRAPAR;
          from (i_cTable) where;
              TRCODFIL = this.PreCodFil;
              and TRNOMCAM = w_TRCAMDST;
              and TRCODIMP = this.oParentObject.w_CodImp;
              and TRTIPTRA = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TrasParz = NVL(cp_ToDate(_read_.TRTRAPAR),cp_NullValue(_read_.TRTRAPAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(this.w_TrasParz)
        * --- Altrimenti controlla se � parziale quella generica per tutti gli import (TRTIPTRA=G e TRCODIMP=Tutti)
        * --- Read from TRASCODI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TRASCODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRTRAPAR"+;
            " from "+i_cTable+" TRASCODI where ";
                +"TRCODFIL = "+cp_ToStrODBC(this.PreCodFil);
                +" and TRNOMCAM = "+cp_ToStrODBC(w_TRCAMDST);
                +" and TRCODIMP = "+cp_ToStrODBC("Tutti");
                +" and TRTIPTRA = "+cp_ToStrODBC("G");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRTRAPAR;
            from (i_cTable) where;
                TRCODFIL = this.PreCodFil;
                and TRNOMCAM = w_TRCAMDST;
                and TRCODIMP = "Tutti";
                and TRTIPTRA = "G";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TrasParz = NVL(cp_ToDate(_read_.TRTRAPAR),cp_NullValue(_read_.TRTRAPAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_TrasParz)
          * --- Per i Clienti ed i Fornitori che non hanno la trascodifica del codice non scrive il resoconto perche'
          * --- dopo la lettura di tutti i campi del tracciato prova a cercare il nominativo per partita iva o per codice fiscale.
          if .not. ( this.PreCodFil = "CF" .and. alltrim(upper(w_TRCAMDST))="ANCODICE" )
            this.w_ResoMode = "NONTRAS"
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_Valore = ""
        else
          * --- Se � una trascodifica parziale un valore non presente nella trascodifica viene restituitito inalterato
          this.w_Valore = w_ValoLett
          * --- Questa variabile viene valorizzata per permettere la cancellazione del tipo (C/F) per il campo PNCODCON.
          * --- Tale cancellazione avviene con un'ulteriore riga nel tracciato.
          store "S" to w_TrasCF
        endif
      else
        * --- Se � una trascodifica parziale un valore non presente nella trascodifica viene restituitito inalterato
        this.w_Valore = w_ValoLett
        * --- Questa variabile viene valorizzata per permettere la cancellazione del tipo (C/F) per il campo PNCODCON.
        * --- Tale cancellazione avviene con un'ulteriore riga nel tracciato.
        store "S" to w_TrasCF
      endif
    endif
    * --- Ripristino area di lavoro
    select CursTracc
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento ultimo costo/prezzo, aggiornamento lista articoli non movimentati
    this.w_FLAVEN = "A"
    * --- Create temporary table TMPSALDI1
    i_nIdx=cp_AddTableDef('TMPSALDI1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('doculco_imp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALDI1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_FLAVEN = "V"
    * --- Try
    local bErr_04EABD50
    bErr_04EABD50=bTrsErr
    this.Try_04EABD50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04EABD50
    * --- End
    * --- Drop temporary table TMPSALDI1
    i_nIdx=cp_GetTableDefIdx('TMPSALDI1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI1')
    endif
    * --- Drop temporary table LISTART
    i_nIdx=cp_GetTableDefIdx('LISTART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('LISTART')
    endif
  endproc
  proc Try_04EABD50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPSALDI1
    i_nConn=i_TableProp[this.TMPSALDI1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPSALDI1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"doculco_imp",this.TMPSALDI1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna Ultimo Costo
    this.w_FLACOSTO = "C"
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gsma3crs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUCA = _t2.MVDATDOC";
          +",SLCODVAA = _t2.MVVALNAZ";
          +",SLVALUCA = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLDATUCA = _t2.MVDATDOC";
          +",SALDIART.SLCODVAA = _t2.MVVALNAZ";
          +",SALDIART.SLVALUCA = _t2.MVVALULT";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLDATUCA,";
          +"SLCODVAA,";
          +"SLVALUCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVDATDOC,";
          +"t2.MVVALNAZ,";
          +"t2.MVVALULT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLDATUCA = _t2.MVDATDOC";
          +",SLCODVAA = _t2.MVVALNAZ";
          +",SLVALUCA = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUCA = (select MVDATDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLCODVAA = (select MVVALNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLVALUCA = (select MVVALULT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Ultimo Prezzo
    this.w_FLACOSTO = "P"
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SLCODICE,SLCODMAG"
      do vq_exec with 'gsma4crs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUPV = _t2.MVDATDOC";
          +",SLCODVAV = _t2.MVVALNAZ";
          +",SLVALUPV = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
          +"SALDIART.SLDATUPV = _t2.MVDATDOC";
          +",SALDIART.SLCODVAV = _t2.MVVALNAZ";
          +",SALDIART.SLVALUPV = _t2.MVVALULT";
          +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
          +"SLDATUPV,";
          +"SLCODVAV,";
          +"SLVALUPV";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVDATDOC,";
          +"t2.MVVALNAZ,";
          +"t2.MVVALULT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
              +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
          +"SLDATUPV = _t2.MVDATDOC";
          +",SLCODVAV = _t2.MVVALNAZ";
          +",SLVALUPV = _t2.MVVALULT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
              +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDATUPV = (select MVDATDOC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLCODVAV = (select MVVALNAZ from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLVALUPV = (select MVVALULT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino dalla lista degli articoli quelli gi� trattati
    * --- Delete from LISTART
    i_nConn=i_TableProp[this.LISTART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".KEYSAL = "+i_cQueryTable+".KEYSAL";
            +" and "+i_cTable+".ACQVEN = "+i_cQueryTable+".ACQVEN";
    
      do vq_exec with 'gsma4brs',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancello tabella temporanea
    return


  procedure Pag12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(this.w_FILEEXC)
      ah_errormsg("Errore di importazione, file o foglio inesistente, o gi� in uso, o formato excel errato.","!","")
      if this.oParentObject.w_IMTRANSA = "S"
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Otteniamo lo stesso nome del cursore
      this.w_CURSOR = STRTRAN(JUSTSTEM(this.w_FILEEXC), " ", "_")
      this.w_FOGLEXC = ALLTRIM(NVL(this.w_FOGLEXC," "))
      L_Err=.f. 
 L_OLDERROR=On("Error") 
 on error L_err=.t.
      * --- I dbf dei file importati andranno nella cartella temporanei di adhoc
      p='"'+TEMPADHOC()+'"'
      CD &p
      if NOT EMPTY(this.w_FOGLEXC)
        IMPORT FROM ALLTRIM(this.w_FILEEXC) XL5 SHEET (this.w_FOGLEXC)
      else
        IMPORT FROM ALLTRIM(this.w_FILEEXC) XLS
      endif
      * --- Torno alla cartella principale
      p='"'+cHomeDir+'"'
      CD &p
      on error &L_OldError 
      if l_err=.t.
        ah_errormsg("Errore di importazione, file o foglio inesistente, o gi� in uso, o formato excel errato.","!","")
        if this.oParentObject.w_IMTRANSA = "S"
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT * FROM (this.w_CURSOR) INTO CURSOR "FOGLIO"
      USE IN (this.w_CURSOR)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,51)]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='CATECOMM'
    this.cWorkTables[6]='CAUIVA'
    this.cWorkTables[7]='CAUIVA1'
    this.cWorkTables[8]='CAUPRI'
    this.cWorkTables[9]='CAUPRI1'
    this.cWorkTables[10]='CAU_CONT'
    this.cWorkTables[11]='COC_MAST'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='DES_DIVE'
    this.cWorkTables[14]='IMPOARCH'
    this.cWorkTables[15]='LINGUE'
    this.cWorkTables[16]='LISTINI'
    this.cWorkTables[17]='MASTRI'
    this.cWorkTables[18]='MOD_CONT'
    this.cWorkTables[19]='MOD_PAGA'
    this.cWorkTables[20]='NAZIONI'
    this.cWorkTables[21]='PAG_2AME'
    this.cWorkTables[22]='PAG_AMEN'
    this.cWorkTables[23]='PREDEFIN'
    this.cWorkTables[24]='PREDEIMP'
    this.cWorkTables[25]='RESOCONT'
    this.cWorkTables[26]='SALDICON'
    this.cWorkTables[27]='SRCMODBC'
    this.cWorkTables[28]='TRASCODI'
    this.cWorkTables[29]='VALUTE'
    this.cWorkTables[30]='VOCIIVA'
    this.cWorkTables[31]='ZONE'
    this.cWorkTables[32]='COD_ABI'
    this.cWorkTables[33]='COD_CAB'
    this.cWorkTables[34]='CAM_BI'
    this.cWorkTables[35]='ESERCIZI'
    this.cWorkTables[36]='CONTATTI'
    this.cWorkTables[37]='IMPORTAZ'
    this.cWorkTables[38]='OFF_NOMI'
    this.cWorkTables[39]='NOM_ATTR'
    this.cWorkTables[40]='CAT_ATTR'
    this.cWorkTables[41]='SALDIART'
    this.cWorkTables[42]='*LISTART'
    this.cWorkTables[43]='*TMPSALDI1'
    this.cWorkTables[44]='ART_ICOL'
    this.cWorkTables[45]='AZIENDA'
    this.cWorkTables[46]='PAR_ALTE'
    this.cWorkTables[47]='COLCRONO'
    this.cWorkTables[48]='PAR_CAUS'
    this.cWorkTables[49]='PRE_STAZ'
    this.cWorkTables[50]='BAN_CONTI'
    this.cWorkTables[51]='DOC_MAST'
    return(this.OpenAllTables(51))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_PREDEFIN')
      use in _Curs_PREDEFIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsim_bac
  procedure CloseCurs()
        if used( "CursFiles" )
         select CursFiles
         use
        endif
        if used( "CursTracc" )
         select CursTracc
         use
        endif
        if used( "CursTrRow" )
         select CursTrRow
         use
        endif
        if used( "CursAscii" )
         select CursAscii
         use
        endif
        if used( "CursAsRow" )
         select CursAsRow
         use
        endif
        if used( "__tmp__" )
         select __tmp__
         use
        endif
      return
  
  Function ToType(uValue, cTypeDest, nLen, nDec)
    local uRet, cType, uTmp	    
    cTypeValue = TYPE('uValue')
  
    if cTypeValue='Y'
       * Converte Currency in numerico
       uValue=MTON(uValue) 
       cTypeValue='N'
    endif
    
    if (cTypeValue=cTypeDest) or (cTypeValue='C' and cTypeDest='M')
       * Stesso Tipo
       if (cTypeDest='C') and len(alltrim(uValue))<=nLen
         if(empty(leftc(uValue,nLen)) and not empty(alltrim(uValue)))
           uValue = leftc(alltrim(uValue),nLen)
         else
           uValue = leftc(uValue,nLen)
         endif
       endif
       uRet = uValue
    else
       do case
         	  * Carattere -> Numero
            case cTypeValue$"C|M" and cTypeDest = "N"
                 uValue = strtran(uValue,".",",")                     
                 uRet = val(uValue)
  
         	  * Carattere -> Data
            case cTypeValue$"C|M" and cTypeDest = "D"               
                 if atc("/",uValue) > 0 or atc("-",uValue) > 0
                    * Formato data gg/mm/aaaa oppure gg-mm-aaaa
                    uRet = cp_CharToDate(uValue)
                 else
                    uTmp = alltrim(uValue)                  
                    if len(uTmp) = 8
                       * Formato data aaaammgg
                       uRet = cp_CharToDate(right(uTmp,2 )+"-"+substr(uTmp,5,2)+"-"+left(uTmp,4))
                    else
                       * Formato data aammgg
                       uRet = cp_CharToDate(right(uTmp,2 )+"-"+substr(uTmp,3,2)+"-"+left(uTmp,2))
                    endif
                 endif
  
         	  * Numero -> Carattere
            case cTypeValue="N" and cTypeDest $ "C|M"    
                 uRet = iif(uValue=0,"",alltrim(str(uValue,nLen,nDec)))
  
         	  * Data -> Carattere in formato gg-mm-aaaa
            case cTypeValue="D" and cTypeDest $ "C|M"                        
                 uRet = dtoc(uValue)
  
         	  * Bool -> Carattere
            case cTypeValue="L" and cTypeDest $ "C|M"                        
                 uRet = iif(uValue,"S","N")
  
         	  * Carattere -> Bool
            case cTypeValue$"C|M" and cTypeDest = "L"                        
                 uRet = iif(upper(uValue)="S",.T.,.F.)
  
         	  * Memo -> Carattere
            case cTypeValue="M" and cTypeDest = "C"                        
                 uRet = left(chrtran(alltrim(uValue),chr(13)+chr(10),"  "),nLen)
                 
            * Datatime -> Data
            case cTypeValue$"T" and cTypeDest = "D"                        
                 uRet = cp_todate(uValue)
                 
            * Datatime -> Carattere
            case cTypeValue$"T" and cTypeDest = "C" 
                 uRet = cp_todate(uValue)
                 uRet = dtoc(uValue)
  
       otherwise
            MessageBox("Impossibile convertire un valore di tipo [" + cTypeValue+;
                       "] in ["+cTypeDest+"]",48,"Conversione Tipo")
       endcase 	 	  
    endif
  
  return (uRet)
  
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
