* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bvv                                                        *
*              Varianti distinta base                                          *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_198]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2007-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bvv",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bvv as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_ODBC = space(30)
  w_OLDDIS = space(15)
  w_OLDART = space(15)
  w_CODDIS = space(15)
  w_CODRIF = space(15)
  w_DESAG1 = space(40)
  w_DESAG2 = space(40)
  w_DATDIS = ctod("  /  /  ")
  w_VALAGG = 0
  w_RICARI = 0
  w_SCHELA = space(10)
  w_CODART = space(15)
  w_RIFART = space(15)
  w_QTAMOV = 0
  w_UNIMIS = space(2)
  w_SFRIDO = space(1)
  w_TIPVAL = space(1)
  w_CODLIS = space(3)
  w_FLESPL = space(1)
  w_ULTRIC = 0
  w_NUMREC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TRASFORMA CURSORE ESPLICITANDO LE VARIANTI COME DISTINTE BASI COMPLETE
    * --- SISTEMA FLAG SFRIDO E FLAG ESPLOSIONE
    * --- CURSORI UTILIZZATI:
    * --- DISTINTE Contiene i codici delle distinte basi (per sistemazione flag esplosione)
    * --- NORMALI Contiene le distinte basi che non sono varianti
    * --- VARIANTI Contiene le distinte basi che sono varianti
    * --- DUPLICA Contiene le distinte basi che non sono varianti e che sono prese come riferimento da una variante 
    * --- MODIFICA Contiene le distinte basi ottenute come composizione delle distinte base che sono prese come riferimento da una variante con la variante stessa
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    this.w_OLDDIS = SPACE(15)
    this.w_OLDART = SPACE(15)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      * --- Creazione cursore distinte contenente i codici delle distinte basi
      SELECT DSCODDIS FROM (this.w_CURSOR) INTO CURSOR DISTINTE
      = WRCURSOR("DISTINTE")
       Index On (DSCODDIS) Tag idx1
      =WRCURSOR(this.w_CURSOR)
      SELECT (this.w_CURSOR)
      GO TOP
      * --- Scansione cursore per sistemazione flag esplosione
      SCAN
      * --- Salvataggio numero record corrente (per sistemare spostamenti dipendenti dalla replace)
      this.w_NUMREC = RECNO()
      this.w_CODDIS = DSCODDIS
      this.w_CODART = DSCODART
      SELECT DISTINTE
      * --- Controllo se il componente � una distinta
      if NOT Seek(this.w_CODART)
        * --- Se non � una distinta il flag esplosione viene forzato a 'N'
        SELECT (this.w_CURSOR)
        REPLACE DSFLESPL WITH "N" WHILE DSCODDIS=this.w_CODDIS AND DSCODART=this.w_CODART
      else
        SELECT (this.w_CURSOR)
      endif
      * --- Posizionamento nel record corrente del cursore
      GOTO this.w_NUMREC
      ENDSCAN
      SELECT * FROM (this.w_CURSOR) WHERE EMPTY(DSCODRIF) INTO CURSOR NORMALI
      SELECT *,"N" AS FLDISB FROM (this.w_CURSOR) WHERE NOT EMPTY(DSCODRIF) INTO CURSOR VARIANTI
      SELECT * FROM (this.w_CURSOR) WHERE DSCODDIS IN (SELECT DSCODRIF FROM VARIANTI) INTO CURSOR DUPLICA
      = WRCURSOR("VARIANTI")
       
 Select VARIANTI
       Index On (DSCODRIF+DSRIFART) Tag idx1
      CREATE CURSOR MODIFICA (DSCODDIS C(15), DSCODRIF C(15), DSDESAG1 C(40), DSDESAG2 C(40), DSDATDIS D(8), DSVALAGG N(15,5), ;
      DSRICARI N(6,2), DSSCHELA C(10), DSCODART C(15), DSRIFART C(15), DSQTAMOV N(12,3), DSUNIMIS C(2), DSSFRIDO C(1), ;
      DSTIPVAL C(1), DSCODLIS C(3), DSFLESPL C(1), DSULTRIC N(6,2))
      SELECT DUPLICA
      GO TOP
      * --- Inserimento nel cursore MODIFICA delle distinte basi che non sono varianti e che sono prese come riferimento da una variante modificate con i dati della variante
      SCAN
      this.w_CODDIS = DSCODDIS
      this.w_CODRIF = DSCODRIF
      this.w_DESAG1 = DSDESAG1
      this.w_DESAG2 = DSDESAG2
      this.w_DATDIS = DSDATDIS
      this.w_VALAGG = DSVALAGG
      this.w_RICARI = DSRICARI
      this.w_SCHELA = DSSCHELA
      this.w_CODART = DSCODART
      this.w_RIFART = DSRIFART
      this.w_QTAMOV = DSQTAMOV
      this.w_UNIMIS = DSUNIMIS
      this.w_SFRIDO = DSSFRIDO
      this.w_TIPVAL = DSTIPVAL
      this.w_CODLIS = DSCODLIS
      this.w_FLESPL = DSFLESPL
      this.w_ULTRIC = DSULTRIC
      SELECT VARIANTI
      if Seek(this.w_CODDIS)
        this.w_OLDDIS = this.w_CODDIS
        this.w_CODDIS = DSCODDIS
        this.w_CODRIF = DSCODRIF
        this.w_DESAG1 = DSDESAG1
        this.w_DESAG2 = DSDESAG2
        this.w_DATDIS = DSDATDIS
        this.w_VALAGG = DSVALAGG
        this.w_RICARI = DSRICARI
        this.w_SCHELA = DSSCHELA
        if Seek(this.w_OLDDIS+this.w_CODART)
          this.w_OLDART = this.w_CODART
          this.w_CODART = DSCODART
          this.w_RIFART = DSRIFART
          this.w_QTAMOV = DSQTAMOV
          this.w_UNIMIS = DSUNIMIS
          this.w_SFRIDO = DSSFRIDO
          this.w_TIPVAL = DSTIPVAL
          this.w_CODLIS = DSCODLIS
          this.w_FLESPL = DSFLESPL
          this.w_ULTRIC = DSULTRIC
          * --- marco le righe gi� modificate nelle distinte basi di riferimento
          UPDATE VARIANTI SET FLDISB="S" WHERE DSCODRIF=this.w_OLDDIS AND DSRIFART=this.w_OLDART
        endif
      else
        this.w_CODDIS = this.w_OLDDIS
      endif
      INSERT INTO MODIFICA (DSCODDIS, DSCODRIF, DSDESAG1, DSDESAG2, DSDATDIS, DSVALAGG, ;
      DSRICARI, DSSCHELA, DSCODART, DSRIFART, DSQTAMOV, DSUNIMIS, DSSFRIDO, DSTIPVAL, DSCODLIS, DSFLESPL, DSULTRIC) ;
      VALUES (this.w_CODDIS, this.w_CODRIF, this.w_DESAG1, this.w_DESAG2, this.w_DATDIS, this.w_VALAGG, this.w_RICARI, this.w_SCHELA, this.w_CODART, this.w_RIFART, ;
      this.w_QTAMOV, this.w_UNIMIS, this.w_SFRIDO, this.w_TIPVAL, this.w_CODLIS, this.w_FLESPL, this.w_ULTRIC)
      SELECT DUPLICA
      ENDSCAN
      SELECT VARIANTI
      * --- Inserimento nel cursore MODIFICA delle distinte basi sono varianti e non sono presenti nelle distinte basi che sono prese come riferimento da una variante
      SCAN FOR FLDISB="N"
      SELECT VARIANTI
      this.w_CODDIS = DSCODDIS
      this.w_CODRIF = DSCODRIF
      this.w_DESAG1 = DSDESAG1
      this.w_DESAG2 = DSDESAG2
      this.w_DATDIS = DSDATDIS
      this.w_VALAGG = DSVALAGG
      this.w_RICARI = DSRICARI
      this.w_SCHELA = DSSCHELA
      this.w_CODART = DSCODART
      this.w_RIFART = DSRIFART
      this.w_QTAMOV = DSQTAMOV
      this.w_UNIMIS = DSUNIMIS
      this.w_SFRIDO = DSSFRIDO
      this.w_TIPVAL = DSTIPVAL
      this.w_CODLIS = DSCODLIS
      this.w_FLESPL = DSFLESPL
      this.w_ULTRIC = DSULTRIC
      INSERT INTO MODIFICA (DSCODDIS, DSCODRIF, DSDESAG1, DSDESAG2, DSDATDIS, DSVALAGG, ;
      DSRICARI, DSSCHELA, DSCODART, DSRIFART, DSQTAMOV, DSUNIMIS, DSSFRIDO, DSTIPVAL, DSCODLIS, DSFLESPL, DSULTRIC) ;
      VALUES (this.w_CODDIS, this.w_CODRIF, this.w_DESAG1, this.w_DESAG2, this.w_DATDIS, this.w_VALAGG, this.w_RICARI, this.w_SCHELA, this.w_CODART, this.w_RIFART, ;
      this.w_QTAMOV, this.w_UNIMIS, this.w_SFRIDO, this.w_TIPVAL, this.w_CODLIS, this.w_FLESPL, this.w_ULTRIC)
      ENDSCAN
      SELECT (this.w_CURSOR)
      use
      * --- Eliminazione dal cursore MODIFICA delle distinte basi che hanno quantit� 0 o hanno attivo il flag SFRIDO
      SELECT * FROM NORMALI WHERE DSSFRIDO<>"S" UNION SELECT * FROM MODIFICA WHERE DSSFRIDO<>"S" AND NOT EMPTY(DSQTAMOV) ;
      INTO CURSOR (this.w_CURSOR) ORDER BY 1
    endif
    if used("DISTINTE")
      select DISTINTE
      use
    endif
    if used("NORMALI")
      select NORMALI
      use
    endif
    if used("VARIANTI")
      select VARIANTI
      use
    endif
    if used("DUPLICA")
      select DUPLICA
      use
    endif
    if used("MODIFICA")
      select MODIFICA
      use
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
