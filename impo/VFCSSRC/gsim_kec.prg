* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kec                                                        *
*              Import dati F24                                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2013-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kec",oParentObject))

* --- Class definition
define class tgsim_kec as StdForm
  Top    = 24
  Left   = 88

  * --- Standard Properties
  Width  = 636
  Height = 385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-16"
  HelpContextID=65650537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  IMPORTAZ_IDX = 0
  SRCMODBC_IDX = 0
  cPrg = "gsim_kec"
  cComment = "Import dati F24"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_INCORSO = .F.
  w_TIPSRC = space(1)
  o_TIPSRC = space(1)
  w_CODIMP = space(20)
  w_DESIMP = space(60)
  w_RADSELEZ = space(10)
  w_SELECONT = space(10)
  w_SELERESO = space(10)
  w_IMPARIVA = space(10)
  w_IMAZZERA = space(10)
  w_IMTRANSA = space(1)
  w_IMCONFER = space(1)
  w_ArchParz = space(45)
  w_ArchPar2 = space(45)
  w_Gestiti = space(45)
  w_SELECS = space(10)
  w_TIPDBF = space(1)
  w_SELECU = space(10)
  w_SELECK = space(10)
  w_SELECJ = space(10)
  w_SELECV = space(10)
  w_SELENC = space(10)
  w_SELESI = space(10)
  w_SELESN = space(10)
  w_PATH = space(200)
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_SELEEL = space(10)
  w_SELERG = space(10)
  * --- Area Manuale = Declare Variables
  * --- gsim_kec
  Autocenter=.T.
  
  Function GetControlObject(cVar,nPage)
    local oObj
    local nIndex
    oObj = .NULL.
    cVar=upper(cVar)
    with this.oPgFrm.Pages(nPage).oPag
      for nIndex=1 to .ControlCount
          if type('this.oPgFrm.Pages(nPage).oPag.Controls(nIndex).cFormVar') = 'C'
             if upper(.Controls(nIndex).cFormVar) == cVar
                oObj= .Controls(nIndex)
                nIndex = .ControlCount + 1
             endif
          endif
      next
    endwith
    return (oObj)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kecPag1","gsim_kec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODIMP_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='IMPORTAZ'
    this.cWorkTables[2]='SRCMODBC'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gsim_bac with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_INCORSO=.f.
      .w_TIPSRC=space(1)
      .w_CODIMP=space(20)
      .w_DESIMP=space(60)
      .w_RADSELEZ=space(10)
      .w_SELECONT=space(10)
      .w_SELERESO=space(10)
      .w_IMPARIVA=space(10)
      .w_IMAZZERA=space(10)
      .w_IMTRANSA=space(1)
      .w_IMCONFER=space(1)
      .w_ArchParz=space(45)
      .w_ArchPar2=space(45)
      .w_Gestiti=space(45)
      .w_SELECS=space(10)
      .w_TIPDBF=space(1)
      .w_SELECU=space(10)
      .w_SELECK=space(10)
      .w_SELECJ=space(10)
      .w_SELECV=space(10)
      .w_SELENC=space(10)
      .w_SELESI=space(10)
      .w_SELESN=space(10)
      .w_PATH=space(200)
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_SELEEL=space(10)
      .w_SELERG=space(10)
        .w_INCORSO = .F.
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODIMP))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_RADSELEZ = 'D'
        .w_SELECONT = 'N'
        .w_SELERESO = 'S'
        .w_IMPARIVA = 'S'
        .w_IMAZZERA = 'N'
        .w_IMTRANSA =  "S"
        .w_IMCONFER = 'N'
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_ArchParz = .w_SELECU+"|"+.w_SELECK+"|"+.w_SELECJ+"|"+.w_SELECV+"|"+.w_SELENC+"|"+.w_SELESI+"|"+.w_SELESN+"|"+.w_SELEEL+"|"+.w_SELERG+"|"
          .DoRTCalc(13,13,.f.)
        .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
        .w_SELECS = '  '
          .DoRTCalc(16,16,.f.)
        .w_SELECU = 'CU'
        .w_SELECK = 'CK'
        .w_SELECJ = 'CJ'
        .w_SELECV = 'CV'
        .w_SELENC = 'NC'
        .w_SELESI = 'SI'
        .w_SELESN = 'SN'
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
          .DoRTCalc(25,25,.f.)
        .w_ODBCPATH = SPACE(200)
          .DoRTCalc(27,28,.f.)
        .w_SELEEL = 'EL'
        .w_SELERG = 'RG'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(1,11,.t.)
            .w_ArchParz = .w_SELECU+"|"+.w_SELECK+"|"+.w_SELECJ+"|"+.w_SELECV+"|"+.w_SELENC+"|"+.w_SELESI+"|"+.w_SELESN+"|"+.w_SELEEL+"|"+.w_SELERG+"|"
        .DoRTCalc(13,13,.t.)
            .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
            .w_SELECS = '  '
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(16,23,.t.)
        if .o_TIPSRC<>.w_TIPSRC
            .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODIMP_1_3.enabled = this.oPgFrm.Page1.oPag.oCODIMP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oRADSELEZ_1_9.enabled_(this.oPgFrm.Page1.oPag.oRADSELEZ_1_9.mCond())
    this.oPgFrm.Page1.oPag.oSELECONT_1_13.enabled = this.oPgFrm.Page1.oPag.oSELECONT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oSELERESO_1_14.enabled = this.oPgFrm.Page1.oPag.oSELERESO_1_14.mCond()
    this.oPgFrm.Page1.oPag.oIMAZZERA_1_16.enabled = this.oPgFrm.Page1.oPag.oIMAZZERA_1_16.mCond()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_18.enabled = this.oPgFrm.Page1.oPag.oIMCONFER_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSELECU_1_28.enabled = this.oPgFrm.Page1.oPag.oSELECU_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSELECK_1_29.enabled = this.oPgFrm.Page1.oPag.oSELECK_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSELECJ_1_30.enabled = this.oPgFrm.Page1.oPag.oSELECJ_1_30.mCond()
    this.oPgFrm.Page1.oPag.oSELECV_1_31.enabled = this.oPgFrm.Page1.oPag.oSELECV_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSELENC_1_32.enabled = this.oPgFrm.Page1.oPag.oSELENC_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSELESI_1_33.enabled = this.oPgFrm.Page1.oPag.oSELESI_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSELESN_1_34.enabled = this.oPgFrm.Page1.oPag.oSELESN_1_34.mCond()
    this.oPgFrm.Page1.oPag.oPATH_1_36.enabled = this.oPgFrm.Page1.oPag.oPATH_1_36.mCond()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_37.enabled = this.oPgFrm.Page1.oPag.oODBCDSN_1_37.mCond()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_39.enabled = this.oPgFrm.Page1.oPag.oODBCPATH_1_39.mCond()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_41.enabled = this.oPgFrm.Page1.oPag.oODBCUSER_1_41.mCond()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.enabled = this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSELEEL_1_48.enabled = this.oPgFrm.Page1.oPag.oSELEEL_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSELERG_1_49.enabled = this.oPgFrm.Page1.oPag.oSELERG_1_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_18.visible=!this.oPgFrm.Page1.oPag.oIMCONFER_1_18.mHide()
    this.oPgFrm.Page1.oPag.oPATH_1_36.visible=!this.oPgFrm.Page1.oPag.oPATH_1_36.mHide()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_37.visible=!this.oPgFrm.Page1.oPag.oODBCDSN_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_39.visible=!this.oPgFrm.Page1.oPag.oODBCPATH_1_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_41.visible=!this.oPgFrm.Page1.oPag.oODBCUSER_1_41.mHide()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.visible=!this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_3'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(60))
      this.w_IMPARIVA = NVL(_Link_.IMPARIVA,space(10))
      this.w_IMAZZERA = NVL(_Link_.IMAZZERA,space(10))
      this.w_SELERESO = NVL(_Link_.IMRESOCO,space(10))
      this.w_SELECONT = NVL(_Link_.IMCONTRO,space(10))
      this.w_TIPSRC = NVL(_Link_.IMTIPSRC,space(1))
      this.w_TIPDBF = NVL(_Link_.IMTIPDBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(60)
      this.w_IMPARIVA = space(10)
      this.w_IMAZZERA = space(10)
      this.w_SELERESO = space(10)
      this.w_SELECONT = space(10)
      this.w_TIPSRC = space(1)
      this.w_TIPDBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_3.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_3.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_4.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_4.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oRADSELEZ_1_9.RadioValue()==this.w_RADSELEZ)
      this.oPgFrm.Page1.oPag.oRADSELEZ_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECONT_1_13.RadioValue()==this.w_SELECONT)
      this.oPgFrm.Page1.oPag.oSELECONT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERESO_1_14.RadioValue()==this.w_SELERESO)
      this.oPgFrm.Page1.oPag.oSELERESO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAZZERA_1_16.RadioValue()==this.w_IMAZZERA)
      this.oPgFrm.Page1.oPag.oIMAZZERA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTRANSA_1_17.RadioValue()==this.w_IMTRANSA)
      this.oPgFrm.Page1.oPag.oIMTRANSA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCONFER_1_18.RadioValue()==this.w_IMCONFER)
      this.oPgFrm.Page1.oPag.oIMCONFER_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECU_1_28.RadioValue()==this.w_SELECU)
      this.oPgFrm.Page1.oPag.oSELECU_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECK_1_29.RadioValue()==this.w_SELECK)
      this.oPgFrm.Page1.oPag.oSELECK_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECJ_1_30.RadioValue()==this.w_SELECJ)
      this.oPgFrm.Page1.oPag.oSELECJ_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECV_1_31.RadioValue()==this.w_SELECV)
      this.oPgFrm.Page1.oPag.oSELECV_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENC_1_32.RadioValue()==this.w_SELENC)
      this.oPgFrm.Page1.oPag.oSELENC_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESI_1_33.RadioValue()==this.w_SELESI)
      this.oPgFrm.Page1.oPag.oSELESI_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESN_1_34.RadioValue()==this.w_SELESN)
      this.oPgFrm.Page1.oPag.oSELESN_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_36.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_36.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCDSN_1_37.value==this.w_ODBCDSN)
      this.oPgFrm.Page1.oPag.oODBCDSN_1_37.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPATH_1_39.value==this.w_ODBCPATH)
      this.oPgFrm.Page1.oPag.oODBCPATH_1_39.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCUSER_1_41.value==this.w_ODBCUSER)
      this.oPgFrm.Page1.oPag.oODBCUSER_1_41.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.value==this.w_ODBCPASSW)
      this.oPgFrm.Page1.oPag.oODBCPASSW_1_42.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEEL_1_48.RadioValue()==this.w_SELEEL)
      this.oPgFrm.Page1.oPag.oSELEEL_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERG_1_49.RadioValue()==this.w_SELERG)
      this.oPgFrm.Page1.oPag.oSELERG_1_49.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODIMP))  and (!.w_INCORSO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMP_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPSRC = this.w_TIPSRC
    return

enddefine

* --- Define pages as container
define class tgsim_kecPag1 as StdContainer
  Width  = 632
  height = 385
  stdWidth  = 632
  stdheight = 385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODIMP_1_3 as StdField with uid="JVRUITIVQR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 248261594,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=104, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  func oCODIMP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_3.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oDESIMP_1_4 as StdField with uid="OMDLTUIPBE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 248202698,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=263, Top=13, InputMask=replicate('X',60)

  add object oRADSELEZ_1_9 as StdRadio with uid="JTFLDCVNJL",rtseq=5,rtrep=.f.,left=365, top=283, width=232,height=23;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELEZ_1_9.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 213763952
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 213763952
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ_1_9.GetRadio()
    this.Parent.oContained.w_RADSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ_1_9.SetRadio()
    this.Parent.oContained.w_RADSELEZ=trim(this.Parent.oContained.w_RADSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ=='D',2,;
      0))
  endfunc

  func oRADSELEZ_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODIMP) and !.w_INCORSO)
    endwith
   endif
  endfunc

  add object oSELECONT_1_13 as StdCheck with uid="HJVITWNLMM",rtseq=6,rtrep=.f.,left=26, top=303, caption="Controllo valori",;
    ToolTipText = "Se attivato consente di controllare i valori assegnati ai singoli campi",;
    HelpContextID = 7320710,;
    cFormVar="w_SELECONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECONT_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELECONT_1_13.GetRadio()
    this.Parent.oContained.w_SELECONT = this.RadioValue()
    return .t.
  endfunc

  func oSELECONT_1_13.SetRadio()
    this.Parent.oContained.w_SELECONT=trim(this.Parent.oContained.w_SELECONT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECONT=='S',1,;
      0)
  endfunc

  func oSELECONT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc


  add object oSELERESO_1_14 as StdCombo with uid="VYMIVBYKOS",rtseq=7,rtrep=.f.,left=26,top=327,width=131,height=21;
    , ToolTipText = "Se attivato genera il resoconto delle operazioni di import";
    , HelpContextID = 109071221;
    , cFormVar="w_SELERESO",RowSource=""+"Errori+segnalazioni,"+"Errori+stop dopo 10,"+"Errori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELERESO_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    'N')))))
  endfunc
  func oSELERESO_1_14.GetRadio()
    this.Parent.oContained.w_SELERESO = this.RadioValue()
    return .t.
  endfunc

  func oSELERESO_1_14.SetRadio()
    this.Parent.oContained.w_SELERESO=trim(this.Parent.oContained.w_SELERESO)
    this.value = ;
      iif(this.Parent.oContained.w_SELERESO=='S',1,;
      iif(this.Parent.oContained.w_SELERESO=='D',2,;
      iif(this.Parent.oContained.w_SELERESO=='E',3,;
      iif(this.Parent.oContained.w_SELERESO=='N',4,;
      0))))
  endfunc

  func oSELERESO_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMAZZERA_1_16 as StdCheck with uid="HMAFHBTQIQ",rtseq=9,rtrep=.f.,left=184, top=319, caption="Azzera movimenti",;
    ToolTipText = "Se attivato la procedura elimina le movimentazioni prima di importarle",;
    HelpContextID = 118792903,;
    cFormVar="w_IMAZZERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAZZERA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAZZERA_1_16.GetRadio()
    this.Parent.oContained.w_IMAZZERA = this.RadioValue()
    return .t.
  endfunc

  func oIMAZZERA_1_16.SetRadio()
    this.Parent.oContained.w_IMAZZERA=trim(this.Parent.oContained.w_IMAZZERA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAZZERA=='S',1,;
      0)
  endfunc

  func oIMAZZERA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMTRANSA_1_17 as StdCheck with uid="GPDSMVITBT",rtseq=10,rtrep=.f.,left=184, top=335, caption="Sotto transazione",;
    ToolTipText = "Se attivato la procedura viene eseguita sotto un'unica transazione",;
    HelpContextID = 243126983,;
    cFormVar="w_IMTRANSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMTRANSA_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMTRANSA_1_17.GetRadio()
    this.Parent.oContained.w_IMTRANSA = this.RadioValue()
    return .t.
  endfunc

  func oIMTRANSA_1_17.SetRadio()
    this.Parent.oContained.w_IMTRANSA=trim(this.Parent.oContained.w_IMTRANSA)
    this.value = ;
      iif(this.Parent.oContained.w_IMTRANSA=='S',1,;
      0)
  endfunc

  add object oIMCONFER_1_18 as StdCheck with uid="RVMTDVMTAB",rtseq=11,rtrep=.f.,left=184, top=351, caption="Richiesta conferma",;
    ToolTipText = "Se attivato viene richiesto la conferma prima di aggiornare il database (valido solo se attivo sotto transazione)",;
    HelpContextID = 122274520,;
    cFormVar="w_IMCONFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMCONFER_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMCONFER_1_18.GetRadio()
    this.Parent.oContained.w_IMCONFER = this.RadioValue()
    return .t.
  endfunc

  func oIMCONFER_1_18.SetRadio()
    this.Parent.oContained.w_IMCONFER=trim(this.Parent.oContained.w_IMCONFER)
    this.value = ;
      iif(this.Parent.oContained.w_IMCONFER=='S',1,;
      0)
  endfunc

  func oIMCONFER_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTRANSA='S' and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oIMCONFER_1_18.mHide()
    with this.Parent.oContained
      return (.w_IMTRANSA<>'S')
    endwith
  endfunc


  add object oBtn_1_19 as StdButton with uid="VQERAGEWET",left=518, top=318, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65621786;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do GSIM_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO and (not empty(.w_CODIMP)) and (.w_TIPSRC<>'0' or !empty(.w_ODBCDSN)) and g_IMPOMAGA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="MRPTSAHHIT",left=575, top=318, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 58333114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="IXBUJQRYLM",left=369, top=318, width=48,height=45,;
    CpPicture="BMP\info.bmp", caption="", nPag=1;
    , HelpContextID = 210539434;
    , caption='\<Info';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      do gsim_kai with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oObj_1_22 as cp_runprogram with uid="TMLZAGARPU",left=296, top=400, width=339,height=19,;
    caption='GSIM_BSE(2)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSE(2)",;
    cEvent = "w_RADSELEZ Changed,w_CODIMP Changed",;
    nPag=1;
    , HelpContextID = 73067819

  add object oSELECU_1_28 as StdCheck with uid="PLBRLEGNFM",rtseq=17,rtrep=.f.,left=27, top=114, caption="Codice Tributo",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 175092954,;
    cFormVar="w_SELECU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECU_1_28.RadioValue()
    return(iif(this.value =1,'CU',;
    ' '))
  endfunc
  func oSELECU_1_28.GetRadio()
    this.Parent.oContained.w_SELECU = this.RadioValue()
    return .t.
  endfunc

  func oSELECU_1_28.SetRadio()
    this.Parent.oContained.w_SELECU=trim(this.Parent.oContained.w_SELECU)
    this.value = ;
      iif(this.Parent.oContained.w_SELECU=='CU',1,;
      0)
  endfunc

  func oSELECU_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECU<>"|")
    endwith
   endif
  endfunc

  add object oSELECK_1_29 as StdCheck with uid="ILDBWXOABO",rtseq=18,rtrep=.f.,left=27, top=152, caption="Causali Altri Enti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 74429658,;
    cFormVar="w_SELECK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECK_1_29.RadioValue()
    return(iif(this.value =1,'CK',;
    ' '))
  endfunc
  func oSELECK_1_29.GetRadio()
    this.Parent.oContained.w_SELECK = this.RadioValue()
    return .t.
  endfunc

  func oSELECK_1_29.SetRadio()
    this.Parent.oContained.w_SELECK=trim(this.Parent.oContained.w_SELECK)
    this.value = ;
      iif(this.Parent.oContained.w_SELECK=='CK',1,;
      0)
  endfunc

  func oSELECK_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECK<>"|")
    endwith
   endif
  endfunc

  add object oSELECJ_1_30 as StdCheck with uid="DKTDVRDCHH",rtseq=19,rtrep=.f.,left=27, top=171, caption="Causali INPS",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 91206874,;
    cFormVar="w_SELECJ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECJ_1_30.RadioValue()
    return(iif(this.value =1,'CJ',;
    ' '))
  endfunc
  func oSELECJ_1_30.GetRadio()
    this.Parent.oContained.w_SELECJ = this.RadioValue()
    return .t.
  endfunc

  func oSELECJ_1_30.SetRadio()
    this.Parent.oContained.w_SELECJ=trim(this.Parent.oContained.w_SELECJ)
    this.value = ;
      iif(this.Parent.oContained.w_SELECJ=='CJ',1,;
      0)
  endfunc

  func oSELECJ_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECJ<>"|")
    endwith
   endif
  endfunc

  add object oSELECV_1_31 as StdCheck with uid="RYXOXHTCLX",rtseq=20,rtrep=.f.,left=27, top=133, caption="Codici Enti Previdenziali/Assicurativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 158315738,;
    cFormVar="w_SELECV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECV_1_31.RadioValue()
    return(iif(this.value =1,'CV',;
    ' '))
  endfunc
  func oSELECV_1_31.GetRadio()
    this.Parent.oContained.w_SELECV = this.RadioValue()
    return .t.
  endfunc

  func oSELECV_1_31.SetRadio()
    this.Parent.oContained.w_SELECV=trim(this.Parent.oContained.w_SELECV)
    this.value = ;
      iif(this.Parent.oContained.w_SELECV=='CV',1,;
      0)
  endfunc

  func oSELECV_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECV<>"|")
    endwith
   endif
  endfunc

  add object oSELENC_1_32 as StdCheck with uid="OYFCHPNVOZ",rtseq=21,rtrep=.f.,left=27, top=190, caption="Codici Enti/Comuni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 197113050,;
    cFormVar="w_SELENC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENC_1_32.RadioValue()
    return(iif(this.value =1,'NC',;
    ' '))
  endfunc
  func oSELENC_1_32.GetRadio()
    this.Parent.oContained.w_SELENC = this.RadioValue()
    return .t.
  endfunc

  func oSELENC_1_32.SetRadio()
    this.Parent.oContained.w_SELENC=trim(this.Parent.oContained.w_SELENC)
    this.value = ;
      iif(this.Parent.oContained.w_SELENC=='NC',1,;
      0)
  endfunc

  func oSELENC_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENC<>"|")
    endwith
   endif
  endfunc

  add object oSELESI_1_33 as StdCheck with uid="JLYKIZSRHZ",rtseq=22,rtrep=.f.,left=27, top=209, caption="Sedi INAIL",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 91206874,;
    cFormVar="w_SELESI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESI_1_33.RadioValue()
    return(iif(this.value =1,'SI',;
    ' '))
  endfunc
  func oSELESI_1_33.GetRadio()
    this.Parent.oContained.w_SELESI = this.RadioValue()
    return .t.
  endfunc

  func oSELESI_1_33.SetRadio()
    this.Parent.oContained.w_SELESI=trim(this.Parent.oContained.w_SELESI)
    this.value = ;
      iif(this.Parent.oContained.w_SELESI=='SI',1,;
      0)
  endfunc

  func oSELESI_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESI<>"|")
    endwith
   endif
  endfunc

  add object oSELESN_1_34 as StdCheck with uid="WHCJAYEIGK",rtseq=23,rtrep=.f.,left=27, top=228, caption="Sedi INPS",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7320794,;
    cFormVar="w_SELESN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESN_1_34.RadioValue()
    return(iif(this.value =1,'SN',;
    ' '))
  endfunc
  func oSELESN_1_34.GetRadio()
    this.Parent.oContained.w_SELESN = this.RadioValue()
    return .t.
  endfunc

  func oSELESN_1_34.SetRadio()
    this.Parent.oContained.w_SELESN=trim(this.Parent.oContained.w_SELESN)
    this.value = ;
      iif(this.Parent.oContained.w_SELESN=='SN',1,;
      0)
  endfunc

  func oSELESN_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESN<>"|")
    endwith
   endif
  endfunc


  add object oObj_1_35 as cp_runprogram with uid="SCCPNNHPBM",left=11, top=400, width=256,height=19,;
    caption='GSIM_BSE(0)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSE(0)",;
    cEvent = "w_CODIMP Changed,Blank",;
    nPag=1;
    , HelpContextID = 73067307

  add object oPATH_1_36 as StdField with uid="RQKMHXSFLN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 60569866,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=106, Top=38, InputMask=replicate('X',200)

  func oPATH_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC<>"O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oPATH_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oODBCDSN_1_37 as StdField with uid="SURNDHANMA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 207771162,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=107, Top=38, InputMask=replicate('X',30)

  func oODBCDSN_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCDSN_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPATH_1_39 as StdField with uid="QVJSXEZNRK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 39692846,;
   bGlobalFont=.t.,;
    Height=21, Width=193, Left=107, Top=61, InputMask=replicate('X',200)

  func oODBCPATH_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDBF='S')
    endwith
   endif
  endfunc

  func oODBCPATH_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc


  add object oBtn_1_40 as StdButton with uid="DEWQRVHFXZ",left=301, top=63, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDBF='S')
      endwith
    endif
  endfunc

  func oBtn_1_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDBF<>'S')
     endwith
    endif
  endfunc

  add object oODBCUSER_1_41 as StdField with uid="KTRJOCAYOA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente per connessione ODBC",;
    HelpContextID = 78490168,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=399, Top=38, InputMask=replicate('X',30)

  func oODBCUSER_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCUSER_1_41.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPASSW_1_42 as StdField with uid="EBTOHTBAVS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Password per connessione ODBC",;
    HelpContextID = 39694249,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=399, Top=61, InputMask=replicate('X',30), PasswordChar='*'

  func oODBCPASSW_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCPASSW_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc


  add object oBtn_1_43 as StdButton with uid="JXODESYAJC",left=585, top=38, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_43.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPSRC<>"O" and !.w_INCORSO)
      endwith
    endif
  endfunc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPSRC="O")
     endwith
    endif
  endfunc

  add object oSELEEL_1_48 as StdCheck with uid="AWGEMJRZCJ",rtseq=29,rtrep=.f.,left=261, top=114, caption="Codici Enti Locali",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 55555290,;
    cFormVar="w_SELEEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEEL_1_48.RadioValue()
    return(iif(this.value =1,'EL',;
    ' '))
  endfunc
  func oSELEEL_1_48.GetRadio()
    this.Parent.oContained.w_SELEEL = this.RadioValue()
    return .t.
  endfunc

  func oSELEEL_1_48.SetRadio()
    this.Parent.oContained.w_SELEEL=trim(this.Parent.oContained.w_SELEEL)
    this.value = ;
      iif(this.Parent.oContained.w_SELEEL=='EL',1,;
      0)
  endfunc

  func oSELEEL_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEEL<>"|")
    endwith
   endif
  endfunc

  add object oSELERG_1_49 as StdCheck with uid="VWQKDKOAEL",rtseq=30,rtrep=.f.,left=261, top=133, caption="Codici Regioni e prov. Autonome",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 125809882,;
    cFormVar="w_SELERG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERG_1_49.RadioValue()
    return(iif(this.value =1,'RG',;
    ' '))
  endfunc
  func oSELERG_1_49.GetRadio()
    this.Parent.oContained.w_SELERG = this.RadioValue()
    return .t.
  endfunc

  func oSELERG_1_49.SetRadio()
    this.Parent.oContained.w_SELERG=trim(this.Parent.oContained.w_SELERG)
    this.value = ;
      iif(this.Parent.oContained.w_SELERG=='RG',1,;
      0)
  endfunc

  func oSELERG_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERG<>"|")
    endwith
   endif
  endfunc

  add object oStr_1_5 as StdString with uid="HNFSNYQVVY",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tracciati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="TXLTEBBQSG",Visible=.t., Left=22, Top=283,;
    Alignment=0, Width=141, Height=15,;
    Caption="Verifiche e resoconti"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QUCHHTNQRF",Visible=.t., Left=18, Top=88,;
    Alignment=0, Width=214, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="SQXRTSXXPT",Visible=.t., Left=182, Top=283,;
    Alignment=0, Width=116, Height=15,;
    Caption="Opzioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="TQFTDCWTOP",Visible=.t., Left=55, Top=63,;
    Alignment=1, Width=50, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="BMACOVKPYD",Visible=.t., Left=25, Top=40,;
    Alignment=1, Width=78, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="TJHOZUKRMZ",Visible=.t., Left=15, Top=40,;
    Alignment=1, Width=88, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="UZMIHDSYKR",Visible=.t., Left=342, Top=40,;
    Alignment=1, Width=53, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="VCPUJPVADO",Visible=.t., Left=329, Top=64,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oBox_1_8 as StdBox with uid="LZRSZSRDKV",left=19, top=300, width=146,height=1

  add object oBox_1_10 as StdBox with uid="VNBLQXCGAX",left=18, top=107, width=487,height=159

  add object oBox_1_12 as StdBox with uid="UECDJXNURS",left=178, top=300, width=127,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kec','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
