* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_std                                                        *
*              Stampa trascodifiche                                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_9]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_std",oParentObject))

* --- Class definition
define class tgsim_std as StdForm
  Top    = 58
  Left   = 72

  * --- Standard Properties
  Width  = 619
  Height = 238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=74039145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  IMPOARCH_IDX = 0
  XDC_FIELDS_IDX = 0
  cPrg = "gsim_std"
  cComment = "Stampa trascodifiche"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPTRA = space(1)
  o_TIPTRA = space(1)
  w_CODIMP = space(20)
  o_CODIMP = space(20)
  w_DESIMP = space(50)
  w_CODIMP = space(20)
  w_ARCINI = space(2)
  w_ARDESINI = space(20)
  w_ARCFIN = space(2)
  w_ARDESFIN = space(20)
  w_CAMINI = space(15)
  w_CAMINI = space(15)
  w_CAMFIN = space(15)
  w_CAMFIN = space(15)
  w_ARTABINI = space(15)
  w_ARTABFIN = space(15)
  w_FLCOMINI = space(80)
  w_FLCOMFIN = space(80)
  w_DESIMP = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_stdPag1","gsim_std",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPTRA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='IMPOARCH'
    this.cWorkTables[4]='XDC_FIELDS'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec("..\IMPO\EXE\QUERY\GSIM_STD.VQR",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPTRA=space(1)
      .w_CODIMP=space(20)
      .w_DESIMP=space(50)
      .w_CODIMP=space(20)
      .w_ARCINI=space(2)
      .w_ARDESINI=space(20)
      .w_ARCFIN=space(2)
      .w_ARDESFIN=space(20)
      .w_CAMINI=space(15)
      .w_CAMINI=space(15)
      .w_CAMFIN=space(15)
      .w_CAMFIN=space(15)
      .w_ARTABINI=space(15)
      .w_ARTABFIN=space(15)
      .w_FLCOMINI=space(80)
      .w_FLCOMFIN=space(80)
      .w_DESIMP=space(50)
        .w_TIPTRA = 'G'
        .w_CODIMP = iif(.w_TIPTRA='G',"",.w_CODIMP)
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_CODIMP))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ARCINI))
          .link_1_7('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_ARCFIN))
          .link_1_10('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CAMINI))
          .link_1_14('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CAMINI))
          .link_1_15('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CAMFIN))
          .link_1_16('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CAMFIN))
          .link_1_17('Full')
        endif
          .DoRTCalc(13,16,.f.)
        .w_DESIMP = iif(.w_TIPTRA='G',"",.w_DESIMP)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPTRA<>.w_TIPTRA.or. .o_CODIMP<>.w_CODIMP
            .w_CODIMP = iif(.w_TIPTRA='G',"",.w_CODIMP)
        endif
        .DoRTCalc(3,16,.t.)
        if .o_TIPTRA<>.w_TIPTRA.or. .o_CODIMP<>.w_CODIMP
            .w_DESIMP = iif(.w_TIPTRA='G',"",.w_DESIMP)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODIMP_1_4.enabled = this.oPgFrm.Page1.oPag.oCODIMP_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCAMINI_1_14.enabled = this.oPgFrm.Page1.oPag.oCAMINI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCAMINI_1_15.enabled = this.oPgFrm.Page1.oPag.oCAMINI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCAMFIN_1_16.enabled = this.oPgFrm.Page1.oPag.oCAMFIN_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCAMFIN_1_17.enabled = this.oPgFrm.Page1.oPag.oCAMFIN_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAMINI_1_14.visible=!this.oPgFrm.Page1.oPag.oCAMINI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCAMINI_1_15.visible=!this.oPgFrm.Page1.oPag.oCAMINI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCAMFIN_1_16.visible=!this.oPgFrm.Page1.oPag.oCAMFIN_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCAMFIN_1_17.visible=!this.oPgFrm.Page1.oPag.oCAMFIN_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_4'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_ARCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_ARCINI))
          select ARCODICE,ARDESCRI,ARTABELL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCINI)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCINI) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oARCINI_1_7'),i_cWhere,'GSIM_AAR',"Archivi di destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_ARCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_ARCINI)
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCINI = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESINI = NVL(_Link_.ARDESCRI,space(20))
      this.w_ARTABINI = NVL(_Link_.ARTABELL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ARCINI = space(2)
      endif
      this.w_ARDESINI = space(20)
      this.w_ARTABINI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_ARCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_ARCFIN))
          select ARCODICE,ARDESCRI,ARTABELL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCFIN)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCFIN) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oARCFIN_1_10'),i_cWhere,'GSIM_AAR',"Archivi di destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_ARCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_ARCFIN)
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCFIN = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESFIN = NVL(_Link_.ARDESCRI,space(20))
      this.w_ARTABFIN = NVL(_Link_.ARTABELL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ARCFIN = space(2)
      endif
      this.w_ARDESFIN = space(20)
      this.w_ARTABFIN = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_CAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNAME',trim(this.w_CAMINI))
          select FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMINI)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMINI) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','FLNAME',cp_AbsName(oSource.parent,'oCAMINI_1_14'),i_cWhere,'',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',oSource.xKey(1))
            select FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_CAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',this.w_CAMINI)
            select FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMINI = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMINI = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CAMINI = space(15)
      endif
      this.w_FLCOMINI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMINI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_CAMINI)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ARTABINI;
                     ,'FLNAME',trim(this.w_CAMINI))
          select TBNAME,FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMINI)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMINI) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oCAMINI_1_15'),i_cWhere,'',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTABINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_CAMINI);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABINI;
                       ,'FLNAME',this.w_CAMINI)
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMINI = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMINI = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CAMINI = space(15)
      endif
      this.w_FLCOMINI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMFIN
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_CAMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNAME',trim(this.w_CAMFIN))
          select FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMFIN)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMFIN) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','FLNAME',cp_AbsName(oSource.parent,'oCAMFIN_1_16'),i_cWhere,'',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',oSource.xKey(1))
            select FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_CAMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',this.w_CAMFIN)
            select FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMFIN = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMFIN = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CAMFIN = space(15)
      endif
      this.w_FLCOMFIN = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.not. .w_CAMINI > .w_CAMFIN) .or. empty(.w_CAMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAMFIN = space(15)
        this.w_FLCOMFIN = space(80)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_CAMFIN)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ARTABINI;
                     ,'FLNAME',trim(this.w_CAMFIN))
          select TBNAME,FLNAME,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMFIN)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMFIN) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oCAMFIN_1_17'),i_cWhere,'',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTABINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_CAMFIN);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABINI;
                       ,'FLNAME',this.w_CAMFIN)
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMFIN = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMFIN = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CAMFIN = space(15)
      endif
      this.w_FLCOMFIN = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.not. .w_CAMINI > .w_CAMFIN) .or. empty(.w_CAMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAMFIN = space(15)
        this.w_FLCOMFIN = space(80)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPTRA_1_1.RadioValue()==this.w_TIPTRA)
      this.oPgFrm.Page1.oPag.oTIPTRA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_3.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_3.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_4.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_4.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oARCINI_1_7.value==this.w_ARCINI)
      this.oPgFrm.Page1.oPag.oARCINI_1_7.value=this.w_ARCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESINI_1_8.value==this.w_ARDESINI)
      this.oPgFrm.Page1.oPag.oARDESINI_1_8.value=this.w_ARDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARCFIN_1_10.value==this.w_ARCFIN)
      this.oPgFrm.Page1.oPag.oARCFIN_1_10.value=this.w_ARCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESFIN_1_11.value==this.w_ARDESFIN)
      this.oPgFrm.Page1.oPag.oARDESFIN_1_11.value=this.w_ARDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMINI_1_14.value==this.w_CAMINI)
      this.oPgFrm.Page1.oPag.oCAMINI_1_14.value=this.w_CAMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMINI_1_15.value==this.w_CAMINI)
      this.oPgFrm.Page1.oPag.oCAMINI_1_15.value=this.w_CAMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMFIN_1_16.value==this.w_CAMFIN)
      this.oPgFrm.Page1.oPag.oCAMFIN_1_16.value=this.w_CAMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMFIN_1_17.value==this.w_CAMFIN)
      this.oPgFrm.Page1.oPag.oCAMFIN_1_17.value=this.w_CAMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABINI_1_20.value==this.w_ARTABINI)
      this.oPgFrm.Page1.oPag.oARTABINI_1_20.value=this.w_ARTABINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABFIN_1_21.value==this.w_ARTABFIN)
      this.oPgFrm.Page1.oPag.oARTABFIN_1_21.value=this.w_ARTABFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMINI_1_23.value==this.w_FLCOMINI)
      this.oPgFrm.Page1.oPag.oFLCOMINI_1_23.value=this.w_FLCOMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMFIN_1_24.value==this.w_FLCOMFIN)
      this.oPgFrm.Page1.oPag.oFLCOMFIN_1_24.value=this.w_FLCOMFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.not. .w_CAMINI > .w_CAMFIN) .or. empty(.w_CAMFIN))  and not(.w_ARTABINI=.w_ARTABFIN)  and (.w_ARTABINI<>.w_ARTABFIN)  and not(empty(.w_CAMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.not. .w_CAMINI > .w_CAMFIN) .or. empty(.w_CAMFIN))  and not(.w_ARTABINI<>.w_ARTABFIN)  and (.w_ARTABINI=.w_ARTABFIN)  and not(empty(.w_CAMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPTRA = this.w_TIPTRA
    this.o_CODIMP = this.w_CODIMP
    return

enddefine

* --- Define pages as container
define class tgsim_stdPag1 as StdContainer
  Width  = 615
  height = 238
  stdWidth  = 615
  stdheight = 238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPTRA_1_1 as StdRadio with uid="FPYLGUTSTB",rtseq=1,rtrep=.f.,left=99, top=11, width=447,height=19;
    , cFormVar="w_TIPTRA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPTRA_1_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutti"
      this.Buttons(1).HelpContextID = 34574134
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Singolo tracciato di importazione"
      this.Buttons(2).HelpContextID = 34574134
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Singolo tracciato di importazione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",19)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPTRA_1_1.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPTRA_1_1.GetRadio()
    this.Parent.oContained.w_TIPTRA = this.RadioValue()
    return .t.
  endfunc

  func oTIPTRA_1_1.SetRadio()
    this.Parent.oContained.w_TIPTRA=trim(this.Parent.oContained.w_TIPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPTRA=='G',1,;
      iif(this.Parent.oContained.w_TIPTRA=='S',2,;
      0))
  endfunc

  add object oDESIMP_1_3 as StdField with uid="IFQBYLSHHJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 11844150,;
   bGlobalFont=.t.,;
    Height=21, Width=348, Left=260, Top=38, InputMask=replicate('X',50)

  add object oCODIMP_1_4 as StdField with uid="YSSOCMPOJS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Specificare il codice dei tracciati.",;
    HelpContextID = 11785254,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=96, Top=38, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPTRA='S')
    endwith
   endif
  endfunc

  func oCODIMP_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_4.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oARCINI_1_7 as StdField with uid="WMYUVOYDTO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARCINI", cQueryName = "ARCINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice archivio di inizio selezione",;
    HelpContextID = 163825414,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=96, Top=74, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_ARCINI"

  func oARCINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oARCINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"Archivi di destinazione",'',this.parent.oContained
  endproc
  proc oARCINI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_ARCINI
     i_obj.ecpSave()
  endproc

  add object oARDESINI_1_8 as StdField with uid="DMVDLEFXPB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARDESINI", cQueryName = "ARDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 99625137,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=139, Top=74, InputMask=replicate('X',20)

  add object oARCFIN_1_10 as StdField with uid="EQFBIJGPMO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ARCFIN", cQueryName = "ARCFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice archivio di fine selezione",;
    HelpContextID = 242272006,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=96, Top=98, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_ARCFIN"

  func oARCFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oARCFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"Archivi di destinazione",'',this.parent.oContained
  endproc
  proc oARCFIN_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_ARCFIN
     i_obj.ecpSave()
  endproc

  add object oARDESFIN_1_11 as StdField with uid="QIZKFBJQPR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ARDESFIN", cQueryName = "ARDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 149956780,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=139, Top=98, InputMask=replicate('X',20)

  add object oCAMINI_1_14 as StdField with uid="KMOTBXZISH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CAMINI", cQueryName = "CAMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Campo di inizio selezione",;
    HelpContextID = 163862054,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=96, Top=137, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="FLNAME", oKey_1_2="this.w_CAMINI"

  func oCAMINI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTABINI<>.w_ARTABFIN)
    endwith
   endif
  endfunc

  func oCAMINI_1_14.mHide()
    with this.Parent.oContained
      return (.w_ARTABINI=.w_ARTABFIN)
    endwith
  endfunc

  func oCAMINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAMINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_FIELDS','*','FLNAME',cp_AbsName(this.parent,'oCAMINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dizionario dati",'',this.parent.oContained
  endproc

  add object oCAMINI_1_15 as StdField with uid="YIVDGZJPUR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAMINI", cQueryName = "CAMINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Campo di inizio selezione",;
    HelpContextID = 163862054,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=96, Top=137, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_ARTABINI", oKey_2_1="FLNAME", oKey_2_2="this.w_CAMINI"

  func oCAMINI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTABINI=.w_ARTABFIN)
    endwith
   endif
  endfunc

  func oCAMINI_1_15.mHide()
    with this.Parent.oContained
      return (.w_ARTABINI<>.w_ARTABFIN)
    endwith
  endfunc

  func oCAMINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMINI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAMINI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ARTABINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ARTABINI)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oCAMINI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dizionario dati",'',this.parent.oContained
  endproc

  add object oCAMFIN_1_16 as StdField with uid="DQYFKVWWXH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CAMFIN", cQueryName = "CAMFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Campo di fine selezione",;
    HelpContextID = 242308646,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=96, Top=160, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="FLNAME", oKey_1_2="this.w_CAMFIN"

  func oCAMFIN_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTABINI<>.w_ARTABFIN)
    endwith
   endif
  endfunc

  func oCAMFIN_1_16.mHide()
    with this.Parent.oContained
      return (.w_ARTABINI=.w_ARTABFIN)
    endwith
  endfunc

  func oCAMFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMFIN_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAMFIN_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_FIELDS','*','FLNAME',cp_AbsName(this.parent,'oCAMFIN_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dizionario dati",'',this.parent.oContained
  endproc

  add object oCAMFIN_1_17 as StdField with uid="JLFCKFEDYT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAMFIN", cQueryName = "CAMFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Campo di fine selezione",;
    HelpContextID = 242308646,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=96, Top=160, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_ARTABINI", oKey_2_1="FLNAME", oKey_2_2="this.w_CAMFIN"

  func oCAMFIN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTABINI=.w_ARTABFIN)
    endwith
   endif
  endfunc

  func oCAMFIN_1_17.mHide()
    with this.Parent.oContained
      return (.w_ARTABINI<>.w_ARTABFIN)
    endwith
  endfunc

  func oCAMFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAMFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ARTABINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ARTABINI)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oCAMFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dizionario dati",'',this.parent.oContained
  endproc


  add object oBtn_1_18 as StdButton with uid="VQERAGEWET",left=504, top=187, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 74010394;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      vx_exec("..\IMPO\EXE\QUERY\GSIM_STD.VQR",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="MRPTSAHHIT",left=560, top=187, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 66721722;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oARTABINI_1_20 as StdField with uid="WVTAUNWUXJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ARTABINI", cQueryName = "ARTABINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 117647537,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=320, Top=74, InputMask=replicate('X',15)

  func oARTABINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CAMINI)
        bRes2=.link_1_15('Full')
      endif
      if .not. empty(.w_CAMFIN)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oARTABFIN_1_21 as StdField with uid="UOWTYGQGER",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ARTABFIN", cQueryName = "ARTABFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 167979180,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=320, Top=98, InputMask=replicate('X',15)

  add object oFLCOMINI_1_23 as StdField with uid="XKXVNSIXPG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FLCOMINI", cQueryName = "FLCOMINI",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 105266785,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=248, Top=136, InputMask=replicate('X',80)

  add object oFLCOMFIN_1_24 as StdField with uid="ZXGCGOLJCD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FLCOMFIN", cQueryName = "FLCOMFIN",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 155598428,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=248, Top=160, InputMask=replicate('X',80)

  add object oStr_1_5 as StdString with uid="QSEFFZATMY",Visible=.t., Left=9, Top=41,;
    Alignment=1, Width=84, Height=15,;
    Caption="Tracciati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="IQWPTKUGEX",Visible=.t., Left=9, Top=75,;
    Alignment=1, Width=84, Height=15,;
    Caption="Da archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="QNLCBQVBYM",Visible=.t., Left=9, Top=102,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ad archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="XRBLLVCOEW",Visible=.t., Left=9, Top=139,;
    Alignment=1, Width=84, Height=15,;
    Caption="Da campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BQXZAMLGXM",Visible=.t., Left=9, Top=163,;
    Alignment=1, Width=84, Height=15,;
    Caption="A campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="UGBUGMVEZJ",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=90, Height=18,;
    Caption="Applicate a:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_std','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
