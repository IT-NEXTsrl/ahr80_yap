* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kex                                                        *
*              Import/export tabelle import dati                               *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_73]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kex",oParentObject))

* --- Class definition
define class tgsim_kex as StdForm
  Top    = 24
  Left   = 75

  * --- Standard Properties
  Width  = 584
  Height = 341+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=65650537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  cPrg = "gsim_kex"
  cComment = "Import/export tabelle import dati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RADSELIE1 = space(10)
  w_SELETR1 = space(10)
  w_DBF1 = space(200)
  w_SELESOR = space(10)
  w_DBF5 = space(200)
  w_DBF51 = space(200)
  w_DBF11 = space(200)
  w_SELEAD1 = space(10)
  w_DBF2 = space(200)
  w_SELEVP1 = space(10)
  w_DBF3 = space(200)
  w_DBF31 = space(200)
  w_SELETS1 = space(10)
  w_DBF4 = space(200)
  w_RADSELEZ1 = space(10)
  w_RADSELIE = space(10)
  w_SELETR = space(10)
  w_ASCII1 = space(200)
  w_SELEAD = space(10)
  w_ASCII2 = space(200)
  w_SELEVP = space(10)
  w_ASCII3 = space(200)
  w_SELETS = space(10)
  w_ASCII4 = space(200)
  w_RADSELEZ = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kexPag1","gsim_kex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Formato DBF")
      .Pages(2).addobject("oPag","tgsim_kexPag2","gsim_kex",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Formato ASCII")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRADSELIE1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RADSELIE1=space(10)
      .w_SELETR1=space(10)
      .w_DBF1=space(200)
      .w_SELESOR=space(10)
      .w_DBF5=space(200)
      .w_DBF51=space(200)
      .w_DBF11=space(200)
      .w_SELEAD1=space(10)
      .w_DBF2=space(200)
      .w_SELEVP1=space(10)
      .w_DBF3=space(200)
      .w_DBF31=space(200)
      .w_SELETS1=space(10)
      .w_DBF4=space(200)
      .w_RADSELEZ1=space(10)
      .w_RADSELIE=space(10)
      .w_SELETR=space(10)
      .w_ASCII1=space(200)
      .w_SELEAD=space(10)
      .w_ASCII2=space(200)
      .w_SELEVP=space(10)
      .w_ASCII3=space(200)
      .w_SELETS=space(10)
      .w_ASCII4=space(200)
      .w_RADSELEZ=space(10)
        .w_RADSELIE1 = ' '
        .w_SELETR1 = 'TR'
        .w_DBF1 = "..\IMPO\EXE\IMPORTAZ.DBF"
        .w_SELESOR = 'SO'
        .w_DBF5 = "..\IMPO\EXE\SRC_ODBC.DBF"
        .w_DBF51 = "..\IMPO\EXE\SRCMODBC.DBF"
        .w_DBF11 = "..\IMPO\EXE\TRACCIAT.DBF"
        .w_SELEAD1 = 'AD'
        .w_DBF2 = "..\IMPO\EXE\IMPOARCH.DBF"
        .w_SELEVP1 = 'VP'
        .w_DBF3 = "..\IMPO\EXE\PREDEFIN.DBF"
        .w_DBF31 = "..\IMPO\EXE\PREDEIMP.DBF"
        .w_SELETS1 = 'TS'
        .w_DBF4 = "..\IMPO\EXE\TRASCODI.DBF"
        .w_RADSELEZ1 = 'S'
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_RADSELIE = ' '
        .w_SELETR = 'TR'
        .w_ASCII1 = "..\IMPO\EXE\IMPORTAZ.TXT"
        .w_SELEAD = 'AD'
        .w_ASCII2 = "..\IMPO\EXE\IMPOARCH.TXT"
        .w_SELEVP = 'VP'
        .w_ASCII3 = "..\IMPO\EXE\PREDEFIN.TXT"
        .w_SELETS = 'TS'
        .w_ASCII4 = "..\IMPO\EXE\TRASCODI.TXT"
        .w_RADSELEZ = 'S'
      .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_20.enabled = this.oPgFrm.Page2.oPag.oBtn_2_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.RadioValue()==this.w_RADSELIE1)
      this.oPgFrm.Page1.oPag.oRADSELIE1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETR1_1_2.RadioValue()==this.w_SELETR1)
      this.oPgFrm.Page1.oPag.oSELETR1_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF1_1_3.value==this.w_DBF1)
      this.oPgFrm.Page1.oPag.oDBF1_1_3.value=this.w_DBF1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESOR_1_5.RadioValue()==this.w_SELESOR)
      this.oPgFrm.Page1.oPag.oSELESOR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF5_1_6.value==this.w_DBF5)
      this.oPgFrm.Page1.oPag.oDBF5_1_6.value=this.w_DBF5
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF51_1_8.value==this.w_DBF51)
      this.oPgFrm.Page1.oPag.oDBF51_1_8.value=this.w_DBF51
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF11_1_10.value==this.w_DBF11)
      this.oPgFrm.Page1.oPag.oDBF11_1_10.value=this.w_DBF11
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAD1_1_12.RadioValue()==this.w_SELEAD1)
      this.oPgFrm.Page1.oPag.oSELEAD1_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF2_1_13.value==this.w_DBF2)
      this.oPgFrm.Page1.oPag.oDBF2_1_13.value=this.w_DBF2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEVP1_1_15.RadioValue()==this.w_SELEVP1)
      this.oPgFrm.Page1.oPag.oSELEVP1_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF3_1_16.value==this.w_DBF3)
      this.oPgFrm.Page1.oPag.oDBF3_1_16.value=this.w_DBF3
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF31_1_18.value==this.w_DBF31)
      this.oPgFrm.Page1.oPag.oDBF31_1_18.value=this.w_DBF31
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETS1_1_20.RadioValue()==this.w_SELETS1)
      this.oPgFrm.Page1.oPag.oSELETS1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF4_1_21.value==this.w_DBF4)
      this.oPgFrm.Page1.oPag.oDBF4_1_21.value=this.w_DBF4
    endif
    if not(this.oPgFrm.Page1.oPag.oRADSELEZ1_1_23.RadioValue()==this.w_RADSELEZ1)
      this.oPgFrm.Page1.oPag.oRADSELEZ1_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRADSELIE_2_2.RadioValue()==this.w_RADSELIE)
      this.oPgFrm.Page2.oPag.oRADSELIE_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELETR_2_3.RadioValue()==this.w_SELETR)
      this.oPgFrm.Page2.oPag.oSELETR_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASCII1_2_4.value==this.w_ASCII1)
      this.oPgFrm.Page2.oPag.oASCII1_2_4.value=this.w_ASCII1
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEAD_2_6.RadioValue()==this.w_SELEAD)
      this.oPgFrm.Page2.oPag.oSELEAD_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASCII2_2_7.value==this.w_ASCII2)
      this.oPgFrm.Page2.oPag.oASCII2_2_7.value=this.w_ASCII2
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEVP_2_9.RadioValue()==this.w_SELEVP)
      this.oPgFrm.Page2.oPag.oSELEVP_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASCII3_2_10.value==this.w_ASCII3)
      this.oPgFrm.Page2.oPag.oASCII3_2_10.value=this.w_ASCII3
    endif
    if not(this.oPgFrm.Page2.oPag.oSELETS_2_12.RadioValue()==this.w_SELETS)
      this.oPgFrm.Page2.oPag.oSELETS_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASCII4_2_13.value==this.w_ASCII4)
      this.oPgFrm.Page2.oPag.oASCII4_2_13.value=this.w_ASCII4
    endif
    if not(this.oPgFrm.Page2.oPag.oRADSELEZ_2_15.RadioValue()==this.w_RADSELEZ)
      this.oPgFrm.Page2.oPag.oRADSELEZ_2_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBF1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF1_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DBF1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF5_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DBF5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF51))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF51_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DBF51)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF11))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF11_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DBF11)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF2_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DBF2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF3_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DBF3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF31))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF31_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DBF31)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF4_1_21.SetFocus()
            i_bnoObbl = !empty(.w_DBF4)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsim_kexPag1 as StdContainer
  Width  = 580
  height = 341
  stdWidth  = 580
  stdheight = 341
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRADSELIE1_1_1 as StdRadio with uid="OKXXQIMNCR",rtseq=1,rtrep=.f.,left=147, top=19, width=415,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELIE1_1_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file DBF"
      this.Buttons(1).HelpContextID = 54670741
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file DBF"
      this.Buttons(2).HelpContextID = 54670741
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE1_1_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(10))))
  endfunc
  func oRADSELIE1_1_1.GetRadio()
    this.Parent.oContained.w_RADSELIE1 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE1_1_1.SetRadio()
    this.Parent.oContained.w_RADSELIE1=trim(this.Parent.oContained.w_RADSELIE1)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE1=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE1=='E',2,;
      0))
  endfunc

  add object oSELETR1_1_2 as StdCheck with uid="AOLULFBESB",rtseq=2,rtrep=.f.,left=19, top=78, caption="Tracciati importazione",;
    ToolTipText = "Se attivato importa gli archivi dei tracciati di importazione",;
    HelpContextID = 60836646,;
    cFormVar="w_SELETR1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETR1_1_2.RadioValue()
    return(iif(this.value =1,'TR',;
    ' '))
  endfunc
  func oSELETR1_1_2.GetRadio()
    this.Parent.oContained.w_SELETR1 = this.RadioValue()
    return .t.
  endfunc

  func oSELETR1_1_2.SetRadio()
    this.Parent.oContained.w_SELETR1=trim(this.Parent.oContained.w_SELETR1)
    this.value = ;
      iif(this.Parent.oContained.w_SELETR1=='TR',1,;
      0)
  endfunc

  add object oDBF1_1_3 as StdField with uid="ZPJIIFTPAA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DBF1", cQueryName = "DBF1",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62134474,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=79, InputMask=replicate('X',200)


  add object oBtn_1_4 as StdButton with uid="MKUMAQUBOB",left=551, top=82, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .w_DBF1=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELESOR_1_5 as StdCheck with uid="BJUUDCEUQA",rtseq=4,rtrep=.f.,left=19, top=127, caption="Sorgenti dati ODBC",;
    ToolTipText = "Se attivato importa gli archivi delle sorgenti dati",;
    HelpContextID = 9456422,;
    cFormVar="w_SELESOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESOR_1_5.RadioValue()
    return(iif(this.value =1,'SO',;
    ' '))
  endfunc
  func oSELESOR_1_5.GetRadio()
    this.Parent.oContained.w_SELESOR = this.RadioValue()
    return .t.
  endfunc

  func oSELESOR_1_5.SetRadio()
    this.Parent.oContained.w_SELESOR=trim(this.Parent.oContained.w_SELESOR)
    this.value = ;
      iif(this.Parent.oContained.w_SELESOR=='SO',1,;
      0)
  endfunc

  add object oDBF5_1_6 as StdField with uid="YWLNADVOMD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBF5", cQueryName = "DBF5",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 61872330,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=128, InputMask=replicate('X',200)


  add object oBtn_1_7 as StdButton with uid="FAWOVLEUNZ",left=551, top=131, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .w_DBF5=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF51_1_8 as StdField with uid="PJCBNWHHVN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DBF51", cQueryName = "DBF51",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 10492106,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=150, InputMask=replicate('X',200)


  add object oBtn_1_9 as StdButton with uid="CMAAGYNRCN",left=551, top=153, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .w_DBF51=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF11_1_10 as StdField with uid="CZIBBZGIKR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DBF11", cQueryName = "DBF11",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 10754250,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=101, InputMask=replicate('X',200)


  add object oBtn_1_11 as StdButton with uid="GJSGDJJLXS",left=551, top=104, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_DBF11=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEAD1_1_12 as StdCheck with uid="XRFZASRLRC",rtseq=8,rtrep=.f.,left=19, top=176, caption="Archivi di destinazione",;
    ToolTipText = "Se attivato importa gli archivi di destinazione",;
    HelpContextID = 74468134,;
    cFormVar="w_SELEAD1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAD1_1_12.RadioValue()
    return(iif(this.value =1,'AD',;
    ' '))
  endfunc
  func oSELEAD1_1_12.GetRadio()
    this.Parent.oContained.w_SELEAD1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEAD1_1_12.SetRadio()
    this.Parent.oContained.w_SELEAD1=trim(this.Parent.oContained.w_SELEAD1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAD1=='AD',1,;
      0)
  endfunc

  add object oDBF2_1_13 as StdField with uid="RLYZERYPVU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DBF2", cQueryName = "DBF2",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62068938,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=177, InputMask=replicate('X',200)


  add object oBtn_1_14 as StdButton with uid="OAPUGTGTUT",left=551, top=180, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .w_DBF2=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEVP1_1_15 as StdCheck with uid="PXMYNZXMED",rtseq=10,rtrep=.f.,left=19, top=204, caption="Valori predefiniti",;
    ToolTipText = "Se attivato importa gli archivi dei valori predefiniti",;
    HelpContextID = 29379366,;
    cFormVar="w_SELEVP1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEVP1_1_15.RadioValue()
    return(iif(this.value =1,'VP',;
    ' '))
  endfunc
  func oSELEVP1_1_15.GetRadio()
    this.Parent.oContained.w_SELEVP1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEVP1_1_15.SetRadio()
    this.Parent.oContained.w_SELEVP1=trim(this.Parent.oContained.w_SELEVP1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVP1=='VP',1,;
      0)
  endfunc

  add object oDBF3_1_16 as StdField with uid="WYAZJRQMFB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DBF3", cQueryName = "DBF3",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62003402,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=205, InputMask=replicate('X',200)


  add object oBtn_1_17 as StdButton with uid="FUNDBAHXGT",left=551, top=208, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .w_DBF3=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF31_1_18 as StdField with uid="LNWHHHPQSU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DBF31", cQueryName = "DBF31",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 10623178,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=227, InputMask=replicate('X',200)


  add object oBtn_1_19 as StdButton with uid="RTVOQXNGQN",left=551, top=230, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .w_DBF31=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELETS1_1_20 as StdCheck with uid="KUXNODWFOT",rtseq=13,rtrep=.f.,left=19, top=253, caption="Trascodifiche",;
    ToolTipText = "Se attivato importa gli archivi delle trascodifiche",;
    HelpContextID = 77613862,;
    cFormVar="w_SELETS1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETS1_1_20.RadioValue()
    return(iif(this.value =1,'TS',;
    ' '))
  endfunc
  func oSELETS1_1_20.GetRadio()
    this.Parent.oContained.w_SELETS1 = this.RadioValue()
    return .t.
  endfunc

  func oSELETS1_1_20.SetRadio()
    this.Parent.oContained.w_SELETS1=trim(this.Parent.oContained.w_SELETS1)
    this.value = ;
      iif(this.Parent.oContained.w_SELETS1=='TS',1,;
      0)
  endfunc

  add object oDBF4_1_21 as StdField with uid="VFKTBKAVMH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DBF4", cQueryName = "DBF4",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 61937866,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=183, Top=254, InputMask=replicate('X',200)


  add object oBtn_1_22 as StdButton with uid="TGSZDFHPXN",left=551, top=257, width=16,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_DBF4=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRADSELEZ1_1_23 as StdRadio with uid="QTINIDGXVL",rtseq=15,rtrep=.f.,left=25, top=304, width=169,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELEZ1_1_23.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 54670720
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 54670720
      this.Buttons(2).Top=15
      this.SetAll("Width",167)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ1_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ1_1_23.GetRadio()
    this.Parent.oContained.w_RADSELEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ1_1_23.SetRadio()
    this.Parent.oContained.w_RADSELEZ1=trim(this.Parent.oContained.w_RADSELEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ1=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ1=='D',2,;
      0))
  endfunc


  add object oBtn_1_27 as StdButton with uid="CZRHGAVNYF",left=467, top=294, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65621786;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        do GSIM_BED with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE1))
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="NYQDDFCUPJ",left=523, top=294, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 58333114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_29 as cp_runprogram with uid="GZXXIOZPIU",left=8, top=364, width=160,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSIM_BSD",;
    cEvent = "w_RADSELEZ1 Changed",;
    nPag=1;
    , HelpContextID = 112347110

  add object oStr_1_24 as StdString with uid="HKXMTIVULE",Visible=.t., Left=12, Top=38,;
    Alignment=0, Width=541, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OJVLVUPTEC",Visible=.t., Left=183, Top=62,;
    Alignment=0, Width=49, Height=15,;
    Caption="PATH"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="RAIJTTRZDL",left=11, top=57, width=565,height=231
enddefine
define class tgsim_kexPag2 as StdContainer
  Width  = 580
  height = 341
  stdWidth  = 580
  stdheight = 341
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRADSELIE_2_2 as StdRadio with uid="KFOVFHMLYO",rtseq=16,rtrep=.f.,left=147, top=19, width=423,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oRADSELIE_2_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file ASCII"
      this.Buttons(1).HelpContextID = 54671525
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file ASCII","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file ASCII"
      this.Buttons(2).HelpContextID = 54671525
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file ASCII","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE_2_2.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(10))))
  endfunc
  func oRADSELIE_2_2.GetRadio()
    this.Parent.oContained.w_RADSELIE = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE_2_2.SetRadio()
    this.Parent.oContained.w_RADSELIE=trim(this.Parent.oContained.w_RADSELIE)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE=='E',2,;
      0))
  endfunc

  add object oSELETR_2_3 as StdCheck with uid="VWOCSWLPES",rtseq=17,rtrep=.f.,left=16, top=82, caption="Tracciati importazione",;
    ToolTipText = "Se attivato importa gli archivi dei tracciati di importazione",;
    HelpContextID = 60836646,;
    cFormVar="w_SELETR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELETR_2_3.RadioValue()
    return(iif(this.value =1,'TR',;
    ' '))
  endfunc
  func oSELETR_2_3.GetRadio()
    this.Parent.oContained.w_SELETR = this.RadioValue()
    return .t.
  endfunc

  func oSELETR_2_3.SetRadio()
    this.Parent.oContained.w_SELETR=trim(this.Parent.oContained.w_SELETR)
    this.value = ;
      iif(this.Parent.oContained.w_SELETR=='TR',1,;
      0)
  endfunc

  add object oASCII1_2_4 as StdField with uid="WYZAMSKWZI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ASCII1", cQueryName = "ASCII1",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 32753670,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=177, Top=82, InputMask=replicate('X',200)


  add object oBtn_2_5 as StdButton with uid="EJDZHIPAVA",left=545, top=82, width=16,height=17,;
    caption="...", nPag=2;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_2_5.Click()
      with this.Parent.oContained
        .w_ASCII1=left(getfile()+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEAD_2_6 as StdCheck with uid="MTWVEAKTOY",rtseq=19,rtrep=.f.,left=16, top=101, caption="Archivi di destinazione",;
    ToolTipText = "Se attivato importa gli archivi di destinazione",;
    HelpContextID = 74468134,;
    cFormVar="w_SELEAD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELEAD_2_6.RadioValue()
    return(iif(this.value =1,'AD',;
    ' '))
  endfunc
  func oSELEAD_2_6.GetRadio()
    this.Parent.oContained.w_SELEAD = this.RadioValue()
    return .t.
  endfunc

  func oSELEAD_2_6.SetRadio()
    this.Parent.oContained.w_SELEAD=trim(this.Parent.oContained.w_SELEAD)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAD=='AD',1,;
      0)
  endfunc

  add object oASCII2_2_7 as StdField with uid="QWVYLWJNXP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ASCII2", cQueryName = "ASCII2",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 49530886,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=177, Top=101, InputMask=replicate('X',200)


  add object oBtn_2_8 as StdButton with uid="TGMYELNHOT",left=545, top=101, width=16,height=17,;
    caption="...", nPag=2;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_2_8.Click()
      with this.Parent.oContained
        .w_ASCII2=left(getfile()+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEVP_2_9 as StdCheck with uid="LOAFQLOTDC",rtseq=21,rtrep=.f.,left=16, top=120, caption="Valori predefiniti",;
    ToolTipText = "Se attivato importa gli archivi dei valori predefiniti",;
    HelpContextID = 29379366,;
    cFormVar="w_SELEVP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELEVP_2_9.RadioValue()
    return(iif(this.value =1,'VP',;
    ' '))
  endfunc
  func oSELEVP_2_9.GetRadio()
    this.Parent.oContained.w_SELEVP = this.RadioValue()
    return .t.
  endfunc

  func oSELEVP_2_9.SetRadio()
    this.Parent.oContained.w_SELEVP=trim(this.Parent.oContained.w_SELEVP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVP=='VP',1,;
      0)
  endfunc

  add object oASCII3_2_10 as StdField with uid="QFCMRDFISV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ASCII3", cQueryName = "ASCII3",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 66308102,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=177, Top=120, InputMask=replicate('X',200)


  add object oBtn_2_11 as StdButton with uid="GHGAVJHAWK",left=545, top=120, width=16,height=17,;
    caption="...", nPag=2;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_2_11.Click()
      with this.Parent.oContained
        .w_ASCII3=left(getfile()+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELETS_2_12 as StdCheck with uid="XBQDTQUJHE",rtseq=23,rtrep=.f.,left=16, top=139, caption="Trascodifiche",;
    ToolTipText = "Se attivato importa gli archivi delle trascodifiche",;
    HelpContextID = 77613862,;
    cFormVar="w_SELETS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSELETS_2_12.RadioValue()
    return(iif(this.value =1,'TS',;
    ' '))
  endfunc
  func oSELETS_2_12.GetRadio()
    this.Parent.oContained.w_SELETS = this.RadioValue()
    return .t.
  endfunc

  func oSELETS_2_12.SetRadio()
    this.Parent.oContained.w_SELETS=trim(this.Parent.oContained.w_SELETS)
    this.value = ;
      iif(this.Parent.oContained.w_SELETS=='TS',1,;
      0)
  endfunc

  add object oASCII4_2_13 as StdField with uid="OXKDOUJUCL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ASCII4", cQueryName = "ASCII4",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 83085318,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=177, Top=139, InputMask=replicate('X',200)


  add object oBtn_2_14 as StdButton with uid="TXXJNQCGXI",left=545, top=139, width=16,height=17,;
    caption="...", nPag=2;
    , HelpContextID = 65449514;
  , bGlobalFont=.t.

    proc oBtn_2_14.Click()
      with this.Parent.oContained
        .w_ASCII4=left(getfile()+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRADSELEZ_2_15 as StdRadio with uid="JGULZFSQUE",rtseq=25,rtrep=.f.,left=25, top=304, width=289,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oRADSELEZ_2_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 54671504
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 54671504
      this.Buttons(2).Top=15
      this.SetAll("Width",287)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ_2_15.GetRadio()
    this.Parent.oContained.w_RADSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ_2_15.SetRadio()
    this.Parent.oContained.w_RADSELEZ=trim(this.Parent.oContained.w_RADSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ=='D',2,;
      0))
  endfunc


  add object oObj_2_17 as cp_runprogram with uid="GGJLPDAYIC",left=0, top=364, width=280,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSIM_BSX",;
    cEvent = "w_RADSELEZ Changed",;
    nPag=2;
    , HelpContextID = 112347110


  add object oBtn_2_20 as StdButton with uid="CTKBPLAFCC",left=467, top=295, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65621786;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_20.Click()
      with this.Parent.oContained
        do GSIM_BEX with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE))
      endwith
    endif
  endfunc


  add object oBtn_2_21 as StdButton with uid="DCQXOQGEUN",left=522, top=295, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 58333114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_16 as StdString with uid="UEMJPAZQHG",Visible=.t., Left=19, Top=38,;
    Alignment=0, Width=541, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="HTVPPJKJPJ",Visible=.t., Left=177, Top=62,;
    Alignment=0, Width=79, Height=15,;
    Caption="PATH"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="RSBPNTISQD",Visible=.t., Left=16, Top=189,;
    Alignment=0, Width=553, Height=18,;
    Caption="Il formato ASCII � stato mantenuto solo per compatibilit� verso il passato con i seguenti limiti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="FLGCDKUIMI",Visible=.t., Left=16, Top=173,;
    Alignment=0, Width=553, Height=19,;
    Caption="ATTENZIONE"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="QAEYYHTKCQ",Visible=.t., Left=24, Top=206,;
    Alignment=0, Width=544, Height=18,;
    Caption="- � possibile importare/esportare solo tracciati con sorgente dati ASCII."  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="QVEDJHAVXI",Visible=.t., Left=24, Top=238,;
    Alignment=0, Width=544, Height=18,;
    Caption="- nei tracciati, per la destinazione, non � gestita la tipologia campo. Si utilizza sempre variabile;"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="ZMDSREKBTB",Visible=.t., Left=33, Top=222,;
    Alignment=0, Width=528, Height=18,;
    Caption="Non sono supportate le sorgenti dati ODBC;"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="YRNANIZJGI",Visible=.t., Left=24, Top=254,;
    Alignment=0, Width=544, Height=18,;
    Caption="- nei tracciati, per la sorgente dell'informazione, non � gestita la tipologia espressione;"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="XOHNMWNYZF",Visible=.t., Left=16, Top=271,;
    Alignment=0, Width=536, Height=18,;
    Caption="Si consiglia di utilizzare l'import/export in formato DBF"  ;
  , bGlobalFont=.t.

  add object oBox_2_1 as StdBox with uid="VGDJRKAYSY",left=11, top=170, width=560,height=122

  add object oBox_2_18 as StdBox with uid="GWELANDZQB",left=11, top=57, width=560,height=115
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
