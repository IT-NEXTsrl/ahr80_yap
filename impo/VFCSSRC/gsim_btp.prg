* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_btp                                                        *
*              Trasf. tabella pagamenti per conversioni ahw                    *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_71]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
* Crea il dettaglio pagamenti per quei pagamenti dove non � definito           *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_btp",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_btp as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  w_ni = 0
  w_P2CODICE = 0
  w_P2TIPPAG = space(2)
  w_P2SCADEN = space(2)
  w_GIOSCA = 0
  w_PANURATE = 0
  w_RECNO = 0
  w_PAGIOSCA = 0
  w_PAINISCA = 0
  w_PAINTERV = 0
  w_PAFL1RAT = space(2)
  w_PAFLGRID = space(2)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TRASF. TABELLA PAGAMENTI PER CONVERSIONI AHW
    * --- Passo 1 - Trasforma il cursore in cursore scrivibile
    *     Passo 2 - Per ogni pagamento senza dettaglio 
    *     Passo 2.1 - Cancella pagamento senza dettaglio
    *     Passo 2.2 - Aggiunge n righe dettaglio dove n � il numero delle rata
    *     Passo 3 - Chiude cursore
    *     
    * --- Init
    * --- Elabora cursore
    if used(this.w_CURSOR)
      * --- Rende scrivibile il cursore
      wrcursor(this.w_CURSOR)
      select (this.w_CURSOR)
      go top
      SCAN FOR empty(PAFLDETT) OR (PAFLDETT="S" AND ISNULL(P2CODICE))
      this.w_RECNO = recno()
      * --- Legge informazioni pagamento
      this.w_P2CODICE = PACODICE
      this.w_P2TIPPAG = PATIPPAG
      this.w_P2SCADEN = PASCADEN
      this.w_PANURATE = PANURATE
      this.w_PAGIOSCA = PAGIOSCA
      this.w_PAINISCA = PAINISCA
      this.w_PAINTERV = PAINTERV
      this.w_PAFL1RAT = PAFL1RAT
      this.w_PAFLGRID = PAFLGRID
      * --- Calcola giorni scadenza prima rata
      this.w_GIOSCA = iif(this.w_PAINISCA > 0, this.w_PAINISCA,iif(this.w_PAGIOSCA>0,this.w_PAGIOSCA,this.w_PAINTERV))
      this.w_ni = 1
      do while this.w_ni <= this.w_PANURATE
        if this.w_GIOSCA>999
          * --- In adhoc Revolution non sono gestiti i pagamenti aventi una rata a pi� di 999 giorni
          this.w_GIOSCA = 999
        endif
        append blank
        replace P2CODICE with this.w_P2CODICE
        replace P2NUMRAT with right("00"+alltrim(str(this.w_ni,2,0)),2)
        replace P2TIPPAG with this.w_P2TIPPAG
        replace P2SCADEN with this.w_P2SCADEN
        replace P2GIOFIS with 0
        replace P2GIOSCA with this.w_GIOSCA
        replace PAFLDETT with "S"
        replace PAFL1RAT with this.w_PAFL1RAT
        replace PAFLGRID with this.w_PAFLGRID
        * --- Calcola giorni scadenza per prossima rata
        this.w_GIOSCA = iif(this.w_ni=1 and this.w_PAINISCA > 0 and this.w_PAGIOSCA > 0, this.w_PAGIOSCA,this.w_GIOSCA + this.w_PAINTERV)
        this.w_ni = this.w_ni + 1
      enddo
      goto (this.w_RECNO)
      ENDSCAN
      * --- Crea nuovo cursore w_CURSOR
      if used("_read_")
        select _read_
        use
      endif
      * --- Passando tramite cursore _read_
      select P2CODICE,P2NUMRAT,P2TIPPAG,P2SCADEN,P2GIOFIS,P2GIOSCA,PAFL1RAT, PAFLGRID from (this.w_CURSOR) into cursor _read_ nofilter where !empty(PAFLDETT)
      select (this.w_CURSOR)
      use
      select * from _read_ into cursor (this.w_CURSOR) nofilter where !isnull(P2CODICE) order by P2CODICE
      if used("_read_")
        select _read_
        use
      endif
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
