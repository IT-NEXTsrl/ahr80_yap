* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bsm                                                        *
*              Sel/des archivi e movimenti magazzino                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_34]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-01                                                      *
* Last revis.: 2006-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParametro
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bsm",oParentObject,m.pParametro)
return(i_retval)

define class tgsim_bsm as StdBatch
  * --- Local variables
  w_Control = .NULL.
  pParametro = 0
  w_ArchDest = space(2)
  * --- WorkFile variables
  IMPORTAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione e Deselezione Archivi Magazzino
    * --- Parametri:
    *     0= Init
    *     1= Selezione o Deselezione in funzione di variabile w_RADSELEZ
    * --- Selezione e Deselezione Archivi
    * --- Traduzioni
    * --- Movimenti
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pParametro=0
      this.oParentObject.w_RADSELEZ = "S"
    endif
    if !empty(this.oParentObject.w_CODIMP)
      * --- Select from IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select IMDESTIN  from "+i_cTable+" IMPORTAZ ";
            +" where IMCODICE="+cp_ToStrODBC(this.oParentObject.w_CODIMP)+" and IMTIPSRC="+cp_ToStrODBC(this.oParentObject.w_TIPSRC)+"";
             ,"_Curs_IMPORTAZ")
      else
        select IMDESTIN from (i_cTable);
         where IMCODICE=this.oParentObject.w_CODIMP and IMTIPSRC=this.oParentObject.w_TIPSRC;
          into cursor _Curs_IMPORTAZ
      endif
      if used('_Curs_IMPORTAZ')
        select _Curs_IMPORTAZ
        locate for 1=1
        do while not(eof())
        w_CMD="this.oParentObject.w_SELE"+_Curs_IMPORTAZ.IMDESTIN
        w_ErrHandler = on("ERROR")
        on error w_Accept=1
        if this.oParentObject.w_RADSELEZ="S"
          &w_CMD=_Curs_IMPORTAZ.IMDESTIN
        else
          &w_CMD=""
        endif
        on error &w_ErrHandler 
          select _Curs_IMPORTAZ
          continue
        enddo
        use
      endif
    endif
    * --- IL FLAG 'SOTTO TRANSAZIONE' DEVE ESSERE ABILITATO SE NON SI IMPORTANO I
    *     DOCUMENTI UTILIZZANDO IL TRACCIATO CHE FA UNA TRANSAZIONE PER DOCUMENTO
    if this.oParentObject.w_SELEDO = "DO"
      this.oParentObject.w_IMTRANSA = "N"
    else
      this.oParentObject.w_IMTRANSA = "S"
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deseleziona tutti
    * --- Archivi
    this.oParentObject.w_SELEAR = "|"
    this.w_ArchDest = "AR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECM = "|"
    this.w_ArchDest = "CM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEMA = "|"
    this.w_ArchDest = "MA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEKA = "|"
    this.w_ArchDest = "KA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEUM = "|"
    this.w_ArchDest = "UM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECD = "|"
    this.w_ArchDest = "CD"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELELS = "|"
    this.w_ArchDest = "LS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECN = "|"
    this.w_ArchDest = "CN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECP = "|"
    this.w_ArchDest = "CP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELESM = "|"
    this.w_ArchDest = "SM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELELA = "|"
    this.w_ArchDest = "LA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEGM = "|"
    this.w_ArchDest = "GM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECG = "|"
    this.w_ArchDest = "CG"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETT = "|"
    this.w_ArchDest = "TT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELENO = "|"
    this.w_ArchDest = "NO"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETC = "|"
    this.w_ArchDest = "TC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEAN = "|"
    this.w_ArchDest = "AN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIN = "|"
    this.w_ArchDest = "IN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIF = "|"
    this.w_ArchDest = "IF"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIC = "|"
    this.w_ArchDest = "IC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIS = "|"
    this.w_ArchDest = "IS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Movimenti
    this.oParentObject.w_SELEMM = "|"
    this.w_ArchDest = "MM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDT = "|"
    this.w_ArchDest = "DT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDR = "|"
    this.w_ArchDest = "DR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDO = "|"
    this.w_ArchDest = "DO"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECI = "|"
    this.w_ArchDest = "CI"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECX = "|"
    this.w_ArchDest = "CX"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETR = "|"
    this.w_ArchDest = "TR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETO = "|"
    this.w_ArchDest = "TO"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDB = "|"
    this.w_ArchDest = "DB"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELERI = "|"
    this.w_ArchDest = "RI"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELESE = "|"
    this.w_ArchDest = "SE"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEAB = "|"
    this.w_ArchDest = "AB"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPR = "|"
    this.w_ArchDest = "PR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEAA = "|"
    this.w_ArchDest = "AA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECF = "|"
    this.w_ArchDest = "CF"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Movimenti
    this.oParentObject.w_SELETA = "|"
    this.w_ArchDest = "TA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETM = "|"
    this.w_ArchDest = "TM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETP = "|"
    this.w_ArchDest = "TP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Control = this.oParentObject.GetControlObject("w_SELE"+this.w_ArchDest,1)
    if type("this.w_Control")="O"
      this.w_Control.BackStyle = 0
      this.w_Control.FontStrikeThru = .F.
    endif
  endproc


  proc Init(oParentObject,pParametro)
    this.pParametro=pParametro
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMPORTAZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_IMPORTAZ')
      use in _Curs_IMPORTAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParametro"
endproc
