* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_asd                                                        *
*              Tabelle database                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_3]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-11                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_asd"))

* --- Class definition
define class tgsim_asd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 650
  Height = 328+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=109690729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  XDC_TABLE_IDX = 0
  cFile = "XDC_TABLE"
  cKeySelect = "TBNAME"
  cKeyWhere  = "TBNAME=this.w_TBNAME"
  cKeyWhereODBC = '"TBNAME="+cp_ToStrODBC(this.w_TBNAME)';

  cKeyWhereODBCqualified = '"XDC_TABLE.TBNAME="+cp_ToStrODBC(this.w_TBNAME)';

  cPrg = "gsim_asd"
  cComment = "Tabelle database"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TBNAME = space(30)
  w_TBPHNAME = space(30)
  w_TBCOMMENT = space(60)
  w_TBCOMPANY = .F.
  w_TBYEAR = .F.
  w_TBUSER = .F.
  w_DSNAME = space(30)
  w_TIMESTAMP = space(14)

  * --- Children pointers
  GSIM_MST = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'XDC_TABLE','gsim_asd')
    stdPageFrame::Init()
    *set procedure to GSIM_MST additive
    with this
      .Pages(1).addobject("oPag","tgsim_asdPag1","gsim_asd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tabella database")
      .Pages(1).HelpContextID = 198545669
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTBNAME_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSIM_MST
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='XDC_TABLE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.XDC_TABLE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.XDC_TABLE_IDX,3]
  return

  function CreateChildren()
    this.GSIM_MST = CREATEOBJECT('stdLazyChild',this,'GSIM_MST')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSIM_MST)
      this.GSIM_MST.DestroyChildrenChain()
      this.GSIM_MST=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSIM_MST.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSIM_MST.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSIM_MST.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSIM_MST.SetKey(;
            .w_TBNAME,"TBNAME";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSIM_MST.ChangeRow(this.cRowID+'      1',1;
             ,.w_TBNAME,"TBNAME";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TBNAME = NVL(TBNAME,space(30))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from XDC_TABLE where TBNAME=KeySet.TBNAME
    *
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('XDC_TABLE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "XDC_TABLE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' XDC_TABLE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TBNAME',this.w_TBNAME  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TBNAME = NVL(TBNAME,space(30))
        .w_TBPHNAME = NVL(TBPHNAME,space(30))
        .w_TBCOMMENT = NVL(TBCOMMENT,space(60))
        .w_TBCOMPANY = !empty(NVL(TBCOMPANY,.f.))
        .w_TBYEAR = !empty(NVL(TBYEAR,.f.))
        .w_TBUSER = !empty(NVL(TBUSER,.f.))
        .w_DSNAME = NVL(DSNAME,space(30))
        .w_TIMESTAMP = NVL(TIMESTAMP,space(14))
        cp_LoadRecExtFlds(this,'XDC_TABLE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TBNAME = space(30)
      .w_TBPHNAME = space(30)
      .w_TBCOMMENT = space(60)
      .w_TBCOMPANY = .f.
      .w_TBYEAR = .f.
      .w_TBUSER = .f.
      .w_DSNAME = space(30)
      .w_TIMESTAMP = space(14)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'XDC_TABLE')
    this.DoRTCalc(1,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTBNAME_1_1.enabled = i_bVal
      .Page1.oPag.oTBPHNAME_1_3.enabled = i_bVal
      .Page1.oPag.oTBCOMMENT_1_5.enabled = i_bVal
      .Page1.oPag.oTBCOMPANY_1_7.enabled = i_bVal
      .Page1.oPag.oTBYEAR_1_9.enabled = i_bVal
      .Page1.oPag.oTBUSER_1_11.enabled = i_bVal
      .Page1.oPag.oDSNAME_1_13.enabled = i_bVal
      .Page1.oPag.oTIMESTAMP_1_15.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTBNAME_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTBNAME_1_1.enabled = .t.
      endif
    endwith
    this.GSIM_MST.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'XDC_TABLE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSIM_MST.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBNAME,"TBNAME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBPHNAME,"TBPHNAME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBCOMMENT,"TBCOMMENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBCOMPANY,"TBCOMPANY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBYEAR,"TBYEAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TBUSER,"TBUSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DSNAME,"DSNAME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIMESTAMP,"TIMESTAMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    i_lTable = "XDC_TABLE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.XDC_TABLE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.XDC_TABLE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into XDC_TABLE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'XDC_TABLE')
        i_extval=cp_InsertValODBCExtFlds(this,'XDC_TABLE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TBNAME,TBPHNAME,TBCOMMENT,TBCOMPANY,TBYEAR"+;
                  ",TBUSER,DSNAME,TIMESTAMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TBNAME)+;
                  ","+cp_ToStrODBC(this.w_TBPHNAME)+;
                  ","+cp_ToStrODBC(this.w_TBCOMMENT)+;
                  ","+cp_ToStrODBC(this.w_TBCOMPANY)+;
                  ","+cp_ToStrODBC(this.w_TBYEAR)+;
                  ","+cp_ToStrODBC(this.w_TBUSER)+;
                  ","+cp_ToStrODBC(this.w_DSNAME)+;
                  ","+cp_ToStrODBC(this.w_TIMESTAMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'XDC_TABLE')
        i_extval=cp_InsertValVFPExtFlds(this,'XDC_TABLE')
        cp_CheckDeletedKey(i_cTable,0,'TBNAME',this.w_TBNAME)
        INSERT INTO (i_cTable);
              (TBNAME,TBPHNAME,TBCOMMENT,TBCOMPANY,TBYEAR,TBUSER,DSNAME,TIMESTAMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TBNAME;
                  ,this.w_TBPHNAME;
                  ,this.w_TBCOMMENT;
                  ,this.w_TBCOMPANY;
                  ,this.w_TBYEAR;
                  ,this.w_TBUSER;
                  ,this.w_DSNAME;
                  ,this.w_TIMESTAMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.XDC_TABLE_IDX,i_nConn)
      *
      * update XDC_TABLE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'XDC_TABLE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TBPHNAME="+cp_ToStrODBC(this.w_TBPHNAME)+;
             ",TBCOMMENT="+cp_ToStrODBC(this.w_TBCOMMENT)+;
             ",TBCOMPANY="+cp_ToStrODBC(this.w_TBCOMPANY)+;
             ",TBYEAR="+cp_ToStrODBC(this.w_TBYEAR)+;
             ",TBUSER="+cp_ToStrODBC(this.w_TBUSER)+;
             ",DSNAME="+cp_ToStrODBC(this.w_DSNAME)+;
             ",TIMESTAMP="+cp_ToStrODBC(this.w_TIMESTAMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'XDC_TABLE')
        i_cWhere = cp_PKFox(i_cTable  ,'TBNAME',this.w_TBNAME  )
        UPDATE (i_cTable) SET;
              TBPHNAME=this.w_TBPHNAME;
             ,TBCOMMENT=this.w_TBCOMMENT;
             ,TBCOMPANY=this.w_TBCOMPANY;
             ,TBYEAR=this.w_TBYEAR;
             ,TBUSER=this.w_TBUSER;
             ,DSNAME=this.w_DSNAME;
             ,TIMESTAMP=this.w_TIMESTAMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSIM_MST : Saving
      this.GSIM_MST.ChangeRow(this.cRowID+'      1',0;
             ,this.w_TBNAME,"TBNAME";
             )
      this.GSIM_MST.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSIM_MST : Deleting
    this.GSIM_MST.ChangeRow(this.cRowID+'      1',0;
           ,this.w_TBNAME,"TBNAME";
           )
    this.GSIM_MST.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.XDC_TABLE_IDX,i_nConn)
      *
      * delete XDC_TABLE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TBNAME',this.w_TBNAME  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTBNAME_1_1.value==this.w_TBNAME)
      this.oPgFrm.Page1.oPag.oTBNAME_1_1.value=this.w_TBNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oTBPHNAME_1_3.value==this.w_TBPHNAME)
      this.oPgFrm.Page1.oPag.oTBPHNAME_1_3.value=this.w_TBPHNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMPANY_1_7.value==this.w_TBCOMPANY)
      this.oPgFrm.Page1.oPag.oTBCOMPANY_1_7.value=this.w_TBCOMPANY
    endif
    if not(this.oPgFrm.Page1.oPag.oTBYEAR_1_9.value==this.w_TBYEAR)
      this.oPgFrm.Page1.oPag.oTBYEAR_1_9.value=this.w_TBYEAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTBUSER_1_11.value==this.w_TBUSER)
      this.oPgFrm.Page1.oPag.oTBUSER_1_11.value=this.w_TBUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDSNAME_1_13.value==this.w_DSNAME)
      this.oPgFrm.Page1.oPag.oDSNAME_1_13.value=this.w_DSNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oTIMESTAMP_1_15.value==this.w_TIMESTAMP)
      this.oPgFrm.Page1.oPag.oTIMESTAMP_1_15.value=this.w_TIMESTAMP
    endif
    cp_SetControlsValueExtFlds(this,'XDC_TABLE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSIM_MST.CheckForm()
      if i_bres
        i_bres=  .GSIM_MST.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSIM_MST : Depends On
    this.GSIM_MST.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsim_asdPag1 as StdContainer
  Width  = 646
  height = 328
  stdWidth  = 646
  stdheight = 328
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTBNAME_1_1 as StdField with uid="BNIRPAUROA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TBNAME", cQueryName = "TBNAME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 59533366,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=174, Top=16, InputMask=replicate('X',30)

  add object oTBPHNAME_1_3 as StdField with uid="SOPTZEXXMJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TBPHNAME", cQueryName = "TBPHNAME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 6059909,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=174, Top=42, InputMask=replicate('X',30)

  add object oTBCOMMENT_1_5 as StdField with uid="JGSCVWURTT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 194624964,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=174, Top=90, InputMask=replicate('X',60)

  add object oTBCOMPANY_1_7 as StdField with uid="GHWDQOOBTU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TBCOMPANY", cQueryName = "TBCOMPANY",;
    bObbl = .f. , nPag = 1, value=.f., bMultilanguage =  .f.,;
    HelpContextID = 244956692,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=315, Top=113

  add object oTBYEAR_1_9 as StdField with uid="CAWGKMIWMD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TBYEAR", cQueryName = "TBYEAR",;
    bObbl = .f. , nPag = 1, value=.f., bMultilanguage =  .f.,;
    HelpContextID = 265361462,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=315, Top=136

  add object oTBUSER_1_11 as StdField with uid="NUYOKKFXOK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TBUSER", cQueryName = "TBUSER",;
    bObbl = .f. , nPag = 1, value=.f., bMultilanguage =  .f.,;
    HelpContextID = 2021430,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=315, Top=159

  add object oDSNAME_1_13 as StdField with uid="VDVAIYJFKR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DSNAME", cQueryName = "DSNAME",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 59537462,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=315, Top=182, InputMask=replicate('X',30)

  add object oTIMESTAMP_1_15 as StdField with uid="QNDPYYEMRQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TIMESTAMP", cQueryName = "TIMESTAMP",;
    bObbl = .f. , nPag = 1, value=space(14), bMultilanguage =  .f.,;
    HelpContextID = 49308803,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=315, Top=205, InputMask=replicate('X',14)


  add object oLinkPC_1_17 as StdButton with uid="NLYKWYJWHM",left=315, top=239, width=48,height=45,;
    CpPicture="BMP\Diba24.bmp", caption="", nPag=1;
    , HelpContextID = 90661510;
    , Caption='\<Strutt. tab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_17.Click()
      this.Parent.oContained.GSIM_MST.LinkPCClick()
    endproc

  add object oStr_1_2 as StdString with uid="QIGKMTPYJR",Visible=.t., Left=20, Top=18,;
    Alignment=1, Width=149, Height=18,;
    Caption="Nome logico tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="DNNDHZEKPV",Visible=.t., Left=20, Top=43,;
    Alignment=1, Width=149, Height=18,;
    Caption="Nome fisico tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="JBGEWRKUBF",Visible=.t., Left=80, Top=95,;
    Alignment=1, Width=89, Height=18,;
    Caption="Commento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QNJMJWNHDZ",Visible=.t., Left=204, Top=117,;
    Alignment=1, Width=110, Height=18,;
    Caption="Flag company:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NUFMKBZMTS",Visible=.t., Left=204, Top=140,;
    Alignment=1, Width=110, Height=18,;
    Caption="Flag year:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BCJFIUZIJB",Visible=.t., Left=204, Top=163,;
    Alignment=1, Width=110, Height=18,;
    Caption="Flag user:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KNJUDEEXFF",Visible=.t., Left=204, Top=186,;
    Alignment=1, Width=110, Height=18,;
    Caption="Nome design:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DRKSCVBIEM",Visible=.t., Left=3, Top=209,;
    Alignment=1, Width=311, Height=18,;
    Caption="Time stampa (data e time ultimo aggiornamento):"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_asd','XDC_TABLE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TBNAME=XDC_TABLE.TBNAME";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
