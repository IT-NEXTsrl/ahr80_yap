* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bsd                                                        *
*              Sel/des archivi import/export DBF                               *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_20]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bsd",oParentObject)
return(i_retval)

define class tgsim_bsd as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione e Deselezione Archivi Import/Export da files DBF
    this.oParentObject.w_SELETR1 = iif( this.oParentObject.w_RADSELEZ1="D" , "", "TR" )
    this.oParentObject.w_SELEAD1 = iif( this.oParentObject.w_RADSELEZ1="D" , "", "AD" )
    this.oParentObject.w_SELEVP1 = iif( this.oParentObject.w_RADSELEZ1="D" , "", "VP" )
    this.oParentObject.w_SELETS1 = iif( this.oParentObject.w_RADSELEZ1="D" , "", "TS" )
    this.oParentObject.w_SELESOR = iif( this.oParentObject.w_RADSELEZ1="D" , "", "SO" )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
