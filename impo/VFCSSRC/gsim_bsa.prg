* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bsa                                                        *
*              Selezione/desel. archivi e movimenti alter ego                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_32]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-01                                                      *
* Last revis.: 2014-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParametro
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bsa",oParentObject,m.pParametro)
return(i_retval)

define class tgsim_bsa as StdBatch
  * --- Local variables
  w_Control = .NULL.
  w_ArchDest = space(2)
  pParametro = 0
  * --- WorkFile variables
  IMPORTAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione e Deselezione checkbox archivi e movimentazioni
    * --- Parametri:
    *     0= Init
    *     1= Selezione o Deselezione in funzione di variabile w_RADSELEZ
    * --- Selezione e Deselezione Archivi
    * --- Archivi
    * --- Biblioteca
    * --- Cespiti
    * --- Prestazioni in anagrafica
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pParametro=0
      this.oParentObject.w_RADSELEZ = "S"
    endif
    if !empty(this.oParentObject.w_CODIMP)
      * --- Select from IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select IMDESTIN  from "+i_cTable+" IMPORTAZ ";
            +" where IMCODICE="+cp_ToStrODBC(this.oParentObject.w_CODIMP)+" and IMTIPSRC="+cp_ToStrODBC(this.oParentObject.w_TIPSRC)+"";
             ,"_Curs_IMPORTAZ")
      else
        select IMDESTIN from (i_cTable);
         where IMCODICE=this.oParentObject.w_CODIMP and IMTIPSRC=this.oParentObject.w_TIPSRC;
          into cursor _Curs_IMPORTAZ
      endif
      if used('_Curs_IMPORTAZ')
        select _Curs_IMPORTAZ
        locate for 1=1
        do while not(eof())
        w_CMD="this.oParentObject.w_SELE"+_Curs_IMPORTAZ.IMDESTIN
        w_ErrHandler = on("ERROR")
        on error w_Accept=1
        if this.oParentObject.w_RADSELEZ="S"
          &w_CMD=_Curs_IMPORTAZ.IMDESTIN
        else
          &w_CMD=""
        endif
        on error &w_ErrHandler 
          select _Curs_IMPORTAZ
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deseleziona tutti
    * --- Archivi
    this.oParentObject.w_SELEOG = "|"
    this.w_ArchDest = "OG"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEST = "|"
    this.w_ArchDest = "ST"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEUB = "|"
    this.w_ArchDest = "UB"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETI = "|"
    this.w_ArchDest = "TI"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEMP = "|"
    this.w_ArchDest = "MP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEUP = "|"
    this.w_ArchDest = "UP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEEP = "|"
    this.w_ArchDest = "EP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA1 = "|"
    this.w_ArchDest = "A1"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA2 = "|"
    this.w_ArchDest = "A2"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA3 = "|"
    this.w_ArchDest = "A3"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA4 = "|"
    this.w_ArchDest = "A4"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELESD = "|"
    this.w_ArchDest = "SD"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDP = "|"
    this.w_ArchDest = "DP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEC1 = "|"
    this.w_ArchDest = "C1"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELESO = "|"
    this.w_ArchDest = "SO"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELERS = "|"
    this.w_ArchDest = "RS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPI = "|"
    this.w_ArchDest = "PI"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPE = "|"
    this.w_ArchDest = "PE"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETL = "|"
    this.w_ArchDest = "TL"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECU = "|"
    this.w_ArchDest = "CU"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIP = "|"
    this.w_ArchDest = "IP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDG = "|"
    this.w_ArchDest = "DG"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEAT = "|"
    this.w_ArchDest = "AT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETF = "|"
    this.w_ArchDest = "TF"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDI = "|"
    this.w_ArchDest = "DI"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEFP = "|"
    this.w_ArchDest = "FP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPS = "|"
    this.w_ArchDest = "PS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELERU = "|"
    this.w_ArchDest = "RU"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEF1 = "|"
    this.w_ArchDest = "F1"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEF2 = "|"
    this.w_ArchDest = "F2"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEF3 = "|"
    this.w_ArchDest = "F3"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEF4 = "|"
    this.w_ArchDest = "F4"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDT = "|"
    this.w_ArchDest = "DT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA5 = "|"
    this.w_ArchDest = "A5"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEGU = "|"
    this.w_ArchDest = "GU"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELERA = "|"
    this.w_ArchDest = "RA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Biblioteca
    this.oParentObject.w_SELEBB = "|"
    this.w_ArchDest = "BB"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBU = "|"
    this.w_ArchDest = "BU"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBE = "|"
    this.w_ArchDest = "BE"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBM = "|"
    this.w_ArchDest = "BM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBR = "|"
    this.w_ArchDest = "BR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBT = "|"
    this.w_ArchDest = "BT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEGN = "|"
    this.w_ArchDest = "GN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECF = "|"
    this.w_ArchDest = "CF"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELENM = "|"
    this.w_ArchDest = "NM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELET1 = "|"
    this.w_ArchDest = "T1"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELET2 = "|"
    this.w_ArchDest = "T2"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPM = "|"
    this.w_ArchDest = "PM"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA6 = "|"
    this.w_ArchDest = "A6"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA7 = "|"
    this.w_ArchDest = "A7"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA8 = "|"
    this.w_ArchDest = "A8"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEA9 = "|"
    this.w_ArchDest = "A9"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEEV = "|"
    this.w_ArchDest = "EV"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBS = "|"
    this.w_ArchDest = "BS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEBC = "|"
    this.w_ArchDest = "BC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELESN = "|"
    this.w_ArchDest = "SN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDR = "|"
    this.w_ArchDest = "DR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEFR = "|"
    this.w_ArchDest = "FR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEON = "|"
    this.w_ArchDest = "ON"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEVA = "|"
    this.w_ArchDest = "VA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELENA = "|"
    this.w_ArchDest = "NA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIV = "|"
    this.w_ArchDest = "IV"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPA = "|"
    this.w_ArchDest = "PA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPD = "|"
    this.w_ArchDest = "PD"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPC = "|"
    this.w_ArchDest = "PC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECC = "|"
    this.w_ArchDest = "CC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEAC = "|"
    this.w_ArchDest = "AC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPN = "|"
    this.w_ArchDest = "PN"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPT = "|"
    this.w_ArchDest = "PT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEFY = "|"
    this.w_ArchDest = "FY"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECR = "|"
    this.w_ArchDest = "CR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECT = "|"
    this.w_ArchDest = "CT"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELECS = "|"
    this.w_ArchDest = "CS"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETU = "|"
    this.w_ArchDest = "TU"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEKR = "|"
    this.w_ArchDest = "KR"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEEC = "|"
    this.w_ArchDest = "EC"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEC2 = "|"
    this.w_ArchDest = "C2"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPZ = "|"
    this.w_ArchDest = "PZ"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEIA = "|"
    this.w_ArchDest = "IA"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDO = "|"
    this.w_ArchDest = "DO"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEDE = "|"
    this.w_ArchDest = "DE"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELETZ = "|"
    this.w_ArchDest = "TZ"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_SELEPP = "|"
    this.w_ArchDest = "PP"
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Control = this.oParentObject.GetControlObject("w_SELE"+this.w_ArchDest,1)
    if type("this.w_Control")="O"
      this.w_Control.BackStyle = 0
      this.w_Control.FontStrikeThru = .F.
    endif
  endproc


  proc Init(oParentObject,pParametro)
    this.pParametro=pParametro
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMPORTAZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_IMPORTAZ')
      use in _Curs_IMPORTAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParametro"
endproc
