* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bds                                                        *
*              Duplicazione tracciato                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_26]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-21                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bds",oParentObject)
return(i_retval)

define class tgsim_bds as StdBatch
  * --- Local variables
  w_Errore = .f.
  w_ESISTE = space(1)
  * --- WorkFile variables
  CAUIVA_idx=0
  CAUIVA1_idx=0
  CAUPRI_idx=0
  CAUPRI1_idx=0
  IMPORTAZ_idx=0
  PAG_2AME_idx=0
  RESOCONT_idx=0
  TRACCIAT_idx=0
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplicazione struttura
    * --- La presente procedura crea una nuova struttura da una o parte di una gi� esistente
    * --- Variabili del batch chiamante
    * --- Codice importazione di partenza
    * --- Archivio di partenza
    * --- Archivio di arrivo
    * --- Codice importazione da creare
    * --- Descrizione struttura nuova
    * --- Variabili locali
    * --- Indica se la generazione ha dato errore
    this.w_ESISTE = " "
    * --- Leggo la chiave per vedere se esiste gi� il tracciato
    * --- Ciclo sui dati vecchi per generare la struttura Nuova
    * --- Provo a leggere se la struttura esiste gi� con quel nome
    * --- Read from IMPORTAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IMCODICE"+;
        " from "+i_cTable+" IMPORTAZ where ";
            +"IMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODIMPN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IMCODICE;
        from (i_cTable) where;
            IMCODICE = this.oParentObject.w_CODIMPN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ESISTE = NVL(cp_ToDate(_read_.IMCODICE),cp_NullValue(_read_.IMCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_ESISTE)
      this.w_Errore = .f.
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_01102E78
      bErr_01102E78=bTrsErr
      this.Try_01102E78()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Tracciato non duplicato
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_Errore = .t.
        ah_ErrorMsg("Il tracciato non � stato duplicato per errore:%0%0%1","stop","",i_TrsMsg)
      endif
      bTrsErr=bTrsErr or bErr_01102E78
      * --- End
      if ! this.w_ERRORE
        * --- Try
        local bErr_01109560
        bErr_01109560=bTrsErr
        this.Try_01109560()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_01109560
        * --- End
      endif
    else
      ah_ErrorMsg("Impossibile eseguire la duplicazione: struttura gi� esistente","!","")
      this.w_Errore = .t.
    endif
  endproc
  proc Try_01102E78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Replico Struttura
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Replico Tracciati
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Replico Trascodifiche
    * --- commit
    cp_EndTrs(.t.)
    * --- Tracciato duplicato
    ah_ErrorMsg("Il tracciato � stato duplicato","i","")
    return
  proc Try_01109560()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Replico Struttura leggendo dalla tabella i records selezionati
    if empty(this.oParentObject.w_ARCFIN)
      * --- Select from IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" IMPORTAZ ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=IMCODICE";
             ,"_Curs_IMPORTAZ")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=IMCODICE;
          into cursor _Curs_IMPORTAZ
      endif
      if used('_Curs_IMPORTAZ')
        select _Curs_IMPORTAZ
        locate for 1=1
        do while not(eof())
        * --- Insert into IMPORTAZ
        i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPORTAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IMTIPSRC"+",IMSEQUEN"+",IM_ASCII"+",IM_ODBC"+",IMDESTIN"+",IMTRASCO"+",IMTRAMUL"+",IMAGGANA"+",IMAGGPAR"+",IMAGGSAL"+",IMCONIND"+",IMFILTRO"+",IMAGGNUR"+",IMANNOTA"+",IMPARIVA"+",IMCODICE"+",IMAZZERA"+",IMCHIMOV"+",IMRESOCO"+",IMCONTRO"+",IMAGGIVA"+",IMROUTIN"+",IM_EXCEL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTIPSRC),'IMPORTAZ','IMTIPSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_ASCII),'IMPORTAZ','IM_ASCII');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_ODBC),'IMPORTAZ','IM_ODBC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMDESTIN),'IMPORTAZ','IMDESTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTRASCO),'IMPORTAZ','IMTRASCO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGANA),'IMPORTAZ','IMAGGANA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCONIND),'IMPORTAZ','IMCONIND');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMFILTRO),'IMPORTAZ','IMFILTRO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESIMPN),'IMPORTAZ','IMANNOTA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMPARIVA),'IMPORTAZ','IMPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'IMPORTAZ','IMCODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAZZERA),'IMPORTAZ','IMAZZERA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMRESOCO),'IMPORTAZ','IMRESOCO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCONTRO),'IMPORTAZ','IMCONTRO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMROUTIN),'IMPORTAZ','IMROUTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_EXCEL),'IMPORTAZ','IM_EXCEL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IMTIPSRC',_Curs_IMPORTAZ.IMTIPSRC,'IMSEQUEN',_Curs_IMPORTAZ.IMSEQUEN,'IM_ASCII',_Curs_IMPORTAZ.IM_ASCII,'IM_ODBC',_Curs_IMPORTAZ.IM_ODBC,'IMDESTIN',_Curs_IMPORTAZ.IMDESTIN,'IMTRASCO',_Curs_IMPORTAZ.IMTRASCO,'IMTRAMUL',_Curs_IMPORTAZ.IMTRAMUL,'IMAGGANA',_Curs_IMPORTAZ.IMAGGANA,'IMAGGPAR',_Curs_IMPORTAZ.IMAGGPAR,'IMAGGSAL',_Curs_IMPORTAZ.IMAGGSAL,'IMCONIND',_Curs_IMPORTAZ.IMCONIND,'IMFILTRO',_Curs_IMPORTAZ.IMFILTRO)
          insert into (i_cTable) (IMTIPSRC,IMSEQUEN,IM_ASCII,IM_ODBC,IMDESTIN,IMTRASCO,IMTRAMUL,IMAGGANA,IMAGGPAR,IMAGGSAL,IMCONIND,IMFILTRO,IMAGGNUR,IMANNOTA,IMPARIVA,IMCODICE,IMAZZERA,IMCHIMOV,IMRESOCO,IMCONTRO,IMAGGIVA,IMROUTIN,IM_EXCEL &i_ccchkf. );
             values (;
               _Curs_IMPORTAZ.IMTIPSRC;
               ,_Curs_IMPORTAZ.IMSEQUEN;
               ,_Curs_IMPORTAZ.IM_ASCII;
               ,_Curs_IMPORTAZ.IM_ODBC;
               ,_Curs_IMPORTAZ.IMDESTIN;
               ,_Curs_IMPORTAZ.IMTRASCO;
               ,_Curs_IMPORTAZ.IMTRAMUL;
               ,_Curs_IMPORTAZ.IMAGGANA;
               ,_Curs_IMPORTAZ.IMAGGPAR;
               ,_Curs_IMPORTAZ.IMAGGSAL;
               ,_Curs_IMPORTAZ.IMCONIND;
               ,_Curs_IMPORTAZ.IMFILTRO;
               ,_Curs_IMPORTAZ.IMAGGNUR;
               ,this.oParentObject.w_DESIMPN;
               ,_Curs_IMPORTAZ.IMPARIVA;
               ,this.oParentObject.w_CODIMPN;
               ,_Curs_IMPORTAZ.IMAZZERA;
               ,_Curs_IMPORTAZ.IMCHIMOV;
               ,_Curs_IMPORTAZ.IMRESOCO;
               ,_Curs_IMPORTAZ.IMCONTRO;
               ,_Curs_IMPORTAZ.IMAGGIVA;
               ,_Curs_IMPORTAZ.IMROUTIN;
               ,_Curs_IMPORTAZ.IM_EXCEL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella creazione del duplicato.'
          return
        endif
          select _Curs_IMPORTAZ
          continue
        enddo
        use
      endif
    else
      * --- Select from IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" IMPORTAZ ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=IMCODICE AND "+cp_ToStrODBC(this.oParentObject.w_ARCINI)+"<=IMDESTIN AND "+cp_ToStrODBC(this.oParentObject.w_ARCFIN)+">=IMDESTIN";
             ,"_Curs_IMPORTAZ")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=IMCODICE AND this.oParentObject.w_ARCINI<=IMDESTIN AND this.oParentObject.w_ARCFIN>=IMDESTIN;
          into cursor _Curs_IMPORTAZ
      endif
      if used('_Curs_IMPORTAZ')
        select _Curs_IMPORTAZ
        locate for 1=1
        do while not(eof())
        * --- Insert into IMPORTAZ
        i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IMPORTAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IMSEQUEN"+",IM_ASCII"+",IMDESTIN"+",IMTRASCO"+",IMTRAMUL"+",IMAGGPAR"+",IMAGGANA"+",IMAGGSAL"+",IMAGGNUR"+",IMCONIND"+",IMFILTRO"+",IMCHIMOV"+",IMANNOTA"+",IMPARIVA"+",IMAZZERA"+",IMRESOCO"+",IMCONTRO"+",IMCODICE"+",IMTIPSRC"+",IM_ODBC"+",IMAGGIVA"+",IMROUTIN"+",IM_EXCEL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMSEQUEN),'IMPORTAZ','IMSEQUEN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_ASCII),'IMPORTAZ','IM_ASCII');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMDESTIN),'IMPORTAZ','IMDESTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTRASCO),'IMPORTAZ','IMTRASCO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTRAMUL),'IMPORTAZ','IMTRAMUL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGPAR),'IMPORTAZ','IMAGGPAR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGANA),'IMPORTAZ','IMAGGANA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGSAL),'IMPORTAZ','IMAGGSAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGNUR),'IMPORTAZ','IMAGGNUR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCONIND),'IMPORTAZ','IMCONIND');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMFILTRO),'IMPORTAZ','IMFILTRO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCHIMOV),'IMPORTAZ','IMCHIMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESIMPN),'IMPORTAZ','IMANNOTA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMPARIVA),'IMPORTAZ','IMPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAZZERA),'IMPORTAZ','IMAZZERA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMRESOCO),'IMPORTAZ','IMRESOCO');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMCONTRO),'IMPORTAZ','IMCONTRO');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'IMPORTAZ','IMCODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMTIPSRC),'IMPORTAZ','IMTIPSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_ODBC),'IMPORTAZ','IM_ODBC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMAGGIVA),'IMPORTAZ','IMAGGIVA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IMROUTIN),'IMPORTAZ','IMROUTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_IMPORTAZ.IM_EXCEL),'IMPORTAZ','IM_EXCEL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IMSEQUEN',_Curs_IMPORTAZ.IMSEQUEN,'IM_ASCII',_Curs_IMPORTAZ.IM_ASCII,'IMDESTIN',_Curs_IMPORTAZ.IMDESTIN,'IMTRASCO',_Curs_IMPORTAZ.IMTRASCO,'IMTRAMUL',_Curs_IMPORTAZ.IMTRAMUL,'IMAGGPAR',_Curs_IMPORTAZ.IMAGGPAR,'IMAGGANA',_Curs_IMPORTAZ.IMAGGANA,'IMAGGSAL',_Curs_IMPORTAZ.IMAGGSAL,'IMAGGNUR',_Curs_IMPORTAZ.IMAGGNUR,'IMCONIND',_Curs_IMPORTAZ.IMCONIND,'IMFILTRO',_Curs_IMPORTAZ.IMFILTRO,'IMCHIMOV',_Curs_IMPORTAZ.IMCHIMOV)
          insert into (i_cTable) (IMSEQUEN,IM_ASCII,IMDESTIN,IMTRASCO,IMTRAMUL,IMAGGPAR,IMAGGANA,IMAGGSAL,IMAGGNUR,IMCONIND,IMFILTRO,IMCHIMOV,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMCODICE,IMTIPSRC,IM_ODBC,IMAGGIVA,IMROUTIN,IM_EXCEL &i_ccchkf. );
             values (;
               _Curs_IMPORTAZ.IMSEQUEN;
               ,_Curs_IMPORTAZ.IM_ASCII;
               ,_Curs_IMPORTAZ.IMDESTIN;
               ,_Curs_IMPORTAZ.IMTRASCO;
               ,_Curs_IMPORTAZ.IMTRAMUL;
               ,_Curs_IMPORTAZ.IMAGGPAR;
               ,_Curs_IMPORTAZ.IMAGGANA;
               ,_Curs_IMPORTAZ.IMAGGSAL;
               ,_Curs_IMPORTAZ.IMAGGNUR;
               ,_Curs_IMPORTAZ.IMCONIND;
               ,_Curs_IMPORTAZ.IMFILTRO;
               ,_Curs_IMPORTAZ.IMCHIMOV;
               ,this.oParentObject.w_DESIMPN;
               ,_Curs_IMPORTAZ.IMPARIVA;
               ,_Curs_IMPORTAZ.IMAZZERA;
               ,_Curs_IMPORTAZ.IMRESOCO;
               ,_Curs_IMPORTAZ.IMCONTRO;
               ,this.oParentObject.w_CODIMPN;
               ,_Curs_IMPORTAZ.IMTIPSRC;
               ,_Curs_IMPORTAZ.IM_ODBC;
               ,_Curs_IMPORTAZ.IMAGGIVA;
               ,_Curs_IMPORTAZ.IMROUTIN;
               ,_Curs_IMPORTAZ.IM_EXCEL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella creazione del duplicato.'
          return
        endif
          select _Curs_IMPORTAZ
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Replico Tracciati leggendo dalla tabella i records selezionati
    if empty(this.oParentObject.w_ARCFIN)
      * --- Select from TRACCIAT
      i_nConn=i_TableProp[this.TRACCIAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2],.t.,this.TRACCIAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TRACCIAT ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=TRCODICE";
             ,"_Curs_TRACCIAT")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=TRCODICE;
          into cursor _Curs_TRACCIAT
      endif
      if used('_Curs_TRACCIAT')
        select _Curs_TRACCIAT
        locate for 1=1
        do while not(eof())
        * --- Insert into TRACCIAT
        i_nConn=i_TableProp[this.TRACCIAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRACCIAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TR_ASCII"+",TRDESTIN"+",TRCAMNUM"+",TRCAMDES"+",TRCAMTIP"+",TRCAMLUN"+",TRCAMDEC"+",TRCAMDST"+",TRCAMOBB"+",TRANNOTA"+",TRTRASCO"+",TRCODICE"+",TRTIPSRC"+",TRCAMSRC"+",TRTIPSRG"+",TREXPSRG"+",TRCAMDLE"+",TRCAMDDE"+",TRTIPDES"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TR_ASCII),'TRACCIAT','TR_ASCII');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRDESTIN),'TRACCIAT','TRDESTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMNUM),'TRACCIAT','TRCAMNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDES),'TRACCIAT','TRCAMDES');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMTIP),'TRACCIAT','TRCAMTIP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMLUN),'TRACCIAT','TRCAMLUN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDEC),'TRACCIAT','TRCAMDEC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDST),'TRACCIAT','TRCAMDST');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMOBB),'TRACCIAT','TRCAMOBB');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRANNOTA),'TRACCIAT','TRANNOTA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTRASCO),'TRACCIAT','TRTRASCO');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'TRACCIAT','TRCODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPSRC),'TRACCIAT','TRTIPSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMSRC),'TRACCIAT','TRCAMSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPSRG),'TRACCIAT','TRTIPSRG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TREXPSRG),'TRACCIAT','TREXPSRG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDLE),'TRACCIAT','TRCAMDLE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDDE),'TRACCIAT','TRCAMDDE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPDES),'TRACCIAT','TRTIPDES');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TR_ASCII',_Curs_TRACCIAT.TR_ASCII,'TRDESTIN',_Curs_TRACCIAT.TRDESTIN,'TRCAMNUM',_Curs_TRACCIAT.TRCAMNUM,'TRCAMDES',_Curs_TRACCIAT.TRCAMDES,'TRCAMTIP',_Curs_TRACCIAT.TRCAMTIP,'TRCAMLUN',_Curs_TRACCIAT.TRCAMLUN,'TRCAMDEC',_Curs_TRACCIAT.TRCAMDEC,'TRCAMDST',_Curs_TRACCIAT.TRCAMDST,'TRCAMOBB',_Curs_TRACCIAT.TRCAMOBB,'TRANNOTA',_Curs_TRACCIAT.TRANNOTA,'TRTRASCO',_Curs_TRACCIAT.TRTRASCO,'TRCODICE',this.oParentObject.w_CODIMPN)
          insert into (i_cTable) (TR_ASCII,TRDESTIN,TRCAMNUM,TRCAMDES,TRCAMTIP,TRCAMLUN,TRCAMDEC,TRCAMDST,TRCAMOBB,TRANNOTA,TRTRASCO,TRCODICE,TRTIPSRC,TRCAMSRC,TRTIPSRG,TREXPSRG,TRCAMDLE,TRCAMDDE,TRTIPDES &i_ccchkf. );
             values (;
               _Curs_TRACCIAT.TR_ASCII;
               ,_Curs_TRACCIAT.TRDESTIN;
               ,_Curs_TRACCIAT.TRCAMNUM;
               ,_Curs_TRACCIAT.TRCAMDES;
               ,_Curs_TRACCIAT.TRCAMTIP;
               ,_Curs_TRACCIAT.TRCAMLUN;
               ,_Curs_TRACCIAT.TRCAMDEC;
               ,_Curs_TRACCIAT.TRCAMDST;
               ,_Curs_TRACCIAT.TRCAMOBB;
               ,_Curs_TRACCIAT.TRANNOTA;
               ,_Curs_TRACCIAT.TRTRASCO;
               ,this.oParentObject.w_CODIMPN;
               ,_Curs_TRACCIAT.TRTIPSRC;
               ,_Curs_TRACCIAT.TRCAMSRC;
               ,_Curs_TRACCIAT.TRTIPSRG;
               ,_Curs_TRACCIAT.TREXPSRG;
               ,_Curs_TRACCIAT.TRCAMDLE;
               ,_Curs_TRACCIAT.TRCAMDDE;
               ,_Curs_TRACCIAT.TRTIPDES;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella duplicazione dei tracciati.'
          return
        endif
          select _Curs_TRACCIAT
          continue
        enddo
        use
      endif
    else
      * --- Select from TRACCIAT
      i_nConn=i_TableProp[this.TRACCIAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2],.t.,this.TRACCIAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TRACCIAT ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=TRCODICE AND "+cp_ToStrODBC(this.oParentObject.w_ARCINI)+"<=TRDESTIN AND "+cp_ToStrODBC(this.oParentObject.w_ARCFIN)+">=TRDESTIN";
             ,"_Curs_TRACCIAT")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=TRCODICE AND this.oParentObject.w_ARCINI<=TRDESTIN AND this.oParentObject.w_ARCFIN>=TRDESTIN;
          into cursor _Curs_TRACCIAT
      endif
      if used('_Curs_TRACCIAT')
        select _Curs_TRACCIAT
        locate for 1=1
        do while not(eof())
        * --- Insert into TRACCIAT
        i_nConn=i_TableProp[this.TRACCIAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRACCIAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TR_ASCII"+",TRDESTIN"+",TRCAMNUM"+",TRCAMDES"+",TRCAMTIP"+",TRCAMLUN"+",TRCAMDEC"+",TRCAMDST"+",TRCAMOBB"+",TRANNOTA"+",TRTRASCO"+",TRCODICE"+",TRTIPSRC"+",TRCAMSRC"+",TRTIPSRG"+",TREXPSRG"+",TRCAMDLE"+",TRCAMDDE"+",TRTIPDES"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TR_ASCII),'TRACCIAT','TR_ASCII');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRDESTIN),'TRACCIAT','TRDESTIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMNUM),'TRACCIAT','TRCAMNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDES),'TRACCIAT','TRCAMDES');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMTIP),'TRACCIAT','TRCAMTIP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMLUN),'TRACCIAT','TRCAMLUN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDEC),'TRACCIAT','TRCAMDEC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDST),'TRACCIAT','TRCAMDST');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMOBB),'TRACCIAT','TRCAMOBB');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRANNOTA),'TRACCIAT','TRANNOTA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTRASCO),'TRACCIAT','TRTRASCO');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'TRACCIAT','TRCODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPSRC),'TRACCIAT','TRTIPSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMSRC),'TRACCIAT','TRCAMSRC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPSRG),'TRACCIAT','TRTIPSRG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TREXPSRG),'TRACCIAT','TREXPSRG');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDLE),'TRACCIAT','TRCAMDLE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRCAMDDE),'TRACCIAT','TRCAMDDE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRACCIAT.TRTIPDES),'TRACCIAT','TRTIPDES');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TR_ASCII',_Curs_TRACCIAT.TR_ASCII,'TRDESTIN',_Curs_TRACCIAT.TRDESTIN,'TRCAMNUM',_Curs_TRACCIAT.TRCAMNUM,'TRCAMDES',_Curs_TRACCIAT.TRCAMDES,'TRCAMTIP',_Curs_TRACCIAT.TRCAMTIP,'TRCAMLUN',_Curs_TRACCIAT.TRCAMLUN,'TRCAMDEC',_Curs_TRACCIAT.TRCAMDEC,'TRCAMDST',_Curs_TRACCIAT.TRCAMDST,'TRCAMOBB',_Curs_TRACCIAT.TRCAMOBB,'TRANNOTA',_Curs_TRACCIAT.TRANNOTA,'TRTRASCO',_Curs_TRACCIAT.TRTRASCO,'TRCODICE',this.oParentObject.w_CODIMPN)
          insert into (i_cTable) (TR_ASCII,TRDESTIN,TRCAMNUM,TRCAMDES,TRCAMTIP,TRCAMLUN,TRCAMDEC,TRCAMDST,TRCAMOBB,TRANNOTA,TRTRASCO,TRCODICE,TRTIPSRC,TRCAMSRC,TRTIPSRG,TREXPSRG,TRCAMDLE,TRCAMDDE,TRTIPDES &i_ccchkf. );
             values (;
               _Curs_TRACCIAT.TR_ASCII;
               ,_Curs_TRACCIAT.TRDESTIN;
               ,_Curs_TRACCIAT.TRCAMNUM;
               ,_Curs_TRACCIAT.TRCAMDES;
               ,_Curs_TRACCIAT.TRCAMTIP;
               ,_Curs_TRACCIAT.TRCAMLUN;
               ,_Curs_TRACCIAT.TRCAMDEC;
               ,_Curs_TRACCIAT.TRCAMDST;
               ,_Curs_TRACCIAT.TRCAMOBB;
               ,_Curs_TRACCIAT.TRANNOTA;
               ,_Curs_TRACCIAT.TRTRASCO;
               ,this.oParentObject.w_CODIMPN;
               ,_Curs_TRACCIAT.TRTIPSRC;
               ,_Curs_TRACCIAT.TRCAMSRC;
               ,_Curs_TRACCIAT.TRTIPSRG;
               ,_Curs_TRACCIAT.TREXPSRG;
               ,_Curs_TRACCIAT.TRCAMDLE;
               ,_Curs_TRACCIAT.TRCAMDDE;
               ,_Curs_TRACCIAT.TRTIPDES;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella duplicazione dei tracciati.'
          return
        endif
          select _Curs_TRACCIAT
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Replico Trascodifiche
    if empty(this.oParentObject.w_ARCFIN)
      * --- Select from TRASCODI
      i_nConn=i_TableProp[this.TRASCODI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TRASCODI ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=TRCODIMP and TRTIPTRA='S'";
             ,"_Curs_TRASCODI")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=TRCODIMP and TRTIPTRA="S";
          into cursor _Curs_TRASCODI
      endif
      if used('_Curs_TRASCODI')
        select _Curs_TRASCODI
        locate for 1=1
        do while not(eof())
        * --- Insert into TRASCODI
        i_nConn=i_TableProp[this.TRASCODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TRCODIMP"+",TRTIPTRA"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRCODENT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'TRASCODI','TRCODIMP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRTIPTRA),'TRASCODI','TRTIPTRA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODFIL),'TRASCODI','TRCODFIL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRNOMCAM),'TRASCODI','TRNOMCAM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODEXT),'TRASCODI','TRCODEXT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODENT),'TRASCODI','TRCODENT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TRCODIMP',this.oParentObject.w_CODIMPN,'TRTIPTRA',_Curs_TRASCODI.TRTIPTRA,'TRCODFIL',_Curs_TRASCODI.TRCODFIL,'TRNOMCAM',_Curs_TRASCODI.TRNOMCAM,'TRCODEXT',_Curs_TRASCODI.TRCODEXT,'TRCODENT',_Curs_TRASCODI.TRCODENT)
          insert into (i_cTable) (TRCODIMP,TRTIPTRA,TRCODFIL,TRNOMCAM,TRCODEXT,TRCODENT &i_ccchkf. );
             values (;
               this.oParentObject.w_CODIMPN;
               ,_Curs_TRASCODI.TRTIPTRA;
               ,_Curs_TRASCODI.TRCODFIL;
               ,_Curs_TRASCODI.TRNOMCAM;
               ,_Curs_TRASCODI.TRCODEXT;
               ,_Curs_TRASCODI.TRCODENT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella duplicazione delle trascodifiche.'
          return
        endif
          select _Curs_TRASCODI
          continue
        enddo
        use
      endif
    else
      * --- Select from TRASCODI
      i_nConn=i_TableProp[this.TRASCODI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TRASCODI ";
            +" where "+cp_ToStrODBC(this.oParentObject.w_CODIMP)+"=TRCODIMP and TRTIPTRA='S' and TRCODFIL >= "+cp_ToStrODBC(this.oParentObject.w_ARCINI)+" and TRCODFIL <= "+cp_ToStrODBC(this.oParentObject.w_ARCFIN)+"";
             ,"_Curs_TRASCODI")
      else
        select * from (i_cTable);
         where this.oParentObject.w_CODIMP=TRCODIMP and TRTIPTRA="S" and TRCODFIL >= this.oParentObject.w_ARCINI and TRCODFIL <= this.oParentObject.w_ARCFIN;
          into cursor _Curs_TRASCODI
      endif
      if used('_Curs_TRASCODI')
        select _Curs_TRASCODI
        locate for 1=1
        do while not(eof())
        * --- Insert into TRASCODI
        i_nConn=i_TableProp[this.TRASCODI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRASCODI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TRCODIMP"+",TRTIPTRA"+",TRCODFIL"+",TRNOMCAM"+",TRCODEXT"+",TRCODENT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMPN),'TRASCODI','TRCODIMP');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRTIPTRA),'TRASCODI','TRTIPTRA');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODFIL),'TRASCODI','TRCODFIL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRNOMCAM),'TRASCODI','TRNOMCAM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODEXT),'TRASCODI','TRCODEXT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TRASCODI.TRCODENT),'TRASCODI','TRCODENT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TRCODIMP',this.oParentObject.w_CODIMPN,'TRTIPTRA',_Curs_TRASCODI.TRTIPTRA,'TRCODFIL',_Curs_TRASCODI.TRCODFIL,'TRNOMCAM',_Curs_TRASCODI.TRNOMCAM,'TRCODEXT',_Curs_TRASCODI.TRCODEXT,'TRCODENT',_Curs_TRASCODI.TRCODENT)
          insert into (i_cTable) (TRCODIMP,TRTIPTRA,TRCODFIL,TRNOMCAM,TRCODEXT,TRCODENT &i_ccchkf. );
             values (;
               this.oParentObject.w_CODIMPN;
               ,_Curs_TRASCODI.TRTIPTRA;
               ,_Curs_TRASCODI.TRCODFIL;
               ,_Curs_TRASCODI.TRNOMCAM;
               ,_Curs_TRASCODI.TRCODEXT;
               ,_Curs_TRASCODI.TRCODENT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella duplicazione delle trascodifiche.'
          return
        endif
          select _Curs_TRASCODI
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CAUIVA'
    this.cWorkTables[2]='CAUIVA1'
    this.cWorkTables[3]='CAUPRI'
    this.cWorkTables[4]='CAUPRI1'
    this.cWorkTables[5]='IMPORTAZ'
    this.cWorkTables[6]='PAG_2AME'
    this.cWorkTables[7]='RESOCONT'
    this.cWorkTables[8]='TRACCIAT'
    this.cWorkTables[9]='TRASCODI'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_IMPORTAZ')
      use in _Curs_IMPORTAZ
    endif
    if used('_Curs_IMPORTAZ')
      use in _Curs_IMPORTAZ
    endif
    if used('_Curs_TRACCIAT')
      use in _Curs_TRACCIAT
    endif
    if used('_Curs_TRACCIAT')
      use in _Curs_TRACCIAT
    endif
    if used('_Curs_TRASCODI')
      use in _Curs_TRASCODI
    endif
    if used('_Curs_TRASCODI')
      use in _Curs_TRASCODI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
