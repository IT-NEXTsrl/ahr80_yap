* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bma                                                        *
*              Import archivi magazzino                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_395]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2016-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsim_bma
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tamGSIM_BMA",oParentObject)
return(i_retval)

define class tamGSIM_BMA as tGSIM_BMA

  proc Done()
    if !empty(i_Error)
      ah_errorMsg(i_Error)
    endif
    if !isnull(this.oParentObject) and this.bUpdateParentObject
      this.oParentObject.mCalc(.t.)
    endif
  return

enddefine

Proc tamGSIM_BMA
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bma",oParentObject)
return(i_retval)

define class tgsim_bma as StdBatch
  * --- Local variables
  w_SimVal = space(5)
  w_CaoVal = 0
  w_DecTot = 0
  w_FLRIFE = space(2)
  w_ANTIPSOT = space(1)
  w_IVPERIND = 0
  w_IVPERIVA = 0
  COR_CLFO = space(15)
  w_RIFERIME = 0
  w_KEY_READ = space(41)
  w_RIGA = 0
  w_INSDET = .f.
  w_INSMAS = .f.
  w_INSERI = space(1)
  w_CODART = space(20)
  ChiavePrec = space(1)
  w_TIPCAT = space(1)
  w_FOUND = space(6)
  w_MaxRowNum = 0
  w_Tariffa = space(10)
  w_ARTIPPKR = space(2)
  w_UMLIS = space(3)
  w_UMLIS = space(3)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CATEGOMO_idx=0
  CAT_SCMA_idx=0
  CON_COSC_idx=0
  CON_TRAD_idx=0
  CON_TRAM_idx=0
  FIFOCONT_idx=0
  GRUMERC_idx=0
  GRUPRO_idx=0
  INVEDETT_idx=0
  INVENTAR_idx=0
  KEY_ARTI_idx=0
  LIFOCONT_idx=0
  LIFOSCAT_idx=0
  LISTINI_idx=0
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  MAGAZZIN_idx=0
  MOP_DETT_idx=0
  MOP_MAST_idx=0
  NOMENCLA_idx=0
  NOT_ARTI_idx=0
  PAR_RIOR_idx=0
  TAB_SCON_idx=0
  TIPICONF_idx=0
  TIPITRAN_idx=0
  TIP_DOCU_idx=0
  UNIMIS_idx=0
  CACOARTI_idx=0
  CONTVEAC_idx=0
  TIP_COLL_idx=0
  TRADCAMA_idx=0
  TRADPAGA_idx=0
  TRADARTI_idx=0
  ART_ALTE_idx=0
  TAR_IFFE_idx=0
  TAR_DETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT ARCHIVI MAGAZZINO
    * --- Variabili del batch precedente
    * --- Modello di segnalazione
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Aggiornamento righe IVA S/N
    * --- Aggiornamento partite S/N
    * --- Aggiornamento analitica S/N
    * --- Aggiornamento saldi S/N
    * --- Ricalcolo numeri registrazioni
    * --- Conto di costo per IVA indetraibile
    * --- File ascii da importare
    * --- Variabili per gestione rottura registrazioni
    * --- Chiave corrente movimentazione
    * --- Ultima chiave di rottura movimentazione
    * --- Ultimo serial
    * --- Ultimo numero registrazione
    * --- Ultimo numero protocollo
    * --- Ultimo tipo cliente/fornitore
    * --- Ultimo Articolo
    * --- Ultimo numero di riga
    * --- Ultimo Codice < 50
    * --- Ultimo Intestatatio Contratto
    * --- Ultimo Codice Contratto Originario
    * --- Progressivo Contratti
    * --- Variabili per gestione valute
    this.w_SimVal = 0
    * --- Simbolo valuta
    this.w_CaoVal = 0
    * --- Cambio valuta
    this.w_DecTot = 0
    * --- Cambio valuta
    * --- Variabili locali
    this.w_FLRIFE = "  "
    * --- Riferimento tipo cliente/fornitore
    this.w_ANTIPSOT = " "
    * --- Tipo sottoconto
    this.w_IVPERIND = 0
    * --- Percentuale indetraibilit� IVA
    this.w_IVPERIVA = 0
    * --- Percentuale IVA
    this.COR_CLFO = space(15)
    * --- Corrente cliente o fornitore del master
    this.w_RIFERIME = 0
    * --- Numero riga di riferimento
    this.w_KEY_READ = space(41)
    * --- Codice letto in key_arti
    this.w_RIGA = 0
    * --- Variabile per inserire una causale documento presente nella tabella ma non utilizzata nei documenti
    this.w_INSERI = "N"
    * --- Aggiornamento archivi
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    * --- Il primo cliente/fornitore della registrazione deve essere scritto in testata.
    * --- Viene quindi memorizzato il codice corrente e la prima volta che cambia riaggiornato il master.
    this.COR_CLFO = this.oParentObject.ULT_CLFO
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_04DA75B0
        bErr_04DA75B0=bTrsErr
        this.Try_04DA75B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04DA75B0
        * --- End
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
    endcase
  endproc
  proc Try_04DA75B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "AR" OR this.oParentObject.w_Destinaz = "T2"
        * --- Articoli di Magazzino
        w_ARCODART = left( w_ARCODART + space(20) , 20)
        w_INCODESE = iif( type("w_INCODESE")="U" or empty( w_INCODESE ), g_CODESE, w_INCODESE)
        * --- Calcolo Serial Inventario
        * --- Se viene passato un valore nella variabile w_ARULTCOS la procedura consente di caricare
        * --- un inventario con l'ultimo costo.
        if empty( this.oParentObject.ULT_SERI ) .and. type("w_ARULTCOS")<>"U"
          w_INNUMINV = space(6)
          w_INNUMINV = GSIM_BPR(this,"SEINV", "i_codazi,w_INCODESE,w_INNUMINV",w_INCODESE,w_INNUMINV)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.ULT_SERI = w_INNUMINV
          * --- Inserimento record inventario
          w_INDESINV = AH_MSGFORMAT("Import archivi del %1" , dtoc(i_datsys))
          w_INCODMAG = g_MAGAZI
          * --- Try
          local bErr_04DBEFD8
          bErr_04DBEFD8=bTrsErr
          this.Try_04DBEFD8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile registrare l'inventario con i dati degli ultimi costi %1" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) )
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DBEFD8
          * --- End
        else
          w_INNUMINV = this.oParentObject.ULT_SERI
        endif
        * --- Inserimento record articoli
        * --- Insert into ART_ICOL
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ARCODART"+",ARDESART"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ARCODART),'ART_ICOL','ARCODART');
          +","+cp_NullLink(cp_ToStrODBC(w_ARDESART),'ART_ICOL','ARDESART');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'ART_ICOL','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'ART_ICOL','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ARCODART',w_ARCODART,'ARDESART',w_ARDESART,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (ARCODART,ARDESART,UTCC,UTDC &i_ccchkf. );
             values (;
               w_ARCODART;
               ,w_ARDESART;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AN"
        * --- Articoli Note
        w_ARCODART = left( w_ARCODART + space(20) , 20)
        * --- Insert into NOT_ARTI
        i_nConn=i_TableProp[this.NOT_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOT_ARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOT_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ARCODART"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ARCODART),'NOT_ARTI','ARCODART');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ARCODART',w_ARCODART)
          insert into (i_cTable) (ARCODART &i_ccchkf. );
             values (;
               w_ARCODART;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CM"
        * --- Causali di Magazzino
        w_CMCODICE = left( ltrim(w_CMCODICE)+space(5), 5)
        * --- Insert into CAM_AGAZ
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAM_AGAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CMCODICE"+",CMDESCRI"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CMCODICE),'CAM_AGAZ','CMCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_CMDESCRI),'CAM_AGAZ','CMDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CAM_AGAZ','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CAM_AGAZ','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CMCODICE',w_CMCODICE,'CMDESCRI',w_CMDESCRI,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (CMCODICE,CMDESCRI,UTCC,UTDC &i_ccchkf. );
             values (;
               w_CMCODICE;
               ,w_CMDESCRI;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "MA"
        * --- Magazzini
        w_MGCODMAG = left( ltrim(w_MGCODMAG)+space(5), 5)
        * --- Insert into MAGAZZIN
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAGAZZIN_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MGCODMAG"+",MGDESMAG"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_MGCODMAG),'MAGAZZIN','MGCODMAG');
          +","+cp_NullLink(cp_ToStrODBC(w_MGDESMAG),'MAGAZZIN','MGDESMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'MAGAZZIN','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'MAGAZZIN','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MGCODMAG',w_MGCODMAG,'MGDESMAG',w_MGDESMAG,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (MGCODMAG,MGDESMAG,UTCC,UTDC &i_ccchkf. );
             values (;
               w_MGCODMAG;
               ,w_MGDESMAG;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "KA"
        * --- Chiavi di Ricerca
        w_CACODICE = left( ltrim(w_CACODICE)+space(20), 20)
        * --- Insert into KEY_ARTI
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CACODICE"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CACODICE),'KEY_ARTI','CACODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'KEY_ARTI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'KEY_ARTI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CACODICE',w_CACODICE,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (CACODICE,UTCC,UTDC &i_ccchkf. );
             values (;
               w_CACODICE;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="DC"
        * --- Causali Documenti (dai documenti di AHW) 
        w_TDTIPDOC = left( ltrim(w_TDTIPDOC)+space(5), 5)
        * --- Insert into TIP_DOCU
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DOCU_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TDTIPDOC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TDTIPDOC),'TIP_DOCU','TDTIPDOC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',w_TDTIPDOC)
          insert into (i_cTable) (TDTIPDOC &i_ccchkf. );
             values (;
               w_TDTIPDOC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CD"
        * --- Causali Documenti (da archivio tabelle di AHW)
        w_TDTIPDOC = left( alltrim(w_TDTIPDOC)+alltrim(w_TDCAUMAG)+space(5), 5)
        i_rows = 0
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                TDTIPDOC = w_TDTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=1
          * --- Causale documento utilizzata nei documenti
          this.w_INSERI = "N"
        else
          * --- Causale documento presente nella tabella ma non utilizzata nei documenti
          this.w_INSERI = "S"
          * --- Insert into TIP_DOCU
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_DOCU_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"TDTIPDOC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_TDTIPDOC),'TIP_DOCU','TDTIPDOC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',w_TDTIPDOC)
            insert into (i_cTable) (TDTIPDOC &i_ccchkf. );
               values (;
                 w_TDTIPDOC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz="LS"
        * --- Anag. Listini
        w_LSCODLIS = left( w_LSCODLIS+space(5), 5)
        * --- Insert into LISTINI
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LISTINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LSCODLIS"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LSCODLIS),'LISTINI','LSCODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'LISTINI','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'LISTINI','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LSCODLIS',w_LSCODLIS,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (LSCODLIS,UTCC,UTDC &i_ccchkf. );
             values (;
               w_LSCODLIS;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="LA"
        * --- Articoli Listini
        w_LICODART = left( w_LICODART+space(20), 20)
        if w_LICODART<>this.oParentObject.ULT_CLFO
          this.oParentObject.ULT_CLFO = w_LICODART
          this.oParentObject.ULT_RNUM = 0
          * --- Verifico la presenza di  righe Listini
          this.w_CODART = w_LICODART
          * --- Select from LIS_TINI
          i_nConn=i_TableProp[this.LIS_TINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_TINI ";
                +" where LICODART="+cp_ToStrODBC(this.w_CODART)+"";
                +" order by CPROWNUM DESC";
                 ,"_Curs_LIS_TINI")
          else
            select * from (i_cTable);
             where LICODART=this.w_CODART;
             order by CPROWNUM DESC;
              into cursor _Curs_LIS_TINI
          endif
          if used('_Curs_LIS_TINI')
            select _Curs_LIS_TINI
            locate for 1=1
            do while not(eof())
            this.oParentObject.ULT_RNUM = _Curs_LIS_TINI.CPROWNUM
            EXIT
              select _Curs_LIS_TINI
              continue
            enddo
            use
          endif
        endif
        this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        * --- Insert into LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LICODART),'LIS_TINI','LICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'LIS_TINI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(w_LICODLIS),'LIS_TINI','LICODLIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LICODART',w_LICODART,'CPROWNUM',this.oParentObject.ULT_RNUM,'LICODLIS',w_LICODLIS)
          insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS &i_ccchkf. );
             values (;
               w_LICODART;
               ,this.oParentObject.ULT_RNUM;
               ,w_LICODLIS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CN"
        * --- Contratti
        if NOT EMPTY(w_EXCODCLI) OR NOT EMPTY(w_EXCODFOR)
          if IIF(NOT EMPTY(w_EXCODCLI), ALLTRIM(w_EXCODCLI)+"C" ,ALLTRIM(w_EXCODFOR)+"F")<>this.oParentObject.ULT_INTES OR w_CONUMERO <> this.oParentObject.ULT_CONT
            * --- Il vecchio codice contratto (viene sostiutito con  un progressivo utilizzato nell'elaborazione  di 7 caratteri + C/F + i  7 caratteri pi� a sinistra del codice cliente/fornitore  )
            this.oParentObject.ULT_PROG = this.oParentObject.ULT_PROG + 1
            this.oParentObject.ULT_CONT = w_CONUMERO
            this.oParentObject.ULT_INTES = IIF(NOT EMPTY(w_EXCODCLI), ALLTRIM(w_EXCODCLI)+"C" ,ALLTRIM(w_EXCODFOR)+"F")
          endif
           
 w_CONUMERO =RIGHT("0000000"+ ALLTRIM(STR(this.oParentObject.ULT_PROG)),7) + ; 
 IIF(NOT EMPTY(w_EXCODCLI), "C"+LEFT(ALLTRIM(w_EXCODCLI)+ SPACE(7),7),"F"+ LEFT(ALLTRIM(w_EXCODFOR) + SPACE(7),7))
          if NOT EMPTY(w_EXCODFOR)
            w_GSQUANTI = "S"
            w_COQUANTI = iif(w_COQUANTI = 0 ,999999.999,w_COQUANTI)
            w_COIVALIS = "N"
          else
            w_GSQUANTI = "S"
            w_COQUANTI = iif(w_COQUANTI = 0 ,999999.999,w_COQUANTI)
          endif
        endif
        if w_CONUMERO <> this.oParentObject.ULT_CLFO
          this.ChiavePrec = this.oParentObject.w_Destinaz + this.oParentObject.ULT_CLFO
          this.w_INSMAS = .T.
          this.oParentObject.ULT_CLFO = w_CONUMERO
          this.oParentObject.ULT_RNUM = 0
          this.oParentObject.ULT_COD1 = space(50)
          * --- ultimo codice articolo
          this.oParentObject.ULT_SERI = space(10)
          * --- ultima categoria merceologica
          * --- Try
          local bErr_04DFD9D8
          bErr_04DFD9D8=bTrsErr
          this.Try_04DFD9D8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DFD9D8
          * --- End
        else
          this.w_INSMAS = .F.
        endif
        * --- Se stesso articolo devo inserire uno scaglione o stesso gruppo merceologico
        if this.oParentObject.ULT_COD1 <> w_COCODART OR w_COGRUMER<>this.oParentObject.ULT_SERI
          this.w_INSDET = .T.
          this.oParentObject.ULT_COD1 = w_COCODART
          * --- ultimo codice articolo
          this.oParentObject.ULT_SERI = w_COGRUMER
          * --- ultima categoria merceologica
          this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
          * --- Try
          local bErr_04DDE1B8
          bErr_04DDE1B8=bTrsErr
          this.Try_04DDE1B8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DDE1B8
          * --- End
        else
          this.w_INSDET = .F.
        endif
        if w_GSQUANTI="S"
          * --- Inserimento scaglione
          * --- Try
          local bErr_04DD9F88
          bErr_04DD9F88=bTrsErr
          this.Try_04DD9F88()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04DD9F88
          * --- End
        endif
      case this.oParentObject.w_Destinaz="UM"
        * --- Unit� di Misura
        w_UMCODICE = left( ltrim(w_UMCODICE)+space(3), 3)
        * --- Insert into UNIMIS
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"UMCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_UMCODICE),'UNIMIS','UMCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'UMCODICE',w_UMCODICE)
          insert into (i_cTable) (UMCODICE &i_ccchkf. );
             values (;
               w_UMCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CP"
        * --- Movimentazione Provvigioni
        w_GPCODICE = left( ltrim(w_GPCODICE)+space(5), 5)
        * --- Insert into GRUPRO
        i_nConn=i_TableProp[this.GRUPRO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUPRO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GRUPRO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"GPCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_GPCODICE),'GRUPRO','GPCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'GPCODICE',w_GPCODICE)
          insert into (i_cTable) (GPCODICE &i_ccchkf. );
             values (;
               w_GPCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        * --- Insert into MOP_DETT
        i_nConn=i_TableProp[this.MOP_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOP_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MPSERIAL"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_MPSERIAL),'MOP_DETT','MPSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'MOP_DETT','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MPSERIAL',w_MPSERIAL,'CPROWNUM',this.oParentObject.ULT_RNUM)
          insert into (i_cTable) (MPSERIAL,CPROWNUM &i_ccchkf. );
             values (;
               w_MPSERIAL;
               ,this.oParentObject.ULT_RNUM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="SM"
        * --- Sconti e Maggiorazioni
        w_TSCATCLI = left( ltrim(w_TSCATCLI)+space(5), 5)
        w_TSCATARR = left( ltrim(w_TSCATARR)+space(5), 5)
        * --- Read from CAT_SCMA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAT_SCMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_SCMA_idx,2],.t.,this.CAT_SCMA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CSTIPCAT"+;
            " from "+i_cTable+" CAT_SCMA where ";
                +"CSCODICE = "+cp_ToStrODBC(w_TSCATCLI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CSTIPCAT;
            from (i_cTable) where;
                CSCODICE = w_TSCATCLI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCAT = NVL(cp_ToDate(_read_.CSTIPCAT),cp_NullValue(_read_.CSTIPCAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- eventuale inserimento categoria clienti
        if EMPTY(NVL(this.w_TIPCAT," "))
          w_CSTIPCAT = "C"
          * --- Try
          local bErr_04E0CC58
          bErr_04E0CC58=bTrsErr
          this.Try_04E0CC58()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E0CC58
          * --- End
        endif
        * --- Read from CAT_SCMA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAT_SCMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_SCMA_idx,2],.t.,this.CAT_SCMA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CSTIPCAT"+;
            " from "+i_cTable+" CAT_SCMA where ";
                +"CSCODICE = "+cp_ToStrODBC(w_TSCATARR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CSTIPCAT;
            from (i_cTable) where;
                CSCODICE = w_TSCATARR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCAT = NVL(cp_ToDate(_read_.CSTIPCAT),cp_NullValue(_read_.CSTIPCAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- eventuale inserimento categoria articoli
        if EMPTY(NVL(this.w_TIPCAT," "))
          w_CSTIPCAT = "A"
          * --- Try
          local bErr_04E1BD78
          bErr_04E1BD78=bTrsErr
          this.Try_04E1BD78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E1BD78
          * --- End
        endif
        if w_TSCATCLI+w_TSCATARR<>this.oParentObject.ULT_CHIMOV
          this.oParentObject.ULT_CHIMOV = w_TSCATCLI+w_TSCATARR
          this.oParentObject.ULT_RNUM = 0
        endif
        this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        * --- Insert into TAB_SCON
        i_nConn=i_TableProp[this.TAB_SCON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_SCON_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_SCON_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TSCATCLI"+",TSCATARR"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TSCATCLI),'TAB_SCON','TSCATCLI');
          +","+cp_NullLink(cp_ToStrODBC(w_TSCATARR),'TAB_SCON','TSCATARR');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'TAB_SCON','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TSCATCLI',w_TSCATCLI,'TSCATARR',w_TSCATARR,'CPROWNUM',this.oParentObject.ULT_RNUM)
          insert into (i_cTable) (TSCATCLI,TSCATARR,CPROWNUM &i_ccchkf. );
             values (;
               w_TSCATCLI;
               ,w_TSCATARR;
               ,this.oParentObject.ULT_RNUM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="GM"
        * --- Gruppi Merceologici
        w_GMCODICE = left( ltrim(w_GMCODICE)+space(5), 5)
        * --- Insert into GRUMERC
        i_nConn=i_TableProp[this.GRUMERC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUMERC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GRUMERC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"GMCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_GMCODICE),'GRUMERC','GMCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'GMCODICE',w_GMCODICE)
          insert into (i_cTable) (GMCODICE &i_ccchkf. );
             values (;
               w_GMCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CG"
        * --- Categorie Omogenee
        w_OMCODICE = left( ltrim(w_OMCODICE)+space(5), 5)
        * --- Insert into CATEGOMO
        i_nConn=i_TableProp[this.CATEGOMO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CATEGOMO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CATEGOMO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"OMCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_OMCODICE),'CATEGOMO','OMCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'OMCODICE',w_OMCODICE)
          insert into (i_cTable) (OMCODICE &i_ccchkf. );
             values (;
               w_OMCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TT"
        * --- Tipi Transazioni Intra
        w_TTCODICE = left( ltrim(w_TTCODICE)+space(3), 3)
        * --- Insert into TIPITRAN
        i_nConn=i_TableProp[this.TIPITRAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPITRAN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIPITRAN_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TTCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TTCODICE),'TIPITRAN','TTCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TTCODICE',w_TTCODICE)
          insert into (i_cTable) (TTCODICE &i_ccchkf. );
             values (;
               w_TTCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="NO"
        * --- Nomenclature
        w_NMCODICE = left( ltrim(w_NMCODICE)+space(8), 8)
        * --- Insert into NOMENCLA
        i_nConn=i_TableProp[this.NOMENCLA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOMENCLA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NMCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_NMCODICE),'NOMENCLA','NMCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NMCODICE',w_NMCODICE)
          insert into (i_cTable) (NMCODICE &i_ccchkf. );
             values (;
               w_NMCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TC"
        * --- Tipi Confezioni
        w_TCCODICE = left( ltrim(w_TCCODICE)+space(3), 3)
        * --- Insert into TIPICONF
        i_nConn=i_TableProp[this.TIPICONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPICONF_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIPICONF_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TCCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TCCODICE),'TIPICONF','TCCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TCCODICE',w_TCCODICE)
          insert into (i_cTable) (TCCODICE &i_ccchkf. );
             values (;
               w_TCCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IN"
        * --- Inventari
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          * --- Nuovo inventario
          * --- Aggiorna progressivo Numero Inventario
          w_INNUMINV = GSIM_BPR(this,"SEINV", "i_codazi,w_INCODESE,w_INNUMINV",w_INCODESE,w_INNUMINV)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Stesso inventario
          w_INNUMINV = this.oParentObject.ULT_SERI
        endif
        * --- Default
        w_INDATINV = iif(empty(w_INDATINV),i_datsys,w_INDATINV)
        w_INDATMEM = iif(empty(w_INDATMEM),i_datsys,w_INDATMEM)
        w_INDESINV = iif(empty(w_INDESINV),"Import archivi del "+dtoc(i_datsys),w_INDESINV)
        w_INCODMAG = iif(empty(w_INCODMAG),g_MAGAZI,w_INCODMAG)
        * --- Aggiorna variabili per importazione di tipo movimento
        this.oParentObject.ULT_SERI = w_INNUMINV
        this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
        * --- Try
        local bErr_04E18E38
        bErr_04E18E38=bTrsErr
        this.Try_04E18E38()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E18E38
        * --- End
        * --- Inserimento record dettaglio
        w_DINUMINV = iif(empty(w_DINUMINV),w_INNUMINV,w_DINUMINV)
        w_DICODESE = iif(empty(w_DICODESE),w_INCODESE,w_DICODESE)
        * --- Insert into INVEDETT
        i_nConn=i_TableProp[this.INVEDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVEDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DINUMINV"+",DICODESE"+",DICODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_DINUMINV),'INVEDETT','DINUMINV');
          +","+cp_NullLink(cp_ToStrODBC(w_DICODESE),'INVEDETT','DICODESE');
          +","+cp_NullLink(cp_ToStrODBC(w_DICODICE),'INVEDETT','DICODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DINUMINV',w_DINUMINV,'DICODESE',w_DICODESE,'DICODICE',w_DICODICE)
          insert into (i_cTable) (DINUMINV,DICODESE,DICODICE &i_ccchkf. );
             values (;
               w_DINUMINV;
               ,w_DICODESE;
               ,w_DICODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IC" or this.oParentObject.w_Destinaz="IS" or this.oParentObject.w_Destinaz="IF"
        * --- Inventari con valorizzazione LIFO Continuo, LIFO Scatti o FIFO Continuo
        if !empty(w_ISNUMINV) and !empty(w_ISCODESE)
          * --- Numero inventario e codice esercizio importato
          w_INNUMINV = w_ISNUMINV
          w_INCODESE = w_ISCODESE
          * --- Contatore Riga
          if empty(w_CPROWNUM)
            if this.oParentObject.ULT_CHIMOV<>this.oParentObject.w_ChiaveRow
              w_CPROWNUM= 1
              this.oParentObject.ULT_RNUM = 1
            else
              this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
            endif
          else
            this.oParentObject.ULT_RNUM = w_CPROWNUM
          endif
          * --- Controllo esistenza inventario
          this.w_FOUND = ""
          * --- Read from INVENTAR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.INVENTAR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "INNUMINV"+;
              " from "+i_cTable+" INVENTAR where ";
                  +"INNUMINV = "+cp_ToStrODBC(w_INNUMINV);
                  +" and INCODESE = "+cp_ToStrODBC(w_INCODESE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              INNUMINV;
              from (i_cTable) where;
                  INNUMINV = w_INNUMINV;
                  and INCODESE = w_INCODESE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FOUND = NVL(cp_ToDate(_read_.INNUMINV),cp_NullValue(_read_.INNUMINV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !empty(this.w_FOUND)
            do case
              case this.oParentObject.w_Destinaz = "IC"
                * --- Aggiorna FLAG LIFO Continuo
                * --- Write into INVENTAR
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.INVENTAR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.INVENTAR_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"INFLGLCO ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INFLGLCO');
                  +",INMAGCOL ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INMAGCOL');
                      +i_ccchkf ;
                  +" where ";
                      +"INNUMINV = "+cp_ToStrODBC(w_INNUMINV);
                      +" and INCODESE = "+cp_ToStrODBC(w_INCODESE);
                         )
                else
                  update (i_cTable) set;
                      INFLGLCO = "S";
                      ,INMAGCOL = "S";
                      &i_ccchkf. ;
                   where;
                      INNUMINV = w_INNUMINV;
                      and INCODESE = w_INCODESE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Inserimento record
                * --- Insert into LIFOCONT
                i_nConn=i_TableProp[this.LIFOCONT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOCONT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ISNUMINV"+",ISCODESE"+",ISCODICE"+",CPROWNUM"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(w_ISNUMINV),'LIFOCONT','ISNUMINV');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODESE),'LIFOCONT','ISCODESE');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODICE),'LIFOCONT','ISCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'LIFOCONT','CPROWNUM');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',w_ISNUMINV,'ISCODESE',w_ISCODESE,'ISCODICE',w_ISCODICE,'CPROWNUM',this.oParentObject.ULT_RNUM)
                  insert into (i_cTable) (ISNUMINV,ISCODESE,ISCODICE,CPROWNUM &i_ccchkf. );
                     values (;
                       w_ISNUMINV;
                       ,w_ISCODESE;
                       ,w_ISCODICE;
                       ,this.oParentObject.ULT_RNUM;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case this.oParentObject.w_Destinaz = "IS"
                * --- Aggiorna FLAG LIFO Scatti
                * --- Write into INVENTAR
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.INVENTAR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.INVENTAR_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"INFLGLSC ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INFLGLSC');
                  +",INMAGCOL ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INMAGCOL');
                      +i_ccchkf ;
                  +" where ";
                      +"INNUMINV = "+cp_ToStrODBC(w_INNUMINV);
                      +" and INCODESE = "+cp_ToStrODBC(w_INCODESE);
                         )
                else
                  update (i_cTable) set;
                      INFLGLSC = "S";
                      ,INMAGCOL = "S";
                      &i_ccchkf. ;
                   where;
                      INNUMINV = w_INNUMINV;
                      and INCODESE = w_INCODESE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Inserimento record
                * --- Insert into LIFOSCAT
                i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIFOSCAT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ISNUMINV"+",ISCODESE"+",ISCODICE"+",CPROWNUM"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(w_ISNUMINV),'LIFOSCAT','ISNUMINV');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODESE),'LIFOSCAT','ISCODESE');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODICE),'LIFOSCAT','ISCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'LIFOSCAT','CPROWNUM');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',w_ISNUMINV,'ISCODESE',w_ISCODESE,'ISCODICE',w_ISCODICE,'CPROWNUM',this.oParentObject.ULT_RNUM)
                  insert into (i_cTable) (ISNUMINV,ISCODESE,ISCODICE,CPROWNUM &i_ccchkf. );
                     values (;
                       w_ISNUMINV;
                       ,w_ISCODESE;
                       ,w_ISCODICE;
                       ,this.oParentObject.ULT_RNUM;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case this.oParentObject.w_Destinaz = "IF"
                * --- Aggiorna FLAG FIFO Continuo
                * --- Write into INVENTAR
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.INVENTAR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.INVENTAR_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"INFLGFCO ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INFLGFCO');
                  +",INMAGCOL ="+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INMAGCOL');
                      +i_ccchkf ;
                  +" where ";
                      +"INNUMINV = "+cp_ToStrODBC(w_INNUMINV);
                      +" and INCODESE = "+cp_ToStrODBC(w_INCODESE);
                         )
                else
                  update (i_cTable) set;
                      INFLGFCO = "S";
                      ,INMAGCOL = "S";
                      &i_ccchkf. ;
                   where;
                      INNUMINV = w_INNUMINV;
                      and INCODESE = w_INCODESE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Inserimento record
                * --- Insert into FIFOCONT
                i_nConn=i_TableProp[this.FIFOCONT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FIFOCONT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ISNUMINV"+",ISCODESE"+",ISCODICE"+",CPROWNUM"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(w_ISNUMINV),'FIFOCONT','ISNUMINV');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODESE),'FIFOCONT','ISCODESE');
                  +","+cp_NullLink(cp_ToStrODBC(w_ISCODICE),'FIFOCONT','ISCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'FIFOCONT','CPROWNUM');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ISNUMINV',w_ISNUMINV,'ISCODESE',w_ISCODESE,'ISCODICE',w_ISCODICE,'CPROWNUM',this.oParentObject.ULT_RNUM)
                  insert into (i_cTable) (ISNUMINV,ISCODESE,ISCODICE,CPROWNUM &i_ccchkf. );
                     values (;
                       w_ISNUMINV;
                       ,w_ISCODESE;
                       ,w_ISCODICE;
                       ,this.oParentObject.ULT_RNUM;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
            endcase
          endif
        endif
        * --- Aggiorna variabili per importazione di tipo movimento
        this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
      case this.oParentObject.w_Destinaz="CI"
        * --- Categorie Contabili Articoli
        w_C1CODICE = left( ltrim(w_C1CODICE), 5)
        * --- Insert into CACOARTI
        i_nConn=i_TableProp[this.CACOARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"C1CODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_C1CODICE),'CACOARTI','C1CODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'C1CODICE',w_C1CODICE)
          insert into (i_cTable) (C1CODICE &i_ccchkf. );
             values (;
               w_C1CODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CX"
        * --- Contropartite Ven./Acq.
        w_CVCODART = left( ltrim(w_CVCODART), 5)
        w_CVCODCLI = left( ltrim(w_CVCODCLI), 5)
        * --- Insert into CONTVEAC
        i_nConn=i_TableProp[this.CONTVEAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CVCODART"+",CVCODCLI"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CVCODART),'CONTVEAC','CVCODART');
          +","+cp_NullLink(cp_ToStrODBC(w_CVCODCLI),'CONTVEAC','CVCODCLI');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CVCODART',w_CVCODART,'CVCODCLI',w_CVCODCLI)
          insert into (i_cTable) (CVCODART,CVCODCLI &i_ccchkf. );
             values (;
               w_CVCODART;
               ,w_CVCODCLI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TO"
        * --- Tipologie Colli
        w_TCCODICE = left( ltrim(w_TCCODICE)+space(5), 5)
        * --- Insert into TIP_COLL
        i_nConn=i_TableProp[this.TIP_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_COLL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_COLL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TCCODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_TCCODICE),'TIP_COLL','TCCODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TCCODICE',w_TCCODICE)
          insert into (i_cTable) (TCCODICE &i_ccchkf. );
             values (;
               w_TCCODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TA"
        * --- Traduzioni Articoli
        w_LGCODICE = left( ltrim(w_LGCODICE)+space(20), 20)
        w_LGCODLIN = left( ltrim(w_LGCODLIN)+space(3), 3)
        * --- Insert into TRADARTI
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LGCODICE"+",LGCODLIN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LGCODICE),'TRADARTI','LGCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_LGCODLIN),'TRADARTI','LGCODLIN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LGCODICE',w_LGCODICE,'LGCODLIN',w_LGCODLIN)
          insert into (i_cTable) (LGCODICE,LGCODLIN &i_ccchkf. );
             values (;
               w_LGCODICE;
               ,w_LGCODLIN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TM"
        * --- Traduzioni Causali Magazzino
        w_LGCODICE = left( ltrim(w_LGCODICE)+space(5), 5)
        w_LGCODLIN = left( ltrim(w_LGCODLIN)+space(3), 3)
        * --- Insert into TRADCAMA
        i_nConn=i_TableProp[this.TRADCAMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADCAMA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADCAMA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LGCODICE"+",LGCODLIN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LGCODICE),'TRADCAMA','LGCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_LGCODLIN),'TRADCAMA','LGCODLIN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LGCODICE',w_LGCODICE,'LGCODLIN',w_LGCODLIN)
          insert into (i_cTable) (LGCODICE,LGCODLIN &i_ccchkf. );
             values (;
               w_LGCODICE;
               ,w_LGCODLIN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TP"
        * --- Traduzioni Pagamenti
        w_LGCODICE = left( ltrim(w_LGCODICE)+space(5), 5)
        w_LGCODLIN = left( ltrim(w_LGCODLIN)+space(3), 3)
        * --- Insert into TRADPAGA
        i_nConn=i_TableProp[this.TRADPAGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADPAGA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADPAGA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LGCODICE"+",LGCODLIN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_LGCODICE),'TRADPAGA','LGCODICE');
          +","+cp_NullLink(cp_ToStrODBC(w_LGCODLIN),'TRADPAGA','LGCODLIN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LGCODICE',w_LGCODICE,'LGCODLIN',w_LGCODLIN)
          insert into (i_cTable) (LGCODICE,LGCODLIN &i_ccchkf. );
             values (;
               w_LGCODICE;
               ,w_LGCODLIN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AA"
        * --- Articoli Alternativi
        w_ARCODICE = left( ltrim(w_ARCODICE)+space(20), 20)
        w_ARCODALT = left( ltrim(w_ARCODALT)+space(20), 20)
        if w_ARCODICE<>this.oParentObject.ULT_CLFO
          this.oParentObject.ULT_CLFO = w_ARCODICE
          this.oParentObject.ULT_RNUM = 0
        endif
        this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        * --- Insert into ART_ALTE
        i_nConn=i_TableProp[this.ART_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ALTE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ALTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ARCODICE"+",CPROWNUM"+",ARCODALT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_ARCODICE),'ART_ALTE','ARCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'ART_ALTE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(w_ARCODALT),'ART_ALTE','ARCODALT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ARCODICE',w_ARCODICE,'CPROWNUM',this.oParentObject.ULT_RNUM,'ARCODALT',w_ARCODALT)
          insert into (i_cTable) (ARCODICE,CPROWNUM,ARCODALT &i_ccchkf. );
             values (;
               w_ARCODICE;
               ,this.oParentObject.ULT_RNUM;
               ,w_ARCODALT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
    endcase
    return
  proc Try_04DBEFD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INVENTAR
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVENTAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"INNUMINV"+",INDATINV"+",INDATMEM"+",INCODESE"+",INTIPINV"+",INSTAINV"+",INELABOR"+",INDESINV"+",INCODMAG"+",INMAGCOL"+",INFLGLCO"+",INFLGLSC"+",INFLGFCO"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_INNUMINV),'INVENTAR','INNUMINV');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'INVENTAR','INDATINV');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'INVENTAR','INDATMEM');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODESE),'INVENTAR','INCODESE');
      +","+cp_NullLink(cp_ToStrODBC("G"),'INVENTAR','INTIPINV');
      +","+cp_NullLink(cp_ToStrODBC("D"),'INVENTAR','INSTAINV');
      +","+cp_NullLink(cp_ToStrODBC("S"),'INVENTAR','INELABOR');
      +","+cp_NullLink(cp_ToStrODBC(w_INDESINV),'INVENTAR','INDESINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODMAG),'INVENTAR','INCODMAG');
      +","+cp_NullLink(cp_ToStrODBC("N"),'INVENTAR','INMAGCOL');
      +","+cp_NullLink(cp_ToStrODBC("N"),'INVENTAR','INFLGLCO');
      +","+cp_NullLink(cp_ToStrODBC("N"),'INVENTAR','INFLGLSC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'INVENTAR','INFLGFCO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'INVENTAR','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'INVENTAR','UTDC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'INNUMINV',w_INNUMINV,'INDATINV',i_datsys,'INDATMEM',i_datsys,'INCODESE',w_INCODESE,'INTIPINV',"G",'INSTAINV',"D",'INELABOR',"S",'INDESINV',w_INDESINV,'INCODMAG',w_INCODMAG,'INMAGCOL',"N",'INFLGLCO',"N",'INFLGLSC',"N")
      insert into (i_cTable) (INNUMINV,INDATINV,INDATMEM,INCODESE,INTIPINV,INSTAINV,INELABOR,INDESINV,INCODMAG,INMAGCOL,INFLGLCO,INFLGLSC,INFLGFCO,UTCC,UTDC &i_ccchkf. );
         values (;
           w_INNUMINV;
           ,i_datsys;
           ,i_datsys;
           ,w_INCODESE;
           ,"G";
           ,"D";
           ,"S";
           ,w_INDESINV;
           ,w_INCODMAG;
           ,"N";
           ,"N";
           ,"N";
           ,"N";
           ,this.oParentObject.w_CreVarUte;
           ,this.oParentObject.w_CreVarDat;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04DFD9D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do GSIM_BAM with this, this.oparentobject.oparentobject.w_IMAZZERA, this.oParentObject.w_Destinaz, this.ChiavePrec , w_CONUMERO , "" , ""
    * --- Insert into CON_TRAM
    i_nConn=i_TableProp[this.CON_TRAM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CONUMERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_CONUMERO),'CON_TRAM','CONUMERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CONUMERO',w_CONUMERO)
      insert into (i_cTable) (CONUMERO &i_ccchkf. );
         values (;
           w_CONUMERO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04DDE1B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CON_TRAD
    i_nConn=i_TableProp[this.CON_TRAD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CONUMERO"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_CONUMERO),'CON_TRAD','CONUMERO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'CON_TRAD','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CONUMERO',w_CONUMERO,'CPROWNUM',this.oParentObject.ULT_RNUM)
      insert into (i_cTable) (CONUMERO,CPROWNUM &i_ccchkf. );
         values (;
           w_CONUMERO;
           ,this.oParentObject.ULT_RNUM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04DD9F88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CON_COSC
    i_nConn=i_TableProp[this.CON_COSC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_COSC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COCODICE"+",CONUMROW"+",COQUANTI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_CONUMERO),'CON_COSC','COCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'CON_COSC','CONUMROW');
      +","+cp_NullLink(cp_ToStrODBC(w_COQUANTI),'CON_COSC','COQUANTI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COCODICE',w_CONUMERO,'CONUMROW',this.oParentObject.ULT_RNUM,'COQUANTI',w_COQUANTI)
      insert into (i_cTable) (COCODICE,CONUMROW,COQUANTI &i_ccchkf. );
         values (;
           w_CONUMERO;
           ,this.oParentObject.ULT_RNUM;
           ,w_COQUANTI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E0CC58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAT_SCMA
    i_nConn=i_TableProp[this.CAT_SCMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAT_SCMA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAT_SCMA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODICE"+",CSDESCRI"+",CSTIPCAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_TSCATCLI),'CAT_SCMA','CSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_TSCATCLI),'CAT_SCMA','CSDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(w_CSTIPCAT),'CAT_SCMA','CSTIPCAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODICE',w_TSCATCLI,'CSDESCRI',w_TSCATCLI,'CSTIPCAT',w_CSTIPCAT)
      insert into (i_cTable) (CSCODICE,CSDESCRI,CSTIPCAT &i_ccchkf. );
         values (;
           w_TSCATCLI;
           ,w_TSCATCLI;
           ,w_CSTIPCAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E1BD78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAT_SCMA
    i_nConn=i_TableProp[this.CAT_SCMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAT_SCMA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAT_SCMA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODICE"+",CSDESCRI"+",CSTIPCAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_TSCATARR),'CAT_SCMA','CSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_TSCATARR),'CAT_SCMA','CSDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(w_CSTIPCAT),'CAT_SCMA','CSTIPCAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODICE',w_TSCATARR,'CSDESCRI',w_TSCATARR,'CSTIPCAT',w_CSTIPCAT)
      insert into (i_cTable) (CSCODICE,CSDESCRI,CSTIPCAT &i_ccchkf. );
         values (;
           w_TSCATARR;
           ,w_TSCATARR;
           ,w_CSTIPCAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E18E38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento record inventario
    * --- Insert into INVENTAR
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVENTAR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"INNUMINV"+",INDATINV"+",INDATMEM"+",INCODESE"+",INTIPINV"+",INSTAINV"+",INELABOR"+",INDESINV"+",INCODMAG"+",INMAGCOL"+",INFLGLCO"+",INFLGLSC"+",INFLGFCO"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",INCATOMO"+",INGRUMER"+",INCODFAM"+",INNUMPRE"+",INESEPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_INNUMINV),'INVENTAR','INNUMINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INDATINV),'INVENTAR','INDATINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INDATMEM),'INVENTAR','INDATMEM');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODESE),'INVENTAR','INCODESE');
      +","+cp_NullLink(cp_ToStrODBC(w_INTIPINV),'INVENTAR','INTIPINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INSTAINV),'INVENTAR','INSTAINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INELABOR),'INVENTAR','INELABOR');
      +","+cp_NullLink(cp_ToStrODBC(w_INDESINV),'INVENTAR','INDESINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODMAG),'INVENTAR','INCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(w_INMAGCOL),'INVENTAR','INMAGCOL');
      +","+cp_NullLink(cp_ToStrODBC(w_INFLGLCO),'INVENTAR','INFLGLCO');
      +","+cp_NullLink(cp_ToStrODBC(w_INFLGLSC),'INVENTAR','INFLGLSC');
      +","+cp_NullLink(cp_ToStrODBC(w_INFLGFCO),'INVENTAR','INFLGFCO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'INVENTAR','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'INVENTAR','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'INVENTAR','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'INVENTAR','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(w_INCATOMO),'INVENTAR','INCATOMO');
      +","+cp_NullLink(cp_ToStrODBC(w_INGRUMER),'INVENTAR','INGRUMER');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODFAM),'INVENTAR','INCODFAM');
      +","+cp_NullLink(cp_ToStrODBC(w_INNUMPRE),'INVENTAR','INNUMPRE');
      +","+cp_NullLink(cp_ToStrODBC(w_INESEPRE),'INVENTAR','INESEPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'INNUMINV',w_INNUMINV,'INDATINV',w_INDATINV,'INDATMEM',w_INDATMEM,'INCODESE',w_INCODESE,'INTIPINV',w_INTIPINV,'INSTAINV',w_INSTAINV,'INELABOR',w_INELABOR,'INDESINV',w_INDESINV,'INCODMAG',w_INCODMAG,'INMAGCOL',w_INMAGCOL,'INFLGLCO',w_INFLGLCO,'INFLGLSC',w_INFLGLSC)
      insert into (i_cTable) (INNUMINV,INDATINV,INDATMEM,INCODESE,INTIPINV,INSTAINV,INELABOR,INDESINV,INCODMAG,INMAGCOL,INFLGLCO,INFLGLSC,INFLGFCO,UTCC,UTCV,UTDC,UTDV,INCATOMO,INGRUMER,INCODFAM,INNUMPRE,INESEPRE &i_ccchkf. );
         values (;
           w_INNUMINV;
           ,w_INDATINV;
           ,w_INDATMEM;
           ,w_INCODESE;
           ,w_INTIPINV;
           ,w_INSTAINV;
           ,w_INELABOR;
           ,w_INDESINV;
           ,w_INCODMAG;
           ,w_INMAGCOL;
           ,w_INFLGLCO;
           ,w_INFLGLSC;
           ,w_INFLGFCO;
           ,this.oParentObject.w_CreVarUte;
           ,this.oParentObject.w_CreVarUte;
           ,this.oParentObject.w_CreVarDat;
           ,this.oParentObject.w_CreVarDat;
           ,w_INCATOMO;
           ,w_INGRUMER;
           ,w_INCODFAM;
           ,w_INNUMPRE;
           ,w_INESEPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento records
    i_rows = 0
    * --- Try
    local bErr_04E59C88
    bErr_04E59C88=bTrsErr
    this.Try_04E59C88()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      this.oParentObject.w_Corretto = .F.
    endif
    bTrsErr=bTrsErr or bErr_04E59C88
    * --- End
  endproc
  proc Try_04E59C88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "AR" OR this.oParentObject.w_Destinaz = "T2"
        * --- Articoli di Magazzino
        this.oParentObject.w_ResoDett = ah_msgformat( "Articolo %1" , alltrim( w_ARCODART ) )
        * --- Inserimento chiave di ricerca
        w_CATIPCON = "R"
        w_CA__TIPO = "R"
        do case
          case w_ARTIPART="AC"
            * --- Articolo Composto
            w_CATIPCON = "A"
            w_CA__TIPO = "A"
          case w_ARTIPART="FO"
            * --- Servizio Forfait
            if this.oParentObject.w_DESTINAZ="AR"
              w_CATIPCON = "1"
            else
              w_CATIPCON = "I"
            endif
            w_CA__TIPO = "F"
          case w_ARTIPART="FM"
            * --- Servizio Fuori Magazzino
            w_CATIPCON = "M"
            w_CA__TIPO = "M"
          case w_ARTIPART="DE"
            * --- Servizio Descrittivo
            w_CATIPCON = "D"
            w_CA__TIPO = "D"
        endcase
        * --- La prima volta che viene importato l'articolo carica anche la chiave di ricerca
        if g_APPLICATION="ADHOC REVOLUTION"
          w_CACODICE = left( w_ARCODART + space(20), 20)
          this.w_KEY_READ = space(20)
        else
          w_CACODICE = left( w_ARCODART + space(41), 41)
          this.w_KEY_READ = space(41)
        endif
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(w_CACODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CACODICE = w_CACODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_KEY_READ = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty( this.w_KEY_READ )
          * --- Try
          local bErr_04E916D8
          bErr_04E916D8=bTrsErr
          this.Try_04E916D8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            this.oParentObject.w_ResoMess = ah_msgformat( "Impossibile registrare la chiave di ricerca dell'articolo %1%2" , trim(w_ARCODART) , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg ,"") )
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E916D8
          * --- End
        endif
        * --- Inserimento record valorizzazione inventario
        if g_APPLICATION="ADHOC REVOLUTION"
          w_DICODICE = w_ARCODART
        else
          w_DICODICE = w_ARCODART+ REPL("#", 20)
        endif
        if type("w_ARULTCOS")<>"U"
          * --- Try
          local bErr_04E8D418
          bErr_04E8D418=bTrsErr
          this.Try_04E8D418()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            this.oParentObject.w_ResoMess = ah_msgformat( "Impossibile registrare l'ultimo costo dell'articolo %1%2" , trim(w_ARCODART) , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) )
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E8D418
          * --- End
        endif
        w_AROPERAT = iif( empty( w_AROPERAT ), "*", w_AROPERAT )
        w_ARFLINVE = iif( empty( w_ARFLINVE ), "N", w_ARFLINVE )
        w_ARFLESAU = iif( empty( w_ARFLESAU ), "N", w_ARFLESAU )
        w_ARTIPART = iif( empty( w_ARTIPART ), "PF", w_ARTIPART )
        w_ARFLLOTT =IIF(type("w_ARFLLOTT")="U" OR EMPTY(w_ARFLLOTT) OR NOT(w_ARFLLOTT$"C-S-N"),"N",w_ARFLLOTT)
         
 w_ARTIPCO1 =IIF(type("w_ARTIPCO1")="U" ,Space(5),w_ARTIPCO1) 
 w_ARTIPCO2 =IIF(type("w_ARTIPCO2")="U" ,Space(5),w_ARTIPCO2) 
 w_ARTIPRES=IIF(type("w_ARTIPRES")="U" or Empty(w_ARTIPRES),"PREST",w_ARTIPRES) 
 w_ARASSIVA=IIF(type("w_ARASSIVA")="U" ," ",w_ARASSIVA)
        if empty( w_ARUMSUPP ) .and. (.not. empty( w_ARNOMENC ))
          * --- Read from NOMENCLA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.NOMENCLA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2],.t.,this.NOMENCLA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NMUNISUP"+;
              " from "+i_cTable+" NOMENCLA where ";
                  +"NMCODICE = "+cp_ToStrODBC(w_ARNOMENC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NMUNISUP;
              from (i_cTable) where;
                  NMCODICE = w_ARNOMENC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_ARUMSUPP = NVL(cp_ToDate(_read_.NMUNISUP),cp_NullValue(_read_.NMUNISUP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Creo l'unit� di misura primaria se non esiste
        * --- Try
        local bErr_04E73B48
        bErr_04E73B48=bTrsErr
        this.Try_04E73B48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E73B48
        * --- End
        * --- Creo l'unit� di misura secondaria se non esiste
        * --- Try
        local bErr_04E74C88
        bErr_04E74C88=bTrsErr
        this.Try_04E74C88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E74C88
        * --- End
        * --- Inserimento dati Articoli
        if this.oParentObject.w_Destinaz = "AR"
          * --- Try
          local bErr_04E781C8
          bErr_04E781C8=bTrsErr
          this.Try_04E781C8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04E781C8
          * --- End
          * --- Write into PAR_RIOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRSCOMIN ="+cp_NullLink(cp_ToStrODBC(w_PRSCOMIN),'PAR_RIOR','PRSCOMIN');
            +",PRSCOMAX ="+cp_NullLink(cp_ToStrODBC(w_PRSCOMAX),'PAR_RIOR','PRSCOMAX');
            +",PRDISMIN ="+cp_NullLink(cp_ToStrODBC(w_PRDISMIN),'PAR_RIOR','PRDISMIN');
            +",PRLOTRIO ="+cp_NullLink(cp_ToStrODBC(w_PRLOTRIO),'PAR_RIOR','PRLOTRIO');
            +",PRGIOINV ="+cp_NullLink(cp_ToStrODBC(w_PRGIOINV),'PAR_RIOR','PRGIOINV');
            +",PRGIOAPP ="+cp_NullLink(cp_ToStrODBC(w_PRGIOAPP),'PAR_RIOR','PRGIOAPP');
            +",PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(w_PRCOSSTA),'PAR_RIOR','PRCOSSTA');
            +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC(w_PRTIPCON),'PAR_RIOR','PRTIPCON');
            +",PRCODPRO ="+cp_NullLink(cp_ToStrODBC(w_PRCODPRO),'PAR_RIOR','PRCODPRO');
            +",PRCODFOR ="+cp_NullLink(cp_ToStrODBC(w_PRCODFOR),'PAR_RIOR','PRCODFOR');
                +i_ccchkf ;
            +" where ";
                +"PRCODART = "+cp_ToStrODBC(w_ARCODART);
                   )
          else
            update (i_cTable) set;
                PRSCOMIN = w_PRSCOMIN;
                ,PRSCOMAX = w_PRSCOMAX;
                ,PRDISMIN = w_PRDISMIN;
                ,PRLOTRIO = w_PRLOTRIO;
                ,PRGIOINV = w_PRGIOINV;
                ,PRGIOAPP = w_PRGIOAPP;
                ,PRCOSSTA = w_PRCOSSTA;
                ,PRTIPCON = w_PRTIPCON;
                ,PRCODPRO = w_PRCODPRO;
                ,PRCODFOR = w_PRCODFOR;
                &i_ccchkf. ;
             where;
                PRCODART = w_ARCODART;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          if VARTYPE(w_TACODLIS)="C"
            * --- Tariffario avvocati
            *     Devo capire qual'� il CPROWNUM da assegnare.
            *     Ci sono poche tariffe da importare e non abbiamo problemi di performance
            this.w_Tariffa = w_ARCODART
            this.w_MaxRowNum = 1
            * --- Select from TAR_IFFE
            i_nConn=i_TableProp[this.TAR_IFFE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TAR_IFFE_idx,2],.t.,this.TAR_IFFE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select TACODART, MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" TAR_IFFE ";
                  +" where TACODART="+cp_ToStrODBC(this.w_Tariffa)+"";
                  +" group by TACODART";
                   ,"_Curs_TAR_IFFE")
            else
              select TACODART, MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
               where TACODART=this.w_Tariffa;
               group by TACODART;
                into cursor _Curs_TAR_IFFE
            endif
            if used('_Curs_TAR_IFFE')
              select _Curs_TAR_IFFE
              locate for 1=1
              do while not(eof())
              this.w_MaxRowNum = CPROWNUM + 1
                select _Curs_TAR_IFFE
                continue
              enddo
              use
            endif
            * --- Try
            local bErr_04E92758
            bErr_04E92758=bTrsErr
            this.Try_04E92758()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04E92758
            * --- End
            * --- Write into TAR_IFFE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TAR_IFFE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TAR_IFFE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TAR_IFFE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TACODLIS ="+cp_NullLink(cp_ToStrODBC(w_TACODLIS),'TAR_IFFE','TACODLIS');
              +",TADATATT ="+cp_NullLink(cp_ToStrODBC(w_TADATATT),'TAR_IFFE','TADATATT');
              +",TADATDIS ="+cp_NullLink(cp_ToStrODBC(w_TADATDIS),'TAR_IFFE','TADATDIS');
              +",TAAUTORI ="+cp_NullLink(cp_ToStrODBC(w_TAAUTORI),'TAR_IFFE','TAAUTORI');
              +",TAGAZUFF ="+cp_NullLink(cp_ToStrODBC(w_TAGAZUFF),'TAR_IFFE','TAGAZUFF');
              +",TARESPON ="+cp_NullLink(cp_ToStrODBC(w_TARESPON),'TAR_IFFE','TARESPON');
              +",TAMANSION ="+cp_NullLink(cp_ToStrODBC(w_TAMANSION),'TAR_IFFE','TAMANSION');
                  +i_ccchkf ;
              +" where ";
                  +"TACODART = "+cp_ToStrODBC(w_ARCODART);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_MaxRowNum);
                     )
            else
              update (i_cTable) set;
                  TACODLIS = w_TACODLIS;
                  ,TADATATT = w_TADATATT;
                  ,TADATDIS = w_TADATDIS;
                  ,TAAUTORI = w_TAAUTORI;
                  ,TAGAZUFF = w_TAGAZUFF;
                  ,TARESPON = w_TARESPON;
                  ,TAMANSION = w_TAMANSION;
                  &i_ccchkf. ;
               where;
                  TACODART = w_ARCODART;
                  and CPROWNUM = this.w_MaxRowNum;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Scrittura degli importi di riga
            *     Fino a valore pratica: OLTRE
            if w_DEPREMIN<>0 OR w_DEPREMAX<>0
              * --- Try
              local bErr_04E94558
              bErr_04E94558=bTrsErr
              this.Try_04E94558()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04E94558
              * --- End
              * --- Fino a valore pratica: INDETERMINABILE
              * --- Try
              local bErr_04E954B8
              bErr_04E954B8=bTrsErr
              this.Try_04E954B8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04E954B8
              * --- End
              * --- Fino a valore pratica: INDETERMINABILE E PARTICOLARE IMPORTANZA
              * --- Try
              local bErr_04E95F08
              bErr_04E95F08=bTrsErr
              this.Try_04E95F08()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_04E95F08
              * --- End
            endif
          endif
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARTIPPKR"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARTIPPKR;
            from (i_cTable) where;
                ARCODART = w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTIPPKR = NVL(cp_ToDate(_read_.ARTIPPKR),cp_NullValue(_read_.ARTIPPKR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ARTIPPKR = EVL(this.w_ARTIPPKR, w_ARTIPART)
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARCATCON ="+cp_NullLink(cp_ToStrODBC(w_ARCATCON),'ART_ICOL','ARCATCON');
          +",ARCATOMO ="+cp_NullLink(cp_ToStrODBC(w_ARCATOMO),'ART_ICOL','ARCATOMO');
          +",ARCATSCM ="+cp_NullLink(cp_ToStrODBC(w_ARCATSCM),'ART_ICOL','ARCATSCM');
          +",ARCOCOL1 ="+cp_NullLink(cp_ToStrODBC(w_ARCOCOL1),'ART_ICOL','ARCOCOL1');
          +",ARCOCOL2 ="+cp_NullLink(cp_ToStrODBC(w_ARCOCOL2),'ART_ICOL','ARCOCOL2');
          +",ARCODCLA ="+cp_NullLink(cp_ToStrODBC(w_ARCODCLA),'ART_ICOL','ARCODCLA');
          +",ARCODFAM ="+cp_NullLink(cp_ToStrODBC(w_ARCODFAM),'ART_ICOL','ARCODFAM');
          +",ARCODIVA ="+cp_NullLink(cp_ToStrODBC(w_ARCODIVA),'ART_ICOL','ARCODIVA');
          +",ARCODMAR ="+cp_NullLink(cp_ToStrODBC(w_ARCODMAR),'ART_ICOL','ARCODMAR');
          +",ARCODRIC ="+cp_NullLink(cp_ToStrODBC(w_ARCODRIC),'ART_ICOL','ARCODRIC');
          +",ARDESART ="+cp_NullLink(cp_ToStrODBC(w_ARDESART),'ART_ICOL','ARDESART');
          +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(w_ARDESSUP),'ART_ICOL','ARDESSUP');
          +",ARDESVO2 ="+cp_NullLink(cp_ToStrODBC(w_ARDESVO2),'ART_ICOL','ARDESVO2');
          +",ARDESVOL ="+cp_NullLink(cp_ToStrODBC(w_ARDESVOL),'ART_ICOL','ARDESVOL');
          +",ARDIMAL2 ="+cp_NullLink(cp_ToStrODBC(w_ARDIMAL2),'ART_ICOL','ARDIMAL2');
          +",ARDIMALT ="+cp_NullLink(cp_ToStrODBC(w_ARDIMALT),'ART_ICOL','ARDIMALT');
          +",ARDIMLA2 ="+cp_NullLink(cp_ToStrODBC(w_ARDIMLA2),'ART_ICOL','ARDIMLA2');
          +",ARDIMLAR ="+cp_NullLink(cp_ToStrODBC(w_ARDIMLAR),'ART_ICOL','ARDIMLAR');
          +",ARDIMLU2 ="+cp_NullLink(cp_ToStrODBC(w_ARDIMLU2),'ART_ICOL','ARDIMLU2');
          +",ARDIMLUN ="+cp_NullLink(cp_ToStrODBC(w_ARDIMLUN),'ART_ICOL','ARDIMLUN');
          +",ARDTINVA ="+cp_NullLink(cp_ToStrODBC(w_ARDTINVA),'ART_ICOL','ARDTINVA');
          +",ARDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_ARDTOBSO),'ART_ICOL','ARDTOBSO');
          +",ARFLCON2 ="+cp_NullLink(cp_ToStrODBC(w_ARFLCON2),'ART_ICOL','ARFLCON2');
          +",ARFLCONA ="+cp_NullLink(cp_ToStrODBC(w_ARFLCONA),'ART_ICOL','ARFLCONA');
          +",ARFLDISP ="+cp_NullLink(cp_ToStrODBC(w_ARFLDISP),'ART_ICOL','ARFLDISP');
          +",ARFLESAU ="+cp_NullLink(cp_ToStrODBC(w_ARFLESAU),'ART_ICOL','ARFLESAU');
          +",ARFLGIS4 ="+cp_NullLink(cp_ToStrODBC(w_ARFLGIS4),'ART_ICOL','ARFLGIS4');
          +",ARFLINVE ="+cp_NullLink(cp_ToStrODBC(w_ARFLINVE),'ART_ICOL','ARFLINVE');
          +",ARFLSTCO ="+cp_NullLink(cp_ToStrODBC(w_ARFLSTCO),'ART_ICOL','ARFLSTCO');
          +",ARGRUMER ="+cp_NullLink(cp_ToStrODBC(w_ARGRUMER),'ART_ICOL','ARGRUMER');
          +",ARGRUPRO ="+cp_NullLink(cp_ToStrODBC(w_ARGRUPRO),'ART_ICOL','ARGRUPRO');
          +",ARMOLSUP ="+cp_NullLink(cp_ToStrODBC(w_ARMOLSUP),'ART_ICOL','ARMOLSUP');
          +",ARMOLTIP ="+cp_NullLink(cp_ToStrODBC(w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
          +",ARNOMENC ="+cp_NullLink(cp_ToStrODBC(w_ARNOMENC),'ART_ICOL','ARNOMENC');
          +",AROPERAT ="+cp_NullLink(cp_ToStrODBC(w_AROPERAT),'ART_ICOL','AROPERAT');
          +",ARPESLO2 ="+cp_NullLink(cp_ToStrODBC(w_ARPESLO2),'ART_ICOL','ARPESLO2');
          +",ARPESLOR ="+cp_NullLink(cp_ToStrODBC(w_ARPESLOR),'ART_ICOL','ARPESLOR');
          +",ARPESNE2 ="+cp_NullLink(cp_ToStrODBC(w_ARPESNE2),'ART_ICOL','ARPESNE2');
          +",ARPESNET ="+cp_NullLink(cp_ToStrODBC(w_ARPESNET),'ART_ICOL','ARPESNET');
          +",ARPROPRE ="+cp_NullLink(cp_ToStrODBC(w_ARPROPRE),'ART_ICOL','ARPROPRE');
          +",ARPZCON2 ="+cp_NullLink(cp_ToStrODBC(w_ARPZCON2),'ART_ICOL','ARPZCON2');
          +",ARPZCONF ="+cp_NullLink(cp_ToStrODBC(w_ARPZCONF),'ART_ICOL','ARPZCONF');
          +",ARSTASUP ="+cp_NullLink(cp_ToStrODBC(w_ARSTASUP),'ART_ICOL','ARSTASUP');
          +",ARTIPART ="+cp_NullLink(cp_ToStrODBC(w_ARTIPART),'ART_ICOL','ARTIPART');
          +",ARTIPPKR ="+cp_NullLink(cp_ToStrODBC(this.w_ARTIPPKR),'ART_ICOL','ARTIPPKR');
          +",ARTIPGES ="+cp_NullLink(cp_ToStrODBC(w_ARTIPGES),'ART_ICOL','ARTIPGES');
          +",ARTPCON2 ="+cp_NullLink(cp_ToStrODBC(w_ARTPCON2),'ART_ICOL','ARTPCON2');
          +",ARTPCONF ="+cp_NullLink(cp_ToStrODBC(w_ARTPCONF),'ART_ICOL','ARTPCONF');
          +",ARUMDIM2 ="+cp_NullLink(cp_ToStrODBC(w_ARUMDIM2),'ART_ICOL','ARUMDIM2');
          +",ARUMDIME ="+cp_NullLink(cp_ToStrODBC(w_ARUMDIME),'ART_ICOL','ARUMDIME');
          +",ARUMSUPP ="+cp_NullLink(cp_ToStrODBC(w_ARUMSUPP),'ART_ICOL','ARUMSUPP');
          +",ARUMVOL2 ="+cp_NullLink(cp_ToStrODBC(w_ARUMVOL2),'ART_ICOL','ARUMVOL2');
          +",ARUMVOLU ="+cp_NullLink(cp_ToStrODBC(w_ARUMVOLU),'ART_ICOL','ARUMVOLU');
          +",ARUNMIS1 ="+cp_NullLink(cp_ToStrODBC(w_ARUNMIS1),'ART_ICOL','ARUNMIS1');
          +",ARUNMIS2 ="+cp_NullLink(cp_ToStrODBC(w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
          +",ARCODDIS ="+cp_NullLink(cp_ToStrODBC(w_ARCODDIS),'ART_ICOL','ARCODDIS');
          +",ARVOCRIC ="+cp_NullLink(cp_ToStrODBC(w_ARVOCRIC),'ART_ICOL','ARVOCRIC');
          +",ARVOCCEN ="+cp_NullLink(cp_ToStrODBC(w_ARVOCCEN),'ART_ICOL','ARVOCCEN');
          +",ARFLLOTT ="+cp_NullLink(cp_ToStrODBC(w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
          +",ARFLDISC ="+cp_NullLink(cp_ToStrODBC(w_ARFLDISC),'ART_ICOL','ARFLDISC');
          +",ARCATOPE ="+cp_NullLink(cp_ToStrODBC("AR"),'ART_ICOL','ARCATOPE');
          +",ARTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_ARTIPOPE),'ART_ICOL','ARTIPOPE');
          +",ARTIPCO1 ="+cp_NullLink(cp_ToStrODBC(w_ARTIPCO1),'ART_ICOL','ARTIPCO1');
          +",ARTIPCO2 ="+cp_NullLink(cp_ToStrODBC(w_ARTIPCO2),'ART_ICOL','ARTIPCO2');
          +",ARPRESTA ="+cp_NullLink(cp_ToStrODBC(w_ARPRESTA),'ART_ICOL','ARPRESTA');
          +",ARFLESCT ="+cp_NullLink(cp_ToStrODBC(w_ARFLESCT),'ART_ICOL','ARFLESCT');
          +",ARTIPRES ="+cp_NullLink(cp_ToStrODBC(w_ARTIPRES),'ART_ICOL','ARTIPRES');
          +",ARTIPRIG ="+cp_NullLink(cp_ToStrODBC(w_ARTIPRIG),'ART_ICOL','ARTIPRIG');
          +",ARTIPRI2 ="+cp_NullLink(cp_ToStrODBC(w_ARTIPRI2),'ART_ICOL','ARTIPRI2');
          +",ARPUBWEB ="+cp_NullLink(cp_ToStrODBC(w_ARPUBWEB),'ART_ICOL','ARPUBWEB');
          +",ARASSIVA ="+cp_NullLink(cp_ToStrODBC(w_ARASSIVA),'ART_ICOL','ARASSIVA');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARCATCON = w_ARCATCON;
              ,ARCATOMO = w_ARCATOMO;
              ,ARCATSCM = w_ARCATSCM;
              ,ARCOCOL1 = w_ARCOCOL1;
              ,ARCOCOL2 = w_ARCOCOL2;
              ,ARCODCLA = w_ARCODCLA;
              ,ARCODFAM = w_ARCODFAM;
              ,ARCODIVA = w_ARCODIVA;
              ,ARCODMAR = w_ARCODMAR;
              ,ARCODRIC = w_ARCODRIC;
              ,ARDESART = w_ARDESART;
              ,ARDESSUP = w_ARDESSUP;
              ,ARDESVO2 = w_ARDESVO2;
              ,ARDESVOL = w_ARDESVOL;
              ,ARDIMAL2 = w_ARDIMAL2;
              ,ARDIMALT = w_ARDIMALT;
              ,ARDIMLA2 = w_ARDIMLA2;
              ,ARDIMLAR = w_ARDIMLAR;
              ,ARDIMLU2 = w_ARDIMLU2;
              ,ARDIMLUN = w_ARDIMLUN;
              ,ARDTINVA = w_ARDTINVA;
              ,ARDTOBSO = w_ARDTOBSO;
              ,ARFLCON2 = w_ARFLCON2;
              ,ARFLCONA = w_ARFLCONA;
              ,ARFLDISP = w_ARFLDISP;
              ,ARFLESAU = w_ARFLESAU;
              ,ARFLGIS4 = w_ARFLGIS4;
              ,ARFLINVE = w_ARFLINVE;
              ,ARFLSTCO = w_ARFLSTCO;
              ,ARGRUMER = w_ARGRUMER;
              ,ARGRUPRO = w_ARGRUPRO;
              ,ARMOLSUP = w_ARMOLSUP;
              ,ARMOLTIP = w_ARMOLTIP;
              ,ARNOMENC = w_ARNOMENC;
              ,AROPERAT = w_AROPERAT;
              ,ARPESLO2 = w_ARPESLO2;
              ,ARPESLOR = w_ARPESLOR;
              ,ARPESNE2 = w_ARPESNE2;
              ,ARPESNET = w_ARPESNET;
              ,ARPROPRE = w_ARPROPRE;
              ,ARPZCON2 = w_ARPZCON2;
              ,ARPZCONF = w_ARPZCONF;
              ,ARSTASUP = w_ARSTASUP;
              ,ARTIPART = w_ARTIPART;
              ,ARTIPPKR = this.w_ARTIPPKR;
              ,ARTIPGES = w_ARTIPGES;
              ,ARTPCON2 = w_ARTPCON2;
              ,ARTPCONF = w_ARTPCONF;
              ,ARUMDIM2 = w_ARUMDIM2;
              ,ARUMDIME = w_ARUMDIME;
              ,ARUMSUPP = w_ARUMSUPP;
              ,ARUMVOL2 = w_ARUMVOL2;
              ,ARUMVOLU = w_ARUMVOLU;
              ,ARUNMIS1 = w_ARUNMIS1;
              ,ARUNMIS2 = w_ARUNMIS2;
              ,ARCODDIS = w_ARCODDIS;
              ,ARVOCRIC = w_ARVOCRIC;
              ,ARVOCCEN = w_ARVOCCEN;
              ,ARFLLOTT = w_ARFLLOTT;
              ,ARFLDISC = w_ARFLDISC;
              ,ARCATOPE = "AR";
              ,ARTIPOPE = w_ARTIPOPE;
              ,ARTIPCO1 = w_ARTIPCO1;
              ,ARTIPCO2 = w_ARTIPCO2;
              ,ARPRESTA = w_ARPRESTA;
              ,ARFLESCT = w_ARFLESCT;
              ,ARTIPRES = w_ARTIPRES;
              ,ARTIPRIG = w_ARTIPRIG;
              ,ARTIPRI2 = w_ARTIPRI2;
              ,ARPUBWEB = w_ARPUBWEB;
              ,ARASSIVA = w_ARASSIVA;
              &i_ccchkf. ;
           where;
              ARCODART = w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AN"
        * --- Articoli Note
        this.oParentObject.w_ResoDett = ah_msgformat( "Note articolo %1" , alltrim( w_ARCODART ) )
        * --- Write into NOT_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.NOT_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOT_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NOT_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AR__NOTE ="+cp_NullLink(cp_ToStrODBC(w_AR__NOTE),'NOT_ARTI','AR__NOTE');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(w_ARCODART);
                 )
        else
          update (i_cTable) set;
              AR__NOTE = w_AR__NOTE;
              &i_ccchkf. ;
           where;
              ARCODART = w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CM"
        * --- Causali di Magazzino
        w_CMFLAVAL = iif( empty( w_CMFLAVAL ), "N", w_CMFLAVAL )
        w_CMFLCLFR = iif( empty( w_CMFLCLFR ), IIF(w_CMFLAVAL="A", "F", IIF(w_CMFLAVAL="V", "C", "N")) , w_CMFLCLFR )
        w_CMCRIVAL = iif( empty( w_CMCRIVAL ), "CM", w_CMCRIVAL )
        w_CMFLRIVA = iif( empty( w_CMFLRIVA ), "N", w_CMFLRIVA )
        w_CMFLCASC = iif( w_CMFLAVAL="N", " ", w_CMFLCASC )
        w_CMFLORDI = iif( w_CMFLAVAL="N", " ", w_CMFLORDI )
        w_CMFLIMPE = iif( w_CMFLAVAL="N", " ", w_CMFLIMPE )
        w_CMFLRISE = iif( w_CMFLAVAL="N", " ", w_CMFLRISE )
        w_CMSEQCAL = iif( empty( w_CMSEQCAL ) .and. w_CMFLRIVA="S", 10, w_CMSEQCAL )
        this.oParentObject.w_ResoDett = ah_msgformat( "Causale di magazzino %1" , trim( w_CMCODICE ) )
        * --- Write into CAM_AGAZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAM_AGAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CMDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CMDESCRI),'CAM_AGAZ','CMDESCRI');
          +",CMFLCLFR ="+cp_NullLink(cp_ToStrODBC(w_CMFLCLFR),'CAM_AGAZ','CMFLCLFR');
          +",CMCAUCOL ="+cp_NullLink(cp_ToStrODBC(w_CMCAUCOL),'CAM_AGAZ','CMCAUCOL');
          +",CMCRIVAL ="+cp_NullLink(cp_ToStrODBC(w_CMCRIVAL),'CAM_AGAZ','CMCRIVAL');
          +",CMSEQCAL ="+cp_NullLink(cp_ToStrODBC(w_CMSEQCAL),'CAM_AGAZ','CMSEQCAL');
          +",CMFLAVAL ="+cp_NullLink(cp_ToStrODBC(w_CMFLAVAL),'CAM_AGAZ','CMFLAVAL');
          +",CMFLCASC ="+cp_NullLink(cp_ToStrODBC(w_CMFLCASC),'CAM_AGAZ','CMFLCASC');
          +",CMFLORDI ="+cp_NullLink(cp_ToStrODBC(w_CMFLORDI),'CAM_AGAZ','CMFLORDI');
          +",CMFLIMPE ="+cp_NullLink(cp_ToStrODBC(w_CMFLIMPE),'CAM_AGAZ','CMFLIMPE');
          +",CMFLRISE ="+cp_NullLink(cp_ToStrODBC(w_CMFLRISE),'CAM_AGAZ','CMFLRISE');
          +",CMNATTRA ="+cp_NullLink(cp_ToStrODBC(w_CMNATTRA),'CAM_AGAZ','CMNATTRA');
          +",CMDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CMDTINVA),'CAM_AGAZ','CMDTINVA');
          +",CMDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CMDTOBSO),'CAM_AGAZ','CMDTOBSO');
          +",CMFLNDOC ="+cp_NullLink(cp_ToStrODBC(w_CMFLNDOC),'CAM_AGAZ','CMFLNDOC');
          +",CMFLRIVA ="+cp_NullLink(cp_ToStrODBC(w_CMFLRIVA),'CAM_AGAZ','CMFLRIVA');
          +",CMFLELGM ="+cp_NullLink(cp_ToStrODBC(w_CMFLELGM),'CAM_AGAZ','CMFLELGM');
          +",CMAGGVAL ="+cp_NullLink(cp_ToStrODBC(w_CMAGGVAL),'CAM_AGAZ','CMAGGVAL');
              +i_ccchkf ;
          +" where ";
              +"CMCODICE = "+cp_ToStrODBC(w_CMCODICE);
                 )
        else
          update (i_cTable) set;
              CMDESCRI = w_CMDESCRI;
              ,CMFLCLFR = w_CMFLCLFR;
              ,CMCAUCOL = w_CMCAUCOL;
              ,CMCRIVAL = w_CMCRIVAL;
              ,CMSEQCAL = w_CMSEQCAL;
              ,CMFLAVAL = w_CMFLAVAL;
              ,CMFLCASC = w_CMFLCASC;
              ,CMFLORDI = w_CMFLORDI;
              ,CMFLIMPE = w_CMFLIMPE;
              ,CMFLRISE = w_CMFLRISE;
              ,CMNATTRA = w_CMNATTRA;
              ,CMDTINVA = w_CMDTINVA;
              ,CMDTOBSO = w_CMDTOBSO;
              ,CMFLNDOC = w_CMFLNDOC;
              ,CMFLRIVA = w_CMFLRIVA;
              ,CMFLELGM = w_CMFLELGM;
              ,CMAGGVAL = w_CMAGGVAL;
              &i_ccchkf. ;
           where;
              CMCODICE = w_CMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "MA"
        * --- Magazzini
        w_MGFISMAG = iif( empty(w_MGFISMAG), "S", w_MGFISMAG )
        this.oParentObject.w_ResoDett = ah_msgformat( "Magazzino %1" , trim( w_MGCODMAG ) )
        * --- Write into MAGAZZIN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MAGAZZIN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MGDESMAG ="+cp_NullLink(cp_ToStrODBC(w_MGDESMAG),'MAGAZZIN','MGDESMAG');
          +",MGINDMAG ="+cp_NullLink(cp_ToStrODBC(w_MGINDMAG),'MAGAZZIN','MGINDMAG');
          +",MGMAGCAP ="+cp_NullLink(cp_ToStrODBC(w_MGCAPMAG),'MAGAZZIN','MGMAGCAP');
          +",MGCITMAG ="+cp_NullLink(cp_ToStrODBC(w_MGCITMAG),'MAGAZZIN','MGCITMAG');
          +",MGPROMAG ="+cp_NullLink(cp_ToStrODBC(w_MGPROMAG),'MAGAZZIN','MGPROMAG');
          +",MGFISMAG ="+cp_NullLink(cp_ToStrODBC(w_MGFISMAG),'MAGAZZIN','MGFISMAG');
          +",MGPERSON ="+cp_NullLink(cp_ToStrODBC(w_MGPERSON),'MAGAZZIN','MGPERSON');
          +",MG__NOTE ="+cp_NullLink(cp_ToStrODBC(w_MG__NOTE),'MAGAZZIN','MG__NOTE');
          +",MGTELEFO ="+cp_NullLink(cp_ToStrODBC(w_MGTELEFO),'MAGAZZIN','MGTELEFO');
          +",MG_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_MG_EMAIL),'MAGAZZIN','MG_EMAIL');
          +",MGMAGRAG ="+cp_NullLink(cp_ToStrODBC(w_MGMAGRAG),'MAGAZZIN','MGMAGRAG');
          +",MGDTINVA ="+cp_NullLink(cp_ToStrODBC(w_MGDTINVA),'MAGAZZIN','MGDTINVA');
          +",MGDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_MGDTOBSO),'MAGAZZIN','MGDTOBSO');
          +",MGMAGWEB ="+cp_NullLink(cp_ToStrODBC(w_MGMAGWEB),'MAGAZZIN','MGMAGWEB');
          +",MGFLUBIC ="+cp_NullLink(cp_ToStrODBC(w_MGFLUBIC),'MAGAZZIN','MGFLUBIC');
          +",MGSTAINT ="+cp_NullLink(cp_ToStrODBC(w_MGSTAINT),'MAGAZZIN','MGSTAINT');
          +",MGPREFIS ="+cp_NullLink(cp_ToStrODBC(w_MGPREFIS),'MAGAZZIN','MGPREFIS');
          +",MGCAPMAG ="+cp_NullLink(cp_ToStrODBC(w_MGCAPMAG),'MAGAZZIN','MGCAPMAG');
          +",MGDISMAG ="+cp_NullLink(cp_ToStrODBC(w_MGDISMAG),'MAGAZZIN','MGDISMAG');
          +",MG_EMPEC ="+cp_NullLink(cp_ToStrODBC(w_MG_EMPEC),'MAGAZZIN','MG_EMPEC');
              +i_ccchkf ;
          +" where ";
              +"MGCODMAG = "+cp_ToStrODBC(w_MGCODMAG);
                 )
        else
          update (i_cTable) set;
              MGDESMAG = w_MGDESMAG;
              ,MGINDMAG = w_MGINDMAG;
              ,MGMAGCAP = w_MGCAPMAG;
              ,MGCITMAG = w_MGCITMAG;
              ,MGPROMAG = w_MGPROMAG;
              ,MGFISMAG = w_MGFISMAG;
              ,MGPERSON = w_MGPERSON;
              ,MG__NOTE = w_MG__NOTE;
              ,MGTELEFO = w_MGTELEFO;
              ,MG_EMAIL = w_MG_EMAIL;
              ,MGMAGRAG = w_MGMAGRAG;
              ,MGDTINVA = w_MGDTINVA;
              ,MGDTOBSO = w_MGDTOBSO;
              ,MGMAGWEB = w_MGMAGWEB;
              ,MGFLUBIC = w_MGFLUBIC;
              ,MGSTAINT = w_MGSTAINT;
              ,MGPREFIS = w_MGPREFIS;
              ,MGCAPMAG = w_MGCAPMAG;
              ,MGDISMAG = w_MGDISMAG;
              ,MG_EMPEC = w_MG_EMPEC;
              &i_ccchkf. ;
           where;
              MGCODMAG = w_MGCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "KA"
        * --- Chiavi di ricerca
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESART,ARDESSUP"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(w_CACODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESART,ARDESSUP;
            from (i_cTable) where;
                ARCODART = w_CACODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_CADESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          w_CADESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        w_CATIPCON = iif( empty(w_CATIPCON), "R", w_CATIPCON )
        w_CA__TIPO = iif( empty(w_CA__TIPO), IIF(w_CATIPCON="I", "F", IIF(w_CATIPCON $ "CF", "R", w_CATIPCON)), w_CA__TIPO )
        w_CATIPBAR = iif( empty(w_CATIPBAR), "0", w_CATIPBAR )
        w_CAFLSTAM = iif( empty(w_CAFLSTAM), "N", w_CAFLSTAM )
        w_CAOPERAT = iif( empty(w_CAOPERAT), "*", w_CAOPERAT )
        w_CAMOLTIP=1
        this.oParentObject.w_ResoDett = ah_msgformat( "Codice %1" , trim( w_CACODICE ) )
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(w_CADESART),'KEY_ARTI','CADESART');
          +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(w_CADESSUP),'KEY_ARTI','CADESSUP');
          +",CACODART ="+cp_NullLink(cp_ToStrODBC(w_CACODART),'KEY_ARTI','CACODART');
          +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(w_CATIPCON),'KEY_ARTI','CATIPCON');
          +",CACODCON ="+cp_NullLink(cp_ToStrODBC(w_CACODCON),'KEY_ARTI','CACODCON');
          +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(w_CA__TIPO),'KEY_ARTI','CA__TIPO');
          +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(w_CATIPBAR),'KEY_ARTI','CATIPBAR');
          +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
          +",CAUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_CAUNIMIS),'KEY_ARTI','CAUNIMIS');
          +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(w_CAOPERAT),'KEY_ARTI','CAOPERAT');
          +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
          +",CAUMDIM3 ="+cp_NullLink(cp_ToStrODBC(w_CAUMDIM3),'KEY_ARTI','CAUMDIM3');
          +",CAFLCON3 ="+cp_NullLink(cp_ToStrODBC(w_CAFLCON3),'KEY_ARTI','CAFLCON3');
          +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(w_CADTINVA),'KEY_ARTI','CADTINVA');
          +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CADTOBSO),'KEY_ARTI','CADTOBSO');
          +",CATIPMA3 ="+cp_NullLink(cp_ToStrODBC(w_CATIPMA3),'KEY_ARTI','CATIPMA3');
          +",CAPESNE3 ="+cp_NullLink(cp_ToStrODBC(w_CAPESNE3),'KEY_ARTI','CAPESNE3');
          +",CAPESLO3 ="+cp_NullLink(cp_ToStrODBC(w_CAPESLO3),'KEY_ARTI','CAPESLO3');
          +",CADESVO3 ="+cp_NullLink(cp_ToStrODBC(w_CADESVO3),'KEY_ARTI','CADESVO3');
          +",CAUMVOL3 ="+cp_NullLink(cp_ToStrODBC(w_CAUMVOL3),'KEY_ARTI','CAUMVOL3');
          +",CATPCON3 ="+cp_NullLink(cp_ToStrODBC(w_CATPCON3),'KEY_ARTI','CATPCON3');
          +",CAPZCON3 ="+cp_NullLink(cp_ToStrODBC(w_CAPZCON3),'KEY_ARTI','CAPZCON3');
          +",CACOCOL3 ="+cp_NullLink(cp_ToStrODBC(w_CACOCOL3),'KEY_ARTI','CACOCOL3');
          +",CADIMLU3 ="+cp_NullLink(cp_ToStrODBC(w_CADIMLU3),'KEY_ARTI','CADIMLU3');
          +",CADIMLA3 ="+cp_NullLink(cp_ToStrODBC(w_CADIMLA3),'KEY_ARTI','CADIMLA3');
          +",CADIMAL3 ="+cp_NullLink(cp_ToStrODBC(w_CADIMAL3),'KEY_ARTI','CADIMAL3');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(w_CACODICE);
                 )
        else
          update (i_cTable) set;
              CADESART = w_CADESART;
              ,CADESSUP = w_CADESSUP;
              ,CACODART = w_CACODART;
              ,CATIPCON = w_CATIPCON;
              ,CACODCON = w_CACODCON;
              ,CA__TIPO = w_CA__TIPO;
              ,CATIPBAR = w_CATIPBAR;
              ,CAFLSTAM = w_CAFLSTAM;
              ,CAUNIMIS = w_CAUNIMIS;
              ,CAOPERAT = w_CAOPERAT;
              ,CAMOLTIP = w_CAMOLTIP;
              ,CAUMDIM3 = w_CAUMDIM3;
              ,CAFLCON3 = w_CAFLCON3;
              ,CADTINVA = w_CADTINVA;
              ,CADTOBSO = w_CADTOBSO;
              ,CATIPMA3 = w_CATIPMA3;
              ,CAPESNE3 = w_CAPESNE3;
              ,CAPESLO3 = w_CAPESLO3;
              ,CADESVO3 = w_CADESVO3;
              ,CAUMVOL3 = w_CAUMVOL3;
              ,CATPCON3 = w_CATPCON3;
              ,CAPZCON3 = w_CAPZCON3;
              ,CACOCOL3 = w_CACOCOL3;
              ,CADIMLU3 = w_CADIMLU3;
              ,CADIMLA3 = w_CADIMLA3;
              ,CADIMAL3 = w_CADIMAL3;
              &i_ccchkf. ;
           where;
              CACODICE = w_CACODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="DC"
        * --- Causali Documenti (dai documenti di AHW) 
        w_TDFLVEAC = iif( empty(NVL(w_TDFLVEAC," ")), "V", w_TDFLVEAC )
        w_TDNUMSCO = iif( empty(NVL(w_TDNUMSCO," ")), g_NUMSCO, w_TDNUMSCO )
        w_TDCATDOC = iif( empty(NVL(w_TDCATDOC," ")), "DI", w_TDCATDOC )
        w_TDPRODOC = iif( empty(w_TDPRODOC), CALCPD(w_TDCATDOC,w_TDFLVEAC), w_TDPRODOC )
        w_TDVOCECR = iif( empty(NVL(w_TDVOCECR," ")), "R", w_TDVOCECR )
        w_TD_SEGNO = iif( empty(NVL(w_TD_SEGNO," ")), "A", w_TD_SEGNO )
        this.oParentObject.w_ResoDett = ah_msgformat( "Causale %1" , trim( w_TDTIPDOC ) )
        * --- Write into TIP_DOCU
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TDDESDOC ="+cp_NullLink(cp_ToStrODBC(w_TDDESDOC),'TIP_DOCU','TDDESDOC');
          +",TDCATDOC ="+cp_NullLink(cp_ToStrODBC(w_TDCATDOC),'TIP_DOCU','TDCATDOC');
          +",TDFLVEAC ="+cp_NullLink(cp_ToStrODBC(w_TDFLVEAC),'TIP_DOCU','TDFLVEAC');
          +",TDFLINTE ="+cp_NullLink(cp_ToStrODBC(w_TDFLINTE),'TIP_DOCU','TDFLINTE');
          +",TDPRODOC ="+cp_NullLink(cp_ToStrODBC(w_TDPRODOC),'TIP_DOCU','TDPRODOC');
          +",TDALFDOC ="+cp_NullLink(cp_ToStrODBC(w_TDALFDOC),'TIP_DOCU','TDALFDOC');
          +",TDSERPRO ="+cp_NullLink(cp_ToStrODBC(w_TDSERPRO),'TIP_DOCU','TDSERPRO');
          +",TDFLPPRO ="+cp_NullLink(cp_ToStrODBC(w_TDFLPPRO),'TIP_DOCU','TDFLPPRO');
          +",TDCAUCON ="+cp_NullLink(cp_ToStrODBC(w_TDCAUCON),'TIP_DOCU','TDCAUCON');
          +",TDCAUMAG ="+cp_NullLink(cp_ToStrODBC(w_TDCAUMAG),'TIP_DOCU','TDCAUMAG');
          +",TDCODMAG ="+cp_NullLink(cp_ToStrODBC(w_TDCODMAG),'TIP_DOCU','TDCODMAG');
          +",TDCODMAT ="+cp_NullLink(cp_ToStrODBC(w_TDCODMAT),'TIP_DOCU','TDCODMAT');
          +",TDPRGSTA ="+cp_NullLink(cp_ToStrODBC(w_TDPRGSTA),'TIP_DOCU','TDPRGSTA');
          +",TDFLACCO ="+cp_NullLink(cp_ToStrODBC(w_TDFLACCO),'TIP_DOCU','TDFLACCO');
          +",TFFLGEFA ="+cp_NullLink(cp_ToStrODBC(w_TFFLGEFA),'TIP_DOCU','TFFLGEFA');
          +",TFFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_TFFLRAGG),'TIP_DOCU','TFFLRAGG');
          +",TDFLRICE ="+cp_NullLink(cp_ToStrODBC(w_TDFLRICE),'TIP_DOCU','TDFLRICE');
          +",TDFLCASH ="+cp_NullLink(cp_ToStrODBC(w_TDFLCASH),'TIP_DOCU','TDFLCASH');
          +",TDFLRISC ="+cp_NullLink(cp_ToStrODBC(w_TDFLRISC),'TIP_DOCU','TDFLRISC');
          +",TDFLCRIS ="+cp_NullLink(cp_ToStrODBC(w_TDFLCRIS),'TIP_DOCU','TDFLCRIS');
          +",TDFLNSRI ="+cp_NullLink(cp_ToStrODBC(w_TDFLNSRI),'TIP_DOCU','TDFLNSRI');
          +",TDFLVSRI ="+cp_NullLink(cp_ToStrODBC(w_TDFLVSRI),'TIP_DOCU','TDFLVSRI');
          +",TDFLDATT ="+cp_NullLink(cp_ToStrODBC(w_TDFLDATT),'TIP_DOCU','TDFLDATT');
          +",TDFLORAT ="+cp_NullLink(cp_ToStrODBC(w_TDFLORAT),'TIP_DOCU','TDFLORAT');
          +",TDFLPROV ="+cp_NullLink(cp_ToStrODBC(w_TDFLPROV),'TIP_DOCU','TDFLPROV');
          +",TDFLDTPR ="+cp_NullLink(cp_ToStrODBC(w_TDFLDTPR),'TIP_DOCU','TDFLDTPR');
          +",TDFLNSTA ="+cp_NullLink(cp_ToStrODBC(w_TDFLNSTA),'TIP_DOCU','TDFLNSTA');
          +",TDFLIMPA ="+cp_NullLink(cp_ToStrODBC(w_TDFLIMPA),'TIP_DOCU','TDFLIMPA');
          +",TDFLIMAC ="+cp_NullLink(cp_ToStrODBC(w_TDFLIMAC),'TIP_DOCU','TDFLIMAC');
          +",TDFLPACK ="+cp_NullLink(cp_ToStrODBC(w_TDFLPACK),'TIP_DOCU','TDFLPACK');
          +",TDASPETT ="+cp_NullLink(cp_ToStrODBC(w_TDASPETT),'TIP_DOCU','TDASPETT');
          +",TDNUMSCO ="+cp_NullLink(cp_ToStrODBC(w_TDNUMSCO),'TIP_DOCU','TDNUMSCO');
              +i_ccchkf ;
          +" where ";
              +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                 )
        else
          update (i_cTable) set;
              TDDESDOC = w_TDDESDOC;
              ,TDCATDOC = w_TDCATDOC;
              ,TDFLVEAC = w_TDFLVEAC;
              ,TDFLINTE = w_TDFLINTE;
              ,TDPRODOC = w_TDPRODOC;
              ,TDALFDOC = w_TDALFDOC;
              ,TDSERPRO = w_TDSERPRO;
              ,TDFLPPRO = w_TDFLPPRO;
              ,TDCAUCON = w_TDCAUCON;
              ,TDCAUMAG = w_TDCAUMAG;
              ,TDCODMAG = w_TDCODMAG;
              ,TDCODMAT = w_TDCODMAT;
              ,TDPRGSTA = w_TDPRGSTA;
              ,TDFLACCO = w_TDFLACCO;
              ,TFFLGEFA = w_TFFLGEFA;
              ,TFFLRAGG = w_TFFLRAGG;
              ,TDFLRICE = w_TDFLRICE;
              ,TDFLCASH = w_TDFLCASH;
              ,TDFLRISC = w_TDFLRISC;
              ,TDFLCRIS = w_TDFLCRIS;
              ,TDFLNSRI = w_TDFLNSRI;
              ,TDFLVSRI = w_TDFLVSRI;
              ,TDFLDATT = w_TDFLDATT;
              ,TDFLORAT = w_TDFLORAT;
              ,TDFLPROV = w_TDFLPROV;
              ,TDFLDTPR = w_TDFLDTPR;
              ,TDFLNSTA = w_TDFLNSTA;
              ,TDFLIMPA = w_TDFLIMPA;
              ,TDFLIMAC = w_TDFLIMAC;
              ,TDFLPACK = w_TDFLPACK;
              ,TDASPETT = w_TDASPETT;
              ,TDNUMSCO = w_TDNUMSCO;
              &i_ccchkf. ;
           where;
              TDTIPDOC = w_TDTIPDOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into TIP_DOCU
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TDSTACL1 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL1),'TIP_DOCU','TDSTACL1');
          +",TDSTACL2 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL2),'TIP_DOCU','TDSTACL2');
          +",TDSTACL3 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL3),'TIP_DOCU','TDSTACL3');
          +",TDSTACL4 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL4),'TIP_DOCU','TDSTACL4');
          +",TDSTACL5 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL5),'TIP_DOCU','TDSTACL5');
          +",TDESCCL1 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL1),'TIP_DOCU','TDESCCL1');
          +",TDESCCL2 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL2),'TIP_DOCU','TDESCCL2');
          +",TDESCCL3 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL3),'TIP_DOCU','TDESCCL3');
          +",TDESCCL4 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL4),'TIP_DOCU','TDESCCL4');
          +",TDESCCL5 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL5),'TIP_DOCU','TDESCCL5');
          +",TDTPNDOC ="+cp_NullLink(cp_ToStrODBC(w_TDTPNDOC),'TIP_DOCU','TDTPNDOC');
          +",TDTPVDOC ="+cp_NullLink(cp_ToStrODBC(w_TDTPVDOC),'TIP_DOCU','TDTPVDOC');
          +",TDSEQPRE ="+cp_NullLink(cp_ToStrODBC(w_TDSEQPRE),'TIP_DOCU','TDSEQPRE');
          +",TDSEQSCO ="+cp_NullLink(cp_ToStrODBC(w_TDSEQSCO),'TIP_DOCU','TDSEQSCO');
          +",TDSEQMA1 ="+cp_NullLink(cp_ToStrODBC(w_TDSEQMA1),'TIP_DOCU','TDSEQMA1');
          +",TDSEQMA2 ="+cp_NullLink(cp_ToStrODBC(w_TDSEQMA2),'TIP_DOCU','TDSEQMA2');
          +",TDNOPRSC ="+cp_NullLink(cp_ToStrODBC(w_TDNOPRSC),'TIP_DOCU','TDNOPRSC');
          +",TDFLPREF ="+cp_NullLink(cp_ToStrODBC(w_TDFLPREF),'TIP_DOCU','TDFLPREF');
          +",TDCODLIS ="+cp_NullLink(cp_ToStrODBC(w_TDCODLIS),'TIP_DOCU','TDCODLIS');
          +",TDQTADEF ="+cp_NullLink(cp_ToStrODBC(w_TDQTADEF),'TIP_DOCU','TDQTADEF');
          +",TDFLANAL ="+cp_NullLink(cp_ToStrODBC(w_TDFLANAL),'TIP_DOCU','TDFLANAL');
          +",TDFLSILI ="+cp_NullLink(cp_ToStrODBC(w_TDFLSILI),'TIP_DOCU','TDFLSILI');
          +",TDVOCECR ="+cp_NullLink(cp_ToStrODBC(w_TDVOCECR),'TIP_DOCU','TDVOCECR');
          +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_TD_SEGNO),'TIP_DOCU','TD_SEGNO');
          +",TDSINCFL ="+cp_NullLink(cp_ToStrODBC(w_TDSINCFL),'TIP_DOCU','TDSINCFL');
              +i_ccchkf ;
          +" where ";
              +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                 )
        else
          update (i_cTable) set;
              TDSTACL1 = w_TDSTACL1;
              ,TDSTACL2 = w_TDSTACL2;
              ,TDSTACL3 = w_TDSTACL3;
              ,TDSTACL4 = w_TDSTACL4;
              ,TDSTACL5 = w_TDSTACL5;
              ,TDESCCL1 = w_TDESCCL1;
              ,TDESCCL2 = w_TDESCCL2;
              ,TDESCCL3 = w_TDESCCL3;
              ,TDESCCL4 = w_TDESCCL4;
              ,TDESCCL5 = w_TDESCCL5;
              ,TDTPNDOC = w_TDTPNDOC;
              ,TDTPVDOC = w_TDTPVDOC;
              ,TDSEQPRE = w_TDSEQPRE;
              ,TDSEQSCO = w_TDSEQSCO;
              ,TDSEQMA1 = w_TDSEQMA1;
              ,TDSEQMA2 = w_TDSEQMA2;
              ,TDNOPRSC = w_TDNOPRSC;
              ,TDFLPREF = w_TDFLPREF;
              ,TDCODLIS = w_TDCODLIS;
              ,TDQTADEF = w_TDQTADEF;
              ,TDFLANAL = w_TDFLANAL;
              ,TDFLSILI = w_TDFLSILI;
              ,TDVOCECR = w_TDVOCECR;
              ,TD_SEGNO = w_TD_SEGNO;
              ,TDSINCFL = w_TDSINCFL;
              &i_ccchkf. ;
           where;
              TDTIPDOC = w_TDTIPDOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CD"
        * --- Causali Documenti (da archivio tabelle di AHW)
        * --- Select from TIP_DOCU
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" TIP_DOCU ";
               ,"_Curs_TIP_DOCU")
        else
          select * from (i_cTable);
            into cursor _Curs_TIP_DOCU
        endif
        if used('_Curs_TIP_DOCU')
          select _Curs_TIP_DOCU
          locate for 1=1
          do while not(eof())
          if LEFT(_Curs_TIP_DOCU.TDTIPDOC,2)=LEFT(w_TDTIPDOC,2)
            w_TDTIPDOC =_Curs_TIP_DOCU.TDTIPDOC
            this.oParentObject.w_ResoDett = ah_msgformat( "Causale %1" , trim( w_TDTIPDOC ) )
            if this.w_INSERI="S" AND RIGHT(_Curs_TIP_DOCU.TDTIPDOC,3)=w_TDCAUMAG
              * --- Causale documento presente nella tabella ma non utilizzata nei documenti
              w_TDFLVEAC = iif( empty(NVL(w_TDFLVEAC," ")), "V", w_TDFLVEAC )
              w_TDNUMSCO = iif( empty(NVL(w_TDNUMSCO," ")), g_NUMSCO, w_TDNUMSCO )
              w_TDCATDOC = iif( empty(NVL(w_TDCATDOC," ")), "DI", w_TDCATDOC )
              w_TDPRODOC = iif( empty(w_TDPRODOC), CALCPD(w_TDCATDOC,w_TDFLVEAC), w_TDPRODOC )
              w_TDVOCECR = iif( empty(NVL(w_TDVOCECR," ")), "R", w_TDVOCECR )
              w_TD_SEGNO = iif( empty(NVL(w_TD_SEGNO," ")), "A", w_TD_SEGNO )
              this.oParentObject.w_ResoDett = ah_msgformat( "Causale %1" , trim( w_TDTIPDOC ) )
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDDESDOC ="+cp_NullLink(cp_ToStrODBC(w_TDDESDOC),'TIP_DOCU','TDDESDOC');
                +",TDCATDOC ="+cp_NullLink(cp_ToStrODBC(w_TDCATDOC),'TIP_DOCU','TDCATDOC');
                +",TDFLVEAC ="+cp_NullLink(cp_ToStrODBC(w_TDFLVEAC),'TIP_DOCU','TDFLVEAC');
                +",TDFLINTE ="+cp_NullLink(cp_ToStrODBC(w_TDFLINTE),'TIP_DOCU','TDFLINTE');
                +",TDPRODOC ="+cp_NullLink(cp_ToStrODBC(w_TDPRODOC),'TIP_DOCU','TDPRODOC');
                +",TDALFDOC ="+cp_NullLink(cp_ToStrODBC(w_TDALFDOC),'TIP_DOCU','TDALFDOC');
                +",TDSERPRO ="+cp_NullLink(cp_ToStrODBC(w_TDSERPRO),'TIP_DOCU','TDSERPRO');
                +",TDFLPPRO ="+cp_NullLink(cp_ToStrODBC(w_TDFLPPRO),'TIP_DOCU','TDFLPPRO');
                +",TDCAUCON ="+cp_NullLink(cp_ToStrODBC(w_TDCAUCON),'TIP_DOCU','TDCAUCON');
                +",TDCAUMAG ="+cp_NullLink(cp_ToStrODBC(w_TDCAUMAG),'TIP_DOCU','TDCAUMAG');
                +",TDCODMAG ="+cp_NullLink(cp_ToStrODBC(w_TDCODMAG),'TIP_DOCU','TDCODMAG');
                +",TDCODMAT ="+cp_NullLink(cp_ToStrODBC(w_TDCODMAT),'TIP_DOCU','TDCODMAT');
                +",TDPRGSTA ="+cp_NullLink(cp_ToStrODBC(w_TDPRGSTA),'TIP_DOCU','TDPRGSTA');
                +",TDFLACCO ="+cp_NullLink(cp_ToStrODBC(w_TDFLACCO),'TIP_DOCU','TDFLACCO');
                +",TFFLGEFA ="+cp_NullLink(cp_ToStrODBC(w_TFFLGEFA),'TIP_DOCU','TFFLGEFA');
                +",TFFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_TFFLRAGG),'TIP_DOCU','TFFLRAGG');
                +",TDFLRICE ="+cp_NullLink(cp_ToStrODBC(w_TDFLRICE),'TIP_DOCU','TDFLRICE');
                +",TDFLCASH ="+cp_NullLink(cp_ToStrODBC(w_TDFLCASH),'TIP_DOCU','TDFLCASH');
                +",TDFLRISC ="+cp_NullLink(cp_ToStrODBC(w_TDFLRISC),'TIP_DOCU','TDFLRISC');
                +",TDFLCRIS ="+cp_NullLink(cp_ToStrODBC(w_TDFLCRIS),'TIP_DOCU','TDFLCRIS');
                +",TDFLNSRI ="+cp_NullLink(cp_ToStrODBC(w_TDFLNSRI),'TIP_DOCU','TDFLNSRI');
                +",TDFLVSRI ="+cp_NullLink(cp_ToStrODBC(w_TDFLVSRI),'TIP_DOCU','TDFLVSRI');
                +",TDFLDATT ="+cp_NullLink(cp_ToStrODBC(w_TDFLDATT),'TIP_DOCU','TDFLDATT');
                +",TDFLORAT ="+cp_NullLink(cp_ToStrODBC(w_TDFLORAT),'TIP_DOCU','TDFLORAT');
                +",TDFLPROV ="+cp_NullLink(cp_ToStrODBC(w_TDFLPROV),'TIP_DOCU','TDFLPROV');
                +",TDFLDTPR ="+cp_NullLink(cp_ToStrODBC(w_TDFLDTPR),'TIP_DOCU','TDFLDTPR');
                +",TDFLNSTA ="+cp_NullLink(cp_ToStrODBC(w_TDFLNSTA),'TIP_DOCU','TDFLNSTA');
                +",TDFLIMPA ="+cp_NullLink(cp_ToStrODBC(w_TDFLIMPA),'TIP_DOCU','TDFLIMPA');
                +",TDFLIMAC ="+cp_NullLink(cp_ToStrODBC(w_TDFLIMAC),'TIP_DOCU','TDFLIMAC');
                +",TDFLPACK ="+cp_NullLink(cp_ToStrODBC(w_TDFLPACK),'TIP_DOCU','TDFLPACK');
                +",TDASPETT ="+cp_NullLink(cp_ToStrODBC(w_TDASPETT),'TIP_DOCU','TDASPETT');
                +",TDNUMSCO ="+cp_NullLink(cp_ToStrODBC(w_TDNUMSCO),'TIP_DOCU','TDNUMSCO');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDDESDOC = w_TDDESDOC;
                    ,TDCATDOC = w_TDCATDOC;
                    ,TDFLVEAC = w_TDFLVEAC;
                    ,TDFLINTE = w_TDFLINTE;
                    ,TDPRODOC = w_TDPRODOC;
                    ,TDALFDOC = w_TDALFDOC;
                    ,TDSERPRO = w_TDSERPRO;
                    ,TDFLPPRO = w_TDFLPPRO;
                    ,TDCAUCON = w_TDCAUCON;
                    ,TDCAUMAG = w_TDCAUMAG;
                    ,TDCODMAG = w_TDCODMAG;
                    ,TDCODMAT = w_TDCODMAT;
                    ,TDPRGSTA = w_TDPRGSTA;
                    ,TDFLACCO = w_TDFLACCO;
                    ,TFFLGEFA = w_TFFLGEFA;
                    ,TFFLRAGG = w_TFFLRAGG;
                    ,TDFLRICE = w_TDFLRICE;
                    ,TDFLCASH = w_TDFLCASH;
                    ,TDFLRISC = w_TDFLRISC;
                    ,TDFLCRIS = w_TDFLCRIS;
                    ,TDFLNSRI = w_TDFLNSRI;
                    ,TDFLVSRI = w_TDFLVSRI;
                    ,TDFLDATT = w_TDFLDATT;
                    ,TDFLORAT = w_TDFLORAT;
                    ,TDFLPROV = w_TDFLPROV;
                    ,TDFLDTPR = w_TDFLDTPR;
                    ,TDFLNSTA = w_TDFLNSTA;
                    ,TDFLIMPA = w_TDFLIMPA;
                    ,TDFLIMAC = w_TDFLIMAC;
                    ,TDFLPACK = w_TDFLPACK;
                    ,TDASPETT = w_TDASPETT;
                    ,TDNUMSCO = w_TDNUMSCO;
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDSTACL1 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL1),'TIP_DOCU','TDSTACL1');
                +",TDSTACL2 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL2),'TIP_DOCU','TDSTACL2');
                +",TDSTACL3 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL3),'TIP_DOCU','TDSTACL3');
                +",TDSTACL4 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL4),'TIP_DOCU','TDSTACL4');
                +",TDSTACL5 ="+cp_NullLink(cp_ToStrODBC(w_TDSTACL5),'TIP_DOCU','TDSTACL5');
                +",TDESCCL1 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL1),'TIP_DOCU','TDESCCL1');
                +",TDESCCL2 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL2),'TIP_DOCU','TDESCCL2');
                +",TDESCCL3 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL3),'TIP_DOCU','TDESCCL3');
                +",TDESCCL4 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL4),'TIP_DOCU','TDESCCL4');
                +",TDESCCL5 ="+cp_NullLink(cp_ToStrODBC(w_TDESCCL5),'TIP_DOCU','TDESCCL5');
                +",TDTPNDOC ="+cp_NullLink(cp_ToStrODBC(w_TDTPNDOC),'TIP_DOCU','TDTPNDOC');
                +",TDTPVDOC ="+cp_NullLink(cp_ToStrODBC(w_TDTPVDOC),'TIP_DOCU','TDTPVDOC');
                +",TDSEQPRE ="+cp_NullLink(cp_ToStrODBC(w_TDSEQPRE),'TIP_DOCU','TDSEQPRE');
                +",TDSEQSCO ="+cp_NullLink(cp_ToStrODBC(w_TDSEQSCO),'TIP_DOCU','TDSEQSCO');
                +",TDSEQMA1 ="+cp_NullLink(cp_ToStrODBC(w_TDSEQMA1),'TIP_DOCU','TDSEQMA1');
                +",TDSEQMA2 ="+cp_NullLink(cp_ToStrODBC(w_TDSEQMA2),'TIP_DOCU','TDSEQMA2');
                +",TDNOPRSC ="+cp_NullLink(cp_ToStrODBC(w_TDNOPRSC),'TIP_DOCU','TDNOPRSC');
                +",TDFLPREF ="+cp_NullLink(cp_ToStrODBC(w_TDFLPREF),'TIP_DOCU','TDFLPREF');
                +",TDCODLIS ="+cp_NullLink(cp_ToStrODBC(w_TDCODLIS),'TIP_DOCU','TDCODLIS');
                +",TDQTADEF ="+cp_NullLink(cp_ToStrODBC(w_TDQTADEF),'TIP_DOCU','TDQTADEF');
                +",TDFLANAL ="+cp_NullLink(cp_ToStrODBC(w_TDFLANAL),'TIP_DOCU','TDFLANAL');
                +",TDFLSILI ="+cp_NullLink(cp_ToStrODBC(w_TDFLSILI),'TIP_DOCU','TDFLSILI');
                +",TDVOCECR ="+cp_NullLink(cp_ToStrODBC(w_TDVOCECR),'TIP_DOCU','TDVOCECR');
                +",TD_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_TD_SEGNO),'TIP_DOCU','TD_SEGNO');
                +",TDSINCFL ="+cp_NullLink(cp_ToStrODBC(w_TDSINCFL),'TIP_DOCU','TDSINCFL');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDSTACL1 = w_TDSTACL1;
                    ,TDSTACL2 = w_TDSTACL2;
                    ,TDSTACL3 = w_TDSTACL3;
                    ,TDSTACL4 = w_TDSTACL4;
                    ,TDSTACL5 = w_TDSTACL5;
                    ,TDESCCL1 = w_TDESCCL1;
                    ,TDESCCL2 = w_TDESCCL2;
                    ,TDESCCL3 = w_TDESCCL3;
                    ,TDESCCL4 = w_TDESCCL4;
                    ,TDESCCL5 = w_TDESCCL5;
                    ,TDTPNDOC = w_TDTPNDOC;
                    ,TDTPVDOC = w_TDTPVDOC;
                    ,TDSEQPRE = w_TDSEQPRE;
                    ,TDSEQSCO = w_TDSEQSCO;
                    ,TDSEQMA1 = w_TDSEQMA1;
                    ,TDSEQMA2 = w_TDSEQMA2;
                    ,TDNOPRSC = w_TDNOPRSC;
                    ,TDFLPREF = w_TDFLPREF;
                    ,TDCODLIS = w_TDCODLIS;
                    ,TDQTADEF = w_TDQTADEF;
                    ,TDFLANAL = w_TDFLANAL;
                    ,TDFLSILI = w_TDFLSILI;
                    ,TDVOCECR = w_TDVOCECR;
                    ,TD_SEGNO = w_TD_SEGNO;
                    ,TDSINCFL = w_TDSINCFL;
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Causale documento utilizzata nei documenti
              * --- Completamento dei dati con la descrizione e la causale contabile
              * --- Write into TIP_DOCU
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_DOCU_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TDDESDOC ="+cp_NullLink(cp_ToStrODBC(w_TDDESDOC),'TIP_DOCU','TDDESDOC');
                +",TDCAUCON ="+cp_NullLink(cp_ToStrODBC(w_TDCAUCON),'TIP_DOCU','TDCAUCON');
                    +i_ccchkf ;
                +" where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(w_TDTIPDOC);
                       )
              else
                update (i_cTable) set;
                    TDDESDOC = w_TDDESDOC;
                    ,TDCAUCON = w_TDCAUCON;
                    &i_ccchkf. ;
                 where;
                    TDTIPDOC = w_TDTIPDOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
            select _Curs_TIP_DOCU
            continue
          enddo
          use
        endif
      case this.oParentObject.w_Destinaz="LS"
        * --- Anag. Listini
        w_LSQUANTI = iif( empty(NVL(w_LSQUANTI," ")), "N", w_LSQUANTI )
        w_LSVALLIS = iif( empty(NVL(w_LSVALLIS," ")), g_PERVAL, w_LSVALLIS )
        this.oParentObject.w_ResoDett = trim( w_LSCODLIS )
        * --- Write into LISTINI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LISTINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LSDESLIS ="+cp_NullLink(cp_ToStrODBC(w_LSDESLIS),'LISTINI','LSDESLIS');
          +",LSIVALIS ="+cp_NullLink(cp_ToStrODBC(w_LSIVALIS),'LISTINI','LSIVALIS');
          +",LSVALLIS ="+cp_NullLink(cp_ToStrODBC(w_LSVALLIS),'LISTINI','LSVALLIS');
          +",LSQUANTI ="+cp_NullLink(cp_ToStrODBC(w_LSQUANTI),'LISTINI','LSQUANTI');
          +",LSFLSCON ="+cp_NullLink(cp_ToStrODBC(w_LSFLSCON),'LISTINI','LSFLSCON');
              +i_ccchkf ;
          +" where ";
              +"LSCODLIS = "+cp_ToStrODBC(w_LSCODLIS);
                 )
        else
          update (i_cTable) set;
              LSDESLIS = w_LSDESLIS;
              ,LSIVALIS = w_LSIVALIS;
              ,LSVALLIS = w_LSVALLIS;
              ,LSQUANTI = w_LSQUANTI;
              ,LSFLSCON = w_LSFLSCON;
              &i_ccchkf. ;
           where;
              LSCODLIS = w_LSCODLIS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="LA"
        * --- Articoli Listini
        w_LIDATDIS = iif( empty(NVL(w_LIDATDIS,"  -  -    ")), i_FINDAT, w_LIDATDIS )
        this.oParentObject.w_ResoDett = trim( w_LICODART+"/"+w_LICODLIS+"/"+ALLTRIM(STR(this.oParentObject.ULT_RNUM)) )
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(w_LICODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1;
            from (i_cTable) where;
                ARCODART = w_LICODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UMLIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into LIS_TINI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LIDATATT ="+cp_NullLink(cp_ToStrODBC(w_LIDATATT),'LIS_TINI','LIDATATT');
          +",LIDATDIS ="+cp_NullLink(cp_ToStrODBC(w_LIDATDIS),'LIS_TINI','LIDATDIS');
          +",LIUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_UMLIS),'LIS_TINI','LIUNIMIS');
              +i_ccchkf ;
          +" where ";
              +"LICODART = "+cp_ToStrODBC(w_LICODART);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
              +" and LICODLIS = "+cp_ToStrODBC(w_LICODLIS);
                 )
        else
          update (i_cTable) set;
              LIDATATT = w_LIDATATT;
              ,LIDATDIS = w_LIDATDIS;
              ,LIUNIMIS = this.w_UMLIS;
              &i_ccchkf. ;
           where;
              LICODART = w_LICODART;
              and CPROWNUM = this.oParentObject.ULT_RNUM;
              and LICODLIS = w_LICODLIS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorna prezzo in scaglioni listino
        if w_LAPREZZO > 0
          * --- Try
          local bErr_04F08B70
          bErr_04F08B70=bTrsErr
          this.Try_04F08B70()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04F08B70
          * --- End
          * --- Write into LIS_SCAG
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(w_LAPREZZO),'LIS_SCAG','LIPREZZO');
            +",LISCONT1 ="+cp_NullLink(cp_ToStrODBC(w_LASCONT1),'LIS_SCAG','LISCONT1');
            +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(w_LASCONT2),'LIS_SCAG','LISCONT2');
            +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(w_LASCONT3),'LIS_SCAG','LISCONT3');
                +i_ccchkf ;
            +" where ";
                +"LICODART = "+cp_ToStrODBC(w_LICODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                +" and LIQUANTI = "+cp_ToStrODBC(w_LAQUANTI);
                   )
          else
            update (i_cTable) set;
                LIPREZZO = w_LAPREZZO;
                ,LISCONT1 = w_LASCONT1;
                ,LISCONT2 = w_LASCONT2;
                ,LISCONT3 = w_LASCONT3;
                &i_ccchkf. ;
             where;
                LICODART = w_LICODART;
                and LIROWNUM = this.oParentObject.ULT_RNUM;
                and LIQUANTI = w_LAQUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz="CN"
        * --- Contratti
        w_COCODVAL = iif( empty(NVL(w_COCODVAL," ")), g_PERVAL, w_COCODVAL )
        w_COIVALIS = NVL(w_COIVALIS," ")
        w_COFLUCOA = iif( empty(NVL(w_COFLUCOA," ")),"N",w_COFLUCOA)
        this.oParentObject.w_ResoDett = trim( w_CONUMERO+"/"+ALLTRIM(w_COCODART)+ALLTRIM(w_COGRUMER)+"/"+ALLTRIM(STR(this.oParentObject.ULT_RNUM)) )
        w_GSQUANTI = iif(w_COQUANTI > 0,"S"," ")
        if this.w_INSMAS
          * --- faccio l'aggiornamento del master
          * --- Write into CON_TRAM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CON_TRAM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CODATCON ="+cp_NullLink(cp_ToStrODBC(w_CODATCON),'CON_TRAM','CODATCON');
            +",COTIPCLF ="+cp_NullLink(cp_ToStrODBC(w_COTIPCLF),'CON_TRAM','COTIPCLF');
            +",COCODCLF ="+cp_NullLink(cp_ToStrODBC(w_COCODCLF),'CON_TRAM','COCODCLF');
            +",COCATCOM ="+cp_NullLink(cp_ToStrODBC(w_COCATCOM),'CON_TRAM','COCATCOM');
            +",CODESCON ="+cp_NullLink(cp_ToStrODBC(w_CODESCON),'CON_TRAM','CODESCON');
            +",CO__NOTE ="+cp_NullLink(cp_ToStrODBC(w_CO__NOTE),'CON_TRAM','CO__NOTE');
            +",CODATINI ="+cp_NullLink(cp_ToStrODBC(w_CODATINI),'CON_TRAM','CODATINI');
            +",CODATFIN ="+cp_NullLink(cp_ToStrODBC(w_CODATFIN),'CON_TRAM','CODATFIN');
            +",COCODVAL ="+cp_NullLink(cp_ToStrODBC(w_COCODVAL),'CON_TRAM','COCODVAL');
            +",COIVALIS ="+cp_NullLink(cp_ToStrODBC(w_COIVALIS),'CON_TRAM','COIVALIS');
            +",COQUANTI ="+cp_NullLink(cp_ToStrODBC(w_GSQUANTI),'CON_TRAM','COQUANTI');
            +",COFLUCOA ="+cp_NullLink(cp_ToStrODBC(w_COFLUCOA),'CON_TRAM','COFLUCOA');
            +",COAFFIDA ="+cp_NullLink(cp_ToStrODBC(w_COAFFIDA),'CON_TRAM','COAFFIDA');
            +",CO__TIPO ="+cp_NullLink(cp_ToStrODBC(w_CO__TIPO),'CON_TRAM','CO__TIPO');
            +",COFLARTI ="+cp_NullLink(cp_ToStrODBC(w_COFLARTI),'CON_TRAM','COFLARTI');
                +i_ccchkf ;
            +" where ";
                +"CONUMERO = "+cp_ToStrODBC(w_CONUMERO);
                   )
          else
            update (i_cTable) set;
                CODATCON = w_CODATCON;
                ,COTIPCLF = w_COTIPCLF;
                ,COCODCLF = w_COCODCLF;
                ,COCATCOM = w_COCATCOM;
                ,CODESCON = w_CODESCON;
                ,CO__NOTE = w_CO__NOTE;
                ,CODATINI = w_CODATINI;
                ,CODATFIN = w_CODATFIN;
                ,COCODVAL = w_COCODVAL;
                ,COIVALIS = w_COIVALIS;
                ,COQUANTI = w_GSQUANTI;
                ,COFLUCOA = w_COFLUCOA;
                ,COAFFIDA = w_COAFFIDA;
                ,CO__TIPO = w_CO__TIPO;
                ,COFLARTI = w_COFLARTI;
                &i_ccchkf. ;
             where;
                CONUMERO = w_CONUMERO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        w_COCODVAR = IIF(TYPE("w_COCODVAR")="U","",w_COCODVAR)
        w_COSCONT1 = IIF(TYPE("w_COSCONT1")="U",0,w_COSCONT1)
        w_COSCONT2 = IIF(TYPE("w_COSCONT2")="U",0,w_COSCONT2)
        w_COSCONT3 = IIF(TYPE("w_COSCONT3")="U",0,w_COSCONT3)
        w_COSCONT4 = IIF(TYPE("w_COSCONT4")="U",0,w_COSCONT4)
        w_COPERPRO = IIF(TYPE("w_COPERPRO")="U",0,w_COPERPRO)
        w_COQTAMIN = IIF(TYPE("w_COQTAMIN")="U",0,w_COQTAMIN)
        w_COQTACON = IIF(TYPE("w_COQTACON")="U",0,w_COQTACON)
        w_COLOTMUL = IIF(TYPE("w_COLOTMUL")="U",0,w_COLOTMUL)
        w_COQTARIC = IIF(TYPE("w_COQTARIC")="U",0,w_COQTARIC)
        w_COGIOAPP = IIF(TYPE("w_COGIOAPP")="U",0,w_COGIOAPP)
        w_CORIFFOR = IIF(TYPE("w_CORIFFOR")="U","",w_CORIFFOR)
        w_COPRIORI = IIF(TYPE("w_COPRIORI")="U",0,w_COPRIORI)
        w_COPERRIP = IIF(TYPE("w_COPERRIP")="U",0,w_COPERRIP)
        if this.w_INSDET
          * --- faccio l'aggiornamento del detail
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(w_COCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1;
              from (i_cTable) where;
                  ARCODART = w_COCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UMLIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_GSQUANTI="S"
            * --- Write into CON_TRAD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CON_TRAD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COGRUMER ="+cp_NullLink(cp_ToStrODBC(w_COGRUMER),'CON_TRAD','COGRUMER');
              +",COCODART ="+cp_NullLink(cp_ToStrODBC(w_COCODART),'CON_TRAD','COCODART');
              +",COPERPRO ="+cp_NullLink(cp_ToStrODBC(w_COPERPRO),'CON_TRAD','COPERPRO');
              +",COQTAMIN ="+cp_NullLink(cp_ToStrODBC(w_COQTAMIN),'CON_TRAD','COQTAMIN');
              +",COQTACON ="+cp_NullLink(cp_ToStrODBC(w_COQTACON),'CON_TRAD','COQTACON');
              +",COLOTMUL ="+cp_NullLink(cp_ToStrODBC(w_COLOTMUL),'CON_TRAD','COLOTMUL');
              +",COQTARIC ="+cp_NullLink(cp_ToStrODBC(w_COQTARIC),'CON_TRAD','COQTARIC');
              +",COGIOAPP ="+cp_NullLink(cp_ToStrODBC(w_COGIOAPP),'CON_TRAD','COGIOAPP');
              +",CORIFFOR ="+cp_NullLink(cp_ToStrODBC(w_CORIFFOR),'CON_TRAD','CORIFFOR');
              +",COPRIORI ="+cp_NullLink(cp_ToStrODBC(w_COPRIORI),'CON_TRAD','COPRIORI');
              +",COPERRIP ="+cp_NullLink(cp_ToStrODBC(w_COPERRIP),'CON_TRAD','COPERRIP');
              +",COUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_UMLIS),'CON_TRAD','COUNIMIS');
                  +i_ccchkf ;
              +" where ";
                  +"CONUMERO = "+cp_ToStrODBC(w_CONUMERO);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                     )
            else
              update (i_cTable) set;
                  COGRUMER = w_COGRUMER;
                  ,COCODART = w_COCODART;
                  ,COPERPRO = w_COPERPRO;
                  ,COQTAMIN = w_COQTAMIN;
                  ,COQTACON = w_COQTACON;
                  ,COLOTMUL = w_COLOTMUL;
                  ,COQTARIC = w_COQTARIC;
                  ,COGIOAPP = w_COGIOAPP;
                  ,CORIFFOR = w_CORIFFOR;
                  ,COPRIORI = w_COPRIORI;
                  ,COPERRIP = w_COPERRIP;
                  ,COUNIMIS = this.w_UMLIS;
                  &i_ccchkf. ;
               where;
                  CONUMERO = w_CONUMERO;
                  and CPROWNUM = this.oParentObject.ULT_RNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into CON_TRAD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CON_TRAD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COGRUMER ="+cp_NullLink(cp_ToStrODBC(w_COGRUMER),'CON_TRAD','COGRUMER');
              +",COCODART ="+cp_NullLink(cp_ToStrODBC(w_COCODART),'CON_TRAD','COCODART');
              +",COPREZZO ="+cp_NullLink(cp_ToStrODBC(w_COPREZZO),'CON_TRAD','COPREZZO');
              +",COSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT1),'CON_TRAD','COSCONT1');
              +",COSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT2),'CON_TRAD','COSCONT2');
              +",COSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT3),'CON_TRAD','COSCONT3');
              +",COSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT4),'CON_TRAD','COSCONT4');
              +",COPERPRO ="+cp_NullLink(cp_ToStrODBC(w_COPERPRO),'CON_TRAD','COPERPRO');
              +",COQTAMIN ="+cp_NullLink(cp_ToStrODBC(w_COQTAMIN),'CON_TRAD','COQTAMIN');
              +",COQTACON ="+cp_NullLink(cp_ToStrODBC(w_COQTACON),'CON_TRAD','COQTACON');
              +",COLOTMUL ="+cp_NullLink(cp_ToStrODBC(w_COLOTMUL),'CON_TRAD','COLOTMUL');
              +",COQTARIC ="+cp_NullLink(cp_ToStrODBC(w_COQTARIC),'CON_TRAD','COQTARIC');
              +",COGIOAPP ="+cp_NullLink(cp_ToStrODBC(w_COGIOAPP),'CON_TRAD','COGIOAPP');
              +",CORIFFOR ="+cp_NullLink(cp_ToStrODBC(w_CORIFFOR),'CON_TRAD','CORIFFOR');
              +",COPRIORI ="+cp_NullLink(cp_ToStrODBC(w_COPRIORI),'CON_TRAD','COPRIORI');
              +",COPERRIP ="+cp_NullLink(cp_ToStrODBC(w_COPERRIP),'CON_TRAD','COPERRIP');
              +",COUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_UMLIS),'CON_TRAD','COUNIMIS');
                  +i_ccchkf ;
              +" where ";
                  +"CONUMERO = "+cp_ToStrODBC(w_CONUMERO);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                     )
            else
              update (i_cTable) set;
                  COGRUMER = w_COGRUMER;
                  ,COCODART = w_COCODART;
                  ,COPREZZO = w_COPREZZO;
                  ,COSCONT1 = w_COSCONT1;
                  ,COSCONT2 = w_COSCONT2;
                  ,COSCONT3 = w_COSCONT3;
                  ,COSCONT4 = w_COSCONT4;
                  ,COPERPRO = w_COPERPRO;
                  ,COQTAMIN = w_COQTAMIN;
                  ,COQTACON = w_COQTACON;
                  ,COLOTMUL = w_COLOTMUL;
                  ,COQTARIC = w_COQTARIC;
                  ,COGIOAPP = w_COGIOAPP;
                  ,CORIFFOR = w_CORIFFOR;
                  ,COPRIORI = w_COPRIORI;
                  ,COPERRIP = w_COPERRIP;
                  ,COUNIMIS = this.w_UMLIS;
                  &i_ccchkf. ;
               where;
                  CONUMERO = w_CONUMERO;
                  and CPROWNUM = this.oParentObject.ULT_RNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if w_GSQUANTI="S"
          * --- faccio l'aggiornamento degli scaglioni
          * --- Write into CON_COSC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CON_COSC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_COSC_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COPREZZO ="+cp_NullLink(cp_ToStrODBC(w_COPREZZO),'CON_COSC','COPREZZO');
            +",COSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT1),'CON_COSC','COSCONT1');
            +",COSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT2),'CON_COSC','COSCONT2');
            +",COSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT3),'CON_COSC','COSCONT3');
            +",COSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_COSCONT4),'CON_COSC','COSCONT4');
            +",COPERPRO ="+cp_NullLink(cp_ToStrODBC(w_COPERPRO),'CON_COSC','COPERPRO');
                +i_ccchkf ;
            +" where ";
                +"COCODICE = "+cp_ToStrODBC(w_CONUMERO);
                +" and CONUMROW = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                +" and COQUANTI = "+cp_ToStrODBC(w_COQUANTI);
                   )
          else
            update (i_cTable) set;
                COPREZZO = w_COPREZZO;
                ,COSCONT1 = w_COSCONT1;
                ,COSCONT2 = w_COSCONT2;
                ,COSCONT3 = w_COSCONT3;
                ,COSCONT4 = w_COSCONT4;
                ,COPERPRO = w_COPERPRO;
                &i_ccchkf. ;
             where;
                COCODICE = w_CONUMERO;
                and CONUMROW = this.oParentObject.ULT_RNUM;
                and COQUANTI = w_COQUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz="UM"
        * --- Unit� di Misura
        this.oParentObject.w_ResoDett = trim( w_UMCODICE )
        * --- Write into UNIMIS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.UNIMIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"UMDESCRI ="+cp_NullLink(cp_ToStrODBC(w_UMDESCRI),'UNIMIS','UMDESCRI');
          +",UMFLFRAZ ="+cp_NullLink(cp_ToStrODBC(w_UMFLFRAZ),'UNIMIS','UMFLFRAZ');
              +i_ccchkf ;
          +" where ";
              +"UMCODICE = "+cp_ToStrODBC(w_UMCODICE);
                 )
        else
          update (i_cTable) set;
              UMDESCRI = w_UMDESCRI;
              ,UMFLFRAZ = w_UMFLFRAZ;
              &i_ccchkf. ;
           where;
              UMCODICE = w_UMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CP"
        * --- Movimentazione Provvigioni
        this.oParentObject.w_ResoDett = trim( w_GPCODICE )
        * --- Write into GRUPRO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.GRUPRO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUPRO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GRUPRO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GPDESCRI ="+cp_NullLink(cp_ToStrODBC(w_GPDESCRI),'GRUPRO','GPDESCRI');
          +",GPTIPPRO ="+cp_NullLink(cp_ToStrODBC(w_GPTIPPRO),'GRUPRO','GPTIPPRO');
          +",GPPERDFA ="+cp_NullLink(cp_ToStrODBC(w_GPPERDFA),'GRUPRO','GPPERDFA');
          +",GPPERDSC ="+cp_NullLink(cp_ToStrODBC(w_GPPERDSC),'GRUPRO','GPPERDSC');
          +",GPPERDIN ="+cp_NullLink(cp_ToStrODBC(w_GPPERDIN),'GRUPRO','GPPERDIN');
              +i_ccchkf ;
          +" where ";
              +"GPCODICE = "+cp_ToStrODBC(w_GPCODICE);
                 )
        else
          update (i_cTable) set;
              GPDESCRI = w_GPDESCRI;
              ,GPTIPPRO = w_GPTIPPRO;
              ,GPPERDFA = w_GPPERDFA;
              ,GPPERDSC = w_GPPERDSC;
              ,GPPERDIN = w_GPPERDIN;
              &i_ccchkf. ;
           where;
              GPCODICE = w_GPCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="SM"
        * --- Sconti e Maggiorazioni
        this.oParentObject.w_ResoDett = trim( w_TSCATCLI+"/"+w_TSCATARR+"/"+ALLTRIM(STR(this.oParentObject.ULT_RNUM)) )
        * --- Write into TAB_SCON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_SCON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_SCON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_SCON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TSCATCLF ="+cp_NullLink(cp_ToStrODBC(w_TSCATCLF),'TAB_SCON','TSCATCLF');
          +",TSCATART ="+cp_NullLink(cp_ToStrODBC(w_TSCATART),'TAB_SCON','TSCATART');
          +",TSDATINI ="+cp_NullLink(cp_ToStrODBC(w_TSDATINI),'TAB_SCON','TSDATINI');
          +",TSDATFIN ="+cp_NullLink(cp_ToStrODBC(w_TSDATFIN),'TAB_SCON','TSDATFIN');
          +",TSSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_TSSCONT1),'TAB_SCON','TSSCONT1');
          +",TSSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_TSSCONT2),'TAB_SCON','TSSCONT2');
          +",TSSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_TSSCONT3),'TAB_SCON','TSSCONT3');
          +",TSSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_TSSCONT4),'TAB_SCON','TSSCONT4');
              +i_ccchkf ;
          +" where ";
              +"TSCATCLI = "+cp_ToStrODBC(w_TSCATCLI);
              +" and TSCATARR = "+cp_ToStrODBC(w_TSCATARR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              TSCATCLF = w_TSCATCLF;
              ,TSCATART = w_TSCATART;
              ,TSDATINI = w_TSDATINI;
              ,TSDATFIN = w_TSDATFIN;
              ,TSSCONT1 = w_TSSCONT1;
              ,TSSCONT2 = w_TSSCONT2;
              ,TSSCONT3 = w_TSSCONT3;
              ,TSSCONT4 = w_TSSCONT4;
              &i_ccchkf. ;
           where;
              TSCATCLI = w_TSCATCLI;
              and TSCATARR = w_TSCATARR;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="GM"
        * --- Gruppi Merceologici
        this.oParentObject.w_ResoDett = trim( w_GMCODICE )
        * --- Write into GRUMERC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.GRUMERC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUMERC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GRUMERC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GMDESCRI ="+cp_NullLink(cp_ToStrODBC(w_GMDESCRI),'GRUMERC','GMDESCRI');
              +i_ccchkf ;
          +" where ";
              +"GMCODICE = "+cp_ToStrODBC(w_GMCODICE);
                 )
        else
          update (i_cTable) set;
              GMDESCRI = w_GMDESCRI;
              &i_ccchkf. ;
           where;
              GMCODICE = w_GMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CG"
        * --- Categorie Omogenee
        this.oParentObject.w_ResoDett = trim( w_OMCODICE )
        * --- Write into CATEGOMO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CATEGOMO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CATEGOMO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CATEGOMO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OMDESCRI ="+cp_NullLink(cp_ToStrODBC(w_OMDESCRI),'CATEGOMO','OMDESCRI');
          +",OMCONRIM ="+cp_NullLink(cp_ToStrODBC(w_OMCONRIM),'CATEGOMO','OMCONRIM');
          +",OMCONRIC ="+cp_NullLink(cp_ToStrODBC(w_OMCONRIC),'CATEGOMO','OMCONRIC');
              +i_ccchkf ;
          +" where ";
              +"OMCODICE = "+cp_ToStrODBC(w_OMCODICE);
                 )
        else
          update (i_cTable) set;
              OMDESCRI = w_OMDESCRI;
              ,OMCONRIM = w_OMCONRIM;
              ,OMCONRIC = w_OMCONRIC;
              &i_ccchkf. ;
           where;
              OMCODICE = w_OMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TT"
        * --- Tipi Transazioni Intra
        this.oParentObject.w_ResoDett = trim( w_TTCODICE )
        * --- Write into TIPITRAN
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TIPITRAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPITRAN_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TIPITRAN_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TTDESCRI ="+cp_NullLink(cp_ToStrODBC(w_TTDESCRI),'TIPITRAN','TTDESCRI');
              +i_ccchkf ;
          +" where ";
              +"TTCODICE = "+cp_ToStrODBC(w_TTCODICE);
                 )
        else
          update (i_cTable) set;
              TTDESCRI = w_TTDESCRI;
              &i_ccchkf. ;
           where;
              TTCODICE = w_TTCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="NO"
        * --- Nomenclature
        this.oParentObject.w_ResoDett = trim( w_NMCODICE )
        * --- Write into NOMENCLA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.NOMENCLA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.NOMENCLA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NMDESCRI ="+cp_NullLink(cp_ToStrODBC(w_NMDESCRI),'NOMENCLA','NMDESCRI');
          +",NMUNISUP ="+cp_NullLink(cp_ToStrODBC(w_NMUNISUP),'NOMENCLA','NMUNISUP');
              +i_ccchkf ;
          +" where ";
              +"NMCODICE = "+cp_ToStrODBC(w_NMCODICE);
                 )
        else
          update (i_cTable) set;
              NMDESCRI = w_NMDESCRI;
              ,NMUNISUP = w_NMUNISUP;
              &i_ccchkf. ;
           where;
              NMCODICE = w_NMCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TC"
        * --- Tipi Confezioni
        this.oParentObject.w_ResoDett = trim( w_TCCODICE )
        * --- Write into TIPICONF
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TIPICONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIPICONF_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TIPICONF_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_TCDESCRI),'TIPICONF','TCDESCRI');
              +i_ccchkf ;
          +" where ";
              +"TCCODICE = "+cp_ToStrODBC(w_TCCODICE);
                 )
        else
          update (i_cTable) set;
              TCDESCRI = w_TCDESCRI;
              &i_ccchkf. ;
           where;
              TCCODICE = w_TCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IN"
        * --- Inventari
        * --- Dettaglio inventario
        this.oParentObject.w_ResoDett = ah_msgformat( "Inventario %1/%2 articolo %3" , trim( w_DINUMINV ) , w_DICODESE , w_DICODICE )
        * --- Write into INVEDETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INVEDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INVEDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DIQTAESI ="+cp_NullLink(cp_ToStrODBC(w_DIQTAESI),'INVEDETT','DIQTAESI');
          +",DIQTAVEN ="+cp_NullLink(cp_ToStrODBC(w_DIQTAVEN),'INVEDETT','DIQTAVEN');
          +",DIQTAACQ ="+cp_NullLink(cp_ToStrODBC(w_DIQTAACQ),'INVEDETT','DIQTAACQ');
          +",DIPRZMPA ="+cp_NullLink(cp_ToStrODBC(w_DIPRZMPA),'INVEDETT','DIPRZMPA');
          +",DIPRZMPP ="+cp_NullLink(cp_ToStrODBC(w_DIPRZMPP),'INVEDETT','DIPRZMPP');
          +",DIPRZULT ="+cp_NullLink(cp_ToStrODBC(w_DIPRZULT),'INVEDETT','DIPRZULT');
          +",DICOSMPA ="+cp_NullLink(cp_ToStrODBC(w_DICOSMPA),'INVEDETT','DICOSMPA');
          +",DICOSMPP ="+cp_NullLink(cp_ToStrODBC(w_DICOSMPP),'INVEDETT','DICOSMPP');
          +",DICOSULT ="+cp_NullLink(cp_ToStrODBC(w_DICOSULT),'INVEDETT','DICOSULT');
          +",DICOSSTA ="+cp_NullLink(cp_ToStrODBC(w_DICOSSTA),'INVEDETT','DICOSSTA');
          +",DICOSLCO ="+cp_NullLink(cp_ToStrODBC(w_DICOSLCO),'INVEDETT','DICOSLCO');
          +",DICOSLSC ="+cp_NullLink(cp_ToStrODBC(w_DICOSLSC),'INVEDETT','DICOSLSC');
          +",DICOSFCO ="+cp_NullLink(cp_ToStrODBC(w_DICOSFCO),'INVEDETT','DICOSFCO');
          +",DIQTAESR ="+cp_NullLink(cp_ToStrODBC(w_DIQTAESR),'INVEDETT','DIQTAESR');
          +",DIQTAVER ="+cp_NullLink(cp_ToStrODBC(w_DIQTAVER),'INVEDETT','DIQTAVER');
          +",DIQTAACR ="+cp_NullLink(cp_ToStrODBC(w_DIQTAACR),'INVEDETT','DIQTAACR');
              +i_ccchkf ;
          +" where ";
              +"DINUMINV = "+cp_ToStrODBC(w_DINUMINV);
              +" and DICODESE = "+cp_ToStrODBC(w_DICODESE);
              +" and DICODICE = "+cp_ToStrODBC(w_DICODICE);
                 )
        else
          update (i_cTable) set;
              DIQTAESI = w_DIQTAESI;
              ,DIQTAVEN = w_DIQTAVEN;
              ,DIQTAACQ = w_DIQTAACQ;
              ,DIPRZMPA = w_DIPRZMPA;
              ,DIPRZMPP = w_DIPRZMPP;
              ,DIPRZULT = w_DIPRZULT;
              ,DICOSMPA = w_DICOSMPA;
              ,DICOSMPP = w_DICOSMPP;
              ,DICOSULT = w_DICOSULT;
              ,DICOSSTA = w_DICOSSTA;
              ,DICOSLCO = w_DICOSLCO;
              ,DICOSLSC = w_DICOSLSC;
              ,DICOSFCO = w_DICOSFCO;
              ,DIQTAESR = w_DIQTAESR;
              ,DIQTAVER = w_DIQTAVER;
              ,DIQTAACR = w_DIQTAACR;
              &i_ccchkf. ;
           where;
              DINUMINV = w_DINUMINV;
              and DICODESE = w_DICODESE;
              and DICODICE = w_DICODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IC"
        * --- Inventari con valorizzazione LIFO Continuo
        this.oParentObject.w_ResoDett = ah_msgformat( "Inventario %1/%2 articolo %3" , trim( w_ISNUMINV ) , w_ISCODESE , w_ISCODICE )
        * --- Write into LIFOCONT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LIFOCONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LIFOCONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ISDATMOV ="+cp_NullLink(cp_ToStrODBC(w_ISDATMOV),'LIFOCONT','ISDATMOV');
          +",ISQTAESI ="+cp_NullLink(cp_ToStrODBC(w_ISQTAESI),'LIFOCONT','ISQTAESI');
          +",ISVALUNI ="+cp_NullLink(cp_ToStrODBC(w_ISVALUNI),'LIFOCONT','ISVALUNI');
              +i_ccchkf ;
          +" where ";
              +"ISNUMINV = "+cp_ToStrODBC(w_ISNUMINV);
              +" and ISCODESE = "+cp_ToStrODBC(w_ISCODESE);
              +" and ISCODICE = "+cp_ToStrODBC(w_ISCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              ISDATMOV = w_ISDATMOV;
              ,ISQTAESI = w_ISQTAESI;
              ,ISVALUNI = w_ISVALUNI;
              &i_ccchkf. ;
           where;
              ISNUMINV = w_ISNUMINV;
              and ISCODESE = w_ISCODESE;
              and ISCODICE = w_ISCODICE;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IS"
        * --- Inventari con valorizzazione LIFO Scatti
        this.oParentObject.w_ResoDett = ah_msgformat( "Inventario %1/%2 articolo %3" , trim( w_ISNUMINV ) , w_ISCODESE , w_ISCODICE )
        * --- Write into LIFOSCAT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LIFOSCAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ISQTAESI ="+cp_NullLink(cp_ToStrODBC(w_ISQTAESI),'LIFOSCAT','ISQTAESI');
          +",ISVALUNI ="+cp_NullLink(cp_ToStrODBC(w_ISVALUNI),'LIFOSCAT','ISVALUNI');
          +",ISDATINI ="+cp_NullLink(cp_ToStrODBC(w_ISDATINI),'LIFOSCAT','ISDATINI');
          +",ISDATFIN ="+cp_NullLink(cp_ToStrODBC(w_ISDATFIN),'LIFOSCAT','ISDATFIN');
              +i_ccchkf ;
          +" where ";
              +"ISNUMINV = "+cp_ToStrODBC(w_ISNUMINV);
              +" and ISCODESE = "+cp_ToStrODBC(w_ISCODESE);
              +" and ISCODICE = "+cp_ToStrODBC(w_ISCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              ISQTAESI = w_ISQTAESI;
              ,ISVALUNI = w_ISVALUNI;
              ,ISDATINI = w_ISDATINI;
              ,ISDATFIN = w_ISDATFIN;
              &i_ccchkf. ;
           where;
              ISNUMINV = w_ISNUMINV;
              and ISCODESE = w_ISCODESE;
              and ISCODICE = w_ISCODICE;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="IF"
        * --- Inventari con valorizzazione FIFO Continuo
        this.oParentObject.w_ResoDett = ah_msgformat( "Inventario %1/%2 articolo %3" , trim( w_ISNUMINV ) , w_ISCODESE , w_ISCODICE )
        * --- Write into FIFOCONT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FIFOCONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FIFOCONT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ISDATMOV ="+cp_NullLink(cp_ToStrODBC(w_ISDATMOV),'FIFOCONT','ISDATMOV');
          +",ISQTAESI ="+cp_NullLink(cp_ToStrODBC(w_ISQTAESI),'FIFOCONT','ISQTAESI');
          +",ISVALUNI ="+cp_NullLink(cp_ToStrODBC(w_ISVALUNI),'FIFOCONT','ISVALUNI');
              +i_ccchkf ;
          +" where ";
              +"ISNUMINV = "+cp_ToStrODBC(w_ISNUMINV);
              +" and ISCODESE = "+cp_ToStrODBC(w_ISCODESE);
              +" and ISCODICE = "+cp_ToStrODBC(w_ISCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              ISDATMOV = w_ISDATMOV;
              ,ISQTAESI = w_ISQTAESI;
              ,ISVALUNI = w_ISVALUNI;
              &i_ccchkf. ;
           where;
              ISNUMINV = w_ISNUMINV;
              and ISCODESE = w_ISCODESE;
              and ISCODICE = w_ISCODICE;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CI"
        * --- Categorie Contabili Articoli
        this.oParentObject.w_ResoDett = trim( w_C1CODICE )
        * --- Write into CACOARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CACOARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CACOARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"C1DESCRI ="+cp_NullLink(cp_ToStrODBC(w_C1DESCRI),'CACOARTI','C1DESCRI');
              +i_ccchkf ;
          +" where ";
              +"C1CODICE = "+cp_ToStrODBC(w_C1CODICE);
                 )
        else
          update (i_cTable) set;
              C1DESCRI = w_C1DESCRI;
              &i_ccchkf. ;
           where;
              C1CODICE = w_C1CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="CX"
        * --- Contropartite Ven./Acq.
        this.oParentObject.w_ResoDett = trim( w_CVCODCLI ) + " + " + trim( w_CVCODART )
        w_CVCONOMV = iif( empty(w_CVCONOMV), this.oParentObject.w_XXCONOMA, w_CVCONOMV)
        w_CVIVAOMA = iif( empty(w_CVIVAOMA), this.oParentObject.w_XXCONIOM, w_CVIVAOMA)
        * --- Try
        local bErr_04F324A0
        bErr_04F324A0=bTrsErr
        this.Try_04F324A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_ResoMode = "NOCONTRO"
          this.oParentObject.Pag4()
          * --- accept error
          bTrsErr=.f.
          store " " to w_CVCONRIC,w_CVCONOMV, w_CVCONACQ, w_CVCONOMA, w_CVIVAOMA, w_CVIVAOMC,w_CVCONNCC, w_CVCONNCF
          * --- Write into CONTVEAC
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTVEAC_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CVTIPCON ="+cp_NullLink(cp_ToStrODBC(w_CVTIPCON),'CONTVEAC','CVTIPCON');
            +",CVCONRIC ="+cp_NullLink(cp_ToStrODBC(w_CVCONRIC),'CONTVEAC','CVCONRIC');
            +",CVCONOMV ="+cp_NullLink(cp_ToStrODBC(w_CVCONOMV),'CONTVEAC','CVCONOMV');
            +",CVCONACQ ="+cp_NullLink(cp_ToStrODBC(w_CVCONACQ),'CONTVEAC','CVCONACQ');
            +",CVCONOMA ="+cp_NullLink(cp_ToStrODBC(w_CVCONOMA),'CONTVEAC','CVCONOMA');
            +",CVIVAOMA ="+cp_NullLink(cp_ToStrODBC(w_CVIVAOMA),'CONTVEAC','CVIVAOMA');
            +",CVIVAOMC ="+cp_NullLink(cp_ToStrODBC(w_CVIVAOMC),'CONTVEAC','CVIVAOMC');
            +",CVCONNCC ="+cp_NullLink(cp_ToStrODBC(w_CVCONNCC),'CONTVEAC','CVCONNCC');
            +",CVCONNCF ="+cp_NullLink(cp_ToStrODBC(w_CVCONNCF),'CONTVEAC','CVCONNCF');
                +i_ccchkf ;
            +" where ";
                +"CVCODART = "+cp_ToStrODBC(w_CVCODART);
                +" and CVCODCLI = "+cp_ToStrODBC(w_CVCODCLI);
                   )
          else
            update (i_cTable) set;
                CVTIPCON = w_CVTIPCON;
                ,CVCONRIC = w_CVCONRIC;
                ,CVCONOMV = w_CVCONOMV;
                ,CVCONACQ = w_CVCONACQ;
                ,CVCONOMA = w_CVCONOMA;
                ,CVIVAOMA = w_CVIVAOMA;
                ,CVIVAOMC = w_CVIVAOMC;
                ,CVCONNCC = w_CVCONNCC;
                ,CVCONNCF = w_CVCONNCF;
                &i_ccchkf. ;
             where;
                CVCODART = w_CVCODART;
                and CVCODCLI = w_CVCODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04F324A0
        * --- End
      case this.oParentObject.w_Destinaz="TO"
        * --- Tipologie Colli
        this.oParentObject.w_ResoDett = trim( w_TCCODICE )
        * --- Write into TIP_COLL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TIP_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_COLL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_COLL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_TCDESCRI),'TIP_COLL','TCDESCRI');
              +i_ccchkf ;
          +" where ";
              +"TCCODICE = "+cp_ToStrODBC(w_TCCODICE);
                 )
        else
          update (i_cTable) set;
              TCDESCRI = w_TCDESCRI;
              &i_ccchkf. ;
           where;
              TCCODICE = w_TCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TA"
        * --- Traduzioni Articoli
        this.oParentObject.w_ResoDett = trim( w_LGCODICE )+" "+trim( w_LGCODLIN )
        * --- Write into TRADARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LGDESCOD ="+cp_NullLink(cp_ToStrODBC(w_LGDESCOD),'TRADARTI','LGDESCOD');
          +",LGDESSUP ="+cp_NullLink(cp_ToStrODBC(w_LGDESSUP),'TRADARTI','LGDESSUP');
              +i_ccchkf ;
          +" where ";
              +"LGCODICE = "+cp_ToStrODBC(w_LGCODICE);
              +" and LGCODLIN = "+cp_ToStrODBC(w_LGCODLIN);
                 )
        else
          update (i_cTable) set;
              LGDESCOD = w_LGDESCOD;
              ,LGDESSUP = w_LGDESSUP;
              &i_ccchkf. ;
           where;
              LGCODICE = w_LGCODICE;
              and LGCODLIN = w_LGCODLIN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TM"
        * --- Traduzioni Causali Magazzino
        this.oParentObject.w_ResoDett = trim( w_LGCODICE )+" "+trim( w_LGCODLIN )
        * --- Write into TRADCAMA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TRADCAMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADCAMA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADCAMA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LGDESCOD ="+cp_NullLink(cp_ToStrODBC(w_LGDESCOD),'TRADCAMA','LGDESCOD');
              +i_ccchkf ;
          +" where ";
              +"LGCODICE = "+cp_ToStrODBC(w_LGCODICE);
              +" and LGCODLIN = "+cp_ToStrODBC(w_LGCODLIN);
                 )
        else
          update (i_cTable) set;
              LGDESCOD = w_LGDESCOD;
              &i_ccchkf. ;
           where;
              LGCODICE = w_LGCODICE;
              and LGCODLIN = w_LGCODLIN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="TP"
        * --- Traduzioni Pagamenti
        this.oParentObject.w_ResoDett = trim( w_LGCODICE )+" "+trim( w_LGCODLIN )
        * --- Write into TRADPAGA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TRADPAGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADPAGA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADPAGA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LGDESCOD ="+cp_NullLink(cp_ToStrODBC(w_LGDESCOD),'TRADPAGA','LGDESCOD');
              +i_ccchkf ;
          +" where ";
              +"LGCODICE = "+cp_ToStrODBC(w_LGCODICE);
              +" and LGCODLIN = "+cp_ToStrODBC(w_LGCODLIN);
                 )
        else
          update (i_cTable) set;
              LGDESCOD = w_LGDESCOD;
              &i_ccchkf. ;
           where;
              LGCODICE = w_LGCODICE;
              and LGCODLIN = w_LGCODLIN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AA"
        this.oParentObject.w_ResoDett = trim( w_ARCODICE+"/"+w_ARCODALT+"/"+ALLTRIM(STR(this.oParentObject.ULT_RNUM)) )
        * --- Write into ART_ALTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ALTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ALTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARDESALT ="+cp_NullLink(cp_ToStrODBC(w_ARDESALT),'ART_ALTE','ARDESALT');
              +i_ccchkf ;
          +" where ";
              +"ARCODICE = "+cp_ToStrODBC(w_ARCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
              +" and ARCODALT = "+cp_ToStrODBC(w_ARCODALT);
                 )
        else
          update (i_cTable) set;
              ARDESALT = w_ARDESALT;
              &i_ccchkf. ;
           where;
              ARCODICE = w_ARCODICE;
              and CPROWNUM = this.oParentObject.ULT_RNUM;
              and ARCODALT = w_ARCODALT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return
  proc Try_04E916D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CACODART"+",CADESSUP"+",CATIPCON"+",CATIPBAR"+",CA__TIPO"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_CACODICE),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_ARDESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(w_ARCODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(w_ARDESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(w_CATIPCON),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC("0"),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC(w_CA__TIPO),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC("S"),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'KEY_ARTI','UTDC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',w_CACODICE,'CADESART',w_ARDESART,'CACODART',w_ARCODART,'CADESSUP',w_ARDESSUP,'CATIPCON',w_CATIPCON,'CATIPBAR',"0",'CA__TIPO',w_CA__TIPO,'CAFLSTAM',"S",'CAOPERAT',"*",'CAMOLTIP',1,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
      insert into (i_cTable) (CACODICE,CADESART,CACODART,CADESSUP,CATIPCON,CATIPBAR,CA__TIPO,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTDC &i_ccchkf. );
         values (;
           w_CACODICE;
           ,w_ARDESART;
           ,w_ARCODART;
           ,w_ARDESSUP;
           ,w_CATIPCON;
           ,"0";
           ,w_CA__TIPO;
           ,"S";
           ,"*";
           ,1;
           ,this.oParentObject.w_CreVarUte;
           ,this.oParentObject.w_CreVarDat;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E8D418()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into INVEDETT
    i_nConn=i_TableProp[this.INVEDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INVEDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DINUMINV"+",DICODESE"+",DICODART"+",DICOSULT"+",DICODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_INNUMINV),'INVEDETT','DINUMINV');
      +","+cp_NullLink(cp_ToStrODBC(w_INCODESE),'INVEDETT','DICODESE');
      +","+cp_NullLink(cp_ToStrODBC(w_ARCODART),'INVEDETT','DICODART');
      +","+cp_NullLink(cp_ToStrODBC(w_ARULTCOS),'INVEDETT','DICOSULT');
      +","+cp_NullLink(cp_ToStrODBC(w_DICODICE),'INVEDETT','DICODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DINUMINV',w_INNUMINV,'DICODESE',w_INCODESE,'DICODART',w_ARCODART,'DICOSULT',w_ARULTCOS,'DICODICE',w_DICODICE)
      insert into (i_cTable) (DINUMINV,DICODESE,DICODART,DICOSULT,DICODICE &i_ccchkf. );
         values (;
           w_INNUMINV;
           ,w_INCODESE;
           ,w_ARCODART;
           ,w_ARULTCOS;
           ,w_DICODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E73B48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UNIMIS
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UMCODICE"+",UMDESCRI"+",UMFLFRAZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARUNMIS1),'UNIMIS','UMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_ARUNMIS1),'UNIMIS','UMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'UNIMIS','UMFLFRAZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UMCODICE',w_ARUNMIS1,'UMDESCRI',w_ARUNMIS1,'UMFLFRAZ'," ")
      insert into (i_cTable) (UMCODICE,UMDESCRI,UMFLFRAZ &i_ccchkf. );
         values (;
           w_ARUNMIS1;
           ,w_ARUNMIS1;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E74C88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UNIMIS
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UMCODICE"+",UMDESCRI"+",UMFLFRAZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(W_ARUNMIS2),'UNIMIS','UMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_ARUNMIS2),'UNIMIS','UMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'UNIMIS','UMFLFRAZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UMCODICE',W_ARUNMIS2,'UMDESCRI',w_ARUNMIS2,'UMFLFRAZ'," ")
      insert into (i_cTable) (UMCODICE,UMDESCRI,UMFLFRAZ &i_ccchkf. );
         values (;
           W_ARUNMIS2;
           ,w_ARUNMIS2;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E781C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARCODART),'PAR_RIOR','PRCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',w_ARCODART)
      insert into (i_cTable) (PRCODART &i_ccchkf. );
         values (;
           w_ARCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E92758()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAR_IFFE
    i_nConn=i_TableProp[this.TAR_IFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAR_IFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAR_IFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TACODART"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARCODART),'TAR_IFFE','TACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MaxRowNum),'TAR_IFFE','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TACODART',w_ARCODART,'CPROWNUM',this.w_MaxRowNum)
      insert into (i_cTable) (TACODART,CPROWNUM &i_ccchkf. );
         values (;
           w_ARCODART;
           ,this.w_MaxRowNum;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E94558()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAR_DETT
    i_nConn=i_TableProp[this.TAR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAR_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DECODART"+",DEROWNUM"+",CPROWNUM"+",DEVALPRA"+",DEPREMIN"+",DEPREMAX"+",DESCAORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARCODART),'TAR_DETT','DECODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MaxRowNum),'TAR_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(1),'TAR_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(0),'TAR_DETT','DEVALPRA');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMIN),'TAR_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMAX),'TAR_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(7000000000),'TAR_DETT','DESCAORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DECODART',w_ARCODART,'DEROWNUM',this.w_MaxRowNum,'CPROWNUM',1,'DEVALPRA',0,'DEPREMIN',w_DEPREMIN,'DEPREMAX',w_DEPREMAX,'DESCAORD',7000000000)
      insert into (i_cTable) (DECODART,DEROWNUM,CPROWNUM,DEVALPRA,DEPREMIN,DEPREMAX,DESCAORD &i_ccchkf. );
         values (;
           w_ARCODART;
           ,this.w_MaxRowNum;
           ,1;
           ,0;
           ,w_DEPREMIN;
           ,w_DEPREMAX;
           ,7000000000;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E954B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAR_DETT
    i_nConn=i_TableProp[this.TAR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAR_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DECODART"+",DEROWNUM"+",CPROWNUM"+",DEVALPRA"+",DEPREMIN"+",DEPREMAX"+",DESCAORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARCODART),'TAR_DETT','DECODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MaxRowNum),'TAR_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(2),'TAR_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(-1),'TAR_DETT','DEVALPRA');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMIN),'TAR_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMAX),'TAR_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(8000000000),'TAR_DETT','DESCAORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DECODART',w_ARCODART,'DEROWNUM',this.w_MaxRowNum,'CPROWNUM',2,'DEVALPRA',-1,'DEPREMIN',w_DEPREMIN,'DEPREMAX',w_DEPREMAX,'DESCAORD',8000000000)
      insert into (i_cTable) (DECODART,DEROWNUM,CPROWNUM,DEVALPRA,DEPREMIN,DEPREMAX,DESCAORD &i_ccchkf. );
         values (;
           w_ARCODART;
           ,this.w_MaxRowNum;
           ,2;
           ,-1;
           ,w_DEPREMIN;
           ,w_DEPREMAX;
           ,8000000000;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E95F08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAR_DETT
    i_nConn=i_TableProp[this.TAR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAR_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DECODART"+",DEROWNUM"+",CPROWNUM"+",DEVALPRA"+",DEPREMIN"+",DEPREMAX"+",DESCAORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_ARCODART),'TAR_DETT','DECODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MaxRowNum),'TAR_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(3),'TAR_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(-2),'TAR_DETT','DEVALPRA');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMIN),'TAR_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(w_DEPREMAX),'TAR_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(9000000000),'TAR_DETT','DESCAORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DECODART',w_ARCODART,'DEROWNUM',this.w_MaxRowNum,'CPROWNUM',3,'DEVALPRA',-2,'DEPREMIN',w_DEPREMIN,'DEPREMAX',w_DEPREMAX,'DESCAORD',9000000000)
      insert into (i_cTable) (DECODART,DEROWNUM,CPROWNUM,DEVALPRA,DEPREMIN,DEPREMAX,DESCAORD &i_ccchkf. );
         values (;
           w_ARCODART;
           ,this.w_MaxRowNum;
           ,3;
           ,-2;
           ,w_DEPREMIN;
           ,w_DEPREMAX;
           ,9000000000;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F08B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LIS_SCAG
    i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_LICODART),'LIS_SCAG','LICODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'LIS_SCAG','LIROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_LAQUANTI),'LIS_SCAG','LIQUANTI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LICODART',w_LICODART,'LIROWNUM',this.oParentObject.ULT_RNUM,'LIQUANTI',w_LAQUANTI)
      insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI &i_ccchkf. );
         values (;
           w_LICODART;
           ,this.oParentObject.ULT_RNUM;
           ,w_LAQUANTI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F324A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CONTVEAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTVEAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CVTIPCON ="+cp_NullLink(cp_ToStrODBC(w_CVTIPCON),'CONTVEAC','CVTIPCON');
      +",CVCONRIC ="+cp_NullLink(cp_ToStrODBC(w_CVCONRIC),'CONTVEAC','CVCONRIC');
      +",CVCONOMV ="+cp_NullLink(cp_ToStrODBC(w_CVCONOMV),'CONTVEAC','CVCONOMV');
      +",CVCONACQ ="+cp_NullLink(cp_ToStrODBC(w_CVCONACQ),'CONTVEAC','CVCONACQ');
      +",CVCONOMA ="+cp_NullLink(cp_ToStrODBC(w_CVCONOMA),'CONTVEAC','CVCONOMA');
      +",CVIVAOMA ="+cp_NullLink(cp_ToStrODBC(w_CVIVAOMA),'CONTVEAC','CVIVAOMA');
      +",CVIVAOMC ="+cp_NullLink(cp_ToStrODBC(w_CVIVAOMC),'CONTVEAC','CVIVAOMC');
      +",CVCONNCC ="+cp_NullLink(cp_ToStrODBC(w_CVCONNCC),'CONTVEAC','CVCONNCC');
      +",CVCONNCF ="+cp_NullLink(cp_ToStrODBC(w_CVCONNCF),'CONTVEAC','CVCONNCF');
          +i_ccchkf ;
      +" where ";
          +"CVCODART = "+cp_ToStrODBC(w_CVCODART);
          +" and CVCODCLI = "+cp_ToStrODBC(w_CVCODCLI);
             )
    else
      update (i_cTable) set;
          CVTIPCON = w_CVTIPCON;
          ,CVCONRIC = w_CVCONRIC;
          ,CVCONOMV = w_CVCONOMV;
          ,CVCONACQ = w_CVCONACQ;
          ,CVCONOMA = w_CVCONOMA;
          ,CVIVAOMA = w_CVIVAOMA;
          ,CVIVAOMC = w_CVIVAOMC;
          ,CVCONNCC = w_CVCONNCC;
          ,CVCONNCF = w_CVCONNCF;
          &i_ccchkf. ;
       where;
          CVCODART = w_CVCODART;
          and CVCODCLI = w_CVCODCLI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,38)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='CAT_SCMA'
    this.cWorkTables[5]='CON_COSC'
    this.cWorkTables[6]='CON_TRAD'
    this.cWorkTables[7]='CON_TRAM'
    this.cWorkTables[8]='FIFOCONT'
    this.cWorkTables[9]='GRUMERC'
    this.cWorkTables[10]='GRUPRO'
    this.cWorkTables[11]='INVEDETT'
    this.cWorkTables[12]='INVENTAR'
    this.cWorkTables[13]='KEY_ARTI'
    this.cWorkTables[14]='LIFOCONT'
    this.cWorkTables[15]='LIFOSCAT'
    this.cWorkTables[16]='LISTINI'
    this.cWorkTables[17]='LIS_SCAG'
    this.cWorkTables[18]='LIS_TINI'
    this.cWorkTables[19]='MAGAZZIN'
    this.cWorkTables[20]='MOP_DETT'
    this.cWorkTables[21]='MOP_MAST'
    this.cWorkTables[22]='NOMENCLA'
    this.cWorkTables[23]='NOT_ARTI'
    this.cWorkTables[24]='PAR_RIOR'
    this.cWorkTables[25]='TAB_SCON'
    this.cWorkTables[26]='TIPICONF'
    this.cWorkTables[27]='TIPITRAN'
    this.cWorkTables[28]='TIP_DOCU'
    this.cWorkTables[29]='UNIMIS'
    this.cWorkTables[30]='CACOARTI'
    this.cWorkTables[31]='CONTVEAC'
    this.cWorkTables[32]='TIP_COLL'
    this.cWorkTables[33]='TRADCAMA'
    this.cWorkTables[34]='TRADPAGA'
    this.cWorkTables[35]='TRADARTI'
    this.cWorkTables[36]='ART_ALTE'
    this.cWorkTables[37]='TAR_IFFE'
    this.cWorkTables[38]='TAR_DETT'
    return(this.OpenAllTables(38))

  proc CloseCursors()
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    if used('_Curs_TAR_IFFE')
      use in _Curs_TAR_IFFE
    endif
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
