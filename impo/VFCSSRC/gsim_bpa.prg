* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bpa                                                        *
*              Aggiorna cursore partite                                        *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_198]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2012-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bpa",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bpa as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  NC = 0
  w_ODBC = space(30)
  w_CODAZI = space(5)
  w_COCAUSAL = space(5)
  w_COCONSAL = space(15)
  w_PTNUMPAR = space(31)
  w_PTCODCON = space(31)
  w_IDPARC = space(0)
  w_RIFCON = space(10)
  w_PTNUMPAR = space(31)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTSERRIF = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  * --- WorkFile variables
  CONTROPA_idx=0
  PAR_TITE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CURSORI UTILIZZATI:
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    this.w_CODAZI = i_CODAZI
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCONSAL,COCAUSAL"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCONSAL,COCAUSAL;
        from (i_cTable) where;
            COCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COCONSAL = NVL(cp_ToDate(_read_.COCONSAL),cp_NullValue(_read_.COCONSAL))
      this.w_COCAUSAL = NVL(cp_ToDate(_read_.COCAUSAL),cp_NullValue(_read_.COCAUSAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      =Wrcursor( (this.w_CURSOR))
      Select (this.w_CURSOR)
      GO TOP 
 Scan
      this.w_IDPARC = Alltrim(str(Nvl(IDPARC,0)))
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVRIFCON"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVRIFPIA = "+cp_ToStrODBC(this.w_IDPARC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVRIFCON;
          from (i_cTable) where;
              MVRIFPIA = this.w_IDPARC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RIFCON = NVL(cp_ToDate(_read_.MVRIFCON),cp_NullValue(_read_.MVRIFCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_RIFCON)
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
              +" where PTSERIAL="+cp_ToStrODBC(this.w_RIFCON)+" and PTROWORD>0";
               ,"_Curs_PAR_TITE")
        else
          select * from (i_cTable);
           where PTSERIAL=this.w_RIFCON and PTROWORD>0;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
          this.w_PTCODCON = _Curs_PAR_TITE.PTCODCON
          this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
          this.w_PTSERRIF = _Curs_PAR_TITE.PTSERIAL
          this.w_PTROWORD = _Curs_PAR_TITE.PTROWORD
          this.w_CPROWNUM = _Curs_PAR_TITE.CPROWNUM
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
        Select (this.w_CURSOR)
        do case
          case Nvl(PTTIPCON," ")="C"
            if Not Empty(Nvl(this.w_PTNUMPAR," "))
               
 Replace PTNUMPAR with this.w_PTNUMPAR 
 Replace PTDATSCA with this.w_PTDATSCA 
 Replace PTSERRIF with this.w_PTSERRIF 
 Replace PTROWORD with this.w_PTROWORD 
 Replace CPROWNUM with this.w_CPROWNUM
            endif
             
 Replace PTCODCON with this.w_PTCODCON 
 Replace PNCODCAU with this.w_COCAUSAL
          case Nvl(PTTIPCON," ")="G"
            Select (this.w_CURSOR)
             
 Replace PTNUMPAR with this.w_PTNUMPAR 
 Replace PTCODCON with this.w_COCONSAL 
 Replace PNCODCAU with this.w_COCAUSAL
        endcase
      endif
      EndScan
      Select (this.w_CURSOR) 
 Go top
      Delete from (this.w_CURSOR) where Empty(Nvl(PTNUMPAR," "))
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='DOC_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
