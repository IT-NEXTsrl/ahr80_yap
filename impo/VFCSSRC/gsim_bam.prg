* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bam                                                        *
*              Azzeramento movimentazioni                                      *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_3]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-01                                                      *
* Last revis.: 2006-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Azzera,w_Destin,w_Preced,w_Campo1,w_Campo2,w_Campo3
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bam",oParentObject,m.w_Azzera,m.w_Destin,m.w_Preced,m.w_Campo1,m.w_Campo2,m.w_Campo3)
return(i_retval)

define class tgsim_bam as StdBatch
  * --- Local variables
  w_Azzera = space(10)
  w_Destin = space(10)
  w_Preced = space(10)
  w_Campo1 = space(10)
  w_Campo2 = space(10)
  w_Campo3 = space(10)
  w_Errore = .f.
  * --- WorkFile variables
  PAG_2AME_idx=0
  CAUPRI_idx=0
  CAUIVA_idx=0
  CAUPRI1_idx=0
  CAUIVA1_idx=0
  RESOCONT_idx=0
  CON_TRAD_idx=0
  CON_COSC_idx=0
  DISTBASE_idx=0
  TAB_CICL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzeramento records per movimentazioni e master-detail
    * --- La presente procedura elimina i record delle movimentazioni prima di inserire i nuovi letti dal file ascii.
    * --- Parametri
    * --- Azzera records dettaglio S/N
    * --- Sigla archivio di destinazione
    * --- Chiave dei records elaborati la volta precedente
    * --- Primo campo della chiave
    * --- Secondo campo della chiave
    * --- Terzo campo della chiave
    * --- Variabili del batch chiamante
    * --- Codice importazione
    * --- Resoconto abilitato S/N
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Variabili locali
    * --- Indica se la cancellazione ha generato un errore
    * --- L'azzeramento dei records avviene solo se e' stata attivata la corrispondente opzione
    if this.w_Azzera = "S"
      * --- L'eliminazione avviene una sola volta a parit� di archivio e chiave
      if this.w_Destin <> left(this.w_Preced, 2) .or. this.w_Destin+this.w_Campo1+this.w_Campo2+this.w_Campo3 <> this.w_Preced
        this.w_Errore = .F.
        * --- Cancellazione records
        * --- Try
        local bErr_036B8E70
        bErr_036B8E70=bTrsErr
        this.Try_036B8E70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_Errore = .T.
        endif
        bTrsErr=bTrsErr or bErr_036B8E70
        * --- End
        * --- Aggiornamento resoconto in caso di errore
        if this.w_Errore .and. this.oParentObject.w_SeleReso $ "SE"
          * --- Try
          local bErr_03731AE8
          bErr_03731AE8=bTrsErr
          this.Try_03731AE8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03731AE8
          * --- End
        endif
      endif
    endif
  endproc
  proc Try_036B8E70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_Destin = "PD"
        * --- Dettaglio Pagamenti
        * --- Delete from PAG_2AME
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"P2CODICE = "+cp_ToStrODBC(this.w_Campo1);
                 )
        else
          delete from (i_cTable) where;
                P2CODICE = this.w_Campo1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_Destin = "AC"
        * --- Automatismi contabili o modelli contabili
        if this.w_Campo2 $ "CF"
          * --- Delete from CAUPRI
          i_nConn=i_TableProp[this.CAUPRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"APCODCAU = "+cp_ToStrODBC(this.w_Campo1);
                  +" and APTIPCLF = "+cp_ToStrODBC(this.w_Campo2);
                  +" and APCODCLF = "+cp_ToStrODBC(this.w_Campo3);
                   )
          else
            delete from (i_cTable) where;
                  APCODCAU = this.w_Campo1;
                  and APTIPCLF = this.w_Campo2;
                  and APCODCLF = this.w_Campo3;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from CAUIVA
          i_nConn=i_TableProp[this.CAUIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.w_Campo1);
                  +" and AITIPCLF = "+cp_ToStrODBC(this.w_Campo2);
                  +" and AICODCLF = "+cp_ToStrODBC(this.w_Campo3);
                   )
          else
            delete from (i_cTable) where;
                  AICODCAU = this.w_Campo1;
                  and AITIPCLF = this.w_Campo2;
                  and AICODCLF = this.w_Campo3;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          * --- Delete from CAUPRI1
          i_nConn=i_TableProp[this.CAUPRI1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"APCODCAU = "+cp_ToStrODBC(this.w_Campo1);
                   )
          else
            delete from (i_cTable) where;
                  APCODCAU = this.w_Campo1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from CAUIVA1
          i_nConn=i_TableProp[this.CAUIVA1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.w_Campo1);
                   )
          else
            delete from (i_cTable) where;
                  AICODCAU = this.w_Campo1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      case this.w_Destin = "CN"
        * --- Contratti
        * --- Delete from CON_COSC
        i_nConn=i_TableProp[this.CON_COSC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COCODICE = "+cp_ToStrODBC(this.w_Campo1);
                 )
        else
          delete from (i_cTable) where;
                COCODICE = this.w_Campo1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from CON_TRAD
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CONUMERO = "+cp_ToStrODBC(this.w_Campo1);
                 )
        else
          delete from (i_cTable) where;
                CONUMERO = this.w_Campo1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_Destin = "DB"
        * --- Delete from DISTBASE
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_Campo1);
                 )
        else
          delete from (i_cTable) where;
                DBCODICE = this.w_Campo1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_Destin = "SE"
        * --- Cicli semplificati
        * --- Delete from TAB_CICL
        i_nConn=i_TableProp[this.TAB_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CSCODICE = "+cp_ToStrODBC(this.w_Campo1);
                 )
        else
          delete from (i_cTable) where;
                CSCODICE = this.w_Campo1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
    return
  proc Try_03731AE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.oParentObject.w_ResoRiga = this.oParentObject.w_ResoRiga + 1
    this.oParentObject.w_ResoNErr = this.oParentObject.w_ResoNErr + 1
    this.oParentObject.w_ResoTipo = "E"
    this.oParentObject.w_ResoMess = "Verificare il file"
    this.oParentObject.w_ResoDett = "Non � stato possibile azzerare la precedente registrazione prima di inserire i nuovi dati"
    * --- Insert into RESOCONT
    i_nConn=i_TableProp[this.RESOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RESOCONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",REDESTIP"+",REDESDET"+",REDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODIMP),'RESOCONT','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ResoRiga),'RESOCONT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ResoTipo),'RESOCONT','REDESTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ResoDett),'RESOCONT','REDESDET');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ResoMess),'RESOCONT','REDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_CODIMP,'CPROWNUM',this.oParentObject.w_ResoRiga,'REDESTIP',this.oParentObject.w_ResoTipo,'REDESDET',this.oParentObject.w_ResoDett,'REDESCRI',this.oParentObject.w_ResoMess)
      insert into (i_cTable) (RECODICE,CPROWNUM,REDESTIP,REDESDET,REDESCRI &i_ccchkf. );
         values (;
           this.oParentObject.w_CODIMP;
           ,this.oParentObject.w_ResoRiga;
           ,this.oParentObject.w_ResoTipo;
           ,this.oParentObject.w_ResoDett;
           ,this.oParentObject.w_ResoMess;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_Azzera,w_Destin,w_Preced,w_Campo1,w_Campo2,w_Campo3)
    this.w_Azzera=w_Azzera
    this.w_Destin=w_Destin
    this.w_Preced=w_Preced
    this.w_Campo1=w_Campo1
    this.w_Campo2=w_Campo2
    this.w_Campo3=w_Campo3
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='PAG_2AME'
    this.cWorkTables[2]='CAUPRI'
    this.cWorkTables[3]='CAUIVA'
    this.cWorkTables[4]='CAUPRI1'
    this.cWorkTables[5]='CAUIVA1'
    this.cWorkTables[6]='RESOCONT'
    this.cWorkTables[7]='CON_TRAD'
    this.cWorkTables[8]='CON_COSC'
    this.cWorkTables[9]='DISTBASE'
    this.cWorkTables[10]='TAB_CICL'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Azzera,w_Destin,w_Preced,w_Campo1,w_Campo2,w_Campo3"
endproc
