* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_btr                                                        *
*              Visualizza trascodifiche                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_9]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-27                                                      *
* Last revis.: 2012-02-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_btr",oParentObject)
return(i_retval)

define class tgsim_btr as StdBatch
  * --- Local variables
  oGSIM_MTD = .NULL.
  * --- WorkFile variables
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RICHIAMA MOVIMENTAZIONE TRASCODIFICHE
    * --- Lanciato da gsim_mtr
    this.oGSIM_MTD = GSIM_MTD()
    * --- Controllo se ha passato il test di accesso 
    if !(this.oGSIM_MTD.bSec1)
      i_retcode = 'stop'
      return
    endif
    if not ISNULL(this.oGSIM_MTD )
      * --- Read from TRASCODI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TRASCODI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2],.t.,this.TRASCODI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" TRASCODI where ";
              +"TRTIPTRA = "+cp_ToStrODBC("S");
              +" and TRCODIMP = "+cp_ToStrODBC(this.oParentObject.w_TRCODICE);
              +" and TRCODFIL = "+cp_ToStrODBC(this.oParentObject.w_TRDESTIN);
              +" and TRNOMCAM = "+cp_ToStrODBC(this.oParentObject.w_TRCAMDST);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              TRTIPTRA = "S";
              and TRCODIMP = this.oParentObject.w_TRCODICE;
              and TRCODFIL = this.oParentObject.w_TRDESTIN;
              and TRNOMCAM = this.oParentObject.w_TRCAMDST;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows > 0
        * --- Interrogazione
        * --- Carica key set
        SETVALUELINKED( "M", this.oGSIM_MTD, "w_TRCODFIL", this.oParentObject.w_TRDESTIN )
        SETVALUELINKED( "M", this.oGSIM_MTD, "w_TRNOMCAM", this.oParentObject.w_TRCAMDST )
        this.oGSIM_MTD.w_TRCODIMP = this.oParentObject.w_TRCODICE
        this.oGSIM_MTD.w_TRTIPTRA = "S"
        this.oGSIM_MTD.QueryKeySet(this.oGSIM_MTD.BuildFilter(),"")     
        this.oGSIM_MTD.LoadRec()     
      else
        * --- Caricamento
        this.oGSIM_MTD.ecpLoad()     
        SETVALUELINKED( "M", this.oGSIM_MTD, "w_TRCODFIL", this.oParentObject.w_TRDESTIN )
        SETVALUELINKED( "M", this.oGSIM_MTD, "w_TRNOMCAM", this.oParentObject.w_TRCAMDST )
        this.oGSIM_MTD.w_TRCODIMP = this.oParentObject.w_TRCODICE
        this.oGSIM_MTD.w_TRTIPTRA = "S"
        this.oGSIM_MTD.ecpLoad()     
        this.oGSIM_MTD.SetControlsValue()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TRASCODI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
