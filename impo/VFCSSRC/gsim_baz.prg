* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_baz                                                        *
*              Azzeramento file ASCII                                          *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_7]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-01                                                      *
* Last revis.: 1999-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_baz",oParentObject)
return(i_retval)

define class tgsim_baz as StdBatch
  * --- Local variables
  w_GESTITI = space(10)
  w_NomeFile = space(10)
  w_Destinaz = space(2)
  w_nHandle = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzeramento contenuto file ascii
    * --- Variabile per il riconoscimento degli archivi gestiti
    this.w_GESTITI = this.oParentObject.oParentObject.w_Gestiti
    * --- Path del file
    * --- Variabili locali
    * --- Nome del file ascii
    * --- Codice archivio destinazione
    * --- Puntatore al file
    * --- Richiesta azzeramento archivi
    if ah_YesNo("Si desidera azzerare il contenuto dei file ASCII importati?")
      select CursFiles
      go top
      do while .not. eof()
        * --- Lettura dati file corrente
        this.w_NomeFile = ALLTRIM(upper(this.oParentObject.w_PATH1))+upper( CursFiles.IM_ASCII )
        this.w_NomeFile = strtran( this.w_NomeFile , "XXXXX", i_codazi )
        this.w_Destinaz = CursFiles.IMDESTIN
        * --- Non elabora i tracciati delle altre importazioni (per ora esclude solo il documenti di vendita)
        if this.w_Destinaz $ this.w_Gestiti
          * --- Verifica esistenza file da importare
          if file( this.w_NomeFile )
            this.w_nHandle = fcreate( this.w_NomeFile )
            if this.w_nHandle < 0
              ah_ErrorMsg("Non � possibile eliminare il file ASCII %1","!","",ALLTRIM(this.w_NomeFile))
            endif
            fclose( this.w_nHandle )
          endif
        endif
        * --- Ricerca del tracciato successivo
        select CursFiles
        skip
      enddo
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
