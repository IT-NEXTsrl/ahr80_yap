* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bel                                                        *
*              Cancellazione tracciati di importazione                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_16]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-01                                                      *
* Last revis.: 2014-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bel",oParentObject)
return(i_retval)

define class tgsim_bel as StdBatch
  * --- Local variables
  * --- WorkFile variables
  IMPOARCH_idx=0
  IMPORTAZ_idx=0
  PREDEFIN_idx=0
  PREDEIMP_idx=0
  SRCMODBC_idx=0
  SRC_ODBC_idx=0
  TRACCIAT_idx=0
  TRASCODI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CANCELLAZIONE TRACCIATI DI IMPORTAZIONE
    * --- Try
    local bErr_02958468
    bErr_02958468=bTrsErr
    this.Try_02958468()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AH_ErrorMsg("Si � verificato l'errore:%0%1%0La transazione � stata annullata","stop","",i_TrsMsg)
    endif
    bTrsErr=bTrsErr or bErr_02958468
    * --- End
  endproc
  proc Try_02958468()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Tracciati Importazione
    AH_msg("Cancellazione tracciati di importazione...",.T.)
    * --- Delete from TRACCIAT
    i_nConn=i_TableProp[this.TRACCIAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRACCIAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from IMPORTAZ
    i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Sorgenti Dati
    AH_msg("Cancellazione sorgente dati ODBC...",.T.)
    * --- Delete from SRC_ODBC
    i_nConn=i_TableProp[this.SRC_ODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRC_ODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from SRCMODBC
    i_nConn=i_TableProp[this.SRCMODBC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SRCMODBC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Valori Predefiniti
    AH_msg("Cancellazione valori predefiniti...",.T.)
    * --- Delete from PREDEIMP
    i_nConn=i_TableProp[this.PREDEIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEIMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PREDEFIN
    i_nConn=i_TableProp[this.PREDEFIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREDEFIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Trascodifiche
    AH_msg("Cancellazione trascodifiche...",.T.)
    * --- Delete from TRASCODI
    i_nConn=i_TableProp[this.TRASCODI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRASCODI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Archivi di Destinazione
    AH_msg("Cancellazione archivi di destinazione...",.T.)
    * --- Delete from IMPOARCH
    i_nConn=i_TableProp[this.IMPOARCH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IMPOARCH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    wait clear
    AH_ErrorMsg("Procedura terminata con successo","i","")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='PREDEFIN'
    this.cWorkTables[4]='PREDEIMP'
    this.cWorkTables[5]='SRCMODBC'
    this.cWorkTables[6]='SRC_ODBC'
    this.cWorkTables[7]='TRACCIAT'
    this.cWorkTables[8]='TRASCODI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
