* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bpe                                                        *
*              Imposta ordinamento prestazioni                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-08                                                      *
* Last revis.: 2013-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bpe",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_bpe as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  w_ODBC = space(30)
  w_lodbconn = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CURSORI UTILIZZATI:
    this.w_ODBC = ALLTRIM(this.oParentObject.w_ODBCIMPO)
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      if Upper(this.oParentObject.Class)="TGSIM_BSO"
        this.w_lodbconn = this.oparentobject.oparentobject.w_odbcconn
      else
        this.w_lodbconn = this.oParentObject.w_odbcconn
      endif
      sqlprepare(this.w_lodbconn,"Select  PTALFPRA AS ALFPRA,PTNUMPRA AS NUMPRA,PTDATPRE AS DATPRE,PTCODVAL AS CODVAL,recno() as ordine from "+SUBSTR(i_codazi,3,3)+"\pre_staz","PRESTAZ") 
 SQLEXEC(this.w_lodbconn)
      Select * from PRESTAZ Right Outer JOIN (this.w_CURSOR) ON PRALFPRA+PRNUMPRA=ALFPRA+NUMPRA AND PTDATPRE=DATPRE AND PTCODVAL=CODVAL ; 
 into cursor (this.w_CURSOR) ORDER BY 4,5,6,7,3
      use in PRESTAZ
    endif
    * --- ORDER BY 1,2,3,4,5
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
