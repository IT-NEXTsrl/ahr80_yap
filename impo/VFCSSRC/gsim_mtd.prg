* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mtd                                                        *
*              Trascodifiche                                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-28                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_mtd"))

* --- Class definition
define class tgsim_mtd as StdTrsForm
  Top    = 13
  Left   = 32

  * --- Standard Properties
  Width  = 642
  Height = 419+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=80330601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TRASCODI_IDX = 0
  IMPOARCH_IDX = 0
  XDC_FIELDS_IDX = 0
  IMPORTAZ_IDX = 0
  cFile = "TRASCODI"
  cKeySelect = "TRCODFIL,TRTIPTRA,TRNOMCAM,TRCODIMP,TRCODIMP"
  cKeyWhere  = "TRCODFIL=this.w_TRCODFIL and TRTIPTRA=this.w_TRTIPTRA and TRNOMCAM=this.w_TRNOMCAM and TRCODIMP=this.w_TRCODIMP and TRCODIMP=this.w_TRCODIMP"
  cKeyDetail  = "TRCODFIL=this.w_TRCODFIL and TRTIPTRA=this.w_TRTIPTRA and TRNOMCAM=this.w_TRNOMCAM and TRCODIMP=this.w_TRCODIMP and TRCODEXT=this.w_TRCODEXT and TRCODIMP=this.w_TRCODIMP"
  cKeyWhereODBC = '"TRCODFIL="+cp_ToStrODBC(this.w_TRCODFIL)';
      +'+" and TRTIPTRA="+cp_ToStrODBC(this.w_TRTIPTRA)';
      +'+" and TRNOMCAM="+cp_ToStrODBC(this.w_TRNOMCAM)';
      +'+" and TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';
      +'+" and TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';

  cKeyDetailWhereODBC = '"TRCODFIL="+cp_ToStrODBC(this.w_TRCODFIL)';
      +'+" and TRTIPTRA="+cp_ToStrODBC(this.w_TRTIPTRA)';
      +'+" and TRNOMCAM="+cp_ToStrODBC(this.w_TRNOMCAM)';
      +'+" and TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';
      +'+" and TRCODEXT="+cp_ToStrODBC(this.w_TRCODEXT)';
      +'+" and TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';

  cKeyWhereODBCqualified = '"TRASCODI.TRCODFIL="+cp_ToStrODBC(this.w_TRCODFIL)';
      +'+" and TRASCODI.TRTIPTRA="+cp_ToStrODBC(this.w_TRTIPTRA)';
      +'+" and TRASCODI.TRNOMCAM="+cp_ToStrODBC(this.w_TRNOMCAM)';
      +'+" and TRASCODI.TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';
      +'+" and TRASCODI.TRCODIMP="+cp_ToStrODBC(this.w_TRCODIMP)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsim_mtd"
  cComment = "Trascodifiche"
  i_nRowNum = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ARTABELL = space(15)
  w_ARTABDET = space(15)
  w_FLCOMMEN = space(80)
  w_TBNAME = space(15)
  w_ARDESCRI = space(20)
  w_TRCODFIL = space(2)
  w_TRTIPTRA = space(1)
  o_TRTIPTRA = space(1)
  w_TRNOMCAM = space(30)
  w_TRCODIMP = space(20)
  w_TRCODEXT = space(50)
  w_TRCODENT = space(0)
  w_SOSQLSRC = space(0)
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_ODBCCURSOR = space(8)
  w_ODBCSQL = 0
  w_ODBCCONN = 0
  w_ParametroAzione = space(10)
  w_TRTRAPAR = space(1)
  w_TRCODIMP = space(20)
  w_TBNAME2 = space(15)
  w_NOMCAM2 = space(30)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TRASCODI','gsim_mtd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_mtdPag1","gsim_mtd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifica")
      .Pages(1).HelpContextID = 21456895
      .Pages(2).addobject("oPag","tgsim_mtdPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Interrogazione SQL")
      .Pages(2).HelpContextID = 249997984
      .Pages(3).addobject("oPag","tgsim_mtdPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Cursore")
      .Pages(3).HelpContextID = 190784986
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODFIL_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='IMPORTAZ'
    this.cWorkTables[4]='TRASCODI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TRASCODI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TRASCODI_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TRCODFIL = NVL(TRCODFIL,space(2))
      .w_TRTIPTRA = NVL(TRTIPTRA,space(1))
      .w_TRNOMCAM = NVL(TRNOMCAM,space(30))
      .w_TRCODIMP = NVL(TRCODIMP,space(20))
      .w_TRCODIMP = NVL(TRCODIMP,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsim_mtd
    if this.w_ODBCCONN > 0
       do gsim_bso with this,"Chiudi"
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TRASCODI where TRCODFIL=KeySet.TRCODFIL
    *                            and TRTIPTRA=KeySet.TRTIPTRA
    *                            and TRNOMCAM=KeySet.TRNOMCAM
    *                            and TRCODIMP=KeySet.TRCODIMP
    *                            and TRCODEXT=KeySet.TRCODEXT
    *                            and TRCODIMP=KeySet.TRCODIMP
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TRASCODI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2],this.bLoadRecFilter,this.TRASCODI_IDX,"gsim_mtd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TRASCODI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TRASCODI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TRASCODI '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODFIL',this.w_TRCODFIL  ,'TRTIPTRA',this.w_TRTIPTRA  ,'TRNOMCAM',this.w_TRNOMCAM  ,'TRCODIMP',this.w_TRCODIMP  ,'TRCODIMP',this.w_TRCODIMP  )
      select * from (i_cTable) TRASCODI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ARTABELL = space(15)
        .w_ARTABDET = space(15)
        .w_FLCOMMEN = space(80)
        .w_TBNAME = space(15)
        .w_ARDESCRI = space(20)
        .w_ODBCDSN = space(30)
        .w_ODBCPATH = SPACE(200)
        .w_ODBCUSER = space(30)
        .w_ODBCPASSW = space(30)
        .w_ODBCCURSOR = sys(2015)
        .w_ODBCSQL = -1
        .w_ODBCCONN = -1
        .w_ParametroAzione = space(10)
        .w_TBNAME2 = space(15)
        .w_TRCODFIL = NVL(TRCODFIL,space(2))
          if link_1_6_joined
            this.w_TRCODFIL = NVL(ARCODICE106,NVL(this.w_TRCODFIL,space(2)))
            this.w_ARDESCRI = NVL(ARDESCRI106,space(20))
            this.w_ARTABELL = NVL(ARTABELL106,space(15))
            this.w_ARTABDET = NVL(ARTABDET106,space(15))
          else
          .link_1_6('Load')
          endif
        .w_TRTIPTRA = NVL(TRTIPTRA,space(1))
        .w_TRNOMCAM = NVL(TRNOMCAM,space(30))
          .link_1_8('Load')
        .w_TRCODIMP = NVL(TRCODIMP,space(20))
          * evitabile
          *.link_1_9('Load')
        .w_SOSQLSRC = NVL(SOSQLSRC,space(0))
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_TRTRAPAR = NVL(TRTRAPAR,space(1))
        .w_TRCODIMP = NVL(TRCODIMP,space(20))
        .w_NOMCAM2 = .w_TRNOMCAM
          .link_1_17('Load')
        cp_LoadRecExtFlds(this,'TRASCODI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_TRCODEXT = NVL(TRCODEXT,space(50))
          .w_TRCODENT = NVL(TRCODENT,space(0))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace TRCODEXT with .w_TRCODEXT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_NOMCAM2 = .w_TRNOMCAM
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page3.oPag.oBtn_5_2.enabled = .oPgFrm.Page3.oPag.oBtn_5_2.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_6.enabled = .oPgFrm.Page3.oPag.oBtn_5_6.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_15.enabled = .oPgFrm.Page3.oPag.oBtn_5_15.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ARTABELL=space(15)
      .w_ARTABDET=space(15)
      .w_FLCOMMEN=space(80)
      .w_TBNAME=space(15)
      .w_ARDESCRI=space(20)
      .w_TRCODFIL=space(2)
      .w_TRTIPTRA=space(1)
      .w_TRNOMCAM=space(30)
      .w_TRCODIMP=space(20)
      .w_TRCODEXT=space(50)
      .w_TRCODENT=space(0)
      .w_SOSQLSRC=space(0)
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_ODBCCURSOR=space(8)
      .w_ODBCSQL=0
      .w_ODBCCONN=0
      .w_ParametroAzione=space(10)
      .w_TRTRAPAR=space(1)
      .w_TRCODIMP=space(20)
      .w_TBNAME2=space(15)
      .w_NOMCAM2=space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        if not(empty(.w_TRCODFIL))
         .link_1_6('Full')
        endif
        .w_TRTIPTRA = 'S'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_TRNOMCAM))
         .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_TRCODIMP))
         .link_1_9('Full')
        endif
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .DoRTCalc(10,13,.f.)
        .w_ODBCPATH = SPACE(200)
        .DoRTCalc(15,16,.f.)
        .w_ODBCCURSOR = sys(2015)
        .w_ODBCSQL = -1
        .w_ODBCCONN = -1
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(20,21,.f.)
        .w_TRCODIMP = iif(.w_TRTIPTRA='G',"Tutti","")
        .DoRTCalc(23,23,.f.)
        .w_NOMCAM2 = .w_TRNOMCAM
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_NOMCAM2))
         .link_1_17('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'TRASCODI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page3.oPag.oBtn_5_2.enabled = this.oPgFrm.Page3.oPag.oBtn_5_2.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_6.enabled = this.oPgFrm.Page3.oPag.oBtn_5_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_15.enabled = this.oPgFrm.Page3.oPag.oBtn_5_15.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODFIL_1_6.enabled = i_bVal
      .Page1.oPag.oTRTIPTRA_1_7.enabled_(i_bVal)
      .Page1.oPag.oTRNOMCAM_1_8.enabled = i_bVal
      .Page1.oPag.oTRCODIMP_1_9.enabled = i_bVal
      .Page2.oPag.oSOSQLSRC_4_1.enabled = i_bVal
      .Page3.oPag.oODBCDSN_5_3.enabled = i_bVal
      .Page3.oPag.oODBCPATH_5_5.enabled = i_bVal
      .Page3.oPag.oODBCUSER_5_8.enabled = i_bVal
      .Page3.oPag.oODBCPASSW_5_10.enabled = i_bVal
      .Page1.oPag.oTRTRAPAR_1_14.enabled = i_bVal
      .Page3.oPag.oBtn_5_2.enabled = .Page3.oPag.oBtn_5_2.mCond()
      .Page3.oPag.oBtn_5_6.enabled = .Page3.oPag.oBtn_5_6.mCond()
      .Page3.oPag.oBtn_5_15.enabled = .Page3.oPag.oBtn_5_15.mCond()
      .Page3.oPag.oBtn_5_17.enabled = i_bVal
      .Page3.oPag.oObj_5_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODFIL_1_6.enabled = .f.
        .Page1.oPag.oTRTIPTRA_1_7.enabled_(.f.)
        .Page1.oPag.oTRNOMCAM_1_8.enabled = .f.
        .Page1.oPag.oTRCODIMP_1_9.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODFIL_1_6.enabled = .t.
        .Page1.oPag.oTRTIPTRA_1_7.enabled_(.t.)
        .Page1.oPag.oTRNOMCAM_1_8.enabled = .t.
        .Page1.oPag.oTRCODIMP_1_9.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TRASCODI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TRASCODI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODFIL,"TRCODFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPTRA,"TRTIPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRNOMCAM,"TRNOMCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODIMP,"TRCODIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SOSQLSRC,"SOSQLSRC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTRAPAR,"TRTRAPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODIMP,"TRCODIMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TRASCODI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
    i_lTable = "TRASCODI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TRASCODI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TRCODEXT C(50);
      ,t_TRCODENT M(10);
      ,TRCODEXT C(50);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_mtdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_1.controlsource=this.cTrsName+'.t_TRCODEXT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODENT_2_2.controlsource=this.cTrsName+'.t_TRCODENT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(314)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TRASCODI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TRASCODI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
      *
      * insert into TRASCODI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TRASCODI')
        i_extval=cp_InsertValODBCExtFlds(this,'TRASCODI')
        i_cFldBody=" "+;
                  "(TRCODFIL,TRTIPTRA,TRNOMCAM,TRCODIMP,TRCODEXT"+;
                  ",TRCODENT,SOSQLSRC,TRTRAPAR,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_TRCODFIL)+","+cp_ToStrODBC(this.w_TRTIPTRA)+","+cp_ToStrODBCNull(this.w_TRNOMCAM)+","+cp_ToStrODBCNull(this.w_TRCODIMP)+","+cp_ToStrODBC(this.w_TRCODEXT)+;
             ","+cp_ToStrODBC(this.w_TRCODENT)+","+cp_ToStrODBC(this.w_SOSQLSRC)+","+cp_ToStrODBC(this.w_TRTRAPAR)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TRASCODI')
        i_extval=cp_InsertValVFPExtFlds(this,'TRASCODI')
        cp_CheckDeletedKey(i_cTable,0,'TRCODFIL',this.w_TRCODFIL,'TRTIPTRA',this.w_TRTIPTRA,'TRNOMCAM',this.w_TRNOMCAM,'TRCODIMP',this.w_TRCODIMP,'TRCODEXT',this.w_TRCODEXT,'TRCODIMP',this.w_TRCODIMP)
        INSERT INTO (i_cTable) (;
                   TRCODFIL;
                  ,TRTIPTRA;
                  ,TRNOMCAM;
                  ,TRCODIMP;
                  ,TRCODEXT;
                  ,TRCODENT;
                  ,SOSQLSRC;
                  ,TRTRAPAR;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TRCODFIL;
                  ,this.w_TRTIPTRA;
                  ,this.w_TRNOMCAM;
                  ,this.w_TRCODIMP;
                  ,this.w_TRCODEXT;
                  ,this.w_TRCODENT;
                  ,this.w_SOSQLSRC;
                  ,this.w_TRTRAPAR;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TRASCODI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_TRCODEXT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TRASCODI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " SOSQLSRC="+cp_ToStrODBC(this.w_SOSQLSRC)+;
                 ",TRTRAPAR="+cp_ToStrODBC(this.w_TRTRAPAR)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and TRCODEXT="+cp_ToStrODBC(&i_TN.->TRCODEXT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TRASCODI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  SOSQLSRC=this.w_SOSQLSRC;
                 ,TRTRAPAR=this.w_TRTRAPAR;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and TRCODEXT=&i_TN.->TRCODEXT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_TRCODEXT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and TRCODEXT="+cp_ToStrODBC(&i_TN.->TRCODEXT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and TRCODEXT=&i_TN.->TRCODEXT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace TRCODEXT with this.w_TRCODEXT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TRASCODI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TRASCODI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TRCODENT="+cp_ToStrODBC(this.w_TRCODENT)+;
                     ",SOSQLSRC="+cp_ToStrODBC(this.w_SOSQLSRC)+;
                     ",TRTRAPAR="+cp_ToStrODBC(this.w_TRTRAPAR)+;
                     ",TRCODEXT="+cp_ToStrODBC(this.w_TRCODEXT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and TRCODEXT="+cp_ToStrODBC(TRCODEXT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TRASCODI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TRCODENT=this.w_TRCODENT;
                     ,SOSQLSRC=this.w_SOSQLSRC;
                     ,TRTRAPAR=this.w_TRTRAPAR;
                     ,TRCODEXT=this.w_TRCODEXT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and TRCODEXT=&i_TN.->TRCODEXT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TRASCODI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TRCODEXT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TRASCODI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and TRCODEXT="+cp_ToStrODBC(&i_TN.->TRCODEXT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and TRCODEXT=&i_TN.->TRCODEXT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TRCODEXT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TRASCODI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRASCODI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,21,.t.)
        if .o_TRTIPTRA<>.w_TRTIPTRA
          .w_TRCODIMP = iif(.w_TRTIPTRA='G',"Tutti","")
        endif
        .DoRTCalc(23,23,.t.)
          .w_NOMCAM2 = .w_TRNOMCAM
          .link_1_17('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.oObj_5_1.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_FLSFHJRMTP()
    with this
          * --- Inserisce gli elementi mancanti
          .w_ParametroAzione = IIF( AH_yesno( "Procedere con il salvataggio delle attuali trascodifiche e poi al caricamento degli eventuali codici mancanti"),'Salva', 'ZZZ' )
          gsim_bso(this;
              ,.w_ParametroAzione;
             )
          .w_ParametroAzione = IIF(.w_ParametroAzione='ZZZ',.w_ParametroAzione,'Carica' )
          gsim_bso(this;
              ,.w_ParametroAzione;
             )
          .w_ParametroAzione = IIF(.w_ParametroAzione='ZZZ',.w_ParametroAzione,'Chiudi' )
          gsim_bso(this;
              ,.w_ParametroAzione;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRCODIMP_1_9.enabled = this.oPgFrm.Page1.oPag.oTRCODIMP_1_9.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_2.enabled = this.oPgFrm.Page3.oPag.oBtn_5_2.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_15.enabled = this.oPgFrm.Page3.oPag.oBtn_5_15.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_17.enabled = this.oPgFrm.Page3.oPag.oBtn_5_17.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(!IsAlt())
    this.oPgFrm.Pages(3).enabled=not(!IsAlt())
    local i_show1
    i_show1=not(!IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1 and not(!IsAlt())
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Interrogazione SQL"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(!IsAlt())
    this.oPgFrm.Pages(3).enabled=i_show2 and not(!IsAlt())
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Cursore"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.oObj_5_1.Event(cEvent)
        if lower(cEvent)==lower("Inserimento")
          .Calculate_FLSFHJRMTP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODFIL
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_TRCODFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL,ARTABDET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_TRCODFIL))
          select ARCODICE,ARDESCRI,ARTABELL,ARTABDET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODFIL)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODFIL) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oTRCODFIL_1_6'),i_cWhere,'GSIM_AAR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL,ARTABDET";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI,ARTABELL,ARTABDET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL,ARTABDET";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_TRCODFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_TRCODFIL)
            select ARCODICE,ARDESCRI,ARTABELL,ARTABDET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODFIL = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESCRI = NVL(_Link_.ARDESCRI,space(20))
      this.w_ARTABELL = NVL(_Link_.ARTABELL,space(15))
      this.w_ARTABDET = NVL(_Link_.ARTABDET,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODFIL = space(2)
      endif
      this.w_ARDESCRI = space(20)
      this.w_ARTABELL = space(15)
      this.w_ARTABDET = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMPOARCH_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ARCODICE as ARCODICE106"+ ",link_1_6.ARDESCRI as ARDESCRI106"+ ",link_1_6.ARTABELL as ARTABELL106"+ ",link_1_6.ARTABDET as ARTABDET106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on TRASCODI.TRCODFIL=link_1_6.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and TRASCODI.TRCODFIL=link_1_6.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRNOMCAM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_TRNOMCAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN,TBNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNAME',trim(this.w_TRNOMCAM))
          select FLNAME,FLCOMMEN,TBNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRNOMCAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','FLNAME',cp_AbsName(oSource.parent,'oTRNOMCAM_1_8'),i_cWhere,'',"Campo dizionario dati",'GSIM_BCC.XDC_FIELDS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN,TBNAME";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',oSource.xKey(1))
            select FLNAME,FLCOMMEN,TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNAME,FLCOMMEN,TBNAME";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_TRNOMCAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNAME',this.w_TRNOMCAM)
            select FLNAME,FLCOMMEN,TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRNOMCAM = NVL(_Link_.FLNAME,space(30))
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(80))
      this.w_TBNAME = NVL(_Link_.TBNAME,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_TRNOMCAM = space(30)
      endif
      this.w_FLCOMMEN = space(80)
      this.w_TBNAME = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY( .w_TBNAME ) OR NOT EMPTY( .w_TBNAME2 )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il campo selezionato � inesistente o non appartiene alle tabelle di destinazione")
        endif
        this.w_TRNOMCAM = space(30)
        this.w_FLCOMMEN = space(80)
        this.w_TBNAME = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODIMP
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_TRCODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_TRCODIMP))
          select IMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oTRCODIMP_1_9'),i_cWhere,'gsim_mim',"Tracciati importazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_TRCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_TRCODIMP)
            select IMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODIMP = NVL(_Link_.IMCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODIMP = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMCAM2
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMCAM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMCAM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_NOMCAM2);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABDET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABDET;
                       ,'FLNAME',this.w_NOMCAM2)
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMCAM2 = NVL(_Link_.FLNAME,space(30))
      this.w_TBNAME2 = NVL(_Link_.TBNAME,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOMCAM2 = space(30)
      endif
      this.w_TBNAME2 = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMCAM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oARTABELL_1_1.value==this.w_ARTABELL)
      this.oPgFrm.Page1.oPag.oARTABELL_1_1.value=this.w_ARTABELL
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABDET_1_2.value==this.w_ARTABDET)
      this.oPgFrm.Page1.oPag.oARTABDET_1_2.value=this.w_ARTABDET
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMMEN_1_3.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oFLCOMMEN_1_3.value=this.w_FLCOMMEN
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESCRI_1_5.value==this.w_ARDESCRI)
      this.oPgFrm.Page1.oPag.oARDESCRI_1_5.value=this.w_ARDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODFIL_1_6.value==this.w_TRCODFIL)
      this.oPgFrm.Page1.oPag.oTRCODFIL_1_6.value=this.w_TRCODFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTIPTRA_1_7.RadioValue()==this.w_TRTIPTRA)
      this.oPgFrm.Page1.oPag.oTRTIPTRA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRNOMCAM_1_8.value==this.w_TRNOMCAM)
      this.oPgFrm.Page1.oPag.oTRNOMCAM_1_8.value=this.w_TRNOMCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODIMP_1_9.value==this.w_TRCODIMP)
      this.oPgFrm.Page1.oPag.oTRCODIMP_1_9.value=this.w_TRCODIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oSOSQLSRC_4_1.value==this.w_SOSQLSRC)
      this.oPgFrm.Page2.oPag.oSOSQLSRC_4_1.value=this.w_SOSQLSRC
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCDSN_5_3.value==this.w_ODBCDSN)
      this.oPgFrm.Page3.oPag.oODBCDSN_5_3.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCPATH_5_5.value==this.w_ODBCPATH)
      this.oPgFrm.Page3.oPag.oODBCPATH_5_5.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCUSER_5_8.value==this.w_ODBCUSER)
      this.oPgFrm.Page3.oPag.oODBCUSER_5_8.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.value==this.w_ODBCPASSW)
      this.oPgFrm.Page3.oPag.oODBCPASSW_5_10.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTRAPAR_1_14.RadioValue()==this.w_TRTRAPAR)
      this.oPgFrm.Page1.oPag.oTRTRAPAR_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_1.value==this.w_TRCODEXT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_1.value=this.w_TRCODEXT
      replace t_TRCODEXT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODEXT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODENT_2_2.value==this.w_TRCODENT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODENT_2_2.value=this.w_TRCODENT
      replace t_TRCODENT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODENT_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'TRASCODI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(NOT EMPTY( .w_TBNAME ) OR NOT EMPTY( .w_TBNAME2 ))  and not(empty(.w_TRNOMCAM))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRNOMCAM_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il campo selezionato � inesistente o non appartiene alle tabelle di destinazione")
          case   (empty(.w_TRCODIMP))  and (.w_TRTIPTRA='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRCODIMP_1_9.SetFocus()
            i_bnoObbl = !empty(.w_TRCODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire tracciato di configurazione.")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsim_mtd
      if i_bnoChk and .w_ODBCCONN > 0
         do gsim_bso with this,"Chiudi"
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_TRCODEXT)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_TRCODEXT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRTIPTRA = this.w_TRTIPTRA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TRCODEXT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TRCODEXT=space(50)
      .w_TRCODENT=space(0)
    endwith
    this.DoRTCalc(1,24,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TRCODEXT = t_TRCODEXT
    this.w_TRCODENT = t_TRCODENT
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TRCODEXT with this.w_TRCODEXT
    replace t_TRCODENT with this.w_TRCODENT
    if i_srv='A'
      replace TRCODEXT with this.w_TRCODEXT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_mtdPag1 as StdContainer
  Width  = 638
  height = 419
  stdWidth  = 638
  stdheight = 419
  resizeXpos=372
  resizeYpos=382
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARTABELL_1_1 as StdField with uid="EJAYNHAENO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ARTABELL", cQueryName = "ARTABELL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 191047854,;
   bGlobalFont=.t.,;
    Height=21, Width=190, Left=387, Top=11, InputMask=replicate('X',15)

  add object oARTABDET_1_2 as StdField with uid="YBIVBYMBGX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ARTABDET", cQueryName = "ARTABDET",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 60610394,;
   bGlobalFont=.t.,;
    Height=21, Width=190, Left=387, Top=36, InputMask=replicate('X',15)

  func oARTABDET_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_NOMCAM2)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFLCOMMEN_1_3 as StdField with uid="WKYMJYLVUT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLCOMMEN", cQueryName = "FLCOMMEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 223986084,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=296, Top=60, InputMask=replicate('X',80)

  add object oARDESCRI_1_5 as StdField with uid="GEBYHRBHIO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARDESCRI", cQueryName = "ARDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 61855567,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=208, Top=11, InputMask=replicate('X',20)

  add object oTRCODFIL_1_6 as StdField with uid="UHCTNJQNPZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TRCODFIL", cQueryName = "TRCODFIL",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Archivio di riferimento",;
    HelpContextID = 171325310,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=167, Top=11, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_TRCODFIL"

  func oTRCODFIL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODFIL_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODFIL_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTRCODFIL_1_6.readonly and this.parent.oTRCODFIL_1_6.isprimarykey)
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oTRCODFIL_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"",'',this.parent.oContained
   endif
  endproc
  proc oTRCODFIL_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_TRCODFIL
    i_obj.ecpSave()
  endproc

  add object oTRTIPTRA_1_7 as StdRadio with uid="DSFTTMLKEQ",rtseq=7,rtrep=.f.,left=167, top=90, width=258,height=18;
    , cFormVar="w_TRTIPTRA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTRTIPTRA_1_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutti"
      this.Buttons(1).HelpContextID = 75815031
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Singolo tracciato di importazione"
      this.Buttons(2).HelpContextID = 75815031
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Singolo tracciato di importazione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",18)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTRTIPTRA_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTIPTRA,&i_cF..t_TRTIPTRA),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oTRTIPTRA_1_7.GetRadio()
    this.Parent.oContained.w_TRTIPTRA = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPTRA_1_7.ToRadio()
    this.Parent.oContained.w_TRTIPTRA=trim(this.Parent.oContained.w_TRTIPTRA)
    return(;
      iif(this.Parent.oContained.w_TRTIPTRA=='G',1,;
      iif(this.Parent.oContained.w_TRTIPTRA=='S',2,;
      0)))
  endfunc

  func oTRTIPTRA_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTRNOMCAM_1_8 as StdField with uid="DJIGSZWFIC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TRNOMCAM", cQueryName = "TRCODFIL,TRTIPTRA,TRNOMCAM",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    sErrorMsg = "Il campo selezionato � inesistente o non appartiene alle tabelle di destinazione",;
    HelpContextID = 56260739,;
   bGlobalFont=.t.,;
    Height=20, Width=118, Left=167, Top=60, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="FLNAME", oKey_1_2="this.w_TRNOMCAM"

  func oTRNOMCAM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRNOMCAM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRNOMCAM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTRNOMCAM_1_8.readonly and this.parent.oTRNOMCAM_1_8.isprimarykey)
    do cp_zoom with 'XDC_FIELDS','*','FLNAME',cp_AbsName(this.parent,'oTRNOMCAM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campo dizionario dati",'GSIM_BCC.XDC_FIELDS_VZM',this.parent.oContained
   endif
  endproc

  add object oTRCODIMP_1_9 as StdField with uid="SDXOGUTRER",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TRCODIMP", cQueryName = "TRCODFIL,TRTIPTRA,TRNOMCAM,TRCODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire tracciato di configurazione.",;
    HelpContextID = 120993658,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=424, Top=85, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_TRCODIMP"

  func oTRCODIMP_1_9.mCond()
    with this.Parent.oContained
      return (.w_TRTIPTRA='S')
    endwith
  endfunc

  func oTRCODIMP_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODIMP_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODIMP_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTRCODIMP_1_9.readonly and this.parent.oTRCODIMP_1_9.isprimarykey)
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oTRCODIMP_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"Tracciati importazione",'',this.parent.oContained
   endif
  endproc
  proc oTRCODIMP_1_9.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_TRCODIMP
    i_obj.ecpSave()
  endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=113, width=628,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="TRCODEXT",Label1="Codice esterno",Field2="TRCODENT",Label2="Codice interno",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235760250

  add object oTRTRAPAR_1_14 as StdCheck with uid="DFUZXDPQSJ",rtseq=21,rtrep=.f.,left=167, top=36, caption="Trascodifica parziale",;
    ToolTipText = "Se attivato un valore non presente nella trascodifica viene restituitito inalterato",;
    HelpContextID = 262002824,;
    cFormVar="w_TRTRAPAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRTRAPAR_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTRAPAR,&i_cF..t_TRTRAPAR),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oTRTRAPAR_1_14.GetRadio()
    this.Parent.oContained.w_TRTRAPAR = this.RadioValue()
    return .t.
  endfunc

  func oTRTRAPAR_1_14.ToRadio()
    this.Parent.oContained.w_TRTRAPAR=trim(this.Parent.oContained.w_TRTRAPAR)
    return(;
      iif(this.Parent.oContained.w_TRTRAPAR=='S',1,;
      0))
  endfunc

  func oTRTRAPAR_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oStr_1_10 as StdString with uid="WCHOCBYEUI",Visible=.t., Left=5, Top=61,;
    Alignment=1, Width=159, Height=18,;
    Caption="Nome campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="VDPNDRLNOV",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=162, Height=18,;
    Caption="Archivio di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="UFMYLHMQTQ",Visible=.t., Left=3, Top=89,;
    Alignment=1, Width=153, Height=18,;
    Caption="Si applica a:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=132,;
    width=622+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=133,width=621+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsim_mtdPag2 as StdContainer
    Width  = 638
    height = 419
    stdWidth  = 638
    stdheight = 419
  resizeXpos=562
  resizeYpos=376
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSOSQLSRC_4_1 as StdMemo with uid="PLOVGMRPJV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SOSQLSRC", cQueryName = "SOSQLSRC",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 55362921,;
   bGlobalFont=.t.,;
    Height=410, Width=630, Left=5, Top=4
enddefine

  define class tgsim_mtdPag3 as StdContainer
    Width  = 638
    height = 419
    stdWidth  = 638
    stdheight = 419
  resizeXpos=238
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_5_1 as ah_CBrowse with uid="LBPJECTCYI",left=7, top=6, width=598,height=285,;
    caption='Browse',;
   bGlobalFont=.t.,;
    nPag=5;
    , HelpContextID = 132425494


  add object oBtn_5_2 as StdButton with uid="EUZJYLNNIJ",left=373, top=313, width=48,height=45,;
    CpPicture="BMP/Auto.bmp", caption="", nPag=5;
    , HelpContextID = 200563113;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_2.Click()
      with this.Parent.oContained
        gsim_bso(this.Parent.oContained,"Trascodifica")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_2.mCond()
    with this.Parent.oContained
      return (!empty(.w_ODBCDSN))
    endwith
  endfunc

  add object oODBCDSN_5_3 as StdField with uid="XEHLZVFDPO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 222451226,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=121, Top=313, InputMask=replicate('X',30)

  add object oODBCPATH_5_5 as StdField with uid="WWNCSLLCQU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 5, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 25012782,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=121, Top=337, InputMask=replicate('X',200)


  add object oBtn_5_6 as StdButton with uid="CEHYJDNFNL",left=323, top=338, width=21,height=21,;
    caption="...", nPag=5;
    , HelpContextID = 80129578;
  , bGlobalFont=.t.

    proc oBtn_5_6.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oODBCUSER_5_8 as StdField with uid="EEJDSJIWXT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 63810104,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=121, Top=361, InputMask=replicate('X',30)

  add object oODBCPASSW_5_10 as StdField with uid="RLZKBTNPUM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 25014185,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=385, InputMask=replicate('X',30), PasswordChar='*'


  add object oBtn_5_15 as StdButton with uid="CAERRCHIWU",left=428, top=313, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=5;
    , HelpContextID = 197323385;
    , caption='\<Inserisce';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_15.Click()
      this.parent.oContained.NotifyEvent("Inserimento")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_15.mCond()
    with this.Parent.oContained
      return (!empty(.w_ODBCDSN))
    endwith
  endfunc


  add object oBtn_5_17 as StdButton with uid="ZCJJBWCFOO",left=485, top=313, width=48,height=45,;
    CpPicture="BMP\LANCIATO.BMP", caption="", nPag=5;
    , HelpContextID = 146200535;
    , caption='C\<hiudi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_17.Click()
      with this.Parent.oContained
        do gsim_bso with this.Parent.oContained,"Chiudi"
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_17.mCond()
    with this.Parent.oContained
      return (.w_ODBCCONN > 0)
    endwith
  endfunc

  add object oStr_5_4 as StdString with uid="PZRNRZAVLX",Visible=.t., Left=19, Top=339,;
    Alignment=1, Width=98, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  add object oStr_5_7 as StdString with uid="MDNRFJNEDP",Visible=.t., Left=19, Top=315,;
    Alignment=1, Width=98, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  add object oStr_5_9 as StdString with uid="WTUPAALUNU",Visible=.t., Left=19, Top=363,;
    Alignment=1, Width=98, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="PBPBMZTTLN",Visible=.t., Left=19, Top=387,;
    Alignment=1, Width=98, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_5_13 as StdString with uid="VJTQYMWNNE",Visible=.t., Left=9, Top=294,;
    Alignment=0, Width=335, Height=18,;
    Caption="Solo per esecuzione interrogazione"  ;
  , bGlobalFont=.t.

  add object oBox_5_12 as StdBox with uid="CACZPQPLOB",left=7, top=309, width=344,height=105
enddefine

* --- Defining Body row
define class tgsim_mtdBodyRow as CPBodyRowCnt
  Width=612
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTRCODEXT_2_1 as StdTrsField with uid="DYLZKTQYLE",rtseq=10,rtrep=.t.,;
    cFormVar="w_TRCODEXT",value=space(50),isprimarykey=.t.,;
    HelpContextID = 188102518,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=303, Left=-2, Top=0, InputMask=replicate('X',50)

  add object oTRCODENT_2_2 as StdTrsMemo with uid="XBDXINVKGI",rtseq=11,rtrep=.t.,;
    cFormVar="w_TRCODENT",value=space(0),;
    HelpContextID = 188102518,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=300, Left=307, Top=0
  add object oLast as LastKeyMover
  * ---
  func oTRCODEXT_2_1.When()
    return(.t.)
  proc oTRCODEXT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTRCODEXT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mtd','TRASCODI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODFIL=TRASCODI.TRCODFIL";
  +" and "+i_cAliasName2+".TRTIPTRA=TRASCODI.TRTIPTRA";
  +" and "+i_cAliasName2+".TRNOMCAM=TRASCODI.TRNOMCAM";
  +" and "+i_cAliasName2+".TRCODIMP=TRASCODI.TRCODIMP";
  +" and "+i_cAliasName2+".TRCODIMP=TRASCODI.TRCODIMP";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsim_mtd
DEFINE CLASS ah_CBrowse AS Grid

  cCursor=""

  * Standard
  RecordSource=""
  RecordSourceType=1   && ALIAS
  ColumnCount=0
  ReadOnly=.t.
  DeleteMark=.f.

  proc Calculate(xValue)
  endproc

  proc Event(cEvent)
    if cEvent="Browse"
       this.Browse()
    endif
  endproc

  proc Browse
    local nI,cI,oCol
    with this
       .RecordSource=''
       .ColumnCount=0
       if !empty(.cCursor)
          .ColumnCount=fcount(.cCursor)
          .recordsource=.cCursor
          for nI=1 to .ColumnCount
	    cI=alltrim(str(nI))
	    oCol=.Columns(nI)
            oCol.Header1.Caption=field(nI,.cCursor)
   	    oCol.Bound=.f.
            oCol.ReadOnly=.ReadOnly
   	    oCol.ControlSource=alltrim(.cCursor)+'.'+field(nI,.cCursor)
   	    oCol.SelectOnEntry=.f.   	
	   next
       endif
    endwith
    Grid::Refresh()
  endproc

ENDDEFINE
* --- Fine Area Manuale
