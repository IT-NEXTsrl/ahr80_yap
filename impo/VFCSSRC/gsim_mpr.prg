* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mpr                                                        *
*              Valori predefiniti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-25                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_mpr"))

* --- Class definition
define class tgsim_mpr as StdTrsForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 761
  Height = 458+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=147439465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PREDEFIN_IDX = 0
  IMPOARCH_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "PREDEFIN"
  cKeySelect = "PRCODFIL"
  cKeyWhere  = "PRCODFIL=this.w_PRCODFIL"
  cKeyDetail  = "PRCODFIL=this.w_PRCODFIL and PRNOMCAM=this.w_PRNOMCAM and PRNOMCAM=this.w_PRNOMCAM"
  cKeyWhereODBC = '"PRCODFIL="+cp_ToStrODBC(this.w_PRCODFIL)';

  cKeyDetailWhereODBC = '"PRCODFIL="+cp_ToStrODBC(this.w_PRCODFIL)';
      +'+" and PRNOMCAM="+cp_ToStrODBC(this.w_PRNOMCAM)';
      +'+" and PRNOMCAM="+cp_ToStrODBC(this.w_PRNOMCAM)';

  cKeyWhereODBCqualified = '"PREDEFIN.PRCODFIL="+cp_ToStrODBC(this.w_PRCODFIL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsim_mpr"
  cComment = "Valori predefiniti"
  i_nRowNum = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODFIL = space(2)
  w_PRNOMCAM = space(15)
  w_PRNOMCAM = space(15)
  w_PRTIPCAM = space(1)
  w_FLCOMMEN = space(80)
  w_ARDESCRI = space(20)
  w_ARTABELLA = space(15)
  w_PRLUNCAM = 0
  w_PRVALPRE = space(0)
  w_PRTIPVAL = space(1)

  * --- Children pointers
  gsim_mpi = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PREDEFIN','gsim_mpr')
    stdPageFrame::Init()
    *set procedure to gsim_mpi additive
    with this
      .Pages(1).addobject("oPag","tgsim_mprPag1","gsim_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Valori predefiniti")
      .Pages(1).HelpContextID = 127435919
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCODFIL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsim_mpi
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='PREDEFIN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PREDEFIN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PREDEFIN_IDX,3]
  return

  function CreateChildren()
    this.gsim_mpi = CREATEOBJECT('stdDynamicChild',this,'gsim_mpi',this.oPgFrm.Page1.oPag.oLinkPC_2_8)
    this.gsim_mpi.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsim_mpi)
      this.gsim_mpi.DestroyChildrenChain()
      this.gsim_mpi=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsim_mpi.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsim_mpi.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsim_mpi.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .gsim_mpi.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PRCODFIL,"PICODFIL";
             ,.w_PRNOMCAM,"PINOMCAM";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PRCODFIL = NVL(PRCODFIL,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PREDEFIN where PRCODFIL=KeySet.PRCODFIL
    *                            and PRNOMCAM=KeySet.PRNOMCAM
    *                            and PRNOMCAM=KeySet.PRNOMCAM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2],this.bLoadRecFilter,this.PREDEFIN_IDX,"gsim_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PREDEFIN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PREDEFIN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PREDEFIN '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODFIL',this.w_PRCODFIL  )
      select * from (i_cTable) PREDEFIN where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ARDESCRI = space(20)
        .w_ARTABELLA = space(15)
        .w_PRCODFIL = NVL(PRCODFIL,space(2))
          if link_1_1_joined
            this.w_PRCODFIL = NVL(ARCODICE101,NVL(this.w_PRCODFIL,space(2)))
            this.w_ARDESCRI = NVL(ARDESCRI101,space(20))
            this.w_ARTABELLA = NVL(ARTABELL101,space(15))
          else
          .link_1_1('Load')
          endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PREDEFIN')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_FLCOMMEN = space(80)
          .w_PRNOMCAM = NVL(PRNOMCAM,space(15))
          .link_2_1('Load')
          .w_PRNOMCAM = NVL(PRNOMCAM,space(15))
          .link_2_2('Load')
          .w_PRTIPCAM = NVL(PRTIPCAM,space(1))
          .w_PRLUNCAM = NVL(PRLUNCAM,0)
          .w_PRVALPRE = NVL(PRVALPRE,space(0))
          .w_PRTIPVAL = NVL(PRTIPVAL,space(1))
          select (this.cTrsName)
          append blank
          replace PRCODFIL with .w_PRCODFIL
          replace PRNOMCAM with .w_PRNOMCAM
          replace PRNOMCAM with .w_PRNOMCAM
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PRCODFIL=space(2)
      .w_PRNOMCAM=space(15)
      .w_PRNOMCAM=space(15)
      .w_PRTIPCAM=space(1)
      .w_FLCOMMEN=space(80)
      .w_ARDESCRI=space(20)
      .w_ARTABELLA=space(15)
      .w_PRLUNCAM=0
      .w_PRVALPRE=space(0)
      .w_PRTIPVAL=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PRCODFIL))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_PRNOMCAM))
         .link_2_1('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PRNOMCAM))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,9,.f.)
        .w_PRTIPVAL = 'P'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PREDEFIN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRCODFIL_1_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPRCODFIL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPRCODFIL_1_1.enabled = .t.
      endif
    endwith
    this.gsim_mpi.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PREDEFIN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsim_mpi.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODFIL,"PRCODFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
    i_lTable = "PREDEFIN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PREDEFIN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PRNOMCAM C(15);
      ,t_PRTIPCAM N(3);
      ,t_FLCOMMEN C(80);
      ,t_PRLUNCAM N(3);
      ,t_PRVALPRE M(10);
      ,t_PRTIPVAL N(3);
      ,PRCODFIL C(2);
      ,PRNOMCAM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_1.controlsource=this.cTrsName+'.t_PRNOMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_2.controlsource=this.cTrsName+'.t_PRNOMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.controlsource=this.cTrsName+'.t_PRTIPCAM'
    this.oPgFRm.Page1.oPag.oFLCOMMEN_2_4.controlsource=this.cTrsName+'.t_FLCOMMEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRLUNCAM_2_5.controlsource=this.cTrsName+'.t_PRLUNCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALPRE_2_6.controlsource=this.cTrsName+'.t_PRVALPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.controlsource=this.cTrsName+'.t_PRTIPVAL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(126)
    this.AddVLine(231)
    this.AddVLine(278)
    this.AddVLine(393)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      *
      * insert into PREDEFIN
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PREDEFIN')
        i_extval=cp_InsertValODBCExtFlds(this,'PREDEFIN')
        i_cFldBody=" "+;
                  "(PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM,PRVALPRE"+;
                  ",PRTIPVAL,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_PRCODFIL)+","+cp_ToStrODBCNull(this.w_PRNOMCAM)+","+cp_ToStrODBC(this.w_PRTIPCAM)+","+cp_ToStrODBC(this.w_PRLUNCAM)+","+cp_ToStrODBC(this.w_PRVALPRE)+;
             ","+cp_ToStrODBC(this.w_PRTIPVAL)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PREDEFIN')
        i_extval=cp_InsertValVFPExtFlds(this,'PREDEFIN')
        cp_CheckDeletedKey(i_cTable,0,'PRCODFIL',this.w_PRCODFIL,'PRNOMCAM',this.w_PRNOMCAM,'PRNOMCAM',this.w_PRNOMCAM)
        INSERT INTO (i_cTable) (;
                   PRCODFIL;
                  ,PRNOMCAM;
                  ,PRTIPCAM;
                  ,PRLUNCAM;
                  ,PRVALPRE;
                  ,PRTIPVAL;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PRCODFIL;
                  ,this.w_PRNOMCAM;
                  ,this.w_PRTIPCAM;
                  ,this.w_PRLUNCAM;
                  ,this.w_PRVALPRE;
                  ,this.w_PRTIPVAL;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_PRCODFIL<>PRCODFIL
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PRNOMCAM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PREDEFIN')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                 " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PREDEFIN')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PRNOMCAM=&i_TN.->PRNOMCAM;
                      and PRNOMCAM=&i_TN.->PRNOMCAM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_PRCODFIL<>PRCODFIL
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_PRNOMCAM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.gsim_mpi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PRCODFIL,"PICODFIL";
                     ,this.w_PRNOMCAM,"PINOMCAM";
                     )
              this.gsim_mpi.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                            " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PRNOMCAM=&i_TN.->PRNOMCAM;
                            and PRNOMCAM=&i_TN.->PRNOMCAM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PRNOMCAM with this.w_PRNOMCAM
              replace PRNOMCAM with this.w_PRNOMCAM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PREDEFIN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PREDEFIN')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PRTIPCAM="+cp_ToStrODBC(this.w_PRTIPCAM)+;
                     ",PRLUNCAM="+cp_ToStrODBC(this.w_PRLUNCAM)+;
                     ",PRVALPRE="+cp_ToStrODBC(this.w_PRVALPRE)+;
                     ",PRTIPVAL="+cp_ToStrODBC(this.w_PRTIPVAL)+;
                     ",PRNOMCAM="+cp_ToStrODBC(this.w_PRNOMCAM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PRNOMCAM="+cp_ToStrODBC(PRNOMCAM)+;
                             " and PRNOMCAM="+cp_ToStrODBC(PRNOMCAM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PREDEFIN')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PRTIPCAM=this.w_PRTIPCAM;
                     ,PRLUNCAM=this.w_PRLUNCAM;
                     ,PRVALPRE=this.w_PRVALPRE;
                     ,PRTIPVAL=this.w_PRTIPVAL;
                     ,PRNOMCAM=this.w_PRNOMCAM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PRNOMCAM=&i_TN.->PRNOMCAM;
                                      and PRNOMCAM=&i_TN.->PRNOMCAM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_PRNOMCAM)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.gsim_mpi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PRCODFIL,"PICODFIL";
               ,this.w_PRNOMCAM,"PINOMCAM";
               )
          this.gsim_mpi.mReplace()
          this.gsim_mpi.bSaveContext=.f.
        endif
      endscan
     this.gsim_mpi.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PRNOMCAM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- gsim_mpi : Deleting
        this.gsim_mpi.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PRCODFIL,"PICODFIL";
               ,this.w_PRNOMCAM,"PINOMCAM";
               )
        this.gsim_mpi.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PREDEFIN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                            " and PRNOMCAM="+cp_ToStrODBC(&i_TN.->PRNOMCAM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PRNOMCAM=&i_TN.->PRNOMCAM;
                              and PRNOMCAM=&i_TN.->PRNOMCAM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PRNOMCAM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODFIL
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_PRCODFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_PRCODFIL))
          select ARCODICE,ARDESCRI,ARTABELL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODFIL)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODFIL) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oPRCODFIL_1_1'),i_cWhere,'GSIM_AAR',"Selezione archivio di destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_PRCODFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_PRCODFIL)
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODFIL = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESCRI = NVL(_Link_.ARDESCRI,space(20))
      this.w_ARTABELLA = NVL(_Link_.ARTABELL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODFIL = space(2)
      endif
      this.w_ARDESCRI = space(20)
      this.w_ARTABELLA = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMPOARCH_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.ARCODICE as ARCODICE101"+ ",link_1_1.ARDESCRI as ARDESCRI101"+ ",link_1_1.ARTABELL as ARTABELL101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PREDEFIN.PRCODFIL=link_1_1.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PREDEFIN.PRCODFIL=link_1_1.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRNOMCAM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_MST',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_PRNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ARTABELLA;
                     ,'FLNAME',trim(this.w_PRNOMCAM))
          select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNOMCAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oPRNOMCAM_2_1'),i_cWhere,'GSIM_MST',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTABELLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PRNOMCAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABELLA;
                       ,'FLNAME',this.w_PRNOMCAM)
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNOMCAM = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(80))
      this.w_PRTIPCAM = NVL(_Link_.FLTYPE,space(1))
      this.w_PRLUNCAM = NVL(_Link_.FLLENGHT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRNOMCAM = space(15)
      endif
      this.w_FLCOMMEN = space(80)
      this.w_PRTIPCAM = space(1)
      this.w_PRLUNCAM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRNOMCAM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_PRNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ARTABELLA;
                     ,'FLNAME',trim(this.w_PRNOMCAM))
          select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNOMCAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FLCOMMEN like "+cp_ToStrODBC(trim(this.w_PRNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);

            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FLCOMMEN like "+cp_ToStr(trim(this.w_PRNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStr(this.w_ARTABELLA);

            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oPRNOMCAM_2_2'),i_cWhere,'',"Dizionario dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTABELLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PRNOMCAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABELLA;
                       ,'FLNAME',this.w_PRNOMCAM)
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNOMCAM = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(80))
      this.w_PRTIPCAM = NVL(_Link_.FLTYPE,space(1))
      this.w_PRLUNCAM = NVL(_Link_.FLLENGHT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRNOMCAM = space(15)
      endif
      this.w_FLCOMMEN = space(80)
      this.w_PRTIPCAM = space(1)
      this.w_PRLUNCAM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPRCODFIL_1_1.value==this.w_PRCODFIL)
      this.oPgFrm.Page1.oPag.oPRCODFIL_1_1.value=this.w_PRCODFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMMEN_2_4.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oFLCOMMEN_2_4.value=this.w_FLCOMMEN
      replace t_FLCOMMEN with this.oPgFrm.Page1.oPag.oFLCOMMEN_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value==this.w_ARDESCRI)
      this.oPgFrm.Page1.oPag.oARDESCRI_1_3.value=this.w_ARDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABELLA_1_4.value==this.w_ARTABELLA)
      this.oPgFrm.Page1.oPag.oARTABELLA_1_4.value=this.w_ARTABELLA
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_1.value==this.w_PRNOMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_1.value=this.w_PRNOMCAM
      replace t_PRNOMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_2.value==this.w_PRNOMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_2.value=this.w_PRNOMCAM
      replace t_PRNOMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNOMCAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.RadioValue()==this.w_PRTIPCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.SetRadio()
      replace t_PRTIPCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLUNCAM_2_5.value==this.w_PRLUNCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLUNCAM_2_5.value=this.w_PRLUNCAM
      replace t_PRLUNCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLUNCAM_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALPRE_2_6.value==this.w_PRVALPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALPRE_2_6.value=this.w_PRVALPRE
      replace t_PRVALPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRVALPRE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.RadioValue()==this.w_PRTIPVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.SetRadio()
      replace t_PRTIPVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'PREDEFIN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_PRNOMCAM)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .gsim_mpi.CheckForm()
      if not(Empty(.w_PRNOMCAM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- gsim_mpi : Depends On
    this.gsim_mpi.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PRNOMCAM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PRNOMCAM=space(15)
      .w_PRNOMCAM=space(15)
      .w_PRTIPCAM=space(1)
      .w_FLCOMMEN=space(80)
      .w_PRLUNCAM=0
      .w_PRVALPRE=space(0)
      .w_PRTIPVAL=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_PRNOMCAM))
        .link_2_1('Full')
      endif
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_PRNOMCAM))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,9,.f.)
        .w_PRTIPVAL = 'P'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PRNOMCAM = t_PRNOMCAM
    this.w_PRNOMCAM = t_PRNOMCAM
    this.w_PRTIPCAM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.RadioValue(.t.)
    this.w_FLCOMMEN = t_FLCOMMEN
    this.w_PRLUNCAM = t_PRLUNCAM
    this.w_PRVALPRE = t_PRVALPRE
    this.w_PRTIPVAL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PRNOMCAM with this.w_PRNOMCAM
    replace t_PRNOMCAM with this.w_PRNOMCAM
    replace t_PRTIPCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_3.ToRadio()
    replace t_FLCOMMEN with this.w_FLCOMMEN
    replace t_PRLUNCAM with this.w_PRLUNCAM
    replace t_PRVALPRE with this.w_PRVALPRE
    replace t_PRTIPVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPVAL_2_7.ToRadio()
    if i_srv='A'
      replace PRNOMCAM with this.w_PRNOMCAM
      replace PRNOMCAM with this.w_PRNOMCAM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_mprPag1 as StdContainer
  Width  = 757
  height = 458
  stdWidth  = 757
  stdheight = 458
  resizeXpos=612
  resizeYpos=252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCODFIL_1_1 as StdField with uid="FKLMBQWQWR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRCODFIL", cQueryName = "PRCODFIL",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 30001218,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=157, Top=9, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_PRCODFIL"

  func oPRCODFIL_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODFIL_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODFIL_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPRCODFIL_1_1.readonly and this.parent.oPRCODFIL_1_1.isprimarykey)
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oPRCODFIL_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"Selezione archivio di destinazione",'',this.parent.oContained
   endif
  endproc
  proc oPRCODFIL_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_PRCODFIL
    i_obj.ecpSave()
  endproc

  add object oARDESCRI_1_3 as StdField with uid="XSRVCXGUEM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARDESCRI", cQueryName = "ARDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 5253297,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=199, Top=9, InputMask=replicate('X',20)

  add object oARTABELLA_1_4 as StdField with uid="UOJCTIWHMD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ARTABELLA", cQueryName = "ARTABELLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 258155678,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=354, Top=9, InputMask=replicate('X',15)

  func oARTABELLA_1_4.mCond()
    with this.Parent.oContained
        if .nLastRow>1
          return (.f.)
        endif
    endwith
  endfunc

  func oARTABELLA_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PRNOMCAM)
        bRes2=.link_2_1('Full')
      endif
      if .not. empty(.w_PRNOMCAM)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=32, width=743,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="PRNOMCAM",Label1="Campo",Field2="PRTIPCAM",Label2="Tipo",Field3="PRLUNCAM",Label3="Lungh.",Field4="PRTIPVAL",Label4="Criterio",Field5="PRVALPRE",Label5="Valore predefinito",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168651386

  add object oStr_1_2 as StdString with uid="TGETQQPVCO",Visible=.t., Left=6, Top=12,;
    Alignment=1, Width=148, Height=18,;
    Caption="Archivio di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JQTKKWCAUE",Visible=.t., Left=4, Top=322,;
    Alignment=0, Width=422, Height=18,;
    Caption="Valori predefiniti per importazioni"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsim_mpi",lower(this.oContained.gsim_mpi.class))=0
        this.oContained.gsim_mpi.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=51,;
    width=737+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.5000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=52,width=736+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.5000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oFLCOMMEN_2_4.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oPRNOMCAM_2_1
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oPRNOMCAM_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oFLCOMMEN_2_4 as StdTrsField with uid="VQELNCRESC",rtseq=5,rtrep=.t.,;
    cFormVar="w_FLCOMMEN",value=space(80),enabled=.f.,;
    HelpContextID = 156877220,;
    cTotal="", bFixedPos=.t., cQueryName = "FLCOMMEN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=5, Top=298, InputMask=replicate('X',80)

  add object oLinkPC_2_8 as stdDynamicChildContainer with uid="GCJRKXZMZZ",bOnScreen=.t.,width=636,height=120,;
   left=117, top=338;


  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsim_mprBodyRow as CPBodyRowCnt
  Width=727
  Height=int(fontmetric(1,"Arial",9,"")*1*1.5000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPRNOMCAM_2_1 as StdTrsField with uid="DPSWSPXSFJ",rtseq=2,rtrep=.t.,;
    cFormVar="w_PRNOMCAM",value=space(15),isprimarykey=.t.,;
    HelpContextID = 10848189,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=118, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", cZoomOnZoom="GSIM_MST", oKey_1_1="TBNAME", oKey_1_2="this.w_ARTABELLA", oKey_2_1="FLNAME", oKey_2_2="this.w_PRNOMCAM"

  func oPRNOMCAM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNOMCAM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRNOMCAM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPRNOMCAM_2_1.readonly and this.parent.oPRNOMCAM_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ARTABELLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ARTABELLA)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oPRNOMCAM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_MST',"Dizionario dati",'',this.parent.oContained
   endif
  endproc
  proc oPRNOMCAM_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSIM_MST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TBNAME=w_ARTABELLA
     i_obj.w_FLNAME=this.parent.oContained.w_PRNOMCAM
    i_obj.ecpSave()
  endproc

  add object oPRNOMCAM_2_2 as StdTrsField with uid="JMMXWSXVBF",rtseq=3,rtrep=.t.,;
    cFormVar="w_PRNOMCAM",value=space(15),isprimarykey=.t.,;
    HelpContextID = 10848189,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=118, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_ARTABELLA", oKey_2_1="FLNAME", oKey_2_2="this.w_PRNOMCAM"

  func oPRNOMCAM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNOMCAM_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRNOMCAM_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPRNOMCAM_2_2.readonly and this.parent.oPRNOMCAM_2_2.isprimarykey)
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ARTABELLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ARTABELLA)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oPRNOMCAM_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dizionario dati",'',this.parent.oContained
   endif
  endproc

  add object oPRTIPCAM_2_3 as StdTrsCombo with uid="IWSMXCXMMI",rtrep=.t.,;
    cFormVar="w_PRTIPCAM", RowSource=""+"Carattere,"+"Numerico,"+"Logico,"+"Memo,"+"Data,"+"Espressione" , ;
    ToolTipText = "Tipo del campo",;
    HelpContextID = 8071101,;
    Height=22, Width=102, Left=120, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPRTIPCAM_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPCAM,&i_cF..t_PRTIPCAM),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    iif(xVal =3,'L',;
    iif(xVal =4,'M',;
    iif(xVal =5,'D',;
    iif(xVal =6,'E',;
    space(1))))))))
  endfunc
  func oPRTIPCAM_2_3.GetRadio()
    this.Parent.oContained.w_PRTIPCAM = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPCAM_2_3.ToRadio()
    this.Parent.oContained.w_PRTIPCAM=trim(this.Parent.oContained.w_PRTIPCAM)
    return(;
      iif(this.Parent.oContained.w_PRTIPCAM=='C',1,;
      iif(this.Parent.oContained.w_PRTIPCAM=='N',2,;
      iif(this.Parent.oContained.w_PRTIPCAM=='L',3,;
      iif(this.Parent.oContained.w_PRTIPCAM=='M',4,;
      iif(this.Parent.oContained.w_PRTIPCAM=='D',5,;
      iif(this.Parent.oContained.w_PRTIPCAM=='E',6,;
      0)))))))
  endfunc

  func oPRTIPCAM_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPRLUNCAM_2_5 as StdTrsField with uid="ONJDFCTRTK",rtseq=8,rtrep=.t.,;
    cFormVar="w_PRLUNCAM",value=0,enabled=.f.,;
    HelpContextID = 9414589,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=38, Left=225, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oPRVALPRE_2_6 as StdTrsMemo with uid="BYRHXARQNG",rtseq=9,rtrep=.t.,;
    cFormVar="w_PRVALPRE",value=space(0),;
    HelpContextID = 205322299,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=20, Width=333, Left=389, Top=0

  add object oPRTIPVAL_2_7 as StdTrsCombo with uid="MCSUQCDWVS",rtrep=.t.,;
    cFormVar="w_PRTIPVAL", RowSource=""+"Predefinito,"+"Nessun valore" , ;
    ToolTipText = "Valore da assegnare al campo se non � stato valorizzato",;
    HelpContextID = 42260546,;
    Height=25, Width=103, Left=278, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPRTIPVAL_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPVAL,&i_cF..t_PRTIPVAL),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oPRTIPVAL_2_7.GetRadio()
    this.Parent.oContained.w_PRTIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPVAL_2_7.ToRadio()
    this.Parent.oContained.w_PRTIPVAL=trim(this.Parent.oContained.w_PRTIPVAL)
    return(;
      iif(this.Parent.oContained.w_PRTIPVAL=='P',1,;
      iif(this.Parent.oContained.w_PRTIPVAL=='N',2,;
      0)))
  endfunc

  func oPRTIPVAL_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPRNOMCAM_2_1.When()
    return(.t.)
  proc oPRNOMCAM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPRNOMCAM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mpr','PREDEFIN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODFIL=PREDEFIN.PRCODFIL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
