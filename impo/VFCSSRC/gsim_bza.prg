* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bza                                                        *
*              Zoom ASCII o ODBC                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_10]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-19                                                      *
* Last revis.: 2006-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CTRLOBJ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bza",oParentObject,m.w_CTRLOBJ)
return(i_retval)

define class tgsim_bza as StdBatch
  * --- Local variables
  w_CTRLOBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ZOOM
    if this.oParentObject.w_IMTIPSRC="O"
      
      do cp_zoom with "SRCMODBC","*","SOCODICE",cp_AbsName(this.w_CTRLOBJ.Parent,this.w_CTRLOBJ.Name),.f.,"gsim_mso",AH_MSGFORMAT( "SORGENTE DATI" )
    else
      this.oParentObject.w_IM_ASCII=left(getfile()+space(254),254)
    endif
  endproc


  proc Init(oParentObject,w_CTRLOBJ)
    this.w_CTRLOBJ=w_CTRLOBJ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CTRLOBJ"
endproc
