* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kac                                                        *
*              Import archivi e movimenti contabili                            *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2013-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kac",oParentObject))

* --- Class definition
define class tgsim_kac as StdForm
  Top    = 6
  Left   = 6

  * --- Standard Properties
  Width  = 636
  Height = 515
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-16"
  HelpContextID=132759401
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=59

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  cPrg = "gsim_kac"
  cComment = "Import archivi e movimenti contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_INCORSO = .F.
  w_TIPSRC = space(1)
  o_TIPSRC = space(1)
  w_CODIMP = space(20)
  w_DESIMP = space(60)
  w_PATH = space(200)
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_TIPDBF = space(1)
  w_SELECT = space(10)
  w_SELELS = space(10)
  w_SELEAG = space(10)
  w_SELEAS = space(10)
  w_SELEAC = space(10)
  w_SELEBA = space(10)
  w_SELEGI = space(10)
  w_SELECC = space(10)
  w_SELECR = space(10)
  w_SELECA = space(10)
  w_SELECL = space(10)
  w_SELECF = space(10)
  w_SELEIV = space(10)
  w_SELECH = space(10)
  w_SELECB = space(10)
  w_SELEDE = space(10)
  w_SELELI = space(10)
  w_SELEMS = space(10)
  w_SELENA = space(10)
  w_SELENM = space(10)
  w_SELEPA = space(10)
  w_SELEPD = space(10)
  w_SELEPC = space(10)
  w_SELEPO = space(10)
  w_SELESC = space(10)
  w_SELERF = space(10)
  w_SELECS = space(10)
  w_SELESP = space(10)
  w_SELETR = space(10)
  w_SELEVA = space(10)
  w_SELEVE = space(10)
  w_SELEZO = space(10)
  w_RADSELEZ = space(10)
  w_SELECONT = space(10)
  w_SELERESO = space(10)
  w_IMPARIVA = space(10)
  w_IMAZZERA = space(10)
  w_IMTRANSA = space(1)
  w_IMCONFER = space(1)
  w_SELECE = space(10)
  w_SELECO = space(10)
  w_SELEMC = space(10)
  w_SELEPT = space(10)
  w_SELEPN = space(10)
  w_SELEPI = space(10)
  w_SELEVC = space(10)
  w_ArchParz = space(45)
  w_ArchPar2 = space(45)
  w_Gestiti = space(45)
  * --- Area Manuale = Declare Variables
  * --- gsim_kac
  Autocenter=.T.
  
  Function GetControlObject(cVar,nPage)
    local oObj
    local nIndex
    oObj = .NULL.
    cVar=upper(cVar)
    with this.oPgFrm.Pages(nPage).oPag
      for nIndex=1 to .ControlCount
          if type('this.oPgFrm.Pages(nPage).oPag.Controls(nIndex).cFormVar') = 'C'
             if upper(.Controls(nIndex).cFormVar) == cVar
                oObj= .Controls(nIndex)
                nIndex = .ControlCount + 1
             endif
          endif
      next
    endwith
    return (oObj)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kacPag1","gsim_kac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODIMP_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gsim_bac with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_INCORSO=.f.
      .w_TIPSRC=space(1)
      .w_CODIMP=space(20)
      .w_DESIMP=space(60)
      .w_PATH=space(200)
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_TIPDBF=space(1)
      .w_SELECT=space(10)
      .w_SELELS=space(10)
      .w_SELEAG=space(10)
      .w_SELEAS=space(10)
      .w_SELEAC=space(10)
      .w_SELEBA=space(10)
      .w_SELEGI=space(10)
      .w_SELECC=space(10)
      .w_SELECR=space(10)
      .w_SELECA=space(10)
      .w_SELECL=space(10)
      .w_SELECF=space(10)
      .w_SELEIV=space(10)
      .w_SELECH=space(10)
      .w_SELECB=space(10)
      .w_SELEDE=space(10)
      .w_SELELI=space(10)
      .w_SELEMS=space(10)
      .w_SELENA=space(10)
      .w_SELENM=space(10)
      .w_SELEPA=space(10)
      .w_SELEPD=space(10)
      .w_SELEPC=space(10)
      .w_SELEPO=space(10)
      .w_SELESC=space(10)
      .w_SELERF=space(10)
      .w_SELECS=space(10)
      .w_SELESP=space(10)
      .w_SELETR=space(10)
      .w_SELEVA=space(10)
      .w_SELEVE=space(10)
      .w_SELEZO=space(10)
      .w_RADSELEZ=space(10)
      .w_SELECONT=space(10)
      .w_SELERESO=space(10)
      .w_IMPARIVA=space(10)
      .w_IMAZZERA=space(10)
      .w_IMTRANSA=space(1)
      .w_IMCONFER=space(1)
      .w_SELECE=space(10)
      .w_SELECO=space(10)
      .w_SELEMC=space(10)
      .w_SELEPT=space(10)
      .w_SELEPN=space(10)
      .w_SELEPI=space(10)
      .w_SELEVC=space(10)
      .w_ArchParz=space(45)
      .w_ArchPar2=space(45)
      .w_Gestiti=space(45)
        .w_INCORSO = .F.
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODIMP))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
          .DoRTCalc(6,6,.f.)
        .w_ODBCPATH = SPACE(200)
          .DoRTCalc(8,10,.f.)
        .w_SELECT = 'CT'
        .w_SELELS = 'LS'
        .w_SELEAG = 'AG'
        .w_SELEAS = 'AS'
        .w_SELEAC = 'AC'
        .w_SELEBA = 'BA'
        .w_SELEGI = 'GI'
        .w_SELECC = 'CC'
        .w_SELECR = 'CR'
        .w_SELECA = 'CA'
        .w_SELECL = 'CL'
        .w_SELECF = 'CF'
        .w_SELEIV = 'IV'
        .w_SELECH = 'CH'
        .w_SELECB = 'CB'
        .w_SELEDE = 'DE'
        .w_SELELI = 'LI'
        .w_SELEMS = 'MS'
        .w_SELENA = 'NA'
        .w_SELENM = 'NM'
        .w_SELEPA = 'PA'
        .w_SELEPD = 'PD'
        .w_SELEPC = 'PC'
        .w_SELEPO = 'PO'
        .w_SELESC = 'SC'
        .w_SELERF = 'RF'
        .w_SELECS = 'CS'
        .w_SELESP = 'SP'
        .w_SELETR = 'TR'
        .w_SELEVA = 'VA'
        .w_SELEVE = 'VE'
        .w_SELEZO = 'ZO'
        .w_RADSELEZ = 'D'
        .w_SELECONT = 'N'
        .w_SELERESO = 'E'
        .w_IMPARIVA = 'S'
        .w_IMAZZERA = 'N'
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .w_IMTRANSA = 'S'
        .w_IMCONFER = 'N'
        .w_SELECE = 'CE'
        .w_SELECO = 'CO'
        .w_SELEMC = 'MC'
        .w_SELEPT = 'PT'
        .w_SELEPN = 'PN'
        .w_SELEPI = 'PI'
        .w_SELEVC = 'VC'
        .w_ArchParz = .w_SELECB+"|"+.w_SELEVE+"|"+.w_SELESP+"|"+.w_SELEPO+"|"+.w_SELELS+"|"+.w_SELEAC+"|"+.w_SELEAG+"|"+.w_SELEBA+"|"+.w_SELECC+"|"+.w_SELECF+"|"+.w_SELEDE+"|"+.w_SELEIV+"|"+.w_SELELI+"|"+.w_SELERF+"|"
        .w_ArchPar2 = .w_SELECA+"|"+.w_SELEPN+"|"+.w_SELEPT+"|"+.w_SELEPI+"|"+.w_SELECO+"|"+.w_SELECE+"|"+.w_SELEVC+"|"+.w_SELEMC+"|"+.w_SELENM+"|"
        .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2+.w_SELENA+"|"+.w_SELEPA+"|"+.w_SELEPC+"|"+.w_SELEPD+"|"+.w_SELEVA+"|"+.w_SELEZO+"|"+.w_SELESC+"|"+.w_SELECR+"|"+.w_SELECS+"|"+.w_SELECT+"|"+.w_SELECL+"|"+.w_SELEGI+"|"+.w_SELEAS+"|"+.w_SELETR+"|"+.w_SELEMS+"|"+.w_SELECH)
      .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPSRC<>.w_TIPSRC
            .w_PATH = IIF(.w_TIPSRC='E', '',sys(5)+sys(2003)+"\")
        endif
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .DoRTCalc(6,56,.t.)
            .w_ArchParz = .w_SELECB+"|"+.w_SELEVE+"|"+.w_SELESP+"|"+.w_SELEPO+"|"+.w_SELELS+"|"+.w_SELEAC+"|"+.w_SELEAG+"|"+.w_SELEBA+"|"+.w_SELECC+"|"+.w_SELECF+"|"+.w_SELEDE+"|"+.w_SELEIV+"|"+.w_SELELI+"|"+.w_SELERF+"|"
            .w_ArchPar2 = .w_SELECA+"|"+.w_SELEPN+"|"+.w_SELEPT+"|"+.w_SELEPI+"|"+.w_SELECO+"|"+.w_SELECE+"|"+.w_SELEVC+"|"+.w_SELEMC+"|"+.w_SELENM+"|"
            .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2+.w_SELENA+"|"+.w_SELEPA+"|"+.w_SELEPC+"|"+.w_SELEPD+"|"+.w_SELEVA+"|"+.w_SELEZO+"|"+.w_SELESC+"|"+.w_SELECR+"|"+.w_SELECS+"|"+.w_SELECT+"|"+.w_SELECL+"|"+.w_SELEGI+"|"+.w_SELEAS+"|"+.w_SELETR+"|"+.w_SELEMS+"|"+.w_SELECH)
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODIMP_1_3.enabled = this.oPgFrm.Page1.oPag.oCODIMP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oPATH_1_5.enabled = this.oPgFrm.Page1.oPag.oPATH_1_5.mCond()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_6.enabled = this.oPgFrm.Page1.oPag.oODBCDSN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_8.enabled = this.oPgFrm.Page1.oPag.oODBCPATH_1_8.mCond()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_10.enabled = this.oPgFrm.Page1.oPag.oODBCUSER_1_10.mCond()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.enabled = this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSELECT_1_14.enabled = this.oPgFrm.Page1.oPag.oSELECT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oSELELS_1_15.enabled = this.oPgFrm.Page1.oPag.oSELELS_1_15.mCond()
    this.oPgFrm.Page1.oPag.oSELEAG_1_16.enabled = this.oPgFrm.Page1.oPag.oSELEAG_1_16.mCond()
    this.oPgFrm.Page1.oPag.oSELEAS_1_17.enabled = this.oPgFrm.Page1.oPag.oSELEAS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oSELEAC_1_18.enabled = this.oPgFrm.Page1.oPag.oSELEAC_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSELEBA_1_19.enabled = this.oPgFrm.Page1.oPag.oSELEBA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oSELEGI_1_20.enabled = this.oPgFrm.Page1.oPag.oSELEGI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSELECC_1_21.enabled = this.oPgFrm.Page1.oPag.oSELECC_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSELECR_1_22.enabled = this.oPgFrm.Page1.oPag.oSELECR_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSELECA_1_23.enabled = this.oPgFrm.Page1.oPag.oSELECA_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSELECL_1_24.enabled = this.oPgFrm.Page1.oPag.oSELECL_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSELECF_1_25.enabled = this.oPgFrm.Page1.oPag.oSELECF_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSELEIV_1_26.enabled = this.oPgFrm.Page1.oPag.oSELEIV_1_26.mCond()
    this.oPgFrm.Page1.oPag.oSELECH_1_27.enabled = this.oPgFrm.Page1.oPag.oSELECH_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSELECB_1_28.enabled = this.oPgFrm.Page1.oPag.oSELECB_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSELEDE_1_29.enabled = this.oPgFrm.Page1.oPag.oSELEDE_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSELELI_1_30.enabled = this.oPgFrm.Page1.oPag.oSELELI_1_30.mCond()
    this.oPgFrm.Page1.oPag.oSELEMS_1_31.enabled = this.oPgFrm.Page1.oPag.oSELEMS_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSELENA_1_32.enabled = this.oPgFrm.Page1.oPag.oSELENA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSELENM_1_33.enabled = this.oPgFrm.Page1.oPag.oSELENM_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSELEPA_1_34.enabled = this.oPgFrm.Page1.oPag.oSELEPA_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSELEPD_1_35.enabled = this.oPgFrm.Page1.oPag.oSELEPD_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSELEPC_1_36.enabled = this.oPgFrm.Page1.oPag.oSELEPC_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSELEPO_1_37.enabled = this.oPgFrm.Page1.oPag.oSELEPO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSELESC_1_38.enabled = this.oPgFrm.Page1.oPag.oSELESC_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSELERF_1_39.enabled = this.oPgFrm.Page1.oPag.oSELERF_1_39.mCond()
    this.oPgFrm.Page1.oPag.oSELECS_1_40.enabled = this.oPgFrm.Page1.oPag.oSELECS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oSELESP_1_41.enabled = this.oPgFrm.Page1.oPag.oSELESP_1_41.mCond()
    this.oPgFrm.Page1.oPag.oSELETR_1_42.enabled = this.oPgFrm.Page1.oPag.oSELETR_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSELEVA_1_43.enabled = this.oPgFrm.Page1.oPag.oSELEVA_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSELEVE_1_44.enabled = this.oPgFrm.Page1.oPag.oSELEVE_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSELEZO_1_45.enabled = this.oPgFrm.Page1.oPag.oSELEZO_1_45.mCond()
    this.oPgFrm.Page1.oPag.oRADSELEZ_1_46.enabled_(this.oPgFrm.Page1.oPag.oRADSELEZ_1_46.mCond())
    this.oPgFrm.Page1.oPag.oSELECONT_1_47.enabled = this.oPgFrm.Page1.oPag.oSELECONT_1_47.mCond()
    this.oPgFrm.Page1.oPag.oSELERESO_1_48.enabled = this.oPgFrm.Page1.oPag.oSELERESO_1_48.mCond()
    this.oPgFrm.Page1.oPag.oIMPARIVA_1_49.enabled = this.oPgFrm.Page1.oPag.oIMPARIVA_1_49.mCond()
    this.oPgFrm.Page1.oPag.oIMAZZERA_1_50.enabled = this.oPgFrm.Page1.oPag.oIMAZZERA_1_50.mCond()
    this.oPgFrm.Page1.oPag.oIMTRANSA_1_59.enabled = this.oPgFrm.Page1.oPag.oIMTRANSA_1_59.mCond()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_60.enabled = this.oPgFrm.Page1.oPag.oIMCONFER_1_60.mCond()
    this.oPgFrm.Page1.oPag.oSELECE_1_68.enabled = this.oPgFrm.Page1.oPag.oSELECE_1_68.mCond()
    this.oPgFrm.Page1.oPag.oSELECO_1_69.enabled = this.oPgFrm.Page1.oPag.oSELECO_1_69.mCond()
    this.oPgFrm.Page1.oPag.oSELEMC_1_70.enabled = this.oPgFrm.Page1.oPag.oSELEMC_1_70.mCond()
    this.oPgFrm.Page1.oPag.oSELEPT_1_71.enabled = this.oPgFrm.Page1.oPag.oSELEPT_1_71.mCond()
    this.oPgFrm.Page1.oPag.oSELEPN_1_72.enabled = this.oPgFrm.Page1.oPag.oSELEPN_1_72.mCond()
    this.oPgFrm.Page1.oPag.oSELEPI_1_73.enabled = this.oPgFrm.Page1.oPag.oSELEPI_1_73.mCond()
    this.oPgFrm.Page1.oPag.oSELEVC_1_74.enabled = this.oPgFrm.Page1.oPag.oSELEVC_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPATH_1_5.visible=!this.oPgFrm.Page1.oPag.oPATH_1_5.mHide()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_6.visible=!this.oPgFrm.Page1.oPag.oODBCDSN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_8.visible=!this.oPgFrm.Page1.oPag.oODBCPATH_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_10.visible=!this.oPgFrm.Page1.oPag.oODBCUSER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.visible=!this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_60.visible=!this.oPgFrm.Page1.oPag.oIMCONFER_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_3'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(60))
      this.w_IMPARIVA = NVL(_Link_.IMPARIVA,space(10))
      this.w_IMAZZERA = NVL(_Link_.IMAZZERA,space(10))
      this.w_SELERESO = NVL(_Link_.IMRESOCO,space(10))
      this.w_SELECONT = NVL(_Link_.IMCONTRO,space(10))
      this.w_TIPSRC = NVL(_Link_.IMTIPSRC,space(1))
      this.w_TIPDBF = NVL(_Link_.IMTIPDBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(60)
      this.w_IMPARIVA = space(10)
      this.w_IMAZZERA = space(10)
      this.w_SELERESO = space(10)
      this.w_SELECONT = space(10)
      this.w_TIPSRC = space(1)
      this.w_TIPDBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_3.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_3.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_4.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_4.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_5.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_5.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCDSN_1_6.value==this.w_ODBCDSN)
      this.oPgFrm.Page1.oPag.oODBCDSN_1_6.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPATH_1_8.value==this.w_ODBCPATH)
      this.oPgFrm.Page1.oPag.oODBCPATH_1_8.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCUSER_1_10.value==this.w_ODBCUSER)
      this.oPgFrm.Page1.oPag.oODBCUSER_1_10.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.value==this.w_ODBCPASSW)
      this.oPgFrm.Page1.oPag.oODBCPASSW_1_11.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECT_1_14.RadioValue()==this.w_SELECT)
      this.oPgFrm.Page1.oPag.oSELECT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELELS_1_15.RadioValue()==this.w_SELELS)
      this.oPgFrm.Page1.oPag.oSELELS_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAG_1_16.RadioValue()==this.w_SELEAG)
      this.oPgFrm.Page1.oPag.oSELEAG_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAS_1_17.RadioValue()==this.w_SELEAS)
      this.oPgFrm.Page1.oPag.oSELEAS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAC_1_18.RadioValue()==this.w_SELEAC)
      this.oPgFrm.Page1.oPag.oSELEAC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBA_1_19.RadioValue()==this.w_SELEBA)
      this.oPgFrm.Page1.oPag.oSELEBA_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEGI_1_20.RadioValue()==this.w_SELEGI)
      this.oPgFrm.Page1.oPag.oSELEGI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECC_1_21.RadioValue()==this.w_SELECC)
      this.oPgFrm.Page1.oPag.oSELECC_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECR_1_22.RadioValue()==this.w_SELECR)
      this.oPgFrm.Page1.oPag.oSELECR_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECA_1_23.RadioValue()==this.w_SELECA)
      this.oPgFrm.Page1.oPag.oSELECA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECL_1_24.RadioValue()==this.w_SELECL)
      this.oPgFrm.Page1.oPag.oSELECL_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECF_1_25.RadioValue()==this.w_SELECF)
      this.oPgFrm.Page1.oPag.oSELECF_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIV_1_26.RadioValue()==this.w_SELEIV)
      this.oPgFrm.Page1.oPag.oSELEIV_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECH_1_27.RadioValue()==this.w_SELECH)
      this.oPgFrm.Page1.oPag.oSELECH_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECB_1_28.RadioValue()==this.w_SELECB)
      this.oPgFrm.Page1.oPag.oSELECB_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDE_1_29.RadioValue()==this.w_SELEDE)
      this.oPgFrm.Page1.oPag.oSELEDE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELELI_1_30.RadioValue()==this.w_SELELI)
      this.oPgFrm.Page1.oPag.oSELELI_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEMS_1_31.RadioValue()==this.w_SELEMS)
      this.oPgFrm.Page1.oPag.oSELEMS_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENA_1_32.RadioValue()==this.w_SELENA)
      this.oPgFrm.Page1.oPag.oSELENA_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENM_1_33.RadioValue()==this.w_SELENM)
      this.oPgFrm.Page1.oPag.oSELENM_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPA_1_34.RadioValue()==this.w_SELEPA)
      this.oPgFrm.Page1.oPag.oSELEPA_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPD_1_35.RadioValue()==this.w_SELEPD)
      this.oPgFrm.Page1.oPag.oSELEPD_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPC_1_36.RadioValue()==this.w_SELEPC)
      this.oPgFrm.Page1.oPag.oSELEPC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPO_1_37.RadioValue()==this.w_SELEPO)
      this.oPgFrm.Page1.oPag.oSELEPO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESC_1_38.RadioValue()==this.w_SELESC)
      this.oPgFrm.Page1.oPag.oSELESC_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERF_1_39.RadioValue()==this.w_SELERF)
      this.oPgFrm.Page1.oPag.oSELERF_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECS_1_40.RadioValue()==this.w_SELECS)
      this.oPgFrm.Page1.oPag.oSELECS_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESP_1_41.RadioValue()==this.w_SELESP)
      this.oPgFrm.Page1.oPag.oSELESP_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETR_1_42.RadioValue()==this.w_SELETR)
      this.oPgFrm.Page1.oPag.oSELETR_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEVA_1_43.RadioValue()==this.w_SELEVA)
      this.oPgFrm.Page1.oPag.oSELEVA_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEVE_1_44.RadioValue()==this.w_SELEVE)
      this.oPgFrm.Page1.oPag.oSELEVE_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZO_1_45.RadioValue()==this.w_SELEZO)
      this.oPgFrm.Page1.oPag.oSELEZO_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRADSELEZ_1_46.RadioValue()==this.w_RADSELEZ)
      this.oPgFrm.Page1.oPag.oRADSELEZ_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECONT_1_47.RadioValue()==this.w_SELECONT)
      this.oPgFrm.Page1.oPag.oSELECONT_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERESO_1_48.RadioValue()==this.w_SELERESO)
      this.oPgFrm.Page1.oPag.oSELERESO_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPARIVA_1_49.RadioValue()==this.w_IMPARIVA)
      this.oPgFrm.Page1.oPag.oIMPARIVA_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAZZERA_1_50.RadioValue()==this.w_IMAZZERA)
      this.oPgFrm.Page1.oPag.oIMAZZERA_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTRANSA_1_59.RadioValue()==this.w_IMTRANSA)
      this.oPgFrm.Page1.oPag.oIMTRANSA_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCONFER_1_60.RadioValue()==this.w_IMCONFER)
      this.oPgFrm.Page1.oPag.oIMCONFER_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECE_1_68.RadioValue()==this.w_SELECE)
      this.oPgFrm.Page1.oPag.oSELECE_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECO_1_69.RadioValue()==this.w_SELECO)
      this.oPgFrm.Page1.oPag.oSELECO_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEMC_1_70.RadioValue()==this.w_SELEMC)
      this.oPgFrm.Page1.oPag.oSELEMC_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPT_1_71.RadioValue()==this.w_SELEPT)
      this.oPgFrm.Page1.oPag.oSELEPT_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPN_1_72.RadioValue()==this.w_SELEPN)
      this.oPgFrm.Page1.oPag.oSELEPN_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPI_1_73.RadioValue()==this.w_SELEPI)
      this.oPgFrm.Page1.oPag.oSELEPI_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEVC_1_74.RadioValue()==this.w_SELEVC)
      this.oPgFrm.Page1.oPag.oSELEVC_1_74.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODIMP))  and (!.w_INCORSO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMP_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPSRC = this.w_TIPSRC
    return

enddefine

* --- Define pages as container
define class tgsim_kacPag1 as StdContainer
  Width  = 632
  height = 515
  stdWidth  = 632
  stdheight = 515
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODIMP_1_3 as StdField with uid="JVRUITIVQR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 46935002,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=102, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  func oCODIMP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_3.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oDESIMP_1_4 as StdField with uid="OMDLTUIPBE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 46876106,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=261, Top=13, InputMask=replicate('X',60)

  add object oPATH_1_5 as StdField with uid="TEKEDCGFLL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 127678730,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=102, Top=42, InputMask=replicate('X',200)

  func oPATH_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC<>"O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oPATH_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oODBCDSN_1_6 as StdField with uid="CCNSWIOJJL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 6444570,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=103, Top=42, InputMask=replicate('X',30)

  func oODBCDSN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCDSN_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPATH_1_8 as StdField with uid="OTCZZZYUDO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 241019438,;
   bGlobalFont=.t.,;
    Height=21, Width=193, Left=103, Top=65, InputMask=replicate('X',200)

  func oODBCPATH_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDBF='S')
    endwith
   endif
  endfunc

  func oODBCPATH_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="JKKEBCSAFN",left=297, top=67, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 132558378;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDBF='S')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDBF<>'S')
     endwith
    endif
  endfunc

  add object oODBCUSER_1_10 as StdField with uid="KUQMZDJHWU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente per connessione ODBC",;
    HelpContextID = 11381304,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=395, Top=42, InputMask=replicate('X',30)

  func oODBCUSER_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCUSER_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oODBCPASSW_1_11 as StdField with uid="DLZRZHNCSZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Password per connessione ODBC",;
    HelpContextID = 241020841,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=395, Top=65, InputMask=replicate('X',30), PasswordChar='*'

  func oODBCPASSW_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCPASSW_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc


  add object oBtn_1_13 as StdButton with uid="SJQLZNXFRO",left=581, top=42, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 132558378;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPSRC<>"O" and !.w_INCORSO)
      endwith
    endif
  endfunc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPSRC="O")
     endwith
    endif
  endfunc

  add object oSELECT_1_14 as StdCheck with uid="PLBRLEGNFM",rtseq=11,rtrep=.f.,left=11, top=112, caption="Anagrafica cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 258979034,;
    cFormVar="w_SELECT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECT_1_14.RadioValue()
    return(iif(this.value =1,'CT',;
    ' '))
  endfunc
  func oSELECT_1_14.GetRadio()
    this.Parent.oContained.w_SELECT = this.RadioValue()
    return .t.
  endfunc

  func oSELECT_1_14.SetRadio()
    this.Parent.oContained.w_SELECT=trim(this.Parent.oContained.w_SELECT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT=='CT',1,;
      0)
  endfunc

  func oSELECT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECT<>"|")
    endwith
   endif
  endfunc

  add object oSELELS_1_15 as StdCheck with uid="ILDBWXOABO",rtseq=12,rtrep=.f.,left=11, top=131, caption="Anagrafica listini",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 266319066,;
    cFormVar="w_SELELS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELELS_1_15.RadioValue()
    return(iif(this.value =1,'LS',;
    ' '))
  endfunc
  func oSELELS_1_15.GetRadio()
    this.Parent.oContained.w_SELELS = this.RadioValue()
    return .t.
  endfunc

  func oSELELS_1_15.SetRadio()
    this.Parent.oContained.w_SELELS=trim(this.Parent.oContained.w_SELELS)
    this.value = ;
      iif(this.Parent.oContained.w_SELELS=='LS',1,;
      0)
  endfunc

  func oSELELS_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELELS<>"|")
    endwith
   endif
  endfunc

  add object oSELEAG_1_16 as StdCheck with uid="DKTDVRDCHH",rtseq=13,rtrep=.f.,left=11, top=150, caption="Agenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 210744538,;
    cFormVar="w_SELEAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAG_1_16.RadioValue()
    return(iif(this.value =1,'AG',;
    ' '))
  endfunc
  func oSELEAG_1_16.GetRadio()
    this.Parent.oContained.w_SELEAG = this.RadioValue()
    return .t.
  endfunc

  func oSELEAG_1_16.SetRadio()
    this.Parent.oContained.w_SELEAG=trim(this.Parent.oContained.w_SELEAG)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAG=='AG',1,;
      0)
  endfunc

  func oSELEAG_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAG<>"|")
    endwith
   endif
  endfunc

  add object oSELEAS_1_17 as StdCheck with uid="RYXOXHTCLX",rtseq=14,rtrep=.f.,left=11, top=169, caption="Aspetto esteriore beni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 9417946,;
    cFormVar="w_SELEAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAS_1_17.RadioValue()
    return(iif(this.value =1,'AS',;
    ' '))
  endfunc
  func oSELEAS_1_17.GetRadio()
    this.Parent.oContained.w_SELEAS = this.RadioValue()
    return .t.
  endfunc

  func oSELEAS_1_17.SetRadio()
    this.Parent.oContained.w_SELEAS=trim(this.Parent.oContained.w_SELEAS)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAS=='AS',1,;
      0)
  endfunc

  func oSELEAS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAS<>"|")
    endwith
   endif
  endfunc

  add object oSELEAC_1_18 as StdCheck with uid="OYFCHPNVOZ",rtseq=15,rtrep=.f.,left=11, top=188, caption="Automatismi / modelli contabili",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 9417946,;
    cFormVar="w_SELEAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAC_1_18.RadioValue()
    return(iif(this.value =1,'AC',;
    ' '))
  endfunc
  func oSELEAC_1_18.GetRadio()
    this.Parent.oContained.w_SELEAC = this.RadioValue()
    return .t.
  endfunc

  func oSELEAC_1_18.SetRadio()
    this.Parent.oContained.w_SELEAC=trim(this.Parent.oContained.w_SELEAC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAC=='AC',1,;
      0)
  endfunc

  func oSELEAC_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAC<>"|")
    endwith
   endif
  endfunc

  add object oSELEBA_1_19 as StdCheck with uid="JLYKIZSRHZ",rtseq=16,rtrep=.f.,left=11, top=207, caption="Banche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 41923802,;
    cFormVar="w_SELEBA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBA_1_19.RadioValue()
    return(iif(this.value =1,'BA',;
    ' '))
  endfunc
  func oSELEBA_1_19.GetRadio()
    this.Parent.oContained.w_SELEBA = this.RadioValue()
    return .t.
  endfunc

  func oSELEBA_1_19.SetRadio()
    this.Parent.oContained.w_SELEBA=trim(this.Parent.oContained.w_SELEBA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBA=='BA',1,;
      0)
  endfunc

  func oSELEBA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBA<>"|")
    endwith
   endif
  endfunc

  add object oSELEGI_1_20 as StdCheck with uid="WHCJAYEIGK",rtseq=17,rtrep=.f.,left=11, top=226, caption="Cambi giornalieri",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 170898650,;
    cFormVar="w_SELEGI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEGI_1_20.RadioValue()
    return(iif(this.value =1,'GI',;
    ' '))
  endfunc
  func oSELEGI_1_20.GetRadio()
    this.Parent.oContained.w_SELEGI = this.RadioValue()
    return .t.
  endfunc

  func oSELEGI_1_20.SetRadio()
    this.Parent.oContained.w_SELEGI=trim(this.Parent.oContained.w_SELEGI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEGI=='GI',1,;
      0)
  endfunc

  func oSELEGI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEGI<>"|")
    endwith
   endif
  endfunc

  add object oSELECC_1_21 as StdCheck with uid="BVAYFXCROB",rtseq=18,rtrep=.f.,left=11, top=245, caption="Causali contabili",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7320794,;
    cFormVar="w_SELECC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECC_1_21.RadioValue()
    return(iif(this.value =1,'CC',;
    ' '))
  endfunc
  func oSELECC_1_21.GetRadio()
    this.Parent.oContained.w_SELECC = this.RadioValue()
    return .t.
  endfunc

  func oSELECC_1_21.SetRadio()
    this.Parent.oContained.w_SELECC=trim(this.Parent.oContained.w_SELECC)
    this.value = ;
      iif(this.Parent.oContained.w_SELECC=='CC',1,;
      0)
  endfunc

  func oSELECC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECC<>"|")
    endwith
   endif
  endfunc

  add object oSELECR_1_22 as StdCheck with uid="PERRYSQHNB",rtseq=19,rtrep=.f.,left=11, top=264, caption="Categorie cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 24098010,;
    cFormVar="w_SELECR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECR_1_22.RadioValue()
    return(iif(this.value =1,'CR',;
    ' '))
  endfunc
  func oSELECR_1_22.GetRadio()
    this.Parent.oContained.w_SELECR = this.RadioValue()
    return .t.
  endfunc

  func oSELECR_1_22.SetRadio()
    this.Parent.oContained.w_SELECR=trim(this.Parent.oContained.w_SELECR)
    this.value = ;
      iif(this.Parent.oContained.w_SELECR=='CR',1,;
      0)
  endfunc

  func oSELECR_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECR<>"|")
    endwith
   endif
  endfunc

  add object oSELECA_1_23 as StdCheck with uid="VXUSEGRUOQ",rtseq=20,rtrep=.f.,left=11, top=283, caption="Categorie commerciali",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 40875226,;
    cFormVar="w_SELECA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECA_1_23.RadioValue()
    return(iif(this.value =1,'CA',;
    ' '))
  endfunc
  func oSELECA_1_23.GetRadio()
    this.Parent.oContained.w_SELECA = this.RadioValue()
    return .t.
  endfunc

  func oSELECA_1_23.SetRadio()
    this.Parent.oContained.w_SELECA=trim(this.Parent.oContained.w_SELECA)
    this.value = ;
      iif(this.Parent.oContained.w_SELECA=='CA',1,;
      0)
  endfunc

  func oSELECA_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECA<>"|")
    endwith
   endif
  endfunc

  add object oSELECL_1_24 as StdCheck with uid="YSMGJBRZRG",rtseq=21,rtrep=.f.,left=11, top=302, caption="Categorie contabili cli/for",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 124761306,;
    cFormVar="w_SELECL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECL_1_24.RadioValue()
    return(iif(this.value =1,'CL',;
    ' '))
  endfunc
  func oSELECL_1_24.GetRadio()
    this.Parent.oContained.w_SELECL = this.RadioValue()
    return .t.
  endfunc

  func oSELECL_1_24.SetRadio()
    this.Parent.oContained.w_SELECL=trim(this.Parent.oContained.w_SELECL)
    this.value = ;
      iif(this.Parent.oContained.w_SELECL=='CL',1,;
      0)
  endfunc

  func oSELECL_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECL<>"|")
    endwith
   endif
  endfunc

  add object oSELECF_1_25 as StdCheck with uid="PDNDFOGEJJ",rtseq=22,rtrep=.f.,left=11, top=321, caption="Clienti e fornitori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 225424602,;
    cFormVar="w_SELECF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECF_1_25.RadioValue()
    return(iif(this.value =1,'CF',;
    ' '))
  endfunc
  func oSELECF_1_25.GetRadio()
    this.Parent.oContained.w_SELECF = this.RadioValue()
    return .t.
  endfunc

  func oSELECF_1_25.SetRadio()
    this.Parent.oContained.w_SELECF=trim(this.Parent.oContained.w_SELECF)
    this.value = ;
      iif(this.Parent.oContained.w_SELECF=='CF',1,;
      0)
  endfunc

  func oSELECF_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECF<>"|")
    endwith
   endif
  endfunc

  add object oSELEIV_1_26 as StdCheck with uid="YWBRQQXDEN",rtseq=23,rtrep=.f.,left=11, top=340, caption="Codici IVA",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 219133146,;
    cFormVar="w_SELEIV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIV_1_26.RadioValue()
    return(iif(this.value =1,'IV',;
    ' '))
  endfunc
  func oSELEIV_1_26.GetRadio()
    this.Parent.oContained.w_SELEIV = this.RadioValue()
    return .t.
  endfunc

  func oSELEIV_1_26.SetRadio()
    this.Parent.oContained.w_SELEIV=trim(this.Parent.oContained.w_SELEIV)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIV=='IV',1,;
      0)
  endfunc

  func oSELEIV_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIV<>"|")
    endwith
   endif
  endfunc

  add object oSELECH_1_27 as StdCheck with uid="QMZWMCOFQM",rtseq=24,rtrep=.f.,left=11, top=359, caption="Conti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 191870170,;
    cFormVar="w_SELECH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECH_1_27.RadioValue()
    return(iif(this.value =1,'CH',;
    ' '))
  endfunc
  func oSELECH_1_27.GetRadio()
    this.Parent.oContained.w_SELECH = this.RadioValue()
    return .t.
  endfunc

  func oSELECH_1_27.SetRadio()
    this.Parent.oContained.w_SELECH=trim(this.Parent.oContained.w_SELECH)
    this.value = ;
      iif(this.Parent.oContained.w_SELECH=='CH',1,;
      0)
  endfunc

  func oSELECH_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECH<>"|")
    endwith
   endif
  endfunc

  add object oSELECB_1_28 as StdCheck with uid="DNJCPWRAQJ",rtseq=25,rtrep=.f.,left=11, top=378, caption="Conti banche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 24098010,;
    cFormVar="w_SELECB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECB_1_28.RadioValue()
    return(iif(this.value =1,'CB',;
    ' '))
  endfunc
  func oSELECB_1_28.GetRadio()
    this.Parent.oContained.w_SELECB = this.RadioValue()
    return .t.
  endfunc

  func oSELECB_1_28.SetRadio()
    this.Parent.oContained.w_SELECB=trim(this.Parent.oContained.w_SELECB)
    this.value = ;
      iif(this.Parent.oContained.w_SELECB=='CB',1,;
      0)
  endfunc

  func oSELECB_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECB<>"|")
    endwith
   endif
  endfunc

  add object oSELEDE_1_29 as StdCheck with uid="AZZQJONGNI",rtseq=26,rtrep=.f.,left=11, top=397, caption="Destinazioni diverse",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 241153242,;
    cFormVar="w_SELEDE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDE_1_29.RadioValue()
    return(iif(this.value =1,'DE',;
    ' '))
  endfunc
  func oSELEDE_1_29.GetRadio()
    this.Parent.oContained.w_SELEDE = this.RadioValue()
    return .t.
  endfunc

  func oSELEDE_1_29.SetRadio()
    this.Parent.oContained.w_SELEDE=trim(this.Parent.oContained.w_SELEDE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDE=='DE',1,;
      0)
  endfunc

  func oSELEDE_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDE<>"|")
    endwith
   endif
  endfunc

  add object oSELELI_1_30 as StdCheck with uid="KKRFRXVJBN",rtseq=27,rtrep=.f.,left=244, top=112, caption="Lingue",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 165655770,;
    cFormVar="w_SELELI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELELI_1_30.RadioValue()
    return(iif(this.value =1,'LI',;
    ' '))
  endfunc
  func oSELELI_1_30.GetRadio()
    this.Parent.oContained.w_SELELI = this.RadioValue()
    return .t.
  endfunc

  func oSELELI_1_30.SetRadio()
    this.Parent.oContained.w_SELELI=trim(this.Parent.oContained.w_SELELI)
    this.value = ;
      iif(this.Parent.oContained.w_SELELI=='LI',1,;
      0)
  endfunc

  func oSELELI_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELELI<>"|")
    endwith
   endif
  endfunc

  add object oSELEMS_1_31 as StdCheck with uid="MEUDIZCRQB",rtseq=28,rtrep=.f.,left=244, top=131, caption="Mastri",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 265270490,;
    cFormVar="w_SELEMS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEMS_1_31.RadioValue()
    return(iif(this.value =1,'MS',;
    ' '))
  endfunc
  func oSELEMS_1_31.GetRadio()
    this.Parent.oContained.w_SELEMS = this.RadioValue()
    return .t.
  endfunc

  func oSELEMS_1_31.SetRadio()
    this.Parent.oContained.w_SELEMS=trim(this.Parent.oContained.w_SELEMS)
    this.value = ;
      iif(this.Parent.oContained.w_SELEMS=='MS',1,;
      0)
  endfunc

  func oSELEMS_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEMS<>"|")
    endwith
   endif
  endfunc

  add object oSELENA_1_32 as StdCheck with uid="RROMUCHGJY",rtseq=29,rtrep=.f.,left=244, top=150, caption="Nazioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 29340890,;
    cFormVar="w_SELENA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENA_1_32.RadioValue()
    return(iif(this.value =1,'NA',;
    ' '))
  endfunc
  func oSELENA_1_32.GetRadio()
    this.Parent.oContained.w_SELENA = this.RadioValue()
    return .t.
  endfunc

  func oSELENA_1_32.SetRadio()
    this.Parent.oContained.w_SELENA=trim(this.Parent.oContained.w_SELENA)
    this.value = ;
      iif(this.Parent.oContained.w_SELENA=='NA',1,;
      0)
  endfunc

  func oSELENA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENA<>"|")
    endwith
   endif
  endfunc

  add object oSELENM_1_33 as StdCheck with uid="YXEDLPFGCE",rtseq=30,rtrep=.f.,left=244, top=169, caption="Nominativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 96449754,;
    cFormVar="w_SELENM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENM_1_33.RadioValue()
    return(iif(this.value =1,'NM',;
    ' '))
  endfunc
  func oSELENM_1_33.GetRadio()
    this.Parent.oContained.w_SELENM = this.RadioValue()
    return .t.
  endfunc

  func oSELENM_1_33.SetRadio()
    this.Parent.oContained.w_SELENM=trim(this.Parent.oContained.w_SELENM)
    this.value = ;
      iif(this.Parent.oContained.w_SELENM=='NM',1,;
      0)
  endfunc

  func oSELENM_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENM<>"|")
    endwith
   endif
  endfunc

  add object oSELEPA_1_34 as StdCheck with uid="GCPRGDIHEQ",rtseq=31,rtrep=.f.,left=244, top=188, caption="Pagamenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 27243738,;
    cFormVar="w_SELEPA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPA_1_34.RadioValue()
    return(iif(this.value =1,'PA',;
    ' '))
  endfunc
  func oSELEPA_1_34.GetRadio()
    this.Parent.oContained.w_SELEPA = this.RadioValue()
    return .t.
  endfunc

  func oSELEPA_1_34.SetRadio()
    this.Parent.oContained.w_SELEPA=trim(this.Parent.oContained.w_SELEPA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPA=='PA',1,;
      0)
  endfunc

  func oSELEPA_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPA<>"|")
    endwith
   endif
  endfunc

  add object oSELEPD_1_35 as StdCheck with uid="WUQVZUVXWH",rtseq=32,rtrep=.f.,left=244, top=207, caption="Pagamenti dettaglio",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 245347546,;
    cFormVar="w_SELEPD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPD_1_35.RadioValue()
    return(iif(this.value =1,'PD',;
    ' '))
  endfunc
  func oSELEPD_1_35.GetRadio()
    this.Parent.oContained.w_SELEPD = this.RadioValue()
    return .t.
  endfunc

  func oSELEPD_1_35.SetRadio()
    this.Parent.oContained.w_SELEPD=trim(this.Parent.oContained.w_SELEPD)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPD=='PD',1,;
      0)
  endfunc

  func oSELEPD_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPD<>"|")
    endwith
   endif
  endfunc

  add object oSELEPC_1_36 as StdCheck with uid="GPSEFEULGX",rtseq=33,rtrep=.f.,left=244, top=226, caption="Piano dei conti / mastri e conti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 262124762,;
    cFormVar="w_SELEPC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPC_1_36.RadioValue()
    return(iif(this.value =1,'PC',;
    ' '))
  endfunc
  func oSELEPC_1_36.GetRadio()
    this.Parent.oContained.w_SELEPC = this.RadioValue()
    return .t.
  endfunc

  func oSELEPC_1_36.SetRadio()
    this.Parent.oContained.w_SELEPC=trim(this.Parent.oContained.w_SELEPC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPC=='PC',1,;
      0)
  endfunc

  func oSELEPC_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPC<>"|")
    endwith
   endif
  endfunc

  add object oSELEPO_1_37 as StdCheck with uid="XCKRQBSHJO",rtseq=34,rtrep=.f.,left=244, top=245, caption="Porti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 60798170,;
    cFormVar="w_SELEPO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPO_1_37.RadioValue()
    return(iif(this.value =1,'PO',;
    ' '))
  endfunc
  func oSELEPO_1_37.GetRadio()
    this.Parent.oContained.w_SELEPO = this.RadioValue()
    return .t.
  endfunc

  func oSELEPO_1_37.SetRadio()
    this.Parent.oContained.w_SELEPO=trim(this.Parent.oContained.w_SELEPO)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPO=='PO',1,;
      0)
  endfunc

  func oSELEPO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPO<>"|")
    endwith
   endif
  endfunc

  add object oSELESC_1_38 as StdCheck with uid="AYNTNLQPOQ",rtseq=35,rtrep=.f.,left=244, top=283, caption="Saldi contabili",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 258979034,;
    cFormVar="w_SELESC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESC_1_38.RadioValue()
    return(iif(this.value =1,'SC',;
    ' '))
  endfunc
  func oSELESC_1_38.GetRadio()
    this.Parent.oContained.w_SELESC = this.RadioValue()
    return .t.
  endfunc

  func oSELESC_1_38.SetRadio()
    this.Parent.oContained.w_SELESC=trim(this.Parent.oContained.w_SELESC)
    this.value = ;
      iif(this.Parent.oContained.w_SELESC=='SC',1,;
      0)
  endfunc

  func oSELESC_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESC<>"|")
    endwith
   endif
  endfunc

  add object oSELERF_1_39 as StdCheck with uid="XBAAOQHLDR",rtseq=36,rtrep=.f.,left=244, top=264, caption="Riferimenti cli/for",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 209695962,;
    cFormVar="w_SELERF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERF_1_39.RadioValue()
    return(iif(this.value =1,'RF',;
    ' '))
  endfunc
  func oSELERF_1_39.GetRadio()
    this.Parent.oContained.w_SELERF = this.RadioValue()
    return .t.
  endfunc

  func oSELERF_1_39.SetRadio()
    this.Parent.oContained.w_SELERF=trim(this.Parent.oContained.w_SELERF)
    this.value = ;
      iif(this.Parent.oContained.w_SELERF=='RF',1,;
      0)
  endfunc

  func oSELERF_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERF<>"|")
    endwith
   endif
  endfunc

  add object oSELECS_1_40 as StdCheck with uid="OPYLKZCWCP",rtseq=37,rtrep=.f.,left=244, top=302, caption="Saldi cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7320794,;
    cFormVar="w_SELECS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECS_1_40.RadioValue()
    return(iif(this.value =1,'CS',;
    ' '))
  endfunc
  func oSELECS_1_40.GetRadio()
    this.Parent.oContained.w_SELECS = this.RadioValue()
    return .t.
  endfunc

  func oSELECS_1_40.SetRadio()
    this.Parent.oContained.w_SELECS=trim(this.Parent.oContained.w_SELECS)
    this.value = ;
      iif(this.Parent.oContained.w_SELECS=='CS',1,;
      0)
  endfunc

  func oSELECS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECS<>"|")
    endwith
   endif
  endfunc

  add object oSELESP_1_41 as StdCheck with uid="LXBNLBGIZL",rtseq=38,rtrep=.f.,left=244, top=321, caption="Spedizioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 40875226,;
    cFormVar="w_SELESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESP_1_41.RadioValue()
    return(iif(this.value =1,'SP',;
    ' '))
  endfunc
  func oSELESP_1_41.GetRadio()
    this.Parent.oContained.w_SELESP = this.RadioValue()
    return .t.
  endfunc

  func oSELESP_1_41.SetRadio()
    this.Parent.oContained.w_SELESP=trim(this.Parent.oContained.w_SELESP)
    this.value = ;
      iif(this.Parent.oContained.w_SELESP=='SP',1,;
      0)
  endfunc

  func oSELESP_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESP<>"|")
    endwith
   endif
  endfunc

  add object oSELETR_1_42 as StdCheck with uid="TPXLNUCXEQ",rtseq=39,rtrep=.f.,left=244, top=340, caption="Trascodifiche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 6272218,;
    cFormVar="w_SELETR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETR_1_42.RadioValue()
    return(iif(this.value =1,'TR',;
    ' '))
  endfunc
  func oSELETR_1_42.GetRadio()
    this.Parent.oContained.w_SELETR = this.RadioValue()
    return .t.
  endfunc

  func oSELETR_1_42.SetRadio()
    this.Parent.oContained.w_SELETR=trim(this.Parent.oContained.w_SELETR)
    this.value = ;
      iif(this.Parent.oContained.w_SELETR=='TR',1,;
      0)
  endfunc

  func oSELETR_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETR<>"|")
    endwith
   endif
  endfunc

  add object oSELEVA_1_43 as StdCheck with uid="VLSCNCXOLM",rtseq=40,rtrep=.f.,left=244, top=359, caption="Valute",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 20952282,;
    cFormVar="w_SELEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEVA_1_43.RadioValue()
    return(iif(this.value =1,'VA',;
    ' '))
  endfunc
  func oSELEVA_1_43.GetRadio()
    this.Parent.oContained.w_SELEVA = this.RadioValue()
    return .t.
  endfunc

  func oSELEVA_1_43.SetRadio()
    this.Parent.oContained.w_SELEVA=trim(this.Parent.oContained.w_SELEVA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVA=='VA',1,;
      0)
  endfunc

  func oSELEVA_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEVA<>"|")
    endwith
   endif
  endfunc

  add object oSELEVE_1_44 as StdCheck with uid="QBTVOHDQER",rtseq=41,rtrep=.f.,left=244, top=378, caption="Vettori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 222278874,;
    cFormVar="w_SELEVE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEVE_1_44.RadioValue()
    return(iif(this.value =1,'VE',;
    ' '))
  endfunc
  func oSELEVE_1_44.GetRadio()
    this.Parent.oContained.w_SELEVE = this.RadioValue()
    return .t.
  endfunc

  func oSELEVE_1_44.SetRadio()
    this.Parent.oContained.w_SELEVE=trim(this.Parent.oContained.w_SELEVE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVE=='VE',1,;
      0)
  endfunc

  func oSELEVE_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEVE<>"|")
    endwith
   endif
  endfunc

  add object oSELEZO_1_45 as StdCheck with uid="XCJXMXZXDA",rtseq=42,rtrep=.f.,left=244, top=397, caption="Zone",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 50312410,;
    cFormVar="w_SELEZO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEZO_1_45.RadioValue()
    return(iif(this.value =1,'ZO',;
    ' '))
  endfunc
  func oSELEZO_1_45.GetRadio()
    this.Parent.oContained.w_SELEZO = this.RadioValue()
    return .t.
  endfunc

  func oSELEZO_1_45.SetRadio()
    this.Parent.oContained.w_SELEZO=trim(this.Parent.oContained.w_SELEZO)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZO=='ZO',1,;
      0)
  endfunc

  func oSELEZO_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEZO<>"|")
    endwith
   endif
  endfunc

  add object oRADSELEZ_1_46 as StdRadio with uid="JTFLDCVNJL",rtseq=43,rtrep=.f.,left=355, top=423, width=263,height=23;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELEZ_1_46.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 146655088
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 146655088
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ_1_46.GetRadio()
    this.Parent.oContained.w_RADSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ_1_46.SetRadio()
    this.Parent.oContained.w_RADSELEZ=trim(this.Parent.oContained.w_RADSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ=='D',2,;
      0))
  endfunc

  func oRADSELEZ_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODIMP) and !.w_INCORSO)
    endwith
   endif
  endfunc

  add object oSELECONT_1_47 as StdCheck with uid="HJVITWNLMM",rtseq=44,rtrep=.f.,left=19, top=444, caption="Controllo valori",;
    ToolTipText = "Se attivato consente di controllare i valori assegnati ai singoli campi",;
    HelpContextID = 74429574,;
    cFormVar="w_SELECONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECONT_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELECONT_1_47.GetRadio()
    this.Parent.oContained.w_SELECONT = this.RadioValue()
    return .t.
  endfunc

  func oSELECONT_1_47.SetRadio()
    this.Parent.oContained.w_SELECONT=trim(this.Parent.oContained.w_SELECONT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECONT=='S',1,;
      0)
  endfunc

  func oSELECONT_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc


  add object oSELERESO_1_48 as StdCombo with uid="VYMIVBYKOS",rtseq=45,rtrep=.f.,left=19,top=467,width=133,height=21;
    , ToolTipText = "Se attivato genera il resoconto delle operazioni di import";
    , HelpContextID = 41962357;
    , cFormVar="w_SELERESO",RowSource=""+"Errori+segnalazioni,"+"Errori+stop dopo 10,"+"Errori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELERESO_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    'N')))))
  endfunc
  func oSELERESO_1_48.GetRadio()
    this.Parent.oContained.w_SELERESO = this.RadioValue()
    return .t.
  endfunc

  func oSELERESO_1_48.SetRadio()
    this.Parent.oContained.w_SELERESO=trim(this.Parent.oContained.w_SELERESO)
    this.value = ;
      iif(this.Parent.oContained.w_SELERESO=='S',1,;
      iif(this.Parent.oContained.w_SELERESO=='D',2,;
      iif(this.Parent.oContained.w_SELERESO=='E',3,;
      iif(this.Parent.oContained.w_SELERESO=='N',4,;
      0))))
  endfunc

  func oSELERESO_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMPARIVA_1_49 as StdCheck with uid="AWRYUUTVDF",rtseq=46,rtrep=.f.,left=205, top=444, caption="Controllo partita IVA",;
    ToolTipText = "Se attivato consente il riconoscimento dei clienti/fornitori per partita IVA",;
    HelpContextID = 108827335,;
    cFormVar="w_IMPARIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMPARIVA_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMPARIVA_1_49.GetRadio()
    this.Parent.oContained.w_IMPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oIMPARIVA_1_49.SetRadio()
    this.Parent.oContained.w_IMPARIVA=trim(this.Parent.oContained.w_IMPARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IMPARIVA=='S',1,;
      0)
  endfunc

  func oIMPARIVA_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMAZZERA_1_50 as StdCheck with uid="HMAFHBTQIQ",rtseq=47,rtrep=.f.,left=205, top=460, caption="Azzera movimenti",;
    ToolTipText = "Se attivato la procedura elimina le movimentazioni prima di importarle",;
    HelpContextID = 51684039,;
    cFormVar="w_IMAZZERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAZZERA_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAZZERA_1_50.GetRadio()
    this.Parent.oContained.w_IMAZZERA = this.RadioValue()
    return .t.
  endfunc

  func oIMAZZERA_1_50.SetRadio()
    this.Parent.oContained.w_IMAZZERA=trim(this.Parent.oContained.w_IMAZZERA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAZZERA=='S',1,;
      0)
  endfunc

  func oIMAZZERA_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc


  add object oObj_1_55 as cp_runprogram with uid="IXGAHCLBMS",left=291, top=532, width=344,height=19,;
    caption='GSIM_BSE(1)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSE(1)",;
    cEvent = "w_RADSELEZ Changed, w_CODIMP Changed",;
    nPag=1;
    , HelpContextID = 5958699

  add object oIMTRANSA_1_59 as StdCheck with uid="GPDSMVITBT",rtseq=48,rtrep=.f.,left=205, top=476, caption="Sotto transazione",;
    ToolTipText = "Se attivato la procedura viene eseguita sotto un'unica transazione",;
    HelpContextID = 176018119,;
    cFormVar="w_IMTRANSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMTRANSA_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMTRANSA_1_59.GetRadio()
    this.Parent.oContained.w_IMTRANSA = this.RadioValue()
    return .t.
  endfunc

  func oIMTRANSA_1_59.SetRadio()
    this.Parent.oContained.w_IMTRANSA=trim(this.Parent.oContained.w_IMTRANSA)
    this.value = ;
      iif(this.Parent.oContained.w_IMTRANSA=='S',1,;
      0)
  endfunc

  func oIMTRANSA_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMCONFER_1_60 as StdCheck with uid="RVMTDVMTAB",rtseq=49,rtrep=.f.,left=205, top=492, caption="Richiesta conferma",;
    ToolTipText = "Se attivato viene richiesto la conferma prima di aggiornare il database (valido solo se attivo sotto transazione)",;
    HelpContextID = 55165656,;
    cFormVar="w_IMCONFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMCONFER_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMCONFER_1_60.GetRadio()
    this.Parent.oContained.w_IMCONFER = this.RadioValue()
    return .t.
  endfunc

  func oIMCONFER_1_60.SetRadio()
    this.Parent.oContained.w_IMCONFER=trim(this.Parent.oContained.w_IMCONFER)
    this.value = ;
      iif(this.Parent.oContained.w_IMCONFER=='S',1,;
      0)
  endfunc

  func oIMCONFER_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTRANSA='S' and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oIMCONFER_1_60.mHide()
    with this.Parent.oContained
      return (.w_IMTRANSA<>'S')
    endwith
  endfunc


  add object oBtn_1_62 as StdButton with uid="VQERAGEWET",left=517, top=462, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 132730650;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        do GSIM_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_62.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO and (not empty(.w_CODIMP)) and (.w_TIPSRC<>'O' or !empty(.w_ODBCDSN))                          and g_IMPO='S')
      endwith
    endif
  endfunc


  add object oBtn_1_63 as StdButton with uid="MRPTSAHHIT",left=572, top=462, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 125441978;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oBtn_1_67 as StdButton with uid="IXBUJQRYLM",left=388, top=462, width=48,height=45,;
    CpPicture="BMP\info.bmp", caption="", nPag=1;
    , HelpContextID = 9212842;
    , caption='\<Info';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      do gsim_kai with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc

  add object oSELECE_1_68 as StdCheck with uid="QJJHUALGEO",rtseq=50,rtrep=.f.,left=450, top=112, caption="Centri costo/ricavo",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 242201818,;
    cFormVar="w_SELECE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECE_1_68.RadioValue()
    return(iif(this.value =1,'CE',;
    ' '))
  endfunc
  func oSELECE_1_68.GetRadio()
    this.Parent.oContained.w_SELECE = this.RadioValue()
    return .t.
  endfunc

  func oSELECE_1_68.SetRadio()
    this.Parent.oContained.w_SELECE=trim(this.Parent.oContained.w_SELECE)
    this.value = ;
      iif(this.Parent.oContained.w_SELECE=='CE',1,;
      0)
  endfunc

  func oSELECE_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECE<>"|")
    endwith
   endif
  endfunc

  add object oSELECO_1_69 as StdCheck with uid="ZDLWWTCCIA",rtseq=51,rtrep=.f.,left=450, top=131, caption="Commesse",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 74429658,;
    cFormVar="w_SELECO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECO_1_69.RadioValue()
    return(iif(this.value =1,'CO',;
    ' '))
  endfunc
  func oSELECO_1_69.GetRadio()
    this.Parent.oContained.w_SELECO = this.RadioValue()
    return .t.
  endfunc

  func oSELECO_1_69.SetRadio()
    this.Parent.oContained.w_SELECO=trim(this.Parent.oContained.w_SELECO)
    this.value = ;
      iif(this.Parent.oContained.w_SELECO=='CO',1,;
      0)
  endfunc

  func oSELECO_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECO<>"|")
    endwith
   endif
  endfunc

  add object oSELEMC_1_70 as StdCheck with uid="XFQVYAIOVR",rtseq=52,rtrep=.f.,left=450, top=150, caption="Movimenti analitica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 265270490,;
    cFormVar="w_SELEMC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEMC_1_70.RadioValue()
    return(iif(this.value =1,'MC',;
    ' '))
  endfunc
  func oSELEMC_1_70.GetRadio()
    this.Parent.oContained.w_SELEMC = this.RadioValue()
    return .t.
  endfunc

  func oSELEMC_1_70.SetRadio()
    this.Parent.oContained.w_SELEMC=trim(this.Parent.oContained.w_SELEMC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEMC=='MC',1,;
      0)
  endfunc

  func oSELEMC_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEMC<>"|")
    endwith
   endif
  endfunc

  add object oSELEPT_1_71 as StdCheck with uid="QBDHZRLWAW",rtseq=53,rtrep=.f.,left=450, top=169, caption="Partite",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 245347546,;
    cFormVar="w_SELEPT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPT_1_71.RadioValue()
    return(iif(this.value =1,'PT',;
    ' '))
  endfunc
  func oSELEPT_1_71.GetRadio()
    this.Parent.oContained.w_SELEPT = this.RadioValue()
    return .t.
  endfunc

  func oSELEPT_1_71.SetRadio()
    this.Parent.oContained.w_SELEPT=trim(this.Parent.oContained.w_SELEPT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPT=='PT',1,;
      0)
  endfunc

  func oSELEPT_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPT<>"|")
    endwith
   endif
  endfunc

  add object oSELEPN_1_72 as StdCheck with uid="JOTPRCUSRR",rtseq=54,rtrep=.f.,left=450, top=188, caption="Primanota",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 77575386,;
    cFormVar="w_SELEPN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPN_1_72.RadioValue()
    return(iif(this.value =1,'PN',;
    ' '))
  endfunc
  func oSELEPN_1_72.GetRadio()
    this.Parent.oContained.w_SELEPN = this.RadioValue()
    return .t.
  endfunc

  func oSELEPN_1_72.SetRadio()
    this.Parent.oContained.w_SELEPN=trim(this.Parent.oContained.w_SELEPN)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPN=='PN',1,;
      0)
  endfunc

  func oSELEPN_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPN<>"|")
    endwith
   endif
  endfunc

  add object oSELEPI_1_73 as StdCheck with uid="ALNQQVANGU",rtseq=55,rtrep=.f.,left=450, top=207, caption="Registrazioni IVA",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 161461466,;
    cFormVar="w_SELEPI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPI_1_73.RadioValue()
    return(iif(this.value =1,'PI',;
    ' '))
  endfunc
  func oSELEPI_1_73.GetRadio()
    this.Parent.oContained.w_SELEPI = this.RadioValue()
    return .t.
  endfunc

  func oSELEPI_1_73.SetRadio()
    this.Parent.oContained.w_SELEPI=trim(this.Parent.oContained.w_SELEPI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPI=='PI',1,;
      0)
  endfunc

  func oSELEPI_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPI<>"|")
    endwith
   endif
  endfunc

  add object oSELEVC_1_74 as StdCheck with uid="IFFYWKGXDM",rtseq=56,rtrep=.f.,left=450, top=226, caption="Voci costo/ricavo",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 255833306,;
    cFormVar="w_SELEVC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEVC_1_74.RadioValue()
    return(iif(this.value =1,'VC',;
    ' '))
  endfunc
  func oSELEVC_1_74.GetRadio()
    this.Parent.oContained.w_SELEVC = this.RadioValue()
    return .t.
  endfunc

  func oSELEVC_1_74.SetRadio()
    this.Parent.oContained.w_SELEVC=trim(this.Parent.oContained.w_SELEVC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVC=='VC',1,;
      0)
  endfunc

  func oSELEVC_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEVC<>"|")
    endwith
   endif
  endfunc


  add object oObj_1_80 as cp_runprogram with uid="SCCPNNHPBM",left=3, top=532, width=256,height=19,;
    caption='GSIM_BSE(0)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSE(0)",;
    cEvent = "w_CODIMP Changed,Blank",;
    nPag=1;
    , HelpContextID = 5958443

  add object oStr_1_7 as StdString with uid="TULOXBETIP",Visible=.t., Left=51, Top=67,;
    Alignment=1, Width=50, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="HNFSNYQVVY",Visible=.t., Left=15, Top=13,;
    Alignment=1, Width=84, Height=15,;
    Caption="Tracciati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="TXLTEBBQSG",Visible=.t., Left=15, Top=423,;
    Alignment=0, Width=132, Height=15,;
    Caption="Verifiche e resoconti"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="QUCHHTNQRF",Visible=.t., Left=4, Top=93,;
    Alignment=0, Width=345, Height=15,;
    Caption="Selezione archivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="SQXRTSXXPT",Visible=.t., Left=202, Top=423,;
    Alignment=0, Width=124, Height=15,;
    Caption="Opzioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="FTEJVJHPFP",Visible=.t., Left=21, Top=44,;
    Alignment=1, Width=78, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="UXUMQQXWYB",Visible=.t., Left=11, Top=44,;
    Alignment=1, Width=88, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="QQMMTLMXER",Visible=.t., Left=338, Top=44,;
    Alignment=1, Width=53, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="KUWPABNYKM",Visible=.t., Left=325, Top=68,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="BRDYMEXVNR",Visible=.t., Left=441, Top=93,;
    Alignment=0, Width=184, Height=15,;
    Caption="Selezione movimenti"  ;
  , bGlobalFont=.t.

  add object oBox_1_54 as StdBox with uid="LZRSZSRDKV",left=12, top=440, width=127,height=1

  add object oBox_1_56 as StdBox with uid="VNBLQXCGAX",left=4, top=107, width=439,height=310

  add object oBox_1_58 as StdBox with uid="UECDJXNURS",left=198, top=440, width=127,height=1

  add object oBox_1_75 as StdBox with uid="CMOBWQCVBO",left=441, top=107, width=184,height=310
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
