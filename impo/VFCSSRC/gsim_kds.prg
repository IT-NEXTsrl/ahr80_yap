* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kds                                                        *
*              Duplicazione tracciati                                          *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_6]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-21                                                      *
* Last revis.: 2009-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kds",oParentObject))

* --- Class definition
define class tgsim_kds as StdForm
  Top    = 50
  Left   = 52

  * --- Standard Properties
  Width  = 686
  Height = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-29"
  HelpContextID=82427753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  IMPOARCH_IDX = 0
  cPrg = "gsim_kds"
  cComment = "Duplicazione tracciati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODIMP = space(20)
  w_DESIMP = space(50)
  w_ARCINI = space(2)
  w_ARDESINI = space(20)
  w_ARDESFIN = space(20)
  w_ARCFIN = space(2)
  w_CODIMPN = space(20)
  w_DESIMPN = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kdsPag1","gsim_kds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODIMP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='IMPOARCH'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODIMP=space(20)
      .w_DESIMP=space(50)
      .w_ARCINI=space(2)
      .w_ARDESINI=space(20)
      .w_ARDESFIN=space(20)
      .w_ARCFIN=space(2)
      .w_CODIMPN=space(20)
      .w_DESIMPN=space(0)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODIMP))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_ARCINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_ARCFIN))
          .link_1_6('Full')
        endif
    endwith
    this.DoRTCalc(7,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_1'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_KDS',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_ARCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_ARCINI))
          select ARCODICE,ARDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCINI)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCINI) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oARCINI_1_3'),i_cWhere,'GSIM_KDS',"Elenco destinazioni",'destin.IMPOARCH_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_ARCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_ARCINI)
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCINI = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESINI = NVL(_Link_.ARDESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ARCINI = space(2)
      endif
      this.w_ARDESINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ARCFIN) or (.w_ARCFIN>=.w_ARCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di inizio selezione > del codice di fine selezione")
        endif
        this.w_ARCINI = space(2)
        this.w_ARDESINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_KDS',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_ARCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_ARCFIN))
          select ARCODICE,ARDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCFIN)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCFIN) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oARCFIN_1_6'),i_cWhere,'GSIM_KDS',"Elenco destinazioni",'destin.IMPOARCH_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_ARCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_ARCFIN)
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCFIN = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESFIN = NVL(_Link_.ARDESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ARCFIN = space(2)
      endif
      this.w_ARDESFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_ARCINI) or (.w_ARCFIN>=.w_ARCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di inizio selezione > del codice di fine selezione")
        endif
        this.w_ARCFIN = space(2)
        this.w_ARDESFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_1.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_1.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_2.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_2.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oARCINI_1_3.value==this.w_ARCINI)
      this.oPgFrm.Page1.oPag.oARCINI_1_3.value=this.w_ARCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESINI_1_4.value==this.w_ARDESINI)
      this.oPgFrm.Page1.oPag.oARDESINI_1_4.value=this.w_ARDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESFIN_1_5.value==this.w_ARDESFIN)
      this.oPgFrm.Page1.oPag.oARDESFIN_1_5.value=this.w_ARDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARCFIN_1_6.value==this.w_ARCFIN)
      this.oPgFrm.Page1.oPag.oARCFIN_1_6.value=this.w_ARCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIMPN_1_7.value==this.w_CODIMPN)
      this.oPgFrm.Page1.oPag.oCODIMPN_1_7.value=this.w_CODIMPN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMPN_1_8.value==this.w_DESIMPN)
      this.oPgFrm.Page1.oPag.oDESIMPN_1_8.value=this.w_DESIMPN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_ARCFIN) or (.w_ARCFIN>=.w_ARCINI))  and not(empty(.w_ARCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di inizio selezione > del codice di fine selezione")
          case   not(empty(.w_ARCINI) or (.w_ARCFIN>=.w_ARCINI))  and not(empty(.w_ARCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di inizio selezione > del codice di fine selezione")
          case   (empty(.w_CODIMPN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMPN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CODIMPN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DESIMPN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESIMPN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DESIMPN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsim_kdsPag1 as StdContainer
  Width  = 682
  height = 170
  stdWidth  = 682
  stdheight = 170
  resizeXpos=467
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODIMP_1_1 as StdField with uid="OIDXSQIERN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Specificare il codice dei tracciato da duplicare",;
    HelpContextID = 265038810,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=151, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oDESIMP_1_2 as StdField with uid="GBRKTWXQIZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 264979914,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=314, Top=12, InputMask=replicate('X',50)

  add object oARCINI_1_3 as StdField with uid="VHGRQLRKAG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ARCINI", cQueryName = "ARCINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di inizio selezione > del codice di fine selezione",;
    ToolTipText = "Codice archivio di inizio selezione",;
    HelpContextID = 112998650,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=151, Top=39, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_KDS", oKey_1_1="ARCODICE", oKey_1_2="this.w_ARCINI"

  func oARCINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oARCINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_KDS',"Elenco destinazioni",'destin.IMPOARCH_VZM',this.parent.oContained
  endproc
  proc oARCINI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSIM_KDS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_ARCINI
     i_obj.ecpSave()
  endproc

  add object oARDESINI_1_4 as StdField with uid="CPTARDIWBM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ARDESINI", cQueryName = "ARDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 108013745,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=193, Top=39, InputMask=replicate('X',20)

  add object oARDESFIN_1_5 as StdField with uid="WVUMCYEPEH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARDESFIN", cQueryName = "ARDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 110090068,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=193, Top=63, InputMask=replicate('X',20)

  add object oARCFIN_1_6 as StdField with uid="VVQZJVZVOO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARCFIN", cQueryName = "ARCFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di inizio selezione > del codice di fine selezione",;
    ToolTipText = "Codice archivio di fine selezione",;
    HelpContextID = 34552058,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=151, Top=63, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_KDS", oKey_1_1="ARCODICE", oKey_1_2="this.w_ARCFIN"

  func oARCFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oARCFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_KDS',"Elenco destinazioni",'destin.IMPOARCH_VZM',this.parent.oContained
  endproc
  proc oARCFIN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSIM_KDS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_ARCFIN
     i_obj.ecpSave()
  endproc

  add object oCODIMPN_1_7 as StdField with uid="DCDMVRCUBY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODIMPN", cQueryName = "CODIMPN",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Specificare il codice del nuovo tracciato",;
    HelpContextID = 265038810,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=151, Top=95, InputMask=replicate('X',20)

  add object oDESIMPN_1_8 as StdMemo with uid="TXAVTTYCWB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESIMPN", cQueryName = "DESIMPN",;
    bObbl = .t. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la descrizione del nuovo tracciato",;
    HelpContextID = 264979914,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=314, Top=95


  add object oBtn_1_13 as StdButton with uid="VQERAGEWET",left=570, top=120, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 82399002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSIM_BDS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODIMPN) and not empty(.w_CODIMP))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="MRPTSAHHIT",left=625, top=120, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75110330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="IJRCUXLTTH",Visible=.t., Left=8, Top=12,;
    Alignment=1, Width=140, Height=15,;
    Caption="Tracciato di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="RPIUSBQWZH",Visible=.t., Left=19, Top=39,;
    Alignment=1, Width=129, Height=15,;
    Caption="Da archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MGHBVYFDRP",Visible=.t., Left=19, Top=63,;
    Alignment=1, Width=129, Height=15,;
    Caption="Ad archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FENCFZHQTL",Visible=.t., Left=19, Top=95,;
    Alignment=1, Width=129, Height=15,;
    Caption="Nuovo tracciato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kds','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
