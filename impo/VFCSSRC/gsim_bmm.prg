* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bmm                                                        *
*              Import mov. magazzino e documenti                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_321]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2018-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsim_bmm
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tamGSIM_BMM",oParentObject)
return(i_retval)

define class tamGSIM_BMM as tGSIM_BMM

  proc Done()
    if !empty(i_Error)
      AH_errorMsg(i_Error)
    endif
    if !isnull(this.oParentObject) and this.bUpdateParentObject
      this.oParentObject.mCalc(.t.)
    endif
  return

enddefine

Proc tamGSIM_BMM
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bmm",oParentObject)
return(i_retval)

define class tgsim_bmm as StdBatch
  * --- Local variables
  w_MMSERIAL = space(10)
  w_MVSERIAL = space(10)
  w_ESISTE = .f.
  w_TDFLPPRO = space(1)
  w_TDALFEST = space(2)
  w_SimVal = space(5)
  w_CaoVal = 0
  w_NzCaoVal = 0
  w_DecTot = 0
  w_FLRIFE = space(2)
  w_ANTIPSOT = space(1)
  w_IVPERIND = 0
  w_IVPERIVA = 0
  COR_CLFO = space(15)
  w_RIFERIME = 0
  w_CAUMAG = space(5)
  w_CODIVA = space(5)
  w_CATCON = space(5)
  w_VOCCEN = space(15)
  w_VOCRIC = space(15)
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_FLCOMM = space(1)
  w_COSRIC = space(1)
  w_CAUCOL = space(5)
  w_LETTO = space(10)
  w_TD_SEGNO = space(1)
  w_ARCH = space(1)
  w_FLAVA1 = space(1)
  w_CMFLAVAL = space(1)
  w_RIPINC = space(1)
  w_RIPTRA = space(1)
  w_RIPIMB = space(1)
  w_FLUSEP = space(1)
  w_NOFRAZ = space(1)
  w_CALPRO = space(2)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_SALCOM = space(1)
  w_TIPOPE = space(1)
  w_NDIC = 0
  w_ALFADIC = space(2)
  w_MESS = space(10)
  w_IMPDIC = 0
  w_ADIC = space(4)
  w_NUCON = 0
  w_IMPUTI = 0
  w_DDIC = ctod("  /  /  ")
  w_APPO = space(1)
  w_CODIVE = space(5)
  w_TDIC = space(1)
  w_OK = .f.
  w_RIFDIC = space(10)
  w_DICODCON = space(15)
  w_DITIPCON = space(1)
  w_DIDATDOC = ctod("  /  /  ")
  w_OK_LET = .f.
  w_FLPPRO = space(1)
  w_ANSCORPO = space(1)
  w_ANCODIVA = space(5)
  w_COPERTRA = 0
  w_COPERIMB = 0
  w_COPERINC = 0
  w_PERIVE = 0
  w_FLVABD = space(1)
  w_AGGSAL = space(10)
  w_FLIMPE = space(1)
  w_F2IMPE = space(1)
  w_FLCASC = space(1)
  w_F2CASC = space(1)
  w_FLORDI = space(1)
  w_F2ORDI = space(1)
  w_FLRISE = space(1)
  w_F2RISE = space(1)
  w_MVFLSCOM = space(1)
  w_FLUBIC = space(1)
  w_MGPROMAG = space(2)
  w_PPCALPRO = space(2)
  w_OLDCALPRO = space(2)
  w_PPCALSCO = space(1)
  w_PPLISRIF = space(5)
  w_MASSGEN = space(1)
  w_SERPRO = space(10)
  w_CODAZI = space(5)
  w_PRSERIAL = space(10)
  w_PRGRURES = space(5)
  w_PRDATMOD = ctod("  /  /  ")
  w_DEOREEFF = 0
  w_DEMINEFF = 0
  w_DECOSINT = 0
  w_DEPREMIN = 0
  w_DEPREMAX = 0
  w_DEGAZUFF = space(6)
  w_PRFLDEFF = space(1)
  w_PRCENCOS = space(15)
  w_PRVOCCOS = space(15)
  w_PR_SEGNO = space(1)
  w_PRATTIVI = space(15)
  w_PRCODNOM = space(15)
  w_PRCODSED = space(5)
  w_PRCOSUNI = 0
  w_PRTCOINI = ctod("  /  /  ")
  w_PRTCOFIN = ctod("  /  /  ")
  w_PRRIFPRE = 0
  w_PRRIGPRE = space(1)
  w_PRTIPRI2 = space(1)
  w_PRRIFFAT = space(10)
  w_PRRIFFAA = space(10)
  w_PRRIFPRO = space(10)
  w_PRRIFPRA = space(10)
  w_PRSERAGG = space(10)
  w_PRTIPDOC = space(5)
  w_PRROWFAT = 0
  w_PRROWPRO = 0
  w_PRROWFAA = 0
  w_PRROWPRA = 0
  w_PRROWBOZ = 0
  w_PRRIFPRA = space(10)
  w_PRROWNOT = 0
  w_PRRIFNOT = space(10)
  w_PRNUMPRE = 0
  w_PRRIFBOZ = space(10)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  ESERCIZI_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  UNIMIS_idx=0
  ASPETTO_idx=0
  MAGAZZIN_idx=0
  AZIENDA_idx=0
  PAR_PROV_idx=0
  ALT_DETT_idx=0
  LISTART_idx=0
  VDATRITE_idx=0
  PRE_STAZ_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT ARCHIVI MAGAZZINO E MOVIMENTI
    * --- Variabili del batch precedente
    * --- Modello di segnalazione
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Aggiornamento righe IVA S/N
    * --- Aggiornamento partite S/N
    * --- Aggiornamento analitica S/N
    * --- Aggiornamento saldi S/N
    * --- Ricalcolo numeri registrazioni
    * --- Conto di costo per IVA indetraibile
    * --- File ascii da importare
    * --- Variabili per gestione rottura registrazioni
    * --- Chiave corrente movimentazione
    * --- Ultima chiave di rottura movimentazione
    * --- Ultimo serial
    * --- Ultimo numero registrazione
    * --- Ultimo numero protocollo
    * --- Ultimo tipo cliente/fornitore
    * --- Ultimo cliente/fornitore
    * --- Ultimo numero di riga
    * --- Messaggio a video - Elaborazione archivio ...
    * --- Variabile per gestione w_DESTINAZ = 'DO'.
    *     Indica se durante l'importazione di un documento si � verificato un errore su almeno una riga
    * --- Nome archivio
    * --- Numero record per il messaggio di errore sul campo obbligatorio
    * --- Variabili per calcolo autonumber (devono essere proprieta' di un oggetto e chiamarsi come i campi corrispondenti)
    * --- Serial
    this.w_MMSERIAL = space(10)
    this.w_MVSERIAL = space(10)
    * --- --
    * --- Controlla se il documento � gi� stato importato
    this.w_ESISTE = .f.
    this.w_TDFLPPRO = " "
    this.w_TDALFEST = "  "
    * --- Variabili per gestione valute
    * --- Simbolo valuta
    this.w_SimVal = 0
    * --- Cambio valuta
    this.w_CaoVal = 0
    * --- Cambio valuta nazionale
    this.w_NzCaoVal = 0
    * --- Cambio valuta
    this.w_DecTot = 0
    * --- Variabili locali
    * --- Riferimento tipo cliente/fornitore
    this.w_FLRIFE = "  "
    * --- Tipo sottoconto
    this.w_ANTIPSOT = " "
    * --- Percentuale indetraibilit� IVA
    this.w_IVPERIND = 0
    * --- Percentuale IVA
    this.w_IVPERIVA = 0
    * --- Corrente cliente o fornitore del master
    this.COR_CLFO = space(15)
    * --- Numero riga di riferimento
    this.w_RIFERIME = 0
    * --- Causale di magazzino della testata da Anagrafica articoli
    this.w_CAUMAG = ""
    * --- Codice IVA da Anagrafica articoli
    this.w_CODIVA = ""
    * --- Categoria Contabile da Anagrafica articoli
    this.w_CATCON = ""
    * --- Voce di Costo da Anagrafica articoli
    this.w_VOCCEN = ""
    * --- Voce di Ricavo da Anagrafica articoli
    this.w_VOCRIC = ""
    * --- Flag Dati Analitica da Causale Documenti
    this.w_FLANAL = ""
    * --- Flag Movimento di Analitica da Causale Documenti
    this.w_FLELAN = ""
    * --- Flag Gestione Progetti da Causale Documenti
    this.w_FLCOMM = ""
    * --- Flag Vendite/Acquisti da Causale Documenti
    this.w_COSRIC = ""
    * --- Causale contabile collegata
    * --- Test lettura
    * --- Segno Analitica
    * --- Archivio (documenti o movimenti di magazzino)
    * --- Aggiornamento archivi
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    * --- Il primo cliente/fornitore della registrazione deve essere scritto in testata.
    * --- Viene quindi memorizzato il codice corrente e la prima volta che cambia riaggiornato il master.
    this.COR_CLFO = this.oParentObject.ULT_CLFO
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_045705C0
        bErr_045705C0=bTrsErr
        this.Try_045705C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_045705C0
        * --- End
        * --- Aggiornamento records
        i_rows = 0
        * --- Try
        local bErr_04571B50
        bErr_04571B50=bTrsErr
        this.Try_04571B50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_04571B50
        * --- End
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
    endcase
    * --- Memorizza la nuova chiave di rottura
    this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
    if this.oParentObject.w_DESTINAZ <> "DO"
      if ! this.oParentObject.w_Corretto
        if this.oParentObject.w_DESTINAZ ="MM"
          * --- Aggiornamento non riuscito (I dati non erano corretti o non e' stato possibile aggiornare il database di Revolution)
          this.oParentObject.w_ResoDett = ah_msgformat("Serial %1 numero registrazione %2 del %3", trim(this.w_MMSERIAL), alltrim(str(w_MMNUMREG,6,0)), dtoc(w_MMDATREG))
        else
          * --- Aggiornamento non riuscito (I dati non erano corretti o non e' stato possibile aggiornare il database di Revolution)
          this.oParentObject.w_ResoDett = AH_MSGFORMAT("Serial %1 numero documento %2 del %3" , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMDOC,15,0)) , dtoc(w_MVDATDOC) )
        endif
        if this.oParentObject.w_ResoMode<>"SCARTO"
          * --- se la var. ResoMode � gi� stata valorizzata con "SCARTO" non sovrascrivo l' i_TrsMsg che gi� contiene un messaggio significativo
          this.oParentObject.w_ResoMode = "SCARTO"
          i_TrsMsg = message()
        endif
      endif
    else
      if ! this.oParentObject.w_CORRETTO
        this.oParentObject.w_ERRDETT = .T.
      endif
    endif
  endproc
  proc Try_045705C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "MM"
        * --- Movimenti di Magazzino
        w_MMCODESE = iif( empty( w_MMCODESE ), g_CODESE, w_MMCODESE)
        * --- Valori di default
        w_MMCODVAL = iif( empty(w_MMCODVAL) , g_PERVAL, w_MMCODVAL )
        w_MMDATREG = iif( empty(w_MMDATREG) , i_datsys , w_MMDATREG )
        w_MMCAOVAL = iif( empty(w_MMCAOVAL) , GETCAM(w_MMCODVAL, IIF(EMPTY(w_MMDATDOC), w_MMDATREG, w_MMDATDOC), 7) , w_MMCAOVAL )
        w_MMCODUTE = iif(g_MAGUTE="S", 0, iif(empty(w_MMCODUTE), i_CODUTE, w_MMCODUTE ) )
        * --- Calcolo Serial
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          this.w_MMSERIAL = space(10)
          i_Conn=i_TableProp[this.MVM_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEMVM", "i_codazi,w_MMSERIAL")
          this.oParentObject.ULT_SERI = this.w_MMSERIAL
          this.oParentObject.ULT_RNUM = 1
          w_MMNUMREG = iif(this.oParentObject.w_IMAGGNUR="S",0,w_MMNUMREG)
          if g_APPLICATION="ADHOC REVOLUTION"
            w_MMNUMREG = gsim_bpr(this,"PRMVM", "i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG",w_MMCODESE,w_MMCODUTE,w_MMNUMREG)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            w_MMNUMREG = gsim_bpr(this,"PRMVM", "i_codazi,w_MMCODESE,w_MMNUMREG",w_MMCODESE,w_MMNUMREG)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.oParentObject.ULT_NREG = w_MMNUMREG
          ah_msg( "Elaborazione %1 - record %2 di %3" , .t. , , , trim(this.oParentObject.w_Archivio) , alltrim(str(this.oParentObject.NumReco,10)) , alltrim(str(this.oParentObject.NumTotReco,10)) )
        else
          this.w_MMSERIAL = this.oParentObject.ULT_SERI
          w_MMNUMREG = this.oParentObject.ULT_NREG
          this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        endif
        * --- Lettura dati esercizio
        if empty( w_MMVALNAZ )
          * --- Read from ESERCIZI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ESERCIZI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ESVALNAZ"+;
              " from "+i_cTable+" ESERCIZI where ";
                  +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                  +" and ESCODESE = "+cp_ToStrODBC(w_MMCODESE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ESVALNAZ;
              from (i_cTable) where;
                  ESCODAZI = i_CODAZI;
                  and ESCODESE = w_MMCODESE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_MMVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Inserimento record master
        if this.oParentObject.ULT_RNUM = 1
          * --- Insert into MVM_MAST
          i_nConn=i_TableProp[this.MVM_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MMSERIAL"+",MMCODESE"+",MMNUMREG"+",MMDATREG"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(w_MMCODESE),'MVM_MAST','MMCODESE');
            +","+cp_NullLink(cp_ToStrODBC(w_MMNUMREG),'MVM_MAST','MMNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(w_MMDATREG),'MVM_MAST','MMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'MVM_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'MVM_MAST','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',w_MMCODESE,'MMNUMREG',w_MMNUMREG,'MMDATREG',w_MMDATREG,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
            insert into (i_cTable) (MMSERIAL,MMCODESE,MMNUMREG,MMDATREG,UTCC,UTDC &i_ccchkf. );
               values (;
                 this.w_MMSERIAL;
                 ,w_MMCODESE;
                 ,w_MMNUMREG;
                 ,w_MMDATREG;
                 ,this.oParentObject.w_CreVarUte;
                 ,this.oParentObject.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Inserimento record detail
        this.w_RIFERIME = -10
        w_CPROWORD = this.oParentObject.ULT_RNUM * 10
        * --- Insert into MVM_DETT
        i_nConn=i_TableProp[this.MVM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MMSERIAL"+",MMNUMRIF"+",CPROWNUM"+",CPROWORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_DETT','MMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFERIME),'MVM_DETT','MMNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'MVM_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(w_CPROWORD),'MVM_DETT','CPROWORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMNUMRIF',this.w_RIFERIME,'CPROWNUM',this.oParentObject.ULT_RNUM,'CPROWORD',w_CPROWORD)
          insert into (i_cTable) (MMSERIAL,MMNUMRIF,CPROWNUM,CPROWORD &i_ccchkf. );
             values (;
               this.w_MMSERIAL;
               ,this.w_RIFERIME;
               ,this.oParentObject.ULT_RNUM;
               ,w_CPROWORD;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "DT"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_Destinaz = "DR"
        * --- Righe Documenti Attivi/Passivi
        * --- Calcolo Numero Riga
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          * --- Ricalcolo i totali del documento precedente. Vedi anche GSIM_BAC Pag.1
          if (.not. empty( this.oParentObject.ULT_SERI ))
            do gsar_brd with this, this.oParentObject.ULT_SERI
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL"
              do vq_exec with 'gsim0bmm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVCODAG2 = _t2.MVCODAG2";
                  +",MVCODAGE = _t2.MVCODAGE";
                  +",MVCODBA2 = _t2.MVCODBA2";
                  +",MVCODBAN = _t2.MVCODBAN";
                  +",MVCODDES = _t2.MVCODDES";
                  +",MVCODIVE = _t2.MVCODIVE";
                  +",MVCODPAG = _t2.MVCODPAG";
                  +",MVCODPOR = _t2.MVCODPOR";
                  +",MVCODSED = _t2.MVCODSED";
                  +",MVCODSPE = _t2.MVCODSPE";
                  +",MVCODVAL = _t2.MVCODVAL";
                  +",MVCODVE2 = _t2.MVCODVE2";
                  +",MVCODVET = _t2.MVCODVET";
                  +",MVFLSCOR = _t2.MVFLSCOR";
                  +",MVIVAIMB = _t2.MVIVAIMB";
                  +",MVIVAINC = _t2.MVIVAINC";
                  +",MVIVATRA = _t2.MVIVATRA";
                  +",MVNUMCOR = _t2.MVNUMCOR";
                  +",MVRIFDIC = _t2.MVRIFDIC";
                  +",MVCODORN = _t2.MVCODORN";
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
                  +"DOC_MAST.MVCODAG2 = _t2.MVCODAG2";
                  +",DOC_MAST.MVCODAGE = _t2.MVCODAGE";
                  +",DOC_MAST.MVCODBA2 = _t2.MVCODBA2";
                  +",DOC_MAST.MVCODBAN = _t2.MVCODBAN";
                  +",DOC_MAST.MVCODDES = _t2.MVCODDES";
                  +",DOC_MAST.MVCODIVE = _t2.MVCODIVE";
                  +",DOC_MAST.MVCODPAG = _t2.MVCODPAG";
                  +",DOC_MAST.MVCODPOR = _t2.MVCODPOR";
                  +",DOC_MAST.MVCODSED = _t2.MVCODSED";
                  +",DOC_MAST.MVCODSPE = _t2.MVCODSPE";
                  +",DOC_MAST.MVCODVAL = _t2.MVCODVAL";
                  +",DOC_MAST.MVCODVE2 = _t2.MVCODVE2";
                  +",DOC_MAST.MVCODVET = _t2.MVCODVET";
                  +",DOC_MAST.MVFLSCOR = _t2.MVFLSCOR";
                  +",DOC_MAST.MVIVAIMB = _t2.MVIVAIMB";
                  +",DOC_MAST.MVIVAINC = _t2.MVIVAINC";
                  +",DOC_MAST.MVIVATRA = _t2.MVIVATRA";
                  +",DOC_MAST.MVNUMCOR = _t2.MVNUMCOR";
                  +",DOC_MAST.MVRIFDIC = _t2.MVRIFDIC";
                  +",DOC_MAST.MVCODORN = _t2.MVCODORN";
                  +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
                  +"MVCODAG2,";
                  +"MVCODAGE,";
                  +"MVCODBA2,";
                  +"MVCODBAN,";
                  +"MVCODDES,";
                  +"MVCODIVE,";
                  +"MVCODPAG,";
                  +"MVCODPOR,";
                  +"MVCODSED,";
                  +"MVCODSPE,";
                  +"MVCODVAL,";
                  +"MVCODVE2,";
                  +"MVCODVET,";
                  +"MVFLSCOR,";
                  +"MVIVAIMB,";
                  +"MVIVAINC,";
                  +"MVIVATRA,";
                  +"MVNUMCOR,";
                  +"MVRIFDIC,";
                  +"MVCODORN";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.MVCODAG2,";
                  +"t2.MVCODAGE,";
                  +"t2.MVCODBA2,";
                  +"t2.MVCODBAN,";
                  +"t2.MVCODDES,";
                  +"t2.MVCODIVE,";
                  +"t2.MVCODPAG,";
                  +"t2.MVCODPOR,";
                  +"t2.MVCODSED,";
                  +"t2.MVCODSPE,";
                  +"t2.MVCODVAL,";
                  +"t2.MVCODVE2,";
                  +"t2.MVCODVET,";
                  +"t2.MVFLSCOR,";
                  +"t2.MVIVAIMB,";
                  +"t2.MVIVAINC,";
                  +"t2.MVIVATRA,";
                  +"t2.MVNUMCOR,";
                  +"t2.MVRIFDIC,";
                  +"t2.MVCODORN";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
                  +"MVCODAG2 = _t2.MVCODAG2";
                  +",MVCODAGE = _t2.MVCODAGE";
                  +",MVCODBA2 = _t2.MVCODBA2";
                  +",MVCODBAN = _t2.MVCODBAN";
                  +",MVCODDES = _t2.MVCODDES";
                  +",MVCODIVE = _t2.MVCODIVE";
                  +",MVCODPAG = _t2.MVCODPAG";
                  +",MVCODPOR = _t2.MVCODPOR";
                  +",MVCODSED = _t2.MVCODSED";
                  +",MVCODSPE = _t2.MVCODSPE";
                  +",MVCODVAL = _t2.MVCODVAL";
                  +",MVCODVE2 = _t2.MVCODVE2";
                  +",MVCODVET = _t2.MVCODVET";
                  +",MVFLSCOR = _t2.MVFLSCOR";
                  +",MVIVAIMB = _t2.MVIVAIMB";
                  +",MVIVAINC = _t2.MVIVAINC";
                  +",MVIVATRA = _t2.MVIVATRA";
                  +",MVNUMCOR = _t2.MVNUMCOR";
                  +",MVRIFDIC = _t2.MVRIFDIC";
                  +",MVCODORN = _t2.MVCODORN";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVCODAG2 = (select MVCODAG2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODAGE = (select MVCODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODBA2 = (select MVCODBA2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODBAN = (select MVCODBAN from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODDES = (select MVCODDES from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODIVE = (select MVCODIVE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODPAG = (select MVCODPAG from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODPOR = (select MVCODPOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODSED = (select MVCODSED from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODSPE = (select MVCODSPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVAL = (select MVCODVAL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVE2 = (select MVCODVE2 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODVET = (select MVCODVET from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLSCOR = (select MVFLSCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVAIMB = (select MVIVAIMB from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVAINC = (select MVIVAINC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVIVATRA = (select MVIVATRA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVNUMCOR = (select MVNUMCOR from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVRIFDIC = (select MVRIFDIC from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVCODORN = (select MVCODORN from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.oParentObject.ULT_RNUM = 1
        else
          this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        endif
        * --- Try
        local bErr_045CC188
        bErr_045CC188=bTrsErr
        this.Try_045CC188()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_045CC188
        * --- End
      case this.oParentObject.w_Destinaz = "DO"
        * --- Parte analoga a w_DESTINAZ = 'DT'
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          this.oParentObject.w_ERRDETT = .F.
          this.oParentObject.ULT_RNUM = 1
          * --- Try
          local bErr_045DFBC8
          bErr_045DFBC8=bTrsErr
          this.Try_045DFBC8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_045DFBC8
          * --- End
        else
          this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
        endif
        if NOT this.w_ESISTE
          * --- Try
          local bErr_045DD2E8
          bErr_045DD2E8=bTrsErr
          this.Try_045DD2E8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_Corretto = .F.
          endif
          bTrsErr=bTrsErr or bErr_045DD2E8
          * --- End
        endif
    endcase
    return
  proc Try_04571B50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "MM"
        * --- Movimenti di Magazzino
        this.oParentObject.w_ResoDett = ah_msgformat("Serial %1 numero registrazione %2 del %3 riga %4", trim(this.w_MMSERIAL), alltrim(str(w_MMNUMREG,6,0)), dtoc(w_MMDATREG), ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
        * --- Lettura dati causale di magazzino
        w_MMCAUMAG = iif( empty(w_MMCAUMAG) , w_MMTCAMAG, w_MMCAUMAG )
        w_CMFLCLFR = ""
        store " " to w_MMFLELGM, w_MMFLELAN, w_MMFLCASC, w_MMFLORDI, w_MMFLIMPE, w_MMFLRISE, w_MM_SEGNO, w_MMCAUCOL
        if g_APPLICATION="ADHOC REVOLUTION"
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCLFR,CMFLELGM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL,CMFLAVAL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(w_MMCAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCLFR,CMFLELGM,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL,CMFLAVAL;
              from (i_cTable) where;
                  CMCODICE = w_MMCAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
            w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Controllo Data Registrazione Magazzino
          if Not Empty(CHKCONS("M",w_MMDATREG,"B","N"))
            this.oParentObject.w_ResoDett = ah_msgformat("Serial %1 numero registrazione %2 del %3 riga %4 %5", trim(this.w_MMSERIAL), alltrim(str(w_MMNUMREG,6,0)), dtoc(w_MMDATREG), ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) , SUBSTR(CHKCONS("M",w_MMDATREG,"B","N"),12) )
            * --- Raise
            i_Error="Data registrazione antecedente data consolidamento magazzino"
            return
          endif
        else
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCLFR,CMFLELGM,CMFLCASC,CMFLELAN,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLANAL,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(w_MMCAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCLFR,CMFLELGM,CMFLCASC,CMFLELAN,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLANAL,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = w_MMCAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CMFLCLFR = NVL(cp_ToDate(_read_.CMFLCLFR),cp_NullValue(_read_.CMFLCLFR))
            w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            w_MMFLELAN = NVL(cp_ToDate(_read_.CMFLELAN),cp_NullValue(_read_.CMFLELAN))
            w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            w_MM_SEGNO = NVL(cp_ToDate(_read_.CMFLANAL),cp_NullValue(_read_.CMFLANAL))
            w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Lettura dati causale collegata di magazzino
        store " " to w_MMF2CASC, w_MMF2ORDI, w_MMF2IMPE, w_MMF2RISE
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(w_MMCAUCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL;
            from (i_cTable) where;
                CMCODICE = w_MMCAUCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_MMF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          w_MMF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          w_MMF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          w_MMF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.w_CMFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Inizializzazione variabili
        w_MMCODUTE = IIF(g_MAGUTE="S", 0, iif( empty(w_MMCODUTE), i_CODUTE, w_MMCODUTE) )
        w_MMFLCLFR = iif( empty(w_MMFLCLFR), w_CMFLCLFR, w_MMFLCLFR )
        w_MMTIPCON = iif( empty(w_MMTIPCON) .and. w_CMFLCLFR$"CF", w_CMFLCLFR, w_MMTIPCON )
        * --- Lettura dati valute
        store 0 to this.w_DECTOT
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(w_MMCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = w_MMCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        store 0 to w_CAONAZ
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(w_MMVALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL;
            from (i_cTable) where;
                VACODVAL = w_MMVALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Scrittura record master
        if this.oParentObject.ULT_RNUM = 1
          if g_APPLICATION="ADHOC REVOLUTION"
            * --- Write into MVM_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MMVALNAZ ="+cp_NullLink(cp_ToStrODBC(w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
              +",MMCODUTE ="+cp_NullLink(cp_ToStrODBC(w_MMCODUTE),'MVM_MAST','MMCODUTE');
              +",MMTCAMAG ="+cp_NullLink(cp_ToStrODBC(w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
              +",MMTCOLIS ="+cp_NullLink(cp_ToStrODBC(w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
              +",MMNUMDOC ="+cp_NullLink(cp_ToStrODBC(w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
              +",MMALFDOC ="+cp_NullLink(cp_ToStrODBC(w_MMALFDOC),'MVM_MAST','MMALFDOC');
              +",MMDATDOC ="+cp_NullLink(cp_ToStrODBC(w_MMDATDOC),'MVM_MAST','MMDATDOC');
              +",MMFLCLFR ="+cp_NullLink(cp_ToStrODBC(w_MMFLCLFR),'MVM_MAST','MMFLCLFR');
              +",MMTIPCON ="+cp_NullLink(cp_ToStrODBC(w_MMTIPCON),'MVM_MAST','MMTIPCON');
              +",MMCODCON ="+cp_NullLink(cp_ToStrODBC(w_MMCODCON),'MVM_MAST','MMCODCON');
              +",MMDESSUP ="+cp_NullLink(cp_ToStrODBC(w_MMDESSUP),'MVM_MAST','MMDESSUP');
              +",MMCODVAL ="+cp_NullLink(cp_ToStrODBC(w_MMCODVAL),'MVM_MAST','MMCODVAL');
              +",MMCAOVAL ="+cp_NullLink(cp_ToStrODBC(w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
              +",MMSCOCL1 ="+cp_NullLink(cp_ToStrODBC(w_MMSCOCL1),'MVM_MAST','MMSCOCL1');
              +",MMSCOCL2 ="+cp_NullLink(cp_ToStrODBC(w_MMSCOCL2),'MVM_MAST','MMSCOCL2');
              +",MMSCOPAG ="+cp_NullLink(cp_ToStrODBC(w_MMSCOPAG),'MVM_MAST','MMSCOPAG');
              +",MMFLGIOM ="+cp_NullLink(cp_ToStrODBC(w_MMFLGIOM),'MVM_MAST','MMFLGIOM');
                  +i_ccchkf ;
              +" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
                     )
            else
              update (i_cTable) set;
                  MMVALNAZ = w_MMVALNAZ;
                  ,MMCODUTE = w_MMCODUTE;
                  ,MMTCAMAG = w_MMTCAMAG;
                  ,MMTCOLIS = w_MMTCOLIS;
                  ,MMNUMDOC = w_MMNUMDOC;
                  ,MMALFDOC = w_MMALFDOC;
                  ,MMDATDOC = w_MMDATDOC;
                  ,MMFLCLFR = w_MMFLCLFR;
                  ,MMTIPCON = w_MMTIPCON;
                  ,MMCODCON = w_MMCODCON;
                  ,MMDESSUP = w_MMDESSUP;
                  ,MMCODVAL = w_MMCODVAL;
                  ,MMCAOVAL = w_MMCAOVAL;
                  ,MMSCOCL1 = w_MMSCOCL1;
                  ,MMSCOCL2 = w_MMSCOCL2;
                  ,MMSCOPAG = w_MMSCOPAG;
                  ,MMFLGIOM = w_MMFLGIOM;
                  &i_ccchkf. ;
               where;
                  MMSERIAL = this.w_MMSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into MVM_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MMVALNAZ ="+cp_NullLink(cp_ToStrODBC(w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
              +",MMCODUTE ="+cp_NullLink(cp_ToStrODBC(w_MMCODUTE),'MVM_MAST','MMCODUTE');
              +",MMTCAMAG ="+cp_NullLink(cp_ToStrODBC(w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
              +",MMTCOCEN ="+cp_NullLink(cp_ToStrODBC(w_MMTCOCEN),'MVM_MAST','MMTCOCEN');
              +",MMTCOMME ="+cp_NullLink(cp_ToStrODBC(w_MMTCOMME),'MVM_MAST','MMTCOMME');
              +",MMTCOLIS ="+cp_NullLink(cp_ToStrODBC(w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
              +",MMTCOMAG ="+cp_NullLink(cp_ToStrODBC(w_MMTCOMAG),'MVM_MAST','MMTCOMAG');
              +",MMTCOMAT ="+cp_NullLink(cp_ToStrODBC(w_MMTCOMAT),'MVM_MAST','MMTCOMAT');
              +",MMNUMDOC ="+cp_NullLink(cp_ToStrODBC(w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
              +",MMALFDOC ="+cp_NullLink(cp_ToStrODBC(w_MMALFDOC),'MVM_MAST','MMALFDOC');
              +",MMDATDOC ="+cp_NullLink(cp_ToStrODBC(w_MMDATDOC),'MVM_MAST','MMDATDOC');
              +",MMFLCLFR ="+cp_NullLink(cp_ToStrODBC(w_MMFLCLFR),'MVM_MAST','MMFLCLFR');
              +",MMTIPCON ="+cp_NullLink(cp_ToStrODBC(w_MMTIPCON),'MVM_MAST','MMTIPCON');
              +",MMCODCON ="+cp_NullLink(cp_ToStrODBC(w_MMCODCON),'MVM_MAST','MMCODCON');
              +",MMDESSUP ="+cp_NullLink(cp_ToStrODBC(w_MMDESSUP),'MVM_MAST','MMDESSUP');
              +",MMCODVAL ="+cp_NullLink(cp_ToStrODBC(w_MMCODVAL),'MVM_MAST','MMCODVAL');
              +",MMCAOVAL ="+cp_NullLink(cp_ToStrODBC(w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
              +",MMSCOCL1 ="+cp_NullLink(cp_ToStrODBC(w_MMSCOCL1),'MVM_MAST','MMSCOCL1');
              +",MMSCOCL2 ="+cp_NullLink(cp_ToStrODBC(w_MMSCOCL2),'MVM_MAST','MMSCOCL2');
              +",MMSCOPAG ="+cp_NullLink(cp_ToStrODBC(w_MMSCOPAG),'MVM_MAST','MMSCOPAG');
              +",MMFLGIOM ="+cp_NullLink(cp_ToStrODBC(w_MMFLGIOM),'MVM_MAST','MMFLGIOM');
                  +i_ccchkf ;
              +" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
                     )
            else
              update (i_cTable) set;
                  MMVALNAZ = w_MMVALNAZ;
                  ,MMCODUTE = w_MMCODUTE;
                  ,MMTCAMAG = w_MMTCAMAG;
                  ,MMTCOCEN = w_MMTCOCEN;
                  ,MMTCOMME = w_MMTCOMME;
                  ,MMTCOLIS = w_MMTCOLIS;
                  ,MMTCOMAG = w_MMTCOMAG;
                  ,MMTCOMAT = w_MMTCOMAT;
                  ,MMNUMDOC = w_MMNUMDOC;
                  ,MMALFDOC = w_MMALFDOC;
                  ,MMDATDOC = w_MMDATDOC;
                  ,MMFLCLFR = w_MMFLCLFR;
                  ,MMTIPCON = w_MMTIPCON;
                  ,MMCODCON = w_MMCODCON;
                  ,MMDESSUP = w_MMDESSUP;
                  ,MMCODVAL = w_MMCODVAL;
                  ,MMCAOVAL = w_MMCAOVAL;
                  ,MMSCOCL1 = w_MMSCOCL1;
                  ,MMSCOCL2 = w_MMSCOCL2;
                  ,MMSCOPAG = w_MMSCOPAG;
                  ,MMFLGIOM = w_MMFLGIOM;
                  &i_ccchkf. ;
               where;
                  MMSERIAL = this.w_MMSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        * --- Inizializzazione variabili
        w_MMCODMAG = iif( empty(w_MMCODMAG) , w_MMTCOMAG, w_MMCODMAG )
        w_MMCODMAT = iif( empty(w_MMCODMAT) , w_MMTCOMAT, w_MMCODMAT )
        w_MMCODLIS = iif( empty(w_MMCODLIS) , w_MMTCOLIS, w_MMCODLIS )
        if empty( w_MMCODICE )
          if empty( w_MMCODVAR )
            w_MMCODICE = w_MMCODART
          else
            w_MMCODICE = trim( w_MMCODART )+ "#" + w_MMCODVAR
          endif
        endif
        * --- Lettura dati articolo
        store " " to w_UNMIS1, w_UNMIS2, w_OPERAT
        store 0 to w_MOLTIP
        if Not Empty(w_MMCODMAG)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGFLUBIC"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(w_MMCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGFLUBIC;
              from (i_cTable) where;
                  MGCODMAG = w_MMCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(w_MMCODMAT)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGFLUBIC"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(w_MMCODMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGFLUBIC;
              from (i_cTable) where;
                  MGCODMAG = w_MMCODMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if .not. empty( w_MMCODART )
          if g_APPLICATION="ADHOC REVOLUTION"
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLLOTT,ARFLUSEP"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(w_MMCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLLOTT,ARFLUSEP;
                from (i_cTable) where;
                    ARCODART = w_MMCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
              w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
              w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
              w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMFLFRAZ"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(w_UNMIS1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMFLFRAZ;
                from (i_cTable) where;
                    UMCODICE = w_UNMIS1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
             
 w_MMFLLOTT=IIF(w_FLLOTT $ "S-C" OR this.w_FLUBIC="S",; 
 LEFT(ALLTRIM(w_MMFLCASC)+IIF(w_MMFLRISE="+", "-", IIF(w_MMFLRISE="-", "+", " ")), 1), " ") 
 
 w_MMF2LOTT=IIF(w_FLLOTT $ "S-C" OR w_F2UBIC="S",; 
 LEFT(ALLTRIM(w_MMF2CASC)+IIF(w_MMF2RISE="+", "-", IIF(w_MMF2RISE="-", "+", " ")), 1), " ") 
 
 w_MMCODUBI=IIF(Not Empty(w_MMCODMAG) AND w_MMFLLOTT $ "+-" ,w_MMCODUBI,Space(20)) 
 w_MMCODUB2=IIF(Not Empty(w_MMCODMAT) AND w_MMF2LOTT $ "+-" ,w_MMCODUB2,Space(20)) 
 w_MMCODLOT=IIF(w_FLLOTT $ "SC" AND (w_MMF2LOTT $ "+-" OR w_MMFLLOTT $ "+-" ) ,w_MMCODLOT,Space(20)) 
 w_MMLOTMAG=iif( Empty( w_MMCODLOT ) And Empty( w_MMCODUBI ) , SPACE(5) , w_MMCODMAG ) 
 w_MMLOTMAT=iif( Empty( w_MMCODLOT ) And Empty( w_MMCODUB2 ) , SPACE(5) , w_MMCODMAT )
          else
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLLOTT,ARFLUBIC"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(w_MMCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLLOTT,ARFLUBIC;
                from (i_cTable) where;
                    ARCODART = w_MMCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
              w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
              w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
              w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              this.w_FLUBIC = NVL(cp_ToDate(_read_.ARFLUBIC),cp_NullValue(_read_.ARFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
             
 w_FLLOTT1 = IIF(w_FLLOTT="S" OR this.w_FLUBIC="S", IIF(w_MMFLRISE="+", "-", w_MMFLCASC)," ") 
 w_MMFLLOTT = IIF(g_PERLOT="S" OR g_PERUBI="S", w_FLLOTT1, " ") 
 w_FLLOTT2 = IIF(w_FLLOTT="S" OR this.w_FLUBIC="S", IIF(w_MMF2RISE="+", "-", w_MMF2CASC), " ") 
 w_MMF2LOTT = IIF(g_PERLOT="S" OR g_PERUBI="S", w_FLLOTT2, " ")
          endif
        endif
        * --- Lettura dati chiavi articoli
        store " " to w_UNMIS3, w_OPERA3
        store 0 to w_MOLTI3
        if .not. empty( w_MMCODICE )
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(w_MMCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) where;
                  CACODICE = w_MMCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        msg=" "
        * --- Se ho impostato la qt� nella 1um nel tracciato prende questa altimenti la ricalcola in base alla
        *     quantit� movimentata
        w_MMQTAUM1=iif(not empty(w_MMQTAUM1), w_MMQTAUM1, iif( empty(w_MMQTAUM1).and.(!empty(w_MMQTAMOV)) ,CALQTA(w_MMQTAMOV,LEFT(w_MMUNIMIS+"   ",3),LEFT(w_UNMIS2+"   ",3),LEFT(w_OPERAT+" ",1), w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, "N", @msg, LEFT(w_UNMIS3+"   ",3), w_OPERA3, w_MOLTI3,, "I"),w_MMQTAMOV))
        if not empty(msg)
          this.oParentObject.w_ResoDett = ah_msgformat("Serial %1 numero registrazione %2 del %3 riga %4 %5", trim(this.w_MMSERIAL), alltrim(str(w_MMNUMREG,6,0)), dtoc(w_MMDATREG), ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) , msg )
          * --- Raise
          i_Error="Quantit� della 1a UM incongruente"
          return
        endif
        if w_MMPREZZO=0 .and. w_MMVALMAG<>0 .and. w_MMQTAMOV<>0
          * --- Ricalcola prezzo in funzione del valore fiscale
          if w_MMSCONT1+w_MMSCONT2+w_MMSCONT3+w_MMSCONT4+w_MMSCOCL1+w_MMSCOCL2+w_MMSCOPAG=0
            w_MMPREZZO = w_MMVALMAG / w_MMQTAMOV
          endif
        endif
        if empty(w_MMVALMAG)
          w_VALUNI = w_MMPREZZO * (1+w_MMSCONT1/100)*(1+w_MMSCONT2/100)*(1+w_MMSCONT3/100)*(1+w_MMSCONT4/100)
          w_MMVALMAG = cp_ROUND((w_MMQTAMOV*w_VALUNI) * (1+w_MMSCOCL1/100)*(1+w_MMSCOCL2/100)*(1+w_MMSCOPAG/100), this.w_DECTOT)
        endif
        if empty( w_MMIMPNAZ )
          if w_MMVALNAZ=w_MMCODVAL
            w_MMIMPNAZ = w_MMVALMAG
          else
            w_MMIMPNAZ = VAL2MON(w_MMVALMAG,w_MMCAOVAL,w_CAONAZ,IIF(EMPTY(w_MMDATDOC), w_MMDATREG, w_MMDATDOC))
          endif
        endif
        w_MMKEYSAL = LEFT(w_MMCODART + SPACE(20), 20)
        w_MMFLOMAG = iif( empty( w_MMFLOMAG ), "X", w_MMFLOMAG )
        w_MMFLULPV = iif( ( this.w_FLAVA1 = "V" AND w_MMFLCASC $ "+-" AND w_MMFLOMAG<>"S" ) And w_MMPREZZO<>0, "=", " " )
        w_MMFLULCA = iif( ( this.w_FLAVA1 = "A" AND w_MMFLCASC $ "+-" AND w_MMFLOMAG<>"S" ) And w_MMPREZZO<>0, "=", " " )
        w_MMCODBUN = iif( empty( w_MMCODBUN ), g_CODBUN , w_MMCODBUN )
        * --- Scrittura riga di dettaglio
        if g_APPLICATION="ADHOC REVOLUTION"
          w_MMVALULT=IIF(w_MMQTAUM1=0, 0, cp_ROUND(w_MMIMPNAZ/w_MMQTAUM1, g_PERPUL))
          * --- Write into MVM_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MMCAUCOL ="+cp_NullLink(cp_ToStrODBC(w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
            +",MMCAUMAG ="+cp_NullLink(cp_ToStrODBC(w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
            +",MMCODART ="+cp_NullLink(cp_ToStrODBC(w_MMCODART),'MVM_DETT','MMCODART');
            +",MMCODATT ="+cp_NullLink(cp_ToStrODBC(w_MMCODATT),'MVM_DETT','MMCODATT');
            +",MMCODCOM ="+cp_NullLink(cp_ToStrODBC(w_MMCODCOM),'MVM_DETT','MMCODCOM');
            +",MMCODCOS ="+cp_NullLink(cp_ToStrODBC(w_MMCODCOS),'MVM_DETT','MMCODCOS');
            +",MMCODICE ="+cp_NullLink(cp_ToStrODBC(w_MMCODICE),'MVM_DETT','MMCODICE');
            +",MMCODLIS ="+cp_NullLink(cp_ToStrODBC(w_MMCODLIS),'MVM_DETT','MMCODLIS');
            +",MMCODMAG ="+cp_NullLink(cp_ToStrODBC(w_MMCODMAG),'MVM_DETT','MMCODMAG');
            +",MMCODMAT ="+cp_NullLink(cp_ToStrODBC(w_MMCODMAT),'MVM_DETT','MMCODMAT');
            +",MMF2CASC ="+cp_NullLink(cp_ToStrODBC(w_MMF2CASC),'MVM_DETT','MMF2CASC');
            +",MMF2IMPE ="+cp_NullLink(cp_ToStrODBC(w_MMF2IMPE),'MVM_DETT','MMF2IMPE');
            +",MMF2ORDI ="+cp_NullLink(cp_ToStrODBC(w_MMF2ORDI),'MVM_DETT','MMF2ORDI');
            +",MMF2RISE ="+cp_NullLink(cp_ToStrODBC(w_MMF2RISE),'MVM_DETT','MMF2RISE');
            +",MMFLCASC ="+cp_NullLink(cp_ToStrODBC(w_MMFLCASC),'MVM_DETT','MMFLCASC');
            +",MMFLCOCO ="+cp_NullLink(cp_ToStrODBC(w_MMFLCOCO),'MVM_DETT','MMFLCOCO');
            +",MMFLELGM ="+cp_NullLink(cp_ToStrODBC(w_MMFLELGM),'MVM_DETT','MMFLELGM');
            +",MMFLIMPE ="+cp_NullLink(cp_ToStrODBC(w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
            +",MMFLOMAG ="+cp_NullLink(cp_ToStrODBC(w_MMFLOMAG),'MVM_DETT','MMFLOMAG');
            +",MMFLORCO ="+cp_NullLink(cp_ToStrODBC(w_MMFLORCO),'MVM_DETT','MMFLORCO');
            +",MMFLORDI ="+cp_NullLink(cp_ToStrODBC(w_MMFLORDI),'MVM_DETT','MMFLORDI');
            +",MMFLRISE ="+cp_NullLink(cp_ToStrODBC(w_MMFLRISE),'MVM_DETT','MMFLRISE');
            +",MMFLULCA ="+cp_NullLink(cp_ToStrODBC(w_MMFLULCA),'MVM_DETT','MMFLULCA');
            +",MMFLULPV ="+cp_NullLink(cp_ToStrODBC(w_MMFLULPV),'MVM_DETT','MMFLULPV');
            +",MMIMPCOM ="+cp_NullLink(cp_ToStrODBC(w_MMIMPCOM),'MVM_DETT','MMIMPCOM');
            +",MMIMPNAZ ="+cp_NullLink(cp_ToStrODBC(w_MMIMPNAZ),'MVM_DETT','MMIMPNAZ');
            +",MMKEYSAL ="+cp_NullLink(cp_ToStrODBC(w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
            +",MMPREZZO ="+cp_NullLink(cp_ToStrODBC(w_MMPREZZO),'MVM_DETT','MMPREZZO');
            +",MMQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
            +",MMQTAUM1 ="+cp_NullLink(cp_ToStrODBC(w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
            +",MMSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT1),'MVM_DETT','MMSCONT1');
            +",MMSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT2),'MVM_DETT','MMSCONT2');
            +",MMSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT3),'MVM_DETT','MMSCONT3');
            +",MMSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT4),'MVM_DETT','MMSCONT4');
            +",MMTIPATT ="+cp_NullLink(cp_ToStrODBC(w_MMTIPATT),'MVM_DETT','MMTIPATT');
            +",MMUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
            +",MMVALMAG ="+cp_NullLink(cp_ToStrODBC(w_MMVALMAG),'MVM_DETT','MMVALMAG');
            +",MMVALULT ="+cp_NullLink(cp_ToStrODBC(w_MMVALULT),'MVM_DETT','MMVALULT');
            +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC(w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
            +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC(w_MMF2LOTT),'MVM_DETT','MMF2LOTT');
            +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(w_MMCODUBI),'MVM_DETT','MMCODUBI');
            +",MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(w_MMCODUB2),'MVM_DETT','MMCODUB2');
            +",MMCODLOT ="+cp_NullLink(cp_ToStrODBC(w_MMCODLOT),'MVM_DETT','MMCODLOT');
            +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(w_MMLOTMAG),'MVM_DETT','MMLOTMAG');
            +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(w_MMLOTMAT),'MVM_DETT','MMLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
                +" and MMNUMRIF = "+cp_ToStrODBC(this.w_RIFERIME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                   )
          else
            update (i_cTable) set;
                MMCAUCOL = w_MMCAUCOL;
                ,MMCAUMAG = w_MMCAUMAG;
                ,MMCODART = w_MMCODART;
                ,MMCODATT = w_MMCODATT;
                ,MMCODCOM = w_MMCODCOM;
                ,MMCODCOS = w_MMCODCOS;
                ,MMCODICE = w_MMCODICE;
                ,MMCODLIS = w_MMCODLIS;
                ,MMCODMAG = w_MMCODMAG;
                ,MMCODMAT = w_MMCODMAT;
                ,MMF2CASC = w_MMF2CASC;
                ,MMF2IMPE = w_MMF2IMPE;
                ,MMF2ORDI = w_MMF2ORDI;
                ,MMF2RISE = w_MMF2RISE;
                ,MMFLCASC = w_MMFLCASC;
                ,MMFLCOCO = w_MMFLCOCO;
                ,MMFLELGM = w_MMFLELGM;
                ,MMFLIMPE = w_MMFLIMPE;
                ,MMFLOMAG = w_MMFLOMAG;
                ,MMFLORCO = w_MMFLORCO;
                ,MMFLORDI = w_MMFLORDI;
                ,MMFLRISE = w_MMFLRISE;
                ,MMFLULCA = w_MMFLULCA;
                ,MMFLULPV = w_MMFLULPV;
                ,MMIMPCOM = w_MMIMPCOM;
                ,MMIMPNAZ = w_MMIMPNAZ;
                ,MMKEYSAL = w_MMKEYSAL;
                ,MMPREZZO = w_MMPREZZO;
                ,MMQTAMOV = w_MMQTAMOV;
                ,MMQTAUM1 = w_MMQTAUM1;
                ,MMSCONT1 = w_MMSCONT1;
                ,MMSCONT2 = w_MMSCONT2;
                ,MMSCONT3 = w_MMSCONT3;
                ,MMSCONT4 = w_MMSCONT4;
                ,MMTIPATT = w_MMTIPATT;
                ,MMUNIMIS = w_MMUNIMIS;
                ,MMVALMAG = w_MMVALMAG;
                ,MMVALULT = w_MMVALULT;
                ,MMFLLOTT = w_MMFLLOTT;
                ,MMF2LOTT = w_MMF2LOTT;
                ,MMCODUBI = w_MMCODUBI;
                ,MMCODUB2 = w_MMCODUB2;
                ,MMCODLOT = w_MMCODLOT;
                ,MMLOTMAG = w_MMLOTMAG;
                ,MMLOTMAT = w_MMLOTMAT;
                &i_ccchkf. ;
             where;
                MMSERIAL = this.w_MMSERIAL;
                and MMNUMRIF = this.w_RIFERIME;
                and CPROWNUM = this.oParentObject.ULT_RNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into MVM_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MVM_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MMCAUMAG ="+cp_NullLink(cp_ToStrODBC(w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
            +",MMCAUCOL ="+cp_NullLink(cp_ToStrODBC(w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
            +",MMCODMAG ="+cp_NullLink(cp_ToStrODBC(w_MMCODMAG),'MVM_DETT','MMCODMAG');
            +",MMCODMAT ="+cp_NullLink(cp_ToStrODBC(w_MMCODMAT),'MVM_DETT','MMCODMAT');
            +",MMCODLIS ="+cp_NullLink(cp_ToStrODBC(w_MMCODLIS),'MVM_DETT','MMCODLIS');
            +",MMCODICE ="+cp_NullLink(cp_ToStrODBC(w_MMCODICE),'MVM_DETT','MMCODICE');
            +",MMCODART ="+cp_NullLink(cp_ToStrODBC(w_MMCODART),'MVM_DETT','MMCODART');
            +",MMCODVAR ="+cp_NullLink(cp_ToStrODBC(w_MMCODVAR),'MVM_DETT','MMCODVAR');
            +",MMUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
            +",MMQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
            +",MMQTAUM1 ="+cp_NullLink(cp_ToStrODBC(w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
            +",MMPREZZO ="+cp_NullLink(cp_ToStrODBC(w_MMPREZZO),'MVM_DETT','MMPREZZO');
            +",MMSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT1),'MVM_DETT','MMSCONT1');
            +",MMSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT2),'MVM_DETT','MMSCONT2');
            +",MMSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT3),'MVM_DETT','MMSCONT3');
            +",MMSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_MMSCONT4),'MVM_DETT','MMSCONT4');
            +",MMVALMAG ="+cp_NullLink(cp_ToStrODBC(w_MMVALMAG),'MVM_DETT','MMVALMAG');
            +",MMIMPNAZ ="+cp_NullLink(cp_ToStrODBC(w_MMIMPNAZ),'MVM_DETT','MMIMPNAZ');
            +",MMKEYSAL ="+cp_NullLink(cp_ToStrODBC(w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
            +",MMFLELGM ="+cp_NullLink(cp_ToStrODBC(w_MMFLELGM),'MVM_DETT','MMFLELGM');
            +",MMFLELAN ="+cp_NullLink(cp_ToStrODBC(w_MMFLELAN),'MVM_DETT','MMFLELAN');
            +",MMFLCASC ="+cp_NullLink(cp_ToStrODBC(w_MMFLCASC),'MVM_DETT','MMFLCASC');
            +",MMF2CASC ="+cp_NullLink(cp_ToStrODBC(w_MMF2CASC),'MVM_DETT','MMF2CASC');
            +",MMFLORDI ="+cp_NullLink(cp_ToStrODBC(w_MMFLORDI),'MVM_DETT','MMFLORDI');
            +",MMF2ORDI ="+cp_NullLink(cp_ToStrODBC(w_MMF2ORDI),'MVM_DETT','MMF2ORDI');
            +",MMFLIMPE ="+cp_NullLink(cp_ToStrODBC(w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
            +",MMF2IMPE ="+cp_NullLink(cp_ToStrODBC(w_MMF2IMPE),'MVM_DETT','MMF2IMPE');
            +",MMFLRISE ="+cp_NullLink(cp_ToStrODBC(w_MMFLRISE),'MVM_DETT','MMFLRISE');
            +",MMF2RISE ="+cp_NullLink(cp_ToStrODBC(w_MMF2RISE),'MVM_DETT','MMF2RISE');
            +",MMFLOMAG ="+cp_NullLink(cp_ToStrODBC(w_MMFLOMAG),'MVM_DETT','MMFLOMAG');
            +",MMVOCCEN ="+cp_NullLink(cp_ToStrODBC(w_MMVOCCEN),'MVM_DETT','MMVOCCEN');
            +",MMCODCEN ="+cp_NullLink(cp_ToStrODBC(w_MMCODCEN),'MVM_DETT','MMCODCEN');
            +",MMCODCOM ="+cp_NullLink(cp_ToStrODBC(w_MMCODCOM),'MVM_DETT','MMCODCOM');
            +",MM_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_MM_SEGNO),'MVM_DETT','MM_SEGNO');
            +",MMINICOM ="+cp_NullLink(cp_ToStrODBC(w_MMINICOM),'MVM_DETT','MMINICOM');
            +",MMFINCOM ="+cp_NullLink(cp_ToStrODBC(w_MMFINCOM),'MVM_DETT','MMFINCOM');
            +",MMFLRIPA ="+cp_NullLink(cp_ToStrODBC(w_MMFLRIPA),'MVM_DETT','MMFLRIPA');
            +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC(w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
            +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC(w_MMF2LOTT),'MVM_DETT','MMF2LOTT');
            +",MMCODBUN ="+cp_NullLink(cp_ToStrODBC(w_MMCODBUN),'MVM_DETT','MMCODBUN');
                +i_ccchkf ;
            +" where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.w_MMSERIAL);
                +" and MMNUMRIF = "+cp_ToStrODBC(this.w_RIFERIME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                   )
          else
            update (i_cTable) set;
                MMCAUMAG = w_MMCAUMAG;
                ,MMCAUCOL = w_MMCAUCOL;
                ,MMCODMAG = w_MMCODMAG;
                ,MMCODMAT = w_MMCODMAT;
                ,MMCODLIS = w_MMCODLIS;
                ,MMCODICE = w_MMCODICE;
                ,MMCODART = w_MMCODART;
                ,MMCODVAR = w_MMCODVAR;
                ,MMUNIMIS = w_MMUNIMIS;
                ,MMQTAMOV = w_MMQTAMOV;
                ,MMQTAUM1 = w_MMQTAUM1;
                ,MMPREZZO = w_MMPREZZO;
                ,MMSCONT1 = w_MMSCONT1;
                ,MMSCONT2 = w_MMSCONT2;
                ,MMSCONT3 = w_MMSCONT3;
                ,MMSCONT4 = w_MMSCONT4;
                ,MMVALMAG = w_MMVALMAG;
                ,MMIMPNAZ = w_MMIMPNAZ;
                ,MMKEYSAL = w_MMKEYSAL;
                ,MMFLELGM = w_MMFLELGM;
                ,MMFLELAN = w_MMFLELAN;
                ,MMFLCASC = w_MMFLCASC;
                ,MMF2CASC = w_MMF2CASC;
                ,MMFLORDI = w_MMFLORDI;
                ,MMF2ORDI = w_MMF2ORDI;
                ,MMFLIMPE = w_MMFLIMPE;
                ,MMF2IMPE = w_MMF2IMPE;
                ,MMFLRISE = w_MMFLRISE;
                ,MMF2RISE = w_MMF2RISE;
                ,MMFLOMAG = w_MMFLOMAG;
                ,MMVOCCEN = w_MMVOCCEN;
                ,MMCODCEN = w_MMCODCEN;
                ,MMCODCOM = w_MMCODCOM;
                ,MM_SEGNO = w_MM_SEGNO;
                ,MMINICOM = w_MMINICOM;
                ,MMFINCOM = w_MMFINCOM;
                ,MMFLRIPA = w_MMFLRIPA;
                ,MMFLLOTT = w_MMFLLOTT;
                ,MMF2LOTT = w_MMF2LOTT;
                ,MMCODBUN = w_MMCODBUN;
                &i_ccchkf. ;
             where;
                MMSERIAL = this.w_MMSERIAL;
                and MMNUMRIF = this.w_RIFERIME;
                and CPROWNUM = this.oParentObject.ULT_RNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if this.oParentObject.w_IMAGGSAL="S"
          * --- Aggiornamento Saldi Articoli
          * --- Variabili da w_MM in w_MV per compatibilit� con Pag 4 Aggiornamento Saldi Articoli
          w_MVKEYSAL = w_MMKEYSAL
          w_MVTIPRIG = "R"
          w_MVCODART = w_MMCODART
          w_MVCODVAR = w_MMCODVAR
          w_MVCODMAG = w_MMCODMAG
          w_MVCODMAT = w_MMCODMAT
          w_MVQTAUM1 = w_MMQTAUM1
          w_MVFLCASC = w_MMFLCASC
          w_MVFLRISE = w_MMFLRISE
          w_MVFLORDI = w_MMFLORDI
          w_MVFLIMPE = w_MMFLIMPE
          w_MVF2CASC = w_MMF2CASC
          w_MVF2RISE = w_MMF2RISE
          w_MVF2ORDI = w_MMF2ORDI
          w_MVF2IMPE = w_MMF2IMPE
          w_MVFLULPV = w_MMFLULPV
          w_MVFLULCA = w_MMFLULCA
          this.w_ARCH = "M"
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into LISTART
          i_nConn=i_TableProp[this.LISTART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LISTART_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"KEYSAL"+",ACQVEN"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(w_MVCODART),'LISTART','KEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLAVA1),'LISTART','ACQVEN');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'KEYSAL',w_MVCODART,'ACQVEN',this.w_FLAVA1)
            insert into (i_cTable) (KEYSAL,ACQVEN &i_ccchkf. );
               values (;
                 w_MVCODART;
                 ,this.w_FLAVA1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.oParentObject.w_Destinaz = "DT"
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_Destinaz = "DR"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_Destinaz = "DO"
        if this.oParentObject.w_CORRETTO
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
    return
  proc Try_045CC188()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_045DFBC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_045DD2E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Righe Documenti Attivi/Passivi - Ricerca Serial Documento ed Inizializzazione Variabili Riga Dettaglio
    * --- Lettura dati causale magazzino
    this.w_CMFLAVAL = ""
    this.w_CAUCOL = space(5)
    if .not. empty(w_MVCAUMAG)
      if g_APPLICATION="ADHOC REVOLUTION"
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLAVAL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLELGM,CMCAUCOL"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(w_MVCAUMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLAVAL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLELGM,CMCAUCOL;
            from (i_cTable) where;
                CMCODICE = w_MVCAUMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CMFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          w_MVFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          w_MVFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          w_MVFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          w_MVFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          w_MVFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
          this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLAVAL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLANAL,CMFLELGM,CMFLELAN,CMCAUCOL"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(w_MVCAUMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLAVAL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLANAL,CMFLELGM,CMFLELAN,CMCAUCOL;
            from (i_cTable) where;
                CMCODICE = w_MVCAUMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CMFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          w_MVFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          w_MVFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          w_MVFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          w_MVFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          w_MV_SEGNO = NVL(cp_ToDate(_read_.CMFLANAL),cp_NullValue(_read_.CMFLANAL))
          w_MVFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
          w_MVFLELAN = NVL(cp_ToDate(_read_.CMFLELAN),cp_NullValue(_read_.CMFLELAN))
          this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    w_MVCAUCOL = iif( empty(w_MVCAUCOL), this.w_CAUCOL, w_MVCAUCOL )
    if .not. empty(w_MVCAUCOL)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(w_MVCAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE;
          from (i_cTable) where;
              CMCODICE = w_MVCAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_MVF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        w_MVF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        w_MVF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        w_MVF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Lettura dati valuta documento e valuta nazionale
    this.w_DecTot = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DecTot = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    w_MVVALNAZ = iif(empty(w_MVVALNAZ), g_PERVAL, w_MVVALNAZ)
    this.w_NzCaoVal = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(w_MVVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL;
        from (i_cTable) where;
            VACODVAL = w_MVVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NzCaoVal = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Valori di default
    if empty( w_MVKEYSAL ) .and. w_MVTIPRIG="R" .and. this.w_CmFlaVal <>"N"
      w_MVKEYSAL = LEFT(w_MVCODART + SPACE(20), 20)
    endif
    if (.not. empty( w_MVKEYSAL )) .and. (w_MVTIPRIG#"R" .or. this.w_CmFlaVal ="N")
      w_MVKEYSAL = space(20)
    endif
    if empty( w_MVCODICE )
      w_MVCODICE = left(trim(w_MVCODART) + space(20), 20)
    endif
    * --- Codice Magazzino
    if empty( w_MVKEYSAL ) OR w_MVTIPRIG<>"R" OR this.w_CmFlaVal="N"
      w_MVCODMAG = space(5)
    endif
    w_MVFLOMAG = iif(empty(w_MVFLOMAG), "X", w_MVFLOMAG)
    w_MVFLULPV = iif( ( this.w_CMFLAVAL = "V" AND w_MVFLCASC = "-" AND w_MVFLOMAG <> "S" AND (w_MVCLADOC <> "DT" OR w_MVPREZZO <> 0 ) ), "=", " ")
    w_MVFLULCA = iif( ( this.w_CMFLAVAL = "A" AND w_MVFLCASC = "+" AND w_MVFLOMAG <> "S" AND (w_MVCLADOC <> "DT" OR w_MVPREZZO <> 0 ) ), "=", " ")
    * --- Lettura flag Dati Analitica, Movimento di Analitica e Gestione Progetti e Segno Analitica
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLANAL,TDFLELAN,TDFLCOMM,TDFLVEAC,TD_SEGNO,TDRIPINC,TDRIPTRA,TDRIPIMB"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLANAL,TDFLELAN,TDFLCOMM,TDFLVEAC,TD_SEGNO,TDRIPINC,TDRIPTRA,TDRIPIMB;
        from (i_cTable) where;
            TDTIPDOC = w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
      this.w_FLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
      this.w_FLCOMM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
      this.w_COSRIC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
      this.w_TD_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
      this.w_RIPINC = NVL(cp_ToDate(_read_.TDRIPINC),cp_NullValue(_read_.TDRIPINC))
      this.w_RIPTRA = NVL(cp_ToDate(_read_.TDRIPTRA),cp_NullValue(_read_.TDRIPTRA))
      this.w_RIPIMB = NVL(cp_ToDate(_read_.TDRIPIMB),cp_NullValue(_read_.TDRIPIMB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    w_MV_SEGNO = iif( empty(NVL(w_MV_SEGNO," ")),this.w_TD_SEGNO,w_MV_SEGNO)
    if g_PERCCR="S"
      * --- Il campo che segue viene sempre valorizzato con quanto specificato nella cusale documento, ignorando quanto viene specificato nell'origine (tracciati o valori predefiniti)
      w_MVFLELAN = this.w_FLELAN
    endif
    * --- Lettura dati articolo
    store " " to w_UNMIS1, w_UNMIS2, w_UNMIS3, w_OPERA3, w_OPERAT, w_IVALIS
    store 0 to w_MOLTIP, w_MOLTI3, w_PERIVA
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARCODCLA,ARCODIVA,ARCATCON,ARVOCCEN,ARVOCRIC,ARFLUSEP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARCODCLA,ARCODIVA,ARCATCON,ARVOCCEN,ARVOCRIC,ARFLUSEP;
        from (i_cTable) where;
            ARCODART = w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
      this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
      this.w_CATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
      this.w_VOCCEN = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
      this.w_VOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
      this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    w_MVCODIVA = iif( empty(w_MVCODIVA), this.w_CODIVA, w_MVCODIVA )
    w_MVCATCON = iif( empty(w_MVCATCON), this.w_CATCON, w_MVCATCON )
    w_MVVOCCEN = iif( empty(w_MVVOCCEN), IIF(this.w_COSRIC="A", this.w_VOCCEN, this.w_VOCRIC), w_MVVOCCEN )
    do case
      case g_COMM="S" AND this.w_FLCOMM="S" AND (EMPTY(w_MVCODCOM) OR EMPTY(w_MVVOCCEN)) AND w_MVTIPRIG $ "MFRA"
        this.oParentObject.w_ResoMode = "NOANACOM"
        this.oParentObject.Pag4()
      case g_PERCCR="S" AND this.w_FLANAL="S" AND (EMPTY(w_MVCODCEN) OR EMPTY(w_MVVOCCEN)) AND w_MVTIPRIG $ "MFRA"
        this.oParentObject.w_ResoMode = "NOANALIT"
        this.oParentObject.Pag4()
    endcase
    do case
      case w_MVTIPRIG="F"
        w_MVUNIMIS = w_UNMIS1
      case w_MVTIPRIG="M"
        if EMPTY(w_MVUNIMIS)
          * --- Controllo della presenza dell'unit� di misura nell'origine.
          this.oParentObject.w_ResoMode = "NOUNIMIS"
          this.oParentObject.Pag4()
        else
          * --- Controllo della presenza dell'unit� di misura dell'origine nell'archivio delle unit� di misura di AHR.
          i_rows = 0
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(w_MVUNIMIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  UMCODICE = w_MVUNIMIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            this.oParentObject.w_ResoMode = "NOUNIMIS"
            this.oParentObject.Pag4()
          endif
        endif
    endcase
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAOPERAT,CAUNIMIS,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(w_MVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAOPERAT,CAUNIMIS,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = w_MVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura aliquota iva
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(w_MVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIVA;
        from (i_cTable) where;
            IVCODIVA = w_MVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_PerIva = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura dati listino
    * --- Read from LISTINI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LISTINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LSIVALIS"+;
        " from "+i_cTable+" LISTINI where ";
            +"LSCODLIS = "+cp_ToStrODBC(w_MVCODLIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LSIVALIS;
        from (i_cTable) where;
            LSCODLIS = w_MVCODLIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Valori di default
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLFRAZ"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(w_UNMIS1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLFRAZ;
        from (i_cTable) where;
            UMCODICE = w_UNMIS1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    w_UMCAL = LEFT(w_UNMIS1+"   ",3)+LEFT(w_UNMIS2+"   ",3)+LEFT(w_UNMIS3+"   ",3)+LEFT(w_MVUNIMIS+"   ",3)+LEFT(w_OPERAT+" ",1)+LEFT(w_OPERA3+" ",1)
    msg=" "
    w_MVQTAUM1=iif(not empty(w_MVQTAUM1),w_MVQTAUM1, iif( empty(w_MVQTAUM1).and.(!empty(w_MVQTAMOV)) ,CALQTA(w_MVQTAMOV,LEFT(w_MVUNIMIS+"   ",3), LEFT(w_UNMIS2+"   ",3),LEFT(w_OPERAT+" ",1), w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, "N", @msg, LEFT(w_UNMIS3+"   ",3), LEFT(w_OPERA3+" ",1), w_MOLTI3,,"I"),w_MVQTAMOV))
    w_MVQTAEV1 =CALQTA(w_MVQTAEVA, LEFT(w_MVUNIMIS+"   ",3), LEFT(w_UNMIS2+"   ",3),LEFT(w_OPERAT+" ",1), w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, "N", @msg, LEFT(w_UNMIS3+"   ",3), LEFT(w_OPERA3+" ",1), w_MOLTI3,,"I")
    if Not Empty(msg)
      this.oParentObject.w_ResoDett = ah_msgformat("Serial %1 numero registrazione %2 del %3 riga %4 %5", trim(this.w_MVSERIAL), alltrim(str(w_MVNUMDOC,15,0)), dtoc(w_MVDATDOC), ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) , msg )
      * --- Raise
      i_Error="Quantit� della 1a UM incongruente"
      return
    endif
    w_MVVALRIG = iif( empty(w_MVVALRIG), CAVALRIG(w_MVPREZZO,w_MVQTAMOV, w_MVSCONT1,w_MVSCONT2,w_MVSCONT3,w_MVSCONT4,this.w_DECTOT), cp_Round(w_MVVALRIG,this.w_DECTOT))
    if empty(w_MVVALMAG)
      * --- Funzione per calcolo MVVALMAG: Considero che l'Iva agevolata non pu� avere aliquota; solo esente
      w_MVVALMAG = CAVALMAG(w_MVFLSCOR, w_MVVALRIG, w_MVIMPSCO, w_MVIMPACC, w_PERIVA, this.w_DECTOT, w_MVCODIVE, 0 )
    endif
    if empty(w_MVIMPNAZ)
      * --- Funzine per calcolo MVIMPNAZ: non considero Iva indetraibile (ultimi 5 parametri vuoti)
      w_MVIMPNAZ = CAIMPNAZ(w_MVFLVEAC, w_MVVALMAG, w_MVCAOVAL, this.w_NZCAOVAL, w_MVDATDOC, w_MVVALNAZ, w_MVCODVAL, " ", 0, 0, 0, 0 )
    endif
    * --- Quantita' aggiornamento saldi (ordinato, impegnato e riservato)
    if empty( w_MVQTASAL )
      w_MVQTASAL = IIF(w_MVFLEVAS="S" .or. w_MVQTAEV1>w_MVQTAUM1, 0, w_MVQTAUM1-w_MVQTAEV1)
    endif
    * --- Tipo provvigione
    * --- Se il tipo import � da ODBC forzo comunque il valore
    if empty(w_MVTIPPRO) OR this.oParentObject.w_tipoimp="O"
      if w_MVFLOMAG<>"X"
        * --- Se ho il flag omaggio il tipo provv. � 'esclusa'
         
 w_MVTIPPRO="ES" 
 w_MVPERPRO=0 
 w_MVTIPPR2="ES" 
 w_MVPROCAP=0
      else
        if w_MVPERPRO<>0
          * --- Se ho una percentuale il tipo provv. � 'forzata'
          w_MVTIPPRO="FO"
        else
          * --- Altrimenti dipende dai parametri provvigioni
          w_MVTIPPRO=IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        endif
        if w_MVPROCAP<>0
          * --- Se ho una percentuale il tipo provv. � 'forzata'
          w_MVTIPPR2="FO"
        else
          * --- Altrimenti dipende dai parametri provvigioni
          w_MVTIPPR2=IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Saldi Articoli
    if this.oParentObject.w_IMAGGSAL="S" .and. (.not. empty( w_MVKEYSAL )) .and. w_MVTIPRIG="R" .and. this.w_CmFlaVal <>"N"
      if this.w_ARCH="M" AND g_MADV="S" AND g_PERLOT="S"
        * --- Aggiorna saldi per i movimenti di magazzino
        GSMD_BRL (this, this.w_MMSERIAL , "M" , "+" , this.oParentObject.ULT_RNUM ,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Aggiornamento saldi articoli magazzino principale
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if .not. empty(NVL(w_MVCODMAG,""))
        * --- Try
        local bErr_0466BA88
        bErr_0466BA88=bTrsErr
        this.Try_0466BA88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0466BA88
        * --- End
        * --- Try
        local bErr_0465B288
        bErr_0465B288=bTrsErr
        this.Try_0465B288()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_ResoMode = "SOLOMESS"
          this.oParentObject.w_ResoTipo = "E"
          this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDIART %1%0serial %2 numero registrazione %3 riga %4", iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
          this.oParentObject.Pag4()
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0465B288
        * --- End
        if this.w_SALCOM="S"
          if empty(nvl(w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = w_MVCODCOM
          endif
          * --- Try
          local bErr_0465C458
          bErr_0465C458=bTrsErr
          this.Try_0465C458()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0465C458
          * --- End
          * --- Try
          local bErr_0465CEA8
          bErr_0465CEA8=bTrsErr
          this.Try_0465CEA8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICOM %1%0serial %2 numero registrazione %3 riga %4", iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0465CEA8
          * --- End
        endif
      endif
      * --- Aggiornamento saldi articoli magazzino collegato
      if .not. empty(NVL(w_MVCODMAT,""))
        * --- Try
        local bErr_0462DEF8
        bErr_0462DEF8=bTrsErr
        this.Try_0462DEF8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0462DEF8
        * --- End
        * --- Try
        local bErr_0462E6A8
        bErr_0462E6A8=bTrsErr
        this.Try_0462E6A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.oParentObject.w_ResoMode = "SOLOMESS"
          this.oParentObject.w_ResoTipo = "E"
          this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi del magazzino collegato nella tabella SALDIART %1%0serial %2 numero registrazione %3 riga %4", iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
          this.oParentObject.Pag4()
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0462E6A8
        * --- End
        if this.w_SALCOM="S"
          if empty(nvl(w_MVCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = w_MVCODCOM
          endif
          * --- Try
          local bErr_0462FE48
          bErr_0462FE48=bTrsErr
          this.Try_0462FE48()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0462FE48
          * --- End
          * --- Try
          local bErr_04630478
          bErr_04630478=bTrsErr
          this.Try_04630478()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICOM %1%0serial %2 numero registrazione %3 riga %4", iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04630478
          * --- End
        endif
      endif
    endif
  endproc
  proc Try_0466BA88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',w_MVKEYSAL,'SLCODMAG',w_MVCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           w_MVKEYSAL;
           ,w_MVCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0465B288()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(w_MVFLCASC,'SLQTAPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_MVFLRISE,'SLQTRPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_MVFLORDI,'SLQTOPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_MVFLIMPE,'SLQTIPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(w_MVCODMAG);
             )
    else
      update (i_cTable) set;
          SLQTAPER = &i_cOp1.;
          ,SLQTRPER = &i_cOp2.;
          ,SLQTOPER = &i_cOp3.;
          ,SLQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SLCODICE = w_MVKEYSAL;
          and SLCODMAG = w_MVCODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if !g_APPLICATION="ADHOC REVOLUTION"
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLCODART ="+cp_NullLink(cp_ToStrODBC(w_MVCODART),'SALDIART','SLCODART');
        +",SLCODVAR ="+cp_NullLink(cp_ToStrODBC(w_MVCODVAR),'SALDIART','SLCODVAR');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(w_MVCODMAG);
               )
      else
        update (i_cTable) set;
            SLCODART = w_MVCODART;
            ,SLCODVAR = w_MVCODVAR;
            &i_ccchkf. ;
         where;
            SLCODICE = w_MVKEYSAL;
            and SLCODMAG = w_MVCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_0465C458()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',w_MVKEYSAL,'SCCODMAG',w_MVCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           w_MVKEYSAL;
           ,w_MVCODMAG;
           ,this.w_COMMAPPO;
           ,w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0465CEA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(w_MVFLCASC,'SCQTAPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_MVFLRISE,'SCQTRPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_MVFLORDI,'SCQTOPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_MVFLIMPE,'SCQTIPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(w_MVCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTRPER = &i_cOp2.;
          ,SCQTOPER = &i_cOp3.;
          ,SCQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = w_MVKEYSAL;
          and SCCODMAG = w_MVCODMAG;
          and SCCODCAN = this.w_COMMAPPO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return
  proc Try_0462DEF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODMAT),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',w_MVKEYSAL,'SLCODMAG',w_MVCODMAT)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           w_MVKEYSAL;
           ,w_MVCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0462E6A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(w_MVF2CASC,'SLQTAPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_MVF2RISE,'SLQTRPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_MVF2ORDI,'SLQTOPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_MVF2IMPE,'SLQTIPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
             )
    else
      update (i_cTable) set;
          SLQTAPER = &i_cOp1.;
          ,SLQTRPER = &i_cOp2.;
          ,SLQTOPER = &i_cOp3.;
          ,SLQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SLCODICE = w_MVKEYSAL;
          and SLCODMAG = w_MVCODMAT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if !g_APPLICATION="ADHOC REVOLUTION"
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLCODART ="+cp_NullLink(cp_ToStrODBC(w_MVCODART),'SALDIART','SLCODART');
        +",SLCODVAR ="+cp_NullLink(cp_ToStrODBC(w_MVCODVAR),'SALDIART','SLCODVAR');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
               )
      else
        update (i_cTable) set;
            SLCODART = w_MVCODART;
            ,SLCODVAR = w_MVCODVAR;
            &i_ccchkf. ;
         where;
            SLCODICE = w_MVKEYSAL;
            and SLCODMAG = w_MVCODMAT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_0462FE48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',w_MVKEYSAL,'SCCODMAG',w_MVCODMAT,'SCCODCAN',this.w_COMMAPPO,'SCCODART',w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           w_MVKEYSAL;
           ,w_MVCODMAT;
           ,this.w_COMMAPPO;
           ,w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04630478()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(w_MVF2CASC,'SCQTAPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_MVF2RISE,'SCQTRPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_MVF2ORDI,'SCQTOPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_MVF2IMPE,'SCQTIPER','w_MVQTAUM1',w_MVQTAUM1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
      +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
             )
    else
      update (i_cTable) set;
          SCQTAPER = &i_cOp1.;
          ,SCQTRPER = &i_cOp2.;
          ,SCQTOPER = &i_cOp3.;
          ,SCQTIPER = &i_cOp4.;
          &i_ccchkf. ;
       where;
          SCCODICE = w_MVKEYSAL;
          and SCCODMAG = w_MVCODMAT;
          and SCCODCAN = this.w_COMMAPPO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore Aggiornamento Saldi Commessa'
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prende Dati dichiarazione di intento
    if w_MVTIPCON $ "CF" AND EMPTY(w_MVCODIVE) AND NOT EMPTY(w_MVCODCON) 
      * --- Se Cliente/Fornitore e no Codice Iva Non Imponibile
      * --- Cerca la Dichiarazione di Intento Valida
      this.w_OK = .F.
      this.w_TIPOPE = "X"
      this.w_IMPDIC = 0
      this.w_IMPUTI = 0
      this.w_CODIVE = SPACE(5)
      this.w_RIFDIC = SPACE(10)
      this.w_NDIC = 0
      this.w_ALFADIC = Space(2)
      this.w_ADIC = SPACE(4)
      this.w_DDIC = cp_CharToDate("  -  -  ")
      this.w_TDIC = "X"
      this.w_DICODCON = w_MVCODCON
      this.w_DITIPCON = w_MVTIPCON
      this.w_DIDATDOC = w_MVDATDOC
      * --- Lettura lettera di intento valida
      DECLARE ARRDIC (14,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRDIC(1)=0
      this.w_OK_LET = CAL_LETT(this.w_DIDATDOC,this.w_DITIPCON,this.w_DICODCON, @ArrDic)
      if this.w_OK_LET
        * --- Parametri
        *     pDatRif : Data di Riferimento per filtro su Lettere di intento
        *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
        *     pCodCon : Codice Cliente/Fornitore
        *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
        *     
        *     pArrDic[ 1 ]   = Numero Dichiarazione
        *     pArrDic[ 2 ]   = Tipo Operazione
        *     pArrDic[ 3 ]   = Anno Dichiarazione
        *     pArrDic[ 4 ]   = Importo Dichiarazione
        *     pArrDic[ 5 ]   = Data Dichiarazione
        *     pArrDic[ 6 ]   = Importo Utilizzato
        *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
        *     pArrDic[ 8 ]   = Codice Iva Agevolata
        *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
        *     pArrDic[ 10 ] = Data Inizio Validit�
        *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
        *     pArrDic[ 12 ] = Data Obsolescenza
        this.w_NDIC = ArrDic(1)
        this.w_TIPOPE = ArrDic(2)
        this.w_ADIC = ArrDic(3)
        this.w_IMPDIC = ArrDic(4)
        this.w_DDIC = ArrDic(5)
        this.w_IMPUTI = ArrDic(6)
        this.w_TDIC = ArrDic(7)
        this.w_CODIVE = ArrDic(8)
        this.w_RIFDIC = ArrDic(11)
        this.w_ALFADIC = ArrDic(13)
      endif
      do case
        case this.w_TIPOPE = "I"
          * --- Importo Definito OK Se Importo Dichiarato>Importo Utilizzato
          if this.w_IMPDIC>this.w_IMPUTI
            this.w_OK = .T.
          else
            AH_ERRORMSG( "Importo disponibile della dichiarazione di esenzione esaurito!%0Rif. N. %1/%27%3 del %4","!","",ALLTRIM(STR(this.w_NDIC)) , this.w_ADIC , this.w_ALFADIC , DTOC(this.w_DDIC) )
          endif
        case this.w_TIPOPE = "O"
          * --- Operazione Specifica
          this.w_OK = .T.
          this.oParentObject.w_ResoMode = "SOLOMESS"
          this.oParentObject.w_ResoTipo = "S"
          this.oParentObject.w_ResoMess = "Verificare dichiarazione di intento (operazione specifica)"
          this.oParentObject.Pag4()
        case this.w_TIPOPE = "D"
          * --- Periodo Definito
          this.w_OK = .T.
      endcase
      * --- Se Ok riporta i dati dell'Esenzione
      w_MVCODIVE = iif(empty(this.w_CODIVE)," ",this.w_CODIVE)
      w_MVRIFDIC =this.w_RIFDIC
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Testate Documenti Attivi/Passivi
    * --- Testate Documenti
    this.w_MVSERIAL = space(10)
    this.w_ESISTE = .t.
    this.w_LETTO = ""
    if empty( NVL( w_MVCODCON , "" ) )
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVNUMDOC = "+cp_ToStrODBC(w_MVNUMDOC);
              +" and MVDATDOC = "+cp_ToStrODBC(w_MVDATDOC);
              +" and MVALFDOC = "+cp_ToStrODBC(w_MVALFDOC);
              +" and MVTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL;
          from (i_cTable) where;
              MVNUMDOC = w_MVNUMDOC;
              and MVDATDOC = w_MVDATDOC;
              and MVALFDOC = w_MVALFDOC;
              and MVTIPDOC = w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LETTO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVNUMDOC = "+cp_ToStrODBC(w_MVNUMDOC);
              +" and MVDATDOC = "+cp_ToStrODBC(w_MVDATDOC);
              +" and MVALFDOC = "+cp_ToStrODBC(w_MVALFDOC);
              +" and MVTIPCON = "+cp_ToStrODBC(w_MVTIPCON);
              +" and MVCODCON = "+cp_ToStrODBC(w_MVCODCON);
              +" and MVTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL;
          from (i_cTable) where;
              MVNUMDOC = w_MVNUMDOC;
              and MVDATDOC = w_MVDATDOC;
              and MVALFDOC = w_MVALFDOC;
              and MVTIPCON = w_MVTIPCON;
              and MVCODCON = w_MVCODCON;
              and MVTIPDOC = w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LETTO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if empty(this.w_LETTO)
      this.w_ESISTE = .f.
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
      w_MVCODESE = iif( empty(w_MVCODESE), g_CodEse, w_MVCODESE )
      w_MVCODUTE = iif(g_MAGUTE="S", 0, iif(empty(w_MVCODUTE), i_codute, w_MVCODUTE ))
      w_MVTIPCON = NVL(w_MVTIPCON," ")
      w_MVAGG_01 = iif( empty(w_MVAGG_01), "", w_MVAGG_01 )
      w_MVAGG_02 = iif( empty(w_MVAGG_02), "", w_MVAGG_02 )
      w_MVAGG_03 = iif( empty(w_MVAGG_03), "", w_MVAGG_03 )
      w_MVAGG_04 = iif( empty(w_MVAGG_04), "", w_MVAGG_04 )
      w_MVAGG_05 = iif( empty(w_MVAGG_05),cp_CharToDate("   -  -    "), w_MVAGG_05 )
      w_MVAGG_06 = iif( empty(w_MVAGG_06),cp_CharToDate("   -  -    "), w_MVAGG_06 )
      * --- Il tipo documento � obbligatorio
      if .not. empty(w_MVTIPDOC)
        w_TDCODMAG = " "
        w_TDALFDOC = " "
        w_TDPRODOC = " "
        w_TDFLPDOC = " "
        w_TDCAUCON = " "
        w_CCTIPREG = " "
        w_CCFLPPRO = " "
        * --- Legge causale documenti
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDALFDOC,TDCATDOC,TDCAUCON,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLPPRO,TDFLVEAC,TDPRODOC,TFFLRAGG,TDFLANAL,TDRIPINC,TDRIPTRA,TDRIPIMB,TDEMERIC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDALFDOC,TDCATDOC,TDCAUCON,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLPPRO,TDFLVEAC,TDPRODOC,TFFLRAGG,TDFLANAL,TDRIPINC,TDRIPTRA,TDRIPIMB,TDEMERIC;
            from (i_cTable) where;
                TDTIPDOC = w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_TDALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
          w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          w_TDCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
          w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          w_TDCODMAG = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
          w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
          w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_TDFLPPRO = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
          w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          w_TDPRODOC = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
          w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          this.w_RIPINC = NVL(cp_ToDate(_read_.TDRIPINC),cp_NullValue(_read_.TDRIPINC))
          this.w_RIPTRA = NVL(cp_ToDate(_read_.TDRIPTRA),cp_NullValue(_read_.TDRIPTRA))
          this.w_RIPIMB = NVL(cp_ToDate(_read_.TDRIPIMB),cp_NullValue(_read_.TDRIPIMB))
          w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se non gestita la contabilit� oppure documento non contabilizzabile
        *      non inizializzo mai la casuale contabile
        if g_COGE="S" AND w_MVCLADOC $ "FA-NC-RF"
          w_MVCAUCON = iif(empty(w_MVCAUCON),w_TDCAUCON,w_MVCAUCON)
        else
          w_MVCAUCON = Space(5)
        endif
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCTIPREG,CCFLPPRO"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(w_MVCAUCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCTIPREG,CCFLPPRO;
            from (i_cTable) where;
                CCCODICE = w_MVCAUCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_CCTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
          w_CCFLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if w_MVFLVEAC="V" and w_MVEMERIC<>"A"
          * --- Documento di Vendita, NO documento ricevuto (es.: reso da cliente)
          w_MVDATREG = w_MVDATDOC
          w_MVNUMREG = w_MVNUMDOC
          w_MVPRD = w_TDPRODOC
          w_MVPRP = IIF(w_CCTIPREG="A" AND w_MVCLADOC $ "FA-NC", "AC", "NN")
        endif
        * --- Valorizzo flag di ripartizione spese con default imposatazione della causale documento
        if TYPE( "w_MVFLRIMB" ) = "U"
          w_MVFLRIMB=this.w_RIPIMB
        else
          w_MVFLRIMB=IIF(Not Empty(w_MVFLRIMB),w_MVFLRIMB,this.w_RIPIMB)
        endif
        if TYPE( "w_MVFLRINC" ) = "U"
          w_MVFLRINC=this.w_RIPINC
        else
          w_MVFLRINC=IIF(Not Empty(w_MVFLRINC),w_MVFLRINC,this.w_RIPINC)
        endif
        if TYPE( "w_MVFLRTRA" ) = "U"
          w_MVFLRTRA=this.w_RIPTRA
        else
          w_MVFLRTRA=IIF(Not Empty(w_MVFLRTRA),w_MVFLRTRA,this.w_RIPTRA)
        endif
        if w_MVFLVEAC="A" or w_MVEMERIC="A"
          * --- Documento di Acquisto
          * --- Legge causale documenti per acquisti
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLPDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLPDOC;
              from (i_cTable) where;
                  TDTIPDOC = w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_TDFLPDOC = NVL(cp_ToDate(_read_.TDFLPDOC),cp_NullValue(_read_.TDFLPDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          w_MVDATREG = iif( empty(w_MVDATREG), i_datsys, w_MVDATREG )
          w_MVNUMREG = w_MVNUMDOC
          w_MVPRD = iif(w_TDFLPDOC="S",iif(w_MVCLADOC="DT","DV",iif(w_MVCLADOC="DI","IV",w_TDPRODOC)),w_TDPRODOC)
          w_MVPRP = IIF(w_CCTIPREG="A" AND w_MVCLADOC $ "FA-NC", "AC", "NN")
          if TYPE( "w_MVFLRIMB" ) = "U"
            if w_MVFLVEAC = "A"
              w_MVFLRIMB="S"
            else
              w_MVFLRIMB=" "
            endif
          endif
          if TYPE( "w_MVFLRINC" ) = "U"
            if w_MVFLVEAC = "A"
              w_MVFLRINC="S"
            else
              w_MVFLRINC=" "
            endif
          endif
          if TYPE( "w_MVFLRTRA" ) = "U"
            if w_MVFLVEAC = "A"
              w_MVFLRTRA="S"
            else
              w_MVFLRTRA=" "
            endif
          endif
          if w_MVPRP="AC" or w_MVEMERIC="A"
            * --- Numerazione protocollo
            *     - Solo per FA e NC e documenti ricevuti (es.: DT reso da cliente)
            *     - No per ORDINI
            *     - No per Documenti di Acquisto
            *     
            * --- - MVANNPRO = esercizio per calcolo protocollo
            *     - MVALFEST = alfanumerico protocollo
            *     - MVNUMEST = numero protocollo
            * --- Esercizio
            if empty(w_MVANNPRO)
              this.w_FLPPRO = IIF(this.w_TDFLPPRO="P" OR w_MVCLADOC $ "FA-NC", w_CCFLPPRO, this.w_TDFLPPRO)
              w_MVANNPRO = CALPRO(w_MVDATREG,w_MVCODESE,this.w_FLPPRO)
            endif
            * --- Alfa
            w_MVALFEST = iif( empty(w_MVALFEST), this.w_TDALFEST, w_MVALFEST )
            * --- Aggiorna progressivo Numero Protocollo
            if not empty(w_MVANNPRO)
              w_MVNUMEST = gsim_bpr(this,"PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST",w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      endif
      w_MVDATCIV = iif( empty(w_MVDATCIV), w_MVDATREG, w_MVDATCIV )
      w_MVTIPORN = iif( empty(w_MVTIPORN), w_MVTIPCON, w_MVTIPORN )
      if empty(w_MVFLSCOR) or empty(w_MVCODIVE)
        * --- Lettura anagrafica clienti/fornitori/conti
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANSCORPO,ANCODIVA"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANSCORPO,ANCODIVA;
            from (i_cTable) where;
                ANTIPCON = w_MVTIPCON;
                and ANCODICE = w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANSCORPO = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
          this.w_ANCODIVA = NVL(cp_ToDate(_read_.ANCODIVA),cp_NullValue(_read_.ANCODIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty (w_MVCODIVE)
          * --- Valorizzazione esenzione
          w_MVCODIVE = this.w_ANCODIVA
          if w_MVTIPCON $ "CF" AND EMPTY(w_MVCODIVE) AND NOT EMPTY(w_MVCODCON) 
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if empty(w_MVFLSCOR)
          * --- Valorizzazione flag scorporo
          w_MVFLSCOR = this.w_ANSCORPO
        endif
      endif
      * --- Lettura valuta nazionale esercizio
      if empty( w_MVVALNAZ )
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESVALNAZ"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(w_MVCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESVALNAZ;
            from (i_cTable) where;
                ESCODAZI = i_CODAZI;
                and ESCODESE = w_MVCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_MVVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Lettura cambio valuta
      if (.not. empty( w_MVCODVAL )) .and. empty( w_MVCAOVAL )
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(w_MVCODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL;
            from (i_cTable) where;
                VACODVAL = w_MVCODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_MVCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- --
      w_MVFLPROV = iif( empty(w_MVFLPROV), "N", w_MVFLPROV )
      w_MVANNDOC = iif( empty(w_MVANNDOC), str(year(w_MVDATDOC),4,0), w_MVANNDOC )
      * --- Codici IVA di testata: Trasporto, Imballo, Incasso e Bolli
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(g_COITRA)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(g_COITRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = g_COITRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COPERTRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_COPERTRA = 0
      endif
      if NOT EMPTY(g_COIIMB)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(g_COIIMB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = g_COIIMB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COPERIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_COPERIMB = 0
      endif
      if NOT EMPTY(g_COIINC)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(g_COIINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = g_COIINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COPERINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_COPERINC = 0
      endif
      w_MVIVATRA = IIF(empty(w_MVIVATRA),IIF(w_MVFLVEAC="V", IIF(NOT EMPTY(w_MVCODIVE),IIF(this.w_PERIVE<this.w_COPERTRA,w_MVCODIVE,g_COITRA), g_COITRA), SPACE(5)),w_MVIVATRA)
      w_MVIVAIMB = IIF(empty(w_MVIVAIMB),IIF(w_MVFLVEAC="V", IIF(NOT EMPTY(w_MVCODIVE), IIF(this.w_PERIVE<this.w_COPERIMB,w_MVCODIVE,g_COIIMB), g_COIIMB), SPACE(5)),w_MVIVAIMB)
      w_MVIVAINC = IIF(empty(w_MVIVAINC),IIF(w_MVFLVEAC="V", IIF(NOT EMPTY(w_MVCODIVE), IIF(this.w_PERIVE<this.w_COPERINC,w_MVCODIVE,g_COIINC), g_COIINC), SPACE(5)),w_MVIVAINC)
      w_MVIVABOL = IIF(empty(w_MVIVABOL),IIF(w_MVFLVEAC="V", g_COIBOL, SPACE(5)),w_MVIVABOL)
      this.w_FLVABD = " "
      do case
        case w_MVTIPCON="C"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZFLVEBD"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZFLVEBD;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVABD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case w_MVTIPCON="F" AND not empty(w_MVCODCON)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANFLACBD"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANFLACBD;
              from (i_cTable) where;
                  ANTIPCON = w_MVTIPCON;
                  and ANCODICE = w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVABD = NVL(cp_ToDate(_read_.ANFLACBD),cp_NullValue(_read_.ANFLACBD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
      w_MVFLVABD = IIF(Not empty(w_MVFLVABD),w_MVFLVABD,this.w_FLVABD)
      * --- Aggiorna progressivo Numero Documento
      if w_MVPRD<>"NN"
        w_MVNUMDOC = gsim_bpr(this,"PRDOC","i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC",w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Lettura del Codice Aspetto Esteriore dei Beni in funzione della descrizione
      i_rows = 0
      if NOT EMPTY(w_MVASPEST) AND EMPTY(w_MVCODASP)
        * --- Try
        local bErr_046A90F0
        bErr_046A90F0=bTrsErr
        this.Try_046A90F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_046A90F0
        * --- End
      endif
      if Type("w_MVNUMCOR")= "U"
        * --- Se non � definito sul tracciato..
        w_MVNUMCOR = Space(25)
      endif
      * --- Se conto corrente non definito e importato tramite tracciato lo leggo dal cliente
      if Empty(w_MVNUMCOR)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANNUMCOR"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANNUMCOR;
            from (i_cTable) where;
                ANTIPCON = w_MVTIPCON;
                and ANCODICE = w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_MVNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      ah_msg( "Elaborazione %1 - record %2 di %3" , .t. , , , trim(this.oParentObject.w_Archivio) , alltrim(str(this.oParentObject.NumReco,10)) , alltrim(str(this.oParentObject.NumTotReco,10)) )
      * --- Inserimento record master
      * --- Insert into DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MVSERIAL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL)
        insert into (i_cTable) (MVSERIAL &i_ccchkf. );
           values (;
             this.w_MVSERIAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc
  proc Try_046A90F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from ASPETTO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ASPETTO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASPETTO_idx,2],.t.,this.ASPETTO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ASCODASP"+;
        " from "+i_cTable+" ASPETTO where ";
            +"ASDESASP = "+cp_ToStrODBC(w_MVASPEST);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ASCODASP;
        from (i_cTable) where;
            ASDESASP = w_MVASPEST;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_MVCODASP = NVL(cp_ToDate(_read_.ASCODASP),cp_NullValue(_read_.ASCODASP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Seriale
    this.w_MVSERIAL = space(10)
    store " " to w_MVVALNAZ, w_MVFLSCOR
    store 0 to w_MVCAOVAL
    * --- Lettura dati da testata documento
    if empty( NVL( w_MVCODCON , "" ) )
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL,MVVALNAZ,MVTCAMAG,MVFLSCOR,MVCAOVAL,MVCODVAL,MVTIPCON"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVNUMDOC = "+cp_ToStrODBC(w_MVNUMDOC);
              +" and MVDATDOC = "+cp_ToStrODBC(w_MVDATDOC);
              +" and MVALFDOC = "+cp_ToStrODBC(w_MVALFDOC);
              +" and MVTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL,MVVALNAZ,MVTCAMAG,MVFLSCOR,MVCAOVAL,MVCODVAL,MVTIPCON;
          from (i_cTable) where;
              MVNUMDOC = w_MVNUMDOC;
              and MVDATDOC = w_MVDATDOC;
              and MVALFDOC = w_MVALFDOC;
              and MVTIPDOC = w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
        this.w_CAUMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
        w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
        w_MVCAOVAL = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
        w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
        w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL,MVVALNAZ,MVTCAMAG,MVFLSCOR,MVCAOVAL,MVCODVAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVNUMDOC = "+cp_ToStrODBC(w_MVNUMDOC);
              +" and MVDATDOC = "+cp_ToStrODBC(w_MVDATDOC);
              +" and MVALFDOC = "+cp_ToStrODBC(w_MVALFDOC);
              +" and MVTIPCON = "+cp_ToStrODBC(w_MVTIPCON);
              +" and MVCODCON = "+cp_ToStrODBC(w_MVCODCON);
              +" and MVTIPDOC = "+cp_ToStrODBC(w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL,MVVALNAZ,MVTCAMAG,MVFLSCOR,MVCAOVAL,MVCODVAL;
          from (i_cTable) where;
              MVNUMDOC = w_MVNUMDOC;
              and MVDATDOC = w_MVDATDOC;
              and MVALFDOC = w_MVALFDOC;
              and MVTIPCON = w_MVTIPCON;
              and MVCODCON = w_MVCODCON;
              and MVTIPDOC = w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
        this.w_CAUMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
        w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
        w_MVCAOVAL = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
        w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !empty(this.w_MVSERIAL)
      w_MVCAUMAG = iif( empty(w_MVCAUMAG), this.w_CAUMAG, w_MVCAUMAG )
      this.oParentObject.ULT_SERI = this.w_MVSERIAL
      this.w_LETTO = "X"
      if IsAlt()
        * --- Se � gi� stato fatto l'import delle prestazioni da Alter2 sono stati certamente inserite in DOC_DETT le righe di prestazione con riferimento al documento PRE
        *     e quindi, nel caso di fatture a forfait, se inseriamo i dettagli di arrotondamento dobbiamo leggere l'ultimo CPROWNUM inserito in modo da accodarli
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS MAXROWNUM  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+"";
               ,"_Curs_DOC_DETT")
        else
          select MAX(CPROWNUM) AS MAXROWNUM from (i_cTable);
           where MVSERIAL=this.w_MVSERIAL;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          this.oParentObject.ULT_RNUM = NVL(MAXROWNUM,0)+1
          this.w_LETTO = " "
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
      else
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL;
            from (i_cTable) where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.oParentObject.ULT_RNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LETTO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.ULT_RNUM = 1 AND i_ROWS = 0
          * --- all'inserimento della prima riga 
          *     di un nuovo documento occorre controllare che la testata gi� inserita sul database non abbia altre righe.
          *     Es. F6 sulla prima riga di un documento gi� importato
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVSERIAL"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVSERIAL;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LETTO = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      this.w_ESISTE = .t.
      if empty(this.w_LETTO)
        this.w_ESISTE = .f.
        * --- Lettura dati causale magazzino
        this.w_CMFLAVAL = ""
        this.w_DecTot = 0
        this.w_NzCaoVal = 0
        store " " to w_UNMIS1, w_UNMIS2, w_UNMIS3, w_OPERA3, w_OPERAT, w_IVALIS, w_UMCAL
        store 0 to w_MOLTIP, w_MOLTI3, w_PERIVA
        * --- Inizializzazione Variabili Riga Dettaglio
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Aggiornamento saldi articoli
        this.w_ARCH = "V"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Insert into LISTART
        i_nConn=i_TableProp[this.LISTART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LISTART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"KEYSAL"+",ACQVEN"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_MVCODART),'LISTART','KEYSAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CMFLAVAL),'LISTART','ACQVEN');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'KEYSAL',w_MVCODART,'ACQVEN',this.w_CMFLAVAL)
          insert into (i_cTable) (KEYSAL,ACQVEN &i_ccchkf. );
             values (;
               w_MVCODART;
               ,this.w_CMFLAVAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Inserimento record detail
        w_MVNUMRIF = -20
        w_CPROWORD = this.oParentObject.ULT_RNUM*10
        * --- Insert into DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'DOC_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL,'CPROWNUM',this.oParentObject.ULT_RNUM,'MVNUMRIF',w_MVNUMRIF)
          insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF &i_ccchkf. );
             values (;
               this.w_MVSERIAL;
               ,this.oParentObject.ULT_RNUM;
               ,w_MVNUMRIF;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if IsAlt()
          * --- Insert into ALT_DETT
          i_nConn=i_TableProp[this.ALT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALT_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DESERIAL"+",DEROWNUM"+",DENUMRIF"+",DEOREEFF"+",DEMINEFF"+",DECOSINT"+",DEPREMIN"+",DEPREMAX"+",DEGAZUFF"+",DETIPPRA"+",DEMATPRA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'ALT_DETT','DESERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'ALT_DETT','DEROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(w_MVNUMRIF),'ALT_DETT','DENUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEOREEFF),'ALT_DETT','DEOREEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEMINEFF),'ALT_DETT','DEMINEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DECOSINT),'ALT_DETT','DECOSINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEPREMIN),'ALT_DETT','DEPREMIN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEPREMAX),'ALT_DETT','DEPREMAX');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEGAZUFF),'ALT_DETT','DEGAZUFF');
            +","+cp_NullLink(cp_ToStrODBC(w_DETIPPRA),'ALT_DETT','DETIPPRA');
            +","+cp_NullLink(cp_ToStrODBC(w_DEMATPRA),'ALT_DETT','DEMATPRA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_MVSERIAL,'DEROWNUM',this.oParentObject.ULT_RNUM,'DENUMRIF',w_MVNUMRIF,'DEOREEFF',this.w_DEOREEFF,'DEMINEFF',this.w_DEMINEFF,'DECOSINT',this.w_DECOSINT,'DEPREMIN',this.w_DEPREMIN,'DEPREMAX',this.w_DEPREMAX,'DEGAZUFF',this.w_DEGAZUFF,'DETIPPRA',w_DETIPPRA,'DEMATPRA',w_DEMATPRA)
            insert into (i_cTable) (DESERIAL,DEROWNUM,DENUMRIF,DEOREEFF,DEMINEFF,DECOSINT,DEPREMIN,DEPREMAX,DEGAZUFF,DETIPPRA,DEMATPRA &i_ccchkf. );
               values (;
                 this.w_MVSERIAL;
                 ,this.oParentObject.ULT_RNUM;
                 ,w_MVNUMRIF;
                 ,this.w_DEOREEFF;
                 ,this.w_DEMINEFF;
                 ,this.w_DECOSINT;
                 ,this.w_DEPREMIN;
                 ,this.w_DEPREMAX;
                 ,this.w_DEGAZUFF;
                 ,w_DETIPPRA;
                 ,w_DEMATPRA;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Testate Documenti Attivi/Passivi
    * --- Non inserisco se gi� memorizzata
    if NOT this.w_ESISTE
      * --- Testate Documenti
      this.oParentObject.w_ResoDett = AH_MSGFORMAT("Serial %1 numero registrazione %2 del %3 riga %4" , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , dtoc(w_MVDATREG) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
      * --- Controllo Data Registrazione Magazzino
      if g_APPLICATION="ADHOC REVOLUTION"
        if .not. empty(w_MVTCAMAG)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLELGM,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(w_MVTCAMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLELGM,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = w_MVTCAMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_FLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_FLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_FLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            w_FLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            this.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if .not. empty(this.w_CAUCOL)
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_CAUCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE;
                from (i_cTable) where;
                    CMCODICE = this.w_CAUCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              this.w_F2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              this.w_F2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_F2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AGGSAL = ALLTRIM(this.w_FLCASC+this.w_FLRISE+this.w_FLORDI+this.w_FLIMPE+this.w_F2CASC+this.w_F2RISE+this.w_F2ORDI+this.w_F2IMPE)
        endif
        if Not Empty(CHKCONS(w_MVFLVEAC+IIF(this.w_FLANAL="S","C","")+IIF(Not Empty(this.w_AGGSAL),"M",""),w_MVDATREG,"B","N"))
          this.oParentObject.w_ResoDett = AH_MSGFORMAT("Serial %1 numero registrazione %2 del %3 riga %4%5" , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMREG,6,0)) , dtoc(w_MVDATREG) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) , SUBSTR(CHKCONS(w_MVFLVEAC+IIF(this.w_FLANAL="S","C","") + IIF(Not Empty(this.w_AGGSAL),"M",""),w_MVDATREG,"B","N"),12) )
          * --- Raise
          i_Error="Data registrazione antecedente data consolidamento"
          return
        endif
      endif
      * --- Prima di inserire il documento, se � vuota la banca svuoto anche il numero del conto corrente
      if Empty(w_MVCODBAN)
        w_MVNUMCOR = Space(25)
      endif
      if TYPE("w_MV__ANNO")="U" OR w_MV__ANNO=0
        * --- Se non definito nel tracciato o se vuoto forzo anno data documento
        if w_MVCLADOC $ "FA-NC"
          w_MV__ANNO = YEAR(w_MVDATDOC)
        endif
      endif
      if TYPE("w_MV__MESE")="U" OR w_MV__MESE=0
        * --- Se non definito nel tracciato o se vuoto forzo anno data documento
        if w_MVCLADOC $ "FA-NC"
          w_MV__MESE =MONTH(w_MVDATDOC)
        endif
      endif
      if TYPE("w_MVDATPLA")="U" OR EMPTY(w_MVDATPLA)
        w_MVDATPLA=IIF(this.oParentObject.w_PLADOC="S",w_MVDATDOC,w_MVDATREG)
      endif
      w_MVFLPROV = iif( empty(w_MVFLPROV), "N", w_MVFLPROV )
      w_MVANNDOC = iif( empty(w_MVANNDOC), str(year(w_MVDATDOC),4,0), w_MVANNDOC )
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVALFDOC ="+cp_NullLink(cp_ToStrODBC(w_MVALFDOC),'DOC_MAST','MVALFDOC');
        +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(w_MVALFEST),'DOC_MAST','MVALFEST');
        +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
        +",MVCAUCON ="+cp_NullLink(cp_ToStrODBC(w_MVCAUCON),'DOC_MAST','MVCAUCON');
        +",MVCLADOC ="+cp_NullLink(cp_ToStrODBC(w_MVCLADOC),'DOC_MAST','MVCLADOC');
        +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(w_MVCODAG2),'DOC_MAST','MVCODAG2');
        +",MVCODAGE ="+cp_NullLink(cp_ToStrODBC(w_MVCODAGE),'DOC_MAST','MVCODAGE');
        +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(w_MVCODBA2),'DOC_MAST','MVCODBA2');
        +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(w_MVCODBAN),'DOC_MAST','MVCODBAN');
        +",MVCODCON ="+cp_NullLink(cp_ToStrODBC(w_MVCODCON),'DOC_MAST','MVCODCON');
        +",MVCODDES ="+cp_NullLink(cp_ToStrODBC(w_MVCODDES),'DOC_MAST','MVCODDES');
        +",MVCODESE ="+cp_NullLink(cp_ToStrODBC(w_MVCODESE),'DOC_MAST','MVCODESE');
        +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(w_MVCODPAG),'DOC_MAST','MVCODPAG');
        +",MVCODUTE ="+cp_NullLink(cp_ToStrODBC(w_MVCODUTE),'DOC_MAST','MVCODUTE');
        +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(w_MVCODVAL),'DOC_MAST','MVCODVAL');
        +",MVDATCIV ="+cp_NullLink(cp_ToStrODBC(w_MVDATCIV),'DOC_MAST','MVDATCIV');
        +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(w_MVDATDIV),'DOC_MAST','MVDATDIV');
        +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(w_MVDATDOC),'DOC_MAST','MVDATDOC');
        +",MVDATEST ="+cp_NullLink(cp_ToStrODBC(w_MVDATEST),'DOC_MAST','MVDATEST');
        +",MVDATREG ="+cp_NullLink(cp_ToStrODBC(w_MVDATREG),'DOC_MAST','MVDATREG');
        +",MVFLACCO ="+cp_NullLink(cp_ToStrODBC(w_MVFLACCO),'DOC_MAST','MVFLACCO');
        +",MVFLINTE ="+cp_NullLink(cp_ToStrODBC(w_MVFLINTE),'DOC_MAST','MVFLINTE');
        +",MVFLPROV ="+cp_NullLink(cp_ToStrODBC(w_MVFLPROV),'DOC_MAST','MVFLPROV');
        +",MVFLVEAC ="+cp_NullLink(cp_ToStrODBC(w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
        +",MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
        +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(w_MVNUMEST),'DOC_MAST','MVNUMEST');
        +",MVNUMREG ="+cp_NullLink(cp_ToStrODBC(w_MVNUMREG),'DOC_MAST','MVNUMREG');
        +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
        +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
        +",MVTCAMAG ="+cp_NullLink(cp_ToStrODBC(w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
        +",MVTCOLIS ="+cp_NullLink(cp_ToStrODBC(w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
        +",MVTCONTR ="+cp_NullLink(cp_ToStrODBC(w_MVTCONTR),'DOC_MAST','MVTCONTR');
        +",MVTFRAGG ="+cp_NullLink(cp_ToStrODBC(w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
        +",MVTIPCON ="+cp_NullLink(cp_ToStrODBC(w_MVTIPCON),'DOC_MAST','MVTIPCON');
        +",MVTIPDOC ="+cp_NullLink(cp_ToStrODBC(w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
        +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
        +",MVANNDOC ="+cp_NullLink(cp_ToStrODBC(w_MVANNDOC),'DOC_MAST','MVANNDOC');
        +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(w_MVANNPRO),'DOC_MAST','MVANNPRO');
        +",MVAGG_01 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_01),'DOC_MAST','MVAGG_01');
        +",MVAGG_02 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_02),'DOC_MAST','MVAGG_02');
        +",MVAGG_03 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_03),'DOC_MAST','MVAGG_03');
        +",MVAGG_04 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_04),'DOC_MAST','MVAGG_04');
        +",MVAGG_05 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_05),'DOC_MAST','MVAGG_05');
        +",MVAGG_06 ="+cp_NullLink(cp_ToStrODBC(w_MVAGG_06),'DOC_MAST','MVAGG_06');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVALFDOC = w_MVALFDOC;
            ,MVALFEST = w_MVALFEST;
            ,MVCAOVAL = w_MVCAOVAL;
            ,MVCAUCON = w_MVCAUCON;
            ,MVCLADOC = w_MVCLADOC;
            ,MVCODAG2 = w_MVCODAG2;
            ,MVCODAGE = w_MVCODAGE;
            ,MVCODBA2 = w_MVCODBA2;
            ,MVCODBAN = w_MVCODBAN;
            ,MVCODCON = w_MVCODCON;
            ,MVCODDES = w_MVCODDES;
            ,MVCODESE = w_MVCODESE;
            ,MVCODPAG = w_MVCODPAG;
            ,MVCODUTE = w_MVCODUTE;
            ,MVCODVAL = w_MVCODVAL;
            ,MVDATCIV = w_MVDATCIV;
            ,MVDATDIV = w_MVDATDIV;
            ,MVDATDOC = w_MVDATDOC;
            ,MVDATEST = w_MVDATEST;
            ,MVDATREG = w_MVDATREG;
            ,MVFLACCO = w_MVFLACCO;
            ,MVFLINTE = w_MVFLINTE;
            ,MVFLPROV = w_MVFLPROV;
            ,MVFLVEAC = w_MVFLVEAC;
            ,MVNUMDOC = w_MVNUMDOC;
            ,MVNUMEST = w_MVNUMEST;
            ,MVNUMREG = w_MVNUMREG;
            ,MVSCOCL1 = w_MVSCOCL1;
            ,MVSCOCL2 = w_MVSCOCL2;
            ,MVTCAMAG = w_MVTCAMAG;
            ,MVTCOLIS = w_MVTCOLIS;
            ,MVTCONTR = w_MVTCONTR;
            ,MVTFRAGG = w_MVTFRAGG;
            ,MVTIPCON = w_MVTIPCON;
            ,MVTIPDOC = w_MVTIPDOC;
            ,MVVALNAZ = w_MVVALNAZ;
            ,MVANNDOC = w_MVANNDOC;
            ,MVANNPRO = w_MVANNPRO;
            ,MVAGG_01 = w_MVAGG_01;
            ,MVAGG_02 = w_MVAGG_02;
            ,MVAGG_03 = w_MVAGG_03;
            ,MVAGG_04 = w_MVAGG_04;
            ,MVAGG_05 = w_MVAGG_05;
            ,MVAGG_06 = w_MVAGG_06;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
        +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(w_MVCODIVE),'DOC_MAST','MVCODIVE');
        +",MVFLGIOM ="+cp_NullLink(cp_ToStrODBC(w_MVFLGIOM),'DOC_MAST','MVFLGIOM');
        +",MVFLCONT ="+cp_NullLink(cp_ToStrODBC(w_MVFLCONT),'DOC_MAST','MVFLCONT');
        +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(w_MVSCONTI),'DOC_MAST','MVSCONTI');
        +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(w_MVFLFOSC),'DOC_MAST','MVFLFOSC');
        +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(w_MVSPEINC),'DOC_MAST','MVSPEINC');
        +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(w_MVIVAINC),'DOC_MAST','MVIVAINC');
        +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(w_MVFLRINC),'DOC_MAST','MVFLRINC');
        +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(w_MVSPETRA),'DOC_MAST','MVSPETRA');
        +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(w_MVIVATRA),'DOC_MAST','MVIVATRA');
        +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
        +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
        +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
        +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
        +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
        +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(w_MVIVABOL),'DOC_MAST','MVIVABOL');
        +",MVIMPARR ="+cp_NullLink(cp_ToStrODBC(w_MVIMPARR),'DOC_MAST','MVIMPARR');
        +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(w_MVACCONT),'DOC_MAST','MVACCONT');
        +",MVCODSPE ="+cp_NullLink(cp_ToStrODBC(w_MVCODSPE),'DOC_MAST','MVCODSPE');
        +",MVCODVET ="+cp_NullLink(cp_ToStrODBC(w_MVCODVET),'DOC_MAST','MVCODVET');
        +",MVCODVE2 ="+cp_NullLink(cp_ToStrODBC(w_MVCODVE2),'DOC_MAST','MVCODVE2');
        +",MVCODVE3 ="+cp_NullLink(cp_ToStrODBC(w_MVCODVE3),'DOC_MAST','MVCODVE3');
        +",MVCODPOR ="+cp_NullLink(cp_ToStrODBC(w_MVCODPOR),'DOC_MAST','MVCODPOR');
        +",MVCONCON ="+cp_NullLink(cp_ToStrODBC(w_MVCONCON),'DOC_MAST','MVCONCON');
        +",MVASPEST ="+cp_NullLink(cp_ToStrODBC(w_MVASPEST),'DOC_MAST','MVASPEST');
        +",MVCODASP ="+cp_NullLink(cp_ToStrODBC(w_MVCODASP),'DOC_MAST','MVCODASP');
        +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(w_MVNOTAGG),'DOC_MAST','MVNOTAGG');
        +",MVQTACOL ="+cp_NullLink(cp_ToStrODBC(w_MVQTACOL),'DOC_MAST','MVQTACOL');
        +",MVQTAPES ="+cp_NullLink(cp_ToStrODBC(w_MVQTAPES),'DOC_MAST','MVQTAPES');
        +",MVQTALOR ="+cp_NullLink(cp_ToStrODBC(w_MVQTALOR),'DOC_MAST','MVQTALOR');
        +",MVTIPORN ="+cp_NullLink(cp_ToStrODBC(w_MVTIPORN),'DOC_MAST','MVTIPORN');
        +",MVCODORN ="+cp_NullLink(cp_ToStrODBC(w_MVCODORN),'DOC_MAST','MVCODORN');
        +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(w_MVORATRA),'DOC_MAST','MVORATRA');
        +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(w_MVDATTRA),'DOC_MAST','MVDATTRA');
        +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(w_MVMINTRA),'DOC_MAST','MVMINTRA');
        +",MVGENEFF ="+cp_NullLink(cp_ToStrODBC(w_MVGENEFF),'DOC_MAST','MVGENEFF');
        +",MVGENPRO ="+cp_NullLink(cp_ToStrODBC(w_MVGENPRO),'DOC_MAST','MVGENPRO');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVSCOPAG = w_MVSCOPAG;
            ,MVCODIVE = w_MVCODIVE;
            ,MVFLGIOM = w_MVFLGIOM;
            ,MVFLCONT = w_MVFLCONT;
            ,MVSCONTI = w_MVSCONTI;
            ,MVFLFOSC = w_MVFLFOSC;
            ,MVSPEINC = w_MVSPEINC;
            ,MVIVAINC = w_MVIVAINC;
            ,MVFLRINC = w_MVFLRINC;
            ,MVSPETRA = w_MVSPETRA;
            ,MVIVATRA = w_MVIVATRA;
            ,MVFLRTRA = w_MVFLRTRA;
            ,MVSPEIMB = w_MVSPEIMB;
            ,MVIVAIMB = w_MVIVAIMB;
            ,MVFLRIMB = w_MVFLRIMB;
            ,MVSPEBOL = w_MVSPEBOL;
            ,MVIVABOL = w_MVIVABOL;
            ,MVIMPARR = w_MVIMPARR;
            ,MVACCONT = w_MVACCONT;
            ,MVCODSPE = w_MVCODSPE;
            ,MVCODVET = w_MVCODVET;
            ,MVCODVE2 = w_MVCODVE2;
            ,MVCODVE3 = w_MVCODVE3;
            ,MVCODPOR = w_MVCODPOR;
            ,MVCONCON = w_MVCONCON;
            ,MVASPEST = w_MVASPEST;
            ,MVCODASP = w_MVCODASP;
            ,MVNOTAGG = w_MVNOTAGG;
            ,MVQTACOL = w_MVQTACOL;
            ,MVQTAPES = w_MVQTAPES;
            ,MVQTALOR = w_MVQTALOR;
            ,MVTIPORN = w_MVTIPORN;
            ,MVCODORN = w_MVCODORN;
            ,MVORATRA = w_MVORATRA;
            ,MVDATTRA = w_MVDATTRA;
            ,MVMINTRA = w_MVMINTRA;
            ,MVGENEFF = w_MVGENEFF;
            ,MVGENPRO = w_MVGENPRO;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA1),'DOC_MAST','MVACIVA1');
        +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA2),'DOC_MAST','MVACIVA2');
        +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA3),'DOC_MAST','MVACIVA3');
        +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA4),'DOC_MAST','MVACIVA4');
        +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA5),'DOC_MAST','MVACIVA5');
        +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(w_MVACIVA6),'DOC_MAST','MVACIVA6');
        +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
        +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
        +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
        +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
        +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
        +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
        +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
        +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
        +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
        +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
        +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
        +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
        +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
        +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
        +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
        +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
        +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
        +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
        +",MVPRD ="+cp_NullLink(cp_ToStrODBC(w_MVPRD),'DOC_MAST','MVPRD');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVACIVA1 = w_MVACIVA1;
            ,MVACIVA2 = w_MVACIVA2;
            ,MVACIVA3 = w_MVACIVA3;
            ,MVACIVA4 = w_MVACIVA4;
            ,MVACIVA5 = w_MVACIVA5;
            ,MVACIVA6 = w_MVACIVA6;
            ,MVAIMPN1 = w_MVAIMPN1;
            ,MVAIMPN2 = w_MVAIMPN2;
            ,MVAIMPN3 = w_MVAIMPN3;
            ,MVAIMPN4 = w_MVAIMPN4;
            ,MVAIMPN5 = w_MVAIMPN5;
            ,MVAIMPN6 = w_MVAIMPN6;
            ,MVAIMPS1 = w_MVAIMPS1;
            ,MVAIMPS2 = w_MVAIMPS2;
            ,MVAIMPS3 = w_MVAIMPS3;
            ,MVAIMPS4 = w_MVAIMPS4;
            ,MVAIMPS5 = w_MVAIMPS5;
            ,MVAIMPS6 = w_MVAIMPS6;
            ,MVAFLOM1 = w_MVAFLOM1;
            ,MVAFLOM2 = w_MVAFLOM2;
            ,MVAFLOM3 = w_MVAFLOM3;
            ,MVAFLOM4 = w_MVAFLOM4;
            ,MVAFLOM5 = w_MVAFLOM5;
            ,MVAFLOM6 = w_MVAFLOM6;
            ,MVPRD = w_MVPRD;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFCON ="+cp_NullLink(cp_ToStrODBC(w_MVRIFCON),'DOC_MAST','MVRIFCON');
        +",MVPRP ="+cp_NullLink(cp_ToStrODBC(w_MVPRP),'DOC_MAST','MVPRP');
        +",MVTINCOM ="+cp_NullLink(cp_ToStrODBC(w_MVTINCOM),'DOC_MAST','MVTINCOM');
        +",MVTFICOM ="+cp_NullLink(cp_ToStrODBC(w_MVTFICOM),'DOC_MAST','MVTFICOM');
        +",MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
        +",MVFLSCOR ="+cp_NullLink(cp_ToStrODBC(w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
        +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'DOC_MAST','UTCC');
        +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'DOC_MAST','UTDC');
        +",MVDESDOC ="+cp_NullLink(cp_ToStrODBC(w_MVDESDOC),'DOC_MAST','MVDESDOC');
        +",MVTIPOPE ="+cp_NullLink(cp_ToStrODBC(w_MVTIPOPE),'DOC_MAST','MVTIPOPE');
        +",MVCATOPE ="+cp_NullLink(cp_ToStrODBC("OP"),'DOC_MAST','MVCATOPE');
        +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(w_MVNUMCOR),'DOC_MAST','MVNUMCOR');
        +",MV__ANNO ="+cp_NullLink(cp_ToStrODBC(w_MV__ANNO),'DOC_MAST','MV__ANNO');
        +",MV__MESE ="+cp_NullLink(cp_ToStrODBC(w_MV__MESE),'DOC_MAST','MV__MESE');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVRIFCON = w_MVRIFCON;
            ,MVPRP = w_MVPRP;
            ,MVTINCOM = w_MVTINCOM;
            ,MVTFICOM = w_MVTFICOM;
            ,MVRIFDIC = w_MVRIFDIC;
            ,MVFLSCOR = w_MVFLSCOR;
            ,UTCC = this.oParentObject.w_CreVarUte;
            ,UTDC = this.oParentObject.w_CreVarDat;
            ,MVDESDOC = w_MVDESDOC;
            ,MVTIPOPE = w_MVTIPOPE;
            ,MVCATOPE = "OP";
            ,MVNUMCOR = w_MVNUMCOR;
            ,MV__ANNO = w_MV__ANNO;
            ,MV__MESE = w_MV__MESE;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if g_APPLICATION="ADHOC REVOLUTION"
        * --- Calcola Sconti su Omaggio
        this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIVAARR ="+cp_NullLink(cp_ToStrODBC(w_MVIVAARR),'DOC_MAST','MVIVAARR');
          +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(w_MVACCPRE),'DOC_MAST','MVACCPRE');
          +",MVMAXACC ="+cp_NullLink(cp_ToStrODBC(w_MVMAXACC),'DOC_MAST','MVMAXACC');
          +",MVACCSUC ="+cp_NullLink(cp_ToStrODBC(w_MVACCSUC),'DOC_MAST','MVACCSUC');
          +",MVTOTRIT ="+cp_NullLink(cp_ToStrODBC(w_MVTOTRIT),'DOC_MAST','MVTOTRIT');
          +",MVTOTENA ="+cp_NullLink(cp_ToStrODBC(w_MVTOTENA),'DOC_MAST','MVTOTENA');
          +",MVRIFPIA ="+cp_NullLink(cp_ToStrODBC(w_MVRIFPIA),'DOC_MAST','MVRIFPIA');
          +",MVRIFACC ="+cp_NullLink(cp_ToStrODBC(w_MVRIFACC),'DOC_MAST','MVRIFACC');
          +",MVRIFFAD ="+cp_NullLink(cp_ToStrODBC(w_MVRIFFAD),'DOC_MAST','MVRIFFAD');
          +",MVPERRET ="+cp_NullLink(cp_ToStrODBC(w_MVPERRET),'DOC_MAST','MVPERRET');
          +",MVANNRET ="+cp_NullLink(cp_ToStrODBC(w_MVANNRET),'DOC_MAST','MVANNRET');
          +",MVFLINTR ="+cp_NullLink(cp_ToStrODBC(w_MVFLINTR),'DOC_MAST','MVFLINTR');
          +",MVTRAINT ="+cp_NullLink(cp_ToStrODBC(w_MVTRAINT),'DOC_MAST','MVTRAINT');
          +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(w_MV__NOTE),'DOC_MAST','MV__NOTE');
          +",MVFLSALD ="+cp_NullLink(cp_ToStrODBC(w_MVFLSALD),'DOC_MAST','MVFLSALD');
          +",MVFLCAPA ="+cp_NullLink(cp_ToStrODBC(w_MVFLCAPA),'DOC_MAST','MVFLCAPA');
          +",MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(w_MVFLSCAF),'DOC_MAST','MVFLSCAF');
          +",MVCODASP ="+cp_NullLink(cp_ToStrODBC(w_MVCODASP),'DOC_MAST','MVCODASP');
          +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(w_MVFLVABD),'DOC_MAST','MVFLVABD');
          +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
          +",MVDATPLA ="+cp_NullLink(cp_ToStrODBC(w_MVDATPLA),'DOC_MAST','MVDATPLA');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVIVAARR = w_MVIVAARR;
              ,MVACCPRE = w_MVACCPRE;
              ,MVMAXACC = w_MVMAXACC;
              ,MVACCSUC = w_MVACCSUC;
              ,MVTOTRIT = w_MVTOTRIT;
              ,MVTOTENA = w_MVTOTENA;
              ,MVRIFPIA = w_MVRIFPIA;
              ,MVRIFACC = w_MVRIFACC;
              ,MVRIFFAD = w_MVRIFFAD;
              ,MVPERRET = w_MVPERRET;
              ,MVANNRET = w_MVANNRET;
              ,MVFLINTR = w_MVFLINTR;
              ,MVTRAINT = w_MVTRAINT;
              ,MV__NOTE = w_MV__NOTE;
              ,MVFLSALD = w_MVFLSALD;
              ,MVFLCAPA = w_MVFLCAPA;
              ,MVFLSCAF = w_MVFLSCAF;
              ,MVCODASP = w_MVCODASP;
              ,MVFLVABD = w_MVFLVABD;
              ,MVFLSCOM = this.w_MVFLSCOM;
              ,MVDATPLA = w_MVDATPLA;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if IsAlt() AND w_MVTOTRIT>0
          * --- Insert into VDATRITE
          i_nConn=i_TableProp[this.VDATRITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VDATRITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VDATRITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DRSERIAL"+",DRSOMESC"+",DRFODPRO"+",DRCODTRI"+",DRCODCAU"+",DRIMPONI"+",DRRITENU"+",DRPERRIT"+",CPROWNUM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'VDATRITE','DRSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(w_SOMESC),'VDATRITE','DRSOMESC');
            +","+cp_NullLink(cp_ToStrODBC(w_MVSPETRA),'VDATRITE','DRFODPRO');
            +","+cp_NullLink(cp_ToStrODBC("1040"),'VDATRITE','DRCODTRI');
            +","+cp_NullLink(cp_ToStrODBC("A"),'VDATRITE','DRCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(w_IMPONI),'VDATRITE','DRIMPONI');
            +","+cp_NullLink(cp_ToStrODBC(w_MVTOTRIT),'VDATRITE','DRRITENU');
            +","+cp_NullLink(cp_ToStrODBC(w_PNPERRIT),'VDATRITE','DRPERRIT');
            +","+cp_NullLink(cp_ToStrODBC(1),'VDATRITE','CPROWNUM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_MVSERIAL,'DRSOMESC',w_SOMESC,'DRFODPRO',w_MVSPETRA,'DRCODTRI',"1040",'DRCODCAU',"A",'DRIMPONI',w_IMPONI,'DRRITENU',w_MVTOTRIT,'DRPERRIT',w_PNPERRIT,'CPROWNUM',1)
            insert into (i_cTable) (DRSERIAL,DRSOMESC,DRFODPRO,DRCODTRI,DRCODCAU,DRIMPONI,DRRITENU,DRPERRIT,CPROWNUM &i_ccchkf. );
               values (;
                 this.w_MVSERIAL;
                 ,w_SOMESC;
                 ,w_MVSPETRA;
                 ,"1040";
                 ,"A";
                 ,w_IMPONI;
                 ,w_MVTOTRIT;
                 ,w_PNPERRIT;
                 ,1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVTCOCEN ="+cp_NullLink(cp_ToStrODBC(w_MVTCOCEN),'DOC_MAST','MVTCOCEN');
          +",MVTCOMAG ="+cp_NullLink(cp_ToStrODBC(w_MVTCOMAG),'DOC_MAST','MVTCOMAG');
          +",MVTCOMAT ="+cp_NullLink(cp_ToStrODBC(w_MVTCOMAT),'DOC_MAST','MVTCOMAT');
          +",MVTCOMME ="+cp_NullLink(cp_ToStrODBC(w_MVTCOMME),'DOC_MAST','MVTCOMME');
          +",MVTDTEVA ="+cp_NullLink(cp_ToStrODBC(w_MVTDTEVA),'DOC_MAST','MVTDTEVA');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVTCOCEN = w_MVTCOCEN;
              ,MVTCOMAG = w_MVTCOMAG;
              ,MVTCOMAT = w_MVTCOMAT;
              ,MVTCOMME = w_MVTCOMME;
              ,MVTDTEVA = w_MVTDTEVA;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      i_rows = 1
      this.oParentObject.w_ResoDett = "<SENZA RESOCONTO>"
      this.oParentObject.w_Corretto = .T.
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Righe Documenti Attivi/Passivi
    this.oParentObject.w_ResoDett = AH_MSGFORMAT("Serial %1 numero documento %2 del %3 riga %4" , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMDOC,15,0)) , dtoc(w_MVDATDOC) , ALLTRIM(str(this.oParentObject.ULT_RNUM,4,0)) )
    if empty(this.w_MVSERIAL)
      * --- Segnala documento non trovato
      this.oParentObject.w_ResoMode = "SCARTO"
      if empty( NVL( w_MVCODCON , "" ) )
        i_TrsMsg = AH_MSGFORMAT( "Impossibile individuare o non presente la testata del documento%0Chiave comune utilizzata: num. doc. %1 %3, data %2,  tipo doc. %4", alltrim(str(w_MVNUMDOC,15,0)) , dtoc(w_MVDATDOC),alltrim(w_MVALFDOC), alltrim(w_MVTIPDOC) )
      else
        i_TrsMsg = AH_MSGFORMAT( "Impossibile individuare o non presente la testata del documento%0Chiave comune utilizzata: num. doc. %1 %3, data %2,  tipo doc. %4, codice cli/for. %5,  tipo conto %6", alltrim(str(w_MVNUMDOC,15,0)) , dtoc(w_MVDATDOC), alltrim(w_MVALFDOC), alltrim(w_MVTIPDOC), alltrim(w_MVCODCON), alltrim(w_MVTIPCON) )
      endif
      this.oParentObject.ULT_SERI = ""
      * --- Raise
      i_Error="errore"
      return
    else
      if NOT this.w_ESISTE
        * --- controllo se esisteva gi� il movimento
        w_MVNUMRIF = -20
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM*10),'DOC_DETT','CPROWORD');
          +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
          +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(w_MVCODICE),'DOC_DETT','MVCODICE');
          +",MVCODART ="+cp_NullLink(cp_ToStrODBC(w_MVCODART),'DOC_DETT','MVCODART');
          +",MVDESART ="+cp_NullLink(cp_ToStrODBC(w_MVDESART),'DOC_DETT','MVDESART');
          +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(w_MVDESSUP),'DOC_DETT','MVDESSUP');
          +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
          +",MVCATCON ="+cp_NullLink(cp_ToStrODBC(w_MVCATCON),'DOC_DETT','MVCATCON');
          +",MVCONTRO ="+cp_NullLink(cp_ToStrODBC(w_MVCONTRO),'DOC_DETT','MVCONTRO');
          +",MVCONIND ="+cp_NullLink(cp_ToStrODBC(w_MVCONIND),'DOC_DETT','MVCONIND');
          +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
          +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(w_MVCODCLA),'DOC_DETT','MVCODCLA');
          +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
          +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(w_MVCODMAG),'DOC_DETT','MVCODMAG');
          +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(w_MVCODMAT),'DOC_DETT','MVCODMAT');
          +",MVCONTRA ="+cp_NullLink(cp_ToStrODBC(w_MVCONTRA),'DOC_DETT','MVCONTRA');
          +",MVCODLIS ="+cp_NullLink(cp_ToStrODBC(w_MVCODLIS),'DOC_DETT','MVCODLIS');
          +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
          +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
          +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(w_MVPREZZO),'DOC_DETT','MVPREZZO');
          +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(w_MVSCONT1),'DOC_DETT','MVSCONT1');
          +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(w_MVSCONT2),'DOC_DETT','MVSCONT2');
          +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(w_MVSCONT3),'DOC_DETT','MVSCONT3');
          +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(w_MVSCONT4),'DOC_DETT','MVSCONT4');
          +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
          +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(w_MVCODIVA),'DOC_DETT','MVCODIVA');
          +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(w_MVVALRIG),'DOC_DETT','MVVALRIG');
          +",MVIMPACC ="+cp_NullLink(cp_ToStrODBC(w_MVIMPACC),'DOC_DETT','MVIMPACC');
          +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
          +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(w_MVVALMAG),'DOC_DETT','MVVALMAG');
          +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
          +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
          +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(w_MVFLCASC),'DOC_DETT','MVFLCASC');
          +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(w_MVF2CASC),'DOC_DETT','MVF2CASC');
          +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(w_MVFLORDI),'DOC_DETT','MVFLORDI');
          +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(w_MVF2ORDI),'DOC_DETT','MVF2ORDI');
          +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
          +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(w_MVF2IMPE),'DOC_DETT','MVF2IMPE');
          +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(w_MVFLRISE),'DOC_DETT','MVFLRISE');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(-20);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.oParentObject.ULT_RNUM*10;
              ,MVTIPRIG = w_MVTIPRIG;
              ,MVCODICE = w_MVCODICE;
              ,MVCODART = w_MVCODART;
              ,MVDESART = w_MVDESART;
              ,MVDESSUP = w_MVDESSUP;
              ,MVUNIMIS = w_MVUNIMIS;
              ,MVCATCON = w_MVCATCON;
              ,MVCONTRO = w_MVCONTRO;
              ,MVCONIND = w_MVCONIND;
              ,MVCAUMAG = w_MVCAUMAG;
              ,MVCODCLA = w_MVCODCLA;
              ,MVCAUCOL = w_MVCAUCOL;
              ,MVCODMAG = w_MVCODMAG;
              ,MVCODMAT = w_MVCODMAT;
              ,MVCONTRA = w_MVCONTRA;
              ,MVCODLIS = w_MVCODLIS;
              ,MVQTAMOV = w_MVQTAMOV;
              ,MVQTAUM1 = w_MVQTAUM1;
              ,MVPREZZO = w_MVPREZZO;
              ,MVSCONT1 = w_MVSCONT1;
              ,MVSCONT2 = w_MVSCONT2;
              ,MVSCONT3 = w_MVSCONT3;
              ,MVSCONT4 = w_MVSCONT4;
              ,MVFLOMAG = w_MVFLOMAG;
              ,MVCODIVA = w_MVCODIVA;
              ,MVVALRIG = w_MVVALRIG;
              ,MVIMPACC = w_MVIMPACC;
              ,MVIMPSCO = w_MVIMPSCO;
              ,MVVALMAG = w_MVVALMAG;
              ,MVIMPNAZ = w_MVIMPNAZ;
              ,MVKEYSAL = w_MVKEYSAL;
              ,MVFLCASC = w_MVFLCASC;
              ,MVF2CASC = w_MVF2CASC;
              ,MVFLORDI = w_MVFLORDI;
              ,MVF2ORDI = w_MVF2ORDI;
              ,MVFLIMPE = w_MVFLIMPE;
              ,MVF2IMPE = w_MVF2IMPE;
              ,MVFLRISE = w_MVFLRISE;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.oParentObject.ULT_RNUM;
              and MVNUMRIF = -20;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODCEN ="+cp_NullLink(cp_ToStrODBC(w_MVCODCEN),'DOC_DETT','MVCODCEN');
          +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(w_MVDATEVA),'DOC_DETT','MVDATEVA');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(w_MVDATGEN),'DOC_DETT','MVDATGEN');
          +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(w_MVEFFEVA),'DOC_DETT','MVEFFEVA');
          +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(w_MVF2RISE),'DOC_DETT','MVF2RISE');
          +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(w_MVFINCOM),'DOC_DETT','MVFINCOM');
          +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(w_MVFLARIF),'DOC_DETT','MVFLARIF');
          +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(w_MVFLELGM),'DOC_DETT','MVFLELGM');
          +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(w_MVFLERIF),'DOC_DETT','MVFLERIF');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(w_MVFLEVAS),'DOC_DETT','MVFLEVAS');
          +",MVFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_MVFLRAGG),'DOC_DETT','MVFLRAGG');
          +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(w_MVFLRIPA),'DOC_DETT','MVFLRIPA');
          +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(w_MVIMPEVA),'DOC_DETT','MVIMPEVA');
          +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
          +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(w_MVINICOM),'DOC_DETT','MVINICOM');
          +",MVMOLSUP ="+cp_NullLink(cp_ToStrODBC(w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
          +",MVNOMENC ="+cp_NullLink(cp_ToStrODBC(w_MVNOMENC),'DOC_DETT','MVNOMENC');
          +",MVNUMCOL ="+cp_NullLink(cp_ToStrODBC(w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
          +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(w_MVPERPRO),'DOC_DETT','MVPERPRO');
          +",MVPESNET ="+cp_NullLink(cp_ToStrODBC(w_MVPESNET),'DOC_DETT','MVPESNET');
          +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(w_MVQTAEV1),'DOC_DETT','MVQTAEV1');
          +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(w_MVQTAEVA),'DOC_DETT','MVQTAEVA');
          +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(w_MVQTAIM1),'DOC_DETT','MVQTAIM1');
          +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(w_MVQTAIMP),'DOC_DETT','MVQTAIMP');
          +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(w_MVQTASAL),'DOC_DETT','MVQTASAL');
          +",MVROWRIF ="+cp_NullLink(cp_ToStrODBC(w_MVROWRIF),'DOC_DETT','MVROWRIF');
          +",MVSERRIF ="+cp_NullLink(cp_ToStrODBC(w_MVSERRIF),'DOC_DETT','MVSERRIF');
          +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(w_MVUMSUPP),'DOC_DETT','MVUMSUPP');
          +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
          +",MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(w_MVNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVCODCEN = w_MVCODCEN;
              ,MVDATEVA = w_MVDATEVA;
              ,MVDATGEN = w_MVDATGEN;
              ,MVEFFEVA = w_MVEFFEVA;
              ,MVF2RISE = w_MVF2RISE;
              ,MVFINCOM = w_MVFINCOM;
              ,MVFLARIF = w_MVFLARIF;
              ,MVFLELGM = w_MVFLELGM;
              ,MVFLERIF = w_MVFLERIF;
              ,MVFLEVAS = w_MVFLEVAS;
              ,MVFLRAGG = w_MVFLRAGG;
              ,MVFLRIPA = w_MVFLRIPA;
              ,MVFLTRAS = w_MVFLTRAS;
              ,MVIMPEVA = w_MVIMPEVA;
              ,MVIMPPRO = w_MVIMPPRO;
              ,MVINICOM = w_MVINICOM;
              ,MVMOLSUP = w_MVMOLSUP;
              ,MVNOMENC = w_MVNOMENC;
              ,MVNUMCOL = w_MVNUMCOL;
              ,MVPERPRO = w_MVPERPRO;
              ,MVPESNET = w_MVPESNET;
              ,MVQTAEV1 = w_MVQTAEV1;
              ,MVQTAEVA = w_MVQTAEVA;
              ,MVQTAIM1 = w_MVQTAIM1;
              ,MVQTAIMP = w_MVQTAIMP;
              ,MVQTASAL = w_MVQTASAL;
              ,MVROWRIF = w_MVROWRIF;
              ,MVSERRIF = w_MVSERRIF;
              ,MVUMSUPP = w_MVUMSUPP;
              ,MVVOCCEN = w_MVVOCCEN;
              ,MVTIPPRO = w_MVTIPPRO;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.oParentObject.ULT_RNUM;
              and MVNUMRIF = w_MVNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODCOM ="+cp_NullLink(cp_ToStrODBC(w_MVCODCOM),'DOC_DETT','MVCODCOM');
          +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(w_MVTIPATT),'DOC_DETT','MVTIPATT');
          +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(w_MVCODATT),'DOC_DETT','MVCODATT');
          +",MVFLCOCO ="+cp_NullLink(cp_ToStrODBC(w_MVFLCOCO),'DOC_DETT','MVFLCOCO');
          +",MVFLORCO ="+cp_NullLink(cp_ToStrODBC(w_MVFLORCO),'DOC_DETT','MVFLORCO');
          +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
          +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
          +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(w_MVPROCAP),'DOC_DETT','MVPROCAP');
          +",MVCODRES ="+cp_NullLink(cp_ToStrODBC(w_MVCODRES),'DOC_DETT','MVCODRES');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(w_MVNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVCODCOM = w_MVCODCOM;
              ,MVTIPATT = w_MVTIPATT;
              ,MVCODATT = w_MVCODATT;
              ,MVFLCOCO = w_MVFLCOCO;
              ,MVFLORCO = w_MVFLORCO;
              ,MVIMPCOM = w_MVIMPCOM;
              ,MVTIPPR2 = w_MVTIPPR2;
              ,MVPROCAP = w_MVPROCAP;
              ,MVCODRES = w_MVCODRES;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.oParentObject.ULT_RNUM;
              and MVNUMRIF = w_MVNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if g_APPLICATION="ADHOC REVOLUTION"
          if Not Empty(NVL(w_MVCODMAG,""))
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGFLUBIC,MGPROMAG"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(w_MVCODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGFLUBIC,MGPROMAG;
                from (i_cTable) where;
                    MGCODMAG = w_MVCODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              this.w_MGPROMAG = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          w_MVPROORD=iif(not empty(w_MVPROORD), w_MVPROORD, iif(not empty(this.w_MGPROMAG),this.w_MGPROMAG, g_PROAZI))
          if Not Empty(NVL(w_MVCODMAT,""))
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGFLUBIC"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGFLUBIC;
                from (i_cTable) where;
                    MGCODMAG = w_MVCODMAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if Not Empty(w_MVCODART)
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLLOTT"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(w_MVCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLLOTT;
                from (i_cTable) where;
                    ARCODART = w_MVCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
           
 w_MVFLLOTT=IIF(w_FLLOTT $ "S-C" OR this.w_FLUBIC="S",; 
 LEFT(ALLTRIM(w_MVFLCASC)+IIF(w_MVFLRISE="+", "-", IIF(w_MVFLRISE="-", "+", " ")), 1), " ") 
 
 w_MVF2LOTT=IIF(w_FLLOTT $ "S-C" OR w_F2UBIC="S",; 
 LEFT(ALLTRIM(w_MVF2CASC)+IIF(w_MVF2RISE="+", "-", IIF(w_MVF2RISE="-", "+", " ")), 1), " ") 
 
 w_MVVALULT=IIF(w_MVQTAUM1=0, 0, cp_ROUND(w_MVIMPNAZ/w_MVQTAUM1, g_PERPUL)) 
 w_MVCODUBI=IIF(Not Empty(w_MVCODMAG) AND w_MVFLLOTT $ "+-" ,w_MVCODUBI,Space(20)) 
 w_MVCODUB2=IIF(Not Empty(w_MVCODMAT) AND w_MVF2LOTT $ "+-" ,w_MVCODUB2,Space(20)) 
 w_MVCODLOT=IIF(w_FLLOTT $ "SC" AND (w_MVF2LOTT $ "+-" OR w_MVFLLOTT $ "+-") ,w_MVCODLOT,Space(20)) 
 w_MVLOTMAG=iif( Empty( w_MVCODLOT ) And Empty( w_MVCODUBI ) , SPACE(5) , w_MVCODMAG ) 
 w_MVLOTMAT=iif( Empty( w_MVCODLOT ) And Empty( w_MVCODUB2 ) , SPACE(5) , w_MVCODMAT )
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVNAZPRO ="+cp_NullLink(cp_ToStrODBC(w_MVNAZPRO),'DOC_DETT','MVNAZPRO');
            +",MVPROORD ="+cp_NullLink(cp_ToStrODBC(w_MVPROORD),'DOC_DETT','MVPROORD');
            +",MVCODCOS ="+cp_NullLink(cp_ToStrODBC(w_MVCODCOS),'DOC_DETT','MVCODCOS');
            +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(w_MVFLULCA),'DOC_DETT','MVFLULCA');
            +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(w_MVFLULPV),'DOC_DETT','MVFLULPV');
            +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(w_MVVALULT),'DOC_DETT','MVVALULT');
            +",MVRIFORD ="+cp_NullLink(cp_ToStrODBC(w_MVRIFORD),'DOC_DETT','MVRIFORD');
            +",MVIMPRBA ="+cp_NullLink(cp_ToStrODBC(w_MVIMPRBA),'DOC_DETT','MVIMPRBA');
            +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(w_MVFLELAN),'DOC_DETT','MVFLELAN');
            +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
            +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(w_MVCODLOT),'DOC_DETT','MVCODLOT');
            +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(w_MVCODUBI),'DOC_DETT','MVCODUBI');
            +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(w_MVCODUB2),'DOC_DETT','MVCODUB2');
            +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
            +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
            +",MVDATOAI ="+cp_NullLink(cp_ToStrODBC(w_MVDATOAI),'DOC_DETT','MVDATOAI');
            +",MVDATOAF ="+cp_NullLink(cp_ToStrODBC(w_MVDATOAF),'DOC_DETT','MVDATOAF');
            +",MVMC_PER ="+cp_NullLink(cp_ToStrODBC(w_MVMC_PER),'DOC_DETT','MVMC_PER');
            +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
            +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVNAZPRO = w_MVNAZPRO;
                ,MVPROORD = w_MVPROORD;
                ,MVCODCOS = w_MVCODCOS;
                ,MVFLULCA = w_MVFLULCA;
                ,MVFLULPV = w_MVFLULPV;
                ,MVVALULT = w_MVVALULT;
                ,MVRIFORD = w_MVRIFORD;
                ,MVIMPRBA = w_MVIMPRBA;
                ,MVFLELAN = w_MVFLELAN;
                ,MV_SEGNO = w_MV_SEGNO;
                ,MVCODLOT = w_MVCODLOT;
                ,MVCODUBI = w_MVCODUBI;
                ,MVCODUB2 = w_MVCODUB2;
                ,MVFLLOTT = w_MVFLLOTT;
                ,MVF2LOTT = w_MVF2LOTT;
                ,MVDATOAI = w_MVDATOAI;
                ,MVDATOAF = w_MVDATOAF;
                ,MVMC_PER = w_MVMC_PER;
                ,MVLOTMAG = w_MVLOTMAG;
                ,MVLOTMAT = w_MVLOTMAT;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.oParentObject.ULT_RNUM;
                and MVNUMRIF = w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if w_MVPERPRO=0
            * --- Se non ho importato un valore specifico di provvigione
            if g_PROGEN= "S"
              this.w_CODAZI = i_CODAZI
              this.w_SERPRO = this.w_MVSERIAL
              this.w_MASSGEN = "S"
              * --- Considero sempre come se precedentemente avesi avuto disattivo
              *     In questo modo mi aggiorna il documento generato nel modo corretto
              this.w_OLDCALPRO = "DI"
              * --- Read from PAR_PROV
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_PROV_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PPCALPRO,PPCALSCO,PPLISRIF"+;
                  " from "+i_cTable+" PAR_PROV where ";
                      +"PPCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PPCALPRO,PPCALSCO,PPLISRIF;
                  from (i_cTable) where;
                      PPCODAZI = this.w_CODAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PPCALPRO = NVL(cp_ToDate(_read_.PPCALPRO),cp_NullValue(_read_.PPCALPRO))
                this.w_PPCALSCO = NVL(cp_ToDate(_read_.PPCALSCO),cp_NullValue(_read_.PPCALSCO))
                this.w_PPLISRIF = NVL(cp_ToDate(_read_.PPLISRIF),cp_NullValue(_read_.PPLISRIF))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              do GSVE_BPP with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        else
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVCODVAR ="+cp_NullLink(cp_ToStrODBC(w_MVCODVAR),'DOC_DETT','MVCODVAR');
            +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
            +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
            +",MVFL2LOTT ="+cp_NullLink(cp_ToStrODBC(w_MVF2LOTT),'DOC_DETT','MVFL2LOTT');
            +",MVCODBUN ="+cp_NullLink(cp_ToStrODBC(w_MVCODBUN),'DOC_DETT','MVCODBUN');
            +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(w_MVFLELAN),'DOC_DETT','MVFLELAN');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(w_MVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVCODVAR = w_MVCODVAR;
                ,MV_SEGNO = w_MV_SEGNO;
                ,MVFLLOTT = w_MVFLLOTT;
                ,MVFL2LOTT = w_MVF2LOTT;
                ,MVCODBUN = w_MVCODBUN;
                ,MVFLELAN = w_MVFLELAN;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.oParentObject.ULT_RNUM;
                and MVNUMRIF = w_MVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if this.oParentObject.w_IMAGGSAL="S" .and. (.not. empty( w_MVKEYSAL )) .and. w_MVTIPRIG="R" .and. this.w_CmFlaVal <>"N" AND g_MADV="S" AND g_PERLOT="S"
          * --- Aggiorna saldi per i documenti
          GSMD_BRL (this, this.w_MVSERIAL , "V" , "+" , this.oParentObject.ULT_RNUM ,.T.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Segnala documento gia esistente
        this.oParentObject.w_ResoDett = AH_MSGFORMAT("Serial %1 numero documento %2 del %3" , trim(this.w_MVSERIAL) , alltrim(str(w_MVNUMDOC,15,0)) , dtoc(w_MVDATDOC) )
        this.oParentObject.w_ResoMode = "SCARTO"
        i_TrsMsg = ah_msgformat("Impossibile aggiungere righe a documento gi� provvisto di dettaglio")
        this.oParentObject.ULT_SERI = ""
        * --- Raise
        i_Error="errore"
        return
      endif
    endif
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_Destinaz $ "DO-DR" AND Isalt()
      * --- Prestazioni per anagrafica
      w_MVDATREG = iif( empty(w_MVDATREG), i_datsys, w_MVDATREG )
      if Not Empty(this.w_MVSERIAL) and Type("w_TIPDOC")="C" and Not Empty(w_TIPDOC) and Len(alltrim(w_TIPDOC))=1
        if Type("w_PRTIPRI2")="U"
          this.w_PRTIPRI2="N"
        endif
        do case
          case w_TIPDOC="F"
            this.w_PRROWFAT = this.oParentObject.ULT_RNUM
            this.w_PRRIFFAT = this.w_MVSERIAL
          case w_TIPDOC="P"
            this.w_PRROWPRO = this.oParentObject.ULT_RNUM
            this.w_PRRIFPRO = this.w_MVSERIAL
          case w_TIPDOC="N"
            this.w_PRROWNOT = this.oParentObject.ULT_RNUM
            this.w_PRRIFNOT = this.w_MVSERIAL
          case w_TIPDOC="A"
            this.w_PRROWFAA = this.oParentObject.ULT_RNUM
            this.w_PRRIFFAA = this.w_MVSERIAL
          case w_TIPDOC="B"
            this.w_PRROWBOZ = this.oParentObject.ULT_RNUM
            this.w_PRRIFBOZ = this.w_MVSERIAL
        endcase
        this.w_PRSERIAL = SPACE(10)
        i_Conn=i_TableProp[this.PRE_STAZ_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEPRE", "i_codazi,w_PRSERIAL")
        * --- Prestazioni per anagrafica
        if w_GENPRO="S"
          * --- Non � stato specificato nessun progressivo della prestazione
          this.w_PRNUMPRE = 0
          i_Conn=i_TableProp[this.PRE_STAZ_IDX, 3]
          cp_NextTableProg(this, i_Conn, "NUPRE", "i_codazi,w_PRNUMPRE")
        endif
        * --- Insert into PRE_STAZ
        i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRE_STAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PRSERIAL"+",PRCODRES"+",PRGRURES"+",PRNUMPRA"+",PR__DATA"+",PRCODESE"+",PRCODATT"+",PRDESPRE"+",PRDESAGG"+",PRCODOPE"+",PRDATMOD"+",PRUNIMIS"+",PRQTAMOV"+",PRQTAUM1"+",PRPREZZO"+",PRVALRIG"+",PROREEFF"+",PRMINEFF"+",PRCOSINT"+",PRFLDEFF"+",PRPREMIN"+",PRPREMAX"+",PRGAZUFF"+",PRCODVAL"+",PRCODLIS"+",PRCENCOS"+",PRVOCCOS"+",PR_SEGNO"+",PRATTIVI"+",PRINICOM"+",PRFINCOM"+",PRCODNOM"+",PRCODSED"+",PRTIPRIG"+",PRCOSUNI"+",PRTCOINI"+",PRTCOFIN"+",PRRIFPRE"+",PRRIGPRE"+",PRTIPRI2"+",PRRIFFAT"+",PRRIFFAA"+",PRRIFPRO"+",PRRIFPRA"+",PRSERAGG"+",PRTIPDOC"+",PRROWFAT"+",PRROWPRO"+",PRROWFAA"+",PRROWPRA"+",PRNUMPRE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PRSERIAL),'PRE_STAZ','PRSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODRES),'PRE_STAZ','PRCODRES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRGRURES),'PRE_STAZ','PRGRURES');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODCOM),'PRE_STAZ','PRNUMPRA');
          +","+cp_NullLink(cp_ToStrODBC(w_MVDATOAI),'PRE_STAZ','PR__DATA');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODESE),'PRE_STAZ','PRCODESE');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODICE),'PRE_STAZ','PRCODATT');
          +","+cp_NullLink(cp_ToStrODBC(w_MVDESART),'PRE_STAZ','PRDESPRE');
          +","+cp_NullLink(cp_ToStrODBC(w_MVDESSUP),'PRE_STAZ','PRDESAGG');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRE_STAZ','PRCODOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRDATMOD),'PRE_STAZ','PRDATMOD');
          +","+cp_NullLink(cp_ToStrODBC(w_MVUNIMIS),'PRE_STAZ','PRUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(w_MVQTAMOV),'PRE_STAZ','PRQTAMOV');
          +","+cp_NullLink(cp_ToStrODBC(w_MVQTAUM1),'PRE_STAZ','PRQTAUM1');
          +","+cp_NullLink(cp_ToStrODBC(w_MVPREZZO),'PRE_STAZ','PRPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(w_MVVALRIG),'PRE_STAZ','PRVALRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEOREEFF),'PRE_STAZ','PROREEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEMINEFF),'PRE_STAZ','PRMINEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DECOSINT),'PRE_STAZ','PRCOSINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRFLDEFF),'PRE_STAZ','PRFLDEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEPREMIN),'PRE_STAZ','PRPREMIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEPREMAX),'PRE_STAZ','PRPREMAX');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEGAZUFF),'PRE_STAZ','PRGAZUFF');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODVAL),'PRE_STAZ','PRCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODLIS),'PRE_STAZ','PRCODLIS');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODCEN),'PRE_STAZ','PRCENCOS');
          +","+cp_NullLink(cp_ToStrODBC(w_MVVOCCEN),'PRE_STAZ','PRVOCCOS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PR_SEGNO),'PRE_STAZ','PR_SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(w_MVCODATT),'PRE_STAZ','PRATTIVI');
          +","+cp_NullLink(cp_ToStrODBC(w_MVINICOM),'PRE_STAZ','PRINICOM');
          +","+cp_NullLink(cp_ToStrODBC(w_MVFINCOM),'PRE_STAZ','PRFINCOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODNOM),'PRE_STAZ','PRCODNOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODSED),'PRE_STAZ','PRCODSED');
          +","+cp_NullLink(cp_ToStrODBC("D"),'PRE_STAZ','PRTIPRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCOSUNI),'PRE_STAZ','PRCOSUNI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRTCOINI),'PRE_STAZ','PRTCOINI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRTCOFIN),'PRE_STAZ','PRTCOFIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIFPRE),'PRE_STAZ','PRRIFPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIGPRE),'PRE_STAZ','PRRIGPRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRTIPRI2),'PRE_STAZ','PRTIPRI2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIFFAT),'PRE_STAZ','PRRIFFAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIFFAA),'PRE_STAZ','PRRIFFAA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIFPRO),'PRE_STAZ','PRRIFPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRRIFPRA),'PRE_STAZ','PRRIFPRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRSERAGG),'PRE_STAZ','PRSERAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRTIPDOC),'PRE_STAZ','PRTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWFAT),'PRE_STAZ','PRROWFAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWPRO),'PRE_STAZ','PRROWPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWFAA),'PRE_STAZ','PRROWFAA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWPRA),'PRE_STAZ','PRROWPRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRNUMPRE),'PRE_STAZ','PRNUMPRE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PRSERIAL',this.w_PRSERIAL,'PRCODRES',w_MVCODRES,'PRGRURES',this.w_PRGRURES,'PRNUMPRA',w_MVCODCOM,'PR__DATA',w_MVDATOAI,'PRCODESE',w_MVCODESE,'PRCODATT',w_MVCODICE,'PRDESPRE',w_MVDESART,'PRDESAGG',w_MVDESSUP,'PRCODOPE',i_CODUTE,'PRDATMOD',this.w_PRDATMOD,'PRUNIMIS',w_MVUNIMIS)
          insert into (i_cTable) (PRSERIAL,PRCODRES,PRGRURES,PRNUMPRA,PR__DATA,PRCODESE,PRCODATT,PRDESPRE,PRDESAGG,PRCODOPE,PRDATMOD,PRUNIMIS,PRQTAMOV,PRQTAUM1,PRPREZZO,PRVALRIG,PROREEFF,PRMINEFF,PRCOSINT,PRFLDEFF,PRPREMIN,PRPREMAX,PRGAZUFF,PRCODVAL,PRCODLIS,PRCENCOS,PRVOCCOS,PR_SEGNO,PRATTIVI,PRINICOM,PRFINCOM,PRCODNOM,PRCODSED,PRTIPRIG,PRCOSUNI,PRTCOINI,PRTCOFIN,PRRIFPRE,PRRIGPRE,PRTIPRI2,PRRIFFAT,PRRIFFAA,PRRIFPRO,PRRIFPRA,PRSERAGG,PRTIPDOC,PRROWFAT,PRROWPRO,PRROWFAA,PRROWPRA,PRNUMPRE &i_ccchkf. );
             values (;
               this.w_PRSERIAL;
               ,w_MVCODRES;
               ,this.w_PRGRURES;
               ,w_MVCODCOM;
               ,w_MVDATOAI;
               ,w_MVCODESE;
               ,w_MVCODICE;
               ,w_MVDESART;
               ,w_MVDESSUP;
               ,i_CODUTE;
               ,this.w_PRDATMOD;
               ,w_MVUNIMIS;
               ,w_MVQTAMOV;
               ,w_MVQTAUM1;
               ,w_MVPREZZO;
               ,w_MVVALRIG;
               ,this.w_DEOREEFF;
               ,this.w_DEMINEFF;
               ,this.w_DECOSINT;
               ,this.w_PRFLDEFF;
               ,this.w_DEPREMIN;
               ,this.w_DEPREMAX;
               ,this.w_DEGAZUFF;
               ,w_MVCODVAL;
               ,w_MVCODLIS;
               ,w_MVCODCEN;
               ,w_MVVOCCEN;
               ,this.w_PR_SEGNO;
               ,w_MVCODATT;
               ,w_MVINICOM;
               ,w_MVFINCOM;
               ,this.w_PRCODNOM;
               ,this.w_PRCODSED;
               ,"D";
               ,this.w_PRCOSUNI;
               ,this.w_PRTCOINI;
               ,this.w_PRTCOFIN;
               ,this.w_PRRIFPRE;
               ,this.w_PRRIGPRE;
               ,this.w_PRTIPRI2;
               ,this.w_PRRIFFAT;
               ,this.w_PRRIFFAA;
               ,this.w_PRRIFPRO;
               ,this.w_PRRIFPRA;
               ,this.w_PRSERAGG;
               ,this.w_PRTIPDOC;
               ,this.w_PRROWFAT;
               ,this.w_PRROWPRO;
               ,this.w_PRROWFAA;
               ,this.w_PRROWPRA;
               ,this.w_PRNUMPRE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,25)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DOC_DETT'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='LISTINI'
    this.cWorkTables[10]='MVM_DETT'
    this.cWorkTables[11]='MVM_MAST'
    this.cWorkTables[12]='SALDIART'
    this.cWorkTables[13]='TIP_DOCU'
    this.cWorkTables[14]='VALUTE'
    this.cWorkTables[15]='VOCIIVA'
    this.cWorkTables[16]='UNIMIS'
    this.cWorkTables[17]='ASPETTO'
    this.cWorkTables[18]='MAGAZZIN'
    this.cWorkTables[19]='AZIENDA'
    this.cWorkTables[20]='PAR_PROV'
    this.cWorkTables[21]='ALT_DETT'
    this.cWorkTables[22]='LISTART'
    this.cWorkTables[23]='VDATRITE'
    this.cWorkTables[24]='PRE_STAZ'
    this.cWorkTables[25]='SALDICOM'
    return(this.OpenAllTables(25))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
