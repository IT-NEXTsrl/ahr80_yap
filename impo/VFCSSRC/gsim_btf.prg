* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_btf                                                        *
*              Trasf. tabella                                                  *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_50]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2006-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_btf",oParentObject,m.w_CURSOR)
return(i_retval)

define class tgsim_btf as StdBatch
  * --- Local variables
  w_CURSOR = space(15)
  w_TBNAME = space(5)
  w_AUY = space(6)
  w_TBDATI = space(6)
  w_TBFIELD = space(6)
  w_TMPCURS = space(0)
  w_cTipCod = space(1)
  w_nFld = 0
  w_nStart = 0
  w_ni = 0
  w_cFldDes = space(10)
  w_cFld = space(10)
  w_cFldT = space(10)
  w_cFldL = space(10)
  w_cSelFld = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TRASFORMA CURSORE CON STRUTTURA RECORD TABELLA CP WINDOWS IN TABELLA
    * --- Init
    this.w_cSelFld = ""
    this.w_nStart = 1
    * --- Elabora struttura tabella
    if used(this.w_CURSOR)
      select (this.w_CURSOR)
      go top
      if reccount() > 0
        * --- Legge nome tabella
        this.w_TBNAME = upper(left(tbchiave+repl(" ",5),5))
        SCAN FOR tbchiave=this.w_TBNAME+"\d"
        this.w_cFldDes = tb__dati
        ENDSCAN
        if !empty(this.w_cFldDes)
          SCAN FOR tbchiave=this.w_TBNAME+"\v3"
          this.w_cTipCod = right(tb__dati,1)
          this.w_nFld = val(left(this.w_cFldDes,2))
          for this.w_ni=1 to this.w_nFld
          this.w_cFld = substr(this.w_cFldDes,(this.w_ni-1)*10+3,6)
          this.w_cFldT = substr(this.w_cFldDes,(this.w_ni-1)*10+9,1)
          this.w_cFldL = substr(this.w_cFldDes,(this.w_ni-1)*10+10,3)
          do case
            case this.w_cFldT="C"
              this.w_cSelFld = this.w_cSelFld +",substr(tb__dati,"+alltrim(str(this.w_nStart))+","+alltrim(this.w_cFldL)+") as "+this.w_cFld
              this.w_nStart = this.w_nStart+val(this.w_cFldL)
            case this.w_cFldT="N"
              this.w_cSelFld = this.w_cSelFld + ",val(strtran(substr(tb__dati,"+alltrim(str(this.w_nStart))+","+alltrim(left(this.w_cFldL,2))+'),".",",")) as '+this.w_cFld
              this.w_nStart = this.w_nStart+val(left(this.w_cFldL,2))
            case this.w_cFldT="D"
              this.w_cSelFld = this.w_cSelFld + ",cp_CharToDate(substr(tb__dati,"+alltrim(str(this.w_nStart))+",8)) as "+this.w_cFld
              this.w_nStart = this.w_nStart+8
            case this.w_cFldT="L"
              this.w_cSelFld = this.w_cSelFld + ",(substr(tb__dati,"+alltrim(str(this.w_nStart))+',1)="T")  as '+this.w_cFld
              this.w_nStart = this.w_nStart+1
          endcase
          next
          ENDSCAN
        endif
      endif
    endif
    * --- Estrae informazioni da cursore in _read_
    if !empty(this.w_cSelFld)
      this.w_cSelFld=iif(this.w_cTipCod="C","substr(tbchiave,6)","int(val(substr(tbchiave,6)))") + " as codice " + this.w_cSelFld
      if used("_read_")
        select _read_
        use
      endif
      * --- Crea cursore _read_
      cSelFld = this.w_cSelFld
      if empty(this.w_AUY) 
 
        select &cSelFld from (this.w_CURSOR) where left(tbchiave,5)=(this.w_TBNAME) and substr(tbchiave,6,1)<>"\" into cursor _read_ nofilter
      endif
      if _tally >= 0
        * --- Crea nuovo cursore w_CURSOR
        select * from _read_ into cursor (this.w_CURSOR) nofilter
        if used("_read_")
          select _read_
          use
        endif
      else
        AH_ERRORMSG("Struttura tabella Codepainter Windows non trovata","i","")
      endif
    else
      AH_ERRORMSG("Struttura tabella Codepainter Windows non trovata","i","")
    endif
  endproc


  proc Init(oParentObject,w_CURSOR)
    this.w_CURSOR=w_CURSOR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR"
endproc
