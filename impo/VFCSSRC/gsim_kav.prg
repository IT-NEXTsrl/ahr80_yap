* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kav                                                        *
*              Opzioni avanzate                                                *
*                                                                              *
*      Author: TAM Software & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2010-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kav",oParentObject))

* --- Class definition
define class tgsim_kav as StdForm
  Top    = 107
  Left   = 52

  * --- Standard Properties
  Width  = 704
  Height = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-08-27"
  HelpContextID=132759401
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsim_kav"
  cComment = "Opzioni avanzate"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_IMDESTIN = space(2)
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_IMFILTRO = space(70)
  w_IMCHIMOV = space(70)
  w_IMCONIND = space(15)
  w_ANDESCRI = space(40)
  w_IMAGGNUR = space(1)
  w_IMAGGIVA = space(1)
  w_IMAGGPAR = space(1)
  w_IMAGGANA = space(1)
  w_IMAGGSAL = space(1)
  w_IMROUTIN = space(30)
  w_IMTIPSRC = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kavPag1","gsim_kav",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMFILTRO_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IMDESTIN=space(2)
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_IMFILTRO=space(70)
      .w_IMCHIMOV=space(70)
      .w_IMCONIND=space(15)
      .w_ANDESCRI=space(40)
      .w_IMAGGNUR=space(1)
      .w_IMAGGIVA=space(1)
      .w_IMAGGPAR=space(1)
      .w_IMAGGANA=space(1)
      .w_IMAGGSAL=space(1)
      .w_IMROUTIN=space(30)
      .w_IMTIPSRC=space(10)
      .w_IMDESTIN=oParentObject.w_IMDESTIN
      .w_IMFILTRO=oParentObject.w_IMFILTRO
      .w_IMCHIMOV=oParentObject.w_IMCHIMOV
      .w_IMCONIND=oParentObject.w_IMCONIND
      .w_IMAGGNUR=oParentObject.w_IMAGGNUR
      .w_IMAGGIVA=oParentObject.w_IMAGGIVA
      .w_IMAGGPAR=oParentObject.w_IMAGGPAR
      .w_IMAGGANA=oParentObject.w_IMAGGANA
      .w_IMAGGSAL=oParentObject.w_IMAGGSAL
      .w_IMROUTIN=oParentObject.w_IMROUTIN
      .w_IMTIPSRC=oParentObject.w_IMTIPSRC
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = "G"
        .w_OBTEST = i_DATSYS
          .DoRTCalc(4,5,.f.)
        .w_IMCONIND = .w_IMCONIND
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_IMCONIND))
          .link_1_8('Full')
        endif
    endwith
    this.DoRTCalc(7,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_IMDESTIN=.w_IMDESTIN
      .oParentObject.w_IMFILTRO=.w_IMFILTRO
      .oParentObject.w_IMCHIMOV=.w_IMCHIMOV
      .oParentObject.w_IMCONIND=.w_IMCONIND
      .oParentObject.w_IMAGGNUR=.w_IMAGGNUR
      .oParentObject.w_IMAGGIVA=.w_IMAGGIVA
      .oParentObject.w_IMAGGPAR=.w_IMAGGPAR
      .oParentObject.w_IMAGGANA=.w_IMAGGANA
      .oParentObject.w_IMAGGSAL=.w_IMAGGSAL
      .oParentObject.w_IMROUTIN=.w_IMROUTIN
      .oParentObject.w_IMTIPSRC=.w_IMTIPSRC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIMCONIND_1_8.enabled = this.oPgFrm.Page1.oPag.oIMCONIND_1_8.mCond()
    this.oPgFrm.Page1.oPag.oIMAGGNUR_1_13.enabled = this.oPgFrm.Page1.oPag.oIMAGGNUR_1_13.mCond()
    this.oPgFrm.Page1.oPag.oIMAGGIVA_1_14.enabled = this.oPgFrm.Page1.oPag.oIMAGGIVA_1_14.mCond()
    this.oPgFrm.Page1.oPag.oIMAGGPAR_1_15.enabled = this.oPgFrm.Page1.oPag.oIMAGGPAR_1_15.mCond()
    this.oPgFrm.Page1.oPag.oIMAGGANA_1_16.enabled = this.oPgFrm.Page1.oPag.oIMAGGANA_1_16.mCond()
    this.oPgFrm.Page1.oPag.oIMAGGSAL_1_17.enabled = this.oPgFrm.Page1.oPag.oIMAGGSAL_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIMROUTIN_1_20.visible=!this.oPgFrm.Page1.oPag.oIMROUTIN_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IMCONIND
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMCONIND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_IMCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_IMCONIND))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMCONIND)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_IMCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_IMCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IMCONIND) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oIMCONIND_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMCONIND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IMCONIND);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_IMCONIND)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMCONIND = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IMCONIND = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMCONIND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIMFILTRO_1_4.value==this.w_IMFILTRO)
      this.oPgFrm.Page1.oPag.oIMFILTRO_1_4.value=this.w_IMFILTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCHIMOV_1_7.value==this.w_IMCHIMOV)
      this.oPgFrm.Page1.oPag.oIMCHIMOV_1_7.value=this.w_IMCHIMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCONIND_1_8.value==this.w_IMCONIND)
      this.oPgFrm.Page1.oPag.oIMCONIND_1_8.value=this.w_IMCONIND
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_9.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_9.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGGNUR_1_13.RadioValue()==this.w_IMAGGNUR)
      this.oPgFrm.Page1.oPag.oIMAGGNUR_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGGIVA_1_14.RadioValue()==this.w_IMAGGIVA)
      this.oPgFrm.Page1.oPag.oIMAGGIVA_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGGPAR_1_15.RadioValue()==this.w_IMAGGPAR)
      this.oPgFrm.Page1.oPag.oIMAGGPAR_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGGANA_1_16.RadioValue()==this.w_IMAGGANA)
      this.oPgFrm.Page1.oPag.oIMAGGANA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGGSAL_1_17.RadioValue()==this.w_IMAGGSAL)
      this.oPgFrm.Page1.oPag.oIMAGGSAL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMROUTIN_1_20.value==this.w_IMROUTIN)
      this.oPgFrm.Page1.oPag.oIMROUTIN_1_20.value=this.w_IMROUTIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsim_kavPag1 as StdContainer
  Width  = 700
  height = 269
  stdWidth  = 700
  stdheight = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMFILTRO_1_4 as StdField with uid="VXXYKLKLQA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMFILTRO", cQueryName = "IMFILTRO",;
    bObbl = .f. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    ToolTipText = "Specificare l'espressione di filtro dei records da importare",;
    HelpContextID = 19133141,;
   bGlobalFont=.t.,;
    Height=21, Width=511, Left=178, Top=88, InputMask=replicate('X',70)

  add object oIMCHIMOV_1_7 as StdField with uid="ZWVUIGBJEU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IMCHIMOV", cQueryName = "IMCHIMOV",;
    bObbl = .f. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la chiave che identifica i records della stessa registrazione",;
    HelpContextID = 166904540,;
   bGlobalFont=.t.,;
    Height=21, Width=511, Left=178, Top=113, InputMask=replicate('X',70)

  add object oIMCONIND_1_8 as StdField with uid="QECVHFUZZG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IMCONIND", cQueryName = "IMCONIND",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto di costo da usare al posto dei conti IVA nelle righe con IVA indetraibile",;
    HelpContextID = 162938166,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=178, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_IMCONIND"

  func oIMCONIND_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN="PN")
    endwith
   endif
  endfunc

  func oIMCONIND_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oIMCONIND_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMCONIND_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oIMCONIND_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oANDESCRI_1_9 as StdField with uid="EYJHEKBDRN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9425743,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=310, Top=138, InputMask=replicate('X',40)

  add object oIMAGGNUR_1_13 as StdCheck with uid="UDNXGDWHTP",rtseq=8,rtrep=.f.,left=27, top=180, caption="Numeri registrazioni",;
    ToolTipText = "Se attivato: la procedura ricalcola la numerazione delle registrazioni.",;
    HelpContextID = 181510872,;
    cFormVar="w_IMAGGNUR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAGGNUR_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAGGNUR_1_13.GetRadio()
    this.Parent.oContained.w_IMAGGNUR = this.RadioValue()
    return .t.
  endfunc

  func oIMAGGNUR_1_13.SetRadio()
    this.Parent.oContained.w_IMAGGNUR=trim(this.Parent.oContained.w_IMAGGNUR)
    this.value = ;
      iif(this.Parent.oContained.w_IMAGGNUR=='S',1,;
      0)
  endfunc

  func oIMAGGNUR_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN $ "PN|MC|MM")
    endwith
   endif
  endfunc

  add object oIMAGGIVA_1_14 as StdCheck with uid="VPEHNHVXJQ",rtseq=9,rtrep=.f.,left=27, top=199, caption="Aggiorna righe IVA",;
    ToolTipText = "Se attivato: aggiorna le righe IVA della primanota",;
    HelpContextID = 97624775,;
    cFormVar="w_IMAGGIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAGGIVA_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAGGIVA_1_14.GetRadio()
    this.Parent.oContained.w_IMAGGIVA = this.RadioValue()
    return .t.
  endfunc

  func oIMAGGIVA_1_14.SetRadio()
    this.Parent.oContained.w_IMAGGIVA=trim(this.Parent.oContained.w_IMAGGIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAGGIVA=='S',1,;
      0)
  endfunc

  func oIMAGGIVA_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN="PN")
    endwith
   endif
  endfunc

  add object oIMAGGPAR_1_15 as StdCheck with uid="BXAFXNWCZF",rtseq=10,rtrep=.f.,left=27, top=218, caption="Aggiorna partite in automatico",;
    ToolTipText = "Se attivato: aggiorna le partite",;
    HelpContextID = 215065304,;
    cFormVar="w_IMAGGPAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAGGPAR_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAGGPAR_1_15.GetRadio()
    this.Parent.oContained.w_IMAGGPAR = this.RadioValue()
    return .t.
  endfunc

  func oIMAGGPAR_1_15.SetRadio()
    this.Parent.oContained.w_IMAGGPAR=trim(this.Parent.oContained.w_IMAGGPAR)
    this.value = ;
      iif(this.Parent.oContained.w_IMAGGPAR=='S',1,;
      0)
  endfunc

  func oIMAGGPAR_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN="PN")
    endwith
   endif
  endfunc

  add object oIMAGGANA_1_16 as StdCheck with uid="KAASSTIVSF",rtseq=11,rtrep=.f.,left=278, top=180, caption="Aggiorna analitica",;
    ToolTipText = "Se attivato: aggiorna gli archivi dell'analitica",;
    HelpContextID = 36592953,;
    cFormVar="w_IMAGGANA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAGGANA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAGGANA_1_16.GetRadio()
    this.Parent.oContained.w_IMAGGANA = this.RadioValue()
    return .t.
  endfunc

  func oIMAGGANA_1_16.SetRadio()
    this.Parent.oContained.w_IMAGGANA=trim(this.Parent.oContained.w_IMAGGANA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAGGANA=='S',1,;
      0)
  endfunc

  func oIMAGGANA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN="PN")
    endwith
   endif
  endfunc

  add object oIMAGGSAL_1_17 as StdCheck with uid="EIUKIXNJFS",rtseq=12,rtrep=.f.,left=278, top=199, caption="Aggiorna saldi",;
    ToolTipText = "Se attivato: aggiorna i saldi degli archivi collegati",;
    HelpContextID = 265396946,;
    cFormVar="w_IMAGGSAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAGGSAL_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAGGSAL_1_17.GetRadio()
    this.Parent.oContained.w_IMAGGSAL = this.RadioValue()
    return .t.
  endfunc

  func oIMAGGSAL_1_17.SetRadio()
    this.Parent.oContained.w_IMAGGSAL=trim(this.Parent.oContained.w_IMAGGSAL)
    this.value = ;
      iif(this.Parent.oContained.w_IMAGGSAL=='S',1,;
      0)
  endfunc

  func oIMAGGSAL_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMDESTIN$"PN-MM-DR-DO")
    endwith
   endif
  endfunc

  add object oIMROUTIN_1_20 as StdField with uid="TAYFVDBLNH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IMROUTIN", cQueryName = "IMROUTIN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Routine di normalizzazione file ASCII",;
    HelpContextID = 239422764,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=478, Top=191, InputMask=replicate('X',30)

  func oIMROUTIN_1_20.mHide()
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'A')
    endwith
  endfunc


  add object oBtn_1_21 as StdButton with uid="VQERAGEWET",left=589, top=220, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 132730650;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="MRPTSAHHIT",left=644, top=220, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 125441978;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="FSUVHCEESQ",Visible=.t., Left=25, Top=89,;
    Alignment=1, Width=150, Height=15,;
    Caption="Espressione di filtro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DJZNIZDGTQ",Visible=.t., Left=25, Top=114,;
    Alignment=1, Width=150, Height=15,;
    Caption="Chiave comune records:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="YJURVPZWQF",Visible=.t., Left=19, Top=21,;
    Alignment=0, Width=664, Height=18,;
    Caption="Nelle espressioni seguenti � necessario inserire il suffisso w_ davanti ai nomi dei campi."  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="WXMOVDXXGL",Visible=.t., Left=19, Top=43,;
    Alignment=0, Width=664, Height=15,;
    Caption="Ad esempio il campo ancodice deve essere utilizzato come w_ancodice."  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="NZUSMKNURJ",Visible=.t., Left=8, Top=139,;
    Alignment=1, Width=167, Height=15,;
    Caption="Conto IVA indetraibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LTQWPYBOZL",Visible=.t., Left=478, Top=173,;
    Alignment=0, Width=213, Height=18,;
    Caption="Routine di normalizzazione file ASCII"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_IMTIPSRC<>'A')
    endwith
  endfunc

  add object oBox_1_10 as StdBox with uid="LXNECEOARK",left=8, top=16, width=683,height=51

  add object oBox_1_18 as StdBox with uid="ZNWUQFQGTF",left=8, top=172, width=466,height=68

  add object oBox_1_24 as StdBox with uid="XYBXECVPNT",left=476, top=172, width=218,height=46
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kav','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
