* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_apr                                                        *
*              Valori predefiniti                                              *
*                                                                              *
*      Author: TAM SOFTWARE & CODE LAB                                         *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_7]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-28                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_apr"))

* --- Class definition
define class tgsim_apr as StdForm
  Top    = 5
  Left   = 58

  * --- Standard Properties
  Width  = 676
  Height = 417+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=160022377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  PREDEFIN_IDX = 0
  IMPOARCH_IDX = 0
  PREDEIMP_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "PREDEFIN"
  cKeySelect = "PRCODFIL,PRNOMCAM"
  cKeyWhere  = "PRCODFIL=this.w_PRCODFIL and PRNOMCAM=this.w_PRNOMCAM"
  cKeyWhereODBC = '"PRCODFIL="+cp_ToStrODBC(this.w_PRCODFIL)';
      +'+" and PRNOMCAM="+cp_ToStrODBC(this.w_PRNOMCAM)';

  cKeyWhereODBCqualified = '"PREDEFIN.PRCODFIL="+cp_ToStrODBC(this.w_PRCODFIL)';
      +'+" and PREDEFIN.PRNOMCAM="+cp_ToStrODBC(this.w_PRNOMCAM)';

  cPrg = "gsim_apr"
  cComment = "Valori predefiniti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODFIL = space(2)
  w_ARDESCRI = space(20)
  w_PRNOMCAM = space(15)
  w_PRTIPCAM = space(1)
  w_PRLUNCAM = 0
  w_PRTIPVAL = space(1)
  w_PRVALPRE = space(0)
  w_ARTABELL = space(15)
  w_FLCOMMEN = space(80)

  * --- Children pointers
  gsim_mpi = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PREDEFIN','gsim_apr')
    stdPageFrame::Init()
    *set procedure to gsim_mpi additive
    with this
      .Pages(1).addobject("oPag","tgsim_aprPag1","gsim_apr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Valori predefiniti")
      .Pages(1).HelpContextID = 114853007
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCODFIL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsim_mpi
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='PREDEIMP'
    this.cWorkTables[3]='XDC_FIELDS'
    this.cWorkTables[4]='PREDEFIN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PREDEFIN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PREDEFIN_IDX,3]
  return

  function CreateChildren()
    this.gsim_mpi = CREATEOBJECT('stdDynamicChild',this,'gsim_mpi',this.oPgFrm.Page1.oPag.oLinkPC_1_12)
    this.gsim_mpi.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsim_mpi)
      this.gsim_mpi.DestroyChildrenChain()
      this.gsim_mpi=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsim_mpi.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsim_mpi.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsim_mpi.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsim_mpi.SetKey(;
            .w_PRCODFIL,"PICODFIL";
            ,.w_PRNOMCAM,"PINOMCAM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsim_mpi.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRCODFIL,"PICODFIL";
             ,.w_PRNOMCAM,"PINOMCAM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.gsim_mpi)
        i_f=.gsim_mpi.BuildFilter()
        if !(i_f==.gsim_mpi.cQueryFilter)
          i_fnidx=.gsim_mpi.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gsim_mpi.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gsim_mpi.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gsim_mpi.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gsim_mpi.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PRCODFIL = NVL(PRCODFIL,space(2))
      .w_PRNOMCAM = NVL(PRNOMCAM,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PREDEFIN where PRCODFIL=KeySet.PRCODFIL
    *                            and PRNOMCAM=KeySet.PRNOMCAM
    *
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PREDEFIN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PREDEFIN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PREDEFIN '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODFIL',this.w_PRCODFIL  ,'PRNOMCAM',this.w_PRNOMCAM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ARDESCRI = space(20)
        .w_ARTABELL = space(15)
        .w_FLCOMMEN = space(80)
        .w_PRCODFIL = NVL(PRCODFIL,space(2))
          if link_1_1_joined
            this.w_PRCODFIL = NVL(ARCODICE101,NVL(this.w_PRCODFIL,space(2)))
            this.w_ARDESCRI = NVL(ARDESCRI101,space(20))
            this.w_ARTABELL = NVL(ARTABELL101,space(15))
          else
          .link_1_1('Load')
          endif
        .w_PRNOMCAM = NVL(PRNOMCAM,space(15))
          .link_1_4('Load')
        .w_PRTIPCAM = NVL(PRTIPCAM,space(1))
        .w_PRLUNCAM = NVL(PRLUNCAM,0)
        .w_PRTIPVAL = NVL(PRTIPVAL,space(1))
        .w_PRVALPRE = NVL(PRVALPRE,space(0))
        cp_LoadRecExtFlds(this,'PREDEFIN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRCODFIL = space(2)
      .w_ARDESCRI = space(20)
      .w_PRNOMCAM = space(15)
      .w_PRTIPCAM = space(1)
      .w_PRLUNCAM = 0
      .w_PRTIPVAL = space(1)
      .w_PRVALPRE = space(0)
      .w_ARTABELL = space(15)
      .w_FLCOMMEN = space(80)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PRCODFIL))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,3,.f.)
          if not(empty(.w_PRNOMCAM))
          .link_1_4('Full')
          endif
        .w_PRTIPCAM = 'C'
          .DoRTCalc(5,5,.f.)
        .w_PRTIPVAL = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PREDEFIN')
    this.DoRTCalc(7,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRCODFIL_1_1.enabled = i_bVal
      .Page1.oPag.oPRNOMCAM_1_4.enabled = i_bVal
      .Page1.oPag.oPRTIPCAM_1_5.enabled = i_bVal
      .Page1.oPag.oPRLUNCAM_1_6.enabled = i_bVal
      .Page1.oPag.oPRTIPVAL_1_7.enabled = i_bVal
      .Page1.oPag.oPRVALPRE_1_10.enabled = i_bVal
      .Page1.oPag.oFLCOMMEN_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPRCODFIL_1_1.enabled = .f.
        .Page1.oPag.oPRNOMCAM_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPRCODFIL_1_1.enabled = .t.
        .Page1.oPag.oPRNOMCAM_1_4.enabled = .t.
      endif
    endwith
    this.gsim_mpi.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PREDEFIN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsim_mpi.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODFIL,"PRCODFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRNOMCAM,"PRNOMCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPCAM,"PRTIPCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLUNCAM,"PRLUNCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPVAL,"PRTIPVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRVALPRE,"PRVALPRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
    i_lTable = "PREDEFIN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PREDEFIN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PREDEFIN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PREDEFIN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PREDEFIN')
        i_extval=cp_InsertValODBCExtFlds(this,'PREDEFIN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM,PRTIPVAL"+;
                  ",PRVALPRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PRCODFIL)+;
                  ","+cp_ToStrODBCNull(this.w_PRNOMCAM)+;
                  ","+cp_ToStrODBC(this.w_PRTIPCAM)+;
                  ","+cp_ToStrODBC(this.w_PRLUNCAM)+;
                  ","+cp_ToStrODBC(this.w_PRTIPVAL)+;
                  ","+cp_ToStrODBC(this.w_PRVALPRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PREDEFIN')
        i_extval=cp_InsertValVFPExtFlds(this,'PREDEFIN')
        cp_CheckDeletedKey(i_cTable,0,'PRCODFIL',this.w_PRCODFIL,'PRNOMCAM',this.w_PRNOMCAM)
        INSERT INTO (i_cTable);
              (PRCODFIL,PRNOMCAM,PRTIPCAM,PRLUNCAM,PRTIPVAL,PRVALPRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRCODFIL;
                  ,this.w_PRNOMCAM;
                  ,this.w_PRTIPCAM;
                  ,this.w_PRLUNCAM;
                  ,this.w_PRTIPVAL;
                  ,this.w_PRVALPRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PREDEFIN_IDX,i_nConn)
      *
      * update PREDEFIN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PREDEFIN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRTIPCAM="+cp_ToStrODBC(this.w_PRTIPCAM)+;
             ",PRLUNCAM="+cp_ToStrODBC(this.w_PRLUNCAM)+;
             ",PRTIPVAL="+cp_ToStrODBC(this.w_PRTIPVAL)+;
             ",PRVALPRE="+cp_ToStrODBC(this.w_PRVALPRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PREDEFIN')
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODFIL',this.w_PRCODFIL  ,'PRNOMCAM',this.w_PRNOMCAM  )
        UPDATE (i_cTable) SET;
              PRTIPCAM=this.w_PRTIPCAM;
             ,PRLUNCAM=this.w_PRLUNCAM;
             ,PRTIPVAL=this.w_PRTIPVAL;
             ,PRVALPRE=this.w_PRVALPRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsim_mpi : Saving
      this.gsim_mpi.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRCODFIL,"PICODFIL";
             ,this.w_PRNOMCAM,"PINOMCAM";
             )
      this.gsim_mpi.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gsim_mpi : Deleting
    this.gsim_mpi.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRCODFIL,"PICODFIL";
           ,this.w_PRNOMCAM,"PINOMCAM";
           )
    this.gsim_mpi.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PREDEFIN_IDX,i_nConn)
      *
      * delete PREDEFIN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRCODFIL',this.w_PRCODFIL  ,'PRNOMCAM',this.w_PRNOMCAM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PREDEFIN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREDEFIN_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODFIL
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_AAR',True,'IMPOARCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_PRCODFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_PRCODFIL))
          select ARCODICE,ARDESCRI,ARTABELL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODFIL)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODFIL) and !this.bDontReportError
            deferred_cp_zoom('IMPOARCH','*','ARCODICE',cp_AbsName(oSource.parent,'oPRCODFIL_1_1'),i_cWhere,'GSIM_AAR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_PRCODFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_PRCODFIL)
            select ARCODICE,ARDESCRI,ARTABELL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODFIL = NVL(_Link_.ARCODICE,space(2))
      this.w_ARDESCRI = NVL(_Link_.ARDESCRI,space(20))
      this.w_ARTABELL = NVL(_Link_.ARTABELL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODFIL = space(2)
      endif
      this.w_ARDESCRI = space(20)
      this.w_ARTABELL = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMPOARCH_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.ARCODICE as ARCODICE101"+ ",link_1_1.ARDESCRI as ARDESCRI101"+ ",link_1_1.ARTABELL as ARTABELL101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PREDEFIN.PRCODFIL=link_1_1.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PREDEFIN.PRCODFIL=link_1_1.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRNOMCAM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNOMCAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_MST',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_PRNOMCAM)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELL);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ARTABELL;
                     ,'FLNAME',trim(this.w_PRNOMCAM))
          select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNOMCAM)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRNOMCAM) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oPRNOMCAM_1_4'),i_cWhere,'GSIM_MST',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTABELL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNOMCAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PRNOMCAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ARTABELL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ARTABELL;
                       ,'FLNAME',this.w_PRNOMCAM)
            select TBNAME,FLNAME,FLCOMMEN,FLTYPE,FLLENGHT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNOMCAM = NVL(_Link_.FLNAME,space(15))
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(80))
      this.w_PRTIPCAM = NVL(_Link_.FLTYPE,space(1))
      this.w_PRLUNCAM = NVL(_Link_.FLLENGHT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRNOMCAM = space(15)
      endif
      this.w_FLCOMMEN = space(80)
      this.w_PRTIPCAM = space(1)
      this.w_PRLUNCAM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNOMCAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRCODFIL_1_1.value==this.w_PRCODFIL)
      this.oPgFrm.Page1.oPag.oPRCODFIL_1_1.value=this.w_PRCODFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESCRI_1_2.value==this.w_ARDESCRI)
      this.oPgFrm.Page1.oPag.oARDESCRI_1_2.value=this.w_ARDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRNOMCAM_1_4.value==this.w_PRNOMCAM)
      this.oPgFrm.Page1.oPag.oPRNOMCAM_1_4.value=this.w_PRNOMCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPCAM_1_5.RadioValue()==this.w_PRTIPCAM)
      this.oPgFrm.Page1.oPag.oPRTIPCAM_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLUNCAM_1_6.value==this.w_PRLUNCAM)
      this.oPgFrm.Page1.oPag.oPRLUNCAM_1_6.value=this.w_PRLUNCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPVAL_1_7.RadioValue()==this.w_PRTIPVAL)
      this.oPgFrm.Page1.oPag.oPRTIPVAL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRVALPRE_1_10.value==this.w_PRVALPRE)
      this.oPgFrm.Page1.oPag.oPRVALPRE_1_10.value=this.w_PRVALPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oARTABELL_1_21.value==this.w_ARTABELL)
      this.oPgFrm.Page1.oPag.oARTABELL_1_21.value=this.w_ARTABELL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMMEN_1_22.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oFLCOMMEN_1_22.value=this.w_FLCOMMEN
    endif
    cp_SetControlsValueExtFlds(this,'PREDEFIN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PRCODFIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODFIL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PRCODFIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRNOMCAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRNOMCAM_1_4.SetFocus()
            i_bnoObbl = !empty(.w_PRNOMCAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRTIPCAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRTIPCAM_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PRTIPCAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRLUNCAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRLUNCAM_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PRLUNCAM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .gsim_mpi.CheckForm()
      if i_bres
        i_bres=  .gsim_mpi.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- gsim_mpi : Depends On
    this.gsim_mpi.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsim_aprPag1 as StdContainer
  Width  = 672
  height = 417
  stdWidth  = 672
  stdheight = 417
  resizeXpos=389
  resizeYpos=306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCODFIL_1_1 as StdField with uid="NWEGIJIYZU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRCODFIL", cQueryName = "PRCODFIL",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 17418306,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=213, Top=15, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_PRCODFIL"

  func oPRCODFIL_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODFIL_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODFIL_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPOARCH','*','ARCODICE',cp_AbsName(this.parent,'oPRCODFIL_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_AAR',"",'',this.parent.oContained
  endproc
  proc oPRCODFIL_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSIM_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_PRCODFIL
     i_obj.ecpSave()
  endproc

  add object oARDESCRI_1_2 as StdField with uid="ZYQTRNRUWT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ARDESCRI", cQueryName = "ARDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 17836209,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=257, Top=15, InputMask=replicate('X',20)

  add object oPRNOMCAM_1_4 as StdField with uid="SCPMWUQSUA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PRNOMCAM", cQueryName = "PRCODFIL,PRNOMCAM",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 23431101,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=213, Top=40, cSayPict='repl("!",15)', cGetPict='repl("!",15)', InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", cZoomOnZoom="GSIM_MST", oKey_1_1="TBNAME", oKey_1_2="this.w_ARTABELL", oKey_2_1="FLNAME", oKey_2_2="this.w_PRNOMCAM"

  func oPRNOMCAM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNOMCAM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRNOMCAM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ARTABELL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ARTABELL)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oPRNOMCAM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_MST',"",'',this.parent.oContained
  endproc
  proc oPRNOMCAM_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSIM_MST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TBNAME=w_ARTABELL
     i_obj.w_FLNAME=this.parent.oContained.w_PRNOMCAM
     i_obj.ecpSave()
  endproc


  add object oPRTIPCAM_1_5 as StdCombo with uid="HMJCXTNLYY",rtseq=4,rtrep=.f.,left=213,top=66,width=128,height=21;
    , ToolTipText = "Tipo del campo";
    , HelpContextID = 20654013;
    , cFormVar="w_PRTIPCAM",RowSource=""+"Carattere,"+"Data,"+"Espressione,"+"Logico,"+"Memo,"+"Numerico", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPCAM_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    iif(this.value =4,'L',;
    iif(this.value =5,'M',;
    iif(this.value =6,'N',;
    space(1))))))))
  endfunc
  func oPRTIPCAM_1_5.GetRadio()
    this.Parent.oContained.w_PRTIPCAM = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPCAM_1_5.SetRadio()
    this.Parent.oContained.w_PRTIPCAM=trim(this.Parent.oContained.w_PRTIPCAM)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPCAM=='C',1,;
      iif(this.Parent.oContained.w_PRTIPCAM=='D',2,;
      iif(this.Parent.oContained.w_PRTIPCAM=='E',3,;
      iif(this.Parent.oContained.w_PRTIPCAM=='L',4,;
      iif(this.Parent.oContained.w_PRTIPCAM=='M',5,;
      iif(this.Parent.oContained.w_PRTIPCAM=='N',6,;
      0))))))
  endfunc

  add object oPRLUNCAM_1_6 as StdField with uid="UMLHQCPFSV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PRLUNCAM", cQueryName = "PRLUNCAM",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza del campo. Per i valori numerici comprende i decimali.",;
    HelpContextID = 21997501,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=426, Top=66


  add object oPRTIPVAL_1_7 as StdCombo with uid="ELJFRWXSSP",rtseq=6,rtrep=.f.,left=214,top=94,width=128,height=21;
    , ToolTipText = "Valore da assegnare al campo se non � stato valorizzato";
    , HelpContextID = 29677634;
    , cFormVar="w_PRTIPVAL",RowSource=""+"Predefinito,"+"Nessun valore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPVAL_1_7.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPRTIPVAL_1_7.GetRadio()
    this.Parent.oContained.w_PRTIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPVAL_1_7.SetRadio()
    this.Parent.oContained.w_PRTIPVAL=trim(this.Parent.oContained.w_PRTIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPVAL=='P',1,;
      iif(this.Parent.oContained.w_PRTIPVAL=='N',2,;
      0))
  endfunc

  add object oPRVALPRE_1_10 as StdMemo with uid="ALISMVMZLO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PRVALPRE", cQueryName = "PRVALPRE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 192739387,;
   bGlobalFont=.t.,;
    Height=81, Width=434, Left=213, Top=118


  add object oLinkPC_1_12 as stdDynamicChildContainer with uid="GLVBSSHDKW",left=8, top=230, width=649, height=179, bOnScreen=.t.;


  add object oARTABELL_1_21 as StdField with uid="GZILFMLAZG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ARTABELL", cQueryName = "ARTABELL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 2304174,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=440, Top=15, InputMask=replicate('X',15)

  func oARTABELL_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PRNOMCAM)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFLCOMMEN_1_22 as StdField with uid="KMUPFYGRDS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FLCOMMEN", cQueryName = "FLCOMMEN",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 144294308,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=352, Top=39, cSayPict='repl("X",80)', cGetPict='repl("X",80)', InputMask=replicate('X',80)

  add object oStr_1_3 as StdString with uid="STKRTVZWRA",Visible=.t., Left=12, Top=16,;
    Alignment=1, Width=193, Height=18,;
    Caption="Archivio di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="WRVSKBXZTV",Visible=.t., Left=12, Top=42,;
    Alignment=1, Width=193, Height=18,;
    Caption="Campo di destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="JIFWLJCHEJ",Visible=.t., Left=12, Top=95,;
    Alignment=1, Width=193, Height=15,;
    Caption="Criterio di assegnazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RTONJNICKA",Visible=.t., Left=12, Top=141,;
    Alignment=1, Width=193, Height=15,;
    Caption="Valore predefinito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="SBXKPGIQIZ",Visible=.t., Left=18, Top=214,;
    Alignment=0, Width=144, Height=15,;
    Caption="Importazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BOUMKYGKGN",Visible=.t., Left=308, Top=214,;
    Alignment=0, Width=304, Height=15,;
    Caption="Valore predefinito"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ZSMOPGYHKD",Visible=.t., Left=192, Top=214,;
    Alignment=0, Width=86, Height=15,;
    Caption="Criterio"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LBDNXZTXMY",Visible=.t., Left=12, Top=69,;
    Alignment=1, Width=193, Height=15,;
    Caption="Tipo del valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="DLRSRVGRNX",Visible=.t., Left=347, Top=69,;
    Alignment=1, Width=75, Height=15,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="ALOJLGLFTG",left=13, top=211, width=646,height=20

  add object oBox_1_16 as StdBox with uid="BSIMRGEJTF",left=305, top=213, width=0,height=16

  add object oBox_1_18 as StdBox with uid="EWKUBYLVFO",left=188, top=213, width=0,height=16
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_apr','PREDEFIN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODFIL=PREDEFIN.PRCODFIL";
  +" and "+i_cAliasName2+".PRNOMCAM=PREDEFIN.PRNOMCAM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
