* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bpn                                                        *
*              Import primanota                                                *
*                                                                              *
*      Author: TAM SOFTWARE & CODE LAB                                         *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_287]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2018-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bpn",oParentObject)
return(i_retval)

define class tgsim_bpn as StdBatch
  * --- Local variables
  w_PNSERIAL = space(10)
  w_FLPPRO = space(2)
  w_FLPDOC = space(2)
  w_SCCODICE = space(10)
  w_CMCODICE = space(10)
  w_CMNUMREG = 0
  w_CMCOMPET = space(4)
  w_SimVal = space(5)
  w_CaoVal = 0
  w_DecTot = 0
  w_FLRIFE = space(2)
  w_ANTIPSOT = space(1)
  w_IVPERIND = 0
  w_IVPERIVA = 0
  COR_CLFO = space(15)
  w_RIFERIME = 0
  w_CASO = space(1)
  w_TTSERIAL = space(10)
  w_TT_SEGNO = space(10)
  w_TTTOTIMP = 0
  w_TTTIPCON = space(1)
  w_TTCODCON = space(15)
  w_FLANAL = space(1)
  w_CONSUP = space(15)
  w_SEZBIL = space(1)
  w_NOPNT = .f.
  w_XXBANAPP = space(10)
  w_XXBANNOS = space(15)
  w_CCTAGG = space(1)
  w_TTCODCOI = space(15)
  w_TTFLOMAG = space(1)
  w_TTCFLOMA = space(1)
  w_TIPPAG = space(2)
  w_ARRSCA = space(10)
  * --- WorkFile variables
  CAN_TIER_idx=0
  CAU_CONT_idx=0
  CDC_MANU_idx=0
  CENCOST_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  ESERCIZI_idx=0
  MOVICOST_idx=0
  PAG_2AME_idx=0
  PAR_TITE_idx=0
  PNT_DETT_idx=0
  PNT_IVA_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  SCA_VARI_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  VOC_COST_idx=0
  MASTRI_idx=0
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT PRIMANOTA ED ARCHIVI COLLEGATI
    * --- Modello di segnalazione
    * --- Segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Aggiornamento righe IVA S/N
    * --- Aggiornamento partite S/N
    * --- Aggiornamento analitica S/N
    * --- Aggiornamento saldi S/N
    * --- Ricalcolo numeri registrazioni
    * --- Conto di costo per IVA indetraibile
    * --- File ascii da importare
    * --- *** Variabili per gestione rottura registrazioni ***
    * --- Chiave corrente movimentazione
    * --- Ultima chiave di rottura movimentazione
    * --- Ultimo serial
    * --- Ultimo numero registrazione
    * --- Ultimo numero protocollo
    * --- Ultimo tipo cliente/fornitore
    * --- Ultimo cliente/fornitore
    * --- Ultimo numero di riga
    * --- Ultimo numero di riga analitica da primanota
    * --- Messaggio a video - Elaborazione archivio ...
    * --- Nome archivio
    * --- Numero record per il messaggio di errore sul campo obbligatorio
    * --- Serial primanota
    this.w_PNSERIAL = space(10)
    * --- Flag per calcolo protocollo
    this.w_FLPPRO = "  "
    * --- Flag per calcolo numerazione documenti
    this.w_FLPDOC = "  "
    * --- Serial partite/scadenze diverse
    this.w_SCCODICE = space(10)
    * --- Serial movimenti analitica
    this.w_CMCODICE = space(10)
    * --- Numero registraz. mov. analitica
    this.w_CMNUMREG = 0
    * --- Esercizio
    this.w_CMCOMPET = space(4)
    * --- Variabili per gestione valute
    this.w_SimVal = 0
    * --- Simbolo valuta
    this.w_CaoVal = 0
    * --- Cambio valuta
    this.w_DecTot = 0
    * --- Cambio valuta
    * --- Variabili locali
    this.w_FLRIFE = "  "
    * --- Riferimento tipo cliente/fornitore
    this.w_ANTIPSOT = " "
    * --- Tipo sottoconto
    this.w_IVPERIND = 0
    * --- Percentuale indetraibilit� IVA
    this.w_IVPERIVA = 0
    * --- Percentuale IVA
    this.COR_CLFO = space(15)
    * --- Corrente cliente o fornitore del master
    this.w_RIFERIME = 0
    * --- Numero riga di riferimento
    * --- Aggiornamento archivi
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    * --- Il primo cliente/fornitore deve essere scritto in testata. Memorizza il codice e la prima volta che cambia aggiorna il master.
    this.COR_CLFO = this.oParentObject.ULT_CLFO
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_04911A18
        bErr_04911A18=bTrsErr
        this.Try_04911A18()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04911A18
        * --- End
        * --- Aggiornamento records
        w_PNAGG_01 = iif( TYPE("w_PNAGG_01")="U", "", w_PNAGG_01 )
        w_PNAGG_02 = iif( TYPE("w_PNAGG_02")="U", "", w_PNAGG_02 )
        w_PNAGG_03 = iif( TYPE("w_PNAGG_03")="U", "", w_PNAGG_03 )
        w_PNAGG_04 = iif( TYPE("w_PNAGG_04")="U", "", w_PNAGG_04 )
        w_PNAGG_05 = iif( TYPE("w_PNAGG_05")="U",cp_CharToDate("   -  -    "), w_PNAGG_05 )
        w_PNAGG_06 = iif( TYPE("w_PNAGG_06")="U",cp_CharToDate("   -  -    "), w_PNAGG_06 )
        i_rows = 0
        * --- Try
        local bErr_04914CE8
        bErr_04914CE8=bTrsErr
        this.Try_04914CE8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_04914CE8
        * --- End
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
    endcase
  endproc
  proc Try_04911A18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "CE"
        * --- Centri di costo/Ricavo
        * --- Insert into CENCOST
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CENCOST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CC_CONTO"+",CCDESPIA"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CC_CONTO),'CENCOST','CC_CONTO');
          +","+cp_NullLink(cp_ToStrODBC(w_CCDESPIA),'CENCOST','CCDESPIA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CENCOST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CENCOST','UTDC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CC_CONTO',w_CC_CONTO,'CCDESPIA',w_CCDESPIA,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
          insert into (i_cTable) (CC_CONTO,CCDESPIA,UTCC,UTDC &i_ccchkf. );
             values (;
               w_CC_CONTO;
               ,w_CCDESPIA;
               ,this.oParentObject.w_CreVarUte;
               ,this.oParentObject.w_CreVarDat;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "VC"
        * --- Voci di Costo/Ricavo
        * --- Insert into VOC_COST
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOC_COST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"VCCODICE"+",UTDC"+",UTCC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_VCCODICE),'VOC_COST','VCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'VOC_COST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'VOC_COST','UTCC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'VCCODICE',w_VCCODICE,'UTDC',this.oParentObject.w_CreVarDat,'UTCC',this.oParentObject.w_CreVarUte)
          insert into (i_cTable) (VCCODICE,UTDC,UTCC &i_ccchkf. );
             values (;
               w_VCCODICE;
               ,this.oParentObject.w_CreVarDat;
               ,this.oParentObject.w_CreVarUte;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "CO"
        * --- Commesse
        * --- Insert into CAN_TIER
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAN_TIER_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CNCODCAN"+",UTDC"+",UTCC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CNCODCAN),'CAN_TIER','CNCODCAN');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'CAN_TIER','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'CAN_TIER','UTCC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CNCODCAN',w_CNCODCAN,'UTDC',this.oParentObject.w_CreVarDat,'UTCC',this.oParentObject.w_CreVarUte)
          insert into (i_cTable) (CNCODCAN,UTDC,UTCC &i_ccchkf. );
             values (;
               w_CNCODCAN;
               ,this.oParentObject.w_CreVarDat;
               ,this.oParentObject.w_CreVarUte;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "PT"
        * --- Partite
        w_PTTIPCON = iif( empty(w_PTTIPCON) , "G", w_PTTIPCON)
        w_EXCODVAL = iif( empty(w_EXCODVAL) , g_PERVAL , w_EXCODVAL )
        w_PTCAOVAL = iif( empty(w_PTCAOVAL) , GETCAM(w_PTCODVAL, w_EXDATDOC, 7) , w_PTCAOVAL )
        if empty(w_PTNUMPAR)
          TmpCodEse = iif( empty(w_EXDATDOC) , g_CODESE, str(year(w_EXDATDOC),4,0) )
          w_PTNUMPAR = CANUMPAR("N", TmpCodEse, w_PTNUMDOC, w_PTALFDOC)
        endif
        * --- In funzione informazione definisce casistica
        *     P= Partita associata a primanota
        *     S= Partita associata a scadenza diversa
        *     A= Partita di tipo scadenza diversa associata ad una partita precedente
        this.w_CASO = iif(empty(w_PTSERIAL),"S","P")
        * --- Nel tracciato � presente la funzione Looktab per la lettura del seriale in funzione di data e numero di registrazione
        if this.w_CASO="S"
          * --- E' una scadenza diversa. Se esiste una partita con lo stesso numero partita l'associa.
          *     Attenzione: legge PTROWORD con il valore di -1 per assegnarlo a w_RIFERIME.
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTSERIAL,PTROWORD"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTNUMPAR = "+cp_ToStrODBC(w_PTNUMPAR);
                  +" and PTTIPCON = "+cp_ToStrODBC(w_PTTIPCON);
                  +" and PTCODCON = "+cp_ToStrODBC(w_PTCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTSERIAL,PTROWORD;
              from (i_cTable) where;
                  PTNUMPAR = w_PTNUMPAR;
                  and PTTIPCON = w_PTTIPCON;
                  and PTCODCON = w_PTCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PTSERIAL = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
            w_PTROWORD = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !empty(w_PTSERIAL)
            this.w_CASO = "A"
            this.w_SCCODICE = w_PTSERIAL
            * --- Determina il numero di riga massimo
            this.oParentObject.ULT_RNUM = 1
            this.w_RIFERIME = -1
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWMAX  from "+i_cTable+" PAR_TITE ";
                  +" where PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE)+" and PTROWORD="+cp_ToStrODBC(this.w_RIFERIME)+"";
                   ,"_Curs_PAR_TITE")
            else
              select MAX(CPROWNUM) AS CPROWMAX from (i_cTable);
               where PTSERIAL = this.w_SCCODICE and PTROWORD=this.w_RIFERIME;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              this.oParentObject.ULT_RNUM = _Curs_PAR_TITE.CPROWMAX
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
            * --- Impostazioni per caso A
            this.oParentObject.ULT_RNUM = NVL(this.oParentObject.ULT_RNUM,0) + 1
          endif
        endif
        if this.w_CASO="S"
          * --- Calcolo Serial
          this.w_SCCODICE = space(10)
          i_Conn=i_TableProp[this.SCA_VARI_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SESCA", "i_codazi,w_SCCODICE")
          ah_msg( "Elaborazione %1 - record %2 di %3" , .t. , , , trim(this.oParentObject.w_Archivio) , alltrim(str(this.oParentObject.NumReco,10)) , alltrim(str(this.oParentObject.NumTotReco,10)) )
          this.w_RIFERIME = -1
          * --- Inserimento record master per partite su scadenze diverse
          w_SCTIPSCA = iif( type("w_SCTIPSCA")="U" or empty(w_SCTIPSCA ) , iif( w_PTTIPCON="F", "F", "C") , w_SCTIPSCA)
          w_SCFLIMPG = IIF(type("w_SCFLIMPG")="U" or EMPTY(w_SCFLIMPG), "G", w_SCFLIMPG)
          w_SCCODPAG = IIF(type("w_SCCODPAG")="U", "", w_SCCODPAG)
          w_SCDATREG=IIF(type("w_SCDATREG")="U" OR EMPTY(w_SCDATREG), w_PTDATDOC, w_SCDATREG)
          w_SCCODAGE=IIF(type("w_SCCODAGE")="U" OR EMPTY(w_SCCODAGE), Space(5), w_SCCODAGE)
          w_PTDATREG=IIF(type("w_PTDATREG")="U" OR EMPTY(w_PTDATREG), w_SCDATREG, w_PTDATREG)
          * --- Insert into SCA_VARI
          i_nConn=i_TableProp[this.SCA_VARI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SCA_VARI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"SCCODICE"+",SCCODSEC"+",SCTIPSCA"+",SCTIPCLF"+",SCCODCLF"+",SCNUMDOC"+",SCALFDOC"+",SCDATDOC"+",SCVALNAZ"+",SCCODVAL"+",SCCAOVAL"+",SCIMPSCA"+",SCFLIMPG"+",SCCODPAG"+",SCCODBAN"+",SCDATVAL"+",SCDESCRI"+",SCBANNOS"+",SCRIFDCO"+",SCCODAGE"+",SCDATREG"+",SCFLVABD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'SCA_VARI','SCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFERIME),'SCA_VARI','SCCODSEC');
            +","+cp_NullLink(cp_ToStrODBC(w_SCTIPSCA),'SCA_VARI','SCTIPSCA');
            +","+cp_NullLink(cp_ToStrODBC(w_PTTIPCON),'SCA_VARI','SCTIPCLF');
            +","+cp_NullLink(cp_ToStrODBC(w_PTCODCON),'SCA_VARI','SCCODCLF');
            +","+cp_NullLink(cp_ToStrODBC(w_PTNUMDOC),'SCA_VARI','SCNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_PTALFDOC),'SCA_VARI','SCALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_PTDATDOC),'SCA_VARI','SCDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_EXCODVAL),'SCA_VARI','SCVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(w_PTCODVAL),'SCA_VARI','SCCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(w_PTCAOVAL),'SCA_VARI','SCCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(w_PTIMPDOC ),'SCA_VARI','SCIMPSCA');
            +","+cp_NullLink(cp_ToStrODBC(w_SCFLIMPG),'SCA_VARI','SCFLIMPG');
            +","+cp_NullLink(cp_ToStrODBC(w_SCCODPAG),'SCA_VARI','SCCODPAG');
            +","+cp_NullLink(cp_ToStrODBC(w_PTBANAPP),'SCA_VARI','SCCODBAN');
            +","+cp_NullLink(cp_ToStrODBC(w_PTDATDOC),'SCA_VARI','SCDATVAL');
            +","+cp_NullLink(cp_ToStrODBC(w_SCDESCRI),'SCA_VARI','SCDESCRI');
            +","+cp_NullLink(cp_ToStrODBC(w_PTBANNOS),'SCA_VARI','SCBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(w_SCRIFDCO),'SCA_VARI','SCRIFDCO');
            +","+cp_NullLink(cp_ToStrODBC(w_SCCODAGE),'SCA_VARI','SCCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(w_SCDATREG),'SCA_VARI','SCDATREG');
            +","+cp_NullLink(cp_ToStrODBC(w_SCFLVABD),'SCA_VARI','SCFLVABD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_SCCODICE,'SCCODSEC',this.w_RIFERIME,'SCTIPSCA',w_SCTIPSCA,'SCTIPCLF',w_PTTIPCON,'SCCODCLF',w_PTCODCON,'SCNUMDOC',w_PTNUMDOC,'SCALFDOC',w_PTALFDOC,'SCDATDOC',w_PTDATDOC,'SCVALNAZ',w_EXCODVAL,'SCCODVAL',w_PTCODVAL,'SCCAOVAL',w_PTCAOVAL,'SCIMPSCA',w_PTIMPDOC )
            insert into (i_cTable) (SCCODICE,SCCODSEC,SCTIPSCA,SCTIPCLF,SCCODCLF,SCNUMDOC,SCALFDOC,SCDATDOC,SCVALNAZ,SCCODVAL,SCCAOVAL,SCIMPSCA,SCFLIMPG,SCCODPAG,SCCODBAN,SCDATVAL,SCDESCRI,SCBANNOS,SCRIFDCO,SCCODAGE,SCDATREG,SCFLVABD &i_ccchkf. );
               values (;
                 this.w_SCCODICE;
                 ,this.w_RIFERIME;
                 ,w_SCTIPSCA;
                 ,w_PTTIPCON;
                 ,w_PTCODCON;
                 ,w_PTNUMDOC;
                 ,w_PTALFDOC;
                 ,w_PTDATDOC;
                 ,w_EXCODVAL;
                 ,w_PTCODVAL;
                 ,w_PTCAOVAL;
                 ,w_PTIMPDOC ;
                 ,w_SCFLIMPG;
                 ,w_SCCODPAG;
                 ,w_PTBANAPP;
                 ,w_PTDATDOC;
                 ,w_SCDESCRI;
                 ,w_PTBANNOS;
                 ,w_SCRIFDCO;
                 ,w_SCCODAGE;
                 ,w_SCDATREG;
                 ,w_SCFLVABD;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.oParentObject.ULT_RNUM = 1
        endif
        if this.w_CASO = "P"
          this.w_SCCODICE = w_PTSERIAL
          * --- Determina il numero di riga massimo
          this.oParentObject.ULT_RNUM = 1
          this.w_RIFERIME = w_PTROWORD
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWMAX  from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE)+" and PTROWORD="+cp_ToStrODBC(this.w_RIFERIME)+"";
                 ,"_Curs_PAR_TITE")
          else
            select MAX(CPROWNUM) AS CPROWMAX from (i_cTable);
             where PTSERIAL = this.w_SCCODICE and PTROWORD=this.w_RIFERIME;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            this.oParentObject.ULT_RNUM = _Curs_PAR_TITE.CPROWMAX
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
          if type("w_PTDATREG")="U" OR EMPTY(w_PTDATREG)
            * --- Se data reistrazione non passata o vuota applico data registrazione della PN collegata
             
 w_PTDATREG=Cp_chartodate("  -  -    ")
            * --- Read from PNT_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PNDATREG"+;
                " from "+i_cTable+" PNT_MAST where ";
                    +"PNSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PNDATREG;
                from (i_cTable) where;
                    PNSERIAL = this.w_SCCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_PTDATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if NOT EMPTY(w_EXKEYINC)
            * --- Il campo PTKEYINC viene valorizzato nel batch GSIM_BTE in modo tale che una stessa registrazione
            *     ha valorizzato questo campo vuoto o pieno per tutte le partite associate alla registrazione.
            *     La scrittura marca le Partite per le quali � gi� stato eseguito uno storno in adhoc Window 5.0.
            * --- Write into PNT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLGDIF');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
                     )
            else
              update (i_cTable) set;
                  PNFLGDIF = "S";
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_SCCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Impostazioni per caso P
          this.oParentObject.ULT_RNUM = NVL(this.oParentObject.ULT_RNUM,0) + 1
        endif
        * --- Inserimento record detail
        w_CPROWORD = this.oParentObject.ULT_RNUM * 10
        if type("w_EXIMPAVE")="N"
          w_PT_SEGNO = IIF(w_EXIMPAVE>0 OR w_EXIMPDAR<0 , "A", "D")
          this.w_TT_SEGNO = IIF(w_EXIMPAVE<>0 , "A", "D")
        else
          this.w_TT_SEGNO = w_PT_SEGNO
        endif
        if this.w_RIFERIME = 0
          * --- Determina il numero di riga in base all'importo della partita.
          *     Codice eseguito solo per le partite in quanto le scadenze hanno il valore w_RIFERIME=-1
          this.w_TTSERIAL = this.w_SCCODICE
          this.w_TTTOTIMP = w_PTTOTIMP
          this.w_TTTIPCON = w_PTTIPCON
          this.w_TTCODCON = w_PTCODCON
          this.w_RIFERIME = 1
          * --- Select from gsim_bpn
          local _hEWFOLEPMIW
          _hEWFOLEPMIW=createobject('prm_container')
          addproperty(_hEWFOLEPMIW,'pPNSERIAL',this.w_TTSERIAL)
          addproperty(_hEWFOLEPMIW,'pPNIMPDAR',this.w_TTTOTIMP)
          addproperty(_hEWFOLEPMIW,'pPNIMPAVE',this.w_TTTOTIMP)
          addproperty(_hEWFOLEPMIW,'pSEGNO',this.w_TT_SEGNO)
          addproperty(_hEWFOLEPMIW,'pPNTIPCON',this.w_TTTIPCON)
          addproperty(_hEWFOLEPMIW,'pPNCODCON',this.w_TTCODCON)
          do vq_exec with 'gsim_bpn',_hEWFOLEPMIW,'_Curs_gsim_bpn','',.f.,.t.
          if used('_Curs_gsim_bpn')
            select _Curs_gsim_bpn
            locate for 1=1
            do while not(eof())
            this.w_RIFERIME = _Curs_gsim_bpn.CPROWNUM
              select _Curs_gsim_bpn
              continue
            enddo
            use
          endif
        endif
        * --- Insert into PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'PAR_TITE','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFERIME),'PAR_TITE','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PAR_TITE','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_SCCODICE,'PTROWORD',this.w_RIFERIME,'CPROWNUM',this.oParentObject.ULT_RNUM)
          insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM &i_ccchkf. );
             values (;
               this.w_SCCODICE;
               ,this.w_RIFERIME;
               ,this.oParentObject.ULT_RNUM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Memorizza la nuova chiave di rottura
        this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
        this.oParentObject.ULT_SERI = this.w_SCCODICE
      case this.oParentObject.w_Destinaz = "MC"
        * --- Movimenti Analitica
        * --- ??? DA CONTROLLARE ???
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    return
  proc Try_04914CE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "PN"
        * --- Primanota
        * --- Lettura dati causale
        w_FLSALI = " "
        w_FLSALF = " "
        w_CALDOC = " "
        w_PNCODCAU = left(alltrim(w_PNCODCAU)+space(5),5)
        if w_EXTIPORI<>"W" AND EMPTY(w_PNCODCAU)
          * --- Segnala se manca il codice conto
          this.oParentObject.w_ResoMode = "NOCAUCON"
          this.oParentObject.Pag4()
        endif
        * --- Read from CAU_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCFLIVDF,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPDOC,CCFLRIFE,CCFLSALI,CCFLSALF,CCCALDOC,CCFLANAL"+;
            " from "+i_cTable+" CAU_CONT where ";
                +"CCCODICE = "+cp_ToStrODBC(w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCFLIVDF,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLPPRO,CCFLPDOC,CCFLRIFE,CCFLSALI,CCFLSALF,CCCALDOC,CCFLANAL;
            from (i_cTable) where;
                CCCODICE = w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_PNFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
          w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
          w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
          w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
          this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
          this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
          this.w_FLRIFE = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
          w_FLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
          w_FLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
          w_CALDOC = NVL(cp_ToDate(_read_.CCCALDOC),cp_NullValue(_read_.CCCALDOC))
          this.w_FLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          * --- Nuova Registrazione
          w_PNDATREG = iif( empty(w_PNDATREG), i_datsys, w_PNDATREG )
          w_PNCOMIVA = iif( empty(w_PNCOMIVA), w_PNDATREG, w_PNCOMIVA )
          w_PNCODUTE = iif(g_UNIUTE$"UE", iif(empty(w_PNCODUTE), i_codute, w_PNCODUTE ),0)
          w_PNCODESE = iif( empty(w_PNCODESE), g_CodEse, w_PNCODESE )
          w_PNCOMPET = iif( empty(w_PNCOMPET), w_PNCODESE, w_PNCOMPET )
          * --- Controllo Data Consolidamento
          if Not Empty(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),w_PNDATREG,"B","N"))
            this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Attenzione, registrazione numero %1 del %2 con %3" , alltrim(str(w_PNNUMRER,6,0)) , dtoc(w_PNDATREG) , SUBSTR(CHKCONS(IIF(this.w_FLANAL="S","PC","P"),w_PNDATREG,"B","N"),12) )
            * --- Raise
            i_Error="Data Registrazione antecedente data Consolidamento!"
            return
          endif
          this.w_PNSERIAL = space(10)
          i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
          * --- Chiave per progressivo
          w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(w_PNDATREG), SPACE(8))
          w_PNNUMRER = iif(w_EXNUMREG>0, w_EXNUMREG, w_PNNUMRER)
          w_PNNUMRER = iif(this.oParentObject.w_IMAGGNUR="S",0,w_PNNUMRER)
          * --- Aggiorna progressivo Numero Registrazione
          w_PNNUMRER = gsim_bpr(this,"PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER",w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Chiave per progressivo
          w_PNPRP = iif( empty(w_PNPRP) , iif( w_PNTIPREG="A", "AC", "NN" ) , w_PNPRP )
          * --- Se il protocollo non viene specificato sul file ascii/odbc lo calcola
          if empty( w_PNNUMPRO )
            w_PNANNPRO = CALPRO(w_PNDATREG, w_PNCOMPET, this.w_FLPPRO)
          endif
          if .not. empty(w_PNANNPRO)
            w_PNNUMPRO = gsim_bpr(this,"PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO",w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            w_PNANNPRO = CALPRO(w_PNDATREG, w_PNCOMPET, this.w_FLPPRO)
          endif
          * --- Chiave per progressivo
          w_PNPRD = IIF(w_PNTIPREG="V", "FV", "NN")
          * --- Se il numero di documento non viene specificato sul file ascii/odbc lo calcola
          if empty( w_PNNUMDOC )
            w_PNANNDOC = CALPRO(IIF(EMPTY(w_PNDATDOC), w_PNDATREG, w_PNDATDOC), w_PNCOMPET, this.w_FLPDOC)
          endif
          if .not. empty(w_PNANNDOC)
            w_PNNUMDOC = gsim_bpr(this,"PRDOC", "i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC",w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            w_PNANNDOC = CALPRO(IIF(EMPTY(w_PNDATDOC), w_PNDATREG, w_PNDATDOC), w_PNCOMPET, this.w_FLPDOC)
          endif
          if empty( w_PNVALNAZ )
            * --- Lettura dati esercizio
            * --- Read from ESERCIZI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ESERCIZI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ESVALNAZ"+;
                " from "+i_cTable+" ESERCIZI where ";
                    +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                    +" and ESCODESE = "+cp_ToStrODBC(w_PNCOMPET);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ESVALNAZ;
                from (i_cTable) where;
                    ESCODAZI = i_CODAZI;
                    and ESCODESE = w_PNCOMPET;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_PNVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if empty(w_PNFLPROV)
            * --- Flag provvisorio o definitiva
            w_PNFLPROV = "N"
          endif
          if empty(w_PNCODVAL)
            * --- Codice Valuta
            w_PNCODVAL = g_PERVAL
          else
            w_PNCODVAL = left(w_PNCODVAL+ space(3), 3)
          endif
          if empty(w_PNCAOVAL)
            * --- Cambio Valuta
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VACAOVAL"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(w_PNCODVAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VACAOVAL;
                from (i_cTable) where;
                    VACODVAL = w_PNCODVAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              w_PNCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if empty(w_PNTIPCLF)
            * --- Tipo Cliente Fornitore
            w_PNTIPCLF = IIF(w_PNTIPREG="V" OR (w_PNTIPREG$"CE" AND w_PNTIPDOC="FC"), "C", IIF(w_PNTIPREG="A", "F", this.w_FLRIFE))
          endif
          * --- Controlli
          if w_PNTIPCLF$"CF" AND w_PNCODVAL<>w_PNVALNAZ AND w_PNFLREGI<>"S" AND empty(w_PNTOTDOC)
            * --- Segnala se il totale documento � vuoto, la registrazione � in valuta e non � stata ancora stampata sui registri iva
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "S"
            this.oParentObject.w_ResoMess = ah_msgformat( "Manca il totale documento in valuta nella registrazione numero %1 del %2 (seriale %3).", ltrim(str(w_PNNUMRER,10,0)) , dtoc(w_PNDATREG) , this.w_PNSERIAL )
            this.oParentObject.Pag4()
          endif
          * --- Calcoli
          * --- Il totale documento deve essere valorizzato solo per i documenti in valuta
          w_PNTOTDOC = iif( w_PNTIPCLF$"CF" , w_PNTOTDOC, 0 )
          if TYPE("w_PN__ANNO")="U" OR w_PN__ANNO=0
            if w_PNTIPREG<>"N" AND w_PNTIPDOC $ "FA-NC-FC-FE-NE" AND w_PNTIPCLF $ "C-F"
               
 w_PN__ANNO = YEAR(w_PNDATDOC)
            endif
          endif
          if TYPE("w_PN__MESE")="U" OR w_PN__MESE=0
            if w_PNTIPREG<>"N" AND w_PNTIPDOC $ "FA-NC-FC-FE-NE" AND w_PNTIPCLF $ "C-F"
               
 w_PN__MESE = MONTH(w_PNDATDOC)
            endif
          endif
          * --- Salva dati progressivi
          this.oParentObject.ULT_SERI = this.w_PNSERIAL
          this.oParentObject.ULT_NREG = w_PNNUMRER
          this.oParentObject.ULT_PROT = w_PNNUMPRO
          this.oParentObject.ULT_TCLF = w_PNTIPCLF
          this.oParentObject.ULT_CLFO = iif(this.oParentObject.ULT_TCLF="N","",Left(iif(!empty(w_EXCLIFOR),w_EXCLIFOR,w_PNCODCLF)+space(15),15))
          this.oParentObject.ULT_RNUM = 1
          this.oParentObject.ULT_RNUMAN = 1
          * --- Scrittura Master Primanota
          ah_msg( "Elaborazione %1 - record %2 di %3" , .t. , , , trim(this.oParentObject.w_Archivio) , alltrim(str(this.oParentObject.NumReco,10)) , alltrim(str(this.oParentObject.NumTotReco,10)) )
          this.oParentObject.w_ResoDett = ah_msgformat("Seriale %1 numero registrazione %2 del %3 riga %4" , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , dtoc(w_PNDATREG) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
          * --- Insert into PNT_MAST
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNNUMDOC"+",PNALFDOC"+",PNDATDOC"+",PNANNDOC"+",UTCC"+",UTDC"+",PNANNPRO"+",PNALFPRO"+",PNNUMPRO"+",PNPRP"+",PNPRD"+",PNPRG"+",PN__ANNO"+",PN__MESE"+",PNAGG_01"+",PNAGG_02"+",PNAGG_03"+",PNAGG_04"+",PNAGG_05"+",PNAGG_06"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(w_PNCODESE),'PNT_MAST','PNCODESE');
            +","+cp_NullLink(cp_ToStrODBC(w_PNCODUTE),'PNT_MAST','PNCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(w_PNNUMRER),'PNT_MAST','PNNUMRER');
            +","+cp_NullLink(cp_ToStrODBC(w_PNDATREG),'PNT_MAST','PNDATREG');
            +","+cp_NullLink(cp_ToStrODBC(w_PNCODCAU),'PNT_MAST','PNCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(w_PNCOMPET),'PNT_MAST','PNCOMPET');
            +","+cp_NullLink(cp_ToStrODBC(w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_PNALFDOC),'PNT_MAST','PNALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_PNDATDOC),'PNT_MAST','PNDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(w_PNANNDOC),'PNT_MAST','PNANNDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'PNT_MAST','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'PNT_MAST','UTDC');
            +","+cp_NullLink(cp_ToStrODBC(w_PNANNPRO),'PNT_MAST','PNANNPRO');
            +","+cp_NullLink(cp_ToStrODBC(w_PNALFPRO),'PNT_MAST','PNALFPRO');
            +","+cp_NullLink(cp_ToStrODBC(w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
            +","+cp_NullLink(cp_ToStrODBC(w_PNPRP),'PNT_MAST','PNPRP');
            +","+cp_NullLink(cp_ToStrODBC(w_PNPRD),'PNT_MAST','PNPRD');
            +","+cp_NullLink(cp_ToStrODBC(w_PNPRG),'PNT_MAST','PNPRG');
            +","+cp_NullLink(cp_ToStrODBC(w_PN__ANNO),'PNT_MAST','PN__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(w_PN__MESE),'PNT_MAST','PN__MESE');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_01),'PNT_MAST','PNAGG_01');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_02),'PNT_MAST','PNAGG_02');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_03),'PNT_MAST','PNAGG_03');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_04),'PNT_MAST','PNAGG_04');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_05),'PNT_MAST','PNAGG_05');
            +","+cp_NullLink(cp_ToStrODBC(w_PNAGG_06),'PNT_MAST','PNAGG_06');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',w_PNCODESE,'PNCODUTE',w_PNCODUTE,'PNNUMRER',w_PNNUMRER,'PNDATREG',w_PNDATREG,'PNCODCAU',w_PNCODCAU,'PNCOMPET',w_PNCOMPET,'PNNUMDOC',w_PNNUMDOC,'PNALFDOC',w_PNALFDOC,'PNDATDOC',w_PNDATDOC,'PNANNDOC',w_PNANNDOC,'UTCC',this.oParentObject.w_CreVarUte)
            insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNNUMDOC,PNALFDOC,PNDATDOC,PNANNDOC,UTCC,UTDC,PNANNPRO,PNALFPRO,PNNUMPRO,PNPRP,PNPRD,PNPRG,PN__ANNO,PN__MESE,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06 &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,w_PNCODESE;
                 ,w_PNCODUTE;
                 ,w_PNNUMRER;
                 ,w_PNDATREG;
                 ,w_PNCODCAU;
                 ,w_PNCOMPET;
                 ,w_PNNUMDOC;
                 ,w_PNALFDOC;
                 ,w_PNDATDOC;
                 ,w_PNANNDOC;
                 ,this.oParentObject.w_CreVarUte;
                 ,this.oParentObject.w_CreVarDat;
                 ,w_PNANNPRO;
                 ,w_PNALFPRO;
                 ,w_PNNUMPRO;
                 ,w_PNPRP;
                 ,w_PNPRD;
                 ,w_PNPRG;
                 ,w_PN__ANNO;
                 ,w_PN__MESE;
                 ,w_PNAGG_01;
                 ,w_PNAGG_02;
                 ,w_PNAGG_03;
                 ,w_PNAGG_04;
                 ,w_PNAGG_05;
                 ,w_PNAGG_06;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Write into PNT_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PNTIPREG ="+cp_NullLink(cp_ToStrODBC(w_PNTIPREG),'PNT_MAST','PNTIPREG');
            +",PNFLIVDF ="+cp_NullLink(cp_ToStrODBC(w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
            +",PNFLGDIF ="+cp_NullLink(cp_ToStrODBC(w_PNFLGDIF),'PNT_MAST','PNFLGDIF');
            +",PNNUMREG ="+cp_NullLink(cp_ToStrODBC(w_PNNUMREG),'PNT_MAST','PNNUMREG');
            +",PNTIPDOC ="+cp_NullLink(cp_ToStrODBC(w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
            +",PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
            +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(w_PNDESSUP),'PNT_MAST','PNDESSUP');
            +",PNTIPCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_TCLF),'PNT_MAST','PNTIPCLF');
            +",PNCODCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_CLFO),'PNT_MAST','PNCODCLF');
            +",PNTOTDOC ="+cp_NullLink(cp_ToStrODBC(w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
            +",PNCOMIVA ="+cp_NullLink(cp_ToStrODBC(w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
            +",PNFLREGI ="+cp_NullLink(cp_ToStrODBC(w_PNFLREGI),'PNT_MAST','PNFLREGI');
            +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC(w_PNFLPROV),'PNT_MAST','PNFLPROV');
            +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
            +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(w_PNCODVAL),'PNT_MAST','PNCODVAL');
            +",PNRIFDOC ="+cp_NullLink(cp_ToStrODBC(w_PNRIFDOC),'PNT_MAST','PNRIFDOC');
            +",PNRIFDIS ="+cp_NullLink(cp_ToStrODBC(w_PNRIFDIS),'PNT_MAST','PNRIFDIS');
            +",PNRIFINC ="+cp_NullLink(cp_ToStrODBC(w_PNRIFINC),'PNT_MAST','PNRIFINC');
            +",PNRIFCES ="+cp_NullLink(cp_ToStrODBC(w_PNRIFCES),'PNT_MAST','PNRIFCES');
            +",PNRIFACC ="+cp_NullLink(cp_ToStrODBC(w_PNRIFACC),'PNT_MAST','PNRIFACC');
            +",PNFLLIBG ="+cp_NullLink(cp_ToStrODBC(w_PNFLLIBG),'PNT_MAST','PNFLLIBG');
            +",PNDATPLA ="+cp_NullLink(cp_ToStrODBC(w_PNCOMIVA),'PNT_MAST','PNDATPLA');
                +i_ccchkf ;
            +" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                PNTIPREG = w_PNTIPREG;
                ,PNFLIVDF = w_PNFLIVDF;
                ,PNFLGDIF = w_PNFLGDIF;
                ,PNNUMREG = w_PNNUMREG;
                ,PNTIPDOC = w_PNTIPDOC;
                ,PNCAOVAL = w_PNCAOVAL;
                ,PNDESSUP = w_PNDESSUP;
                ,PNTIPCLF = this.oParentObject.ULT_TCLF;
                ,PNCODCLF = this.oParentObject.ULT_CLFO;
                ,PNTOTDOC = w_PNTOTDOC;
                ,PNCOMIVA = w_PNCOMIVA;
                ,PNFLREGI = w_PNFLREGI;
                ,PNFLPROV = w_PNFLPROV;
                ,PNVALNAZ = w_PNVALNAZ;
                ,PNCODVAL = w_PNCODVAL;
                ,PNRIFDOC = w_PNRIFDOC;
                ,PNRIFDIS = w_PNRIFDIS;
                ,PNRIFINC = w_PNRIFINC;
                ,PNRIFCES = w_PNRIFCES;
                ,PNRIFACC = w_PNRIFACC;
                ,PNFLLIBG = w_PNFLLIBG;
                ,PNDATPLA = w_PNCOMIVA;
                &i_ccchkf. ;
             where;
                PNSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Stessa registrazione
          this.w_PNSERIAL = this.oParentObject.ULT_SERI
          w_PNNUMRER = this.oParentObject.ULT_NREG
          w_PNNUMPRO = this.oParentObject.ULT_PROT
          this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Seriale %1 numero registrazione %2 del %3 riga %4" , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , dtoc(w_PNDATREG) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
          if empty(this.oParentObject.ULT_CLFO) and !(empty(w_PNCODCLF) and empty(w_EXCLIFOR))
            * --- Aggiorna cliente / fornitore
            this.oParentObject.ULT_TCLF = iif( empty(this.oParentObject.ULT_TCLF), w_PNTIPCLF, this.oParentObject.ULT_TCLF)
            this.oParentObject.ULT_CLFO = iif( empty(this.oParentObject.ULT_CLFO) .and. this.oParentObject.ULT_TCLF=w_PNTIPCON .and. (.not. empty(this.oParentObject.ULT_TCLF)), iif(!empty(w_EXCLIFOR),w_EXCLIFOR,w_PNCODCLF), this.oParentObject.ULT_CLFO)
            this.oParentObject.ULT_CLFO = Left(this.oParentObject.ULT_CLFO+space(15),15)
            * --- Write into PNT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNTIPCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_TCLF),'PNT_MAST','PNTIPCLF');
              +",PNCODCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_CLFO),'PNT_MAST','PNCODCLF');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                     )
            else
              update (i_cTable) set;
                  PNTIPCLF = this.oParentObject.ULT_TCLF;
                  ,PNCODCLF = this.oParentObject.ULT_CLFO;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if w_EXNUMRIG=0 .or. this.oParentObject.ULT_CHIRIG <> this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
            this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
          endif
          this.oParentObject.ULT_RNUMAN = this.oParentObject.ULT_RNUMAN + 1
          * --- Rilegge informazioni di testata da database di adhoc (quelle che servono)
          * --- Read from PNT_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PNVALNAZ,PNFLPROV,PNCODVAL,PNCAOVAL,PNTOTDOC,PNCODESE,PNCODUTE,PNDATREG,PNCOMPET,PNCOMIVA"+;
              " from "+i_cTable+" PNT_MAST where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PNVALNAZ,PNFLPROV,PNCODVAL,PNCAOVAL,PNTOTDOC,PNCODESE,PNCODUTE,PNDATREG,PNCOMPET,PNCOMIVA;
              from (i_cTable) where;
                  PNSERIAL = this.w_PNSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PNVALNAZ = NVL(cp_ToDate(_read_.PNVALNAZ),cp_NullValue(_read_.PNVALNAZ))
            w_PNFLPROV = NVL(cp_ToDate(_read_.PNFLPROV),cp_NullValue(_read_.PNFLPROV))
            w_PNCODVAL = NVL(cp_ToDate(_read_.PNCODVAL),cp_NullValue(_read_.PNCODVAL))
            w_PNCAOVAL = NVL(cp_ToDate(_read_.PNCAOVAL),cp_NullValue(_read_.PNCAOVAL))
            w_PNTOTDOC = NVL(cp_ToDate(_read_.PNTOTDOC),cp_NullValue(_read_.PNTOTDOC))
            w_PNCODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
            w_PNCODUTE = NVL(cp_ToDate(_read_.PNCODUTE),cp_NullValue(_read_.PNCODUTE))
            w_PNDATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
            w_PNCOMPET = NVL(cp_ToDate(_read_.PNCOMPET),cp_NullValue(_read_.PNCOMPET))
            w_PNCOMIVA = NVL(cp_ToDate(_read_.PNCOMIVA),cp_NullValue(_read_.PNCOMIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        w_PNTIPCLF = this.oParentObject.ULT_TCLF
        w_PNCODCLF = this.oParentObject.ULT_CLFO
        w_PNCODPAG = left(alltrim(w_PNCODPAG)+space(5),5)
        w_EXCODIVA = left(alltrim(w_EXCODIVA)+space(5),5)
        w_PNCODCON = left(alltrim(w_PNCODCON)+space(15),15)
        if w_EXTIPORI<>"W" AND EMPTY(w_PNCODCON)
          * --- Segnala se manca il codice conto
          this.oParentObject.w_ResoMode = "NOCODCON"
          this.oParentObject.Pag4()
        endif
        w_PNTIPCON = iif( empty(w_PNTIPCON), "G", left(w_PNTIPCON,1))
        * --- Se il codice del pagamento non � presente in AHW ed � attiva la gestione partite viene letto il cod. pag. presente in anagrafica cliente/fornitore
        if EMPTY(w_PNCODPAG) AND g_PERPAR="S" AND w_PNTIPCON<>"G"
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODPAG"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODPAG;
              from (i_cTable) where;
                  ANTIPCON = w_PNTIPCON;
                  and ANCODICE = w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PNCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(w_PNCODPAG) AND (w_PNFLPART="S" OR w_PNFLPART="C" OR w_PNFLPART="A")
            * --- Segnala se manca il codice pagamento
            this.oParentObject.w_ResoMode = "NOCODPAG"
            this.oParentObject.Pag4()
          endif
        endif
        w_PNFLZERO = iif( empty(w_PNFLZERO) .and. w_PNIMPDAR=0 .and. w_PNIMPAVE=0, "S" , w_PNFLZERO )
        w_PNDESRIG = iif( empty(w_PNDESRIG) , w_PNDESSUP , w_PNDESRIG )
        w_PNCAURIG = iif( empty(w_PNCAURIG) , w_PNCODCAU , w_PNCAURIG )
        w_PNFLPART = iif( empty(w_PNFLPART) , "N" , w_PNFLPART )
        * --- Flag provvisorio o definitiva
        if w_PNFLPROV="S"
          w_PNFLSALD = " "
          w_PNFLSALI = " "
          w_PNFLSALF = " "
        else
          w_PNFLSALD = "+"
          w_PNFLSALI = IIF(w_FLSALI="S" , "+", " ")
          w_PNFLSALF = IIF(w_FLSALF="S" , "+", " ")
          if w_PNTIPCON="G" AND (w_PNFLSALF<>" " OR w_PNFLSALI<>" ")
            * --- Verifica se Conto Transitorio (Non aggiorna Saldi Iniziale/Finale)
            this.w_CONSUP = SPACE(15)
            this.w_SEZBIL = " "
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCONSUP"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(w_PNTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(w_PNCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCONSUP;
                from (i_cTable) where;
                    ANTIPCON = w_PNTIPCON;
                    and ANCODICE = w_PNCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from MASTRI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MCSEZBIL"+;
                " from "+i_cTable+" MASTRI where ";
                    +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MCSEZBIL;
                from (i_cTable) where;
                    MCCODICE = this.w_CONSUP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_SEZBIL="T"
              w_PNFLSALF = " "
              w_PNFLSALI = " "
            endif
          endif
        endif
        * --- Aggiornamento archivi collegati (Iva, Partite, Saldi, ecc.)
        * --- Se riga a Zero che crea Castelletto Iva non inserisco la riga in Primanota
        *     ma creo solo il record nel castelletto
        this.w_NOPNT = .not. empty(w_EXCODIVA) AND w_PNFLZERO="S"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.w_NOPNT
          * --- Inserimento record detail
          if w_EXNUMRIG=0 .or. this.oParentObject.ULT_CHIRIG <> this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
            w_CPROWORD = this.oParentObject.ULT_RNUM * 10
            * --- Try
            local bErr_048FD4E0
            bErr_048FD4E0=bTrsErr
            this.Try_048FD4E0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_048FD4E0
            * --- End
            * --- Scrittura riga di dettaglio
            * --- Resoconto
            this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Seriale %1 numero registrazione %2 del %3 riga %4" , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , dtoc(w_PNDATREG) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
            * --- Write into PNT_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
              +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
              +",PNFLPART ="+cp_NullLink(cp_ToStrODBC(w_PNFLPART),'PNT_DETT','PNFLPART');
              +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(w_PNFLZERO),'PNT_DETT','PNFLZERO');
              +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(w_PNDESRIG),'PNT_DETT','PNDESRIG');
              +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(w_PNCAURIG),'PNT_DETT','PNCAURIG');
              +",PNCODPAG ="+cp_NullLink(cp_ToStrODBC(w_PNCODPAG),'PNT_DETT','PNCODPAG');
              +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(w_PNFLSALD),'PNT_DETT','PNFLSALD');
              +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(w_PNFLSALI),'PNT_DETT','PNFLSALI');
              +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(w_PNFLSALF),'PNT_DETT','PNFLSALF');
              +",PNINICOM ="+cp_NullLink(cp_ToStrODBC(w_PNINICOM),'PNT_DETT','PNINICOM');
              +",PNFINCOM ="+cp_NullLink(cp_ToStrODBC(w_PNFINCOM),'PNT_DETT','PNFINCOM');
              +",PNLIBGIO ="+cp_NullLink(cp_ToStrODBC(w_PNLIBGIO),'PNT_DETT','PNLIBGIO');
              +",PNFLABAN ="+cp_NullLink(cp_ToStrODBC(w_PNFLABAN),'PNT_DETT','PNFLABAN');
              +",PNIMPIND ="+cp_NullLink(cp_ToStrODBC(w_PNIMPIND),'PNT_DETT','PNIMPIND');
              +",PNCODBUN ="+cp_NullLink(cp_ToStrODBC(w_PNCODBUN),'PNT_DETT','PNCODBUN');
              +",PNCODAGE ="+cp_NullLink(cp_ToStrODBC(w_PNCODAGE),'PNT_DETT','PNCODAGE');
              +",PNFLVABD ="+cp_NullLink(cp_ToStrODBC(w_PNFLVABD),'PNT_DETT','PNFLVABD');
                  +i_ccchkf ;
              +" where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                     )
            else
              update (i_cTable) set;
                  PNIMPDAR = w_PNIMPDAR;
                  ,PNIMPAVE = w_PNIMPAVE;
                  ,PNFLPART = w_PNFLPART;
                  ,PNFLZERO = w_PNFLZERO;
                  ,PNDESRIG = w_PNDESRIG;
                  ,PNCAURIG = w_PNCAURIG;
                  ,PNCODPAG = w_PNCODPAG;
                  ,PNFLSALD = w_PNFLSALD;
                  ,PNFLSALI = w_PNFLSALI;
                  ,PNFLSALF = w_PNFLSALF;
                  ,PNINICOM = w_PNINICOM;
                  ,PNFINCOM = w_PNFINCOM;
                  ,PNLIBGIO = w_PNLIBGIO;
                  ,PNFLABAN = w_PNFLABAN;
                  ,PNIMPIND = w_PNIMPIND;
                  ,PNCODBUN = w_PNCODBUN;
                  ,PNCODAGE = w_PNCODAGE;
                  ,PNFLVABD = w_PNFLVABD;
                  &i_ccchkf. ;
               where;
                  PNSERIAL = this.w_PNSERIAL;
                  and CPROWNUM = this.oParentObject.ULT_RNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        * --- Memorizza la nuova chiave di rottura della primanota
        this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
        this.oParentObject.ULT_CHIRIG = this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
      case this.oParentObject.w_Destinaz = "PI"
        * --- Registrazioni IVA
        * --- Se e' stato scelto di non generare automaticamente le righe IVA dalle righe di primanota e'
        * --- possibile importarle da una file ascii apposito. Il legame con le registrazioni di primanota
        * --- avviene per numero e data registrazione.
        * --- Lettura del serial della registrazione di primanota
        this.w_PNSERIAL = " "
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNSERIAL"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNDATREG = "+cp_ToStrODBC(w_EXDATREG);
                +" and PNNUMRER = "+cp_ToStrODBC(w_EXNUMREG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNSERIAL;
            from (i_cTable) where;
                PNDATREG = w_EXDATREG;
                and PNNUMRER = w_EXNUMREG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNSERIAL = NVL(cp_ToDate(_read_.PNSERIAL),cp_NullValue(_read_.PNSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if .not. empty( this.w_PNSERIAL )
          w_PNTIPCON = "G"
          w_IVNUMREG = iif( empty( w_IVNUMREG ), 1, w_IVNUMREG)
          For this.oParentObject.ULT_RNUM = 1 to 9
          w_IVCODIVA = "w_EXCODIV" + str(this.oParentObject.ULT_RNUM,1,0)
          w_IVCODIVA = &w_IVCODIVA
          w_IVIMPONI = "w_EXIMPON" + str(this.oParentObject.ULT_RNUM,1,0)
          w_IVIMPONI = &w_IVIMPONI
          w_IVIMPIVA = "w_EXIMPIV" + str(this.oParentObject.ULT_RNUM,1,0)
          w_IVIMPIVA = &w_IVIMPIVA
          if (.not. empty( w_IVCODIVA ))
            this.w_IVPERIND = 0
            this.w_IVPERIVA = 0
            * --- Read from VOCIIVA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOCIIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IVPERIND,IVPERIVA"+;
                " from "+i_cTable+" VOCIIVA where ";
                    +"IVCODIVA = "+cp_ToStrODBC(w_IVCODIVA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IVPERIND,IVPERIVA;
                from (i_cTable) where;
                    IVCODIVA = w_IVCODIVA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IVPERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
              this.w_IVPERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Try
            local bErr_049B4348
            bErr_049B4348=bTrsErr
            this.Try_049B4348()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.oParentObject.w_ResoMode = "SOLOMESS"
              this.oParentObject.w_ResoTipo = "E"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile scrivere la riga IVA nella tabella PNT_IVA %1%0serial %2 numero registrazione %3 riga %4" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_PNSERIAL) , alltrim(str(w_EXNUMREG,6,0)) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
              this.oParentObject.Pag4()
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_049B4348
            * --- End
          endif
          next
        else
          this.oParentObject.w_ResoMode = "SOLOMESS"
          this.oParentObject.w_ResoTipo = "E"
          this.oParentObject.w_ResoMess = ah_msgformat("Scartato il record numero %1 del file %2.%0La riga IVA fa riferimento ad una registrazione non presente in primanota.%0Registrazione numero %3 del %4" , alltrim(str(recno("CursAscii"),10)) , alltrim(this.oParentObject.w_IM_ASCII) , alltrim(str(w_EXNUMREG,6,0)) , dtoc(w_EXDATREG) )
          this.oParentObject.Pag4()
        endif
        i_rows = 1
        this.oParentObject.w_ResoDett = "<SENZA RESOCONTO>"
      case this.oParentObject.w_Destinaz = "CE"
        * --- Centri di costo
        w_CCNUMLIV = iif( empty( w_CCNUMLIV ), 1, w_CCNUMLIV )
        * --- Write into CENCOST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CENCOST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CC__NOTE ="+cp_NullLink(cp_ToStrODBC(w_CC__NOTE),'CENCOST','CC__NOTE');
          +",CCDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CCDTINVA),'CENCOST','CCDTINVA');
          +",CCDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CCDTOBSO),'CENCOST','CCDTOBSO');
          +",CCCODCAN ="+cp_NullLink(cp_ToStrODBC(w_CCCODCAN),'CENCOST','CCCODCAN');
          +",CCNUMLIV ="+cp_NullLink(cp_ToStrODBC(w_CCNUMLIV),'CENCOST','CCNUMLIV');
          +",CCDESPIA ="+cp_NullLink(cp_ToStrODBC(w_CCDESPIA),'CENCOST','CCDESPIA');
              +i_ccchkf ;
          +" where ";
              +"CC_CONTO = "+cp_ToStrODBC(w_CC_CONTO);
                 )
        else
          update (i_cTable) set;
              CC__NOTE = w_CC__NOTE;
              ,CCDTINVA = w_CCDTINVA;
              ,CCDTOBSO = w_CCDTOBSO;
              ,CCCODCAN = w_CCCODCAN;
              ,CCNUMLIV = w_CCNUMLIV;
              ,CCDESPIA = w_CCDESPIA;
              &i_ccchkf. ;
           where;
              CC_CONTO = w_CC_CONTO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.w_ResoDett = trim( w_CC_CONTO )
      case this.oParentObject.w_Destinaz = "VC"
        * --- Voci di Costo/Ricavo
        w_VCTIPVOC = iif( empty( w_VCTIPVOC ), "C" , w_VCTIPVOC )
        * --- Write into VOC_COST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.VOC_COST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"VCDESCRI ="+cp_NullLink(cp_ToStrODBC(w_VCDESCRI),'VOC_COST','VCDESCRI');
          +",VCTIPVOC ="+cp_NullLink(cp_ToStrODBC(w_VCTIPVOC),'VOC_COST','VCTIPVOC');
          +",VCDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_VCDTOBSO),'VOC_COST','VCDTOBSO');
          +",VCDTINVA ="+cp_NullLink(cp_ToStrODBC(w_VCDTINVA),'VOC_COST','VCDTINVA');
              +i_ccchkf ;
          +" where ";
              +"VCCODICE = "+cp_ToStrODBC(w_VCCODICE);
                 )
        else
          update (i_cTable) set;
              VCDESCRI = w_VCDESCRI;
              ,VCTIPVOC = w_VCTIPVOC;
              ,VCDTOBSO = w_VCDTOBSO;
              ,VCDTINVA = w_VCDTINVA;
              &i_ccchkf. ;
           where;
              VCCODICE = w_VCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.w_ResoDett = trim( w_VCCODICE )
      case this.oParentObject.w_Destinaz = "CO"
        * --- Commesse
        w_CNTIPCON = iif( empty( w_CNTIPCON ), "C" , w_CNTIPCON )
        * --- Valuta
        if type("w_CNCODVAL")="U"
          w_CNCODVAL=g_PERVAL
        endif
        if nvl(w_CNCODVAL,g_PERVAL)<>g_CODLIR and nvl(w_CNCODVAL,g_PERVAL)<>g_PERVAL
          w_CNCODVAL=g_PERVAL
        endif
        * --- Date inizio e fine
        if type("w_CNDATINI")="U"
          w_CNDATINI=cp_CharToDate("   -  -    ")
        endif
        if type("w_CNDATFIN")="U"
          w_CNDATFIN=cp_CharToDate("   -  -    ")
        endif
        * --- Valore
        if type("w_CNIMPORT")="U"
          w_CNIMPORT=0
        endif
        * --- Write into CAN_TIER
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CNDESCAN ="+cp_NullLink(cp_ToStrODBC(w_CNDESCAN),'CAN_TIER','CNDESCAN');
          +",CN__NOTE ="+cp_NullLink(cp_ToStrODBC(w_CN__NOTE),'CAN_TIER','CN__NOTE');
          +",CNINDIRI ="+cp_NullLink(cp_ToStrODBC(w_CNINDIRI),'CAN_TIER','CNINDIRI');
          +",CN___CAP ="+cp_NullLink(cp_ToStrODBC(w_CN___CAP),'CAN_TIER','CN___CAP');
          +",CNLOCALI ="+cp_NullLink(cp_ToStrODBC(w_CNLOCALI),'CAN_TIER','CNLOCALI');
          +",CNPROVIN ="+cp_NullLink(cp_ToStrODBC(w_CNPROVIN),'CAN_TIER','CNPROVIN');
          +",CNRESPON ="+cp_NullLink(cp_ToStrODBC(w_CNRESPON),'CAN_TIER','CNRESPON');
          +",CNTELEFO ="+cp_NullLink(cp_ToStrODBC(w_CNTELEFO),'CAN_TIER','CNTELEFO');
          +",CN_EMAIL ="+cp_NullLink(cp_ToStrODBC(w_CN_EMAIL),'CAN_TIER','CN_EMAIL');
          +",CNCODVET ="+cp_NullLink(cp_ToStrODBC(w_CNCODVET),'CAN_TIER','CNCODVET');
          +",CNCODPOR ="+cp_NullLink(cp_ToStrODBC(w_CNCODPOR),'CAN_TIER','CNCODPOR');
          +",CNCODSPE ="+cp_NullLink(cp_ToStrODBC(w_CNCODSPE),'CAN_TIER','CNCODSPE');
          +",CNDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_CNDTOBSO),'CAN_TIER','CNDTOBSO');
          +",CNDTINVA ="+cp_NullLink(cp_ToStrODBC(w_CNDTINVA),'CAN_TIER','CNDTINVA');
          +",CNTIPCON ="+cp_NullLink(cp_ToStrODBC(w_CNTIPCON),'CAN_TIER','CNTIPCON');
          +",CNCODCON ="+cp_NullLink(cp_ToStrODBC(w_CNCODCON),'CAN_TIER','CNCODCON');
          +",CNCODVAL ="+cp_NullLink(cp_ToStrODBC(w_CNCODVAL),'CAN_TIER','CNCODVAL');
          +",CNDATINI ="+cp_NullLink(cp_ToStrODBC(w_CNDATINI),'CAN_TIER','CNDATINI');
          +",CNDATFIN ="+cp_NullLink(cp_ToStrODBC(w_CNDATFIN),'CAN_TIER','CNDATFIN');
          +",CNIMPORT ="+cp_NullLink(cp_ToStrODBC(w_CNIMPORT),'CAN_TIER','CNIMPORT');
          +",CNCODLIS ="+cp_NullLink(cp_ToStrODBC(w_CNCODLIS),'CAN_TIER','CNCODLIS');
          +",CNSERVIZ ="+cp_NullLink(cp_ToStrODBC(w_CNSERVIZ),'CAN_TIER','CNSERVIZ');
          +",CNFLAGIM ="+cp_NullLink(cp_ToStrODBC(w_CNFLAGIM),'CAN_TIER','CNFLAGIM');
          +",CNPRJMOD ="+cp_NullLink(cp_ToStrODBC(w_CNPRJMOD),'CAN_TIER','CNPRJMOD');
              +i_ccchkf ;
          +" where ";
              +"CNCODCAN = "+cp_ToStrODBC(w_CNCODCAN);
                 )
        else
          update (i_cTable) set;
              CNDESCAN = w_CNDESCAN;
              ,CN__NOTE = w_CN__NOTE;
              ,CNINDIRI = w_CNINDIRI;
              ,CN___CAP = w_CN___CAP;
              ,CNLOCALI = w_CNLOCALI;
              ,CNPROVIN = w_CNPROVIN;
              ,CNRESPON = w_CNRESPON;
              ,CNTELEFO = w_CNTELEFO;
              ,CN_EMAIL = w_CN_EMAIL;
              ,CNCODVET = w_CNCODVET;
              ,CNCODPOR = w_CNCODPOR;
              ,CNCODSPE = w_CNCODSPE;
              ,CNDTOBSO = w_CNDTOBSO;
              ,CNDTINVA = w_CNDTINVA;
              ,CNTIPCON = w_CNTIPCON;
              ,CNCODCON = w_CNCODCON;
              ,CNCODVAL = w_CNCODVAL;
              ,CNDATINI = w_CNDATINI;
              ,CNDATFIN = w_CNDATFIN;
              ,CNIMPORT = w_CNIMPORT;
              ,CNCODLIS = w_CNCODLIS;
              ,CNSERVIZ = w_CNSERVIZ;
              ,CNFLAGIM = w_CNFLAGIM;
              ,CNPRJMOD = w_CNPRJMOD;
              &i_ccchkf. ;
           where;
              CNCODCAN = w_CNCODCAN;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.w_ResoDett = trim( w_CNCODCAN )
      case this.oParentObject.w_Destinaz = "PT"
        * --- Partite
        * --- Flag Partita C= Crea, S=Salda
        if this.w_RIFERIME=-1
          w_PTFLCRSA = iif(empty(w_PTFLCRSA),"C",w_PTFLCRSA)
        else
          * --- Rilegge il flag partita dalla primanota
          * --- Read from PNT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PNFLPART"+;
              " from "+i_cTable+" PNT_DETT where ";
                  +"PNSERIAL = "+cp_ToStrODBC(w_PTSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFERIME);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PNFLPART;
              from (i_cTable) where;
                  PNSERIAL = w_PTSERIAL;
                  and CPROWNUM = this.w_RIFERIME;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PTFLCRSA = NVL(cp_ToDate(_read_.PNFLPART),cp_NullValue(_read_.PNFLPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          w_PTFLCRSA = iif(w_PTFLCRSA$"CS",w_PTFLCRSA,"C")
        endif
        * --- Codici BANCA - Se non sono definiti i valori per il codice nostra banca e la banca di appoggio rilegge le informazioni dal conto cliente/fornitore
        if w_PTTIPCON$"CF" and (empty(w_PTBANNOS) or empty(w_PTBANAPP))
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODBAN,ANCODBA2"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(w_PTTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(w_PTCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODBAN,ANCODBA2;
              from (i_cTable) where;
                  ANTIPCON = w_PTTIPCON;
                  and ANCODICE = w_PTCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_XXBANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
            this.w_XXBANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          w_PTBANAPP=iif(empty(w_PTBANAPP),this.w_XXBANAPP,w_PTBANAPP)
          w_PTBANNOS=iif(empty(w_PTBANNOS),this.w_XXBANNOS,w_PTBANNOS)
        endif
        if this.w_RIFERIME=-1
          this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Scadenza diversa %1 del %2 tipo %3" , trim( w_PTNUMPAR ) , dtoc(w_PTDATSCA) , + w_PTTIPCON + w_PTCODCON )
        else
          this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Partita %1 del %2 tipo %3" , trim( w_PTNUMPAR ) , dtoc(w_PTDATSCA) , + w_PTTIPCON + w_PTCODCON )
        endif
        w_PTTOTIMP = ABS(w_PTTOTIMP)
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +",PTDATSCA ="+cp_NullLink(cp_ToStrODBC(w_PTDATSCA),'PAR_TITE','PTDATSCA');
          +",PTTIPCON ="+cp_NullLink(cp_ToStrODBC(w_PTTIPCON),'PAR_TITE','PTTIPCON');
          +",PTCODCON ="+cp_NullLink(cp_ToStrODBC(w_PTCODCON),'PAR_TITE','PTCODCON');
          +",PT_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
          +",PTTOTIMP ="+cp_NullLink(cp_ToStrODBC(w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
          +",PTCODVAL ="+cp_NullLink(cp_ToStrODBC(w_PTCODVAL),'PAR_TITE','PTCODVAL');
          +",PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
          +",PTDATAPE ="+cp_NullLink(cp_ToStrODBC(w_PTDATAPE),'PAR_TITE','PTDATAPE');
          +",PTNUMDOC ="+cp_NullLink(cp_ToStrODBC(w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
          +",PTALFDOC ="+cp_NullLink(cp_ToStrODBC(w_PTALFDOC),'PAR_TITE','PTALFDOC');
          +",PTDATDOC ="+cp_NullLink(cp_ToStrODBC(w_PTDATDOC),'PAR_TITE','PTDATDOC');
          +",PTIMPDOC ="+cp_NullLink(cp_ToStrODBC(w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
          +",PTMODPAG ="+cp_NullLink(cp_ToStrODBC(w_PTMODPAG),'PAR_TITE','PTMODPAG');
          +",PTFLSOSP ="+cp_NullLink(cp_ToStrODBC(w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
          +",PTBANAPP ="+cp_NullLink(cp_ToStrODBC(w_PTBANAPP),'PAR_TITE','PTBANAPP');
          +",PTBANNOS ="+cp_NullLink(cp_ToStrODBC(w_PTBANNOS),'PAR_TITE','PTBANNOS');
          +",PTFLRAGG ="+cp_NullLink(cp_ToStrODBC(w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
          +",PTFLRITE ="+cp_NullLink(cp_ToStrODBC(w_PTFLRITE),'PAR_TITE','PTFLRITE');
          +",PTTOTABB ="+cp_NullLink(cp_ToStrODBC(w_PTTOTABB),'PAR_TITE','PTTOTABB');
          +",PTFLCRSA ="+cp_NullLink(cp_ToStrODBC(w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
          +",PTFLIMPE ="+cp_NullLink(cp_ToStrODBC(w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
          +",PTNUMDIS ="+cp_NullLink(cp_ToStrODBC(w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
          +",PTNUMEFF ="+cp_NullLink(cp_ToStrODBC(w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
          +",PTFLINDI ="+cp_NullLink(cp_ToStrODBC(w_PTFLINDI),'PAR_TITE','PTFLINDI');
          +",PTDESRIG ="+cp_NullLink(cp_ToStrODBC(w_PTDESRIG),'PAR_TITE','PTDESRIG');
          +",PTNUMPRO ="+cp_NullLink(cp_ToStrODBC(w_PTNUMPRO),'PAR_TITE','PTNUMPRO');
          +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(w_PTCODAGE),'PAR_TITE','PTCODAGE');
          +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(w_PTSERRIF),'PAR_TITE','PTSERRIF');
          +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(w_PTORDRIF),'PAR_TITE','PTORDRIF');
          +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
          +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(w_PTFLVABD),'PAR_TITE','PTFLVABD');
          +",PTDATINT ="+cp_NullLink(cp_ToStrODBC(w_PTDATINT),'PAR_TITE','PTDATINT');
          +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(w_PTRIFIND),'PAR_TITE','PTRIFIND');
          +",PTDATREG ="+cp_NullLink(cp_ToStrODBC(w_PTDATREG),'PAR_TITE','PTDATREG');
          +",PTNUMCOR ="+cp_NullLink(cp_ToStrODBC(w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_SCCODICE);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_RIFERIME);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              PTNUMPAR = w_PTNUMPAR;
              ,PTDATSCA = w_PTDATSCA;
              ,PTTIPCON = w_PTTIPCON;
              ,PTCODCON = w_PTCODCON;
              ,PT_SEGNO = w_PT_SEGNO;
              ,PTTOTIMP = w_PTTOTIMP;
              ,PTCODVAL = w_PTCODVAL;
              ,PTCAOVAL = w_PTCAOVAL;
              ,PTCAOAPE = w_PTCAOVAL;
              ,PTDATAPE = w_PTDATAPE;
              ,PTNUMDOC = w_PTNUMDOC;
              ,PTALFDOC = w_PTALFDOC;
              ,PTDATDOC = w_PTDATDOC;
              ,PTIMPDOC = w_PTIMPDOC;
              ,PTMODPAG = w_PTMODPAG;
              ,PTFLSOSP = w_PTFLSOSP;
              ,PTBANAPP = w_PTBANAPP;
              ,PTBANNOS = w_PTBANNOS;
              ,PTFLRAGG = w_PTFLRAGG;
              ,PTFLRITE = w_PTFLRITE;
              ,PTTOTABB = w_PTTOTABB;
              ,PTFLCRSA = w_PTFLCRSA;
              ,PTFLIMPE = w_PTFLIMPE;
              ,PTNUMDIS = w_PTNUMDIS;
              ,PTNUMEFF = w_PTNUMEFF;
              ,PTFLINDI = w_PTFLINDI;
              ,PTDESRIG = w_PTDESRIG;
              ,PTNUMPRO = w_PTNUMPRO;
              ,PTCODAGE = w_PTCODAGE;
              ,PTSERRIF = w_PTSERRIF;
              ,PTORDRIF = w_PTORDRIF;
              ,PTNUMRIF = w_PTNUMRIF;
              ,PTFLVABD = w_PTFLVABD;
              ,PTDATINT = w_PTDATINT;
              ,PTRIFIND = w_PTRIFIND;
              ,PTDATREG = w_PTDATREG;
              ,PTNUMCOR = w_PTNUMCOR;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_SCCODICE;
              and PTROWORD = this.w_RIFERIME;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if EMPTY(NVL(w_PTMODPAG,""))
          * --- Segnala se manca il codice Pagamento
          this.oParentObject.w_ResoMode = "NOMODPAG"
          this.oParentObject.Pag4()
        endif
      case this.oParentObject.w_Destinaz = "MC"
        * --- Movimenti Analitica
        w_MR_SEGNO = iif( empty( w_MR_SEGNO ), "D", w_MR_SEGNO )
        this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Registrazione numero %1 del %2" , alltrim( str(this.w_CMNUMREG,6,0) ) , dtoc(w_EXDATREG) )
        if NOT EMPTY(w_MRCODCOM)
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(w_MRCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CNCODCAN = w_MRCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              this.oParentObject.w_ResoMode = "SCARTO"
              this.oParentObject.w_ResoDett = AH_MSGFORMAT( "Impossibile aggiornare commessa %2 non presente nella tabella CAN_TIER" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) ,alltrim(w_MRCODCOM) )
              this.oParentObject.Pag4()
              * --- Raise
              i_Error="errore"
              return
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Write into MOVICOST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MR_SEGNO ="+cp_NullLink(cp_ToStrODBC(w_MR_SEGNO),'MOVICOST','MR_SEGNO');
          +",MRTOTIMP ="+cp_NullLink(cp_ToStrODBC(w_MRTOTIMP),'MOVICOST','MRTOTIMP');
          +",MRPARAME ="+cp_NullLink(cp_ToStrODBC(w_MRPARAME),'MOVICOST','MRPARAME');
          +",MRINICOM ="+cp_NullLink(cp_ToStrODBC(w_MRINICOM),'MOVICOST','MRINICOM');
          +",MRFINCOM ="+cp_NullLink(cp_ToStrODBC(w_MRFINCOM),'MOVICOST','MRFINCOM');
          +",MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(w_MRFLRIPA),'MOVICOST','MRFLRIPA');
          +",MRCODVOC ="+cp_NullLink(cp_ToStrODBC(w_MRCODVOC),'MOVICOST','MRCODVOC');
          +",MRCODICE ="+cp_NullLink(cp_ToStrODBC(w_MRCODICE),'MOVICOST','MRCODICE');
          +",MRCODCOM ="+cp_NullLink(cp_ToStrODBC(w_MRCODCOM),'MOVICOST','MRCODCOM');
              +i_ccchkf ;
          +" where ";
              +"MRSERIAL = "+cp_ToStrODBC(this.w_CMCODICE);
              +" and MRROWORD = "+cp_ToStrODBC(this.w_RIFERIME);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                 )
        else
          update (i_cTable) set;
              MR_SEGNO = w_MR_SEGNO;
              ,MRTOTIMP = w_MRTOTIMP;
              ,MRPARAME = w_MRPARAME;
              ,MRINICOM = w_MRINICOM;
              ,MRFINCOM = w_MRFINCOM;
              ,MRFLRIPA = w_MRFLRIPA;
              ,MRCODVOC = w_MRCODVOC;
              ,MRCODICE = w_MRCODICE;
              ,MRCODCOM = w_MRCODCOM;
              &i_ccchkf. ;
           where;
              MRSERIAL = this.w_CMCODICE;
              and MRROWORD = this.w_RIFERIME;
              and CPROWNUM = this.oParentObject.ULT_RNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return
  proc Try_048FD4E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODCON),'PNT_DETT','PNCODCON');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.oParentObject.ULT_RNUM,'CPROWORD',w_CPROWORD,'PNTIPCON',w_PNTIPCON,'PNCODCON',w_PNCODCON)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,w_CPROWORD;
           ,w_PNTIPCON;
           ,w_PNCODCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_049B4348()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_IVA
    i_nConn=i_TableProp[this.PNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPIVA"+",IVIMPONI"+",IVCODCON"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PNT_IVA','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_IVCODIVA),'PNT_IVA','IVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'PNT_IVA','IVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IVPERIND),'PNT_IVA','IVPERIND');
      +","+cp_NullLink(cp_ToStrODBC(w_IVTIPREG),'PNT_IVA','IVTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(w_IVNUMREG),'PNT_IVA','IVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
      +","+cp_NullLink(cp_ToStrODBC(w_IVIMPONI),'PNT_IVA','IVIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(w_IVCODCON),'PNT_IVA','IVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(w_IVCFLOMA),'PNT_IVA','IVCFLOMA');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCONTRO),'PNT_IVA','IVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(w_EXTIPCOP),'PNT_IVA','IVTIPCOP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.oParentObject.ULT_RNUM,'IVCODIVA',w_IVCODIVA,'IVTIPCON',w_PNTIPCON,'IVPERIND',this.w_IVPERIND,'IVTIPREG',w_IVTIPREG,'IVNUMREG',w_IVNUMREG,'IVIMPIVA',w_IVIMPIVA,'IVIMPONI',w_IVIMPONI,'IVCODCON',w_IVCODCON,'IVFLOMAG',w_IVFLOMAG,'IVCFLOMA',w_IVCFLOMA)
      insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPIVA,IVIMPONI,IVCODCON,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,w_IVCODIVA;
           ,w_PNTIPCON;
           ,this.w_IVPERIND;
           ,w_IVTIPREG;
           ,w_IVNUMREG;
           ,w_IVIMPIVA;
           ,w_IVIMPONI;
           ,w_IVCODCON;
           ,w_IVFLOMAG;
           ,w_IVCFLOMA;
           ,w_EXCONTRO;
           ,w_EXTIPCOP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento records negli archivi collegati alla primanota
    * --- Lettura dati Cliente/Fornitore
    this.w_ANTIPSOT = " "
    w_BANAPP = " "
    w_BANNOS = " "
    w_CAONAZ = 0
    this.w_DECTOT = 0
    w_NUMCOR = " "
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL,VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(w_PNVALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL,VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = w_PNVALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    store 0 to w_MESE1, w_MESE2, w_GIORN1, w_GIORN2, w_GIOFIS
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANTIPSOT,ANCODBAN,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODBA2,ANCCTAGG ,ANNUMCOR"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(w_PNTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(w_PNCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANTIPSOT,ANCODBAN,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS,ANCODBA2,ANCCTAGG ,ANNUMCOR;
        from (i_cTable) where;
            ANTIPCON = w_PNTIPCON;
            and ANCODICE = w_PNCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANTIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
      w_BANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
      w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
      w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
      w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
      w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
      w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
      w_BANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
      this.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG ),cp_NullValue(_read_.ANCCTAGG ))
      w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Con l'analitica la stessa riga di primanota pu� essere presente pi� volte. Considera solo la prima.
    if w_EXNUMRIG=0 .or. this.oParentObject.ULT_CHIRIG <> this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
      * --- Righe IVA
      if w_PNTIPCON="G" .and. (.not. empty(w_EXCODIVA))
        if this.w_ANTIPSOT = "I"
          this.w_TTCODCOI = SPACE(15)
          this.w_TTFLOMAG = "X"
          this.w_TTCFLOMA = "X"
          this.w_IVPERIND = 0
          this.w_IVPERIVA = 0
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIND,IVPERIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(w_EXCODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIND,IVPERIVA;
              from (i_cTable) where;
                  IVCODIVA = w_EXCODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVPERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            this.w_IVPERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Gestione conto IVA Indetraibile
          * --- E' possibile specificare, nei tracciati, un codice di conto da utilizzare per le righe con IVA indetraibile
          this.oParentObject.w_IMCONIND = NVL(this.oParentObject.w_IMCONIND,space(15))
          if this.w_IVPERIND<>0 .and. (.not. empty( this.oParentObject.w_IMCONIND ))
            this.w_TTCODCOI = this.oParentObject.w_IMCONIND
          endif
          * --- Scrittura riga IVA
          if this.oParentObject.w_IMAGGIVA="S"
            w_IVFLOMAG=IIF(TYPE("w_IVFLOMAG")="U","X",w_IVFLOMAG) 
 w_IVCFLOMA=IIF(TYPE("w_IVCFLOMA")="U","X",w_IVCFLOMA) 
 w_IVCODCOI=IIF(TYPE("w_IVCODCOI")="U","",w_IVCODCOI)
            w_EXIMPIVA = iif( empty(w_EXIMPIVA), iif( empty(w_PNIMPDAR) , w_PNIMPAVE , w_PNIMPDAR ) , w_EXIMPIVA)
            w_EXIMPONI = iif( empty(w_EXIMPONI) .and. this.w_IVPERIVA #0, cp_round( w_EXIMPIVA *100/ this.w_IVPERIVA , this.w_DECTOT ) , w_EXIMPONI )
            * --- Le variabili TTFLOMAG e TTCFLOMAG per la gestione del Flag Omaggio e Flag Omaggio x Test sono valorizzate a 'X'
            * --- vengono valorizzate a 'S' se gli importi sono a zero.
            if w_EXIMPIVA=0 AND w_EXIMPONI=0
              this.w_TTFLOMAG = "S"
              this.w_TTCFLOMA = "S"
            endif
            * --- Try
            local bErr_04A653D8
            bErr_04A653D8=bTrsErr
            this.Try_04A653D8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.oParentObject.w_ResoMode = "SOLOMESS"
              this.oParentObject.w_ResoTipo = "E"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile scrivere la riga IVA nella tabella PNT_IVA %1%0serial %2 numero registrazione %3 riga %4" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
              this.oParentObject.Pag4()
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04A653D8
            * --- End
          endif
        endif
      endif
      * --- Partite
      if this.oParentObject.w_IMAGGPAR="S" .and. (.Not. this.w_NOPNT)
        w_PTMODPAG = space(10)
        w_PTNUMCOR = space(25)
        * --- Read from PAG_2AME
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAG_2AME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "P2MODPAG"+;
            " from "+i_cTable+" PAG_2AME where ";
                +"P2CODICE = "+cp_ToStrODBC(w_PNCODPAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            P2MODPAG;
            from (i_cTable) where;
                P2CODICE = w_PNCODPAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_PTMODPAG = NVL(cp_ToDate(_read_.P2MODPAG),cp_NullValue(_read_.P2MODPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not empty(nvl(w_NUMCOR,""))
          * --- Read from MOD_PAGA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MPTIPPAG"+;
              " from "+i_cTable+" MOD_PAGA where ";
                  +"MPCODICE = "+cp_ToStrODBC(w_PTMODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MPTIPPAG;
              from (i_cTable) where;
                  MPCODICE = w_PTMODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          w_PTNUMCOR=IIF(this.w_TIPPAG $ "BO-RI",w_NUMCOR, SPACE(25))
        endif
        do case
          case (.not. empty( w_EXNUMPAR )) .and. (.not. empty( w_EXDATSCA ))
            * --- Caso A : Una riga di partita per ogni riga di primanota
            * --- La procedura crea una riga di partita per ogni riga per la quale siano stati specificati il
            * --- numero della partita, la data di scadenza e l'importo della partita.
            w_CPROWNUM = 1
            w_EXFLDAVE = iif( empty( w_EXFLDAVE ) , iif( w_PNIMPAVE=0 , "D", "A" ) , w_EXFLDAVE )
            w_PTBANAPP = IIF( w_PNTIPCON $ "CF", w_BANAPP, SPACE(10) )
            w_PTBANNOS = IIF( w_PNTIPCON $ "CF", w_BANNOS, SPACE(10) )
            * --- Try
            local bErr_04A50938
            bErr_04A50938=bTrsErr
            this.Try_04A50938()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.oParentObject.w_ResoMode = "SOLOMESS"
              this.oParentObject.w_ResoTipo = "E"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile scrivere la partita nella tabella PAR_TITE %1%0serial %2 numero registrazione %3 riga %4" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
              this.oParentObject.Pag4()
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04A50938
            * --- End
          case (.not. empty(w_PNCODPAG)) .and. w_PNFLPART = "C" AND g_PERPAR="S"
            * --- Caso B : Generazione automatica partite se il tipo e' CREA
            * --- Vettore contenente i Dati Delle Scadenze
            DIMENSION w_ARRSCA[999,6]
            w_PTTOTSAL = 0
            w_TOTIVA = w_EXIVARAT
            * --- Sommatoria importi iva delle righe di PNT_IVA
            * --- ?????? w_TOTIVA vale sempre 0 ?????????
            w_IVA = IIF(w_CALDOC="E", w_TOTIVA, 0)
            w_NETTO = ABS(w_PNIMPDAR-w_PNIMPAVE)
            w_DATINI = IIF(EMPTY(w_PNDATDOC), w_PNDATREG, w_PNDATDOC)
            w_ESCL1 = STR(w_MESE1, 2, 0) + STR(w_GIORN1, 2, 0) + STR(w_GIOFIS, 2, 0)
            w_ESCL2 = STR(w_MESE2, 2, 0) + STR(w_GIORN2, 2, 0)
            if w_PNCODVAL<>w_PNVALNAZ AND w_PNCAOVAL<>0
              * --- Partite in Valuta (se Esiste il Documento ed e' riferito al Cli/For di Riga prende Quello...
              if w_PNTOTDOC<>0 .and. w_PNTIPCON=w_PNTIPCLF .and. w_PNCODCON=w_PNCODCLF
                w_NETTO = ABS(w_PNTOTDOC)
              else
                w_NETTO = cp_ROUND(MON2VAL(w_NETTO, w_PNCAOVAL, w_CAONAZ, w_DATINI, w_PNVALNAZ), this.w_DECTOT)
              endif
            endif
            w_NETTO = w_NETTO - w_IVA
            w_NURATE = SCADENZE("w_ARRSCA", w_PNCODPAG, w_DATINI, w_NETTO, w_IVA, 0, w_ESCL1, w_ESCL2, this.w_DECTOT)
            * --- Cicla Sulle Rate e scrive le Partite
            FOR L_i = 1 to w_NURATE
            w_PTDATSCA = w_ARRSCA[L_i, 1]
            w_PTNUMPAR = CANUMPAR("N", w_PNCOMPET, w_PNNUMDOC, w_PNALFDOC)
            w_PT_SEGNO = IIF(w_PNIMPAVE>0 OR w_PNIMPDAR<0 , "A", "D")
            w_PTTOTIMP = ABS(w_ARRSCA[L_i, 2] +w_ARRSCA[L_i, 3])
            w_PTMODPAG = w_ARRSCA[L_i, 5]
            * --- Banca di Appoggio Cli/For
            w_PTBANAPP = IIF( w_PNTIPCON $ "CF", w_BANAPP, SPACE(10) )
            w_PTBANNOS = IIF( w_PNTIPCON $ "CF", w_BANNOS, SPACE(10) )
            w_PTNUMCOR=IIF(this.w_TIPPAG $ "BO-RI",w_NUMCOR, SPACE(25))
            * --- Aggiornamento file partite
            * --- Try
            local bErr_04A5DA48
            bErr_04A5DA48=bTrsErr
            this.Try_04A5DA48()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.oParentObject.w_ResoMode = "SOLOMESS"
              this.oParentObject.w_ResoTipo = "E"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile scrivere la partita nella tabella PAR_TITE %1%0serial %2 numero registrazione %3 riga %4" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
              this.oParentObject.Pag4()
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04A5DA48
            * --- End
            ENDFOR
        endcase
      endif
      * --- Saldi
      if this.oParentObject.w_IMAGGSAL="S" .and. (.Not. this.w_NOPNT)
        * --- Try
        local bErr_04A41488
        bErr_04A41488=bTrsErr
        this.Try_04A41488()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04A41488
        * --- End
        * --- Se importi negativi di pnimpdar e pnimpave cambio il segno e inserisco l'importo pnimpave in pnimpdar e viceversa
        if w_PNIMPDAR>0 OR w_PNIMPAVE>0
          * --- Try
          local bErr_04A425C8
          bErr_04A425C8=bTrsErr
          this.Try_04A425C8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            if type("i_trsmsg")="C"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICON%0%1%0Seriale %2 numero registrazione %3 riga %4" , i_trsmsg , trim(this.w_PNSERIAL) , str(w_PNNUMRER,6,0) , str(this.oParentObject.ULT_RNUM,4,0) )
            else
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICON%0Seriale %1 numero registrazione %2 riga %3" , trim(this.w_PNSERIAL),str(w_PNNUMRER,6,0) , str(this.oParentObject.ULT_RNUM,4,0) )
            endif
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04A425C8
          * --- End
        else
          * --- Try
          local bErr_04A46A68
          bErr_04A46A68=bTrsErr
          this.Try_04A46A68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.oParentObject.w_ResoMode = "SOLOMESS"
            this.oParentObject.w_ResoTipo = "E"
            if type("i_trsmsg")="C"
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICON%0%1%0Seriale %2 numero registrazione %3 riga %4" , i_trsmsg , trim(this.w_PNSERIAL) , str(w_PNNUMRER,6,0) , str(this.oParentObject.ULT_RNUM,4,0) )
            else
              this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare i saldi nella tabella SALDICON%0Seriale %1 numero registrazione %2 riga %3" , trim(this.w_PNSERIAL),str(w_PNNUMRER,6,0) , str(this.oParentObject.ULT_RNUM,4,0) )
            endif
            this.oParentObject.Pag4()
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04A46A68
          * --- End
        endif
      endif
    endif
    * --- Analitica
    if this.oParentObject.w_IMAGGANA="S" .and. (.not. empty(w_EXCODVOC)) .and. (.not. empty(w_EXCODCEN)) .and. (.Not. this.w_NOPNT) .and. Nvl(this.w_CCTAGG," ")<>"E"
      * --- Inserimento record analitica
      w_EX_SEGNO = iif( empty(w_EX_SEGNO) , iif( w_PNIMPAVE<>0 ,"A" ,"D" ) , w_EX_SEGNO )
      * --- Try
      local bErr_04A25B00
      bErr_04A25B00=bTrsErr
      this.Try_04A25B00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.oParentObject.w_ResoMode = "SOLOMESS"
        this.oParentObject.w_ResoTipo = "E"
        this.oParentObject.w_ResoMess = AH_MSGFORMAT( "Impossibile aggiornare l'analitica nella tabella MOVICOST %1%0serial %2 numero registrazione %3 riga %4" , iif( type("i_trsmsg")="C" , chr(13)+i_trsmsg , "" ) , trim(this.w_PNSERIAL) , alltrim(str(w_PNNUMRER,6,0)) , alltrim(str(this.oParentObject.ULT_RNUM,4,0)) )
        this.oParentObject.Pag4()
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04A25B00
      * --- End
    endif
  endproc
  proc Try_04A653D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_IVA
    i_nConn=i_TableProp[this.PNT_IVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",IVCODCOI"+",IVCODCON"+",IVCODIVA"+",IVIMPIVA"+",IVIMPONI"+",IVNUMREG"+",IVPERIND"+",IVSERIAL"+",IVTIPCON"+",IVTIPREG"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PNT_IVA','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_IVCODCOI),'PNT_IVA','IVCODCOI');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODCON),'PNT_IVA','IVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCODIVA),'PNT_IVA','IVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(w_EXIMPIVA),'PNT_IVA','IVIMPIVA');
      +","+cp_NullLink(cp_ToStrODBC(w_EXIMPONI),'PNT_IVA','IVIMPONI');
      +","+cp_NullLink(cp_ToStrODBC(w_PNNUMREG),'PNT_IVA','IVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IVPERIND),'PNT_IVA','IVPERIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'PNT_IVA','IVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPREG),'PNT_IVA','IVTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(w_IVCFLOMA),'PNT_IVA','IVCFLOMA');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCONTRO),'PNT_IVA','IVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(w_EXTIPCOP),'PNT_IVA','IVTIPCOP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.oParentObject.ULT_RNUM,'IVCODCOI',w_IVCODCOI,'IVCODCON',w_PNCODCON,'IVCODIVA',w_EXCODIVA,'IVIMPIVA',w_EXIMPIVA,'IVIMPONI',w_EXIMPONI,'IVNUMREG',w_PNNUMREG,'IVPERIND',this.w_IVPERIND,'IVSERIAL',this.w_PNSERIAL,'IVTIPCON',w_PNTIPCON,'IVTIPREG',w_PNTIPREG,'IVFLOMAG',w_IVFLOMAG)
      insert into (i_cTable) (CPROWNUM,IVCODCOI,IVCODCON,IVCODIVA,IVIMPIVA,IVIMPONI,IVNUMREG,IVPERIND,IVSERIAL,IVTIPCON,IVTIPREG,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
         values (;
           this.oParentObject.ULT_RNUM;
           ,w_IVCODCOI;
           ,w_PNCODCON;
           ,w_EXCODIVA;
           ,w_EXIMPIVA;
           ,w_EXIMPONI;
           ,w_PNNUMREG;
           ,this.w_IVPERIND;
           ,this.w_PNSERIAL;
           ,w_PNTIPCON;
           ,w_PNTIPREG;
           ,w_IVFLOMAG;
           ,w_IVCFLOMA;
           ,w_EXCONTRO;
           ,w_EXTIPCOP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A50938()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTBANAPP"+",PTFLCRSA"+",PTFLSOSP"+",PTTIPCON"+",PTCODCON"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTCODAGE"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(w_CPROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_EXDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(w_EXNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(w_EXFLDAVE),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(w_EXVALVAL),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(w_PNFLPART),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(iif(empty(w_PNDATDOC),w_PNDATREG,w_PNDATDOC)),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(w_PNDATREG),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.oParentObject.ULT_RNUM,'CPROWNUM',w_CPROWNUM,'PTDATSCA',w_EXDATSCA,'PTNUMPAR',w_EXNUMPAR,'PT_SEGNO',w_EXFLDAVE,'PTTOTIMP',w_EXVALVAL,'PTCODVAL',w_PNCODVAL,'PTCAOVAL',w_PNCAOVAL,'PTMODPAG',w_PTMODPAG,'PTBANAPP',w_PTBANAPP,'PTFLCRSA',w_PNFLPART)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTBANAPP,PTFLCRSA,PTFLSOSP,PTTIPCON,PTCODCON,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTCODAGE,PTDATREG,PTNUMCOR &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,w_CPROWNUM;
           ,w_EXDATSCA;
           ,w_EXNUMPAR;
           ,w_EXFLDAVE;
           ,w_EXVALVAL;
           ,w_PNCODVAL;
           ,w_PNCAOVAL;
           ,w_PTMODPAG;
           ,w_PTBANAPP;
           ,w_PNFLPART;
           ,w_PTFLSOSP;
           ,w_PNTIPCON;
           ,w_PNCODCON;
           ,w_PNCAOVAL;
           ,iif(empty(w_PNDATDOC),w_PNDATREG,w_PNDATDOC);
           ,w_PNNUMDOC;
           ,w_PNALFDOC;
           ,w_PNDATDOC;
           ,w_PNTOTDOC;
           ,w_PTBANNOS;
           ,w_PNCODAGE;
           ,w_PNDATREG;
           ,w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A5DA48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTBANAPP"+",PTFLSOSP"+",PTTIPCON"+",PTCODCON"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTBANNOS"+",PTCODAGE"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(L_i),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(w_PNFLPART),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(iif(empty(w_PNDATDOC),w_PNDATREG,w_PNDATDOC)),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(w_PNDATREG),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.oParentObject.ULT_RNUM,'CPROWNUM',L_i,'PTDATSCA',w_PTDATSCA,'PTNUMPAR',w_PTNUMPAR,'PT_SEGNO',w_PT_SEGNO,'PTTOTIMP',w_PTTOTIMP,'PTCODVAL',w_PNCODVAL,'PTCAOVAL',w_PNCAOVAL,'PTMODPAG',w_PTMODPAG,'PTFLCRSA',w_PNFLPART,'PTBANAPP',w_PTBANAPP)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTBANAPP,PTFLSOSP,PTTIPCON,PTCODCON,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTBANNOS,PTCODAGE,PTDATREG,PTNUMCOR &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,L_i;
           ,w_PTDATSCA;
           ,w_PTNUMPAR;
           ,w_PT_SEGNO;
           ,w_PTTOTIMP;
           ,w_PNCODVAL;
           ,w_PNCAOVAL;
           ,w_PTMODPAG;
           ,w_PNFLPART;
           ,w_PTBANAPP;
           ,w_PTFLSOSP;
           ,w_PNTIPCON;
           ,w_PNCODCON;
           ,w_PNCAOVAL;
           ,iif(empty(w_PNDATDOC),w_PNDATREG,w_PNDATDOC);
           ,w_PNNUMDOC;
           ,w_PNALFDOC;
           ,w_PNDATDOC;
           ,w_PNTOTDOC;
           ,w_PTBANNOS;
           ,w_PNCODAGE;
           ,w_PNDATREG;
           ,w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A41488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_PNCOMPET),'SALDICON','SLCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',w_PNTIPCON,'SLCODICE',w_PNCODCON,'SLCODESE',w_PNCOMPET)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE &i_ccchkf. );
         values (;
           w_PNTIPCON;
           ,w_PNCODCON;
           ,w_PNCOMPET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A425C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_cOp1=cp_SetTrsOp(w_PNFLSALD,'SLDARPER','w_PNIMPDAR',w_PNIMPDAR,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_PNFLSALD,'SLAVEPER','w_PNIMPAVE',w_PNIMPAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_PNFLSALI,'SLDARINI','w_PNIMPDAR',w_PNIMPDAR,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_PNFLSALI,'SLAVEINI','w_PNIMPAVE',w_PNIMPAVE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(w_PNFLSALF,'SLDARFIN','w_PNIMPAVE',w_PNIMPAVE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(w_PNFLSALF,'SLAVEFIN','w_PNIMPDAR',w_PNIMPDAR,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER ="+cp_NullLink(i_cOp1,'SALDICON','SLDARPER');
      +",SLAVEPER ="+cp_NullLink(i_cOp2,'SALDICON','SLAVEPER');
      +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(w_PNCOMPET);
             )
    else
      update (i_cTable) set;
          SLDARPER = &i_cOp1.;
          ,SLAVEPER = &i_cOp2.;
          ,SLDARINI = &i_cOp3.;
          ,SLAVEINI = &i_cOp4.;
          ,SLDARFIN = &i_cOp5.;
          ,SLAVEFIN = &i_cOp6.;
          &i_ccchkf. ;
       where;
          SLTIPCON = w_PNTIPCON;
          and SLCODICE = w_PNCODCON;
          and SLCODESE = w_PNCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04A46A68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_cOp1=cp_SetTrsOp(w_PNFLSALD,'SLDARPER','w_PNIMPAVE*(-1)',w_PNIMPAVE*(-1),'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_PNFLSALD,'SLAVEPER','w_PNIMPDAR*(-1)',w_PNIMPDAR*(-1),'update',i_nConn)
      i_cOp3=cp_SetTrsOp(w_PNFLSALI,'SLDARINI','w_PNIMPAVE*(-1)',w_PNIMPAVE*(-1),'update',i_nConn)
      i_cOp4=cp_SetTrsOp(w_PNFLSALI,'SLAVEINI','w_PNIMPDAR*(-1)',w_PNIMPDAR*(-1),'update',i_nConn)
      i_cOp5=cp_SetTrsOp(w_PNFLSALF,'SLDARFIN','w_PNIMPDAR*(-1)',w_PNIMPDAR*(-1),'update',i_nConn)
      i_cOp6=cp_SetTrsOp(w_PNFLSALF,'SLAVEFIN','w_PNIMPAVE*(-1)',w_PNIMPAVE*(-1),'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER ="+cp_NullLink(i_cOp1,'SALDICON','SLDARPER');
      +",SLAVEPER ="+cp_NullLink(i_cOp2,'SALDICON','SLAVEPER');
      +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(w_PNCOMPET);
             )
    else
      update (i_cTable) set;
          SLDARPER = &i_cOp1.;
          ,SLAVEPER = &i_cOp2.;
          ,SLDARINI = &i_cOp3.;
          ,SLAVEINI = &i_cOp4.;
          ,SLDARFIN = &i_cOp5.;
          ,SLAVEFIN = &i_cOp6.;
          &i_ccchkf. ;
       where;
          SLTIPCON = w_PNTIPCON;
          and SLCODICE = w_PNCODCON;
          and SLCODESE = w_PNCOMPET;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04A25B00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MRCODCOM"+",MR_SEGNO"+",MRTOTIMP"+",MRPARAME"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'MOVICOST','MRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUMAN),'MOVICOST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCODVOC),'MOVICOST','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCODCEN),'MOVICOST','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(w_EXCODCOM),'MOVICOST','MRCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(w_EX_SEGNO),'MOVICOST','MR_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(w_EXVALORE),'MOVICOST','MRTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(w_EXPARAME),'MOVICOST','MRPARAME');
      +","+cp_NullLink(cp_ToStrODBC(w_EXINICOM),'MOVICOST','MRINICOM');
      +","+cp_NullLink(cp_ToStrODBC(w_EXFINCOM),'MOVICOST','MRFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(w_EX__TIPO),'MOVICOST','MRFLRIPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.oParentObject.ULT_RNUM,'CPROWNUM',this.oParentObject.ULT_RNUMAN,'MRCODVOC',w_EXCODVOC,'MRCODICE',w_EXCODCEN,'MRCODCOM',w_EXCODCOM,'MR_SEGNO',w_EX_SEGNO,'MRTOTIMP',w_EXVALORE,'MRPARAME',w_EXPARAME,'MRINICOM',w_EXINICOM,'MRFINCOM',w_EXFINCOM,'MRFLRIPA',w_EX__TIPO)
      insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MRCODCOM,MR_SEGNO,MRTOTIMP,MRPARAME,MRINICOM,MRFINCOM,MRFLRIPA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,this.oParentObject.ULT_RNUMAN;
           ,w_EXCODVOC;
           ,w_EXCODCEN;
           ,w_EXCODCOM;
           ,w_EX_SEGNO;
           ,w_EXVALORE;
           ,w_EXPARAME;
           ,w_EXINICOM;
           ,w_EXFINCOM;
           ,w_EX__TIPO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento records analitica
    this.w_CMCOMPET = iif( empty( w_EXCODESE ), g_CODESE, w_EXCODESE)
    * --- Calcolo Serial
    if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
      this.w_CMCODICE = space(10)
      i_Conn=i_TableProp[this.CDC_MANU_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SECDC", "i_codazi,w_CMCODICE")
      this.oParentObject.ULT_SERI = this.w_CMCODICE
      this.oParentObject.ULT_RNUM = 1
      if w_EXNUMREG = 0 .or. this.oParentObject.w_IMAGGNUR = "S"
        this.w_CMNUMREG = 0
        cp_NextTableProg(this, i_Conn, "PRCDC", "i_codazi,w_CMCOMPET,w_CMNUMREG" )
      else
        this.w_CMNUMREG = w_EXNUMREG
      endif
      this.oParentObject.ULT_NREG = this.w_CMNUMREG
    else
      this.w_CMCODICE = this.oParentObject.ULT_SERI
      this.w_CMNUMREG = this.oParentObject.ULT_NREG
      this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
    endif
    * --- Lettura dati esercizio
    if empty( w_EXCODVAL )
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_CMCOMPET);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_CMCOMPET;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_EXCODVAL = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Inserimento record master
    this.w_RIFERIME = -1
    w_EX__TIPO = iif( empty(w_EX__TIPO) , "E" , w_EX__TIPO )
    w_EXDATREG = iif( empty(w_EXDATREG) , i_datsys , w_EXDATREG )
    w_EXCODESE = iif( empty(w_EXCODESE) , g_CODESE , w_EXCODESE )
    if this.oParentObject.ULT_RNUM = 1
      * --- Insert into CDC_MANU
      i_nConn=i_TableProp[this.CDC_MANU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CDC_MANU_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CMCODICE"+",CMCODSEC"+",CMFLGMOV"+",CMDESAGG"+",CMNUMREG"+",CMDATREG"+",CMVALNAZ"+",CMNUMDOC"+",CMALFDOC"+",CMDATDOC"+",CMCOMPET"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CMCODICE),'CDC_MANU','CMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIFERIME),'CDC_MANU','CMCODSEC');
        +","+cp_NullLink(cp_ToStrODBC(w_EX__TIPO),'CDC_MANU','CMFLGMOV');
        +","+cp_NullLink(cp_ToStrODBC(w_EXDESCRI),'CDC_MANU','CMDESAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CMNUMREG),'CDC_MANU','CMNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(w_EXDATREG),'CDC_MANU','CMDATREG');
        +","+cp_NullLink(cp_ToStrODBC(w_EXCODVAL),'CDC_MANU','CMVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(w_EXNUMDOC),'CDC_MANU','CMNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(w_EXALFDOC),'CDC_MANU','CMALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(w_EXDATDOC),'CDC_MANU','CMDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(w_EXCODESE),'CDC_MANU','CMCOMPET');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE,'CMCODSEC',this.w_RIFERIME,'CMFLGMOV',w_EX__TIPO,'CMDESAGG',w_EXDESCRI,'CMNUMREG',this.w_CMNUMREG,'CMDATREG',w_EXDATREG,'CMVALNAZ',w_EXCODVAL,'CMNUMDOC',w_EXNUMDOC,'CMALFDOC',w_EXALFDOC,'CMDATDOC',w_EXDATDOC,'CMCOMPET',w_EXCODESE)
        insert into (i_cTable) (CMCODICE,CMCODSEC,CMFLGMOV,CMDESAGG,CMNUMREG,CMDATREG,CMVALNAZ,CMNUMDOC,CMALFDOC,CMDATDOC,CMCOMPET &i_ccchkf. );
           values (;
             this.w_CMCODICE;
             ,this.w_RIFERIME;
             ,w_EX__TIPO;
             ,w_EXDESCRI;
             ,this.w_CMNUMREG;
             ,w_EXDATREG;
             ,w_EXCODVAL;
             ,w_EXNUMDOC;
             ,w_EXALFDOC;
             ,w_EXDATDOC;
             ,w_EXCODESE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Inserimento record detail
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CMCODICE),'MOVICOST','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFERIME),'MOVICOST','MRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'MOVICOST','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_CMCODICE,'MRROWORD',this.w_RIFERIME,'CPROWNUM',this.oParentObject.ULT_RNUM)
      insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM &i_ccchkf. );
         values (;
           this.w_CMCODICE;
           ,this.w_RIFERIME;
           ,this.oParentObject.ULT_RNUM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Memorizza la nuova chiave di rottura
    this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,21)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CDC_MANU'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='MOVICOST'
    this.cWorkTables[10]='PAG_2AME'
    this.cWorkTables[11]='PAR_TITE'
    this.cWorkTables[12]='PNT_DETT'
    this.cWorkTables[13]='PNT_IVA'
    this.cWorkTables[14]='PNT_MAST'
    this.cWorkTables[15]='SALDICON'
    this.cWorkTables[16]='SCA_VARI'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='VOCIIVA'
    this.cWorkTables[19]='VOC_COST'
    this.cWorkTables[20]='MASTRI'
    this.cWorkTables[21]='MOD_PAGA'
    return(this.OpenAllTables(21))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_gsim_bpn')
      use in _Curs_gsim_bpn
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
