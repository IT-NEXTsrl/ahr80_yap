* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_mtr                                                        *
*              Dettaglio                                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2014-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsim_mtr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsim_mtr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsim_mtr")
  return

* --- Class definition
define class tgsim_mtr as StdPCForm
  Width  = 821
  Height = 431
  Top    = 53
  Left   = 5
  cComment = "Dettaglio"
  cPrg = "gsim_mtr"
  HelpContextID=80330601
  add object cnt as tcgsim_mtr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsim_mtr as PCContext
  w_TABELLA = space(30)
  w_TABEDET = space(30)
  w_XLNK_TIPSRC = space(1)
  w_XLNK_ASCII = space(20)
  w_TRCODICE = space(20)
  w_TR_ASCII = space(200)
  w_TRDESTIN = space(2)
  w_DESARC = space(20)
  w_XLNK_DESTIN = space(20)
  w_TRCAMNUM = 0
  w_TRTIPSRG = space(1)
  w_TRCAMSRC = space(30)
  w_TRCAMDES = space(30)
  w_TREXPSRG = space(10)
  w_TRCAMLUN = 0
  w_TRCAMDEC = 0
  w_TRTIPDES = space(1)
  w_TRCAMDST = space(15)
  w_TRCAMDST = space(15)
  w_TRCAMDST = space(15)
  w_TRCAMTIP = space(1)
  w_TRCAMOBB = space(1)
  w_TRTRASCO = space(1)
  w_TRANNOTA = space(10)
  w_TRTIPSRC = space(1)
  w_TRCAMDLE = 0
  w_TRCAMDDE = 0
  w_TRCAMDST = space(15)
  w_DESSOR = space(30)
  w_FLCOMMEN = space(254)
  w_FLCOMMEND = space(254)
  proc Save(i_oFrom)
    this.w_TABELLA = i_oFrom.w_TABELLA
    this.w_TABEDET = i_oFrom.w_TABEDET
    this.w_XLNK_TIPSRC = i_oFrom.w_XLNK_TIPSRC
    this.w_XLNK_ASCII = i_oFrom.w_XLNK_ASCII
    this.w_TRCODICE = i_oFrom.w_TRCODICE
    this.w_TR_ASCII = i_oFrom.w_TR_ASCII
    this.w_TRDESTIN = i_oFrom.w_TRDESTIN
    this.w_DESARC = i_oFrom.w_DESARC
    this.w_XLNK_DESTIN = i_oFrom.w_XLNK_DESTIN
    this.w_TRCAMNUM = i_oFrom.w_TRCAMNUM
    this.w_TRTIPSRG = i_oFrom.w_TRTIPSRG
    this.w_TRCAMSRC = i_oFrom.w_TRCAMSRC
    this.w_TRCAMDES = i_oFrom.w_TRCAMDES
    this.w_TREXPSRG = i_oFrom.w_TREXPSRG
    this.w_TRCAMLUN = i_oFrom.w_TRCAMLUN
    this.w_TRCAMDEC = i_oFrom.w_TRCAMDEC
    this.w_TRTIPDES = i_oFrom.w_TRTIPDES
    this.w_TRCAMDST = i_oFrom.w_TRCAMDST
    this.w_TRCAMDST = i_oFrom.w_TRCAMDST
    this.w_TRCAMDST = i_oFrom.w_TRCAMDST
    this.w_TRCAMTIP = i_oFrom.w_TRCAMTIP
    this.w_TRCAMOBB = i_oFrom.w_TRCAMOBB
    this.w_TRTRASCO = i_oFrom.w_TRTRASCO
    this.w_TRANNOTA = i_oFrom.w_TRANNOTA
    this.w_TRTIPSRC = i_oFrom.w_TRTIPSRC
    this.w_TRCAMDLE = i_oFrom.w_TRCAMDLE
    this.w_TRCAMDDE = i_oFrom.w_TRCAMDDE
    this.w_TRCAMDST = i_oFrom.w_TRCAMDST
    this.w_DESSOR = i_oFrom.w_DESSOR
    this.w_FLCOMMEN = i_oFrom.w_FLCOMMEN
    this.w_FLCOMMEND = i_oFrom.w_FLCOMMEND
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TABELLA = this.w_TABELLA
    i_oTo.w_TABEDET = this.w_TABEDET
    i_oTo.w_XLNK_TIPSRC = this.w_XLNK_TIPSRC
    i_oTo.w_XLNK_ASCII = this.w_XLNK_ASCII
    i_oTo.w_TRCODICE = this.w_TRCODICE
    i_oTo.w_TR_ASCII = this.w_TR_ASCII
    i_oTo.w_TRDESTIN = this.w_TRDESTIN
    i_oTo.w_DESARC = this.w_DESARC
    i_oTo.w_XLNK_DESTIN = this.w_XLNK_DESTIN
    i_oTo.w_TRCAMNUM = this.w_TRCAMNUM
    i_oTo.w_TRTIPSRG = this.w_TRTIPSRG
    i_oTo.w_TRCAMSRC = this.w_TRCAMSRC
    i_oTo.w_TRCAMDES = this.w_TRCAMDES
    i_oTo.w_TREXPSRG = this.w_TREXPSRG
    i_oTo.w_TRCAMLUN = this.w_TRCAMLUN
    i_oTo.w_TRCAMDEC = this.w_TRCAMDEC
    i_oTo.w_TRTIPDES = this.w_TRTIPDES
    i_oTo.w_TRCAMDST = this.w_TRCAMDST
    i_oTo.w_TRCAMDST = this.w_TRCAMDST
    i_oTo.w_TRCAMDST = this.w_TRCAMDST
    i_oTo.w_TRCAMTIP = this.w_TRCAMTIP
    i_oTo.w_TRCAMOBB = this.w_TRCAMOBB
    i_oTo.w_TRTRASCO = this.w_TRTRASCO
    i_oTo.w_TRANNOTA = this.w_TRANNOTA
    i_oTo.w_TRTIPSRC = this.w_TRTIPSRC
    i_oTo.w_TRCAMDLE = this.w_TRCAMDLE
    i_oTo.w_TRCAMDDE = this.w_TRCAMDDE
    i_oTo.w_TRCAMDST = this.w_TRCAMDST
    i_oTo.w_DESSOR = this.w_DESSOR
    i_oTo.w_FLCOMMEN = this.w_FLCOMMEN
    i_oTo.w_FLCOMMEND = this.w_FLCOMMEND
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsim_mtr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 821
  Height = 431
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-24"
  HelpContextID=80330601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TRACCIAT_IDX = 0
  IMPOARCH_IDX = 0
  IMPORTAZ_IDX = 0
  SRC_ODBC_IDX = 0
  XDC_FIELDS_IDX = 0
  XDC_TABLE_IDX = 0
  cFile = "TRACCIAT"
  cKeySelect = "TRCODICE,TR_ASCII,TRDESTIN,TRTIPSRC"
  cKeyWhere  = "TRCODICE=this.w_TRCODICE and TR_ASCII=this.w_TR_ASCII and TRDESTIN=this.w_TRDESTIN and TRTIPSRC=this.w_TRTIPSRC"
  cKeyDetail  = "TRCODICE=this.w_TRCODICE and TR_ASCII=this.w_TR_ASCII and TRDESTIN=this.w_TRDESTIN and TRCAMNUM=this.w_TRCAMNUM and TRTIPSRC=this.w_TRTIPSRC"
  cKeyWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';
      +'+" and TR_ASCII="+cp_ToStrODBC(this.w_TR_ASCII)';
      +'+" and TRDESTIN="+cp_ToStrODBC(this.w_TRDESTIN)';
      +'+" and TRTIPSRC="+cp_ToStrODBC(this.w_TRTIPSRC)';

  cKeyDetailWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';
      +'+" and TR_ASCII="+cp_ToStrODBC(this.w_TR_ASCII)';
      +'+" and TRDESTIN="+cp_ToStrODBC(this.w_TRDESTIN)';
      +'+" and TRCAMNUM="+cp_ToStrODBC(this.w_TRCAMNUM)';
      +'+" and TRTIPSRC="+cp_ToStrODBC(this.w_TRTIPSRC)';

  cKeyWhereODBCqualified = '"TRACCIAT.TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';
      +'+" and TRACCIAT.TR_ASCII="+cp_ToStrODBC(this.w_TR_ASCII)';
      +'+" and TRACCIAT.TRDESTIN="+cp_ToStrODBC(this.w_TRDESTIN)';
      +'+" and TRACCIAT.TRTIPSRC="+cp_ToStrODBC(this.w_TRTIPSRC)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsim_mtr"
  cComment = "Dettaglio"
  i_nRowNum = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TABELLA = space(30)
  w_TABEDET = space(30)
  w_XLNK_TIPSRC = space(1)
  w_XLNK_ASCII = space(20)
  w_TRCODICE = space(20)
  w_TR_ASCII = space(200)
  w_TRDESTIN = space(2)
  w_DESARC = space(20)
  w_XLNK_DESTIN = space(20)
  w_TRCAMNUM = 0
  w_TRTIPSRG = space(1)
  w_TRCAMSRC = space(30)
  o_TRCAMSRC = space(30)
  w_TRCAMDES = space(30)
  w_TREXPSRG = space(0)
  w_TRCAMLUN = 0
  w_TRCAMDEC = 0
  w_TRTIPDES = space(1)
  o_TRTIPDES = space(1)
  w_TRCAMDST = space(15)
  o_TRCAMDST = space(15)
  w_TRCAMDST = space(15)
  w_TRCAMDST = space(15)
  w_TRCAMTIP = space(1)
  w_TRCAMOBB = space(1)
  w_TRTRASCO = space(1)
  w_TRANNOTA = space(0)
  w_TRTIPSRC = space(1)
  w_TRCAMDLE = 0
  w_TRCAMDDE = 0
  w_TRCAMDST = space(15)
  w_DESSOR = space(30)
  w_FLCOMMEN = space(254)
  w_FLCOMMEND = space(254)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_mtrPag1","gsim_mtr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='IMPOARCH'
    this.cWorkTables[2]='IMPORTAZ'
    this.cWorkTables[3]='SRC_ODBC'
    this.cWorkTables[4]='XDC_FIELDS'
    this.cWorkTables[5]='XDC_TABLE'
    this.cWorkTables[6]='TRACCIAT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TRACCIAT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TRACCIAT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsim_mtr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TRACCIAT where TRCODICE=KeySet.TRCODICE
    *                            and TR_ASCII=KeySet.TR_ASCII
    *                            and TRDESTIN=KeySet.TRDESTIN
    *                            and TRCAMNUM=KeySet.TRCAMNUM
    *                            and TRTIPSRC=KeySet.TRTIPSRC
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsim_mtr
      * --- Setta Ordine per Anno e Periodo
      i_cOrder = 'order by TRCAMNUM Asc'
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2],this.bLoadRecFilter,this.TRACCIAT_IDX,"gsim_mtr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TRACCIAT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TRACCIAT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TRACCIAT '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  ,'TR_ASCII',this.w_TR_ASCII  ,'TRDESTIN',this.w_TRDESTIN  ,'TRTIPSRC',this.w_TRTIPSRC  )
      select * from (i_cTable) TRACCIAT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TABELLA = space(30)
        .w_TABEDET = space(30)
        .w_XLNK_TIPSRC = this.oParentObject.w_IMTIPSRC
        .w_XLNK_ASCII = this.oParentObject.w_IM_ASCII
        .w_DESARC = space(20)
        .w_XLNK_DESTIN = this.oParentObject.w_IMDESTIN
        .w_TRCODICE = NVL(TRCODICE,space(20))
        .w_TR_ASCII = NVL(TR_ASCII,space(200))
        .w_TRDESTIN = NVL(TRDESTIN,space(2))
          if link_1_7_joined
            this.w_TRDESTIN = NVL(ARCODICE107,NVL(this.w_TRDESTIN,space(2)))
            this.w_DESARC = NVL(ARDESCRI107,space(20))
            this.w_TABELLA = NVL(ARTABELL107,space(30))
            this.w_TABEDET = NVL(ARTABDET107,space(30))
          else
          .link_1_7('Load')
          endif
          .link_1_9('Load')
        .w_TRTIPSRC = NVL(TRTIPSRC,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TRACCIAT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESSOR = space(30)
          .w_FLCOMMEN = space(254)
          .w_FLCOMMEND = space(254)
          .w_TRCAMNUM = NVL(TRCAMNUM,0)
          .w_TRTIPSRG = NVL(TRTIPSRG,space(1))
          .w_TRCAMSRC = NVL(TRCAMSRC,space(30))
          .link_2_3('Load')
          .w_TRCAMDES = NVL(TRCAMDES,space(30))
          .w_TREXPSRG = NVL(TREXPSRG,space(0))
          .w_TRCAMLUN = NVL(TRCAMLUN,0)
          .w_TRCAMDEC = NVL(TRCAMDEC,0)
          .w_TRTIPDES = NVL(TRTIPDES,space(1))
          .w_TRCAMDST = NVL(TRCAMDST,space(15))
          .w_TRCAMDST = NVL(TRCAMDST,space(15))
          .link_2_10('Load')
          .w_TRCAMDST = NVL(TRCAMDST,space(15))
          .link_2_11('Load')
          .w_TRCAMTIP = NVL(TRCAMTIP,space(1))
          .w_TRCAMOBB = NVL(TRCAMOBB,space(1))
          .w_TRTRASCO = NVL(TRTRASCO,space(1))
          .w_TRANNOTA = NVL(TRANNOTA,space(0))
          .w_TRCAMDLE = NVL(TRCAMDLE,0)
          .w_TRCAMDDE = NVL(TRCAMDDE,0)
          .w_TRCAMDST = NVL(TRCAMDST,space(15))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace TRCAMNUM with .w_TRCAMNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_TABELLA=space(30)
      .w_TABEDET=space(30)
      .w_XLNK_TIPSRC=space(1)
      .w_XLNK_ASCII=space(20)
      .w_TRCODICE=space(20)
      .w_TR_ASCII=space(200)
      .w_TRDESTIN=space(2)
      .w_DESARC=space(20)
      .w_XLNK_DESTIN=space(20)
      .w_TRCAMNUM=0
      .w_TRTIPSRG=space(1)
      .w_TRCAMSRC=space(30)
      .w_TRCAMDES=space(30)
      .w_TREXPSRG=space(0)
      .w_TRCAMLUN=0
      .w_TRCAMDEC=0
      .w_TRTIPDES=space(1)
      .w_TRCAMDST=space(15)
      .w_TRCAMDST=space(15)
      .w_TRCAMDST=space(15)
      .w_TRCAMTIP=space(1)
      .w_TRCAMOBB=space(1)
      .w_TRTRASCO=space(1)
      .w_TRANNOTA=space(0)
      .w_TRTIPSRC=space(1)
      .w_TRCAMDLE=0
      .w_TRCAMDDE=0
      .w_TRCAMDST=space(15)
      .w_DESSOR=space(30)
      .w_FLCOMMEN=space(254)
      .w_FLCOMMEND=space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_XLNK_TIPSRC = this.oParentObject.w_IMTIPSRC
        .w_XLNK_ASCII = this.oParentObject.w_IM_ASCII
        .DoRTCalc(5,7,.f.)
        if not(empty(.w_TRDESTIN))
         .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_XLNK_DESTIN = this.oParentObject.w_IMDESTIN
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_XLNK_DESTIN))
         .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        .w_TRTIPSRG = 'C'
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_TRCAMSRC))
         .link_2_3('Full')
        endif
        .w_TRCAMDES = iif(EMPTY(NVL(.w_TRCAMDES,'')),iif(empty(nvl(.w_DESSOR,'')),iif(.w_TRTIPDES='D',SUBSTR(.w_FLCOMMEND,1,30),SUBSTR(.w_FLCOMMEN,1,30)),.w_DESSOR),.w_TRCAMDES)
        .DoRTCalc(14,16,.f.)
        .w_TRTIPDES = 'C'
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_TRCAMDST))
         .link_2_10('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_TRCAMDST))
         .link_2_11('Full')
        endif
        .w_TRCAMTIP = "C"
        .w_TRCAMOBB = 'N'
        .w_TRTRASCO = iif(.w_TRTIPDES # 'V',.w_TRTRASCO,'N')
        .DoRTCalc(24,27,.f.)
        .w_TRCAMDST = alltrim(.w_TRCAMDST)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TRACCIAT')
    this.DoRTCalc(29,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTR_ASCII_1_6.enabled = !i_bVal
      .Page1.oPag.oTRDESTIN_1_7.enabled = !i_bVal
      .Page1.oPag.oTREXPSRG_2_5.enabled = i_bVal
      .Page1.oPag.oTRANNOTA_2_15.enabled = i_bVal
      .Page1.oPag.oTRCAMDLE_2_16.enabled = i_bVal
      .Page1.oPag.oTRCAMDDE_2_17.enabled = i_bVal
      .Page1.oPag.oBtn_2_18.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TRACCIAT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODICE,"TRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TR_ASCII,"TR_ASCII",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESTIN,"TRDESTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPSRC,"TRTIPSRC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TRCAMNUM N(5);
      ,t_TRTIPSRG N(3);
      ,t_TRCAMSRC C(30);
      ,t_TRCAMDES C(30);
      ,t_TREXPSRG M(10);
      ,t_TRCAMLUN N(5);
      ,t_TRCAMDEC N(2);
      ,t_TRTIPDES N(3);
      ,t_TRCAMDST C(15);
      ,t_TRCAMTIP N(3);
      ,t_TRCAMOBB N(3);
      ,t_TRTRASCO N(3);
      ,t_TRANNOTA M(10);
      ,t_TRCAMDLE N(5);
      ,t_TRCAMDDE N(2);
      ,TRCAMNUM N(5);
      ,t_DESSOR C(30);
      ,t_FLCOMMEN C(254);
      ,t_FLCOMMEND C(254);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsim_mtrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMNUM_2_1.controlsource=this.cTrsName+'.t_TRCAMNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.controlsource=this.cTrsName+'.t_TRTIPSRG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.controlsource=this.cTrsName+'.t_TRCAMSRC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDES_2_4.controlsource=this.cTrsName+'.t_TRCAMDES'
    this.oPgFRm.Page1.oPag.oTREXPSRG_2_5.controlsource=this.cTrsName+'.t_TREXPSRG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMLUN_2_6.controlsource=this.cTrsName+'.t_TRCAMLUN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDEC_2_7.controlsource=this.cTrsName+'.t_TRCAMDEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.controlsource=this.cTrsName+'.t_TRTIPDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.controlsource=this.cTrsName+'.t_TRCAMDST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.controlsource=this.cTrsName+'.t_TRCAMDST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.controlsource=this.cTrsName+'.t_TRCAMDST'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.controlsource=this.cTrsName+'.t_TRCAMTIP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.controlsource=this.cTrsName+'.t_TRCAMOBB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.controlsource=this.cTrsName+'.t_TRTRASCO'
    this.oPgFRm.Page1.oPag.oTRANNOTA_2_15.controlsource=this.cTrsName+'.t_TRANNOTA'
    this.oPgFRm.Page1.oPag.oTRCAMDLE_2_16.controlsource=this.cTrsName+'.t_TRCAMDLE'
    this.oPgFRm.Page1.oPag.oTRCAMDDE_2_17.controlsource=this.cTrsName+'.t_TRCAMDDE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(69)
    this.AddVLine(164)
    this.AddVLine(358)
    this.AddVLine(396)
    this.AddVLine(424)
    this.AddVLine(500)
    this.AddVLine(596)
    this.AddVLine(699)
    this.AddVLine(747)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMNUM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2])
      *
      * insert into TRACCIAT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TRACCIAT')
        i_extval=cp_InsertValODBCExtFlds(this,'TRACCIAT')
        i_cFldBody=" "+;
                  "(TRCODICE,TR_ASCII,TRDESTIN,TRCAMNUM,TRTIPSRG"+;
                  ",TRCAMSRC,TRCAMDES,TREXPSRG,TRCAMLUN,TRCAMDEC"+;
                  ",TRTIPDES,TRCAMDST,TRCAMTIP,TRCAMOBB,TRTRASCO"+;
                  ",TRANNOTA,TRTIPSRC,TRCAMDLE,TRCAMDDE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TRCODICE)+","+cp_ToStrODBC(this.w_TR_ASCII)+","+cp_ToStrODBCNull(this.w_TRDESTIN)+","+cp_ToStrODBC(this.w_TRCAMNUM)+","+cp_ToStrODBC(this.w_TRTIPSRG)+;
             ","+cp_ToStrODBCNull(this.w_TRCAMSRC)+","+cp_ToStrODBC(this.w_TRCAMDES)+","+cp_ToStrODBC(this.w_TREXPSRG)+","+cp_ToStrODBC(this.w_TRCAMLUN)+","+cp_ToStrODBC(this.w_TRCAMDEC)+;
             ","+cp_ToStrODBC(this.w_TRTIPDES)+","+cp_ToStrODBC(this.w_TRCAMDST)+","+cp_ToStrODBC(this.w_TRCAMTIP)+","+cp_ToStrODBC(this.w_TRCAMOBB)+","+cp_ToStrODBC(this.w_TRTRASCO)+;
             ","+cp_ToStrODBC(this.w_TRANNOTA)+","+cp_ToStrODBC(this.w_TRTIPSRC)+","+cp_ToStrODBC(this.w_TRCAMDLE)+","+cp_ToStrODBC(this.w_TRCAMDDE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TRACCIAT')
        i_extval=cp_InsertValVFPExtFlds(this,'TRACCIAT')
        cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE,'TR_ASCII',this.w_TR_ASCII,'TRDESTIN',this.w_TRDESTIN,'TRCAMNUM',this.w_TRCAMNUM,'TRTIPSRC',this.w_TRTIPSRC)
        INSERT INTO (i_cTable) (;
                   TRCODICE;
                  ,TR_ASCII;
                  ,TRDESTIN;
                  ,TRCAMNUM;
                  ,TRTIPSRG;
                  ,TRCAMSRC;
                  ,TRCAMDES;
                  ,TREXPSRG;
                  ,TRCAMLUN;
                  ,TRCAMDEC;
                  ,TRTIPDES;
                  ,TRCAMDST;
                  ,TRCAMTIP;
                  ,TRCAMOBB;
                  ,TRTRASCO;
                  ,TRANNOTA;
                  ,TRTIPSRC;
                  ,TRCAMDLE;
                  ,TRCAMDDE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TRCODICE;
                  ,this.w_TR_ASCII;
                  ,this.w_TRDESTIN;
                  ,this.w_TRCAMNUM;
                  ,this.w_TRTIPSRG;
                  ,this.w_TRCAMSRC;
                  ,this.w_TRCAMDES;
                  ,this.w_TREXPSRG;
                  ,this.w_TRCAMLUN;
                  ,this.w_TRCAMDEC;
                  ,this.w_TRTIPDES;
                  ,this.w_TRCAMDST;
                  ,this.w_TRCAMTIP;
                  ,this.w_TRCAMOBB;
                  ,this.w_TRTRASCO;
                  ,this.w_TRANNOTA;
                  ,this.w_TRTIPSRC;
                  ,this.w_TRCAMDLE;
                  ,this.w_TRCAMDDE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for ((t_TRCAMNUM<>0) and !(iif(type('t_TRTIPDES')='C',t_TRTIPDES='C',t_TRTIPDES=1) and empty(NVL(t_TRCAMSRC,""))  and  empty(NVL(t_TRCAMDST,"")) )) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TRACCIAT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and TRCAMNUM="+cp_ToStrODBC(&i_TN.->TRCAMNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TRACCIAT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and TRCAMNUM=&i_TN.->TRCAMNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for ((t_TRCAMNUM<>0) and !(iif(type('t_TRTIPDES')='C',t_TRTIPDES='C',t_TRTIPDES=1) and empty(NVL(t_TRCAMSRC,""))  and  empty(NVL(t_TRCAMDST,"")) )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and TRCAMNUM="+cp_ToStrODBC(&i_TN.->TRCAMNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and TRCAMNUM=&i_TN.->TRCAMNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace TRCAMNUM with this.w_TRCAMNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TRACCIAT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TRACCIAT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TRTIPSRG="+cp_ToStrODBC(this.w_TRTIPSRG)+;
                     ",TRCAMSRC="+cp_ToStrODBCNull(this.w_TRCAMSRC)+;
                     ",TRCAMDES="+cp_ToStrODBC(this.w_TRCAMDES)+;
                     ",TREXPSRG="+cp_ToStrODBC(this.w_TREXPSRG)+;
                     ",TRCAMLUN="+cp_ToStrODBC(this.w_TRCAMLUN)+;
                     ",TRCAMDEC="+cp_ToStrODBC(this.w_TRCAMDEC)+;
                     ",TRTIPDES="+cp_ToStrODBC(this.w_TRTIPDES)+;
                     ",TRCAMDST="+cp_ToStrODBC(this.w_TRCAMDST)+;
                     ",TRCAMTIP="+cp_ToStrODBC(this.w_TRCAMTIP)+;
                     ",TRCAMOBB="+cp_ToStrODBC(this.w_TRCAMOBB)+;
                     ",TRTRASCO="+cp_ToStrODBC(this.w_TRTRASCO)+;
                     ",TRANNOTA="+cp_ToStrODBC(this.w_TRANNOTA)+;
                     ",TRCAMDLE="+cp_ToStrODBC(this.w_TRCAMDLE)+;
                     ",TRCAMDDE="+cp_ToStrODBC(this.w_TRCAMDDE)+;
                     ",TRCAMNUM="+cp_ToStrODBC(this.w_TRCAMNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and TRCAMNUM="+cp_ToStrODBC(TRCAMNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TRACCIAT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TRTIPSRG=this.w_TRTIPSRG;
                     ,TRCAMSRC=this.w_TRCAMSRC;
                     ,TRCAMDES=this.w_TRCAMDES;
                     ,TREXPSRG=this.w_TREXPSRG;
                     ,TRCAMLUN=this.w_TRCAMLUN;
                     ,TRCAMDEC=this.w_TRCAMDEC;
                     ,TRTIPDES=this.w_TRTIPDES;
                     ,TRCAMDST=this.w_TRCAMDST;
                     ,TRCAMTIP=this.w_TRCAMTIP;
                     ,TRCAMOBB=this.w_TRCAMOBB;
                     ,TRTRASCO=this.w_TRTRASCO;
                     ,TRANNOTA=this.w_TRANNOTA;
                     ,TRCAMDLE=this.w_TRCAMDLE;
                     ,TRCAMDDE=this.w_TRCAMDDE;
                     ,TRCAMNUM=this.w_TRCAMNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and TRCAMNUM=&i_TN.->TRCAMNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for ((t_TRCAMNUM<>0) and !(iif(type('t_TRTIPDES')='C',t_TRTIPDES='C',t_TRTIPDES=1) and empty(NVL(t_TRCAMSRC,""))  and  empty(NVL(t_TRCAMDST,"")) )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TRACCIAT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and TRCAMNUM="+cp_ToStrODBC(&i_TN.->TRCAMNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and TRCAMNUM=&i_TN.->TRCAMNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for ((t_TRCAMNUM<>0) and !(iif(type('t_TRTIPDES')='C',t_TRTIPDES='C',t_TRTIPDES=1) and empty(NVL(t_TRCAMSRC,""))  and  empty(NVL(t_TRCAMDST,"")) )) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TRACCIAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TRACCIAT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_1_7('Full')
        .DoRTCalc(8,8,.t.)
          .link_1_9('Full')
        .DoRTCalc(10,12,.t.)
        if .o_TRCAMSRC<>.w_TRCAMSRC.or. .o_TRCAMDST<>.w_TRCAMDST.or. .o_TRTIPDES<>.w_TRTIPDES
          .w_TRCAMDES = iif(EMPTY(NVL(.w_TRCAMDES,'')),iif(empty(nvl(.w_DESSOR,'')),iif(.w_TRTIPDES='D',SUBSTR(.w_FLCOMMEND,1,30),SUBSTR(.w_FLCOMMEN,1,30)),.w_DESSOR),.w_TRCAMDES)
        endif
        .DoRTCalc(14,22,.t.)
        if .o_TRTIPDES<>.w_TRTIPDES
          .w_TRTRASCO = iif(.w_TRTIPDES # 'V',.w_TRTRASCO,'N')
        endif
        .DoRTCalc(24,27,.t.)
          .w_TRCAMDST = alltrim(.w_TRCAMDST)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TRCAMDST with this.w_TRCAMDST
      replace t_DESSOR with this.w_DESSOR
      replace t_FLCOMMEN with this.w_FLCOMMEN
      replace t_FLCOMMEND with this.w_FLCOMMEND
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMSRC_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMSRC_2_3.mCond()
    this.oPgFrm.Page1.oPag.oTREXPSRG_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTREXPSRG_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMLUN_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMLUN_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDEC_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDEC_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMDST_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMTIP_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRCAMTIP_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRTRASCO_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTRTRASCO_2_14.mCond()
    this.oPgFrm.Page1.oPag.oTRCAMDLE_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRCAMDLE_2_16.mCond()
    this.oPgFrm.Page1.oPag.oTRCAMDDE_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRCAMDDE_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_18.enabled =this.oPgFrm.Page1.oPag.oBtn_2_18.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRDESTIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRDESTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRDESTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI,ARTABELL,ARTABDET";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_TRDESTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_TRDESTIN)
            select ARCODICE,ARDESCRI,ARTABELL,ARTABDET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRDESTIN = NVL(_Link_.ARCODICE,space(2))
      this.w_DESARC = NVL(_Link_.ARDESCRI,space(20))
      this.w_TABELLA = NVL(_Link_.ARTABELL,space(30))
      this.w_TABEDET = NVL(_Link_.ARTABDET,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRDESTIN = space(2)
      endif
      this.w_DESARC = space(20)
      this.w_TABELLA = space(30)
      this.w_TABEDET = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRDESTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMPOARCH_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.ARCODICE as ARCODICE107"+ ",link_1_7.ARDESCRI as ARDESCRI107"+ ",link_1_7.ARTABELL as ARTABELL107"+ ",link_1_7.ARTABDET as ARTABDET107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on TRACCIAT.TRDESTIN=link_1_7.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and TRACCIAT.TRDESTIN=link_1_7.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=XLNK_DESTIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPOARCH_IDX,3]
    i_lTable = "IMPOARCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2], .t., this.IMPOARCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_XLNK_DESTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_XLNK_DESTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARTABELL,ARDESCRI,ARTABDET";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_XLNK_DESTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_XLNK_DESTIN)
            select ARCODICE,ARTABELL,ARDESCRI,ARTABDET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_XLNK_DESTIN = NVL(_Link_.ARCODICE,space(20))
      this.w_TABELLA = NVL(_Link_.ARTABELL,space(30))
      this.w_DESARC = NVL(_Link_.ARDESCRI,space(20))
      this.w_TABEDET = NVL(_Link_.ARTABDET,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_XLNK_DESTIN = space(20)
      endif
      this.w_TABELLA = space(30)
      this.w_DESARC = space(20)
      this.w_TABEDET = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPOARCH_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPOARCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_XLNK_DESTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCAMSRC
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SRC_ODBC_IDX,3]
    i_lTable = "SRC_ODBC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2], .t., this.SRC_ODBC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAMSRC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSIM_MSO',True,'SRC_ODBC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_TRCAMSRC)+"%");
                   +" and SOCODICE="+cp_ToStrODBC(this.w_XLNK_ASCII);

          i_ret=cp_SQL(i_nConn,"select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SOCODICE,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SOCODICE',this.w_XLNK_ASCII;
                     ,'FLNAME',trim(this.w_TRCAMSRC))
          select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SOCODICE,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAMSRC)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAMSRC) and !this.bDontReportError
            deferred_cp_zoom('SRC_ODBC','*','SOCODICE,FLNAME',cp_AbsName(oSource.parent,'oTRCAMSRC_2_3'),i_cWhere,'GSIM_MSO',"Campo sorgente dati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_XLNK_ASCII<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SOCODICE="+cp_ToStrODBC(this.w_XLNK_ASCII);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAMSRC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_TRCAMSRC);
                   +" and SOCODICE="+cp_ToStrODBC(this.w_XLNK_ASCII);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SOCODICE',this.w_XLNK_ASCII;
                       ,'FLNAME',this.w_TRCAMSRC)
            select SOCODICE,FLNAME,FLCOMMEN,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAMSRC = NVL(_Link_.FLNAME,space(30))
      this.w_DESSOR = NVL(_Link_.FLCOMMEN,space(30))
      this.w_TRCAMLUN = NVL(_Link_.FLLENGHT,0)
      this.w_TRCAMDEC = NVL(_Link_.FLDECIMA,0)
    else
      if i_cCtrl<>'Load'
        this.w_TRCAMSRC = space(30)
      endif
      this.w_DESSOR = space(30)
      this.w_TRCAMLUN = 0
      this.w_TRCAMDEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SRC_ODBC_IDX,2])+'\'+cp_ToStr(_Link_.SOCODICE,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.SRC_ODBC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAMSRC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCAMDST
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAMDST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_TRCAMDST)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABEDET);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_TABEDET;
                     ,'FLNAME',trim(this.w_TRCAMDST))
          select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAMDST)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAMDST) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oTRCAMDST_2_10'),i_cWhere,'',"Campo destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TABEDET<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_TABEDET);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAMDST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_TRCAMDST);
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABEDET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABEDET;
                       ,'FLNAME',this.w_TRCAMDST)
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAMDST = NVL(_Link_.FLNAME,space(15))
      this.w_TRCAMTIP = NVL(_Link_.FLTYPE,space(1))
      this.w_TRCAMDLE = NVL(_Link_.FLLENGHT,0)
      this.w_TRCAMDDE = NVL(_Link_.FLDECIMA,0)
      this.w_FLCOMMEND = NVL(_Link_.FLCOMMEN,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAMDST = space(15)
      endif
      this.w_TRCAMTIP = space(1)
      this.w_TRCAMDLE = 0
      this.w_TRCAMDDE = 0
      this.w_FLCOMMEND = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAMDST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCAMDST
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCAMDST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_TRCAMDST)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_TABELLA;
                     ,'FLNAME',trim(this.w_TRCAMDST))
          select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCAMDST)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCAMDST) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oTRCAMDST_2_11'),i_cWhere,'',"Campo destinazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TABELLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCAMDST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_TRCAMDST);
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA;
                       ,'FLNAME',this.w_TRCAMDST)
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCAMDST = NVL(_Link_.FLNAME,space(15))
      this.w_TRCAMTIP = NVL(_Link_.FLTYPE,space(1))
      this.w_TRCAMDLE = NVL(_Link_.FLLENGHT,0)
      this.w_TRCAMDDE = NVL(_Link_.FLDECIMA,0)
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_TRCAMDST = space(15)
      endif
      this.w_TRCAMTIP = space(1)
      this.w_TRCAMDLE = 0
      this.w_TRCAMDDE = 0
      this.w_FLCOMMEN = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCAMDST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTR_ASCII_1_6.value==this.w_TR_ASCII)
      this.oPgFrm.Page1.oPag.oTR_ASCII_1_6.value=this.w_TR_ASCII
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESTIN_1_7.value==this.w_TRDESTIN)
      this.oPgFrm.Page1.oPag.oTRDESTIN_1_7.value=this.w_TRDESTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARC_1_8.value==this.w_DESARC)
      this.oPgFrm.Page1.oPag.oDESARC_1_8.value=this.w_DESARC
    endif
    if not(this.oPgFrm.Page1.oPag.oTREXPSRG_2_5.value==this.w_TREXPSRG)
      this.oPgFrm.Page1.oPag.oTREXPSRG_2_5.value=this.w_TREXPSRG
      replace t_TREXPSRG with this.oPgFrm.Page1.oPag.oTREXPSRG_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRANNOTA_2_15.value==this.w_TRANNOTA)
      this.oPgFrm.Page1.oPag.oTRANNOTA_2_15.value=this.w_TRANNOTA
      replace t_TRANNOTA with this.oPgFrm.Page1.oPag.oTRANNOTA_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAMDLE_2_16.value==this.w_TRCAMDLE)
      this.oPgFrm.Page1.oPag.oTRCAMDLE_2_16.value=this.w_TRCAMDLE
      replace t_TRCAMDLE with this.oPgFrm.Page1.oPag.oTRCAMDLE_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCAMDDE_2_17.value==this.w_TRCAMDDE)
      this.oPgFrm.Page1.oPag.oTRCAMDDE_2_17.value=this.w_TRCAMDDE
      replace t_TRCAMDDE with this.oPgFrm.Page1.oPag.oTRCAMDDE_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMNUM_2_1.value==this.w_TRCAMNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMNUM_2_1.value=this.w_TRCAMNUM
      replace t_TRCAMNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMNUM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.RadioValue()==this.w_TRTIPSRG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.SetRadio()
      replace t_TRTIPSRG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.value==this.w_TRCAMSRC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.value=this.w_TRCAMSRC
      replace t_TRCAMSRC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMSRC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDES_2_4.value==this.w_TRCAMDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDES_2_4.value=this.w_TRCAMDES
      replace t_TRCAMDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDES_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMLUN_2_6.value==this.w_TRCAMLUN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMLUN_2_6.value=this.w_TRCAMLUN
      replace t_TRCAMLUN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMLUN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDEC_2_7.value==this.w_TRCAMDEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDEC_2_7.value=this.w_TRCAMDEC
      replace t_TRCAMDEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDEC_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.RadioValue()==this.w_TRTIPDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.SetRadio()
      replace t_TRTIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.value==this.w_TRCAMDST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.value=this.w_TRCAMDST
      replace t_TRCAMDST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.value==this.w_TRCAMDST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.value=this.w_TRCAMDST
      replace t_TRCAMDST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.value==this.w_TRCAMDST)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.value=this.w_TRCAMDST
      replace t_TRCAMDST with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMDST_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.RadioValue()==this.w_TRCAMTIP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.SetRadio()
      replace t_TRCAMTIP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.RadioValue()==this.w_TRCAMOBB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.SetRadio()
      replace t_TRCAMOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.RadioValue()==this.w_TRTRASCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.SetRadio()
      replace t_TRTRASCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'TRACCIAT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_TRCAMLUN) and (.w_XLNK_TIPSRC<>'O') and ((.w_TRCAMNUM<>0) and !(iif(type('.w_TRTIPDES')='C',.w_TRTIPDES='C',.w_TRTIPDES=1) and empty(NVL(.w_TRCAMSRC,""))  and  empty(NVL(.w_TRCAMDST,"")) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMLUN_2_6
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(iif(.w_TRTIPDES='D' and empty(.w_TABEDET),.F.,.T.)) and ((.w_TRCAMNUM<>0) and !(iif(type('.w_TRTIPDES')='C',.w_TRTIPDES='C',.w_TRTIPDES=1) and empty(NVL(.w_TRCAMSRC,""))  and  empty(NVL(.w_TRCAMDST,"")) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("L'archivio di destinazione non ha una tabella di dettaglio")
        case   empty(.w_TRCAMTIP) and (.w_TRTIPDES='V') and ((.w_TRCAMNUM<>0) and !(iif(type('.w_TRTIPDES')='C',.w_TRTIPDES='C',.w_TRTIPDES=1) and empty(NVL(.w_TRCAMSRC,""))  and  empty(NVL(.w_TRCAMDST,"")) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if (.w_TRCAMNUM<>0) and !(iif(type('.w_TRTIPDES')='C',.w_TRTIPDES='C',.w_TRTIPDES=1) and empty(NVL(.w_TRCAMSRC,""))  and  empty(NVL(.w_TRCAMDST,"")) )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRCAMSRC = this.w_TRCAMSRC
    this.o_TRTIPDES = this.w_TRTIPDES
    this.o_TRCAMDST = this.w_TRCAMDST
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=((t_TRCAMNUM<>0) and !(iif(type('t_TRTIPDES')='C',t_TRTIPDES='C',t_TRTIPDES=1) and empty(NVL(t_TRCAMSRC,""))  and  empty(NVL(t_TRCAMDST,"")) ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TRCAMNUM=0
      .w_TRTIPSRG=space(1)
      .w_TRCAMSRC=space(30)
      .w_TRCAMDES=space(30)
      .w_TREXPSRG=space(0)
      .w_TRCAMLUN=0
      .w_TRCAMDEC=0
      .w_TRTIPDES=space(1)
      .w_TRCAMDST=space(15)
      .w_TRCAMDST=space(15)
      .w_TRCAMDST=space(15)
      .w_TRCAMTIP=space(1)
      .w_TRCAMOBB=space(1)
      .w_TRTRASCO=space(1)
      .w_TRANNOTA=space(0)
      .w_TRCAMDLE=0
      .w_TRCAMDDE=0
      .w_TRCAMDST=space(15)
      .w_DESSOR=space(30)
      .w_FLCOMMEN=space(254)
      .w_FLCOMMEND=space(254)
      .DoRTCalc(1,10,.f.)
        .w_TRTIPSRG = 'C'
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_TRCAMSRC))
        .link_2_3('Full')
      endif
        .w_TRCAMDES = iif(EMPTY(NVL(.w_TRCAMDES,'')),iif(empty(nvl(.w_DESSOR,'')),iif(.w_TRTIPDES='D',SUBSTR(.w_FLCOMMEND,1,30),SUBSTR(.w_FLCOMMEN,1,30)),.w_DESSOR),.w_TRCAMDES)
      .DoRTCalc(14,16,.f.)
        .w_TRTIPDES = 'C'
      .DoRTCalc(18,19,.f.)
      if not(empty(.w_TRCAMDST))
        .link_2_10('Full')
      endif
      .DoRTCalc(20,20,.f.)
      if not(empty(.w_TRCAMDST))
        .link_2_11('Full')
      endif
        .w_TRCAMTIP = "C"
        .w_TRCAMOBB = 'N'
        .w_TRTRASCO = iif(.w_TRTIPDES # 'V',.w_TRTRASCO,'N')
      .DoRTCalc(24,27,.f.)
        .w_TRCAMDST = alltrim(.w_TRCAMDST)
    endwith
    this.DoRTCalc(29,31,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TRCAMNUM = t_TRCAMNUM
    this.w_TRTIPSRG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.RadioValue(.t.)
    this.w_TRCAMSRC = t_TRCAMSRC
    this.w_TRCAMDES = t_TRCAMDES
    this.w_TREXPSRG = t_TREXPSRG
    this.w_TRCAMLUN = t_TRCAMLUN
    this.w_TRCAMDEC = t_TRCAMDEC
    this.w_TRTIPDES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.RadioValue(.t.)
    this.w_TRCAMDST = t_TRCAMDST
    this.w_TRCAMDST = t_TRCAMDST
    this.w_TRCAMDST = t_TRCAMDST
    this.w_TRCAMTIP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.RadioValue(.t.)
    this.w_TRCAMOBB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.RadioValue(.t.)
    this.w_TRTRASCO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.RadioValue(.t.)
    this.w_TRANNOTA = t_TRANNOTA
    this.w_TRCAMDLE = t_TRCAMDLE
    this.w_TRCAMDDE = t_TRCAMDDE
    this.w_TRCAMDST = t_TRCAMDST
    this.w_DESSOR = t_DESSOR
    this.w_FLCOMMEN = t_FLCOMMEN
    this.w_FLCOMMEND = t_FLCOMMEND
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TRCAMNUM with this.w_TRCAMNUM
    replace t_TRTIPSRG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPSRG_2_2.ToRadio()
    replace t_TRCAMSRC with this.w_TRCAMSRC
    replace t_TRCAMDES with this.w_TRCAMDES
    replace t_TREXPSRG with this.w_TREXPSRG
    replace t_TRCAMLUN with this.w_TRCAMLUN
    replace t_TRCAMDEC with this.w_TRCAMDEC
    replace t_TRTIPDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTIPDES_2_8.ToRadio()
    replace t_TRCAMDST with this.w_TRCAMDST
    replace t_TRCAMDST with this.w_TRCAMDST
    replace t_TRCAMDST with this.w_TRCAMDST
    replace t_TRCAMTIP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMTIP_2_12.ToRadio()
    replace t_TRCAMOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCAMOBB_2_13.ToRadio()
    replace t_TRTRASCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRTRASCO_2_14.ToRadio()
    replace t_TRANNOTA with this.w_TRANNOTA
    replace t_TRCAMDLE with this.w_TRCAMDLE
    replace t_TRCAMDDE with this.w_TRCAMDDE
    replace t_TRCAMDST with this.w_TRCAMDST
    replace t_DESSOR with this.w_DESSOR
    replace t_FLCOMMEN with this.w_FLCOMMEN
    replace t_FLCOMMEND with this.w_FLCOMMEND
    if i_srv='A'
      replace TRCAMNUM with this.w_TRCAMNUM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsim_mtrPag1 as StdContainer
  Width  = 817
  height = 431
  stdWidth  = 817
  stdheight = 431
  resizeXpos=299
  resizeYpos=294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTR_ASCII_1_6 as StdField with uid="GIKWHNJFMI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TR_ASCII", cQueryName = "TRCODICE,TR_ASCII",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 61704319,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=97, Top=5, InputMask=replicate('X',200)

  add object oTRDESTIN_1_7 as StdField with uid="OCAEGVNLOO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TRDESTIN", cQueryName = "TRCODICE,TR_ASCII,TRDESTIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 189802364,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=561, Top=5, InputMask=replicate('X',2), cLinkFile="IMPOARCH", cZoomOnZoom="GSIM_AAR", oKey_1_1="ARCODICE", oKey_1_2="this.w_TRDESTIN"

  func oTRDESTIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESARC_1_8 as StdField with uid="SAZEOYUYWS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESARC", cQueryName = "DESARC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 207832522,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=597, Top=5, InputMask=replicate('X',20)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=31, width=805,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=11,Field1="TRCAMNUM",Label1="Seq.",Field2="TRTIPSRG",Label2="Exp.",Field3="TRCAMSRC",Label3="Campo",Field4="TRCAMDES",Label4="Descrizione",Field5="TRCAMLUN",Label5="Lung.",Field6="TRCAMDEC",Label6="Dec.",Field7="TRTIPDES",Label7="Tipo campo",Field8="TRCAMDST",Label8="Destinazione",Field9="TRCAMTIP",Label9="Tipo",Field10="TRCAMOBB",Label10="Obblig.",Field11="TRTRASCO",Label11="Trascod.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235760250

  add object oStr_1_10 as StdString with uid="XWMRFYEXWZ",Visible=.t., Left=11, Top=356,;
    Alignment=0, Width=74, Height=18,;
    Caption="Nota"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="ELCKPSTLPM",Visible=.t., Left=421, Top=8,;
    Alignment=1, Width=138, Height=18,;
    Caption="Archivio destinazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YUTKHVTHEV",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=89, Height=18,;
    Caption="Sorgente dati"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RGQYUXDDDB",Visible=.t., Left=425, Top=332,;
    Alignment=1, Width=78, Height=18,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UJSUTNEOBM",Visible=.t., Left=558, Top=332,;
    Alignment=1, Width=72, Height=18,;
    Caption="Decimali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="XFARLLVDFY",Visible=.t., Left=5, Top=324,;
    Alignment=1, Width=80, Height=18,;
    Caption="Espressione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=51,;
    width=799+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.5000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=52,width=798+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.5000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='SRC_ODBC|XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTREXPSRG_2_5.Refresh()
      this.Parent.oTRANNOTA_2_15.Refresh()
      this.Parent.oTRCAMDLE_2_16.Refresh()
      this.Parent.oTRCAMDDE_2_17.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='SRC_ODBC'
        oDropInto=this.oBodyCol.oRow.oTRCAMSRC_2_3
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oTRCAMDST_2_10
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oTRCAMDST_2_11
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oTREXPSRG_2_5 as StdTrsMemo with uid="OGGGECPZUA",rtseq=14,rtrep=.t.,;
    cFormVar="w_TREXPSRG",value=space(0),;
    HelpContextID = 59959421,;
    cTotal="", bFixedPos=.t., cQueryName = "TREXPSRG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=43, Width=326, Left=90, Top=323

  func oTREXPSRG_2_5.mCond()
    with this.Parent.oContained
      return (.w_TRTIPSRG='E')
    endwith
  endfunc

  add object oTRANNOTA_2_15 as StdTrsMemo with uid="YKJJHTOUHS",rtseq=24,rtrep=.t.,;
    cFormVar="w_TRANNOTA",value=space(0),;
    ToolTipText = "Ulteriori annotazioni relative al campo del tracciato record",;
    HelpContextID = 258517111,;
    cTotal="", bFixedPos=.t., cQueryName = "TRANNOTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=51, Width=802, Left=7, Top=373

  add object oTRCAMDLE_2_16 as StdTrsField with uid="XBBJWPUMWR",rtseq=26,rtrep=.t.,;
    cFormVar="w_TRCAMDLE",value=0,;
    ToolTipText = "Lunghezza della destinazione. Se la lunghezza non � definita si utilizzano le informazioni della sorgente dati.",;
    HelpContextID = 196360069,;
    cTotal="", bFixedPos=.t., cQueryName = "TRCAMDLE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=507, Top=330, cSayPict=[v_ZR+"99999"], cGetPict=[v_ZR+"99999"]

  func oTRCAMDLE_2_16.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='V')
    endwith
  endfunc

  add object oTRCAMDDE_2_17 as StdTrsField with uid="QAOJWEEHIX",rtseq=27,rtrep=.t.,;
    cFormVar="w_TRCAMDDE",value=0,;
    ToolTipText = "Numero decimali della destinazione. Se la lunghezza non � definita si utilizzano le informazioni della sorgente dati.",;
    HelpContextID = 72075387,;
    cTotal="", bFixedPos=.t., cQueryName = "TRCAMDDE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=632, Top=330, cSayPict=[v_ZR+"999"], cGetPict=[v_ZR+"999"]

  func oTRCAMDDE_2_17.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='V')
    endwith
  endfunc

  add object oBtn_2_18 as StdButton with uid="LNXXASQQCV",width=48,height=45,;
   left=680, top=321,;
    CpPicture="BMP\trascodifica.bmp", caption="", nPag=2;
    , ToolTipText = "Richiama le trascodifiche per il campo";
    , HelpContextID = 127821823;
    , Caption='\<Trascod.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        do gsim_btr with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mCond()
    with this.Parent.oContained
      return (.w_TRTRASCO='S' and .w_TRTIPDES # 'V')
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsim_mtrBodyRow as CPBodyRowCnt
  Width=789
  Height=int(fontmetric(1,"Arial",9,"")*1*1.5000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTRCAMNUM_2_1 as StdTrsField with uid="BBFNFQKXEJ",rtseq=10,rtrep=.t.,;
    cFormVar="w_TRCAMNUM",value=0,isprimarykey=.t.,;
    ToolTipText = "Numero progressivo del campo all'interno del tracciato",;
    HelpContextID = 239847555,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=[v_ZR+"99999"], cGetPict=[v_ZR+"99999"]

  add object oTRTIPSRG_2_2 as StdTrsCheck with uid="RSVXOSVDHV",rtrep=.t.,;
    cFormVar="w_TRTIPSRG",  caption="",;
    ToolTipText = "Se impostato indica che la sorgente � una espressione",;
    HelpContextID = 59037821,;
    Left=40, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oTRTIPSRG_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTIPSRG,&i_cF..t_TRTIPSRG),this.value)
    return(iif(xVal =1,'E',;
    space(1)))
  endfunc
  func oTRTIPSRG_2_2.GetRadio()
    this.Parent.oContained.w_TRTIPSRG = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPSRG_2_2.ToRadio()
    this.Parent.oContained.w_TRTIPSRG=trim(this.Parent.oContained.w_TRTIPSRG)
    return(;
      iif(this.Parent.oContained.w_TRTIPSRG=='E',1,;
      0))
  endfunc

  func oTRTIPSRG_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTRCAMSRC_2_3 as StdTrsField with uid="JUQDQBUBXM",rtseq=12,rtrep=.t.,;
    cFormVar="w_TRCAMSRC",value=space(30),;
    HelpContextID = 55298169,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=59, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="SRC_ODBC", cZoomOnZoom="GSIM_MSO", oKey_1_1="SOCODICE", oKey_1_2="this.w_XLNK_ASCII", oKey_2_1="FLNAME", oKey_2_2="this.w_TRCAMSRC"

  func oTRCAMSRC_2_3.mCond()
    with this.Parent.oContained
      return (.w_XLNK_TIPSRC<>'A' and .w_TRTIPSRG<>'E')
    endwith
  endfunc

  func oTRCAMSRC_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_XLNK_TIPSRC='A')
    endwith
    endif
  endfunc

  func oTRCAMSRC_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAMSRC_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCAMSRC_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SRC_ODBC_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SOCODICE="+cp_ToStrODBC(this.Parent.oContained.w_XLNK_ASCII)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SOCODICE="+cp_ToStr(this.Parent.oContained.w_XLNK_ASCII)
    endif
    do cp_zoom with 'SRC_ODBC','*','SOCODICE,FLNAME',cp_AbsName(this.parent,'oTRCAMSRC_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSIM_MSO',"Campo sorgente dati",'',this.parent.oContained
  endproc
  proc oTRCAMSRC_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSIM_MSO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SOCODICE=w_XLNK_ASCII
     i_obj.w_FLNAME=this.parent.oContained.w_TRCAMSRC
    i_obj.ecpSave()
  endproc

  add object oTRCAMDES_2_4 as StdTrsField with uid="MDNGYESHUF",rtseq=13,rtrep=.t.,;
    cFormVar="w_TRCAMDES",value=space(30),;
    ToolTipText = "Descrizione del contenuto del campo",;
    HelpContextID = 72075401,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=192, Left=155, Top=0, InputMask=replicate('X',30)

  add object oTRCAMLUN_2_6 as StdTrsField with uid="KQVIXMLZGQ",rtseq=15,rtrep=.t.,;
    cFormVar="w_TRCAMLUN",value=0,;
    ToolTipText = "Lunghezza del campo nel file ASCII comprensiva di separatori e decimali",;
    HelpContextID = 206293124,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=349, Top=0, cSayPict=[v_ZR+"99999"], cGetPict=[v_ZR+"99999"]

  func oTRCAMLUN_2_6.mCond()
    with this.Parent.oContained
      return (.w_XLNK_TIPSRC<>'O')
    endwith
  endfunc

  add object oTRCAMDEC_2_7 as StdTrsField with uid="XNMXEDNUSX",rtseq=16,rtrep=.t.,;
    cFormVar="w_TRCAMDEC",value=0,;
    ToolTipText = "Numero di decimali",;
    HelpContextID = 72075385,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=24, Left=388, Top=0, cSayPict=[v_ZR+"99"], cGetPict=[v_ZR+"99"]

  func oTRCAMDEC_2_7.mCond()
    with this.Parent.oContained
      return (.w_TRCAMTIP="N" and .w_XLNK_TIPSRC<>'O')
    endwith
  endfunc

  add object oTRTIPDES_2_8 as StdTrsCombo with uid="MUELSQBSCQ",rtrep=.t.,;
    cFormVar="w_TRTIPDES", RowSource=""+"Campo,"+"Campo dettaglio,"+"Variabile" , ;
    HelpContextID = 75815049,;
    Height=22, Width=74, Left=416, Top=-1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "L'archivio di destinazione non ha una tabella di dettaglio";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTRTIPDES_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTIPDES,&i_cF..t_TRTIPDES),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'D',;
    iif(xVal =3,'V',;
    space(1)))))
  endfunc
  func oTRTIPDES_2_8.GetRadio()
    this.Parent.oContained.w_TRTIPDES = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPDES_2_8.ToRadio()
    this.Parent.oContained.w_TRTIPDES=trim(this.Parent.oContained.w_TRTIPDES)
    return(;
      iif(this.Parent.oContained.w_TRTIPDES=='C',1,;
      iif(this.Parent.oContained.w_TRTIPDES=='D',2,;
      iif(this.Parent.oContained.w_TRTIPDES=='V',3,;
      0))))
  endfunc

  func oTRTIPDES_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRTIPDES_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_TRTIPDES='D' and empty(.w_TABEDET),.F.,.T.))
    endwith
    return bRes
  endfunc

  add object oTRCAMDST_2_9 as StdTrsField with uid="YSGVGCTZLP",rtseq=18,rtrep=.t.,;
    cFormVar="w_TRCAMDST",value=space(15),;
    ToolTipText = "Nome della variabile di destinazione",;
    HelpContextID = 72075402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=491, Top=0, InputMask=replicate('X',15)

  func oTRCAMDST_2_9.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='V')
    endwith
  endfunc

  func oTRCAMDST_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPDES<>'V')
    endwith
    endif
  endfunc

  add object oTRCAMDST_2_10 as StdTrsField with uid="HFXAIQANMX",rtseq=19,rtrep=.t.,;
    cFormVar="w_TRCAMDST",value=space(15),;
    ToolTipText = "Nome del campo di destinazione negli archivi di adhoc",;
    HelpContextID = 72075402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=491, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_TABEDET", oKey_2_1="FLNAME", oKey_2_2="this.w_TRCAMDST"

  func oTRCAMDST_2_10.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='D')
    endwith
  endfunc

  func oTRCAMDST_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPDES<>'D')
    endwith
    endif
  endfunc

  func oTRCAMDST_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAMDST_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCAMDST_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_TABEDET)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_TABEDET)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oTRCAMDST_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campo destinazione",'',this.parent.oContained
  endproc

  add object oTRCAMDST_2_11 as StdTrsField with uid="RYMCRKOMFH",rtseq=20,rtrep=.t.,;
    cFormVar="w_TRCAMDST",value=space(15),;
    ToolTipText = "Nome del campo di destinazione negli archivi di adhoc",;
    HelpContextID = 72075402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=491, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA", oKey_2_1="FLNAME", oKey_2_2="this.w_TRCAMDST"

  func oTRCAMDST_2_11.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='C')
    endwith
  endfunc

  func oTRCAMDST_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPDES<>'C')
    endwith
    endif
  endfunc

  func oTRCAMDST_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCAMDST_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCAMDST_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_TABELLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_TABELLA)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oTRCAMDST_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campo destinazione",'',this.parent.oContained
  endproc

  add object oTRCAMTIP_2_12 as StdTrsCombo with uid="PWLOPGISYO",rtrep=.t.,;
    cFormVar="w_TRCAMTIP", RowSource=""+"Carattere,"+"Data,"+"Numerico,"+"Memo (solo ODBC),"+"DateTime" , ;
    ToolTipText = "Tipo del campo nell'archivio di destinazione",;
    HelpContextID = 196360058,;
    Height=21, Width=100, Left=588, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTRCAMTIP_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRCAMTIP,&i_cF..t_TRCAMTIP),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'D',;
    iif(xVal =3,'N',;
    iif(xVal =4,'M',;
    iif(xVal =5,'T',;
    space(1)))))))
  endfunc
  func oTRCAMTIP_2_12.GetRadio()
    this.Parent.oContained.w_TRCAMTIP = this.RadioValue()
    return .t.
  endfunc

  func oTRCAMTIP_2_12.ToRadio()
    this.Parent.oContained.w_TRCAMTIP=trim(this.Parent.oContained.w_TRCAMTIP)
    return(;
      iif(this.Parent.oContained.w_TRCAMTIP=='C',1,;
      iif(this.Parent.oContained.w_TRCAMTIP=='D',2,;
      iif(this.Parent.oContained.w_TRCAMTIP=='N',3,;
      iif(this.Parent.oContained.w_TRCAMTIP=='M',4,;
      iif(this.Parent.oContained.w_TRCAMTIP=='T',5,;
      0))))))
  endfunc

  func oTRCAMTIP_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRCAMTIP_2_12.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES='V')
    endwith
  endfunc

  add object oTRCAMOBB_2_13 as StdTrsCombo with uid="MCEQEVIZQB",rtrep=.t.,;
    cFormVar="w_TRCAMOBB", RowSource=""+"Si,"+"No" , ;
    ToolTipText = "Specificare se il campo � obbligatorio",;
    HelpContextID = 256624760,;
    Height=21, Width=45, Left=691, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTRCAMOBB_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRCAMOBB,&i_cF..t_TRCAMOBB),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oTRCAMOBB_2_13.GetRadio()
    this.Parent.oContained.w_TRCAMOBB = this.RadioValue()
    return .t.
  endfunc

  func oTRCAMOBB_2_13.ToRadio()
    this.Parent.oContained.w_TRCAMOBB=trim(this.Parent.oContained.w_TRCAMOBB)
    return(;
      iif(this.Parent.oContained.w_TRCAMOBB=='S',1,;
      iif(this.Parent.oContained.w_TRCAMOBB=='N',2,;
      0)))
  endfunc

  func oTRCAMOBB_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTRTRASCO_2_14 as StdTrsCombo with uid="RVEHJFGSEQ",rtrep=.t.,;
    cFormVar="w_TRTRASCO", RowSource=""+"No,"+"Si" , ;
    HelpContextID = 43899013,;
    Height=21, Width=45, Left=739, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTRTRASCO_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTRASCO,&i_cF..t_TRTRASCO),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oTRTRASCO_2_14.GetRadio()
    this.Parent.oContained.w_TRTRASCO = this.RadioValue()
    return .t.
  endfunc

  func oTRTRASCO_2_14.ToRadio()
    this.Parent.oContained.w_TRTRASCO=trim(this.Parent.oContained.w_TRTRASCO)
    return(;
      iif(this.Parent.oContained.w_TRTRASCO=='N',1,;
      iif(this.Parent.oContained.w_TRTRASCO=='S',2,;
      0)))
  endfunc

  func oTRTRASCO_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRTRASCO_2_14.mCond()
    with this.Parent.oContained
      return (.w_TRTIPDES # 'V')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oTRCAMNUM_2_1.When()
    return(.t.)
  proc oTRCAMNUM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTRCAMNUM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_mtr','TRACCIAT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODICE=TRACCIAT.TRCODICE";
  +" and "+i_cAliasName2+".TR_ASCII=TRACCIAT.TR_ASCII";
  +" and "+i_cAliasName2+".TRDESTIN=TRACCIAT.TRDESTIN";
  +" and "+i_cAliasName2+".TRTIPSRC=TRACCIAT.TRTIPSRC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
