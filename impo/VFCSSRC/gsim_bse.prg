* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bse                                                        *
*              Selezione/desel. archivi e movimenti contabili                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_32]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-02-01                                                      *
* Last revis.: 2013-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParametro
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bse",oParentObject,m.pParametro)
return(i_retval)

define class tgsim_bse as StdBatch
  * --- Local variables
  w_Control = .NULL.
  w_ArchDest = space(2)
  pParametro = 0
  * --- WorkFile variables
  IMPORTAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione e Deselezione checkbox archivi e movimentazioni
    * --- Parametri:
    *     0= Init
    *     1= Selezione o Deselezione in funzione di variabile w_RADSELEZ
    * --- 2= Selezione o Deselezione in funzione di variabile w_RADSELEZ nella 'gsim_kec'
    * --- Selezione e Deselezione Archivi
    * --- Archivi
    * --- Movimenti
    * --- Tabelle F24
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pParametro=0
      this.oParentObject.w_RADSELEZ = "S"
    endif
    if !empty(this.oParentObject.w_CODIMP)
      * --- Select from IMPORTAZ
      i_nConn=i_TableProp[this.IMPORTAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IMPORTAZ_idx,2],.t.,this.IMPORTAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select IMDESTIN  from "+i_cTable+" IMPORTAZ ";
            +" where IMCODICE="+cp_ToStrODBC(this.oParentObject.w_CODIMP)+" and IMTIPSRC="+cp_ToStrODBC(this.oParentObject.w_TIPSRC)+"";
             ,"_Curs_IMPORTAZ")
      else
        select IMDESTIN from (i_cTable);
         where IMCODICE=this.oParentObject.w_CODIMP and IMTIPSRC=this.oParentObject.w_TIPSRC;
          into cursor _Curs_IMPORTAZ
      endif
      if used('_Curs_IMPORTAZ')
        select _Curs_IMPORTAZ
        locate for 1=1
        do while not(eof())
        w_CMD="this.oParentObject.w_SELE"+_Curs_IMPORTAZ.IMDESTIN
        w_ErrHandler = on("ERROR")
        on error w_Accept=1
        if this.oParentObject.w_RADSELEZ="S"
          &w_CMD=_Curs_IMPORTAZ.IMDESTIN
        else
          &w_CMD=""
        endif
        on error &w_ErrHandler 
          select _Curs_IMPORTAZ
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deseleziona tutti
    if this.pParametro = 1
      * --- Archivi
      this.oParentObject.w_SELEAC = "|"
      this.w_ArchDest = "AC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEAG = "|"
      this.w_ArchDest = "AG"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEBA = "|"
      this.w_ArchDest = "BA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECC = "|"
      this.w_ArchDest = "CC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPD = "|"
      this.w_ArchDest = "PD"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECF = "|"
      this.w_ArchDest = "CF"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEIV = "|"
      this.w_ArchDest = "IV"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEDE = "|"
      this.w_ArchDest = "DE"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELELI = "|"
      this.w_ArchDest = "LI"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELENA = "|"
      this.w_ArchDest = "NA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPA = "|"
      this.w_ArchDest = "PA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPC = "|"
      this.w_ArchDest = "PC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELESC = "|"
      this.w_ArchDest = "SC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEVA = "|"
      this.w_ArchDest = "VA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEZO = "|"
      this.w_ArchDest = "ZO"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELELS = "|"
      this.w_ArchDest = "LS"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPO = "|"
      this.w_ArchDest = "PO"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEVE = "|"
      this.w_ArchDest = "VE"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELESP = "|"
      this.w_ArchDest = "SP"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECB = "|"
      this.w_ArchDest = "CB"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECA = "|"
      this.w_ArchDest = "CA"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECR = "|"
      this.w_ArchDest = "CR"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECS = "|"
      this.w_ArchDest = "CS"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECT = "|"
      this.w_ArchDest = "CT"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECL = "|"
      this.w_ArchDest = "CL"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEGI = "|"
      this.w_ArchDest = "GI"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEAS = "|"
      this.w_ArchDest = "AS"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Movimenti
      this.oParentObject.w_SELEPN = "|"
      this.w_ArchDest = "PN"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPT = "|"
      this.w_ArchDest = "PT"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEPI = "|"
      this.w_ArchDest = "PI"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECO = "|"
      this.w_ArchDest = "CO"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECE = "|"
      this.w_ArchDest = "CE"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEVC = "|"
      this.w_ArchDest = "VC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEMC = "|"
      this.w_ArchDest = "MC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELETR = "|"
      this.w_ArchDest = "TR"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEMS = "|"
      this.w_ArchDest = "MS"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECH = "|"
      this.w_ArchDest = "CH"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELERF = "|"
      this.w_ArchDest = "RF"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELENM = "|"
      this.w_ArchDest = "NM"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.pParametro = 2
      * --- Tabelle F24
      this.oParentObject.w_SELECU = "|"
      this.w_ArchDest = "CU"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECK = "|"
      this.w_ArchDest = "CK"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECJ = "|"
      this.w_ArchDest = "CJ"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELECV = "|"
      this.w_ArchDest = "CV"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELENC = "|"
      this.w_ArchDest = "NC"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELESI = "|"
      this.w_ArchDest = "SI"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELESN = "|"
      this.w_ArchDest = "SN"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELEEL = "|"
      this.w_ArchDest = "EL"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_SELERG = "|"
      this.w_ArchDest = "RG"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Control = this.oParentObject.GetControlObject("w_SELE"+this.w_ArchDest,1)
    if type("this.w_Control")="O"
      this.w_Control.BackStyle = 0
      this.w_Control.FontStrikeThru = .F.
    endif
  endproc


  proc Init(oParentObject,pParametro)
    this.pParametro=pParametro
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMPORTAZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_IMPORTAZ')
      use in _Curs_IMPORTAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParametro"
endproc
