* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_kae                                                        *
*              Import archivi alterego                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-08-06                                                      *
* Last revis.: 2014-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsim_kae",oParentObject))

* --- Class definition
define class tgsim_kae as StdForm
  Top    = 0
  Left   = -2

  * --- Standard Properties
  Width  = 751
  Height = 604
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-22"
  HelpContextID=132759401
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=116

  * --- Constant Properties
  _IDX = 0
  importaz_IDX = 0
  IMPORTAZ_IDX = 0
  cPrg = "gsim_kae"
  cComment = "Import archivi alterego"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODIMP = space(20)
  w_ODBCDSN = space(30)
  w_PATH = space(200)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_SELEVA = space(10)
  w_SELENA = space(10)
  w_SELERU = space(10)
  w_SELEGN = space(10)
  w_SELESN = space(10)
  w_SELEON = space(10)
  w_SELETZ = space(10)
  w_SELENM = space(10)
  w_SELERA = space(10)
  w_SELEIV = space(10)
  w_SELEPA = space(10)
  w_SELEPD = space(10)
  w_SELEPC = space(10)
  w_SELECF = space(10)
  w_SELEDE = space(10)
  w_SELETU = space(10)
  w_SELEGU = space(10)
  w_SELECC = space(10)
  w_SELEAC = space(10)
  w_SELEKR = space(10)
  w_SELEEC = space(10)
  w_SELEC2 = space(10)
  w_SELEOG = space(10)
  w_SELEST = space(10)
  w_SELEUB = space(10)
  w_SELEUP = space(10)
  w_SELESD = space(10)
  w_SELEDP = space(10)
  w_SELEA1 = space(10)
  w_SELEA2 = space(10)
  w_SELEA3 = space(10)
  w_SELEA4 = space(10)
  w_SELEFP = space(10)
  w_SELEF1 = space(10)
  w_SELEF2 = space(10)
  w_SELEF3 = space(10)
  w_SELEF4 = space(10)
  w_SELEC1 = space(10)
  w_SELESO = space(10)
  w_SELERS = space(10)
  w_SELET1 = space(10)
  w_SELEPM = space(10)
  w_SELEEV = space(10)
  w_SELEA6 = space(10)
  w_SELEDT = space(10)
  w_SELEFR = space(10)
  w_SELEDO = space(10)
  w_SELETL = space(10)
  w_SELECU = space(10)
  w_SELET2 = space(10)
  w_SELEIP = space(10)
  w_SELEIA = space(10)
  w_SELEAT = space(10)
  w_SELEDR = space(10)
  w_SELEA5 = space(10)
  w_SELEPE = space(10)
  w_SELEFY = space(10)
  w_SELEPP = space(10)
  w_SELEPZ = space(10)
  w_SELEPN = space(10)
  w_SELEPT = space(10)
  w_SELEBM = space(10)
  w_SELEBR = space(10)
  w_SELEBU = space(10)
  w_SELEBE = space(10)
  w_SELEBT = space(10)
  w_SELEBS = space(10)
  w_SELEBC = space(10)
  w_SELEBB = space(10)
  w_SELEA7 = space(10)
  w_SELEA8 = space(10)
  w_SELEA9 = space(10)
  w_SELECONT = space(10)
  w_SELERESO = space(10)
  w_IMPARIVA = space(10)
  w_IMAZZERA = space(10)
  w_IMTRANSA = space(1)
  w_IMCONFER = space(1)
  w_RADSELEZ = space(10)
  w_INCORSO = .F.
  w_TIPSRC = space(1)
  w_DESIMP = space(60)
  w_TIPDBF = space(1)
  w_ArchParz = space(45)
  w_ArchPar2 = space(45)
  w_Gestiti = space(45)
  w_SELETI = space(10)
  w_SELEMP = space(10)
  w_SELEEP = space(10)
  w_SELEPI = space(10)
  w_SELETF = space(10)
  w_SELEDI = space(10)
  w_SELEDG = space(10)
  w_SELEPS = space(10)
  w_TRCODFIL = space(2)
  w_TRTIPTRA = space(1)
  w_TRNOMCAM = space(30)
  w_TRCODIMP = space(20)
  w_TRTRAPAR = space(1)
  w_SOSQLSRC = space(0)
  w_ODBCCONN = 0
  w_ODBCDSN = space(30)
  w_ODBCPATH = space(200)
  w_ODBCUSER = space(30)
  w_ODBCPASSW = space(30)
  w_ODBCCURSOR = space(8)
  w_ODBCSQL = 0
  w_SELECR = space(10)
  w_SELECT = space(10)
  w_SELECS = space(10)
  * --- Area Manuale = Declare Variables
  * --- gsim_kae
  Function GetControlObject(cVar,nPage)
    local oObj
    local nIndex
    oObj = .NULL.
    cVar=upper(cVar)
    with this.oPgFrm.Pages(nPage).oPag
      for nIndex=1 to .ControlCount
          if type('this.oPgFrm.Pages(nPage).oPag.Controls(nIndex).cFormVar') = 'C'
             if upper(.Controls(nIndex).cFormVar) == cVar
                oObj= .Controls(nIndex)
                nIndex = .ControlCount + 1
             endif
          endif
      next
    endwith
    return (oObj)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsim_kaePag1","gsim_kae",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODIMP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='importaz'
    this.cWorkTables[2]='IMPORTAZ'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gsim_bac with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODIMP=space(20)
      .w_ODBCDSN=space(30)
      .w_PATH=space(200)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_SELEVA=space(10)
      .w_SELENA=space(10)
      .w_SELERU=space(10)
      .w_SELEGN=space(10)
      .w_SELESN=space(10)
      .w_SELEON=space(10)
      .w_SELETZ=space(10)
      .w_SELENM=space(10)
      .w_SELERA=space(10)
      .w_SELEIV=space(10)
      .w_SELEPA=space(10)
      .w_SELEPD=space(10)
      .w_SELEPC=space(10)
      .w_SELECF=space(10)
      .w_SELEDE=space(10)
      .w_SELETU=space(10)
      .w_SELEGU=space(10)
      .w_SELECC=space(10)
      .w_SELEAC=space(10)
      .w_SELEKR=space(10)
      .w_SELEEC=space(10)
      .w_SELEC2=space(10)
      .w_SELEOG=space(10)
      .w_SELEST=space(10)
      .w_SELEUB=space(10)
      .w_SELEUP=space(10)
      .w_SELESD=space(10)
      .w_SELEDP=space(10)
      .w_SELEA1=space(10)
      .w_SELEA2=space(10)
      .w_SELEA3=space(10)
      .w_SELEA4=space(10)
      .w_SELEFP=space(10)
      .w_SELEF1=space(10)
      .w_SELEF2=space(10)
      .w_SELEF3=space(10)
      .w_SELEF4=space(10)
      .w_SELEC1=space(10)
      .w_SELESO=space(10)
      .w_SELERS=space(10)
      .w_SELET1=space(10)
      .w_SELEPM=space(10)
      .w_SELEEV=space(10)
      .w_SELEA6=space(10)
      .w_SELEDT=space(10)
      .w_SELEFR=space(10)
      .w_SELEDO=space(10)
      .w_SELETL=space(10)
      .w_SELECU=space(10)
      .w_SELET2=space(10)
      .w_SELEIP=space(10)
      .w_SELEIA=space(10)
      .w_SELEAT=space(10)
      .w_SELEDR=space(10)
      .w_SELEA5=space(10)
      .w_SELEPE=space(10)
      .w_SELEFY=space(10)
      .w_SELEPP=space(10)
      .w_SELEPZ=space(10)
      .w_SELEPN=space(10)
      .w_SELEPT=space(10)
      .w_SELEBM=space(10)
      .w_SELEBR=space(10)
      .w_SELEBU=space(10)
      .w_SELEBE=space(10)
      .w_SELEBT=space(10)
      .w_SELEBS=space(10)
      .w_SELEBC=space(10)
      .w_SELEBB=space(10)
      .w_SELEA7=space(10)
      .w_SELEA8=space(10)
      .w_SELEA9=space(10)
      .w_SELECONT=space(10)
      .w_SELERESO=space(10)
      .w_IMPARIVA=space(10)
      .w_IMAZZERA=space(10)
      .w_IMTRANSA=space(1)
      .w_IMCONFER=space(1)
      .w_RADSELEZ=space(10)
      .w_INCORSO=.f.
      .w_TIPSRC=space(1)
      .w_DESIMP=space(60)
      .w_TIPDBF=space(1)
      .w_ArchParz=space(45)
      .w_ArchPar2=space(45)
      .w_Gestiti=space(45)
      .w_SELETI=space(10)
      .w_SELEMP=space(10)
      .w_SELEEP=space(10)
      .w_SELEPI=space(10)
      .w_SELETF=space(10)
      .w_SELEDI=space(10)
      .w_SELEDG=space(10)
      .w_SELEPS=space(10)
      .w_TRCODFIL=space(2)
      .w_TRTIPTRA=space(1)
      .w_TRNOMCAM=space(30)
      .w_TRCODIMP=space(20)
      .w_TRTRAPAR=space(1)
      .w_SOSQLSRC=space(0)
      .w_ODBCCONN=0
      .w_ODBCDSN=space(30)
      .w_ODBCPATH=space(200)
      .w_ODBCUSER=space(30)
      .w_ODBCPASSW=space(30)
      .w_ODBCCURSOR=space(8)
      .w_ODBCSQL=0
      .w_SELECR=space(10)
      .w_SELECT=space(10)
      .w_SELECS=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODIMP))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_PATH = sys(5)+sys(2003)+"\"
        .w_ODBCPATH = SPACE(200)
          .DoRTCalc(5,6,.f.)
        .w_SELEVA = 'VA'
        .w_SELENA = 'NA'
        .w_SELERU = 'RU'
        .w_SELEGN = 'GN'
        .w_SELESN = 'SN'
        .w_SELEON = 'ON'
        .w_SELETZ = 'TZ'
        .w_SELENM = 'NM'
        .w_SELERA = 'RA'
        .w_SELEIV = 'IV'
        .w_SELEPA = 'PA'
        .w_SELEPD = 'PD'
        .w_SELEPC = 'PC'
        .w_SELECF = 'CF'
        .w_SELEDE = 'DE'
        .w_SELETU = 'TU'
        .w_SELEGU = 'GU'
        .w_SELECC = 'CC'
        .w_SELEAC = 'AC'
        .w_SELEKR = 'KR'
        .w_SELEEC = 'EC'
        .w_SELEC2 = 'C2'
        .w_SELEOG = 'OG'
        .w_SELEST = 'ST'
        .w_SELEUB = 'UB'
        .w_SELEUP = 'UP'
        .w_SELESD = 'SD'
        .w_SELEDP = 'DP'
        .w_SELEA1 = 'A1'
        .w_SELEA2 = IIF(.w_SELEA1='A1','A2','|')
        .w_SELEA3 = IIF(.w_SELEA1='A1','A3','|')
        .w_SELEA4 = IIF(.w_SELEA1='A1','A4','|')
        .w_SELEFP = 'FP'
        .w_SELEF1 = 'F1'
        .w_SELEF2 = 'F2'
        .w_SELEF3 = 'F3'
        .w_SELEF4 = 'F4'
        .w_SELEC1 = 'C1'
        .w_SELESO = 'SO'
        .w_SELERS = 'RS'
        .w_SELET1 = 'T1'
        .w_SELEPM = 'PM'
        .w_SELEEV = 'EV'
        .w_SELEA6 = 'A6'
        .w_SELEDT = 'DT'
        .w_SELEFR = 'FR'
        .w_SELEDO = 'DO'
        .w_SELETL = 'TL'
        .w_SELECU = 'CU'
        .w_SELET2 = 'T2'
        .w_SELEIP = 'IP'
        .w_SELEIA = 'IA'
        .w_SELEAT = 'AT'
        .w_SELEDR = 'DR'
        .w_SELEA5 = 'A5'
        .w_SELEPE = 'PE'
        .w_SELEFY = 'FY'
        .w_SELEPP = 'PP'
        .w_SELEPZ = 'PZ'
        .w_SELEPN = 'PN'
        .w_SELEPT = 'PT'
        .w_SELEBM = 'BM'
        .w_SELEBR = 'BR'
        .w_SELEBU = 'BU'
        .w_SELEBE = 'BE'
        .w_SELEBT = 'BT'
        .w_SELEBS = 'BS'
        .w_SELEBC = 'BC'
        .w_SELEBB = 'BB'
        .w_SELEA7 = IIF(.w_SELEA6='A6','A7','|')
        .w_SELEA8 = IIF(.w_SELEA6='A6','A8','|')
        .w_SELEA9 = IIF(.w_SELEA6='A6','A9','|')
        .w_SELECONT = 'N'
        .w_SELERESO = 'E'
        .w_IMPARIVA = 'S'
        .w_IMAZZERA = 'N'
        .w_IMTRANSA = 'S'
        .w_IMCONFER = 'N'
        .w_RADSELEZ = 'D'
        .w_INCORSO = .F.
          .DoRTCalc(87,89,.f.)
        .w_ArchParz = .w_SELEOG+"|"+.w_SELEST+"|"+.w_SELEUB+"|"+.w_SELETI+"|"+.w_SELEMP+"|"+.w_SELEUP+"|"+.w_SELEEP+"|"+.w_SELEA1+"|"+.w_SELEA2+"|"+.w_SELEA3+"|"+.w_SELEA4+"|"+.w_SELESD+"|"+.w_SELEDP+"|"+.w_SELEC1+"|"+ .w_SELEDO+"|"
        .w_ArchPar2 = .w_SELESO+"|"+.w_SELERS+"|"+.w_SELEPI+"|"+.w_SELEPE+"|"+.w_SELETL+"|"+.w_SELECU+"|"+.w_SELEIP+"|"+.w_SELEDG+"|"+.w_SELEAT+"|"+.w_SELETF+"|"+.w_SELEDI+"|"+.w_SELERU+"|"+.w_SELEFP+"|"+.w_SELEPS+"|"+.w_SELEF1+"|"+.w_SELEF2+"|"+.w_SELEF3+"|"+.w_SELEF4+"|"+.w_SELEDT+"|"+.w_SELEA5+"|"+.w_SELEGU+"|"+.w_SELERA+"|"+.w_SELEBB+"|"+.w_SELEBU+"|"+.w_SELEBE+"|"+.w_SELEBM+"|"+.w_SELEBR+"|"+.w_SELEBT+"|"+.w_SELEGN+"|"+.w_SELECF+"|"+.w_SELENM+"|"+.w_SELET1+"|"+.w_SELET2+"|"+.w_SELEPM+"|"+.w_SELEA6+"|"+.w_SELEA7+"|"+.w_SELEA8+"|"+.w_SELEA9+"|"+.w_SELEEV+"|"+.w_SELEBS+"|"+.w_SELEBC+"|"+.w_SELESN+"|"+.w_SELEDR+"|"+.w_SELEFR+"|"+.w_SELEON+"|"+.w_SELEVA+"|"+.w_SELENA+"|"+.w_SELEIV+"|"+.w_SELEPA+"|"+.w_SELEPD+"|"+.w_SELEPC+"|"+.w_SELECC+"|"+.w_SELEAC+"|"+.w_SELEPN+"|"+.w_SELEPT+"|"+.w_SELEFY+"|"+.w_SELECR+"|"+.w_SELECS+"|"+.w_SELECT+"|"+.w_SELETU+"|"+.w_SELEKR+"|"+.w_SELEEC+"|"+.w_SELEC2+"|"+.w_SELEPZ+"|"+.w_SELEIA + "|"+.w_SELEDE+"|"+.w_SELETZ+"|"+.w_SELEPP
        .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
        .w_SELETI = 'TI'
        .w_SELEMP = 'MP'
        .w_SELEEP = 'EP'
        .w_SELEPI = 'PI'
        .w_SELETF = 'TF'
        .w_SELEDI = 'DI'
        .w_SELEDG = 'DG'
        .w_SELEPS = 'PS'
          .DoRTCalc(101,106,.f.)
        .w_ODBCCONN = -1
          .DoRTCalc(108,108,.f.)
        .w_ODBCPATH = SPACE(200)
          .DoRTCalc(110,111,.f.)
        .w_ODBCCURSOR = sys(2015)
        .w_ODBCSQL = -1
        .w_SELECR = 'CR'
        .w_SELECT = 'CT'
        .w_SELECS = 'CS'
      .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_97.enabled = this.oPgFrm.Page1.oPag.oBtn_1_97.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,35,.t.)
            .w_SELEA2 = IIF(.w_SELEA1='A1','A2','|')
            .w_SELEA3 = IIF(.w_SELEA1='A1','A3','|')
            .w_SELEA4 = IIF(.w_SELEA1='A1','A4','|')
        .DoRTCalc(39,75,.t.)
            .w_SELEA7 = IIF(.w_SELEA6='A6','A7','|')
            .w_SELEA8 = IIF(.w_SELEA6='A6','A8','|')
            .w_SELEA9 = IIF(.w_SELEA6='A6','A9','|')
        .DoRTCalc(79,89,.t.)
            .w_ArchParz = .w_SELEOG+"|"+.w_SELEST+"|"+.w_SELEUB+"|"+.w_SELETI+"|"+.w_SELEMP+"|"+.w_SELEUP+"|"+.w_SELEEP+"|"+.w_SELEA1+"|"+.w_SELEA2+"|"+.w_SELEA3+"|"+.w_SELEA4+"|"+.w_SELESD+"|"+.w_SELEDP+"|"+.w_SELEC1+"|"+ .w_SELEDO+"|"
            .w_ArchPar2 = .w_SELESO+"|"+.w_SELERS+"|"+.w_SELEPI+"|"+.w_SELEPE+"|"+.w_SELETL+"|"+.w_SELECU+"|"+.w_SELEIP+"|"+.w_SELEDG+"|"+.w_SELEAT+"|"+.w_SELETF+"|"+.w_SELEDI+"|"+.w_SELERU+"|"+.w_SELEFP+"|"+.w_SELEPS+"|"+.w_SELEF1+"|"+.w_SELEF2+"|"+.w_SELEF3+"|"+.w_SELEF4+"|"+.w_SELEDT+"|"+.w_SELEA5+"|"+.w_SELEGU+"|"+.w_SELERA+"|"+.w_SELEBB+"|"+.w_SELEBU+"|"+.w_SELEBE+"|"+.w_SELEBM+"|"+.w_SELEBR+"|"+.w_SELEBT+"|"+.w_SELEGN+"|"+.w_SELECF+"|"+.w_SELENM+"|"+.w_SELET1+"|"+.w_SELET2+"|"+.w_SELEPM+"|"+.w_SELEA6+"|"+.w_SELEA7+"|"+.w_SELEA8+"|"+.w_SELEA9+"|"+.w_SELEEV+"|"+.w_SELEBS+"|"+.w_SELEBC+"|"+.w_SELESN+"|"+.w_SELEDR+"|"+.w_SELEFR+"|"+.w_SELEON+"|"+.w_SELEVA+"|"+.w_SELENA+"|"+.w_SELEIV+"|"+.w_SELEPA+"|"+.w_SELEPD+"|"+.w_SELEPC+"|"+.w_SELECC+"|"+.w_SELEAC+"|"+.w_SELEPN+"|"+.w_SELEPT+"|"+.w_SELEFY+"|"+.w_SELECR+"|"+.w_SELECS+"|"+.w_SELECT+"|"+.w_SELETU+"|"+.w_SELEKR+"|"+.w_SELEEC+"|"+.w_SELEC2+"|"+.w_SELEPZ+"|"+.w_SELEIA + "|"+.w_SELEDE+"|"+.w_SELETZ+"|"+.w_SELEPP
            .w_Gestiti = iif(.w_INCORSO,.w_Gestiti,.w_ArchParz+.w_ArchPar2)
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(93,116,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODIMP_1_1.enabled = this.oPgFrm.Page1.oPag.oCODIMP_1_1.mCond()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_2.enabled = this.oPgFrm.Page1.oPag.oODBCDSN_1_2.mCond()
    this.oPgFrm.Page1.oPag.oPATH_1_3.enabled = this.oPgFrm.Page1.oPag.oPATH_1_3.mCond()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_4.enabled = this.oPgFrm.Page1.oPag.oODBCPATH_1_4.mCond()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_5.enabled = this.oPgFrm.Page1.oPag.oODBCUSER_1_5.mCond()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.enabled = this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.mCond()
    this.oPgFrm.Page1.oPag.oSELEVA_1_9.enabled = this.oPgFrm.Page1.oPag.oSELEVA_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSELENA_1_10.enabled = this.oPgFrm.Page1.oPag.oSELENA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oSELERU_1_11.enabled = this.oPgFrm.Page1.oPag.oSELERU_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSELEGN_1_12.enabled = this.oPgFrm.Page1.oPag.oSELEGN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oSELESN_1_13.enabled = this.oPgFrm.Page1.oPag.oSELESN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oSELEON_1_14.enabled = this.oPgFrm.Page1.oPag.oSELEON_1_14.mCond()
    this.oPgFrm.Page1.oPag.oSELETZ_1_15.enabled = this.oPgFrm.Page1.oPag.oSELETZ_1_15.mCond()
    this.oPgFrm.Page1.oPag.oSELENM_1_16.enabled = this.oPgFrm.Page1.oPag.oSELENM_1_16.mCond()
    this.oPgFrm.Page1.oPag.oSELERA_1_17.enabled = this.oPgFrm.Page1.oPag.oSELERA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oSELEIV_1_18.enabled = this.oPgFrm.Page1.oPag.oSELEIV_1_18.mCond()
    this.oPgFrm.Page1.oPag.oSELEPA_1_19.enabled = this.oPgFrm.Page1.oPag.oSELEPA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oSELEPD_1_20.enabled = this.oPgFrm.Page1.oPag.oSELEPD_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSELEPC_1_21.enabled = this.oPgFrm.Page1.oPag.oSELEPC_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSELECF_1_22.enabled = this.oPgFrm.Page1.oPag.oSELECF_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSELEDE_1_23.enabled = this.oPgFrm.Page1.oPag.oSELEDE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSELETU_1_24.enabled = this.oPgFrm.Page1.oPag.oSELETU_1_24.mCond()
    this.oPgFrm.Page1.oPag.oSELEGU_1_25.enabled = this.oPgFrm.Page1.oPag.oSELEGU_1_25.mCond()
    this.oPgFrm.Page1.oPag.oSELECC_1_26.enabled = this.oPgFrm.Page1.oPag.oSELECC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oSELEAC_1_27.enabled = this.oPgFrm.Page1.oPag.oSELEAC_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSELEKR_1_28.enabled = this.oPgFrm.Page1.oPag.oSELEKR_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSELEEC_1_29.enabled = this.oPgFrm.Page1.oPag.oSELEEC_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSELEC2_1_31.enabled = this.oPgFrm.Page1.oPag.oSELEC2_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSELEOG_1_32.enabled = this.oPgFrm.Page1.oPag.oSELEOG_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSELEST_1_33.enabled = this.oPgFrm.Page1.oPag.oSELEST_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSELEUB_1_34.enabled = this.oPgFrm.Page1.oPag.oSELEUB_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSELEUP_1_35.enabled = this.oPgFrm.Page1.oPag.oSELEUP_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSELESD_1_36.enabled = this.oPgFrm.Page1.oPag.oSELESD_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSELEDP_1_37.enabled = this.oPgFrm.Page1.oPag.oSELEDP_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSELEA1_1_38.enabled = this.oPgFrm.Page1.oPag.oSELEA1_1_38.mCond()
    this.oPgFrm.Page1.oPag.oSELEFP_1_42.enabled = this.oPgFrm.Page1.oPag.oSELEFP_1_42.mCond()
    this.oPgFrm.Page1.oPag.oSELEF1_1_43.enabled = this.oPgFrm.Page1.oPag.oSELEF1_1_43.mCond()
    this.oPgFrm.Page1.oPag.oSELEF2_1_44.enabled = this.oPgFrm.Page1.oPag.oSELEF2_1_44.mCond()
    this.oPgFrm.Page1.oPag.oSELEF3_1_45.enabled = this.oPgFrm.Page1.oPag.oSELEF3_1_45.mCond()
    this.oPgFrm.Page1.oPag.oSELEF4_1_46.enabled = this.oPgFrm.Page1.oPag.oSELEF4_1_46.mCond()
    this.oPgFrm.Page1.oPag.oSELEC1_1_47.enabled = this.oPgFrm.Page1.oPag.oSELEC1_1_47.mCond()
    this.oPgFrm.Page1.oPag.oSELESO_1_48.enabled = this.oPgFrm.Page1.oPag.oSELESO_1_48.mCond()
    this.oPgFrm.Page1.oPag.oSELERS_1_49.enabled = this.oPgFrm.Page1.oPag.oSELERS_1_49.mCond()
    this.oPgFrm.Page1.oPag.oSELET1_1_51.enabled = this.oPgFrm.Page1.oPag.oSELET1_1_51.mCond()
    this.oPgFrm.Page1.oPag.oSELEPM_1_52.enabled = this.oPgFrm.Page1.oPag.oSELEPM_1_52.mCond()
    this.oPgFrm.Page1.oPag.oSELEEV_1_54.enabled = this.oPgFrm.Page1.oPag.oSELEEV_1_54.mCond()
    this.oPgFrm.Page1.oPag.oSELEA6_1_55.enabled = this.oPgFrm.Page1.oPag.oSELEA6_1_55.mCond()
    this.oPgFrm.Page1.oPag.oSELEDT_1_56.enabled = this.oPgFrm.Page1.oPag.oSELEDT_1_56.mCond()
    this.oPgFrm.Page1.oPag.oSELEFR_1_57.enabled = this.oPgFrm.Page1.oPag.oSELEFR_1_57.mCond()
    this.oPgFrm.Page1.oPag.oSELEDO_1_58.enabled = this.oPgFrm.Page1.oPag.oSELEDO_1_58.mCond()
    this.oPgFrm.Page1.oPag.oSELETL_1_60.enabled = this.oPgFrm.Page1.oPag.oSELETL_1_60.mCond()
    this.oPgFrm.Page1.oPag.oSELECU_1_61.enabled = this.oPgFrm.Page1.oPag.oSELECU_1_61.mCond()
    this.oPgFrm.Page1.oPag.oSELET2_1_62.enabled = this.oPgFrm.Page1.oPag.oSELET2_1_62.mCond()
    this.oPgFrm.Page1.oPag.oSELEIP_1_63.enabled = this.oPgFrm.Page1.oPag.oSELEIP_1_63.mCond()
    this.oPgFrm.Page1.oPag.oSELEIA_1_64.enabled = this.oPgFrm.Page1.oPag.oSELEIA_1_64.mCond()
    this.oPgFrm.Page1.oPag.oSELEAT_1_65.enabled = this.oPgFrm.Page1.oPag.oSELEAT_1_65.mCond()
    this.oPgFrm.Page1.oPag.oSELEDR_1_66.enabled = this.oPgFrm.Page1.oPag.oSELEDR_1_66.mCond()
    this.oPgFrm.Page1.oPag.oSELEA5_1_67.enabled = this.oPgFrm.Page1.oPag.oSELEA5_1_67.mCond()
    this.oPgFrm.Page1.oPag.oSELEPE_1_68.enabled = this.oPgFrm.Page1.oPag.oSELEPE_1_68.mCond()
    this.oPgFrm.Page1.oPag.oSELEFY_1_69.enabled = this.oPgFrm.Page1.oPag.oSELEFY_1_69.mCond()
    this.oPgFrm.Page1.oPag.oSELEPP_1_70.enabled = this.oPgFrm.Page1.oPag.oSELEPP_1_70.mCond()
    this.oPgFrm.Page1.oPag.oSELEPZ_1_71.enabled = this.oPgFrm.Page1.oPag.oSELEPZ_1_71.mCond()
    this.oPgFrm.Page1.oPag.oSELEPN_1_73.enabled = this.oPgFrm.Page1.oPag.oSELEPN_1_73.mCond()
    this.oPgFrm.Page1.oPag.oSELEPT_1_74.enabled = this.oPgFrm.Page1.oPag.oSELEPT_1_74.mCond()
    this.oPgFrm.Page1.oPag.oSELEBM_1_76.enabled = this.oPgFrm.Page1.oPag.oSELEBM_1_76.mCond()
    this.oPgFrm.Page1.oPag.oSELEBR_1_77.enabled = this.oPgFrm.Page1.oPag.oSELEBR_1_77.mCond()
    this.oPgFrm.Page1.oPag.oSELEBU_1_78.enabled = this.oPgFrm.Page1.oPag.oSELEBU_1_78.mCond()
    this.oPgFrm.Page1.oPag.oSELEBE_1_79.enabled = this.oPgFrm.Page1.oPag.oSELEBE_1_79.mCond()
    this.oPgFrm.Page1.oPag.oSELEBT_1_80.enabled = this.oPgFrm.Page1.oPag.oSELEBT_1_80.mCond()
    this.oPgFrm.Page1.oPag.oSELEBS_1_81.enabled = this.oPgFrm.Page1.oPag.oSELEBS_1_81.mCond()
    this.oPgFrm.Page1.oPag.oSELEBC_1_82.enabled = this.oPgFrm.Page1.oPag.oSELEBC_1_82.mCond()
    this.oPgFrm.Page1.oPag.oSELEBB_1_83.enabled = this.oPgFrm.Page1.oPag.oSELEBB_1_83.mCond()
    this.oPgFrm.Page1.oPag.oSELECONT_1_87.enabled = this.oPgFrm.Page1.oPag.oSELECONT_1_87.mCond()
    this.oPgFrm.Page1.oPag.oSELERESO_1_88.enabled = this.oPgFrm.Page1.oPag.oSELERESO_1_88.mCond()
    this.oPgFrm.Page1.oPag.oIMPARIVA_1_89.enabled = this.oPgFrm.Page1.oPag.oIMPARIVA_1_89.mCond()
    this.oPgFrm.Page1.oPag.oIMAZZERA_1_90.enabled = this.oPgFrm.Page1.oPag.oIMAZZERA_1_90.mCond()
    this.oPgFrm.Page1.oPag.oIMTRANSA_1_91.enabled = this.oPgFrm.Page1.oPag.oIMTRANSA_1_91.mCond()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_92.enabled = this.oPgFrm.Page1.oPag.oIMCONFER_1_92.mCond()
    this.oPgFrm.Page1.oPag.oRADSELEZ_1_93.enabled_(this.oPgFrm.Page1.oPag.oRADSELEZ_1_93.mCond())
    this.oPgFrm.Page1.oPag.oSELECR_1_139.enabled = this.oPgFrm.Page1.oPag.oSELECR_1_139.mCond()
    this.oPgFrm.Page1.oPag.oSELECT_1_140.enabled = this.oPgFrm.Page1.oPag.oSELECT_1_140.mCond()
    this.oPgFrm.Page1.oPag.oSELECS_1_141.enabled = this.oPgFrm.Page1.oPag.oSELECS_1_141.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_96.enabled = this.oPgFrm.Page1.oPag.oBtn_1_96.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_97.enabled = this.oPgFrm.Page1.oPag.oBtn_1_97.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oODBCDSN_1_2.visible=!this.oPgFrm.Page1.oPag.oODBCDSN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oPATH_1_3.visible=!this.oPgFrm.Page1.oPag.oPATH_1_3.mHide()
    this.oPgFrm.Page1.oPag.oODBCPATH_1_4.visible=!this.oPgFrm.Page1.oPag.oODBCPATH_1_4.mHide()
    this.oPgFrm.Page1.oPag.oODBCUSER_1_5.visible=!this.oPgFrm.Page1.oPag.oODBCUSER_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.visible=!this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.mHide()
    this.oPgFrm.Page1.oPag.oIMCONFER_1_92.visible=!this.oPgFrm.Page1.oPag.oIMCONFER_1_92.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_98.visible=!this.oPgFrm.Page1.oPag.oBtn_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_110.visible=!this.oPgFrm.Page1.oPag.oStr_1_110.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_111.visible=!this.oPgFrm.Page1.oPag.oStr_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_144.visible=!this.oPgFrm.Page1.oPag.oStr_1_144.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_142.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_143.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODIMP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMPORTAZ_IDX,3]
    i_lTable = "IMPORTAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2], .t., this.IMPORTAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsim_mim',True,'IMPORTAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CODIMP))
          select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMPORTAZ','*','IMCODICE',cp_AbsName(oSource.parent,'oCODIMP_1_1'),i_cWhere,'gsim_mim',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMANNOTA,IMPARIVA,IMAZZERA,IMRESOCO,IMCONTRO,IMTIPSRC,IMTIPDBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(20))
      this.w_DESIMP = NVL(_Link_.IMANNOTA,space(60))
      this.w_IMPARIVA = NVL(_Link_.IMPARIVA,space(10))
      this.w_IMAZZERA = NVL(_Link_.IMAZZERA,space(10))
      this.w_SELERESO = NVL(_Link_.IMRESOCO,space(10))
      this.w_SELECONT = NVL(_Link_.IMCONTRO,space(10))
      this.w_TIPSRC = NVL(_Link_.IMTIPSRC,space(1))
      this.w_TIPDBF = NVL(_Link_.IMTIPDBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(20)
      endif
      this.w_DESIMP = space(60)
      this.w_IMPARIVA = space(10)
      this.w_IMAZZERA = space(10)
      this.w_SELERESO = space(10)
      this.w_SELECONT = space(10)
      this.w_TIPSRC = space(1)
      this.w_TIPDBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMPORTAZ_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMPORTAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODIMP_1_1.value==this.w_CODIMP)
      this.oPgFrm.Page1.oPag.oCODIMP_1_1.value=this.w_CODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCDSN_1_2.value==this.w_ODBCDSN)
      this.oPgFrm.Page1.oPag.oODBCDSN_1_2.value=this.w_ODBCDSN
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_3.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_3.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPATH_1_4.value==this.w_ODBCPATH)
      this.oPgFrm.Page1.oPag.oODBCPATH_1_4.value=this.w_ODBCPATH
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCUSER_1_5.value==this.w_ODBCUSER)
      this.oPgFrm.Page1.oPag.oODBCUSER_1_5.value=this.w_ODBCUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.value==this.w_ODBCPASSW)
      this.oPgFrm.Page1.oPag.oODBCPASSW_1_7.value=this.w_ODBCPASSW
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEVA_1_9.RadioValue()==this.w_SELEVA)
      this.oPgFrm.Page1.oPag.oSELEVA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENA_1_10.RadioValue()==this.w_SELENA)
      this.oPgFrm.Page1.oPag.oSELENA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERU_1_11.RadioValue()==this.w_SELERU)
      this.oPgFrm.Page1.oPag.oSELERU_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEGN_1_12.RadioValue()==this.w_SELEGN)
      this.oPgFrm.Page1.oPag.oSELEGN_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESN_1_13.RadioValue()==this.w_SELESN)
      this.oPgFrm.Page1.oPag.oSELESN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEON_1_14.RadioValue()==this.w_SELEON)
      this.oPgFrm.Page1.oPag.oSELEON_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETZ_1_15.RadioValue()==this.w_SELETZ)
      this.oPgFrm.Page1.oPag.oSELETZ_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELENM_1_16.RadioValue()==this.w_SELENM)
      this.oPgFrm.Page1.oPag.oSELENM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERA_1_17.RadioValue()==this.w_SELERA)
      this.oPgFrm.Page1.oPag.oSELERA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIV_1_18.RadioValue()==this.w_SELEIV)
      this.oPgFrm.Page1.oPag.oSELEIV_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPA_1_19.RadioValue()==this.w_SELEPA)
      this.oPgFrm.Page1.oPag.oSELEPA_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPD_1_20.RadioValue()==this.w_SELEPD)
      this.oPgFrm.Page1.oPag.oSELEPD_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPC_1_21.RadioValue()==this.w_SELEPC)
      this.oPgFrm.Page1.oPag.oSELEPC_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECF_1_22.RadioValue()==this.w_SELECF)
      this.oPgFrm.Page1.oPag.oSELECF_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDE_1_23.RadioValue()==this.w_SELEDE)
      this.oPgFrm.Page1.oPag.oSELEDE_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETU_1_24.RadioValue()==this.w_SELETU)
      this.oPgFrm.Page1.oPag.oSELETU_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEGU_1_25.RadioValue()==this.w_SELEGU)
      this.oPgFrm.Page1.oPag.oSELEGU_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECC_1_26.RadioValue()==this.w_SELECC)
      this.oPgFrm.Page1.oPag.oSELECC_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAC_1_27.RadioValue()==this.w_SELEAC)
      this.oPgFrm.Page1.oPag.oSELEAC_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEKR_1_28.RadioValue()==this.w_SELEKR)
      this.oPgFrm.Page1.oPag.oSELEKR_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEEC_1_29.RadioValue()==this.w_SELEEC)
      this.oPgFrm.Page1.oPag.oSELEEC_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEC2_1_31.RadioValue()==this.w_SELEC2)
      this.oPgFrm.Page1.oPag.oSELEC2_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEOG_1_32.RadioValue()==this.w_SELEOG)
      this.oPgFrm.Page1.oPag.oSELEOG_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEST_1_33.RadioValue()==this.w_SELEST)
      this.oPgFrm.Page1.oPag.oSELEST_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEUB_1_34.RadioValue()==this.w_SELEUB)
      this.oPgFrm.Page1.oPag.oSELEUB_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEUP_1_35.RadioValue()==this.w_SELEUP)
      this.oPgFrm.Page1.oPag.oSELEUP_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESD_1_36.RadioValue()==this.w_SELESD)
      this.oPgFrm.Page1.oPag.oSELESD_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDP_1_37.RadioValue()==this.w_SELEDP)
      this.oPgFrm.Page1.oPag.oSELEDP_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEA1_1_38.RadioValue()==this.w_SELEA1)
      this.oPgFrm.Page1.oPag.oSELEA1_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEFP_1_42.RadioValue()==this.w_SELEFP)
      this.oPgFrm.Page1.oPag.oSELEFP_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEF1_1_43.RadioValue()==this.w_SELEF1)
      this.oPgFrm.Page1.oPag.oSELEF1_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEF2_1_44.RadioValue()==this.w_SELEF2)
      this.oPgFrm.Page1.oPag.oSELEF2_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEF3_1_45.RadioValue()==this.w_SELEF3)
      this.oPgFrm.Page1.oPag.oSELEF3_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEF4_1_46.RadioValue()==this.w_SELEF4)
      this.oPgFrm.Page1.oPag.oSELEF4_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEC1_1_47.RadioValue()==this.w_SELEC1)
      this.oPgFrm.Page1.oPag.oSELEC1_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELESO_1_48.RadioValue()==this.w_SELESO)
      this.oPgFrm.Page1.oPag.oSELESO_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERS_1_49.RadioValue()==this.w_SELERS)
      this.oPgFrm.Page1.oPag.oSELERS_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELET1_1_51.RadioValue()==this.w_SELET1)
      this.oPgFrm.Page1.oPag.oSELET1_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPM_1_52.RadioValue()==this.w_SELEPM)
      this.oPgFrm.Page1.oPag.oSELEPM_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEEV_1_54.RadioValue()==this.w_SELEEV)
      this.oPgFrm.Page1.oPag.oSELEEV_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEA6_1_55.RadioValue()==this.w_SELEA6)
      this.oPgFrm.Page1.oPag.oSELEA6_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDT_1_56.RadioValue()==this.w_SELEDT)
      this.oPgFrm.Page1.oPag.oSELEDT_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEFR_1_57.RadioValue()==this.w_SELEFR)
      this.oPgFrm.Page1.oPag.oSELEFR_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDO_1_58.RadioValue()==this.w_SELEDO)
      this.oPgFrm.Page1.oPag.oSELEDO_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELETL_1_60.RadioValue()==this.w_SELETL)
      this.oPgFrm.Page1.oPag.oSELETL_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECU_1_61.RadioValue()==this.w_SELECU)
      this.oPgFrm.Page1.oPag.oSELECU_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELET2_1_62.RadioValue()==this.w_SELET2)
      this.oPgFrm.Page1.oPag.oSELET2_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIP_1_63.RadioValue()==this.w_SELEIP)
      this.oPgFrm.Page1.oPag.oSELEIP_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEIA_1_64.RadioValue()==this.w_SELEIA)
      this.oPgFrm.Page1.oPag.oSELEIA_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEAT_1_65.RadioValue()==this.w_SELEAT)
      this.oPgFrm.Page1.oPag.oSELEAT_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEDR_1_66.RadioValue()==this.w_SELEDR)
      this.oPgFrm.Page1.oPag.oSELEDR_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEA5_1_67.RadioValue()==this.w_SELEA5)
      this.oPgFrm.Page1.oPag.oSELEA5_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPE_1_68.RadioValue()==this.w_SELEPE)
      this.oPgFrm.Page1.oPag.oSELEPE_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEFY_1_69.RadioValue()==this.w_SELEFY)
      this.oPgFrm.Page1.oPag.oSELEFY_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPP_1_70.RadioValue()==this.w_SELEPP)
      this.oPgFrm.Page1.oPag.oSELEPP_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPZ_1_71.RadioValue()==this.w_SELEPZ)
      this.oPgFrm.Page1.oPag.oSELEPZ_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPN_1_73.RadioValue()==this.w_SELEPN)
      this.oPgFrm.Page1.oPag.oSELEPN_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEPT_1_74.RadioValue()==this.w_SELEPT)
      this.oPgFrm.Page1.oPag.oSELEPT_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBM_1_76.RadioValue()==this.w_SELEBM)
      this.oPgFrm.Page1.oPag.oSELEBM_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBR_1_77.RadioValue()==this.w_SELEBR)
      this.oPgFrm.Page1.oPag.oSELEBR_1_77.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBU_1_78.RadioValue()==this.w_SELEBU)
      this.oPgFrm.Page1.oPag.oSELEBU_1_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBE_1_79.RadioValue()==this.w_SELEBE)
      this.oPgFrm.Page1.oPag.oSELEBE_1_79.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBT_1_80.RadioValue()==this.w_SELEBT)
      this.oPgFrm.Page1.oPag.oSELEBT_1_80.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBS_1_81.RadioValue()==this.w_SELEBS)
      this.oPgFrm.Page1.oPag.oSELEBS_1_81.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBC_1_82.RadioValue()==this.w_SELEBC)
      this.oPgFrm.Page1.oPag.oSELEBC_1_82.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEBB_1_83.RadioValue()==this.w_SELEBB)
      this.oPgFrm.Page1.oPag.oSELEBB_1_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECONT_1_87.RadioValue()==this.w_SELECONT)
      this.oPgFrm.Page1.oPag.oSELECONT_1_87.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELERESO_1_88.RadioValue()==this.w_SELERESO)
      this.oPgFrm.Page1.oPag.oSELERESO_1_88.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPARIVA_1_89.RadioValue()==this.w_IMPARIVA)
      this.oPgFrm.Page1.oPag.oIMPARIVA_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAZZERA_1_90.RadioValue()==this.w_IMAZZERA)
      this.oPgFrm.Page1.oPag.oIMAZZERA_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTRANSA_1_91.RadioValue()==this.w_IMTRANSA)
      this.oPgFrm.Page1.oPag.oIMTRANSA_1_91.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCONFER_1_92.RadioValue()==this.w_IMCONFER)
      this.oPgFrm.Page1.oPag.oIMCONFER_1_92.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRADSELEZ_1_93.RadioValue()==this.w_RADSELEZ)
      this.oPgFrm.Page1.oPag.oRADSELEZ_1_93.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_101.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_101.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECR_1_139.RadioValue()==this.w_SELECR)
      this.oPgFrm.Page1.oPag.oSELECR_1_139.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECT_1_140.RadioValue()==this.w_SELECT)
      this.oPgFrm.Page1.oPag.oSELECT_1_140.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELECS_1_141.RadioValue()==this.w_SELECS)
      this.oPgFrm.Page1.oPag.oSELECS_1_141.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODIMP))  and (!.w_INCORSO)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIMP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODIMP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsim_kaePag1 as StdContainer
  Width  = 803
  height = 604
  stdWidth  = 803
  stdheight = 604
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODIMP_1_1 as StdField with uid="JVRUITIVQR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODIMP", cQueryName = "CODIMP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 221500454,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=98, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="IMPORTAZ", cZoomOnZoom="gsim_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_CODIMP"

  func oCODIMP_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  func oCODIMP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIMP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIMP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMPORTAZ','*','IMCODICE',cp_AbsName(this.parent,'oCODIMP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsim_mim',"",'',this.parent.oContained
  endproc
  proc oCODIMP_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsim_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CODIMP
     i_obj.ecpSave()
  endproc

  add object oODBCDSN_1_2 as StdField with uid="CCNSWIOJJL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ODBCDSN", cQueryName = "ODBCDSN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Specificare la fonte dati ODBC (data source name)",;
    HelpContextID = 6444570,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=99, Top=42, InputMask=replicate('X',30)

  func oODBCDSN_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCDSN_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oPATH_1_3 as StdField with uid="TEKEDCGFLL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 127678730,;
   bGlobalFont=.t.,;
    Height=21, Width=603, Left=98, Top=42, InputMask=replicate('X',200)

  func oPATH_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC<>"O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oPATH_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oODBCPATH_1_4 as StdField with uid="OTCZZZYUDO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ODBCPATH", cQueryName = "ODBCPATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzato la lettura avviene direttamente da file DBF",;
    HelpContextID = 241019438,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=99, Top=65, InputMask=replicate('X',200)

  func oODBCPATH_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDBF='S')
    endwith
   endif
  endfunc

  func oODBCPATH_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oODBCUSER_1_5 as StdField with uid="KUQMZDJHWU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ODBCUSER", cQueryName = "ODBCUSER",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome utente per connessione ODBC",;
    HelpContextID = 11381304,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=493, Top=42, InputMask=replicate('X',30)

  func oODBCUSER_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCUSER_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc


  add object oBtn_1_6 as StdButton with uid="SJQLZNXFRO",left=710, top=42, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 132558378;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .w_PATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPSRC<>"O" and !.w_INCORSO)
      endwith
    endif
  endfunc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPSRC="O")
     endwith
    endif
  endfunc

  add object oODBCPASSW_1_7 as StdField with uid="DLZRZHNCSZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ODBCPASSW", cQueryName = "ODBCPASSW",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Password per connessione ODBC",;
    HelpContextID = 241020841,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=493, Top=65, InputMask=replicate('X',30), PasswordChar='*'

  func oODBCPASSW_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSRC="O" and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oODBCPASSW_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oSELEVA_1_9 as StdCheck with uid="VLSCNCXOLM",rtseq=7,rtrep=.f.,left=18, top=116, caption="Valute",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 20952282,;
    cFormVar="w_SELEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEVA_1_9.RadioValue()
    return(iif(this.value =1,'VA',;
    ' '))
  endfunc
  func oSELEVA_1_9.GetRadio()
    this.Parent.oContained.w_SELEVA = this.RadioValue()
    return .t.
  endfunc

  func oSELEVA_1_9.SetRadio()
    this.Parent.oContained.w_SELEVA=trim(this.Parent.oContained.w_SELEVA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEVA=='VA',1,;
      0)
  endfunc

  func oSELEVA_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEVA<>"|")
    endwith
   endif
  endfunc

  add object oSELENA_1_10 as StdCheck with uid="RROMUCHGJY",rtseq=8,rtrep=.f.,left=18, top=135, caption="Nazioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 29340890,;
    cFormVar="w_SELENA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENA_1_10.RadioValue()
    return(iif(this.value =1,'NA',;
    ' '))
  endfunc
  func oSELENA_1_10.GetRadio()
    this.Parent.oContained.w_SELENA = this.RadioValue()
    return .t.
  endfunc

  func oSELENA_1_10.SetRadio()
    this.Parent.oContained.w_SELENA=trim(this.Parent.oContained.w_SELENA)
    this.value = ;
      iif(this.Parent.oContained.w_SELENA=='NA',1,;
      0)
  endfunc

  func oSELENA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENA<>"|")
    endwith
   endif
  endfunc

  add object oSELERU_1_11 as StdCheck with uid="HQGMTTQBFB",rtseq=9,rtrep=.f.,left=18, top=154, caption="Persone-Utenti-Gruppi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 41962278,;
    cFormVar="w_SELERU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERU_1_11.RadioValue()
    return(iif(this.value =1,'RU',;
    ' '))
  endfunc
  func oSELERU_1_11.GetRadio()
    this.Parent.oContained.w_SELERU = this.RadioValue()
    return .t.
  endfunc

  func oSELERU_1_11.SetRadio()
    this.Parent.oContained.w_SELERU=trim(this.Parent.oContained.w_SELERU)
    this.value = ;
      iif(this.Parent.oContained.w_SELERU=='RU',1,;
      0)
  endfunc

  func oSELERU_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERU<>"|")
    endwith
   endif
  endfunc

  add object oSELEGN_1_12 as StdCheck with uid="DLGVBYOFOP",rtseq=10,rtrep=.f.,left=18, top=173, caption="Gruppi nominativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 181422886,;
    cFormVar="w_SELEGN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEGN_1_12.RadioValue()
    return(iif(this.value =1,'GN',;
    ' '))
  endfunc
  func oSELEGN_1_12.GetRadio()
    this.Parent.oContained.w_SELEGN = this.RadioValue()
    return .t.
  endfunc

  func oSELEGN_1_12.SetRadio()
    this.Parent.oContained.w_SELEGN=trim(this.Parent.oContained.w_SELEGN)
    this.value = ;
      iif(this.Parent.oContained.w_SELEGN=='GN',1,;
      0)
  endfunc

  func oSELEGN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEGN<>"|")
    endwith
   endif
  endfunc

  add object oSELESN_1_13 as StdCheck with uid="THJLFEAINX",rtseq=11,rtrep=.f.,left=18, top=193, caption="Formule di cortesia",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 194005798,;
    cFormVar="w_SELESN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESN_1_13.RadioValue()
    return(iif(this.value =1,'SN',;
    ' '))
  endfunc
  func oSELESN_1_13.GetRadio()
    this.Parent.oContained.w_SELESN = this.RadioValue()
    return .t.
  endfunc

  func oSELESN_1_13.SetRadio()
    this.Parent.oContained.w_SELESN=trim(this.Parent.oContained.w_SELESN)
    this.value = ;
      iif(this.Parent.oContained.w_SELESN=='SN',1,;
      0)
  endfunc

  func oSELESN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESN<>"|")
    endwith
   endif
  endfunc

  add object oSELEON_1_14 as StdCheck with uid="ANOWIGEGUN",rtseq=12,rtrep=.f.,left=18, top=211, caption="Origine nominativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 189811494,;
    cFormVar="w_SELEON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEON_1_14.RadioValue()
    return(iif(this.value =1,'ON',;
    ' '))
  endfunc
  func oSELEON_1_14.GetRadio()
    this.Parent.oContained.w_SELEON = this.RadioValue()
    return .t.
  endfunc

  func oSELEON_1_14.SetRadio()
    this.Parent.oContained.w_SELEON=trim(this.Parent.oContained.w_SELEON)
    this.value = ;
      iif(this.Parent.oContained.w_SELEON=='ON',1,;
      0)
  endfunc

  func oSELEON_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEON<>"|")
    endwith
   endif
  endfunc

  add object oSELETZ_1_15 as StdCheck with uid="ZPMSLKQTNN",rtseq=13,rtrep=.f.,left=18, top=230, caption="Categorie attributi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 127945510,;
    cFormVar="w_SELETZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETZ_1_15.RadioValue()
    return(iif(this.value =1,'TZ',;
    ' '))
  endfunc
  func oSELETZ_1_15.GetRadio()
    this.Parent.oContained.w_SELETZ = this.RadioValue()
    return .t.
  endfunc

  func oSELETZ_1_15.SetRadio()
    this.Parent.oContained.w_SELETZ=trim(this.Parent.oContained.w_SELETZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELETZ=='TZ',1,;
      0)
  endfunc

  func oSELETZ_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETZ<>"|")
    endwith
   endif
  endfunc

  add object oSELENM_1_16 as StdCheck with uid="YXEDLPFGCE",rtseq=14,rtrep=.f.,left=18, top=249, caption="Nominativi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 171985702,;
    cFormVar="w_SELENM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELENM_1_16.RadioValue()
    return(iif(this.value =1,'NM',;
    ' '))
  endfunc
  func oSELENM_1_16.GetRadio()
    this.Parent.oContained.w_SELENM = this.RadioValue()
    return .t.
  endfunc

  func oSELENM_1_16.SetRadio()
    this.Parent.oContained.w_SELENM=trim(this.Parent.oContained.w_SELENM)
    this.value = ;
      iif(this.Parent.oContained.w_SELENM=='NM',1,;
      0)
  endfunc

  func oSELENM_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELENM<>"|")
    endwith
   endif
  endfunc

  add object oSELERA_1_17 as StdCheck with uid="VJIAHQBWWS",rtseq=15,rtrep=.f.,left=18, top=268, caption="Nominativi (persone)",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 25146586,;
    cFormVar="w_SELERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERA_1_17.RadioValue()
    return(iif(this.value =1,'RA',;
    ' '))
  endfunc
  func oSELERA_1_17.GetRadio()
    this.Parent.oContained.w_SELERA = this.RadioValue()
    return .t.
  endfunc

  func oSELERA_1_17.SetRadio()
    this.Parent.oContained.w_SELERA=trim(this.Parent.oContained.w_SELERA)
    this.value = ;
      iif(this.Parent.oContained.w_SELERA=='RA',1,;
      0)
  endfunc

  func oSELERA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERA<>"|")
    endwith
   endif
  endfunc

  add object oSELEIV_1_18 as StdCheck with uid="YWBRQQXDEN",rtseq=16,rtrep=.f.,left=18, top=287, caption="Codici IVA",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 49302310,;
    cFormVar="w_SELEIV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIV_1_18.RadioValue()
    return(iif(this.value =1,'IV',;
    ' '))
  endfunc
  func oSELEIV_1_18.GetRadio()
    this.Parent.oContained.w_SELEIV = this.RadioValue()
    return .t.
  endfunc

  func oSELEIV_1_18.SetRadio()
    this.Parent.oContained.w_SELEIV=trim(this.Parent.oContained.w_SELEIV)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIV=='IV',1,;
      0)
  endfunc

  func oSELEIV_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIV<>"|")
    endwith
   endif
  endfunc

  add object oSELEPA_1_19 as StdCheck with uid="GCPRGDIHEQ",rtseq=17,rtrep=.f.,left=18, top=306, caption="Pagamenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 27243738,;
    cFormVar="w_SELEPA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPA_1_19.RadioValue()
    return(iif(this.value =1,'PA',;
    ' '))
  endfunc
  func oSELEPA_1_19.GetRadio()
    this.Parent.oContained.w_SELEPA = this.RadioValue()
    return .t.
  endfunc

  func oSELEPA_1_19.SetRadio()
    this.Parent.oContained.w_SELEPA=trim(this.Parent.oContained.w_SELEPA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPA=='PA',1,;
      0)
  endfunc

  func oSELEPA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPA<>"|")
    endwith
   endif
  endfunc

  add object oSELEPD_1_20 as StdCheck with uid="WUQVZUVXWH",rtseq=18,rtrep=.f.,left=18, top=325, caption="Pagamenti dettaglio",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 23087910,;
    cFormVar="w_SELEPD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPD_1_20.RadioValue()
    return(iif(this.value =1,'PD',;
    ' '))
  endfunc
  func oSELEPD_1_20.GetRadio()
    this.Parent.oContained.w_SELEPD = this.RadioValue()
    return .t.
  endfunc

  func oSELEPD_1_20.SetRadio()
    this.Parent.oContained.w_SELEPD=trim(this.Parent.oContained.w_SELEPD)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPD=='PD',1,;
      0)
  endfunc

  func oSELEPD_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPD<>"|")
    endwith
   endif
  endfunc

  add object oSELEPC_1_21 as StdCheck with uid="GPSEFEULGX",rtseq=19,rtrep=.f.,left=18, top=344, caption="Piano dei conti / mastri e conti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 6310694,;
    cFormVar="w_SELEPC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPC_1_21.RadioValue()
    return(iif(this.value =1,'PC',;
    ' '))
  endfunc
  func oSELEPC_1_21.GetRadio()
    this.Parent.oContained.w_SELEPC = this.RadioValue()
    return .t.
  endfunc

  func oSELEPC_1_21.SetRadio()
    this.Parent.oContained.w_SELEPC=trim(this.Parent.oContained.w_SELEPC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPC=='PC',1,;
      0)
  endfunc

  func oSELEPC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPC<>"|")
    endwith
   endif
  endfunc

  add object oSELECF_1_22 as StdCheck with uid="PDNDFOGEJJ",rtseq=20,rtrep=.f.,left=18, top=362, caption="Clienti e fornitori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 43010854,;
    cFormVar="w_SELECF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECF_1_22.RadioValue()
    return(iif(this.value =1,'CF',;
    ' '))
  endfunc
  func oSELECF_1_22.GetRadio()
    this.Parent.oContained.w_SELECF = this.RadioValue()
    return .t.
  endfunc

  func oSELECF_1_22.SetRadio()
    this.Parent.oContained.w_SELECF=trim(this.Parent.oContained.w_SELECF)
    this.value = ;
      iif(this.Parent.oContained.w_SELECF=='CF',1,;
      0)
  endfunc

  func oSELECF_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECF<>"|")
    endwith
   endif
  endfunc

  add object oSELEDE_1_23 as StdCheck with uid="AZZQJONGNI",rtseq=21,rtrep=.f.,left=18, top=382, caption="Sedi cliente",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 27282214,;
    cFormVar="w_SELEDE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDE_1_23.RadioValue()
    return(iif(this.value =1,'DE',;
    ' '))
  endfunc
  func oSELEDE_1_23.GetRadio()
    this.Parent.oContained.w_SELEDE = this.RadioValue()
    return .t.
  endfunc

  func oSELEDE_1_23.SetRadio()
    this.Parent.oContained.w_SELEDE=trim(this.Parent.oContained.w_SELEDE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDE=='DE',1,;
      0)
  endfunc

  func oSELEDE_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDE<>"|")
    endwith
   endif
  endfunc

  add object oSELETU_1_24 as StdCheck with uid="QXSJTLNYYX",rtseq=22,rtrep=.f.,left=18, top=401, caption="Codice tributo nei fornitori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 44059430,;
    cFormVar="w_SELETU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETU_1_24.RadioValue()
    return(iif(this.value =1,'TU',;
    ' '))
  endfunc
  func oSELETU_1_24.GetRadio()
    this.Parent.oContained.w_SELETU = this.RadioValue()
    return .t.
  endfunc

  func oSELETU_1_24.SetRadio()
    this.Parent.oContained.w_SELETU=trim(this.Parent.oContained.w_SELETU)
    this.value = ;
      iif(this.Parent.oContained.w_SELETU=='TU',1,;
      0)
  endfunc

  func oSELETU_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETU<>"|")
    endwith
   endif
  endfunc

  add object oSELEGU_1_25 as StdCheck with uid="ANJYOYTNYQ",rtseq=23,rtrep=.f.,left=18, top=421, caption="Giudici",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 30427942,;
    cFormVar="w_SELEGU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEGU_1_25.RadioValue()
    return(iif(this.value =1,'GU',;
    ' '))
  endfunc
  func oSELEGU_1_25.GetRadio()
    this.Parent.oContained.w_SELEGU = this.RadioValue()
    return .t.
  endfunc

  func oSELEGU_1_25.SetRadio()
    this.Parent.oContained.w_SELEGU=trim(this.Parent.oContained.w_SELEGU)
    this.value = ;
      iif(this.Parent.oContained.w_SELEGU=='GU',1,;
      0)
  endfunc

  func oSELEGU_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEGU<>"|")
    endwith
   endif
  endfunc

  add object oSELECC_1_26 as StdCheck with uid="BVAYFXCROB",rtseq=24,rtrep=.f.,left=18, top=440, caption="Causali contabili",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7320794,;
    cFormVar="w_SELECC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECC_1_26.RadioValue()
    return(iif(this.value =1,'CC',;
    ' '))
  endfunc
  func oSELECC_1_26.GetRadio()
    this.Parent.oContained.w_SELECC = this.RadioValue()
    return .t.
  endfunc

  func oSELECC_1_26.SetRadio()
    this.Parent.oContained.w_SELECC=trim(this.Parent.oContained.w_SELECC)
    this.value = ;
      iif(this.Parent.oContained.w_SELECC=='CC',1,;
      0)
  endfunc

  func oSELECC_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECC<>"|")
    endwith
   endif
  endfunc

  add object oSELEAC_1_27 as StdCheck with uid="OYFCHPNVOZ",rtseq=25,rtrep=.f.,left=18, top=459, caption="Automatismi / modelli contabili",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 9417946,;
    cFormVar="w_SELEAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAC_1_27.RadioValue()
    return(iif(this.value =1,'AC',;
    ' '))
  endfunc
  func oSELEAC_1_27.GetRadio()
    this.Parent.oContained.w_SELEAC = this.RadioValue()
    return .t.
  endfunc

  func oSELEAC_1_27.SetRadio()
    this.Parent.oContained.w_SELEAC=trim(this.Parent.oContained.w_SELEAC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAC=='AC',1,;
      0)
  endfunc

  func oSELEAC_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAC<>"|")
    endwith
   endif
  endfunc

  add object oSELEKR_1_28 as StdCheck with uid="BEKDAVKPWQ",rtseq=26,rtrep=.f.,left=18, top=478, caption="Colonne cronologico",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 252726054,;
    cFormVar="w_SELEKR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEKR_1_28.RadioValue()
    return(iif(this.value =1,'KR',;
    ' '))
  endfunc
  func oSELEKR_1_28.GetRadio()
    this.Parent.oContained.w_SELEKR = this.RadioValue()
    return .t.
  endfunc

  func oSELEKR_1_28.SetRadio()
    this.Parent.oContained.w_SELEKR=trim(this.Parent.oContained.w_SELEKR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEKR=='KR',1,;
      0)
  endfunc

  func oSELEKR_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEKR<>"|")
    endwith
   endif
  endfunc

  add object oSELEEC_1_29 as StdCheck with uid="YDQNXDZIYM",rtseq=27,rtrep=.f.,left=18, top=497, caption="Causali da escludere",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 5223642,;
    cFormVar="w_SELEEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEEC_1_29.RadioValue()
    return(iif(this.value =1,'EC',;
    ' '))
  endfunc
  func oSELEEC_1_29.GetRadio()
    this.Parent.oContained.w_SELEEC = this.RadioValue()
    return .t.
  endfunc

  func oSELEEC_1_29.SetRadio()
    this.Parent.oContained.w_SELEEC=trim(this.Parent.oContained.w_SELEEC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEEC=='EC',1,;
      0)
  endfunc

  func oSELEEC_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEEC<>"|")
    endwith
   endif
  endfunc

  add object oSELEC2_1_31 as StdCheck with uid="WYSMZQNAPV",rtseq=28,rtrep=.f.,left=219, top=116, caption="Classificazione pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 24098010,;
    cFormVar="w_SELEC2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEC2_1_31.RadioValue()
    return(iif(this.value =1,'C2',;
    ' '))
  endfunc
  func oSELEC2_1_31.GetRadio()
    this.Parent.oContained.w_SELEC2 = this.RadioValue()
    return .t.
  endfunc

  func oSELEC2_1_31.SetRadio()
    this.Parent.oContained.w_SELEC2=trim(this.Parent.oContained.w_SELEC2)
    this.value = ;
      iif(this.Parent.oContained.w_SELEC2=='C2',1,;
      0)
  endfunc

  func oSELEC2_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEC2<>"|")
    endwith
   endif
  endfunc

  add object oSELEOG_1_32 as StdCheck with uid="OFIFXZLLAR",rtseq=29,rtrep=.f.,left=219, top=135, caption="Oggetti pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 72370982,;
    cFormVar="w_SELEOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEOG_1_32.RadioValue()
    return(iif(this.value =1,'OG',;
    ' '))
  endfunc
  func oSELEOG_1_32.GetRadio()
    this.Parent.oContained.w_SELEOG = this.RadioValue()
    return .t.
  endfunc

  func oSELEOG_1_32.SetRadio()
    this.Parent.oContained.w_SELEOG=trim(this.Parent.oContained.w_SELEOG)
    this.value = ;
      iif(this.Parent.oContained.w_SELEOG=='OG',1,;
      0)
  endfunc

  func oSELEOG_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEOG<>"|")
    endwith
   endif
  endfunc

  add object oSELEST_1_33 as StdCheck with uid="QQMHCIFVGA",rtseq=30,rtrep=.f.,left=219, top=154, caption="Stati pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 26233638,;
    cFormVar="w_SELEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEST_1_33.RadioValue()
    return(iif(this.value =1,'ST',;
    ' '))
  endfunc
  func oSELEST_1_33.GetRadio()
    this.Parent.oContained.w_SELEST = this.RadioValue()
    return .t.
  endfunc

  func oSELEST_1_33.SetRadio()
    this.Parent.oContained.w_SELEST=trim(this.Parent.oContained.w_SELEST)
    this.value = ;
      iif(this.Parent.oContained.w_SELEST=='ST',1,;
      0)
  endfunc

  func oSELEST_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEST<>"|")
    endwith
   endif
  endfunc

  add object oSELEUB_1_34 as StdCheck with uid="GTKCNQMJMY",rtseq=31,rtrep=.f.,left=219, top=173, caption="Ubicazioni pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 5223642,;
    cFormVar="w_SELEUB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEUB_1_34.RadioValue()
    return(iif(this.value =1,'UB',;
    ' '))
  endfunc
  func oSELEUB_1_34.GetRadio()
    this.Parent.oContained.w_SELEUB = this.RadioValue()
    return .t.
  endfunc

  func oSELEUB_1_34.SetRadio()
    this.Parent.oContained.w_SELEUB=trim(this.Parent.oContained.w_SELEUB)
    this.value = ;
      iif(this.Parent.oContained.w_SELEUB=='UB',1,;
      0)
  endfunc

  func oSELEUB_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEUB<>"|")
    endwith
   endif
  endfunc

  add object oSELEUP_1_35 as StdCheck with uid="KCWKXGEXJN",rtseq=32,rtrep=.f.,left=219, top=192, caption="Uffici/sezioni pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 229657382,;
    cFormVar="w_SELEUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEUP_1_35.RadioValue()
    return(iif(this.value =1,'UP',;
    ' '))
  endfunc
  func oSELEUP_1_35.GetRadio()
    this.Parent.oContained.w_SELEUP = this.RadioValue()
    return .t.
  endfunc

  func oSELEUP_1_35.SetRadio()
    this.Parent.oContained.w_SELEUP=trim(this.Parent.oContained.w_SELEUP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEUP=='UP',1,;
      0)
  endfunc

  func oSELEUP_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEUP<>"|")
    endwith
   endif
  endfunc

  add object oSELESD_1_36 as StdCheck with uid="TFHHVTFIUS",rtseq=33,rtrep=.f.,left=219, top=211, caption="Sedi pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 26233638,;
    cFormVar="w_SELESD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESD_1_36.RadioValue()
    return(iif(this.value =1,'SD',;
    ' '))
  endfunc
  func oSELESD_1_36.GetRadio()
    this.Parent.oContained.w_SELESD = this.RadioValue()
    return .t.
  endfunc

  func oSELESD_1_36.SetRadio()
    this.Parent.oContained.w_SELESD=trim(this.Parent.oContained.w_SELESD)
    this.value = ;
      iif(this.Parent.oContained.w_SELESD=='SD',1,;
      0)
  endfunc

  func oSELESD_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESD<>"|")
    endwith
   endif
  endfunc

  add object oSELEDP_1_37 as StdCheck with uid="YDXLBODQOK",rtseq=34,rtrep=.f.,left=219, top=230, caption="Dipartimenti pratica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 211831590,;
    cFormVar="w_SELEDP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDP_1_37.RadioValue()
    return(iif(this.value =1,'DP',;
    ' '))
  endfunc
  func oSELEDP_1_37.GetRadio()
    this.Parent.oContained.w_SELEDP = this.RadioValue()
    return .t.
  endfunc

  func oSELEDP_1_37.SetRadio()
    this.Parent.oContained.w_SELEDP=trim(this.Parent.oContained.w_SELEDP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDP=='DP',1,;
      0)
  endfunc

  func oSELEDP_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDP<>"|")
    endwith
   endif
  endfunc

  add object oSELEA1_1_38 as StdCheck with uid="KXGWHJQYVV",rtseq=35,rtrep=.f.,left=219, top=249, caption="Campi liberi alfanumerici",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 42972378,;
    cFormVar="w_SELEA1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEA1_1_38.RadioValue()
    return(iif(this.value =1,'A1',;
    ' '))
  endfunc
  func oSELEA1_1_38.GetRadio()
    this.Parent.oContained.w_SELEA1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEA1_1_38.SetRadio()
    this.Parent.oContained.w_SELEA1=trim(this.Parent.oContained.w_SELEA1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEA1=='A1',1,;
      0)
  endfunc

  func oSELEA1_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEA1<>"|")
    endwith
   endif
  endfunc

  add object oSELEFP_1_42 as StdCheck with uid="HXJQQARKFP",rtseq=39,rtrep=.f.,left=219, top=268, caption="Pratiche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 213928742,;
    cFormVar="w_SELEFP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEFP_1_42.RadioValue()
    return(iif(this.value =1,'FP',;
    ' '))
  endfunc
  func oSELEFP_1_42.GetRadio()
    this.Parent.oContained.w_SELEFP = this.RadioValue()
    return .t.
  endfunc

  func oSELEFP_1_42.SetRadio()
    this.Parent.oContained.w_SELEFP=trim(this.Parent.oContained.w_SELEFP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEFP=='FP',1,;
      0)
  endfunc

  func oSELEFP_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEFP<>"|")
    endwith
   endif
  endfunc

  add object oSELEF1_1_43 as StdCheck with uid="TKHSXOGNFA",rtseq=40,rtrep=.f.,left=219, top=287, caption="Pratiche - Foro",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 37729498,;
    cFormVar="w_SELEF1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEF1_1_43.RadioValue()
    return(iif(this.value =1,'F1',;
    ' '))
  endfunc
  func oSELEF1_1_43.GetRadio()
    this.Parent.oContained.w_SELEF1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEF1_1_43.SetRadio()
    this.Parent.oContained.w_SELEF1=trim(this.Parent.oContained.w_SELEF1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEF1=='F1',1,;
      0)
  endfunc

  func oSELEF1_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEF1<>"|")
    endwith
   endif
  endfunc

  add object oSELEF2_1_44 as StdCheck with uid="TLPQFDPZBO",rtseq=41,rtrep=.f.,left=219, top=306, caption="Pratiche - Note",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 20952282,;
    cFormVar="w_SELEF2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEF2_1_44.RadioValue()
    return(iif(this.value =1,'F2',;
    ' '))
  endfunc
  func oSELEF2_1_44.GetRadio()
    this.Parent.oContained.w_SELEF2 = this.RadioValue()
    return .t.
  endfunc

  func oSELEF2_1_44.SetRadio()
    this.Parent.oContained.w_SELEF2=trim(this.Parent.oContained.w_SELEF2)
    this.value = ;
      iif(this.Parent.oContained.w_SELEF2=='F2',1,;
      0)
  endfunc

  func oSELEF2_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEF2<>"|")
    endwith
   endif
  endfunc

  add object oSELEF3_1_45 as StdCheck with uid="PAWNSMHLRI",rtseq=42,rtrep=.f.,left=219, top=325, caption="Pratiche - Descrizione",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 4175066,;
    cFormVar="w_SELEF3", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEF3_1_45.RadioValue()
    return(iif(this.value =1,'F3',;
    ' '))
  endfunc
  func oSELEF3_1_45.GetRadio()
    this.Parent.oContained.w_SELEF3 = this.RadioValue()
    return .t.
  endfunc

  func oSELEF3_1_45.SetRadio()
    this.Parent.oContained.w_SELEF3=trim(this.Parent.oContained.w_SELEF3)
    this.value = ;
      iif(this.Parent.oContained.w_SELEF3=='F3',1,;
      0)
  endfunc

  func oSELEF3_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEF3<>"|")
    endwith
   endif
  endfunc

  add object oSELEF4_1_46 as StdCheck with uid="KHXXZAINBZ",rtseq=43,rtrep=.f.,left=219, top=344, caption="Pratiche - Riferimenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 255833306,;
    cFormVar="w_SELEF4", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEF4_1_46.RadioValue()
    return(iif(this.value =1,'F4',;
    ' '))
  endfunc
  func oSELEF4_1_46.GetRadio()
    this.Parent.oContained.w_SELEF4 = this.RadioValue()
    return .t.
  endfunc

  func oSELEF4_1_46.SetRadio()
    this.Parent.oContained.w_SELEF4=trim(this.Parent.oContained.w_SELEF4)
    this.value = ;
      iif(this.Parent.oContained.w_SELEF4=='F4',1,;
      0)
  endfunc

  func oSELEF4_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEF4<>"|")
    endwith
   endif
  endfunc

  add object oSELEC1_1_47 as StdCheck with uid="KMDTGVHTLC",rtseq=44,rtrep=.f.,left=219, top=363, caption="Categoria soggetti esterni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 40875226,;
    cFormVar="w_SELEC1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEC1_1_47.RadioValue()
    return(iif(this.value =1,'C1',;
    ' '))
  endfunc
  func oSELEC1_1_47.GetRadio()
    this.Parent.oContained.w_SELEC1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEC1_1_47.SetRadio()
    this.Parent.oContained.w_SELEC1=trim(this.Parent.oContained.w_SELEC1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEC1=='C1',1,;
      0)
  endfunc

  func oSELEC1_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEC1<>"|")
    endwith
   endif
  endfunc

  add object oSELESO_1_48 as StdCheck with uid="QBVBQUAHDH",rtseq=45,rtrep=.f.,left=219, top=382, caption="Soggetti esterni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 210783014,;
    cFormVar="w_SELESO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELESO_1_48.RadioValue()
    return(iif(this.value =1,'SO',;
    ' '))
  endfunc
  func oSELESO_1_48.GetRadio()
    this.Parent.oContained.w_SELESO = this.RadioValue()
    return .t.
  endfunc

  func oSELESO_1_48.SetRadio()
    this.Parent.oContained.w_SELESO=trim(this.Parent.oContained.w_SELESO)
    this.value = ;
      iif(this.Parent.oContained.w_SELESO=='SO',1,;
      0)
  endfunc

  func oSELESO_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELESO<>"|")
    endwith
   endif
  endfunc

  add object oSELERS_1_49 as StdCheck with uid="VOUTHESMCP",rtseq=46,rtrep=.f.,left=219, top=401, caption="Soggetti interni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 8407846,;
    cFormVar="w_SELERS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELERS_1_49.RadioValue()
    return(iif(this.value =1,'RS',;
    ' '))
  endfunc
  func oSELERS_1_49.GetRadio()
    this.Parent.oContained.w_SELERS = this.RadioValue()
    return .t.
  endfunc

  func oSELERS_1_49.SetRadio()
    this.Parent.oContained.w_SELERS=trim(this.Parent.oContained.w_SELERS)
    this.value = ;
      iif(this.Parent.oContained.w_SELERS=='RS',1,;
      0)
  endfunc

  func oSELERS_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELERS<>"|")
    endwith
   endif
  endfunc

  add object oSELET1_1_51 as StdCheck with uid="VRILZHKISL",rtseq=47,rtrep=.f.,left=219, top=440, caption="Documenti archiviati",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 23049434,;
    cFormVar="w_SELET1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELET1_1_51.RadioValue()
    return(iif(this.value =1,'T1',;
    ' '))
  endfunc
  func oSELET1_1_51.GetRadio()
    this.Parent.oContained.w_SELET1 = this.RadioValue()
    return .t.
  endfunc

  func oSELET1_1_51.SetRadio()
    this.Parent.oContained.w_SELET1=trim(this.Parent.oContained.w_SELET1)
    this.value = ;
      iif(this.Parent.oContained.w_SELET1=='T1',1,;
      0)
  endfunc

  func oSELET1_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELET1<>"|")
    endwith
   endif
  endfunc

  add object oSELEPM_1_52 as StdCheck with uid="LWLSRZNTHU",rtseq=48,rtrep=.f.,left=219, top=459, caption="Modelli documenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 174082854,;
    cFormVar="w_SELEPM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPM_1_52.RadioValue()
    return(iif(this.value =1,'PM',;
    ' '))
  endfunc
  func oSELEPM_1_52.GetRadio()
    this.Parent.oContained.w_SELEPM = this.RadioValue()
    return .t.
  endfunc

  func oSELEPM_1_52.SetRadio()
    this.Parent.oContained.w_SELEPM=trim(this.Parent.oContained.w_SELEPM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPM=='PM',1,;
      0)
  endfunc

  func oSELEPM_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPM<>"|")
    endwith
   endif
  endfunc

  add object oSELEEV_1_54 as StdCheck with uid="ENHJUXXGYD",rtseq=49,rtrep=.f.,left=403, top=116, caption="Eventi Polis Web",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 45108006,;
    cFormVar="w_SELEEV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEEV_1_54.RadioValue()
    return(iif(this.value =1,'EV',;
    ' '))
  endfunc
  func oSELEEV_1_54.GetRadio()
    this.Parent.oContained.w_SELEEV = this.RadioValue()
    return .t.
  endfunc

  func oSELEEV_1_54.SetRadio()
    this.Parent.oContained.w_SELEEV=trim(this.Parent.oContained.w_SELEEV)
    this.value = ;
      iif(this.Parent.oContained.w_SELEEV=='EV',1,;
      0)
  endfunc

  func oSELEEV_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEEV<>"|")
    endwith
   endif
  endfunc

  add object oSELEA6_1_55 as StdCheck with uid="ZTEHRIHNWI",rtseq=50,rtrep=.f.,left=403, top=135, caption="Antiriciclaggio",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 227521754,;
    cFormVar="w_SELEA6", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEA6_1_55.RadioValue()
    return(iif(this.value =1,'A6',;
    ' '))
  endfunc
  func oSELEA6_1_55.GetRadio()
    this.Parent.oContained.w_SELEA6 = this.RadioValue()
    return .t.
  endfunc

  func oSELEA6_1_55.SetRadio()
    this.Parent.oContained.w_SELEA6=trim(this.Parent.oContained.w_SELEA6)
    this.value = ;
      iif(this.Parent.oContained.w_SELEA6=='A6',1,;
      0)
  endfunc

  func oSELEA6_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEA6<>"|")
    endwith
   endif
  endfunc

  add object oSELEDT_1_56 as StdCheck with uid="ESKSNVYCLV",rtseq=51,rtrep=.f.,left=403, top=154, caption="Fatture e proforme",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 10504998,;
    cFormVar="w_SELEDT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDT_1_56.RadioValue()
    return(iif(this.value =1,'DT',;
    ' '))
  endfunc
  func oSELEDT_1_56.GetRadio()
    this.Parent.oContained.w_SELEDT = this.RadioValue()
    return .t.
  endfunc

  func oSELEDT_1_56.SetRadio()
    this.Parent.oContained.w_SELEDT=trim(this.Parent.oContained.w_SELEDT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDT=='DT',1,;
      0)
  endfunc

  func oSELEDT_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDT<>"|")
    endwith
   endif
  endfunc

  add object oSELEFR_1_57 as StdCheck with uid="PETGWAHYZV",rtseq=52,rtrep=.f.,left=403, top=173, caption="Note fatture e proforme",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 247483174,;
    cFormVar="w_SELEFR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEFR_1_57.RadioValue()
    return(iif(this.value =1,'FR',;
    ' '))
  endfunc
  func oSELEFR_1_57.GetRadio()
    this.Parent.oContained.w_SELEFR = this.RadioValue()
    return .t.
  endfunc

  func oSELEFR_1_57.SetRadio()
    this.Parent.oContained.w_SELEFR=trim(this.Parent.oContained.w_SELEFR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEFR=='FR',1,;
      0)
  endfunc

  func oSELEFR_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEFR<>"|")
    endwith
   endif
  endfunc

  add object oSELEDO_1_58 as StdCheck with uid="OUCQVGNSXX",rtseq=53,rtrep=.f.,left=403, top=192, caption="Parcelle",;
    ToolTipText = "Importa i documenti se previsto dai tracciati records e gestisce la transazione a livello di documento",;
    HelpContextID = 195054374,;
    cFormVar="w_SELEDO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDO_1_58.RadioValue()
    return(iif(this.value =1,'DO',;
    ' '))
  endfunc
  func oSELEDO_1_58.GetRadio()
    this.Parent.oContained.w_SELEDO = this.RadioValue()
    return .t.
  endfunc

  func oSELEDO_1_58.SetRadio()
    this.Parent.oContained.w_SELEDO=trim(this.Parent.oContained.w_SELEDO)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDO=='DO',1,;
      0)
  endfunc

  func oSELEDO_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDO<>"|")
    endwith
   endif
  endfunc

  add object oSELETL_1_60 as StdCheck with uid="OLSDIPBHTW",rtseq=54,rtrep=.f.,left=403, top=230, caption="Tipologie attivit�",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 161499942,;
    cFormVar="w_SELETL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELETL_1_60.RadioValue()
    return(iif(this.value =1,'TL',;
    ' '))
  endfunc
  func oSELETL_1_60.GetRadio()
    this.Parent.oContained.w_SELETL = this.RadioValue()
    return .t.
  endfunc

  func oSELETL_1_60.SetRadio()
    this.Parent.oContained.w_SELETL=trim(this.Parent.oContained.w_SELETL)
    this.value = ;
      iif(this.Parent.oContained.w_SELETL=='TL',1,;
      0)
  endfunc

  func oSELETL_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELETL<>"|")
    endwith
   endif
  endfunc

  add object oSELECU_1_61 as StdCheck with uid="IGQGTQNJRM",rtseq=55,rtrep=.f.,left=403, top=249, caption="Tipi attivit�",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 26233638,;
    cFormVar="w_SELECU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECU_1_61.RadioValue()
    return(iif(this.value =1,'CU',;
    ' '))
  endfunc
  func oSELECU_1_61.GetRadio()
    this.Parent.oContained.w_SELECU = this.RadioValue()
    return .t.
  endfunc

  func oSELECU_1_61.SetRadio()
    this.Parent.oContained.w_SELECU=trim(this.Parent.oContained.w_SELECU)
    this.value = ;
      iif(this.Parent.oContained.w_SELECU=='CU',1,;
      0)
  endfunc

  func oSELECU_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECU<>"|")
    endwith
   endif
  endfunc

  add object oSELET2_1_62 as StdCheck with uid="UUUGYERWHY",rtseq=56,rtrep=.f.,left=403, top=268, caption="Tariffe",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 6272218,;
    cFormVar="w_SELET2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELET2_1_62.RadioValue()
    return(iif(this.value =1,'T2',;
    ' '))
  endfunc
  func oSELET2_1_62.GetRadio()
    this.Parent.oContained.w_SELET2 = this.RadioValue()
    return .t.
  endfunc

  func oSELET2_1_62.SetRadio()
    this.Parent.oContained.w_SELET2=trim(this.Parent.oContained.w_SELET2)
    this.value = ;
      iif(this.Parent.oContained.w_SELET2=='T2',1,;
      0)
  endfunc

  func oSELET2_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELET2<>"|")
    endwith
   endif
  endfunc

  add object oSELEIP_1_63 as StdCheck with uid="GYAKSKTNVV",rtseq=57,rtrep=.f.,left=403, top=287, caption="Raggruppamenti di prestazioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 217074470,;
    cFormVar="w_SELEIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIP_1_63.RadioValue()
    return(iif(this.value =1,'IP',;
    ' '))
  endfunc
  func oSELEIP_1_63.GetRadio()
    this.Parent.oContained.w_SELEIP = this.RadioValue()
    return .t.
  endfunc

  func oSELEIP_1_63.SetRadio()
    this.Parent.oContained.w_SELEIP=trim(this.Parent.oContained.w_SELEIP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIP=='IP',1,;
      0)
  endfunc

  func oSELEIP_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIP<>"|")
    endwith
   endif
  endfunc

  add object oSELEIA_1_64 as StdCheck with uid="VDFLAPKHBF",rtseq=58,rtrep=.f.,left=403, top=306, caption="Raggruppamenti di attivit�",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 34583770,;
    cFormVar="w_SELEIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEIA_1_64.RadioValue()
    return(iif(this.value =1,'IA',;
    ' '))
  endfunc
  func oSELEIA_1_64.GetRadio()
    this.Parent.oContained.w_SELEIA = this.RadioValue()
    return .t.
  endfunc

  func oSELEIA_1_64.SetRadio()
    this.Parent.oContained.w_SELEIA=trim(this.Parent.oContained.w_SELEIA)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIA=='IA',1,;
      0)
  endfunc

  func oSELEIA_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEIA<>"|")
    endwith
   endif
  endfunc

  add object oSELEAT_1_65 as StdCheck with uid="OCULQOTKVX",rtseq=59,rtrep=.f.,left=403, top=325, caption="Attivit� con prestazioni",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 7359270,;
    cFormVar="w_SELEAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEAT_1_65.RadioValue()
    return(iif(this.value =1,'AT',;
    ' '))
  endfunc
  func oSELEAT_1_65.GetRadio()
    this.Parent.oContained.w_SELEAT = this.RadioValue()
    return .t.
  endfunc

  func oSELEAT_1_65.SetRadio()
    this.Parent.oContained.w_SELEAT=trim(this.Parent.oContained.w_SELEAT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEAT=='AT',1,;
      0)
  endfunc

  func oSELEAT_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEAT<>"|")
    endwith
   endif
  endfunc

  add object oSELEDR_1_66 as StdCheck with uid="WWFOKPOYVL",rtseq=60,rtrep=.f.,left=403, top=363, caption="Dettaglio fatture e proforme",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 245386022,;
    cFormVar="w_SELEDR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEDR_1_66.RadioValue()
    return(iif(this.value =1,'DR',;
    ' '))
  endfunc
  func oSELEDR_1_66.GetRadio()
    this.Parent.oContained.w_SELEDR = this.RadioValue()
    return .t.
  endfunc

  func oSELEDR_1_66.SetRadio()
    this.Parent.oContained.w_SELEDR=trim(this.Parent.oContained.w_SELEDR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEDR=='DR',1,;
      0)
  endfunc

  func oSELEDR_1_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEDR<>"|")
    endwith
   endif
  endfunc

  add object oSELEA5_1_67 as StdCheck with uid="ABWUVBYPBU",rtseq=61,rtrep=.f.,left=403, top=382, caption="Attivit�",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 244298970,;
    cFormVar="w_SELEA5", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEA5_1_67.RadioValue()
    return(iif(this.value =1,'A5',;
    ' '))
  endfunc
  func oSELEA5_1_67.GetRadio()
    this.Parent.oContained.w_SELEA5 = this.RadioValue()
    return .t.
  endfunc

  func oSELEA5_1_67.SetRadio()
    this.Parent.oContained.w_SELEA5=trim(this.Parent.oContained.w_SELEA5)
    this.value = ;
      iif(this.Parent.oContained.w_SELEA5=='A5',1,;
      0)
  endfunc

  func oSELEA5_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEA5<>"|")
    endwith
   endif
  endfunc

  add object oSELEPE_1_68 as StdCheck with uid="IKHBDAGDWK",rtseq=62,rtrep=.f.,left=403, top=401, caption="Partecipanti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 39865126,;
    cFormVar="w_SELEPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPE_1_68.RadioValue()
    return(iif(this.value =1,'PE',;
    ' '))
  endfunc
  func oSELEPE_1_68.GetRadio()
    this.Parent.oContained.w_SELEPE = this.RadioValue()
    return .t.
  endfunc

  func oSELEPE_1_68.SetRadio()
    this.Parent.oContained.w_SELEPE=trim(this.Parent.oContained.w_SELEPE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPE=='PE',1,;
      0)
  endfunc

  func oSELEPE_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPE<>"|")
    endwith
   endif
  endfunc

  add object oSELEFY_1_69 as StdCheck with uid="UGTLJLBDAL",rtseq=63,rtrep=.f.,left=403, top=420, caption="Acconti non detratti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 96488230,;
    cFormVar="w_SELEFY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEFY_1_69.RadioValue()
    return(iif(this.value =1,'FY',;
    ' '))
  endfunc
  func oSELEFY_1_69.GetRadio()
    this.Parent.oContained.w_SELEFY = this.RadioValue()
    return .t.
  endfunc

  func oSELEFY_1_69.SetRadio()
    this.Parent.oContained.w_SELEFY=trim(this.Parent.oContained.w_SELEFY)
    this.value = ;
      iif(this.Parent.oContained.w_SELEFY=='FY',1,;
      0)
  endfunc

  func oSELEFY_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEFY<>"|")
    endwith
   endif
  endfunc

  add object oSELEPP_1_70 as StdCheck with uid="XBIMKOJKCH",rtseq=64,rtrep=.f.,left=403, top=440, caption="Prestazioni Provvisorie",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 224414502,;
    cFormVar="w_SELEPP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPP_1_70.RadioValue()
    return(iif(this.value =1,'PP',;
    ' '))
  endfunc
  func oSELEPP_1_70.GetRadio()
    this.Parent.oContained.w_SELEPP = this.RadioValue()
    return .t.
  endfunc

  func oSELEPP_1_70.SetRadio()
    this.Parent.oContained.w_SELEPP=trim(this.Parent.oContained.w_SELEPP)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPP=='PP',1,;
      0)
  endfunc

  func oSELEPP_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPP<>"|")
    endwith
   endif
  endfunc

  add object oSELEPZ_1_71 as StdCheck with uid="JLVPHSXWBK",rtseq=65,rtrep=.f.,left=403, top=344, caption="Prestazioni in anagrafica",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 123751206,;
    cFormVar="w_SELEPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPZ_1_71.RadioValue()
    return(iif(this.value =1,'PZ',;
    ' '))
  endfunc
  func oSELEPZ_1_71.GetRadio()
    this.Parent.oContained.w_SELEPZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEPZ_1_71.SetRadio()
    this.Parent.oContained.w_SELEPZ=trim(this.Parent.oContained.w_SELEPZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPZ=='PZ',1,;
      0)
  endfunc

  func oSELEPZ_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPZ<>"|")
    endwith
   endif
  endfunc

  add object oSELEPN_1_73 as StdCheck with uid="JOTPRCUSRR",rtseq=66,rtrep=.f.,left=614, top=116, caption="Primanota",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 190860070,;
    cFormVar="w_SELEPN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPN_1_73.RadioValue()
    return(iif(this.value =1,'PN',;
    ' '))
  endfunc
  func oSELEPN_1_73.GetRadio()
    this.Parent.oContained.w_SELEPN = this.RadioValue()
    return .t.
  endfunc

  func oSELEPN_1_73.SetRadio()
    this.Parent.oContained.w_SELEPN=trim(this.Parent.oContained.w_SELEPN)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPN=='PN',1,;
      0)
  endfunc

  func oSELEPN_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPN<>"|")
    endwith
   endif
  endfunc

  add object oSELEPT_1_74 as StdCheck with uid="QBDHZRLWAW",rtseq=67,rtrep=.f.,left=614, top=135, caption="Partite",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 23087910,;
    cFormVar="w_SELEPT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEPT_1_74.RadioValue()
    return(iif(this.value =1,'PT',;
    ' '))
  endfunc
  func oSELEPT_1_74.GetRadio()
    this.Parent.oContained.w_SELEPT = this.RadioValue()
    return .t.
  endfunc

  func oSELEPT_1_74.SetRadio()
    this.Parent.oContained.w_SELEPT=trim(this.Parent.oContained.w_SELEPT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEPT=='PT',1,;
      0)
  endfunc

  func oSELEPT_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEPT<>"|")
    endwith
   endif
  endfunc

  add object oSELEBM_1_76 as StdCheck with uid="NDPQRVFOHZ",rtseq=68,rtrep=.f.,left=614, top=173, caption="Aree tematiche",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 159402790,;
    cFormVar="w_SELEBM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBM_1_76.RadioValue()
    return(iif(this.value =1,'BM',;
    ' '))
  endfunc
  func oSELEBM_1_76.GetRadio()
    this.Parent.oContained.w_SELEBM = this.RadioValue()
    return .t.
  endfunc

  func oSELEBM_1_76.SetRadio()
    this.Parent.oContained.w_SELEBM=trim(this.Parent.oContained.w_SELEBM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBM=='BM',1,;
      0)
  endfunc

  func oSELEBM_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBM<>"|")
    endwith
   endif
  endfunc

  add object oSELEBR_1_77 as StdCheck with uid="XCVUEUETYY",rtseq=69,rtrep=.f.,left=614, top=192, caption="Aree + argomenti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 243288870,;
    cFormVar="w_SELEBR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBR_1_77.RadioValue()
    return(iif(this.value =1,'BR',;
    ' '))
  endfunc
  func oSELEBR_1_77.GetRadio()
    this.Parent.oContained.w_SELEBR = this.RadioValue()
    return .t.
  endfunc

  func oSELEBR_1_77.SetRadio()
    this.Parent.oContained.w_SELEBR=trim(this.Parent.oContained.w_SELEBR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBR=='BR',1,;
      0)
  endfunc

  func oSELEBR_1_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBR<>"|")
    endwith
   endif
  endfunc

  add object oSELEBU_1_78 as StdCheck with uid="KFBSWPRIEC",rtseq=70,rtrep=.f.,left=614, top=211, caption="Autori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 25185062,;
    cFormVar="w_SELEBU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBU_1_78.RadioValue()
    return(iif(this.value =1,'BU',;
    ' '))
  endfunc
  func oSELEBU_1_78.GetRadio()
    this.Parent.oContained.w_SELEBU = this.RadioValue()
    return .t.
  endfunc

  func oSELEBU_1_78.SetRadio()
    this.Parent.oContained.w_SELEBU=trim(this.Parent.oContained.w_SELEBU)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBU=='BU',1,;
      0)
  endfunc

  func oSELEBU_1_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBU<>"|")
    endwith
   endif
  endfunc

  add object oSELEBE_1_79 as StdCheck with uid="MTOMLKTGWB",rtseq=71,rtrep=.f.,left=614, top=230, caption="Editori",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 25185062,;
    cFormVar="w_SELEBE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBE_1_79.RadioValue()
    return(iif(this.value =1,'BE',;
    ' '))
  endfunc
  func oSELEBE_1_79.GetRadio()
    this.Parent.oContained.w_SELEBE = this.RadioValue()
    return .t.
  endfunc

  func oSELEBE_1_79.SetRadio()
    this.Parent.oContained.w_SELEBE=trim(this.Parent.oContained.w_SELEBE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBE=='BE',1,;
      0)
  endfunc

  func oSELEBE_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBE<>"|")
    endwith
   endif
  endfunc

  add object oSELEBT_1_80 as StdCheck with uid="IBTUJMPLME",rtseq=72,rtrep=.f.,left=614, top=249, caption="Tipologie",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 8407846,;
    cFormVar="w_SELEBT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBT_1_80.RadioValue()
    return(iif(this.value =1,'BT',;
    ' '))
  endfunc
  func oSELEBT_1_80.GetRadio()
    this.Parent.oContained.w_SELEBT = this.RadioValue()
    return .t.
  endfunc

  func oSELEBT_1_80.SetRadio()
    this.Parent.oContained.w_SELEBT=trim(this.Parent.oContained.w_SELEBT)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBT=='BT',1,;
      0)
  endfunc

  func oSELEBT_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBT<>"|")
    endwith
   endif
  endfunc

  add object oSELEBS_1_81 as StdCheck with uid="TWBLNHCJKL",rtseq=73,rtrep=.f.,left=614, top=268, caption="Stanze",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 260066086,;
    cFormVar="w_SELEBS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBS_1_81.RadioValue()
    return(iif(this.value =1,'BS',;
    ' '))
  endfunc
  func oSELEBS_1_81.GetRadio()
    this.Parent.oContained.w_SELEBS = this.RadioValue()
    return .t.
  endfunc

  func oSELEBS_1_81.SetRadio()
    this.Parent.oContained.w_SELEBS=trim(this.Parent.oContained.w_SELEBS)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBS=='BS',1,;
      0)
  endfunc

  func oSELEBS_1_81.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBS<>"|")
    endwith
   endif
  endfunc

  add object oSELEBC_1_82 as StdCheck with uid="WPWTXIQLTT",rtseq=74,rtrep=.f.,left=614, top=287, caption="Scaffali",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 8369370,;
    cFormVar="w_SELEBC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBC_1_82.RadioValue()
    return(iif(this.value =1,'BC',;
    ' '))
  endfunc
  func oSELEBC_1_82.GetRadio()
    this.Parent.oContained.w_SELEBC = this.RadioValue()
    return .t.
  endfunc

  func oSELEBC_1_82.SetRadio()
    this.Parent.oContained.w_SELEBC=trim(this.Parent.oContained.w_SELEBC)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBC=='BC',1,;
      0)
  endfunc

  func oSELEBC_1_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBC<>"|")
    endwith
   endif
  endfunc

  add object oSELEBB_1_83 as StdCheck with uid="SPQYTCNFJI",rtseq=75,rtrep=.f.,left=614, top=306, caption="Testi",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 25146586,;
    cFormVar="w_SELEBB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELEBB_1_83.RadioValue()
    return(iif(this.value =1,'BB',;
    ' '))
  endfunc
  func oSELEBB_1_83.GetRadio()
    this.Parent.oContained.w_SELEBB = this.RadioValue()
    return .t.
  endfunc

  func oSELEBB_1_83.SetRadio()
    this.Parent.oContained.w_SELEBB=trim(this.Parent.oContained.w_SELEBB)
    this.value = ;
      iif(this.Parent.oContained.w_SELEBB=='BB',1,;
      0)
  endfunc

  func oSELEBB_1_83.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELEBB<>"|")
    endwith
   endif
  endfunc

  add object oSELECONT_1_87 as StdCheck with uid="HJVITWNLMM",rtseq=79,rtrep=.f.,left=19, top=546, caption="Controllo valori",;
    ToolTipText = "Se attivato consente di controllare i valori assegnati ai singoli campi",;
    HelpContextID = 74429574,;
    cFormVar="w_SELECONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECONT_1_87.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELECONT_1_87.GetRadio()
    this.Parent.oContained.w_SELECONT = this.RadioValue()
    return .t.
  endfunc

  func oSELECONT_1_87.SetRadio()
    this.Parent.oContained.w_SELECONT=trim(this.Parent.oContained.w_SELECONT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECONT=='S',1,;
      0)
  endfunc

  func oSELECONT_1_87.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc


  add object oSELERESO_1_88 as StdCombo with uid="VYMIVBYKOS",rtseq=80,rtrep=.f.,left=19,top=569,width=133,height=21;
    , ToolTipText = "Se attivato genera il resoconto delle operazioni di import";
    , HelpContextID = 41962357;
    , cFormVar="w_SELERESO",RowSource=""+"Errori+segnalazioni,"+"Errori+stop dopo 10,"+"Errori,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELERESO_1_88.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    'N')))))
  endfunc
  func oSELERESO_1_88.GetRadio()
    this.Parent.oContained.w_SELERESO = this.RadioValue()
    return .t.
  endfunc

  func oSELERESO_1_88.SetRadio()
    this.Parent.oContained.w_SELERESO=trim(this.Parent.oContained.w_SELERESO)
    this.value = ;
      iif(this.Parent.oContained.w_SELERESO=='S',1,;
      iif(this.Parent.oContained.w_SELERESO=='D',2,;
      iif(this.Parent.oContained.w_SELERESO=='E',3,;
      iif(this.Parent.oContained.w_SELERESO=='N',4,;
      0))))
  endfunc

  func oSELERESO_1_88.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMPARIVA_1_89 as StdCheck with uid="AWRYUUTVDF",rtseq=81,rtrep=.f.,left=178, top=546, caption="Controllo partita IVA",;
    ToolTipText = "Se attivato consente il riconoscimento dei clienti/fornitori per partita IVA",;
    HelpContextID = 108827335,;
    cFormVar="w_IMPARIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMPARIVA_1_89.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMPARIVA_1_89.GetRadio()
    this.Parent.oContained.w_IMPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oIMPARIVA_1_89.SetRadio()
    this.Parent.oContained.w_IMPARIVA=trim(this.Parent.oContained.w_IMPARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_IMPARIVA=='S',1,;
      0)
  endfunc

  func oIMPARIVA_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMAZZERA_1_90 as StdCheck with uid="HMAFHBTQIQ",rtseq=82,rtrep=.f.,left=178, top=562, caption="Azzera movimenti",;
    ToolTipText = "Se attivato la procedura elimina le movimentazioni prima di importarle",;
    HelpContextID = 51684039,;
    cFormVar="w_IMAZZERA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMAZZERA_1_90.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMAZZERA_1_90.GetRadio()
    this.Parent.oContained.w_IMAZZERA = this.RadioValue()
    return .t.
  endfunc

  func oIMAZZERA_1_90.SetRadio()
    this.Parent.oContained.w_IMAZZERA=trim(this.Parent.oContained.w_IMAZZERA)
    this.value = ;
      iif(this.Parent.oContained.w_IMAZZERA=='S',1,;
      0)
  endfunc

  func oIMAZZERA_1_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMTRANSA_1_91 as StdCheck with uid="GPDSMVITBT",rtseq=83,rtrep=.f.,left=309, top=546, caption="Sotto transazione",;
    ToolTipText = "Se attivato la procedura viene eseguita sotto un'unica transazione",;
    HelpContextID = 176018119,;
    cFormVar="w_IMTRANSA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMTRANSA_1_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMTRANSA_1_91.GetRadio()
    this.Parent.oContained.w_IMTRANSA = this.RadioValue()
    return .t.
  endfunc

  func oIMTRANSA_1_91.SetRadio()
    this.Parent.oContained.w_IMTRANSA=trim(this.Parent.oContained.w_IMTRANSA)
    this.value = ;
      iif(this.Parent.oContained.w_IMTRANSA=='S',1,;
      0)
  endfunc

  func oIMTRANSA_1_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_INCORSO)
    endwith
   endif
  endfunc

  add object oIMCONFER_1_92 as StdCheck with uid="RVMTDVMTAB",rtseq=84,rtrep=.f.,left=309, top=562, caption="Richiesta conferma",;
    ToolTipText = "Se attivato viene richiesto la conferma prima di aggiornare il database (valido solo se attivo sotto transazione)",;
    HelpContextID = 55165656,;
    cFormVar="w_IMCONFER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIMCONFER_1_92.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIMCONFER_1_92.GetRadio()
    this.Parent.oContained.w_IMCONFER = this.RadioValue()
    return .t.
  endfunc

  func oIMCONFER_1_92.SetRadio()
    this.Parent.oContained.w_IMCONFER=trim(this.Parent.oContained.w_IMCONFER)
    this.value = ;
      iif(this.Parent.oContained.w_IMCONFER=='S',1,;
      0)
  endfunc

  func oIMCONFER_1_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMTRANSA='S' and !.w_INCORSO)
    endwith
   endif
  endfunc

  func oIMCONFER_1_92.mHide()
    with this.Parent.oContained
      return (.w_IMTRANSA<>'S')
    endwith
  endfunc

  add object oRADSELEZ_1_93 as StdRadio with uid="JTFLDCVNJL",rtseq=85,rtrep=.f.,left=477, top=526, width=266,height=23;
    , ToolTipText = "Seleziona/deseleziona tutti gli archivi";
    , cFormVar="w_RADSELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELEZ_1_93.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 146655088
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 146655088
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti gli archivi")
      StdRadio::init()
    endproc

  func oRADSELEZ_1_93.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oRADSELEZ_1_93.GetRadio()
    this.Parent.oContained.w_RADSELEZ = this.RadioValue()
    return .t.
  endfunc

  func oRADSELEZ_1_93.SetRadio()
    this.Parent.oContained.w_RADSELEZ=trim(this.Parent.oContained.w_RADSELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELEZ=='S',1,;
      iif(this.Parent.oContained.w_RADSELEZ=='D',2,;
      0))
  endfunc

  func oRADSELEZ_1_93.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODIMP) and !.w_INCORSO)
    endwith
   endif
  endfunc


  add object oBtn_1_94 as StdButton with uid="IXBUJQRYLM",left=478, top=554, width=48,height=45,;
    CpPicture="BMP\info.bmp", caption="", nPag=1;
    , HelpContextID = 9212842;
    , caption='\<Info';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      do gsim_kai with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_94.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oBtn_1_95 as StdButton with uid="OODZLUZFUW",left=532, top=554, width=48,height=45,;
    CpPicture="BMP\trascodifica.bmp", caption="", nPag=1;
    , HelpContextID = 45005466;
    , caption='\<Trascod.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_95.Click()
      with this.Parent.oContained
        GSIM_BSO(this.Parent.oContained,"Import")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_95.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_IsAdministrator() AND !.w_INCORSO and (not empty(.w_CODIMP)) and (.w_TIPSRC='A' or !empty(.w_ODBCDSN)) and g_IMPO='S')
      endwith
    endif
  endfunc


  add object oBtn_1_96 as StdButton with uid="VQERAGEWET",left=627, top=554, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 132730650;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_96.Click()
      with this.Parent.oContained
        do GSIM_BAC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_96.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (cp_IsAdministrator() AND !.w_INCORSO and (not empty(.w_CODIMP)) and (.w_TIPSRC='A' or !empty(.w_ODBCDSN))                          and g_IMPO='S')
      endwith
    endif
  endfunc


  add object oBtn_1_97 as StdButton with uid="MRPTSAHHIT",left=682, top=554, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 125441978;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_97.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_97.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!.w_INCORSO)
      endwith
    endif
  endfunc


  add object oBtn_1_98 as StdButton with uid="JKKEBCSAFN",left=396, top=67, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 132558378;
  , bGlobalFont=.t.

    proc oBtn_1_98.Click()
      with this.Parent.oContained
        .w_ODBCPATH=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_98.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDBF='S')
      endwith
    endif
  endfunc

  func oBtn_1_98.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDBF<>'S')
     endwith
    endif
  endfunc

  add object oDESIMP_1_101 as StdField with uid="OMDLTUIPBE",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 221559350,;
   bGlobalFont=.t.,;
    Height=21, Width=472, Left=257, Top=13, InputMask=replicate('X',60)

  add object oSELECR_1_139 as StdCheck with uid="PERRYSQHNB",rtseq=114,rtrep=.f.,left=614, top=344, caption="Categorie cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 244337446,;
    cFormVar="w_SELECR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECR_1_139.RadioValue()
    return(iif(this.value =1,'CR',;
    ' '))
  endfunc
  func oSELECR_1_139.GetRadio()
    this.Parent.oContained.w_SELECR = this.RadioValue()
    return .t.
  endfunc

  func oSELECR_1_139.SetRadio()
    this.Parent.oContained.w_SELECR=trim(this.Parent.oContained.w_SELECR)
    this.value = ;
      iif(this.Parent.oContained.w_SELECR=='CR',1,;
      0)
  endfunc

  func oSELECR_1_139.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECR<>"|")
    endwith
   endif
  endfunc

  add object oSELECT_1_140 as StdCheck with uid="PLBRLEGNFM",rtseq=115,rtrep=.f.,left=614, top=363, caption="Anagrafica cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 9456422,;
    cFormVar="w_SELECT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECT_1_140.RadioValue()
    return(iif(this.value =1,'CT',;
    ' '))
  endfunc
  func oSELECT_1_140.GetRadio()
    this.Parent.oContained.w_SELECT = this.RadioValue()
    return .t.
  endfunc

  func oSELECT_1_140.SetRadio()
    this.Parent.oContained.w_SELECT=trim(this.Parent.oContained.w_SELECT)
    this.value = ;
      iif(this.Parent.oContained.w_SELECT=='CT',1,;
      0)
  endfunc

  func oSELECT_1_140.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECT<>"|")
    endwith
   endif
  endfunc

  add object oSELECS_1_141 as StdCheck with uid="WARSCXGZBD",rtseq=116,rtrep=.f.,left=614, top=382, caption="Saldi cespiti",;
    ToolTipText = "Se attivato importa i dati dell'archivio se previsto dai tracciati records",;
    HelpContextID = 261114662,;
    cFormVar="w_SELECS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELECS_1_141.RadioValue()
    return(iif(this.value =1,'CS',;
    ' '))
  endfunc
  func oSELECS_1_141.GetRadio()
    this.Parent.oContained.w_SELECS = this.RadioValue()
    return .t.
  endfunc

  func oSELECS_1_141.SetRadio()
    this.Parent.oContained.w_SELECS=trim(this.Parent.oContained.w_SELECS)
    this.value = ;
      iif(this.Parent.oContained.w_SELECS=='CS',1,;
      0)
  endfunc

  func oSELECS_1_141.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SELECS<>"|")
    endwith
   endif
  endfunc


  add object oObj_1_142 as cp_runprogram with uid="IXGAHCLBMS",left=364, top=627, width=337,height=19,;
    caption='GSIM_BSA(1)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSA(1)",;
    cEvent = "w_RADSELEZ Changed,w_CODIMP Changed",;
    nPag=1;
    , HelpContextID = 5958695


  add object oObj_1_143 as cp_runprogram with uid="SCCPNNHPBM",left=76, top=627, width=256,height=19,;
    caption='GSIM_BSA(0)',;
   bGlobalFont=.t.,;
    prg="GSIM_BSA(0)",;
    cEvent = "w_CODIMP Changed,Blank",;
    nPag=1;
    , HelpContextID = 5958439

  add object oStr_1_8 as StdString with uid="LRTPADRUIZ",Visible=.t., Left=18, Top=97,;
    Alignment=0, Width=189, Height=18,;
    Caption="Anagrafiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="DZXHCMSCRH",Visible=.t., Left=219, Top=97,;
    Alignment=0, Width=179, Height=18,;
    Caption="Pratiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="AWDEFBQNDW",Visible=.t., Left=219, Top=420,;
    Alignment=0, Width=72, Height=18,;
    Caption="Documenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="GHPJYNGRVH",Visible=.t., Left=403, Top=97,;
    Alignment=0, Width=72, Height=18,;
    Caption="Altri archivi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="CPLYADPTKV",Visible=.t., Left=403, Top=211,;
    Alignment=0, Width=179, Height=18,;
    Caption="Attivit� e prestazioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="BRDYMEXVNR",Visible=.t., Left=614, Top=96,;
    Alignment=0, Width=189, Height=18,;
    Caption="Movimenti contabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="OKTUKTAAMX",Visible=.t., Left=614, Top=154,;
    Alignment=0, Width=128, Height=18,;
    Caption="Biblioteca"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_102 as StdString with uid="TULOXBETIP",Visible=.t., Left=7, Top=67,;
    Alignment=1, Width=88, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="HNFSNYQVVY",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=88, Height=15,;
    Caption="Tracciati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="TXLTEBBQSG",Visible=.t., Left=15, Top=525,;
    Alignment=0, Width=132, Height=15,;
    Caption="Verifiche e resoconti"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="SQXRTSXXPT",Visible=.t., Left=175, Top=525,;
    Alignment=0, Width=124, Height=15,;
    Caption="Opzioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_109 as StdString with uid="FTEJVJHPFP",Visible=.t., Left=7, Top=44,;
    Alignment=1, Width=88, Height=15,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC="O")
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="UXUMQQXWYB",Visible=.t., Left=7, Top=44,;
    Alignment=1, Width=88, Height=18,;
    Caption="Origine dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_110.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_111 as StdString with uid="QQMMTLMXER",Visible=.t., Left=436, Top=44,;
    Alignment=1, Width=53, Height=18,;
    Caption="User:"  ;
  , bGlobalFont=.t.

  func oStr_1_111.mHide()
    with this.Parent.oContained
      return (.w_TIPSRC<>'O')
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="KUWPABNYKM",Visible=.t., Left=419, Top=68,;
    Alignment=1, Width=70, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (.w_TIPDBF<>'S')
    endwith
  endfunc

  add object oStr_1_138 as StdString with uid="KNOWFOOKEI",Visible=.t., Left=614, Top=325,;
    Alignment=0, Width=72, Height=18,;
    Caption="Cespiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_144 as StdString with uid="PANSOWXHNH",Visible=.t., Left=195, Top=500,;
    Alignment=0, Width=535, Height=17,;
    Caption="Ricordati di eseguire  REPLACE ptarrval WITH RECNO() WHILE 1=1 in PRE_STAZ.DBF e STO_PRES.DBF"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_144.mHide()
    with this.Parent.oContained
      return (Upper(.w_CODIMP) <>'AE2' and Upper(.w_CODIMP) <>'STORICO')
    endwith
  endfunc

  add object oBox_1_106 as StdBox with uid="LZRSZSRDKV",left=12, top=542, width=127,height=1

  add object oBox_1_108 as StdBox with uid="UECDJXNURS",left=171, top=542, width=263,height=1

  add object oBox_1_124 as StdBox with uid="HEWQCGIKYV",left=10, top=89, width=725,height=432
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsim_kae','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
