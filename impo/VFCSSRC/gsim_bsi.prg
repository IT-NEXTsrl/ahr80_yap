* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsim_bsi                                                        *
*              Stampa tracciato importazione                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_32]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2013-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsim_bsi",oParentObject)
return(i_retval)

define class tgsim_bsi as StdBatch
  * --- Local variables
  * --- WorkFile variables
  IMPORTAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la stampa del tracciato di importazione ---> da GSIM_SIM
    do case
      case this.oParentObject.w_Sorgente="A"
        VQ_EXEC("..\impo\exe\query\gsim_sim.vqr",this,"__tmp__") 
        CP_CHPRN("..\impo\exe\query\gsim_sim.frx", " ", this )
      case this.oParentObject.w_Sorgente="O"
        VQ_EXEC("..\impo\exe\query\gsim_stm.vqr",this,"__tmp__") 
        CP_CHPRN("..\impo\exe\query\gsim_stm.frx", " ", this)
      otherwise
        VQ_EXEC("..\impo\exe\query\gsim_sxm.vqr",this,"__tmp__") 
        CP_CHPRN("..\impo\exe\query\gsim_sxm.frx", " ", this)
    endcase
    * --- Rilascio cursore
    if used("__tmp__")
      select __tmp__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMPORTAZ'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
