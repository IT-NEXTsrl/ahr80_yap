* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_blr                                                        *
*              Importazione materiali ciclo                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-28                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSCELTA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_blr",oParentObject,m.pSCELTA)
return(i_retval)

define class tgsci_blr as StdBatch
  * --- Local variables
  pSCELTA = space(3)
  w_CODCOMP = space(40)
  w_CODCOMPF = space(40)
  w_ZOOM = space(10)
  w_OREC = 0
  OutCursor = space(10)
  w_KEYRIF = space(10)
  w_CODINI = space(41)
  w_CODFIN = space(41)
  w_CODIAR = space(20)
  w_VARIAR = space(20)
  w_LenLvl = 0
  w_CONFDES = space(0)
  w_KEYGROUP = space(10)
  w_CONTA = 0
  w_DESSUP = space(0)
  w_GRUPPO = space(3)
  w_ACTIVEREC = 0
  w_MESS = space(40)
  PunPadN = .NULL.
  w_DISPROV = space(1)
  w_DISMANU = space(1)
  w_OBJ = .NULL.
  GSCI_KLR = .NULL.
  GSCI_KVR = .NULL.
  GSCI_MMI = .NULL.
  GSCI_MLR = .NULL.
  ROWORD = 0
  w_DBCODICE = space(41)
  w_CLRIFMAT = space(41)
  PunPadN = .NULL.
  w_DIBA = space(41)
  w_CODCOMP = space(41)
  w_RIFRIGA = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATINI1 = ctod("  /  /  ")
  w_DATFIN1 = ctod("  /  /  ")
  GSCI_KLR = .NULL.
  GSCI_KVR = .NULL.
  GSCI_MMI = .NULL.
  GSCI_MLR = .NULL.
  ROWORD = 0
  w_DBCODICE = space(41)
  w_CLRIFMAT = space(41)
  w_ROWNUM = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Da GSCI_MLR importazione Materiali in Input
    * --- --Lista Materiali in Input
    do case
      case this.pSCELTA="VIS"
        VQ_EXEC("..\DIBA\EXE\QUERY\GSCI"+this.oParentObject.w_TIPGES+"SDP.VQR",this,"Componenti")
        * --- --OCCORRE NOTIFICARE L'EVENTO INTERROGA
        if reccount("Componenti")>0
          do GSCI_KVR with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_MESS = "Per le selezioni impostate non ci sono componenti da importare"
          ah_ErrorMsg (this.w_MESS, 48, "AdHoc Enterprise")
        endif
      case this.pSCELTA="CHK"
        * --- Eseguito il Check sul Master
        * --- --Acquisizione  materiale alternativo
        this.GSCI_KVR = this.oParentObject
        this.GSCI_KLR = this.GSCI_KVR.OparentObject.OparentObject
        this.GSCI_MMI = this.GSCI_KLR.OparentObject.GSCI_MMI
        this.GSCI_MLR = this.GSCI_MMI.OparentObject.OparentObject.gsci_mlr
        this.w_CLRIFMAT = this.GSCI_MLR.OparentObject.w_CLRIFMAT
        this.w_ZOOM = this.oParentObject.w_ZoomMast
        NC = this.w_Zoom.cCursor
        if !(this.w_CLRIFMAT<>COCODCOM AND COIDEGRU="AF" AND COOPELOG="AP") 
          Replace XCHK with 1 in (NC)
          FOR I=1 TO This.w_Zoom.grd.ColumnCount
          NC = ALLTRIM(STR(I))
          if UPPER(This.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
            This.w_Zoom.grd.Column&NC..DynamicForeColor = ;
            "IIF(XCHK=0, RGB(0,0,255), RGB(255,0,0))"
          endif
          ENDFOR
        else
          * --- --Materiale alternativo non valido
          SELECT (NC)
          AA=ALLTRIM(STR(This.w_Zoom.grd.ColumnCount))
          This.w_Zoom.grd.Column&AA..chk.Value = 0
          ah_ErrorMsg("Materiale alternativo non valido",16)
        endif
        * --- Setta Proprieta' Campi del Cursore
      case this.pSCELTA="UNC"
        * --- Eseguito il Check sul Master
        this.w_ZOOM = this.oParentObject.w_ZoomMast
        NC = this.w_Zoom.cCursor
        Replace XCHK with 0 in (NC)
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if UPPER(This.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
          This.w_Zoom.grd.Column&NC..DynamicForeColor = ;
          "IIF(XCHK=0, RGB(0,0,255), RGB(255,0,0))"
        endif
        ENDFOR
        GO TOP
      case this.pSCELTA="SSH"
        this.GSCI_KVR = this.oParentObject
        this.GSCI_KLR = this.GSCI_KVR.OparentObject.OparentObject
        this.GSCI_MMI = this.GSCI_KLR.OparentObject.GSCI_MMI
        this.GSCI_MLR = this.GSCI_MMI.OparentObject.OparentObject.gsci_mlr
        this.w_CLRIFMAT = this.GSCI_MLR.OparentObject.w_CLRIFMAT
        this.w_ZOOM = this.oParentObject.w_ZoomMast
        NC = this.w_Zoom.cCursor
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if this.oParentObject.w_SELEZI = "S"
          * --- Seleziona Tutto
          UPDATE (NC) SET XCHK = 1 where !(this.w_CLRIFMAT<>COCODCOM AND COIDEGRU="AF" AND COOPELOG="AP")
        else
          * --- deseleziona Tutto
          UPDATE (NC) SET XCHK = 0
        endif
      case this.pSCELTA="INS"
        * --- --Inserimento componenti nelle risorse del ciclo
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.PunPadN = this.oParentObject.oParentObject.oParentObject
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pSCELTA="CAN"
      case this.pSCELTA="BLA"
        this.w_ZOOM = this.oParentObject.w_ZoomMast
        * --- Lancia la Query
        ah_msg( "Ricerca componenti da importare..." )
        this.oParentObject.NotifyEvent("Calcola")     
        WAIT CLEAR
        NC = this.w_Zoom.cCursor
        UPDATE (NC) SET XCHK = 0
        * --- Operazioni sul� cursore per 'vedere' l'ultima riga
        this.w_OBJ = this.oParentObject.oParentObject.oParentObject.oParentObject.oParentObject
        NR = this.w_OBJ.w_CICMAT
        if USED(NR)
          Select (NR) 
 APPEND BLANK 
 DELETE
          Delete from (NC) where CPROWNUM in (Select MIDIBROW from (NR))
          Select (NC) 
 GO TOP
          this.w_ZOOM.grd.refresh()
        endif
        GO TOP
        * --- Setta Proprieta' Campi del Cursore
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if UPPER(This.w_Zoom.grd.Column&NC..ControlSource)<>"XCHK"
          This.w_Zoom.grd.Column&NC..DynamicForeColor = ;
          "IIF(XCHK=0, RGB(0,0,255), RGB(255,0,0))"
        endif
        ENDFOR
    endcase
    if used("Componenti")
      USE IN ("Componenti")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Inserimento dettaglio in GSCI_MLR
    this.w_ZOOM = this.oParentObject.w_ZoomMast
    * --- --Lista Materiali in Input
    this.GSCI_KVR = this.oParentObject
    this.GSCI_KLR = this.GSCI_KVR.OparentObject.OparentObject
    this.GSCI_MMI = this.GSCI_KLR.OparentObject.GSCI_MMI
    this.GSCI_MLR = this.GSCI_MMI.OparentObject.OparentObject.gsci_mlr
    this.w_CLRIFMAT = this.GSCI_MLR.OparentObject.w_CLRIFMAT
    Private nc,ic 
 nc =this.GSCI_MMI.cTrsName 
 ic= this.w_Zoom.cCursor
    SELECT (NC) 
 delete for empty(t_NUMDIS)
    * --- --ic contiene le risorse da associare alla fase
    SELECT (IC)
    SCAN FOR XCHK = 1 
    this.GSCI_MMI.InitRow()     
    SELECT (IC)
    this.GSCI_MMI.w_MIDIBCOD = CORIFDIS
    this.GSCI_MMI.w_CODCOM = COCODCOM
    this.GSCI_MMI.w_MIDIBROW = CPROWNUM
    this.w_ROWNUM = CPROWNUM
    this.GSCI_MMI.w_NUMDIS = CPROWORD
    this.GSCI_MMI.w_DESCOS = ARDESART
    this.GSCI_MMI.w_DATINI = DATINI
    this.GSCI_MMI.w_DATFIN = DATFIN
    this.GSCI_MMI.bUpdated = .t.
    * --- Inserimento dati nella riga
    this.GSCI_MMI.TrsFromWork()     
    RC = this.oParentObject.oParentObject.oParentObject.oParentObject.oParentObject.w_CICMAT
    if USED(RC)
      INSERT INTO (RC) (MIDIBROW,MIROWNUM) VALUES (this.w_ROWNUM, this.GSCI_MMI.w_MIROWNUM)
    endif
    ENDSCAN
    * --- Refresch
    With this.GSCI_MMI 
 SELECT (NC) 
 GO TOP 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=0 
 .oPgFrm.Page1.oPag.oBody.nRelRow=0 
 EndWith
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Chiusura cursori e Maschera
    this.GSCI_KLR.ecpQuit()     
    this.GSCI_KVR.ecpQuit()     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pSCELTA)
    this.pSCELTA=pSCELTA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSCELTA"
endproc
