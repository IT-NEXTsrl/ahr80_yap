* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bfo                                                        *
*              Gestione fasi ordine di lavorazione                             *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-07                                                      *
* Last revis.: 2015-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CLCODODL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bfo",oParentObject,m.w_CLCODODL)
return(i_retval)

define class tgsci_bfo as StdBatch
  * --- Local variables
  w_CLCODODL = space(15)
  w_CLCLAOUT = space(1)
  w_CPROWORD = 0
  w_CLFASOUT = space(1)
  w_PRIMOGIRO = .f.
  w_COUNTPOI = space(1)
  w_CLOURIFE = 0
  w_CPROWNUM = 0
  w_FASPRECLA = 0
  w_CLBFRIFE = 0
  w_PRECLAOUT = .f.
  w_FASCLA = 0
  w_CLCOUPOI = space(1)
  w_CLROWORD = space(6)
  w_CLINDPRE = 0
  w_FASCLA = 0
  w_QTAVER = 0
  w_MESS = space(10)
  w_FLCP = space(1)
  w_FLOU = space(1)
  w_FLCF = space(1)
  w_ROWNUM = 0
  w_CLWIPOUT = space(41)
  w_CLWIPFPR = space(41)
  w_CLDESFAS = space(40)
  w_WIPFASEPREC = space(41)
  w_WIPFASEOLD = space(41)
  w_ROWORD = 0
  * --- WorkFile variables
  ODL_DETT_idx=0
  COD_FASI_idx=0
  ODL_CICL_idx=0
  ODL_MAIN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_msg("Verifica fasi di conto/lavoro in corso...")
    * --- Per le fasi non attive sbianco flag count point e output
    * --- Write into ODL_CICL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLCOUPOI');
      +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASOUT');
      +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLULTFAS');
      +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLPRIFAS');
      +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLCODFAS');
      +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLCPRIFE');
      +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLBFRIFE');
      +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPFPR');
      +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPOUT');
          +i_ccchkf ;
      +" where ";
          +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
          +" and CLFASSEL <> "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          CLCOUPOI = "N";
          ,CLFASOUT = "N";
          ,CLULTFAS = "N";
          ,CLPRIFAS = "N";
          ,CLCODFAS = SPACE(20);
          ,CLCPRIFE = 0;
          ,CLBFRIFE = 0;
          ,CLWIPFPR = SPACE(20);
          ,CLWIPOUT = SPACE(20);
          &i_ccchkf. ;
       where;
          CLCODODL = this.w_CLCODODL;
          and CLFASSEL <> "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Resetta flag fase di conto lavoro
    * --- Write into ODL_CICL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLFASCLA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASCLA');
          +i_ccchkf ;
      +" where ";
          +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
             )
    else
      update (i_cTable) set;
          CLFASCLA = "N";
          &i_ccchkf. ;
       where;
          CLCODODL = this.w_CLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Riassegna flag fase di conto lavoro
    * --- Select from GSCI5BTC
    do vq_exec with 'GSCI5BTC',this,'_Curs_GSCI5BTC','',.f.,.t.
    if used('_Curs_GSCI5BTC')
      select _Curs_GSCI5BTC
      locate for 1=1
      do while not(eof())
      this.w_CLROWORD = _Curs_GSCI5BTC.CPROWORD
      this.w_CLINDPRE = _Curs_GSCI5BTC.CLINDPRE
      * --- Se fase su centro di lavoro esterno, alza flag di gestione output di fase
      if _Curs_GSCI5BTC.RLINTEST = "E"
        * --- Sulla fase � presente un Centro di lavoro esterno
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASOUT');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
          +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCLA');
          +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCOS');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLFASOUT = "S";
              ,CLCOUPOI = "S";
              ,CLFASCLA = "S";
              ,CLFASCOS = "S";
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PRECLAOUT = .T.
      else
        if this.w_PRECLAOUT and this.w_FASPRECLA <> _Curs_GSCI5BTC.CPROWORD
          * --- Sulla fase precedente era presente un Centro di lavoro esterno
          this.w_FASCLA = _Curs_GSCI5BTC.CPROWORD
          this.w_PRECLAOUT = .F.
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASOUT');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
            +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCOS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_FASCLA);
                   )
          else
            update (i_cTable) set;
                CLFASOUT = "S";
                ,CLCOUPOI = "S";
                ,CLFASCOS = "S";
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_CLCODODL;
                and CLROWORD = this.w_FASCLA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      this.w_FASPRECLA = _Curs_GSCI5BTC.CPROWORD
        select _Curs_GSCI5BTC
        continue
      enddo
      use
    endif
    * --- Imposta flag output di fase
    ah_msg("Verifica fasi di output in corso...")
    this.w_FLCP = SPACE(1)
    this.w_FLOU = SPACE(1)
    this.w_FLCF = SPACE(1)
    this.w_PRIMOGIRO = .T.
    * --- Select from ODL_CICL
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
          +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLFASSEL='S'";
          +" order by CLROWORD";
           ,"_Curs_ODL_CICL")
    else
      select * from (i_cTable);
       where CLCODODL=this.w_CLCODODL AND CLFASSEL="S";
       order by CLROWORD;
        into cursor _Curs_ODL_CICL
    endif
    if used('_Curs_ODL_CICL')
      select _Curs_ODL_CICL
      locate for 1=1
      do while not(eof())
      this.w_CLROWORD = _Curs_ODL_CICL.CLROWORD
      this.w_FLCP = iif(_Curs_ODL_CICL.CLCOUPOI="S","="," ")
      this.w_FLOU = iif(_Curs_ODL_CICL.CLFASOUT="S","="," ")
      this.w_FLCF = iif(_Curs_ODL_CICL.CLFASCOS="S","="," ")
      if this.w_PRIMOGIRO
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_ODL_CICL.CLCOUPOI',_Curs_ODL_CICL.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_ODL_CICL.CLFASOUT',_Curs_ODL_CICL.CLFASOUT,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_ODL_CICL.CLFASCOS',_Curs_ODL_CICL.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'ODL_CICL','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'ODL_CICL','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLCODFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp4,'ODL_CICL','CLFASCOS');
          +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLPRIFAS');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_CICL.CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = SPACE(20);
              ,CLFASCOS = &i_cOp4.;
              ,CLPRIFAS = "S";
              &i_ccchkf. ;
           where;
              CLCODODL = _Curs_ODL_CICL.CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PRIMOGIRO = .F.
      else
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_ODL_CICL.CLCOUPOI',_Curs_ODL_CICL.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_ODL_CICL.CLFASOUT',_Curs_ODL_CICL.CLFASOUT,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_ODL_CICL.CLFASCOS',_Curs_ODL_CICL.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'ODL_CICL','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'ODL_CICL','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLCODFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp4,'ODL_CICL','CLFASCOS');
          +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLPRIFAS');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_CICL.CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = SPACE(20);
              ,CLFASCOS = &i_cOp4.;
              ,CLPRIFAS = "N";
              &i_ccchkf. ;
           where;
              CLCODODL = _Curs_ODL_CICL.CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_ODL_CICL
        continue
      enddo
      use
    endif
    * --- Setta ultima fase, fase OUTPUT di riferimento e fase BF(CP) di riferimento
    ah_msg("Verifica fasi count point in corso...")
    * --- Cicla su cursore
    this.w_CLBFRIFE = 0
    this.w_CLOURIFE = 0
    this.w_PRIMOGIRO = .T.
    this.w_CLROWORD = 0
    * --- GSCI4BTC
    * --- Select from GSCI4BTC
    do vq_exec with 'GSCI4BTC',this,'_Curs_GSCI4BTC','',.f.,.t.
    if used('_Curs_GSCI4BTC')
      select _Curs_GSCI4BTC
      locate for 1=1
      do while not(eof())
      * --- Coordinate fase
      if this.w_CLROWORD <> nvl (_Curs_GSCI4BTC.CPROWORD , 0)
        this.w_CLROWORD = nvl (_Curs_GSCI4BTC.CPROWORD , 0)
        this.w_CLFASOUT = nvl (_Curs_GSCI4BTC.CLFASOUT , "N")
        this.w_CLCOUPOI = nvl (_Curs_GSCI4BTC.CLCOUPOI , "N")
        this.w_QTAVER = nvl (_Curs_GSCI4BTC.QTAVER , 0)
        * --- Se primo giro, siamo sull'ultima fase
        if this.w_PRIMOGIRO
          this.w_PRIMOGIRO = .F.
          this.w_CLBFRIFE = this.w_CLROWORD
          this.w_CLOURIFE = this.w_CLROWORD
          this.w_CLFASOUT = "S"
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLULTFAS');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC(this.w_CLFASOUT),'ODL_CICL','CLFASOUT');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'ODL_CICL','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'ODL_CICL','CLBFRIFE');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "S";
                ,CLCOUPOI = "S";
                ,CLFASOUT = this.w_CLFASOUT;
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_CLCODODL;
                and CLROWORD = this.w_CLROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_CLOURIFE = iif( this.w_CLFASOUT="S", this.w_CLROWORD, this.w_CLOURIFE)
          this.w_CLBFRIFE = iif( this.w_CLCOUPOI="S", this.w_CLROWORD, this.w_CLBFRIFE)
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLULTFAS');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'ODL_CICL','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'ODL_CICL','CLBFRIFE');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "N";
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_CLCODODL;
                and CLROWORD = this.w_CLROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLULTFAS');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
          +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'ODL_CICL','CLCPRIFE');
          +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'ODL_CICL','CLBFRIFE');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLULTFAS = "S";
              ,CLCOUPOI = "S";
              ,CLCPRIFE = this.w_CLOURIFE;
              ,CLBFRIFE = this.w_CLBFRIFE;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_GSCI4BTC
        continue
      enddo
      use
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Genera codici di fase per l'ODL
    GSCI_BGC(this,"O", .F., this.w_CLCODODL)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_WIPFASEPREC = space(41)
    this.w_WIPFASEOLD = space(41)
    * --- Individuo i codici delle fasi precedenti
    if .F.
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLFASSEL='S'";
            +" order by CLROWORD";
             ,"_Curs_ODL_CICL")
      else
        select * from (i_cTable);
         where CLCODODL=this.w_CLCODODL AND CLFASSEL="S";
         order by CLROWORD;
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_ROWNUM = _Curs_ODL_CICL.CPROWNUM
        this.w_ROWORD = _Curs_ODL_CICL.CLROWORD
        if (this.w_WIPFASEOLD <> nvl(_Curs_ODL_CICL.CLCODFAS,space(41))) or empty(this.w_WIPFASEOLD+nvl(_Curs_ODL_CICL.CLCODFAS,space(41)))
          this.w_WIPFASEPREC = this.w_WIPFASEOLD
          this.w_WIPFASEOLD = nvl(_Curs_ODL_CICL.CLCODFAS, space(41))
        endif
        * --- Articolo-WIP Output Fase
        this.w_CLWIPOUT = nvl(_Curs_ODL_CICL.CLCODFAS,space(41))
        * --- Articolo-WIP Input Fase
        this.w_CLWIPFPR = nvl(this.w_WIPFASEPREC, space(41))
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(this.w_CLWIPFPR),'ODL_CICL','CLWIPFPR');
          +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(this.w_CLWIPOUT),'ODL_CICL','CLWIPOUT');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                 )
        else
          update (i_cTable) set;
              CLWIPFPR = this.w_CLWIPFPR;
              ,CLWIPOUT = this.w_CLWIPOUT;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_ROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_CLCODODL)
    this.w_CLCODODL=w_CLCODODL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ODL_DETT'
    this.cWorkTables[2]='COD_FASI'
    this.cWorkTables[3]='ODL_CICL'
    this.cWorkTables[4]='ODL_MAIN'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSCI5BTC')
      use in _Curs_GSCI5BTC
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_GSCI4BTC')
      use in _Curs_GSCI4BTC
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CLCODODL"
endproc
