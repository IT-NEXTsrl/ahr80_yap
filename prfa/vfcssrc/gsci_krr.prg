* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_krr                                                        *
*              Caricamento rapido                                              *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-22                                                      *
* Last revis.: 2014-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_krr",oParentObject))

* --- Class definition
define class tgsci_krr as StdForm
  Top    = 13
  Left   = 65

  * --- Standard Properties
  Width  = 565
  Height = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-22"
  HelpContextID=116000105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  RIS_ORSE_IDX = 0
  TIP_RISO_IDX = 0
  STR_CONF_IDX = 0
  cPrg = "gsci_krr"
  cComment = "Caricamento rapido"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RL__TIPO = space(2)
  w_SCTIPUNP = space(2)
  w_SCTIPPAD = space(2)
  w_SCCODPAD = space(20)
  w_TPUNIPRO = space(2)
  w_EREWIP = space(5)
  w_ERECAL = space(5)
  w_SCDESPAD = space(40)
  w_SCUNIPRO = space(20)
  w_SCDESUNP = space(40)
  w_TPPADRE = space(2)
  w_DESRISO = space(10)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_krrPag1","gsci_krr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='TIP_RISO'
    this.cWorkTables[3]='STR_CONF'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RL__TIPO=space(2)
      .w_SCTIPUNP=space(2)
      .w_SCTIPPAD=space(2)
      .w_SCCODPAD=space(20)
      .w_TPUNIPRO=space(2)
      .w_EREWIP=space(5)
      .w_ERECAL=space(5)
      .w_SCDESPAD=space(40)
      .w_SCUNIPRO=space(20)
      .w_SCDESUNP=space(40)
      .w_TPPADRE=space(2)
      .w_DESRISO=space(10)
      .w_RL__TIPO=oParentObject.w_RL__TIPO
      .w_SCTIPUNP=oParentObject.w_SCTIPUNP
      .w_SCTIPPAD=oParentObject.w_SCTIPPAD
      .w_SCCODPAD=oParentObject.w_SCCODPAD
      .w_SCUNIPRO=oParentObject.w_SCUNIPRO
        .w_RL__TIPO = .w_RL__TIPO
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_RL__TIPO))
          .link_1_1('Full')
        endif
        .w_SCTIPUNP = 'UP'
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_SCCODPAD))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_msgformat('Selezionare la risorsa di tipo '+ alltrim(.w_DESRISO) +' da aggiungere al layout'))
      .oPgFrm.Page1.oPag.ZOOM.Calculate(.F.)
        .DoRTCalc(5,9,.f.)
        if not(empty(.w_SCUNIPRO))
          .link_1_14('Full')
        endif
    endwith
    this.DoRTCalc(10,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_RL__TIPO=.w_RL__TIPO
      .oParentObject.w_SCTIPUNP=.w_SCTIPUNP
      .oParentObject.w_SCTIPPAD=.w_SCTIPPAD
      .oParentObject.w_SCCODPAD=.w_SCCODPAD
      .oParentObject.w_SCUNIPRO=.w_SCUNIPRO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_RL__TIPO = .w_RL__TIPO
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
          .link_1_4('Full')
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_msgformat('Selezionare la risorsa di tipo '+ alltrim(.w_DESRISO) +' da aggiungere al layout'))
        .oPgFrm.Page1.oPag.ZOOM.Calculate(.F.)
        .DoRTCalc(5,8,.t.)
          .link_1_14('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_msgformat('Selezionare la risorsa di tipo '+ alltrim(.w_DESRISO) +' da aggiungere al layout'))
        .oPgFrm.Page1.oPag.ZOOM.Calculate(.F.)
    endwith
  return

  proc Calculate_UVFBEXZHFT()
    with this
          * --- 
          GSCI_BTV(this;
              ,"SalvaDaZoom";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Update end")
          .Calculate_UVFBEXZHFT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RL__TIPO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISO_IDX,3]
    i_lTable = "TIP_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISO_IDX,2], .t., this.TIP_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RL__TIPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RL__TIPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_RL__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_RL__TIPO)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RL__TIPO = NVL(_Link_.TRCODICE,space(2))
      this.w_DESRISO = NVL(_Link_.TRDESCRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_RL__TIPO = space(2)
      endif
      this.w_DESRISO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISO_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RL__TIPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODPAD
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCCODPAD);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPPAD;
                       ,'RLCODICE',this.w_SCCODPAD)
            select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODPAD = NVL(_Link_.RLCODICE,space(20))
      this.w_SCDESPAD = NVL(_Link_.RLDESCRI,space(40))
      this.w_TPPADRE = NVL(_Link_.RL__TIPO,space(2))
      this.w_EREWIP = NVL(_Link_.RLMAGWIP,space(5))
      this.w_ERECAL = NVL(_Link_.RLCODCAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODPAD = space(20)
      endif
      this.w_SCDESPAD = space(40)
      this.w_TPPADRE = space(2)
      this.w_EREWIP = space(5)
      this.w_ERECAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCUNIPRO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCUNIPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCUNIPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCUNIPRO);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPUNP;
                       ,'RLCODICE',this.w_SCUNIPRO)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCUNIPRO = NVL(_Link_.RLCODICE,space(20))
      this.w_SCDESUNP = NVL(_Link_.RLDESCRI,space(40))
      this.w_TPUNIPRO = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SCUNIPRO = space(20)
      endif
      this.w_SCDESUNP = space(40)
      this.w_TPUNIPRO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCUNIPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCCODPAD_1_4.value==this.w_SCCODPAD)
      this.oPgFrm.Page1.oPag.oSCCODPAD_1_4.value=this.w_SCCODPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESPAD_1_12.value==this.w_SCDESPAD)
      this.oPgFrm.Page1.oPag.oSCDESPAD_1_12.value=this.w_SCDESPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oSCUNIPRO_1_14.value==this.w_SCUNIPRO)
      this.oPgFrm.Page1.oPag.oSCUNIPRO_1_14.value=this.w_SCUNIPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESUNP_1_15.value==this.w_SCDESUNP)
      this.oPgFrm.Page1.oPag.oSCDESUNP_1_15.value=this.w_SCDESUNP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsci_krrPag1 as StdContainer
  Width  = 561
  height = 397
  stdWidth  = 561
  stdheight = 397
  resizeXpos=213
  resizeYpos=217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCODPAD_1_4 as StdField with uid="TDLGGOQQCF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCCODPAD", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare uno stabilimento/reparto/area/centro di lavoro",;
    ToolTipText = "Codice padre struttura di configurazione",;
    HelpContextID = 39226518,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=181, Left=119, Top=33, InputMask=replicate('X',20), cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPPAD", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCCODPAD"

  func oSCCODPAD_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oObj_1_8 as cp_calclbl with uid="LEIOVHBAQL",left=119, top=59, width=430,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Seleziona risorsa da aggiungere al layout",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=1,fontname="Arial",;
    nPag=1;
    , HelpContextID = 61997542


  add object ZOOM as cp_szoombox with uid="AGTCSJKKPI",left=4, top=83, width=551,height=259,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="RIS_ORSE",cZoomFile="GSCI_KRR",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,QueryonLoad=.f.,cZoomOnZoom="",bQueryOnLoad=.t.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 61997542


  add object oBtn_1_10 as StdButton with uid="HAMCKPWIBP",left=506, top=346, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 243381270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="TBZSAFIPXU",left=450, top=346, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma caricamento rapido";
    , HelpContextID = 243381270;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSCDESPAD_1_12 as StdField with uid="LFZSLITHHI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SCDESPAD", cQueryName = "SCDESPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 24149142,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=302, Top=33, InputMask=replicate('X',40)

  add object oSCUNIPRO_1_14 as StdField with uid="AQPIHMOUJU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SCUNIPRO", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare uno stabilimento/reparto/area/centro di lavoro",;
    ToolTipText = "Codice padre struttura di configurazione",;
    HelpContextID = 234460021,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=181, Left=119, Top=7, InputMask=replicate('X',20), cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPUNP", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCUNIPRO"

  func oSCUNIPRO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oSCDESUNP_1_15 as StdField with uid="BENKXYALZR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SCDESUNP", cQueryName = "SCDESUNP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208698506,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=302, Top=7, InputMask=replicate('X',40)

  add object oStr_1_13 as StdString with uid="HIJJSNFHCM",Visible=.t., Left=32, Top=35,;
    Alignment=1, Width=83, Height=18,;
    Caption="Risorsa padre:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="KDUXZZPOEX",Visible=.t., Left=14, Top=9,;
    Alignment=1, Width=101, Height=18,;
    Caption="Unit� produttiva:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_krr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
