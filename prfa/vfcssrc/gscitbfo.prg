* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscitbfo                                                        *
*              Gestione fasi ordine di lavorazione                             *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-07                                                      *
* Last revis.: 2015-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CLCODODL,w_CLKEYRIF,w_TRSODL_CICL,w_TRSODL_RISO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscitbfo",oParentObject,m.w_CLCODODL,m.w_CLKEYRIF,m.w_TRSODL_CICL,m.w_TRSODL_RISO)
return(i_retval)

define class tgscitbfo as StdBatch
  * --- Local variables
  w_CLCODODL = space(15)
  w_CLKEYRIF = space(10)
  w_TRSODL_CICL = space(10)
  w_TRSODL_RISO = space(10)
  w_CLCLAOUT = space(1)
  w_CPROWORD = 0
  w_CLFASOUT = space(1)
  w_PRIMOGIRO = .f.
  w_COUNTPOI = space(1)
  w_CLOURIFE = 0
  w_CPROWNUM = 0
  w_FASPRECLA = 0
  w_CLBFRIFE = 0
  w_PRECLAOUT = .f.
  w_FASCLA = 0
  w_CLCOUPOI = space(1)
  w_CLROWORD = space(6)
  w_CLINDPRE = 0
  w_FASCLA = 0
  w_QTAVER = 0
  w_MESS = space(10)
  w_DISTINTA = space(20)
  w_NGRUPPO = 0
  w_CICLO = space(5)
  w_CODARTM = space(41)
  w_lunghFase = 0
  w_ARCODART = space(20)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_CACODICE = space(41)
  w_DVCODICE = space(10)
  w_OLTSECIC = space(10)
  w_FASE = space(3)
  w_GRUPPO = space(2)
  w_ROWNUM = 0
  w_CLWIPOUT = space(41)
  w_CLWIPFPR = space(41)
  w_CLDESFAS = space(40)
  w_WIPFASEPREC = space(41)
  w_WIPFASEOLD = space(41)
  w_ROWORD = 0
  w_CLCODFAS = space(20)
  w_PPPREFAS = space(5)
  w_PPFASSEP = space(1)
  w_PPCLAFAS = space(5)
  w_PPDSUPFA = space(1)
  w_CLSERIAL = space(10)
  w_Recno = 0
  * --- WorkFile variables
  ODL_DETT_idx=0
  COD_FASI_idx=0
  TMPODL_CICL_idx=0
  ODL_MAST_idx=0
  CIC_MAST_idx=0
  PAR_PROD_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera codici di fase per l'ODL
    * --- Verifica se definito la variante di supporto al c/lavoro
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPDSUPFA,PPPREFAS,PPFASSEP,PPCLAFAS"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPDSUPFA,PPPREFAS,PPFASSEP,PPCLAFAS;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPDSUPFA = NVL(cp_ToDate(_read_.PPDSUPFA),cp_NullValue(_read_.PPDSUPFA))
      this.w_PPPREFAS = NVL(cp_ToDate(_read_.PPPREFAS),cp_NullValue(_read_.PPPREFAS))
      this.w_PPFASSEP = NVL(cp_ToDate(_read_.PPFASSEP),cp_NullValue(_read_.PPFASSEP))
      this.w_PPCLAFAS = NVL(cp_ToDate(_read_.PPCLAFAS),cp_NullValue(_read_.PPCLAFAS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se non esiste tabella temporanea la creo
    * --- Create temporary table TMPODL_CICL
    if !cp_ExistTableDef('TMPODL_CICL')
      i_nIdx=cp_AddTableDef('TMPODL_CICL',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
            )
      this.TMPODL_CICL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.TMPODL_CICL_idx=0
        this.TMPODL_CICL_idx=cp_ascan(@i_TableProp,lower('TMPODL_CICL'),i_nTables)
      endif
    endif
    if cp_ExistTableDef("TMPODL_CICL")
      * --- Try
      local bErr_0386DAF8
      bErr_0386DAF8=bTrsErr
      this.Try_0386DAF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Create temporary table TMPODL_CICL
        i_nIdx=cp_AddTableDef('TMPODL_CICL') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
              )
        this.TMPODL_CICL_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      bTrsErr=bTrsErr or bErr_0386DAF8
      * --- End
    endif
    do case
      case empty(this.w_PPPREFAS)
        ah_ErrorMsg("Impossibile generare i codici di fase%0Indicare il prefisso da utilizzare per la generazione dei codici di fase","STOP","" )
        i_retcode = 'stop'
        return
      case empty(this.w_PPFASSEP)
        ah_ErrorMsg("Impossibile generare i codici di fase%0Indicare il separatore da utilizzare per la generazione dei codici di fase","STOP","" )
        i_retcode = 'stop'
        return
    endcase
    L_TRSODL_CICL = this.w_TRSODL_CICL
    L_TRSODL_RISO = this.w_TRSODL_RISO
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_0386DAF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from TMPODL_CICL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TMPODL_CICL where ";
            +"1 = "+cp_ToStrODBC(2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    L_TRSODL_CICL = this.w_TRSODL_CICL
    L_TRSODL_RISO = this.w_TRSODL_RISO
    if .F.
      * --- Write into TMPODL_CICL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("N"),'TMPODL_CICL','CLCOUPOI');
        +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("N"),'TMPODL_CICL','CLFASOUT');
        +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'TMPODL_CICL','CLULTFAS');
        +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'TMPODL_CICL','CLCODFAS');
        +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(0),'TMPODL_CICL','CLCPRIFE');
        +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(0),'TMPODL_CICL','CLBFRIFE');
        +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'TMPODL_CICL','CLWIPFPR');
        +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'TMPODL_CICL','CLWIPOUT');
            +i_ccchkf ;
        +" where ";
            +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
            +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
            +" and CLFASSEL <> "+cp_ToStrODBC("S");
               )
      else
        update (i_cTable) set;
            CLCOUPOI = "N";
            ,CLFASOUT = "N";
            ,CLULTFAS = "N";
            ,CLCODFAS = SPACE(20);
            ,CLCPRIFE = 0;
            ,CLBFRIFE = 0;
            ,CLWIPFPR = SPACE(20);
            ,CLWIPOUT = SPACE(20);
            &i_ccchkf. ;
         where;
            CLKEYRIF = this.w_CLKEYRIF;
            and CLCODODL = this.w_CLCODODL;
            and CLFASSEL <> "S";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Per le fasi non attive sbianco flag count point e output
    SELECT(L_TRSODL_CICL)
    Update (L_TRSODL_CICL) Set CLCOUPOI = "N", CLFASOUT = "N", CLULTFAS = "N", CLCODFAS = SPACE(20), ; 
 CLCPRIFE = 0, CLBFRIFE = 0, CLWIPFPR = SPACE(20), CLWIPOUT = SPACE(20) Where CLFASSEL<>"S"
    * --- Resetta flag fase di conto lavoro
    if .f.
      * --- Write into TMPODL_CICL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFASCLA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPODL_CICL','CLFASCLA');
            +i_ccchkf ;
        +" where ";
            +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
            +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
               )
      else
        update (i_cTable) set;
            CLFASCLA = "N";
            &i_ccchkf. ;
         where;
            CLKEYRIF = this.w_CLKEYRIF;
            and CLCODODL = this.w_CLCODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Riassegna flag fase di conto lavoro
    select B.CLROWORD as CPROWORD,B.CLINDPRE,MAX(IIF(B.CLFASCLA="S" , "E" , "I")) as RLINTEST ; 
 from (L_TRSODL_CICL) B INTO CURSOR _CURS_GSCIC8BTC ; 
 where (B.CLFASSEL = "S") group by B.CLROWORD, B.CLINDPRE order by 1 desc
    SELECT("_CURS_GSCIC8BTC")
    SCAN
    this.w_CLROWORD = _Curs_GSCIC8BTC.CPROWORD
    this.w_CLINDPRE = _Curs_GSCIC8BTC.CLINDPRE
    * --- Se fase su centro di lavoro esterno, alza flag di gestione output di fase
    if _Curs_GSCIC8BTC.RLINTEST = "E"
      * --- Sulla fase � presente un Centro di lavoro esterno
      Update (L_TRSODL_CICL) Set CLCOUPOI = "S", CLFASOUT = "S", CLFASCLA = "S", CLFASCOS = "S" WHERE CLROWORD = this.w_CLROWORD
      SELECT("_CURS_GSCIC8BTC")
      if .F.
        * --- Write into TMPODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLFASOUT');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLCOUPOI');
          +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLFASCLA');
          +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLFASCOS');
              +i_ccchkf ;
          +" where ";
              +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
              +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLFASOUT = "S";
              ,CLCOUPOI = "S";
              ,CLFASCLA = "S";
              ,CLFASCOS = "S";
              &i_ccchkf. ;
           where;
              CLKEYRIF = this.w_CLKEYRIF;
              and CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_PRECLAOUT = .T.
    else
      if this.w_PRECLAOUT and this.w_FASPRECLA <> _Curs_GSCIC8BTC.CPROWORD
        * --- Sulla fase precedente era presente un Centro di lavoro esterno
        this.w_FASCLA = _Curs_GSCIC8BTC.CPROWORD
        this.w_PRECLAOUT = .F.
        Update (L_TRSODL_CICL) Set CLCOUPOI = "S", CLFASOUT = "S", CLFASCOS = "S" WHERE CLROWORD = this.w_CLROWORD
        SELECT("_CURS_GSCIC8BTC")
        if .F.
          * --- Write into TMPODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLFASOUT');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLCOUPOI');
            +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'TMPODL_CICL','CLFASCOS');
                +i_ccchkf ;
            +" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_FASCLA);
                   )
          else
            update (i_cTable) set;
                CLFASOUT = "S";
                ,CLCOUPOI = "S";
                ,CLFASCOS = "S";
                &i_ccchkf. ;
             where;
                CLKEYRIF = this.w_CLKEYRIF;
                and CLCODODL = this.w_CLCODODL;
                and CLROWORD = this.w_FASCLA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    this.w_FASPRECLA = _Curs_GSCIC8BTC.CPROWORD
    SELECT("_CURS_GSCIC8BTC")
    ENDSCAN
    USE IN SELECT("_CURS_GSCIC8BTC")
    * --- Imposta flag output di fase
    if .F.
      ah_msg("Verifica fasi di output in corso...")
      SELECT(L_TRSODL_CICL)
      SET ORDER TO CLROWORD
      SCAN FOR CLFASSEL="S"
      this.w_CLROWORD = CLROWORD
      this.w_FLCP = iif(CLCOUPOI="S","="," ")
      this.w_FLOU = iif(CLFASOUT="S","="," ")
      this.w_FLCF = iif(CLFASCOS="S","="," ")
      this.w_Recno = Recno()
      Update (L_TRSODL_CICL) Set CLCOUPOI = iif(CLCOUPOI="S","S", CLCOUPOI) , CLFASOUT = iif(CLFASOUT="S","S", CLFASOUT), ; 
 CLFASCOS= iif(CLFASCOS="S","S", CLFASCOS), CLCODFAS=SPACE(20) WHERE CLROWORD = this.w_CLROWORD
      Go this.w_Recno
      if .F.
        * --- Write into TMPODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_TMPODL_CICL.CLCOUPOI',_Curs_TMPODL_CICL.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_TMPODL_CICL.CLFASOUT',_Curs_TMPODL_CICL.CLFASOUT,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_TMPODL_CICL.CLFASCOS',_Curs_TMPODL_CICL.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'TMPODL_CICL','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'TMPODL_CICL','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'TMPODL_CICL','CLCODFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp4,'TMPODL_CICL','CLFASCOS');
              +i_ccchkf ;
          +" where ";
              +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
              +" and CLCODODL = "+cp_ToStrODBC(_Curs_TMPODL_CICL.CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = SPACE(20);
              ,CLFASCOS = &i_cOp4.;
              &i_ccchkf. ;
           where;
              CLKEYRIF = this.w_CLKEYRIF;
              and CLCODODL = _Curs_TMPODL_CICL.CLCODODL;
              and CLROWORD = this.w_CLROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      
      SELECT(L_TRSODL_CICL)
      ENDSCAN
    endif
    * --- Setta ultima fase, fase OUTPUT di riferimento e fase BF(CP) di riferimento
    if .f.
      ah_msg("Verifica fasi count point in corso...")
    endif
    * --- Cicla su cursore
    this.w_CLBFRIFE = 0
    this.w_CLOURIFE = 0
    this.w_PRIMOGIRO = .T.
    this.w_CLROWORD = 0
    select B.CLCODODL as CLSERIAL,B.CPROWNUM,B.CLROWORD as CPROWORD,B.CLFASOUT,B.CLDESFAS as CLFASDES,B.CLINDPRE as CLINDFAS, ; 
 B.CLCOUPOI,B.CLDATINI as CLINIVAL,B.CLDATFIN as CLFINVAL,B.CLULTFAS,B.CLCPRIFE,B.CLBFRIFE, B.CLAVAUM1+B.CLSCAUM1 as QTAVER ; 
 from (L_TRSODL_CICL) B Into Cursor _CURS_GSCIC9BTC where (NVL( B.CLFASSEL, "N") = "S") order by 3 desc
    SELECT("_CURS_GSCIC9BTC")
    SCAN
    * --- Coordinate fase
    if this.w_CLROWORD <> nvl (_Curs_GSCIC9BTC.CPROWORD , 0)
      this.w_CLROWORD = nvl (_Curs_GSCIC9BTC.CPROWORD , 0)
      this.w_CLFASOUT = nvl (_Curs_GSCIC9BTC.CLFASOUT , "N")
      this.w_CLCOUPOI = nvl (_Curs_GSCIC9BTC.CLCOUPOI , "N")
      this.w_QTAVER = nvl (_Curs_GSCIC9BTC.QTAVER , 0)
      * --- Se primo giro, siamo sull'ultima fase
      if this.w_PRIMOGIRO
        this.w_PRIMOGIRO = .F.
        this.w_CLBFRIFE = this.w_CLROWORD
        this.w_CLOURIFE = this.w_CLROWORD
        Update (L_TRSODL_CICL) Set CLCOUPOI = "S", CLULTFAS = "S", CLCPRIFE = this.w_CLOURIFE, CLBFRIFE = this.w_CLBFRIFE Where CLROWORD = this.w_CLROWORD
      else
        this.w_CLOURIFE = iif( this.w_CLFASOUT="S", this.w_CLROWORD, this.w_CLOURIFE)
        this.w_CLBFRIFE = iif( this.w_CLCOUPOI="S", this.w_CLROWORD, this.w_CLBFRIFE)
        Update (L_TRSODL_CICL) Set CLULTFAS = "N", CLCPRIFE = this.w_CLOURIFE, CLBFRIFE = this.w_CLBFRIFE Where CLROWORD = this.w_CLROWORD
      endif
    else
      Update (L_TRSODL_CICL) Set CLCOUPOI = "S", CLULTFAS = "S", CLCPRIFE = this.w_CLOURIFE, CLBFRIFE = this.w_CLBFRIFE Where CLROWORD = this.w_CLROWORD
    endif
    SELECT("_CURS_GSCIC9BTC")
    ENDSCAN
    USE IN SELECT("_CURS_GSCIC9BTC")
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if !Empty(this.w_CLCODODL)
      if .F.
        GSCI_BGC(this,"T", .F., this.w_CLCODODL, , this.w_CLKEYRIF)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTCOART,OLTDISBA,OLTSECIC"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTCOART,OLTDISBA,OLTSECIC;
          from (i_cTable) where;
              OLCODODL = this.w_CLCODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
        this.w_DISTINTA = NVL(cp_ToDate(_read_.OLTDISBA),cp_NullValue(_read_.OLTDISBA))
        this.w_OLTSECIC = NVL(cp_ToDate(_read_.OLTSECIC),cp_NullValue(_read_.OLTSECIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CIC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CIC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLSERIAL"+;
          " from "+i_cTable+" CIC_MAST where ";
              +"CLSERIAL = "+cp_ToStrODBC(this.w_OLTSECIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLSERIAL;
          from (i_cTable) where;
              CLSERIAL = this.w_OLTSECIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLSERIAL = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ARCODART = alltrim(this.w_CODART)+"."
      this.w_lunghFase = 3
      SELECT(L_TRSODL_CICL)
      SCAN FOR CLFASSEL = "S"
      this.w_FASE = right("000"+alltrim(str(&L_TRSODL_CICL..CLROWORD,this.w_lunghFase,0)),this.w_lunghFase)
      this.w_CODART = this.w_PPPREFAS+this.w_PPFASSEP+ALLTRIM(STR(VAL(this.w_CLSERIAL),10,0))+this.w_PPFASSEP+this.w_FASE
      REPLACE CLCODFAS With this.w_CODART
      SELECT(L_TRSODL_CICL)
      ENDSCAN
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_WIPFASEPREC = SPACE(20)
    this.w_WIPFASEOLD = SPACE(20)
    * --- Individuo i codici delle fasi precedenti
    SELECT(L_TRSODL_CICL)
    SET ORDER TO CLROWORD
    SCAN FOR CLFASSEL = "S"
    this.w_ROWNUM = &L_TRSODL_CICL..CPROWNUM
    this.w_ROWORD = &L_TRSODL_CICL..CLROWORD
    this.w_CLCODFAS = nvl( &L_TRSODL_CICL..CLCODFAS , SPACE(20))
    if &L_TRSODL_CICL..CLFASOUT="S"
      * --- Articolo-WIP Output Fase
      this.w_CLWIPOUT = this.w_CLCODFAS
      * --- Articolo-WIP Input Fase
      this.w_CLWIPFPR = nvl(this.w_WIPFASEPREC, SPACE(20))
      this.w_WIPFASEPREC = this.w_CLCODFAS
    else
      this.w_CLWIPOUT = SPACE(20)
      this.w_CLWIPFPR = SPACE(20)
    endif
    if .F.
      if (this.w_WIPFASEOLD <> nvl(&L_TRSODL_CICL..CLCODFAS,SPACE(20))) or empty(this.w_WIPFASEOLD+nvl(&L_TRSODL_CICL..CLCODFAS,SPACE(20)))
        this.w_WIPFASEPREC = this.w_WIPFASEOLD
        this.w_WIPFASEOLD = nvl(&L_TRSODL_CICL..CLCODFAS, SPACE(20))
      endif
      * --- Articolo-WIP Output Fase
      this.w_CLWIPOUT = nvl(&L_TRSODL_CICL..CLCODFAS,SPACE(20))
      * --- Articolo-WIP Input Fase
      this.w_CLWIPFPR = nvl(this.w_WIPFASEPREC, SPACE(20))
    endif
    this.w_Recno = Recno()
    Update (L_TRSODL_CICL) Set CLWIPFPR = this.w_CLWIPFPR, CLWIPOUT = this.w_CLWIPOUT Where CLROWORD = this.w_ROWORD
    Go this.w_Recno
    SELECT(L_TRSODL_CICL)
    ENDSCAN
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_CLCODODL,w_CLKEYRIF,w_TRSODL_CICL,w_TRSODL_RISO)
    this.w_CLCODODL=w_CLCODODL
    this.w_CLKEYRIF=w_CLKEYRIF
    this.w_TRSODL_CICL=w_TRSODL_CICL
    this.w_TRSODL_RISO=w_TRSODL_RISO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ODL_DETT'
    this.cWorkTables[2]='COD_FASI'
    this.cWorkTables[3]='*TMPODL_CICL'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='CIC_MAST'
    this.cWorkTables[6]='PAR_PROD'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CLCODODL,w_CLKEYRIF,w_TRSODL_CICL,w_TRSODL_RISO"
endproc
