* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bec                                                        *
*              Elimina ordini di fase collegati a ODL                          *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-27                                                      *
* Last revis.: 2018-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Operazione,w_OLCODODL,w_OLTSEODL,w_OLTFAODL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bec",oParentObject,m.w_Operazione,m.w_OLCODODL,m.w_OLTSEODL,m.w_OLTFAODL)
return(i_retval)

define class tgsci_bec as StdBatch
  * --- Local variables
  w_Operazione = space(10)
  w_OLCODODL = space(15)
  w_OLTSEODL = space(15)
  w_OLTFAODL = 0
  w_FLORD = space(1)
  w_FLIMP = space(1)
  w_FLRIS = space(1)
  w_FLCOMM = space(1)
  w_FLCOMD = space(1)
  w_CODCOM = space(15)
  w_CLROWORD = 0
  w_FLUPD = space(1)
  w_CLROWORD = 0
  w_FLUPD = space(1)
  w_ARTIPART = space(2)
  * --- WorkFile variables
  ODL_MAST_idx=0
  ODL_DETT_idx=0
  ODL_CICL_idx=0
  SALDIART_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  ODL_MAIN_idx=0
  ODL_MOUT_idx=0
  ODL_SMPL_idx=0
  ODL_RISO_idx=0
  ODLMRISO_idx=0
  ODL_RISF_idx=0
  SCCI_ASF_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice ODL di origine
    * --- Operazione
    *     ALL       -> Tutti gli ordini di fase collegati
    *     SINGLE -> Elimina singolo ordine di fase
    * --- Cancella gli ordini di fase collegati
    if g_PRFA="S" and g_CICLILAV="S"
      do case
        case this.w_Operazione = "ALL"
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where OLTSEODL="+cp_ToStrODBC(this.w_OLCODODL)+"";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where OLTSEODL=this.w_OLCODODL;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.w_OLTSEODL = this.w_OLCODODL
            this.w_OLTFAODL = NVL(_Curs_ODL_MAST.OLTFAODL, 0)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Legge numero fase
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLROWORD"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_OLTFAODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLROWORD;
                from (i_cTable) where;
                    CLCODODL = this.w_OLTSEODL;
                    and CPROWNUM = this.w_OLTFAODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLROWORD = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if not empty(this.w_OLTSEODL) and this.w_OLTFAODL<>0
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
        case this.w_Operazione = "SINGLEBYPHASE"
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where OLTSEODL="+cp_ToStrODBC(this.w_OLTSEODL)+" AND OLTFAODL="+cp_ToStrODBC(this.w_OLTFAODL)+"";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where OLTSEODL=this.w_OLTSEODL AND OLTFAODL=this.w_OLTFAODL;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
        case this.w_Operazione = "SINGLEBYORD"
          * --- Select from ODL_MAST
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_MAST ";
                +" where OLCODODL="+cp_ToStrODBC(this.w_OLCODODL)+"";
                 ,"_Curs_ODL_MAST")
          else
            select * from (i_cTable);
             where OLCODODL=this.w_OLCODODL;
              into cursor _Curs_ODL_MAST
          endif
          if used('_Curs_ODL_MAST')
            select _Curs_ODL_MAST
            locate for 1=1
            do while not(eof())
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Legge numero fase
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLROWORD"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_OLTFAODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLROWORD;
                from (i_cTable) where;
                    CLCODODL = this.w_OLTSEODL;
                    and CPROWNUM = this.w_OLTFAODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLROWORD = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if not empty(this.w_OLTSEODL) and this.w_OLTFAODL<>0
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
              select _Curs_ODL_MAST
              continue
            enddo
            use
          endif
        case this.w_Operazione = "REOPENMAT"
          if not empty(this.w_OLTSEODL) and this.w_OLTFAODL<>0
            * --- Read from ODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2],.t.,this.ODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLROWORD"+;
                " from "+i_cTable+" ODL_CICL where ";
                    +"CLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_OLTFAODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLROWORD;
                from (i_cTable) where;
                    CLCODODL = this.w_OLTSEODL;
                    and CPROWNUM = this.w_OLTFAODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLROWORD = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_CLROWORD <> 0
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
      endcase
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CODCOM = _Curs_ODL_MAST.OLTCOMME
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLCOMM,ARTIPART"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLCOMM,ARTIPART;
        from (i_cTable) where;
            ARCODART = _Curs_ODL_MAST.OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
      this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Cancella dettaglio ciclo
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL="+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL)+"";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL=_Curs_ODL_MAST.OLCODODL;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      * --- Storno saldi
      this.w_FLORD = iif(_Curs_ODL_DETT.OLFLORDI="+","-", iif(_Curs_ODL_DETT.OLFLORDI="-","+"," "))
      this.w_FLIMP = iif(_Curs_ODL_DETT.OLFLIMPE="+","-", iif(_Curs_ODL_DETT.OLFLIMPE="-","+"," "))
      this.w_FLRIS = iif(_Curs_ODL_DETT.OLFLRISE="+","-", iif(_Curs_ODL_DETT.OLFLRISE="-","+"," "))
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLCOMM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLCOMM;
          from (i_cTable) where;
              ARCODART = _Curs_ODL_DETT.OLCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOMD = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_FLRIS,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            ,SLQTRPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
            and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
        * --- Aggiorna i saldi commessa
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              ,SCQTRPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
              and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
              and SCCODCAN = this.w_CODCOM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
    * --- Cancella
    * --- Delete from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MAIN
    i_nConn=i_TableProp[this.ODL_MAIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAIN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MICODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            MICODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MOCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            MOCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancella dettaglio Risorse della fase
    * --- Delete from ODL_SMPL
    i_nConn=i_TableProp[this.ODL_SMPL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_SMPL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SMCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            SMCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISO
    i_nConn=i_TableProp[this.ODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODLMRISO
    i_nConn=i_TableProp[this.ODLMRISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODLMRISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RLCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ODL_RISF
    i_nConn=i_TableProp[this.ODL_RISF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RFCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            RFCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancella dettaglio Ciclo ODL
    * --- Delete from ODL_CICL
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            CLCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Storno saldi OCL
    this.w_FLORD = iif(_Curs_ODL_MAST.OLTFLORD="+","-", iif(_Curs_ODL_MAST.OLTFLORD="-","+"," "))
    this.w_FLIMP = iif(_Curs_ODL_MAST.OLTFLIMP="+","-", iif(_Curs_ODL_MAST.OLTFLIMP="-","+"," "))
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','_Curs_ODL_MAST.OLTQTSAL',_Curs_ODL_MAST.OLTQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','_Curs_ODL_MAST.OLTQTSAL',_Curs_ODL_MAST.OLTQTSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTKEYSA);
          +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SLCODICE = _Curs_ODL_MAST.OLTKEYSA;
          and SLCODMAG = _Curs_ODL_MAST.OLTCOMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
      * --- Aggiorna i saldi commessa
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','_Curs_ODL_MAST.OLTQTSAL',_Curs_ODL_MAST.OLTQTSAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','_Curs_ODL_MAST.OLTQTSAL',_Curs_ODL_MAST.OLTQTSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTKEYSA);
            +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_MAST.OLTCOMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SCCODICE = _Curs_ODL_MAST.OLTKEYSA;
            and SCCODMAG = _Curs_ODL_MAST.OLTCOMAG;
            and SCCODCAN = this.w_CODCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Cancella OCL
    * --- Delete from ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_MAST.OLCODODL);
             )
    else
      delete from (i_cTable) where;
            OLCODODL = _Curs_ODL_MAST.OLCODODL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL)+" and OLFASRIF = "+cp_ToStrODBC(this.w_CLROWORD)+"";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL = this.w_OLTSEODL and OLFASRIF = this.w_CLROWORD;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      * --- Cerca i materiali associati alla fase per la quale era stato generato l'OCL in cancellazione
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLCOMM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLCOMM;
          from (i_cTable) where;
              ARCODART = _Curs_ODL_DETT.OLCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOMD = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTCOMME"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLTSEODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTCOMME;
          from (i_cTable) where;
              OLCODODL = this.w_OLTSEODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if nvl(_Curs_ODL_DETT.OLEVAAUT,"N") ="S"
        * --- Storno saldi
        this.w_FLORD = nvl(_Curs_ODL_DETT.OLFLORDI, " ")
        this.w_FLIMP = nvl(_Curs_ODL_DETT.OLFLIMPE, " ")
        this.w_FLRIS = nvl(_Curs_ODL_DETT.OLFLRISE, " ")
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLRIS,'SLQTRPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              ,SLQTRPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
              and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
          * --- Aggiorna i saldi commessa
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLRIS,'SCQTRPER','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                ,SCQTRPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                and SCCODCAN = this.w_CODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_FLUPD = "="
        * --- Aggiorna riga ODL padre ...
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLUPD,'OLQTAEVA','0',0,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLUPD,'OLQTAEV1','0',0,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_FLUPD,'OLQTASAL','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLQTAEVA ="+cp_NullLink(i_cOp1,'ODL_DETT','OLQTAEVA');
          +",OLQTAEV1 ="+cp_NullLink(i_cOp2,'ODL_DETT','OLQTAEV1');
          +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLFLEVAS');
          +",OLEVAAUT ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLEVAAUT');
          +",OLQTASAL ="+cp_NullLink(i_cOp5,'ODL_DETT','OLQTASAL');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              OLQTAEVA = &i_cOp1.;
              ,OLQTAEV1 = &i_cOp2.;
              ,OLFLEVAS = "N";
              ,OLEVAAUT = "N";
              ,OLQTASAL = &i_cOp5.;
              &i_ccchkf. ;
           where;
              OLCODODL = _Curs_ODL_DETT.OLCODODL;
              and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Operazione,w_OLCODODL,w_OLTSEODL,w_OLTFAODL)
    this.w_Operazione=w_Operazione
    this.w_OLCODODL=w_OLCODODL
    this.w_OLTSEODL=w_OLTSEODL
    this.w_OLTFAODL=w_OLTFAODL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ODL_DETT'
    this.cWorkTables[3]='ODL_CICL'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='SALDICOM'
    this.cWorkTables[7]='ODL_MAIN'
    this.cWorkTables[8]='ODL_MOUT'
    this.cWorkTables[9]='ODL_SMPL'
    this.cWorkTables[10]='ODL_RISO'
    this.cWorkTables[11]='ODLMRISO'
    this.cWorkTables[12]='ODL_RISF'
    this.cWorkTables[13]='SCCI_ASF'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_MAST')
      use in _Curs_ODL_MAST
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Operazione,w_OLCODODL,w_OLTSEODL,w_OLTFAODL"
endproc
