* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_ksc                                                        *
*              Layout sistema produttivo                                       *
*                                                                              *
*      Author: Zucchetti S.P.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2014-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_ksc",oParentObject))

* --- Class definition
define class tgsci_ksc as StdForm
  Top    = 0
  Left   = 7

  * --- Standard Properties
  Width  = 632
  Height = 470
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-03"
  HelpContextID=99222889
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  RIS_ORSE_IDX = 0
  cPrg = "gsci_ksc"
  cComment = "Layout sistema produttivo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_UPDEFA = space(15)
  w_SCTIPUNP = space(10)
  w_RL__TIPO = space(10)
  w_SCUNIPRO = space(20)
  w_NODOSELE = space(20)
  w_NODOTIPO = space(2)
  w_TP = space(2)
  w_EREWIP = space(5)
  w_ERECAL = space(5)
  w_RLDESCRI = space(40)
  w_RLTIPSEL = space(2)
  w_SCCODPAD = space(20)
  w_SCTIPPAD = space(2)
  w_CURSTREE = space(10)
  w_AlbeDist = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_kscPag1","gsci_ksc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCUNIPRO_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AlbeDist = this.oPgFrm.Pages(1).oPag.AlbeDist
    DoDefault()
    proc Destroy()
      this.w_AlbeDist = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='RIS_ORSE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UPDEFA=space(15)
      .w_SCTIPUNP=space(10)
      .w_RL__TIPO=space(10)
      .w_SCUNIPRO=space(20)
      .w_NODOSELE=space(20)
      .w_NODOTIPO=space(2)
      .w_TP=space(2)
      .w_EREWIP=space(5)
      .w_ERECAL=space(5)
      .w_RLDESCRI=space(40)
      .w_RLTIPSEL=space(2)
      .w_SCCODPAD=space(20)
      .w_SCTIPPAD=space(2)
      .w_CURSTREE=space(10)
          .DoRTCalc(1,1,.f.)
        .w_SCTIPUNP = 'UP'
          .DoRTCalc(3,3,.f.)
        .w_SCUNIPRO = .w_UPDEFA
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_SCUNIPRO))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.AlbeDist.Calculate()
        .w_NODOSELE = .w_ALBEDIST.GETVAR('RLCODICE')
        .w_NODOTIPO = .w_ALBEDIST.GETVAR('RL__TIPO')
          .DoRTCalc(7,10,.f.)
        .w_RLTIPSEL = .w_Albedist.GetVar('RL__TIPO')
        .w_SCCODPAD = .w_Albedist.GetVar('RLCODICE')
        .w_SCTIPPAD = .w_Albedist.GetVar('RL__TIPO')
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AlbeDist.Calculate()
        .DoRTCalc(1,4,.t.)
            .w_NODOSELE = .w_ALBEDIST.GETVAR('RLCODICE')
            .w_NODOTIPO = .w_ALBEDIST.GETVAR('RL__TIPO')
        .DoRTCalc(7,10,.t.)
            .w_RLTIPSEL = .w_Albedist.GetVar('RL__TIPO')
            .w_SCCODPAD = .w_Albedist.GetVar('RLCODICE')
            .w_SCTIPPAD = .w_Albedist.GetVar('RL__TIPO')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AlbeDist.Calculate()
    endwith
  return

  proc Calculate_ZVXYUIBPRJ()
    with this
          * --- 
          .w_Albedist.cLvlSep = CHR(255)
    endwith
  endproc
  proc Calculate_QSZDGFNHPO()
    with this
          * --- 
          GSCI_BTV(this;
              ,"Reload";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AlbeDist.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_ZVXYUIBPRJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_QSZDGFNHPO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCUNIPRO
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCUNIPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_AUP',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_SCTIPUNP;
                     ,'RLCODICE',trim(this.w_SCUNIPRO))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCUNIPRO)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCUNIPRO) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oSCUNIPRO_1_4'),i_cWhere,'GSCI_AUP',"Elenco unit� produttive",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPUNP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCUNIPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCUNIPRO);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPUNP;
                       ,'RLCODICE',this.w_SCUNIPRO)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCUNIPRO = NVL(_Link_.RLCODICE,space(20))
      this.w_RLDESCRI = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SCUNIPRO = space(20)
      endif
      this.w_RLDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCUNIPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCUNIPRO_1_4.value==this.w_SCUNIPRO)
      this.oPgFrm.Page1.oPag.oSCUNIPRO_1_4.value=this.w_SCUNIPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_18.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_18.value=this.w_RLDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsci_kscPag1 as StdContainer
  Width  = 628
  height = 470
  stdWidth  = 628
  stdheight = 470
  resizeXpos=478
  resizeYpos=321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCUNIPRO_1_4 as StdField with uid="PSHKVDSFCB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCUNIPRO", cQueryName = "SCUNIPRO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Unit� produttiva di qui si vuole visualizzare la struttura",;
    HelpContextID = 251237237,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=14, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", cZoomOnZoom="GSCI_AUP", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPUNP", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCUNIPRO"

  func oSCUNIPRO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCUNIPRO_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCUNIPRO_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPUNP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_SCTIPUNP)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oSCUNIPRO_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_AUP',"Elenco unit� produttive",'',this.parent.oContained
  endproc
  proc oSCUNIPRO_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSCI_AUP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.RL__TIPO=w_SCTIPUNP
     i_obj.w_RLCODICE=this.parent.oContained.w_SCUNIPRO
     i_obj.ecpSave()
  endproc


  add object oBtn_1_5 as StdButton with uid="PPWFAOCXOY",left=570, top=14, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per riempire la tree view";
    , HelpContextID = 260158486;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSCI_BTV(this.Parent.oContained,"Reload")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_SCUNIPRO))
      endwith
    endif
  endfunc


  add object AlbeDist as cp_Treeview with uid="SUOSXHMIBK",left=6, top=46, width=550,height=416,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="_TmpCoSiPr_",cShowFields="ALLTRIM(RLCODICE)+' - '+ALLTRIM(RLDESCRI)",cNodeShowField="",cLeafShowField="",cNodeBmp="magazz.bmp,reparto.bmp,area.bmp,centro_lavoro.bmp,macchina.bmp",cLeafBmp="",nIndent=20,;
    cEvent = "Reload",;
    nPag=1;
    , HelpContextID = 78774758


  add object oBtn_1_7 as StdButton with uid="YXYFCMZNER",left=570, top=417, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 260158486;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="EDOPRFXLCY",left=570, top=94, width=48,height=45,;
    CpPicture="BMP\GESTPC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per gestione elemento";
    , HelpContextID = 260158486;
    , Caption='\<Gest.Ele';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSCI_BTV(this.Parent.oContained,"Risorsa")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="JFJITZPFSS",left=570, top=140, width=48,height=45,;
    CpPicture="BMP\LEGAMI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per gestione legame";
    , HelpContextID = 260158486;
    , Caption='Gest.\<Leg';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSCI_BTV(this.Parent.oContained,"Relazione")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="XWAVWHNZRN",left=570, top=186, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per espandere la struttura";
    , HelpContextID = 260158486;
    , Caption='Esplo\<di';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_AlbeDist.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="ZVAMGQYVJH",left=570, top=232, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la struttura";
    , HelpContextID = 260158486;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_AlbeDist.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRLDESCRI_1_18 as StdField with uid="SOTYYQSHVD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42962015,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=14, InputMask=replicate('X',40)

  add object oStr_1_17 as StdString with uid="DUAGDEFNHB",Visible=.t., Left=10, Top=15,;
    Alignment=1, Width=96, Height=18,;
    Caption="Unit� produttiva:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_ksc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
