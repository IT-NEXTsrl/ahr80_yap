* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bdc                                                        *
*              Stampe distinta costificata                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-16                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM1,w_cCursor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bdc",oParentObject,m.w_PARAM1,m.w_cCursor)
return(i_retval)

define class tgsci_bdc as StdBatch
  * --- Local variables
  w_PARAM1 = space(10)
  w_cCursor = space(10)
  w_maxlvl = 0
  w_codiartico = space(20)
  w_VARIAR = space(20)
  w_mykey = space(10)
  w_CODIAR = space(20)
  w_i = 0
  w_qtaparzin = 0
  w_qtaparzil = 0
  w_qtalmstp = 0
  w_cosselez = 0
  w_cossta = 0
  w_cosmed = 0
  w_cosult = 0
  w_COPERRIC = 0
  w_CODTMP = space(5)
  w_CODLEN = 0
  w_TROVATO = .f.
  w_COECONV = 0
  OutCursor = space(10)
  w_KEYRIF = space(10)
  w_LenLvl = 0
  w_LIVFIN = space(1)
  TipoGestione = space(1)
  w_PARZ = space(1)
  w_PREZUM = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_LOTPRO = 0
  w_CMNETSEL = 0
  w_CMSCASEL = 0
  w_CMRICSEL = 0
  w_CAO = 0
  w_CMNETSTD = 0
  w_CMSCASTD = 0
  w_CMRICSTD = 0
  w_PREZZO = 0
  w_CMNETMED = 0
  w_CMSCAMED = 0
  w_CMRICMED = 0
  w_ERRO = .f.
  w_CMNETULT = 0
  w_CMSCAULT = 0
  w_CMRICULT = 0
  w_CSNETSEL = 0
  w_CSSCASEL = 0
  w_CSRICSEL = 0
  w_CSOVHSEL = 0
  w_PARELAB = .f.
  w_MESS = space(10)
  w_codiartic1 = space(20)
  w_CONTRATTO = .f.
  w_RECADDED = .f.
  w_COFLLOTP = space(1)
  w_CODFOR = space(15)
  w_CODFAS = space(41)
  w_ORAUNI = space(1)
  w_VALCONT = space(3)
  w_PRZSET = 0
  w_CAKEYSAL = space(40)
  w_QTAUM1 = 0
  w_CODARTI = space(20)
  w_FLCOMPPAD = space(1)
  w_Tmp = space(20)
  w_TipoValoOld = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_OBTEST = space(0)
  w_TC = space(2)
  w_SC = space(20)
  w_TIPOCOS = space(20)
  w_TMPCUR = space(10)
  w_MultiReportVarEnv = .NULL.
  w_CSRICSELI = 0
  w_CSNETSELI = 0
  w_CSSCASELI = 0
  w_CSOVHSELI = 0
  w_CSRICSELE = 0
  w_CSNETSELE = 0
  w_CSSCASELE = 0
  w_CSOVHSELE = 0
  w_CSRICSELIA = 0
  w_CSNETSELIA = 0
  w_CSSCASELIA = 0
  w_CSOUTSTD = 0
  w_CSOUTMED = 0
  w_CSOUTULT = 0
  w_FORNITO = .f.
  w_DecStampa = 0
  w_CAUNMISU = space(3)
  w_QTALOR = 0
  w_QTALO1 = 0
  w_QTANET = 0
  w_QTAFIN = 0
  w_cosappo = 0
  w_LVLKEY = space(250)
  w_codiartic2 = space(20)
  w_CodMatOut = space(41)
  w_cMatOut = 0
  w_TROVATOMO = .f.
  w_cMatOutUnit = 0
  w_PRCODART = space(40)
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCSGENE = 0
  w_PRCOSSTA = 0
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCUGENE = 0
  w_PRCMLAVO = 0
  w_PRCMMATE = 0
  w_PRCMGENE = 0
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_PRCLGENE = 0
  w_LOTMED = 0
  w_STD = space(1)
  w_UTE = space(1)
  w_ULT = space(1)
  w_MED = space(1)
  w_LST = space(1)
  w_PRCSLAVO = 0
  w_PRCSLAVE = 0
  w_PRCSMATE = 0
  w_PRCSSPGE = 0
  w_PRCSMOUT = 0
  w_PRCSLAVC = 0
  w_PRCSLVCE = 0
  w_PRCSMATC = 0
  w_PRCSSPGC = 0
  w_PRCSMOUC = 0
  w_PRCULAVO = 0
  w_PRCULAVE = 0
  w_PRCUMATE = 0
  w_PRCUSPGE = 0
  w_PRCUMOUT = 0
  w_PRCULAVC = 0
  w_PRCULVCE = 0
  w_PRCUMATC = 0
  w_PRCUSPGC = 0
  w_PRCUMOUC = 0
  w_PRCMLAVO = 0
  w_PRCMLAVE = 0
  w_PRCMMATE = 0
  w_PRCMSPGE = 0
  w_PRCMMOUT = 0
  w_PRCMLAVC = 0
  w_PRCMLVCE = 0
  w_PRCMMATC = 0
  w_PRCMSPGC = 0
  w_PRCMMOUC = 0
  w_PRCLLAVO = 0
  w_PRCLLAVE = 0
  w_PRCLMATE = 0
  w_PRCLSPGE = 0
  w_PRCLMOUT = 0
  w_PRCLLAVC = 0
  w_PRCLLVCE = 0
  w_PRCLMATC = 0
  w_PRCLSPGC = 0
  w_PRCLMOUC = 0
  w_PRCODICE = space(41)
  w_ARTCOM = space(20)
  w_QTAMOV = 0
  w_UNIMIS = space(3)
  w_LOTMEDFE = 0
  w_codicediba = space(20)
  w_Contratto = space(15)
  w_oldkey = space(10)
  w_RECNO = 0
  w_KEYRIF2 = space(0)
  w_ROWNUM = 0
  w_DECTOT = 0
  w_CODVAL = space(3)
  w_LIPREZZO = 0
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_SCOLIS = space(1)
  w_CAQTAMOV = 0
  w_CAQTAMO1 = 0
  w_CALPRZ = 0
  w_UMLIPREZZO = space(3)
  w_UNIMISLI = space(3)
  w_VALLIS2 = space(3)
  w_CAOLIS = 0
  w_QTALIS = 0
  w_CI = 0
  w_RECSEL = 0
  w_LIVELLO = space(41)
  w_CODDIS = space(41)
  w_CJ = 0
  * --- WorkFile variables
  ART_TEMP_idx=0
  MAGAZZIN_idx=0
  PAR_RIOR_idx=0
  VALUTE_idx=0
  CAM_BI_idx=0
  TMPCAMBI_idx=0
  STOCOSTA_idx=0
  LISTINI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa costificata della Distinta Base (da GSDB_SDC)
    * --- ------ variabili solo in GSDB_KDC
    * --- ------ variabili output di fase
    * --- Variabili utilizzate per l'analisi dei costi
    if False
      * --- Create temporary table TMPCAMBI
      i_nIdx=cp_AddTableDef('TMPCAMBI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.CAM_BI_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"CGCODVAL, CGCAMBIO "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMPCAMBI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_DATRIF = this.oParentObject.w_DATSTA
    this.w_OBTEST = this.oParentObject.w_DATSTA
    this.OutCursor = "__bsi__"
    this.w_KEYRIF = SYS(2015)
    this.w_LenLvl = 4
    this.w_LIVFIN = " "
    this.w_PARZ = " "
    this.w_PARELAB = type("this.w_PARAM1")="C" and this.w_PARAM1="ELABORA"
    this.w_cCursor = iif(type("this.w_cCursor")="C", this.w_cCursor, SPACE(10))
    this.OutCursor = this.w_cCursor
    if Empty(this.OutCursor)
      i_retcode = 'stop'
      return
    endif
    if this.w_PARELAB
      * --- Nel caso sto elaborando la determinazione del costo standard e ho dichiarato e valorizzato 
      *     la variabile g_OLDDETCOSSTD a .T.la eseguo come se fosse sintetica (vecchio modo)
      this.oParentObject.w_Analisi = IIF(VarType(g_OLDDETCOSSTD)="L" and g_OLDDETCOSSTD, "S", this.oParentObject.w_Analisi)
    endif
    L_OUT = this.OutCursor
    * --- Controlli preliminari
    * --- Calcola il livello dei componenti (utilizzando lvlkey) e aggiunge i campi dei costi
    Select cacodice,cadesart,cacodart,caoperat,caprezum, ; 
 arunmis1,arunmis2,aroperat,armoltip,artipart,arpropre,flcomp,corifdis,cprownum,cproword,cocodcom, ; 
 cocodart,counimis,cocoeimp,cocoeum1,coscapro,corecsca,cosfrido,corecsfr, ; 
 coriffas,cocorlea,coperric,coattges ,codisges ,tippro, ; 
 lvlkey,qtacomp,qtacompn,qtacompl,stadis,costo,livello,999999999999999.99999*0 AS cstandr, ; 
 999999999999999.99999*0 AS cmedio,999999999999999.99999*0 AS cultim, codicediba, "N" AS evasocl, ; 
 costo*0 AS costocl, 1 AS raggrup, space(2) as tipocosto, space(20) as vtipocosto, space(20) as sezcosto, qtanet, qtane1, qtalor, qtalo1, qtarec as qtafin, qtare1 as qtafi1 ; 
 from (this.OutCursor) into cursor appogg nofilter
    Use in Select(this.OutCursor)
     Select * from appogg into cursor (this.OutCursor) nofilter 
 =wrcursor(this.OutCursor) 
 INDEX ON lvlkey TAG lvlkey collate "MACHINE" 
 =FLOCK() 
 UPDATE (this.OutCursor) SET livello = (1+len(rtrim(lvlkey)))/this.w_LenLvl-1
    if this.oParentObject.w_Analisi<>"N"
      * --- Aggiunge i record vuoti necessari per l'analisi dei costi
      Select lvlkey FROM (this.OutCursor) WHERE livello=0 INTO CURSOR appogg nofilter
      SCAN
      this.w_CODTMP = lvlkey
      Select (this.OutCursor)
      SEEK this.w_CODTMP ORDER lvlkey
      SCATTER MEMVAR
      this.w_CODTMP = left(lvlkey,this.w_lenlvl-1)+chr(255)
      this.w_CODLEN = len(lvlkey)
      raggrup=7
      livello=-1
      this.w_i = IIF(this.oParentObject.w_Analisi = "S" , 8, 24)
      do while this.w_i>0
        this.w_TC = PADL(ALLTRIM(STR(this.w_i)),2,"0")
        if this.oParentObject.w_Analisi = "S"
          this.w_SC = iif(this.w_i =1,"Costo materiali", iif( this.w_i =5,"Costo lavorazione", space(20) ))
          this.w_TIPOCOS = iif(inlist( this.w_i , 1,5),"Netto", iif(inlist( this.w_i , 2,6),"Scarti", ; 
 iif(inlist( this.w_i ,3) ,"Ricar.", iif(inlist( this.w_i , 7), "Setup warm-up", ; 
 iif(inlist( this.w_i ,8), "Overhead", iif(inlist( this.w_i ,4 ), "Materiali di output" , "" ) )))))
        else
          this.w_SC = iif(inlist( this.w_i , 1,13),"Costo materiali", iif(inlist( this.w_i , 5,17),"Lavorazione interna", iif(inlist( this.w_i ,9, 21) ,"Lavorazione esterna", space(20) )))
          * --- Analisi dei costi del prodotto 
          * --- 01 - Costo materiali
          *     04 - Costo lavorazione interna prodotto
          *     08 - Costo lavorazione esterna prodotto
          * --- Analisi dei costi dei componenti
          * --- 12 - Costo materiali
          *     15 - Costo lavorazione interna componenti
          *     19 - Costo lavorazione esterna componenti
          this.w_TIPOCOS = iif(inlist( this.w_i , 1,5,9,13,17,21),"Netto", iif(inlist( this.w_i , 2,6,10,14,18,22),"Scarti", ; 
 iif(inlist( this.w_i ,3,15) ,"Ricar.", iif(inlist( this.w_i , 7,11,19,23), "Setup warm-up", ; 
 iif(inlist( this.w_i ,8,12,20,24), "Overhead", iif(inlist( this.w_i ,4 , 16) , "Materiali di output" , "" ) )))))
        endif
        if .F.
          lvlkey = padr(this.w_CODTMP+str(this.w_i,2,0),this.w_CODLEN)
        endif
        lvlkey = padr(this.w_CODTMP+this.w_TC,this.w_CODLEN)
        sezcosto = this.w_SC
        tipocosto = this.w_TC
        vtipocosto = this.w_TIPOCOS
        INSERT INTO (this.OutCursor) FROM MEMVAR
        this.w_i = this.w_i -1
      enddo
      Select appogg
      ENDSCAN
    endif
    if used("appogg")
      USE IN appogg
    endif
    SELECT max(livello) AS massimo FROM (this.OutCursor) INTO CURSOR maxlevel
    this.w_maxlvl = maxlevel.massimo
    USE IN maxlevel
    * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_PARELAB
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Lancia la stampa
      private l_FAMAINI, l_FAMAFIN , l_FAMINI, l_FAMFIN , l_GRUINI , l_GRUFIN , l_PIAINI , l_PIAFIN , l_CLAINI , l_CLAFIN , l_CLPINI , l_CLPFIN , l_CATINI , l_CATFIN
      l_lotmed = this.oParentObject.w_LOTTOMED
      l_ovrh = (this.oParentObject.w_SCDRSR="S")
      l_numinv =this.oParentObject.w_NUMINV
      l_LenLvl = this.w_LenLvl + 1
      l_TIPVALCIC = this.oParentObject.w_TIPVALCIC
      l_SCDRSR = this.oParentObject.w_SCDRSR
      l_Analisi = this.oParentObject.w_Analisi
      l_Cicli = True
      l_CODINI = this.oParentObject.w_CODINI
      l_CODFIN = this.oParentObject.w_CODFIN
      l_FAMAINI = this.oParentObject.w_FAMAINI
      l_FAMAFIN = this.oParentObject.w_FAMAFIN
      l_GRUINI = this.oParentObject.w_GRUINI
      l_GRUFIN = this.oParentObject.w_GRUFIN
      l_CATINI = this.oParentObject.w_CATINI
      l_CATFIN = this.oParentObject.w_CATFIN
      L_FLCOSINV = this.oParentObject.w_FLCOSINV
      L_PDCOSPAR = this.oParentObject.w_PDCOSPAR
      * --- Cancella i figli dei SL forniti da terzista
      delete from (this.OutCursor) where evasocl="X" 
      this.w_TMPCUR = SYS(2015)
      this.w_MultiReportVarEnv=createobject("ReportEnvironment")
      this.w_MultiReportVarEnv.AddVariable("l_listino" , l_listino)     
      this.w_MultiReportVarEnv.AddVariable("l_numinv" , l_numinv)     
      this.w_MultiReportVarEnv.AddVariable("l_lotmed ", l_lotmed )     
      this.w_MultiReportVarEnv.AddVariable("l_ovrh", l_ovrh)     
      this.w_MultiReportVarEnv.AddVariable("l_LenLvl" , l_LenLvl)     
      this.w_MultiReportVarEnv.AddVariable("l_costomp" , l_costomp)     
      this.w_MultiReportVarEnv.AddVariable("l_chkLoop" , l_chkLoop)     
      this.w_MultiReportVarEnv.AddVariable("l_SCDRSR" , l_SCDRSR)     
      this.w_MultiReportVarEnv.AddVariable("l_Analisi" , l_Analisi)     
      this.w_MultiReportVarEnv.AddVariable("l_Cicli" , l_CICLI)     
      this.w_MultiReportVarEnv.AddVariable("l_TIPVALCIC" , l_TIPVALCIC)     
      this.w_MultiReportVarEnv.AddVariable("l_CODINI" , l_CODINI)     
      this.w_MultiReportVarEnv.AddVariable("l_CODFIN" , l_CODFIN)     
      this.w_MultiReportVarEnv.AddVariable("l_FAMAINI" , l_FAMAINI)     
      this.w_MultiReportVarEnv.AddVariable("l_FAMAFIN" , l_FAMAFIN)     
      this.w_MultiReportVarEnv.AddVariable("l_GRUINI" , l_GRUINI)     
      this.w_MultiReportVarEnv.AddVariable("l_GRUFIN" , l_GRUFIN)     
      this.w_MultiReportVarEnv.AddVariable("l_CATINI" , l_CATINI)     
      this.w_MultiReportVarEnv.AddVariable("l_CATFIN" , l_CATFIN)     
      if this.oParentObject.w_SCDRSR="N"
        Select *, 1 as dplotmed FROM (this.OutCursor) ORDER BY lvlkey INTO CURSOR (this.w_TMPCUR) nofilter
      else
        lenlvlkey = len(&L_OUT..lvlkey)
        l_um_ore = this.oParentObject.w_UM_ORE
        * --- Aggiunge nel cursore di stampa i Cicli di Lavorazione
        Select * FROM (this.OutCursor) LEFT OUTER JOIN CicLavo ON 1=0 INTO CURSOR _temp1_ 
 =wrcursor("_temp1_") 
 Select CicLavo.*, padr(left(lvlkey,this.w_LenLvl-1)+chr(254)+chr(livello)+chr(raggrupp)+cacodric,lenlvlkey) AS lvlkey, ; 
 qtacomp*cocoeum1/cocoeimp as qtacomp, qtacompn*cocoeum1/cocoeimp as qtacompn, ; 
 qtacompl*cocoeum1/cocoeimp as qtacompl, livello, costo ; 
 from (this.OutCursor) RIGHT OUTER JOIN CicLavo ON ; 
 (Ciclavo.cacodric=&L_OUT..cacodice AND Ciclavo.lvlkey1=&L_OUT..lvlkey AND raggrup=1) ; 
 where evasocl<>"S" INTO CURSOR _temp2_ nofilter
        if this.oParentObject.w_SMATOU<>"N"
           Select MatOut.*, padr(left(lvlkey,this.w_LenLvl-1)+chr(254)+chr(livello)+chr(raggrupp)+cacodric,lenlvlkey) AS lvlkey, ; 
 qtacomp*cocoeum1/cocoeimp as qtacomp, qtacompn*cocoeum1/cocoeimp as qtacompn, ; 
 qtacompl*cocoeum1/cocoeimp as qtacompl, livello, costo ; 
 from (this.OutCursor) RIGHT OUTER JOIN MatOut ON ; 
 (MatOut.cacodric=&L_OUT..cacodice AND MatOut.lvlkey1=&L_OUT..lvlkey AND raggrup=1) ; 
 INTO CURSOR _temp3_ nofilter
        endif
        * --- Aggiunge i record dei cicli di lavorazione
        SELECT _temp2_ 
 SCAN for not isnull(lvlkey) 
 scatter memvar 
 raggrup=raggrupp 
 SELECT _temp1_ 
 append blank 
 gather memvar 
 SELECT _temp2_ 
 ENDSCAN
        if this.oParentObject.w_SMATOU<>"N"
          SELECT _temp3_ 
 SCAN for not isnull(lvlkey) 
 scatter memvar 
 raggrup=raggrupp 
 SELECT _temp1_ 
 append blank 
 gather memvar 
 SELECT _temp3_ 
 ENDSCAN
        endif
        * --- Cursore di stampa
        Select * FROM _temp1_ ORDER BY lvlkey,cproword1,cproword2 INTO CURSOR (this.w_TMPCUR) nofilter 
 USE IN _temp1_ 
 USE IN _temp2_ 
 USE IN select("_temp3_") 
 USE IN Ciclavo 
 select(this.w_TMPCUR)
        this.w_MultiReportVarEnv.AddVariable("lenlvlkey" , lenlvlkey)     
        this.w_MultiReportVarEnv.AddVariable("l_um_ore" , l_um_ore)     
      endif
      this.oParentObject.w_MultiRep.AddNewReport(this.oParentObject.w_OREP , this.oParentObject.oParentObject.Caption , this.w_TMPCUR, .null. , this.w_MultiReportVarEnv , .t., this.oParentObject.w_FIRSTCUR , .f.,"","","",1, .t.)     
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione dati
    do while this.w_maxlvl>=0
      SELECT (this.OutCursor)
      SCAN FOR livello=this.w_maxlvl
      if cocoeum1=0 or cocoeimp=0
        this.w_MESS = "Nella distinta base %1 riga %2 - %3 � stato impostato un coefficiente d'impiego uguale a zero%0Si raccomanda di correggere l'errore%0Calcolo della costificazione sospeso" 
        ah_Errormsg(this.w_MESS, 16, "",alltrim(corifdis), alltrim(str(cproword)), alltrim(cocodcom))
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      this.w_COECONV = cocoeum1/cocoeimp
      this.w_CMNETSEL = 0
      this.w_CMSCASEL = 0
      this.w_CMRICSEL = 0
      this.w_CMNETSTD = 0
      this.w_CMSCASTD = 0
      this.w_CMRICSTD = 0
      this.w_CMNETMED = 0
      this.w_CMSCAMED = 0
      this.w_CMRICMED = 0
      this.w_CMNETULT = 0
      this.w_CMSCAULT = 0
      this.w_CMRICULT = 0
      this.w_CSNETSEL = 0
      this.w_CSSCASEL = 0
      this.w_CSRICSEL = 0
      this.w_CSOVHSEL = 0
      * --- -----------------------------------------------------------------------------------------------------
      this.w_CSRICSELI = 0
      this.w_CSNETSELI = 0
      this.w_CSSCASELI = 0
      this.w_CSOVHSELI = 0
      this.w_CSRICSELE = 0
      this.w_CSNETSELE = 0
      this.w_CSSCASELE = 0
      this.w_CSOVHSELE = 0
      * --- -----------------------------------------------------------------------------------------------------
      this.w_CSRICSELIA = 0
      this.w_CSNETSELIA = 0
      this.w_CSSCASELIA = 0
      * --- -----------------------------------------------------------------------------------------------------
      this.w_CSOUTSTD = 0
      this.w_CSOUTMED = 0
      this.w_CSOUTULT = 0
      * --- -----------------------------------------------------------------------------------------------------
      this.w_FORNITO = (evasocl="N")
      this.w_DecStampa = 10
      if FLCOMP="S" OR NOT this.w_FORNITO
        * --- Per gli articoli esterni (es. materia prima) cerca il prezzo
        this.w_codiartico = cacodart
        this.w_QTALOR = cp_ROUND(qtalor, this.w_DecStampa)
        this.w_QTALO1 = cp_ROUND(qtalo1, this.w_DecStampa)
        this.w_QTANET = cp_ROUND(qtanet, this.w_DecStampa)
        this.w_QTAFIN = cp_ROUND(qtafin, this.w_DecStampa)
        this.w_CAUNMISU = NVL(COUNIMIS, SPACE(3))
        if this.w_FORNITO
          SELECT przart
          LOCATE FOR cacodric=this.w_codiartico
          do case
            case this.oParentObject.w_TipoValo="L"
              locate for NVL(cacodric,"  ")=this.w_codiartico and nvl(unimis, space(3))=this.w_CAUNMISU and cp_round(nvl(caqtamov,0), this.w_DecStampa)=cp_round(this.w_qtalor, this.w_DecStampa)
            otherwise
              locate for cacodric=this.w_codiartico
          endcase
          this.w_TROVATO = FOUND()
          if this.w_TROVATO
            do case
              case this.oParentObject.w_TipoValo="S"
                this.w_cosselez = NVL(przart.dicossta, 0) * this.w_COECONV
              case this.oParentObject.w_TipoValo="X"
                this.w_cosselez = NVL(przart.dicossta_ar, 0) *this.w_COECONV
              case this.oParentObject.w_TipoValo="U"
                this.w_cosselez = NVL(przart.dicosult, 0) * this.w_COECONV
              case this.oParentObject.w_TipoValo="M"
                this.w_cosselez = NVL(przart.dicosmpa, 0) * this.w_COECONV
              case this.oParentObject.w_TipoValo="P"
                this.w_cosselez = NVL(przart.dicosmpp, 0)*this.w_COECONV
              case this.oParentObject.w_TipoValo="L"
                this.w_cosselez = NVL(przart.laprezzo, 0)
                if this.w_cosselez<>0
                  if this.oParentObject.w_VALLIS<>g_PERVAL
                    * --- Se Listino di altra Valuta , Allinea alla Valuta di Conto
                    this.w_cosselez = (this.w_cosselez * g_CAOVAL) / this.w_CAOLIS
                  endif
                  if this.oParentObject.w_TIPOLN="L" AND NVL(IVPERIVA,0)<>0
                    * --- Se Listino al Lordo , Nettifica
                    this.w_cosappo = this.w_cosselez - (this.w_cosselez / (1 + (IVPERIVA / 100)))
                    this.w_cosselez = this.w_cosselez - this.w_cosappo
                    this.w_cosappo = 0
                  endif
                endif
              case this.oParentObject.w_TipoValo="A"
                this.w_cosselez = NVL(przart.slvaluca, 0)*this.w_COECONV
            endcase
            if this.w_PARELAB
              if this.oParentObject.w_STANDARD="X"
                this.w_cossta = cp_ROUND(przart.dicossta_ar*this.w_COECONV, this.w_DecStampa)
              else
                this.w_cossta = cp_ROUND(przart.dicossta*this.w_COECONV, this.w_DecStampa)
              endif
              if this.oParentObject.w_ULTIMO="A"
                this.w_cosult = cp_ROUND(przart.slvaluca*this.w_COECONV, this.w_DecStampa)
              else
                this.w_cosult = cp_ROUND(przart.dicosult*this.w_COECONV, this.w_DecStampa)
              endif
              this.w_cosmed = cp_ROUND(przart.dicosmpa*this.w_COECONV, this.w_DecStampa)
            else
              if this.oParentObject.w_Analisi<>"N"
                this.w_cossta = przart.dicossta*this.w_COECONV
                this.w_cosmed = przart.dicosmpa*this.w_COECONV
                this.w_cosult = przart.dicosult*this.w_COECONV
              endif
            endif
            SELECT (this.OutCursor)
          else
            * --- ERRORE: prezzo non definito!
            this.w_cosselez = 0
            this.w_cossta = 0
            this.w_cosmed = 0
            this.w_cosult = 0
          endif
          SELECT (this.OutCursor)
        else
          * --- Per gli articoli fornito dal terzista imposta il prezzo del contratto
          this.w_TROVATO = .T.
          this.w_cosselez = costocl*this.w_COECONV
          if this.oParentObject.w_Analisi<>"N"
            this.w_cossta = this.w_cosselez
            this.w_cosmed = this.w_cosselez
            this.w_cosult = this.w_cosselez
          endif
        endif
        SELECT (this.OutCursor)
        if isnull(this.w_cosselez)
          * --- ERRORE: prezzo non definito!
          this.w_cosselez = 0
        else
          if this.oParentObject.w_Analisi<>"N"
            this.w_CMNETSEL = this.w_cosselez * qtacompn
            this.w_CMSCASEL = this.w_cosselez * (qtacompl - qtacompn)
            this.w_CMRICSEL = iif(this.oParentObject.w_PERRIC="S", this.w_cosselez * qtacompl * coperric/100,0)
          endif
          this.w_cosselez = this.w_cosselez * qtacompl * (1 + iif(this.oParentObject.w_PERRIC="S",coperric/100,0))
        endif
        if this.oParentObject.w_Analisi<>"N"
          if isnull(this.w_cossta)
            this.w_cossta = 0
          else
            this.w_CMNETSTD = this.w_cossta * qtacompn
            this.w_CMSCASTD = this.w_cossta * (qtacompl - qtacompn)
            this.w_CMRICSTD = iif(this.oParentObject.w_PERRIC="S", this.w_cossta * qtacompl * coperric/100,0)
            this.w_cossta = this.w_cossta * qtacompl * (1 + iif(this.oParentObject.w_PERRIC="S",coperric/100,0))
          endif
          if isnull(this.w_cosmed)
            this.w_cosmed = 0
          else
            this.w_CMNETMED = this.w_cosmed * qtacompn
            this.w_CMSCAMED = this.w_cosmed * (qtacompl - qtacompn)
            this.w_CMRICMED = iif(this.oParentObject.w_PERRIC="S", this.w_cosmed * qtacompl * coperric/100,0)
            this.w_cosmed = this.w_cosmed * qtacompl * (1 + iif(this.oParentObject.w_PERRIC="S",coperric/100,0))
          endif
          if isnull(this.w_cosult)
            this.w_cosult = 0
          else
            this.w_CMNETULT = this.w_cosult * qtacompn
            this.w_CMSCAULT = this.w_cosult * (qtacompl - qtacompn)
            this.w_CMRICULT = iif(this.oParentObject.w_PERRIC="S", this.w_cosult * qtacompl * coperric/100,0)
            this.w_cosult = this.w_cosult * qtacompl * (1 + iif(this.oParentObject.w_PERRIC="S",coperric/100,0))
          endif
          REPLACE costo WITH this.w_cosselez,cstandr WITH this.w_cossta, ;
          cmedio WITH this.w_cosmed, cultim WITH this.w_cosult
        else
          REPLACE costo WITH this.w_cosselez
        endif
      else
        * --- Per gli articoli interni, nodi dell'albero, calcola il costo del Ciclo di Lavorazione
        this.w_LVLKEY = LVLKEY
        this.w_codiartico = codicediba
        this.w_codiartic1 = cacodart
        this.w_codiartic1 = this.w_codiartico
        this.w_codiartic2 = cacodart
        this.w_TROVATO = .T.
        this.w_qtaparzin = qtacompn*this.w_COECONV
        this.w_qtaparzil = qtacompl*this.w_COECONV
        this.w_cosselez = 0
        this.w_CONTRATTO = (g_COLA="S")
        this.w_cMatOut = 0
        if this.oParentObject.w_TipoValoOut<>"NN"
          * --- Voglio valorizzare i materiali di output
          SELECT MatOut
          SCAN FOR cacodric=this.w_codiartico and lvlkey1=this.w_lvlkey
          this.w_CONTRATTO = False
          this.w_CodMatOut = MOCODART
          this.w_cMatOutUnit = 0
          select tmpprzmatout
          LOCATE FOR cacodart=this.w_CodMatOut
          this.w_TROVATOMO = FOUND()
          if this.w_TROVATOMO
            * --- Il valore � sempre nello stesso campo valido per tutti
            this.w_cMatOutUnit = nvl( tmpprzmatout.valmatout, 0)
          endif
          SELECT MatOut
          REPLACE RLCOSSTA WITH this.w_cMatOutUnit
          this.w_cMatOutUnit = nvl(this.w_cMatOutUnit, 0) * nvl(MatOut.MOCOEIM1, 0) * this.w_qtaparzil
          this.w_cMatOut = this.w_cMatOut + this.w_cMatOutUnit
          ENDSCAN
          if this.oParentObject.w_Analisi <> "N"
            this.w_CSOUTSTD = this.w_CSOUTSTD + this.w_cMatOut
            this.w_CSOUTMED = this.w_CSOUTMED + this.w_cMatOut
            this.w_CSOUTULT = this.w_CSOUTULT + this.w_cMatOut
          endif
        endif
        SELECT CicLavo
        SCAN FOR cacodric=this.w_codiartic2 and lvlkey1=this.w_lvlkey &&and codicediba=this.w_codiartico
        this.w_CONTRATTO = False
        if cofllott="N"
          * --- Costo setup non spalmabile su lotto
          this.w_qtalmstp = 1
        else
          this.w_qtalmstp = IIF(this.w_PARELAB or this.oParentObject.w_LOTTOMED or this.w_maxlvl>0, this.w_qtaparzil / nvl(dplotmed,1), 1)
        endif
        if lrtiptem $ "S-W"
          this.w_cosselez = this.w_cosselez + ((Ciclavo.lrtemsec * this.w_qtalmstp)) * CicLavo.rlcossta + Ciclavo.cocosstp * this.w_qtalmstp
        else
          this.w_cosselez = this.w_cosselez + (Ciclavo.lrtemsec * (1+CicLavo.droverhe/100) * this.w_qtaparzil) * CicLavo.rlcossta
        endif
        do case
          case this.oParentObject.w_Analisi="S"
            * --- Atrezzaggio+avviamento
            if lrtiptem $ "S-W"
              this.w_CSRICSEL = this.w_CSRICSEL + Ciclavo.lrtemsec * CicLavo.rlcossta * this.w_qtalmstp
              if rlintest="E"
                * --- Fase esterna
                this.w_CSRICSEL = this.w_CSRICSEL + Ciclavo.cocosstp * this.w_qtalmstp
              endif
            else
              this.w_CSNETSEL = this.w_CSNETSEL + Ciclavo.lrtemsec * CicLavo.rlcossta * this.w_qtaparzin
              this.w_CSSCASEL = this.w_CSSCASEL + Ciclavo.lrtemsec * CicLavo.rlcossta * (this.w_qtaparzil - this.w_qtaparzin)
              this.w_CSOVHSEL = this.w_CSOVHSEL + Ciclavo.lrtemsec * CicLavo.rlcossta * CicLavo.droverhe/100 * this.w_qtaparzil
            endif
          case this.oParentObject.w_Analisi="D"
            * --- Atrezzaggio+avviamento
            if lrtiptem $ "S-W"
              if rlintest="E"
                * --- Fase esterna
                this.w_CSRICSELE = this.w_CSRICSELE + CicLavo.lrtemsec * CicLavo.rlcossta * this.w_qtalmstp
                this.w_CSRICSELE = this.w_CSRICSELE + Ciclavo.cocosstp * this.w_qtalmstp
              else
                this.w_CSRICSEL = this.w_CSRICSEL + CicLavo.lrtemsec * CicLavo.rlcossta * this.w_qtalmstp
                this.w_CSRICSEL = this.w_CSRICSEL + Ciclavo.cocosstp * this.w_qtalmstp
              endif
            else
              do case
                case rlintest="E"
                  this.w_CSNETSELE = this.w_CSNETSELE + CicLavo.lrtemsec * CicLavo.rlcossta * this.w_qtaparzin
                  this.w_CSSCASELE = this.w_CSSCASELE + CicLavo.lrtemsec * CicLavo.rlcossta * (this.w_qtaparzil - this.w_qtaparzin)
                  this.w_CSOVHSELE = this.w_CSOVHSELE + CicLavo.lrtemsec * CicLavo.rlcossta * CicLavo.droverhe/100 * this.w_qtaparzil
                otherwise
                  this.w_CSNETSEL = this.w_CSNETSEL + CicLavo.lrtemsec * CicLavo.rlcossta * this.w_qtaparzin
                  this.w_CSSCASEL = this.w_CSSCASEL + CicLavo.lrtemsec * CicLavo.rlcossta * (this.w_qtaparzil - this.w_qtaparzin)
                  this.w_CSOVHSEL = this.w_CSOVHSEL + CicLavo.lrtemsec * CicLavo.rlcossta * CicLavo.droverhe/100 * this.w_qtaparzil
              endcase
            endif
        endcase
        ENDSCAN
        SELECT (this.OutCursor)
        this.w_UNIMIS = NVL(COUNIMIS, SPACE(3))
        this.w_QTAMOV = nvl(COCOEIMP, 0)
        this.w_QTAUM1 = nvl(COCOEUM1, 0)
        this.w_PREZUM = nvl(CAPREZUM,space(1))
        this.w_UNMIS1 = nvl(ARUNMIS1,space(3))
        this.w_UNMIS2 = nvl(ARUNMIS2,space(3))
        this.w_OPERAT = nvl(AROPERAT,space(1))
        this.w_MOLTIP = nvl(ARMOLTIP,0)
        if this.w_CONTRATTO and arpropre="L"
          * --- Nessun Ciclo associato: cerca nei contratti
          Select Terzisti 
 locate for cacodric=this.w_codiartic1
          if nvl(this.w_PREZUM,"N")<>"S"
            * --- Ogni contratto valido va bene
            if found() and not empty(prcodfor)
              * --- Cerca un contratto valido per l'articolo ed il fornitore abituale
              Select Contratti 
 locate for cacodric=this.w_codiartic1 and cocodclf=Terzisti.prcodfor
            else
              * --- Cerca un contratto valido per l'articolo
              Select Contratti 
 locate for cacodric=this.w_codiartic1
            endif
          else
            * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
            *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
            if found() and not empty(prcodfor)
              * --- Cerca un contratto valido per l'articolo ed il fornitore abituale
              Select Contratti 
 locate for cacodric=this.w_codiartic1 and cocodclf=Terzisti.prcodfor and counimis=this.w_UNMIS1
            else
              * --- Cerca un contratto valido per l'articolo
              Select Contratti 
 locate for cacodric=this.w_codiartic1 and counimis=this.w_UNMIS1
            endif
          endif
          if found()
            this.w_COFLLOTP = cofllotp
            this.w_VALCONT = cocodval
            this.w_PRZSET = cocosset
            if counimis=this.w_UNMIS1
              this.w_PREZZO = coprezzo
              this.w_LOTPRO = nvl(colotpro , 0)
            else
              * --- Effettuo le conversioni (ho usato la Seconda Unit� di misura)
              this.w_LOTPRO = CALQTA(nvl(colotpro , 0), counimis,this.w_UNMIS2, this.w_OPERAT, this.w_MOLTIP, "", "N", "",, this.w_UNMIS1, "", 0)
              if this.w_OPERAT="/"
                this.w_PREZZO = cp_ROUND(coprezzo / this.w_MOLTIP, 5)
              else
                this.w_PREZZO = cp_ROUND(coprezzo * this.w_MOLTIP, 5)
              endif
            endif
            if .f.
              if this.w_VALCONT<>g_PERVAL
                * --- Cambio valuta
                this.w_CAO = GETCAM(this.w_VALCONT, i_DATSYS, 0)
                if this.w_CAO<>0
                  this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
                  this.w_PRZSET = iif(this.w_PRZSET=0, 0, VAL2MON(this.w_PRZSET, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL))
                else
                  ah_ErrorMsg("Impossibile fare il cambio di valuta per il contratto %1",48,"", Contratti.conumero)
                endif
              endif
            endif
            Insert into CicLavo (cacodric, cldescic, rlcodice, rlintest, lfcodfor, ; 
 lrcodris, codescri, cocosstp, clfascla, cofllott, raggrupp, ; 
 rldescri, rlumtdef, rlcossta, droverhe, utfatcon, lrtiptem, lrtemsec, dplotmed,lotmedfa,lvlkey1) ; 
 Values ; 
 (this.w_codiartico, "Listino C/lavoro: "+Contratti.conumero, "Listino C/lavoro", "E", Contratti.cocodclf, ; 
 Contratti.conumero, Contratti.codescon, this.w_PRZSET, "S", this.w_COFLLOTP, 3, ; 
 Contratti.codescon, "<uni>", this.w_PREZZO, 0, 3600, "L", 1, iif(this.w_LOTPRO=0, 1, this.w_LOTPRO), iif(this.w_LOTPRO=0, 1, this.w_LOTPRO) , this.w_lvlkey)
            this.w_RECADDED = True
            Select CicLavo
            if this.w_COFLLOTP="N"
              * --- Costo setup non spalmabile su lotto
              this.w_qtalmstp = 1
            else
              this.w_qtalmstp = IIF(this.w_PARELAB or this.oParentObject.w_LOTTOMED or this.w_maxlvl>0, this.w_qtaparzil / nvl(dplotmed,1), 1)
            endif
            if this.oParentObject.w_Analisi<>"N"
              * --- Atrezzaggio+avviamento=0 - Fase esterna salto codice
              this.w_CSRICSEL = this.w_CSRICSEL + Ciclavo.cocosstp * this.w_qtalmstp
              this.w_CSNETSEL = this.w_CSNETSEL + CicLavo.lrtemsec * CicLavo.rlcossta * this.w_qtaparzin
              this.w_CSSCASEL = this.w_CSSCASEL + CicLavo.lrtemsec * CicLavo.rlcossta * (this.w_qtaparzil - this.w_qtaparzin)
              this.w_CSOVHSEL = this.w_CSOVHSEL + CicLavo.lrtemsec * CicLavo.rlcossta * CicLavo.droverhe/100 * this.w_qtaparzil
            endif
            this.w_cosselez = this.w_cosselez + (CicLavo.lrtemsec * (1+CicLavo.droverhe/100) * this.w_qtaparzil) * CicLavo.rlcossta + ; 
 Ciclavo.cocosstp * this.w_qtalmstp
          endif
        endif
        SELECT (this.OutCursor)
        this.w_COPERRIC = iif(this.oParentObject.w_PERRIC="S", 1+coperric/100, 1)
        if isnull(this.w_cosselez)
          * --- ERRORE: prezzo non definito!
          this.w_cosselez = 0
        else
          this.w_cosselez = this.w_cosselez && * (1 + iif(this.oParentObject.w_PERRIC="S",coperric/100,0))
        endif
        if this.oParentObject.w_Analisi<>"N"
          this.w_CSNETSEL = nvl(this.w_CSNETSEL,0)
          this.w_CSSCASEL = nvl(this.w_CSSCASEL,0)
          this.w_CSRICSEL = nvl(this.w_CSRICSEL,0)
          * --- Storno gli output di fase
          this.w_cossta = this.w_cossta - this.w_cMatOut
          this.w_cosmed = this.w_cosmed - this.w_cMatOut
          this.w_cosult = this.w_cosult - this.w_cMatOut
          this.w_cosselez = this.w_cosselez - this.w_cMatOut
          if this.oParentObject.w_PERRIC="S"
            this.w_CMRICSEL = (costo+this.w_cosselez) * (this.w_COPERRIC-1)
            this.w_CMRICSTD = (cstandr+this.w_cosselez) * (this.w_COPERRIC-1)
            this.w_CMRICMED = (cmedio+this.w_cosselez) * (this.w_COPERRIC-1)
            this.w_CMRICULT = (cultim+this.w_cosselez) * (this.w_COPERRIC-1)
          endif
          this.w_cossta = (cstandr+this.w_cosselez) * this.w_COPERRIC
          this.w_cosmed = (cmedio+this.w_cosselez) * this.w_COPERRIC
          this.w_cosult = (cultim+this.w_cosselez) * this.w_COPERRIC
          this.w_cosselez = (costo+this.w_cosselez) * this.w_COPERRIC
          REPLACE costo WITH this.w_cosselez, cstandr WITH this.w_cossta ;
          cmedio WITH this.w_cosmed, cultim WITH this.w_cosult
        else
          this.w_cosselez = this.w_cosselez - this.w_cMatOut
          this.w_cosselez = (costo+this.w_cosselez) * this.w_COPERRIC
          REPLACE costo WITH this.w_cosselez
        endif
      endif
      this.w_FLCOMPPAD = SPACE(1)
      SELECT (this.OutCursor)
      this.w_mykey = lvlkey
      if this.w_maxlvl>0 and this.w_TROVATO
        * --- Aggiorna il padre
        SEEK left(this.w_mykey, this.w_maxlvl*this.w_LenLvl-1) ORDER lvlkey
        this.w_FLCOMPPAD = NVL(FLCOMP, SPACE(1))
        if this.w_FLCOMPPAD<>"S"
          * --- se il padre non � un componente allora aggiorno il valore
          if this.oParentObject.w_Analisi<>"N"
            REPLACE costo WITH costo+this.w_cosselez, cstandr WITH cstandr+this.w_cossta, ;
            cmedio WITH cmedio+this.w_cosmed, cultim WITH cultim+this.w_cosult
          else
            REPLACE costo WITH costo+this.w_cosselez
          endif
        endif
        SEEK this.w_mykey ORDER lvlkey
      endif
      if this.w_FLCOMPPAD<>"S"
        if this.oParentObject.w_Analisi<>"N" and this.w_TROVATO
          this.w_CODIAR = left(this.w_mykey, this.w_LenLvl-1) + chr(255)
          if FLCOMP="S" OR NOT this.w_FORNITO
            if this.oParentObject.w_Analisi="S" or this.w_maxlvl = 1
              * --- Nel caso di analisi sintetica oppure se siamo nel caso si analisi dettagliata
              *     e siamo a primo livello il costo dei materiali li metto nel costo del prodotto
              * --- Costo materiali netto
              this.w_CODTMP = left(this.w_CODIAR + "01" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmnetsel, cstandr WITH cstandr+ this.w_Cmnetstd, ;
              cmedio WITH cmedio+ this.w_Cmnetmed, cultim WITH cultim+ this.w_Cmnetult
              * --- Costo materiali scarto
              this.w_CODTMP = left(this.w_CODIAR + "02" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmscasel, cstandr WITH cstandr+ this.w_Cmscastd, ;
              cmedio WITH cmedio+ this.w_Cmscamed, cultim WITH cultim+ this.w_Cmscault
              * --- Costo materiali ricarico
              this.w_CODTMP = left(this.w_CODIAR + "03" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
              cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
            else
              * --- Costo dei materiali nel costo dei componenti
              * --- Costo materiali netto
              this.w_CODTMP = left(this.w_CODIAR + "13" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmnetsel, cstandr WITH cstandr+ this.w_Cmnetstd, ;
              cmedio WITH cmedio+ this.w_Cmnetmed, cultim WITH cultim+ this.w_Cmnetult
              * --- Costo materiali scarto
              this.w_CODTMP = left(this.w_CODIAR + "14" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmscasel, cstandr WITH cstandr+ this.w_Cmscastd, ;
              cmedio WITH cmedio+ this.w_Cmscamed, cultim WITH cultim+ this.w_Cmscault
              * --- Costo materiali ricarico
              this.w_CODTMP = left(this.w_CODIAR + "15" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
              cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
            endif
          else
            if this.oParentObject.w_Analisi="S" or this.w_maxlvl = 0
              if this.oParentObject.w_PERRIC="S"
                * --- Costo materiali ricarico
                this.w_CODTMP = left(this.w_CODIAR + "03" + space(this.w_CODLEN),this.w_CODLEN)
                SEEK this.w_CODTMP ORDER lvlkey
                REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
                cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
              endif
              * --- ------------------------------------------------------------------------------------------------------
              * --- Materiali di output
              this.w_CODTMP = left(this.w_CODIAR + "04" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo - this.w_cMatOut , cstandr WITH cstandr - this.w_CSOUTSTD, ;
              cmedio WITH cmedio - this.w_CSOUTMED, cultim WITH cultim - this.w_CSOUTULT
              * --- ------------------------------------------------------------------------------------------------------
              * --- Costo lavorazioni netto
              this.w_CODTMP = left(this.w_CODIAR + "05" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csnetsel, cstandr WITH cstandr+ this.w_Csnetsel, ;
              cmedio WITH cmedio+ this.w_Csnetsel, cultim WITH cultim+ this.w_Csnetsel
              * --- Costo lavorazioni scarto
              this.w_CODTMP = left(this.w_CODIAR + "06" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csscasel, cstandr WITH cstandr+ this.w_Csscasel, ;
              cmedio WITH cmedio+ this.w_Csscasel, cultim WITH cultim+ this.w_Csscasel
              * --- Costo lavorazioni setup + warm-up
              this.w_CODTMP = left(this.w_CODIAR + "07" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csricsel, cstandr WITH cstandr+ this.w_Csricsel, ;
              cmedio WITH cmedio+ this.w_Csricsel, cultim WITH cultim+ this.w_Csricsel
              * --- Costo lavorazioni overhead lavorazione
              this.w_CODTMP = left(this.w_CODIAR + "08" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_CSOVHSEL, cstandr WITH cstandr+ this.w_CSOVHSEL, ;
              cmedio WITH cmedio+ this.w_CSOVHSEL, cultim WITH cultim+ this.w_CSOVHSEL
              * --- ------------------------------------------------------------------------------------------------------
              * --- Costo lavorazioni esterne netto
              this.w_CODTMP = left(this.w_CODIAR + "09" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csnetsele , cstandr WITH cstandr+ this.w_Csnetsele, ;
              cmedio WITH cmedio+ this.w_Csnetsele, cultim WITH cultim+ this.w_Csnetsele
              * --- Costo lavorazioni esterne scarto
              this.w_CODTMP = left(this.w_CODIAR + "10" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csscasele ,cstandr WITH cstandr+ this.w_Csscasele, ;
              cmedio WITH cmedio+ this.w_Csscasele, cultim WITH cultim+ this.w_Csscasele
              * --- Costo lavorazioni esterne setup + warm-up
              this.w_CODTMP = left(this.w_CODIAR + "11" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csricsele , cstandr WITH cstandr+ this.w_Csricsele, ;
              cmedio WITH cmedio+ this.w_Csricsele, cultim WITH cultim+ this.w_Csricsele
              * --- Costo lavorazioni esterne overhead lavorazione
              this.w_CODTMP = left(this.w_CODIAR + "12" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_CSOVHSELE , cstandr WITH cstandr+ this.w_CSOVHsele, ;
              cmedio WITH cmedio+ this.w_CSOVHsele, cultim WITH cultim+ this.w_CSOVHsele
            else
              if this.oParentObject.w_PERRIC="S"
                * --- Costo materiali ricarico
                this.w_CODTMP = left(this.w_CODIAR + "15" + space(this.w_CODLEN),this.w_CODLEN)
                SEEK this.w_CODTMP ORDER lvlkey
                REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
                cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
              endif
              * --- ------------------------------------------------------------------------------------------------------
              * --- Materiali di output
              this.w_CODTMP = left(this.w_CODIAR + "16" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo - this.w_cMatOut , cstandr WITH cstandr - this.w_CSOUTSTD, ;
              cmedio WITH cmedio - this.w_CSOUTMED, cultim WITH cultim - this.w_CSOUTULT
              * --- ------------------------------------------------------------------------------------------------------
              * --- Costo lavorazioni Interne netto
              this.w_CODTMP = left(this.w_CODIAR + "17" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csnetsel, cstandr WITH cstandr+ this.w_Csnetsel, ;
              cmedio WITH cmedio+ this.w_Csnetsel, cultim WITH cultim+ this.w_Csnetsel
              * --- Costo lavorazioni Interne scarto
              this.w_CODTMP = left(this.w_CODIAR + "18" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csscasel ,cstandr WITH cstandr+ this.w_Csscasel, ;
              cmedio WITH cmedio+ this.w_Csscasel, cultim WITH cultim+ this.w_Csscasel
              * --- Costo lavorazioni interne setup + warm-up
              this.w_CODTMP = left(this.w_CODIAR + "19" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csricsel , cstandr WITH cstandr+ this.w_Csricsel, ;
              cmedio WITH cmedio+ this.w_Csricsel, cultim WITH cultim+ this.w_Csricsel
              * --- Costo lavorazioni interne overhead lavorazione
              this.w_CODTMP = left(this.w_CODIAR + "20" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_CSOVHsel , cstandr WITH cstandr+ this.w_CSOVHsel, ;
              cmedio WITH cmedio+ this.w_CSOVHsel, cultim WITH cultim+ this.w_CSOVHsel
              * --- ------------------------------------------------------------------------------------------------------
              * --- Costo lavorazioni esterne netto
              this.w_CODTMP = left(this.w_CODIAR + "21" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csnetsele , cstandr WITH cstandr+ this.w_Csnetsele, ;
              cmedio WITH cmedio+ this.w_Csnetsele, cultim WITH cultim+ this.w_Csnetsele
              * --- Costo lavorazioni esterne scarto
              this.w_CODTMP = left(this.w_CODIAR + "22" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csscasele ,cstandr WITH cstandr+ this.w_Csscasele, ;
              cmedio WITH cmedio+ this.w_Csscasele, cultim WITH cultim+ this.w_Csscasele
              * --- Costo lavorazioni esterne setup + warm-up
              this.w_CODTMP = left(this.w_CODIAR + "23" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Csricsele , cstandr WITH cstandr+ this.w_Csricsele, ;
              cmedio WITH cmedio+ this.w_Csricsele, cultim WITH cultim+ this.w_Csricsele
              * --- Costo lavorazioni esterne overhead lavorazione
              this.w_CODTMP = left(this.w_CODIAR + "24" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_CSOVHSELE , cstandr WITH cstandr+ this.w_CSOVHsele, ;
              cmedio WITH cmedio+ this.w_CSOVHsele, cultim WITH cultim+ this.w_CSOVHsele
            endif
          endif
          SEEK this.w_mykey ORDER lvlkey
        endif
      endif
      SELECT (this.OutCursor)
      ENDSCAN
      this.w_maxlvl = this.w_maxlvl-1
    enddo
    if this.w_RECADDED
      * --- Questo trucco non fa perdere l'ultimo record aggiunto dai contratti
      Select CicLavo 
 append blank 
 delete
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare w_VARIAR per definire il cursore e il campo dei codici da passare
    * --- Il campo cakeysal ha il nome fisso (questo campo non ha indice)
    if at(".", this.w_VARIAR)>0
      cCurstmp = left(this.w_VARIAR ,at(".", this.w_VARIAR)-1) 
 SELECT (cCurstmp)
      * --- begin transaction
      cp_BeginTrs()
      if (type(cCurstmp+".cakeysal") = "C")
        SCAN
        this.w_CODIAR = padr(eval(this.w_VARIAR),20)
        this.w_CAKEYSAL = eval(cCurstmp+".cakeysal")
        this.w_CODARTI = SPACE(20)
        if (type(cCurstmp+".cacodart")="C")
          this.w_CODARTI = LEFT(eval(cCurstmp+".cacodart")+SPACE(20) , 20)
        endif
        if Empty(this.w_CODARTI)
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
            +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CACODDIS',SPACE(20),'CPROWNUM',0)
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODDIS,CPROWNUM &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,SPACE(20);
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTI),'ART_TEMP','CACODART');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
            +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CACODART',this.w_CODARTI,'CACODDIS',SPACE(20),'CPROWNUM',0)
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,this.w_CODARTI;
                 ,SPACE(20);
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        ENDSCAN
      else
        SCAN
        this.w_CODIAR = padr(eval(this.w_VARIAR),20)
        * --- Insert into ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CAKEYRIF"+",CACODRIC"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
          +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CACODDIS',SPACE(20),'CPROWNUM',0)
          insert into (i_cTable) (CAKEYRIF,CACODRIC,CACODDIS,CPROWNUM &i_ccchkf. );
             values (;
               this.w_KEYRIF;
               ,this.w_CODIAR;
               ,SPACE(20);
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        ENDSCAN
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Costi
    this.w_STD = iif(this.oParentObject.w_STANDARD<>"N", "=", " ")
    this.w_UTE = iif(this.oParentObject.w_UTENTE, "=", " ")
    this.w_ULT = iif(this.oParentObject.w_ULTIMO<>"N", "=", " ")
    this.w_MED = iif(this.oParentObject.w_MEDIOPON, "=", " ")
    this.w_LST = iif(this.oParentObject.w_CLISTINO, "=", " ")
    SELECT cacodric, lvlkey1, max(nvl(dplotmed,1)) as dplotmed FROM ciclavo INTO CURSOR ciclavo1 GROUP BY 1,2
    Select cacodice, cacodart as codice, stadis, ; 
 max(nvl(dplotmed,1)) as dplotmed, tipocosto as tipocosto, sum(costo) as costo, costo*0 as overhe, ; 
 sum(cstandr) as cstandr, sum(cmedio) as cmedio, sum(cultim) as cultim ; 
 from (this.OutCursor) left outer join CicLavo1 on cacodric=cacodice and alltrim(lvlkey1)=alltrim(lvlkey) ; 
 where &L_OUT..livello=-1 AND &L_OUT..flcomp<>"S" group by cacodice, codice,tipocosto order by codice,tipocosto ; 
 into cursor tmpupd nofilter
    Select tmpupd 
 =FLOCK() 
 scan
    if TIPOCOSTO="01"
      this.w_PRCSMATE = 0
      this.w_PRCMMATE = 0
      this.w_PRCUMATE = 0
      this.w_PRCLMATE = 0
      this.w_PRCSMOUT = 0
      this.w_PRCMMOUT = 0
      this.w_PRCUMOUT = 0
      this.w_PRCLMOUT = 0
      this.w_PRCSLAVO = 0
      this.w_PRCULAVO = 0
      this.w_PRCMLAVO = 0
      this.w_PRCLLAVO = 0
      this.w_PRCSLAVE = 0
      this.w_PRCULAVE = 0
      this.w_PRCMLAVE = 0
      this.w_PRCLLAVE = 0
      this.w_PRCSSPGE = 0
      this.w_PRCMSPGE = 0
      this.w_PRCUSPGE = 0
      this.w_PRCLSPGE = 0
      this.w_PRCSMATC = 0
      this.w_PRCMMATC = 0
      this.w_PRCUMATC = 0
      this.w_PRCLMATC = 0
      this.w_PRCSMOUC = 0
      this.w_PRCMMOUC = 0
      this.w_PRCUMOUC = 0
      this.w_PRCLMOUC = 0
      this.w_PRCSLAVC = 0
      this.w_PRCULAVC = 0
      this.w_PRCMLAVC = 0
      this.w_PRCLLAVC = 0
      this.w_PRCSSPGC = 0
      this.w_PRCMSPGC = 0
      this.w_PRCUSPGC = 0
      this.w_PRCLSPGC = 0
      this.w_PRCSLVCE = 0
      this.w_PRCULVCE = 0
      this.w_PRCMLVCE = 0
      this.w_PRCLLVCE = 0
      * --- ------------------------------------------
      this.w_PRCOSSTA = 0
      this.w_PRCSMATE = 0
      this.w_PRCMMATE = 0
      this.w_PRCUMATE = 0
      this.w_PRCLMATE = 0
      this.w_PRCSLAVO = 0
      this.w_PRCULAVO = 0
      this.w_PRCMLAVO = 0
      this.w_PRCLLAVO = 0
      this.w_PRCSGENE = 0
      this.w_PRCUGENE = 0
      this.w_PRCMGENE = 0
      this.w_PRCLGENE = 0
    endif
    do case
      case INLIST(TIPOCOSTO , "01" , "02" , "03")
        * --- Materiali
        this.w_PRCSMATE = this.w_PRCSMATE + cstandr
        this.w_PRCMMATE = this.w_PRCMMATE + cmedio
        this.w_PRCUMATE = this.w_PRCUMATE + cultim
        this.w_PRCLMATE = this.w_PRCLMATE + costo
      case TIPOCOSTO = "04"
        * --- Materiali di output
        this.w_PRCSMOUT = this.w_PRCSMOUT +cstandr
        this.w_PRCMMOUT = this.w_PRCMMOUT +cmedio
        this.w_PRCUMOUT = this.w_PRCUMOUT +cultim
        this.w_PRCLMOUT = this.w_PRCLMOUT +costo
      case INLIST(TIPOCOSTO , "05" , "06" , "07")
        * --- Lavorazioni interne
        this.w_PRCSLAVO = this.w_PRCSLAVO + costo
        this.w_PRCULAVO = this.w_PRCULAVO + costo
        this.w_PRCMLAVO = this.w_PRCMLAVO + costo
        this.w_PRCLLAVO = this.w_PRCLLAVO + costo
      case INLIST(TIPOCOSTO , "09" , "10" , "11")
        * --- Lavorazioni esterne
        this.w_PRCSLAVE = this.w_PRCSLAVE + costo
        this.w_PRCULAVE = this.w_PRCULAVE + costo
        this.w_PRCMLAVE = this.w_PRCMLAVE + costo
        this.w_PRCLLAVE = this.w_PRCLLAVE + costo
      case INLIST(TIPOCOSTO , "08" , "12" )
        * --- overhead
        this.w_PRCSSPGE = this.w_PRCSSPGE + costo
        this.w_PRCMSPGE = this.w_PRCMSPGE + costo
        this.w_PRCUSPGE = this.w_PRCUSPGE + costo
        this.w_PRCLSPGE = this.w_PRCLSPGE + costo
      case INLIST(TIPOCOSTO , "13" , "14" , "15")
        * --- Materiali componenti
        this.w_PRCSMATC = this.w_PRCSMATC + cstandr
        this.w_PRCMMATC = this.w_PRCMMATC + cmedio
        this.w_PRCUMATC = this.w_PRCUMATC + cultim
        this.w_PRCLMATC = this.w_PRCLMATC + costo
      case TIPOCOSTO = "16"
        * --- Materiali di output
        this.w_PRCSMOUC = this.w_PRCSMOUC + cstandr
        this.w_PRCMMOUC = this.w_PRCMMOUC + cmedio
        this.w_PRCUMOUC = this.w_PRCUMOUC + cultim
        this.w_PRCLMOUC = this.w_PRCLMOUC + costo
      case INLIST(TIPOCOSTO , "17" , "18" , "19")
        * --- Lavorazione interna compenenti
        this.w_PRCSLAVC = this.w_PRCSLAVC + costo
        this.w_PRCULAVC = this.w_PRCULAVC + costo
        this.w_PRCMLAVC = this.w_PRCMLAVC + costo
        this.w_PRCLLAVC = this.w_PRCLLAVC + costo
      case INLIST(TIPOCOSTO , "21" , "22" , "23")
        * --- Lavorazione esterna compenenti
        this.w_PRCSLVCE = this.w_PRCSLVCE + costo
        this.w_PRCULVCE = this.w_PRCULVCE + costo
        this.w_PRCMLVCE = this.w_PRCMLVCE + costo
        this.w_PRCLLVCE = this.w_PRCLLVCE + costo
      case INLIST(TIPOCOSTO , "20" , "24" )
        * --- overhead
        this.w_PRCSSPGC = this.w_PRCSSPGC + costo
        this.w_PRCMSPGC = this.w_PRCMSPGC + costo
        this.w_PRCUSPGC = this.w_PRCUSPGC + costo
        this.w_PRCLSPGC = this.w_PRCLSPGC + costo
        if TIPOCOSTO=="24"
          this.w_PRCODART = codice
          this.w_PRCODICE = cacodice
          * --- Materiali
          this.w_PRCSMATE = this.w_PRCSMATE + this.w_PRCSMOUT
          this.w_PRCMMATE = this.w_PRCMMATE + this.w_PRCMMOUT
          this.w_PRCUMATE = this.w_PRCUMATE + this.w_PRCUMOUT
          this.w_PRCLMATE = this.w_PRCLMATE + this.w_PRCLMOUC
          * --- Materiali
          this.w_PRCSMATC = this.w_PRCSMATC + this.w_PRCSMOUC
          this.w_PRCMMATC = this.w_PRCMMATC + this.w_PRCMMOUC
          this.w_PRCUMATC = this.w_PRCUMATC + this.w_PRCUMOUC
          this.w_PRCLMATC = this.w_PRCLMATC + this.w_PRCLMOUC
          * --- --
          this.w_LOTMED = dplotmed
          if this.oParentObject.w_UTENTE
            do case
              case this.oParentObject.w_AGGSTAND="S"
                this.w_PRCOSSTA = this.w_PRCSLAVO + this.w_PRCSLAVE + this.w_PRCSMATE + this.w_PRCSSPGE - this.w_PRCSMOUT
                this.w_PRCOSSTA = this.w_PRCOSSTA + this.w_PRCSLAVC + this.w_PRCSLVCE + this.w_PRCSMATC + this.w_PRCSSPGC - this.w_PRCSMOUC
              case this.oParentObject.w_AGGSTAND="U"
                this.w_PRCOSSTA = this.w_PRCULAVO + this.w_PRCULAVE + this.w_PRCUMATE + this.w_PRCUSPGE - this.w_PRCUMOUT
                this.w_PRCOSSTA = this.w_PRCOSSTA + this.w_PRCULAVC + this.w_PRCULVCE + this.w_PRCUMATC + this.w_PRCUSPGC - this.w_PRCUMOUC
              case this.oParentObject.w_AGGSTAND="M"
                this.w_PRCOSSTA = this.w_PRCMLAVO + this.w_PRCMLAVE + this.w_PRCMMATE + this.w_PRCMSPGE - this.w_PRCMMOUT
                this.w_PRCOSSTA = this.w_PRCOSSTA + this.w_PRCMLAVC + this.w_PRCMLVCE + this.w_PRCMMATC + this.w_PRCMSPGC - this.w_PRCMMOUC
              case this.oParentObject.w_AGGSTAND="L"
                this.w_PRCOSSTA = this.w_PRCLLAVO + this.w_PRCLLAVE + this.w_PRCLMATE + this.w_PRCLSPGE - this.w_PRCLMOUT
                this.w_PRCOSSTA = this.w_PRCOSSTA + this.w_PRCLLAVC + this.w_PRCLLVCE + this.w_PRCLMATC + this.w_PRCLSPGC - this.w_PRCLMOUC
            endcase
          endif
          if stadis="S"
            * --- Distinte Rilasciate
            * --- Try
            local bErr_04744FE8
            bErr_04744FE8=bTrsErr
            this.Try_04744FE8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.w_MESS = "Errore di scrittura nel database%0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL"
              ah_ErrorMsg(this.w_MESS, 48, "")
            endif
            bTrsErr=bTrsErr or bErr_04744FE8
            * --- End
            if this.oParentObject.w_AGCOSSTD="S"
              * --- Variabile di appoggio per il calcolo del numero di riga
              this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO + 1
              * --- Try
              local bErr_04747AD8
              bErr_04747AD8=bTrsErr
              this.Try_04747AD8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO - 1
              endif
              bTrsErr=bTrsErr or bErr_04747AD8
              * --- End
            endif
          endif
        endif
    endcase
    endscan
    use in select("tmpupd")
    * --- Aggiorna i costi delle Materie prime
    this.w_LOTMED = 1
    Select distinct przart.*,cacodart from przart inner join (this.OutCursor) ; 
 on cacodice=cacodric ; 
 into cursor Matpri where flcomp="S" 
 Select Matpri 
 scan
    this.w_ARTCOM = CACODART
    this.w_PRCSMATE = 0
    do case
      case this.oParentObject.w_STANDARD="S"
        this.w_PRCSMATE = nvl(dicossta,0)
      case this.oParentObject.w_STANDARD="X"
        this.w_PRCSMATE = nvl(dicossta_ar, 0)
    endcase
    this.w_PRCUMATE = 0
    do case
      case this.oParentObject.w_ULTIMO="U"
        this.w_PRCUMATE = nvl(dicosult,0)
      case this.oParentObject.w_ULTIMO="A"
        this.w_PRCUMATE = nvl(slvaluca, 0)
    endcase
    this.w_PRCMMATE = iif(this.oParentObject.w_MEDIOPON, nvl(dicosmpa,0), 0)
    this.w_PRCLMATE = iif(this.oParentObject.w_CLISTINO, nvl(laprezzo,0), 0)
    * --- Try
    local bErr_046EDB98
    bErr_046EDB98=bTrsErr
    this.Try_046EDB98()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Errore di scrittura nel database %0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL")
    endif
    bTrsErr=bTrsErr or bErr_046EDB98
    * --- End
    Select Matpri 
 Endscan 
 use in Matpri
  endproc
  proc Try_04744FE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_STD,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_ULT,'PRLOTULT','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp11=cp_SetTrsOp(this.w_MED,'PRESECOS','this.oParentObject.w_ESERC',this.oParentObject.w_ESERC,'update',i_nConn)
      i_cOp12=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.oParentObject.w_NUMINV',this.oParentObject.w_NUMINV,'update',i_nConn)
      i_cOp13=cp_SetTrsOp(this.w_MED,'PRLOTMED','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
      i_cOp15=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
      i_cOp16=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp17=cp_SetTrsOp(this.w_LST,'PRLOTLIS','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp18=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.oParentObject.w_Listino',this.oParentObject.w_Listino,'update',i_nConn)
      i_cOp19=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
      i_cOp21=cp_SetTrsOp(this.w_STD,'PRCSSPGE','this.w_PRCSGENE',this.w_PRCSGENE,'update',i_nConn)
      i_cOp22=cp_SetTrsOp(this.w_ULT,'PRCUSPGE','this.w_PRCUGENE',this.w_PRCUGENE,'update',i_nConn)
      i_cOp23=cp_SetTrsOp(this.w_MED,'PRCMSPGE','this.w_PRCMGENE',this.w_PRCMGENE,'update',i_nConn)
      i_cOp24=cp_SetTrsOp(this.w_LST,'PRCLSPGE','this.w_PRCLGENE',this.w_PRCLGENE,'update',i_nConn)
      i_cOp25=cp_SetTrsOp(this.w_UTE,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVO');
      +",PRCSMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSMATE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRDATCOS');
      +",PRCULAVO ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCULAVO');
      +",PRCUMATE ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCUMATE');
      +",PRLOTULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRLOTULT');
      +",PRDATULT ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRDATULT');
      +",PRCMLAVO ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCMLAVO');
      +",PRCMMATE ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRCMMATE');
      +",PRESECOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRINVCOS');
      +",PRLOTMED ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRLOTMED');
      +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
      +",PRCLLAVO ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRCLLAVO');
      +",PRCLMATE ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRCLMATE');
      +",PRLOTLIS ="+cp_NullLink(i_cOp17,'PAR_RIOR','PRLOTLIS');
      +",PRLISCOS ="+cp_NullLink(i_cOp18,'PAR_RIOR','PRLISCOS');
      +",PRDATLIS ="+cp_NullLink(i_cOp19,'PAR_RIOR','PRDATLIS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
      +",PRCSSPGE ="+cp_NullLink(i_cOp21,'PAR_RIOR','PRCSSPGE');
      +",PRCUSPGE ="+cp_NullLink(i_cOp22,'PAR_RIOR','PRCUSPGE');
      +",PRCMSPGE ="+cp_NullLink(i_cOp23,'PAR_RIOR','PRCMSPGE');
      +",PRCLSPGE ="+cp_NullLink(i_cOp24,'PAR_RIOR','PRCLSPGE');
      +",PRCOSSTA ="+cp_NullLink(i_cOp25,'PAR_RIOR','PRCOSSTA');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCSLAVO = &i_cOp1.;
          ,PRCSMATE = &i_cOp2.;
          ,PRLOTSTD = &i_cOp3.;
          ,PRDATCOS = &i_cOp4.;
          ,PRCULAVO = &i_cOp5.;
          ,PRCUMATE = &i_cOp6.;
          ,PRLOTULT = &i_cOp7.;
          ,PRDATULT = &i_cOp8.;
          ,PRCMLAVO = &i_cOp9.;
          ,PRCMMATE = &i_cOp10.;
          ,PRESECOS = &i_cOp11.;
          ,PRINVCOS = &i_cOp12.;
          ,PRLOTMED = &i_cOp13.;
          ,PRDATMED = &i_cOp14.;
          ,PRCLLAVO = &i_cOp15.;
          ,PRCLMATE = &i_cOp16.;
          ,PRLOTLIS = &i_cOp17.;
          ,PRLISCOS = &i_cOp18.;
          ,PRDATLIS = &i_cOp19.;
          ,PUUTEELA = i_CODUTE;
          ,PRCSSPGE = &i_cOp21.;
          ,PRCUSPGE = &i_cOp22.;
          ,PRCMSPGE = &i_cOp23.;
          ,PRCLSPGE = &i_cOp24.;
          ,PRCOSSTA = &i_cOp25.;
          ,PRTIPCON = "F";
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_rows=0
      * --- Insert into PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODART"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PRCODART),'PAR_RIOR','PRCODART');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_PRCODART)
        insert into (i_cTable) (PRCODART &i_ccchkf. );
           values (;
             this.w_PRCODART;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_STD,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
        i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_ULT,'PRLOTULT','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
        i_cOp11=cp_SetTrsOp(this.w_MED,'PRESECOS','this.oParentObject.w_ESERC',this.oParentObject.w_ESERC,'update',i_nConn)
        i_cOp12=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.oParentObject.w_NUMINV',this.oParentObject.w_NUMINV,'update',i_nConn)
        i_cOp13=cp_SetTrsOp(this.w_MED,'PRLOTMED','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
        i_cOp15=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
        i_cOp16=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
        i_cOp17=cp_SetTrsOp(this.w_LST,'PRLOTLIS','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp18=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.oParentObject.w_Listino',this.oParentObject.w_Listino,'update',i_nConn)
        i_cOp19=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
        i_cOp21=cp_SetTrsOp(this.w_STD,'PRCSSPGE','this.w_PRCSGENE',this.w_PRCSGENE,'update',i_nConn)
        i_cOp22=cp_SetTrsOp(this.w_ULT,'PRCUSPGE','this.w_PRCUGENE',this.w_PRCUGENE,'update',i_nConn)
        i_cOp23=cp_SetTrsOp(this.w_MED,'PRCMSPGE','this.w_PRCMGENE',this.w_PRCMGENE,'update',i_nConn)
        i_cOp24=cp_SetTrsOp(this.w_LST,'PRCLSPGE','this.w_PRCLGENE',this.w_PRCLGENE,'update',i_nConn)
        i_cOp25=cp_SetTrsOp(this.w_UTE,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCSLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVO');
        +",PRCSMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSMATE');
        +",PRLOTSTD ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRLOTSTD');
        +",PRDATCOS ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRDATCOS');
        +",PRCULAVO ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCULAVO');
        +",PRCUMATE ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCUMATE');
        +",PRLOTULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRLOTULT');
        +",PRDATULT ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRDATULT');
        +",PRCMLAVO ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCMLAVO');
        +",PRCMMATE ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRCMMATE');
        +",PRESECOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRESECOS');
        +",PRINVCOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRINVCOS');
        +",PRLOTMED ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRLOTMED');
        +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
        +",PRCLLAVO ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRCLLAVO');
        +",PRCLMATE ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRCLMATE');
        +",PRLOTLIS ="+cp_NullLink(i_cOp17,'PAR_RIOR','PRLOTLIS');
        +",PRLISCOS ="+cp_NullLink(i_cOp18,'PAR_RIOR','PRLISCOS');
        +",PRDATLIS ="+cp_NullLink(i_cOp19,'PAR_RIOR','PRDATLIS');
        +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
        +",PRCSSPGE ="+cp_NullLink(i_cOp21,'PAR_RIOR','PRCSSPGE');
        +",PRCUSPGE ="+cp_NullLink(i_cOp22,'PAR_RIOR','PRCUSPGE');
        +",PRCMSPGE ="+cp_NullLink(i_cOp23,'PAR_RIOR','PRCMSPGE');
        +",PRCLSPGE ="+cp_NullLink(i_cOp24,'PAR_RIOR','PRCLSPGE');
        +",PRCOSSTA ="+cp_NullLink(i_cOp25,'PAR_RIOR','PRCOSSTA');
        +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
               )
      else
        update (i_cTable) set;
            PRCSLAVO = &i_cOp1.;
            ,PRCSMATE = &i_cOp2.;
            ,PRLOTSTD = &i_cOp3.;
            ,PRDATCOS = &i_cOp4.;
            ,PRCULAVO = &i_cOp5.;
            ,PRCUMATE = &i_cOp6.;
            ,PRLOTULT = &i_cOp7.;
            ,PRDATULT = &i_cOp8.;
            ,PRCMLAVO = &i_cOp9.;
            ,PRCMMATE = &i_cOp10.;
            ,PRESECOS = &i_cOp11.;
            ,PRINVCOS = &i_cOp12.;
            ,PRLOTMED = &i_cOp13.;
            ,PRDATMED = &i_cOp14.;
            ,PRCLLAVO = &i_cOp15.;
            ,PRCLMATE = &i_cOp16.;
            ,PRLOTLIS = &i_cOp17.;
            ,PRLISCOS = &i_cOp18.;
            ,PRDATLIS = &i_cOp19.;
            ,PUUTEELA = i_CODUTE;
            ,PRCSSPGE = &i_cOp21.;
            ,PRCUSPGE = &i_cOp22.;
            ,PRCMSPGE = &i_cOp23.;
            ,PRCLSPGE = &i_cOp24.;
            ,PRCOSSTA = &i_cOp25.;
            ,PRTIPCON = "F";
            &i_ccchkf. ;
         where;
            PRCODART = this.w_PRCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Costo Standard
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSLAVE','this.w_PRCSLAVE',this.w_PRCSLAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_STD,'PRCSSPGE','this.w_PRCSSPGE',this.w_PRCSSPGE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_STD,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVO');
      +",PRCSLAVE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSLAVE');
      +",PRCSMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCSMATE');
      +",PRCSSPGE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCSSPGE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCSLAVO = &i_cOp1.;
          ,PRCSLAVE = &i_cOp2.;
          ,PRCSMATE = &i_cOp3.;
          ,PRCSSPGE = &i_cOp4.;
          ,PRLOTSTD = &i_cOp5.;
          ,PRDATCOS = &i_cOp6.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVC','this.w_PRCSLAVC',this.w_PRCSLAVC,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSLVCE','this.w_PRCSLVCE',this.w_PRCSLVCE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_STD,'PRCSMATC','this.w_PRCSMATC',this.w_PRCSMATC,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_STD,'PRCSSPGC','this.w_PRCSSPGC',this.w_PRCSSPGC,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSLAVC ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVC');
      +",PRCSLVCE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSLVCE');
      +",PRCSMATC ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCSMATC');
      +",PRCSSPGC ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCSSPGC');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCSLAVC = &i_cOp1.;
          ,PRCSLVCE = &i_cOp2.;
          ,PRCSMATC = &i_cOp3.;
          ,PRCSSPGC = &i_cOp4.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Costo Medio
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MED,'PRCMLAVE','this.w_PRCMLAVE',this.w_PRCMLAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MED,'PRCMSPGE','this.w_PRCMSPGE',this.w_PRCMSPGE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_MED,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_MED,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCMLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCMLAVO');
      +",PRCMLAVE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCMLAVE');
      +",PRCMMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATE');
      +",PRCMSPGE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCMSPGE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCMLAVO = &i_cOp1.;
          ,PRCMLAVE = &i_cOp2.;
          ,PRCMMATE = &i_cOp3.;
          ,PRCMSPGE = &i_cOp4.;
          ,PRLOTSTD = &i_cOp5.;
          ,PRDATCOS = &i_cOp6.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_MED,'PRCMLAVC','this.w_PRCMLAVC',this.w_PRCMLAVC,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MED,'PRCMLVCE','this.w_PRCMLVCE',this.w_PRCMLVCE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATC','this.w_PRCMMATC',this.w_PRCMMATC,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MED,'PRCMSPGC','this.w_PRCMSPGC',this.w_PRCMSPGC,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCMLAVC ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCMLAVC');
      +",PRCMLVCE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCMLVCE');
      +",PRCMMATC ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATC');
      +",PRCMSPGC ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCMSPGC');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCMLAVC = &i_cOp1.;
          ,PRCMLVCE = &i_cOp2.;
          ,PRCMMATC = &i_cOp3.;
          ,PRCMSPGC = &i_cOp4.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Costo Ultimo
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCULAVE','this.w_PRCULAVE',this.w_PRCULAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_ULT,'PRCUSPGE','this.w_PRCUSPGE',this.w_PRCUSPGE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_ULT,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_ULT,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCULAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCULAVO');
      +",PRCULAVE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCULAVE');
      +",PRCUMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCUMATE');
      +",PRCUSPGE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCUSPGE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCULAVO = &i_cOp1.;
          ,PRCULAVE = &i_cOp2.;
          ,PRCUMATE = &i_cOp3.;
          ,PRCUSPGE = &i_cOp4.;
          ,PRLOTSTD = &i_cOp5.;
          ,PRDATCOS = &i_cOp6.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_ULT,'PRCULAVC','this.w_PRCULAVC',this.w_PRCULAVC,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCULVCE','this.w_PRCULVCE',this.w_PRCULVCE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_ULT,'PRCUMATC','this.w_PRCUMATC',this.w_PRCUMATC,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_ULT,'PRCUSPGC','this.w_PRCUSPGC',this.w_PRCUSPGC,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCULAVC ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCULAVC');
      +",PRCULVCE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCULVCE');
      +",PRCUMATC ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCUMATC');
      +",PRCUSPGC ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCUSPGC');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCULAVC = &i_cOp1.;
          ,PRCULVCE = &i_cOp2.;
          ,PRCUMATC = &i_cOp3.;
          ,PRCUSPGC = &i_cOp4.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Costo Listino
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_LST,'PRCLLAVE','this.w_PRCLLAVE',this.w_PRCLLAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLSPGE','this.w_PRCLSPGE',this.w_PRCLSPGE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_LST,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_LST,'PRDATCOS','this.w_DATRIF',this.w_DATRIF,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCLLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCLLAVO');
      +",PRCLLAVE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCLLAVE');
      +",PRCLMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCLMATE');
      +",PRCLSPGE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLSPGE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCLLAVO = &i_cOp1.;
          ,PRCLLAVE = &i_cOp2.;
          ,PRCLMATE = &i_cOp3.;
          ,PRCLSPGE = &i_cOp4.;
          ,PRLOTSTD = &i_cOp5.;
          ,PRDATCOS = &i_cOp6.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_LST,'PRCLLAVC','this.w_PRCLLAVC',this.w_PRCLLAVC,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_LST,'PRCLLVCE','this.w_PRCLLVCE',this.w_PRCLLVCE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_LST,'PRCLMATC','this.w_PRCLMATC',this.w_PRCLMATC,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLSPGC','this.w_PRCLSPGC',this.w_PRCLSPGC,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCLLAVC ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCLLAVC');
      +",PRCLLVCE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCLLVCE');
      +",PRCLMATC ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCLMATC');
      +",PRCLSPGC ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLSPGC');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRCODART);
             )
    else
      update (i_cTable) set;
          PRCLLAVC = &i_cOp1.;
          ,PRCLLVCE = &i_cOp2.;
          ,PRCLMATC = &i_cOp3.;
          ,PRCLSPGC = &i_cOp4.;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04747AD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIFSTO),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODICE),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODART),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRCODART),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMRIGSTO),'ART_TEMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIFSTO,'CACODRIC',this.w_PRCODICE,'CAKEYSAL',this.w_PRCODART,'CACODART',this.w_PRCODART,'CPROWNUM',this.oParentObject.w_NUMRIGSTO,'CACODDIS',SPACE(20))
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CPROWNUM,CACODDIS &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIFSTO;
           ,this.w_PRCODICE;
           ,this.w_PRCODART;
           ,this.w_PRCODART;
           ,this.oParentObject.w_NUMRIGSTO;
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_046EDB98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_MED,'PRESECOS','this.oParentObject.w_ESERC',this.oParentObject.w_ESERC,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.oParentObject.w_NUMINV',this.oParentObject.w_NUMINV,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_MED,'PRDATMED','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
      i_cOp11=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.oParentObject.w_Listino',this.oParentObject.w_Listino,'update',i_nConn)
      i_cOp12=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSMATE ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSMATE');
      +",PRCUMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCUMATE');
      +",PRCMMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATE');
      +",PRCLMATE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLMATE');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
      +",PRDATULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRDATULT');
      +",PRESECOS ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRINVCOS');
      +",PRDATMED ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATMED');
      +",PRLISCOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRLISCOS');
      +",PRDATLIS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRDATLIS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
             )
    else
      update (i_cTable) set;
          PRCSMATE = &i_cOp1.;
          ,PRCUMATE = &i_cOp2.;
          ,PRCMMATE = &i_cOp3.;
          ,PRCLMATE = &i_cOp4.;
          ,PRTIPCON = "F";
          ,PRDATCOS = &i_cOp6.;
          ,PRDATULT = &i_cOp7.;
          ,PRESECOS = &i_cOp8.;
          ,PRINVCOS = &i_cOp9.;
          ,PRDATMED = &i_cOp10.;
          ,PRLISCOS = &i_cOp11.;
          ,PRDATLIS = &i_cOp12.;
          ,PUUTEELA = i_CODUTE;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARTCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_rows=0
      * --- Insert into PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODART"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIOR','PRCODART');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM)
        insert into (i_cTable) (PRCODART &i_ccchkf. );
           values (;
             this.w_ARTCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_MED,'PRESECOS','this.oParentObject.w_ESERC',this.oParentObject.w_ESERC,'update',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.oParentObject.w_NUMINV',this.oParentObject.w_NUMINV,'update',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_MED,'PRDATMED','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
        i_cOp11=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.oParentObject.w_Listino',this.oParentObject.w_Listino,'update',i_nConn)
        i_cOp12=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.oParentObject.w_DATSTA',this.oParentObject.w_DATSTA,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCSMATE ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSMATE');
        +",PRCUMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCUMATE');
        +",PRCMMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATE');
        +",PRCLMATE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLMATE');
        +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
        +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
        +",PRDATULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRDATULT');
        +",PRESECOS ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRESECOS');
        +",PRINVCOS ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRINVCOS');
        +",PRDATMED ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATMED');
        +",PRLISCOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRLISCOS');
        +",PRDATLIS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRDATLIS');
        +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
               )
      else
        update (i_cTable) set;
            PRCSMATE = &i_cOp1.;
            ,PRCUMATE = &i_cOp2.;
            ,PRCMMATE = &i_cOp3.;
            ,PRCLMATE = &i_cOp4.;
            ,PRTIPCON = "F";
            ,PRDATCOS = &i_cOp6.;
            ,PRDATULT = &i_cOp7.;
            ,PRESECOS = &i_cOp8.;
            ,PRINVCOS = &i_cOp9.;
            ,PRDATMED = &i_cOp10.;
            ,PRLISCOS = &i_cOp11.;
            ,PRDATLIS = &i_cOp12.;
            ,PUUTEELA = i_CODUTE;
            &i_ccchkf. ;
         where;
            PRCODART = this.w_ARTCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiude i cursori
    use in select("appogg")
    use in select("CicLavo")
    use in select("PrzList")
    USE IN SELECT(this.OutCursor)
    use in select("Contratti")
    use in select("Terzisti")
    use in select("FasClMono")
    use in select("matout")
    use in select ("tmpprzmatout")
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
     SELECT DISTINCT cacodice as cacodice, ; 
 cacodart + "####################" AS cakeysal , cacodart as cacodart, codicediba as cacoddis; 
 from (this.OutCursor) where (tippro $ "PF-SE-PH" and arpropre $ "IL" and !empty(codicediba)) INTO cursor ArtDist nofilter
    * --- Try
    local bErr_048A0780
    bErr_048A0780=bTrsErr
    this.Try_048A0780()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=MSG_TRANSACTION_ERROR
    endif
    bTrsErr=bTrsErr or bErr_048A0780
    * --- End
    if .f.
      this.w_VARIAR = "ArtDist.cacodice"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used("ArtDist")
      USE IN ("ArtDist")
    endif
    * --- Carica in memoria i Cicli di Lavorazione con i costi delle risorse
    vq_exec("..\PRFA\EXE\QUERY\GSCI_SDC", this, "CicLavo")
    * --- Nuova query che estrae i materiali di output del ciclo per calcolo sfrido
    vq_exec("..\PRFA\EXE\QUERY\GSCI2SDC", this, "MATOUT")
    SELECT distinct &L_OUT..codicediba as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , MATOUT.* ; 
 FROM MATOUT inner JOIN &L_OUT ON MATOUT.cacodric= &L_OUT..cacodice AND &L_OUT..livello=0 ; 
 INTO CURSOR TmpMatOut readwrite ; 
 Union ;
    SELECT distinct ; 
 &L_OUT..corifdis as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , MATOUT.* ; 
 FROM MATOUT inner JOIN &L_OUT ON MATOUT.cacodric= &L_OUT..cacodice AND !EMPTY(NVL(&L_OUT..codicediba, "")) ; 
 AND &L_OUT..livello>0
    Select * From TmpMatOut into Cursor MATOUT
    Use in select("TmpMatOut")
    =WRCURSOR("MATOUT")
    if g_COLA="S"
      * --- Carica Contratti delle lavorazioni esterne
      vq_exec("..\COLA\EXE\QUERY\GSCO_SDC", this, "Contratti") 
 vq_exec("..\COLA\EXE\QUERY\GSCO_SDC2", this, "Terzisti") 
 vq_exec("..\COLA\EXE\QUERY\GSCO_SDC7", this, "Matforn")
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Carica su ART_TEMP i codici relativi alle fasi esterne
      Select distinct cacodart as cacodart, clcoddis as cacoddis, clcodfas from Ciclavo where clfascla="S" into cursor ArtDist nofilter
      * --- Try
      local bErr_048838A0
      bErr_048838A0=bTrsErr
      this.Try_048838A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
      bTrsErr=bTrsErr or bErr_048838A0
      * --- End
      if .f.
        this.w_VARIAR = "ArtDist.clcodfas"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      USE IN SELECT("ArtDist")
      vq_exec("..\COLA\EXE\QUERY\GSCO_SDC3", this, "FasClMono")
      vq_exec("..\COLA\EXE\QUERY\GSCO_SDC1", this, "Contrfasi")
      vq_exec("..\COLA\EXE\QUERY\GSCO_SDC8", this, "Matfase")
      SELECT distinct &L_OUT..codicediba as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , ciclavo.* ; 
 FROM ciclavo inner JOIN &L_OUT ON ciclavo.cacodric= &L_OUT..cacodice AND &L_OUT..livello=0 ; 
 INTO CURSOR TmpCicLavo ; 
 Union ;
      SELECT distinct ; 
 &L_OUT..corifdis as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , ciclavo.* ; 
 FROM ciclavo inner JOIN &L_OUT ON ciclavo.cacodric= &L_OUT..cacodice AND !EMPTY(NVL(&L_OUT..codicediba, "")) ; 
 AND &L_OUT..livello>0
      Select * From TmpCicLavo into Cursor CicLavo
      if Used("TmpCicLavo")
        Use in "TmpCicLavo"
      endif
      * --- Aggiorna i Costi sulle risorse esterne
      =wrcursor("Ciclavo") 
 Select Ciclavo 
 scan for rlintest="E" and LRTIPTEM="L"
      * --- Inizializza le variabili per l'elaborazione
      if this.oParentObject.w_LOTTOMED
        this.w_LOTMEDFE = NVL(LOTMEDFA, 1)
      endif
      this.w_UNIMIS = NVL(UNIMIS, SPACE(3))
      this.w_QTAMOV = nvl(QTAMOV, 0)
      this.w_QTAUM1 = nvl(QTAUM1, 0)
      this.w_PREZUM = nvl(CAPREZUM,space(1))
      this.w_UNMIS1 = nvl(ARUNMIS1,space(3))
      this.w_UNMIS2 = nvl(ARUNMIS2,space(3))
      this.w_OPERAT = nvl(AROPERAT,space(1))
      this.w_MOLTIP = nvl(ARMOLTIP,0)
      this.w_CODFOR = lfcodfor
      this.w_CODFAS = clcodfas
      this.w_LVLKEY = LVLKEY1
      this.w_codiartico = cacodart
      this.w_codicediba = codicediba
      this.w_Contratto = SPACE(15)
      if this.oParentObject.w_LOTTOMED
        * --- Faccio la ricerca per fase
        if nvl(this.w_PREZUM,"N")="N"
          * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
          *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
          Select Contrfasi 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_CODFAS and counimis=this.w_UNMIS1 and coquant1>=this.w_LOTMEDFE
        else
          * --- Ogni contratto valido va bene
          Select Contrfasi 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_CODFAS and coquant1>=this.w_LOTMEDFE
        endif
      else
        if nvl(this.w_PREZUM,"N")="N"
          * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
          *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
          Select Contrfasi 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_CODFAS and counimis=this.w_UNMIS1 and coquant1>=this.w_QTAUM1
        else
          * --- Ogni contratto valido va bene
          Select Contrfasi 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_CODFAS and coquant1>=this.w_QTAUM1
        endif
      endif
      if !Eof()
        this.w_Contratto = Contrfasi.conumero
      endif
      if !found()
        * --- Se non trovo niente per la fase provo a vedere se � presente 
        *     un contratto valido per tutte le fasi, codice articolo della distinta
        if nvl(this.w_PREZUM,"N")="N"
          * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
          *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
          Select Contratti 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_codiartico and counimis=this.w_UNMIS1
        else
          * --- Ogni contratto valido va bene
          Select Contratti 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_codiartico
        endif
        if !Eof()
          this.w_Contratto = Contratti.conumero
        endif
      endif
      if found()
        this.w_PRZSET = cocosset
        this.w_ORAUNI = coorauni
        this.w_VALCONT = cocodval
        if counimis=this.w_UNMIS1 or this.w_ORAUNI="O"
          * --- Se il costo non � unitario ma orario non lo devo convertire
          this.w_PREZZO = coprezzo
          this.w_PRZSET = cocosset
        else
          * --- Effettuo le conversioni (ho usato la Seconda Unit� di misura)
          if this.w_OPERAT="/"
            this.w_PREZZO = cp_ROUND(coprezzo / this.w_MOLTIP, 5)
          else
            this.w_PREZZO = cp_ROUND(coprezzo * this.w_MOLTIP, 5)
          endif
        endif
        SELECT CicLavo 
 replace rlcossta with this.w_PREZZO, cocosstp with this.w_PRZSET 
 replace lrcodris with Contrfasi.conumero, codescri with Contrfasi.codescon
        if this.w_ORAUNI="U"
          replace lrtiptem with "L" , lrtemsec with 1, utfatcon with 3600, rlumtdef with "<uni>"
        endif
        * --- Se trovo un cotratto elimino dal cursore dei cicli i costi di setup e warm up
        Select Ciclavo
        this.w_RECNO = Recno()
        Delete for codicediba=this.w_codicediba and lvlkey1=this.w_lvlkey and clcodfas=this.w_CODFAS and lrtiptem<>"L"
        Select Ciclavo
        go this.w_RECNO
      endif
      * --- Cicli monofase esterne: caso salto codice. Sostituisce il codice di fase con il codice padre
      Select FasClMono 
 locate for clcodfas=this.w_CODFAS
      if found()
        this.w_Tmp = clcodart
        if nvl(this.w_PREZUM,"N")="N"
          * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
          *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
          Select Contratti 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_Tmp and counimis=this.w_UNMIS1
        else
          * --- Ogni contratto valido va bene
          Select Contratti 
 locate for cocodclf=this.w_CODFOR and cacodric=this.w_Tmp
        endif
        if found()
          this.w_PRZSET = cocosset
          this.w_COFLLOTP = cofllotp
          this.w_ORAUNI = coorauni
          this.w_VALCONT = cocodval
          if counimis=this.w_UNMIS1 or this.w_ORAUNI="O"
            * --- Se il costo non � unitario ma orario non lo devo convertire
            this.w_PREZZO = coprezzo
            this.w_PRZSET = cocosset
          else
            * --- Effettuo le conversioni (ho usato la Seconda Unit� di misura)
            if this.w_OPERAT="/"
              this.w_PREZZO = cp_ROUND(coprezzo / this.w_MOLTIP, 5)
            else
              this.w_PREZZO = cp_ROUND(coprezzo * this.w_MOLTIP, 5)
            endif
          endif
          if this.w_VALCONT<>g_PERVAL
            * --- Cambio valuta
            this.w_CAO = GETCAM(this.w_VALCONT, i_DATSYS, 0)
            if this.w_CAO<>0
              this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
              this.w_PRZSET = VAL2MON(this.w_PRZSET, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
            else
              ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato",48)
            endif
          endif
          SELECT CicLavo 
 replace rlcossta with this.w_PREZZO, cocosstp with this.w_PRZSET, cofllott with this.w_COFLLOTP
          replace lrcodris with Contratti.conumero, codescri with Contratti.codescon
          if this.w_ORAUNI="U"
            replace lrtiptem with "L" , lrtemsec with 1, utfatcon with 3600, rlumtdef with "<uni>"
          endif
          * --- Se trovo un cotratto elimino dal cursore dei cicli i costi di setup e warm up
          Select Ciclavo
          this.w_RECNO = Recno()
          Delete for codicediba=this.w_codicediba and lvlkey1=this.w_lvlkey and clcodfas=this.w_CODFAS and lrtiptem<>"L"
          Select Ciclavo
          go this.w_RECNO
        endif
      endif
      SELECT CicLavo 
 ENDSCAN
      if used("Contrfasi")
        use in Contrfasi
      endif
      if true
        * --- Segna i materiali forniti dal terzista
        Select (this.OutCursor) 
 Scan
        if livello<=0
          this.w_mykey = rtrim(lvlkey)
          this.w_oldkey = this.w_mykey
          this.w_FORNITO = .F.
        else
          * --- Ricerca nei contratti i materiali forniti dal terzista
          this.w_mykey = rtrim(lvlkey)
          this.w_codiartic1 = corifdis
          this.w_codiartico = cocodcom
          if this.w_FORNITO and this.w_mykey=this.w_oldkey
            * --- Aggiorna anche tutti i discendenti
            replace evasocl with "X"
          else
            this.w_RECNO = Recno()
            seek padr(left(this.w_mykey,len(this.w_mykey)-this.w_LenLvl), 200)
            this.w_FORNITO = arpropre="L"
            go this.w_RECNO
            if this.w_FORNITO
              * --- Ricerca nei contratti i materiali forniti dal terzista (salto codice)
              Select Terzisti 
 locate for cacodric=this.w_codiartic1
              if found() and not empty(prcodfor)
                * --- Cerca un contratto valido per l'articolo ed il fornitore abituale
                Select Matforn 
 locate for cacodric=this.w_codiartic1 and cocodclf=Terzisti.prcodfor and mccodice=this.w_codiartico
              else
                * --- Cerca un contratto valido per l'articolo
                Select Matforn 
 locate for cacodric=this.w_codiartic1 and mccodice=this.w_codiartico
              endif
              this.w_FORNITO = found()
              this.w_PREZZO = mccosart
            else
              * --- Ricerca nei contratti i materiali forniti dal terzista (fasi)
              Select Matfase 
 locate for cacodric=this.w_codiartic1 and mccodice=this.w_codiartico
              this.w_FORNITO = found()
              this.w_PREZZO = mccosart
            endif
            if this.w_FORNITO
              * --- Segna la riga
              SELECT (this.OutCursor)
              replace evasocl with "S", costocl with this.w_PREZZO
              this.w_oldkey = this.w_mykey
            endif
          endif
        endif
        SELECT (this.OutCursor)
        ENDSCAN
      endif
      use in select("matfase")
      use in select("matforn")
    else
      SELECT distinct &L_OUT..codicediba as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , ciclavo.* ; 
 FROM ciclavo inner JOIN &L_OUT ON ciclavo.cacodric= &L_OUT..cacodice AND &L_OUT..livello=0 ; 
 INTO CURSOR TmpCicLavo ; 
 Union ;
      SELECT distinct ; 
 &L_OUT..corifdis as codicediba, &L_OUT..lvlkey as lvlkey1, NVL(&L_OUT..cocoeum1,0) AS QTAUM1, ; 
 NVL(&L_OUT..counimis, space(3)) AS UNIMIS , NVL(&L_OUT..cocoeimp,0) AS QTAMOV , ciclavo.* ; 
 FROM ciclavo inner JOIN &L_OUT ON ciclavo.cacodric= &L_OUT..cacodice AND !EMPTY(NVL(&L_OUT..codicediba, "")) ; 
 AND &L_OUT..livello>0
      Select * From TmpCicLavo into Cursor CicLavo
      if Used("TmpCicLavo")
        Use in "TmpCicLavo"
      endif
    endif
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    Select CicLavo 
 INDEX ON cacodric TAG cacodric
    if this.oParentObject.w_TipoValoOut<>"NN"
      this.w_KEYRIF2 = SYS(2015)
      this.w_KEYRIF = this.w_KEYRIF2
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF2);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF2;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if this.oParentObject.w_TipoValoOut="LI"
        SELECT DISTINCT clcodfas as cacodice, mocodart AS cakeysal , mocodart as cacodart, unimis as counimis, qtaum1 as cocoeum1, qtamov as cocoeimp, recno() as cprownum from MATOUT into cursor ArtDist nofilter
      else
        SELECT DISTINCT clcodfas as cacodice, mocodart AS cakeysal , mocodart as cacodart from MATOUT into cursor ArtDist nofilter
      endif
      * --- Try
      local bErr_048007D8
      bErr_048007D8=bTrsErr
      this.Try_048007D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
      bTrsErr=bTrsErr or bErr_048007D8
      * --- End
      USE IN Select("ArtDist")
      do case
        case this.oParentObject.w_TipoValoOut="LI"
          * --- Leggo valuta del listino
          this.w_SCOLIS = "S"
          * --- Read from LISTINI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LISTINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LSVALLIS,LSFLSCON"+;
              " from "+i_cTable+" LISTINI where ";
                  +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ListinoMo);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LSVALLIS,LSFLSCON;
              from (i_cTable) where;
                  LSCODLIS = this.oParentObject.w_ListinoMo;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_VALLISMO = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
            this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_LIPREZZO = 0
          * --- Calcola Prezzo e Sconti
          * --- Azzero l'Array che verr� riempito dalla Funzione
          * --- Prezzi di listino
          USE IN SELECT("TMPPRZMATOUT")
          vq_exec("..\DISB\EXE\QUERY\GSDSLBDC2", this, "TMPPRZMATOUT")
          SELECT "TMPPRZMATOUT"
          GO TOP
          SCAN FOR !EMPTY(LICODLIS)
          this.w_LIPREZZO = 0
          this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
          this.w_MOLTIP = NVL(ARMOLTIP, 0)
          this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
          this.w_OPERAT = NVL(AROPERAT , "*" )
          this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
          this.w_OPERA3 = NVL(CAOPERAT , "*" )
          this.w_MOLTI3 = NVL(CAMOLTIP, 0)
          this.w_PREZUM = ARPREZUM
          this.w_CAUNMISU = NVL(CAUNMISU, SPACE(3))
          this.w_CAQTAMOV = NVL(CAQTAMOV, 0)
          this.w_CAQTAMO1 = NVL(CAQTAMO1, 0)
          this.w_CODIAR = left(nvl(LICODART, space(20))+space(20) , 20)
          this.w_CODVAL = nvl(LSVALLIS, g_PERVAL)
          this.w_VALLIS2 = nvl(LSVALLIS, g_PERVAL)
          DECLARE ARRCALC (16)
          ARRCALC(1)=0
          this.w_QTAUM3 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
          this.w_QTAUM2 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
          DIMENSION pArrUm[9]
          pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_CAUNMISU 
 pArrUm [3] = this.w_CAQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_CAQTAMO1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
          this.w_CAOLIS = GETCAM(this.w_VALLIS2, i_DATSYS, 0)
          this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.oParentObject.w_ListinoMo , this.w_CODIAR , " " , this.w_CAQTAMO1 , this.w_VALLIS2 , this.w_CAOLIS , this.w_DATRIF , " " , " ", g_PERVAL , " ", " ", " ", this.w_SCOLIS, 0,"M", @ARRCALC, " ", " ","N",@pArrUm )
          this.w_DECTOT = getvalut(this.oParentObject.w_VALESE,"VADECUNI")
          this.w_CLUNIMIS = ARRCALC(16)
          this.w_LIPREZZO = CAVALRIG(ARRCALC(5),1, ARRCALC(1),ARRCALC(2),ARRCALC(3),ARRCALC(4),this.w_DECTOT)
          if this.w_CAUNMISU<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
            this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_CAQTAMO1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
            this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_CAQTAMOV, this.w_QTALIS, "N", 0, this.w_DECTOT), this.w_DECTOT)
          endif
          if this.w_VALLIS2<>g_PERVAL
            * --- Cambio valuta
            this.w_CAO = GETCAM(this.w_VALLIS2, i_DATSYS, 0)
            if this.w_CAO<>0
              this.w_LIPREZZO = VAL2MON(this.w_LIPREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
            else
              ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato",48)
              this.w_ERRO = .T.
            endif
          endif
          SELECT "TMPPRZMATOUT"
          REPLACE VALMATOUT WITH this.w_LIPREZZO
          ENDSCAN
        case this.oParentObject.w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"
          vq_exec("..\DISB\EXE\QUERY\GSDS8BDC", this, "TMPPRZMATOUT")
        case this.oParentObject.w_TipoValoOut="US"
          vq_exec("..\DISB\EXE\QUERY\GSDS2BDC1", this, "TMPPRZMATOUT")
      endcase
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF2);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF2;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc
  proc Try_048A0780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    SCAN
    SCATTER MEMVAR
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(m.cacodice),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(m.cakeysal),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(m.cacodart),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(m.cacoddis),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',m.cacodice,'CAKEYSAL',m.cakeysal,'CACODART',m.cacodart,'CACODDIS',m.cacoddis,'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.w_KEYRIF;
           ,m.cacodice;
           ,m.cakeysal;
           ,m.cacodart;
           ,m.cacoddis;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT ("ArtDist")
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_048838A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    SCAN
    SCATTER MEMVAR
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(m.clcodfas),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(m.cacodart),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(m.cacoddis),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',m.clcodfas,'CAKEYSAL',SPACE(20),'CACODART',m.cacodart,'CACODDIS',m.cacoddis,'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.w_KEYRIF;
           ,m.clcodfas;
           ,SPACE(20);
           ,m.cacodart;
           ,m.cacoddis;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT ("ArtDist")
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_048007D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    SCAN
    SCATTER MEMVAR
    this.w_CODIAR = ArtDist.cacodice
    this.w_CODARTI = ArtDist.cacodart
    this.w_CAKEYSAL = ArtDist.cakeysal
    if this.oParentObject.w_TipoValoOut="LI"
      this.w_ROWNUM = ArtDist.CPROWNUM
      this.w_CAUNMISU = ArtDist.counimis
      this.w_CAQTAMOV = ArtDist.cocoeimp
      this.w_CAQTAMO1 = ArtDist.cocoeum1
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+",CAUNMISU"+",CAQTAMOV"+",CAQTAMO1"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTI),'ART_TEMP','CACODART');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAUNMISU),'ART_TEMP','CAUNMISU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMOV),'ART_TEMP','CAQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMO1),'ART_TEMP','CAQTAMO1');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CACODART',this.w_CODARTI,'CACODDIS',SPACE(20),'CPROWNUM',this.w_ROWNUM,'CAUNMISU',this.w_CAUNMISU,'CAQTAMOV',this.w_CAQTAMOV,'CAQTAMO1',this.w_CAQTAMO1)
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM,CAUNMISU,CAQTAMOV,CAQTAMO1 &i_ccchkf. );
           values (;
             this.w_KEYRIF;
             ,this.w_CODIAR;
             ,this.w_CAKEYSAL;
             ,this.w_CODARTI;
             ,SPACE(20);
             ,this.w_ROWNUM;
             ,this.w_CAUNMISU;
             ,this.w_CAQTAMOV;
             ,this.w_CAQTAMO1;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTI),'ART_TEMP','CACODART');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
        +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CACODART',this.w_CODARTI,'CACODDIS',SPACE(20),'CPROWNUM',0)
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
           values (;
             this.w_KEYRIF;
             ,this.w_CODIAR;
             ,this.w_CAKEYSAL;
             ,this.w_CODARTI;
             ,SPACE(20);
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    SELECT ("ArtDist")
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura ricorsiva di implosione distinte (Ripresa da GSDB_BIM).
    this.w_CI = 0
    * --- Metto in un cursore tutti gli articoli presenti in DIS_STOR in considerazione dei filtri inseriti sulla maschera GSDB_KDC.
    vq_exec("..\PRFA\exe\query\gsdb_kdc", this, "selezione")
    this.w_RECSEL = reccount("selezione")
    * --- Il numero massimo di distinte controllabili dall'implosione � 9.999.999
    if this.w_RECSEL>0 and this.w_RECSEL<10000000
      select selezione 
 go top 
 scan for PROCESSA="N"
      this.w_LIVELLO = selezione.lvlkey
      * --- Calcolo il livello della distinta
      if empty(this.w_LIVELLO)
        this.w_CI = this.w_CI + 1
        this.w_LIVELLO = right("000000"+alltrim(str(this.w_CI)),7)
        replace lvlkey with this.w_LIVELLO
      endif
      this.w_CODDIS = selezione.cacodice
      riga=recno()
      ah_msg("Analisi componente %1 in corso",.t.,.f.,.f.,alltrim(this.w_coddis))
      vq_exec("..\PRFA\exe\query\gsdb2kdc", this, "implosione")
      * --- Il numero massimo di livelli di implosione considerabili � 30: (250+1/8)=31, uno lo elimino per sicurezza
      if reccount("implosione")>0
        this.w_CJ = 0
        select implosione 
 go top 
 scan
        this.w_CJ = this.w_CJ + 1
        replace lvlkey with (alltrim(this.w_LIVELLO)+"."+right("000000"+alltrim(str(this.w_CJ)),7))
        scatter memvar 
 m.livellod=occurs(".",m.lvlkey) 
 insert into selezione from memvar 
 endscan
        if m.livellod+1>30
          * --- Superato il limite gestito - potrebbe anche esserci un loop.
          ah_ErrorMsg("Ci potrebbe essere un loop sul codice distinta %1%0oppure tale distinta potrebbe superare il numero di sottolivelli massimo gestito (30)","!", "",alltrim(this.w_CODDIS))
          i_retcode = 'stop'
          return
        endif
        select selezione 
 go riga
        * --- se implosione contiene almeno un record significa che questo non � un livello finale,scrivo 'N' su PSUPREMO e 
        *     segno la riga processata mettendo 'S' a PROCESSA
        replace PSUPREMO with "N", PROCESSA with "S"
      else
        select selezione 
 go riga
        * --- questo � un livello finale, lascio 'S' su PSUPREMO e segno la riga processata mettendo 'S' a PROCESSA
        replace PROCESSA with "S"
      endif
      endscan
      * --- Cancello i record dove PSUPREMO<>'S'
      delete for PSUPREMO<>"S"
      * --- Preparo il cursore di input a BSDB_BTV
      vq_exec("..\PRFA\exe\query\gsdb1kdc", this, "selezione1")
      select distinct cacodice,cadesart,cacodart,cacodvar,caunimis,caoperat,camoltip,caconfig,caconato,caconcar,caprezum,; 
 arunmis1,aroperat,armoltip,arunmis2,artipart,arpropre,dbstadit,dbstadig,dbkeysal,dbstadip,dbcodice,artipges,armagpre ; 
 from selezione union ; 
 select distinct cacodice,cadesart,cacodart,cacodvar,caunimis,caoperat,camoltip,caconfig,caconato,caconcar,caprezum,; 
 arunmis1,aroperat,armoltip,arunmis2,artipart,arpropre,dbstadit,dbstadig,dbkeysal,dbstadip,dbcodice,artipges,armagpre ; 
 from selezione1 order by 1 into cursor Incursor
    else
      if this.w_RECSEL>9999999
        ah_ErrorMsg("Il numero massimo di distinte verificabili nella selezione (9.999.999) � stato superato","!")
        i_retcode = 'stop'
        return
      else
        * --- Potrebbero non esistere dati da aggiornare per la selezione effettuata, ma � anche possibile che tutti gli articoli dell'intervallo selezionato siano livelli finali.
        *     In questo caso non passo a GSDB_BTV un cursore ma direttamente l'intervallo di selezione.
        this.w_LIVFIN = "S"
      endif
    endif
    if used("Selezione")
      USE IN "Selezione"
    endif
    if used("Implosione")
      USE IN "Implosione"
    endif
    if used("Selezione1")
      USE IN "Selezione1"
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM1,w_cCursor)
    this.w_PARAM1=w_PARAM1
    this.w_cCursor=w_cCursor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CAM_BI'
    this.cWorkTables[6]='*TMPCAMBI'
    this.cWorkTables[7]='STOCOSTA'
    this.cWorkTables[8]='LISTINI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM1,w_cCursor"
endproc
