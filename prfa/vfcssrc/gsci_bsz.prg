* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bsz                                                        *
*              Controlli gestione classe codice di fase                        *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-31                                                      *
* Last revis.: 2017-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bsz",oParentObject,m.pPROV)
return(i_retval)

define class tgsci_bsz as StdBatch
  * --- Local variables
  pPROV = space(1)
  w_KEY = space(254)
  w_OBJMOV = .NULL.
  w_FLAUTO = space(1)
  w_MESS = space(10)
  w_CONTA = 0
  * --- WorkFile variables
  CCF_DETT_idx=0
  CIC_MAST_idx=0
  PAR_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pPROV='A'=Valorizza Ultimo Numero da GSCI_ACC
    * --- pPROV='P'=Verifiche da GSCI_ACC
    * --- pPROV='D'=Verifiche eliminazione classi
    * --- Calcolo Ultimo Progressivo 
    do case
      case this.pPROV="A"
        * --- Calcola l'Ultimo Numero
        * --- Select from CCF_DETT
        i_nConn=i_TableProp[this.CCF_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2],.t.,this.CCF_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select max(CFPROGRE) as PR_PROGR  from "+i_cTable+" CCF_DETT ";
              +" where CFCODICE="+cp_ToStrODBC(this.oParentObject.w_CFCODICE)+"";
               ,"_Curs_CCF_DETT")
        else
          select max(CFPROGRE) as PR_PROGR from (i_cTable);
           where CFCODICE=this.oParentObject.w_CFCODICE;
            into cursor _Curs_CCF_DETT
        endif
        if used('_Curs_CCF_DETT')
          select _Curs_CCF_DETT
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ULTPROG = NVL(PR_PROGR,0)
            select _Curs_CCF_DETT
            continue
          enddo
          use
        endif
      case this.pPROV="P"
        * --- Verifico che esista almeno una riga tipo progressivo o dettaglio vuoto
        * --- Read from CCF_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CCF_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2],.t.,this.CCF_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CCF_DETT where ";
                +"CFCODICE = "+cp_ToStrODBC(this.oParentObject.w_CFCODICE);
                +" and CF__FUNC = "+cp_ToStrODBC("P");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CFCODICE = this.oParentObject.w_CFCODICE;
                and CF__FUNC = "P";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          * --- non ho trovato il progressivo, non devo fare niente
        endif
        if i_Rows>1
          * --- Non sono gestiti codici con pi� di un progressivo
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_Msgformat("Non � possibile utilizzare due progressivi nella stessa classe")
          i_retcode = 'stop'
          return
        endif
      case this.pPROV="D"
        * --- Select from CIC_MAST
        i_nConn=i_TableProp[this.CIC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" CIC_MAST ";
              +" where CLPREFAS="+cp_ToStrODBC(this.oParentObject.w_CFCODICE)+"";
               ,"_Curs_CIC_MAST")
        else
          select Count(*) As Conta from (i_cTable);
           where CLPREFAS=this.oParentObject.w_CFCODICE;
            into cursor _Curs_CIC_MAST
        endif
        if used('_Curs_CIC_MAST')
          select _Curs_CIC_MAST
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_CIC_MAST.Conta , 0)
            select _Curs_CIC_MAST
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei cicli di lavorazione. Impossibile cancellare",alltrim(this.oParentObject.w_CFCODICE))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
        * --- Select from CIC_MAST
        i_nConn=i_TableProp[this.CIC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2],.t.,this.CIC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" CIC_MAST ";
              +" where CLPRECOD="+cp_ToStrODBC(this.oParentObject.w_CFCODICE)+"";
               ,"_Curs_CIC_MAST")
        else
          select Count(*) As Conta from (i_cTable);
           where CLPRECOD=this.oParentObject.w_CFCODICE;
            into cursor _Curs_CIC_MAST
        endif
        if used('_Curs_CIC_MAST')
          select _Curs_CIC_MAST
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_CIC_MAST.Conta , 0)
            select _Curs_CIC_MAST
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei cicli di lavorazione. Impossibile cancellare",alltrim(this.oParentObject.w_CFCODICE))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
        * --- Select from PAR_PROD
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" PAR_PROD ";
              +" where PPCLAFAS="+cp_ToStrODBC(this.oParentObject.w_CFCODICE)+"";
               ,"_Curs_PAR_PROD")
        else
          select Count(*) As Conta from (i_cTable);
           where PPCLAFAS=this.oParentObject.w_CFCODICE;
            into cursor _Curs_PAR_PROD
        endif
        if used('_Curs_PAR_PROD')
          select _Curs_PAR_PROD
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_PAR_PROD.Conta , 0)
            select _Curs_PAR_PROD
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei parametri produzione. Impossibile cancellare",alltrim(this.oParentObject.w_CFCODICE))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
        * --- Select from PAR_PROD
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" PAR_PROD ";
              +" where PPCLAART="+cp_ToStrODBC(this.oParentObject.w_CFCODICE)+"";
               ,"_Curs_PAR_PROD")
        else
          select Count(*) As Conta from (i_cTable);
           where PPCLAART=this.oParentObject.w_CFCODICE;
            into cursor _Curs_PAR_PROD
        endif
        if used('_Curs_PAR_PROD')
          select _Curs_PAR_PROD
          locate for 1=1
          do while not(eof())
          this.w_CONTA = Nvl ( _Curs_PAR_PROD.Conta , 0)
            select _Curs_PAR_PROD
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          this.w_MESS = ah_MsgFormat("Attivit� %1 presente nei parametri produzione. Impossibile cancellare",alltrim(this.oParentObject.w_CFCODICE))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CCF_DETT'
    this.cWorkTables[2]='CIC_MAST'
    this.cWorkTables[3]='PAR_PROD'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CCF_DETT')
      use in _Curs_CCF_DETT
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_PAR_PROD')
      use in _Curs_PAR_PROD
    endif
    if used('_Curs_PAR_PROD')
      use in _Curs_PAR_PROD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
