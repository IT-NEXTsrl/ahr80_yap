* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bco                                                        *
*              Creazione ordini di fase                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-28                                                      *
* Last revis.: 2017-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Operazione,p_OLCODODL,p_OLTFAODL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bco",oParentObject,m.Operazione,m.p_OLCODODL,m.p_OLTFAODL)
return(i_retval)

define class tgsci_bco as StdBatch
  * --- Local variables
  Operazione = space(10)
  p_OLCODODL = space(15)
  p_OLTFAODL = 0
  w_ODLCUR = space(15)
  w_OLTSTATO = space(1)
  w_OLTPROVE = space(1)
  w_OLTCOFOR = space(15)
  w_MAGTER = space(5)
  w_WIPNET = space(1)
  w_OLTCOMAG = space(5)
  w_OLTEMLAV = 0
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLTCOMME = space(15)
  w_OLTCOATT = space(15)
  w_OLTCODIC = space(41)
  w_CLROWNUM = 0
  w_CLROWORD = 0
  w_OLTCOART = space(20)
  w_OLTUNMIS = space(5)
  w_OLTQTODL = 0
  w_OLTQTOD1 = 0
  w_OLTSECIC = space(10)
  w_OLCODODL = space(15)
  w_CONUMERO = space(15)
  w_CONUMROW = 0
  w_DATORD = ctod("  /  /  ")
  w_ARMOLTIP = 0
  w_ARUNMIS2 = space(3)
  w_ARPREZUM = space(1)
  w_AROPERAT = space(1)
  w_FLCOMM = space(1)
  w_CLULTFAS = space(1)
  w_CLCOUPOI = space(1)
  w_CLFASCLA = space(1)
  w_OLTCOPOI = space(1)
  w_OLTULFAS = space(1)
  w_OLTFAOUT = space(1)
  w_OLTSECPR = space(15)
  w_CLSEOLFA = space(15)
  w_OLTCAMAG = space(5)
  w_MAGWIP = space(5)
  w_OLCAUMAG = space(5)
  w_DATAVUOTA = ctod("  /  /  ")
  w_FLAGVUOTO = space(1)
  w_OLTKEYSA = space(20)
  w_OLTIPPRE = space(1)
  w_OLTCOCEN = space(15)
  w_OLTVOCEN = space(15)
  w_OLTMGWIP = space(5)
  w_DATRIF = ctod("  /  /  ")
  w_OLTPERAS = space(3)
  PADRE = .NULL.
  w_MAGIMPW = space(5)
  w_FLCOM1 = space(1)
  w_COMMDETT = space(15)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_FLCOMD = space(1)
  w_OLQTAUM1 = 0
  w_MAGIMP = space(5)
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLQTASAL = 0
  w_OLFLEVAS = space(1)
  w_OLEVAAUT = space(1)
  w_MGDISMAG = space(1)
  m_OLQTAEV1 = 0
  w_FLUPD = space(1)
  w_QRYMRP = space(9)
  w_OLCODICF = space(20)
  w_OLCPRMAT = 0
  w_LNumOk = 0
  w_LOggErr = space(41)
  w_LErrore = space(80)
  * --- WorkFile variables
  MAGAZZIN_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  CON_TRAM_idx=0
  CON_TRAD_idx=0
  ODL_MAST_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  ODL_DETT_idx=0
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  ODL_RISF_idx=0
  CON_MACL_idx=0
  ODL_MOUT_idx=0
  ODL_CICL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.PADRE = this.oParentObject
    * --- Legge da parametri produzione
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPMAGPRO,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPMAGPRO,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTCAMAG = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_MAGWIP = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_OLCAUMAG = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLIMPE,CMFLORDI"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_OLTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLIMPE,CMFLORDI;
        from (i_cTable) where;
            CMCODICE = this.w_OLTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_OLTFLIMP = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      w_OLTFLORD = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLORDI,CMFLIMPE,CMFLRISE"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_OLCAUMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLORDI,CMFLIMPE,CMFLRISE;
        from (i_cTable) where;
            CMCODICE = this.w_OLCAUMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_OLFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      w_OLFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      w_OLFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DATAVUOTA = cp_CharToDate("  -  -  ")
    this.w_FLAGVUOTO = " "
    do case
      case this.Operazione = "CREATEALL"
        VQ_EXEC("GSCI_BCO", this, "_Curs_GSCI_BCO")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione = "MRP"
        * --- --Provengo da MRP
        * --- Ritempifica tutti gli ordini di fase
        GSCI_BTF(this," ")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_QRYMRP = IIF(this.oParentObject.w_SELEZ<>"S", "GSCI_BCO.VQR", "GSCI1BCO.VQR")
        * --- Cambio la query se ho impostato i filtri sull'MRP
        * --- Per non perdere sulle prestazioni
        VQ_EXEC(this.w_QRYMRP, this, "_Curs_GSCI_BCO")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione = "PIAORDFA"
        * --- Ritempifica tutti gli ordini di fase
        * --- Try
        local bErr_02A221F0
        bErr_02A221F0=bTrsErr
        this.Try_02A221F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_02A221F0
        * --- End
      case this.Operazione = "CREATESEL"
        * --- Da pianificazione ordini di fase (GSCI_KGP)
        * --- Riferimento al cursore dello zoom
        NC = this.PADRE.w_ODL.cCursor
        if used(NC)
          Select (NC) 
 Locate for xchk=1
          if found()
            Select clcododl,clroword from (NC) where xchk=1 into cursor elab 
 Select elab 
 GO TOP 
 SCAN
            this.p_OLCODODL = clcododl
            this.p_OLTFAODL = clroword
            VQ_EXEC("GSCI_BCO", this, "_Curs_GSCI_BCO")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            ENDSCAN
            USE IN Select ("Elab")
          else
            ah_errormsg("Selezionare almeno un ordine!",48)
          endif
          this.PADRE.NotifyEvent("ActivatePage 2")     
          this.PADRE.NotifyEvent("Interroga")     
        endif
    endcase
  endproc
  proc Try_02A221F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    GSCI_BTF(this," ")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Used("_Curs_GSCI_BCO")
      select _Curs_GSCI_BCO
      locate for 1=1
      do while not(eof())
        if Empty(NVL(_Curs_GSCI_BCO.CODODLFA, SPACE(15)))
          this.w_CLSEOLFA = Null
          this.w_ODLCUR = _Curs_GSCI_BCO.CLCODODL
          this.w_OLTSTATO = _Curs_GSCI_BCO.OLTSTATO
          this.w_OLTPROVE = IIF(_Curs_GSCI_BCO.CLFASCLA = "S", "L", "I")
          if this.w_OLTPROVE="L"
            this.w_OLTCOFOR = LFCODFOR
            this.w_MAGTER = ANMAGTER
            * --- Verifica se magazzino terzista � nettificabile
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDISMAG"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGTER);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDISMAG;
                from (i_cTable) where;
                    MGCODMAG = this.w_MAGTER;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_WIPNET = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_OLTCOFOR = LFCODFOR
            this.w_MAGTER = ANMAGTER
            this.w_WIPNET = "N"
          endif
          * --- Magazzino - Genera ordine su magazzino WIP del C/L esterno
          this.w_OLTCOMAG = NVL( _Curs_GSCI_BCO.CLWIPDES, " ")
          if _Curs_GSCI_BCO.CLULTFAS="S"
            this.w_OLTCOMAG = NVL(_Curs_GSCI_BCO.TCOMAG, SPACE(5))
          endif
          this.w_OLTCOMAG = iif(empty(this.w_OLTCOMAG), this.w_MAGWIP, this.w_OLTCOMAG)
          this.w_OLTMGWIP = _Curs_GSCI_BCO.RLMAGWIP
          * --- Usa il LT specificato sul centro di lavoro
          this.w_OLTEMLAV = _Curs_GSCI_BCO.RLTEMCOD
          * --- Dati tempo
          this.w_OLTDINRIC = _Curs_GSCI_BCO.CLDATINI
          this.w_OLTDTRIC = _Curs_GSCI_BCO.CLDATFIN
          * --- Dati Commessa
          this.w_OLTCOMME = _Curs_GSCI_BCO.OLTCOMME
          this.w_OLTCOATT = _Curs_GSCI_BCO.OLTCOATT
          * --- Dati analitica
          this.w_OLTCOCEN = _Curs_GSCI_BCO.OLTCOCEN
          this.w_OLTVOCEN = _Curs_GSCI_BCO.OLTVOCEN
          * --- Dati Fase
          this.w_OLTCOPOI = _Curs_GSCI_BCO.CLCOUPOI
          this.w_OLTULFAS = _Curs_GSCI_BCO.CLULTFAS
          this.w_OLTFAOUT = _Curs_GSCI_BCO.CLFASOUT
          * --- Dati codice da generare
          this.w_OLTCODIC = _Curs_GSCI_BCO.CLCODFAS && CLWIPOUT
          this.w_OLTKEYSA = _Curs_GSCI_BCO.CLCODFAS && CLWIPOUT
          this.w_CLROWNUM = _Curs_GSCI_BCO.CPROWNUM
          this.w_CLROWORD = _Curs_GSCI_BCO.CLROWORD
          * --- --------------------------------------------------------------------------------------------------------------
          * --- Verifico se sto rigenerando un ordine di fase preesistente
          *     Leggo il seriale
          this.w_CLSEOLFA = NVL(_Curs_GSCI_BCO.CLSEOLFA, SPACE(15))
          * --- --------------------------------------------------------------------------------------------------------------
          if not empty(this.w_OLTCODIC)
            * --- Legge articolo e variante ...
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CACODART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_OLTCODIC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CACODART;
                from (i_cTable) where;
                    CACODICE = this.w_OLTCODIC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLTCOART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            SELECT("_Curs_GSCI_BCO")
            this.w_OLTUNMIS = _Curs_GSCI_BCO.OLTUNMIS
            this.w_OLTQTODL = _Curs_GSCI_BCO.OLTQTODL
            this.w_OLTQTOD1 = _Curs_GSCI_BCO.OLTQTOD1
            * --- Gli ODL non hanno ciclo
            this.w_OLTSECIC = space(10)
            * --- Flag Commessa
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLCOMM,ARPREZUM,ARUNMIS2,AROPERAT,ARMOLTIP"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLCOMM,ARPREZUM,ARUNMIS2,AROPERAT,ARMOLTIP;
                from (i_cTable) where;
                    ARCODART = this.w_OLTCOART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
              this.w_ARPREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
              this.w_ARUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
              this.w_AROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
              this.w_ARMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Try
          local bErr_042F6CE0
          bErr_042F6CE0=bTrsErr
          this.Try_042F6CE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_LOggErr = this.w_OLCODODL
            this.w_LErrore = "Pianificazione fasi: impossibile generare ordini di fase ("
            this.w_LErrore = this.w_LErrore + str(this.w_CLROWORD)+")"
            if .f.
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_042F6CE0
          * --- End
        endif
        select _Curs_GSCI_BCO
        continue
      enddo
      use
    endif
  endproc
  proc Try_042F6CE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Per ogni riga, genera un ordine di fase
    if Empty(this.w_CLSEOLFA)
      this.w_CLSEOLFA = space(15)
      i_Conn=i_TableProp[this.ODL_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "PRODL", "i_codazi,w_CLSEOLFA")
    endif
    this.w_OLCODODL = this.w_CLSEOLFA
    * --- Cerca Contratto
    this.w_CONUMERO = " "
    this.w_CONUMROW = 0
    this.w_DATORD = this.w_OLTDTRIC
    if this.w_OLTPROVE="L" and not empty(this.w_OLTCOFOR)
      * --- Select from CON_TRAM
      i_nConn=i_TableProp[this.CON_TRAM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CONUMERO  from "+i_cTable+" CON_TRAM ";
            +" where COTIPCLF='F' and COCODCLF="+cp_ToStrODBC(this.w_OLTCOFOR)+" and CODATINI<="+cp_ToStrODBC(this.w_DATORD)+" and "+cp_ToStrODBC(this.w_DATORD)+"<=CODATFIN";
            +" order by CODATINI DESC, CODATFIN";
             ,"_Curs_CON_TRAM")
      else
        select CONUMERO from (i_cTable);
         where COTIPCLF="F" and COCODCLF=this.w_OLTCOFOR and CODATINI<=this.w_DATORD and this.w_DATORD<=CODATFIN;
         order by CODATINI DESC, CODATFIN;
          into cursor _Curs_CON_TRAM
      endif
      if used('_Curs_CON_TRAM')
        select _Curs_CON_TRAM
        locate for 1=1
        do while not(eof())
        * --- Select from CON_TRAD
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" CON_TRAD ";
              +" where CONUMERO="+cp_ToStrODBC(_Curs_CON_TRAM.CONUMERO)+" and COCODART="+cp_ToStrODBC(this.w_OLTCOART)+"";
               ,"_Curs_CON_TRAD")
        else
          select * from (i_cTable);
           where CONUMERO=_Curs_CON_TRAM.CONUMERO and COCODART=this.w_OLTCOART;
            into cursor _Curs_CON_TRAD
        endif
        if used('_Curs_CON_TRAD')
          select _Curs_CON_TRAD
          locate for 1=1
          do while not(eof())
          this.w_CONUMERO = _Curs_CON_TRAM.CONUMERO
          this.w_CONUMROW = _Curs_CON_TRAD.CPROWNUM
          this.w_OLTEMLAV = NVL(_Curs_CON_TRAD.COGIOAPP, 0)
            select _Curs_CON_TRAD
            continue
          enddo
          use
        endif
        exit
          select _Curs_CON_TRAM
          continue
        enddo
        use
      endif
    endif
    this.w_DATRIF = this.w_OLTDTRIC
    * --- Determina periodo di appartenenza dell'ODL/OCL
    vq_exec ("..\COLA\EXE\QUERY\GSCO_BOL", this, "__Temp__")
    if used("__Temp__")
      * --- Preleva periodo assoluto
      select __Temp__
      go top
      this.w_OLTPERAS = nvl(__Temp__.TPPERASS, "XXX")
      * --- Chiude cursore
      USE IN __Temp__
    endif
    this.w_OLTPERAS = IIF(Empty(this.w_OLTPERAS), "XXX" , this.w_OLTPERAS)
    * --- Scrive testata ODL
    this.w_OLTSTATO = iif(this.w_OLTSTATO="L","P",this.w_OLTSTATO)
    * --- Legge Ordine della fase precedente da scrivere OLTSECPR
    * --- ---------------------------------------------------------------------------------------------------------------------
    this.w_OLCODICF = nvl(_Curs_GSCI_BCO.CLWIPFPR, space(20))
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLCODODL"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLTSEODL = "+cp_ToStrODBC(this.w_ODLCUR);
            +" and OLTCODIC = "+cp_ToStrODBC(this.w_OLCODICF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLCODODL;
        from (i_cTable) where;
            OLTSEODL = this.w_ODLCUR;
            and OLTCODIC = this.w_OLCODICF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTSECPR = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- ---------------------------------------------------------------------------------------------------------------------
    * --- Insert into ODL_MAST
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OLCODODL"+",OLDATODP"+",OLOPEODP"+",OLDATODL"+",OLOPEODL"+",OLTSTATO"+",OLTCAMAG"+",OLTFLORD"+",OLTFLIMP"+",OLTCOMAG"+",OLTDTMPS"+",OLTDINRIC"+",OLTDTRIC"+",OLTKEYSA"+",OLTFLSUG"+",OLTFLCON"+",OLTFLDAP"+",OLTFLPIA"+",OLTFLLAN"+",OLTFLEVA"+",OLTEMLAV"+",OLTCOMME"+",OLTTIPAT"+",OLTCOATT"+",OLTQTSAL"+",OLTCODIC"+",OLTCOART"+",OLTUNMIS"+",OLTQTODL"+",OLTQTOD1"+",OLTVARIA"+",OLTDTCON"+",OLTSAFLT"+",OLTCONTR"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",OLTCOPOI"+",OLTFAOUT"+",OLTULFAS"+",OLTSECPR"+",OLTMGWIP"+",OLTCOCEN"+",OLTVOCEN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_MAST','OLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODL');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSTATO),'ODL_MAST','OLTSTATO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCAMAG),'ODL_MAST','OLTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(w_OLTFLORD),'ODL_MAST','OLTFLORD');
      +","+cp_NullLink(cp_ToStrODBC(w_OLTFLIMP),'ODL_MAST','OLTFLIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'ODL_MAST','OLTDTMPS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDINRIC),'ODL_MAST','OLTDINRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDTRIC),'ODL_MAST','OLTDTRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_MAST','OLTKEYSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFLSUG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFLCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFLDAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFLPIA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFLLAN');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTFLEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTEMLAV),'ODL_MAST','OLTEMLAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'ODL_MAST','OLTCOMME');
      +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOATT),'ODL_MAST','OLTCOATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_MAST','OLTQTSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_MAST','OLTCODIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'ODL_MAST','OLTCOART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTUNMIS),'ODL_MAST','OLTUNMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTODL),'ODL_MAST','OLTQTODL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_MAST','OLTQTOD1');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTVARIA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTDTRIC),'ODL_MAST','OLTDTCON');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTSAFLT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONUMERO),'ODL_MAST','OLTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'ODL_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOPOI),'ODL_MAST','OLTCOPOI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFAOUT),'ODL_MAST','OLTFAOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTULFAS),'ODL_MAST','OLTULFAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSECPR),'ODL_MAST','OLTSECPR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTMGWIP),'ODL_MAST','OLTMGWIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOCEN),'ODL_MAST','OLTCOCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTVOCEN),'ODL_MAST','OLTVOCEN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'OLDATODP',i_DATSYS,'OLOPEODP',i_CODUTE,'OLDATODL',i_DATSYS,'OLOPEODL',i_CODUTE,'OLTSTATO',this.w_OLTSTATO,'OLTCAMAG',this.w_OLTCAMAG,'OLTFLORD',w_OLTFLORD,'OLTFLIMP',w_OLTFLIMP,'OLTCOMAG',this.w_OLTCOMAG,'OLTDTMPS',this.w_DATAVUOTA,'OLTDINRIC',this.w_OLTDINRIC)
      insert into (i_cTable) (OLCODODL,OLDATODP,OLOPEODP,OLDATODL,OLOPEODL,OLTSTATO,OLTCAMAG,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTDTMPS,OLTDINRIC,OLTDTRIC,OLTKEYSA,OLTFLSUG,OLTFLCON,OLTFLDAP,OLTFLPIA,OLTFLLAN,OLTFLEVA,OLTEMLAV,OLTCOMME,OLTTIPAT,OLTCOATT,OLTQTSAL,OLTCODIC,OLTCOART,OLTUNMIS,OLTQTODL,OLTQTOD1,OLTVARIA,OLTDTCON,OLTSAFLT,OLTCONTR,UTCC,UTCV,UTDC,UTDV,OLTCOPOI,OLTFAOUT,OLTULFAS,OLTSECPR,OLTMGWIP,OLTCOCEN,OLTVOCEN &i_ccchkf. );
         values (;
           this.w_OLCODODL;
           ,i_DATSYS;
           ,i_CODUTE;
           ,i_DATSYS;
           ,i_CODUTE;
           ,this.w_OLTSTATO;
           ,this.w_OLTCAMAG;
           ,w_OLTFLORD;
           ,w_OLTFLIMP;
           ,this.w_OLTCOMAG;
           ,this.w_DATAVUOTA;
           ,this.w_OLTDINRIC;
           ,this.w_OLTDTRIC;
           ,this.w_OLTKEYSA;
           ,this.w_FLAGVUOTO;
           ,this.w_FLAGVUOTO;
           ,this.w_FLAGVUOTO;
           ,this.w_FLAGVUOTO;
           ,this.w_FLAGVUOTO;
           ,"N";
           ,this.w_OLTEMLAV;
           ,this.w_OLTCOMME;
           ,"A";
           ,this.w_OLTCOATT;
           ,this.w_OLTQTOD1;
           ,this.w_OLTCODIC;
           ,this.w_OLTCOART;
           ,this.w_OLTUNMIS;
           ,this.w_OLTQTODL;
           ,this.w_OLTQTOD1;
           ,"N";
           ,this.w_OLTDTRIC;
           ,0;
           ,this.w_CONUMERO;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_OLTCOPOI;
           ,this.w_OLTFAOUT;
           ,this.w_OLTULFAS;
           ,this.w_OLTSECPR;
           ,this.w_OLTMGWIP;
           ,this.w_OLTCOCEN;
           ,this.w_OLTVOCEN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTSECIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTSECIC),'ODL_MAST','OLTSECIC');
      +",OLTCOFOR ="+cp_NullLink(cp_ToStrODBC(this.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
      +",OLTFCOCO ="+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFCOCO');
      +",OLTFORCO ="+cp_NullLink(cp_ToStrODBC(this.w_FLAGVUOTO),'ODL_MAST','OLTFORCO');
      +",OLTIMCOM ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTIMCOM');
      +",OLTPROVE ="+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
      +",OLTTICON ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
      +",OLTSEODL ="+cp_NullLink(cp_ToStrODBC(this.w_ODLCUR),'ODL_MAST','OLTSEODL');
      +",OLTFAODL ="+cp_NullLink(cp_ToStrODBC(this.w_CLROWNUM),'ODL_MAST','OLTFAODL');
      +",OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTPERAS),'ODL_MAST','OLTPERAS');
      +",OLTDTLAN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'ODL_MAST','OLTDTLAN');
      +",OLTDTINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'ODL_MAST','OLTDTINI');
      +",OLTDTFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'ODL_MAST','OLTDTFIN');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      update (i_cTable) set;
          OLTSECIC = this.w_OLTSECIC;
          ,OLTCOFOR = this.w_OLTCOFOR;
          ,OLTFCOCO = this.w_FLAGVUOTO;
          ,OLTFORCO = this.w_FLAGVUOTO;
          ,OLTIMCOM = 0;
          ,OLTPROVE = this.w_OLTPROVE;
          ,OLTTICON = "F";
          ,OLTSEODL = this.w_ODLCUR;
          ,OLTFAODL = this.w_CLROWNUM;
          ,OLTPERAS = this.w_OLTPERAS;
          ,OLTDTLAN = this.w_DATAVUOTA;
          ,OLTDTINI = this.w_DATAVUOTA;
          ,OLTDTFIN = this.w_DATAVUOTA;
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_OLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- ---------------------------------------------------------------------------------------------------------------------
    * --- Aggiorno il seriale dell'ordine di fase nella tabella cicli di lavorazione legata all'ordine
    * --- Write into ODL_CICL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLSEOLFA ="+cp_NullLink(cp_ToStrODBC(this.w_CLSEOLFA),'ODL_CICL','CLSEOLFA');
          +i_ccchkf ;
      +" where ";
          +"CLCODODL = "+cp_ToStrODBC(this.w_ODLCUR);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CLROWNUM);
             )
    else
      update (i_cTable) set;
          CLSEOLFA = this.w_CLSEOLFA;
          &i_ccchkf. ;
       where;
          CLCODODL = this.w_ODLCUR;
          and CPROWNUM = this.w_CLROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- ---------------------------------------------------------------------------------------------------------------------
    * --- Aggiorna saldo articolo
    * --- Try
    local bErr_0430BD28
    bErr_0430BD28=bTrsErr
    this.Try_0430BD28()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0430BD28
    * --- End
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(w_OLTFLORD,'SLQTOPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(w_OLTFLIMP,'SLQTIPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_OLTCOART);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_OLTCOART;
          and SLCODMAG = this.w_OLTCOMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
      * --- Aggiorna i saldi commessa
      * --- Try
      local bErr_04343610
      bErr_04343610=bTrsErr
      this.Try_04343610()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04343610
      * --- End
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(w_OLTFLORD,'SCQTOPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(w_OLTFLIMP,'SCQTIPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_OLTCOART);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_OLTCOART;
            and SCCODMAG = this.w_OLTCOMAG;
            and SCCODCAN = this.w_OLTCOMME;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if .F.
      * --- --Memorizzo le informazioni appena generate 
      insert into AGGMAG; 
 (OLCODODL, OLTSEODL ,OLTFAODL ,OLTCAMAG, OLTQTSAL, OLTCODIC,; 
 OLTCOART,OLTCOVAR,OLTCOMAG, OLTUNMIS, OLTQTODL, OLTQTOD1,OLTKEYSA, ARFLCOMM) VALUES; 
 (this.w_OLCODODL, this.w_ODLCUR ,this.w_CLROWNUM ,this.w_OLTCAMAG, this.w_OLTQTOD1, this.w_OLTCODIC,; 
 this.w_OLTCOART,w_OLTCOVAR,this.w_OLTCOMAG, this.w_OLTUNMIS, this.w_OLTQTODL, this.w_OLTQTOD1,this.w_OLTKEYSA, ; 
 this.w_FLCOMM)
    endif
    * --- Crea dettaglio ordine
    this.w_CPROWORD = 1
    this.w_CPROWNUM = 10
    * --- Sulla prima riga dettaglio mette materiale di output della fase precedente
    SELECT("_Curs_GSCI_BCO")
    this.w_OLTCODIC = nvl(_Curs_GSCI_BCO.CLWIPFPR, space(20))
    this.w_OLTCOART = nvl(_Curs_GSCI_BCO.CLWIPFPR, space(20))
    this.w_OLTKEYSA = nvl(_Curs_GSCI_BCO.CLWIPFPR, space(20))
    this.w_OLTUNMIS = nvl(_Curs_GSCI_BCO.CLUM1FPR, space(3))
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLCOMM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLCOMM;
        from (i_cTable) where;
            ARCODART = this.w_OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCOM1 = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_COMMDETT = iif(NVL(this.w_FLCOM1,"N")="S" and ! empty(nvl(this.w_OLTCOMME," ")),this.w_OLTCOMME,space(15))
    if not empty(this.w_OLTCODIC)
      * --- Prima riga dettaglio ordine: output della fase precedente
      this.w_OLTIPPRE = IIF(this.w_OLTPROVE = "L" , "M", "N")
      if this.Operazione = "MRP"
        this.w_MAGIMPW = _Curs_GSCI_BCO.RLWIPRIS
      else
        this.w_MAGIMPW = _Curs_GSCI_BCO.RLMAGWIP
      endif
      * --- Insert into ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLFLORDI"+",OLFLIMPE"+",OLFLRISE"+",OLCODMAG"+",OLCODICE"+",OLCODART"+",OLUNIMIS"+",OLCOEIMP"+",OLQTAMOV"+",OLQTAUM1"+",OLQTAEVA"+",OLQTAEV1"+",OLFLEVAS"+",OLKEYSAL"+",OLQTASAL"+",OLQTAPRE"+",OLQTAPR1"+",OLDATRIC"+",OLSTARIG"+",OLPABPRE"+",OLMAGPRE"+",OLFASRIF"+",OLCPRIFE"+",OLCORRLT"+",OLEVAAUT"+",OLTIPPRE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_DETT','OLCODODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ODL_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ODL_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLCAUMAG),'ODL_DETT','OLCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(w_OLFLORDI),'ODL_DETT','OLFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(w_OLFLIMPE),'ODL_DETT','OLFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(w_OLFLRISE),'ODL_DETT','OLFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMPW),'ODL_DETT','OLCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCODIC),'ODL_DETT','OLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'ODL_DETT','OLCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTUNMIS),'ODL_DETT','OLUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(1),'ODL_DETT','OLCOEIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_DETT','OLQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_DETT','OLQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAEVA');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAEV1');
        +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLFLEVAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'ODL_DETT','OLKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_DETT','OLQTASAL');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
        +","+cp_NullLink(cp_ToStrODBC(OLTDINRIC),'ODL_DETT','OLDATRIC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLSTARIG');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLPABPRE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMPW),'ODL_DETT','OLMAGPRE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLFASRIF');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLCPRIFE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLCORRLT');
        +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLEVAAUT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'OLCAUMAG',this.w_OLCAUMAG,'OLFLORDI',w_OLFLORDI,'OLFLIMPE',w_OLFLIMPE,'OLFLRISE',w_OLFLRISE,'OLCODMAG',this.w_MAGIMPW,'OLCODICE',this.w_OLTCODIC,'OLCODART',this.w_OLTCOART,'OLUNIMIS',this.w_OLTUNMIS,'OLCOEIMP',1)
        insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLFLORDI,OLFLIMPE,OLFLRISE,OLCODMAG,OLCODICE,OLCODART,OLUNIMIS,OLCOEIMP,OLQTAMOV,OLQTAUM1,OLQTAEVA,OLQTAEV1,OLFLEVAS,OLKEYSAL,OLQTASAL,OLQTAPRE,OLQTAPR1,OLDATRIC,OLSTARIG,OLPABPRE,OLMAGPRE,OLFASRIF,OLCPRIFE,OLCORRLT,OLEVAAUT,OLTIPPRE &i_ccchkf. );
           values (;
             this.w_OLCODODL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_OLCAUMAG;
             ,w_OLFLORDI;
             ,w_OLFLIMPE;
             ,w_OLFLRISE;
             ,this.w_MAGIMPW;
             ,this.w_OLTCODIC;
             ,this.w_OLTCOART;
             ,this.w_OLTUNMIS;
             ,1;
             ,this.w_OLTQTOD1;
             ,this.w_OLTQTOD1;
             ,0;
             ,0;
             ,"N";
             ,this.w_OLTKEYSA;
             ,this.w_OLTQTOD1;
             ,0;
             ,0;
             ,OLTDINRIC;
             ," ";
             ,0;
             ,this.w_MAGIMPW;
             ,0;
             ,0;
             ,0;
             ,"N";
             ,this.w_OLTIPPRE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Aggiorna nuovo deposito
      * --- Try
      local bErr_0433BF30
      bErr_0433BF30=bTrsErr
      this.Try_0433BF30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0433BF30
      * --- End
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(w_OLFLRISE,'SLQTRPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(w_OLFLORDI,'SLQTOPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(w_OLFLIMPE,'SLQTIPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGIMPW);
               )
      else
        update (i_cTable) set;
            SLQTRPER = &i_cOp1.;
            ,SLQTOPER = &i_cOp2.;
            ,SLQTIPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_OLTKEYSA;
            and SLCODMAG = this.w_MAGIMPW;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if NVL(this.w_FLCOM1,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
        * --- Aggiorna i saldi commessa
        * --- Try
        local bErr_0433DFA0
        bErr_0433DFA0=bTrsErr
        this.Try_0433DFA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0433DFA0
        * --- End
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(w_OLFLRISE,'SCQTRPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(w_OLFLORDI,'SCQTOPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(w_OLFLIMPE,'SCQTIPER','this.w_OLTQTOD1',this.w_OLTQTOD1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGIMPW);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                 )
        else
          update (i_cTable) set;
              SCQTRPER = &i_cOp1.;
              ,SCQTOPER = &i_cOp2.;
              ,SCQTIPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_OLTKEYSA;
              and SCCODMAG = this.w_MAGIMPW;
              and SCCODCAN = this.w_OLTCOMME;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL="+cp_ToStrODBC(this.w_ODLCUR)+" and OLFASRIF="+cp_ToStrODBC(this.w_CLROWORD)+"";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL=this.w_ODLCUR and OLFASRIF=this.w_CLROWORD;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      * --- Riga attiva (se Materiale Alternativo => selezionato)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLCOMM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLCOMM;
          from (i_cTable) where;
              ARCODART = _Curs_ODL_DETT.OLCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLCOMD = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWNUM * 10
      this.w_OLQTAUM1 = _Curs_ODL_DETT.OLQTAUM1
      if this.w_OLTPROVE = "L"
        this.w_MAGIMP = iif(this.w_WIPNET="S", this.w_MAGTER, _Curs_ODL_DETT.OLCODMAG)
        this.w_MAGIMPW = this.w_MAGIMP
      else
        this.w_MAGIMPW = _Curs_GSCI_BCO.RLMAGWIP
      endif
      this.w_OLQTAEVA = _Curs_ODL_DETT.OLQTAEVA
      this.w_OLQTAEV1 = _Curs_ODL_DETT.OLQTAEV1
      this.w_OLQTASAL = _Curs_ODL_DETT.OLQTASAL
      this.w_OLTIPPRE = IIF(this.w_OLTPROVE = "L" , "M", _Curs_ODL_DETT.OLTIPPRE)
      * --- Memorizzo il cprownum del materiale del padre
      this.w_OLCPRMAT = _Curs_ODL_DETT.CPROWNUM
      this.w_OLFLEVAS = "N"
      this.w_COMMDETT = iif(NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_OLTCOMME," ")),this.w_OLTCOMME,space(15))
      * --- Guarda se il materiale e' nella lista dei materiali che mette il terzista
      this.w_OLEVAAUT = "N"
      if not empty(this.w_CONUMERO)
        * --- Guarda se il materiale e' nella lista dei materiali che mette il terzista
        * --- Read from CON_MACL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_MACL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_MACL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CON_MACL where ";
                +"MCCODCON = "+cp_ToStrODBC(this.w_CONUMERO);
                +" and MCNUMROW = "+cp_ToStrODBC(this.w_CONUMROW);
                +" and MCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                MCCODCON = this.w_CONUMERO;
                and MCNUMROW = this.w_CONUMROW;
                and MCCODICE = _Curs_ODL_DETT.OLCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = _Curs_ODL_DETT.OLCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MGDISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_MGDISMAG="S"
            * --- Magazzino di impegno nettificabile: aggiorna i saldi
            this.w_OLQTASAL = -this.w_OLQTASAL
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(_Curs_ODL_DETT.OLFLRISE,'SLQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(_Curs_ODL_DETT.OLFLORDI,'SLQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(_Curs_ODL_DETT.OLFLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTRPER = &i_cOp1.;
                  ,SLQTOPER = &i_cOp2.;
                  ,SLQTIPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = _Curs_ODL_DETT.OLKEYSAL;
                  and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if NVL(this.w_FLCOMD,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
              * --- Aggiorna i saldi commessa
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(_Curs_ODL_DETT.OLFLRISE,'SCQTRPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(_Curs_ODL_DETT.OLFLORDI,'SCQTOPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(_Curs_ODL_DETT.OLFLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
                +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                       )
              else
                update (i_cTable) set;
                    SCQTRPER = &i_cOp1.;
                    ,SCQTOPER = &i_cOp2.;
                    ,SCQTIPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                    and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                    and SCCODCAN = this.w_OLTCOMME;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          this.w_OLFLEVAS = "S"
          this.w_OLQTASAL = 0
          this.w_OLQTAEVA = _Curs_ODL_DETT.OLQTAMOV
          this.w_OLQTAEV1 = this.w_OLQTAUM1
          this.w_OLEVAAUT = "S"
        endif
      endif
      * --- Dettaglio ordine  - Lascia l'impegno sul magazzino di origine (l'impegno viene trasferito dal Buono di prelievo/DDT)
      * --- Insert into ODL_DETT
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OLCODODL"+",CPROWNUM"+",CPROWORD"+",OLCAUMAG"+",OLFLORDI"+",OLFLIMPE"+",OLFLRISE"+",OLCODMAG"+",OLCODICE"+",OLCODART"+",OLUNIMIS"+",OLCOEIMP"+",OLQTAMOV"+",OLQTAUM1"+",OLQTAEVA"+",OLQTAEV1"+",OLFLEVAS"+",OLKEYSAL"+",OLQTASAL"+",OLQTAPRE"+",OLQTAPR1"+",OLDATRIC"+",OLSTARIG"+",OLPABPRE"+",OLMAGPRE"+",OLFASRIF"+",OLCPRIFE"+",OLCORRLT"+",OLEVAAUT"+",OLTIPPRE"+",OLCPRMAT"+",OLMAGWIP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OLCODODL),'ODL_DETT','OLCODODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ODL_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ODL_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCAUMAG),'ODL_DETT','OLCAUMAG');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLORDI),'ODL_DETT','OLFLORDI');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLIMPE),'ODL_DETT','OLFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLFLRISE),'ODL_DETT','OLFLRISE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG),'ODL_DETT','OLCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCODICE),'ODL_DETT','OLCODICE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCODART),'ODL_DETT','OLCODART');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLUNIMIS),'ODL_DETT','OLUNIMIS');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLCOEIMP),'ODL_DETT','OLCOEIMP');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLQTAMOV),'ODL_DETT','OLQTAMOV');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLQTAUM1),'ODL_DETT','OLQTAUM1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL),'ODL_DETT','OLKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLQTAPRE),'ODL_DETT','OLQTAPRE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLQTAPR1),'ODL_DETT','OLQTAPR1');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLDATRIC),'ODL_DETT','OLDATRIC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLSTARIG');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLPABPRE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_ODL_DETT.OLMAGPRE),'ODL_DETT','OLMAGPRE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLFASRIF');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLCPRIFE');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLCORRLT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLCPRMAT),'ODL_DETT','OLCPRMAT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMPW),'ODL_DETT','OLMAGWIP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'OLCAUMAG',_Curs_ODL_DETT.OLCAUMAG,'OLFLORDI',_Curs_ODL_DETT.OLFLORDI,'OLFLIMPE',_Curs_ODL_DETT.OLFLIMPE,'OLFLRISE',_Curs_ODL_DETT.OLFLRISE,'OLCODMAG',_Curs_ODL_DETT.OLCODMAG,'OLCODICE',_Curs_ODL_DETT.OLCODICE,'OLCODART',_Curs_ODL_DETT.OLCODART,'OLUNIMIS',_Curs_ODL_DETT.OLUNIMIS,'OLCOEIMP',_Curs_ODL_DETT.OLCOEIMP)
        insert into (i_cTable) (OLCODODL,CPROWNUM,CPROWORD,OLCAUMAG,OLFLORDI,OLFLIMPE,OLFLRISE,OLCODMAG,OLCODICE,OLCODART,OLUNIMIS,OLCOEIMP,OLQTAMOV,OLQTAUM1,OLQTAEVA,OLQTAEV1,OLFLEVAS,OLKEYSAL,OLQTASAL,OLQTAPRE,OLQTAPR1,OLDATRIC,OLSTARIG,OLPABPRE,OLMAGPRE,OLFASRIF,OLCPRIFE,OLCORRLT,OLEVAAUT,OLTIPPRE,OLCPRMAT,OLMAGWIP &i_ccchkf. );
           values (;
             this.w_OLCODODL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,_Curs_ODL_DETT.OLCAUMAG;
             ,_Curs_ODL_DETT.OLFLORDI;
             ,_Curs_ODL_DETT.OLFLIMPE;
             ,_Curs_ODL_DETT.OLFLRISE;
             ,_Curs_ODL_DETT.OLCODMAG;
             ,_Curs_ODL_DETT.OLCODICE;
             ,_Curs_ODL_DETT.OLCODART;
             ,_Curs_ODL_DETT.OLUNIMIS;
             ,_Curs_ODL_DETT.OLCOEIMP;
             ,_Curs_ODL_DETT.OLQTAMOV;
             ,_Curs_ODL_DETT.OLQTAUM1;
             ,this.w_OLQTAEVA;
             ,this.w_OLQTAEV1;
             ,this.w_OLFLEVAS;
             ,_Curs_ODL_DETT.OLKEYSAL;
             ,this.w_OLQTASAL;
             ,_Curs_ODL_DETT.OLQTAPRE;
             ,_Curs_ODL_DETT.OLQTAPR1;
             ,_Curs_ODL_DETT.OLDATRIC;
             ," ";
             ,0;
             ,_Curs_ODL_DETT.OLMAGPRE;
             ,0;
             ,0;
             ,0;
             ,this.w_OLEVAAUT;
             ,this.w_OLTIPPRE;
             ,this.w_OLCPRMAT;
             ,this.w_MAGIMPW;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_FLUPD = "="
      * --- Aggiorna lista di origine in ODL
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLUPD,'OLQTAEVA','_Curs_ODL_DETT.OLQTAMOV',_Curs_ODL_DETT.OLQTAMOV,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLUPD,'OLQTAEV1','_Curs_ODL_DETT.OLQTAUM1',_Curs_ODL_DETT.OLQTAUM1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLQTAEVA ="+cp_NullLink(i_cOp1,'ODL_DETT','OLQTAEVA');
        +",OLQTAEV1 ="+cp_NullLink(i_cOp2,'ODL_DETT','OLQTAEV1');
        +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
        +",OLEVAAUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLEVAAUT');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
               )
      else
        update (i_cTable) set;
            OLQTAEVA = &i_cOp1.;
            ,OLQTAEV1 = &i_cOp2.;
            ,OLFLEVAS = "S";
            ,OLQTASAL = 0;
            ,OLEVAAUT = "S";
            &i_ccchkf. ;
         where;
            OLCODODL = _Curs_ODL_DETT.OLCODODL;
            and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
    * --- Insert into ODL_RISF
    i_nConn=i_TableProp[this.ODL_RISF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_RISF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSCO_BOL5",this.ODL_RISF_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into ODL_MOUT
    i_nConn=i_TableProp[this.ODL_MOUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MOUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\COLA\EXE\QUERY\GSCO_BOL8",this.ODL_MOUT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Contatore
    this.w_LNumOk = this.w_LNumOk + 1
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0430BD28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLTCOART,'SLCODMAG',this.w_OLTCOMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLTCOART;
           ,this.w_OLTCOMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04343610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLTCOART,'SCCODMAG',this.w_OLTCOMAG,'SCCODCAN',this.w_OLTCOMME,'SCCODART',this.w_OLTCOART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_OLTCOART;
           ,this.w_OLTCOMAG;
           ,this.w_OLTCOMME;
           ,this.w_OLTCOART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0433BF30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMPW),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLTKEYSA,'SLCODMAG',this.w_MAGIMPW)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLTKEYSA;
           ,this.w_MAGIMPW;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0433DFA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLTKEYSA),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGIMPW),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLTKEYSA,'SCCODMAG',this.w_MAGIMPW,'SCCODCAN',this.w_OLTCOMME)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLTKEYSA;
           ,this.w_MAGIMPW;
           ,this.w_OLTCOMME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,Operazione,p_OLCODODL,p_OLTFAODL)
    this.Operazione=Operazione
    this.p_OLCODODL=p_OLCODODL
    this.p_OLTFAODL=p_OLTFAODL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='CON_TRAM'
    this.cWorkTables[5]='CON_TRAD'
    this.cWorkTables[6]='ODL_MAST'
    this.cWorkTables[7]='SALDIART'
    this.cWorkTables[8]='SALDICOM'
    this.cWorkTables[9]='ODL_DETT'
    this.cWorkTables[10]='PAR_PROD'
    this.cWorkTables[11]='CAM_AGAZ'
    this.cWorkTables[12]='ODL_RISF'
    this.cWorkTables[13]='CON_MACL'
    this.cWorkTables[14]='ODL_MOUT'
    this.cWorkTables[15]='ODL_CICL'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_CON_TRAM')
      use in _Curs_CON_TRAM
    endif
    if used('_Curs_CON_TRAD')
      use in _Curs_CON_TRAD
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Operazione,p_OLCODODL,p_OLTFAODL"
endproc
