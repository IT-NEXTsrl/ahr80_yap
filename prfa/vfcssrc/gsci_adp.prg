* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_adp                                                        *
*              Dichiarazioni di produzione fasi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_172]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-17                                                      *
* Last revis.: 2018-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_adp"))

* --- Class definition
define class tgsci_adp as StdForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 727
  Height = 501+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-04-03"
  HelpContextID=175504023
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=139

  * --- Constant Properties
  DIC_PROD_IDX = 0
  ODL_MAST_IDX = 0
  PAR_PROD_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  KEY_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  cpusers_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  CAM_AGAZ_IDX = 0
  TIP_DOCU_IDX = 0
  UNIMIS_IDX = 0
  CAU_VERS_IDX = 0
  ODL_CICL_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  VALUTE_IDX = 0
  cFile = "DIC_PROD"
  cKeySelect = "DPSERIAL"
  cKeyWhere  = "DPSERIAL=this.w_DPSERIAL"
  cKeyWhereODBC = '"DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cKeyWhereODBCqualified = '"DIC_PROD.DPSERIAL="+cp_ToStrODBC(this.w_DPSERIAL)';

  cPrg = "gsci_adp"
  cComment = "Dichiarazioni di produzione fasi"
  icon = "anag.ico"
  cAutoZoom = 'GSCI_ADP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DPSERIAL = space(10)
  w_STATOGES = space(10)
  w_READPAR2 = space(1)
  w_CAUDEFA = space(5)
  w_DPDATREG = ctod('  /  /  ')
  w_DPNUMOPE = 0
  w_DPCAUVER = space(5)
  o_DPCAUVER = space(5)
  w_DPFLPBSC = space(1)
  w_READPAR = space(10)
  w_CAUSCA = space(5)
  w_CAUCAR = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DPTIPATT = space(1)
  w_DPCODODL = space(15)
  o_DPCODODL = space(15)
  w_OLTSTATO = space(1)
  w_OLTPROVE = space(1)
  w_OLTQTODL = 0
  w_OLTQTOEV = 0
  w_QTARES = 0
  w_OLTFLEVA = space(1)
  w_DPCODART = space(20)
  o_DPCODART = space(20)
  w_FLLOTT = space(1)
  w_OLTUNMIS = space(3)
  w_DPCODMAG = space(5)
  o_DPCODMAG = space(5)
  w_FLUBIC = space(1)
  w_DPCODICE = space(20)
  w_DPUNIMIS = space(3)
  w_TIPVOC = space(1)
  w_DPQTAPRO = 0
  o_DPQTAPRO = 0
  w_DPQTASCA = 0
  o_DPQTASCA = 0
  w_DPFLEVAS = space(1)
  w_DPUMTEMP = space(5)
  w_DPQTATEM = 0
  w_DPDATINI = ctod('  /  /  ')
  w_DPORAINI = space(5)
  w_DPDATFIN = ctod('  /  /  ')
  w_DPORAFIN = space(5)
  w_STADOC = space(10)
  w_DPCODCEN = space(15)
  w_DESCON = space(40)
  w_DPCODVOC = space(15)
  w_VOCDES = space(40)
  w_DPCODCOM = space(15)
  w_DPCODATT = space(15)
  w_DESCAN = space(40)
  w_DPCODLOT = space(20)
  o_DPCODLOT = space(20)
  w_DESATT = space(30)
  w_DPODLFAS = space(15)
  w_DPODLFAS = space(15)
  w_DPCODPAD = space(20)
  w_DPARTPAD = space(20)
  o_DPARTPAD = space(20)
  w_FLLOTT1 = space(1)
  w_GESMAT1 = space(1)
  w_DPULTFAS = space(1)
  w_DPCODLOT = space(20)
  w_DPCODUBI = space(20)
  o_DPCODUBI = space(20)
  w_DTOBVO = ctod('  /  /  ')
  w_CODCOS = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_DESART = space(40)
  w_DESMAG = space(30)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_DPQTAPR1 = 0
  w_DPQTASC1 = 0
  w_QTAUM1 = 0
  w_DATOBSO = ctod('  /  /  ')
  w_DESUTE = space(20)
  w_OLTQTOD1 = 0
  w_OLTQTOE1 = 0
  w_TESTFORM = .F.
  w_DPRIFCAR = space(10)
  w_DPRIFSCA = space(10)
  w_DPRIFSER = space(10)
  w_OLTKEYSA = space(20)
  w_OLTQTSAL = 0
  w_OLTCICLO = space(15)
  w_MGSCAR = space(5)
  w_CRIVAL = space(2)
  w_ARTLOT = space(20)
  w_FLSTAT = space(1)
  w_MATENABL = .F.
  w_DPCOMMAT = space(1)
  w_GESMAT = space(1)
  w_EDCOMMAT = .F.
  w_CAUMGC = space(5)
  w_MTCARI = space(1)
  w_FLUBIC1 = space(1)
  w_MATCAR = 0
  w_MATSCA = 0
  w_RIGMOVLOT = .F.
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_IMPLOT = space(1)
  w_ROWDOC = 0
  w_NUMRIF = 0
  w_FLPRG = space(1)
  w_CVDESCRI = space(40)
  w_DPCRIVAL = space(1)
  w_PRDATINV = ctod('  /  /  ')
  w_DPTIPVAL = space(1)
  o_DPTIPVAL = space(1)
  w_DESLIS = space(40)
  w_DESMAG1 = space(30)
  w_DPESERCZ = space(4)
  o_DPESERCZ = space(4)
  w_DPNUMINV = space(6)
  w_DPLISTIN = space(5)
  o_DPLISTIN = space(5)
  w_DPMAGCOD = space(5)
  w_VALESE = space(3)
  w_CAOLIS = 0
  w_ESFINESE = ctod('  /  /  ')
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INCODMAG = space(5)
  w_INMAGCOL = space(1)
  w_MAGRAG = space(5)
  w_INCATOMO = space(5)
  w_TIPOLN = space(1)
  w_VALLIS = space(3)
  w_CAOVAL = 0
  w_UMFLTEMP = space(1)
  w_CAOESE = 0
  w_DECTOT = 0
  w_TATTIV = space(1)
  w_CAUSER = space(5)
  w_DPCPRFAS = 0
  w_DPODLFAS = space(15)
  w_FASE = 0
  w_COUPOI = space(1)
  w_DPFASOUT = space(1)
  w_OLTKEYSA1 = space(20)
  w_DPRIFMOU = space(10)
  w_CAUMOU = space(5)
  w_OLTSECPR = space(15)
  w_ODLSOSPE = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DPSERIAL = this.W_DPSERIAL

  * --- Children pointers
  GSCO_MMP = .NULL.
  GSCI_MRP = .NULL.
  GSCO_MMT = .NULL.
  GSCO_MCO = .NULL.
  GSCO_MOU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsci_adp
  * Varibile che se valorizzatat a .t. alla Pressione dell'F10 oppure del tasto ESC chiude la maschera
  w_AUTOCLOSE=.f.
  
    proc ecpsave()
  	  DoDefault()
  	  * Se attivo Flussi e Autorizzazioni chiudo la maschera a seguito della sua conferma
      if this.w_AUTOCLOSE
  		  * Se bUpdated=.f. significa che la registrazione e' stata salvata sul database con successo	
  		  * e quindi posso chiudere la maschera
  		  if Not this.bUpdated
  			  this.ecpquit()
  		  endif
  	  endif
    endproc
  
    proc ecpquit()
  	  DoDefault()
  	  * Se attivo Flussi e Autorizzazioni chiudo la maschera a seguito del primo Esc (Abbandoni le modifiche?)
      if this.w_AUTOCLOSE
  		  * Se lo stato non � di modifica allora chiudo la maschera
  		  * Il codice all'interno dell'IF e' preso dalla CP_FORMS
  		  if !inlist(this.cFunction,'Edit','Load')
  		  	this.NotifyEvent("Done")
  		  	this.Hide()
  		  	this.Release()
  		  EndiF
  	  Endif
    endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIC_PROD','gsci_adp')
    stdPageFrame::Init()
    *set procedure to GSCO_MMT additive
    *set procedure to GSCO_MCO additive
    with this
      .Pages(1).addobject("oPag","tgsci_adpPag1","gsci_adp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dichiarazione")
      .Pages(1).HelpContextID = 113641161
      .Pages(2).addobject("oPag","tgsci_adpPag2","gsci_adp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Materiali")
      .Pages(2).HelpContextID = 83712062
      .Pages(3).addobject("oPag","tgsci_adpPag3","gsci_adp",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Risorse")
      .Pages(3).HelpContextID = 84711702
      .Pages(4).addobject("oPag","tgsci_adpPag4","gsci_adp",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Materiali di output")
      .Pages(4).HelpContextID = 59128778
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDPSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCO_MMT
    *release procedure GSCO_MCO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[22]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='VOC_COST'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='cpusers'
    this.cWorkTables[11]='LOTTIART'
    this.cWorkTables[12]='UBICAZIO'
    this.cWorkTables[13]='CAM_AGAZ'
    this.cWorkTables[14]='TIP_DOCU'
    this.cWorkTables[15]='UNIMIS'
    this.cWorkTables[16]='CAU_VERS'
    this.cWorkTables[17]='ODL_CICL'
    this.cWorkTables[18]='ESERCIZI'
    this.cWorkTables[19]='LISTINI'
    this.cWorkTables[20]='INVENTAR'
    this.cWorkTables[21]='VALUTE'
    this.cWorkTables[22]='DIC_PROD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(22))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_PROD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_PROD_IDX,3]
  return

  function CreateChildren()
    this.GSCO_MMP = CREATEOBJECT('stdDynamicChild',this,'GSCO_MMP',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSCO_MMP.createrealchild()
    this.GSCI_MRP = CREATEOBJECT('stdDynamicChild',this,'GSCI_MRP',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSCI_MRP.createrealchild()
    this.GSCO_MMT = CREATEOBJECT('stdLazyChild',this,'GSCO_MMT')
    this.GSCO_MCO = CREATEOBJECT('stdLazyChild',this,'GSCO_MCO')
    this.GSCO_MOU = CREATEOBJECT('stdDynamicChild',this,'GSCO_MOU',this.oPgFrm.Page4.oPag.oLinkPC_4_1)
    this.GSCO_MOU.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCO_MMP)
      this.GSCO_MMP.DestroyChildrenChain()
      this.GSCO_MMP=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.GSCI_MRP)
      this.GSCI_MRP.DestroyChildrenChain()
      this.GSCI_MRP=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSCO_MMT)
      this.GSCO_MMT.DestroyChildrenChain()
      this.GSCO_MMT=.NULL.
    endif
    if !ISNULL(this.GSCO_MCO)
      this.GSCO_MCO.DestroyChildrenChain()
      this.GSCO_MCO=.NULL.
    endif
    if !ISNULL(this.GSCO_MOU)
      this.GSCO_MOU.DestroyChildrenChain()
      this.GSCO_MOU=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCO_MMP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCI_MRP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MMT.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MOU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCO_MMP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCI_MRP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MMT.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MOU.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCO_MMP.NewDocument()
    this.GSCI_MRP.NewDocument()
    this.GSCO_MMT.NewDocument()
    this.GSCO_MCO.NewDocument()
    this.GSCO_MOU.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCO_MMP.SetKey(;
            .w_DPSERIAL,"MPSERIAL";
            ,.w_ROWDOC,"MPROWDOC";
            ,.w_NUMRIF,"MPNUMRIF";
            )
      this.GSCI_MRP.SetKey(;
            .w_DPSERIAL,"RFSERIAL";
            )
      this.GSCO_MMT.SetKey(;
            .w_DPSERIAL,"MTSERIAL";
            )
      this.GSCO_MCO.SetKey(;
            .w_DPSERIAL,"MTSERIAL";
            )
      this.GSCO_MOU.SetKey(;
            .w_DPSERIAL,"MDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCO_MMP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MPSERIAL";
             ,.w_ROWDOC,"MPROWDOC";
             ,.w_NUMRIF,"MPNUMRIF";
             )
      .GSCI_MRP.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"RFSERIAL";
             )
      .GSCO_MMT.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MTSERIAL";
             )
      .GSCO_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MTSERIAL";
             )
      .WriteTo_GSCO_MCO()
      .GSCO_MOU.ChangeRow(this.cRowID+'      1',1;
             ,.w_DPSERIAL,"MDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCO_MMP)
        i_f=.GSCO_MMP.BuildFilter()
        if !(i_f==.GSCO_MMP.cQueryFilter)
          i_fnidx=.GSCO_MMP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MMP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MMP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MMP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MMP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCI_MRP)
        i_f=.GSCI_MRP.BuildFilter()
        if !(i_f==.GSCI_MRP.cQueryFilter)
          i_fnidx=.GSCI_MRP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MRP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MRP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MRP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MRP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MOU)
        i_f=.GSCO_MOU.BuildFilter()
        if !(i_f==.GSCO_MOU.cQueryFilter)
          i_fnidx=.GSCO_MOU.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MOU.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MOU.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MOU.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MOU.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSCO_MCO()
  if at('gsco_mco',lower(this.GSCO_MCO.class))<>0
    if this.GSCO_MCO.cnt.w_CODLOTTES<>this.w_DPCODLOT or this.GSCO_MCO.cnt.w_CODUBITES<>this.w_DPCODUBI or this.GSCO_MCO.cnt.w_CODUBI<>this.w_DPCODUBI
      this.GSCO_MCO.cnt.w_CODLOTTES = this.w_DPCODLOT
      this.GSCO_MCO.cnt.w_CODUBITES = this.w_DPCODUBI
      this.GSCO_MCO.cnt.w_CODUBI = this.w_DPCODUBI
      this.GSCO_MCO.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DPSERIAL = NVL(DPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_46_joined
    link_1_46_joined=.f.
    local link_1_49_joined
    link_1_49_joined=.f.
    local link_1_52_joined
    link_1_52_joined=.f.
    local link_1_61_joined
    link_1_61_joined=.f.
    local link_1_161_joined
    link_1_161_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIC_PROD where DPSERIAL=KeySet.DPSERIAL
    *
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_PROD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_PROD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_PROD '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_46_joined=this.AddJoinedLink_1_46(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_49_joined=this.AddJoinedLink_1_49(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_52_joined=this.AddJoinedLink_1_52(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_61_joined=this.AddJoinedLink_1_61(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_161_joined=this.AddJoinedLink_1_161(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR2 = 'S'
        .w_CAUDEFA = space(5)
        .w_CAUSCA = space(5)
        .w_CAUCAR = space(5)
        .w_OBTEST = i_datsys
        .w_OLTSTATO = space(1)
        .w_OLTPROVE = space(1)
        .w_OLTQTODL = 0
        .w_OLTQTOEV = 0
        .w_OLTFLEVA = space(1)
        .w_FLLOTT = space(1)
        .w_OLTUNMIS = space(3)
        .w_FLUBIC = space(1)
        .w_TIPVOC = space(1)
        .w_STADOC = space(10)
        .w_DESCON = space(40)
        .w_VOCDES = space(40)
        .w_DESCAN = space(40)
        .w_DESATT = space(30)
        .w_FLLOTT1 = space(1)
        .w_DTOBVO = ctod("  /  /  ")
        .w_CODCOS = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESART = space(40)
        .w_DESMAG = space(30)
        .w_UNMIS3 = space(3)
        .w_OPERA3 = space(1)
        .w_MOLTI3 = 0
        .w_UNMIS1 = space(3)
        .w_OPERAT = space(1)
        .w_MOLTIP = 0
        .w_UNMIS2 = space(3)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESUTE = space(20)
        .w_OLTQTOD1 = 0
        .w_OLTQTOE1 = 0
        .w_TESTFORM = .T.
        .w_OLTKEYSA = space(20)
        .w_OLTQTSAL = 0
        .w_OLTCICLO = space(15)
        .w_MGSCAR = space(5)
        .w_ARTLOT = space(20)
        .w_FLSTAT = space(1)
        .w_EDCOMMAT = True
        .w_CAUMGC = space(5)
        .w_MTCARI = space(1)
        .w_FLUBIC1 = space(1)
        .w_MATCAR = -1
        .w_MATSCA = -1
        .w_RIGMOVLOT = .f.
        .w_FLUSEP = space(1)
        .w_MODUM2 = space(1)
        .w_IMPLOT = space(1)
        .w_ROWDOC = 0
        .w_NUMRIF = -50
        .w_CVDESCRI = space(40)
        .w_PRDATINV = ctod("  /  /  ")
        .w_DESLIS = space(40)
        .w_DESMAG1 = space(30)
        .w_VALESE = space(3)
        .w_ESFINESE = ctod("  /  /  ")
        .w_INGRUMER = space(5)
        .w_INCODFAM = space(5)
        .w_INCODMAG = space(5)
        .w_INMAGCOL = space(1)
        .w_MAGRAG = space(5)
        .w_INCATOMO = space(5)
        .w_TIPOLN = space(1)
        .w_VALLIS = space(3)
        .w_CAOVAL = 0
        .w_UMFLTEMP = 'S'
        .w_CAOESE = 0
        .w_DECTOT = 0
        .w_TATTIV = space(1)
        .w_CAUSER = space(5)
        .w_FASE = 0
        .w_COUPOI = space(1)
        .w_OLTKEYSA1 = space(20)
        .w_CAUMOU = space(5)
        .w_OLTSECPR = space(15)
        .w_ODLSOSPE = space(1)
        .w_DPSERIAL = NVL(DPSERIAL,space(10))
        .op_DPSERIAL = .w_DPSERIAL
        .w_STATOGES = upper(this.cFunction)
          .link_1_3('Load')
        .w_DPDATREG = NVL(cp_ToDate(DPDATREG),ctod("  /  /  "))
        .w_DPNUMOPE = NVL(DPNUMOPE,0)
          .link_1_6('Load')
        .w_DPCAUVER = NVL(DPCAUVER,space(5))
          if link_1_7_joined
            this.w_DPCAUVER = NVL(CVCODICE107,NVL(this.w_DPCAUVER,space(5)))
            this.w_CVDESCRI = NVL(CVDESCRI107,space(40))
            this.w_DPFLPBSC = NVL(CVFLPBSC107,space(1))
            this.w_CAUCAR = NVL(CVCAUCAR107,space(5))
            this.w_CAUSCA = NVL(CVCAUSCA107,space(5))
            this.w_CAUSER = NVL(CVCAUSER107,space(5))
            this.w_CAUMOU = NVL(CVCAUMOU107,space(5))
            this.w_DPUMTEMP = NVL(CVUMTEMP107,space(5))
            this.w_TATTIV = NVL(CVTATTIV107,space(1))
          else
          .link_1_7('Load')
          endif
        .w_DPFLPBSC = NVL(DPFLPBSC,space(1))
        .w_READPAR = 'PP'
          .link_1_9('Load')
          .link_1_11('Load')
        .w_DPTIPATT = NVL(DPTIPATT,space(1))
        .w_DPCODODL = NVL(DPCODODL,space(15))
          .link_1_15('Load')
        .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .w_DPCODART = NVL(DPCODART,space(20))
          if link_1_22_joined
            this.w_DPCODART = NVL(ARCODART122,NVL(this.w_DPCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1122,space(3))
            this.w_OPERAT = NVL(AROPERAT122,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP122,0)
            this.w_UNMIS2 = NVL(ARUNMIS2122,space(3))
            this.w_FLLOTT = NVL(ARFLLOTT122,space(1))
            this.w_GESMAT = NVL(ARGESMAT122,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP122,space(1))
          else
          .link_1_22('Load')
          endif
        .w_DPCODMAG = NVL(DPCODMAG,space(5))
          if link_1_25_joined
            this.w_DPCODMAG = NVL(MGCODMAG125,NVL(this.w_DPCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG125,space(30))
            this.w_FLUBIC = NVL(MGFLUBIC125,space(1))
          else
          .link_1_25('Load')
          endif
        .w_DPCODICE = NVL(DPCODICE,space(20))
          if link_1_27_joined
            this.w_DPCODICE = NVL(CACODICE127,NVL(this.w_DPCODICE,space(20)))
            this.w_DESART = NVL(CADESART127,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS127,space(3))
            this.w_OPERA3 = NVL(CAOPERAT127,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP127,0)
          else
          .link_1_27('Load')
          endif
        .w_DPUNIMIS = NVL(DPUNIMIS,space(3))
        .w_DPQTAPRO = NVL(DPQTAPRO,0)
        .w_DPQTASCA = NVL(DPQTASCA,0)
        .w_DPFLEVAS = NVL(DPFLEVAS,space(1))
        .w_DPUMTEMP = NVL(DPUMTEMP,space(5))
          * evitabile
          *.link_1_33('Load')
        .w_DPQTATEM = NVL(DPQTATEM,0)
        .w_DPDATINI = NVL(cp_ToDate(DPDATINI),ctod("  /  /  "))
        .w_DPORAINI = NVL(DPORAINI,space(5))
        .w_DPDATFIN = NVL(cp_ToDate(DPDATFIN),ctod("  /  /  "))
        .w_DPORAFIN = NVL(DPORAFIN,space(5))
        .w_DPCODCEN = NVL(DPCODCEN,space(15))
          if link_1_46_joined
            this.w_DPCODCEN = NVL(CC_CONTO146,NVL(this.w_DPCODCEN,space(15)))
            this.w_DESCON = NVL(CCDESPIA146,space(40))
          else
          .link_1_46('Load')
          endif
        .w_DPCODVOC = NVL(DPCODVOC,space(15))
          if link_1_49_joined
            this.w_DPCODVOC = NVL(VCCODICE149,NVL(this.w_DPCODVOC,space(15)))
            this.w_VOCDES = NVL(VCDESCRI149,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC149,space(1))
            this.w_DTOBVO = NVL(cp_ToDate(VCDTOBSO149),ctod("  /  /  "))
            this.w_CODCOS = NVL(VCTIPCOS149,space(5))
          else
          .link_1_49('Load')
          endif
        .w_DPCODCOM = NVL(DPCODCOM,space(15))
          if link_1_52_joined
            this.w_DPCODCOM = NVL(CNCODCAN152,NVL(this.w_DPCODCOM,space(15)))
            this.w_DTOBSO = NVL(cp_ToDate(CNDTOBSO152),ctod("  /  /  "))
            this.w_DESCAN = NVL(CNDESCAN152,space(40))
          else
          .link_1_52('Load')
          endif
        .w_DPCODATT = NVL(DPCODATT,space(15))
          .link_1_54('Load')
        .w_DPCODLOT = NVL(DPCODLOT,space(20))
          .link_1_56('Load')
        .w_DPODLFAS = NVL(DPODLFAS,space(15))
          * evitabile
          *.link_1_58('Load')
        .w_DPODLFAS = NVL(DPODLFAS,space(15))
          .link_1_59('Load')
        .w_DPCODPAD = NVL(DPCODPAD,space(20))
          * evitabile
          *.link_1_60('Load')
        .w_DPARTPAD = NVL(DPARTPAD,space(20))
          if link_1_61_joined
            this.w_DPARTPAD = NVL(ARCODART161,NVL(this.w_DPARTPAD,space(20)))
            this.w_FLLOTT1 = NVL(ARFLLOTT161,space(1))
            this.w_GESMAT1 = NVL(ARGESMAT161,space(1))
          else
          .link_1_61('Load')
          endif
        .w_GESMAT1 = iif(not .w_MATENABL, "N", .w_GESMAT1)
        .w_DPULTFAS = NVL(DPULTFAS,space(1))
        .w_DPCODLOT = NVL(DPCODLOT,space(20))
          .link_1_65('Load')
        .w_DPCODUBI = NVL(DPCODUBI,space(20))
          * evitabile
          *.link_1_67('Load')
          .link_1_78('Load')
        .w_DPQTAPR1 = NVL(DPQTAPR1,0)
        .w_DPQTASC1 = NVL(DPQTASC1,0)
        .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .w_DPRIFCAR = NVL(DPRIFCAR,space(10))
        .w_DPRIFSCA = NVL(DPRIFSCA,space(10))
        .w_DPRIFSER = NVL(DPRIFSER,space(10))
          .link_1_114('Load')
        .w_CRIVAL = iif(.w_DPTIPVAL='X','US',iif(.w_DPTIPVAL='L','CL',iif(.w_DPTIPVAL='S','CS',iif(.w_DPTIPVAL='M','CM',iif(.w_DPTIPVAL='U','UC',iif(.w_DPTIPVAL='P','UP',iif(.w_DPTIPVAL='A','UA','US')))))))
        .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
        .w_DPCOMMAT = NVL(DPCOMMAT,space(1))
        .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
          .link_1_126('Load')
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_132.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_133.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_135.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .w_FLPRG = 'K'
        .w_DPCRIVAL = NVL(DPCRIVAL,space(1))
        .w_DPTIPVAL = NVL(DPTIPVAL,space(1))
        .w_DPESERCZ = NVL(DPESERCZ,space(4))
          .link_1_158('Load')
        .w_DPNUMINV = NVL(DPNUMINV,space(6))
          .link_1_159('Load')
        .w_DPLISTIN = NVL(DPLISTIN,space(5))
          .link_1_160('Load')
        .w_DPMAGCOD = NVL(DPMAGCOD,space(5))
          if link_1_161_joined
            this.w_DPMAGCOD = NVL(MGCODMAG261,NVL(this.w_DPMAGCOD,space(5)))
            this.w_DESMAG1 = NVL(MGDESMAG261,space(30))
          else
          .link_1_161('Load')
          endif
          .link_1_162('Load')
        .w_CAOLIS = IIF(.w_DPTIPVAL='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, i_datsys, 7), .w_CAOVAL), .w_CAOLIS)
          .link_1_172('Load')
        .w_DPCPRFAS = NVL(DPCPRFAS,0)
        .w_DPODLFAS = NVL(DPODLFAS,space(15))
          .link_1_180('Load')
        .w_DPFASOUT = NVL(DPFASOUT,space(1))
        .w_DPRIFMOU = NVL(DPRIFMOU,space(10))
        .oPgFrm.Page1.oPag.oObj_1_190.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIC_PROD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DPSERIAL = space(10)
      .w_STATOGES = space(10)
      .w_READPAR2 = space(1)
      .w_CAUDEFA = space(5)
      .w_DPDATREG = ctod("  /  /  ")
      .w_DPNUMOPE = 0
      .w_DPCAUVER = space(5)
      .w_DPFLPBSC = space(1)
      .w_READPAR = space(10)
      .w_CAUSCA = space(5)
      .w_CAUCAR = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DPTIPATT = space(1)
      .w_DPCODODL = space(15)
      .w_OLTSTATO = space(1)
      .w_OLTPROVE = space(1)
      .w_OLTQTODL = 0
      .w_OLTQTOEV = 0
      .w_QTARES = 0
      .w_OLTFLEVA = space(1)
      .w_DPCODART = space(20)
      .w_FLLOTT = space(1)
      .w_OLTUNMIS = space(3)
      .w_DPCODMAG = space(5)
      .w_FLUBIC = space(1)
      .w_DPCODICE = space(20)
      .w_DPUNIMIS = space(3)
      .w_TIPVOC = space(1)
      .w_DPQTAPRO = 0
      .w_DPQTASCA = 0
      .w_DPFLEVAS = space(1)
      .w_DPUMTEMP = space(5)
      .w_DPQTATEM = 0
      .w_DPDATINI = ctod("  /  /  ")
      .w_DPORAINI = space(5)
      .w_DPDATFIN = ctod("  /  /  ")
      .w_DPORAFIN = space(5)
      .w_STADOC = space(10)
      .w_DPCODCEN = space(15)
      .w_DESCON = space(40)
      .w_DPCODVOC = space(15)
      .w_VOCDES = space(40)
      .w_DPCODCOM = space(15)
      .w_DPCODATT = space(15)
      .w_DESCAN = space(40)
      .w_DPCODLOT = space(20)
      .w_DESATT = space(30)
      .w_DPODLFAS = space(15)
      .w_DPODLFAS = space(15)
      .w_DPCODPAD = space(20)
      .w_DPARTPAD = space(20)
      .w_FLLOTT1 = space(1)
      .w_GESMAT1 = space(1)
      .w_DPULTFAS = space(1)
      .w_DPCODLOT = space(20)
      .w_DPCODUBI = space(20)
      .w_DTOBVO = ctod("  /  /  ")
      .w_CODCOS = space(5)
      .w_DTOBSO = ctod("  /  /  ")
      .w_DESART = space(40)
      .w_DESMAG = space(30)
      .w_UNMIS3 = space(3)
      .w_OPERA3 = space(1)
      .w_MOLTI3 = 0
      .w_UNMIS1 = space(3)
      .w_OPERAT = space(1)
      .w_MOLTIP = 0
      .w_UNMIS2 = space(3)
      .w_DPQTAPR1 = 0
      .w_DPQTASC1 = 0
      .w_QTAUM1 = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESUTE = space(20)
      .w_OLTQTOD1 = 0
      .w_OLTQTOE1 = 0
      .w_TESTFORM = .f.
      .w_DPRIFCAR = space(10)
      .w_DPRIFSCA = space(10)
      .w_DPRIFSER = space(10)
      .w_OLTKEYSA = space(20)
      .w_OLTQTSAL = 0
      .w_OLTCICLO = space(15)
      .w_MGSCAR = space(5)
      .w_CRIVAL = space(2)
      .w_ARTLOT = space(20)
      .w_FLSTAT = space(1)
      .w_MATENABL = .f.
      .w_DPCOMMAT = space(1)
      .w_GESMAT = space(1)
      .w_EDCOMMAT = .f.
      .w_CAUMGC = space(5)
      .w_MTCARI = space(1)
      .w_FLUBIC1 = space(1)
      .w_MATCAR = 0
      .w_MATSCA = 0
      .w_RIGMOVLOT = .f.
      .w_FLUSEP = space(1)
      .w_MODUM2 = space(1)
      .w_IMPLOT = space(1)
      .w_ROWDOC = 0
      .w_NUMRIF = 0
      .w_FLPRG = space(1)
      .w_CVDESCRI = space(40)
      .w_DPCRIVAL = space(1)
      .w_PRDATINV = ctod("  /  /  ")
      .w_DPTIPVAL = space(1)
      .w_DESLIS = space(40)
      .w_DESMAG1 = space(30)
      .w_DPESERCZ = space(4)
      .w_DPNUMINV = space(6)
      .w_DPLISTIN = space(5)
      .w_DPMAGCOD = space(5)
      .w_VALESE = space(3)
      .w_CAOLIS = 0
      .w_ESFINESE = ctod("  /  /  ")
      .w_INGRUMER = space(5)
      .w_INCODFAM = space(5)
      .w_INCODMAG = space(5)
      .w_INMAGCOL = space(1)
      .w_MAGRAG = space(5)
      .w_INCATOMO = space(5)
      .w_TIPOLN = space(1)
      .w_VALLIS = space(3)
      .w_CAOVAL = 0
      .w_UMFLTEMP = space(1)
      .w_CAOESE = 0
      .w_DECTOT = 0
      .w_TATTIV = space(1)
      .w_CAUSER = space(5)
      .w_DPCPRFAS = 0
      .w_DPODLFAS = space(15)
      .w_FASE = 0
      .w_COUPOI = space(1)
      .w_DPFASOUT = space(1)
      .w_OLTKEYSA1 = space(20)
      .w_DPRIFMOU = space(10)
      .w_CAUMOU = space(5)
      .w_OLTSECPR = space(15)
      .w_ODLSOSPE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_STATOGES = upper(this.cFunction)
        .w_READPAR2 = 'S'
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_READPAR2))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_DPDATREG = i_datsys
        .w_DPNUMOPE = i_CODUTE
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_DPNUMOPE))
          .link_1_6('Full')
          endif
        .w_DPCAUVER = iif(.w_STATOGES='LOAD' and empty(.w_DPCAUVER),.w_CAUDEFA,.w_DPCAUVER)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_DPCAUVER))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,8,.f.)
        .w_READPAR = 'PP'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_READPAR))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_CAUCAR))
          .link_1_11('Full')
          endif
        .w_OBTEST = i_datsys
        .w_DPTIPATT = 'A'
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_DPCODODL))
          .link_1_15('Full')
          endif
          .DoRTCalc(15,18,.f.)
        .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .DoRTCalc(20,21,.f.)
          if not(empty(.w_DPCODART))
          .link_1_22('Full')
          endif
        .DoRTCalc(22,24,.f.)
          if not(empty(.w_DPCODMAG))
          .link_1_25('Full')
          endif
        .DoRTCalc(25,26,.f.)
          if not(empty(.w_DPCODICE))
          .link_1_27('Full')
          endif
        .w_DPUNIMIS = iif(.w_TATTIV='T','   ',.w_OLTUNMIS)
          .DoRTCalc(28,28,.f.)
        .w_DPQTAPRO = iif(.w_DPFLPBSC $ 'B-E' and .w_TATTIV $ 'Q-E',.w_QTARES,0)
        .w_DPQTASCA = 0
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_DPUMTEMP))
          .link_1_33('Full')
          endif
        .w_DPQTATEM = iif(.w_TATTIV = 'T',.w_DPQTATEM,0)
        .DoRTCalc(34,39,.f.)
          if not(empty(.w_DPCODCEN))
          .link_1_46('Full')
          endif
        .DoRTCalc(40,41,.f.)
          if not(empty(.w_DPCODVOC))
          .link_1_49('Full')
          endif
        .DoRTCalc(42,43,.f.)
          if not(empty(.w_DPCODCOM))
          .link_1_52('Full')
          endif
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_DPCODATT))
          .link_1_54('Full')
          endif
          .DoRTCalc(45,45,.f.)
        .w_DPCODLOT = SPACE(20)
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_DPCODLOT))
          .link_1_56('Full')
          endif
        .DoRTCalc(47,48,.f.)
          if not(empty(.w_DPODLFAS))
          .link_1_58('Full')
          endif
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_DPODLFAS))
          .link_1_59('Full')
          endif
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_DPCODPAD))
          .link_1_60('Full')
          endif
        .DoRTCalc(51,51,.f.)
          if not(empty(.w_DPARTPAD))
          .link_1_61('Full')
          endif
          .DoRTCalc(52,52,.f.)
        .w_GESMAT1 = iif(not .w_MATENABL, "N", .w_GESMAT1)
          .DoRTCalc(54,54,.f.)
        .w_DPCODLOT = SPACE(20)
        .DoRTCalc(55,55,.f.)
          if not(empty(.w_DPCODLOT))
          .link_1_65('Full')
          endif
        .w_DPCODUBI = SPACE(20)
        .DoRTCalc(56,56,.f.)
          if not(empty(.w_DPCODUBI))
          .link_1_67('Full')
          endif
        .DoRTCalc(57,65,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_78('Full')
          endif
          .DoRTCalc(66,68,.f.)
        .w_DPQTAPR1 = CALQTAADV(.w_DPQTAPRO,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTAPRO")
        .w_DPQTASC1 = CALQTAADV(.w_DPQTASCA,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTASCA")
        .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
          .DoRTCalc(72,75,.f.)
        .w_TESTFORM = .T.
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .DoRTCalc(77,83,.f.)
          if not(empty(.w_MGSCAR))
          .link_1_114('Full')
          endif
        .w_CRIVAL = iif(.w_DPTIPVAL='X','US',iif(.w_DPTIPVAL='L','CL',iif(.w_DPTIPVAL='S','CS',iif(.w_DPTIPVAL='M','CM',iif(.w_DPTIPVAL='U','UC',iif(.w_DPTIPVAL='P','UP',iif(.w_DPTIPVAL='A','UA','US')))))))
          .DoRTCalc(85,86,.f.)
        .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
          .DoRTCalc(88,88,.f.)
        .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
        .w_EDCOMMAT = True
        .DoRTCalc(91,91,.f.)
          if not(empty(.w_CAUMGC))
          .link_1_126('Full')
          endif
          .DoRTCalc(92,93,.f.)
        .w_MATCAR = -1
        .w_MATSCA = -1
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_132.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_133.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_135.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
          .DoRTCalc(96,99,.f.)
        .w_ROWDOC = 0
        .w_NUMRIF = -50
        .w_FLPRG = 'K'
          .DoRTCalc(103,103,.f.)
        .w_DPCRIVAL = 'D'
          .DoRTCalc(105,105,.f.)
        .w_DPTIPVAL = 'X'
          .DoRTCalc(107,108,.f.)
        .w_DPESERCZ = g_CODESE
        .DoRTCalc(109,109,.f.)
          if not(empty(.w_DPESERCZ))
          .link_1_158('Full')
          endif
        .DoRTCalc(110,110,.f.)
          if not(empty(.w_DPNUMINV))
          .link_1_159('Full')
          endif
        .DoRTCalc(111,111,.f.)
          if not(empty(.w_DPLISTIN))
          .link_1_160('Full')
          endif
        .w_DPMAGCOD = g_MAGAZI
        .DoRTCalc(112,112,.f.)
          if not(empty(.w_DPMAGCOD))
          .link_1_161('Full')
          endif
        .DoRTCalc(113,113,.f.)
          if not(empty(.w_VALESE))
          .link_1_162('Full')
          endif
        .w_CAOLIS = IIF(.w_DPTIPVAL='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, i_datsys, 7), .w_CAOVAL), .w_CAOLIS)
        .DoRTCalc(115,123,.f.)
          if not(empty(.w_VALLIS))
          .link_1_172('Full')
          endif
          .DoRTCalc(124,124,.f.)
        .w_UMFLTEMP = 'S'
        .DoRTCalc(126,131,.f.)
          if not(empty(.w_DPODLFAS))
          .link_1_180('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_190.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_PROD')
    this.DoRTCalc(132,139,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DPSERIAL = .w_DPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDPSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oDPDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oDPCAUVER_1_7.enabled = i_bVal
      .Page1.oPag.oDPCODODL_1_15.enabled = i_bVal
      .Page1.oPag.oDPQTAPRO_1_30.enabled = i_bVal
      .Page1.oPag.oDPQTASCA_1_31.enabled = i_bVal
      .Page1.oPag.oDPFLEVAS_1_32.enabled = i_bVal
      .Page1.oPag.oDPUMTEMP_1_33.enabled = i_bVal
      .Page1.oPag.oDPQTATEM_1_34.enabled = i_bVal
      .Page1.oPag.oSTADOC_1_39.enabled = i_bVal
      .Page1.oPag.oDPCODCEN_1_46.enabled = i_bVal
      .Page1.oPag.oDPCODVOC_1_49.enabled = i_bVal
      .Page1.oPag.oDPCODCOM_1_52.enabled = i_bVal
      .Page1.oPag.oDPCODATT_1_54.enabled = i_bVal
      .Page1.oPag.oDPCODLOT_1_56.enabled = i_bVal
      .Page1.oPag.oDPODLFAS_1_58.enabled = i_bVal
      .Page1.oPag.oDPCODLOT_1_65.enabled = i_bVal
      .Page1.oPag.oDPCODUBI_1_67.enabled = i_bVal
      .Page1.oPag.oDPCRIVAL_1_146.enabled = i_bVal
      .Page1.oPag.oDPTIPVAL_1_152.enabled = i_bVal
      .Page1.oPag.oDPESERCZ_1_158.enabled = i_bVal
      .Page1.oPag.oDPNUMINV_1_159.enabled = i_bVal
      .Page1.oPag.oDPLISTIN_1_160.enabled = i_bVal
      .Page1.oPag.oDPMAGCOD_1_161.enabled = i_bVal
      .Page1.oPag.oCAOLIS_1_163.enabled = i_bVal
      .Page1.oPag.oBtn_1_66.enabled = i_bVal
      .Page1.oPag.oBtn_1_108.enabled = .Page1.oPag.oBtn_1_108.mCond()
      .Page1.oPag.oBtn_1_110.enabled = .Page1.oPag.oBtn_1_110.mCond()
      .Page1.oPag.oBtn_1_111.enabled = .Page1.oPag.oBtn_1_111.mCond()
      .Page1.oPag.oBtn_1_113.enabled = .Page1.oPag.oBtn_1_113.mCond()
      .Page1.oPag.oObj_1_96.enabled = i_bVal
      .Page1.oPag.oObj_1_99.enabled = i_bVal
      .Page1.oPag.oObj_1_100.enabled = i_bVal
      .Page1.oPag.oObj_1_102.enabled = i_bVal
      .Page1.oPag.oObj_1_103.enabled = i_bVal
      .Page1.oPag.oObj_1_131.enabled = i_bVal
      .Page1.oPag.oObj_1_132.enabled = i_bVal
      .Page1.oPag.oObj_1_133.enabled = i_bVal
      .Page1.oPag.oObj_1_135.enabled = i_bVal
      .Page1.oPag.oObj_1_138.enabled = i_bVal
      .Page1.oPag.oObj_1_190.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDPSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDPSERIAL_1_1.enabled = .t.
        .Page1.oPag.oDPDATREG_1_5.enabled = .t.
        .Page1.oPag.oDPCAUVER_1_7.enabled = .t.
        .Page1.oPag.oDPCODODL_1_15.enabled = .t.
      endif
    endwith
    this.GSCO_MMP.SetStatus(i_cOp)
    this.GSCI_MRP.SetStatus(i_cOp)
    this.GSCO_MMT.SetStatus(i_cOp)
    this.GSCO_MCO.SetStatus(i_cOp)
    this.GSCO_MOU.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIC_PROD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsci_adp
    this.w_STATOGES=UPPER(this.cFunction)
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCO_MMP.SetChildrenStatus(i_cOp)
  *  this.GSCI_MRP.SetChildrenStatus(i_cOp)
  *  this.GSCO_MMT.SetChildrenStatus(i_cOp)
  *  this.GSCO_MCO.SetChildrenStatus(i_cOp)
  *  this.GSCO_MOU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPSERIAL,"DPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATREG,"DPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNUMOPE,"DPNUMOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCAUVER,"DPCAUVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLPBSC,"DPFLPBSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPATT,"DPTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODODL,"DPCODODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODART,"DPCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODMAG,"DPCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODICE,"DPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPUNIMIS,"DPUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTAPRO,"DPQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTASCA,"DPQTASCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFLEVAS,"DPFLEVAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPUMTEMP,"DPUMTEMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTATEM,"DPQTATEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATINI,"DPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPORAINI,"DPORAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPDATFIN,"DPDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPORAFIN,"DPORAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCEN,"DPCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODVOC,"DPCODVOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODCOM,"DPCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODATT,"DPCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODLOT,"DPCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPODLFAS,"DPODLFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPODLFAS,"DPODLFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODPAD,"DPCODPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPARTPAD,"DPARTPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPULTFAS,"DPULTFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODLOT,"DPCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCODUBI,"DPCODUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTAPR1,"DPQTAPR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPQTASC1,"DPQTASC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFCAR,"DPRIFCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFSCA,"DPRIFSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFSER,"DPRIFSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCOMMAT,"DPCOMMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCRIVAL,"DPCRIVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPTIPVAL,"DPTIPVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPESERCZ,"DPESERCZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPNUMINV,"DPNUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPLISTIN,"DPLISTIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPMAGCOD,"DPMAGCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPCPRFAS,"DPCPRFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPODLFAS,"DPODLFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPFASOUT,"DPFASOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DPRIFMOU,"DPRIFMOU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    i_lTable = "DIC_PROD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIC_PROD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIC_PROD_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIC_PROD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_PROD')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_PROD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DPSERIAL,DPDATREG,DPNUMOPE,DPCAUVER,DPFLPBSC"+;
                  ",DPTIPATT,DPCODODL,DPCODART,DPCODMAG,DPCODICE"+;
                  ",DPUNIMIS,DPQTAPRO,DPQTASCA,DPFLEVAS,DPUMTEMP"+;
                  ",DPQTATEM,DPDATINI,DPORAINI,DPDATFIN,DPORAFIN"+;
                  ",DPCODCEN,DPCODVOC,DPCODCOM,DPCODATT,DPCODLOT"+;
                  ",DPODLFAS,DPCODPAD,DPARTPAD,DPULTFAS,DPCODUBI"+;
                  ",DPQTAPR1,DPQTASC1,DPRIFCAR,DPRIFSCA,DPRIFSER"+;
                  ",DPCOMMAT,DPCRIVAL,DPTIPVAL,DPESERCZ,DPNUMINV"+;
                  ",DPLISTIN,DPMAGCOD,DPCPRFAS,DPFASOUT,DPRIFMOU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DPDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_DPNUMOPE)+;
                  ","+cp_ToStrODBCNull(this.w_DPCAUVER)+;
                  ","+cp_ToStrODBC(this.w_DPFLPBSC)+;
                  ","+cp_ToStrODBC(this.w_DPTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODODL)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODART)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODICE)+;
                  ","+cp_ToStrODBC(this.w_DPUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_DPQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_DPQTASCA)+;
                  ","+cp_ToStrODBC(this.w_DPFLEVAS)+;
                  ","+cp_ToStrODBCNull(this.w_DPUMTEMP)+;
                  ","+cp_ToStrODBC(this.w_DPQTATEM)+;
                  ","+cp_ToStrODBC(this.w_DPDATINI)+;
                  ","+cp_ToStrODBC(this.w_DPORAINI)+;
                  ","+cp_ToStrODBC(this.w_DPDATFIN)+;
                  ","+cp_ToStrODBC(this.w_DPORAFIN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODVOC)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODCOM)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODLOT)+;
                  ","+cp_ToStrODBCNull(this.w_DPODLFAS)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODPAD)+;
                  ","+cp_ToStrODBCNull(this.w_DPARTPAD)+;
                  ","+cp_ToStrODBC(this.w_DPULTFAS)+;
                  ","+cp_ToStrODBCNull(this.w_DPCODUBI)+;
                  ","+cp_ToStrODBC(this.w_DPQTAPR1)+;
                  ","+cp_ToStrODBC(this.w_DPQTASC1)+;
                  ","+cp_ToStrODBC(this.w_DPRIFCAR)+;
                  ","+cp_ToStrODBC(this.w_DPRIFSCA)+;
                  ","+cp_ToStrODBC(this.w_DPRIFSER)+;
                  ","+cp_ToStrODBC(this.w_DPCOMMAT)+;
                  ","+cp_ToStrODBC(this.w_DPCRIVAL)+;
                  ","+cp_ToStrODBC(this.w_DPTIPVAL)+;
                  ","+cp_ToStrODBCNull(this.w_DPESERCZ)+;
                  ","+cp_ToStrODBCNull(this.w_DPNUMINV)+;
                  ","+cp_ToStrODBCNull(this.w_DPLISTIN)+;
                  ","+cp_ToStrODBCNull(this.w_DPMAGCOD)+;
                  ","+cp_ToStrODBC(this.w_DPCPRFAS)+;
                  ","+cp_ToStrODBC(this.w_DPFASOUT)+;
                  ","+cp_ToStrODBC(this.w_DPRIFMOU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_PROD')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_PROD')
        cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_DPSERIAL)
        INSERT INTO (i_cTable);
              (DPSERIAL,DPDATREG,DPNUMOPE,DPCAUVER,DPFLPBSC,DPTIPATT,DPCODODL,DPCODART,DPCODMAG,DPCODICE,DPUNIMIS,DPQTAPRO,DPQTASCA,DPFLEVAS,DPUMTEMP,DPQTATEM,DPDATINI,DPORAINI,DPDATFIN,DPORAFIN,DPCODCEN,DPCODVOC,DPCODCOM,DPCODATT,DPCODLOT,DPODLFAS,DPCODPAD,DPARTPAD,DPULTFAS,DPCODUBI,DPQTAPR1,DPQTASC1,DPRIFCAR,DPRIFSCA,DPRIFSER,DPCOMMAT,DPCRIVAL,DPTIPVAL,DPESERCZ,DPNUMINV,DPLISTIN,DPMAGCOD,DPCPRFAS,DPFASOUT,DPRIFMOU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DPSERIAL;
                  ,this.w_DPDATREG;
                  ,this.w_DPNUMOPE;
                  ,this.w_DPCAUVER;
                  ,this.w_DPFLPBSC;
                  ,this.w_DPTIPATT;
                  ,this.w_DPCODODL;
                  ,this.w_DPCODART;
                  ,this.w_DPCODMAG;
                  ,this.w_DPCODICE;
                  ,this.w_DPUNIMIS;
                  ,this.w_DPQTAPRO;
                  ,this.w_DPQTASCA;
                  ,this.w_DPFLEVAS;
                  ,this.w_DPUMTEMP;
                  ,this.w_DPQTATEM;
                  ,this.w_DPDATINI;
                  ,this.w_DPORAINI;
                  ,this.w_DPDATFIN;
                  ,this.w_DPORAFIN;
                  ,this.w_DPCODCEN;
                  ,this.w_DPCODVOC;
                  ,this.w_DPCODCOM;
                  ,this.w_DPCODATT;
                  ,this.w_DPCODLOT;
                  ,this.w_DPODLFAS;
                  ,this.w_DPCODPAD;
                  ,this.w_DPARTPAD;
                  ,this.w_DPULTFAS;
                  ,this.w_DPCODUBI;
                  ,this.w_DPQTAPR1;
                  ,this.w_DPQTASC1;
                  ,this.w_DPRIFCAR;
                  ,this.w_DPRIFSCA;
                  ,this.w_DPRIFSER;
                  ,this.w_DPCOMMAT;
                  ,this.w_DPCRIVAL;
                  ,this.w_DPTIPVAL;
                  ,this.w_DPESERCZ;
                  ,this.w_DPNUMINV;
                  ,this.w_DPLISTIN;
                  ,this.w_DPMAGCOD;
                  ,this.w_DPCPRFAS;
                  ,this.w_DPFASOUT;
                  ,this.w_DPRIFMOU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIC_PROD_IDX,i_nConn)
      *
      * update DIC_PROD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_PROD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DPDATREG="+cp_ToStrODBC(this.w_DPDATREG)+;
             ",DPNUMOPE="+cp_ToStrODBCNull(this.w_DPNUMOPE)+;
             ",DPCAUVER="+cp_ToStrODBCNull(this.w_DPCAUVER)+;
             ",DPFLPBSC="+cp_ToStrODBC(this.w_DPFLPBSC)+;
             ",DPTIPATT="+cp_ToStrODBC(this.w_DPTIPATT)+;
             ",DPCODODL="+cp_ToStrODBCNull(this.w_DPCODODL)+;
             ",DPCODART="+cp_ToStrODBCNull(this.w_DPCODART)+;
             ",DPCODMAG="+cp_ToStrODBCNull(this.w_DPCODMAG)+;
             ",DPCODICE="+cp_ToStrODBCNull(this.w_DPCODICE)+;
             ",DPUNIMIS="+cp_ToStrODBC(this.w_DPUNIMIS)+;
             ",DPQTAPRO="+cp_ToStrODBC(this.w_DPQTAPRO)+;
             ",DPQTASCA="+cp_ToStrODBC(this.w_DPQTASCA)+;
             ",DPFLEVAS="+cp_ToStrODBC(this.w_DPFLEVAS)+;
             ",DPUMTEMP="+cp_ToStrODBCNull(this.w_DPUMTEMP)+;
             ",DPQTATEM="+cp_ToStrODBC(this.w_DPQTATEM)+;
             ",DPDATINI="+cp_ToStrODBC(this.w_DPDATINI)+;
             ",DPORAINI="+cp_ToStrODBC(this.w_DPORAINI)+;
             ",DPDATFIN="+cp_ToStrODBC(this.w_DPDATFIN)+;
             ",DPORAFIN="+cp_ToStrODBC(this.w_DPORAFIN)+;
             ",DPCODCEN="+cp_ToStrODBCNull(this.w_DPCODCEN)+;
             ",DPCODVOC="+cp_ToStrODBCNull(this.w_DPCODVOC)+;
             ",DPCODCOM="+cp_ToStrODBCNull(this.w_DPCODCOM)+;
             ",DPCODATT="+cp_ToStrODBCNull(this.w_DPCODATT)+;
             ",DPCODLOT="+cp_ToStrODBCNull(this.w_DPCODLOT)+;
             ",DPODLFAS="+cp_ToStrODBCNull(this.w_DPODLFAS)+;
             ",DPCODPAD="+cp_ToStrODBCNull(this.w_DPCODPAD)+;
             ",DPARTPAD="+cp_ToStrODBCNull(this.w_DPARTPAD)+;
             ",DPULTFAS="+cp_ToStrODBC(this.w_DPULTFAS)+;
             ",DPCODUBI="+cp_ToStrODBCNull(this.w_DPCODUBI)+;
             ",DPQTAPR1="+cp_ToStrODBC(this.w_DPQTAPR1)+;
             ",DPQTASC1="+cp_ToStrODBC(this.w_DPQTASC1)+;
             ",DPRIFCAR="+cp_ToStrODBC(this.w_DPRIFCAR)+;
             ",DPRIFSCA="+cp_ToStrODBC(this.w_DPRIFSCA)+;
             ",DPRIFSER="+cp_ToStrODBC(this.w_DPRIFSER)+;
             ",DPCOMMAT="+cp_ToStrODBC(this.w_DPCOMMAT)+;
             ",DPCRIVAL="+cp_ToStrODBC(this.w_DPCRIVAL)+;
             ",DPTIPVAL="+cp_ToStrODBC(this.w_DPTIPVAL)+;
             ",DPESERCZ="+cp_ToStrODBCNull(this.w_DPESERCZ)+;
             ",DPNUMINV="+cp_ToStrODBCNull(this.w_DPNUMINV)+;
             ",DPLISTIN="+cp_ToStrODBCNull(this.w_DPLISTIN)+;
             ",DPMAGCOD="+cp_ToStrODBCNull(this.w_DPMAGCOD)+;
             ",DPCPRFAS="+cp_ToStrODBC(this.w_DPCPRFAS)+;
             ",DPFASOUT="+cp_ToStrODBC(this.w_DPFASOUT)+;
             ",DPRIFMOU="+cp_ToStrODBC(this.w_DPRIFMOU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_PROD')
        i_cWhere = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
        UPDATE (i_cTable) SET;
              DPDATREG=this.w_DPDATREG;
             ,DPNUMOPE=this.w_DPNUMOPE;
             ,DPCAUVER=this.w_DPCAUVER;
             ,DPFLPBSC=this.w_DPFLPBSC;
             ,DPTIPATT=this.w_DPTIPATT;
             ,DPCODODL=this.w_DPCODODL;
             ,DPCODART=this.w_DPCODART;
             ,DPCODMAG=this.w_DPCODMAG;
             ,DPCODICE=this.w_DPCODICE;
             ,DPUNIMIS=this.w_DPUNIMIS;
             ,DPQTAPRO=this.w_DPQTAPRO;
             ,DPQTASCA=this.w_DPQTASCA;
             ,DPFLEVAS=this.w_DPFLEVAS;
             ,DPUMTEMP=this.w_DPUMTEMP;
             ,DPQTATEM=this.w_DPQTATEM;
             ,DPDATINI=this.w_DPDATINI;
             ,DPORAINI=this.w_DPORAINI;
             ,DPDATFIN=this.w_DPDATFIN;
             ,DPORAFIN=this.w_DPORAFIN;
             ,DPCODCEN=this.w_DPCODCEN;
             ,DPCODVOC=this.w_DPCODVOC;
             ,DPCODCOM=this.w_DPCODCOM;
             ,DPCODATT=this.w_DPCODATT;
             ,DPCODLOT=this.w_DPCODLOT;
             ,DPODLFAS=this.w_DPODLFAS;
             ,DPCODPAD=this.w_DPCODPAD;
             ,DPARTPAD=this.w_DPARTPAD;
             ,DPULTFAS=this.w_DPULTFAS;
             ,DPCODUBI=this.w_DPCODUBI;
             ,DPQTAPR1=this.w_DPQTAPR1;
             ,DPQTASC1=this.w_DPQTASC1;
             ,DPRIFCAR=this.w_DPRIFCAR;
             ,DPRIFSCA=this.w_DPRIFSCA;
             ,DPRIFSER=this.w_DPRIFSER;
             ,DPCOMMAT=this.w_DPCOMMAT;
             ,DPCRIVAL=this.w_DPCRIVAL;
             ,DPTIPVAL=this.w_DPTIPVAL;
             ,DPESERCZ=this.w_DPESERCZ;
             ,DPNUMINV=this.w_DPNUMINV;
             ,DPLISTIN=this.w_DPLISTIN;
             ,DPMAGCOD=this.w_DPMAGCOD;
             ,DPCPRFAS=this.w_DPCPRFAS;
             ,DPFASOUT=this.w_DPFASOUT;
             ,DPRIFMOU=this.w_DPRIFMOU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCO_MMP : Saving
      this.GSCO_MMP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MPSERIAL";
             ,this.w_ROWDOC,"MPROWDOC";
             ,this.w_NUMRIF,"MPNUMRIF";
             )
      this.GSCO_MMP.mReplace()
      * --- GSCI_MRP : Saving
      this.GSCI_MRP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"RFSERIAL";
             )
      this.GSCI_MRP.mReplace()
      * --- GSCO_MMT : Saving
      this.GSCO_MMT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MTSERIAL";
             )
      this.GSCO_MMT.mReplace()
      * --- GSCO_MCO : Saving
      this.GSCO_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MTSERIAL";
             )
      this.GSCO_MCO.mReplace()
      * --- GSCO_MOU : Saving
      this.GSCO_MOU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DPSERIAL,"MDSERIAL";
             )
      this.GSCO_MOU.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsci_adp
    * --- Controlli Finali su DB
    if not(bTrsErr) and (this.w_GESMAT='S' or this.w_DPCOMMAT="S")
        * --- Controlli Righe Lotti/Ubicazioni
        this.NotifyEvent('Controlli Updated')
    endif
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsci_adp
    this.NotifyEvent('CTRLMAT')
    * --- Fine Area Manuale
    * --- GSCO_MMP : Deleting
    this.GSCO_MMP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MPSERIAL";
           ,this.w_ROWDOC,"MPROWDOC";
           ,this.w_NUMRIF,"MPNUMRIF";
           )
    this.GSCO_MMP.mDelete()
    * --- GSCI_MRP : Deleting
    this.GSCI_MRP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"RFSERIAL";
           )
    this.GSCI_MRP.mDelete()
    * --- GSCO_MMT : Deleting
    this.GSCO_MMT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MTSERIAL";
           )
    this.GSCO_MMT.mDelete()
    * --- GSCO_MCO : Deleting
    this.GSCO_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MTSERIAL";
           )
    this.GSCO_MCO.mDelete()
    * --- GSCO_MOU : Deleting
    this.GSCO_MOU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DPSERIAL,"MDSERIAL";
           )
    this.GSCO_MOU.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIC_PROD_IDX,i_nConn)
      *
      * delete DIC_PROD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DPSERIAL',this.w_DPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_PROD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_PROD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_STATOGES = upper(this.cFunction)
          .link_1_3('Full')
        .DoRTCalc(4,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,8,.t.)
            .w_READPAR = 'PP'
          .link_1_9('Full')
        .DoRTCalc(10,10,.t.)
          .link_1_11('Full')
        .DoRTCalc(12,18,.t.)
            .w_QTARES = MAX(0, .w_OLTQTODL-.w_OLTQTOEV)
        .DoRTCalc(20,20,.t.)
          .link_1_22('Full')
        .DoRTCalc(22,23,.t.)
          .link_1_25('Full')
        .DoRTCalc(25,25,.t.)
          .link_1_27('Full')
        if .o_DPCAUVER<>.w_DPCAUVER.or. .o_DPCODODL<>.w_DPCODODL
            .w_DPUNIMIS = iif(.w_TATTIV='T','   ',.w_OLTUNMIS)
        endif
        .DoRTCalc(28,28,.t.)
        if .o_DPCODODL<>.w_DPCODODL.or. .o_DPCAUVER<>.w_DPCAUVER
            .w_DPQTAPRO = iif(.w_DPFLPBSC $ 'B-E' and .w_TATTIV $ 'Q-E',.w_QTARES,0)
        endif
        if .o_DPCODODL<>.w_DPCODODL.or. .o_DPCAUVER<>.w_DPCAUVER
            .w_DPQTASCA = 0
        endif
        .DoRTCalc(31,32,.t.)
        if .o_DPCAUVER<>.w_DPCAUVER
            .w_DPQTATEM = iif(.w_TATTIV = 'T',.w_DPQTATEM,0)
        endif
        .DoRTCalc(34,38,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_46('Full')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_49('Full')
        endif
        .DoRTCalc(42,42,.t.)
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_52('Full')
        endif
        if .o_DPCODODL<>.w_DPCODODL
          .link_1_54('Full')
        endif
        .DoRTCalc(45,45,.t.)
        if .o_DPARTPAD<>.w_DPARTPAD
            .w_DPCODLOT = SPACE(20)
          .link_1_56('Full')
        endif
        .DoRTCalc(47,48,.t.)
          .link_1_59('Full')
          .link_1_60('Full')
          .link_1_61('Full')
        .DoRTCalc(52,52,.t.)
            .w_GESMAT1 = iif(not .w_MATENABL, "N", .w_GESMAT1)
        .DoRTCalc(54,54,.t.)
        if .o_DPCODART<>.w_DPCODART
            .w_DPCODLOT = SPACE(20)
          .link_1_65('Full')
        endif
        if .o_DPCODART<>.w_DPCODART.or. .o_DPCODMAG<>.w_DPCODMAG
            .w_DPCODUBI = SPACE(20)
          .link_1_67('Full')
        endif
        .DoRTCalc(57,64,.t.)
          .link_1_78('Full')
        .DoRTCalc(66,68,.t.)
        if .o_DPQTAPRO<>.w_DPQTAPRO
            .w_DPQTAPR1 = CALQTAADV(.w_DPQTAPRO,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTAPRO")
        endif
        if .o_DPQTASCA<>.w_DPQTASCA
            .w_DPQTASC1 = CALQTAADV(.w_DPQTASCA,.w_DPUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "DPQTASCA")
        endif
            .w_QTAUM1 = .w_DPQTAPR1+.w_DPQTASC1
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .DoRTCalc(72,82,.t.)
          .link_1_114('Full')
        if .o_DPTIPVAL<>.w_DPTIPVAL
            .w_CRIVAL = iif(.w_DPTIPVAL='X','US',iif(.w_DPTIPVAL='L','CL',iif(.w_DPTIPVAL='S','CS',iif(.w_DPTIPVAL='M','CM',iif(.w_DPTIPVAL='U','UC',iif(.w_DPTIPVAL='P','UP',iif(.w_DPTIPVAL='A','UA','US')))))))
        endif
        if  .o_DPCODLOT<>.w_DPCODLOT.or. .o_DPCODUBI<>.w_DPCODUBI.or. .o_DPCODUBI<>.w_DPCODUBI
          .WriteTo_GSCO_MCO()
        endif
        .DoRTCalc(85,86,.t.)
            .w_MATENABL = g_MATR="S" and g_DATMAT<=.w_DPDATREG
        .DoRTCalc(88,88,.t.)
            .w_GESMAT = iif(not .w_MATENABL, "N", .w_GESMAT)
        .DoRTCalc(90,90,.t.)
          .link_1_126('Full')
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_132.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_133.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_135.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .DoRTCalc(92,101,.t.)
            .w_FLPRG = 'K'
        .DoRTCalc(103,108,.t.)
        if .o_DPTIPVAL<>.w_DPTIPVAL
          .link_1_158('Full')
        endif
        if .o_DPESERCZ<>.w_DPESERCZ
          .link_1_159('Full')
        endif
        .DoRTCalc(111,112,.t.)
          .link_1_162('Full')
        if .o_DPLISTIN<>.w_DPLISTIN.or. .o_DPTIPVAL<>.w_DPTIPVAL
            .w_CAOLIS = IIF(.w_DPTIPVAL='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, i_datsys, 7), .w_CAOVAL), .w_CAOLIS)
        endif
        .DoRTCalc(115,122,.t.)
          .link_1_172('Full')
        .DoRTCalc(124,130,.t.)
          .link_1_180('Full')
        .oPgFrm.Page1.oPag.oObj_1_190.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEDICPR","i_CODAZI,w_DPSERIAL")
          .op_DPSERIAL = .w_DPSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(132,139,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_132.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_133.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_135.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_190.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDPCODODL_1_15.enabled = this.oPgFrm.Page1.oPag.oDPCODODL_1_15.mCond()
    this.oPgFrm.Page1.oPag.oDPQTAPRO_1_30.enabled = this.oPgFrm.Page1.oPag.oDPQTAPRO_1_30.mCond()
    this.oPgFrm.Page1.oPag.oDPQTASCA_1_31.enabled = this.oPgFrm.Page1.oPag.oDPQTASCA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDPFLEVAS_1_32.enabled = this.oPgFrm.Page1.oPag.oDPFLEVAS_1_32.mCond()
    this.oPgFrm.Page1.oPag.oDPUMTEMP_1_33.enabled = this.oPgFrm.Page1.oPag.oDPUMTEMP_1_33.mCond()
    this.oPgFrm.Page1.oPag.oDPQTATEM_1_34.enabled = this.oPgFrm.Page1.oPag.oDPQTATEM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.enabled = this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.mCond()
    this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.enabled = this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.mCond()
    this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.enabled = this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.mCond()
    this.oPgFrm.Page1.oPag.oDPCODATT_1_54.enabled = this.oPgFrm.Page1.oPag.oDPCODATT_1_54.mCond()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.enabled = this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.mCond()
    this.oPgFrm.Page1.oPag.oDPODLFAS_1_58.enabled = this.oPgFrm.Page1.oPag.oDPODLFAS_1_58.mCond()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.enabled = this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.mCond()
    this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.enabled = this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.mCond()
    this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.enabled = this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.mCond()
    this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.enabled = this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.mCond()
    this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.enabled = this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.mCond()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_163.enabled = this.oPgFrm.Page1.oPag.oCAOLIS_1_163.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_108.enabled = this.oPgFrm.Page1.oPag.oBtn_1_108.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_115.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_115.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_117.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_117.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.visible=!this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_47.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.visible=!this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.mHide()
    this.oPgFrm.Page1.oPag.oVOCDES_1_50.visible=!this.oPgFrm.Page1.oPag.oVOCDES_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.visible=!this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oDPCODATT_1_54.visible=!this.oPgFrm.Page1.oPag.oDPCODATT_1_54.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_55.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.visible=!this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_1_57.visible=!this.oPgFrm.Page1.oPag.oDESATT_1_57.mHide()
    this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.visible=!this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_66.visible=!this.oPgFrm.Page1.oPag.oBtn_1_66.mHide()
    this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.visible=!this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_108.visible=!this.oPgFrm.Page1.oPag.oBtn_1_108.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_110.visible=!this.oPgFrm.Page1.oPag.oBtn_1_110.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_111.visible=!this.oPgFrm.Page1.oPag.oBtn_1_111.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_113.visible=!this.oPgFrm.Page1.oPag.oBtn_1_113.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_115.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_115.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_117.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_117.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_119.visible=!this.oPgFrm.Page1.oPag.oStr_1_119.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_148.visible=!this.oPgFrm.Page1.oPag.oStr_1_148.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_149.visible=!this.oPgFrm.Page1.oPag.oStr_1_149.mHide()
    this.oPgFrm.Page1.oPag.oPRDATINV_1_150.visible=!this.oPgFrm.Page1.oPag.oPRDATINV_1_150.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_151.visible=!this.oPgFrm.Page1.oPag.oStr_1_151.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_153.visible=!this.oPgFrm.Page1.oPag.oStr_1_153.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_154.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_154.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_155.visible=!this.oPgFrm.Page1.oPag.oStr_1_155.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_156.visible=!this.oPgFrm.Page1.oPag.oStr_1_156.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG1_1_157.visible=!this.oPgFrm.Page1.oPag.oDESMAG1_1_157.mHide()
    this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.visible=!this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.mHide()
    this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.visible=!this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.mHide()
    this.oPgFrm.Page1.oPag.oDPLISTIN_1_160.visible=!this.oPgFrm.Page1.oPag.oDPLISTIN_1_160.mHide()
    this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.visible=!this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.mHide()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_163.visible=!this.oPgFrm.Page1.oPag.oCAOLIS_1_163.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsci_adp
     * istanzio immediatamente i figli della seconda e terza pagina
     * perch� vengono richiamati
     IF Upper(CEVENT)='INIT'
       if Upper(this.GSCO_MCO.class)='STDLAZYCHILD'
          this.GSCO_MCO.createrealchild()
          This.oPgFrm.ActivePage=1
       Endif
     Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_99.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_100.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_103.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_131.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_132.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_133.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_135.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_138.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_190.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_lTable = "CAU_VERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2], .t., this.CAU_VERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CVFLDEFA,CVCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CVFLDEFA="+cp_ToStrODBC(this.w_READPAR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CVFLDEFA',this.w_READPAR2)
            select CVFLDEFA,CVCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR2 = NVL(_Link_.CVFLDEFA,space(1))
      this.w_CAUDEFA = NVL(_Link_.CVCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR2 = space(1)
      endif
      this.w_CAUDEFA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])+'\'+cp_ToStr(_Link_.CVFLDEFA,1)
      cp_ShowWarn(i_cKey,this.CAU_VERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPNUMOPE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPNUMOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPNUMOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_DPNUMOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_DPNUMOPE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPNUMOPE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPNUMOPE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPNUMOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCAUVER
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_lTable = "CAU_VERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2], .t., this.CAU_VERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCAUVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_ACV',True,'CAU_VERS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CVCODICE like "+cp_ToStrODBC(trim(this.w_DPCAUVER)+"%");

          i_ret=cp_SQL(i_nConn,"select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CVCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CVCODICE',trim(this.w_DPCAUVER))
          select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CVCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCAUVER)==trim(_Link_.CVCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CVDESCRI like "+cp_ToStrODBC(trim(this.w_DPCAUVER)+"%");

            i_ret=cp_SQL(i_nConn,"select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CVDESCRI like "+cp_ToStr(trim(this.w_DPCAUVER)+"%");

            select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCAUVER) and !this.bDontReportError
            deferred_cp_zoom('CAU_VERS','*','CVCODICE',cp_AbsName(oSource.parent,'oDPCAUVER_1_7'),i_cWhere,'GSCI_ACV',"Causali versamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV";
                     +" from "+i_cTable+" "+i_lTable+" where CVCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CVCODICE',oSource.xKey(1))
            select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCAUVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV";
                   +" from "+i_cTable+" "+i_lTable+" where CVCODICE="+cp_ToStrODBC(this.w_DPCAUVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CVCODICE',this.w_DPCAUVER)
            select CVCODICE,CVDESCRI,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVCAUMOU,CVUMTEMP,CVTATTIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCAUVER = NVL(_Link_.CVCODICE,space(5))
      this.w_CVDESCRI = NVL(_Link_.CVDESCRI,space(40))
      this.w_DPFLPBSC = NVL(_Link_.CVFLPBSC,space(1))
      this.w_CAUCAR = NVL(_Link_.CVCAUCAR,space(5))
      this.w_CAUSCA = NVL(_Link_.CVCAUSCA,space(5))
      this.w_CAUSER = NVL(_Link_.CVCAUSER,space(5))
      this.w_CAUMOU = NVL(_Link_.CVCAUMOU,space(5))
      this.w_DPUMTEMP = NVL(_Link_.CVUMTEMP,space(5))
      this.w_TATTIV = NVL(_Link_.CVTATTIV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCAUVER = space(5)
      endif
      this.w_CVDESCRI = space(40)
      this.w_DPFLPBSC = space(1)
      this.w_CAUCAR = space(5)
      this.w_CAUSCA = space(5)
      this.w_CAUSER = space(5)
      this.w_CAUMOU = space(5)
      this.w_DPUMTEMP = space(5)
      this.w_TATTIV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])+'\'+cp_ToStr(_Link_.CVCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_VERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCAUVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_VERS_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CVCODICE as CVCODICE107"+ ",link_1_7.CVDESCRI as CVDESCRI107"+ ",link_1_7.CVFLPBSC as CVFLPBSC107"+ ",link_1_7.CVCAUCAR as CVCAUCAR107"+ ",link_1_7.CVCAUSCA as CVCAUSCA107"+ ",link_1_7.CVCAUSER as CVCAUSER107"+ ",link_1_7.CVCAUMOU as CVCAUMOU107"+ ",link_1_7.CVUMTEMP as CVUMTEMP107"+ ",link_1_7.CVTATTIV as CVTATTIV107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on DIC_PROD.DPCAUVER=link_1_7.CVCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCAUVER=link_1_7.CVCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPMAGSCA,PPIMPLOT";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPMAGSCA,PPIMPLOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(10))
      this.w_MGSCAR = NVL(_Link_.PPMAGSCA,space(5))
      this.w_IMPLOT = NVL(_Link_.PPIMPLOT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_MGSCAR = space(5)
      this.w_IMPLOT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCAR
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUCAR)
            select TDTIPDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCAR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_CAUMGC = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCAR = space(5)
      endif
      this.w_CAUMGC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODODL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_DPCODODL)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_DPCODODL))
          select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODODL)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODODL) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oDPCODODL_1_15'),i_cWhere,'GSCO_BZA',"Elenco ODL",'GSCI_ADP.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_DPCODODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_DPCODODL)
            select OLCODODL,OLTSTATO,OLTPROVE,OLTUNMIS,OLTCODIC,OLTCOMAG,OLTQTODL,OLTQTOEV,OLTFLEVA,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTCOART,OLTQTOD1,OLTQTOE1,OLTKEYSA,OLTQTSAL,OLTCICLO,OLTSEODL,OLTFAODL,OLTCOPOI,OLTULFAS,OLTFAOUT,OLTSECPR,OLTSOSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODODL = NVL(_Link_.OLCODODL,space(15))
      this.w_OLTSTATO = NVL(_Link_.OLTSTATO,space(1))
      this.w_OLTPROVE = NVL(_Link_.OLTPROVE,space(1))
      this.w_OLTUNMIS = NVL(_Link_.OLTUNMIS,space(3))
      this.w_DPCODICE = NVL(_Link_.OLTCODIC,space(20))
      this.w_DPCODMAG = NVL(_Link_.OLTCOMAG,space(5))
      this.w_OLTQTODL = NVL(_Link_.OLTQTODL,0)
      this.w_OLTQTOEV = NVL(_Link_.OLTQTOEV,0)
      this.w_OLTFLEVA = NVL(_Link_.OLTFLEVA,space(1))
      this.w_DPCODCEN = NVL(_Link_.OLTCOCEN,space(15))
      this.w_DPCODVOC = NVL(_Link_.OLTVOCEN,space(15))
      this.w_DPCODCOM = NVL(_Link_.OLTCOMME,space(15))
      this.w_DPCODATT = NVL(_Link_.OLTCOATT,space(15))
      this.w_DPCODART = NVL(_Link_.OLTCOART,space(20))
      this.w_OLTQTOD1 = NVL(_Link_.OLTQTOD1,0)
      this.w_OLTQTOE1 = NVL(_Link_.OLTQTOE1,0)
      this.w_OLTKEYSA = NVL(_Link_.OLTKEYSA,space(20))
      this.w_OLTQTSAL = NVL(_Link_.OLTQTSAL,0)
      this.w_OLTCICLO = NVL(_Link_.OLTCICLO,space(15))
      this.w_DPODLFAS = NVL(_Link_.OLTSEODL,space(15))
      this.w_DPCPRFAS = NVL(_Link_.OLTFAODL,0)
      this.w_COUPOI = NVL(_Link_.OLTCOPOI,space(1))
      this.w_DPULTFAS = NVL(_Link_.OLTULFAS,space(1))
      this.w_DPFASOUT = NVL(_Link_.OLTFAOUT,space(1))
      this.w_OLTSECPR = NVL(_Link_.OLTSECPR,space(15))
      this.w_ODLSOSPE = NVL(_Link_.OLTSOSPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODODL = space(15)
      endif
      this.w_OLTSTATO = space(1)
      this.w_OLTPROVE = space(1)
      this.w_OLTUNMIS = space(3)
      this.w_DPCODICE = space(20)
      this.w_DPCODMAG = space(5)
      this.w_OLTQTODL = 0
      this.w_OLTQTOEV = 0
      this.w_OLTFLEVA = space(1)
      this.w_DPCODCEN = space(15)
      this.w_DPCODVOC = space(15)
      this.w_DPCODCOM = space(15)
      this.w_DPCODATT = space(15)
      this.w_DPCODART = space(20)
      this.w_OLTQTOD1 = 0
      this.w_OLTQTOE1 = 0
      this.w_OLTKEYSA = space(20)
      this.w_OLTQTSAL = 0
      this.w_OLTCICLO = space(15)
      this.w_DPODLFAS = space(15)
      this.w_DPCPRFAS = 0
      this.w_COUPOI = space(1)
      this.w_DPULTFAS = space(1)
      this.w_DPFASOUT = space(1)
      this.w_OLTSECPR = space(15)
      this.w_ODLSOSPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLSOSPE<>'S' and (.w_STATOGES<>'LOAD' OR (.w_OLTSTATO='L' AND .w_OLTPROVE='I' AND .w_OLTFLEVA<>'S' AND .w_OLTQTODL>.w_OLTQTOEV) and not empty(.w_DPODLFAS) and nvl(.w_COUPOI,'')='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti")
        endif
        this.w_DPCODODL = space(15)
        this.w_OLTSTATO = space(1)
        this.w_OLTPROVE = space(1)
        this.w_OLTUNMIS = space(3)
        this.w_DPCODICE = space(20)
        this.w_DPCODMAG = space(5)
        this.w_OLTQTODL = 0
        this.w_OLTQTOEV = 0
        this.w_OLTFLEVA = space(1)
        this.w_DPCODCEN = space(15)
        this.w_DPCODVOC = space(15)
        this.w_DPCODCOM = space(15)
        this.w_DPCODATT = space(15)
        this.w_DPCODART = space(20)
        this.w_OLTQTOD1 = 0
        this.w_OLTQTOE1 = 0
        this.w_OLTKEYSA = space(20)
        this.w_OLTQTSAL = 0
        this.w_OLTCICLO = space(15)
        this.w_DPODLFAS = space(15)
        this.w_DPCPRFAS = 0
        this.w_COUPOI = space(1)
        this.w_DPULTFAS = space(1)
        this.w_DPFASOUT = space(1)
        this.w_OLTSECPR = space(15)
        this.w_ODLSOSPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODART
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARFLLOTT,ARGESMAT,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DPCODART)
            select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARFLLOTT,ARGESMAT,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_FLLOTT = space(1)
      this.w_GESMAT = space(1)
      this.w_FLUSEP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.ARCODART as ARCODART122"+ ",link_1_22.ARUNMIS1 as ARUNMIS1122"+ ",link_1_22.AROPERAT as AROPERAT122"+ ",link_1_22.ARMOLTIP as ARMOLTIP122"+ ",link_1_22.ARUNMIS2 as ARUNMIS2122"+ ",link_1_22.ARFLLOTT as ARFLLOTT122"+ ",link_1_22.ARGESMAT as ARGESMAT122"+ ",link_1_22.ARFLUSEP as ARFLUSEP122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on DIC_PROD.DPCODART=link_1_22.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODART=link_1_22.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODMAG
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DPCODMAG)
            select MGCODMAG,MGDESMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.MGCODMAG as MGCODMAG125"+ ",link_1_25.MGDESMAG as MGDESMAG125"+ ",link_1_25.MGFLUBIC as MGFLUBIC125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on DIC_PROD.DPCODMAG=link_1_25.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODMAG=link_1_25.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODICE
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DPCODICE)
            select CACODICE,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DPCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.CACODICE as CACODICE127"+ ",link_1_27.CADESART as CADESART127"+ ",link_1_27.CAUNIMIS as CAUNIMIS127"+ ",link_1_27.CAOPERAT as CAOPERAT127"+ ",link_1_27.CAMOLTIP as CAMOLTIP127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on DIC_PROD.DPCODICE=link_1_27.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODICE=link_1_27.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPUMTEMP
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPUMTEMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DPUMTEMP)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_DPUMTEMP))
          select UMFLTEMP,UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPUMTEMP)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPUMTEMP) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oDPUMTEMP_1_33'),i_cWhere,'GSAR_AUM',"UM tempi",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPUMTEMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DPUMTEMP);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_DPUMTEMP)
            select UMFLTEMP,UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPUMTEMP = NVL(_Link_.UMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DPUMTEMP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPUMTEMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODCEN
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DPCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_DPCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_DPCODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDPCODCEN_1_46'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DPCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DPCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCON = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCEN = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_46(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_46.CC_CONTO as CC_CONTO146"+ ",link_1_46.CCDESPIA as CCDESPIA146"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_46 on DIC_PROD.DPCODCEN=link_1_46.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_46"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODCEN=link_1_46.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODVOC
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DPCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DPCODVOC))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDPCODVOC_1_49'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DPCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DPCODVOC)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_VOCDES = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_DTOBVO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODVOC = space(15)
      endif
      this.w_VOCDES = space(40)
      this.w_TIPVOC = space(1)
      this.w_DTOBVO = ctod("  /  /  ")
      this.w_CODCOS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_DPCODVOC = space(15)
        this.w_VOCDES = space(40)
        this.w_TIPVOC = space(1)
        this.w_DTOBVO = ctod("  /  /  ")
        this.w_CODCOS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_49(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_49.VCCODICE as VCCODICE149"+ ",link_1_49.VCDESCRI as VCDESCRI149"+ ",link_1_49.VCTIPVOC as VCTIPVOC149"+ ",link_1_49.VCDTOBSO as VCDTOBSO149"+ ",link_1_49.VCTIPCOS as VCTIPCOS149"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_49 on DIC_PROD.DPCODVOC=link_1_49.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_49"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODVOC=link_1_49.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODCOM
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DPCODCOM))
          select CNCODCAN,CNDTOBSO,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDPCODCOM_1_52'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DPCODCOM)
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODCOM = space(15)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DESCAN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_DPCODCOM = space(15)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DESCAN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_52(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_52.CNCODCAN as CNCODCAN152"+ ",link_1_52.CNDTOBSO as CNDTOBSO152"+ ",link_1_52.CNDESCAN as CNDESCAN152"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_52 on DIC_PROD.DPCODCOM=link_1_52.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_52"
          i_cKey=i_cKey+'+" and DIC_PROD.DPCODCOM=link_1_52.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODATT
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DPCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DPCODCOM;
                     ,'ATTIPATT',this.w_DPTIPATT;
                     ,'ATCODATT',trim(this.w_DPCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDPCODATT_1_54'),i_cWhere,'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODCOM<>oSource.xKey(1);
           .or. this.w_DPTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DPCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DPCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_DPTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DPCODCOM;
                       ,'ATTIPATT',this.w_DPTIPATT;
                       ,'ATCODATT',this.w_DPCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_DPCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_DPCODCOM))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        endif
        this.w_DPCODATT = space(15)
        this.w_DESATT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODLOT
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_DPCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPARTPAD);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_DPARTPAD;
                     ,'LOCODICE',trim(this.w_DPCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oDPCODLOT_1_56'),i_cWhere,'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPARTPAD<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_DPARTPAD);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_DPCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPARTPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_DPARTPAD;
                       ,'LOCODICE',this.w_DPCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DPCODLOT) OR (iif(.w_DPULTFAS='S',.w_ARTLOT=.w_DPARTPAD,.w_ARTLOT=.w_DPCODART) AND .w_FLSTAT<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        endif
        this.w_DPCODLOT = space(20)
        this.w_ARTLOT = space(20)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPODLFAS
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPODLFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_DPODLFAS)+"%");
                   +" and OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL,OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',this.w_DPODLFAS;
                     ,'OLCODODL',trim(this.w_DPODLFAS))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL,OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPODLFAS)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPODLFAS) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL,OLCODODL',cp_AbsName(oSource.parent,'oDPODLFAS_1_58'),i_cWhere,'GSCO_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPODLFAS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1);
                       ,'OLCODODL',oSource.xKey(2))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPODLFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                   +" and OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_DPODLFAS;
                       ,'OLCODODL',this.w_DPODLFAS)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPODLFAS = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DPODLFAS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPODLFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPODLFAS
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPODLFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPODLFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL,OLTCODIC,OLTCOART,OLTKEYSA";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                   +" and OLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_DPODLFAS;
                       ,'OLCODODL',this.w_DPODLFAS)
            select OLCODODL,OLTCODIC,OLTCOART,OLTKEYSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPODLFAS = NVL(_Link_.OLCODODL,space(15))
      this.w_DPCODPAD = NVL(_Link_.OLTCODIC,space(20))
      this.w_DPARTPAD = NVL(_Link_.OLTCOART,space(20))
      this.w_OLTKEYSA1 = NVL(_Link_.OLTKEYSA,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPODLFAS = space(15)
      endif
      this.w_DPCODPAD = space(20)
      this.w_DPARTPAD = space(20)
      this.w_OLTKEYSA1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPODLFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODPAD
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DPCODPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DPCODPAD)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODPAD = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODPAD = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPARTPAD
  func Link_1_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPARTPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPARTPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARFLLOTT,ARGESMAT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DPARTPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DPARTPAD)
            select ARCODART,ARFLLOTT,ARGESMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPARTPAD = NVL(_Link_.ARCODART,space(20))
      this.w_FLLOTT1 = NVL(_Link_.ARFLLOTT,space(1))
      this.w_GESMAT1 = NVL(_Link_.ARGESMAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPARTPAD = space(20)
      endif
      this.w_FLLOTT1 = space(1)
      this.w_GESMAT1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPARTPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_61(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_61.ARCODART as ARCODART161"+ ",link_1_61.ARFLLOTT as ARFLLOTT161"+ ",link_1_61.ARGESMAT as ARGESMAT161"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_61 on DIC_PROD.DPARTPAD=link_1_61.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_61"
          i_cKey=i_cKey+'+" and DIC_PROD.DPARTPAD=link_1_61.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DPCODLOT
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_DPCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_DPCODART;
                     ,'LOCODICE',trim(this.w_DPCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oDPCODLOT_1_65'),i_cWhere,'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_DPCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_DPCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_DPCODART;
                       ,'LOCODICE',this.w_DPCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DPCODLOT) OR (iif(.w_DPULTFAS='S',.w_ARTLOT=.w_DPARTPAD,.w_ARTLOT=.w_DPCODART) AND .w_FLSTAT<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        endif
        this.w_DPCODLOT = space(20)
        this.w_ARTLOT = space(20)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPCODUBI
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_DPCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_DPCODMAG;
                     ,'UBCODICE',trim(this.w_DPCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oDPCODUBI_1_67'),i_cWhere,'GSMD_MUB',"Ubicazioni",'GSCO_ADP.UBICAZIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_DPCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DPCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_DPCODMAG;
                       ,'UBCODICE',this.w_DPCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DPCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MGSCAR
  func Link_1_114(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MGSCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MGSCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MGSCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MGSCAR)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MGSCAR = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC1 = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MGSCAR = space(5)
      endif
      this.w_FLUBIC1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MGSCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMGC
  func Link_1_126(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMGC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMGC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMMTCARI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUMGC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUMGC)
            select CMCODICE,CMMTCARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMGC = NVL(_Link_.CMCODICE,space(5))
      this.w_MTCARI = NVL(_Link_.CMMTCARI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMGC = space(5)
      endif
      this.w_MTCARI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMGC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPESERCZ
  func Link_1_158(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPESERCZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_DPESERCZ)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_DPESERCZ))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPESERCZ)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPESERCZ) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oDPESERCZ_1_158'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPESERCZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_DPESERCZ);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_DPESERCZ)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPESERCZ = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
      this.w_ESFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DPESERCZ = space(4)
      endif
      this.w_VALESE = space(3)
      this.w_ESFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPESERCZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPNUMINV
  func Link_1_159(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPNUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_DPNUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_DPESERCZ);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_DPESERCZ;
                     ,'INNUMINV',trim(this.w_DPNUMINV))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPNUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPNUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oDPNUMINV_1_159'),i_cWhere,'',"",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DPESERCZ<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_DPESERCZ);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPNUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_DPNUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_DPESERCZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_DPESERCZ;
                       ,'INNUMINV',this.w_DPNUMINV)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPNUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DPNUMINV = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_PRDATINV = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPNUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPLISTIN
  func Link_1_160(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPLISTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DPLISTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DPLISTIN))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPLISTIN)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DPLISTIN) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDPLISTIN_1_160'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPLISTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DPLISTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DPLISTIN)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPLISTIN = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DPLISTIN = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPLISTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPMAGCOD
  func Link_1_161(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPMAGCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DPMAGCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DPMAGCOD))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DPMAGCOD)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_DPMAGCOD)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_DPMAGCOD)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DPMAGCOD) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDPMAGCOD_1_161'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPMAGCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DPMAGCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DPMAGCOD)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPMAGCOD = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG1 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DPMAGCOD = space(5)
      endif
      this.w_DESMAG1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPMAGCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_161(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_161.MGCODMAG as MGCODMAG261"+ ",link_1_161.MGDESMAG as MGDESMAG261"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_161 on DIC_PROD.DPMAGCOD=link_1_161.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_161"
          i_cKey=i_cKey+'+" and DIC_PROD.DPMAGCOD=link_1_161.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_162(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_1_172(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DPODLFAS
  func Link_1_180(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_lTable = "ODL_CICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2], .t., this.ODL_CICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DPODLFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DPODLFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPROWNUM,CLCODODL,CLROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where CLCODODL="+cp_ToStrODBC(this.w_DPODLFAS);
                   +" and CPROWNUM="+cp_ToStrODBC(this.w_DPCPRFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPROWNUM',this.w_DPCPRFAS;
                       ,'CLCODODL',this.w_DPODLFAS)
            select CPROWNUM,CLCODODL,CLROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DPODLFAS = NVL(_Link_.CLCODODL,space(15))
      this.w_FASE = NVL(_Link_.CLROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_DPODLFAS = space(15)
      endif
      this.w_FASE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])+'\'+cp_ToStr(_Link_.CPROWNUM,1)+'\'+cp_ToStr(_Link_.CLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_CICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DPODLFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDPSERIAL_1_1.value==this.w_DPSERIAL)
      this.oPgFrm.Page1.oPag.oDPSERIAL_1_1.value=this.w_DPSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDATREG_1_5.value==this.w_DPDATREG)
      this.oPgFrm.Page1.oPag.oDPDATREG_1_5.value=this.w_DPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDPNUMOPE_1_6.value==this.w_DPNUMOPE)
      this.oPgFrm.Page1.oPag.oDPNUMOPE_1_6.value=this.w_DPNUMOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCAUVER_1_7.value==this.w_DPCAUVER)
      this.oPgFrm.Page1.oPag.oDPCAUVER_1_7.value=this.w_DPCAUVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFLPBSC_1_8.RadioValue()==this.w_DPFLPBSC)
      this.oPgFrm.Page1.oPag.oDPFLPBSC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODODL_1_15.value==this.w_DPCODODL)
      this.oPgFrm.Page1.oPag.oDPCODODL_1_15.value=this.w_DPCODODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTODL_1_18.value==this.w_OLTQTODL)
      this.oPgFrm.Page1.oPag.oOLTQTODL_1_18.value=this.w_OLTQTODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTOEV_1_19.value==this.w_OLTQTOEV)
      this.oPgFrm.Page1.oPag.oOLTQTOEV_1_19.value=this.w_OLTQTOEV
    endif
    if not(this.oPgFrm.Page1.oPag.oQTARES_1_20.value==this.w_QTARES)
      this.oPgFrm.Page1.oPag.oQTARES_1_20.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTUNMIS_1_24.value==this.w_OLTUNMIS)
      this.oPgFrm.Page1.oPag.oOLTUNMIS_1_24.value=this.w_OLTUNMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODMAG_1_25.value==this.w_DPCODMAG)
      this.oPgFrm.Page1.oPag.oDPCODMAG_1_25.value=this.w_DPCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODICE_1_27.value==this.w_DPCODICE)
      this.oPgFrm.Page1.oPag.oDPCODICE_1_27.value=this.w_DPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPUNIMIS_1_28.value==this.w_DPUNIMIS)
      this.oPgFrm.Page1.oPag.oDPUNIMIS_1_28.value=this.w_DPUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPQTAPRO_1_30.value==this.w_DPQTAPRO)
      this.oPgFrm.Page1.oPag.oDPQTAPRO_1_30.value=this.w_DPQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDPQTASCA_1_31.value==this.w_DPQTASCA)
      this.oPgFrm.Page1.oPag.oDPQTASCA_1_31.value=this.w_DPQTASCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFLEVAS_1_32.RadioValue()==this.w_DPFLEVAS)
      this.oPgFrm.Page1.oPag.oDPFLEVAS_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPUMTEMP_1_33.value==this.w_DPUMTEMP)
      this.oPgFrm.Page1.oPag.oDPUMTEMP_1_33.value=this.w_DPUMTEMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDPQTATEM_1_34.value==this.w_DPQTATEM)
      this.oPgFrm.Page1.oPag.oDPQTATEM_1_34.value=this.w_DPQTATEM
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADOC_1_39.RadioValue()==this.w_STADOC)
      this.oPgFrm.Page1.oPag.oSTADOC_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.value==this.w_DPCODCEN)
      this.oPgFrm.Page1.oPag.oDPCODCEN_1_46.value=this.w_DPCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_47.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_47.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.value==this.w_DPCODVOC)
      this.oPgFrm.Page1.oPag.oDPCODVOC_1_49.value=this.w_DPCODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCDES_1_50.value==this.w_VOCDES)
      this.oPgFrm.Page1.oPag.oVOCDES_1_50.value=this.w_VOCDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.value==this.w_DPCODCOM)
      this.oPgFrm.Page1.oPag.oDPCODCOM_1_52.value=this.w_DPCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODATT_1_54.value==this.w_DPCODATT)
      this.oPgFrm.Page1.oPag.oDPCODATT_1_54.value=this.w_DPCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_55.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_55.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.value==this.w_DPCODLOT)
      this.oPgFrm.Page1.oPag.oDPCODLOT_1_56.value=this.w_DPCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_57.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_57.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDPODLFAS_1_58.value==this.w_DPODLFAS)
      this.oPgFrm.Page1.oPag.oDPODLFAS_1_58.value=this.w_DPODLFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDPULTFAS_1_64.RadioValue()==this.w_DPULTFAS)
      this.oPgFrm.Page1.oPag.oDPULTFAS_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.value==this.w_DPCODLOT)
      this.oPgFrm.Page1.oPag.oDPCODLOT_1_65.value=this.w_DPCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.value==this.w_DPCODUBI)
      this.oPgFrm.Page1.oPag.oDPCODUBI_1_67.value=this.w_DPCODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_71.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_71.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_74.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_74.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_95.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_95.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCVDESCRI_1_142.value==this.w_CVDESCRI)
      this.oPgFrm.Page1.oPag.oCVDESCRI_1_142.value=this.w_CVDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDPCRIVAL_1_146.RadioValue()==this.w_DPCRIVAL)
      this.oPgFrm.Page1.oPag.oDPCRIVAL_1_146.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_150.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_150.value=this.w_PRDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDPTIPVAL_1_152.RadioValue()==this.w_DPTIPVAL)
      this.oPgFrm.Page1.oPag.oDPTIPVAL_1_152.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_154.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_154.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG1_1_157.value==this.w_DESMAG1)
      this.oPgFrm.Page1.oPag.oDESMAG1_1_157.value=this.w_DESMAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.value==this.w_DPESERCZ)
      this.oPgFrm.Page1.oPag.oDPESERCZ_1_158.value=this.w_DPESERCZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.value==this.w_DPNUMINV)
      this.oPgFrm.Page1.oPag.oDPNUMINV_1_159.value=this.w_DPNUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDPLISTIN_1_160.value==this.w_DPLISTIN)
      this.oPgFrm.Page1.oPag.oDPLISTIN_1_160.value=this.w_DPLISTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.value==this.w_DPMAGCOD)
      this.oPgFrm.Page1.oPag.oDPMAGCOD_1_161.value=this.w_DPMAGCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLIS_1_163.value==this.w_CAOLIS)
      this.oPgFrm.Page1.oPag.oCAOLIS_1_163.value=this.w_CAOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFASE_1_181.value==this.w_FASE)
      this.oPgFrm.Page1.oPag.oFASE_1_181.value=this.w_FASE
    endif
    if not(this.oPgFrm.Page1.oPag.oDPFASOUT_1_184.RadioValue()==this.w_DPFASOUT)
      this.oPgFrm.Page1.oPag.oDPFASOUT_1_184.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSECPR_1_188.value==this.w_OLTSECPR)
      this.oPgFrm.Page1.oPag.oOLTSECPR_1_188.value=this.w_OLTSECPR
    endif
    cp_SetControlsValueExtFlds(this,'DIC_PROD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DPSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPSERIAL_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DPSERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DPDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPDATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DPCAUVER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCAUVER_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DPCAUVER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLSOSPE<>'S' and (.w_STATOGES<>'LOAD' OR (.w_OLTSTATO='L' AND .w_OLTPROVE='I' AND .w_OLTFLEVA<>'S' AND .w_OLTQTODL>.w_OLTQTOEV) and not empty(.w_DPODLFAS) and nvl(.w_COUPOI,'')='S'))  and (upper(.cFunction)<>'EDIT')  and not(empty(.w_DPCODODL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODODL_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti")
          case   not(.w_DPQTATEM>=0)  and (not empty(nvl(.w_DPUMTEMP,"")) and .w_TATTIV = 'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPQTATEM_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST))  and not(g_PERCCR<>'S')  and (g_PERCCR='S')  and not(empty(.w_DPCODVOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODVOC_1_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(g_PERCAN<>'S' and g_COMM<>'S' and g_COAN<>'S')  and (g_PERCAN='S' or g_COMM='S')  and not(empty(.w_DPCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODCOM_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(NOT EMPTY(.w_DPCODATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_DPCODCOM)))  and not(g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))  and ((g_PERCAN='S' or g_COMM='S')  AND NOT EMPTY(.w_DPCODCOM))  and not(empty(.w_DPCODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODATT_1_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� inesistente o incongruente")
          case   not(EMPTY(.w_DPCODLOT) OR (iif(.w_DPULTFAS='S',.w_ARTLOT=.w_DPARTPAD,.w_ARTLOT=.w_DPCODART) AND .w_FLSTAT<>'S'))  and not(g_PERLOT<>'S' or .w_DPULTFAS<>'S')  and (g_PERLOT='S' AND iif(.w_DPULTFAS='S',.w_FLLOTT1$ 'SC',.w_FLLOTT$ 'SC'))  and not(empty(.w_DPCODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODLOT_1_56.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
          case   not(EMPTY(.w_DPCODLOT) OR (iif(.w_DPULTFAS='S',.w_ARTLOT=.w_DPARTPAD,.w_ARTLOT=.w_DPCODART) AND .w_FLSTAT<>'S'))  and not(g_PERLOT<>'S' or .w_DPULTFAS='S')  and (g_PERLOT='S' AND iif(.w_DPULTFAS='S',.w_FLLOTT1$ 'SC',.w_FLLOTT$ 'SC'))  and not(empty(.w_DPCODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPCODLOT_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente o sospeso")
          case   (empty(.w_DPESERCZ))  and not((! .w_DPTIPVAL $ "S-M-U-P"))  and (.w_DPTIPVAL $ "S-M-U-P-A")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPESERCZ_1_158.SetFocus()
            i_bnoObbl = !empty(.w_DPESERCZ)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DPNUMINV))  and not(! .w_DPTIPVAL $ "S-M-U-P")  and (.w_DPTIPVAL $ 'SMUP' AND NOT EMPTY(.w_DPESERCZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDPNUMINV_1_159.SetFocus()
            i_bnoObbl = !empty(.w_DPNUMINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOLIS))  and not(.w_CAOVAL<>0 OR .w_DPTIPVAL<>"L" OR EMPTY(.w_DPLISTIN))  and (.w_CAOVAL=0 AND .w_DPTIPVAL="L" AND NOT EMPTY(.w_DPLISTIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLIS_1_163.SetFocus()
            i_bnoObbl = !empty(.w_CAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCO_MMP.CheckForm()
      if i_bres
        i_bres=  .GSCO_MMP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCI_MRP.CheckForm()
      if i_bres
        i_bres=  .GSCI_MRP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MMT.CheckForm()
      if i_bres
        i_bres=  .GSCO_MMT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MCO.CheckForm()
      if i_bres
        i_bres=  .GSCO_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MOU.CheckForm()
      if i_bres
        i_bres=  .GSCO_MOU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsci_adp
      * Prima esegue i controlli finali
      .w_TESTFORM=.T.
      if i_bRes
         .NotifyEvent('Controlli Finali')
         i_bRes = .w_TESTFORM
      endif
      if i_bRes and .w_FLLOTT$'SC' and EMPTY(.w_DPCODLOT)
        if .w_IMPLOT = 'S'
           i_bRes = .f.
           i_bnoChk = .f.	
           i_cErrorMsg = ah_MsgFormat("Non � stato inserito il dettaglio lotti/ubicazioni. %0Impossibile confermare")
        else
           ah_ErrorMsg("Non � stato inserito il dettaglio lotti/ubicazioni")
        endif
      endif
      
      if i_bres
        if alltrim(str(year(.w_DPDATREG)))<>g_CODESE
          i_bres = ah_yesno("Il documento verr� generato con data esterna all'esercizio di competenza. %0Si desidera proseguire?")
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DPCAUVER = this.w_DPCAUVER
    this.o_DPCODODL = this.w_DPCODODL
    this.o_DPCODART = this.w_DPCODART
    this.o_DPCODMAG = this.w_DPCODMAG
    this.o_DPQTAPRO = this.w_DPQTAPRO
    this.o_DPQTASCA = this.w_DPQTASCA
    this.o_DPCODLOT = this.w_DPCODLOT
    this.o_DPARTPAD = this.w_DPARTPAD
    this.o_DPCODUBI = this.w_DPCODUBI
    this.o_DPTIPVAL = this.w_DPTIPVAL
    this.o_DPESERCZ = this.w_DPESERCZ
    this.o_DPLISTIN = this.w_DPLISTIN
    * --- GSCO_MMP : Depends On
    this.GSCO_MMP.SaveDependsOn()
    * --- GSCI_MRP : Depends On
    this.GSCI_MRP.SaveDependsOn()
    * --- GSCO_MMT : Depends On
    this.GSCO_MMT.SaveDependsOn()
    * --- GSCO_MCO : Depends On
    this.GSCO_MCO.SaveDependsOn()
    * --- GSCO_MOU : Depends On
    this.GSCO_MOU.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsci_adpPag1 as StdContainer
  Width  = 723
  height = 501
  stdWidth  = 723
  stdheight = 501
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDPSERIAL_1_1 as StdField with uid="JYHDWBXNBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DPSERIAL", cQueryName = "DPSERIAL",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo dichiarazione",;
    HelpContextID = 119505022,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=100, Top=11, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oDPDATREG_1_5 as StdField with uid="HEPMYBLWIJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DPDATREG", cQueryName = "DPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 33263485,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=240, Top=11

  add object oDPNUMOPE_1_6 as StdField with uid="BPWYKEWEZG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DPNUMOPE", cQueryName = "DPNUMOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'operatore",;
    HelpContextID = 23056517,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=439, Top=11, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_DPNUMOPE"

  func oDPNUMOPE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDPCAUVER_1_7 as StdField with uid="TXVVMRWCUC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DPCAUVER", cQueryName = "DPCAUVER",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di versamento",;
    HelpContextID = 101416840,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=100, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_VERS", cZoomOnZoom="GSCI_ACV", oKey_1_1="CVCODICE", oKey_1_2="this.w_DPCAUVER"

  func oDPCAUVER_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCAUVER_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCAUVER_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_VERS','*','CVCODICE',cp_AbsName(this.parent,'oDPCAUVER_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_ACV',"Causali versamento",'',this.parent.oContained
  endproc
  proc oDPCAUVER_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCI_ACV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CVCODICE=this.parent.oContained.w_DPCAUVER
     i_obj.ecpSave()
  endproc


  add object oDPFLPBSC_1_8 as StdCombo with uid="QSNTINDTYR",rtseq=8,rtrep=.f.,left=525,top=37,width=111,height=21, enabled=.f.;
    , ToolTipText = "Pezzi buoni/scarti";
    , HelpContextID = 238637191;
    , cFormVar="w_DPFLPBSC",RowSource=""+"Nessus vers.,"+"Versamento,"+"Scarto,"+"Vers. e scarto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDPFLPBSC_1_8.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"B",;
    iif(this.value =3,"S",;
    iif(this.value =4,"E",;
    space(1))))))
  endfunc
  func oDPFLPBSC_1_8.GetRadio()
    this.Parent.oContained.w_DPFLPBSC = this.RadioValue()
    return .t.
  endfunc

  func oDPFLPBSC_1_8.SetRadio()
    this.Parent.oContained.w_DPFLPBSC=trim(this.Parent.oContained.w_DPFLPBSC)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLPBSC=="N",1,;
      iif(this.Parent.oContained.w_DPFLPBSC=="B",2,;
      iif(this.Parent.oContained.w_DPFLPBSC=="S",3,;
      iif(this.Parent.oContained.w_DPFLPBSC=="E",4,;
      0))))
  endfunc

  add object oDPCODODL_1_15 as StdField with uid="CLDBIJETVB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DPCODODL", cQueryName = "DPCODODL",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "ODL non valido, sospeso o dichiarato in contemporanea da pi� utenti",;
    ToolTipText = "Codice ODL di fase",;
    HelpContextID = 32931966,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=100, Top=64, cSayPict='"999999999999999"', cGetPict='"999999999999999"', InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_DPCODODL"

  func oDPCODODL_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(.cFunction)<>'EDIT')
    endwith
   endif
  endfunc

  func oDPCODODL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODODL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODODL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oDPCODODL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"Elenco ODL",'GSCI_ADP.ODL_MAST_VZM',this.parent.oContained
  endproc
  proc oDPCODODL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_DPCODODL
     i_obj.ecpSave()
  endproc

  add object oOLTQTODL_1_18 as StdField with uid="NWZEJLGNGL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_OLTQTODL", cQueryName = "OLTQTODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� ordinata",;
    HelpContextID = 15954894,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=235, Top=225, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oOLTQTOEV_1_19 as StdField with uid="RYNLYEQOZP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OLTQTOEV", cQueryName = "OLTQTOEV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� evasa",;
    HelpContextID = 252480572,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=402, Top=225, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oQTARES_1_20 as StdField with uid="UVAHAVZKUM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� residua",;
    HelpContextID = 233020410,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=576, Top=225, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oOLTUNMIS_1_24 as StdField with uid="ZCWUZUZJOF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_OLTUNMIS", cQueryName = "OLTUNMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura ODL",;
    HelpContextID = 212896825,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=109, Top=225, InputMask=replicate('X',3)

  add object oDPCODMAG_1_25 as StdField with uid="QQUMLUETCH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DPCODMAG", cQueryName = "DPCODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 66486403,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=199, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DPCODMAG"

  func oDPCODMAG_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DPCODUBI)
        bRes2=.link_1_67('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDPCODICE_1_27 as StdField with uid="ICPCACFDPB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DPCODICE", cQueryName = "DPCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo associato all'ODL",;
    HelpContextID = 133595269,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=173, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DPCODICE"

  func oDPCODICE_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDPUNIMIS_1_28 as StdField with uid="SNMPAXFHOA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DPUNIMIS", cQueryName = "DPUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura ODL",;
    HelpContextID = 207200137,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=100, Top=90, InputMask=replicate('X',3)

  add object oDPQTAPRO_1_30 as StdField with uid="BRPROJYBUN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DPQTAPRO", cQueryName = "DPQTAPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� da produrre",;
    HelpContextID = 18915451,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=240, Top=90, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oDPQTAPRO_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPFLPBSC $ 'B-E' and .w_TATTIV $ 'Q-E')
    endwith
   endif
  endfunc

  add object oDPQTASCA_1_31 as StdField with uid="HHKIPKDNNC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DPQTASCA", cQueryName = "DPQTASCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale quantit� scartata",;
    HelpContextID = 237019273,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=456, Top=90, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oDPQTASCA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPFLPBSC $ 'S-E' and .w_TATTIV $ 'Q-E')
    endwith
   endif
  endfunc

  add object oDPFLEVAS_1_32 as StdCheck with uid="AFRCDQEPBT",rtseq=31,rtrep=.f.,left=558, top=91, caption="Fase completata",;
    ToolTipText = "Se attivo: fase completata",;
    HelpContextID = 183062647,;
    cFormVar="w_DPFLEVAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDPFLEVAS_1_32.RadioValue()
    return(iif(this.value =1,"S",;
    ' '))
  endfunc
  func oDPFLEVAS_1_32.GetRadio()
    this.Parent.oContained.w_DPFLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oDPFLEVAS_1_32.SetRadio()
    this.Parent.oContained.w_DPFLEVAS=trim(this.Parent.oContained.w_DPFLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_DPFLEVAS=="S",1,;
      0)
  endfunc

  func oDPFLEVAS_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oDPUMTEMP_1_33 as StdField with uid="TSVRBKLSFU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DPUMTEMP", cQueryName = "DPUMTEMP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura tempo",;
    HelpContextID = 183984250,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=100, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_DPUMTEMP"

  func oDPUMTEMP_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TATTIV = 'T')
    endwith
   endif
  endfunc

  func oDPUMTEMP_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPUMTEMP_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPUMTEMP_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oDPUMTEMP_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"UM tempi",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oDPUMTEMP_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_DPUMTEMP
     i_obj.ecpSave()
  endproc

  add object oDPQTATEM_1_34 as StdField with uid="HOQRTLLSPT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DPQTATEM", cQueryName = "DPQTATEM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Tempo della fase da consuntivare",;
    HelpContextID = 48193411,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=240, Top=118, cSayPict='"@Z 999999999.999"', cGetPict='"999999999.999"'

  func oDPQTATEM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(nvl(.w_DPUMTEMP,"")) and .w_TATTIV = 'T')
    endwith
   endif
  endfunc

  func oDPQTATEM_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DPQTATEM>=0)
    endwith
    return bRes
  endfunc

  add object oSTADOC_1_39 as StdCheck with uid="JNBDANBLVK",rtseq=38,rtrep=.f.,left=350, top=118, caption="Stampa immediata",;
    ToolTipText = "Se attivo: stampa immediata dei documenti generati",;
    HelpContextID = 223452122,;
    cFormVar="w_STADOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADOC_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADOC_1_39.GetRadio()
    this.Parent.oContained.w_STADOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADOC_1_39.SetRadio()
    this.Parent.oContained.w_STADOC=trim(this.Parent.oContained.w_STADOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADOC=='S',1,;
      0)
  endfunc

  add object oDPCODCEN_1_46 as StdField with uid="SMAGWUUUCR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DPCODCEN", cQueryName = "DPCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo associato",;
    HelpContextID = 34176900,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=285, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DPCODCEN"

  func oDPCODCEN_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oDPCODCEN_1_46.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oDPCODCEN_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODCEN_1_46.ecpDrop(oSource)
    this.Parent.oContained.link_1_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCEN_1_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDPCODCEN_1_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oDPCODCEN_1_46.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DPCODCEN
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_47 as StdField with uid="ETZQFDWCRD",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38898634,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=285, InputMask=replicate('X',40)

  func oDESCON_1_47.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDPCODVOC_1_49 as StdField with uid="ZAQEESVTFE",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DPCODVOC", cQueryName = "DPCODVOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    ToolTipText = "Voce di costo analitica abbinata all'articolo",;
    HelpContextID = 183926919,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=313, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DPCODVOC"

  func oDPCODVOC_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S')
    endwith
   endif
  endfunc

  func oDPCODVOC_1_49.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oDPCODVOC_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODVOC_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODVOC_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDPCODVOC_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDPCODVOC_1_49.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DPCODVOC
     i_obj.ecpSave()
  endproc

  add object oVOCDES_1_50 as StdField with uid="FNEYLPUUBZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_VOCDES", cQueryName = "VOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233930922,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=313, InputMask=replicate('X',40)

  func oVOCDES_1_50.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDPCODCOM_1_52 as StdField with uid="EOKQCWHWCJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DPCODCOM", cQueryName = "DPCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice della commessa associata al centro di costo",;
    HelpContextID = 234258557,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=341, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DPCODCOM"

  func oDPCODCOM_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' or g_COMM='S')
    endwith
   endif
  endfunc

  func oDPCODCOM_1_52.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S' and g_COAN<>'S')
    endwith
  endfunc

  func oDPCODCOM_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_52('Part',this)
      if .not. empty(.w_DPCODATT)
        bRes2=.link_1_54('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDPCODCOM_1_52.ecpDrop(oSource)
    this.Parent.oContained.link_1_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODCOM_1_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDPCODCOM_1_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oDPCODCOM_1_52.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DPCODCOM
     i_obj.ecpSave()
  endproc

  add object oDPCODATT_1_54 as StdField with uid="VVYUUEIBCE",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DPCODATT", cQueryName = "DPCODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� inesistente o incongruente",;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 267812982,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=369, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DPCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_DPTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DPCODATT"

  func oDPCODATT_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S')  AND NOT EMPTY(.w_DPCODCOM))
    endwith
   endif
  endfunc

  func oDPCODATT_1_54.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  func oDPCODATT_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODATT_1_54.ecpDrop(oSource)
    this.Parent.oContained.link_1_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODATT_1_54.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_DPTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DPCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_DPTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDPCODATT_1_54'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oDPCODATT_1_54.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DPCODCOM
    i_obj.ATTIPATT=w_DPTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DPCODATT
     i_obj.ecpSave()
  endproc

  add object oDESCAN_1_55 as StdField with uid="YHHBBWWUZS",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53578698,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=231, Top=341, InputMask=replicate('X',40)

  func oDESCAN_1_55.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oDPCODLOT_1_56 as StdField with uid="YXUTTVBVHM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DPCODLOT", cQueryName = "DPCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
    ToolTipText = "Codice lotto associato all'articolo da caricare",;
    HelpContextID = 83263606,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=397, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_DPARTPAD", oKey_2_1="LOCODICE", oKey_2_2="this.w_DPCODLOT"

  func oDPCODLOT_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT='S' AND iif(.w_DPULTFAS='S',.w_FLLOTT1$ 'SC',.w_FLLOTT$ 'SC'))
    endwith
   endif
  endfunc

  func oDPCODLOT_1_56.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S' or .w_DPULTFAS<>'S')
    endwith
  endfunc

  func oDPCODLOT_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODLOT_1_56.ecpDrop(oSource)
    this.Parent.oContained.link_1_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODLOT_1_56.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_DPARTPAD)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_DPARTPAD)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oDPCODLOT_1_56'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oDPCODLOT_1_56.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_DPARTPAD
     i_obj.w_LOCODICE=this.parent.oContained.w_DPCODLOT
     i_obj.ecpSave()
  endproc

  add object oDESATT_1_57 as StdField with uid="BDHTVMWFVL",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 201558986,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=231, Top=369, InputMask=replicate('X',30)

  func oDESATT_1_57.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oDPODLFAS_1_58 as StdField with uid="GNPDBZQPJG",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DPODLFAS", cQueryName = "DPODLFAS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL padre",;
    HelpContextID = 176210039,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=338, Top=64, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_DPODLFAS", oKey_2_1="OLCODODL", oKey_2_2="this.w_DPODLFAS"

  func oDPODLFAS_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=2)
    endwith
   endif
  endfunc

  func oDPODLFAS_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
      if .not. empty(.w_DPODLFAS)
        bRes2=.link_1_58('Full')
      endif
      if .not. empty(.w_DPODLFAS)
        bRes2=.link_1_59('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDPODLFAS_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPODLFAS_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ODL_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OLCODODL="+cp_ToStrODBC(this.Parent.oContained.w_DPODLFAS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"OLCODODL="+cp_ToStr(this.Parent.oContained.w_DPODLFAS)
    endif
    do cp_zoom with 'ODL_MAST','*','OLCODODL,OLCODODL',cp_AbsName(this.parent,'oDPODLFAS_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"",'',this.parent.oContained
  endproc
  proc oDPODLFAS_1_58.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.OLCODODL=w_DPODLFAS
     i_obj.w_OLCODODL=this.parent.oContained.w_DPODLFAS
     i_obj.ecpSave()
  endproc

  add object oDPULTFAS_1_64 as StdCheck with uid="POMSJIYLBK",rtseq=54,rtrep=.f.,left=558, top=118, caption="Ultima fase", enabled=.f.,;
    HelpContextID = 167272567,;
    cFormVar="w_DPULTFAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDPULTFAS_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDPULTFAS_1_64.GetRadio()
    this.Parent.oContained.w_DPULTFAS = this.RadioValue()
    return .t.
  endfunc

  func oDPULTFAS_1_64.SetRadio()
    this.Parent.oContained.w_DPULTFAS=trim(this.Parent.oContained.w_DPULTFAS)
    this.value = ;
      iif(this.Parent.oContained.w_DPULTFAS=='S',1,;
      0)
  endfunc

  add object oDPCODLOT_1_65 as StdField with uid="MJAKQGSQMM",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DPCODLOT", cQueryName = "DPCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
    ToolTipText = "Codice lotto associato all'articolo da caricare",;
    HelpContextID = 83263606,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=398, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_DPCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_DPCODLOT"

  func oDPCODLOT_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT='S' AND iif(.w_DPULTFAS='S',.w_FLLOTT1$ 'SC',.w_FLLOTT$ 'SC'))
    endwith
   endif
  endfunc

  func oDPCODLOT_1_65.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S' or .w_DPULTFAS='S')
    endwith
  endfunc

  func oDPCODLOT_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODLOT_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODLOT_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_DPCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_DPCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oDPCODLOT_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Anagrafica lotti",'GSMD_BZL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oDPCODLOT_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_DPCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_DPCODLOT
     i_obj.ecpSave()
  endproc


  add object oBtn_1_66 as StdButton with uid="GKUCVKDSGM",left=263, top=397, width=22,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per creare nuovo lotto";
    , HelpContextID = 175705046;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_66.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_PERLOT='S' AND iif(.w_DPULTFAS='S',.w_FLLOTT1$ 'SC',.w_FLLOTT$ 'SC'))
      endwith
    endif
  endfunc

  func oBtn_1_66.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_PERLOT<>'S')
     endwith
    endif
  endfunc

  add object oDPCODUBI_1_67 as StdField with uid="MPIVRIWUFC",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DPCODUBI", cQueryName = "DPCODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente o sospeso",;
    ToolTipText = "Codice ubicazione associata al magazzino da caricare",;
    HelpContextID = 200704129,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=375, Top=397, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_DPCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_DPCODUBI"

  func oDPCODUBI_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_FLUBIC='S')
    endwith
   endif
  endfunc

  func oDPCODUBI_1_67.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  func oDPCODUBI_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPCODUBI_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPCODUBI_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_DPCODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_DPCODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oDPCODUBI_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'GSCO_ADP.UBICAZIO_VZM',this.parent.oContained
  endproc
  proc oDPCODUBI_1_67.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_DPCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_DPCODUBI
     i_obj.ecpSave()
  endproc

  add object oDESART_1_71 as StdField with uid="MIKMVMQFZH",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203656138,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=266, Top=173, InputMask=replicate('X',40)

  add object oDESMAG_1_74 as StdField with uid="CEOVSXFBFF",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 170363850,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=174, Top=199, InputMask=replicate('X',30)

  add object oDESUTE_1_95 as StdField with uid="TIOPLZQQPB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 183471050,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=484, Top=11, InputMask=replicate('X',20)


  add object oObj_1_96 as cp_runprogram with uid="SIYYGYFHQY",left=17, top=555, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('MATERIALI')",;
    cEvent = "w_DPCODODL Changed",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_99 as cp_runprogram with uid="TGJXHGSNRT",left=17, top=623, width=515,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('QTAMAT')",;
    cEvent = "w_DPQTAPRO Changed,w_DPQTASCA Changed,w_DPFLEVAS Changed,w_DPQTATEM Changed,w_DPUMTEMP Changed,w_DPCODCOM Changed",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_100 as cp_runprogram with uid="VMOFKKFTFG",left=17, top=572, width=275,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CARICA')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_102 as cp_runprogram with uid="YJMSCAUSBL",left=17, top=589, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CONTROLLI')",;
    cEvent = "Controlli Finali",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_103 as cp_runprogram with uid="PXTMGCWWPR",left=17, top=606, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('ELIMINA')",;
    cEvent = "Delete start,Update start",;
    nPag=1;
    , HelpContextID = 222723402


  add object oBtn_1_108 as StdButton with uid="KKTWUWGJAF",left=670, top=271, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di carico prodotti per produzione";
    , HelpContextID = 266960858;
    , Caption='\<Carico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_108.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"CARI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_108.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFCAR) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_108.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFCAR))
     endwith
    endif
  endfunc


  add object oBtn_1_110 as StdButton with uid="FZPWBPRUQW",left=670, top=317, width=48,height=45,;
    CpPicture="BMP\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di scarico materiali";
    , HelpContextID = 193039578;
    , Caption='\<Scarico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_110.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"SCAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_110.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFSCA) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_110.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFSCA))
     endwith
    endif
  endfunc


  add object oBtn_1_111 as StdButton with uid="RBQMJUNGDP",left=670, top=363, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di carico materiali di output";
    , HelpContextID = 77339782;
    , Caption='Mat.\<Output';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_111.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"MOUT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_111.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFMOU) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_111.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFMOU))
     endwith
    endif
  endfunc


  add object oBtn_1_113 as StdButton with uid="AIZFAEKXFN",left=670, top=409, width=48,height=45,;
    CpPicture="BMP\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento associato alle risorse";
    , HelpContextID = 84711702;
    , Caption='\<Risorse';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_113.Click()
      with this.Parent.oContained
        GSCO_BDP(this.Parent.oContained,"SERV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_113.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DPRIFSER) AND .w_STATOGES<>'LOAD')
      endwith
    endif
  endfunc

  func oBtn_1_113.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DPRIFSER))
     endwith
    endif
  endfunc


  add object oLinkPC_1_115 as StdButton with uid="EDNQHYPCNP",left=619, top=455, width=48,height=45,;
    CpPicture="bmp\MATRICOLE.bmp", caption="", nPag=1;
    , ToolTipText = "Dettaglio matricole articoli prodotti";
    , HelpContextID = 192960638;
    , Caption='\<Matricole';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_115.Click()
      this.Parent.oContained.GSCO_MMT.LinkPCClick()
    endproc

  func oLinkPC_1_115.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (iif(.w_DPULTFAS='S',.w_GESMAT1='S',.w_GESMAT='S') And .w_DPQTAPRO+.w_DPQTASCA>0)
      endwith
    endif
  endfunc

  func oLinkPC_1_115.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (iif(.w_DPULTFAS='S',.w_GESMAT1<>'S',.w_GESMAT<>'S'))
     endwith
    endif
  endfunc


  add object oLinkPC_1_117 as StdButton with uid="DMSDMXHHNV",left=670, top=455, width=48,height=45,;
    CpPicture="bmp\Matrcomp.bmp", caption="", nPag=1;
    , ToolTipText = "Dettaglio matricole componenti utilizzati";
    , HelpContextID = 266186452;
    , Caption='\<Componenti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_117.Click()
      this.Parent.oContained.GSCO_MCO.LinkPCClick()
      this.Parent.oContained.WriteTo_GSCO_MCO()
    endproc

  func oLinkPC_1_117.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_MATENABL and .w_DPCOMMAT="S")
      endwith
    endif
  endfunc

  func oLinkPC_1_117.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not .w_MATENABL or .w_DPCOMMAT<>"S")
     endwith
    endif
  endfunc


  add object oObj_1_131 as cp_runprogram with uid="PYXEELJVCC",left=17, top=640, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('SETMATR')",;
    cEvent = "w_DPCODODL Changed",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_132 as cp_runprogram with uid="UJFNRIMMLG",left=17, top=657, width=353,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('UPDMATR')",;
    cEvent = "w_DPQTAPRO Changed,w_DPQTASCA Changed",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_133 as cp_runprogram with uid="QFSYCKOZLJ",left=17, top=674, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('F3PRESSED')",;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_135 as cp_runprogram with uid="CNJMISAOPR",left=17, top=691, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CHKUPDATED')",;
    cEvent = "Controlli Updated",;
    nPag=1;
    , HelpContextID = 222723402


  add object oObj_1_138 as cp_runprogram with uid="XGPHTFWEDU",left=326, top=578, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('CTRLMAT')",;
    cEvent = "CTRLMAT",;
    nPag=1;
    , HelpContextID = 222723402

  add object oCVDESCRI_1_142 as StdField with uid="SSWHOLLIAQ",rtseq=103,rtrep=.f.,;
    cFormVar = "w_CVDESCRI", cQueryName = "CVDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219179665,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=163, Top=38, InputMask=replicate('X',40)


  add object oDPCRIVAL_1_146 as StdCombo with uid="GRXKKCJZZV",rtseq=104,rtrep=.f.,left=109,top=425,width=83,height=22;
    , ToolTipText = "Criterio di valorizzazione del movimento di carico della fase";
    , HelpContextID = 178487422;
    , cFormVar="w_DPCRIVAL",RowSource=""+"Preventivo,"+"Puntuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDPCRIVAL_1_146.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oDPCRIVAL_1_146.GetRadio()
    this.Parent.oContained.w_DPCRIVAL = this.RadioValue()
    return .t.
  endfunc

  func oDPCRIVAL_1_146.SetRadio()
    this.Parent.oContained.w_DPCRIVAL=trim(this.Parent.oContained.w_DPCRIVAL)
    this.value = ;
      iif(this.Parent.oContained.w_DPCRIVAL=='D',1,;
      iif(this.Parent.oContained.w_DPCRIVAL=='P',2,;
      0))
  endfunc

  add object oPRDATINV_1_150 as StdField with uid="UEIFXOYOKT",rtseq=105,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 117730740,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=384, Top=452

  func oPRDATINV_1_150.mHide()
    with this.Parent.oContained
      return (! .w_DPTIPVAL $ "S-M-U-P")
    endwith
  endfunc


  add object oDPTIPVAL_1_152 as StdCombo with uid="ROGLXQXYQQ",rtseq=106,rtrep=.f.,left=204,top=425,width=193,height=22;
    , ToolTipText = "Tipologia valorizzazione";
    , HelpContextID = 171667582;
    , cFormVar="w_DPTIPVAL",RowSource=""+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Ultimo costo dei saldi (articolo),"+"Ultimo costo standard (articolo),"+"Costo di listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDPTIPVAL_1_152.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"M",;
    iif(this.value =3,"P",;
    iif(this.value =4,"U",;
    iif(this.value =5,"A",;
    iif(this.value =6,"X",;
    iif(this.value =7,"L",;
    space(1)))))))))
  endfunc
  func oDPTIPVAL_1_152.GetRadio()
    this.Parent.oContained.w_DPTIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oDPTIPVAL_1_152.SetRadio()
    this.Parent.oContained.w_DPTIPVAL=trim(this.Parent.oContained.w_DPTIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_DPTIPVAL=="S",1,;
      iif(this.Parent.oContained.w_DPTIPVAL=="M",2,;
      iif(this.Parent.oContained.w_DPTIPVAL=="P",3,;
      iif(this.Parent.oContained.w_DPTIPVAL=="U",4,;
      iif(this.Parent.oContained.w_DPTIPVAL=="A",5,;
      iif(this.Parent.oContained.w_DPTIPVAL=="X",6,;
      iif(this.Parent.oContained.w_DPTIPVAL=="L",7,;
      0)))))))
  endfunc

  add object oDESLIS_1_154 as StdField with uid="WGBDTWDQZP",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229149642,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=162, Top=478, InputMask=replicate('X',40)

  func oDESLIS_1_154.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL<>"L")
    endwith
  endfunc

  add object oDESMAG1_1_157 as StdField with uid="IGDPZQPCGC",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESMAG1", cQueryName = "DESMAG1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 170363850,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=162, Top=478, InputMask=replicate('X',30)

  func oDESMAG1_1_157.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL <> 'A')
    endwith
  endfunc

  add object oDPESERCZ_1_158 as StdField with uid="RSGQFPNKAK",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DPESERCZ", cQueryName = "DPESERCZ",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 249716848,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=452, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_DPESERCZ"

  func oDPESERCZ_1_158.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPVAL $ "S-M-U-P-A")
    endwith
   endif
  endfunc

  func oDPESERCZ_1_158.mHide()
    with this.Parent.oContained
      return ((! .w_DPTIPVAL $ "S-M-U-P"))
    endwith
  endfunc

  func oDPESERCZ_1_158.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_158('Part',this)
      if .not. empty(.w_DPNUMINV)
        bRes2=.link_1_159('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDPESERCZ_1_158.ecpDrop(oSource)
    this.Parent.oContained.link_1_158('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDPNUMINV_1_159 as StdField with uid="CMMAFHJUSL",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DPNUMINV", cQueryName = "DPNUMINV",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 123719796,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=278, Top=452, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", oKey_1_1="INCODESE", oKey_1_2="this.w_DPESERCZ", oKey_2_1="INNUMINV", oKey_2_2="this.w_DPNUMINV"

  func oDPNUMINV_1_159.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPVAL $ 'SMUP' AND NOT EMPTY(.w_DPESERCZ))
    endwith
   endif
  endfunc

  func oDPNUMINV_1_159.mHide()
    with this.Parent.oContained
      return (! .w_DPTIPVAL $ "S-M-U-P")
    endwith
  endfunc

  func oDPNUMINV_1_159.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_159('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPNUMINV_1_159.ecpDrop(oSource)
    this.Parent.oContained.link_1_159('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPNUMINV_1_159.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_DPESERCZ)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_DPESERCZ)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oDPNUMINV_1_159'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc

  add object oDPLISTIN_1_160 as StdField with uid="AFMWOWGTGG",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DPLISTIN", cQueryName = "DPLISTIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 66326404,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=109, Top=478, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DPLISTIN"

  func oDPLISTIN_1_160.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL<>"L")
    endwith
  endfunc

  func oDPLISTIN_1_160.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_160('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPLISTIN_1_160.ecpDrop(oSource)
    this.Parent.oContained.link_1_160('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPLISTIN_1_160.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDPLISTIN_1_160'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDPMAGCOD_1_161 as StdField with uid="PYEEHOGDMJ",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DPMAGCOD", cQueryName = "DPMAGCOD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli ",;
    HelpContextID = 231989382,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=109, Top=478, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DPMAGCOD"

  func oDPMAGCOD_1_161.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPVAL = 'A')
    endwith
   endif
  endfunc

  func oDPMAGCOD_1_161.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL <> 'A')
    endwith
  endfunc

  func oDPMAGCOD_1_161.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_161('Part',this)
    endwith
    return bRes
  endfunc

  proc oDPMAGCOD_1_161.ecpDrop(oSource)
    this.Parent.oContained.link_1_161('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDPMAGCOD_1_161.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDPMAGCOD_1_161'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oDPMAGCOD_1_161.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DPMAGCOD
     i_obj.ecpSave()
  endproc

  add object oCAOLIS_1_163 as StdField with uid="IQCVTLZTCN",rtseq=114,rtrep=.f.,;
    cFormVar = "w_CAOLIS", cQueryName = "CAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 229167066,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=505, Top=478

  func oCAOLIS_1_163.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_DPTIPVAL="L" AND NOT EMPTY(.w_DPLISTIN))
    endwith
   endif
  endfunc

  func oCAOLIS_1_163.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_DPTIPVAL<>"L" OR EMPTY(.w_DPLISTIN))
    endwith
  endfunc

  add object oFASE_1_181 as StdField with uid="LKUBWWSDZD",rtseq=132,rtrep=.f.,;
    cFormVar = "w_FASE", cQueryName = "FASE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fase",;
    HelpContextID = 180383830,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=475, Top=64

  add object oDPFASOUT_1_184 as StdCheck with uid="FVRZLGWMHK",rtseq=134,rtrep=.f.,left=558, top=143, caption="Fase di output", enabled=.f.,;
    HelpContextID = 250326922,;
    cFormVar="w_DPFASOUT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDPFASOUT_1_184.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDPFASOUT_1_184.GetRadio()
    this.Parent.oContained.w_DPFASOUT = this.RadioValue()
    return .t.
  endfunc

  func oDPFASOUT_1_184.SetRadio()
    this.Parent.oContained.w_DPFASOUT=trim(this.Parent.oContained.w_DPFASOUT)
    this.value = ;
      iif(this.Parent.oContained.w_DPFASOUT=='S',1,;
      0)
  endfunc

  add object oOLTSECPR_1_188 as StdField with uid="EIUUXPNIZG",rtseq=138,rtrep=.f.,;
    cFormVar = "w_OLTSECPR", cQueryName = "OLTSECPR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 232879048,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=594, Top=64, InputMask=replicate('X',15)


  add object oObj_1_190 as cp_runprogram with uid="PIVNIHZEGA",left=17, top=708, width=234,height=17,;
    caption='GSCO_BDP',;
   bGlobalFont=.t.,;
    prg="GSCO_BDP('USCITA')",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 222723402

  add object oStr_1_14 as StdString with uid="DIGFMBVGOE",Visible=.t., Left=352, Top=11,;
    Alignment=1, Width=82, Height=18,;
    Caption="Operatore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="WXPCDTXCKN",Visible=.t., Left=3, Top=67,;
    Alignment=1, Width=95, Height=18,;
    Caption="ODL:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="ZIQFMADMCZ",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=95, Height=18,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="NVUJQZDGDF",Visible=.t., Left=203, Top=11,;
    Alignment=1, Width=35, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="LOAHVGVQKE",Visible=.t., Left=5, Top=250,;
    Alignment=0, Width=184, Height=18,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="OHIYQZEKQI",Visible=.t., Left=12, Top=285,;
    Alignment=1, Width=95, Height=19,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="PVEMQUYUNU",Visible=.t., Left=12, Top=313,;
    Alignment=1, Width=95, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="MNYUTFZFML",Visible=.t., Left=12, Top=341,;
    Alignment=1, Width=95, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="CTCXTYMFHC",Visible=.t., Left=12, Top=369,;
    Alignment=1, Width=95, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="GEWDGAGYKJ",Visible=.t., Left=5, Top=145,;
    Alignment=0, Width=184, Height=18,;
    Caption="Dati ODL"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="NUCDGRRCSM",Visible=.t., Left=3, Top=92,;
    Alignment=1, Width=95, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="CDSCIQDCHO",Visible=.t., Left=145, Top=92,;
    Alignment=1, Width=93, Height=18,;
    Caption="Qta prodotta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="HLCTRMYUDF",Visible=.t., Left=350, Top=92,;
    Alignment=1, Width=104, Height=18,;
    Caption="Qta scartata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="BMHBEQZKXC",Visible=.t., Left=12, Top=175,;
    Alignment=1, Width=95, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="CLUZGRALEB",Visible=.t., Left=12, Top=199,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="RMWUWNXGSO",Visible=.t., Left=12, Top=225,;
    Alignment=1, Width=95, Height=15,;
    Caption="UM:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="IZKADZIYEQ",Visible=.t., Left=342, Top=225,;
    Alignment=1, Width=55, Height=15,;
    Caption="Evasa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="RZVHFBWORF",Visible=.t., Left=155, Top=225,;
    Alignment=1, Width=77, Height=15,;
    Caption="Pianificata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="ZAAPRCQBLL",Visible=.t., Left=507, Top=225,;
    Alignment=1, Width=67, Height=18,;
    Caption="Residua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="AUOXIAUZIZ",Visible=.t., Left=12, Top=397,;
    Alignment=1, Width=95, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="PBFSNOWELL",Visible=.t., Left=294, Top=397,;
    Alignment=1, Width=78, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_119.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oStr_1_141 as StdString with uid="LHDSBCNIAA",Visible=.t., Left=-8, Top=40,;
    Alignment=1, Width=106, Height=18,;
    Caption="Causale vers.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_143 as StdString with uid="SEBNLFYRBV",Visible=.t., Left=490, Top=40,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_144 as StdString with uid="KATZMSEZBE",Visible=.t., Left=35, Top=120,;
    Alignment=1, Width=63, Height=18,;
    Caption="UM tempo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_145 as StdString with uid="JCUVMVZDWO",Visible=.t., Left=174, Top=120,;
    Alignment=1, Width=64, Height=18,;
    Caption="Qta tempo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_147 as StdString with uid="TNVVSDAATH",Visible=.t., Left=-13, Top=426,;
    Alignment=1, Width=120, Height=18,;
    Caption="Criterio di valorizz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_148 as StdString with uid="XROSXMIRDD",Visible=.t., Left=17, Top=454,;
    Alignment=1, Width=90, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_148.mHide()
    with this.Parent.oContained
      return (! .w_DPTIPVAL $ "S-M-U-P")
    endwith
  endfunc

  add object oStr_1_149 as StdString with uid="CMQQHAIKKN",Visible=.t., Left=166, Top=454,;
    Alignment=1, Width=104, Height=15,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_149.mHide()
    with this.Parent.oContained
      return (! .w_DPTIPVAL $ "S-M-U-P")
    endwith
  endfunc

  add object oStr_1_151 as StdString with uid="ZFXOXTIOFY",Visible=.t., Left=348, Top=454,;
    Alignment=1, Width=32, Height=15,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_151.mHide()
    with this.Parent.oContained
      return (! .w_DPTIPVAL $ "S-M-U-P")
    endwith
  endfunc

  add object oStr_1_153 as StdString with uid="WNERIRPNMR",Visible=.t., Left=12, Top=479,;
    Alignment=1, Width=95, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_153.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL<>"L")
    endwith
  endfunc

  add object oStr_1_155 as StdString with uid="MACUDGSSKF",Visible=.t., Left=431, Top=479,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_155.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_DPTIPVAL<>"L" OR EMPTY(.w_DPLISTIN))
    endwith
  endfunc

  add object oStr_1_156 as StdString with uid="QCEJHAMSPS",Visible=.t., Left=12, Top=479,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_156.mHide()
    with this.Parent.oContained
      return (.w_DPTIPVAL <> 'A')
    endwith
  endfunc

  add object oStr_1_182 as StdString with uid="XPVLAGCVYA",Visible=.t., Left=238, Top=67,;
    Alignment=1, Width=95, Height=18,;
    Caption="Rif.ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_189 as StdString with uid="FSWHQFTPKG",Visible=.t., Left=524, Top=67,;
    Alignment=1, Width=65, Height=15,;
    Caption="Fase prec.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_44 as StdBox with uid="OANOEOLUAB",left=2, top=266, width=712,height=2

  add object oBox_1_73 as StdBox with uid="IGSZBYYUKD",left=2, top=166, width=712,height=2
enddefine
define class tgsci_adpPag2 as StdContainer
  Width  = 723
  height = 501
  stdWidth  = 723
  stdheight = 501
  resizeXpos=291
  resizeYpos=322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="AXVJSZOODA",left=2, top=7, width=718, height=489, bOnScreen=.t.;

enddefine
define class tgsci_adpPag3 as StdContainer
  Width  = 723
  height = 501
  stdWidth  = 723
  stdheight = 501
  resizeXpos=286
  resizeYpos=324
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="ZHDDJHLJPA",left=-4, top=2, width=724, height=488, bOnScreen=.t.;

enddefine
define class tgsci_adpPag4 as StdContainer
  Width  = 723
  height = 501
  stdWidth  = 723
  stdheight = 501
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="EPGOSDQTQX",left=-1, top=5, width=718, height=489, bOnScreen=.t.;

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_adp','DIC_PROD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DPSERIAL=DIC_PROD.DPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
