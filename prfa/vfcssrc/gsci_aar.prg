* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_aar                                                        *
*              Aree                                                            *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-29                                                      *
* Last revis.: 2015-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_aar"))

* --- Class definition
define class tgsci_aar as StdForm
  Top    = 36
  Left   = 8

  * --- Standard Properties
  Width  = 544
  Height = 232+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-28"
  HelpContextID=143263081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  RIS_ORSE_IDX = 0
  CENCOST_IDX = 0
  TAB_CALE_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  LIS_MACC_IDX = 0
  STR_CONF_IDX = 0
  cFile = "RIS_ORSE"
  cKeySelect = "RL__TIPO,RLCODICE"
  cQueryFilter="RL__TIPO='AR'"
  cKeyWhere  = "RL__TIPO=this.w_RL__TIPO and RLCODICE=this.w_RLCODICE"
  cKeyWhereODBC = '"RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cKeyWhereODBCqualified = '"RIS_ORSE.RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RIS_ORSE.RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cPrg = "gsci_aar"
  cComment = "Aree"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RL__TIPO = space(2)
  w_RLCODICE = space(20)
  w_RLDESCRI = space(40)
  w_RL__NOTE = space(0)
  w_CODICE = space(20)
  w_TIPOPAD = space(2)
  w_CODPAD = space(20)
  w_DESREP = space(40)
  w_ERECAL = space(5)
  w_DERCAL = space(40)
  w_EREWIP = space(5)
  w_DERMAG = space(30)
  w_RLCALRIS = space(5)
  w_DESCAL = space(40)
  w_RLCODCAL = space(5)
  w_RLWIPRIS = space(5)
  w_DESMAG = space(30)
  w_RLMAGWIP = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPMAG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIS_ORSE','gsci_aar')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_aarPag1","gsci_aar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 135880650
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRLCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='TAB_CALE'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='LIS_MACC'
    this.cWorkTables[6]='STR_CONF'
    this.cWorkTables[7]='RIS_ORSE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_ORSE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RL__TIPO = NVL(RL__TIPO,space(2))
      .w_RLCODICE = NVL(RLCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIS_ORSE where RL__TIPO=KeySet.RL__TIPO
    *                            and RLCODICE=KeySet.RLCODICE
    *
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_ORSE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_ORSE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIS_ORSE '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPOPAD = 'UP'
        .w_CODPAD = space(20)
        .w_DESREP = space(40)
        .w_ERECAL = space(5)
        .w_DERCAL = space(40)
        .w_EREWIP = space(5)
        .w_DERMAG = space(30)
        .w_DESCAL = space(40)
        .w_DESMAG = space(30)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPMAG = space(1)
        .w_RL__TIPO = NVL(RL__TIPO,space(2))
        .w_RLCODICE = NVL(RLCODICE,space(20))
        .w_RLDESCRI = NVL(RLDESCRI,space(40))
        .w_RL__NOTE = NVL(RL__NOTE,space(0))
        .w_CODICE = .w_RLCODICE
          .link_1_6('Load')
          .link_1_8('Load')
          .link_1_10('Load')
          .link_1_12('Load')
        .w_RLCALRIS = NVL(RLCALRIS,space(5))
          if link_1_14_joined
            this.w_RLCALRIS = NVL(TCCODICE114,NVL(this.w_RLCALRIS,space(5)))
            this.w_DESCAL = NVL(TCDESCRI114,space(40))
          else
          .link_1_14('Load')
          endif
        .w_RLCODCAL = NVL(RLCODCAL,space(5))
          * evitabile
          *.link_1_16('Load')
        .w_RLWIPRIS = NVL(RLWIPRIS,space(5))
          if link_1_17_joined
            this.w_RLWIPRIS = NVL(MGCODMAG117,NVL(this.w_RLWIPRIS,space(5)))
            this.w_DESMAG = NVL(MGDESMAG117,space(30))
            this.w_TIPMAG = NVL(MGTIPMAG117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_RLMAGWIP = NVL(RLMAGWIP,space(5))
          * evitabile
          *.link_1_21('Load')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        cp_LoadRecExtFlds(this,'RIS_ORSE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RL__TIPO = space(2)
      .w_RLCODICE = space(20)
      .w_RLDESCRI = space(40)
      .w_RL__NOTE = space(0)
      .w_CODICE = space(20)
      .w_TIPOPAD = space(2)
      .w_CODPAD = space(20)
      .w_DESREP = space(40)
      .w_ERECAL = space(5)
      .w_DERCAL = space(40)
      .w_EREWIP = space(5)
      .w_DERMAG = space(30)
      .w_RLCALRIS = space(5)
      .w_DESCAL = space(40)
      .w_RLCODCAL = space(5)
      .w_RLWIPRIS = space(5)
      .w_DESMAG = space(30)
      .w_RLMAGWIP = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPMAG = space(1)
      if .cFunction<>"Filter"
        .w_RL__TIPO = 'AR'
          .DoRTCalc(2,4,.f.)
        .w_CODICE = .w_RLCODICE
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CODICE))
          .link_1_6('Full')
          endif
        .w_TIPOPAD = 'UP'
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CODPAD))
          .link_1_8('Full')
          endif
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_ERECAL))
          .link_1_10('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_EREWIP))
          .link_1_12('Full')
          endif
        .DoRTCalc(12,13,.f.)
          if not(empty(.w_RLCALRIS))
          .link_1_14('Full')
          endif
          .DoRTCalc(14,14,.f.)
        .w_RLCODCAL = iif(empty(.w_RLCALRIS), .w_ERECAL, .w_RLCALRIS)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_RLCODCAL))
          .link_1_16('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_RLWIPRIS))
          .link_1_17('Full')
          endif
          .DoRTCalc(17,17,.f.)
        .w_RLMAGWIP = iif(empty(.w_RLWIPRIS), .w_EREWIP, .w_RLWIPRIS)
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_RLMAGWIP))
          .link_1_21('Full')
          endif
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_ORSE')
    this.DoRTCalc(20,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRLCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oRLDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oRL__NOTE_1_5.enabled = i_bVal
      .Page1.oPag.oRLCALRIS_1_14.enabled = i_bVal
      .Page1.oPag.oRLWIPRIS_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRLCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRLCODICE_1_2.enabled = .t.
        .Page1.oPag.oRLDESCRI_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIS_ORSE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__TIPO,"RL__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODICE,"RLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLDESCRI,"RLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__NOTE,"RL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCALRIS,"RLCALRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODCAL,"RLCODCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLWIPRIS,"RLWIPRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLMAGWIP,"RLMAGWIP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    i_lTable = "RIS_ORSE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SAR with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIS_ORSE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIS_ORSE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_ORSE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLCALRIS"+;
                  ",RLCODCAL,RLWIPRIS,RLMAGWIP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RL__TIPO)+;
                  ","+cp_ToStrODBC(this.w_RLCODICE)+;
                  ","+cp_ToStrODBC(this.w_RLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RL__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_RLCALRIS)+;
                  ","+cp_ToStrODBCNull(this.w_RLCODCAL)+;
                  ","+cp_ToStrODBCNull(this.w_RLWIPRIS)+;
                  ","+cp_ToStrODBCNull(this.w_RLMAGWIP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_ORSE')
        cp_CheckDeletedKey(i_cTable,0,'RL__TIPO',this.w_RL__TIPO,'RLCODICE',this.w_RLCODICE)
        INSERT INTO (i_cTable);
              (RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLCALRIS,RLCODCAL,RLWIPRIS,RLMAGWIP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RL__TIPO;
                  ,this.w_RLCODICE;
                  ,this.w_RLDESCRI;
                  ,this.w_RL__NOTE;
                  ,this.w_RLCALRIS;
                  ,this.w_RLCODCAL;
                  ,this.w_RLWIPRIS;
                  ,this.w_RLMAGWIP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIS_ORSE_IDX,i_nConn)
      *
      * update RIS_ORSE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_ORSE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RLDESCRI="+cp_ToStrODBC(this.w_RLDESCRI)+;
             ",RL__NOTE="+cp_ToStrODBC(this.w_RL__NOTE)+;
             ",RLCALRIS="+cp_ToStrODBCNull(this.w_RLCALRIS)+;
             ",RLCODCAL="+cp_ToStrODBCNull(this.w_RLCODCAL)+;
             ",RLWIPRIS="+cp_ToStrODBCNull(this.w_RLWIPRIS)+;
             ",RLMAGWIP="+cp_ToStrODBCNull(this.w_RLMAGWIP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_ORSE')
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        UPDATE (i_cTable) SET;
              RLDESCRI=this.w_RLDESCRI;
             ,RL__NOTE=this.w_RL__NOTE;
             ,RLCALRIS=this.w_RLCALRIS;
             ,RLCODCAL=this.w_RLCODCAL;
             ,RLWIPRIS=this.w_RLWIPRIS;
             ,RLMAGWIP=this.w_RLMAGWIP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIS_ORSE_IDX,i_nConn)
      *
      * delete RIS_ORSE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_CODICE = .w_RLCODICE
          .link_1_6('Full')
        .DoRTCalc(6,6,.t.)
          .link_1_8('Full')
        .DoRTCalc(8,8,.t.)
          .link_1_10('Full')
        .DoRTCalc(10,10,.t.)
          .link_1_12('Full')
        .DoRTCalc(12,14,.t.)
            .w_RLCODCAL = iif(empty(.w_RLCALRIS), .w_ERECAL, .w_RLCALRIS)
          .link_1_16('Full')
        .DoRTCalc(16,17,.t.)
            .w_RLMAGWIP = iif(empty(.w_RLWIPRIS), .w_EREWIP, .w_RLWIPRIS)
          .link_1_21('Full')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_lTable = "STR_CONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2], .t., this.STR_CONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPFIG,SCCODFIG,SCCODPAD,SCTIPPAD";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODFIG="+cp_ToStrODBC(this.w_CODICE);
                   +" and SCTIPFIG="+cp_ToStrODBC(this.w_RL__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPFIG',this.w_RL__TIPO;
                       ,'SCCODFIG',this.w_CODICE)
            select SCTIPFIG,SCCODFIG,SCCODPAD,SCTIPPAD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.SCCODFIG,space(20))
      this.w_CODPAD = NVL(_Link_.SCCODPAD,space(20))
      this.w_TIPOPAD = NVL(_Link_.SCTIPPAD,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(20)
      endif
      this.w_CODPAD = space(20)
      this.w_TIPOPAD = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPFIG,1)+'\'+cp_ToStr(_Link_.SCCODFIG,1)
      cp_ShowWarn(i_cKey,this.STR_CONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPAD
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLMAGWIP,RLCODCAL,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODPAD);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPOPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPOPAD;
                       ,'RLCODICE',this.w_CODPAD)
            select RL__TIPO,RLCODICE,RLMAGWIP,RLCODCAL,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAD = NVL(_Link_.RLCODICE,space(20))
      this.w_EREWIP = NVL(_Link_.RLMAGWIP,space(5))
      this.w_ERECAL = NVL(_Link_.RLCODCAL,space(5))
      this.w_DESREP = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAD = space(20)
      endif
      this.w_EREWIP = space(5)
      this.w_ERECAL = space(5)
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ERECAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ERECAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ERECAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ERECAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ERECAL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ERECAL = NVL(_Link_.TCCODICE,space(5))
      this.w_DERCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ERECAL = space(5)
      endif
      this.w_DERCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ERECAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EREWIP
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EREWIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EREWIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_EREWIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_EREWIP)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EREWIP = NVL(_Link_.MGCODMAG,space(5))
      this.w_DERMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_EREWIP = space(5)
      endif
      this.w_DERMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EREWIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCALRIS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCALRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_RLCALRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_RLCALRIS))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCALRIS)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCALRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oRLCALRIS_1_14'),i_cWhere,'',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCALRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_RLCALRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_RLCALRIS)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCALRIS = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCALRIS = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCALRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.TCCODICE as TCCODICE114"+ ",link_1_14.TCDESCRI as TCDESCRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on RIS_ORSE.RLCALRIS=link_1_14.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCALRIS=link_1_14.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLCODCAL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_RLCODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_RLCODCAL)
            select TCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCAL = NVL(_Link_.TCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCAL = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLWIPRIS
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLWIPRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_RLWIPRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_RLWIPRIS))
          select MGCODMAG,MGDESMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLWIPRIS)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_RLWIPRIS)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_RLWIPRIS)+"%");

            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RLWIPRIS) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oRLWIPRIS_1_17'),i_cWhere,'GSAR_AMA',"Magazzini WIP",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLWIPRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RLWIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RLWIPRIS)
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLWIPRIS = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_TIPMAG = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RLWIPRIS = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_TIPMAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAG='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure di tipo diverso da WIP")
        endif
        this.w_RLWIPRIS = space(5)
        this.w_DESMAG = space(30)
        this.w_TIPMAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLWIPRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.MGCODMAG as MGCODMAG117"+ ",link_1_17.MGDESMAG as MGDESMAG117"+ ",link_1_17.MGTIPMAG as MGTIPMAG117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on RIS_ORSE.RLWIPRIS=link_1_17.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLWIPRIS=link_1_17.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLMAGWIP
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLMAGWIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLMAGWIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RLMAGWIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RLMAGWIP)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLMAGWIP = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RLMAGWIP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLMAGWIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRLCODICE_1_2.value==this.w_RLCODICE)
      this.oPgFrm.Page1.oPag.oRLCODICE_1_2.value=this.w_RLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_4.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_4.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRL__NOTE_1_5.value==this.w_RL__NOTE)
      this.oPgFrm.Page1.oPag.oRL__NOTE_1_5.value=this.w_RL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPAD_1_8.value==this.w_CODPAD)
      this.oPgFrm.Page1.oPag.oCODPAD_1_8.value=this.w_CODPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_1_9.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_1_9.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page1.oPag.oERECAL_1_10.value==this.w_ERECAL)
      this.oPgFrm.Page1.oPag.oERECAL_1_10.value=this.w_ERECAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDERCAL_1_11.value==this.w_DERCAL)
      this.oPgFrm.Page1.oPag.oDERCAL_1_11.value=this.w_DERCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oEREWIP_1_12.value==this.w_EREWIP)
      this.oPgFrm.Page1.oPag.oEREWIP_1_12.value=this.w_EREWIP
    endif
    if not(this.oPgFrm.Page1.oPag.oDERMAG_1_13.value==this.w_DERMAG)
      this.oPgFrm.Page1.oPag.oDERMAG_1_13.value=this.w_DERMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCALRIS_1_14.value==this.w_RLCALRIS)
      this.oPgFrm.Page1.oPag.oRLCALRIS_1_14.value=this.w_RLCALRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_15.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_15.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oRLWIPRIS_1_17.value==this.w_RLWIPRIS)
      this.oPgFrm.Page1.oPag.oRLWIPRIS_1_17.value=this.w_RLWIPRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_20.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_20.value=this.w_DESMAG
    endif
    cp_SetControlsValueExtFlds(this,'RIS_ORSE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RLCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_RLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPMAG='W')  and not(empty(.w_RLWIPRIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLWIPRIS_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure di tipo diverso da WIP")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsci_aarPag1 as StdContainer
  Width  = 540
  height = 232
  stdWidth  = 540
  stdheight = 232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRLCODICE_1_2 as StdField with uid="GNCKMBJDCB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RLCODICE", cQueryName = "RL__TIPO,RLCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice area",;
    HelpContextID = 84507739,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=80, Top=8, InputMask=replicate('X',20)

  add object oRLDESCRI_1_4 as StdField with uid="DPYUBFLRYN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve",;
    HelpContextID = 1078177,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=8, InputMask=replicate('X',40)

  add object oRL__NOTE_1_5 as StdMemo with uid="VBIILKWMRF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RL__NOTE", cQueryName = "RL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione estesa",;
    HelpContextID = 196820059,;
   bGlobalFont=.t.,;
    Height=49, Width=456, Left=80, Top=31

  add object oCODPAD_1_8 as StdField with uid="GHICHQEOZS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODPAD", cQueryName = "CODPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 2453978,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=80, Top=90, InputMask=replicate('X',20), cLinkFile="RIS_ORSE", cZoomOnZoom="GSCI_ARE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPOPAD", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODPAD"

  func oCODPAD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESREP_1_9 as StdField with uid="HGTDDVFPED",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65178570,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=90, InputMask=replicate('X',40)

  add object oERECAL_1_10 as StdField with uid="AXSXAOZHOO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ERECAL", cQueryName = "ERECAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 137518778,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=185, Top=113, InputMask=replicate('X',5), cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_ERECAL"

  func oERECAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDERCAL_1_11 as StdField with uid="MVOOSJLKZJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DERCAL", cQueryName = "DERCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 137468874,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=113, InputMask=replicate('X',40)

  add object oEREWIP_1_12 as StdField with uid="CSIDZAFHTI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EREWIP", cQueryName = "EREWIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 60710586,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=185, Top=136, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_EREWIP"

  func oEREWIP_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDERMAG_1_13 as StdField with uid="QSVXPMMNUC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DERMAG", cQueryName = "DERMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220699594,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=136, InputMask=replicate('X',30)

  add object oRLCALRIS_1_14 as StdField with uid="DMCHUIZLJB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RLCALRIS", cQueryName = "RLCALRIS",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario dell'area",;
    HelpContextID = 242973801,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=174, Top=185, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_RLCALRIS"

  func oRLCALRIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCALRIS_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCALRIS_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oRLCALRIS_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Calendari",'',this.parent.oContained
  endproc

  add object oDESCAL_1_15 as StdField with uid="YRBSEHPUAE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 137464778,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=185, InputMask=replicate('X',40)

  add object oRLWIPRIS_1_17 as StdField with uid="XTQWVJXJEL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_RLWIPRIS", cQueryName = "RLWIPRIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure di tipo diverso da WIP",;
    ToolTipText = "Magazzino WIP dell'area",;
    HelpContextID = 247774313,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=174, Top=209, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_RLWIPRIS"

  func oRLWIPRIS_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLWIPRIS_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLWIPRIS_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oRLWIPRIS_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini WIP",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oRLWIPRIS_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_RLWIPRIS
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_20 as StdField with uid="EOKZTVXZSB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220695498,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=240, Top=209, InputMask=replicate('X',30)


  add object oObj_1_30 as cp_runprogram with uid="YHZPPCKKIK",left=404, top=243, width=116,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCW("NODO")',;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 34734566

  add object oStr_1_3 as StdString with uid="FLNBCPKWBR",Visible=.t., Left=21, Top=8,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="BLNFKYHIRP",Visible=.t., Left=56, Top=186,;
    Alignment=1, Width=114, Height=15,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ODYSTFHFLB",Visible=.t., Left=35, Top=209,;
    Alignment=1, Width=135, Height=15,;
    Caption="Magazzino WIP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="GTHKABEJGX",Visible=.t., Left=29, Top=158,;
    Alignment=0, Width=80, Height=15,;
    Caption="Gestionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="UELIDMKHSS",Visible=.t., Left=57, Top=114,;
    Alignment=1, Width=124, Height=15,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XYBXPGCBXG",Visible=.t., Left=36, Top=136,;
    Alignment=1, Width=145, Height=15,;
    Caption="Magazzino WIP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UVHQWSRWKE",Visible=.t., Left=4, Top=90,;
    Alignment=1, Width=70, Height=15,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="WIDPPMEULH",left=25, top=174, width=514,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="RL__TIPO='AR'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsci_aar','RIS_ORSE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName2+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
