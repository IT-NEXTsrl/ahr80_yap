* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bcw                                                        *
*              Calendari / WIP ereditati                                       *
*                                                                              *
*      Author: Zucchetti Tam                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2014-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_AZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bcw",oParentObject,m.w_AZIONE)
return(i_retval)

define class tgsci_bcw as StdBatch
  * --- Local variables
  w_AZIONE = space(10)
  w_OCODCAL = space(5)
  w_OMAGWIP = space(5)
  w_MESS = space(10)
  w_PUNPAD = .NULL.
  w_cCURSOR = space(10)
  w_cPOS = 0
  * --- WorkFile variables
  RIS_ORSE_idx=0
  STR_CONF_idx=0
  DIS_RISO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di esplosione della struttura aziedale per WIP e Calen. ereditati
    * --- Codice Figlio (da GSCI_MSC)
    * --- Codice Risorsa (da GSCI_A??, ??='RE','AR','CL','MA' )
    * --- Codice Calendario in uso della Risorsa (da GSCI_A??, ??='RE','AR','CL')
    * --- Codice WIP in uso della Risorsa (da GSCI_A??, ??='RE','AR','CL')
    * --- Codice Calendario della Risorsa
    * --- Codice WIP della Risorsa
    * --- Codice Calendario del padre della Risorsa
    * --- Codice WIP del padre della Risorsa
    * --- Tipo Risorsa
    * --- Tipo Risorsa (da GSCI_MSC)
    * --- Qta risorsa
    * --- Tipo Risorsa Figlio (da GSCI_MSC)
    do case
      case this.w_AZIONE="ARCO"
        * --- Controlla le risorse siano inserite una sola volta
        * --- Select from STR_CONF
        i_nConn=i_TableProp[this.STR_CONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STR_CONF_idx,2],.t.,this.STR_CONF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" STR_CONF ";
              +" where SCCODFIG="+cp_ToStrODBC(this.oParentObject.w_SCCODFIG)+"";
               ,"_Curs_STR_CONF")
        else
          select * from (i_cTable);
           where SCCODFIG=this.oParentObject.w_SCCODFIG;
            into cursor _Curs_STR_CONF
        endif
        if used('_Curs_STR_CONF')
          select _Curs_STR_CONF
          locate for 1=1
          do while not(eof())
          this.w_MESS = AH_Msgformat("La risorsa <%1> � gi� utilizzata in %2",rtrim(this.oParentObject.w_SCCODFIG),rtrim(_Curs_STR_CONF.SCCODPAD))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
            select _Curs_STR_CONF
            continue
          enddo
          use
        endif
        if empty(this.oParentObject.w_RLCALRIS) and this.oParentObject.w_TP<>"CL"
          * --- Lancio Procedura Esplosione per aggiornamento Calendario
          GSCI_BXS(this,this.oParentObject.w_SCCODFIG,"C",this.oParentObject.w_ERECAL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Write into RIS_ORSE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RLCODCAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ERECAL),'RIS_ORSE','RLCODCAL');
                +i_ccchkf ;
            +" where ";
                +"RLCODICE = "+cp_ToStrODBC(this.oParentObject.w_SCCODFIG);
                   )
          else
            update (i_cTable) set;
                RLCODCAL = this.oParentObject.w_ERECAL;
                &i_ccchkf. ;
             where;
                RLCODICE = this.oParentObject.w_SCCODFIG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.oParentObject.w_TF="CL"
            * --- Write into DIS_RISO
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_RISO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_RISO_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_RISO_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DRCODCAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ERECAL),'DIS_RISO','DRCODCAL');
                  +i_ccchkf ;
              +" where ";
                  +"DRCODRIS = "+cp_ToStrODBC(this.oParentObject.w_SCCODFIG);
                  +" and DRCALRIS is null ";
                     )
            else
              update (i_cTable) set;
                  DRCODCAL = this.oParentObject.w_ERECAL;
                  &i_ccchkf. ;
               where;
                  DRCODRIS = this.oParentObject.w_SCCODFIG;
                  and DRCALRIS is null;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if empty(this.oParentObject.w_RLWIPRIS)
          * --- Lancio Procedura Esplosione per aggiornamento Magazzino
          GSCI_BXS(this,this.oParentObject.w_SCCODFIG,"W",this.oParentObject.w_EREWIP)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Write into RIS_ORSE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_EREWIP),'RIS_ORSE','RLMAGWIP');
                +i_ccchkf ;
            +" where ";
                +"RLCODICE = "+cp_ToStrODBC(this.oParentObject.w_SCCODFIG);
                   )
          else
            update (i_cTable) set;
                RLMAGWIP = this.oParentObject.w_EREWIP;
                &i_ccchkf. ;
             where;
                RLCODICE = this.oParentObject.w_SCCODFIG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.w_AZIONE="NODO"
        * --- Legge i vecchi valori del Calendario e del WIP
        * --- Read from RIS_ORSE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RLCODCAL,RLMAGWIP"+;
            " from "+i_cTable+" RIS_ORSE where ";
                +"RLCODICE = "+cp_ToStrODBC(this.oParentObject.w_RLCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RLCODCAL,RLMAGWIP;
            from (i_cTable) where;
                RLCODICE = this.oParentObject.w_RLCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OCODCAL = NVL(cp_ToDate(_read_.RLCODCAL),cp_NullValue(_read_.RLCODCAL))
          this.w_OMAGWIP = NVL(cp_ToDate(_read_.RLMAGWIP),cp_NullValue(_read_.RLMAGWIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_RL__TIPO<>"CL"
          * --- Se il Calendario della risorsa � vuoto usa quello del padre
          this.oParentObject.w_RLCODCAL = iif(empty(this.oParentObject.w_RLCALRIS), this.oParentObject.w_ERECAL, this.oParentObject.w_RLCALRIS)
          if this.oParentObject.w_RLCODCAL<>this.w_OCODCAL
            * --- Aggiorna il Calendario dei Figli
            GSCI_BXS(this,this.oParentObject.w_RLCODICE,"C",this.oParentObject.w_RLCODCAL)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Se il Magazzino WIP della risorsa � vuoto usa quello del padre
        this.oParentObject.w_RLMAGWIP = iif(empty(this.oParentObject.w_RLWIPRIS), this.oParentObject.w_EREWIP, this.oParentObject.w_RLWIPRIS)
        if this.oParentObject.w_RLMAGWIP <> this.w_OMAGWIP
          * --- Aggiorna il Mag. WIP dei Figli
          GSCI_BXS(this,this.oParentObject.w_RLCODICE,"W",this.oParentObject.w_RLMAGWIP)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_AZIONE= "QTARISORSA"
        if upper( this.oParentObject.GSCI_MDR.class )="STDDYNAMICCHILD"
          this.oParentObject.oPgfrm.Pages[2].opag.uienable(.T.)
        endif
        this.w_PUNPAD = this.oParentObject.GSCI_MDR
        this.w_cCURSOR = this.w_PUNPAD.cTrsName
        this.w_cPOS = 0
        * --- Memorizza posizione
        select (this.w_cCURSOR)
        this.w_cPOS = recno()
        * --- Conta preferenziali
        CALCULATE Max(t_DRQTARIS) to this.oParentObject.w_RLQTARIS
        * --- Ripristina posizione
        if this.w_cPOS>0 and ! eof()
          select (this.w_cCURSOR)
          goto this.w_cPOS
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_AZIONE)
    this.w_AZIONE=w_AZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='STR_CONF'
    this.cWorkTables[3]='DIS_RISO'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_STR_CONF')
      use in _Curs_STR_CONF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_AZIONE"
endproc
