* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_mdr                                                        *
*              Disponibilitą risorse                                           *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-27                                                      *
* Last revis.: 2014-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsci_mdr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsci_mdr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsci_mdr")
  return

* --- Class definition
define class tgsci_mdr as StdPCForm
  Width  = 629
  Height = 300
  Top    = 16
  Left   = 65
  cComment = "Disponibilitą risorse"
  cPrg = "gsci_mdr"
  HelpContextID=80348521
  add object cnt as tcgsci_mdr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsci_mdr as PCContext
  w_DRCODRIS = space(20)
  w_DRTIPRIS = space(2)
  w_ERECAL = space(5)
  w_DRDATINI = space(8)
  w_DRQTARIS = 0
  w_DRCALRIS = space(5)
  w_DRSERVIZ = space(20)
  w_DRCOEEFF = 0
  w_DRPERRID = 0
  w_DRCOSRIS = 0
  w_DESCAL = space(40)
  w_DRCODCAL = space(5)
  w_DROVERHE = 0
  w_CODICE = space(2)
  w_TIPART = space(2)
  w_ARTIPART = space(2)
  w_SERVIZ = space(20)
  w_UNMIS1 = space(3)
  w_FLTEMP1 = space(1)
  proc Save(i_oFrom)
    this.w_DRCODRIS = i_oFrom.w_DRCODRIS
    this.w_DRTIPRIS = i_oFrom.w_DRTIPRIS
    this.w_ERECAL = i_oFrom.w_ERECAL
    this.w_DRDATINI = i_oFrom.w_DRDATINI
    this.w_DRQTARIS = i_oFrom.w_DRQTARIS
    this.w_DRCALRIS = i_oFrom.w_DRCALRIS
    this.w_DRSERVIZ = i_oFrom.w_DRSERVIZ
    this.w_DRCOEEFF = i_oFrom.w_DRCOEEFF
    this.w_DRPERRID = i_oFrom.w_DRPERRID
    this.w_DRCOSRIS = i_oFrom.w_DRCOSRIS
    this.w_DESCAL = i_oFrom.w_DESCAL
    this.w_DRCODCAL = i_oFrom.w_DRCODCAL
    this.w_DROVERHE = i_oFrom.w_DROVERHE
    this.w_CODICE = i_oFrom.w_CODICE
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_ARTIPART = i_oFrom.w_ARTIPART
    this.w_SERVIZ = i_oFrom.w_SERVIZ
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_FLTEMP1 = i_oFrom.w_FLTEMP1
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DRCODRIS = this.w_DRCODRIS
    i_oTo.w_DRTIPRIS = this.w_DRTIPRIS
    i_oTo.w_ERECAL = this.w_ERECAL
    i_oTo.w_DRDATINI = this.w_DRDATINI
    i_oTo.w_DRQTARIS = this.w_DRQTARIS
    i_oTo.w_DRCALRIS = this.w_DRCALRIS
    i_oTo.w_DRSERVIZ = this.w_DRSERVIZ
    i_oTo.w_DRCOEEFF = this.w_DRCOEEFF
    i_oTo.w_DRPERRID = this.w_DRPERRID
    i_oTo.w_DRCOSRIS = this.w_DRCOSRIS
    i_oTo.w_DESCAL = this.w_DESCAL
    i_oTo.w_DRCODCAL = this.w_DRCODCAL
    i_oTo.w_DROVERHE = this.w_DROVERHE
    i_oTo.w_CODICE = this.w_CODICE
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_ARTIPART = this.w_ARTIPART
    i_oTo.w_SERVIZ = this.w_SERVIZ
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_FLTEMP1 = this.w_FLTEMP1
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsci_mdr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 629
  Height = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-26"
  HelpContextID=80348521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIS_RISO_IDX = 0
  TAB_CALE_IDX = 0
  PAR_PROD_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  cFile = "DIS_RISO"
  cKeySelect = "DRCODRIS,DRTIPRIS"
  cKeyWhere  = "DRCODRIS=this.w_DRCODRIS and DRTIPRIS=this.w_DRTIPRIS"
  cKeyDetail  = "DRCODRIS=this.w_DRCODRIS and DRTIPRIS=this.w_DRTIPRIS and DRDATINI=this.w_DRDATINI"
  cKeyWhereODBC = '"DRCODRIS="+cp_ToStrODBC(this.w_DRCODRIS)';
      +'+" and DRTIPRIS="+cp_ToStrODBC(this.w_DRTIPRIS)';

  cKeyDetailWhereODBC = '"DRCODRIS="+cp_ToStrODBC(this.w_DRCODRIS)';
      +'+" and DRTIPRIS="+cp_ToStrODBC(this.w_DRTIPRIS)';
      +'+" and DRDATINI="+cp_ToStrODBC(this.w_DRDATINI,"D")';

  cKeyWhereODBCqualified = '"DIS_RISO.DRCODRIS="+cp_ToStrODBC(this.w_DRCODRIS)';
      +'+" and DIS_RISO.DRTIPRIS="+cp_ToStrODBC(this.w_DRTIPRIS)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsci_mdr"
  cComment = "Disponibilitą risorse"
  i_nRowNum = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DRCODRIS = space(20)
  w_DRTIPRIS = space(2)
  w_ERECAL = space(5)
  w_DRDATINI = ctod('  /  /  ')
  w_DRQTARIS = 0
  w_DRCALRIS = space(5)
  w_DRSERVIZ = space(20)
  w_DRCOEEFF = 0
  w_DRPERRID = 0
  w_DRCOSRIS = 0
  w_DESCAL = space(40)
  w_DRCODCAL = space(5)
  w_DROVERHE = 0
  w_CODICE = space(2)
  w_TIPART = space(2)
  w_ARTIPART = space(2)
  w_SERVIZ = space(20)
  w_UNMIS1 = space(3)
  w_FLTEMP1 = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_mdrPag1","gsci_mdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TAB_CALE'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='DIS_RISO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_RISO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_RISO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsci_mdr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIS_RISO where DRCODRIS=KeySet.DRCODRIS
    *                            and DRTIPRIS=KeySet.DRTIPRIS
    *                            and DRDATINI=KeySet.DRDATINI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2],this.bLoadRecFilter,this.DIS_RISO_IDX,"gsci_mdr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_RISO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_RISO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_RISO '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DRCODRIS',this.w_DRCODRIS  ,'DRTIPRIS',this.w_DRTIPRIS  )
      select * from (i_cTable) DIS_RISO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODICE = 'PP'
        .w_TIPART = 'FM'
        .w_SERVIZ = space(20)
        .w_DRCODRIS = NVL(DRCODRIS,space(20))
        .w_DRTIPRIS = NVL(DRTIPRIS,space(2))
        .w_ERECAL = this.oParentObject.w_ERECAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
          .link_1_5('Load')
        cp_LoadRecExtFlds(this,'DIS_RISO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCAL = space(40)
          .w_ARTIPART = space(2)
          .w_UNMIS1 = space(3)
          .w_FLTEMP1 = space(1)
          .w_DRDATINI = NVL(cp_ToDate(DRDATINI),ctod("  /  /  "))
          .w_DRQTARIS = NVL(DRQTARIS,0)
          .w_DRCALRIS = NVL(DRCALRIS,space(5))
          if link_2_3_joined
            this.w_DRCALRIS = NVL(TCCODICE203,NVL(this.w_DRCALRIS,space(5)))
            this.w_DESCAL = NVL(TCDESCRI203,space(40))
          else
          .link_2_3('Load')
          endif
          .w_DRSERVIZ = NVL(DRSERVIZ,space(20))
          if link_2_4_joined
            this.w_DRSERVIZ = NVL(ARCODART204,NVL(this.w_DRSERVIZ,space(20)))
            this.w_ARTIPART = NVL(ARTIPART204,space(2))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
          else
          .link_2_4('Load')
          endif
          .w_DRCOEEFF = NVL(DRCOEEFF,0)
          .w_DRPERRID = NVL(DRPERRID,0)
          .w_DRCOSRIS = NVL(DRCOSRIS,0)
          .w_DRCODCAL = NVL(DRCODCAL,space(5))
          * evitabile
          *.link_2_10('Load')
          .w_DROVERHE = NVL(DROVERHE,0)
          .link_2_13('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DRDATINI with .w_DRDATINI
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ERECAL = this.oParentObject.w_ERECAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DRCODRIS=space(20)
      .w_DRTIPRIS=space(2)
      .w_ERECAL=space(5)
      .w_DRDATINI=ctod("  /  /  ")
      .w_DRQTARIS=0
      .w_DRCALRIS=space(5)
      .w_DRSERVIZ=space(20)
      .w_DRCOEEFF=0
      .w_DRPERRID=0
      .w_DRCOSRIS=0
      .w_DESCAL=space(40)
      .w_DRCODCAL=space(5)
      .w_DROVERHE=0
      .w_CODICE=space(2)
      .w_TIPART=space(2)
      .w_ARTIPART=space(2)
      .w_SERVIZ=space(20)
      .w_UNMIS1=space(3)
      .w_FLTEMP1=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_ERECAL = this.oParentObject.w_ERECAL
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_DRCALRIS))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DRSERVIZ))
         .link_2_4('Full')
        endif
        .w_DRCOEEFF = 1
        .DoRTCalc(9,11,.f.)
        .w_DRCODCAL = iif(empty(.w_DRCALRIS), .w_ERECAL, .w_DRCALRIS)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_DRCODCAL))
         .link_2_10('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(13,13,.f.)
        .w_CODICE = 'PP'
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODICE))
         .link_1_5('Full')
        endif
        .w_TIPART = 'FM'
        .DoRTCalc(16,18,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_13('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_RISO')
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DIS_RISO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODRIS,"DRCODRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRTIPRIS,"DRTIPRIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DRDATINI D(8);
      ,t_DRQTARIS N(5);
      ,t_DRCALRIS C(5);
      ,t_DRSERVIZ C(20);
      ,t_DRCOEEFF N(7,2);
      ,t_DRPERRID N(7,2);
      ,t_DRCOSRIS N(18,4);
      ,t_DESCAL C(40);
      ,t_DROVERHE N(7,2);
      ,DRDATINI D(8);
      ,t_DRCODCAL C(5);
      ,t_ARTIPART C(2);
      ,t_UNMIS1 C(3);
      ,t_FLTEMP1 C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mdrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1.controlsource=this.cTrsName+'.t_DRDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRQTARIS_2_2.controlsource=this.cTrsName+'.t_DRQTARIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRCALRIS_2_3.controlsource=this.cTrsName+'.t_DRCALRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRSERVIZ_2_4.controlsource=this.cTrsName+'.t_DRSERVIZ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOEEFF_2_5.controlsource=this.cTrsName+'.t_DRCOEEFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRPERRID_2_6.controlsource=this.cTrsName+'.t_DRPERRID'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOSRIS_2_7.controlsource=this.cTrsName+'.t_DRCOSRIS'
    this.oPgFRm.Page1.oPag.oDESCAL_2_8.controlsource=this.cTrsName+'.t_DESCAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDROVERHE_2_11.controlsource=this.cTrsName+'.t_DROVERHE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(83)
    this.AddVLine(129)
    this.AddVLine(193)
    this.AddVLine(343)
    this.AddVLine(384)
    this.AddVLine(438)
    this.AddVLine(556)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2])
      *
      * insert into DIS_RISO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_RISO')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_RISO')
        i_cFldBody=" "+;
                  "(DRCODRIS,DRTIPRIS,DRDATINI,DRQTARIS,DRCALRIS"+;
                  ",DRSERVIZ,DRCOEEFF,DRPERRID,DRCOSRIS,DRCODCAL"+;
                  ",DROVERHE,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DRCODRIS)+","+cp_ToStrODBC(this.w_DRTIPRIS)+","+cp_ToStrODBC(this.w_DRDATINI)+","+cp_ToStrODBC(this.w_DRQTARIS)+","+cp_ToStrODBCNull(this.w_DRCALRIS)+;
             ","+cp_ToStrODBCNull(this.w_DRSERVIZ)+","+cp_ToStrODBC(this.w_DRCOEEFF)+","+cp_ToStrODBC(this.w_DRPERRID)+","+cp_ToStrODBC(this.w_DRCOSRIS)+","+cp_ToStrODBCNull(this.w_DRCODCAL)+;
             ","+cp_ToStrODBC(this.w_DROVERHE)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_RISO')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_RISO')
        cp_CheckDeletedKey(i_cTable,0,'DRCODRIS',this.w_DRCODRIS,'DRTIPRIS',this.w_DRTIPRIS,'DRDATINI',this.w_DRDATINI)
        INSERT INTO (i_cTable) (;
                   DRCODRIS;
                  ,DRTIPRIS;
                  ,DRDATINI;
                  ,DRQTARIS;
                  ,DRCALRIS;
                  ,DRSERVIZ;
                  ,DRCOEEFF;
                  ,DRPERRID;
                  ,DRCOSRIS;
                  ,DRCODCAL;
                  ,DROVERHE;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DRCODRIS;
                  ,this.w_DRTIPRIS;
                  ,this.w_DRDATINI;
                  ,this.w_DRQTARIS;
                  ,this.w_DRCALRIS;
                  ,this.w_DRSERVIZ;
                  ,this.w_DRCOEEFF;
                  ,this.w_DRPERRID;
                  ,this.w_DRCOSRIS;
                  ,this.w_DRCODCAL;
                  ,this.w_DROVERHE;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not empty(t_DRDATINI)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_RISO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DRDATINI="+cp_ToStrODBC(&i_TN.->DRDATINI)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_RISO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DRDATINI=&i_TN.->DRDATINI;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not empty(t_DRDATINI)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DRDATINI="+cp_ToStrODBC(&i_TN.->DRDATINI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DRDATINI=&i_TN.->DRDATINI;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DRDATINI with this.w_DRDATINI
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIS_RISO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_RISO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DRQTARIS="+cp_ToStrODBC(this.w_DRQTARIS)+;
                     ",DRCALRIS="+cp_ToStrODBCNull(this.w_DRCALRIS)+;
                     ",DRSERVIZ="+cp_ToStrODBCNull(this.w_DRSERVIZ)+;
                     ",DRCOEEFF="+cp_ToStrODBC(this.w_DRCOEEFF)+;
                     ",DRPERRID="+cp_ToStrODBC(this.w_DRPERRID)+;
                     ",DRCOSRIS="+cp_ToStrODBC(this.w_DRCOSRIS)+;
                     ",DRCODCAL="+cp_ToStrODBCNull(this.w_DRCODCAL)+;
                     ",DROVERHE="+cp_ToStrODBC(this.w_DROVERHE)+;
                     ",DRDATINI="+cp_ToStrODBC(this.w_DRDATINI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DRDATINI="+cp_ToStrODBC(DRDATINI)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_RISO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DRQTARIS=this.w_DRQTARIS;
                     ,DRCALRIS=this.w_DRCALRIS;
                     ,DRSERVIZ=this.w_DRSERVIZ;
                     ,DRCOEEFF=this.w_DRCOEEFF;
                     ,DRPERRID=this.w_DRPERRID;
                     ,DRCOSRIS=this.w_DRCOSRIS;
                     ,DRCODCAL=this.w_DRCODCAL;
                     ,DROVERHE=this.w_DROVERHE;
                     ,DRDATINI=this.w_DRDATINI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DRDATINI=&i_TN.->DRDATINI;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not empty(t_DRDATINI)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIS_RISO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DRDATINI="+cp_ToStrODBC(&i_TN.->DRDATINI)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DRDATINI=&i_TN.->DRDATINI;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not empty(t_DRDATINI)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_RISO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_ERECAL = this.oParentObject.w_ERECAL
        .DoRTCalc(4,11,.t.)
          .w_DRCODCAL = iif(empty(.w_DRCALRIS), .w_ERECAL, .w_DRCALRIS)
          .link_2_10('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(13,13,.t.)
          .link_1_5('Full')
        .DoRTCalc(15,17,.t.)
          .link_2_13('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DRCODCAL with this.w_DRCODCAL
      replace t_ARTIPART with this.w_ARTIPART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_FLTEMP1 with this.w_FLTEMP1
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DRCALRIS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCALRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_DRCALRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_DRCALRIS))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCALRIS)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCALRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oDRCALRIS_2_3'),i_cWhere,'',"Elenco calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCALRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_DRCALRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_DRCALRIS)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCALRIS = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DRCALRIS = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCALRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.TCCODICE as TCCODICE203"+ ",link_2_3.TCDESCRI as TCDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on DIS_RISO.DRCALRIS=link_2_3.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and DIS_RISO.DRCALRIS=link_2_3.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRSERVIZ
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRSERVIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DRSERVIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DRSERVIZ))
          select ARCODART,ARTIPART,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRSERVIZ)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRSERVIZ) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDRSERVIZ_2_4'),i_cWhere,'',"Servizi",'GSCO_KPP.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRSERVIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DRSERVIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DRSERVIZ)
            select ARCODART,ARTIPART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRSERVIZ = NVL(_Link_.ARCODART,space(20))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DRSERVIZ = space(20)
      endif
      this.w_ARTIPART = space(2)
      this.w_UNMIS1 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTIPART='FM'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un servizio di tipo quantitą e valore")
        endif
        this.w_DRSERVIZ = space(20)
        this.w_ARTIPART = space(2)
        this.w_UNMIS1 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRSERVIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARTIPART as ARTIPART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DIS_RISO.DRSERVIZ=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DIS_RISO.DRSERVIZ=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRCODCAL
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_DRCODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_DRCODCAL)
            select TCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODCAL = NVL(_Link_.TCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODCAL = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPSRVDFT";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_CODICE)
            select PPCODICE,PPSRVDFT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_SERVIZ = NVL(_Link_.PPSRVDFT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(2)
      endif
      this.w_SERVIZ = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLTEMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLTEMP1 = NVL(_Link_.UMFLTEMP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLTEMP1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCAL_2_8.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_2_8.value=this.w_DESCAL
      replace t_DESCAL with this.oPgFrm.Page1.oPag.oDESCAL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1.value==this.w_DRDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1.value=this.w_DRDATINI
      replace t_DRDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRQTARIS_2_2.value==this.w_DRQTARIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRQTARIS_2_2.value=this.w_DRQTARIS
      replace t_DRQTARIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRQTARIS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCALRIS_2_3.value==this.w_DRCALRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCALRIS_2_3.value=this.w_DRCALRIS
      replace t_DRCALRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCALRIS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRSERVIZ_2_4.value==this.w_DRSERVIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRSERVIZ_2_4.value=this.w_DRSERVIZ
      replace t_DRSERVIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRSERVIZ_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOEEFF_2_5.value==this.w_DRCOEEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOEEFF_2_5.value=this.w_DRCOEEFF
      replace t_DRCOEEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOEEFF_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRPERRID_2_6.value==this.w_DRPERRID)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRPERRID_2_6.value=this.w_DRPERRID
      replace t_DRPERRID with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRPERRID_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOSRIS_2_7.value==this.w_DRCOSRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOSRIS_2_7.value=this.w_DRCOSRIS
      replace t_DRCOSRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOSRIS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDROVERHE_2_11.value==this.w_DROVERHE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDROVERHE_2_11.value=this.w_DROVERHE
      replace t_DROVERHE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDROVERHE_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'DIS_RISO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsci_mdr
      with this
        if .bUpdated
          do case
            case .cFunction='Load' or .cFunction='Edit'
              select(.ctrsname)
              scan
                if t_fltemp1<>'S'
                  i_bnoChk = .f.
                  i_bRes = .f.
                  i_cErrorMsg =Ah_MsgFormat("Indicare un servizio con U.M. principale gestita a tempo!")
                  exit
                endif
              endscan
          endcase
        endif
      endwith
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_DRDATINI) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRDATINI_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_DRQTARIS>=1) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRQTARIS_2_2
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_ARTIPART='FM') and not(empty(.w_DRSERVIZ)) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRSERVIZ_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire un servizio di tipo quantitą e valore")
        case   not(0<=.w_DRCOEEFF and .w_DRCOEEFF<=1) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRCOEEFF_2_5
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(0<=.w_DRPERRID and .w_DRPERRID<=100) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDRPERRID_2_6
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_DROVERHE>=0) and (not empty(.w_DRDATINI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDROVERHE_2_11
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not empty(.w_DRDATINI)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not empty(t_DRDATINI))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DRDATINI=ctod("  /  /  ")
      .w_DRQTARIS=0
      .w_DRCALRIS=space(5)
      .w_DRSERVIZ=space(20)
      .w_DRCOEEFF=0
      .w_DRPERRID=0
      .w_DRCOSRIS=0
      .w_DESCAL=space(40)
      .w_DRCODCAL=space(5)
      .w_DROVERHE=0
      .w_ARTIPART=space(2)
      .w_UNMIS1=space(3)
      .w_FLTEMP1=space(1)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_DRCALRIS))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_DRSERVIZ))
        .link_2_4('Full')
      endif
        .w_DRCOEEFF = 1
      .DoRTCalc(9,11,.f.)
        .w_DRCODCAL = iif(empty(.w_DRCALRIS), .w_ERECAL, .w_DRCALRIS)
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_DRCODCAL))
        .link_2_10('Full')
      endif
      .DoRTCalc(13,18,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_13('Full')
      endif
    endwith
    this.DoRTCalc(19,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DRDATINI = t_DRDATINI
    this.w_DRQTARIS = t_DRQTARIS
    this.w_DRCALRIS = t_DRCALRIS
    this.w_DRSERVIZ = t_DRSERVIZ
    this.w_DRCOEEFF = t_DRCOEEFF
    this.w_DRPERRID = t_DRPERRID
    this.w_DRCOSRIS = t_DRCOSRIS
    this.w_DESCAL = t_DESCAL
    this.w_DRCODCAL = t_DRCODCAL
    this.w_DROVERHE = t_DROVERHE
    this.w_ARTIPART = t_ARTIPART
    this.w_UNMIS1 = t_UNMIS1
    this.w_FLTEMP1 = t_FLTEMP1
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DRDATINI with this.w_DRDATINI
    replace t_DRQTARIS with this.w_DRQTARIS
    replace t_DRCALRIS with this.w_DRCALRIS
    replace t_DRSERVIZ with this.w_DRSERVIZ
    replace t_DRCOEEFF with this.w_DRCOEEFF
    replace t_DRPERRID with this.w_DRPERRID
    replace t_DRCOSRIS with this.w_DRCOSRIS
    replace t_DESCAL with this.w_DESCAL
    replace t_DRCODCAL with this.w_DRCODCAL
    replace t_DROVERHE with this.w_DROVERHE
    replace t_ARTIPART with this.w_ARTIPART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_FLTEMP1 with this.w_FLTEMP1
    if i_srv='A'
      replace DRDATINI with this.w_DRDATINI
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mdrPag1 as StdContainer
  Width  = 625
  height = 300
  stdWidth  = 625
  stdheight = 300
  resizeXpos=266
  resizeYpos=225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=4, width=593,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="DRDATINI",Label1="Da data",Field2="DRQTARIS",Label2="Quant.",Field3="DRCALRIS",Label3="Calendario",Field4="DRSERVIZ",Label4="Servizio",Field5="DRCOEEFF",Label5="Effic.",Field6="DRPERRID",Label6="% Riduz.",Field7="DRCOSRIS",Label7="Costo orario std",Field8="DROVERHE",Label8="Overhead",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235742330

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=22,;
    width=609+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=23,width=608+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_CALE|ART_ICOL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCAL_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_CALE'
        oDropInto=this.oBodyCol.oRow.oDRCALRIS_2_3
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oDRSERVIZ_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCAL_2_8 as StdTrsField with uid="JQIKBRZAYQ",rtseq=11,rtrep=.t.,;
    cFormVar="w_DESCAL",value=space(40),enabled=.f.,;
    HelpContextID = 74550218,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=78, Top=275, InputMask=replicate('X',40)

  add object oStr_2_9 as StdString with uid="JSFYWPLDSL",Visible=.t., Left=8, Top=277,;
    Alignment=1, Width=64, Height=15,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsci_mdrBodyRow as CPBodyRowCnt
  Width=599
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDRDATINI_2_1 as StdTrsField with uid="OIVVETGIMG",rtseq=4,rtrep=.t.,;
    cFormVar="w_DRDATINI",value=ctod("  /  /  "),isprimarykey=.t.,;
    ToolTipText = "Data",;
    HelpContextID = 105148033,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=-2, Top=0

  add object oDRQTARIS_2_2 as StdTrsField with uid="ZSUMTQOKWL",rtseq=5,rtrep=.t.,;
    cFormVar="w_DRQTARIS",value=0,;
    ToolTipText = "Quantitą risorsa",;
    HelpContextID = 27222409,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=76, Top=0, cSayPict=["@Z 9999"], cGetPict=["@Z 9999"]

  func oDRQTARIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DRQTARIS>=1)
    endwith
    return bRes
  endfunc

  add object oDRCALRIS_2_3 as StdTrsField with uid="CISCCCFLYT",rtseq=6,rtrep=.t.,;
    cFormVar="w_DRCALRIS",value=space(5),;
    ToolTipText = "Codice calendario",;
    HelpContextID = 37454217,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=61, Left=122, Top=0, cSayPict=["XXXXX"], cGetPict=["XXXXX"], InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_DRCALRIS"

  func oDRCALRIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCALRIS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDRCALRIS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oDRCALRIS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco calendari",'',this.parent.oContained
  endproc

  add object oDRSERVIZ_2_4 as StdTrsField with uid="IMQCGXQHKV",rtseq=7,rtrep=.t.,;
    cFormVar="w_DRSERVIZ",value=space(20),;
    HelpContextID = 111182224,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un servizio di tipo quantitą e valore",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=188, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DRSERVIZ"

  proc oDRSERVIZ_2_4.mDefault
    with this.Parent.oContained
      if empty(.w_DRSERVIZ)
        .w_DRSERVIZ = .w_SERVIZ
      endif
    endwith
  endproc

  func oDRSERVIZ_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRSERVIZ_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDRSERVIZ_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDRSERVIZ_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Servizi",'GSCO_KPP.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDRCOEEFF_2_5 as StdTrsField with uid="EPEOGFRPGQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_DRCOEEFF",value=0,;
    ToolTipText = "Coefficiente di efficienza [0..1]",;
    HelpContextID = 81363324,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=39, Left=336, Top=0, cSayPict=["9.99"], cGetPict=["9.99"]

  func oDRCOEEFF_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_DRCOEEFF and .w_DRCOEEFF<=1)
    endwith
    return bRes
  endfunc

  add object oDRPERRID_2_6 as StdTrsField with uid="OFGRBEMXLZ",rtseq=9,rtrep=.t.,;
    cFormVar="w_DRPERRID",value=0,;
    ToolTipText = "Percentuale di riduzione della capacitą produttiva della risorsa",;
    HelpContextID = 44061050,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=378, Top=0, cSayPict=["@Z 999.9"], cGetPict=["@Z 999.9"]

  func oDRPERRID_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_DRPERRID and .w_DRPERRID<=100)
    endwith
    return bRes
  endfunc

  add object oDRCOSRIS_2_7 as StdTrsField with uid="BRCFULXSLI",rtseq=10,rtrep=.t.,;
    cFormVar="w_DRCOSRIS",value=0,;
    ToolTipText = "Costo orario standard risorsa",;
    HelpContextID = 45711753,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=432, Top=0, cSayPict=['@Z '+v_PU(140)], cGetPict=[v_GU(140)]

  add object oDROVERHE_2_11 as StdTrsField with uid="GIZMBBJRGN",rtseq=13,rtrep=.t.,;
    cFormVar="w_DROVERHE",value=0,;
    ToolTipText = "Overhead",;
    HelpContextID = 31539579,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=550, Top=0, cSayPict=["@Z 999.99"], cGetPict=["999.99"]

  func oDROVERHE_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DROVERHE>=0)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oDRDATINI_2_1.When()
    return(.t.)
  proc oDRDATINI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDRDATINI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_mdr','DIS_RISO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DRCODRIS=DIS_RISO.DRCODRIS";
  +" and "+i_cAliasName2+".DRTIPRIS=DIS_RISO.DRTIPRIS";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
