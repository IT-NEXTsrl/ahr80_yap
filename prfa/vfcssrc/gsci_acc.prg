* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_acc                                                        *
*              Classe codice di fase                                           *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_108]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-31                                                      *
* Last revis.: 2017-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_acc"))

* --- Class definition
define class tgsci_acc as StdForm
  Top    = 4
  Left   = 14

  * --- Standard Properties
  Width  = 613
  Height = 322+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-17"
  HelpContextID=109708649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  CCF_MAST_IDX = 0
  cFile = "CCF_MAST"
  cKeySelect = "CFCODICE"
  cKeyWhere  = "CFCODICE=this.w_CFCODICE"
  cKeyWhereODBC = '"CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cKeyWhereODBCqualified = '"CCF_MAST.CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cPrg = "gsci_acc"
  cComment = "Classe codice di fase"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFCODICE = space(5)
  o_CFCODICE = space(5)
  w_CFTIPCLA = space(1)
  w_MAXLENGTH = 0
  w_CFDESCRI = space(30)
  w_CFINCREM = 0
  w_ULTPROG = 0
  w_CODPRE = space(5)
  w_CFSEPASN = space(1)
  w_CFSEPARA = space(1)

  * --- Children pointers
  GSCI_MCC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CCF_MAST','gsci_acc')
    stdPageFrame::Init()
    *set procedure to GSCI_MCC additive
    with this
      .Pages(1).addobject("oPag","tgsci_accPag1","gsci_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Classe codice di fase")
      .Pages(1).HelpContextID = 61009634
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCI_MCC
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CCF_MAST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCF_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCF_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MCC = CREATEOBJECT('stdDynamicChild',this,'GSCI_MCC',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSCI_MCC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MCC)
      this.GSCI_MCC.DestroyChildrenChain()
      this.GSCI_MCC=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MCC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MCC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MCC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCI_MCC.SetKey(;
            .w_CFCODICE,"CFCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCI_MCC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CFCODICE,"CFCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCI_MCC)
        i_f=.GSCI_MCC.BuildFilter()
        if !(i_f==.GSCI_MCC.cQueryFilter)
          i_fnidx=.GSCI_MCC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MCC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MCC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MCC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MCC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CFCODICE = NVL(CFCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CCF_MAST where CFCODICE=KeySet.CFCODICE
    *
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCF_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCF_MAST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CCF_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ULTPROG = 0
        .w_CFCODICE = NVL(CFCODICE,space(5))
        .w_CFTIPCLA = NVL(CFTIPCLA,space(1))
        .w_MAXLENGTH = IIF(.w_CFTIPCLA='A', 20, 66)
        .w_CFDESCRI = NVL(CFDESCRI,space(30))
        .w_CFINCREM = NVL(CFINCREM,0)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(iiF(Not empty(.w_CFCODICE),AH_Msgformat('Lunghezza max codice %1 caratteri', alltrim(str(.w_MAXLENGTH)) ),''))
        .w_CODPRE = .w_CFCODICE
          .link_1_14('Load')
        .w_CFSEPASN = NVL(CFSEPASN,space(1))
        .w_CFSEPARA = NVL(CFSEPARA,space(1))
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        cp_LoadRecExtFlds(this,'CCF_MAST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsci_acc
    *Calcola Ultimo Progressivo (associato alla classe)
    this.NotifyEvent("Carica")
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CFCODICE = space(5)
      .w_CFTIPCLA = space(1)
      .w_MAXLENGTH = 0
      .w_CFDESCRI = space(30)
      .w_CFINCREM = 0
      .w_ULTPROG = 0
      .w_CODPRE = space(5)
      .w_CFSEPASN = space(1)
      .w_CFSEPARA = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CFTIPCLA = 'C'
        .w_MAXLENGTH = IIF(.w_CFTIPCLA='A', 20, 66)
          .DoRTCalc(4,4,.f.)
        .w_CFINCREM = 1
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(iiF(Not empty(.w_CFCODICE),AH_Msgformat('Lunghezza max codice %1 caratteri', alltrim(str(.w_MAXLENGTH)) ),''))
          .DoRTCalc(6,6,.f.)
        .w_CODPRE = .w_CFCODICE
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CODPRE))
          .link_1_14('Full')
          endif
        .w_CFSEPASN = 'S'
        .w_CFSEPARA = '.'
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCF_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCFCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCFTIPCLA_1_2.enabled = i_bVal
      .Page1.oPag.oCFDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oCFSEPASN_1_15.enabled = i_bVal
      .Page1.oPag.oCFSEPARA_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCFCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCFCODICE_1_1.enabled = .t.
        .Page1.oPag.oCFDESCRI_1_4.enabled = .t.
      endif
    endwith
    this.GSCI_MCC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CCF_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MCC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODICE,"CFCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFTIPCLA,"CFTIPCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDESCRI,"CFDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINCREM,"CFINCREM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSEPASN,"CFSEPASN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFSEPARA,"CFSEPARA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    i_lTable = "CCF_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CCF_MAST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CCF_MAST_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CCF_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCF_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CCF_MAST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CFCODICE,CFTIPCLA,CFDESCRI,CFINCREM,CFSEPASN"+;
                  ",CFSEPARA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CFCODICE)+;
                  ","+cp_ToStrODBC(this.w_CFTIPCLA)+;
                  ","+cp_ToStrODBC(this.w_CFDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CFINCREM)+;
                  ","+cp_ToStrODBC(this.w_CFSEPASN)+;
                  ","+cp_ToStrODBC(this.w_CFSEPARA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCF_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CCF_MAST')
        cp_CheckDeletedKey(i_cTable,0,'CFCODICE',this.w_CFCODICE)
        INSERT INTO (i_cTable);
              (CFCODICE,CFTIPCLA,CFDESCRI,CFINCREM,CFSEPASN,CFSEPARA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CFCODICE;
                  ,this.w_CFTIPCLA;
                  ,this.w_CFDESCRI;
                  ,this.w_CFINCREM;
                  ,this.w_CFSEPASN;
                  ,this.w_CFSEPARA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CCF_MAST_IDX,i_nConn)
      *
      * update CCF_MAST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CCF_MAST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CFTIPCLA="+cp_ToStrODBC(this.w_CFTIPCLA)+;
             ",CFDESCRI="+cp_ToStrODBC(this.w_CFDESCRI)+;
             ",CFINCREM="+cp_ToStrODBC(this.w_CFINCREM)+;
             ",CFSEPASN="+cp_ToStrODBC(this.w_CFSEPASN)+;
             ",CFSEPARA="+cp_ToStrODBC(this.w_CFSEPARA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CCF_MAST')
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        UPDATE (i_cTable) SET;
              CFTIPCLA=this.w_CFTIPCLA;
             ,CFDESCRI=this.w_CFDESCRI;
             ,CFINCREM=this.w_CFINCREM;
             ,CFSEPASN=this.w_CFSEPASN;
             ,CFSEPARA=this.w_CFSEPARA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCI_MCC : Saving
      this.GSCI_MCC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CFCODICE,"CFCODICE";
             )
      this.GSCI_MCC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsci_acc
    this.Notifyevent('CHECKPRO')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCI_MCC : Deleting
    this.GSCI_MCC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CFCODICE,"CFCODICE";
           )
    this.GSCI_MCC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CCF_MAST_IDX,i_nConn)
      *
      * delete CCF_MAST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_MAXLENGTH = IIF(.w_CFTIPCLA='A', 20, 66)
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(iiF(Not empty(.w_CFCODICE),AH_Msgformat('Lunghezza max codice %1 caratteri', alltrim(str(.w_MAXLENGTH)) ),''))
        .DoRTCalc(4,6,.t.)
        if .o_CFCODICE<>.w_CFCODICE
            .w_CODPRE = .w_CFCODICE
          .link_1_14('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(iiF(Not empty(.w_CFCODICE),AH_Msgformat('Lunghezza max codice %1 caratteri', alltrim(str(.w_MAXLENGTH)) ),''))
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPRE
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCF_MAST_IDX,3]
    i_lTable = "CCF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2], .t., this.CCF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_CODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',this.w_CODPRE)
            select CFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRE = NVL(_Link_.CFCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CCF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value==this.w_CFCODICE)
      this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value=this.w_CFCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFTIPCLA_1_2.RadioValue()==this.w_CFTIPCLA)
      this.oPgFrm.Page1.oPag.oCFTIPCLA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRI_1_4.value==this.w_CFDESCRI)
      this.oPgFrm.Page1.oPag.oCFDESCRI_1_4.value=this.w_CFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPROG_1_10.value==this.w_ULTPROG)
      this.oPgFrm.Page1.oPag.oULTPROG_1_10.value=this.w_ULTPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oCFSEPASN_1_15.RadioValue()==this.w_CFSEPASN)
      this.oPgFrm.Page1.oPag.oCFSEPASN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFSEPARA_1_16.value==this.w_CFSEPARA)
      this.oPgFrm.Page1.oPag.oCFSEPARA_1_16.value=this.w_CFSEPARA
    endif
    cp_SetControlsValueExtFlds(this,'CCF_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CFCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CFCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MCC.CheckForm()
      if i_bres
        i_bres=  .GSCI_MCC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsci_acc
      with this
        if .bUpdated OR .gsci_mcc.bUpdated
          do case
            case Not Empty(.w_CODPRE) AND .cFunction='Load'
             i_bnoChk = .f.
             i_bRes = .f.
             i_cErrorMsg =Ah_MsgFormat("Esiste una classe con lo stesso codice")
            case .gsci_mcc.w_TOTLUNGHE+iif(This.w_CFSEPASN='S', reccount(.gsci_mcc.ctrsname)-1 , 0) > This.w_MAXLENGTH
              i_bnoChk = .f.
              i_bRes = .f.
              i_cErrorMsg = Ah_MsgFormat("Il codice non deve essere maggiore di %1 caratteri", alltrim(str(This.w_MAXLENGTH)))
          endcase
        endif
      endwith
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CFCODICE = this.w_CFCODICE
    * --- GSCI_MCC : Depends On
    this.GSCI_MCC.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=this.w_CFTIPCLA<>'A'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile variare una classe di tipo codifica articolo di fase"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsci_accPag1 as StdContainer
  Width  = 609
  height = 322
  stdWidth  = 609
  stdheight = 322
  resizeXpos=346
  resizeYpos=252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFCODICE_1_1 as StdField with uid="TACRJIJIKB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CFCODICE", cQueryName = "CFCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe",;
    HelpContextID = 118060395,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=141, Top=19, InputMask=replicate('X',5)


  add object oCFTIPCLA_1_2 as StdCombo with uid="YLPWBKIBIB",rtseq=2,rtrep=.f.,left=210,top=19,width=147,height=22;
    , ToolTipText = "Modalit� di generazione dell'articolo di fase";
    , HelpContextID = 238779033;
    , cFormVar="w_CFTIPCLA",RowSource=""+"Classe articolo di fase,"+"Classe codice di fase", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCFTIPCLA_1_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCFTIPCLA_1_2.GetRadio()
    this.Parent.oContained.w_CFTIPCLA = this.RadioValue()
    return .t.
  endfunc

  func oCFTIPCLA_1_2.SetRadio()
    this.Parent.oContained.w_CFTIPCLA=trim(this.Parent.oContained.w_CFTIPCLA)
    this.value = ;
      iif(this.Parent.oContained.w_CFTIPCLA=='A',1,;
      iif(this.Parent.oContained.w_CFTIPCLA=='C',2,;
      0))
  endfunc

  add object oCFDESCRI_1_4 as StdField with uid="MDQVTXOEWP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CFDESCRI", cQueryName = "CFDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe",;
    HelpContextID = 32474479,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=141, Top=47, InputMask=replicate('X',30)


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="BQCSXLSDMN",left=2, top=75, width=605, height=218, bOnScreen=.t.;



  add object oObj_1_9 as cp_runprogram with uid="IKTKTRDRQJ",left=0, top=364, width=169,height=24,;
    caption='GSCI_BSZ',;
   bGlobalFont=.t.,;
    prg="GSCI_BSZ('A')",;
    cEvent = "Carica,Blank",;
    nPag=1;
    , HelpContextID = 28541632

  add object oULTPROG_1_10 as StdField with uid="OXNYTBTAME",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ULTPROG", cQueryName = "ULTPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo progressivo utilizzato",;
    HelpContextID = 233540678,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=465, Top=19, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'


  add object oObj_1_11 as cp_runprogram with uid="KZTKXFXZBP",left=0, top=337, width=169,height=24,;
    caption='GSCI_BSZ',;
   bGlobalFont=.t.,;
    prg="GSCI_BSZ('P')",;
    cEvent = "CHECKPRO",;
    nPag=1;
    , ToolTipText = "verifica che esista almeno un progressivo";
    , HelpContextID = 28541632


  add object oObj_1_12 as cp_calclbl with uid="UWOIDONZWZ",left=2, top=306, width=264,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=0,FontSize=8,FontItalic=.t.,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 68288998


  add object oCFSEPASN_1_15 as StdCombo with uid="NWEPWWWYHD",rtseq=8,rtrep=.f.,left=465,top=50,width=48,height=22;
    , HelpContextID = 264271220;
    , cFormVar="w_CFSEPASN",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCFSEPASN_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oCFSEPASN_1_15.GetRadio()
    this.Parent.oContained.w_CFSEPASN = this.RadioValue()
    return .t.
  endfunc

  func oCFSEPASN_1_15.SetRadio()
    this.Parent.oContained.w_CFSEPASN=trim(this.Parent.oContained.w_CFSEPASN)
    this.value = ;
      iif(this.Parent.oContained.w_CFSEPASN=='S',1,;
      iif(this.Parent.oContained.w_CFSEPASN=='N',2,;
      0))
  endfunc

  add object oCFSEPARA_1_16 as StdField with uid="VHFDZJILEL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CFSEPARA", cQueryName = "CFSEPARA",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 4164249,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=521, Top=50, InputMask=replicate('X',1)


  add object oObj_1_18 as cp_runprogram with uid="CYJMQZNOUN",left=0, top=391, width=169,height=24,;
    caption='GSCI_BSZ',;
   bGlobalFont=.t.,;
    prg="GSCI_BSZ('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Verifica se la classe � utilizzata";
    , HelpContextID = 28541632

  add object oStr_1_5 as StdString with uid="NPTDJABPXL",Visible=.t., Left=27, Top=50,;
    Alignment=1, Width=110, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QOAHOCUUTF",Visible=.t., Left=323, Top=23,;
    Alignment=1, Width=139, Height=18,;
    Caption="Ultimo numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CINYWLVENA",Visible=.t., Left=3, Top=23,;
    Alignment=1, Width=134, Height=18,;
    Caption="Classe codice di fase:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EHDESVBBGD",Visible=.t., Left=361, Top=50,;
    Alignment=1, Width=101, Height=18,;
    Caption="Separatore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_acc','CCF_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFCODICE=CCF_MAST.CFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
