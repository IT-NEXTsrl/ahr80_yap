* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_btc                                                        *
*              Test ciclo lavorazione ordine di lavorazione                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2017-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_btc",oParentObject,m.pAzione)
return(i_retval)

define class tgsci_btc as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_CLCODODL = space(15)
  w_CLROWNUM = 0
  w_OPOS = 0
  w_MSG = space(100)
  w_TMPN = 0
  GSCO_MCL = .NULL.
  GSCO_AOP = .NULL.
  GSCO_MOL = .NULL.
  GSCO_MRL = .NULL.
  w_TRSNAME = space(15)
  w_OAREA = space(10)
  w_CLKEYRIF = space(10)
  w_CLSERIAL = space(10)
  w_CLBFRIFE = 0
  w_CLOURIFE = 0
  w_PRIMOGIRO = .f.
  w_CLROWORD = 0
  w_WIPFASEPREC = space(41)
  w_WIPFASEOLD = space(41)
  w_CLWIPOUT = space(41)
  w_CLWIPFPR = space(41)
  w_CLDESFAS = space(40)
  w_ROWNUM = 0
  w_OLDCP = space(1)
  w_OLDOU = space(1)
  w_NUMFAS = 0
  w_FLTPROVE = space(1)
  w_CLULTFAS = space(1)
  w_CLINDPRE = space(2)
  w_CLFASSEL = space(2)
  o_CLFASSEL = space(2)
  w_CLROWORD = 0
  o_CLROWORD = 0
  w_CLFASSEP = space(1)
  w_CLQTAPRE = 0
  o_CLINDPRE = space(2)
  w_CLCOUPOI = space(1)
  o_CLCOUPOI = space(1)
  w_CLFASOUT = space(1)
  o_CLFASOUT = space(1)
  w_OLTQTODL = 0
  w_OLTQTOD1 = 0
  w_OLTUNMIS = space(3)
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_OLTDISBA = space(20)
  w_CLGRUPPO = space(20)
  w_OLTCILAV = space(20)
  w_CLINDCIC = 0
  w_CLDESCIC = space(40)
  w_ULTFASE = .f.
  w_OLVARCIC = space(1)
  w_OPECIC = space(1)
  w_CLQTADIC = 0
  w_CLDICUM1 = 0
  w_WIPFASPRE = space(5)
  w_WIPFASOLD = space(5)
  w_PRIMAFASINT = .f.
  w_CPPRIMAFASINT = 0
  w_FAPRIMAFASINT = 0
  w_WPRIMAFASINT = space(5)
  w_CPFASEEXT = 0
  w_FAFASEEXT = 0
  w_WIPFASEEXT = space(5)
  w_OINTEXT = space(1)
  w_oldlock = .f.
  w_cFunction = space(10)
  w_oMess = .NULL.
  w_oMess1 = .NULL.
  w_oMess2 = .NULL.
  w_oPart = .NULL.
  w_oPart1 = .NULL.
  w_oPart2 = .NULL.
  w_TIPOPE = space(1)
  w_EDITROW = .f.
  w_OLFLMODC = space(1)
  w_CURFASE = .f.
  w_QTAVER = 0
  w_EDITCHK = .f.
  w_FLQTAEVA = 0
  w_FLTSTAOL = space(1)
  w_EDITALT = .f.
  w_OLTSTATO = space(1)
  w_OLEVAAUT = space(1)
  Curso = space(10)
  w_NFASI = 0
  w_L_CANDELROW = space(1)
  w_p_CLCODOCL = space(15)
  w_p_CLCODODL = space(15)
  w_p_CPROWNUM = 0
  w_p_CHECK = space(10)
  w_L_QTADIC = 0
  w_FOUND = .f.
  w_WARN = .f.
  w_STOP = .f.
  w_YesNo = .f.
  w_ULTIMAFASE = 0
  w_DULTIFASE = space(40)
  w_CPULTFASE = 0
  w_CLFASCLA = space(1)
  w_L_CLROWORD = 0
  w_L_CHECK = space(10)
  w_FASECOLA = .f.
  w_SRV = space(1)
  w_COCLCOLL = space(10)
  w_CHIAVE = space(10)
  w_CLCODOCL = space(15)
  w_STATOOCL = space(1)
  TmpC = space(100)
  w_IMLASTPHASE = .f.
  w_IMFIRSTPHASE = .f.
  w_ROPREVPHASE = 0
  w_RNPREVPHASE = 0
  w_DEPREVPHASE = space(40)
  w_RONEXTPHASE = 0
  w_RNNEXTPHASE = 0
  w_DENEXTPHASE = space(40)
  w_L_CMD = space(254)
  w_L_WHERE = space(50)
  w_CURCHECK = 0
  w_TOTCHECK = 0
  w_CHECKERROR = .f.
  w_ROREADPHASE = 0
  w_RNREADPHASE = 0
  w_DEREADPHASE = space(40)
  w_MEMO = space(0)
  w_NOBLOC = space(0)
  w_ANNULLA = .f.
  w_CONFKME = .f.
  w_DAIM = .f.
  GSCO_MCL = .NULL.
  GSCO_MRL = .NULL.
  w_DETT = space(1)
  w_OLTQTSAL = 0
  w_COD_CLA = space(5)
  w_COD_RISO = space(5)
  w_LRUMTATT = space(5)
  w_LRUMTAVV = space(5)
  w_LRUMTLAV = space(5)
  w_LRATTTEM = 0
  w_LRAVVTEM = 0
  w_LRLAVTEM = 0
  w_LRATTSEC = 0
  w_LRAVVSEC = 0
  w_LRLAVSEC = 0
  w_LRNUMIMP = 0
  w_LRPROORA = 0
  w_TIPO = space(2)
  ROWORD = 0
  w_DES_CLA = space(40)
  w_LR__TIPO = space(2)
  w_LRTEMRIS = 0
  w_LRUMTRIS = space(5)
  w_LRTEMSEC = 0
  w_DES_RISO = space(40)
  w_RLINTEST = space(1)
  p_L_CLCODODL = space(15)
  p_L_CPROWNUM = 0
  p_L_CLROWORD = 0
  * --- WorkFile variables
  SCCI_ASF_idx=0
  ODL_CICL_idx=0
  ODL_DETT_idx=0
  COD_FASI_idx=0
  LIS_FORN_idx=0
  CONTI_idx=0
  CIC_MAST_idx=0
  ODL_MAST_idx=0
  CIC_DETT_idx=0
  RIS_MAST_idx=0
  RIS_DETT_idx=0
  RIS_ORSE_idx=0
  CLR_MAST_idx=0
  DET_FASI_idx=0
  MOU_FASI_idx=0
  ODL_MAIN_idx=0
  PAR_PROD_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  MAGAZZIN_idx=0
  TMPODL_CICL_idx=0
  TMPODL_RISO_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Chiamato da GSCO_MCL
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- POSSO ELIMINARE OPPURE AGGIUNGERE OPPURE SPOSTARE UNA FASE:
    * --- 1) La precedente non � dichiarata
    *     2) La precedente � di conto lavoro e non � ordinata
    *     3) la fase corrente � dichiarata oppure � di conto lavoro e ordinata
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- --------------------------------------------------------------------------------------------------
    this.GSCO_MCL = this.oParentObject
    this.GSCO_MRL = this.GSCO_MCL.GSCO_MRL
    this.GSCO_AOP = this.GSCO_MCL.oParentObject
    this.GSCO_MOL = this.GSCO_AOP.GSCO_MOL
    * --- --------------------------------------------------------------------------------------------------
    this.w_CLCODODL = this.GSCO_AOP.w_OLCODODL
    this.w_TRSNAME = this.GSCO_MCL.CTRSNAME
    this.w_CLSERIAL = this.GSCO_AOP.w_OLTSECIC
    this.w_CLROWORD = this.GSCO_MCL.w_CLROWORD
    this.o_CLROWORD = this.GSCO_MCL.o_CLROWORD
    this.w_CLINDPRE = this.GSCO_MCL.w_CLINDPRE
    this.o_CLINDPRE = this.GSCO_MCL.o_CLINDPRE
    this.w_CLCOUPOI = this.GSCO_MCL.w_CLCOUPOI
    this.o_CLCOUPOI = this.GSCO_MCL.o_CLCOUPOI
    this.w_CLFASOUT = this.GSCO_MCL.w_CLFASOUT
    this.o_CLFASOUT = this.GSCO_MCL.o_CLFASOUT
    this.w_CLFASSEL = this.GSCO_MCL.w_CLFASSEL
    this.o_CLFASSEL = this.GSCO_MCL.o_CLFASSEL
    this.w_CLQTAPRE = this.GSCO_MCL.w_CLQTAPRE
    * --- --------------------------------------------------------------------------------------------------
    this.w_OLTQTODL = this.GSCO_AOP.w_OLTQTODL
    this.w_OLTQTOD1 = this.GSCO_AOP.w_OLTQTOD1
    this.w_OLTUNMIS = this.GSCO_AOP.w_OLTUNMIS
    this.w_OLTDINRIC = this.GSCO_AOP.w_OLTDINRIC
    this.w_OLTDTRIC = this.GSCO_AOP.w_OLTDTRIC
    * --- --------------------------------------------------------------------------------------------------
    * --- Il w_CLKEYRIF � stato definito a livello di anagrafica ordine
    this.w_CLKEYRIF = this.GSCO_AOP.w_CLKEYRIF
    this.w_OLVARCIC = "S"
    * --- --------------------------------------------------------------------------------------------------
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Variabili utilizzate per il caricamento del ciclo di lavoro dall'ordine
    this.w_OLTDISBA = this.GSCO_AOP.w_OLTDISBA
    this.w_OLTCILAV = this.GSCO_AOP.w_OLTCILAV
    this.w_CLINDCIC = this.GSCO_AOP.w_CLINDCIC
    this.w_CLDESCIC = this.GSCO_AOP.w_DESCILAV
    this.w_CLGRUPPO = this.w_OLTDISBA
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_oldlock = this.GSCO_AOP.LockScreen
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Recupero stato della form
    this.w_cFunction = Upper(Alltrim(this.GSCO_AOP.cFunction))
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_oMess=createobject("Ah_message")
    this.w_oMess1=createobject("ah_message")
    this.w_oMess2=createobject("ah_message")
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Nome del corsore della Movimentazione
    this.w_OAREA = alias()
    do case
      case this.pAzione = "LoadResourceDetail"
        if g_PRFA="S" and g_CICLILAV="S" and !Empty(this.w_CLSERIAL)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAzione = "DEL-ASSFAS"
        * --- Cancella i dati dello schedulatore (TIPO_FASE='P' � gi� cancellato dal figlio integrato)
        * --- Delete from SCCI_ASF
        i_nConn=i_TableProp[this.SCCI_ASF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCCI_ASF_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"COD_ODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and FAS_CPROWNUM = "+cp_ToStrODBC(this.w_CLROWNUM);
                 )
        else
          delete from (i_cTable) where;
                COD_ODL = this.w_CLCODODL;
                and FAS_CPROWNUM = this.w_CLROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pAzione = "EDITSTARTED"
        if g_PRFA="S" and g_CICLILAV="S"
          * --- Memorizzo i dati prima del salvataggio in modo da poter effettuare i controlli alla fine di tutti gli aggiornamenti
          if !Empty(this.w_CLSERIAL) and this.w_cFunction = "EDIT"
            * --- Delete from TMPODL_CICL
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                     )
            else
              delete from (i_cTable) where;
                    CLKEYRIF = this.w_CLKEYRIF;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Insert into TMPODL_CICL
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCIABTC",this.TMPODL_CICL_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if Empty(this.GSCO_AOP.w_Cur_Cic_Odl)
              this.GSCO_AOP.w_Cur_Cic_Odl = Sys(2015)
            endif
          endif
        endif
      case this.pAzione = "UltFase"
        if g_PRFA="S" and g_CICLILAV="S"
          * --- Carico il ciclo di lavoro CIC_DETT partendo dall' ODL
          * --- Prima di lanciare la generazione dei codici inserisco la testata del ciclo mi serve per avere i riferimenti alla distinta, codice ciclo indice di prefrenza etc....
          * --- Nella routine GSCI_BGC legge i dati da CIC_MAST
          this.w_OLTDISBA = this.GSCO_AOP.w_OLTDISBA
          this.w_CLGRUPPO = this.GSCO_AOP.w_OLTDISBA
          this.w_OLTCILAV = this.GSCO_AOP.w_OLTCILAV
          this.w_CLINDCIC = this.GSCO_AOP.w_CLINDCIC
          this.w_CLDESCIC = this.GSCO_AOP.w_DESCILAV
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          if this.w_cFunction = "LOAD" and this.GSCO_MCL.bupdated
            * --- Caso di inserimento ordine
            if !Empty(this.w_CLSERIAL)
              * --- Controlla le Risorse delle fasi
              * --- Select from GSCIC2BTC
              do vq_exec with 'GSCIC2BTC',this,'_Curs_GSCIC2BTC','',.f.,.t.
              if used('_Curs_GSCIC2BTC')
                select _Curs_GSCIC2BTC
                locate for 1=1
                do while not(eof())
                this.w_MSG = ah_msgformat("Inserire un centro di lavoro nella fase (%1) %2", alltrim(str(_Curs_GSCIC2BTC.CPROWORD)), alltrim(_Curs_GSCIC2BTC.CLFASDES) )
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.w_MSG
                i_retcode = 'stop'
                return
                  select _Curs_GSCIC2BTC
                  continue
                enddo
                use
              endif
            endif
          endif
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          if Inlist( this.w_cFunction , "LOAD" , "EDIT") and this.GSCO_MCL.bupdated
            if !Empty(this.w_CLSERIAL)
              * --- Se � associato un ciclo di lavorazione all'ordine
              *     sistema flag CLULTFAS, le fasi di riferimento e genera codici di fase
              GSCI_BFO(this,this.w_CLCODODL)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          if this.w_cFunction = "EDIT" and this.GSCO_MCL.bupdated
            * --- Se w_NFASDICH = 0 significa che non � stata dichiarate nessuna fase, l'utente pu� effettuare tutte le modifiche che desidera
            * --- Alla fine del salvataggio controllo se le modifiche sono congruenti
            * --- Select from GSCIC6BTC
            do vq_exec with 'GSCIC6BTC',this,'_Curs_GSCIC6BTC','',.f.,.t.
            if used('_Curs_GSCIC6BTC')
              select _Curs_GSCIC6BTC
              locate for 1=1
              do while not(eof())
              do case
                case NVL(_Curs_GSCIC6BTC.OLFASOUT, "N") <> "S" AND NVL(_Curs_GSCIC6BTC.CLFASOUT, "N") = "S" AND NVL(_Curs_GSCIC6BTC.OLDQTVER, 0) <> NVL(_Curs_GSCIC6BTC.CURQTVER, 0) AND NVL(_Curs_GSCIC6BTC.OLDQTVER, 0)=0
                  this.w_MSG = ah_msgformat("La fase (%1) non pu� essere di output perch� dichiarata, inserire una fase intermedia di provenienza interna", alltrim(str(_Curs_GSCIC6BTC.CLROWORD)) )
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MSG
                case NVL(_Curs_GSCIC6BTC.OLULTFAS, "N") <>"S" and NVL(_Curs_GSCIC6BTC.CLULTFAS, "N") ="S" AND NVL(_Curs_GSCIC6BTC.OLDQTVER, 0) > 0
                  this.w_MSG = ah_msgformat("Impossibile impostare la fase (%1) di output perch� dichiarata.%0Eliminare la diciarazione e riprovare.", alltrim(str(_Curs_GSCIC6BTC.CLROWORD)) )
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MSG
              endcase
                select _Curs_GSCIC6BTC
                continue
              enddo
              use
            endif
            if !Empty(this.w_CLSERIAL) and this.w_cFunction = "EDIT"
              * --- Pulisco la tabella temporanea dai vecchi cicli di lavoro
              * --- Delete from TMPODL_CICL
              i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                       )
              else
                delete from (i_cTable) where;
                      CLKEYRIF = this.w_CLKEYRIF;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            endif
          endif
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          if Inlist( this.w_cFunction , "LOAD" , "EDIT") and this.GSCO_MCL.bupdated
            * --- Esegue tempificazione fasi
            *     La tempificazione fasi � stata variata ora vengono tempificate tutte le fasi anche se sono presenti solo fasi interne
            GSCI_BTF(this,this.w_CLCODODL, this.GSCO_AOP.w_OLTDINRIC , this.GSCO_AOP.w_OLTDTRIC )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Devo aggiornare il riferimento alla fase count point di riferimento OLCPRIFE su ODL_DETT 
            *     per settare in maniera corretta la fase di utilizzo dei materiali altrimenti non vengono scaricati
            * --- Write into ODL_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="OLCODODL,OLFASRIF"
              do vq_exec with 'GSCI11BTC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                      +" and "+"ODL_DETT.OLFASRIF = _t2.OLFASRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLCPRIFE = _t2.OLCPRIFE";
                  +i_ccchkf;
                  +" from "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                      +" and "+"ODL_DETT.OLFASRIF = _t2.OLFASRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT, "+i_cQueryTable+" _t2 set ";
                  +"ODL_DETT.OLCPRIFE = _t2.OLCPRIFE";
                  +Iif(Empty(i_ccchkf),"",",ODL_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ODL_DETT.OLCODODL = t2.OLCODODL";
                      +" and "+"ODL_DETT.OLFASRIF = t2.OLFASRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT set (";
                  +"OLCPRIFE";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.OLCPRIFE";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ODL_DETT.OLCODODL = _t2.OLCODODL";
                      +" and "+"ODL_DETT.OLFASRIF = _t2.OLFASRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ODL_DETT set ";
                  +"OLCPRIFE = _t2.OLCPRIFE";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".OLCODODL = "+i_cQueryTable+".OLCODODL";
                      +" and "+i_cTable+".OLFASRIF = "+i_cQueryTable+".OLFASRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLCPRIFE = (select OLCPRIFE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        endif
      case this.pAzione = "SetValueAddeRow"
        SELECT(this.w_TRSNAME)
        this.GSCO_MCL.NotifyEvent("SetEditableRow")     
      case this.pAzione = "SetEditableRow"
        * --- Controllo se posso editare la riga del ciclo di lavorazione sull'ODL
        if this.GSCO_AOP.w_TIPGES = "Z"
          SELECT(this.w_TRSNAME)
          this.GSCO_MCL.w_EDITROW = .F.
          Local l_Ctrl
          l_Ctrl = this.GSCO_MCL.GetBodyCtrl("w_CLROWORD")
          l_Ctrl.ReadOnly = Not(this.w_EDITROW)
          l_Ctrl = .NULL.
        else
          do case
            case this.w_cFunction = "LOAD"
              * --- Nel caso di caricamnto di un nuovo ODL ho poche limitazioni
              SELECT(this.w_TRSNAME)
              this.w_EDITROW = g_PRFA="S" and g_CICLILAV="S" and !Empty(this.w_CLSERIAL)
              this.GSCO_MCL.w_EDITROW = this.w_EDITROW
              this.GSCO_MCL.w_EDITCHK = this.w_EDITROW
              this.GSCO_MCL.mEnableControls()     
              this.GSCO_MCL.w_UM1 = this.w_OLTUNMIS
              this.GSCO_MCL.w_CLQTAPRE = this.w_OLTQTODL
              this.GSCO_MCL.w_CLPREUM1 = this.w_OLTQTOD1
              Local l_Ctrl
              l_Ctrl = this.GSCO_MCL.GetBodyCtrl("w_CLROWORD")
              l_Ctrl.ReadOnly = Not(this.w_EDITROW)
              l_Ctrl = .NULL.
            case this.w_cFunction = "EDIT"
              * --- Leggo i valori dalla gestione
              this.w_FLQTAEVA = this.GSCO_MCL.w_FLQTAEVA
              this.w_FLTPROVE = this.GSCO_MCL.w_FLTPROVE
              this.w_FLTSTAOL = this.GSCO_MCL.w_FLTSTAOL
              this.w_EDITROW = g_PRFA="S" and g_CICLILAV="S" and !Empty(this.w_CLSERIAL)
              if !Empty(this.GSCO_MCL.w_CLSEOLFA)
                if this.w_FLQTAEVA=0
                  * --- Controllo la quantit� versata
                  this.w_EDITROW = .T.
                endif
                * --- Controllo sullo stato
                do case
                  case this.w_FLTPROVE="L"
                    this.w_EDITROW = this.w_EDITROW and !this.GSCO_MCL.w_FLTSTAOL $ "F-L"
                  case this.w_FLTPROVE="I"
                    this.w_EDITROW = this.w_EDITROW and this.GSCO_MCL.w_FLTSTAOL <> "F"
                  otherwise
                    this.w_EDITROW = .F.
                endcase
              endif
              this.GSCO_MCL.w_EDITROW = this.w_EDITROW
              this.GSCO_MCL.w_EDITCHK = this.w_EDITROW
              this.GSCO_MCL.mEnableControls()     
              Local l_Ctrl
              l_Ctrl = this.GSCO_MCL.GetBodyCtrl("w_CLROWORD")
              l_Ctrl.ReadOnly = Not(this.w_EDITROW)
              l_Ctrl = .NULL.
              * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
              if .F.
                if this.GSCO_MCL.w_CLQTAPRE=0
                  this.GSCO_MCL.w_UM1 = this.w_OLTUNMIS
                  this.GSCO_MCL.w_CLQTAPRE = this.w_OLTQTODL
                  this.GSCO_MCL.w_CLPREUM1 = this.w_OLTQTOD1
                endif
                this.w_CURFASE = .T.
                * --- Controllo se ci sono delle dichiarazioni per la fase
                this.w_QTAVER = this.GSCO_MCL.w_FLQTAEVA
                * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                * --- Controllo se per questa fase ci sono fasi alternative
                this.w_OPOS = recno()
                select count(*) as alte from (this.w_TRSNAME) into cursor _alte_ where t_CLROWORD=this.w_CLROWORD
                this.w_EDITALT = _alte_.alte > 1
                USE IN SELECT("_alte_")
                if this.w_oPOS > 0
                  select (this.w_TRSNAME)
                  count to this.w_TMPN for not deleted()
                  if this.w_oPOS <= this.w_TMPN
                    goto this.w_OPOS
                  endif
                endif
                * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                SELECT(this.w_TRSNAME)
                this.w_EDITROW = g_PRFA="S" and g_CICLILAV="S" and !Empty(this.w_CLSERIAL)
                Local l_Ctrl
                l_Ctrl = this.GSCO_MCL.GetBodyCtrl("w_CLROWORD")
                do case
                  case this.w_QTAVER>0
                    this.w_EDITROW = .F.
                    this.GSCO_MCL.w_EDITCHK = this.w_EDITROW
                    this.GSCO_MCL.w_EDITROW = this.w_EDITROW
                    this.GSCO_MCL.w_EDITALT = this.w_EDITALT
                    l_Ctrl.ReadOnly = Not(this.w_EDITROW) Or Not(this.GSCO_MCL.FullRow())
                    l_Ctrl = .NULL.
                  case this.w_QTAVER=0
                    if !Empty(this.w_CLSERIAL)
                      this.GSCO_MCL.w_EDITCHK = .T.
                      this.GSCO_MCL.w_EDITROW = .T.
                      l_Ctrl.ReadOnly = .F. Or Not(this.GSCO_MCL.FullRow())
                      l_Ctrl = .NULL.
                    else
                      this.GSCO_MCL.w_EDITCHK = .T.
                      this.GSCO_MCL.w_EDITROW = .F.
                      l_Ctrl.ReadOnly = .T. Or Not(this.GSCO_MCL.FullRow())
                      l_Ctrl = .NULL.
                    endif
                endcase
              endif
            otherwise
              SELECT(this.w_TRSNAME)
              this.GSCO_MCL.w_EDITROW = .T.
          endcase
        endif
      case this.pAzione = "CanAddRow"
        this.oParentObject.w_CANADDROW = .T.
        if this.GSCO_AOP.w_TIPGES = "Z"
          this.GSCO_MCL.w_CANADDROW = .F.
        else
          do case
            case this.w_cFunction = "LOAD"
              this.oParentObject.w_CANADDROW = .T.
            case this.w_cFunction = "EDIT"
              if this.GSCO_MCL.w_CLULTFAS="S" And this.GSCO_MCL.w_FLQTAEVA > 0 OR this.GSCO_AOP.w_OLFLMODC<>"S"
                this.oParentObject.w_CANADDROW = .F.
              endif
            otherwise
              this.oParentObject.w_CANADDROW = .F.
          endcase
        endif
      case Inlist(this.pAzione, "CanDeleteRow", "CanDeactRow" , "ChangeOrder")
        Create Cursor _SettingsUtente_ (CPROWNUM N(4), CODOCL C(15), STATOOCL C(1))
        =WRCURSOR("_SettingsUtente_")
        do case
          case this.pAzione="CanDeleteRow"
            this.GSCO_MCL.w_CANDELROW = True
            this.GSCO_MCL.w_CANCHROW = False
            this.GSCO_MCL.w_CANDEAROW = False
          case this.pAzione = "CanDeactRow"
            this.GSCO_MCL.w_CANDEAROW = True
            this.GSCO_MCL.w_CANCHROW = False
            this.GSCO_MCL.w_CANDELROW = False
          case this.pAzione = "ChangeOrder"
            this.GSCO_MCL.w_CANCHROW = True
            this.GSCO_MCL.w_CANDELROW = False
            this.GSCO_MCL.w_CANDEAROW = False
        endcase
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case this.pAzione = "CanDeactRow"
            if !this.GSCO_MCL.w_CANDEAROW
              * --- Non � stata permessa la disattivazione della fase (riassegno valore old))
              this.GSCO_MCL.w_CLFASSEL = this.o_CLFASSEL
              this.GSCO_MCL.w_CLCOUPOI = this.o_CLCOUPOI
              this.GSCO_MCL.w_CLFASOUT = this.o_CLFASOUT
              SELECT(this.w_TRSNAME)
              REPLACE I_SRV WITH this.oParentObject.w_ROWSTATUS
            else
              this.GSCO_MCL.w_UPDATED = .F.
              this.GSCO_MOL.MarkPos()     
              this.GSCO_MOL.FirstRow()     
              do while Not this.GSCO_MOL.Eof_Trs()
                this.GSCO_MOL.SetRow()     
                if this.GSCO_MOL.FullRow() and this.GSCO_MOL.w_OLFASRIF = this.w_CLROWORD
                  this.GSCO_MOL.w_OLFASRIF = 0
                  this.GSCO_MOL.w_FASEPREF = this.GSCO_MCL.w_CLINDPRE
                  this.GSCO_MOL.Notifyevent("w_OLFASRIF Changed")     
                  this.GSCO_MOL.mEnableControls()     
                  this.GSCO_MOL.SaveRow()     
                endif
                * --- Passo alla prossima riga...
                this.GSCO_MOL.NextRow()     
              enddo
              this.GSCO_MOL.RePos(.t.)     
            endif
          case this.pAzione = "ChangeOrder"
            if !this.GSCO_MCL.w_CANCHROW
              * --- Non � stata permessa la variazione della sequenza della fase (riassegno valore old))
              this.GSCO_MCL.w_CLROWORD = this.o_CLROWORD
              this.GSCO_MCL.w_CLCOUPOI = this.o_CLCOUPOI
              this.GSCO_MCL.w_CLFASOUT = this.o_CLFASOUT
              SELECT(this.w_TRSNAME)
              REPLACE I_SRV WITH this.oParentObject.w_ROWSTATUS
            else
              this.GSCO_MCL.w_UPDATED = .F.
              this.GSCO_MOL.MarkPos()     
              this.GSCO_MOL.FirstRow()     
              do while Not this.GSCO_MOL.Eof_Trs()
                this.GSCO_MOL.SetRow()     
                if this.GSCO_MOL.FullRow() and this.GSCO_MOL.w_OLFASRIF = this.o_CLROWORD
                  this.GSCO_MOL.w_OLFASRIF = this.GSCO_MCL.w_CLROWORD
                  this.GSCO_MOL.w_FASEPREF = this.GSCO_MCL.w_CLINDPRE
                  this.GSCO_MOL.Notifyevent("w_OLFASRIF Changed")     
                  this.GSCO_MOL.mEnableControls()     
                  this.GSCO_MOL.SaveRow()     
                endif
                * --- Passo alla prossima riga...
                this.GSCO_MOL.NextRow()     
              enddo
              this.GSCO_MOL.RePos(.t.)     
            endif
        endcase
        USE IN SELECT("_SettingsUtente_")
      case this.pAzione = "BeforeRowDeleted"
        * --- Posso eliminare la fase la elimino dal temporaneo
        * --- Delete from TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.GSCO_MCL.w_CPROWDEL);
                +" and CLROWORD = "+cp_ToStrODBC(this.GSCO_MCL.w_ROWORDEL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and CLCODODL = this.w_CLCODODL;
                and CPROWNUM = this.GSCO_MCL.w_CPROWDEL;
                and CLROWORD = this.GSCO_MCL.w_ROWORDEL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPODL_RISO
        i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and RLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and RLROWNUM = "+cp_ToStrODBC(this.GSCO_MCL.w_CPROWDEL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and RLCODODL = this.w_CLCODODL;
                and RLROWNUM = this.GSCO_MCL.w_CPROWDEL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pAzione = "RowDeleted"
        this.GSCO_MCL.MarkPos()     
        this.GSCO_AOP.__Dummy__.Enabled = .T.
        this.GSCO_AOP.__Dummy__.SetFocus()     
        this.GSCO_MCL.w_UPDATED = .F.
        * --- Aggiorno le fasi nel temporaneo
        this.GSCO_MOL.NotifyEvent("INSFASI")     
        * --- Se posso eliminare/disattivarespostare la fase allora modifico la lista dei materiali stimati
        *     A questo punto libero i materiali presenti sull'ordine associati alla fase di conto lavoro
        this.GSCO_MOL.MarkPos()     
        this.GSCO_MOL.FirstRow()     
        do while Not this.GSCO_MOL.Eof_Trs()
          this.GSCO_MOL.SetRow()     
          if this.GSCO_MOL.w_OLFASRIF = this.GSCO_MCL.w_ROWORDEL
            this.GSCO_MOL.w_OLQTAEVA = 0
            this.GSCO_MOL.w_OLQTAEV1 = 0
            this.GSCO_MOL.w_OLFLEVAS = "N"
            this.GSCO_MOL.w_OLEVAAUT = "N"
            * --- Memorizzo il vecchio valore della fase che mi serve per sostituirlo con quello nuovo dopo aver eliminato la fase
            this.GSCO_MOL.w_OLFASRIF = 0
            * --- Azzero il riferimento della fase
            this.GSCO_MOL.Notifyevent("w_OLFASRIF Changed")     
            this.GSCO_MOL.mEnableControls()     
            this.GSCO_MOL.SaveRow()     
          endif
          this.GSCO_MOL.NextRow()     
        enddo
        this.GSCO_MOL.RePos(.T.)     
        this.GSCO_AOP.__Dummy__.Enabled = .T.
        this.GSCO_MCL.RePos()     
        Local l_Ctrl
        l_Ctrl = this.GSCO_MCL.GetBodyCtrl("w_CLROWORD")
        l_Ctrl.SetFocus()
        l_Ctrl = .NULL.
      case this.pAzione = "ChangeResource"
        this.Curso = SYS(2015)
        this.w_NFASI = 0
        this.GSCO_MRL.MarkPos()     
        l_cmd = " CPROWNUM, min(t_rlintest) as t_rlintest "
        this.GSCO_MRL.Exec_Select(this.Curso , l_cmd, "t_rl__tipo=1 and t_rlintest=1", " ", "CPROWNUM", " " )     
        this.GSCO_MRL.RePos()     
        this.w_NFASI = Reccount(this.Curso)
        if this.w_NFASI > 0 AND this.GSCO_MCL.w_CLFASCLA <> "S"
          this.GSCO_MCL.w_CLFASCLA = "S"
          this.GSCO_MCL.mCalc(.t.)     
        else
          if this.w_NFASI = 0 AND this.GSCO_MCL.w_CLFASCLA = "S"
            this.GSCO_MCL.w_CLFASCLA = "N"
            this.GSCO_MCL.mCalc(.t.)     
          endif
        endif
        use in select(this.Curso)
        this.GSCO_MCL.w_UPDATED = .F.
        * --- Delete from TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.GSCO_MCL.w_CPROWDEL);
                +" and CLROWORD = "+cp_ToStrODBC(this.GSCO_MCL.w_ROWORDEL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and CLCODODL = this.w_CLCODODL;
                and CPROWNUM = this.GSCO_MCL.w_CPROWDEL;
                and CLROWORD = this.GSCO_MCL.w_ROWORDEL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPODL_RISO
        i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_CLKEYRIF);
                +" and RLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and RLROWNUM = "+cp_ToStrODBC(this.GSCO_MCL.w_CPROWDEL);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_CLKEYRIF;
                and RLCODODL = this.w_CLCODODL;
                and RLROWNUM = this.GSCO_MCL.w_CPROWDEL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Aggiorno le fasi nel temporaneo
        this.GSCO_MOL.NotifyEvent("INSFASI")     
        this.GSCO_MOL.MarkPos()     
        this.GSCO_MOL.FirstRow()     
        do while Not this.GSCO_MOL.Eof_Trs()
          this.GSCO_MOL.SetRow()     
          this.GSCO_MOL.SaveDependsOn()     
          if this.GSCO_MOL.FullRow() and this.GSCO_MOL.w_OLFASRIF = this.GSCO_MCL.w_CLROWORD
            this.GSCO_MOL.w_OLFASRIF = this.GSCO_MCL.w_CLROWORD
            this.GSCO_MOL.w_FASEPREF = this.GSCO_MCL.w_CLINDPRE
            SELECT(this.GSCO_MOL.cTrsName)
            Scatter TO this.GSCO_MOL.RowData
            this.GSCO_MOL.Notifyevent("w_OLFASRIF Changed")     
            this.GSCO_MOL.mEnableControls()     
            this.GSCO_MOL.SaveRow()     
          endif
          * --- Passo alla prossima riga...
          this.GSCO_MOL.NextRow()     
        enddo
        this.GSCO_MOL.RePos(.t.)     
      case this.pAzione = "DeleteMater"
      case this.pAzione = "UpdateMater"
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -----------------------------------------------------------------------------------------------------
    this.GSCO_MRL = this.GSCO_MCL.GSCO_MRL
    * --- Leggo quantit� del saldo
    this.w_OLTQTSAL = this.GSCO_AOP.w_OLTQTOD1
    * --- --Lettura codice Fase 
    * --- Read from COD_FASI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COD_FASI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_FASI_idx,2],.t.,this.COD_FASI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CFINDETT"+;
        " from "+i_cTable+" COD_FASI where ";
            +"CFCODICE = "+cp_ToStrODBC(this.oParentObject.w_CL__FASE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CFINDETT;
        from (i_cTable) where;
            CFCODICE = this.oParentObject.w_CL__FASE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DETT = NVL(cp_ToDate(_read_.CFINDETT),cp_NullValue(_read_.CFINDETT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_DETT="S"
      if not empty(this.GSCO_MRL.CTRSNAME) 
        Select (this.GSCO_MRL.CTRSNAME)
        delete all
      endif
      this.ROWORD = 0
      * --- Select from DET_FASI
      i_nConn=i_TableProp[this.DET_FASI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_FASI_idx,2],.t.,this.DET_FASI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DET_FASI ";
            +" where DFCODICE="+cp_ToStrODBC(this.oParentObject.w_CL__FASE)+"";
            +" order by CPROWORD";
             ,"_Curs_DET_FASI")
      else
        select * from (i_cTable);
         where DFCODICE=this.oParentObject.w_CL__FASE;
         order by CPROWORD;
          into cursor _Curs_DET_FASI
      endif
      if used('_Curs_DET_FASI')
        select _Curs_DET_FASI
        locate for 1=1
        do while not(eof())
        * --- --Modifica transitorio
        this.GSCO_MRL.AddRow()     
        this.ROWORD = this.ROWORD+1
        this.w_COD_CLA = NVL(_Curs_DET_FASI.DFCODCLA,space(5))
        * --- Tipologia risorsa
        this.w_LR__TIPO = _Curs_DET_FASI.DF__TIPO
        * --- Risorsa
        this.w_COD_RISO = _Curs_DET_FASI.DFCODRIS
        this.w_LRUMTRIS = _Curs_DET_FASI.DFUMTRIS
        * --- --CALCOLO TEMPI 
        * --- Read from RIS_ORSE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RLCODICE,RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS"+;
            " from "+i_cTable+" RIS_ORSE where ";
                +"RL__TIPO = "+cp_ToStrODBC(this.w_LR__TIPO);
                +" and RLCODICE = "+cp_ToStrODBC(this.w_COD_RISO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RLCODICE,RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS;
            from (i_cTable) where;
                RL__TIPO = this.w_LR__TIPO;
                and RLCODICE = this.w_COD_RISO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COD_RISO = NVL(cp_ToDate(_read_.RLCODICE),cp_NullValue(_read_.RLCODICE))
          this.w_DES_RISO = NVL(cp_ToDate(_read_.RLDESCRI),cp_NullValue(_read_.RLDESCRI))
          this.w_RLINTEST = NVL(cp_ToDate(_read_.RLINTEST),cp_NullValue(_read_.RLINTEST))
          w_UMTDEF = NVL(cp_ToDate(_read_.RLUMTDEF),cp_NullValue(_read_.RLUMTDEF))
          w_QTARIS = NVL(cp_ToDate(_read_.RLQTARIS),cp_NullValue(_read_.RLQTARIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.GSCO_MRL.w_RL__TIPO = this.w_LR__TIPO
        this.GSCO_MRL.w_RLCODICE = this.w_COD_RISO
        this.GSCO_MRL.w_RLDESCRI = this.w_DES_RISO
        this.GSCO_MRL.w_RLINTEST = this.w_RLINTEST
        this.GSCO_MRL.w_RLTIPTEM = _Curs_DET_FASI.DFTIPTEM
        this.GSCO_MRL.w_RLQTARIS = _Curs_DET_FASI.DFQTARIS
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMDURSEC"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_LRUMTRIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMDURSEC;
            from (i_cTable) where;
                UMCODICE = this.w_LRUMTRIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_CONSEC = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_LRTEMRIS = _Curs_DET_FASI.DFTEMRIS
        this.w_LRTEMSEC = this.w_LRTEMRIS*w_CONSEC
        if NOT EMPTY(this.w_COD_CLA)
          * --- Read from CLR_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLR_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLR_MAST_idx,2],.t.,this.CLR_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CSDESCLA"+;
              " from "+i_cTable+" CLR_MAST where ";
                  +"CSCODCLA = "+cp_ToStrODBC(this.w_COD_CLA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CSDESCLA;
              from (i_cTable) where;
                  CSCODCLA = this.w_COD_CLA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DES_CLA = NVL(cp_ToDate(_read_.CSDESCLA),cp_NullValue(_read_.CSDESCLA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_LRNUMIMP = IIF(NVL(_Curs_DET_FASI.DFNUMIMP,0)=0, 1, _Curs_DET_FASI.DFNUMIMP) 
        this.w_LRPROORA = iif(this.w_LRTEMSEC=0, 0, 3600/this.w_LRTEMSEC*IIF(this.w_LR__TIPO="AT", this.w_LRNUMIMP, 1))
        this.GSCO_MRL.w_CPROWORD = this.ROWORD*10
        this.GSCO_MRL.w_RLCODCLA = NVL(_Curs_DET_FASI.DFCODCLA,space(5))
        this.GSCO_MRL.w_UMTDEF = w_UMTDEF
        this.GSCO_MRL.w_QTARIS = w_QTARIS
        this.GSCO_MRL.w_CONSEC = w_CONSEC
        this.GSCO_MRL.w_RLUMTRIS = this.w_LRUMTRIS
        this.GSCO_MRL.w_RLTEMRIS = this.w_LRTEMRIS
        this.GSCO_MRL.w_RLTEMSEC = this.w_LRTEMSEC
        this.GSCO_MRL.w_RLNUMIMP = IIF(NVL(_Curs_DET_FASI.DFNUMIMP,0)=0, 1, _Curs_DET_FASI.DFNUMIMP) 
        this.GSCO_MRL.w_RLTMPCIC = iif(this.w_LRTEMRIS > 0 , this.w_LRTEMRIS * this.w_LRNUMIMP, 0)
        this.GSCO_MRL.w_RLTCONSS = 0
        this.GSCO_MRL.mCalc(.t.)     
        this.GSCO_MRL.SaveRow()     
          select _Curs_DET_FASI
          continue
        enddo
        use
      endif
       SELECT (this.GSCO_MRL.CTRSNAME) 
 GO TOP 
 With this.GSCO_MRL 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
    endif
    this.GSCO_MCL.NotifyEvent("ChangeResource")     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prima di cambiare ciclo, verifica se ci sono degli OCL di fase collegati
    this.w_STOP = FALSE
    this.w_WARN = FALSE
    this.w_YesNo = .F.
    * --- Controlli preliminari per verificare se posso disattivare / eliminare la fase
    this.w_ULTIMAFASE = 0
    this.w_DULTIFASE = " "
    this.w_CPULTFASE = 0
    * --- memorizzo il numero della fase
    * --- Verifico se la prima/ultima fase � una fase di conto lavoro
    *     Controllo se la fase ha generato degli OCL
    this.w_L_CHECK = SYS(2015)
    this.w_SRV = this.GSCO_MCL.RowStatus()
    if this.w_SRV="A"
      do case
        case this.pAzione="CanDeleteRow"
          this.GSCO_MCL.w_CANDELROW = True
          i_retcode = 'stop'
          return
        case this.pAzione = "CanDeactRow"
          this.GSCO_MCL.w_CANDEAROW = True
          i_retcode = 'stop'
          return
        case this.pAzione = "ChangeOrder"
          this.GSCO_MCL.w_CANCHROW = True
      endcase
    endif
    this.w_COCLCOLL = SYS(2015)
    * --- Faccio una query per recuperare tutti gli ocl di fase legati all'ordine
    VQ_EXEC("..\PRFA\EXE\QUERY\GSCIOBTC", this, this.w_COCLCOLL)
    SELECT(this.w_COCLCOLL)
    INDEX ON OLTSEODL+STR(OLTFAODL) TAG ORDINE
    * --- Controllo se la fase corrente ha generato un ordine di conto lavoro
    this.w_CLCODOCL = SPACE(15)
    this.w_STATOOCL = SPACE(1)
    this.Pag4("CONTROLLO",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,0,this.w_L_CHECK)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    do case
      case this.w_L_CANDELROW = "Y"
        this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
        this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
        this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
        * --- Prima di tutto verifico se la fase ha gi� generato l'ordine di fase
        *     Se si verifico lo stato e se ci sono dei materiali gi� trasferiti in modo da mostrare un avviso
      case this.w_L_CANDELROW = "S"
        this.GSCO_MCL.w_CANDELROW = False
        this.GSCO_MCL.w_CANDEAROW = False
        this.GSCO_MCL.w_CANCHROW = False
        this.w_CHECKERROR = .T.
        do case
          case this.pAzione="CanDeleteRow"
            this.TmpC = "Impossibile eliminare la fase [%1 - %2] , la fase ha generato l'ordine di conto lavoro di fase %3 con stato %4."
          case this.pAzione = "CanDeactRow"
            this.TmpC = "Impossibile disattivare la fase [%1 - %2] , la fase ha generato l'ordine di conto lavoro di fase %3 con stato %4."
          case this.pAzione = "ChangeOrder"
            this.TmpC = "Impossibile modificare la squenza della fase [%1 - %2] , la fase ha generato l'ordine di conto lavoro di fase %3 con stato %4."
        endcase
        * --- Notifico che non posso eliminare la fase ha generato OCL con stato maggiore di da ordinare
        ah_ErrorMsg(this.TmpC,16,"", ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,this.w_CLCODOCL, this.w_STATOOCL )
      case this.w_L_CANDELROW = "W"
        * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
        this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
        this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
        this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
        do case
          case this.pAzione="CanDeleteRow"
            this.TmpC = "Eliminando la fase [%1 - %2] verr� eliminato anche l'ordine di conto lavoto di fase %3."
          case this.pAzione = "CanDeactRow"
            this.TmpC = "Disattivando la fase [%1 - %2] verr� eliminato anche l'ordine di conto lavoto di fase %3."
          case this.pAzione = "ChangeOrder"
            this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� eliminato anche l'ordine di conto lavoto di fase %3."
        endcase
        this.TmpC = AH_MSGFORMAT(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,this.w_CLCODOCL )
        this.w_oMess.addMsgPartNL(this.TmpC)     
      case this.w_L_CANDELROW = "D"
        this.GSCO_MCL.w_CANDELROW = False
        this.GSCO_MCL.w_CANDEAROW = False
        this.GSCO_MCL.w_CANCHROW = False
        this.w_CHECKERROR = .T.
    endcase
    * --- Se posso eliminare la fase devo verificare se la fase successiva ha generato un OCL di fase con stato maggiore di Da Ordinare
    *     Nel caso non sia di conto lavoro oppure abbia generato un OCL con stato minore di ordinato la posso eliminare
    if !this.w_CHECKERROR
      * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      this.w_L_CLROWORD = this.GSCO_MCL.o_CLROWORD
      this.w_CLFASCLA = this.GSCO_MCL.w_CLFASCLA
      this.GSCO_MCL.MarkPos()     
      * --- Verifico se la fase che che sto per eliminare/disattivare/spostare � l'ultima oppure la prima
      * --- Verifico se sono l'ultima fase
      this.Pag5("LASTPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMLASTPHASE
        * --- Non sono l'ultima fase verifico se sono la prima
        this.Pag5("FIRSTPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Ho passato il test della fase corrente effetuo altri controlli
      this.w_CURCHECK = 1
      this.w_CHECKERROR = .F.
      do case
        case this.w_IMFIRSTPHASE Or this.w_IMLASTPHASE
          do case
            case this.pAzione = "ChangeOrder"
              this.w_TOTCHECK = 2
            otherwise
              this.w_TOTCHECK = 1
          endcase
        otherwise
          do case
            case this.pAzione = "ChangeOrder"
              this.w_TOTCHECK = 2
            otherwise
              this.w_TOTCHECK = 1
          endcase
      endcase
      do while this.w_CURCHECK <= this.w_TOTCHECK AND !this.w_CHECKERROR
        do case
          case (this.w_IMFIRSTPHASE OR this.w_IMLASTPHASE) AND this.w_CURCHECK=1
            do case
              case this.w_IMFIRSTPHASE
                this.Pag5("NEXTPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Controllo se posso modificare la fase successiva
                this.w_ROREADPHASE = this.w_RONEXTPHASE
                this.w_RNREADPHASE = this.w_RNNEXTPHASE
                this.w_DEREADPHASE = this.w_DENEXTPHASE
              case this.w_IMLASTPHASE
                * --- Se non ha generato OCL di fase controllo se � stata dichiarata per verificare se posso impostare il flag ultima fase sulla precedente
                * --- Recupero dati fase precedente
                this.Pag5("PREVPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Controllo se posso modificare la fase precedente
                this.w_ROREADPHASE = this.w_ROPREVPHASE
                this.w_RNREADPHASE = this.w_RNPREVPHASE
                this.w_DEREADPHASE = this.w_DEPREVPHASE
            endcase
            this.Pag4("CONTROLLO",this.w_CLCODODL,this.w_RNREADPHASE,this.w_ROREADPHASE,this.w_L_CHECK)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            do case
              case this.w_L_CANDELROW = "Y"
                this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
              case this.w_L_CANDELROW = "S"
                this.GSCO_MCL.w_CANDELROW = False
                this.GSCO_MCL.w_CANDEAROW = False
                this.GSCO_MCL.w_CANCHROW = False
                this.w_CHECKERROR = .T.
                do case
                  case this.w_IMFIRSTPHASE
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Impossibile eliminare la fase [%1 - %2], la fase successiva [%3 - %4] ha generato l'ordine di conto lavoro %5 in stato %6."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Impossibile disattivare la fase [%1 - %2], la fase successiva [%3 - %4] ha generato l'ordine di conto lavoro %5 in stato %6."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Impossibile cambiare la sequenza della fase [%1 - %2], la fase successiva [%3 - %4] ha generato l'ordine di conto lavoro %5 in stato %6."
                    endcase
                  case this.w_IMLASTPHASE
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Impossibile eliminare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Impossibile disattivare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Impossibile cambiare la sequenza della fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    endcase
                endcase
                this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL, this.w_STATOOCL )
                ah_ErrorMsg(this.TmpC,16,"")
              case this.w_L_CANDELROW = "W"
                * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
                this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                do case
                  case this.w_IMFIRSTPHASE
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    endcase
                  case this.w_IMLASTPHASE
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    endcase
                endcase
                this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL )
                this.w_oMess.addMsgPartNL(this.TmpC)     
              case this.w_L_CANDELROW = "D"
                this.GSCO_MCL.w_CANDELROW = False
                this.GSCO_MCL.w_CANDEAROW = False
                this.GSCO_MCL.w_CANCHROW = False
                this.w_CHECKERROR = .T.
            endcase
          case !this.w_IMFIRSTPHASE AND !this.w_IMLASTPHASE AND this.w_CURCHECK=1
            * --- Se entro in questo ramo significa che l'utente vuole modificare la sequenza della fase
            *     Esempio fase 40 deve diventare la 20 devo verificare se posso inserirla tra la 10 e la 30
            * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
            this.Pag5("NEXTPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_ROREADPHASE = this.w_RONEXTPHASE
            this.w_RNREADPHASE = this.w_RNNEXTPHASE
            this.w_DEREADPHASE = this.w_DENEXTPHASE
            * --- Imposto il controllo a Tutto Ok prima di verificare se esiste
            this.w_L_CANDELROW = "Y"
            * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
            if this.w_ROREADPHASE > 0
              this.Pag4("CONTROLLO",this.w_CLCODODL,this.w_RNREADPHASE,this.w_ROREADPHASE,this.w_L_CHECK)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case this.w_L_CANDELROW = "Y"
                  this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                  this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                  this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                case this.w_L_CANDELROW = "S"
                  this.GSCO_MCL.w_CANDELROW = False
                  this.GSCO_MCL.w_CANDEAROW = False
                  this.GSCO_MCL.w_CANCHROW = False
                  this.w_CHECKERROR = .T.
                  do case
                    case this.pAzione="CanDeleteRow"
                      this.TmpC = "Impossibile eliminare la fase [%1 - %2]%0la fase [%3 - %4] ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    case this.pAzione = "CanDeactRow"
                      this.TmpC = "Impossibile disattivare la fase [%1 - %2]%0la fase [%3 - %4] ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    case this.pAzione = "ChangeOrder"
                      this.TmpC = "Impossibile cambiare la sequenza della fase [%1 - %2]%0la fase [%3 - %4] ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                  endcase
                  this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL, this.w_STATOOCL )
                  ah_ErrorMsg(this.TmpC,16,"")
                case this.w_L_CANDELROW = "W"
                  * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
                  this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                  this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                  this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                  do case
                    case this.pAzione="CanDeleteRow"
                      this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    case this.pAzione = "CanDeactRow"
                      this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    case this.pAzione = "ChangeOrder"
                      this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                  endcase
                  this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL )
                  this.w_oMess.addMsgPartNL(this.TmpC)     
                case this.w_L_CANDELROW = "D"
                  this.GSCO_MCL.w_CANDELROW = False
                  this.GSCO_MCL.w_CANDEAROW = False
                  this.GSCO_MCL.w_CANCHROW = False
                  this.w_CHECKERROR = .T.
              endcase
            endif
            if !this.w_CHECKERROR
              * --- Verifico se esiste la fase precedente e recupero i dati che mi servono
              this.Pag5("PREVPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.o_CLROWORD)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_ROREADPHASE = this.w_ROPREVPHASE
              this.w_RNREADPHASE = this.w_RNPREVPHASE
              this.w_DEREADPHASE = this.w_DEPREVPHASE
              * --- Imposto il controllo a Tutto Ok prima di verificare se esiste
              this.w_L_CANDELROW = "Y"
              * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
              if this.w_ROREADPHASE > 0
                this.Pag4("CONTROLLO",this.w_CLCODODL,this.w_RNREADPHASE,this.w_ROREADPHASE,this.w_L_CHECK)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                do case
                  case this.w_L_CANDELROW = "Y"
                    this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                    this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                    this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                  case this.w_L_CANDELROW = "S"
                    this.GSCO_MCL.w_CANDELROW = False
                    this.GSCO_MCL.w_CANDEAROW = False
                    this.GSCO_MCL.w_CANCHROW = False
                    this.w_CHECKERROR = .T.
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Impossibile eliminare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Impossibile disattivare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Impossibile cambiare la sequenza della fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    endcase
                    this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL, this.w_STATOOCL )
                    ah_ErrorMsg(this.TmpC,16,"")
                  case this.w_L_CANDELROW = "W"
                    * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
                    this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                    this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                    this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    endcase
                    this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL )
                    this.w_oMess.addMsgPartNL(this.TmpC)     
                  case this.w_L_CANDELROW = "D"
                    this.GSCO_MCL.w_CANDELROW = False
                    this.GSCO_MCL.w_CANDEAROW = False
                    this.GSCO_MCL.w_CANCHROW = False
                    this.w_CHECKERROR = .T.
                endcase
              endif
            endif
          case this.w_CURCHECK=2
            * --- Se entro in questo ramo significa che l'utente vuole modificare la sequenza della fase
            *     Esempio fase 40 deve diventare la 20 devo verificare se posso inserirla tra la 10 e la 30
            * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
            this.Pag5("NEXTPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.w_CLROWORD)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_ROREADPHASE = this.w_RONEXTPHASE
            this.w_RNREADPHASE = this.w_RNNEXTPHASE
            this.w_DEREADPHASE = this.w_DENEXTPHASE
            * --- Imposto il controllo a Tutto Ok prima di verificare se esiste
            this.w_L_CANDELROW = "Y"
            * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
            if this.w_ROREADPHASE > 0
              this.Pag4("CONTROLLO",this.w_CLCODODL,this.w_RNREADPHASE,this.w_ROREADPHASE,this.w_L_CHECK)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case this.w_L_CANDELROW = "Y"
                  this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                  this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                  this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                case this.w_L_CANDELROW = "S"
                  this.GSCO_MCL.w_CANDELROW = False
                  this.GSCO_MCL.w_CANDEAROW = False
                  this.GSCO_MCL.w_CANCHROW = False
                  this.w_CHECKERROR = .T.
                  do case
                    case this.pAzione="CanDeleteRow"
                      this.TmpC = "Impossibile eliminare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    case this.pAzione = "CanDeactRow"
                      this.TmpC = "Impossibile disattivare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    case this.pAzione = "ChangeOrder"
                      this.TmpC = "Impossibile cambiare la sequenza della fase da [%1 - %2] a  [%3 - %4]%0Impossibile impostare la fase [%5 - %6] come successiva, la fase ha generato l'ordine di conto lavoro di fase %7 con stato %8."
                      this.TmpC = "Impossibile modificare l'imput della fase [%1 - %2] da [%3 - %4] a  [%5 - %6], la fase ha generato l'ordine di conto lavoro di fase %7 con stato %8."
                  endcase
                  this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_CLROWORD)), ALLTRIM(this.w_CLDESFAS), this.w_CLCODOCL, this.w_STATOOCL )
                  ah_ErrorMsg(this.TmpC,16,"")
                case this.w_L_CANDELROW = "W"
                  * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
                  this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                  this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                  this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                  do case
                    case this.pAzione="CanDeleteRow"
                      this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    case this.pAzione = "CanDeactRow"
                      this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    case this.pAzione = "ChangeOrder"
                      this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                  endcase
                  this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL )
                  this.w_oMess.addMsgPartNL(this.TmpC)     
                case this.w_L_CANDELROW = "D"
                  this.GSCO_MCL.w_CANDELROW = False
                  this.GSCO_MCL.w_CANDEAROW = False
                  this.GSCO_MCL.w_CANCHROW = False
                  this.w_CHECKERROR = .T.
              endcase
            endif
            if !this.w_CHECKERROR
              * --- Verifico se esiste la fase precedente e recupero i dati che mi servono
              this.Pag5("PREVPHASE",this.w_CLCODODL,this.GSCO_MCL.w_CPROWNUM,this.w_CLROWORD)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_ROREADPHASE = this.w_ROPREVPHASE
              this.w_RNREADPHASE = this.w_RNPREVPHASE
              this.w_DEREADPHASE = this.w_DEPREVPHASE
              * --- Imposto il controllo a Tutto Ok prima di verificare se esiste
              this.w_L_CANDELROW = "Y"
              * --- Verifico se esiste la fase successiva e recupero i dati che mi servono
              if this.w_ROREADPHASE > 0
                this.Pag4("CONTROLLO",this.w_CLCODODL,this.w_RNREADPHASE,this.w_ROREADPHASE,this.w_L_CHECK)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                do case
                  case this.w_L_CANDELROW = "Y"
                    this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                    this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                    this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                  case this.w_L_CANDELROW = "S"
                    this.GSCO_MCL.w_CANDELROW = False
                    this.GSCO_MCL.w_CANDEAROW = False
                    this.GSCO_MCL.w_CANCHROW = False
                    this.w_CHECKERROR = .T.
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Impossibile eliminare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Impossibile disattivare la fase [%1 - %2]%0Impossibile impostare la fase precedente [%3 - %4] come ultima fase, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Impossibile cambiare la sequenza della fase [%1 - %2]%0Impossibile impostare la fase [%3 - %4] come precedente, la fase ha generato l'ordine di conto lavoro di fase %5 con stato %6."
                    endcase
                    this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL, this.w_STATOOCL )
                    ah_ErrorMsg(this.TmpC,16,"")
                  case this.w_L_CANDELROW = "W"
                    * --- Caso Warning � come se rispondessi sempre si, visualizzo maschera al termine dell'operazione
                    this.GSCO_MCL.w_CANDELROW = this.pAzione="CanDeleteRow"
                    this.GSCO_MCL.w_CANDEAROW = this.pAzione = "CanDeactRow"
                    this.GSCO_MCL.w_CANCHROW = this.pAzione = "ChangeOrder"
                    do case
                      case this.pAzione="CanDeleteRow"
                        this.TmpC = "Eliminando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "CanDeactRow"
                        this.TmpC = "Disattivando la fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                      case this.pAzione = "ChangeOrder"
                        this.TmpC = "Modificando la sequenza della fase [%1 - %2] verr� modificata la fase di input della fase [%3 - %4] che comporter� l'eliminazione dell'ordine di conto lavoro associato: la fase ha generato l'ordine di conto lavoro [%5]."
                    endcase
                    this.TmpC = ah_msgformat(this.TmpC, ALLTRIM(STR(this.o_CLROWORD)), ALLTRIM(this.w_CLDESFAS) ,ALLTRIM(STR(this.w_ROREADPHASE)), ALLTRIM(this.w_DEREADPHASE), this.w_CLCODOCL )
                    this.w_oMess.addMsgPartNL(this.TmpC)     
                  case this.w_L_CANDELROW = "D"
                    this.GSCO_MCL.w_CANDELROW = False
                    this.GSCO_MCL.w_CANDEAROW = False
                    this.GSCO_MCL.w_CANCHROW = False
                    this.w_CHECKERROR = .T.
                endcase
              endif
            endif
        endcase
        this.w_CURCHECK = this.w_CURCHECK + 1
      enddo
      this.GSCO_MCL.RePos(.T.)     
    endif
    * --- Se non ci sono stati errori bloccanti allora verifico se ci sono warning e li visualizzo
    if !this.w_CHECKERROR
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      * --- Al termine dei controlli se ci sono stati messaggi di errore li visualizzo
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      this.w_MEMO = ""
      this.w_NOBLOC = this.w_oMess.ComposeMessage()
      if ! Empty (this.w_MEMO) Or !Empty(this.w_NOBLOC)
        * --- Visualizzo la maschera con all'interno tutti i messagi di errore
        this.w_ANNULLA = False
        this.w_CONFKME = False
        this.w_DAIM = False
        do GSVE_KLG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CONFKME = this.w_ANNULLA
      else
        this.w_CONFKME = True
      endif
      this.GSCO_MCL.w_CANDELROW = this.w_CONFKME
      this.GSCO_MCL.w_CANDEAROW = this.w_CONFKME
      this.GSCO_MCL.w_CANCHROW = this.w_CONFKME
      if !this.w_CONFKME
        if !Empty(this.GSCO_AOP.w_Cur_Del_Ocl) and used(this.GSCO_AOP.w_Cur_Del_Ocl)
          SELECT(this.GSCO_AOP.w_Cur_Del_Ocl)
          DELETE FOR TIPO="M" AND CCHECK=this.w_L_CHECK
        endif
      endif
    else
      this.GSCO_MCL.w_CANDELROW = False
      this.GSCO_MCL.w_CANDEAROW = False
      this.GSCO_MCL.w_CANCHROW = False
    endif
    USE IN SELECT(this.w_COCLCOLL)
  endproc


  procedure Pag4
    param p_AZIONE,p_CLCODODL,p_CPROWNUM,p_CPROWORD,p_CHECK
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valori che pu� assumere la variabile w_L_CANDELROW
    *     Y = YES
    *     N = NO
    *     S = STOP
    *     W = WARN
    * --- Assegno i parametri
    this.w_p_CLCODODL = m.p_CLCODODL
    this.w_p_CPROWNUM = m.p_CPROWNUM
    this.w_p_CHECK = m.p_CHECK
    this.w_STOP = FALSE
    this.w_WARN = FALSE
    do case
      case m.p_AZIONE="CONTROLLO"
        this.w_L_CANDELROW = "Y"
        this.w_CHIAVE = this.w_p_CLCODODL+STR(this.w_p_CPROWNUM)
        SELECT(this.w_COCLCOLL)
        if SEEK(this.w_CHIAVE, this.w_COCLCOLL)
          this.w_STOP = this.w_STOP or NVL(OLTSTATO," ") $ "L-F"
          this.w_WARN = TRUE
          this.w_FASECOLA = .T.
          this.w_CLCODOCL = OLCODODL
          this.w_STATOOCL = IIF(OLTSTATO="M", "Suggerito", IIF(OLTSTATO="P", "Da Ordinare", IIF(OLTSTATO="L", "Ordinato" , "Finito")))
        endif
        if !this.w_STOP and !this.w_WARN
          this.w_L_CANDELROW = "Y"
        else
          if this.w_STOP
            * --- Notifico che non posso eliminare la fase ha generato OCL con stato maggiore di da ordinare
            this.w_L_CANDELROW = "S"
          else
            if this.w_WARN
              if !this.GSCO_AOP.w_DELOCLFA
                if this.GSCO_AOP.w_OLTSTATO $ "MPL"
                  this.w_L_CANDELROW = "W"
                  this.Pag4("PROSEGUI",this.w_CLCODOCL,0,0,this.w_p_CHECK)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
        * --- Se non ho errori ne warning significa che non ha generato ordini di conto lavoro di fase
        if this.w_L_CANDELROW $ "W-Y"
          * --- Leggo il CPROWNUM e la descrizione della fase
          this.w_L_CMD = "NVL(T_CLQTAAVA,0) as QTADIC"
          this.w_L_WHERE = "CPROWNUM=" + cp_ToStrODBC(this.w_p_CPROWNUM)
          this.GSCO_MCL.Exec_Select("TmpQtaDic", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
          this.w_L_QTADIC = NVL(TmpQtaDic.QTADIC,0)
          if this.w_L_QTADIC > 0
            this.w_L_CANDELROW = "D"
          endif
          USE IN SELECT("TmpQtaDic")
        endif
      case m.p_AZIONE="PROSEGUI"
        this.w_FOUND = .F.
        this.w_TIPOPE = "M"
        L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
        if !Empty(L_Cur_Del_Ocl ) and Used( L_Cur_Del_Ocl )
          SELECT( L_Cur_Del_Ocl )
          GO TOP
          LOCATE FOR CLCODODL = this.w_p_CLCODODL and TIPO = this.w_TIPOPE
          this.w_FOUND = FOUND()
        endif
        if !this.w_FOUND
          this.GSCO_AOP.w_bOclfadadel = True
          if Empty(this.GSCO_AOP.w_Cur_Del_Ocl)
            this.GSCO_AOP.w_Cur_Del_Ocl = Sys(2015)
          endif
          * --- Assegno ad una variabile L_.. poich� devo utilizzare la Macro con il nome del Cursore 
          *     per gli OCL di fase da eliminare
          *     Creo il cursore contenente gli OCL
          L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
          if Not Used( L_Cur_Del_Ocl )
            CREATE CURSOR ( L_Cur_Del_Ocl ) ( CLCODODL C(15) , TIPO C(1), CCHECK C(10) )
            =wrcursor( L_Cur_Del_Ocl )
          endif
          INSERT INTO ( L_Cur_Del_Ocl ) VALUES (this.w_p_CLCODODL, this.w_TIPOPE, this.w_p_CHECK)
        endif
    endcase
  endproc


  procedure Pag5
    param p_AZIONE,p_CLCODODL,p_CPROWNUM,p_CPROWORD
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.p_L_CLCODODL = m.p_CLCODODL
    this.p_L_CPROWNUM = m.p_CPROWNUM
    this.p_L_CLROWORD = m.p_CPROWORD
    do case
      case m.p_AZIONE="PREVPHASE"
        * --- Recupero CLROWORD fase precedente
        this.w_L_CMD = "MAX(T_CLROWORD) AS CLROWORD"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) + " and T_CLROWORD < "+cp_ToStrODBC(this.p_L_CLROWORD)
        this.GSCO_MCL.Exec_Select("TmpUltFas1", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_ROPREVPHASE = NVL(TmpUltFas1.CLROWORD,0)
        * --- Leggo il CPROWNUM e la descrizione della fase
        this.w_L_CMD = "CPROWNUM, T_CLDESFAS"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) + " and T_CLROWORD=" + cp_ToStrODBC(this.w_ROPREVPHASE)
        this.GSCO_MCL.Exec_Select("TmpUltFas", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_RNPREVPHASE = NVL(TmpUltFas.CPROWNUM,0)
        this.w_DEPREVPHASE = NVL(TmpUltFas.T_CLDESFAS, "")
      case m.p_AZIONE="NEXTPHASE"
        * --- Recupero CLROWORD fase successiva
        this.w_L_CMD = "MIN(T_CLROWORD) AS CLROWORD"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) + " and T_CLROWORD > "+cp_ToStrODBC(this.p_L_CLROWORD)
        this.GSCO_MCL.Exec_Select("TmpUltFas1", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_RONEXTPHASE = NVL(TmpUltFas1.CLROWORD,0)
        * --- Leggo il CPROWNUM e la descrizione della fase
        this.w_L_CMD = "CPROWNUM, T_CLDESFAS"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) + " and T_CLROWORD=" + cp_ToStrODBC(this.w_RONEXTPHASE)
        this.GSCO_MCL.Exec_Select("TmpUltFas", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_RNNEXTPHASE = NVL(TmpUltFas.CPROWNUM,0)
        this.w_DENEXTPHASE = NVL(TmpUltFas.T_CLDESFAS, "")
      case m.p_AZIONE="FIRSTPHASE"
        * --- Verifico se sono la prima fase
        this.w_L_CMD = "COUNT(T_CLROWORD) AS CLROWORD"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) +" and T_CLROWORD < "+cp_ToStrODBC(this.w_L_CLROWORD) + " and  T_CLROWORD<> "+cp_ToStrODBC(0)+" and !Empty(T_CLDESFAS)"
        this.GSCO_MCL.Exec_Select("TmpUltFas", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_IMFIRSTPHASE = NVL(TmpUltFas.CLROWORD,0) = 0
      case m.p_AZIONE="LASTPHASE"
        * --- Verifico se sono l'ultima fase
        this.w_L_CMD = "COUNT(T_CLROWORD) AS CLROWORD"
        this.w_L_WHERE = "T_CLFASSEL="+cp_ToStrODBC(1) +" and T_CLROWORD > "+cp_ToStrODBC(this.w_L_CLROWORD) + " and  T_CLROWORD<> "+cp_ToStrODBC(0)+" and !Empty(T_CLDESFAS)"
        this.GSCO_MCL.Exec_Select("TmpUltFas", this.w_L_CMD, this.w_L_WHERE, " ", " ", " " )     
        this.w_IMLASTPHASE = NVL(TmpUltFas.CLROWORD,0) = 0
    endcase
    USE IN SELECT("TmpUltFas")
    USE IN SELECT("TmpUltFas1")
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,23)]
    this.cWorkTables[1]='SCCI_ASF'
    this.cWorkTables[2]='ODL_CICL'
    this.cWorkTables[3]='ODL_DETT'
    this.cWorkTables[4]='COD_FASI'
    this.cWorkTables[5]='LIS_FORN'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='CIC_MAST'
    this.cWorkTables[8]='ODL_MAST'
    this.cWorkTables[9]='CIC_DETT'
    this.cWorkTables[10]='RIS_MAST'
    this.cWorkTables[11]='RIS_DETT'
    this.cWorkTables[12]='RIS_ORSE'
    this.cWorkTables[13]='CLR_MAST'
    this.cWorkTables[14]='DET_FASI'
    this.cWorkTables[15]='MOU_FASI'
    this.cWorkTables[16]='ODL_MAIN'
    this.cWorkTables[17]='PAR_PROD'
    this.cWorkTables[18]='ART_ICOL'
    this.cWorkTables[19]='KEY_ARTI'
    this.cWorkTables[20]='MAGAZZIN'
    this.cWorkTables[21]='TMPODL_CICL'
    this.cWorkTables[22]='TMPODL_RISO'
    this.cWorkTables[23]='UNIMIS'
    return(this.OpenAllTables(23))

  proc CloseCursors()
    if used('_Curs_GSCIC2BTC')
      use in _Curs_GSCIC2BTC
    endif
    if used('_Curs_GSCIC6BTC')
      use in _Curs_GSCIC6BTC
    endif
    if used('_Curs_DET_FASI')
      use in _Curs_DET_FASI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsci_btc
  Func cp_maxclroword()
    LOCAL i_a,i_r
    i_a=SELECT()
    SELECT MAX(t_clroword) as mo FROM alias() INTO CURSOR __maxord__
    i_r=__maxord__.mo
    use
    SELECT (i_a)
    RETURN (i_r)
  EndFunc  
   Func cp_maxclrownum()
    LOCAL i_a,i_r
    i_a=SELECT()
    SELECT MAX(t_clrownum) as mo FROM alias() INTO CURSOR __maxord__
    i_r=__maxord__.mo
    use
    SELECT (i_a)
    RETURN (i_r)
   EndFunc 
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
