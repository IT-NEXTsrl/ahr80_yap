* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bcl                                                        *
*              Controlli cicli lavorazione                                     *
*                                                                              *
*      Author: Zucchetti TAM srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-28                                                      *
* Last revis.: 2017-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTrans
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bcl",oParentObject,m.pTrans)
return(i_retval)

define class tgsci_bcl as StdBatch
  * --- Local variables
  pTrans = space(10)
  PunPadre = .NULL.
  Curso = space(10)
  w_Save = .f.
  w_SERIAL = space(10)
  w_MESS = space(10)
  w_ARTIPART = space(2)
  w_COCODCOM = space(41)
  MAXFAS = 0
  MAXFAS1 = 0
  w_CHF1 = .f.
  w_CHF2 = .f.
  w_EXTCERR = .f.
  w_EXTCONS = .f.
  w_NODATE = .f.
  w_NOCLDES = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_FATCON = 0
  w_UMTSPO = space(3)
  w_DESCOD = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MATINP = space(1)
  w_TESTFORM = .f.
  w_LOOP = 0
  w_FORMCOUNT = 0
  w_GSCI_MCL = .NULL.
  w_GSCI_MLR = .NULL.
  w_GSCI_MMI = .NULL.
  w_GSCI_MMO = .NULL.
  w_FIRSTLAP = .f.
  w_COD_FASI = space(5)
  w_COD_CLA = space(5)
  w_COD_RISO = space(5)
  w_DES_RISO = space(20)
  w_DES_CLA = space(20)
  w_TIPO = space(2)
  w_RLINTEST = space(1)
  w_UMTDEF = space(5)
  w_CONATT = 0
  w_CONAVV = 0
  w_CONSEC = 0
  w_LRUMTRIS = space(5)
  w_LRTMPSEC = 0
  w_LRTEMRIS = 0
  w_QTARIS = 0
  w_CLARIS = space(5)
  w_LRPROORA = 0
  w_LRNUMIMP = 0
  w_DETT = space(0)
  w_TIPOARTI = space(2)
  w_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_FAMINI = space(5)
  w_FAMFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  ROWORD = 0
  w_ZOOM = .f.
  w_CHK = .f.
  w_NREC = 0
  w_CLCLAOUT = space(1)
  w_CPROWORD = 0
  w_CLFASOUT = space(1)
  w_PRIMOGIRO = .f.
  w_COUNTPOI = space(1)
  w_CLOURIFE = 0
  w_CPROWNUM = 0
  w_FASPRECLA = 0
  w_CLBFRIFE = 0
  w_PRECLAOUT = .f.
  w_FASCLA = 0
  w_CLCOUPOI = space(1)
  w_CLMAGWIP = space(5)
  w_CLLTFASE = 0
  w_RLTEMCOD = 0
  w_FLCP = space(1)
  w_FLOU = space(1)
  w_FLCF = space(1)
  w_CODART = space(20)
  * --- WorkFile variables
  CIC_MAST_idx=0
  CIC_DETT_idx=0
  CIC_MAIN_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  DET_FASI_idx=0
  RIS_ORSE_idx=0
  CLR_MAST_idx=0
  UNIMIS_idx=0
  COD_FASI_idx=0
  ART_DIST_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo univocit� (da GSCI_MCL)
    * --- Codice articolo della Distinta Base
    * --- Codice Distinta Base
    * --- Descrizione Distinta Base
    * --- Codice Distinta Base precedente (Variabile dichiarata nell'area manuale)
    * --- Codice Ciclo
    * --- Chiave Primaria Ciclo
    * --- Indice di preferenza del ciclo
    * --- Cursore dei componenti della Tree-View
    * --- All'uscita dello zoom vale .t. se si esce con Save
    this.PunPadre = this.oParentObject
    do case
      case this.pTrans $ "SOTTO FUORI" and not empty(this.oParentObject.w_CLCODART) and not empty(this.oParentObject.w_CLCODDIS)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTrans = "Refresh"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTrans = "UltFase"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTrans="ChkRighe"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTrans="DONE"
        * --- Chiuse i Cursori all'uscita della maschera
        if Used(this.oParentObject.w_COMPON)
          Use IN (this.oParentObject.w_COMPON)
        endif
        if Used(this.oParentObject.w_COUTPUT)
          Use IN (this.oParentObject.w_COUTPUT)
        endif
      case this.pTrans="ENDEDIT" OR this.pTrans="STARTEDIT"
        this.w_FORMCOUNT = 0
        this.w_LOOP = 1
        do while this.w_LOOP<=_Screen.FormCount and this.w_FORMCOUNT<2
          if VarType( _Screen.Forms[ this.w_LOOP ].cComment )="C" 
            if UPPER(_Screen.Forms( this.w_Loop ).Class)="TGSCI_MCL" and _Screen.Forms( this.w_Loop ).cFunction="Edit"
              this.w_FORMCOUNT = this.w_FORMCOUNT+1
            endif
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        if this.w_FORMCOUNT>1
          ah_ERRORMSG("Impossibile modificare pi� cicli contemporaneamente.",48)
          this.PunPadre.ecpQuery()     
        else
          * --- Riga selezionata (delle fasi)
          this.oParentObject.l_CURREC = iif(this.pTrans="ENDEDIT", 0, 1)
          if upper( this.PunPadre.gsci_mlr.gsci_mmi.class )="STDDYNAMICCHILD"
            this.PunPadre.gsci_mlr.oPgfrm.Pages[2].opag.uienable(.t.)     
          endif
          * --- Riga selezionata (dei materiali di input)
          this.PunPadre.gsci_mlr.gsci_mmi.l_currec = iif(this.pTrans="ENDEDIT", 0, 1)
        endif
      case this.pTrans="COD_FASI"
        * --- --Inserimento automatico dettaglio fasi
        this.w_CLARIS = SPACE(5)
        this.w_FIRSTLAP = .T.
        this.w_GSCI_MCL = this.OparentObject
        this.w_COD_FASI = this.w_GSCI_MCL.w_CL__FASE
        this.w_GSCI_MLR = this.w_GSCI_MCL.GSCI_MLR
        this.w_GSCI_MMI = this.w_GSCI_MLR.GSCI_MMI
        this.w_GSCI_MMO = this.w_GSCI_MLR.GSCI_MMO
        * --- --Lettura codice Fase 
        * --- Read from COD_FASI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COD_FASI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_FASI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CFINDETT"+;
            " from "+i_cTable+" COD_FASI where ";
                +"CFCODICE = "+cp_ToStrODBC(this.w_COD_FASI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CFINDETT;
            from (i_cTable) where;
                CFCODICE = this.w_COD_FASI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DETT = NVL(cp_ToDate(_read_.CFINDETT),cp_NullValue(_read_.CFINDETT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DETT="S"
          if not empty(this.w_GSCI_MLR.cTrsname) 
            Select (this.w_GSCI_MLR.cTrsname)
            delete all
             SELECT (this.w_GSCI_MLR.cTrsname) 
 GO TOP 
 With this.w_GSCI_MLR 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 EndWith
          endif
          this.ROWORD = 0
          * --- Select from DET_FASI
          i_nConn=i_TableProp[this.DET_FASI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_FASI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" DET_FASI ";
                +" where DFCODICE="+cp_ToStrODBC(this.w_COD_FASI)+"";
                +" order by CPROWORD";
                 ,"_Curs_DET_FASI")
          else
            select * from (i_cTable);
             where DFCODICE=this.w_COD_FASI;
             order by CPROWORD;
              into cursor _Curs_DET_FASI
          endif
          if used('_Curs_DET_FASI')
            select _Curs_DET_FASI
            locate for 1=1
            do while not(eof())
            * --- --Modifica transitorio
            this.w_GSCI_MLR.InitRow()     
            this.ROWORD = this.ROWORD+1
            this.w_COD_CLA = NVL(_Curs_DET_FASI.DFCODCLA,space(5))
            this.w_COD_RISO = _Curs_DET_FASI.DFCODRIS
            this.w_TIPO = nvl(_Curs_DET_FASI.DF__TIPO, space(2))
            * --- --CALCOLO TEMPI 
            this.w_LRUMTRIS = _Curs_DET_FASI.DFUMTRIS
            * --- Read from RIS_ORSE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS"+;
                " from "+i_cTable+" RIS_ORSE where ";
                    +"RLCODICE = "+cp_ToStrODBC(this.w_COD_RISO);
                    +" and RL__TIPO = "+cp_ToStrODBC(this.w_TIPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RLDESCRI,RLINTEST,RLUMTDEF,RLQTARIS;
                from (i_cTable) where;
                    RLCODICE = this.w_COD_RISO;
                    and RL__TIPO = this.w_TIPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DES_RISO = NVL(cp_ToDate(_read_.RLDESCRI),cp_NullValue(_read_.RLDESCRI))
              this.w_RLINTEST = NVL(cp_ToDate(_read_.RLINTEST),cp_NullValue(_read_.RLINTEST))
              this.w_UMTDEF = NVL(cp_ToDate(_read_.RLUMTDEF),cp_NullValue(_read_.RLUMTDEF))
              this.w_QTARIS = NVL(cp_ToDate(_read_.RLQTARIS),cp_NullValue(_read_.RLQTARIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMDURSEC"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(this.w_LRUMTRIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMDURSEC;
                from (i_cTable) where;
                    UMCODICE = this.w_LRUMTRIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CONSEC = NVL(cp_ToDate(_read_.UMDURSEC),cp_NullValue(_read_.UMDURSEC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_LRTEMRIS = _Curs_DET_FASI.DFTEMRIS
            this.w_LRTMPSEC = this.w_LRTEMRIS*this.w_CONSEC
            if NOT EMPTY(this.w_COD_CLA)
              * --- Read from CLR_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CLR_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CLR_MAST_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CSDESCLA"+;
                  " from "+i_cTable+" CLR_MAST where ";
                      +"CSCODCLA = "+cp_ToStrODBC(this.w_COD_CLA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CSDESCLA;
                  from (i_cTable) where;
                      CSCODCLA = this.w_COD_CLA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DES_CLA = NVL(cp_ToDate(_read_.CSDESCLA),cp_NullValue(_read_.CSDESCLA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.w_LRNUMIMP = _Curs_DET_FASI.DFNUMIMP
            this.w_LRPROORA = iif(this.w_LRTMPSEC=0, 0, 3600/this.w_LRTMPSEC*IIF(this.w_TIPO="AT", this.w_LRNUMIMP, 1))
            this.w_GSCI_MLR.w_CPROWORD = this.ROWORD*10
            this.w_GSCI_MLR.w_LRCODCLA = NVL(_Curs_DET_FASI.DFCODCLA,space(5))
            this.w_GSCI_MLR.w_LR__TIPO = this.w_TIPO
            this.w_GSCI_MLR.w_LR1TIPO = this.w_TIPO
            this.w_GSCI_MLR.w_LRCODRIS = nvl(_Curs_DET_FASI.DFCODRIS, space(5))
            this.w_GSCI_MLR.w_LRTIPTEM = NVL(_Curs_DET_FASI.DFTIPTEM, "L")
            this.w_GSCI_MLR.w_LRQTARIS = nvl(_Curs_DET_FASI.DFQTARIS,0)
            this.w_GSCI_MLR.w_LRUMTRIS = nvl(_Curs_DET_FASI.DFUMTRIS,space(5))
            this.w_GSCI_MLR.w_LRTEMRIS = nvl(_Curs_DET_FASI.DFTEMRIS,0)
            this.w_GSCI_MLR.w_LRTEMSEC = nvl(_Curs_DET_FASI.DFTMPSEC,0)
            this.w_GSCI_MLR.w_LRNUMIMP = nvl(_Curs_DET_FASI.DFNUMIMP,0)
            this.w_GSCI_MLR.w_LRISTLAV = nvl(_Curs_DET_FASI.DFISTLAV," ")
            this.w_GSCI_MLR.w_LRPROORA = this.w_LRPROORA
            this.w_GSCI_MLR.w_RLINTEST = this.w_RLINTEST
            this.w_GSCI_MLR.w_DESCLA = this.w_DES_CLA
            this.w_GSCI_MLR.w_DESRIS = this.w_DES_RISO
            this.w_GSCI_MLR.w_UMTDEF = this.w_UMTDEF
            this.w_GSCI_MLR.w_QTARIS = this.w_QTARIS
            this.w_GSCI_MLR.w_CLARIS = this.w_CLARIS
            this.w_GSCI_MLR.w_CONSEC = this.w_CONSEC
            this.w_GSCI_MLR.w_LRTMPCIC = iif(this.w_GSCI_MLR.w_LRTEMRIS>0 , this.w_GSCI_MLR.w_LRTEMRIS * this.w_GSCI_MLR.w_LRNUMIMP, 0)
            this.w_GSCI_MLR.w_LRTMPSEC = this.w_GSCI_MLR.w_LRTMPCIC*this.w_CONSEC
            this.w_GSCI_MLR.SaveRow()     
            this.w_GSCI_MLR.bUpdated = .t.
              select _Curs_DET_FASI
              continue
            enddo
            use
          endif
          if this.ROWORD=0
            this.w_GSCI_MLR.InitRow()     
          endif
          * --- Rinfrescamenti vari
           SELECT (this.w_GSCI_MLR.cTrsname) 
 GO TOP 
 With this.w_GSCI_MLR 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 EndWith
          * --- Materiali di input
          if not empty(this.w_GSCI_MMI.cTrsname) 
            Select (this.w_GSCI_MMI.cTrsname)
            delete all
            this.w_GSCI_MMI.InitRow()     
          endif
          * --- Select from COD_FASI
          i_nConn=i_TableProp[this.COD_FASI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_FASI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CFTIPART,CFCODINI,CFCODFIN,CFFAMAIN,CFFAMAFI,CFGRUINI,CFGRUFIN,CFCATINI,CFCATFIN,CFMAGINI,CFMAGFIN,CFPIAINI,CFPIAFIN,CFFAMINI,CFFAMFIN,CFMATINP  from "+i_cTable+" COD_FASI ";
                +" where CFCODICE="+cp_ToStrODBC(this.w_COD_FASI)+"";
                 ,"_Curs_COD_FASI")
          else
            select CFTIPART,CFCODINI,CFCODFIN,CFFAMAIN,CFFAMAFI,CFGRUINI,CFGRUFIN,CFCATINI,CFCATFIN,CFMAGINI,CFMAGFIN,CFPIAINI,CFPIAFIN,CFFAMINI,CFFAMFIN,CFMATINP from (i_cTable);
             where CFCODICE=this.w_COD_FASI;
              into cursor _Curs_COD_FASI
          endif
          if used('_Curs_COD_FASI')
            select _Curs_COD_FASI
            locate for 1=1
            do while not(eof())
            this.w_MATINP = nvl(_Curs_COD_FASI.CFMATINP,"N")
            this.w_TIPOARTI = _Curs_COD_FASI.CFTIPART
            this.w_ARTINI = nvl(_Curs_COD_FASI.CFCODINI,space(20))
            this.w_ARTFIN = nvl(_Curs_COD_FASI.CFCODFIN,space(20))
            this.w_FAMINI = nvl(_Curs_COD_FASI.CFFAMAIN,space(5))
            this.w_FAMFIN = nvl(_Curs_COD_FASI.CFFAMAFI,space(5))
            this.w_GRUINI = nvl(_Curs_COD_FASI.CFGRUINI,space(5))
            this.w_GRUFIN = nvl(_Curs_COD_FASI.CFGRUFIN,space(5))
            this.w_CATINI = nvl(_Curs_COD_FASI.CFCATINI,space(5))
            this.w_CATFIN = nvl(_Curs_COD_FASI.CFCATFIN,space(5))
            this.w_MAGINI = nvl(_Curs_COD_FASI.CFMAGINI,space(5))
            this.w_MAGFIN = nvl(_Curs_COD_FASI.CFMAGFIN,space(5))
              select _Curs_COD_FASI
              continue
            enddo
            use
          endif
          if this.w_MATINP="S"
            * --- Usa MIN_FASI
            vq_exec("..\PRFA\EXE\QUERY\GSCI_BCL2", this, "_MatInp_")
          else
            * --- Usa i filtri di GSCI_ACF
            vq_exec("..\PRFA\EXE\QUERY\GSCI_BCL1", this, "_MatInp_")
          endif
          if RecCount("_MatInp_")>0
            Select _MatInp_ 
 go top 
 scan
            * --- Modifica transitorio
            this.w_GSCI_MMI.AddRow()     
            this.w_GSCI_MMI.w_MIDIBCOD = _MatInp_.CORIFDIS
            this.w_GSCI_MMI.w_CODCOM = _MatInp_.COCODCOM
            this.w_GSCI_MMI.w_MIDIBROW = _MatInp_.CPROWNUM
            this.w_GSCI_MMI.w_NUMDIS = _MatInp_.CPROWORD
            this.w_GSCI_MMI.w_DESCOS = _MatInp_.ARDESART
            this.w_GSCI_MMI.w_DATINI = _MatInp_.COATTGES
            this.w_GSCI_MMI.w_DATFIN = _MatInp_.CODISGES
            this.w_GSCI_MMI.SaveRow()     
            endscan
            * --- Rinfrescamenti vari
            this.w_GSCI_MMI.FirstRow()     
            this.w_GSCI_MMI.SetRow()     
          endif
          if used("_MatInp_")
            USE IN _MatInp_
          endif
          * --- Materiali di output
          * --- Usa MOU_FASI
          vq_exec("..\PRFA\EXE\QUERY\GSCI_BCL3", this, "_MatOut_")
          if RecCount("_MatOut_")>0
            Select _MatOut_ 
 go top 
 scan
            * --- Modifica transitorio
            this.w_GSCI_MMO.AddRow()     
            this.w_GSCI_MMO.w_MODIBCOD = _MatOut_.CORIFDIS
            this.w_GSCI_MMO.w_CODCOM = _MatOut_.COCODCOM
            this.w_GSCI_MMO.w_MODIBROW = _MatOut_.CPROWNUM
            this.w_GSCI_MMO.w_NUMDIS = _MatOut_.CPROWORD
            this.w_GSCI_MMO.w_DESCOS = _MatOut_.ARDESART
            this.w_GSCI_MMO.w_DATINI = _MatOut_.COATTGES
            this.w_GSCI_MMO.w_DATFIN = _MatOut_.CODISGES
            this.w_GSCI_MMO.SaveRow()     
            endscan
            * --- Rinfrescamenti vari
            this.w_GSCI_MMO.FirstRow()     
            this.w_GSCI_MMO.SetRow()     
          endif
          if used("_MatOut_")
            USE IN _MatOut_
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zoom Mat-Alt
    this.w_ZOOM = .T.
    * --- Filtra sul prefisso dei codici dei materiali
    SELECT * FROM (w_MATALT) INTO CURSOR MateAlt &l_filtro
    if this.pTrans = "ChkMateAlt"
      do case
        case reccount()=0 or reccount()>1
          * --- se si abilita con (reccount()>1) lo zoom genera errore quando si preme ESC
          ah_ErrorMsg("Valore non ammesso",48)
          this.oParentObject.w_CLRIFMAT = space(41)
          this.w_ZOOM = .F.
          this.oParentObject.bHeaderUpdated = .t.
        case reccount()=1
          * --- Valore OK
          this.oParentObject.w_CLRIFMAT = MateAlt.cocodcom
          this.oParentObject.w_CLDISMAT = MateAlt.corifdis
          this.oParentObject.w_CLROWMAT = MateAlt.cprownum
          this.w_ZOOM = .F.
          this.w_CHK = .T.
      endcase
    else
      if reccount()=0
        this.w_MESS = "In questa distinta non sono presenti materiali alternativi di fabbricazione"
        ah_ErrorMsg(this.w_MESS, 48)
        this.w_ZOOM = .F.
      endif
    endif
    if this.w_ZOOM
      * --- Zoom di selezione del materiale alternativo
      do GSCI_KMA with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used("MateAlt")
        if this.w_Save
          this.oParentObject.w_CLRIFMAT = MateAlt.cocodcom
          this.oParentObject.w_CLDISMAT = MateAlt.corifdis
          this.oParentObject.w_CLROWMAT = MateAlt.cprownum
          this.w_CHK = .T.
          this.oParentObject.bHeaderUpdated = .t.
        endif
        USE IN MateAlt
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli
    if this.pTrans $ "SOTTO FUORI" and this.oParentObject.cFunction $ "Load Edit"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPART"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CLCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPART;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_CLCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if not this.w_ARTIPART $ "PF-SE-PH-DC-CC-MP"
        this.oParentObject.w_CLCODDIS = space(41)
        this.oParentObject.w_CLCODART = space(20)
        this.oParentObject.w_DESDIS = space(40)
        ah_ErrorMsg ("Tipo articolo della distinta base non valido", 16)
        i_retcode = 'stop'
        return
      endif
      if !Empty(this.oParentObject.w_CLCODART) and !Empty(this.oParentObject.w_CLCODDIS)
        * --- Read from ART_DIST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_DIST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ART_DIST where ";
                +"DICODART = "+cp_ToStrODBC(this.oParentObject.w_CLCODART);
                +" and DICODDIS = "+cp_ToStrODBC(this.oParentObject.w_CLCODDIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DICODART = this.oParentObject.w_CLCODART;
                and DICODDIS = this.oParentObject.w_CLCODDIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
          if i_Rows=0
            this.oParentObject.w_CLCODDIS = space(41)
            this.oParentObject.w_CLCODART = space(20)
            this.oParentObject.w_DESDIS = space(40)
            ah_ErrorMsg ("Distinta base non associata all'articolo", 16)
            i_retcode = 'stop'
            return
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Read from CIC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CIC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLSERIAL"+;
          " from "+i_cTable+" CIC_MAST where ";
              +"CLCODART = "+cp_ToStrODBC(this.oParentObject.w_CLCODART);
              +" and CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_CLCODDIS);
              +" and CLCODCIC = "+cp_ToStrODBC(this.oParentObject.w_CLCODCIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLSERIAL;
          from (i_cTable) where;
              CLCODART = this.oParentObject.w_CLCODART;
              and CLCODDIS = this.oParentObject.w_CLCODDIS;
              and CLCODCIC = this.oParentObject.w_CLCODCIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERIAL = NVL(cp_ToDate(_read_.CLSERIAL),cp_NullValue(_read_.CLSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SERIAL <> this.oParentObject.w_CLSERIAL and not empty(this.w_SERIAL)
        if this.pTrans = "SOTTO"
          this.w_MESS = ah_MsgFormat("Chiave gi� utilizzata")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        else
          ah_ErrorMsg("Attenzione: chiave gi� utilizzata", 16)
        endif
      else
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.pTrans = "SOTTO" and empty(this.w_MESS)
      if empty(this.w_MESS) and this.oParentObject.w_CLINDCIC="00"
        * --- Verifica se esiste un solo ciclo con preferenza 00
        * --- Select from CIC_MAST
        i_nConn=i_TableProp[this.CIC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CLCODCIC  from "+i_cTable+" CIC_MAST ";
              +" where CLCODDIS="+cp_ToStrODBC(this.oParentObject.w_CLCODDIS)+" and CLCODART="+cp_ToStrODBC(this.oParentObject.w_CLCODART)+" and CLINDCIC="+cp_ToStrODBC(this.oParentObject.w_CLINDCIC)+"";
               ,"_Curs_CIC_MAST")
        else
          select CLCODCIC from (i_cTable);
           where CLCODDIS=this.oParentObject.w_CLCODDIS and CLCODART=this.oParentObject.w_CLCODART and CLINDCIC=this.oParentObject.w_CLINDCIC;
            into cursor _Curs_CIC_MAST
        endif
        if used('_Curs_CIC_MAST')
          select _Curs_CIC_MAST
          locate for 1=1
          do while not(eof())
          if empty(this.w_MESS) and _Curs_CIC_MAST.CLCODCIC<>this.oParentObject.w_CLCODCIC
            this.w_MESS = ah_msgformat("Per la distinta selezionata esiste gi� un ciclo con indice di preferenza 00")
          endif
            select _Curs_CIC_MAST
            continue
          enddo
          use
        endif
      endif
      if not empty(this.w_MESS)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
    if this.pTrans = "SOTTO" and empty(this.w_MESS)
      if this.oParentObject.w_CLPRCOST="S"
        * --- Select from CIC_MAST
        i_nConn=i_TableProp[this.CIC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CLSERIAL  from "+i_cTable+" CIC_MAST ";
              +" where CLSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL)+" AND CLCODDIS = "+cp_ToStrODBC(this.oParentObject.w_CLCODDIS)+" AND CLCODART = "+cp_ToStrODBC(this.oParentObject.w_CLCODART)+" AND CLPRCOST='S'";
               ,"_Curs_CIC_MAST")
        else
          select CLSERIAL from (i_cTable);
           where CLSERIAL <> this.oParentObject.w_CLSERIAL AND CLCODDIS = this.oParentObject.w_CLCODDIS AND CLCODART = this.oParentObject.w_CLCODART AND CLPRCOST="S";
            into cursor _Curs_CIC_MAST
        endif
        if used('_Curs_CIC_MAST')
          select _Curs_CIC_MAST
          locate for 1=1
          do while not(eof())
          this.w_NREC = this.w_NREC + 1
            select _Curs_CIC_MAST
            continue
          enddo
          use
        endif
        if this.w_NREC > 0
          this.w_MESS = ah_msgformat("Per la distinta selezionata esiste gi� un ciclo con flag considera per costificazione attivo")
        endif
        if not empty(this.w_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SetUltFase
    * --- Setta ultima fase ed assegna, ad ogni fase, fase COUNT POINT di riferimento
    * --- Controlla le Risorse delle fasi
    * --- Select from GSCI2BCL
    do vq_exec with 'GSCI2BCL',this,'_Curs_GSCI2BCL','',.f.,.t.
    if used('_Curs_GSCI2BCL')
      select _Curs_GSCI2BCL
      locate for 1=1
      do while not(eof())
      this.w_MESS = ah_msgformat("Inserire un centro di lavoro nella fase (%1) %2", alltrim(str(_Curs_GSCI2BCL.CPROWORD)), alltrim(_Curs_GSCI2BCL.CLFASDES) )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
        select _Curs_GSCI2BCL
        continue
      enddo
      use
    endif
    * --- Controlla i Materiali di Input delle fasi
    vq_exec("..\PRFA\exe\query\GSCI3BCL", this, "_ListFasi_")
    Select * from _ListFasi_ into array prefas
    this.w_CHF1 = .t.
    this.MAXFAS = iif(type("prefas")="U", 0, alen(prefas,1))
    do while this.MAXFAS>1 and this.w_CHF1
      this.MAXFAS1 = this.MAXFAS - 1
      do while this.w_CHF1 and this.MAXFAS1>0 and prefas(this.MAXFAS,1)=prefas(this.MAXFAS1,1)
        if ((prefas(this.MAXFAS,5)<=prefas(this.MAXFAS1,5) and prefas(this.MAXFAS1,5)<=prefas(this.MAXFAS,6)) ;
          or (prefas(this.MAXFAS1,5)<=prefas(this.MAXFAS,5) and prefas(this.MAXFAS,5)<=prefas(this.MAXFAS1,6)))
          this.w_MESS = ah_msgformat("Il materiale '%1' � stato inserito nelle fasi %2 (%3) e %4 (%5)", alltrim(prefas(this.MAXFAS,2)), alltrim(str(prefas(this.MAXFAS,3))), alltrim(prefas(this.MAXFAS,4)), alltrim(str(prefas(this.MAXFAS1,3))), alltrim(prefas(this.MAXFAS1,4)) )
          this.w_CHF1 = .f.
        endif
        this.MAXFAS1 = this.MAXFAS1 - 1
      enddo
      this.MAXFAS = this.MAXFAS-1
    enddo
    release prefas
    if used("_ListFasi_")
      use in _ListFasi_
    endif
    if not this.w_CHF1
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Imposta flag output di fase
    ah_msg("Verifica fasi di conto/lavoro in corso...")
    * --- Write into CIC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CIC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLFASCLA ="+cp_NullLink(cp_ToStrODBC(" "),'CIC_DETT','CLFASCLA');
          +i_ccchkf ;
      +" where ";
          +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
             )
    else
      update (i_cTable) set;
          CLFASCLA = " ";
          &i_ccchkf. ;
       where;
          CLSERIAL = this.oParentObject.w_CLSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from GSCI5BCL
    do vq_exec with 'GSCI5BCL',this,'_Curs_GSCI5BCL','',.f.,.t.
    if used('_Curs_GSCI5BCL')
      select _Curs_GSCI5BCL
      locate for 1=1
      do while not(eof())
      this.w_CPROWORD = _Curs_GSCI5BCL.CPROWORD
      this.w_RLTEMCOD = _Curs_GSCI5BCL.RLTEMCOD
      this.w_CLLTFASE = _Curs_GSCI5BCL.CLLTFASE
      if this.w_CLLTFASE=0
        this.w_CLLTFASE = this.w_RLTEMCOD
      endif
      this.w_CLMAGWIP = _Curs_GSCI5BCL.RLMAGWIP
      * --- Se fase su centro di lavoro esterno, alza flag di gestione output di fase
      if _Curs_GSCI5BCL.RLINTEST = "E"
        * --- Sulla fase � presente un Centro di lavoro esterno
        * --- Write into CIC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CIC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
          +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCLA');
          +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCOS');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                 )
        else
          update (i_cTable) set;
              CLFASOUT = "S";
              ,CLCOUPOI = "S";
              ,CLFASCLA = "S";
              ,CLFASCOS = "S";
              &i_ccchkf. ;
           where;
              CLSERIAL = this.oParentObject.w_CLSERIAL;
              and CPROWORD = this.w_CPROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PRECLAOUT = .T.
      else
        if this.w_PRECLAOUT and this.w_FASPRECLA <> _Curs_GSCI5BCL.CPROWORD
          * --- Sulla fase precedente era presente un Centro di lavoro esterno
          this.w_FASCLA = _Curs_GSCI5BCL.CPROWORD
          this.w_PRECLAOUT = .F.
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
            +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCOS');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_FASCLA);
                   )
          else
            update (i_cTable) set;
                CLFASOUT = "S";
                ,CLCOUPOI = "S";
                ,CLFASCOS = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.oParentObject.w_CLSERIAL;
                and CPROWORD = this.w_FASCLA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      this.w_FASPRECLA = _Curs_GSCI5BCL.CPROWORD
      * --- Write into CIC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CIC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLLTFASE ="+cp_NullLink(cp_ToStrODBC(this.w_CLLTFASE),'CIC_DETT','CLLTFASE');
        +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_CLMAGWIP),'CIC_DETT','CLMAGWIP');
            +i_ccchkf ;
        +" where ";
            +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
               )
      else
        update (i_cTable) set;
            CLLTFASE = this.w_CLLTFASE;
            ,CLMAGWIP = this.w_CLMAGWIP;
            &i_ccchkf. ;
         where;
            CLSERIAL = this.oParentObject.w_CLSERIAL;
            and CPROWORD = this.w_CPROWORD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSCI5BCL
        continue
      enddo
      use
    endif
    * --- Imposta flag output di fase
    ah_msg("Verifica fasi di output in corso...")
    this.w_PRIMOGIRO = .T.
    * --- Select from CIC_DETT
    i_nConn=i_TableProp[this.CIC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_DETT ";
          +" where CLSERIAL="+cp_ToStrODBC(this.oParentObject.w_CLSERIAL)+"";
          +" order by CPROWORD";
           ,"_Curs_CIC_DETT")
    else
      select * from (i_cTable);
       where CLSERIAL=this.oParentObject.w_CLSERIAL;
       order by CPROWORD;
        into cursor _Curs_CIC_DETT
    endif
    if used('_Curs_CIC_DETT')
      select _Curs_CIC_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWORD = _Curs_CIC_DETT.CPROWORD
      this.w_FLCP = iif(_Curs_CIC_DETT.CLCOUPOI="S","="," ")
      this.w_FLOU = iif(_Curs_CIC_DETT.CLFASOUT="S","="," ")
      this.w_FLCF = iif(_Curs_CIC_DETT.CLFASCOS="S","="," ")
      if nvl(_Curs_CIC_DETT.CLCOUPOI,"N")<>"S"
        * --- Write into CIC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CIC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_CIC_DETT.CLCOUPOI',_Curs_CIC_DETT.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_CIC_DETT.CLFASOUT',_Curs_CIC_DETT.CLFASOUT,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_CIC_DETT.CLFASCOS',_Curs_CIC_DETT.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'CIC_DETT','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'CIC_DETT','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(space(66)),'CIC_DETT','CLCODFAS');
          +",CLARTFAS ="+cp_NullLink(cp_ToStrODBC(space(20)),'CIC_DETT','CLARTFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp5,'CIC_DETT','CLFASCOS');
          +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLPRIFAS');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = space(66);
              ,CLARTFAS = space(20);
              ,CLFASCOS = &i_cOp5.;
              ,CLPRIFAS = "N";
              &i_ccchkf. ;
           where;
              CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
              and CPROWORD = this.w_CPROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_PRIMOGIRO
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_CIC_DETT.CLCOUPOI',_Curs_CIC_DETT.CLCOUPOI,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_CIC_DETT.CLFASOUT',_Curs_CIC_DETT.CLFASOUT,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_CIC_DETT.CLFASCOS',_Curs_CIC_DETT.CLFASCOS,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCOUPOI ="+cp_NullLink(i_cOp1,'CIC_DETT','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(i_cOp2,'CIC_DETT','CLFASOUT');
            +",CLFASCOS ="+cp_NullLink(i_cOp3,'CIC_DETT','CLFASCOS');
            +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLPRIFAS');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLCOUPOI = &i_cOp1.;
                ,CLFASOUT = &i_cOp2.;
                ,CLFASCOS = &i_cOp3.;
                ,CLPRIFAS = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PRIMOGIRO = .F.
        else
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_CIC_DETT.CLCOUPOI',_Curs_CIC_DETT.CLCOUPOI,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_CIC_DETT.CLFASOUT',_Curs_CIC_DETT.CLFASOUT,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_CIC_DETT.CLFASCOS',_Curs_CIC_DETT.CLFASCOS,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCOUPOI ="+cp_NullLink(i_cOp1,'CIC_DETT','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(i_cOp2,'CIC_DETT','CLFASOUT');
            +",CLFASCOS ="+cp_NullLink(i_cOp3,'CIC_DETT','CLFASCOS');
            +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLPRIFAS');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLCOUPOI = &i_cOp1.;
                ,CLFASOUT = &i_cOp2.;
                ,CLFASCOS = &i_cOp3.;
                ,CLPRIFAS = "N";
                &i_ccchkf. ;
             where;
                CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_CIC_DETT
        continue
      enddo
      use
    endif
    * --- Setta ultima fase, fase OUTPUT di riferimento e fase BF(CP) di riferimento
    ah_msg("Verifica fasi count point in corso...")
    * --- Cicla su cursore
    this.w_CLBFRIFE = 0
    this.w_CLOURIFE = 0
    this.w_PRIMOGIRO = .T.
    this.w_CPROWORD = 0
    * --- Select from GSCI4BCL
    do vq_exec with 'GSCI4BCL',this,'_Curs_GSCI4BCL','',.f.,.t.
    if used('_Curs_GSCI4BCL')
      select _Curs_GSCI4BCL
      locate for 1=1
      do while not(eof())
      * --- Coordinate fase
      if this.w_CPROWORD <> nvl (_Curs_GSCI4BCL.CPROWORD , 0)
        this.w_CPROWORD = nvl (_Curs_GSCI4BCL.CPROWORD , 0)
        this.w_CLFASOUT = nvl (_Curs_GSCI4BCL.CLFASOUT , "N")
        this.w_CLCOUPOI = nvl (_Curs_GSCI4BCL.CLCOUPOI , "N")
        * --- Se primo giro, siamo sull'ultima fase
        if this.w_PRIMOGIRO
          this.w_PRIMOGIRO = .F.
          this.w_CLBFRIFE = this.w_CPROWORD
          this.w_CLOURIFE = this.w_CPROWORD
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLULTFAS');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'CIC_DETT','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'CIC_DETT','CLBFRIFE');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "S";
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                ,CLCOUPOI = "S";
                ,CLFASOUT = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.oParentObject.w_CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_CLOURIFE = iif( this.w_CLFASOUT="S", this.w_CPROWORD, this.w_CLOURIFE)
          this.w_CLBFRIFE = iif( this.w_CLCOUPOI="S", this.w_CPROWORD, this.w_CLBFRIFE)
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLULTFAS');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'CIC_DETT','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'CIC_DETT','CLBFRIFE');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "N";
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                &i_ccchkf. ;
             where;
                CLSERIAL = this.oParentObject.w_CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_GSCI4BCL
        continue
      enddo
      use
    endif
    * --- Controlla che i materiali di input siano associati ad una fase con flag output attivo
    this.w_CHF1 = .t.
    * --- Select from GSCI3BCL1
    do vq_exec with 'GSCI3BCL1',this,'_Curs_GSCI3BCL1','',.f.,.t.
    if used('_Curs_GSCI3BCL1')
      select _Curs_GSCI3BCL1
      locate for 1=1
      do while not(eof())
      this.w_MESS = ah_msgformat("Il materiale '%1' � stato inserito nella fase %2 (%3) che non ha il flag di Output attivo.", alltrim(_Curs_GSCI3BCL1.COCODCOM) , alltrim(str(_Curs_GSCI3BCL1.CPROWORD)), alltrim(_Curs_GSCI3BCL1.CLFASDES))
      this.w_CHF1 = .f.
        select _Curs_GSCI3BCL1
        continue
      enddo
      use
    endif
    if not this.w_CHF1
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
    * --- Elimina i Materiali di Input delle fasi alternative
    ah_msg("Verifica materiali di input/output in corso...")
    * --- Select from GSCI1BCL
    do vq_exec with 'GSCI1BCL',this,'_Curs_GSCI1BCL','',.f.,.t.
    if used('_Curs_GSCI1BCL')
      select _Curs_GSCI1BCL
      locate for 1=1
      do while not(eof())
      * --- Delete from CIC_MAIN
      i_nConn=i_TableProp[this.CIC_MAIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAIN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MISERIAL = "+cp_ToStrODBC(this.oParentObject.w_CLSERIAL);
              +" and MIROWNUM = "+cp_ToStrODBC(_Curs_GSCI1BCL.CPROWNUM);
               )
      else
        delete from (i_cTable) where;
              MISERIAL = this.oParentObject.w_CLSERIAL;
              and MIROWNUM = _Curs_GSCI1BCL.CPROWNUM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
        select _Curs_GSCI1BCL
        continue
      enddo
      use
    endif
    * --- Aggiorna i Materiali di Input delle fasi alternative
    * --- Select from GSCI_BCL
    do vq_exec with 'GSCI_BCL',this,'_Curs_GSCI_BCL','',.f.,.t.
    if used('_Curs_GSCI_BCL')
      select _Curs_GSCI_BCL
      locate for 1=1
      do while not(eof())
      * --- Insert into CIC_MAIN
      i_nConn=i_TableProp[this.CIC_MAIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAIN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CIC_MAIN_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MISERIAL"+",MIROWNUM"+",CPROWNUM"+",MIDIBCOD"+",MIDIBROW"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLSERIAL),'CIC_MAIN','MISERIAL');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCI_BCL.MIROWNUM),'CIC_MAIN','MIROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCI_BCL.CPROWNUM),'CIC_MAIN','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCI_BCL.MIDIBCOD),'CIC_MAIN','MIDIBCOD');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_GSCI_BCL.MIDIBROW),'CIC_MAIN','MIDIBROW');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MISERIAL',this.oParentObject.w_CLSERIAL,'MIROWNUM',_Curs_GSCI_BCL.MIROWNUM,'CPROWNUM',_Curs_GSCI_BCL.CPROWNUM,'MIDIBCOD',_Curs_GSCI_BCL.MIDIBCOD,'MIDIBROW',_Curs_GSCI_BCL.MIDIBROW)
        insert into (i_cTable) (MISERIAL,MIROWNUM,CPROWNUM,MIDIBCOD,MIDIBROW &i_ccchkf. );
           values (;
             this.oParentObject.w_CLSERIAL;
             ,_Curs_GSCI_BCL.MIROWNUM;
             ,_Curs_GSCI_BCL.CPROWNUM;
             ,_Curs_GSCI_BCL.MIDIBCOD;
             ,_Curs_GSCI_BCL.MIDIBROW;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSCI_BCL
        continue
      enddo
      use
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CheckForm
    this.Curso = this.PunPadre.cTrsname
    * --- Controlla che siano inserite le date di validit�
    this.w_NODATE = .F.
    this.w_NOCLDES = .F.
    this.w_oMESS=createobject("AH_MESSAGE")
    if this.oParentObject.w_CHECKFORM
      Select (this.Curso)
      scan
      this.w_NODATE = empty(t_clinival) or empty(t_clfinval)
      this.w_NOCLDES = empty(t_CLFASDES)
      this.oParentObject.w_CHECKFORM = Not (this.w_NODATE or this.w_NOCLDES)
      this.w_CPROWORD = t_cproword
      this.w_MESS = alltrim(t_clfasdes)
      endscan
    endif
    if not this.oParentObject.w_CHECKFORM
      do case
        case this.w_NODATE
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Inserire le date di validit� nella fase (%1)")
          this.w_oPART.AddParam(Alltrim(this.w_MESS))     
        case this.w_NOCLDES
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Inserire la descrizione della fase (%1)" )
          this.w_oPART.AddParam(Alltrim(str(this.w_CPROWORD)))     
      endcase
      this.w_oMESS.ah_ErrorMsg()
    else
      * --- Controlla che esista uno e un solo indice di preferenza '00' per ogni Fase
      Select t_cproword,t_clfasdes,sum(iif(t_clindfas="00",1,0)) as indfas from (this.Curso) ; 
 group by t_cproword having indfas>1 into cursor minfas
      scan for this.oParentObject.w_CHECKFORM
      this.w_CPROWORD = t_cproword
      this.w_MESS = alltrim(t_clfasdes)
      Select t_clinival, t_clfinval from (this.Curso) ; 
 where t_cproword=this.w_CPROWORD and t_clindfas="00" into array prefas
      this.MAXFAS = alen(prefas,1)
      do while this.MAXFAS>1 and this.oParentObject.w_CHECKFORM
        this.MAXFAS1 = this.MAXFAS - 1
        do while this.MAXFAS1>0 and this.oParentObject.w_CHECKFORM
          * --- w_CHECKFORM=false se gli intervalli di validit� non sono tutti disgiunti
          this.oParentObject.w_CHECKFORM = not ((prefas(this.MAXFAS,1)<=prefas(this.MAXFAS1,1) and prefas(this.MAXFAS1,1)<=prefas(this.MAXFAS,2)) ;
          or (prefas(this.MAXFAS1,1)<=prefas(this.MAXFAS,1) and prefas(this.MAXFAS,1)<=prefas(this.MAXFAS1,2)))
          this.MAXFAS1 = this.MAXFAS1 - 1
        enddo
        this.MAXFAS = this.MAXFAS-1
      enddo
      release prefas
      endscan
      if not this.oParentObject.w_CHECKFORM
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Nella fase (%1) deve essere impostato un solo indice di preferenza '00'")
        this.w_oPART.AddParam(Alltrim(this.w_MESS))     
        this.w_oMESS.ah_ErrorMsg(48, "")
      else
        * --- Controlla che esista una fase pref. per ogni fase non pref.
        Select t_cproword, t_clfasdes, t_clinival, t_clfinval from (this.Curso) ; 
 where t_clindfas<>"00" into cursor minfas
        scan for this.oParentObject.w_CHECKFORM
        this.w_CPROWORD = t_cproword
        this.w_MESS = alltrim(t_clfasdes)
        Select t_clinival, t_clfinval from (this.Curso) ; 
 where t_cproword=this.w_CPROWORD and t_clindfas="00" into array prefas
        this.MAXFAS = iif(type("prefas")="U", 0, alen(prefas,1))
        this.w_CHF1 = .F.
        this.w_CHF2 = .F.
        do while this.MAXFAS>0
          this.w_CHF1 = this.w_CHF1 or (prefas(this.MAXFAS,1)<=minfas.t_clinival and minfas.t_clinival<=prefas(this.MAXFAS,2))
          this.w_CHF2 = this.w_CHF2 or (prefas(this.MAXFAS,1)<=minfas.t_clfinval and minfas.t_clfinval<=prefas(this.MAXFAS,2))
          this.MAXFAS = this.MAXFAS-1
        enddo
        release prefas
        this.oParentObject.w_CHECKFORM = this.w_CHF1 and this.w_CHF2
        endscan
        if not this.oParentObject.w_CHECKFORM
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Nella fase (%1) deve essere impostato un solo indice di preferenza '00'")
          this.w_oPART.AddParam(Alltrim(this.w_MESS))     
          this.w_oMESS.ah_ErrorMsg(48, "")
        else
          * --- Controlla che esista una fase pref. per ogni fase non pref.
          this.Curso = this.PunPadre.GSCI_MLR.cTrsname
          Select * from (this.Curso) ; 
 where t_lrtemris=0 and not empty(nvl(t_lrcodris," ")) ; 
 into cursor minfas
          scan for this.oParentObject.w_CHECKFORM
          this.oParentObject.w_CHECKFORM = .F.
          if NOT this.oParentObject.w_CHECKFORM
            this.w_MESS = "Sono presenti risorse senza tempi di attrezzaggio, avviamento o lavorazione"
            ah_ErrorMsg(this.w_MESS, 48, "")
          endif
          endscan
        endif
      endif
      USE IN minfas
    endif
    if this.oParentObject.w_CHECKFORM
      * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      * --- Controlla che per ogni fase sia presente un solo centro di lavoro
      * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      this.w_TESTFORM = .T.
      Select DISTINCT this.oparentobject.w_CPROWNUM as T_LRROWNUM, T_LRCODRIS , T_LR1TIPO from (this.Curso) into cursor __btc1__ where T_LR1TIPO="CL"
      if reccount("__btc1__")>0
        Select T_LRROWNUM, SUM(IIF(Empty(T_LRCODRIS), 0 , iif(T_LR1TIPO="CL", 1, 0))) as t_lr_tipo from __BTC1__ ; 
 into cursor minfas group by T_LRROWNUM having SUM(IIF(Empty(T_LRCODRIS), 0 , iif(T_LR1TIPO="CL", 1, 0)))=0 ; 
 or SUM(IIF(Empty(T_LRCODRIS), 0 , iif(T_LR1TIPO="CL", 1, 0)))>1
        scan for this.w_TESTFORM
        this.w_TESTFORM = .F.
        if NOT this.w_TESTFORM
          if t_lr_tipo > 1
            this.oParentObject.w_CHECKFORM = .F.
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Sono presenti fasi con pi� di un centro di lavoro associato%0Inserire un solo centro di lavoro")
            this.w_oPART.AddParam(Alltrim(this.w_MESS))     
            this.w_oMESS.ah_ErrorMsg(48, "")
          endif
        endif
        endscan
        use in select("minfas")
      endif
      use in select("__btc1_")
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione Distinta
    if this.oParentObject.w_CLCODDIS<>this.oParentObject.w_CODDIS
      this.oParentObject.w_CODDIS = this.oParentObject.w_CLCODDIS
      this.w_CODART = this.oParentObject.w_CLCODART
      if empty(this.oParentObject.w_CLCODDIS)
        if Used(this.oParentObject.w_COMPON)
          Use IN (this.oParentObject.w_COMPON)
        endif
        if Used(this.oParentObject.w_COUTPUT)
          Use IN (this.oParentObject.w_COUTPUT)
        endif
      else
        * --- Carica solo il primo livello della distinta
        GSCI_BEX(this,this.w_CODART, this.oParentObject.w_CODDIS, this.oParentObject.w_COMPON, .f., this.oParentObject.w_DATRIF,"I")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        GSCI_BEX(this,this.w_CODART, this.oParentObject.w_CODDIS, this.oParentObject.w_COUTPUT, .f., this.oParentObject.w_DATRIF,"O")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Type("this.PunPadre.gsci_mlr.gsci_mmi")<>"U"
        if upper( this.PunPadre.gsci_mlr.gsci_mmi.class )="STDDYNAMICCHILD"
          this.PunPadre.gsci_mlr.gsci_mmi.createrealchild()     
        endif
        * --- Aggiorna la TreeView
        this.PunPadre.gsci_mlr.gsci_mmi.NotifyEvent("Treeview")     
      endif
      if Type("this.PunPadre.gsci_mlr.gsci_mmo")<>"U"
        if upper( this.PunPadre.gsci_mlr.gsci_mmo.class )="STDDYNAMICCHILD"
          this.PunPadre.gsci_mlr.gsci_mmo.createrealchild()     
        endif
        * --- Aggiorna la TreeView
        this.PunPadre.gsci_mlr.gsci_mmo.NotifyEvent("Treeview")     
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTrans)
    this.pTrans=pTrans
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CIC_MAST'
    this.cWorkTables[2]='CIC_DETT'
    this.cWorkTables[3]='CIC_MAIN'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='DET_FASI'
    this.cWorkTables[7]='RIS_ORSE'
    this.cWorkTables[8]='CLR_MAST'
    this.cWorkTables[9]='UNIMIS'
    this.cWorkTables[10]='COD_FASI'
    this.cWorkTables[11]='ART_DIST'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_DET_FASI')
      use in _Curs_DET_FASI
    endif
    if used('_Curs_COD_FASI')
      use in _Curs_COD_FASI
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_GSCI2BCL')
      use in _Curs_GSCI2BCL
    endif
    if used('_Curs_GSCI5BCL')
      use in _Curs_GSCI5BCL
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_GSCI4BCL')
      use in _Curs_GSCI4BCL
    endif
    if used('_Curs_GSCI3BCL1')
      use in _Curs_GSCI3BCL1
    endif
    if used('_Curs_GSCI1BCL')
      use in _Curs_GSCI1BCL
    endif
    if used('_Curs_GSCI_BCL')
      use in _Curs_GSCI_BCL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTrans"
endproc
