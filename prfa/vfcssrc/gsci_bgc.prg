* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bgc                                                        *
*              Genera codici di fase                                           *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-28                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOREC,pTRANS,pSERIAL,pSERIAL2,pKEYRIF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bgc",oParentObject,m.pTIPOREC,m.pTRANS,m.pSERIAL,m.pSERIAL2,m.pKEYRIF)
return(i_retval)

define class tgsci_bgc as StdBatch
  * --- Local variables
  w_CLAFAS = space(5)
  pTIPOREC = space(1)
  pTRANS = .f.
  pSERIAL = space(10)
  pSERIAL2 = space(10)
  pKEYRIF = space(10)
  w_LNumErr = 0
  w_LNumOk = 0
  w_LSerial = space(10)
  w_LSelOpe = space(1)
  w_LDettTec = space(1)
  w_LOperaz = space(2)
  w_LEERR = space(250)
  w_LDatini = ctod("  /  /  ")
  w_LDatfin = ctod("  /  /  ")
  PUNPAD = .NULL.
  w_ARDESART = space(40)
  w_TmpC = space(100)
  w_ARDESSUP = space(0)
  w_ARUNMIS1 = space(3)
  w_CICLO = space(20)
  w_ARUNMIS2 = space(3)
  w_FASE = space(3)
  w_AROPERAT = space(1)
  w_FASED = space(3)
  w_ARMOLTIP = 0
  w_DISTINTA = space(20)
  w_ARGRUMER = space(5)
  w_ARCODFAM = space(5)
  w_CODART = space(20)
  w_ARCATOMO = space(5)
  w_ARCATCON = space(5)
  w_CACODICE = space(20)
  w_ARCODIVA = space(5)
  w_ARFLINVE = space(1)
  w_DVDESSUP1 = space(40)
  w_DVDESSUP = space(0)
  w_DESSUP = space(0)
  w_ARTIPART = space(2)
  w_DPFAMPRO = space(5)
  w_CLOURIFE = 0
  w_CLBFRIFE = 0
  w_SALVAPAR = space(10)
  w_ARFLLOTT = space(1)
  w_DESCFASE = space(40)
  w_ARCODRIC = space(5)
  w_ARFLDISP = space(1)
  w_ARMAGPRE = space(5)
  w_ARCODART = space(20)
  w_ARSTASUP = space(1)
  w_CLCOUPOI = space(1)
  w_FASCLA = 0
  w_CLCPRIFE = 0
  w_CPROWORD = 0
  w_PRECLAOUT = .f.
  w_PRIMOGIRO = .f.
  w_PRECLAOUT = .f.
  w_FASPRECLA = 0
  w_CLFASOUT = space(1)
  w_CLCODFAS = space(66)
  w_ARCODCEN = space(15)
  w_CODARTM = space(20)
  w_DESARTM = space(40)
  w_ARPREZUM = space(1)
  w_ARCCRIFE = space(20)
  w_PPCATCON = space(5)
  w_PPDSUPFA = space(1)
  w_TRANS = .f.
  w_CLASS = space(10)
  w_CLCODODL = space(15)
  w_CLSERIAL = space(10)
  w_CLSERIAL1 = space(10)
  w_ASKAGDES = .f.
  w_AGDESSUP = .f.
  w_PPPREFAS = space(5)
  w_PPFASSEP = space(1)
  w_PPCLAFAS = space(5)
  w_PPDCODFAS = space(1)
  w_PPCLAART = space(5)
  w_PPNOTFAS = space(1)
  w_LogTitle = space(10)
  w_INIZ = 0
  w_FINE = 0
  w_TOTELAB = 0
  w_INIZ_DATE = ctod("  /  /  ")
  w_oWIPLOG = .NULL.
  w_CLARTFAS = space(20)
  w_CLMGARFS = space(1)
  w_CLPRECOD = space(5)
  w_CLPREFAS = space(5)
  w_CLFASSEP = space(1)
  w_lunghFase = 0
  w_PREFFASE = space(5)
  w_SEPFASE = space(1)
  w_EXISTART = .f.
  w_CACODFAS = space(66)
  w_AGALLSUP = .f.
  w_TMPC1 = space(40)
  w_First = .f.
  w_LOggErr = space(41)
  w_LOperaz = space(20)
  w_LErrore = space(80)
  w_LOra = space(8)
  w_LMessage = space(0)
  w_CLMAGWIP = space(5)
  w_FLCP = space(1)
  w_FLOU = space(1)
  w_FLCF = space(1)
  w_CLCLAOUT = space(1)
  w_CPROWORD = 0
  w_CLFASOUT = space(1)
  w_PRIMOGIRO = .f.
  w_COUNTPOI = space(1)
  w_CLOURIFE = 0
  w_CPROWNUM = 0
  w_FASPRECLA = 0
  w_CLBFRIFE = 0
  w_PRECLAOUT = .f.
  w_FASCLA = 0
  w_CLCOUPOI = space(1)
  w_CLROWORD = space(6)
  w_FASCLA = 0
  w_GESTLOG = .f.
  w_CLLTFASE = 0
  w_CLINDPRE = space(2)
  w_FLCP = space(1)
  w_FLOU = space(1)
  w_FLCF = space(1)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_CLWIPOUT = space(20)
  w_CLWIPFPR = space(20)
  w_CLDESFAS = space(40)
  w_WIPFASEPREC = space(41)
  w_WIPFASEOLD = space(41)
  w_CODICE = space(100)
  w_LUNGH = 0
  w_FUNZIONE = space(1)
  w_VALORE = space(66)
  w_CFSEPASN = space(1)
  w_SEPARA = space(1)
  w_PROGRE = 0
  w_PROGOLD = 0
  w_PARPAD = space(1)
  w_START = space(35)
  * --- WorkFile variables
  CIC_DETT_idx=0
  CIC_MAST_idx=0
  PAR_PROD_idx=0
  DISMBASE_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  PRD_ERRO_idx=0
  RIS_MAST_idx=0
  PAR_RIOR_idx=0
  TMPCNLAV_idx=0
  ODL_CICL_idx=0
  ODL_MAST_idx=0
  ART_TEMP_idx=0
  CCF_DETT_idx=0
  CCF_MAST_idx=0
  TMPODL_CICL_idx=0
  COD_ACFA_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Archivio su cui eseguire la generazione
    *     O = ODL_CICL
    *     C = CIC_DETT
    * --- Indica se deve aprire oppure no la transazione
    * --- Eventuale seriale del ciclo da generare
    * --- Utilizzato per la copia del ciclo di lavorazione
    * --- KEYRIF per elaborazione temporaneo ODL
    * --- Variabile di test che indica se aprire la transazione
    do case
      case this.pTIPOREC $ "O-T"
        this.w_CLCODODL = this.pSERIAL
        * --- Read from ODL_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OLTSECIC"+;
            " from "+i_cTable+" ODL_MAST where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OLTSECIC;
            from (i_cTable) where;
                OLCODODL = this.w_CLCODODL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CLSERIAL = NVL(cp_ToDate(_read_.OLTSECIC),cp_NullValue(_read_.OLTSECIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      otherwise
        this.w_CLSERIAL = this.pSERIAL
        this.w_CLSERIAL1 = this.pSERIAL2
    endcase
    this.w_SALVAPAR = this.w_CLSERIAL
    * --- Recupero il padre chiamante per vedere se devo aprire la transazione
    this.PUNPAD = this.oParentObject
    this.w_CLASS = UPPER(ALLTRIM(this.PUNPAD.Class))
    * --- Effettuo il test perch� la chiamata potrebbe non arrivare direttamente dall'ODL
    this.w_TRANS = this.pTRANS
    * --- Variabili per gestione errori
    this.w_LNumErr = 0
    this.w_LNumOk = 0
    this.w_LSerial = space(10)
    this.w_LSelOpe = "U"
    this.w_LDettTec = "N"
    this.w_LOperaz = "CL"
    this.w_LDatini = i_DATSYS
    this.w_LDatfin = i_DATSYS
    this.PUNPAD = this.oParentObject
    * --- Verifica preliminare. Deve fare qualcosa ?
    do case
      case this.pTIPOREC="O"
        if not empty(this.w_CLCODODL)
          * --- Read from ODL_CICL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" ODL_CICL where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                  +" and CLFASOUT = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CLCODODL = this.w_CLCODODL;
                  and CLFASOUT = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            * --- Blocca tutto, non c'e' nulla da generare
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pTIPOREC="T"
        * --- Se non esiste tabella temporanea la creo
        * --- Create temporary table TMPODL_CICL
        if !cp_ExistTableDef('TMPODL_CICL')
          i_nIdx=cp_AddTableDef('TMPODL_CICL',.t.) && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
                )
          this.TMPODL_CICL_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          if this.TMPODL_CICL_idx=0
            this.TMPODL_CICL_idx=cp_ascan(@i_TableProp,lower('TMPODL_CICL'),i_nTables)
          endif
        endif
        if cp_ExistTableDef("TMPODL_CICL")
          * --- Try
          local bErr_04436D00
          bErr_04436D00=bTrsErr
          this.Try_04436D00()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Create temporary table TMPODL_CICL
            i_nIdx=cp_AddTableDef('TMPODL_CICL') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
                  )
            this.TMPODL_CICL_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          bTrsErr=bTrsErr or bErr_04436D00
          * --- End
        endif
        if not empty(this.w_CLCODODL)
          * --- Read from TMPODL_CICL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" TMPODL_CICL where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                  +" and CLFASOUT = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CLCODODL = this.w_CLCODODL;
                  and CLFASOUT = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            * --- Blocca tutto, non c'e' nulla da generare
            i_retcode = 'stop'
            return
          endif
        endif
      otherwise
        if not empty(this.w_CLSERIAL)
          * --- Read from CIC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CIC_DETT where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CLSERIAL = this.w_CLSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            * --- Blocca tutto, non c'e' nulla da generare
            i_retcode = 'stop'
            return
          endif
        endif
    endcase
    if Upper(Alltrim(this.PUNPAD.Class))="TGSCI_BSR" or Upper(Alltrim(this.PUNPAD.Class))="TGSCI_BDF"
      * --- Se arrivo da Duplicazione distinta base oppure duplicazione cicli di lavoro aggiorno sempre descrizione supplementare
      this.w_ASKAGDES = .F.
      this.w_AGALLSUP = .T.
      this.w_AGDESSUP = .T.
    else
      if not empty(this.w_CLSERIAL)
        this.w_ASKAGDES = .T.
        this.w_First = .T.
      else
        * --- Caso in cui arrivo dalla generazione codici WIP/fase
        this.w_ASKAGDES = .F.
        this.w_AGALLSUP = .T.
        this.w_AGDESSUP = .T.
      endif
    endif
    * --- Verifica se definito la variante di supporto al c/lavoro
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPDSUPFA,PPPREFAS,PPFASSEP,PPCLAFAS,PPDCODFAS,PPCLAART,PPNOTFAS"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPDSUPFA,PPPREFAS,PPFASSEP,PPCLAFAS,PPDCODFAS,PPCLAART,PPNOTFAS;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPDSUPFA = NVL(cp_ToDate(_read_.PPDSUPFA),cp_NullValue(_read_.PPDSUPFA))
      this.w_PPPREFAS = NVL(cp_ToDate(_read_.PPPREFAS),cp_NullValue(_read_.PPPREFAS))
      this.w_PPFASSEP = NVL(cp_ToDate(_read_.PPFASSEP),cp_NullValue(_read_.PPFASSEP))
      this.w_PPCLAFAS = NVL(cp_ToDate(_read_.PPCLAFAS),cp_NullValue(_read_.PPCLAFAS))
      this.w_PPDCODFAS = NVL(cp_ToDate(_read_.PPDCODFAS),cp_NullValue(_read_.PPDCODFAS))
      this.w_PPCLAART = NVL(cp_ToDate(_read_.PPCLAART),cp_NullValue(_read_.PPCLAART))
      this.w_PPNOTFAS = NVL(cp_ToDate(_read_.PPNOTFAS),cp_NullValue(_read_.PPNOTFAS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case empty(this.w_PPPREFAS)
        ah_ErrorMsg("Impossibile generare i codici di fase%0Indicare il prefisso da utilizzare per la generazione dei codici di fase","STOP","" )
        i_retcode = 'stop'
        return
      case empty(this.w_PPFASSEP)
        ah_ErrorMsg("Impossibile generare i codici di fase%0Indicare il separatore da utilizzare per la generazione dei codici di fase","STOP","" )
        i_retcode = 'stop'
        return
    endcase
    * --- Pulizia Log Errori
    * --- Try
    local bErr_02B75458
    bErr_02B75458=bTrsErr
    this.Try_02B75458()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_02B75458
    * --- End
    * --- Cerca le fasi marcate come output
    do case
      case this.pTIPOREC $ "O-T"
        this.w_GESTLOG = .F.
        this.w_LogTitle = space(10)+"Log elaborazione %2 ad hoc ENTERPRISE %1"
        * --- CURSORE PER I MESSAGGI
        if this.w_GESTLOG
          this.w_oWIPLOG=createobject("AH_ERRORLOG")
          this.w_oWIPLOG.ADDMSGLOG(this.w_LogTitle, g_VERSION, "Ordini")     
          this.w_oWIPLOG.ADDMSGLOG("================================================================================")     
          this.w_INIZ_DATE = DATE()
          this.w_INIZ = seconds()
          this.w_oWIPLOG.ADDMSGLOG("Inizio elaborazione M.R.P: [%1]", ALLTRIM(TTOC(DATETIME())))     
        endif
        if this.w_CLASS <> "TGSCI_KGC"
          if Empty(this.w_CLCODODL)
            this.w_CLCODODL = REPLICATE("X", 15)
          endif
        endif
        ah_msg("Generazione codici di fase in corso...")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_GESTLOG
          this.w_FINE = seconds()
          * --- se � cambiato il giorno faccio il conto con il numero di secondi di un giorno per il numero di giorni di elaborazione
          this.w_TOTELAB = this.w_fine+86400*(DATE()-this.w_INIZ_DATE)-this.w_iniz
          this.w_oWIPLOG.ADDMSGLOG("Tempo totale di elaborazione: %1", this.TimeMRP(this.w_totelab))     
          this.w_oWIPLOG.ADDMSGLOG("Fine elaborazione: [%1]", ALLTRIM(TTOC(DATETIME())))     
          this.w_oWIPLOG.ADDMSGLOG("================================================================================")     
          if ah_YesNo("Desideri la stampa del Log dell'elaborazione?")
            * --- Lancia Stampa
            this.w_WIPLOG.PRINTLOG(This , ah_MsgFormat(this.w_LogTitle, g_VERSION, "Ordini") , .f.)     
          endif
        endif
        if this.pTIPOREC="O"
          if ! empty(this.w_CLCODODL)
            ah_msg("Generazione codici supporto WIP/conto lavoro in corso...")
            * --- Select from ODL_CICL
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
                  +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" and CLFASSEL = 'S' and CLFASOUT = 'S'";
                   ,"_Curs_ODL_CICL")
            else
              select * from (i_cTable);
               where CLCODODL=this.w_CLCODODL and CLFASSEL = "S" and CLFASOUT = "S";
                into cursor _Curs_ODL_CICL
            endif
            if used('_Curs_ODL_CICL')
              select _Curs_ODL_CICL
              locate for 1=1
              do while not(eof())
              * --- Carica Codice
              this.w_CLCODODL = nvl(_Curs_ODL_CICL.CLCODODL,space(15))
              this.w_CLARTFAS = space(20)
              this.w_CLCODFAS = space(66)
              * --- Read from KEY_ARTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CACODICE"+;
                  " from "+i_cTable+" KEY_ARTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.w_CLCODFAS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CACODICE;
                  from (i_cTable) where;
                      CACODICE = this.w_CLCODFAS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CLCODFAS = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if empty(this.w_CLCODFAS)
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_ODL_CICL
                continue
              enddo
              use
            endif
          endif
        else
          if ! empty(this.w_CLCODODL)
            ah_msg("Generazione codici supporto WIP/conto lavoro in corso...")
            * --- Select from TMPODL_CICL
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPODL_CICL ";
                  +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" and CLFASSEL = 'S' and CLFASOUT = 'S' and CLKEYRIF = "+cp_ToStrODBC(this.pKEYRIF)+"";
                   ,"_Curs_TMPODL_CICL")
            else
              select * from (i_cTable);
               where CLCODODL=this.w_CLCODODL and CLFASSEL = "S" and CLFASOUT = "S" and CLKEYRIF = this.pKEYRIF;
                into cursor _Curs_TMPODL_CICL
            endif
            if used('_Curs_TMPODL_CICL')
              select _Curs_TMPODL_CICL
              locate for 1=1
              do while not(eof())
              * --- Carica Codice
              this.w_CLCODODL = nvl(_Curs_TMPODL_CICL.CLCODODL,space(15))
              this.w_CLARTFAS = space(20)
              this.w_CLCODFAS = space(66)
              if empty(this.w_CLCODFAS)
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_TMPODL_CICL
                continue
              enddo
              use
            endif
          endif
        endif
      otherwise
        if empty(this.w_CLSERIAL)
          * --- Select from CIC_MAST
          i_nConn=i_TableProp[this.CIC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_MAST ";
                 ,"_Curs_CIC_MAST")
          else
            select * from (i_cTable);
              into cursor _Curs_CIC_MAST
          endif
          if used('_Curs_CIC_MAST')
            select _Curs_CIC_MAST
            locate for 1=1
            do while not(eof())
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_CIC_MAST
              continue
            enddo
            use
          endif
          ah_msg("Generazione codici di fase in corso...")
          * --- Select from CIC_DETT
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_DETT ";
                +" where CLCOUPOI = 'S'";
                 ,"_Curs_CIC_DETT")
          else
            select * from (i_cTable);
             where CLCOUPOI = "S";
              into cursor _Curs_CIC_DETT
          endif
          if used('_Curs_CIC_DETT')
            select _Curs_CIC_DETT
            locate for 1=1
            do while not(eof())
            * --- Carica Codice
            this.w_CLSERIAL = nvl(_Curs_CIC_DETT.CLSERIAL,space(10))
            this.w_CLARTFAS = NVL(_Curs_CIC_DETT.CLARTFAS,space(20))
            this.w_CLCODFAS = nvl(_Curs_CIC_DETT.CLCODFAS,SPACE(66))
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CACODICE"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CLCODFAS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CACODICE;
                from (i_cTable) where;
                    CACODICE = this.w_CLCODFAS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLCODFAS = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Empty(this.w_CLARTFAS) or empty(this.w_CLCODFAS)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
              select _Curs_CIC_DETT
              continue
            enddo
            use
          endif
        else
          * --- Select from CIC_MAST
          i_nConn=i_TableProp[this.CIC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_MAST ";
                +" where CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL)+"";
                 ,"_Curs_CIC_MAST")
          else
            select * from (i_cTable);
             where CLSERIAL = this.w_CLSERIAL;
              into cursor _Curs_CIC_MAST
          endif
          if used('_Curs_CIC_MAST')
            select _Curs_CIC_MAST
            locate for 1=1
            do while not(eof())
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_CIC_MAST
              continue
            enddo
            use
          endif
          ah_msg("Generazione codici di fase in corso...")
          this.w_PRIMOGIRO = .T.
          * --- Select from CIC_DETT
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_DETT ";
                +" where CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)+" and CLCOUPOI = 'S'";
                +" order by CPROWORD";
                 ,"_Curs_CIC_DETT")
          else
            select * from (i_cTable);
             where CLSERIAL=this.w_CLSERIAL and CLCOUPOI = "S";
             order by CPROWORD;
              into cursor _Curs_CIC_DETT
          endif
          if used('_Curs_CIC_DETT')
            select _Curs_CIC_DETT
            locate for 1=1
            do while not(eof())
            * --- Carica Codice
            this.w_CLARTFAS = NVL(_Curs_CIC_DETT.CLARTFAS,space(20))
            this.w_CLCODFAS = NVL(_Curs_CIC_DETT.CLCODFAS,space(66))
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARCODART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_CLARTFAS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARCODART;
                from (i_cTable) where;
                    ARCODART = this.w_CLARTFAS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLARTFAS = NVL(cp_ToDate(_read_.ARCODART),cp_NullValue(_read_.ARCODART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_CIC_DETT
              continue
            enddo
            use
          endif
        endif
    endcase
    * --- Pulisce wait window
    wait CLEAR
    * --- Completa
    * --- Inizializza Variabili Locali
    do case
      case this.pTIPOREC="C" or (this.pTIPOREC="O" and Upper(Alltrim(this.oParentobject.class))="TGSCI_KGC")
        if empty(this.w_SALVAPAR)
          if this.w_LNumErr=0
            if this.w_LNumOk=0
              this.w_TmpC = "Non esistono nuovi codici da generare%0Elaborazione completata con successo"
            else
              this.w_TmpC = "Generazione codici di fase completata con successo%0Sono stati generati/verificati %1 codici"
            endif
            ah_ErrorMsg(this.w_TmpC,48," ",alltrim(str(this.w_LNumOk,6,0)))
          else
            if this.w_LNumOk=0
              this.w_TmpC = this.w_TmpC + "Si sono verificati errori (%2) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?"
            else
              this.w_TmpC = "Sono stati generati/verificati %1 codici con successo%0Si sono verificati errori (%2) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?"
            endif
            if ah_YesNo(this.w_TmpC,"",alltrim(str(this.w_LNumOk,6,0)), alltrim(str(this.w_LNumErr,5,0)))
              * --- Lancia Stampa
              do GSCO_BLE with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if Upper(Alltrim(this.oParentobject.class))="TGSCI_KGC"
            * --- Chiude maschera chiamante
            this.PUNPAD.ecpQuit()     
          endif
        else
          if this.w_LNumErr > 0
            this.w_TmpC = "Si sono verificati errori (%1) durante la generazione/aggiornamento dei codici di fase.%0Desideri la stampa dell'elenco degli errori?"
            if ah_YesNo(this.w_TmpC,"",alltrim(str(this.w_LNumErr,5,0)))
              * --- Lancia Stampa
              do GSCO_BLE with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
    endcase
  endproc
  proc Try_04436D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from TMPODL_CICL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TMPODL_CICL where ";
            +"1 = "+cp_ToStrODBC(2);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            1 = 2;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return
  proc Try_02B75458()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRD_ERRO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LESTORIC ="+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTORIC');
          +i_ccchkf ;
      +" where ";
          +"LECODUTE = "+cp_ToStrODBC(i_CODUTE);
          +" and LEOPERAZ = "+cp_ToStrODBC(this.w_LOperaz);
          +" and LESTORIC = "+cp_ToStrODBC("S");
             )
    else
      update (i_cTable) set;
          LESTORIC = "N";
          &i_ccchkf. ;
       where;
          LECODUTE = i_CODUTE;
          and LEOPERAZ = this.w_LOperaz;
          and LESTORIC = "S";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione CODICI C/LAVORO e WIP
    * --- Legge dati da master
    this.w_lunghFase = 3
    * --- Read from CIC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CIC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CLCODCIC,CLCODART,CLCODDIS,CLMGARFS,CLPREFAS,CLFASSEP,CLPRECOD"+;
        " from "+i_cTable+" CIC_MAST where ";
            +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CLCODCIC,CLCODART,CLCODDIS,CLMGARFS,CLPREFAS,CLFASSEP,CLPRECOD;
        from (i_cTable) where;
            CLSERIAL = this.w_CLSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CICLO = NVL(cp_ToDate(_read_.CLCODCIC),cp_NullValue(_read_.CLCODCIC))
      this.w_CODART = NVL(cp_ToDate(_read_.CLCODART),cp_NullValue(_read_.CLCODART))
      this.w_CODARTM = NVL(cp_ToDate(_read_.CLCODART),cp_NullValue(_read_.CLCODART))
      this.w_DISTINTA = NVL(cp_ToDate(_read_.CLCODDIS),cp_NullValue(_read_.CLCODDIS))
      this.w_CLMGARFS = NVL(cp_ToDate(_read_.CLMGARFS),cp_NullValue(_read_.CLMGARFS))
      this.w_CLPREFAS = NVL(cp_ToDate(_read_.CLPREFAS),cp_NullValue(_read_.CLPREFAS))
      this.w_CLFASSEP = NVL(cp_ToDate(_read_.CLFASSEP),cp_NullValue(_read_.CLFASSEP))
      this.w_CLPRECOD = NVL(cp_ToDate(_read_.CLPRECOD),cp_NullValue(_read_.CLPRECOD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pTIPOREC="O"
        * --- Determina fase
        this.w_FASE = right("000"+alltrim(str(_Curs_ODL_CICL.CLROWORD,this.w_lunghFase,0)),this.w_lunghFase)
        this.w_DESCFASE = ALLTRIM(NVL(_Curs_ODL_CICL.CLDESFAS, ""))
      case this.pTIPOREC="T"
        * --- Determina fase
        this.w_FASE = right("000"+alltrim(str(_Curs_TMPODL_CICL.CLROWORD,this.w_lunghFase,0)),this.w_lunghFase)
        this.w_DESCFASE = rtrim(_Curs_TMPODL_CICL.CLDESFAS)
      otherwise
        * --- Determina fase
        this.w_FASE = right("000"+alltrim(str(_Curs_CIC_DETT.CPROWORD,this.w_lunghFase,0)),this.w_lunghFase)
        this.w_DESCFASE = ALLTRIM(NVL(_Curs_CIC_DETT.CLFASDES, ""))
    endcase
    * --- Crea articolo di appoggio gestito a varianti
    * --- Read from DISMBASE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DBDESCRI"+;
        " from "+i_cTable+" DISMBASE where ";
            +"DBCODICE = "+cp_ToStrODBC(this.w_DISTINTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DBDESCRI;
        from (i_cTable) where;
            DBCODICE = this.w_DISTINTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESARTM = NVL(cp_ToDate(_read_.DBDESCRI),cp_NullValue(_read_.DBDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Try
    local bErr_04461648
    bErr_04461648=bTrsErr
    this.Try_04461648()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      do case
        case this.w_TRANS
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
      endcase
      this.w_LOggErr = this.w_DISTINTA
      this.w_LErrore = i_ErrMsg
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_04461648
    * --- End
  endproc
  proc Try_04461648()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_TRANS
        * --- Nel caso arrivo dai controlli dell'ODL la transazione � gi� aperta
        * --- begin transaction
        cp_BeginTrs()
    endcase
    if this.w_CLMGARFS = "P"
      if !Empty(this.w_CLARTFAS)
        * --- Se esiste il codice di fase lo scompongo per recuperare il vecchio prefisso e vecchio separatore
        this.w_PREFFASE = LEFT(this.w_CLARTFAS, 5)
        this.w_SEPFASE = SUBSTR(this.w_CLARTFAS, 6, 1)
      endif
      * --- Deve creare un nuovo articolo con codice formato da (prefisso+separatore+seriale ciclo+separatore+fase)
      * --- Inizializzo con prefisso e separatore del ciclo
      this.w_PREFFASE = this.w_CLPREFAS
      this.w_SEPFASE = this.w_CLFASSEP
      this.w_CODART = this.w_PREFFASE+this.w_SEPFASE+ALLTRIM(STR(VAL(this.w_CLSERIAL),10,0))+this.w_SEPFASE+this.w_FASE
    else
      if empty(nvl(this.w_CLPREFAS,space(5)))
        this.w_CODART = ""
      else
        * --- Legge parametrizzazione e valorizza il codice di fase di conseguenza
        this.w_CLAFAS = this.w_CLPREFAS
        this.w_PARPAD = "A"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Assegno il valore della variabile w_CODICE calcolato pagina 6, reso generico anche per calcolo codice articolo
        this.w_CODART = this.w_CODICE
      endif
    endif
    if len(alltrim(this.w_CODART))>20
      * --- Al massimo possiamo gestire codici lunghi 20
      this.w_TmpC = Ah_msgformat("Codice Articolo (%1) superiore a 20 C",alltrim(this.w_CODART))
      * --- Raise
      i_Error=this.w_TmpC
      return
    else
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARDESART,ARDESSUP,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGRUMER,ARCODFAM,ARCATOMO,ARCATCON,ARCODIVA,ARFLINVE,ARFLLOTT,ARCODRIC,ARFLDISP,ARMAGPRE,ARSTASUP,ARCODCEN,ARPREZUM,ARTIPART,ARCCRIFE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODARTM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARDESART,ARDESSUP,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGRUMER,ARCODFAM,ARCATOMO,ARCATCON,ARCODIVA,ARFLINVE,ARFLLOTT,ARCODRIC,ARFLDISP,ARMAGPRE,ARSTASUP,ARCODCEN,ARPREZUM,ARTIPART,ARCCRIFE;
          from (i_cTable) where;
              ARCODART = this.w_CODARTM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
        this.w_ARDESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
        this.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_AROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_ARMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        this.w_ARUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        this.w_ARGRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
        this.w_ARCODFAM = NVL(cp_ToDate(_read_.ARCODFAM),cp_NullValue(_read_.ARCODFAM))
        this.w_ARCATOMO = NVL(cp_ToDate(_read_.ARCATOMO),cp_NullValue(_read_.ARCATOMO))
        this.w_ARCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
        this.w_ARCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
        this.w_ARFLINVE = NVL(cp_ToDate(_read_.ARFLINVE),cp_NullValue(_read_.ARFLINVE))
        this.w_ARFLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_ARCODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
        this.w_ARFLDISP = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
        this.w_ARMAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        this.w_ARSTASUP = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
        this.w_ARCODCEN = NVL(cp_ToDate(_read_.ARCODCEN),cp_NullValue(_read_.ARCODCEN))
        this.w_ARPREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
        this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        this.w_ARCCRIFE = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ARTIPART = "FS"
      * --- Verifica se l'articolo c'� gi� ...
      this.w_ARCODART = alltrim(this.w_CODART)
      this.w_EXISTART = .T.
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ARCODART = this.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
        if i_Rows=0
          this.w_EXISTART = .F.
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Devo prendere al massimo i primi 40 caratteri
      do case
        case this.w_PPDCODFAS="D"
          * --- Descrizione della fase
          this.w_ARDESART = this.w_DESCFASE
        case this.w_PPDCODFAS="C"
          * --- Descrizione articolo fase
          this.w_ARDESART = LEFT(ALLTRIM(this.w_ARDESART) + " " + this.w_DESCFASE + SPACE(40), 40)
        case this.w_PPDCODFAS="A"
          * --- Codice articolo+codice fase
          this.w_ARDESART = LEFT(ALLTRIM(this.w_CODARTM) + " " + this.w_FASE + SPACE(40), 40)
        otherwise
          * --- Non faccio niente gi� letto da descrizione articolo
      endcase
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      * --- Descrizione supplementare
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      this.w_DESSUP = SPACE(0)
      * --- Devo prendere al massimo i primi 4000 caratteri
      do case
        case this.w_PPDSUPFA="D"
          * --- Descrizione della fase
          this.w_ARDESSUP = this.w_DESCFASE
        case this.w_PPDSUPFA="G"
          * --- Dettaglio
          this.w_ARDESSUP = ah_msgformat("CODICE ARTICOLO=%1%0DESCRIZIONE ARTICOLO=%2%0DISTINTA BASE=%3%0CICLO=%4%0FASE=%5",alltrim(this.w_CODARTM),alltrim(this.w_DESARTM),alltrim(this.w_DISTINTA),alltrim(this.w_CICLO),alltrim(this.w_FASE) )
          this.w_DESSUP = this.w_DESSUP + chr(13) 
        case this.w_PPDSUPFA="N"
          * --- Nessuna descizione
          this.w_ARDESSUP = ""
      endcase
      * --- Se � presente il flag Note fase aggiungo le note della fase
      do case
        case this.pTIPOREC="C"
          this.w_CLSERIAL1 = _Curs_CIC_DETT.CLSERIAL
      endcase
      if this.w_PPNOTFAS="S"
        do case
          case this.pTIPOREC="O"
            do case
              case this.w_PPDSUPFA <> "N"
                * --- Create temporary table TMPCNLAV
                i_nIdx=cp_AddTableDef('TMPCNLAV') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('gsci100bcl',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPCNLAV_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Select from TMPCNLAV
                i_nConn=i_TableProp[this.TMPCNLAV_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPCNLAV_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCNLAV ";
                      +" where FASE="+cp_ToStrODBC(_Curs_ODL_CICL.CLROWORD)+"";
                       ,"_Curs_TMPCNLAV")
                else
                  select * from (i_cTable);
                   where FASE=_Curs_ODL_CICL.CLROWORD;
                    into cursor _Curs_TMPCNLAV
                endif
                if used('_Curs_TMPCNLAV')
                  select _Curs_TMPCNLAV
                  locate for 1=1
                  do while not(eof())
                  this.w_DESSUP = this.w_DESSUP+ ah_msgformat(iif(Empty(_Curs_TMPCNLAV.LRISTLAV),"Operazione: %1%0Pref: %2","Operazione: %1%0Pref: %2 NOTE FASE=%3"),alltrim(_Curs_TMPCNLAV.CLFASDES),alltrim(_Curs_TMPCNLAV.CLINDFAS) ,alltrim(NVL(_Curs_TMPCNLAV.LRISTLAV,space(0))))+chr(10) 
                    select _Curs_TMPCNLAV
                    continue
                  enddo
                  use
                endif
            endcase
          case this.pTIPOREC="T"
            do case
              case this.w_PPDSUPFA <> "N"
                * --- Create temporary table TMPCNLAV
                i_nIdx=cp_AddTableDef('TMPCNLAV') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('gsci101bcl',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPCNLAV_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Select from TMPCNLAV
                i_nConn=i_TableProp[this.TMPCNLAV_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPCNLAV_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCNLAV ";
                      +" where FASE="+cp_ToStrODBC(_Curs_TMPODL_CICL.CLROWORD)+"";
                       ,"_Curs_TMPCNLAV")
                else
                  select * from (i_cTable);
                   where FASE=_Curs_TMPODL_CICL.CLROWORD;
                    into cursor _Curs_TMPCNLAV
                endif
                if used('_Curs_TMPCNLAV')
                  select _Curs_TMPCNLAV
                  locate for 1=1
                  do while not(eof())
                  this.w_DESSUP = this.w_DESSUP+ ah_msgformat(iif(Empty(_Curs_TMPCNLAV.LRISTLAV),"Operazione: %1%0Pref: %2","Operazione: %1%0Pref: %2 NOTE FASE=%3"),alltrim(_Curs_TMPCNLAV.CLFASDES),alltrim(_Curs_TMPCNLAV.CLINDFAS) ,alltrim(NVL(_Curs_TMPCNLAV.LRISTLAV,space(0))))+chr(10) 
                    select _Curs_TMPCNLAV
                    continue
                  enddo
                  use
                endif
            endcase
          otherwise
            * --- Create temporary table TMPCNLAV
            i_nIdx=cp_AddTableDef('TMPCNLAV') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('gsci7bcl',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPCNLAV_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Select from TMPCNLAV
            i_nConn=i_TableProp[this.TMPCNLAV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCNLAV_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCNLAV ";
                  +" where FASE="+cp_ToStrODBC(_Curs_CIC_DETT.CPROWORD)+" ";
                   ,"_Curs_TMPCNLAV")
            else
              select * from (i_cTable);
               where FASE=_Curs_CIC_DETT.CPROWORD ;
                into cursor _Curs_TMPCNLAV
            endif
            if used('_Curs_TMPCNLAV')
              select _Curs_TMPCNLAV
              locate for 1=1
              do while not(eof())
              this.w_DESSUP = this.w_DESSUP+ ah_msgformat(iif(Empty(_Curs_TMPCNLAV.LRISTLAV),"Operazione: %1%0Pref: %2","Operazione: %1%0Pref: %2 NOTE FASE=%3"),alltrim(_Curs_TMPCNLAV.CLFASDES),alltrim(_Curs_TMPCNLAV.CLINDFAS) ,alltrim(NVL(_Curs_TMPCNLAV.LRISTLAV,space(0))))+chr(10) 
                select _Curs_TMPCNLAV
                continue
              enddo
              use
            endif
        endcase
        * --- Drop temporary table TMPCNLAV
        i_nIdx=cp_GetTableDefIdx('TMPCNLAV')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPCNLAV')
        endif
        this.w_ARDESSUP = this.w_ARDESSUP + this.w_DESSUP
      endif
      * --- 1) Crea nuovo codice articolo
      if this.w_PPDSUPFA<>"N" OR this.w_PPNOTFAS="S"
        this.w_ARDESSUP = left(alltrim(this.w_ARDESSUP),iif(CP_DBTYPE="Oracle",1954,3954)) + iif(empty(this.w_ARDESSUP),"",chr(13))
      endif
      if !this.w_EXISTART
        * --- Try
        local bErr_044A07E8
        bErr_044A07E8=bTrsErr
        this.Try_044A07E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_TmpC = Ah_msgformat("Impossibile creare codice %1",alltrim(this.w_ARCODART))
          * --- Raise
          i_Error=this.w_TmpC
          return
        endif
        bTrsErr=bTrsErr or bErr_044A07E8
        * --- End
      else
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
          +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARDESART = this.w_ARDESART;
              ,ARDESSUP = this.w_ARDESSUP;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_CACODICE = this.w_ARCODART
      if empty(nvl(this.w_PPCLAFAS,space(5)))
        this.w_CACODFAS = ALLTRIM(this.w_CODARTM)+"."+ALLTRIM(this.w_DISTINTA)+"."+ALLTRIM(this.w_CICLO)+"."+this.w_FASE
      else
        * --- Legge parametrizzazione e valorizza il codice di fase di conseguenza
        this.w_CLAFAS = this.w_CLPRECOD
        this.w_PARPAD = "F"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Assegno il valore della variabile w_CODICE calcolato pagina 6, reso generico anche per calcolo codice articolo
        this.w_CACODFAS = this.w_CODICE
      endif
      do case
        case this.pTIPOREC$"O-C"
          * --- Try
          local bErr_0447C998
          bErr_0447C998=bTrsErr
          this.Try_0447C998()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            do case
              case this.pTIPOREC="O"
                this.w_AGDESSUP = .f.
                this.w_AGALLSUP = .t.
                this.w_First = .F.
            endcase
            * --- Controllo aggiornamento descrizione supplementare codice di ricerca
            if this.w_ASKAGDES AND Not this.w_AGALLSUP
              this.w_AGDESSUP = ah_YesNo("Desideri che venga aggiornata la descrizione supplementare del codice di fase %1?" , ,alltrim(this.w_CACODICE))
              if this.w_First
                this.w_AGALLSUP = ah_YesNo("Applico la scelta effettuata per tutte le fasi?")
                this.w_First = .F.
              endif
            endif
            if this.w_AGDESSUP
              * --- Write into KEY_ARTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
                    +i_ccchkf ;
                +" where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
                       )
              else
                update (i_cTable) set;
                    CADESSUP = this.w_ARDESSUP;
                    &i_ccchkf. ;
                 where;
                    CACODICE = this.w_CACODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_0447C998
          * --- End
          * --- 4) Inserisce dati su PAR_RIOR (in particolare LLC)
          * --- Write into PAR_RIOR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
                +i_ccchkf ;
            +" where ";
                +"PRCODART = "+cp_ToStrODBC(this.w_ARCODART);
                   )
          else
            update (i_cTable) set;
                PRLOWLEV = -1;
                &i_ccchkf. ;
             where;
                PRCODART = this.w_ARCODART;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_rows=0
            * --- Try
            local bErr_0447E7C8
            bErr_0447E7C8=bTrsErr
            this.Try_0447E7C8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_0447E7C8
            * --- End
          endif
        case this.pTIPOREC="T"
          * --- Sono in fase di controllo checkform dell'ordine
          *     Non serve creare i codici, verranno creati al salvataggio se � tutto OK
          * --- Try
          local bErr_044AC0F8
          bErr_044AC0F8=bTrsErr
          this.Try_044AC0F8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_044AC0F8
          * --- End
      endcase
      * --- Aggiorna anche riga ciclo
      do case
        case this.pTIPOREC="O"
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCODFAS ="+cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'ODL_CICL','CLCODFAS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_CICL.CLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_CICL.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CLCODFAS = this.w_CACODICE;
                &i_ccchkf. ;
             where;
                CLCODODL = _Curs_ODL_CICL.CLCODODL;
                and CPROWNUM = _Curs_ODL_CICL.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.pTIPOREC="T"
          * --- Write into TMPODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCODFAS ="+cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'TMPODL_CICL','CLCODFAS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(_Curs_TMPODL_CICL.CLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_TMPODL_CICL.CPROWNUM);
                +" and CLKEYRIF = "+cp_ToStrODBC(_Curs_TMPODL_CICL.CLKEYRIF);
                   )
          else
            update (i_cTable) set;
                CLCODFAS = this.w_CACODICE;
                &i_ccchkf. ;
             where;
                CLCODODL = _Curs_TMPODL_CICL.CLCODODL;
                and CPROWNUM = _Curs_TMPODL_CICL.CPROWNUM;
                and CLKEYRIF = _Curs_TMPODL_CICL.CLKEYRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        otherwise
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLARTFAS ="+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'CIC_DETT','CLARTFAS');
            +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(this.w_CACODFAS),'CIC_DETT','CLCODFAS');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_CIC_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CLARTFAS = this.w_ARCODART;
                ,CLCODFAS = this.w_CACODFAS;
                &i_ccchkf. ;
             where;
                CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
                and CPROWNUM = _Curs_CIC_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
      * --- Messaggio
      this.w_LNumOk = this.w_LNumOk +1
      this.w_TmpC = "Generazione codice (%1) in corso..."
      ah_msg(this.w_TmpC,.t.,.f.,.f.,alltrim(this.w_CACODICE))
    endif
    do case
      case this.w_TRANS
        * --- commit
        cp_EndTrs(.t.)
    endcase
    return
  proc Try_044A07E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nei codici WIP/Fase non si devono gestire i lotti
    this.w_ARFLLOTT = "N"
    * --- Insert into ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ARCODART"+",ARDESART"+",ARUNMIS1"+",AROPERAT"+",ARMOLTIP"+",ARUNMIS2"+",ARGRUMER"+",ARCODFAM"+",ARCATOMO"+",ARCATCON"+",ARCODIVA"+",ARFLINVE"+",ARTIPART"+",ARTIPPKR"+",ARFLLOTT"+",ARCODRIC"+",ARFLDISP"+",ARMAGPRE"+",ARTIPGES"+",ARPROPRE"+",ARDTINVA"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",ARFLSTCO"+",ARSTASUP"+",ARCODCEN"+",ARPREZUM"+",ARFSRIFE"+",ARTIPPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_ICOL','ARCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS1),'ART_ICOL','ARUNMIS1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AROPERAT),'ART_ICOL','AROPERAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARMOLTIP),'ART_ICOL','ARMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARUNMIS2),'ART_ICOL','ARUNMIS2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARGRUMER),'ART_ICOL','ARGRUMER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODFAM),'ART_ICOL','ARCODFAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATOMO),'ART_ICOL','ARCATOMO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCATCON),'ART_ICOL','ARCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODIVA),'ART_ICOL','ARCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLINVE),'ART_ICOL','ARFLINVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPPKR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLLOTT),'ART_ICOL','ARFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODRIC),'ART_ICOL','ARCODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLDISP),'ART_ICOL','ARFLDISP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARMAGPRE),'ART_ICOL','ARMAGPRE');
      +","+cp_NullLink(cp_ToStrODBC("F"),'ART_ICOL','ARTIPGES');
      +","+cp_NullLink(cp_ToStrODBC("I"),'ART_ICOL','ARPROPRE');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ART_ICOL','ARDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ART_ICOL','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_ICOL','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ART_ICOL','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'ART_ICOL','UTDV');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ART_ICOL','ARFLSTCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARSTASUP),'ART_ICOL','ARSTASUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODCEN),'ART_ICOL','ARCODCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARPREZUM),'ART_ICOL','ARPREZUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTM),'ART_ICOL','ARFSRIFE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'ART_ICOL','ARTIPPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ARCODART',this.w_ARCODART,'ARDESART',this.w_ARDESART,'ARUNMIS1',this.w_ARUNMIS1,'AROPERAT',this.w_AROPERAT,'ARMOLTIP',this.w_ARMOLTIP,'ARUNMIS2',this.w_ARUNMIS2,'ARGRUMER',this.w_ARGRUMER,'ARCODFAM',this.w_ARCODFAM,'ARCATOMO',this.w_ARCATOMO,'ARCATCON',this.w_ARCATCON,'ARCODIVA',this.w_ARCODIVA,'ARFLINVE',this.w_ARFLINVE)
      insert into (i_cTable) (ARCODART,ARDESART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARGRUMER,ARCODFAM,ARCATOMO,ARCATCON,ARCODIVA,ARFLINVE,ARTIPART,ARTIPPKR,ARFLLOTT,ARCODRIC,ARFLDISP,ARMAGPRE,ARTIPGES,ARPROPRE,ARDTINVA,UTCC,UTCV,UTDC,UTDV,ARFLSTCO,ARSTASUP,ARCODCEN,ARPREZUM,ARFSRIFE,ARTIPPRE &i_ccchkf. );
         values (;
           this.w_ARCODART;
           ,this.w_ARDESART;
           ,this.w_ARUNMIS1;
           ,this.w_AROPERAT;
           ,this.w_ARMOLTIP;
           ,this.w_ARUNMIS2;
           ,this.w_ARGRUMER;
           ,this.w_ARCODFAM;
           ,this.w_ARCATOMO;
           ,this.w_ARCATCON;
           ,this.w_ARCODIVA;
           ,this.w_ARFLINVE;
           ,this.w_ARTIPART;
           ,this.w_ARTIPART;
           ,this.w_ARFLLOTT;
           ,this.w_ARCODRIC;
           ,this.w_ARFLDISP;
           ,this.w_ARMAGPRE;
           ,"F";
           ,"I";
           ,i_DATSYS;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,"N";
           ,this.w_ARSTASUP;
           ,this.w_ARCODCEN;
           ,this.w_ARPREZUM;
           ,this.w_CODARTM;
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'ART_ICOL','ARDESSUP');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
             )
    else
      update (i_cTable) set;
          ARDESSUP = this.w_ARDESSUP;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_ARCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0447C998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",CADTINVA"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CACODFAS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC("R"),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC("R"),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC("0"),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC("N"),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'KEY_ARTI','CADTINVA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACODFAS),'KEY_ARTI','CACODFAS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_ARCODART,'CADESART',this.w_ARDESART,'CADESSUP',this.w_ARDESSUP,'CACODART',this.w_ARCODART,'CATIPCON',"R",'CA__TIPO',"R",'CATIPBAR',"0",'CAFLSTAM',"N",'CAOPERAT',"*",'CAMOLTIP',1,'CADTINVA',i_DATSYS,'UTCC',i_CODUTE)
      insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,CADTINVA,UTCC,UTCV,UTDC,UTDV,CACODFAS &i_ccchkf. );
         values (;
           this.w_ARCODART;
           ,this.w_ARDESART;
           ,this.w_ARDESSUP;
           ,this.w_ARCODART;
           ,"R";
           ,"R";
           ,"0";
           ,"N";
           ,"*";
           ,1;
           ,i_DATSYS;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_CACODFAS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0447E7C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+",PRSCOMIN"+",PRSCOMAX"+",PRDISMIN"+",PRQTAMIN"+",PRLOTRIO"+",PRPUNRIO"+",PRGIOAPP"+",PRGIOINV"+",PRCOSSTA"+",PUUTEELA"+",PRESECOS"+",PRINVCOS"+",PRCSLAVO"+",PRCSMATE"+",PRDATCOS"+",PRCULAVO"+",PRCUMATE"+",PRDATULT"+",PRCMMATE"+",PRCMLAVO"+",PRDATMED"+",PRCLLAVO"+",PRCLMATE"+",PRDATLIS"+",PRTIPCON"+",PRCODPRO"+",PRCODFOR"+",PRLISCOS"+",PRLOWLEV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'PAR_RIOR','PRCODART');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRSCOMIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRSCOMAX');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRDISMIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRQTAMIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRLOTRIO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRPUNRIO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRGIOAPP');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRGIOINV');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCOSSTA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
      +","+cp_NullLink(cp_ToStrODBC("    "),'PAR_RIOR','PRESECOS');
      +","+cp_NullLink(cp_ToStrODBC("      "),'PAR_RIOR','PRINVCOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCSLAVO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCSMATE');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PAR_RIOR','PRDATCOS');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCULAVO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCUMATE');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PAR_RIOR','PRDATULT');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCMMATE');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCMLAVO');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PAR_RIOR','PRDATMED');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCLLAVO');
      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_RIOR','PRCLMATE');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PAR_RIOR','PRDATLIS');
      +","+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(REPL(" ",15)),'PAR_RIOR','PRCODPRO');
      +","+cp_NullLink(cp_ToStrODBC(REPL(" ",15)),'PAR_RIOR','PRCODFOR');
      +","+cp_NullLink(cp_ToStrODBC("     "),'PAR_RIOR','PRLISCOS');
      +","+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARCODART,'PRSCOMIN',0,'PRSCOMAX',0,'PRDISMIN',0,'PRQTAMIN',0,'PRLOTRIO',0,'PRPUNRIO',0,'PRGIOAPP',0,'PRGIOINV',0,'PRCOSSTA',0,'PUUTEELA',i_CODUTE,'PRESECOS',"    ")
      insert into (i_cTable) (PRCODART,PRSCOMIN,PRSCOMAX,PRDISMIN,PRQTAMIN,PRLOTRIO,PRPUNRIO,PRGIOAPP,PRGIOINV,PRCOSSTA,PUUTEELA,PRESECOS,PRINVCOS,PRCSLAVO,PRCSMATE,PRDATCOS,PRCULAVO,PRCUMATE,PRDATULT,PRCMMATE,PRCMLAVO,PRDATMED,PRCLLAVO,PRCLMATE,PRDATLIS,PRTIPCON,PRCODPRO,PRCODFOR,PRLISCOS,PRLOWLEV &i_ccchkf. );
         values (;
           this.w_ARCODART;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,i_CODUTE;
           ,"    ";
           ,"      ";
           ,0;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,0;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,0;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,0;
           ,0;
           ,cp_CharToDate("  -  -  ");
           ,"F";
           ,REPL(" ",15);
           ,REPL(" ",15);
           ,"     ";
           ,-1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044AC0F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.pKEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.pKEYRIF,'CACODRIC',this.w_CACODICE,'CACODDIS',SPACE(20),'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.pKEYRIF;
           ,this.w_CACODICE;
           ,SPACE(20);
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    this.w_LEERR = " "
    if empty(this.w_LSerial)
      * --- Determina progressivo log
      do case
        case this.w_TRANS
          * --- begin transaction
          cp_BeginTrs()
      endcase
      * --- Try
      local bErr_04500A00
      bErr_04500A00=bTrsErr
      this.Try_04500A00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        do case
          case this.w_TRANS
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
        endcase
        ah_ErrorMsg("Impossibile aggiornare log-errori",16)
      endif
      bTrsErr=bTrsErr or bErr_04500A00
      * --- End
    endif
    * --- Scrive LOG
    if not empty(this.w_LSerial)
      * --- Scrive LOG
      this.w_LOra = time()
      this.w_LMessage = "Message()= "+message()
      * --- Try
      local bErr_044FC740
      bErr_044FC740=bTrsErr
      this.Try_044FC740()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_044FC740
      * --- End
    endif
  endproc
  proc Try_04500A00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_LSerial = space(10)
    this.w_LSerial = cp_GetProg("PRD_ERRO","PRERR",this.w_LSerial,i_CODAZI)
    do case
      case this.w_TRANS
        * --- commit
        cp_EndTrs(.t.)
    endcase
    return
  proc Try_044FC740()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRD_ERRO
    i_nConn=i_TableProp[this.PRD_ERRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRD_ERRO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRD_ERRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LESERIAL"+",LEROWNUM"+",LEOPERAZ"+",LEDATOPE"+",LEORAOPE"+",LECODUTE"+",LECODICE"+",LEERRORE"+",LEMESSAG"+",LESTORIC"+",LESTAMPA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSerial),'PRD_ERRO','LESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LNumErr),'PRD_ERRO','LEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOperaz),'PRD_ERRO','LEOPERAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PRD_ERRO','LEDATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOra),'PRD_ERRO','LEORAOPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PRD_ERRO','LECODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOggErr),'PRD_ERRO','LECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LErrore),'PRD_ERRO','LEERRORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMessage),'PRD_ERRO','LEMESSAG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PRD_ERRO','LESTORIC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PRD_ERRO','LESTAMPA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LESERIAL',this.w_LSerial,'LEROWNUM',this.w_LNumErr,'LEOPERAZ',this.w_LOperaz,'LEDATOPE',i_DATSYS,'LEORAOPE',this.w_LOra,'LECODUTE',i_CODUTE,'LECODICE',this.w_LOggErr,'LEERRORE',this.w_LErrore,'LEMESSAG',this.w_LMessage,'LESTORIC',"S",'LESTAMPA',"N")
      insert into (i_cTable) (LESERIAL,LEROWNUM,LEOPERAZ,LEDATOPE,LEORAOPE,LECODUTE,LECODICE,LEERRORE,LEMESSAG,LESTORIC,LESTAMPA &i_ccchkf. );
         values (;
           this.w_LSerial;
           ,this.w_LNumErr;
           ,this.w_LOperaz;
           ,i_DATSYS;
           ,this.w_LOra;
           ,i_CODUTE;
           ,this.w_LOggErr;
           ,this.w_LErrore;
           ,this.w_LMessage;
           ,"S";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sistema output e count point
    ah_msg("Verifica fasi count point in corso...")
    * --- Ciclo in esame
    this.w_CLSERIAL = _Curs_CIC_MAST.CLSERIAL
    this.w_PRECLAOUT = .F.
    * --- Imposta flag output di fase
    * --- Write into CIC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CIC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLFASCLA ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLFASCLA');
          +i_ccchkf ;
      +" where ";
          +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
             )
    else
      update (i_cTable) set;
          CLFASCLA = "N";
          &i_ccchkf. ;
       where;
          CLSERIAL = this.w_CLSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from GSCI5BCL
    do vq_exec with 'GSCI5BCL',this,'_Curs_GSCI5BCL','',.f.,.t.
    if used('_Curs_GSCI5BCL')
      select _Curs_GSCI5BCL
      locate for 1=1
      do while not(eof())
      this.w_CPROWORD = _Curs_GSCI5BCL.CPROWORD
      this.w_CLLTFASE = _Curs_GSCI5BCL.RLTEMCOD
      this.w_CLMAGWIP = _Curs_GSCI5BCL.RLMAGWIP
      * --- Se fase su centro di lavoro esterno, alza flag di gestione output di fase
      if _Curs_GSCI5BCL.RLINTEST = "E"
        * --- Write into CIC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CIC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
          +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCLA');
          +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCOS');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                 )
        else
          update (i_cTable) set;
              CLFASOUT = "S";
              ,CLCOUPOI = "S";
              ,CLFASCLA = "S";
              ,CLFASCOS = "S";
              &i_ccchkf. ;
           where;
              CLSERIAL = this.w_CLSERIAL;
              and CPROWORD = this.w_CPROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PRECLAOUT = .T.
      else
        if this.w_PRECLAOUT and this.w_FASPRECLA <> _Curs_GSCI5BCL.CPROWORD
          * --- Sulla fase precedente era presente un Centro di lavoro esterno
          this.w_FASCLA = _Curs_GSCI5BCL.CPROWORD
          this.w_PRECLAOUT = .F.
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
            +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASCOS');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_FASCLA);
                   )
          else
            update (i_cTable) set;
                CLFASOUT = "S";
                ,CLCOUPOI = "S";
                ,CLFASCOS = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;
                and CPROWORD = this.w_FASCLA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      this.w_FASPRECLA = _Curs_GSCI5BCL.CPROWORD
      * --- Write into CIC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CIC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLLTFASE ="+cp_NullLink(cp_ToStrODBC(this.w_CLLTFASE),'CIC_DETT','CLLTFASE');
        +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_CLMAGWIP),'CIC_DETT','CLMAGWIP');
            +i_ccchkf ;
        +" where ";
            +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
            +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
               )
      else
        update (i_cTable) set;
            CLLTFASE = this.w_CLLTFASE;
            ,CLMAGWIP = this.w_CLMAGWIP;
            &i_ccchkf. ;
         where;
            CLSERIAL = this.w_CLSERIAL;
            and CPROWORD = this.w_CPROWORD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSCI5BCL
        continue
      enddo
      use
    endif
    * --- Imposta flag output di fase
    this.w_PRIMOGIRO = .T.
    * --- Select from CIC_DETT
    i_nConn=i_TableProp[this.CIC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CIC_DETT ";
          +" where CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)+"";
          +" order by CPROWORD";
           ,"_Curs_CIC_DETT")
    else
      select * from (i_cTable);
       where CLSERIAL=this.w_CLSERIAL;
       order by CPROWORD;
        into cursor _Curs_CIC_DETT
    endif
    if used('_Curs_CIC_DETT')
      select _Curs_CIC_DETT
      locate for 1=1
      do while not(eof())
      this.w_CPROWORD = _Curs_CIC_DETT.CPROWORD
      this.w_FLCP = iif(_Curs_CIC_DETT.CLCOUPOI="S","="," ")
      this.w_FLOU = iif(_Curs_CIC_DETT.CLFASOUT="S","="," ")
      this.w_FLCF = iif(_Curs_CIC_DETT.CLFASCOS="S","="," ")
      if this.w_FLOU="=" and this.w_PRIMOGIRO
        * --- Write into CIC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CIC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_CIC_DETT.CLCOUPOI',_Curs_CIC_DETT.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_CIC_DETT.CLFASOUT',_Curs_CIC_DETT.CLFASOUT,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_CIC_DETT.CLFASCOS',_Curs_CIC_DETT.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'CIC_DETT','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'CIC_DETT','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(66)),'CIC_DETT','CLCODFAS');
          +",CLARTFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'CIC_DETT','CLARTFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp5,'CIC_DETT','CLFASCOS');
          +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLPRIFAS');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = SPACE(66);
              ,CLARTFAS = SPACE(20);
              ,CLFASCOS = &i_cOp5.;
              ,CLPRIFAS = "S";
              &i_ccchkf. ;
           where;
              CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
              and CPROWORD = this.w_CPROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PRIMOGIRO = .F.
      else
        * --- Write into CIC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CIC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_CIC_DETT.CLCOUPOI',_Curs_CIC_DETT.CLCOUPOI,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_CIC_DETT.CLFASOUT',_Curs_CIC_DETT.CLFASOUT,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_CIC_DETT.CLFASCOS',_Curs_CIC_DETT.CLFASCOS,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(i_cOp1,'CIC_DETT','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(i_cOp2,'CIC_DETT','CLFASOUT');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(66)),'CIC_DETT','CLCODFAS');
          +",CLARTFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'CIC_DETT','CLARTFAS');
          +",CLFASCOS ="+cp_NullLink(i_cOp5,'CIC_DETT','CLFASCOS');
          +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLPRIFAS');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(_Curs_CIC_DETT.CLSERIAL);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = &i_cOp1.;
              ,CLFASOUT = &i_cOp2.;
              ,CLCODFAS = SPACE(66);
              ,CLARTFAS = SPACE(20);
              ,CLFASCOS = &i_cOp5.;
              ,CLPRIFAS = "N";
              &i_ccchkf. ;
           where;
              CLSERIAL = _Curs_CIC_DETT.CLSERIAL;
              and CPROWORD = this.w_CPROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_CIC_DETT
        continue
      enddo
      use
    endif
    * --- Setta ultima fase e fase CP di riferimento
    * --- Cicla su cursore
    this.w_CLBFRIFE = 0
    this.w_CLOURIFE = 0
    this.w_PRIMOGIRO = .T.
    this.w_CPROWORD = 0
    * --- Select from GSCI4BCL
    do vq_exec with 'GSCI4BCL',this,'_Curs_GSCI4BCL','',.f.,.t.
    if used('_Curs_GSCI4BCL')
      select _Curs_GSCI4BCL
      locate for 1=1
      do while not(eof())
      * --- Coordinate fase
      if this.w_CPROWORD <> nvl (_Curs_GSCI4BCL.CPROWORD , 0)
        this.w_CPROWORD = nvl (_Curs_GSCI4BCL.CPROWORD , 0)
        this.w_CLFASOUT = nvl (_Curs_GSCI4BCL.CLFASOUT , "N")
        this.w_CLCOUPOI = nvl (_Curs_GSCI4BCL.CLCOUPOI , "N")
        * --- Se primo giro, siamo sull'ultima fase
        if this.w_PRIMOGIRO
          this.w_PRIMOGIRO = .F.
          this.w_CLBFRIFE = this.w_CPROWORD
          this.w_CLOURIFE = this.w_CPROWORD
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLULTFAS');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'CIC_DETT','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'CIC_DETT','CLBFRIFE');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'CIC_DETT','CLFASOUT');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "S";
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                ,CLCOUPOI = "S";
                ,CLFASOUT = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_CLOURIFE = iif( this.w_CLFASOUT="S", this.w_CPROWORD, this.w_CLOURIFE)
          this.w_CLBFRIFE = iif( this.w_CLCOUPOI="S", this.w_CPROWORD, this.w_CLBFRIFE)
          * --- Write into CIC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CIC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'CIC_DETT','CLULTFAS');
            +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'CIC_DETT','CLCPRIFE');
            +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'CIC_DETT','CLBFRIFE');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
                   )
          else
            update (i_cTable) set;
                CLULTFAS = "N";
                ,CLCPRIFE = this.w_CLOURIFE;
                ,CLBFRIFE = this.w_CLBFRIFE;
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;
                and CPROWORD = this.w_CPROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_GSCI4BCL
        continue
      enddo
      use
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from gsciobgc
    do vq_exec with 'gsciobgc',this,'_Curs_gsciobgc','',.f.,.t.
    if used('_Curs_gsciobgc')
      select _Curs_gsciobgc
      locate for 1=1
      do while not(eof())
      this.w_CLCODODL = _Curs_GSCIOBGC.CLCODODL
      this.w_CLSERIAL = _Curs_GSCIOBGC.CLSERIAL
      if this.w_GESTLOG
        this.w_oWIPLOG.ADDMSGLOG("Elaborazione ordine N. [%1]", ALLTRIM(this.w_CLCODODL))     
      endif
      ah_msg("Verifica fasi di conto/lavoro in corso...")
      * --- Per le fasi non attive sbianco flag count point e output
      if .f.
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASOUT');
          +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLULTFAS');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLCODFAS');
          +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLCPRIFE');
          +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLBFRIFE');
          +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPFPR');
          +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPOUT');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLFASSEL = "+cp_ToStrODBC("S");
                 )
        else
          update (i_cTable) set;
              CLCOUPOI = "N";
              ,CLFASOUT = "N";
              ,CLULTFAS = "N";
              ,CLCODFAS = SPACE(20);
              ,CLCPRIFE = 0;
              ,CLBFRIFE = 0;
              ,CLWIPFPR = SPACE(20);
              ,CLWIPOUT = SPACE(20);
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLFASSEL = "S";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLCPRIFE');
          +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_CICL','CLBFRIFE');
          +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASCLA');
          +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLCODFAS');
          +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPFPR');
          +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ODL_CICL','CLWIPOUT');
          +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLCOUPOI');
          +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASOUT');
          +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLULTFAS');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                 )
        else
          update (i_cTable) set;
              CLCPRIFE = 0;
              ,CLBFRIFE = 0;
              ,CLFASCLA = "N";
              ,CLCODFAS = SPACE(20);
              ,CLWIPFPR = SPACE(20);
              ,CLWIPOUT = SPACE(20);
              ,CLCOUPOI = "N";
              ,CLFASOUT = "N";
              ,CLULTFAS = "N";
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Resetta flag fase di conto lavoro
      * --- Write into ODL_CICL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CLFASCLA ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLFASCLA');
            +i_ccchkf ;
        +" where ";
            +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
               )
      else
        update (i_cTable) set;
            CLFASCLA = "N";
            &i_ccchkf. ;
         where;
            CLCODODL = this.w_CLCODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Riassegna flag fase di conto lavoro
      this.w_CLROWORD = 0
      this.w_PRECLAOUT = .F.
      this.w_FASCLA = 0
      this.w_CLINDPRE = "00"
      * --- Select from GSCI5BTC
      do vq_exec with 'GSCI5BTC',this,'_Curs_GSCI5BTC','',.f.,.t.
      if used('_Curs_GSCI5BTC')
        select _Curs_GSCI5BTC
        locate for 1=1
        do while not(eof())
        this.w_CLROWORD = _Curs_GSCI5BTC.CPROWORD
        this.w_CLINDPRE = _Curs_GSCI5BTC.CLINDPRE
        this.w_CLLTFASE = _Curs_GSCI5BTC.CLLTFASE
        * --- Se fase su centro di lavoro esterno, alza flag di gestione output di fase
        if _Curs_GSCI5BTC.RLINTEST = "E"
          * --- Sulla fase � presente un Centro di lavoro esterno
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASOUT');
            +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
            +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCLA');
            +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCOS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                +" and CLINDPRE = "+cp_ToStrODBC(this.w_CLINDPRE);
                   )
          else
            update (i_cTable) set;
                CLFASOUT = "S";
                ,CLCOUPOI = "S";
                ,CLFASCLA = "S";
                ,CLFASCOS = "S";
                &i_ccchkf. ;
             where;
                CLCODODL = this.w_CLCODODL;
                and CLROWORD = this.w_CLROWORD;
                and CLINDPRE = this.w_CLINDPRE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PRECLAOUT = .T.
        else
          if this.w_PRECLAOUT and this.w_FASPRECLA <> _Curs_GSCI5BTC.CPROWORD
            * --- Sulla fase precedente era presente un Centro di lavoro esterno
            this.w_FASCLA = _Curs_GSCI5BTC.CPROWORD
            this.w_PRECLAOUT = .F.
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASOUT');
              +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
              +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASCOS');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                  +" and CLROWORD = "+cp_ToStrODBC(this.w_FASCLA);
                  +" and CLINDPRE = "+cp_ToStrODBC(this.w_CLINDPRE);
                     )
            else
              update (i_cTable) set;
                  CLFASOUT = "S";
                  ,CLCOUPOI = "S";
                  ,CLFASCOS = "S";
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.w_CLCODODL;
                  and CLROWORD = this.w_FASCLA;
                  and CLINDPRE = this.w_CLINDPRE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        this.w_FASPRECLA = _Curs_GSCI5BTC.CPROWORD
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLLTFASE ="+cp_NullLink(cp_ToStrODBC(this.w_CLLTFASE),'ODL_CICL','CLLTFASE');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
              +" and CLINDPRE = "+cp_ToStrODBC(this.w_CLINDPRE);
                 )
        else
          update (i_cTable) set;
              CLLTFASE = this.w_CLLTFASE;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_CLROWORD;
              and CLINDPRE = this.w_CLINDPRE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_GSCI5BTC
          continue
        enddo
        use
      endif
      * --- Imposta flag output di fase
      ah_msg("Verifica fasi di output in corso...")
      this.w_FLCP = SPACE(1)
      this.w_FLOU = SPACE(1)
      this.w_FLCF = SPACE(1)
      this.w_PRIMOGIRO = .T.
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLFASSEL='S'";
            +" order by CLROWORD";
             ,"_Curs_ODL_CICL")
      else
        select * from (i_cTable);
         where CLCODODL=this.w_CLCODODL AND CLFASSEL="S";
         order by CLROWORD;
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_CLROWORD = _Curs_ODL_CICL.CLROWORD
        this.w_FLCP = iif(_Curs_ODL_CICL.CLCOUPOI="S","="," ")
        this.w_FLOU = iif(_Curs_ODL_CICL.CLFASOUT="S","="," ")
        this.w_FLCF = iif(_Curs_ODL_CICL.CLFASCOS="S","="," ")
        if this.w_FLOU="=" and this.w_PRIMOGIRO
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_ODL_CICL.CLCOUPOI',_Curs_ODL_CICL.CLCOUPOI,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_ODL_CICL.CLFASOUT',_Curs_ODL_CICL.CLFASOUT,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_ODL_CICL.CLFASCOS',_Curs_ODL_CICL.CLFASCOS,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCOUPOI ="+cp_NullLink(i_cOp1,'ODL_CICL','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(i_cOp2,'ODL_CICL','CLFASOUT');
            +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(space(20)),'ODL_CICL','CLCODFAS');
            +",CLFASCOS ="+cp_NullLink(i_cOp4,'ODL_CICL','CLFASCOS');
            +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLPRIFAS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_CICL.CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                   )
          else
            update (i_cTable) set;
                CLCOUPOI = &i_cOp1.;
                ,CLFASOUT = &i_cOp2.;
                ,CLCODFAS = space(20);
                ,CLFASCOS = &i_cOp4.;
                ,CLPRIFAS = "S";
                &i_ccchkf. ;
             where;
                CLCODODL = _Curs_ODL_CICL.CLCODODL;
                and CLROWORD = this.w_CLROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_PRIMOGIRO = .F.
        else
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLCP,'CLCOUPOI','_Curs_ODL_CICL.CLCOUPOI',_Curs_ODL_CICL.CLCOUPOI,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLOU,'CLFASOUT','_Curs_ODL_CICL.CLFASOUT',_Curs_ODL_CICL.CLFASOUT,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_FLCF,'CLFASCOS','_Curs_ODL_CICL.CLFASCOS',_Curs_ODL_CICL.CLFASCOS,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLCOUPOI ="+cp_NullLink(i_cOp1,'ODL_CICL','CLCOUPOI');
            +",CLFASOUT ="+cp_NullLink(i_cOp2,'ODL_CICL','CLFASOUT');
            +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(space(20)),'ODL_CICL','CLCODFAS');
            +",CLFASCOS ="+cp_NullLink(i_cOp4,'ODL_CICL','CLFASCOS');
            +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLPRIFAS');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(_Curs_ODL_CICL.CLCODODL);
                +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                   )
          else
            update (i_cTable) set;
                CLCOUPOI = &i_cOp1.;
                ,CLFASOUT = &i_cOp2.;
                ,CLCODFAS = space(20);
                ,CLFASCOS = &i_cOp4.;
                ,CLPRIFAS = "N";
                &i_ccchkf. ;
             where;
                CLCODODL = _Curs_ODL_CICL.CLCODODL;
                and CLROWORD = this.w_CLROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
      * --- Setta ultima fase, fase OUTPUT di riferimento e fase BF(CP) di riferimento
      ah_msg("Verifica fasi count point in corso...")
      * --- Cicla su cursore
      this.w_CLBFRIFE = 0
      this.w_CLOURIFE = 0
      this.w_PRIMOGIRO = .T.
      this.w_CLROWORD = 0
      * --- Select from GSCI4BTC
      do vq_exec with 'GSCI4BTC',this,'_Curs_GSCI4BTC','',.f.,.t.
      if used('_Curs_GSCI4BTC')
        select _Curs_GSCI4BTC
        locate for 1=1
        do while not(eof())
        * --- Coordinate fase
        if this.w_CLROWORD <> nvl (_Curs_GSCI4BTC.CPROWORD , 0)
          this.w_CLROWORD = nvl (_Curs_GSCI4BTC.CPROWORD , 0)
          this.w_CLFASOUT = nvl (_Curs_GSCI4BTC.CLFASOUT , "N")
          this.w_CLCOUPOI = nvl (_Curs_GSCI4BTC.CLCOUPOI , "N")
          * --- Se primo giro, siamo sull'ultima fase
          if this.w_PRIMOGIRO
            this.w_PRIMOGIRO = .F.
            this.w_CLBFRIFE = this.w_CLROWORD
            this.w_CLOURIFE = this.w_CLROWORD
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLULTFAS');
              +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'ODL_CICL','CLCPRIFE');
              +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'ODL_CICL','CLBFRIFE');
              +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLCOUPOI');
              +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_CICL','CLFASOUT');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                  +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                     )
            else
              update (i_cTable) set;
                  CLULTFAS = "S";
                  ,CLCPRIFE = this.w_CLOURIFE;
                  ,CLBFRIFE = this.w_CLBFRIFE;
                  ,CLCOUPOI = "S";
                  ,CLFASOUT = "S";
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.w_CLCODODL;
                  and CLROWORD = this.w_CLROWORD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            this.w_CLOURIFE = iif( this.w_CLFASOUT="S", this.w_CLROWORD, this.w_CLOURIFE)
            this.w_CLBFRIFE = iif( this.w_CLCOUPOI="S", this.w_CLROWORD, this.w_CLBFRIFE)
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLULTFAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_CICL','CLULTFAS');
              +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLOURIFE),'ODL_CICL','CLCPRIFE');
              +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CLBFRIFE),'ODL_CICL','CLBFRIFE');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
                  +" and CLROWORD = "+cp_ToStrODBC(this.w_CLROWORD);
                     )
            else
              update (i_cTable) set;
                  CLULTFAS = "N";
                  ,CLCPRIFE = this.w_CLOURIFE;
                  ,CLBFRIFE = this.w_CLBFRIFE;
                  &i_ccchkf. ;
               where;
                  CLCODODL = this.w_CLCODODL;
                  and CLROWORD = this.w_CLROWORD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
          select _Curs_GSCI4BTC
          continue
        enddo
        use
      endif
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" and CLFASSEL = 'S'";
             ,"_Curs_ODL_CICL")
      else
        select * from (i_cTable);
         where CLCODODL=this.w_CLCODODL and CLFASSEL = "S";
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_CLCODFAS = SPACE(66)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
      this.w_WIPFASEPREC = SPACE(20)
      this.w_WIPFASEOLD = SPACE(20)
      * --- Select from ODL_CICL
      i_nConn=i_TableProp[this.ODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_CICL ";
            +" where CLCODODL="+cp_ToStrODBC(this.w_CLCODODL)+" AND CLFASSEL='S'";
            +" order by CLROWORD";
             ,"_Curs_ODL_CICL")
      else
        select * from (i_cTable);
         where CLCODODL=this.w_CLCODODL AND CLFASSEL="S";
         order by CLROWORD;
          into cursor _Curs_ODL_CICL
      endif
      if used('_Curs_ODL_CICL')
        select _Curs_ODL_CICL
        locate for 1=1
        do while not(eof())
        this.w_ROWNUM = _Curs_ODL_CICL.CPROWNUM
        this.w_ROWORD = _Curs_ODL_CICL.CLROWORD
        this.w_CLCODFAS = nvl( _Curs_ODL_CICL.CLCODFAS , SPACE(20))
        if _Curs_ODL_CICL.CLFASOUT="S"
          * --- Articolo-WIP Output Fase
          this.w_CLWIPOUT = this.w_CLCODFAS
          * --- Articolo-WIP Input Fase
          this.w_CLWIPFPR = nvl(this.w_WIPFASEPREC, SPACE(20))
          this.w_WIPFASEPREC = this.w_CLCODFAS
        else
          this.w_CLWIPOUT = SPACE(20)
          this.w_CLWIPFPR = SPACE(20)
        endif
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(this.w_CLWIPFPR),'ODL_CICL','CLWIPFPR');
          +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(this.w_CLWIPOUT),'ODL_CICL','CLWIPOUT');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(this.w_CLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                 )
        else
          update (i_cTable) set;
              CLWIPFPR = this.w_CLWIPFPR;
              ,CLWIPOUT = this.w_CLWIPOUT;
              &i_ccchkf. ;
           where;
              CLCODODL = this.w_CLCODODL;
              and CLROWORD = this.w_ROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_ODL_CICL
          continue
        enddo
        use
      endif
        select _Curs_gsciobgc
        continue
      enddo
      use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from CCF_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CCF_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCF_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CFSEPARA,CFSEPASN"+;
        " from "+i_cTable+" CCF_MAST where ";
            +"CFCODICE = "+cp_ToStrODBC(this.w_CLAFAS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CFSEPARA,CFSEPASN;
        from (i_cTable) where;
            CFCODICE = this.w_CLAFAS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SEPARA = NVL(cp_ToDate(_read_.CFSEPARA),cp_NullValue(_read_.CFSEPARA))
      this.w_CFSEPASN = NVL(cp_ToDate(_read_.CFSEPASN),cp_NullValue(_read_.CFSEPASN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODICE = ""
    if i_rows>0
      if this.w_CFSEPASN<>"S"
        this.w_SEPARA = ""
      endif
      * --- Select from CCF_DETT
      i_nConn=i_TableProp[this.CCF_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CCF_DETT ";
            +" where CFCODICE="+cp_ToStrODBC(this.w_CLAFAS)+"";
            +" order by CFPOSIZI,CPROWNUM";
             ,"_Curs_CCF_DETT")
      else
        select * from (i_cTable);
         where CFCODICE=this.w_CLAFAS;
         order by CFPOSIZI,CPROWNUM;
          into cursor _Curs_CCF_DETT
      endif
      if used('_Curs_CCF_DETT')
        select _Curs_CCF_DETT
        locate for 1=1
        do while not(eof())
        this.w_LUNGH = _Curs_CCF_DETT.CFLUNGHE
        this.w_FUNZIONE = _Curs_CCF_DETT.CF__FUNC
        this.w_PROGRE = _Curs_CCF_DETT.CFPROGRE
        this.w_START = _Curs_CCF_DETT.CF_START
        do case
          case this.w_FUNZIONE="A"
            * --- Articolo
            this.w_VALORE = left(ALLTRIM(this.w_CODARTM),this.w_LUNGH)
          case this.w_FUNZIONE="D"
            * --- Distinta base
            this.w_VALORE = left(ALLTRIM(this.w_DISTINTA),this.w_LUNGH)
          case this.w_FUNZIONE="C"
            * --- Codice ciclo
            this.w_VALORE = left(ALLTRIM(this.w_CICLO),this.w_LUNGH)
          case this.w_FUNZIONE="S"
            * --- Seriale ciclo
            this.w_VALORE = left(ALLTRIM(this.w_CLSERIAL),this.w_LUNGH)
          case this.w_FUNZIONE="F"
            * --- Fase
            this.w_VALORE = left(ALLTRIM(this.w_FASE)+space(3),this.w_LUNGH)
          case this.w_FUNZIONE="P"
            * --- Progressivo
            * --- Read from COD_ACFA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.COD_ACFA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COD_ACFA_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CAPROGRE"+;
                " from "+i_cTable+" COD_ACFA where ";
                    +"CASERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                    +" and CA__FASE = "+cp_ToStrODBC(this.w_FASE);
                    +" and CA_PARAM = "+cp_ToStrODBC(this.w_PARPAD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CAPROGRE;
                from (i_cTable) where;
                    CASERIAL = this.w_CLSERIAL;
                    and CA__FASE = this.w_FASE;
                    and CA_PARAM = this.w_PARPAD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PROGOLD = NVL(cp_ToDate(_read_.CAPROGRE),cp_NullValue(_read_.CAPROGRE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_rows=0
              * --- Aggiorno progressivo tabella
              this.w_PROGRE = this.w_PROGRE+1
              if val(this.w_START)>this.w_PROGRE
                this.w_PROGRE = val(this.w_START)
              endif
              * --- Write into CCF_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CCF_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CCF_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CCF_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CFPROGRE ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'CCF_DETT','CFPROGRE');
                    +i_ccchkf ;
                +" where ";
                    +"CFCODICE = "+cp_ToStrODBC(this.w_CLAFAS);
                    +" and CPROWNUM = "+cp_ToStrODBC(_Curs_CCF_DETT.CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    CFPROGRE = this.w_PROGRE;
                    &i_ccchkf. ;
                 where;
                    CFCODICE = this.w_CLAFAS;
                    and CPROWNUM = _Curs_CCF_DETT.CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Insert into COD_ACFA
              i_nConn=i_TableProp[this.COD_ACFA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.COD_ACFA_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_ACFA_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CASERIAL"+",CA__FASE"+",CA_PARAM"+",CAPROGRE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CLSERIAL),'COD_ACFA','CASERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FASE),'COD_ACFA','CA__FASE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PARPAD),'COD_ACFA','CA_PARAM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PROGRE),'COD_ACFA','CAPROGRE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CASERIAL',this.w_CLSERIAL,'CA__FASE',this.w_FASE,'CA_PARAM',this.w_PARPAD,'CAPROGRE',this.w_PROGRE)
                insert into (i_cTable) (CASERIAL,CA__FASE,CA_PARAM,CAPROGRE &i_ccchkf. );
                   values (;
                     this.w_CLSERIAL;
                     ,this.w_FASE;
                     ,this.w_PARPAD;
                     ,this.w_PROGRE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Mantengo il vecchio progressivo (deve essere calcolato solo la prima volta)
              this.w_PROGRE = this.w_PROGOLD
            endif
            this.w_VALORE = ALLTRIM(STR(this.w_PROGRE,18))
          case this.w_FUNZIONE="N"
            * --- Nessuno
            this.w_VALORE = left(ALLTRIM(_Curs_CCF_DETT.CF_START),this.w_LUNGH)
        endcase
        if empty(this.w_CODICE)
          this.w_CODICE = this.w_VALORE
        else
          this.w_CODICE = this.w_CODICE+this.w_SEPARA+this.w_VALORE
        endif
          select _Curs_CCF_DETT
          continue
        enddo
        use
      endif
    endif
    if Empty (this.w_CODICE)
      ah_ErrorMsg("Nessun codice generato, classe inesistente o priva del dettaglio","!","")
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,pTIPOREC,pTRANS,pSERIAL,pSERIAL2,pKEYRIF)
    this.pTIPOREC=pTIPOREC
    this.pTRANS=pTRANS
    this.pSERIAL=pSERIAL
    this.pSERIAL2=pSERIAL2
    this.pKEYRIF=pKEYRIF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='CIC_DETT'
    this.cWorkTables[2]='CIC_MAST'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='PRD_ERRO'
    this.cWorkTables[8]='RIS_MAST'
    this.cWorkTables[9]='PAR_RIOR'
    this.cWorkTables[10]='*TMPCNLAV'
    this.cWorkTables[11]='ODL_CICL'
    this.cWorkTables[12]='ODL_MAST'
    this.cWorkTables[13]='ART_TEMP'
    this.cWorkTables[14]='CCF_DETT'
    this.cWorkTables[15]='CCF_MAST'
    this.cWorkTables[16]='*TMPODL_CICL'
    this.cWorkTables[17]='COD_ACFA'
    return(this.OpenAllTables(17))

  proc CloseCursors()
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_TMPODL_CICL')
      use in _Curs_TMPODL_CICL
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_CIC_MAST')
      use in _Curs_CIC_MAST
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_TMPCNLAV')
      use in _Curs_TMPCNLAV
    endif
    if used('_Curs_TMPCNLAV')
      use in _Curs_TMPCNLAV
    endif
    if used('_Curs_TMPCNLAV')
      use in _Curs_TMPCNLAV
    endif
    if used('_Curs_GSCI5BCL')
      use in _Curs_GSCI5BCL
    endif
    if used('_Curs_CIC_DETT')
      use in _Curs_CIC_DETT
    endif
    if used('_Curs_GSCI4BCL')
      use in _Curs_GSCI4BCL
    endif
    if used('_Curs_gsciobgc')
      use in _Curs_gsciobgc
    endif
    if used('_Curs_GSCI5BTC')
      use in _Curs_GSCI5BTC
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_GSCI4BTC')
      use in _Curs_GSCI4BTC
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_ODL_CICL')
      use in _Curs_ODL_CICL
    endif
    if used('_Curs_CCF_DETT')
      use in _Curs_CCF_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOREC,pTRANS,pSERIAL,pSERIAL2,pKEYRIF"
endproc
