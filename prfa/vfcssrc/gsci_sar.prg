* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_sar                                                        *
*              Stampa aree                                                     *
*                                                                              *
*      Author: Zucchetti Tam srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-22                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_sar",oParentObject))

* --- Class definition
define class tgsci_sar as StdForm
  Top    = 37
  Left   = 62

  * --- Standard Properties
  Width  = 553
  Height = 179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-29"
  HelpContextID=124388713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  RIS_ORSE_IDX = 0
  cPrg = "gsci_sar"
  cComment = "Stampa aree"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_TIPO = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_sarPag1","gsci_sar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='RIS_ORSE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_TIPO=space(2)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_3('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(3,4,.f.)
        .w_TIPO = 'AR'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_TIPO;
                     ,'RLCODICE',trim(this.w_CODINI))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_TIPO);

            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oCODINI_1_3'),i_cWhere,'',"Aree",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPO;
                       ,'RLCODICE',this.w_CODINI)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.RLCODICE,space(20))
      this.w_DESINI = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_TIPO;
                     ,'RLCODICE',trim(this.w_CODFIN))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_TIPO);

            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_4'),i_cWhere,'',"Aree",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o minore di quello iniziale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPO;
                       ,'RLCODICE',this.w_CODFIN)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.RLCODICE,space(20))
      this.w_DESFIN = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o minore di quello iniziale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_3.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_3.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_4.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_4.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_5.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_5.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_6.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_6.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o maggiore di quello finale")
          case   not((.w_CODINI<=.w_CODFIN or empty(.w_CODINI)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o minore di quello iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    return

enddefine

* --- Define pages as container
define class tgsci_sarPag1 as StdContainer
  Width  = 549
  height = 179
  stdWidth  = 549
  stdheight = 179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_3 as StdField with uid="WWOLPDYBPZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o maggiore di quello finale",;
    ToolTipText = "Codice area di inizio selezione",;
    HelpContextID = 154956250,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=76, Top=28, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oCODINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Aree",'',this.parent.oContained
  endproc

  add object oCODFIN_1_4 as StdField with uid="HNZEUVMBZC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o minore di quello iniziale",;
    ToolTipText = "Codice area di fine selezione",;
    HelpContextID = 76509658,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=76, Top=53, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oCODFIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Aree",'',this.parent.oContained
  endproc

  add object oDESINI_1_5 as StdField with uid="CZBVRIWMZS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 154897354,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=246, Top=28, InputMask=replicate('X',40)

  add object oDESFIN_1_6 as StdField with uid="VWFYKWHCJN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76450762,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=246, Top=53, InputMask=replicate('X',40)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=99, width=422,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53608934


  add object oBtn_1_9 as StdButton with uid="AIHWYAKILI",left=433, top=129, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la stampa";
    , HelpContextID = 33442794;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="OTHNDXTWZX",left=483, top=129, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33442794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="VRSTNSRGKI",Visible=.t., Left=7, Top=28,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="KMITLJNPGD",Visible=.t., Left=16, Top=53,;
    Alignment=1, Width=55, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="YWPOEDITKP",Visible=.t., Left=4, Top=99,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_sar','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
