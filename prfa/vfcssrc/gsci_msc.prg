* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_msc                                                        *
*              Layout sistema produttivo                                       *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_msc"))

* --- Class definition
define class tgsci_msc as StdTrsForm
  Top    = 27
  Left   = 13

  * --- Standard Properties
  Width  = 673
  Height = 377+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-21"
  HelpContextID=97125737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  STR_CONF_IDX = 0
  RIS_ORSE_IDX = 0
  TIP_RISO_IDX = 0
  cFile = "STR_CONF"
  cKeySelect = "SCTIPUNP,SCUNIPRO,SCTIPPAD,SCCODPAD"
  cKeyWhere  = "SCTIPUNP=this.w_SCTIPUNP and SCUNIPRO=this.w_SCUNIPRO and SCTIPPAD=this.w_SCTIPPAD and SCCODPAD=this.w_SCCODPAD"
  cKeyDetail  = "SCTIPUNP=this.w_SCTIPUNP and SCUNIPRO=this.w_SCUNIPRO and SCTIPPAD=this.w_SCTIPPAD and SCCODPAD=this.w_SCCODPAD"
  cKeyWhereODBC = '"SCTIPUNP="+cp_ToStrODBC(this.w_SCTIPUNP)';
      +'+" and SCUNIPRO="+cp_ToStrODBC(this.w_SCUNIPRO)';
      +'+" and SCTIPPAD="+cp_ToStrODBC(this.w_SCTIPPAD)';
      +'+" and SCCODPAD="+cp_ToStrODBC(this.w_SCCODPAD)';

  cKeyDetailWhereODBC = '"SCTIPUNP="+cp_ToStrODBC(this.w_SCTIPUNP)';
      +'+" and SCUNIPRO="+cp_ToStrODBC(this.w_SCUNIPRO)';
      +'+" and SCTIPPAD="+cp_ToStrODBC(this.w_SCTIPPAD)';
      +'+" and SCCODPAD="+cp_ToStrODBC(this.w_SCCODPAD)';

  cKeyWhereODBCqualified = '"STR_CONF.SCTIPUNP="+cp_ToStrODBC(this.w_SCTIPUNP)';
      +'+" and STR_CONF.SCUNIPRO="+cp_ToStrODBC(this.w_SCUNIPRO)';
      +'+" and STR_CONF.SCTIPPAD="+cp_ToStrODBC(this.w_SCTIPPAD)';
      +'+" and STR_CONF.SCCODPAD="+cp_ToStrODBC(this.w_SCCODPAD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsci_msc"
  cComment = "Layout sistema produttivo"
  i_nRowNum = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EREWIP = space(5)
  w_ERECAL = space(5)
  w_NODOSELE = space(20)
  o_NODOSELE = space(20)
  w_NODOTIPO = space(2)
  w_TP = space(2)
  o_TP = space(2)
  w_F3PRESSED = .F.
  w_SCTIPFIG = space(2)
  w_SCCODFIG = space(20)
  w_DESFIG = space(40)
  w_RLCALRIS = space(5)
  w_RLWIPRIS = space(5)
  w_SCTIPUNP = space(2)
  w_SCUNIPRO = space(20)
  o_SCUNIPRO = space(20)
  w_SCTIPPAD = space(2)
  o_SCTIPPAD = space(2)
  w_SCCODPAD = space(20)
  o_SCCODPAD = space(20)
  w_SCDESPAD = space(40)
  w_SCDESUNP = space(40)
  w_TPUNIPRO = space(2)
  o_TPUNIPRO = space(2)
  w_TF = space(2)
  w_TPFIL = space(2)
  o_TPFIL = space(2)
  w_PADWIP = space(5)
  w_PADCAL = space(5)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STR_CONF','gsci_msc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_mscPag1","gsci_msc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Struttura")
      .Pages(1).HelpContextID = 99785384
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCTIPUNP_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='TIP_RISO'
    this.cWorkTables[3]='STR_CONF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STR_CONF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STR_CONF_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SCTIPUNP = NVL(SCTIPUNP,space(2))
      .w_SCUNIPRO = NVL(SCUNIPRO,space(20))
      .w_SCTIPPAD = NVL(SCTIPPAD,space(2))
      .w_SCCODPAD = NVL(SCCODPAD,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from STR_CONF where SCTIPUNP=KeySet.SCTIPUNP
    *                            and SCUNIPRO=KeySet.SCUNIPRO
    *                            and SCTIPPAD=KeySet.SCTIPPAD
    *                            and SCCODPAD=KeySet.SCCODPAD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2],this.bLoadRecFilter,this.STR_CONF_IDX,"gsci_msc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STR_CONF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STR_CONF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STR_CONF '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCTIPUNP',this.w_SCTIPUNP  ,'SCUNIPRO',this.w_SCUNIPRO  ,'SCTIPPAD',this.w_SCTIPPAD  ,'SCCODPAD',this.w_SCCODPAD  )
      select * from (i_cTable) STR_CONF where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_EREWIP = space(5)
        .w_ERECAL = space(5)
        .w_NODOSELE = space(20)
        .w_NODOTIPO = space(2)
        .w_F3PRESSED = .F.
        .w_SCDESPAD = space(40)
        .w_SCDESUNP = space(40)
        .w_TPUNIPRO = space(2)
        .w_TF = space(2)
        .w_PADWIP = space(5)
        .w_PADCAL = space(5)
        .w_TP = .w_SCTIPPAD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_SCTIPUNP = NVL(SCTIPUNP,space(2))
        .w_SCUNIPRO = NVL(SCUNIPRO,space(20))
          .link_1_9('Load')
        .w_SCTIPPAD = NVL(SCTIPPAD,space(2))
        .w_SCCODPAD = NVL(SCCODPAD,space(20))
          .link_1_11('Load')
        .w_TPFIL = subst("RE-AR-CL-MA", at(.w_TP,"UP-RE-AR-CL"),2)
        cp_LoadRecExtFlds(this,'STR_CONF')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESFIG = space(40)
          .w_RLCALRIS = space(5)
          .w_RLWIPRIS = space(5)
          .w_SCTIPFIG = NVL(SCTIPFIG,space(2))
          .w_SCCODFIG = NVL(SCCODFIG,space(20))
          .link_2_2('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TP = .w_SCTIPPAD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_TPFIL = subst("RE-AR-CL-MA", at(.w_TP,"UP-RE-AR-CL"),2)
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_EREWIP=space(5)
      .w_ERECAL=space(5)
      .w_NODOSELE=space(20)
      .w_NODOTIPO=space(2)
      .w_TP=space(2)
      .w_F3PRESSED=.f.
      .w_SCTIPFIG=space(2)
      .w_SCCODFIG=space(20)
      .w_DESFIG=space(40)
      .w_RLCALRIS=space(5)
      .w_RLWIPRIS=space(5)
      .w_SCTIPUNP=space(2)
      .w_SCUNIPRO=space(20)
      .w_SCTIPPAD=space(2)
      .w_SCCODPAD=space(20)
      .w_SCDESPAD=space(40)
      .w_SCDESUNP=space(40)
      .w_TPUNIPRO=space(2)
      .w_TF=space(2)
      .w_TPFIL=space(2)
      .w_PADWIP=space(5)
      .w_PADCAL=space(5)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_TP = .w_SCTIPPAD
        .w_F3PRESSED = .F.
        .w_SCTIPFIG = .w_TPFIL
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SCCODFIG))
         .link_2_2('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(9,11,.f.)
        .w_SCTIPUNP = 'UP'
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_SCUNIPRO))
         .link_1_9('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_SCCODPAD))
         .link_1_11('Full')
        endif
        .DoRTCalc(16,19,.f.)
        .w_TPFIL = subst("RE-AR-CL-MA", at(.w_TP,"UP-RE-AR-CL"),2)
      endif
    endwith
    cp_BlankRecExtFlds(this,'STR_CONF')
    this.DoRTCalc(21,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCTIPUNP_1_8.enabled = i_bVal
      .Page1.oPag.oSCUNIPRO_1_9.enabled = i_bVal
      .Page1.oPag.oSCTIPPAD_1_10.enabled = i_bVal
      .Page1.oPag.oSCCODPAD_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCTIPUNP_1_8.enabled = .f.
        .Page1.oPag.oSCUNIPRO_1_9.enabled = .f.
        .Page1.oPag.oSCTIPPAD_1_10.enabled = .f.
        .Page1.oPag.oSCCODPAD_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCTIPUNP_1_8.enabled = .t.
        .Page1.oPag.oSCUNIPRO_1_9.enabled = .t.
        .Page1.oPag.oSCTIPPAD_1_10.enabled = .t.
        .Page1.oPag.oSCCODPAD_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STR_CONF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPUNP,"SCTIPUNP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCUNIPRO,"SCUNIPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCTIPPAD,"SCTIPPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODPAD,"SCCODPAD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
    i_lTable = "STR_CONF"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STR_CONF_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SST with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_SCTIPFIG N(3);
      ,t_SCCODFIG C(20);
      ,t_DESFIG C(40);
      ,t_RLCALRIS C(5);
      ,t_RLWIPRIS C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mscbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.controlsource=this.cTrsName+'.t_SCTIPFIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODFIG_2_2.controlsource=this.cTrsName+'.t_SCCODFIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_3.controlsource=this.cTrsName+'.t_DESFIG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(178)
    this.AddVLine(345)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODFIG_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STR_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
      *
      * insert into STR_CONF
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STR_CONF')
        i_extval=cp_InsertValODBCExtFlds(this,'STR_CONF')
        i_cFldBody=" "+;
                  "(SCTIPFIG,SCCODFIG,SCTIPUNP,SCUNIPRO,SCTIPPAD"+;
                  ",SCCODPAD,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SCTIPFIG)+","+cp_ToStrODBCNull(this.w_SCCODFIG)+","+cp_ToStrODBC(this.w_SCTIPUNP)+","+cp_ToStrODBCNull(this.w_SCUNIPRO)+","+cp_ToStrODBC(this.w_SCTIPPAD)+;
             ","+cp_ToStrODBCNull(this.w_SCCODPAD)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STR_CONF')
        i_extval=cp_InsertValVFPExtFlds(this,'STR_CONF')
        cp_CheckDeletedKey(i_cTable,0,'SCTIPUNP',this.w_SCTIPUNP,'SCUNIPRO',this.w_SCUNIPRO,'SCTIPPAD',this.w_SCTIPPAD,'SCCODPAD',this.w_SCCODPAD)
        INSERT INTO (i_cTable) (;
                   SCTIPFIG;
                  ,SCCODFIG;
                  ,SCTIPUNP;
                  ,SCUNIPRO;
                  ,SCTIPPAD;
                  ,SCCODPAD;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SCTIPFIG;
                  ,this.w_SCCODFIG;
                  ,this.w_SCTIPUNP;
                  ,this.w_SCUNIPRO;
                  ,this.w_SCTIPPAD;
                  ,this.w_SCCODPAD;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STR_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_SCCODFIG<>space(20)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'STR_CONF')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'STR_CONF')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_SCCODFIG<>space(20)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STR_CONF
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'STR_CONF')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " SCTIPFIG="+cp_ToStrODBC(this.w_SCTIPFIG)+;
                     ",SCCODFIG="+cp_ToStrODBCNull(this.w_SCCODFIG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'STR_CONF')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      SCTIPFIG=this.w_SCTIPFIG;
                     ,SCCODFIG=this.w_SCCODFIG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STR_CONF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_SCCODFIG<>space(20)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete STR_CONF
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_SCCODFIG<>space(20)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_SCTIPPAD<>.w_SCTIPPAD.or. .o_SCCODPAD<>.w_SCCODPAD
          .w_TP = .w_SCTIPPAD
        endif
        .DoRTCalc(6,6,.t.)
        if .o_SCUNIPRO<>.w_SCUNIPRO.or. .o_SCCODPAD<>.w_SCCODPAD.or. .o_TPUNIPRO<>.w_TPUNIPRO.or. .o_TP<>.w_TP.or. .o_TPFIL<>.w_TPFIL
          .w_SCTIPFIG = .w_TPFIL
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(8,12,.t.)
        if .o_NODOSELE<>.w_NODOSELE
          .link_1_9('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_NODOSELE<>.w_NODOSELE
          .link_1_11('Full')
        endif
        .DoRTCalc(16,19,.t.)
        if .o_TPUNIPRO<>.w_TPUNIPRO.or. .o_TP<>.w_TP
          .w_TPFIL = subst("RE-AR-CL-MA", at(.w_TP,"UP-RE-AR-CL"),2)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RLCALRIS with this.w_RLCALRIS
      replace t_RLWIPRIS with this.w_RLWIPRIS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_VNBIWUZXOX()
    with this
          * --- 
          GSCI_BTV(this;
              ,"CheckDett";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCTIPUNP_1_8.enabled = this.oPgFrm.Page1.oPag.oSCTIPUNP_1_8.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Insert row start") or lower(cEvent)==lower("Update row start")
          .Calculate_VNBIWUZXOX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCCODFIG
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODFIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCCODFIG)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPFIG);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_SCTIPFIG;
                     ,'RLCODICE',trim(this.w_SCCODFIG))
          select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODFIG)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODFIG) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oSCCODFIG_2_2'),i_cWhere,'',"",'GSCI_MSC.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPFIG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso - tipo componente non compatibile con tipo struttura")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPFIG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODFIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCCODFIG);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPFIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPFIG;
                       ,'RLCODICE',this.w_SCCODFIG)
            select RL__TIPO,RLCODICE,RLDESCRI,RLCALRIS,RLWIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODFIG = NVL(_Link_.RLCODICE,space(20))
      this.w_DESFIG = NVL(_Link_.RLDESCRI,space(40))
      this.w_RLCALRIS = NVL(_Link_.RLCALRIS,space(5))
      this.w_RLWIPRIS = NVL(_Link_.RLWIPRIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODFIG = space(20)
      endif
      this.w_DESFIG = space(40)
      this.w_RLCALRIS = space(5)
      this.w_RLWIPRIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODFIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCUNIPRO
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCUNIPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_SCTIPUNP;
                     ,'RLCODICE',trim(this.w_SCUNIPRO))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCUNIPRO)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_SCUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_SCUNIPRO)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_SCTIPUNP);

            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCUNIPRO) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oSCUNIPRO_1_9'),i_cWhere,'',"Elenco componenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPUNP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Specificare una unit� produttiva")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCUNIPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCUNIPRO);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPUNP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPUNP;
                       ,'RLCODICE',this.w_SCUNIPRO)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCUNIPRO = NVL(_Link_.RLCODICE,space(20))
      this.w_SCDESUNP = NVL(_Link_.RLDESCRI,space(40))
      this.w_TPUNIPRO = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SCUNIPRO = space(20)
      endif
      this.w_SCDESUNP = space(40)
      this.w_TPUNIPRO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCUNIPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCCODPAD
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_SCCODPAD)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPPAD);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_SCTIPPAD;
                     ,'RLCODICE',trim(this.w_SCCODPAD))
          select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODPAD)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_SCCODPAD)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPPAD);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_SCCODPAD)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_SCTIPPAD);

            select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCCODPAD) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oSCCODPAD_1_11'),i_cWhere,'',"Elenco componenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SCTIPPAD<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Specificare una unit� produttiva/reparto/area/centro di lavoro")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPPAD);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_SCCODPAD);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_SCTIPPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_SCTIPPAD;
                       ,'RLCODICE',this.w_SCCODPAD)
            select RL__TIPO,RLCODICE,RLDESCRI,RLMAGWIP,RLCODCAL,RLWIPRIS,RLCALRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODPAD = NVL(_Link_.RLCODICE,space(20))
      this.w_SCDESPAD = NVL(_Link_.RLDESCRI,space(40))
      this.w_EREWIP = NVL(_Link_.RLMAGWIP,space(5))
      this.w_ERECAL = NVL(_Link_.RLCODCAL,space(5))
      this.w_PADWIP = NVL(_Link_.RLWIPRIS,space(5))
      this.w_PADCAL = NVL(_Link_.RLCALRIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODPAD = space(20)
      endif
      this.w_SCDESPAD = space(40)
      this.w_EREWIP = space(5)
      this.w_ERECAL = space(5)
      this.w_PADWIP = space(5)
      this.w_PADCAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TP $ 'UP-RE-AR-CL'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Specificare una unit� produttiva/reparto/area/centro di lavoro")
        endif
        this.w_SCCODPAD = space(20)
        this.w_SCDESPAD = space(40)
        this.w_EREWIP = space(5)
        this.w_ERECAL = space(5)
        this.w_PADWIP = space(5)
        this.w_PADCAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSCTIPUNP_1_8.RadioValue()==this.w_SCTIPUNP)
      this.oPgFrm.Page1.oPag.oSCTIPUNP_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCUNIPRO_1_9.value==this.w_SCUNIPRO)
      this.oPgFrm.Page1.oPag.oSCUNIPRO_1_9.value=this.w_SCUNIPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSCTIPPAD_1_10.RadioValue()==this.w_SCTIPPAD)
      this.oPgFrm.Page1.oPag.oSCTIPPAD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODPAD_1_11.value==this.w_SCCODPAD)
      this.oPgFrm.Page1.oPag.oSCCODPAD_1_11.value=this.w_SCCODPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESPAD_1_12.value==this.w_SCDESPAD)
      this.oPgFrm.Page1.oPag.oSCDESPAD_1_12.value=this.w_SCDESPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDESUNP_1_14.value==this.w_SCDESUNP)
      this.oPgFrm.Page1.oPag.oSCDESUNP_1_14.value=this.w_SCDESUNP
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.RadioValue()==this.w_SCTIPFIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.SetRadio()
      replace t_SCTIPFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODFIG_2_2.value==this.w_SCCODFIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODFIG_2_2.value=this.w_SCCODFIG
      replace t_SCCODFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCCODFIG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_3.value==this.w_DESFIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_3.value=this.w_DESFIG
      replace t_DESFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESFIG_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'STR_CONF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TP $ 'UP-RE-AR-CL')  and not(empty(.w_SCCODPAD))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCCODPAD_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare una unit� produttiva/reparto/area/centro di lavoro")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_SCCODFIG<>space(20));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_SCCODFIG<>space(20)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NODOSELE = this.w_NODOSELE
    this.o_TP = this.w_TP
    this.o_SCUNIPRO = this.w_SCUNIPRO
    this.o_SCTIPPAD = this.w_SCTIPPAD
    this.o_SCCODPAD = this.w_SCCODPAD
    this.o_TPUNIPRO = this.w_TPUNIPRO
    this.o_TPFIL = this.w_TPFIL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_SCCODFIG<>space(20))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_SCTIPFIG=space(2)
      .w_SCCODFIG=space(20)
      .w_DESFIG=space(40)
      .w_RLCALRIS=space(5)
      .w_RLWIPRIS=space(5)
      .DoRTCalc(1,6,.f.)
        .w_SCTIPFIG = .w_TPFIL
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_SCCODFIG))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(9,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_SCTIPFIG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.RadioValue(.t.)
    this.w_SCCODFIG = t_SCCODFIG
    this.w_DESFIG = t_DESFIG
    this.w_RLCALRIS = t_RLCALRIS
    this.w_RLWIPRIS = t_RLWIPRIS
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_SCTIPFIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSCTIPFIG_2_1.ToRadio()
    replace t_SCCODFIG with this.w_SCCODFIG
    replace t_DESFIG with this.w_DESFIG
    replace t_RLCALRIS with this.w_RLCALRIS
    replace t_RLWIPRIS with this.w_RLWIPRIS
    if i_srv='A'
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mscPag1 as StdContainer
  Width  = 669
  height = 377
  stdWidth  = 669
  stdheight = 377
  resizeXpos=202
  resizeYpos=317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=62, width=645,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="SCCODFIG",Label1="Componente",Field2="DESFIG",Label2="Descrizione",Field3="TF",Label3="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218965114


  add object oSCTIPUNP_1_8 as StdTableCombo with uid="DYKUPOUGMD",rtseq=12,rtrep=.f.,left=115,top=8,width=111,height=22;
    , HelpContextID = 192642186;
    , cFormVar="w_SCTIPUNP",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='TIP_RISO',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.


  func oSCTIPUNP_1_8.mCond()
    with this.Parent.oContained
      return (1=0)
    endwith
  endfunc

  func oSCTIPUNP_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCUNIPRO)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSCUNIPRO_1_9 as StdField with uid="AQPIHMOUJU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCUNIPRO", cQueryName = "SCTIPUNP,SCUNIPRO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare una unit� produttiva",;
    ToolTipText = "Codice padre struttura di configurazione",;
    HelpContextID = 253334389,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=181, Left=230, Top=9, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPUNP", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCUNIPRO"

  func oSCUNIPRO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCUNIPRO_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCUNIPRO_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSCUNIPRO_1_9.readonly and this.parent.oSCUNIPRO_1_9.isprimarykey)
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPUNP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_SCTIPUNP)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oSCUNIPRO_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco componenti",'',this.parent.oContained
   endif
  endproc


  add object oSCTIPPAD_1_10 as StdTableCombo with uid="ISPXCWXFEC",rtseq=14,rtrep=.f.,left=115,top=36,width=111,height=22;
    , HelpContextID = 260342634;
    , cFormVar="w_SCTIPPAD",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='TIP_RISO',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.


  func oSCTIPPAD_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCCODPAD)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSCCODPAD_1_11 as StdField with uid="RGHGLAIUUA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SCCODPAD", cQueryName = "SCTIPUNP,SCUNIPRO,SCTIPPAD,SCCODPAD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare una unit� produttiva/reparto/area/centro di lavoro",;
    ToolTipText = "Codice padre struttura di configurazione",;
    HelpContextID = 248083306,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=181, Left=230, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPPAD", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCCODPAD"

  func oSCCODPAD_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODPAD_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODPAD_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oSCCODPAD_1_11.readonly and this.parent.oSCCODPAD_1_11.isprimarykey)
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPPAD)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_SCTIPPAD)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oSCCODPAD_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco componenti",'',this.parent.oContained
   endif
  endproc

  add object oSCDESPAD_1_12 as StdField with uid="PWRJLUJNQE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCDESPAD", cQueryName = "SCDESPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 263160682,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=414, Top=35, InputMask=replicate('X',40)

  add object oSCDESUNP_1_14 as StdField with uid="BENKXYALZR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SCDESUNP", cQueryName = "SCDESUNP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 189824138,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=414, Top=9, InputMask=replicate('X',40)

  add object oStr_1_13 as StdString with uid="THPMZJKFJM",Visible=.t., Left=27, Top=38,;
    Alignment=1, Width=83, Height=18,;
    Caption="Risorsa padre:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="KDUXZZPOEX",Visible=.t., Left=9, Top=11,;
    Alignment=1, Width=101, Height=18,;
    Caption="Unit� produttiva:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=4,top=81,;
    width=641+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3999999999999999)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=5,top=82,width=640+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3999999999999999)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='RIS_ORSE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='RIS_ORSE'
        oDropInto=this.oBodyCol.oRow.oSCCODFIG_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsci_mscBodyRow as CPBodyRowCnt
  Width=631
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3999999999999999)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oSCTIPFIG_2_1 as stdTrsTableCombo with uid="ARONQXOVRJ",rtrep=.t.,;
    cFormVar="w_SCTIPFIG", tablefilter="" , enabled=.f.,;
    HelpContextID = 175864979,;
    Height=22, Width=160, Left=-2, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Selezionare un tipo risorsa';
, bIsInHeader=.f.;
    , cTable='TIP_RISO',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.



  func oSCTIPFIG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_SCCODFIG)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSCCODFIG_2_2 as StdTrsField with uid="OFFTSSBGRK",rtseq=8,rtrep=.t.,;
    cFormVar="w_SCCODFIG",value=space(20),;
    HelpContextID = 188124307,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso - tipo componente non compatibile con tipo struttura",;
   bGlobalFont=.t.,;
    Height=17, Width=160, Left=166, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_SCTIPFIG", oKey_2_1="RLCODICE", oKey_2_2="this.w_SCCODFIG"

  func oSCCODFIG_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODFIG_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSCCODFIG_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_SCTIPFIG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_SCTIPFIG)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oSCCODFIG_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_MSC.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oDESFIG_2_3 as StdTrsField with uid="NONUQBENJD",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESFIG",value=space(40),enabled=.f.,;
    HelpContextID = 166628298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=333, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oSCCODFIG_2_2.When()
    return(.t.)
  proc oSCCODFIG_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oSCCODFIG_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_msc','STR_CONF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCTIPUNP=STR_CONF.SCTIPUNP";
  +" and "+i_cAliasName2+".SCUNIPRO=STR_CONF.SCUNIPRO";
  +" and "+i_cAliasName2+".SCTIPPAD=STR_CONF.SCTIPPAD";
  +" and "+i_cAliasName2+".SCCODPAD=STR_CONF.SCCODPAD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
