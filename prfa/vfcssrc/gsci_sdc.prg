* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_sdc                                                        *
*              Stampa distinta costificata                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-22                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_sdc",oParentObject))

* --- Class definition
define class tgsci_sdc as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 924
  Height = 560+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=74057065
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=96

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  PAR_PROD_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  VALUTE_IDX = 0
  PAR_DISB_IDX = 0
  cPrg = "gsci_sdc"
  cComment = "Stampa distinta costificata"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_CODGES = space(2)
  w_FLDIGE = space(1)
  w_TIPGES = space(1)
  w_CODAZI = space(5)
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  w_QUANTI = 0
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_TIPVALCIC = space(2)
  w_TIPOARTI = space(1)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_DISPRO = space(1)
  w_FLGPRE = space(1)
  w_PERRIC = space(1)
  w_LOTTOMED = .F.
  w_TipoValo = space(1)
  o_TipoValo = space(1)
  w_Analisi = space(1)
  w_SCDRSR = space(1)
  w_AGGMPOND = space(1)
  w_ESERC = space(4)
  w_VALESE = space(3)
  w_NUMINV = space(6)
  o_NUMINV = space(6)
  w_Listino = space(5)
  o_Listino = space(5)
  w_VALLIS = space(3)
  w_CAOVAL = 0
  w_DESLIS = space(40)
  w_UM_ORE = space(3)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INCODBUN = space(3)
  w_INCODMAG = space(5)
  w_INMAGCOL = space(1)
  w_MAGRAG = space(5)
  w_INCATOMO = space(5)
  w_PRDATINV = ctod('  /  /  ')
  w_INDATINV = ctod('  /  /  ')
  w_INCODESE = space(5)
  w_INNUMPRE = space(6)
  w_INESEPRE = space(5)
  w_FLCHKLP = space(1)
  w_FDBNOLEG = space(1)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_OUNOMQUE = space(254)
  o_OUNOMQUE = space(254)
  w_SELEZI = space(1)
  w_ORIQUERY = space(10)
  w_QUERY = space(254)
  w_COSTILOG = space(1)
  w_NOMPRG = space(30)
  w_TipoValoOut = space(1)
  o_TipoValoOut = space(1)
  w_SMATOU = space(1)
  w_ESERCMO = space(4)
  w_NUMINVMO = space(6)
  w_ListinoMo = space(5)
  o_ListinoMo = space(5)
  w_DESLISMO = space(40)
  w_DATINVMO = ctod('  /  /  ')
  w_VALLISMO = space(3)
  w_FLSTATMO = space(1)
  w_INESCONT = space(1)
  w_PRESCONT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DIS1 = space(20)
  w_DIS2 = space(20)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_FLCOSINV = space(1)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CAOLIS = 0
  w_TIPOLN = space(1)
  w_CAOLISMO = 0
  w_TIPOLNMO = space(1)
  w_DIS1OB = ctod('  /  /  ')
  w_DIS2OB = ctod('  /  /  ')
  w_CAOVALMO = 0
  w_CAOESE = 0
  w_DECTOT = 0
  w_ESFINESE = ctod('  /  /  ')
  w_AZIENDA = space(5)
  w_FLDESART = space(1)
  w_PDCOSPAR = space(1)
  w_TIPCOS = space(1)
  w_FLDETTMAG = space(1)
  w_PDCOECOS = space(1)
  w_PDCOECOP = space(1)
  w_BTNQRY = .NULL.
  w_SelDis = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_sdcPag1","gsci_sdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsci_sdcPag2","gsci_sdc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsci_sdc
    This.Pages(2).Enabled=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    this.w_SelDis = this.oPgFrm.Pages(2).oPag.SelDis
    DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      this.w_SelDis = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='INVENTAR'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='FAM_ARTI'
    this.cWorkTables[7]='GRUMERC'
    this.cWorkTables[8]='CATEGOMO'
    this.cWorkTables[9]='MAGAZZIN'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='VALUTE'
    this.cWorkTables[12]='PAR_DISB'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDS1BDC(this,"STAMPA")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_CODGES=space(2)
      .w_FLDIGE=space(1)
      .w_TIPGES=space(1)
      .w_CODAZI=space(5)
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_QUANTI=0
      .w_DATSTA=ctod("  /  /  ")
      .w_TIPVALCIC=space(2)
      .w_TIPOARTI=space(1)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_DISPRO=space(1)
      .w_FLGPRE=space(1)
      .w_PERRIC=space(1)
      .w_LOTTOMED=.f.
      .w_TipoValo=space(1)
      .w_Analisi=space(1)
      .w_SCDRSR=space(1)
      .w_AGGMPOND=space(1)
      .w_ESERC=space(4)
      .w_VALESE=space(3)
      .w_NUMINV=space(6)
      .w_Listino=space(5)
      .w_VALLIS=space(3)
      .w_CAOVAL=0
      .w_DESLIS=space(40)
      .w_UM_ORE=space(3)
      .w_INGRUMER=space(5)
      .w_INCODFAM=space(5)
      .w_INCODBUN=space(3)
      .w_INCODMAG=space(5)
      .w_INMAGCOL=space(1)
      .w_MAGRAG=space(5)
      .w_INCATOMO=space(5)
      .w_PRDATINV=ctod("  /  /  ")
      .w_INDATINV=ctod("  /  /  ")
      .w_INCODESE=space(5)
      .w_INNUMPRE=space(6)
      .w_INESEPRE=space(5)
      .w_FLCHKLP=space(1)
      .w_FDBNOLEG=space(1)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_OUNOMQUE=space(254)
      .w_SELEZI=space(1)
      .w_ORIQUERY=space(10)
      .w_QUERY=space(254)
      .w_COSTILOG=space(1)
      .w_NOMPRG=space(30)
      .w_TipoValoOut=space(1)
      .w_SMATOU=space(1)
      .w_ESERCMO=space(4)
      .w_NUMINVMO=space(6)
      .w_ListinoMo=space(5)
      .w_DESLISMO=space(40)
      .w_DATINVMO=ctod("  /  /  ")
      .w_VALLISMO=space(3)
      .w_FLSTATMO=space(1)
      .w_PRESCONT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DIS1=space(20)
      .w_DIS2=space(20)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_FLCOSINV=space(1)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_CAOLIS=0
      .w_TIPOLN=space(1)
      .w_CAOLISMO=0
      .w_TIPOLNMO=space(1)
      .w_DIS1OB=ctod("  /  /  ")
      .w_DIS2OB=ctod("  /  /  ")
      .w_CAOVALMO=0
      .w_CAOESE=0
      .w_DECTOT=0
      .w_ESFINESE=ctod("  /  /  ")
      .w_AZIENDA=space(5)
      .w_FLDESART=space(1)
      .w_PDCOSPAR=space(1)
      .w_TIPCOS=space(1)
      .w_FLDETTMAG=space(1)
      .w_PDCOECOS=space(1)
      .w_PDCOECOP=space(1)
        .w_PARAM = iif(TYPE('this.oParentObject')="O", "G", this.oParentObject)
        .w_CODGES = 'PP'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODGES))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_TIPGES = iif(.w_PARAM="P" or .w_FLDIGE="S", .w_PARAM, "G")
        .w_CODAZI = i_CODAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODINI))
          .link_1_6('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODFIN))
          .link_1_7('Full')
        endif
        .w_QUANTI = 1
        .w_DATSTA = i_datsys
        .w_TIPVALCIC = '00'
        .w_TIPOARTI = ''
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_GRUINI))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CATINI))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CATFIN))
          .link_1_17('Full')
        endif
        .w_DISPRO = "N"
        .w_FLGPRE = "S"
        .w_PERRIC = "S"
        .w_LOTTOMED = True
        .w_TipoValo = "S"
        .w_Analisi = "S"
        .w_SCDRSR = "S"
        .w_AGGMPOND = "M"
        .w_ESERC = g_CODESE
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_ESERC))
          .link_1_35('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_VALESE))
          .link_1_36('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_NUMINV))
          .link_1_37('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_Listino))
          .link_1_39('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_VALLIS))
          .link_1_40('Full')
        endif
          .DoRTCalc(31,32,.f.)
        .w_UM_ORE = ah_msgformat('Ore')
          .DoRTCalc(34,41,.f.)
        .w_INDATINV = .w_DATSTA
        .w_INCODESE = .w_ESERC
        .w_INNUMPRE = .w_NUMINV
        .w_INESEPRE = .w_ESERC
        .w_FLCHKLP = g_CHKDIBALOOP
        .w_FDBNOLEG = 'N'
      .oPgFrm.Page1.oPag.BTNQRY.Calculate()
      .oPgFrm.Page2.oPag.SelDis.Calculate()
          .DoRTCalc(48,54,.f.)
        .w_SELEZI = 'S'
        .w_ORIQUERY = alltrim(.w_SelDis.cCpQueryName)
        .w_QUERY = IIF(FILE(.w_OUNOMQUE) And lower(Alltrim(right(.w_OUNOMQUE,4)))=='.vqr', .w_QUERY, "")
        .w_COSTILOG = "N"
        .w_NOMPRG = IIF(.w_SCDRSR='N', "GSCI_SDC" ,  "GSCI_SDC_D")
      .oPgFrm.Page1.oPag.oObj_1_87.Calculate(.w_NOMPRG)
        .w_TipoValoOut = "NN"
        .w_SMATOU = "S"
        .w_ESERCMO = g_CODESE
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_ESERCMO))
          .link_1_95('Full')
        endif
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_NUMINVMO))
          .link_1_96('Full')
        endif
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_ListinoMo))
          .link_1_98('Full')
        endif
        .DoRTCalc(65,67,.f.)
        if not(empty(.w_VALLISMO))
          .link_1_105('Full')
        endif
          .DoRTCalc(68,69,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(71,72,.f.)
        if not(empty(.w_DIS1))
          .link_1_112('Full')
        endif
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_DIS2))
          .link_1_113('Full')
        endif
          .DoRTCalc(74,75,.f.)
        .w_FLCOSINV = IIF(.w_TipoValo $ 'SMUP' , "S", "N")
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(77,77,.f.)
        if not(empty(.w_CODMAG))
          .link_1_121('Full')
        endif
          .DoRTCalc(78,78,.f.)
        .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
          .DoRTCalc(80,80,.f.)
        .w_CAOLISMO = IIF(.w_TipoValoOut='L', IIF(.w_CAOVALMO=0, GETCAM(.w_VALLISMO, .w_DATSTA, 7), .w_CAOVALMO), .w_CAOLISMO)
          .DoRTCalc(82,88,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_137('Full')
        endif
        .w_FLDESART = 'S'
          .DoRTCalc(91,91,.f.)
        .w_TIPCOS = .w_PDCOSPAR
        .w_FLDETTMAG = 'S'
        .w_PDCOECOS = .w_PDCOECOP
    endwith
    this.DoRTCalc(95,95,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,6,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_7('Full')
        endif
        .DoRTCalc(8,26,.t.)
          .link_1_36('Full')
        .DoRTCalc(28,29,.t.)
          .link_1_40('Full')
        .DoRTCalc(31,41,.t.)
            .w_INDATINV = .w_DATSTA
            .w_INCODESE = .w_ESERC
            .w_INNUMPRE = .w_NUMINV
            .w_INESEPRE = .w_ESERC
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.SelDis.Calculate()
        .DoRTCalc(46,56,.t.)
            .w_QUERY = IIF(FILE(.w_OUNOMQUE) And lower(Alltrim(right(.w_OUNOMQUE,4)))=='.vqr', .w_QUERY, "")
        if .o_OUNOMQUE<>.w_OUNOMQUE
          .Calculate_TWKEFLENMI()
        endif
        .DoRTCalc(58,58,.t.)
            .w_NOMPRG = IIF(.w_SCDRSR='N', "GSCI_SDC" ,  "GSCI_SDC_D")
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate(.w_NOMPRG)
        .DoRTCalc(60,61,.t.)
          .link_1_95('Full')
        .DoRTCalc(63,66,.t.)
          .link_1_105('Full')
        .DoRTCalc(68,68,.t.)
        if .o_NUMINV<>.w_NUMINV
            .w_INESCONT = iif(empty(.w_NUMINV),'N', iif(empty(.w_INESCONT), 'N', .w_INESCONT))
        endif
        .DoRTCalc(70,72,.t.)
          .link_1_112('Full')
          .link_1_113('Full')
        .DoRTCalc(75,76,.t.)
        if .o_TipoValo<>.w_TipoValo
            .w_FLCOSINV = IIF(.w_TipoValo $ 'SMUP' , "S", "N")
        endif
        .DoRTCalc(78,79,.t.)
        if .o_DATSTA<>.w_DATSTA.or. .o_Listino<>.w_Listino.or. .o_TipoValo<>.w_TipoValo
            .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
        endif
        .DoRTCalc(81,81,.t.)
        if .o_DATSTA<>.w_DATSTA.or. .o_ListinoMo<>.w_ListinoMo.or. .o_TipoValoOut<>.w_TipoValoOut
            .w_CAOLISMO = IIF(.w_TipoValoOut='L', IIF(.w_CAOVALMO=0, GETCAM(.w_VALLISMO, .w_DATSTA, 7), .w_CAOVALMO), .w_CAOLISMO)
        endif
        .DoRTCalc(83,89,.t.)
          .link_1_137('Full')
        .DoRTCalc(91,93,.t.)
            .w_FLDETTMAG = 'S'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(95,96,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.SelDis.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate(.w_NOMPRG)
    endwith
  return

  proc Calculate_WKQUULDECR()
    with this
          * --- Selezione Zoom
          GSDS2BDC(this;
              ,'SELDESEL';
             )
    endwith
  endproc
  proc Calculate_TWKEFLENMI()
    with this
          * --- Eventi damodifica query di selezione
          .w_OUNOMQUE = SYS(2014, .w_OUNOMQUE)
          .w_OUNOMQUE = IIF(FILE(.w_OUNOMQUE) And lower(Alltrim(right(.w_OUNOMQUE,4)))=='.vqr', .w_OUNOMQUE, "")
          .w_CODINI = IIF(Empty(.w_OUNOMQUE), .w_CODINI, "")
          GSDS2BDC(this;
              ,'ANTEPRIMA';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oESERC_1_35.enabled = this.oPgFrm.Page1.oPag.oESERC_1_35.mCond()
    this.oPgFrm.Page1.oPag.oINESCONT_1_107.enabled = this.oPgFrm.Page1.oPag.oINESCONT_1_107.mCond()
    this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.enabled = this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.mCond()
    this.oPgFrm.Page1.oPag.oCODMAG_1_121.enabled = this.oPgFrm.Page1.oPag.oCODMAG_1_121.mCond()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_124.enabled = this.oPgFrm.Page1.oPag.oCAOLIS_1_124.mCond()
    this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.enabled = this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oESERC_1_35.visible=!this.oPgFrm.Page1.oPag.oESERC_1_35.mHide()
    this.oPgFrm.Page1.oPag.oNUMINV_1_37.visible=!this.oPgFrm.Page1.oPag.oNUMINV_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oListino_1_39.visible=!this.oPgFrm.Page1.oPag.oListino_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_43.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_43.mHide()
    this.oPgFrm.Page1.oPag.oPRDATINV_1_54.visible=!this.oPgFrm.Page1.oPag.oPRDATINV_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oESERCMO_1_95.visible=!this.oPgFrm.Page1.oPag.oESERCMO_1_95.mHide()
    this.oPgFrm.Page1.oPag.oNUMINVMO_1_96.visible=!this.oPgFrm.Page1.oPag.oNUMINVMO_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oListinoMo_1_98.visible=!this.oPgFrm.Page1.oPag.oListinoMo_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oDESLISMO_1_100.visible=!this.oPgFrm.Page1.oPag.oDESLISMO_1_100.mHide()
    this.oPgFrm.Page1.oPag.oDATINVMO_1_101.visible=!this.oPgFrm.Page1.oPag.oDATINVMO_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oINESCONT_1_107.visible=!this.oPgFrm.Page1.oPag.oINESCONT_1_107.mHide()
    this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.visible=!this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.mHide()
    this.oPgFrm.Page1.oPag.oCODMAG_1_121.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_121.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_122.visible=!this.oPgFrm.Page1.oPag.oStr_1_122.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_123.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_123.mHide()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_124.visible=!this.oPgFrm.Page1.oPag.oCAOLIS_1_124.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.visible=!this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
      .oPgFrm.Page2.oPag.SelDis.Event(cEvent)
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_WKQUULDECR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OUNOMQUE Changed")
          .Calculate_TWKEFLENMI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODGES
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPFLDIGE,PPUM_ORE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_CODGES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_CODGES)
            select PPCODICE,PPFLDIGE,PPUM_ORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGES = NVL(_Link_.PPCODICE,space(2))
      this.w_FLDIGE = NVL(_Link_.PPFLDIGE,space(1))
      this.w_UM_ORE = NVL(_Link_.PPUM_ORE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODGES = space(2)
      endif
      this.w_FLDIGE = space(1)
      this.w_UM_ORE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_6'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DIS1 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DIS1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DIS1 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_7'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DIS2 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DIS2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DIS2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_12'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_13'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_14'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_15'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_16'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_17'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERC
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERC)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESERC))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERC)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERC) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERC_1_35'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERC)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERC = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
      this.w_ESFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESERC = space(4)
      endif
      this.w_VALESE = space(3)
      this.w_ESFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINV
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERC;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_1_37'),i_cWhere,'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERC;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
      this.w_PRESCONT = NVL(_Link_.INESCONT,space(1))
      this.w_INESCONT = NVL(_Link_.INESCONT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_PRDATINV = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
      this.w_PRESCONT = space(1)
      this.w_INESCONT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Listino
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Listino) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_Listino)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_Listino))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Listino)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Listino) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oListino_1_39'),i_cWhere,'',"Anagrafica listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Listino)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_Listino);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_Listino)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Listino = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_Listino = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Listino Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCMO
  func Link_1_95(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCMO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCMO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCMO = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINVMO
  func Link_1_96(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINVMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINVMO)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERCMO;
                     ,'INNUMINV',trim(this.w_NUMINVMO))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINVMO)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINVMO) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINVMO_1_96'),i_cWhere,'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERCMO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINVMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINVMO);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERCMO;
                       ,'INNUMINV',this.w_NUMINVMO)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINVMO = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_DATINVMO = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINVMO = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_DATINVMO = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINVMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ListinoMo
  func Link_1_98(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ListinoMo) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ListinoMo)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ListinoMo))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ListinoMo)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ListinoMo) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oListinoMo_1_98'),i_cWhere,'',"Anagrafica listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ListinoMo)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ListinoMo);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ListinoMo)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ListinoMo = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLISMO = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLISMO = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLNMO = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ListinoMo = space(5)
      endif
      this.w_DESLISMO = space(40)
      this.w_VALLISMO = space(3)
      this.w_TIPOLNMO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ListinoMo Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLISMO
  func Link_1_105(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLISMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLISMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLISMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLISMO)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLISMO = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVALMO = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLISMO = space(3)
      endif
      this.w_CAOVALMO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLISMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS1
  func Link_1_112(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS1)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS1 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS1OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS1 = space(20)
      endif
      this.w_DIS1OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS2
  func Link_1_113(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS2)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS2 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS2OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS2 = space(20)
      endif
      this.w_DIS2OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_121(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_121'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_1_137(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDCOSPAR,PDCOECOS";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_AZIENDA)
            select PDCODAZI,PDCOSPAR,PDCOECOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.PDCODAZI,space(5))
      this.w_PDCOSPAR = NVL(_Link_.PDCOSPAR,space(1))
      this.w_PDCOECOP = NVL(_Link_.PDCOECOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PDCOSPAR = space(1)
      this.w_PDCOECOP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_6.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_6.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_7.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_7.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oQUANTI_1_8.value==this.w_QUANTI)
      this.oPgFrm.Page1.oPag.oQUANTI_1_8.value=this.w_QUANTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_9.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_9.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVALCIC_1_10.RadioValue()==this.w_TIPVALCIC)
      this.oPgFrm.Page1.oPag.oTIPVALCIC_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOARTI_1_11.RadioValue()==this.w_TIPOARTI)
      this.oPgFrm.Page1.oPag.oTIPOARTI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_12.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_12.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_13.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_13.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_14.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_14.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_15.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_15.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_16.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_16.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_17.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_17.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPRO_1_18.RadioValue()==this.w_DISPRO)
      this.oPgFrm.Page1.oPag.oDISPRO_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERRIC_1_27.RadioValue()==this.w_PERRIC)
      this.oPgFrm.Page1.oPag.oPERRIC_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOMED_1_28.RadioValue()==this.w_LOTTOMED)
      this.oPgFrm.Page1.oPag.oLOTTOMED_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoValo_1_29.RadioValue()==this.w_TipoValo)
      this.oPgFrm.Page1.oPag.oTipoValo_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnalisi_1_31.RadioValue()==this.w_Analisi)
      this.oPgFrm.Page1.oPag.oAnalisi_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDRSR_1_32.RadioValue()==this.w_SCDRSR)
      this.oPgFrm.Page1.oPag.oSCDRSR_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESERC_1_35.value==this.w_ESERC)
      this.oPgFrm.Page1.oPag.oESERC_1_35.value=this.w_ESERC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINV_1_37.value==this.w_NUMINV)
      this.oPgFrm.Page1.oPag.oNUMINV_1_37.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oListino_1_39.value==this.w_Listino)
      this.oPgFrm.Page1.oPag.oListino_1_39.value=this.w_Listino
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_43.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_43.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_54.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_54.value=this.w_PRDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLP_1_62.RadioValue()==this.w_FLCHKLP)
      this.oPgFrm.Page1.oPag.oFLCHKLP_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_66.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_66.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_67.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_67.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_68.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_68.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_72.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_72.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_73.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_73.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_74.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_74.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMQUE_1_78.value==this.w_OUNOMQUE)
      this.oPgFrm.Page1.oPag.oOUNOMQUE_1_78.value=this.w_OUNOMQUE
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_3.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSTILOG_1_84.RadioValue()==this.w_COSTILOG)
      this.oPgFrm.Page1.oPag.oCOSTILOG_1_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoValoOut_1_90.RadioValue()==this.w_TipoValoOut)
      this.oPgFrm.Page1.oPag.oTipoValoOut_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSMATOU_1_92.RadioValue()==this.w_SMATOU)
      this.oPgFrm.Page1.oPag.oSMATOU_1_92.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCMO_1_95.value==this.w_ESERCMO)
      this.oPgFrm.Page1.oPag.oESERCMO_1_95.value=this.w_ESERCMO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINVMO_1_96.value==this.w_NUMINVMO)
      this.oPgFrm.Page1.oPag.oNUMINVMO_1_96.value=this.w_NUMINVMO
    endif
    if not(this.oPgFrm.Page1.oPag.oListinoMo_1_98.value==this.w_ListinoMo)
      this.oPgFrm.Page1.oPag.oListinoMo_1_98.value=this.w_ListinoMo
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLISMO_1_100.value==this.w_DESLISMO)
      this.oPgFrm.Page1.oPag.oDESLISMO_1_100.value=this.w_DESLISMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINVMO_1_101.value==this.w_DATINVMO)
      this.oPgFrm.Page1.oPag.oDATINVMO_1_101.value=this.w_DATINVMO
    endif
    if not(this.oPgFrm.Page1.oPag.oINESCONT_1_107.RadioValue()==this.w_INESCONT)
      this.oPgFrm.Page1.oPag.oINESCONT_1_107.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_114.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_114.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_115.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_115.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.RadioValue()==this.w_FLCOSINV)
      this.oPgFrm.Page1.oPag.oFLCOSINV_1_118.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_121.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_121.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_123.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_123.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLIS_1_124.value==this.w_CAOLIS)
      this.oPgFrm.Page1.oPag.oCAOLIS_1_124.value=this.w_CAOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.value==this.w_CAOLISMO)
      this.oPgFrm.Page1.oPag.oCAOLISMO_1_127.value=this.w_CAOLISMO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCOS_1_140.RadioValue()==this.w_TIPCOS)
      this.oPgFrm.Page1.oPag.oTIPCOS_1_140.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOECOS_1_142.RadioValue()==this.w_PDCOECOS)
      this.oPgFrm.Page1.oPag.oPDCOECOS_1_142.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1))))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not((EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not(.w_QUANTI>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQUANTI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODMAG))  and not(.w_TipoValo <> 'A')  and (.w_TipoValo = 'A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_121.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOLIS))  and not(.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))  and (.w_CAOVAL=0 AND .w_TipoValo="L" AND NOT EMPTY(.w_Listino))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLIS_1_124.SetFocus()
            i_bnoObbl = !empty(.w_CAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOLISMO))  and not(.w_CAOVALMO<>0 OR .w_TipoValoOut<>"L" OR EMPTY(.w_ListinoMo))  and (.w_CAOVALMO=0 AND .w_TipoValoOut="L" AND NOT EMPTY(.w_ListinoMo))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLISMO_1_127.SetFocus()
            i_bnoObbl = !empty(.w_CAOLISMO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_DATSTA = this.w_DATSTA
    this.o_TipoValo = this.w_TipoValo
    this.o_NUMINV = this.w_NUMINV
    this.o_Listino = this.w_Listino
    this.o_OUNOMQUE = this.w_OUNOMQUE
    this.o_TipoValoOut = this.w_TipoValoOut
    this.o_ListinoMo = this.w_ListinoMo
    return

enddefine

* --- Define pages as container
define class tgsci_sdcPag1 as StdContainer
  Width  = 920
  height = 560
  stdWidth  = 920
  stdheight = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_6 as StdField with uid="YLDHOMMPET",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 104624602,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=127, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_7 as StdField with uid="VXFHEBELNK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Distinta base di fine selezione (spazio=no selezione)",;
    HelpContextID = 26178010,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=127, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oQUANTI_1_8 as StdField with uid="YMUSPKJVYW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_QUANTI", cQueryName = "QUANTI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 98015994,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=127, Top=60, cSayPict="V_PQ(12)", cGetPict="V_GQ(12)"

  func oQUANTI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_QUANTI>0)
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_9 as StdField with uid="RWKCWSGLTB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di validit� dei legami delle distinte",;
    HelpContextID = 231833546,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=503, Top=60


  add object oTIPVALCIC_1_10 as StdCombo with uid="IXIOKXTFBG",rtseq=10,rtrep=.f.,left=685,top=60,width=166,height=21;
    , ToolTipText = "Seleziona il criterio di valorizzazione del ciclo";
    , HelpContextID = 201412015;
    , cFormVar="w_TIPVALCIC",RowSource=""+"Ciclo preferenziale,"+"Da anagrafica ciclo,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPVALCIC_1_10.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'99',;
    iif(this.value =3,'NN',;
    space(2)))))
  endfunc
  func oTIPVALCIC_1_10.GetRadio()
    this.Parent.oContained.w_TIPVALCIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPVALCIC_1_10.SetRadio()
    this.Parent.oContained.w_TIPVALCIC=trim(this.Parent.oContained.w_TIPVALCIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVALCIC=='00',1,;
      iif(this.Parent.oContained.w_TIPVALCIC=='99',2,;
      iif(this.Parent.oContained.w_TIPVALCIC=='NN',3,;
      0)))
  endfunc


  add object oTIPOARTI_1_11 as StdCombo with uid="OYHVZTLCJK",value=4,rtseq=11,rtrep=.f.,left=127,top=84,width=236,height=22;
    , ToolTipText = "Seleziona il tipo articolo legato alla distinta base";
    , HelpContextID = 33180031;
    , cFormVar="w_TIPOARTI",RowSource=""+"Prodotto finito,"+"Semilavorato,"+"Fanstasma,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOARTI_1_11.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'PH',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oTIPOARTI_1_11.GetRadio()
    this.Parent.oContained.w_TIPOARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPOARTI_1_11.SetRadio()
    this.Parent.oContained.w_TIPOARTI=trim(this.Parent.oContained.w_TIPOARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOARTI=='PF',1,;
      iif(this.Parent.oContained.w_TIPOARTI=='SE',2,;
      iif(this.Parent.oContained.w_TIPOARTI=='PH',3,;
      iif(this.Parent.oContained.w_TIPOARTI=='',4,;
      0))))
  endfunc

  add object oFAMAINI_1_12 as StdField with uid="BAUZFWPTWC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 241963094,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_13 as StdField with uid="GHDDKOKVIG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 113504170,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=549, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_13.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_14 as StdField with uid="VYQYSTJFCH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 104554138,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_15 as StdField with uid="BKTBROMCMD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 26107546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=549, Top=132, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_15.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_16 as StdField with uid="WQNLFQEXDD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 104562650,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_17 as StdField with uid="WHQQEZYTLE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 26116058,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=549, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDISPRO_1_18 as StdCheck with uid="ADZNMGANBB",rtseq=18,rtrep=.f.,left=23, top=201, caption="Provvisorie",;
    ToolTipText = "Stampa la distinta provvisoria",;
    HelpContextID = 267683786,;
    cFormVar="w_DISPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDISPRO_1_18.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDISPRO_1_18.GetRadio()
    this.Parent.oContained.w_DISPRO = this.RadioValue()
    return .t.
  endfunc

  func oDISPRO_1_18.SetRadio()
    this.Parent.oContained.w_DISPRO=trim(this.Parent.oContained.w_DISPRO)
    this.value = ;
      iif(this.Parent.oContained.w_DISPRO=="S",1,;
      0)
  endfunc

  add object oPERRIC_1_27 as StdCheck with uid="ITJTQSYEXZ",rtseq=20,rtrep=.f.,left=344, top=201, caption="% Ricarico",;
    ToolTipText = "Aggiunge la percentuale di ricarico nei costi",;
    HelpContextID = 209885962,;
    cFormVar="w_PERRIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERRIC_1_27.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPERRIC_1_27.GetRadio()
    this.Parent.oContained.w_PERRIC = this.RadioValue()
    return .t.
  endfunc

  func oPERRIC_1_27.SetRadio()
    this.Parent.oContained.w_PERRIC=trim(this.Parent.oContained.w_PERRIC)
    this.value = ;
      iif(this.Parent.oContained.w_PERRIC=="S",1,;
      0)
  endfunc

  add object oLOTTOMED_1_28 as StdCheck with uid="UXMYKZWBJJ",rtseq=21,rtrep=.f.,left=451, top=201, caption="Su lotto medio",;
    ToolTipText = "Calcola i costi di fissi in funzione del lotto medio",;
    HelpContextID = 232754938,;
    cFormVar="w_LOTTOMED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLOTTOMED_1_28.RadioValue()
    return(iif(this.value =1,True,;
    False))
  endfunc
  func oLOTTOMED_1_28.GetRadio()
    this.Parent.oContained.w_LOTTOMED = this.RadioValue()
    return .t.
  endfunc

  func oLOTTOMED_1_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LOTTOMED==True,1,;
      0)
  endfunc


  add object oTipoValo_1_29 as StdCombo with uid="PBTHRECYUL",rtseq=22,rtrep=.f.,left=127,top=246,width=242,height=20;
    , ToolTipText = "Criterio di valorizzazione dei materiali d'acquisto";
    , HelpContextID = 227776091;
    , cFormVar="w_TipoValo",RowSource=""+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Ultimo costo dei saldi (articolo),"+"Ultimo costo standard (articolo),"+"Costo di listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoValo_1_29.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"M",;
    iif(this.value =3,"P",;
    iif(this.value =4,"U",;
    iif(this.value =5,"A",;
    iif(this.value =6,"X",;
    iif(this.value =7,"L",;
    space(1)))))))))
  endfunc
  func oTipoValo_1_29.GetRadio()
    this.Parent.oContained.w_TipoValo = this.RadioValue()
    return .t.
  endfunc

  func oTipoValo_1_29.SetRadio()
    this.Parent.oContained.w_TipoValo=trim(this.Parent.oContained.w_TipoValo)
    this.value = ;
      iif(this.Parent.oContained.w_TipoValo=="S",1,;
      iif(this.Parent.oContained.w_TipoValo=="M",2,;
      iif(this.Parent.oContained.w_TipoValo=="P",3,;
      iif(this.Parent.oContained.w_TipoValo=="U",4,;
      iif(this.Parent.oContained.w_TipoValo=="A",5,;
      iif(this.Parent.oContained.w_TipoValo=="X",6,;
      iif(this.Parent.oContained.w_TipoValo=="L",7,;
      0)))))))
  endfunc


  add object oAnalisi_1_31 as StdCombo with uid="DYQZGYNLZI",rtseq=23,rtrep=.f.,left=80,top=441,width=146,height=22;
    , ToolTipText = "Stampa l'analisi dei costi dei prodotti";
    , HelpContextID = 174555898;
    , cFormVar="w_Analisi",RowSource=""+"Sintetica,"+"Dettagliata,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAnalisi_1_31.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    iif(this.value =3,"N",;
    space(1)))))
  endfunc
  func oAnalisi_1_31.GetRadio()
    this.Parent.oContained.w_Analisi = this.RadioValue()
    return .t.
  endfunc

  func oAnalisi_1_31.SetRadio()
    this.Parent.oContained.w_Analisi=trim(this.Parent.oContained.w_Analisi)
    this.value = ;
      iif(this.Parent.oContained.w_Analisi=="S",1,;
      iif(this.Parent.oContained.w_Analisi=="D",2,;
      iif(this.Parent.oContained.w_Analisi=="N",3,;
      0)))
  endfunc

  add object oSCDRSR_1_32 as StdCheck with uid="DXEMKOIEXI",rtseq=24,rtrep=.f.,left=234, top=441, caption="Ciclo di lavorazione",;
    ToolTipText = "Stampa i costi delle risorse",;
    HelpContextID = 216235226,;
    cFormVar="w_SCDRSR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCDRSR_1_32.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSCDRSR_1_32.GetRadio()
    this.Parent.oContained.w_SCDRSR = this.RadioValue()
    return .t.
  endfunc

  func oSCDRSR_1_32.SetRadio()
    this.Parent.oContained.w_SCDRSR=trim(this.Parent.oContained.w_SCDRSR)
    this.value = ;
      iif(this.Parent.oContained.w_SCDRSR=="S",1,;
      0)
  endfunc

  add object oESERC_1_35 as StdField with uid="QVYDMKMJJA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ESERC", cQueryName = "ESERC",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 266558906,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=270, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERC"

  func oESERC_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ "S-M-U-P-A" or .w_Analisi <> "N")
    endwith
   endif
  endfunc

  func oESERC_1_35.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  func oESERC_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
      if .not. empty(.w_NUMINV)
        bRes2=.link_1_37('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERC_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oNUMINV_1_37 as StdField with uid="LQIQGVGGRS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 154917674,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=297, Top=270, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERC", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_1_37.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "SMEPCU") AND .w_Analisi = "N")
    endwith
  endfunc

  func oNUMINV_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERC)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINV_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERC
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINV
     i_obj.ecpSave()
  endproc

  add object oListino_1_39 as StdField with uid="RQFSDHQWGW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_Listino", cQueryName = "Listino",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 10590390,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=296, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_Listino"

  func oListino_1_39.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc

  func oListino_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oListino_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oListino_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oListino_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica listini",'',this.parent.oContained
  endproc

  add object oDESLIS_1_43 as StdField with uid="WGBDTWDQZP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210275274,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=192, Top=296, InputMask=replicate('X',40)

  func oDESLIS_1_43.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc


  add object oBtn_1_44 as StdButton with uid="UAKOHXBSJK",left=811, top=514, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 16888854;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        do GSDS1BDC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_45 as StdButton with uid="EBPRNWCFJX",left=866, top=514, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 16888854;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRDATINV_1_54 as StdField with uid="UEIFXOYOKT",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 98856372,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=402, Top=270

  func oPRDATINV_1_54.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "SMEPCU") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oFLCHKLP_1_62 as StdCheck with uid="SKYASPWUKP",rtseq=46,rtrep=.f.,left=164, top=201, caption="Loop distinta",;
    ToolTipText = "Se attivo abilita controllo del loop in distinta base",;
    HelpContextID = 57509034,;
    cFormVar="w_FLCHKLP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLP_1_62.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCHKLP_1_62.GetRadio()
    this.Parent.oContained.w_FLCHKLP = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLP_1_62.SetRadio()
    this.Parent.oContained.w_FLCHKLP=trim(this.Parent.oContained.w_FLCHKLP)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLP=="S",1,;
      0)
  endfunc

  add object oDESFAMAI_1_66 as StdField with uid="AOYBYAMKXI",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51284865,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=195, Top=109, InputMask=replicate('X',35)

  add object oDESGRUI_1_67 as StdField with uid="ZABORREMDH",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167611338,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=195, Top=132, InputMask=replicate('X',35)

  add object oDESCATI_1_68 as StdField with uid="ZFWYARIWSP",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202476490,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=195, Top=155, InputMask=replicate('X',35)

  add object oDESFAMAF_1_72 as StdField with uid="WOCDSQXKSA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51284868,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=615, Top=109, InputMask=replicate('X',35)

  add object oDESGRUF_1_73 as StdField with uid="PYGQUHDJMJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100824118,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=615, Top=132, InputMask=replicate('X',35)

  add object oDESCATF_1_74 as StdField with uid="SXSGGSHCII",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 65958966,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=615, Top=155, InputMask=replicate('X',35)

  add object oOUNOMQUE_1_78 as StdField with uid="QUARTVKHNU",rtseq=54,rtrep=.f.,;
    cFormVar = "w_OUNOMQUE", cQueryName = "OUNOMQUE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Query di filtro delle distinte base",;
    HelpContextID = 28980523,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=138, Top=472, InputMask=replicate('X',254)


  add object BTNQRY as cp_askfile with uid="PJDGLKPWNN",left=587, top=472, width=23,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_OUNOMQUE",cExt="VQR",;
    nPag=1;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 73856042

  add object oCOSTILOG_1_84 as StdCheck with uid="AOSDRGZMLX",rtseq=58,rtrep=.f.,left=690, top=201, caption="Attiva log elaborazione",;
    ToolTipText = "Attiva la scrittura del log di elaborazione costi",;
    HelpContextID = 58753427,;
    cFormVar="w_COSTILOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOSTILOG_1_84.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCOSTILOG_1_84.GetRadio()
    this.Parent.oContained.w_COSTILOG = this.RadioValue()
    return .t.
  endfunc

  func oCOSTILOG_1_84.SetRadio()
    this.Parent.oContained.w_COSTILOG=trim(this.Parent.oContained.w_COSTILOG)
    this.value = ;
      iif(this.Parent.oContained.w_COSTILOG=="S",1,;
      0)
  endfunc


  add object oObj_1_87 as cp_outputCombo with uid="ZLERXIVPWT",left=138, top=514, width=473,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103940582


  add object oTipoValoOut_1_90 as StdCombo with uid="NWGKZEMQES",rtseq=60,rtrep=.f.,left=130,top=350,width=242,height=20;
    , ToolTipText = "Criterio di valorizzazione dei materiali di output";
    , HelpContextID = 227269739;
    , cFormVar="w_TipoValoOut",RowSource=""+"Costo standard (dati articolo),"+"Costo standard (inventario),"+"Costo medio ponderato per esercizio,"+"Costo medio ponderato per periodo,"+"Costo ultimo,"+"Listino,"+"Prezzo medio ponderato per esercizio,"+"Prezzo medio ponderato per periodo,"+"Ultimo prezzo,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoValoOut_1_90.RadioValue()
    return(iif(this.value =1,'US',;
    iif(this.value =2,'CS',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'CP',;
    iif(this.value =5,'CU',;
    iif(this.value =6,'LI',;
    iif(this.value =7,'PA',;
    iif(this.value =8,'PP',;
    iif(this.value =9,'UP',;
    iif(this.value =10,"NN",;
    space(1))))))))))))
  endfunc
  func oTipoValoOut_1_90.GetRadio()
    this.Parent.oContained.w_TipoValoOut = this.RadioValue()
    return .t.
  endfunc

  func oTipoValoOut_1_90.SetRadio()
    this.Parent.oContained.w_TipoValoOut=trim(this.Parent.oContained.w_TipoValoOut)
    this.value = ;
      iif(this.Parent.oContained.w_TipoValoOut=='US',1,;
      iif(this.Parent.oContained.w_TipoValoOut=='CS',2,;
      iif(this.Parent.oContained.w_TipoValoOut=='CA',3,;
      iif(this.Parent.oContained.w_TipoValoOut=='CP',4,;
      iif(this.Parent.oContained.w_TipoValoOut=='CU',5,;
      iif(this.Parent.oContained.w_TipoValoOut=='LI',6,;
      iif(this.Parent.oContained.w_TipoValoOut=='PA',7,;
      iif(this.Parent.oContained.w_TipoValoOut=='PP',8,;
      iif(this.Parent.oContained.w_TipoValoOut=='UP',9,;
      iif(this.Parent.oContained.w_TipoValoOut=="NN",10,;
      0))))))))))
  endfunc

  add object oSMATOU_1_92 as StdCheck with uid="BSHLGPISIO",rtseq=61,rtrep=.f.,left=374, top=441, caption="Materiali di output",;
    ToolTipText = "Stampa i costi dei materiali di output",;
    HelpContextID = 169976538,;
    cFormVar="w_SMATOU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSMATOU_1_92.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSMATOU_1_92.GetRadio()
    this.Parent.oContained.w_SMATOU = this.RadioValue()
    return .t.
  endfunc

  func oSMATOU_1_92.SetRadio()
    this.Parent.oContained.w_SMATOU=trim(this.Parent.oContained.w_SMATOU)
    this.value = ;
      iif(this.Parent.oContained.w_SMATOU=="S",1,;
      0)
  endfunc

  add object oESERCMO_1_95 as StdField with uid="UQEJHJLUJV",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ESERCMO", cQueryName = "ESERCMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 48455098,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=130, Top=374, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCMO"

  func oESERCMO_1_95.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  func oESERCMO_1_95.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_NUMINVMO)
        bRes2=.link_1_96('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNUMINVMO_1_96 as StdField with uid="EZVRCZLAGN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_NUMINVMO", cQueryName = "NUMINVMO",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali di output",;
    HelpContextID = 154917595,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=297, Top=374, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERCMO", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINVMO"

  func oNUMINVMO_1_96.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  func oNUMINVMO_1_96.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_96('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINVMO_1_96.ecpDrop(oSource)
    this.Parent.oContained.link_1_96('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINVMO_1_96.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERCMO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERCMO)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINVMO_1_96'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINVMO_1_96.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERCMO
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINVMO
     i_obj.ecpSave()
  endproc

  add object oListinoMo_1_98 as StdField with uid="YFTLERWREL",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ListinoMo", cQueryName = "ListinoMo",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali di output",;
    HelpContextID = 10592243,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=398, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ListinoMo"

  func oListinoMo_1_98.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  func oListinoMo_1_98.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_98('Part',this)
    endwith
    return bRes
  endfunc

  proc oListinoMo_1_98.ecpDrop(oSource)
    this.Parent.oContained.link_1_98('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oListinoMo_1_98.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oListinoMo_1_98'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica listini",'',this.parent.oContained
  endproc

  add object oDESLISMO_1_100 as StdField with uid="KOEYJNGEOP",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESLISMO", cQueryName = "DESLISMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210275195,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=192, Top=398, InputMask=replicate('X',40)

  func oDESLISMO_1_100.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  add object oDATINVMO_1_101 as StdField with uid="AQZCXIFWVE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DATINVMO", cQueryName = "DATINVMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 154894203,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=402, Top=374

  func oDATINVMO_1_101.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc


  add object oINESCONT_1_107 as StdCombo with uid="LNVAXODUBN",rtseq=69,rtrep=.f.,left=498,top=271,width=156,height=21;
    , ToolTipText = "Metodo di calcolo del costo e prezzo medio esercizio";
    , HelpContextID = 14836262;
    , cFormVar="w_INESCONT",RowSource=""+"Standard,"+"Continuo,"+"Per movimento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINESCONT_1_107.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oINESCONT_1_107.GetRadio()
    this.Parent.oContained.w_INESCONT = this.RadioValue()
    return .t.
  endfunc

  func oINESCONT_1_107.SetRadio()
    this.Parent.oContained.w_INESCONT=trim(this.Parent.oContained.w_INESCONT)
    this.value = ;
      iif(this.Parent.oContained.w_INESCONT=='N',1,;
      iif(this.Parent.oContained.w_INESCONT=='S',2,;
      iif(this.Parent.oContained.w_INESCONT=='M',3,;
      0)))
  endfunc

  func oINESCONT_1_107.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_NUMINV))
    endwith
   endif
  endfunc

  func oINESCONT_1_107.mHide()
    with this.Parent.oContained
      return (.w_Analisi="N" and .w_TipoValo <> "M")
    endwith
  endfunc

  add object oDESINI_1_114 as StdField with uid="YJQMLZHVQA",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104565706,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=286, Top=12, InputMask=replicate('X',40)

  add object oDESFIN_1_115 as StdField with uid="DESEQAHWJU",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26119114,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=286, Top=36, InputMask=replicate('X',40)

  add object oFLCOSINV_1_118 as StdCheck with uid="FLKVCBFKWG",rtseq=77,rtrep=.f.,left=690, top=270, caption="Applica costi inventario",;
    HelpContextID = 98993236,;
    cFormVar="w_FLCOSINV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCOSINV_1_118.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCOSINV_1_118.GetRadio()
    this.Parent.oContained.w_FLCOSINV = this.RadioValue()
    return .t.
  endfunc

  func oFLCOSINV_1_118.SetRadio()
    this.Parent.oContained.w_FLCOSINV=trim(this.Parent.oContained.w_FLCOSINV)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOSINV=="S",1,;
      0)
  endfunc

  func oFLCOSINV_1_118.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ 'SMUP')
    endwith
   endif
  endfunc

  func oFLCOSINV_1_118.mHide()
    with this.Parent.oContained
      return (! .w_TipoValo $ 'SMUP')
    endwith
  endfunc

  add object oCODMAG_1_121 as StdField with uid="XMPGNCFZCF",rtseq=78,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli da verificare (spazio=verifica per tutti i magazzini)",;
    HelpContextID = 151548378,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=296, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_121.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo = 'A')
    endwith
   endif
  endfunc

  func oCODMAG_1_121.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  func oCODMAG_1_121.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_121('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_121.ecpDrop(oSource)
    this.Parent.oContained.link_1_121('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_121.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_121'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_121.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_123 as StdField with uid="IGDPZQPCGC",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 151489482,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=192, Top=296, InputMask=replicate('X',30)

  func oDESMAG_1_123.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  add object oCAOLIS_1_124 as StdField with uid="IQCVTLZTCN",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CAOLIS", cQueryName = "CAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 210292698,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=559, Top=296

  func oCAOLIS_1_124.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_TipoValo="L" AND NOT EMPTY(.w_Listino))
    endwith
   endif
  endfunc

  func oCAOLIS_1_124.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oCAOLISMO_1_127 as StdField with uid="MOTUECDKNJ",rtseq=82,rtrep=.f.,;
    cFormVar = "w_CAOLISMO", cQueryName = "CAOLISMO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 210292619,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=559, Top=398

  func oCAOLISMO_1_127.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVALMO=0 AND .w_TipoValoOut="L" AND NOT EMPTY(.w_ListinoMo))
    endwith
   endif
  endfunc

  func oCAOLISMO_1_127.mHide()
    with this.Parent.oContained
      return (.w_CAOVALMO<>0 OR .w_TipoValoOut<>"L" OR EMPTY(.w_ListinoMo))
    endwith
  endfunc

  add object oTIPCOS_1_140 as StdCheck with uid="VERNBIWUUK",rtseq=93,rtrep=.f.,left=690, top=296, caption="Costificazione parziale",;
    ToolTipText = "Se attivo: la costificazione avverr� in funzione del check esplosione impostato in distinta",;
    HelpContextID = 204584650,;
    cFormVar="w_TIPCOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCOS_1_140.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCOS_1_140.GetRadio()
    this.Parent.oContained.w_TIPCOS = this.RadioValue()
    return .t.
  endfunc

  func oTIPCOS_1_140.SetRadio()
    this.Parent.oContained.w_TIPCOS=trim(this.Parent.oContained.w_TIPCOS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCOS=='S',1,;
      0)
  endfunc

  add object oPDCOECOS_1_142 as StdCheck with uid="WHCLBVISGJ",rtseq=95,rtrep=.f.,left=690, top=322, caption="Considera coeff. impiego",;
    ToolTipText = "Considera coeff. impiego in fase di costificazione",;
    HelpContextID = 214338487,;
    cFormVar="w_PDCOECOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCOECOS_1_142.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPDCOECOS_1_142.GetRadio()
    this.Parent.oContained.w_PDCOECOS = this.RadioValue()
    return .t.
  endfunc

  func oPDCOECOS_1_142.SetRadio()
    this.Parent.oContained.w_PDCOECOS=trim(this.Parent.oContained.w_PDCOECOS)
    this.value = ;
      iif(this.Parent.oContained.w_PDCOECOS=='S',1,;
      0)
  endfunc

  add object oStr_1_19 as StdString with uid="VMACKHOGGO",Visible=.t., Left=402, Top=62,;
    Alignment=1, Width=99, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RUZSSABKYI",Visible=.t., Left=26, Top=183,;
    Alignment=0, Width=175, Height=15,;
    Caption="Stato distinte"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="DIWVCYAPZU",Visible=.t., Left=26, Top=87,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="YHCPSSENNQ",Visible=.t., Left=26, Top=422,;
    Alignment=0, Width=175, Height=15,;
    Caption="Opzioni di visualizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="DDLAKBXANU",Visible=.t., Left=26, Top=62,;
    Alignment=1, Width=99, Height=15,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FRXPGPJUNO",Visible=.t., Left=18, Top=248,;
    Alignment=1, Width=108, Height=18,;
    Caption="Materiali d'acquisto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="WNERIRPNMR",Visible=.t., Left=51, Top=298,;
    Alignment=1, Width=75, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="XROSXMIRDD",Visible=.t., Left=51, Top=272,;
    Alignment=1, Width=75, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "SMEPCU") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="CMQQHAIKKN",Visible=.t., Left=176, Top=272,;
    Alignment=1, Width=119, Height=18,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "SMEPCU") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="ZFXOXTIOFY",Visible=.t., Left=368, Top=272,;
    Alignment=1, Width=31, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "SMEPCU") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="MMIWXNCEGX",Visible=.t., Left=342, Top=180,;
    Alignment=0, Width=175, Height=15,;
    Caption="Opzioni di elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="SJOQRQEYXN",Visible=.t., Left=164, Top=180,;
    Alignment=0, Width=84, Height=15,;
    Caption="Controllo"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=19, Top=112,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=19, Top=132,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=19, Top=155,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="BETNLUNOYI",Visible=.t., Left=478, Top=112,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=453, Top=132,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=456, Top=155,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="BQPXSTBRUO",Visible=.t., Left=25, Top=475,;
    Alignment=1, Width=109, Height=18,;
    Caption="Query"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="VUNLBHDDVA",Visible=.t., Left=607, Top=62,;
    Alignment=1, Width=74, Height=18,;
    Caption="Lavorazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="CPUIYIXXTN",Visible=.t., Left=33, Top=517,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="ITSFXNBZJR",Visible=.t., Left=26, Top=494,;
    Alignment=0, Width=135, Height=15,;
    Caption="Selezione stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="JCXGBEPVYI",Visible=.t., Left=3, Top=352,;
    Alignment=1, Width=126, Height=18,;
    Caption="Materiali di output:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="KMIOAFJKJL",Visible=.t., Left=7, Top=221,;
    Alignment=0, Width=224, Height=17,;
    Caption="Criteri di valorizzazione dei materiali"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="PRFSUKMZOZ",Visible=.t., Left=52, Top=401,;
    Alignment=1, Width=75, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="EXEPDHPGVR",Visible=.t., Left=54, Top=376,;
    Alignment=1, Width=75, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="PIYYUNCMCZ",Visible=.t., Left=176, Top=376,;
    Alignment=1, Width=119, Height=18,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="UNQANDMSCL",Visible=.t., Left=368, Top=376,;
    Alignment=1, Width=31, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="ZEKEHZGBJR",Visible=.t., Left=23, Top=443,;
    Alignment=1, Width=54, Height=18,;
    Caption="Analisi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_109 as StdString with uid="PGPCUPGELN",Visible=.t., Left=431, Top=248,;
    Alignment=0, Width=223, Height=17,;
    Caption="Opzioni costo medio ponderato esercizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="AZAYVNOACS",Visible=.t., Left=11, Top=15,;
    Alignment=1, Width=114, Height=15,;
    Caption="Da distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="YBUTHVTDPZ",Visible=.t., Left=26, Top=39,;
    Alignment=1, Width=99, Height=15,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_120 as StdString with uid="PVPNQOXTDO",Visible=.t., Left=7, Top=327,;
    Alignment=0, Width=281, Height=17,;
    Caption="Criteri di valorizzazione dei materiali di output"  ;
  , bGlobalFont=.t.

  add object oStr_1_122 as StdString with uid="QCEJHAMSPS",Visible=.t., Left=31, Top=298,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_122.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="MACUDGSSKF",Visible=.t., Left=485, Top=298,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="OOPLVOKRTK",Visible=.t., Left=485, Top=401,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (.w_CAOVALMO<>0 OR .w_TipoValoOut<>"L" OR EMPTY(.w_ListinoMo))
    endwith
  endfunc

  add object oBox_1_23 as StdBox with uid="ROUJTFLULJ",left=15, top=436, width=636,height=1

  add object oBox_1_25 as StdBox with uid="DTCUEIXICO",left=5, top=237, width=851,height=1

  add object oBox_1_61 as StdBox with uid="OXKAOEXVLS",left=19, top=197, width=99,height=1

  add object oBox_1_65 as StdBox with uid="ZXUPTCHTYW",left=148, top=197, width=148,height=1

  add object oBox_1_88 as StdBox with uid="HZZUJLZEMD",left=19, top=508, width=879,height=1

  add object oBox_1_104 as StdBox with uid="GBWYHTNJGP",left=340, top=197, width=252,height=1

  add object oBox_1_119 as StdBox with uid="XLBDKOLDSZ",left=5, top=343, width=851,height=1

  add object oBox_1_129 as StdBox with uid="UQVSBCIIVP",left=431, top=264, width=223,height=2
enddefine
define class tgsci_sdcPag2 as StdContainer
  Width  = 920
  height = 560
  stdWidth  = 920
  stdheight = 560
  resizeXpos=491
  resizeYpos=321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SelDis as cp_szoombox with uid="SHVQPKONZP",left=-1, top=5, width=920,height=506,;
    caption='Browse',;
   bGlobalFont=.t.,;
    cTable="DISMBASE",cZoomFile="GSDSSKCS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 138699030


  add object oBtn_2_2 as StdButton with uid="VIVRYPRXRO",left=866, top=514, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 16888854;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_3 as StdRadio with uid="PGKTVHMOKS",rtseq=55,rtrep=.f.,left=3, top=511, width=139,height=50;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 92273370
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 92273370
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 92273370
      this.Buttons(3).Top=32
      this.SetAll("Width",137)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'Z',;
    space(1)))))
  endfunc
  func oSELEZI_2_3.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_3.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      iif(this.Parent.oContained.w_SELEZI=='Z',3,;
      0)))
  endfunc


  add object oBtn_2_5 as StdButton with uid="ZYQNADMBYV",left=811, top=514, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 16888854;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSDS1BDC(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_sdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
