* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bcf                                                        *
*              Codifica fasi                                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2015-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bcf",oParentObject,m.Operazione)
return(i_retval)

define class tgsci_bcf as StdBatch
  * --- Local variables
  Operazione = space(15)
  GSCI_ACF = .NULL.
  GSCI_MDF = .NULL.
  GSCI_MIN = .NULL.
  ROWORD = 0
  w_cFunction = space(10)
  w_MSG = space(10)
  w_TIPOARTI = space(2)
  w_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_FAMINI = space(5)
  w_FAMFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_PIAINI = space(5)
  w_PIAFIN = space(5)
  w_FAPINI = space(5)
  w_FAPFIN = space(5)
  w_CODINI = space(20)
  w_CODFIN = space(41)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione di GSCI_ACF - GSCI_MIN
    * --- Filtri per query GSCI_BCF
    this.Operazione = alltrim(this.Operazione)
    if this.Operazione="MATINP"
      this.GSCI_ACF = this.oparentobject
      this.GSCI_MDF = this.GSCI_ACF.GSCI_MDF
      this.GSCI_MIN = this.GSCI_MDF.GSCI_MIN
      if this.oParentObject.w_CFMATINP="S"
        this.GSCI_MDF.w_MATINP = "S"
        this.GSCI_MDF.oPgFrm.Pages(2).enabled = .t.
      else
        this.GSCI_MDF.w_MATINP = "N"
        this.GSCI_MDF.oPgFrm.Pages(2).enabled = .f.
      endif
      this.w_TIPOARTI = this.oParentObject.w_CFTIPART
      this.w_ARTINI = this.oParentObject.w_COCODART
      this.w_ARTFIN = this.oParentObject.w_COCODAR1
      this.w_CODINI = this.oParentObject.w_CFCODINI
      this.w_CODFIN = this.oParentObject.w_CFCODFIN
      this.w_FAMINI = this.oParentObject.w_CFFAMAIN
      this.w_FAMFIN = this.oParentObject.w_CFFAMAFI
      this.w_GRUINI = this.oParentObject.w_CFGRUINI
      this.w_GRUFIN = this.oParentObject.w_CFGRUFIN
      this.w_CATINI = this.oParentObject.w_CFCATINI
      this.w_CATFIN = this.oParentObject.w_CFCATFIN
      this.w_MAGINI = this.oParentObject.w_CFMAGINI
      this.w_MAGFIN = this.oParentObject.w_CFMAGFIN
      * --- Riferimento al cursore transitorio dei materiali di input
      Private nc 
 nc = this.GSCI_MIN.cTrsName
    else
      this.GSCI_MIN = this.oparentobject
    endif
    do case
      case this.Operazione="MATINP"
        * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
        SELECT (nc)
        zap
        if this.oParentObject.w_CFMATINP="S"
          this.ROWORD = 0
          vq_exec("..\PRFA\EXE\QUERY\GSCI_BCF", this, "_MatInp_")
          * --- Nuova Riga del Temporaneo
          this.GSCI_MIN.InitRow()     
          if RecCount("_MatInp_")>0
            Select _MatInp_ 
 go top 
 scan
            if this.ROWORD>0
              * --- Nuova Riga del Temporaneo
              this.GSCI_MIN.InitRow()     
            endif
            * --- Valorizza Variabili di Riga ...
            this.ROWORD = this.ROWORD+1
            this.GSCI_MIN.w_CPROWORD = this.ROWORD*10
            this.GSCI_MIN.w_DIRIGSEL = "S"
            this.GSCI_MIN.w_DIDIBCOD = _MatInp_.COCODCOM
            this.GSCI_MIN.w_DESCOD = _MatInp_.ARDESART
            * --- Carica il Temporaneo dei Dati e skippa al record successivo
            this.GSCI_MIN.SaveRow()     
            ENDSCAN
          endif
          * --- Rinfrescamenti vari
          SELECT (nc) 
 GO TOP 
 With this.GSCI_MIN 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
          * --- Messaggio Finale
          ah_ErrorMsg("Caricamento materiali di input terminato",48,"")
        endif
      case this.Operazione="SS"
        * --- --Selezione Deselezione
        this.GSCI_MIN.MarkPos()     
        this.GSCI_MIN.FirstRow()     
        do while not this.GSCI_MIN.Eof_Trs()
          this.GSCI_MIN.SetRow()     
          if this.GSCI_MIN.FullRow()
            do case
              case this.oParentObject.w_SELEZI="S"
                * --- Seleziona tutte le righe dello zoom
                this.GSCI_MIN.w_DIRIGSEL = "S"
              case this.oParentObject.w_SELEZI="D"
                this.GSCI_MIN.w_DIRIGSEL = " "
              otherwise
                * --- Inverte Selezione
                this.GSCI_MIN.w_DIRIGSEL = iif(this.GSCI_MIN.w_DIRIGSEL="S"," ","S")
            endcase
            this.GSCI_MIN.SaveRow()     
          endif
          this.GSCI_MIN.NextRow()     
        enddo
        this.GSCI_MIN.RePos(.t.)     
      case this.Operazione="ChkFase"
        this.GSCI_ACF = this.oparentobject.oparentobject
        this.w_cFunction = Upper(Alltrim(this.GSCI_ACF.cFunction))
        if this.w_cFunction $ "LOAD-EDIT" and this.GSCI_MIN.bupdated
          * --- Caso di inserimento ordine
          if !Empty(this.oParentObject.w_DFCODICE)
            * --- Controlla le Risorse delle fasi
            * --- Select from GSCIQBCF
            do vq_exec with 'GSCIQBCF',this,'_Curs_GSCIQBCF','',.f.,.t.
            if used('_Curs_GSCIQBCF')
              select _Curs_GSCIQBCF
              locate for 1=1
              do while not(eof())
              this.w_MSG = ah_msgformat("E' necessario inserire un solo centro di lavoro")
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MSG
              i_retcode = 'stop'
              return
                select _Curs_GSCIQBCF
                continue
              enddo
              use
            endif
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,Operazione)
    this.Operazione=Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSCIQBCF')
      use in _Curs_GSCIQBCF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Operazione"
endproc
