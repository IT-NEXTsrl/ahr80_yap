* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_ard                                                        *
*              Risorsa descrittiva                                             *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-29                                                      *
* Last revis.: 2015-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_ard"))

* --- Class definition
define class tgsci_ard as StdForm
  Top    = 8
  Left   = 8

  * --- Standard Properties
  Width  = 570
  Height = 213+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-15"
  HelpContextID=126485865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  RIS_ORSE_IDX = 0
  CENCOST_IDX = 0
  TAB_CALE_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  LIS_MACC_IDX = 0
  CES_PITI_IDX = 0
  TIP_RISO_IDX = 0
  UNIMIS_IDX = 0
  cFile = "RIS_ORSE"
  cKeySelect = "RL__TIPO,RLCODICE"
  cQueryFilter="RL__TIPO='RD'"
  cKeyWhere  = "RL__TIPO=this.w_RL__TIPO and RLCODICE=this.w_RLCODICE"
  cKeyWhereODBC = '"RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cKeyWhereODBCqualified = '"RIS_ORSE.RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RIS_ORSE.RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cPrg = "gsci_ard"
  cComment = "Risorsa descrittiva"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RL__TIPO = space(2)
  w_UMFLTEMP = space(1)
  w_RLCODICE = space(20)
  w_RLDESCRI = space(40)
  w_RL__NOTE = space(0)
  w_RLQTARIS = 0
  w_RLUMTDEF = space(3)
  w_DTEMDEF = space(35)
  w_RLCODCES = space(20)
  w_DESCES = space(40)
  w_DURSEC = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIS_ORSE','gsci_ard')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_ardPag1","gsci_ard",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 119103434
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRLCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='TAB_CALE'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='LIS_MACC'
    this.cWorkTables[6]='CES_PITI'
    this.cWorkTables[7]='TIP_RISO'
    this.cWorkTables[8]='UNIMIS'
    this.cWorkTables[9]='RIS_ORSE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_ORSE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RL__TIPO = NVL(RL__TIPO,space(2))
      .w_RLCODICE = NVL(RLCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIS_ORSE where RL__TIPO=KeySet.RL__TIPO
    *                            and RLCODICE=KeySet.RLCODICE
    *
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_ORSE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_ORSE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIS_ORSE '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UMFLTEMP = 'S'
        .w_DTEMDEF = space(35)
        .w_DESCES = space(40)
        .w_DURSEC = 0
        .w_RL__TIPO = NVL(RL__TIPO,space(2))
        .w_RLCODICE = NVL(RLCODICE,space(20))
        .w_RLDESCRI = NVL(RLDESCRI,space(40))
        .w_RL__NOTE = NVL(RL__NOTE,space(0))
        .w_RLQTARIS = NVL(RLQTARIS,0)
        .w_RLUMTDEF = NVL(RLUMTDEF,space(3))
          .link_1_10('Load')
        .w_RLCODCES = NVL(RLCODCES,space(20))
          if link_1_14_joined
            this.w_RLCODCES = NVL(CECODICE114,NVL(this.w_RLCODCES,space(20)))
            this.w_DESCES = NVL(CEDESCRI114,space(40))
          else
          .link_1_14('Load')
          endif
        cp_LoadRecExtFlds(this,'RIS_ORSE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RL__TIPO = space(2)
      .w_UMFLTEMP = space(1)
      .w_RLCODICE = space(20)
      .w_RLDESCRI = space(40)
      .w_RL__NOTE = space(0)
      .w_RLQTARIS = 0
      .w_RLUMTDEF = space(3)
      .w_DTEMDEF = space(35)
      .w_RLCODCES = space(20)
      .w_DESCES = space(40)
      .w_DURSEC = 0
      if .cFunction<>"Filter"
        .w_RL__TIPO = 'RD'
        .w_UMFLTEMP = 'S'
          .DoRTCalc(3,5,.f.)
        .w_RLQTARIS = 1
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_RLUMTDEF))
          .link_1_10('Full')
          endif
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_RLCODCES))
          .link_1_14('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_ORSE')
    this.DoRTCalc(10,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRLCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oRLDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oRL__NOTE_1_6.enabled = i_bVal
      .Page1.oPag.oRLQTARIS_1_8.enabled = i_bVal
      .Page1.oPag.oRLUMTDEF_1_10.enabled = i_bVal
      .Page1.oPag.oRLCODCES_1_14.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRLCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRLCODICE_1_3.enabled = .t.
        .Page1.oPag.oRLDESCRI_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIS_ORSE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__TIPO,"RL__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODICE,"RLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLDESCRI,"RLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__NOTE,"RL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLQTARIS,"RLQTARIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLUMTDEF,"RLUMTDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODCES,"RLCODCES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    i_lTable = "RIS_ORSE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SRD with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIS_ORSE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIS_ORSE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_ORSE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLQTARIS"+;
                  ",RLUMTDEF,RLCODCES "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RL__TIPO)+;
                  ","+cp_ToStrODBC(this.w_RLCODICE)+;
                  ","+cp_ToStrODBC(this.w_RLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RL__NOTE)+;
                  ","+cp_ToStrODBC(this.w_RLQTARIS)+;
                  ","+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
                  ","+cp_ToStrODBCNull(this.w_RLCODCES)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_ORSE')
        cp_CheckDeletedKey(i_cTable,0,'RL__TIPO',this.w_RL__TIPO,'RLCODICE',this.w_RLCODICE)
        INSERT INTO (i_cTable);
              (RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLQTARIS,RLUMTDEF,RLCODCES  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RL__TIPO;
                  ,this.w_RLCODICE;
                  ,this.w_RLDESCRI;
                  ,this.w_RL__NOTE;
                  ,this.w_RLQTARIS;
                  ,this.w_RLUMTDEF;
                  ,this.w_RLCODCES;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIS_ORSE_IDX,i_nConn)
      *
      * update RIS_ORSE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_ORSE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RLDESCRI="+cp_ToStrODBC(this.w_RLDESCRI)+;
             ",RL__NOTE="+cp_ToStrODBC(this.w_RL__NOTE)+;
             ",RLQTARIS="+cp_ToStrODBC(this.w_RLQTARIS)+;
             ",RLUMTDEF="+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
             ",RLCODCES="+cp_ToStrODBCNull(this.w_RLCODCES)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_ORSE')
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        UPDATE (i_cTable) SET;
              RLDESCRI=this.w_RLDESCRI;
             ,RL__NOTE=this.w_RL__NOTE;
             ,RLQTARIS=this.w_RLQTARIS;
             ,RLUMTDEF=this.w_RLUMTDEF;
             ,RLCODCES=this.w_RLCODCES;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIS_ORSE_IDX,i_nConn)
      *
      * delete RIS_ORSE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RLUMTDEF
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLUMTDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RLUMTDEF)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_RLUMTDEF))
          select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLUMTDEF)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLUMTDEF) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oRLUMTDEF_1_10'),i_cWhere,'',"",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLUMTDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RLUMTDEF);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RLUMTDEF)
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLUMTDEF = NVL(_Link_.UMCODICE,space(3))
      this.w_DTEMDEF = NVL(_Link_.UMDESCRI,space(35))
      this.w_DURSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLUMTDEF = space(3)
      endif
      this.w_DTEMDEF = space(35)
      this.w_DURSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLUMTDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCODCES
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_RLCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_RLCODCES))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oRLCODCES_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_RLCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_RLCODCES)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCES = space(20)
      endif
      this.w_DESCES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CECODICE as CECODICE114"+ ",link_1_14.CEDESCRI as CEDESCRI114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on RIS_ORSE.RLCODCES=link_1_14.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCODCES=link_1_14.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRLCODICE_1_3.value==this.w_RLCODICE)
      this.oPgFrm.Page1.oPag.oRLCODICE_1_3.value=this.w_RLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_5.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_5.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRL__NOTE_1_6.value==this.w_RL__NOTE)
      this.oPgFrm.Page1.oPag.oRL__NOTE_1_6.value=this.w_RL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oRLQTARIS_1_8.value==this.w_RLQTARIS)
      this.oPgFrm.Page1.oPag.oRLQTARIS_1_8.value=this.w_RLQTARIS
    endif
    if not(this.oPgFrm.Page1.oPag.oRLUMTDEF_1_10.value==this.w_RLUMTDEF)
      this.oPgFrm.Page1.oPag.oRLUMTDEF_1_10.value=this.w_RLUMTDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDTEMDEF_1_11.value==this.w_DTEMDEF)
      this.oPgFrm.Page1.oPag.oDTEMDEF_1_11.value=this.w_DTEMDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCODCES_1_14.value==this.w_RLCODCES)
      this.oPgFrm.Page1.oPag.oRLCODCES_1_14.value=this.w_RLCODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_16.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_16.value=this.w_DESCES
    endif
    cp_SetControlsValueExtFlds(this,'RIS_ORSE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RLCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_RLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RLQTARIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLQTARIS_1_8.SetFocus()
            i_bnoObbl = !empty(.w_RLQTARIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RLUMTDEF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLUMTDEF_1_10.SetFocus()
            i_bnoObbl = !empty(.w_RLUMTDEF)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsci_ardPag1 as StdContainer
  Width  = 566
  height = 213
  stdWidth  = 566
  stdheight = 213
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRLCODICE_1_3 as StdField with uid="CKXPTVGGNZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RLCODICE", cQueryName = "RL__TIPO,RLCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice risorsa / componente",;
    HelpContextID = 101284955,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=84, Top=12, InputMask=replicate('X',20)

  add object oRLDESCRI_1_5 as StdField with uid="QCPXFWUKUW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve",;
    HelpContextID = 15699039,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=256, Top=12, InputMask=replicate('X',40)

  add object oRL__NOTE_1_6 as StdMemo with uid="HAIGSVUWZV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RL__NOTE", cQueryName = "RL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione estesa",;
    HelpContextID = 213597275,;
   bGlobalFont=.t.,;
    Height=49, Width=477, Left=84, Top=37

  add object oRLQTARIS_1_8 as StdField with uid="CNIWJBHUKF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RLQTARIS", cQueryName = "RLQTARIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero elementi",;
    HelpContextID = 18916247,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=119, Top=114, cSayPict='"@Z 99,999"', cGetPict='"99,999"'

  add object oRLUMTDEF_1_10 as StdField with uid="EOSEIGGIPR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RLUMTDEF", cQueryName = "RLUMTDEF",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura di default (proposta in gestione cicli)",;
    HelpContextID = 34118748,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=136, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_RLUMTDEF"

  func oRLUMTDEF_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLUMTDEF_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLUMTDEF_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oRLUMTDEF_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oDTEMDEF_1_11 as StdField with uid="IYJUZODFVK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DTEMDEF", cQueryName = "DTEMDEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34054966,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=184, Top=136, InputMask=replicate('X',35)

  add object oRLCODCES_1_14 as StdField with uid="DVAVGRCFZO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_RLCODCES", cQueryName = "RLCODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice cespite",;
    HelpContextID = 621673,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=119, Top=188, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", oKey_1_1="CECODICE", oKey_1_2="this.w_RLCODCES"

  func oRLCODCES_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODCES_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCODCES_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oRLCODCES_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCES_1_16 as StdField with uid="KHPDFDJTWR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 947254,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=288, Top=188, InputMask=replicate('X',40)

  add object oStr_1_4 as StdString with uid="ZYEECUGVBT",Visible=.t., Left=25, Top=12,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="VEXQQSXVOZ",Visible=.t., Left=12, Top=114,;
    Alignment=1, Width=103, Height=15,;
    Caption="Numero elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="TIWSQYLGUR",Visible=.t., Left=11, Top=90,;
    Alignment=0, Width=80, Height=15,;
    Caption="Gestionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="NIWQCDZKNA",Visible=.t., Left=20, Top=136,;
    Alignment=1, Width=95, Height=15,;
    Caption="UM tempo pref.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="HZXTUZZRCZ",Visible=.t., Left=24, Top=188,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WAQXORODBQ",Visible=.t., Left=11, Top=162,;
    Alignment=0, Width=88, Height=15,;
    Caption="Cespiti"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="IGMYKGJNDA",left=7, top=106, width=553,height=1

  add object oBox_1_17 as StdBox with uid="DORFZOVTNO",left=7, top=178, width=553,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="RL__TIPO='RD'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsci_ard','RIS_ORSE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName2+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
