* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_aat                                                        *
*              Attrezzature                                                    *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-29                                                      *
* Last revis.: 2015-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_aat"))

* --- Class definition
define class tgsci_aat as StdForm
  Top    = 12
  Left   = 22

  * --- Standard Properties
  Width  = 623
  Height = 314+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-27"
  HelpContextID=143263081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  RIS_ORSE_IDX = 0
  CENCOST_IDX = 0
  TAB_CALE_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  LIS_MACC_IDX = 0
  UNIMIS_IDX = 0
  PAR_PROD_IDX = 0
  CES_PITI_IDX = 0
  CAT_RISO_IDX = 0
  TIP_RISO_IDX = 0
  cFile = "RIS_ORSE"
  cKeySelect = "RL__TIPO,RLCODICE"
  cQueryFilter="RL__TIPO='AT'"
  cKeyWhere  = "RL__TIPO=this.w_RL__TIPO and RLCODICE=this.w_RLCODICE"
  cKeyWhereODBC = '"RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cKeyWhereODBCqualified = '"RIS_ORSE.RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RIS_ORSE.RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cPrg = "gsci_aat"
  cComment = "Attrezzature"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PPCODICE = space(2)
  w_UMFLTEMP = space(1)
  w_RL__TIPO = space(2)
  w_RLCODICE = space(20)
  w_RLDESCRI = space(40)
  w_RL__NOTE = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ERECAL = space(5)
  w_DERCAL = space(40)
  w_RLCODCAT = space(5)
  w_RLUMTDEF = space(3)
  w_DTEMDEF = space(40)
  w_RLCENCOS = space(15)
  w_DESCCO = space(40)
  w_RLCODCES = space(20)
  w_DESCES = space(40)
  w_DESCAT = space(40)
  w_RLQTARIS = 0
  w_DURSEC = 0

  * --- Children pointers
  GSCI_MLM = .NULL.
  GSCI_MDR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIS_ORSE','gsci_aat')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_aatPag1","gsci_aat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 135880650
      .Pages(2).addobject("oPag","tgsci_aatPag2","gsci_aat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Disponibilit�")
      .Pages(2).HelpContextID = 78079192
      .Pages(3).addobject("oPag","tgsci_aatPag3","gsci_aat",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Macchine")
      .Pages(3).HelpContextID = 144731861
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRLCODICE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='TAB_CALE'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='LIS_MACC'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='PAR_PROD'
    this.cWorkTables[8]='CES_PITI'
    this.cWorkTables[9]='CAT_RISO'
    this.cWorkTables[10]='TIP_RISO'
    this.cWorkTables[11]='RIS_ORSE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_ORSE_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MLM = CREATEOBJECT('stdDynamicChild',this,'GSCI_MLM',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSCI_MDR = CREATEOBJECT('stdDynamicChild',this,'GSCI_MDR',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MLM)
      this.GSCI_MLM.DestroyChildrenChain()
      this.GSCI_MLM=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSCI_MDR)
      this.GSCI_MDR.DestroyChildrenChain()
      this.GSCI_MDR=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MLM.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCI_MDR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MLM.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCI_MDR.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MLM.NewDocument()
    this.GSCI_MDR.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCI_MLM.SetKey(;
            .w_RLCODICE,"LMCODATT";
            ,.w_RL__TIPO,"LMTIPATT";
            )
      this.GSCI_MDR.SetKey(;
            .w_RLCODICE,"DRCODRIS";
            ,.w_RL__TIPO,"DRTIPRIS";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCI_MLM.ChangeRow(this.cRowID+'      1',1;
             ,.w_RLCODICE,"LMCODATT";
             ,.w_RL__TIPO,"LMTIPATT";
             )
      .GSCI_MDR.ChangeRow(this.cRowID+'      1',1;
             ,.w_RLCODICE,"DRCODRIS";
             ,.w_RL__TIPO,"DRTIPRIS";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCI_MLM)
        i_f=.GSCI_MLM.BuildFilter()
        if !(i_f==.GSCI_MLM.cQueryFilter)
          i_fnidx=.GSCI_MLM.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MLM.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MLM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MLM.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MLM.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCI_MDR)
        i_f=.GSCI_MDR.BuildFilter()
        if !(i_f==.GSCI_MDR.cQueryFilter)
          i_fnidx=.GSCI_MDR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MDR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MDR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MDR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MDR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RL__TIPO = NVL(RL__TIPO,space(2))
      .w_RLCODICE = NVL(RLCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIS_ORSE where RL__TIPO=KeySet.RL__TIPO
    *                            and RLCODICE=KeySet.RLCODICE
    *
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_ORSE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_ORSE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIS_ORSE '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PPCODICE = 'PP'
        .w_UMFLTEMP = 'S'
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_ERECAL = space(5)
        .w_DERCAL = space(40)
        .w_DTEMDEF = space(40)
        .w_DESCCO = space(40)
        .w_DESCES = space(40)
        .w_DESCAT = space(40)
        .w_DURSEC = 0
          .link_1_1('Load')
        .w_RL__TIPO = NVL(RL__TIPO,space(2))
        .w_RLCODICE = NVL(RLCODICE,space(20))
        .w_RLDESCRI = NVL(RLDESCRI,space(40))
        .w_RL__NOTE = NVL(RL__NOTE,space(0))
          .link_1_12('Load')
        .w_RLCODCAT = NVL(RLCODCAT,space(5))
          if link_1_14_joined
            this.w_RLCODCAT = NVL(CRCODCAT114,NVL(this.w_RLCODCAT,space(5)))
            this.w_DESCAT = NVL(CRDESCAT114,space(40))
          else
          .link_1_14('Load')
          endif
        .w_RLUMTDEF = NVL(RLUMTDEF,space(3))
          .link_1_15('Load')
        .w_RLCENCOS = NVL(RLCENCOS,space(15))
          if link_1_19_joined
            this.w_RLCENCOS = NVL(CC_CONTO119,NVL(this.w_RLCENCOS,space(15)))
            this.w_DESCCO = NVL(CCDESPIA119,space(40))
          else
          .link_1_19('Load')
          endif
        .w_RLCODCES = NVL(RLCODCES,space(20))
          if link_1_22_joined
            this.w_RLCODCES = NVL(CECODICE122,NVL(this.w_RLCODCES,space(20)))
            this.w_DESCES = NVL(CEDESCRI122,space(40))
          else
          .link_1_22('Load')
          endif
        .w_RLQTARIS = NVL(RLQTARIS,0)
        cp_LoadRecExtFlds(this,'RIS_ORSE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsci_aat
    if this.w_RL__TIPO <> 'AT'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE = space(2)
      .w_UMFLTEMP = space(1)
      .w_RL__TIPO = space(2)
      .w_RLCODICE = space(20)
      .w_RLDESCRI = space(40)
      .w_RL__NOTE = space(0)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_ERECAL = space(5)
      .w_DERCAL = space(40)
      .w_RLCODCAT = space(5)
      .w_RLUMTDEF = space(3)
      .w_DTEMDEF = space(40)
      .w_RLCENCOS = space(15)
      .w_DESCCO = space(40)
      .w_RLCODCES = space(20)
      .w_DESCES = space(40)
      .w_DESCAT = space(40)
      .w_RLQTARIS = 0
      .w_DURSEC = 0
      if .cFunction<>"Filter"
        .w_PPCODICE = 'PP'
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PPCODICE))
          .link_1_1('Full')
          endif
        .w_UMFLTEMP = 'S'
        .w_RL__TIPO = 'AT'
          .DoRTCalc(4,6,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_ERECAL))
          .link_1_12('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_RLCODCAT))
          .link_1_14('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_RLUMTDEF))
          .link_1_15('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_RLCENCOS))
          .link_1_19('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_RLCODCES))
          .link_1_22('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_ORSE')
    this.DoRTCalc(17,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRLCODICE_1_4.enabled = i_bVal
      .Page1.oPag.oRLDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oRL__NOTE_1_7.enabled = i_bVal
      .Page1.oPag.oRLCODCAT_1_14.enabled = i_bVal
      .Page1.oPag.oRLUMTDEF_1_15.enabled = i_bVal
      .Page1.oPag.oRLCENCOS_1_19.enabled = i_bVal
      .Page1.oPag.oRLCODCES_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRLCODICE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRLCODICE_1_4.enabled = .t.
        .Page1.oPag.oRLDESCRI_1_6.enabled = .t.
      endif
    endwith
    this.GSCI_MLM.SetStatus(i_cOp)
    this.GSCI_MDR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'RIS_ORSE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MLM.SetChildrenStatus(i_cOp)
  *  this.GSCI_MDR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__TIPO,"RL__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODICE,"RLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLDESCRI,"RLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__NOTE,"RL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODCAT,"RLCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLUMTDEF,"RLUMTDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCENCOS,"RLCENCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODCES,"RLCODCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLQTARIS,"RLQTARIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    i_lTable = "RIS_ORSE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SAT with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIS_ORSE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIS_ORSE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_ORSE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLCODCAT"+;
                  ",RLUMTDEF,RLCENCOS,RLCODCES,RLQTARIS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RL__TIPO)+;
                  ","+cp_ToStrODBC(this.w_RLCODICE)+;
                  ","+cp_ToStrODBC(this.w_RLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RL__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_RLCODCAT)+;
                  ","+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
                  ","+cp_ToStrODBCNull(this.w_RLCENCOS)+;
                  ","+cp_ToStrODBCNull(this.w_RLCODCES)+;
                  ","+cp_ToStrODBC(this.w_RLQTARIS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_ORSE')
        cp_CheckDeletedKey(i_cTable,0,'RL__TIPO',this.w_RL__TIPO,'RLCODICE',this.w_RLCODICE)
        INSERT INTO (i_cTable);
              (RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLCODCAT,RLUMTDEF,RLCENCOS,RLCODCES,RLQTARIS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RL__TIPO;
                  ,this.w_RLCODICE;
                  ,this.w_RLDESCRI;
                  ,this.w_RL__NOTE;
                  ,this.w_RLCODCAT;
                  ,this.w_RLUMTDEF;
                  ,this.w_RLCENCOS;
                  ,this.w_RLCODCES;
                  ,this.w_RLQTARIS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIS_ORSE_IDX,i_nConn)
      *
      * update RIS_ORSE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_ORSE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RLDESCRI="+cp_ToStrODBC(this.w_RLDESCRI)+;
             ",RL__NOTE="+cp_ToStrODBC(this.w_RL__NOTE)+;
             ",RLCODCAT="+cp_ToStrODBCNull(this.w_RLCODCAT)+;
             ",RLUMTDEF="+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
             ",RLCENCOS="+cp_ToStrODBCNull(this.w_RLCENCOS)+;
             ",RLCODCES="+cp_ToStrODBCNull(this.w_RLCODCES)+;
             ",RLQTARIS="+cp_ToStrODBC(this.w_RLQTARIS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_ORSE')
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        UPDATE (i_cTable) SET;
              RLDESCRI=this.w_RLDESCRI;
             ,RL__NOTE=this.w_RL__NOTE;
             ,RLCODCAT=this.w_RLCODCAT;
             ,RLUMTDEF=this.w_RLUMTDEF;
             ,RLCENCOS=this.w_RLCENCOS;
             ,RLCODCES=this.w_RLCODCES;
             ,RLQTARIS=this.w_RLQTARIS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCI_MLM : Saving
      this.GSCI_MLM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RLCODICE,"LMCODATT";
             ,this.w_RL__TIPO,"LMTIPATT";
             )
      this.GSCI_MLM.mReplace()
      * --- GSCI_MDR : Saving
      this.GSCI_MDR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RLCODICE,"DRCODRIS";
             ,this.w_RL__TIPO,"DRTIPRIS";
             )
      this.GSCI_MDR.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCI_MLM : Deleting
    this.GSCI_MLM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RLCODICE,"LMCODATT";
           ,this.w_RL__TIPO,"LMTIPATT";
           )
    this.GSCI_MLM.mDelete()
    * --- GSCI_MDR : Deleting
    this.GSCI_MDR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RLCODICE,"DRCODRIS";
           ,this.w_RL__TIPO,"DRTIPRIS";
           )
    this.GSCI_MDR.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIS_ORSE_IDX,i_nConn)
      *
      * delete RIS_ORSE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,8,.t.)
          .link_1_12('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCALSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCALSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_ERECAL = NVL(_Link_.PPCALSTA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_ERECAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ERECAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ERECAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ERECAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ERECAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ERECAL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ERECAL = NVL(_Link_.TCCODICE,space(5))
      this.w_DERCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ERECAL = space(5)
      endif
      this.w_DERCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ERECAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCODCAT
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_RISO_IDX,3]
    i_lTable = "CAT_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2], .t., this.CAT_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODCAT like "+cp_ToStrODBC(trim(this.w_RLCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODCAT',trim(this.w_RLCODCAT))
          select CRCODCAT,CRDESCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODCAT)==trim(_Link_.CRCODCAT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_RISO','*','CRCODCAT',cp_AbsName(oSource.parent,'oRLCODCAT_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODCAT',oSource.xKey(1))
            select CRCODCAT,CRDESCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODCAT="+cp_ToStrODBC(this.w_RLCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODCAT',this.w_RLCODCAT)
            select CRCODCAT,CRDESCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCAT = NVL(_Link_.CRCODCAT,space(5))
      this.w_DESCAT = NVL(_Link_.CRDESCAT,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCAT = space(5)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])+'\'+cp_ToStr(_Link_.CRCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_RISO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CRCODCAT as CRCODCAT114"+ ",link_1_14.CRDESCAT as CRDESCAT114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on RIS_ORSE.RLCODCAT=link_1_14.CRCODCAT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCODCAT=link_1_14.CRCODCAT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLUMTDEF
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLUMTDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RLUMTDEF)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_RLUMTDEF))
          select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLUMTDEF)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLUMTDEF) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oRLUMTDEF_1_15'),i_cWhere,'GSAR_AUM',"",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLUMTDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RLUMTDEF);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RLUMTDEF)
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLUMTDEF = NVL(_Link_.UMCODICE,space(3))
      this.w_DTEMDEF = NVL(_Link_.UMDESCRI,space(40))
      this.w_DURSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLUMTDEF = space(3)
      endif
      this.w_DTEMDEF = space(40)
      this.w_DURSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DURSEC > 0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endif
        this.w_RLUMTDEF = space(3)
        this.w_DTEMDEF = space(40)
        this.w_DURSEC = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLUMTDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCENCOS
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_RLCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_RLCENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oRLCENCOS_1_19'),i_cWhere,'',"Elenco centro di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_RLCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_RLCENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCCO = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCENCOS = space(15)
      endif
      this.w_DESCCO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.CC_CONTO as CC_CONTO119"+ ",link_1_19.CCDESPIA as CCDESPIA119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on RIS_ORSE.RLCENCOS=link_1_19.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCENCOS=link_1_19.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLCODCES
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CES_PITI_IDX,3]
    i_lTable = "CES_PITI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2], .t., this.CES_PITI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CES_PITI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODICE like "+cp_ToStrODBC(trim(this.w_RLCODCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODICE',trim(this.w_RLCODCES))
          select CECODICE,CEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODCES)==trim(_Link_.CECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCODCES) and !this.bDontReportError
            deferred_cp_zoom('CES_PITI','*','CECODICE',cp_AbsName(oSource.parent,'oRLCODCES_1_22'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',oSource.xKey(1))
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODICE,CEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CECODICE="+cp_ToStrODBC(this.w_RLCODCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODICE',this.w_RLCODCES)
            select CECODICE,CEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCES = NVL(_Link_.CECODICE,space(20))
      this.w_DESCES = NVL(_Link_.CEDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCES = space(20)
      endif
      this.w_DESCES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])+'\'+cp_ToStr(_Link_.CECODICE,1)
      cp_ShowWarn(i_cKey,this.CES_PITI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CES_PITI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CES_PITI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CECODICE as CECODICE122"+ ",link_1_22.CEDESCRI as CEDESCRI122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on RIS_ORSE.RLCODCES=link_1_22.CECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCODCES=link_1_22.CECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRLCODICE_1_4.value==this.w_RLCODICE)
      this.oPgFrm.Page1.oPag.oRLCODICE_1_4.value=this.w_RLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_6.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_6.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRL__NOTE_1_7.value==this.w_RL__NOTE)
      this.oPgFrm.Page1.oPag.oRL__NOTE_1_7.value=this.w_RL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oERECAL_1_12.value==this.w_ERECAL)
      this.oPgFrm.Page1.oPag.oERECAL_1_12.value=this.w_ERECAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDERCAL_1_13.value==this.w_DERCAL)
      this.oPgFrm.Page1.oPag.oDERCAL_1_13.value=this.w_DERCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCODCAT_1_14.value==this.w_RLCODCAT)
      this.oPgFrm.Page1.oPag.oRLCODCAT_1_14.value=this.w_RLCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oRLUMTDEF_1_15.value==this.w_RLUMTDEF)
      this.oPgFrm.Page1.oPag.oRLUMTDEF_1_15.value=this.w_RLUMTDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDTEMDEF_1_16.value==this.w_DTEMDEF)
      this.oPgFrm.Page1.oPag.oDTEMDEF_1_16.value=this.w_DTEMDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCENCOS_1_19.value==this.w_RLCENCOS)
      this.oPgFrm.Page1.oPag.oRLCENCOS_1_19.value=this.w_RLCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCO_1_21.value==this.w_DESCCO)
      this.oPgFrm.Page1.oPag.oDESCCO_1_21.value=this.w_DESCCO
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCODCES_1_22.value==this.w_RLCODCES)
      this.oPgFrm.Page1.oPag.oRLCODCES_1_22.value=this.w_RLCODCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_24.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_24.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_27.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_27.value=this.w_DESCAT
    endif
    cp_SetControlsValueExtFlds(this,'RIS_ORSE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RLCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLCODICE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_RLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_RLUMTDEF)) or not(.w_DURSEC > 0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLUMTDEF_1_15.SetFocus()
            i_bnoObbl = !empty(.w_RLUMTDEF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MLM.CheckForm()
      if i_bres
        i_bres=  .GSCI_MLM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSCI_MDR.CheckForm()
      if i_bres
        i_bres=  .GSCI_MDR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsci_aat
      if i_bRes
          GSCI_BCW(this,"QTARISORSA")
          i_bRes = i_bRes .and. nvl(this.w_RLQTARIS,0)>0
          if !i_bRes
               ah_ErrorMsg("Inserire la quantit� risorsa nella scheda disponibilit�",48)
          endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCI_MLM : Depends On
    this.GSCI_MLM.SaveDependsOn()
    * --- GSCI_MDR : Depends On
    this.GSCI_MDR.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsci_aatPag1 as StdContainer
  Width  = 620
  height = 318
  stdWidth  = 620
  stdheight = 318
  resizeXpos=350
  resizeYpos=80
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRLCODICE_1_4 as StdField with uid="NSFCZVWNHR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RLCODICE", cQueryName = "RL__TIPO,RLCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice risorsa / componente",;
    HelpContextID = 84507739,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=73, Top=12, InputMask=replicate('X',20)

  add object oRLDESCRI_1_6 as StdField with uid="QOVLHIEMEA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve",;
    HelpContextID = 267357279,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=245, Top=12, InputMask=replicate('X',40)

  add object oRL__NOTE_1_7 as StdMemo with uid="KCHIZIXSUG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_RL__NOTE", cQueryName = "RL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione estesa",;
    HelpContextID = 196820059,;
   bGlobalFont=.t.,;
    Height=84, Width=468, Left=73, Top=35

  add object oERECAL_1_12 as StdField with uid="VZWPDCMYPC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ERECAL", cQueryName = "ERECAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 137518778,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=190, Top=123, InputMask=replicate('X',5), cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_ERECAL"

  func oERECAL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDERCAL_1_13 as StdField with uid="QYRJMQOSKC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DERCAL", cQueryName = "DERCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 137468874,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=245, Top=123, InputMask=replicate('X',40)

  add object oRLCODCAT_1_14 as StdField with uid="UNYIGSJWQF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RLCODCAT", cQueryName = "RLCODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria risorsa",;
    HelpContextID = 16155542,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=123, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_RISO", oKey_1_1="CRCODCAT", oKey_1_2="this.w_RLCODCAT"

  func oRLCODCAT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODCAT_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCODCAT_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_RISO','*','CRCODCAT',cp_AbsName(this.parent,'oRLCODCAT_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oRLUMTDEF_1_15 as StdField with uid="CXOXJIBYZO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RLUMTDEF", cQueryName = "RLUMTDEF",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura non gestita a tempo oppure senza conversione in secondi",;
    ToolTipText = "Unit� di misura di default (proposta in gestione cicli)",;
    HelpContextID = 17341532,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=123, Top=202, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_RLUMTDEF"

  func oRLUMTDEF_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLUMTDEF_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLUMTDEF_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oRLUMTDEF_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oRLUMTDEF_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_RLUMTDEF
     i_obj.ecpSave()
  endproc

  add object oDTEMDEF_1_16 as StdField with uid="CBTPWFZRZF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DTEMDEF", cQueryName = "DTEMDEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17277750,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=188, Top=202, InputMask=replicate('X',40)

  add object oRLCENCOS_1_19 as StdField with uid="VUMDDNBHAP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_RLCENCOS", cQueryName = "RLCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 6325143,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=124, Top=263, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_RLCENCOS"

  func oRLCENCOS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCENCOS_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCENCOS_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oRLCENCOS_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco centro di costo",'',this.parent.oContained
  endproc

  add object oDESCCO_1_21 as StdField with uid="FINBQXUXSV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCCO", cQueryName = "DESCCO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 85035978,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=258, Top=263, InputMask=replicate('X',40)

  add object oRLCODCES_1_22 as StdField with uid="EFIGDLWRCD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_RLCODCES", cQueryName = "RLCODCES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice cespite",;
    HelpContextID = 252279913,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=124, Top=288, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CES_PITI", oKey_1_1="CECODICE", oKey_1_2="this.w_RLCODCES"

  func oRLCODCES_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODCES_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCODCES_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CES_PITI','*','CECODICE',cp_AbsName(this.parent,'oRLCODCES_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCES_1_24 as StdField with uid="RQXPIJKKYP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15829962,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=294, Top=288, InputMask=replicate('X',40)

  add object oDESCAT_1_27 as StdField with uid="IDZJSNCUBS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 3247050,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=187, Top=177, InputMask=replicate('X',40)

  add object oStr_1_5 as StdString with uid="GEDSKYSLUY",Visible=.t., Left=14, Top=12,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="IKZHASEGNC",Visible=.t., Left=19, Top=147,;
    Alignment=0, Width=80, Height=15,;
    Caption="Gestionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZTNLRXXYUN",Visible=.t., Left=19, Top=237,;
    Alignment=0, Width=141, Height=15,;
    Caption="Analitica/cespiti"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="NJLRTVJEZP",Visible=.t., Left=17, Top=263,;
    Alignment=1, Width=102, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="IUIOCTFPLV",Visible=.t., Left=28, Top=288,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice cespite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KQZGICKXSG",Visible=.t., Left=24, Top=202,;
    Alignment=1, Width=95, Height=15,;
    Caption="UM tempo pref.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XXRBPKINYV",Visible=.t., Left=32, Top=123,;
    Alignment=1, Width=154, Height=15,;
    Caption="Calendario di stabilimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FNDPNLIVFV",Visible=.t., Left=61, Top=177,;
    Alignment=1, Width=57, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="JSAVHVQRGB",left=15, top=163, width=568,height=1

  add object oBox_1_11 as StdBox with uid="WYJRGMGZVO",left=16, top=253, width=568,height=1
enddefine
define class tgsci_aatPag2 as StdContainer
  Width  = 620
  height = 318
  stdWidth  = 620
  stdheight = 318
  resizeXpos=482
  resizeYpos=239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="VKYNWTLUUJ",left=3, top=8, width=617, height=310, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mdr",lower(this.oContained.GSCI_MDR.class))=0
        this.oContained.GSCI_MDR.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsci_aatPag3 as StdContainer
  Width  = 620
  height = 318
  stdWidth  = 620
  stdheight = 318
  resizeXpos=509
  resizeYpos=269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="NZWBTYBFLZ",left=2, top=7, width=583, height=308, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mlm",lower(this.oContained.GSCI_MLM.class))=0
        this.oContained.GSCI_MLM.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="RL__TIPO='AT'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsci_aat','RIS_ORSE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName2+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
