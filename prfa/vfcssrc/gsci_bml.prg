* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bml                                                        *
*              Gestione WIP materiali ODL                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-24                                                      *
* Last revis.: 2015-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bml",oParentObject,m.w_OPERAZ)
return(i_retval)

define class tgsci_bml as StdBatch
  * --- Local variables
  w_LockScreen = .f.
  w_OPERAZ = space(10)
  w_KEYRIF = space(10)
  w_GSCO_MOL = .NULL.
  w_GSCO_AOP = .NULL.
  w_GSCO_MCL = .NULL.
  w_RLMAGWIP = space(5)
  w_RL__TIPO = space(2)
  w_ROWNUM = 0
  w_ORDFAS = space(15)
  w_OLCODODL = space(15)
  w_CPROWNUM = 0
  w_CTRSNAME = space(10)
  w_GSCO_MRL = .NULL.
  w_STATOOCL = space(1)
  w_WIPCENTRO = space(5)
  w_ROWORDC = 0
  w_CHKOBSO = .f.
  w_OLDTOBSO = ctod("  /  /  ")
  w_OLTDINRIC = ctod("  /  /  ")
  w_CANDELROW = .f.
  w_FASOUT = space(1)
  w_CLFASCLA = space(1)
  w_NOCPRIFE = .f.
  w_TRSODL_CICL = space(10)
  w_TRSODLTCICL = space(10)
  w_TRSODL_RISO = space(10)
  w_TRSODLTRISO = space(10)
  w_CLKEYRIF = space(10)
  w_CLROWNUM = 0
  w_CLROWORD = 0
  w_FASEPREF = space(2)
  w_CLFASEVA = space(1)
  w_QTAVER = 0
  w_CLDICUM1 = 0
  w_CLINDPRE = space(2)
  GSCO_AOP = .NULL.
  GSCO_MOL = .NULL.
  w_GSCO_MCL = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_OLEVAAUT = space(1)
  w_FOUND = .f.
  w_L_CHECK = space(10)
  * --- WorkFile variables
  RIS_DETT_idx=0
  CIC_DETT_idx=0
  RIS_ORSE_idx=0
  ODL_MAST_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  PAR_PROD_idx=0
  ART_PROD_idx=0
  CAM_AGAZ_idx=0
  ODL_DETT_idx=0
  SALDICOM_idx=0
  ODL_CICL_idx=0
  CON_MACL_idx=0
  TMPODL_CICL_idx=0
  ODL_RISO_idx=0
  TMPODL_RISO_idx=0
  ART_TEMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Magazzino WIP modifica materiali stimati (da w_GSCO_MOL)
    if g_PRFA<>"S" OR g_CICLILAV<>"S"
      i_retcode = 'stop'
      return
    endif
    this.w_oMESS=createobject("AH_MESSAGE")
    * --- Punta al padre
    this.w_GSCO_MOL = this.oParentObject
    this.w_CTRSNAME = this.w_GSCO_MOL.ctrsName
    * --- Punta al nonno
    this.w_GSCO_AOP = this.w_GSCO_MOL.oParentObject
    this.w_GSCO_MCL = this.w_GSCO_AOP.GSCO_MCL
    if Type("this.w_GSCO_MCL.GSCO_MRL")="O"
      this.w_GSCO_MRL = this.w_GSCO_MCL.GSCO_MRL
    endif
    this.w_OLCODODL = this.w_GSCO_AOP.w_OLCODODL
    this.w_KEYRIF = this.w_GSCO_AOP.w_CLKEYRIF
    * --- Se non esiste tabella temporanea la creo
    * --- Create temporary table TMPODL_CICL
    if !cp_ExistTableDef('TMPODL_CICL')
      i_nIdx=cp_AddTableDef('TMPODL_CICL',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_CICL_proto';
            )
      this.TMPODL_CICL_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.TMPODL_CICL_idx=0
        this.TMPODL_CICL_idx=cp_ascan(@i_TableProp,lower('TMPODL_CICL'),i_nTables)
      endif
    endif
    * --- Create temporary table TMPODL_RISO
    if !cp_ExistTableDef('TMPODL_RISO')
      i_nIdx=cp_AddTableDef('TMPODL_RISO',.t.) && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPODL_RISO_proto';
            )
      this.TMPODL_RISO_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.TMPODL_RISO_idx=0
        this.TMPODL_RISO_idx=cp_ascan(@i_TableProp,lower('TMPODL_RISO'),i_nTables)
      endif
    endif
    if .F.
      this.TMPODL_CICL_idx = cp_OpenTable("TMPODL_CICL")
      this.TMPODL_RISO_idx = cp_OpenTable("TMPODL_RISO")
    endif
    do case
      case this.w_OPERAZ = "SETWIP"
        * --- Il Magazzino WIP � gestito dal batch solo per gli ODL lanciati, altrimenti dalla maschera
        do case
          case this.w_GSCO_AOP.w_OLTSTATO="L"
            * --- Inizializza w_MAGPRE perch� sul padre non ha ancora fatto il link
            if not empty(this.oParentObject.w_OLCODART)
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARMAGPRE"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_OLCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARMAGPRE;
                  from (i_cTable) where;
                      ARCODART = this.oParentObject.w_OLCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if empty(this.w_GSCO_AOP.w_OLTSECIC) or this.w_GSCO_AOP.w_OLTPROVE="L"
              * --- Nessun ciclo associato all'ODL
              this.oParentObject.w_OLMAGPRE = iif(empty(this.oParentObject.w_MAGPRE), iif(empty(this.oParentObject.w_MAGPRO),g_MAGAZI, this.oParentObject.w_MAGPRO),this.oParentObject.w_MAGPRE)
              this.oParentObject.w_OLCODMAG = iif(this.w_GSCO_AOP.w_OLTPROVE="I", this.oParentObject.w_MAGWIP, this.oParentObject.w_OLMAGPRE)
              this.oParentObject.w_OLMAGWIP = iif(this.w_GSCO_AOP.w_OLTPROVE="I", this.oParentObject.w_MAGWIP, space(5))
            else
              this.oParentObject.w_OLMAGPRE = iif(empty(this.oParentObject.w_MAGPRE), iif(empty(this.oParentObject.w_MAGPRO),g_MAGAZI, this.oParentObject.w_MAGPRO),this.oParentObject.w_MAGPRE)
              * --- Esiste un ciclo associato all'ODL
              if this.oParentObject.w_OLFASRIF>0
                * --- Se modifico l'ordine manualmente prima verifico che le fasi siano corrette
                * --- Priama di effettuare il controllo aggiorno la tabella TMPODL_CICL
                if !this.w_GSCO_MCL.w_UPDATED
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                * --- Read from TMPODL_CICL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CLFASCLA,CPROWNUM,CLROWORD"+;
                    " from "+i_cTable+" TMPODL_CICL where ";
                        +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                        +" and CLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                        +" and CLROWORD = "+cp_ToStrODBC(this.oParentObject.w_OLFASRIF);
                        +" and CLINDPRE = "+cp_ToStrODBC("00");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CLFASCLA,CPROWNUM,CLROWORD;
                    from (i_cTable) where;
                        CLKEYRIF = this.w_KEYRIF;
                        and CLCODODL = this.w_OLCODODL;
                        and CLROWORD = this.oParentObject.w_OLFASRIF;
                        and CLINDPRE = "00";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CLFASCLA = NVL(cp_ToDate(_read_.CLFASCLA),cp_NullValue(_read_.CLFASCLA))
                  this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  this.w_ROWORDC = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_CLFASCLA="S"
                  * --- Fase esterna - Cerca OCL associato
                  * --- Read from ODL_MAST
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ODL_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "OLCODODL,OLTSTATO"+;
                      " from "+i_cTable+" ODL_MAST where ";
                          +"OLTSEODL = "+cp_ToStrODBC(this.w_OLCODODL);
                          +" and OLTFAODL = "+cp_ToStrODBC(this.w_ROWNUM);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      OLCODODL,OLTSTATO;
                      from (i_cTable) where;
                          OLTSEODL = this.w_OLCODODL;
                          and OLTFAODL = this.w_ROWNUM;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ORDFAS = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
                    this.w_STATOOCL = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if empty(this.w_ORDFAS)
                    this.oParentObject.w_OLCODMAG = this.oParentObject.w_OLMAGPRE
                    this.oParentObject.w_OLMAGWIP = space(5)
                  endif
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Legge il WIP del Centro di Lavoro
                  this.oParentObject.w_OLCODMAG = space(5)
                  * --- Select from TMPODL_RISO
                  i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2],.t.,this.TMPODL_RISO_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select RLCODICE  from "+i_cTable+" TMPODL_RISO ";
                        +" where CLKEYRIF="+cp_ToStrODBC(this.w_KEYRIF)+" AND RLCODODL="+cp_ToStrODBC(this.w_OLCODODL)+" AND RLROWNUM="+cp_ToStrODBC(this.w_ROWNUM)+"";
                         ,"_Curs_TMPODL_RISO")
                  else
                    select RLCODICE from (i_cTable);
                     where CLKEYRIF=this.w_KEYRIF AND RLCODODL=this.w_OLCODODL AND RLROWNUM=this.w_ROWNUM;
                      into cursor _Curs_TMPODL_RISO
                  endif
                  if used('_Curs_TMPODL_RISO')
                    select _Curs_TMPODL_RISO
                    locate for 1=1
                    do while not(eof())
                    * --- Read from RIS_ORSE
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2],.t.,this.RIS_ORSE_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "RLMAGWIP,RL__TIPO"+;
                        " from "+i_cTable+" RIS_ORSE where ";
                            +"RLCODICE = "+cp_ToStrODBC(_Curs_TMPODL_RISO.RLCODICE);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        RLMAGWIP,RL__TIPO;
                        from (i_cTable) where;
                            RLCODICE = _Curs_TMPODL_RISO.RLCODICE;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_RLMAGWIP = NVL(cp_ToDate(_read_.RLMAGWIP),cp_NullValue(_read_.RLMAGWIP))
                      this.w_RL__TIPO = NVL(cp_ToDate(_read_.RL__TIPO),cp_NullValue(_read_.RL__TIPO))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if this.w_RL__TIPO="CL"
                      this.w_WIPCENTRO = this.w_RLMAGWIP
                    endif
                      select _Curs_TMPODL_RISO
                      continue
                    enddo
                    use
                  endif
                  if this.oParentObject.w_OLTIPPRE="N"
                    this.oParentObject.w_OLMAGWIP = SPACE(5)
                    this.w_GSCO_MOL.mCalc(.t.)     
                    this.oParentObject.w_OLCODMAG = iif(empty(nvl(this.oParentObject.w_ARMAGIMP,"")),iif(empty(this.oParentObject.w_MAGPRE),iif(empty(this.oParentObject.w_MAGPRO),g_MAGAZI, this.oParentObject.w_MAGPRO),this.oParentObject.w_MAGPRE),this.oParentObject.w_ARMAGIMP)
                    this.w_GSCO_MOL.mCalc(.t.)     
                  else
                    this.oParentObject.w_OLCODMAG = iif(empty(this.w_WIPCENTRO),this.oParentObject.w_MAGWIP, this.w_WIPCENTRO)
                    this.w_GSCO_MOL.mCalc(.t.)     
                    this.oParentObject.w_OLMAGWIP = this.oParentObject.w_OLCODMAG
                    this.w_GSCO_MOL.mCalc(.t.)     
                  endif
                endif
              endif
            endif
          case this.w_GSCO_AOP.w_OLTSTATO $ "MP" and this.oParentObject.w_OLFASRIF>0 and this.w_GSCO_AOP.w_OLTPROVE<>"L"
            * --- Priama di effettuare il controllo aggiorno la tabella TMPODL_CICL
            if !this.w_GSCO_MCL.w_UPDATED
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Controlla che la fase non sia legata ad un OCL/fase
            * --- Read from TMPODL_CICL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CLFASCLA,CPROWNUM,CLROWORD"+;
                " from "+i_cTable+" TMPODL_CICL where ";
                    +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                    +" and CLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
                    +" and CLROWORD = "+cp_ToStrODBC(this.oParentObject.w_OLFASRIF);
                    +" and CLINDPRE = "+cp_ToStrODBC("00");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CLFASCLA,CPROWNUM,CLROWORD;
                from (i_cTable) where;
                    CLKEYRIF = this.w_KEYRIF;
                    and CLCODODL = this.w_OLCODODL;
                    and CLROWORD = this.oParentObject.w_OLFASRIF;
                    and CLINDPRE = "00";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLFASCLA = NVL(cp_ToDate(_read_.CLFASCLA),cp_NullValue(_read_.CLFASCLA))
              this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              this.w_ROWORDC = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Fase - Cerca ordine associato
            if this.w_ROWNUM > 0
              * --- Read from ODL_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLCODODL,OLTSTATO"+;
                  " from "+i_cTable+" ODL_MAST where ";
                      +"OLTSEODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
                      +" and OLTFAODL = "+cp_ToStrODBC(this.w_ROWNUM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLCODODL,OLTSTATO;
                  from (i_cTable) where;
                      OLTSEODL = this.w_GSCO_AOP.w_OLCODODL;
                      and OLTFAODL = this.w_ROWNUM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ORDFAS = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
                this.w_STATOOCL = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if empty(this.w_ORDFAS)
                this.oParentObject.w_OLCODMAG = this.oParentObject.w_OLMAGPRE
                this.oParentObject.w_OLMAGWIP = space(5)
              endif
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
        endcase
      case this.w_OPERAZ = "CHKOBSO"
        this.w_OLTDINRIC = this.w_GSCO_MOL.w_OLDATRIC
        this.w_OLDTOBSO = this.w_GSCO_MOL.w_OLDTOBSO
        this.w_CHKOBSO = NOT(this.w_OLDTOBSO>this.w_OLTDINRIC OR this.w_OLDTOBSO>i_DATSYS or Empty(this.w_OLDTOBSO) )
        if this.w_CHKOBSO
          ah_ErrorMsg("Attenzione%0Codice di ricerca obsoleto",48)
        endif
        * --- Assegno il magazzino con quello preferenziale in caso di prelievo nessuno
        if this.oParentObject.w_OLTIPPRE="N"
          this.oParentObject.w_OLCODMAG = this.oParentObject.w_OLMAGPRE
        endif
      case this.w_OPERAZ = "BeforeLoadDetail"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case INLIST(this.w_OPERAZ , "INSFASI" , "DELFASI", "ALLFASI")
        if !this.w_GSCO_MCL.w_UPDATED or this.w_OPERAZ="ALLFASI"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case INLIST(this.w_OPERAZ , "INPUTFASE" , "ZOOMFASE")
        if !this.w_GSCO_MCL.w_UPDATED
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPERAZ = "UPDATEPHASE"
        if !this.w_GSCO_MCL.w_UPDATED
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_OPERAZ = "CanDeleteRow"
        if !Empty(this.w_GSCO_AOP.w_OLTSECIC)
        else
        endif
      case this.w_OPERAZ = "CHECKFAS"
        if this.oParentObject.w_OLFASRIF>0 and not empty(this.w_GSCO_AOP.w_OLTSECIC)
          * --- Read from CIC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CIC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CIC_DETT_idx,2],.t.,this.CIC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CLFASOUT"+;
              " from "+i_cTable+" CIC_DETT where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLTSECIC);
                  +" and CPROWORD = "+cp_ToStrODBC(this.oParentObject.w_OLFASRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CLFASOUT;
              from (i_cTable) where;
                  CLSERIAL = this.w_GSCO_AOP.w_OLTSECIC;
                  and CPROWORD = this.oParentObject.w_OLFASRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FASOUT = NVL(cp_ToDate(_read_.CLFASOUT),cp_NullValue(_read_.CLFASOUT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FASOUT<>"S"
            ah_errorMsg("Attenzione! La fase selezionata non � di output.",16, "")
            this.w_GSCO_MOL.SaveDependsOn()     
            SELECT(this.w_GSCO_MOL.cTrsName)
            GATHER FROM this.w_GSCO_MOL.RowData
            this.w_GSCO_MOL.SetRow()     
            this.w_GSCO_MOL.SaveDependsOn()     
            this.w_GSCO_MOL.mEnableControls()     
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LockScreen = this.w_GSCO_AOP.LockScreen
    this.w_GSCO_AOP.LockScreen = .t.
    if this.w_OPERAZ = "DELFASI"
      * --- Delete from TMPODL_CICL
      i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CLKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from TMPODL_RISO
      i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CLKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      if inlist(this.w_OPERAZ , "BeforeLoadDetail", "ALLFASI")
        * --- Se non sto aggiornando allora pulisco anche il temporaneo
        * --- L'aggiornamento mi consente di essere pi� veloce e di modificare/aggiungere solo le fasi modificate
        * --- Delete from TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TMPODL_RISO
        i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CLKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CLKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if this.w_OPERAZ = "BeforeLoadDetail"
        * --- Se sono in fase di lettura dell'ODL leggo il ciclo da ODL_CICL e lo inserisco nel temporaneo
        * --- AND ODL_CICL->CLFASSEL='S'
        * --- Insert into TMPODL_CICL
        i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCIABTC",this.TMPODL_CICL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Insert into TMPODL_RISO
        i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\PRFA\EXE\QUERY\GSCIBBTC",this.TMPODL_RISO_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      * --- Memorizzo le vecchie impostazioni in modo da modificare solo le righe modificate
      if Empty(this.w_GSCO_AOP.w_Cur_Cic_Odl)
        this.w_GSCO_AOP.w_Cur_Cic_Odl = Sys(2015)
      endif
      if !Used(this.w_GSCO_AOP.w_Cur_Cic_Odl) OR this.w_OPERAZ = "BeforeLoadDetail"
        * --- In fase di lettura dell'ordine la eseguo, in caricamento e in modifica la faccio una volta sola
        USE IN SELECT(this.w_GSCO_AOP.w_Cur_Cic_Odl)
        vq_exec("..\PRFA\EXE\QUERY\GSCIQBTC", this, this.w_GSCO_AOP.w_Cur_Cic_Odl)
        Update (this.w_GSCO_AOP.w_Cur_Cic_Odl) SET CLFASCLA = "N" WHERE CLFASCLA<>"S"
        Update (this.w_GSCO_AOP.w_Cur_Cic_Odl) SET CLULTFAS = "N" WHERE CLULTFAS<>"S"
        * --- Creo il CheckSum del record che mi serve per verificare se ho cambiato qualcosa
      endif
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      if this.w_OPERAZ = "BeforeLoadDetail"
        * --- Non faccio niente gi� fatto prima
      else
        this.w_CLFASCLA = "N"
        this.w_NOCPRIFE = .F.
        this.w_GSCO_MCL.MarkPos()     
        this.w_GSCO_MCL.FirstRow()     
        this.w_TRSODL_CICL = SYS(2015)
        this.w_TRSODLTCICL = SYS(2015)
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        l_cmd = "I_SRV AS I_SRV, "+cp_ToStrODBC(this.w_OLCODODL)+" as CLCODODL, T_CLROWNUM  as CLROWNUM, CPROWNUM   as CPROWNUM, "
        l_cmd = l_cmd + "T_CLROWORD   as CLROWORD,T_CLINDPRE as CLINDPRE, "
        l_cmd = l_cmd + "T_CL__FASE as CL__FASE,T_CLDESFAS as CLDESFAS, "
        l_cmd = l_cmd + "T_CLQTAAVA as CLQTAAVA,T_CLAVAUM1 as CLAVAUM1,T_CLQTASCA as CLQTASCA,T_CLSCAUM1 as CLSCAUM1, "
        l_cmd = l_cmd + "T_FLFASEVA as CLFASEVA, T_FLCOUPOI as CLCOUPOI, T_FLPRIFAS as CLPRIFAS, T_FLULTFAS as CLULTFAS, "
        l_cmd = l_cmd + "T_FLFASSEL as CLFASSEL,T_CLCPRIFE   as CLCPRIFE, T_CLBFRIFE   as CLBFRIFE,T_CLDATINI as CLDATINI, "
        l_cmd = l_cmd + "T_CLDATFIN as CLDATFIN,T_CLWIPOUT as CLWIPOUT,T_CLWIPDES as CLWIPDES,T_CLWIPFPR as CLWIPFPR, "
        l_cmd = l_cmd + "T_CLCODERR as CLCODERR, T_CLLTFASE AS CLLTFASE, T_FLFASCOS as CLFASCOS, "
        l_cmd = l_cmd + "T_CLCODFAS as CLCODFAS, T_FLFASOUT as CLFASOUT, IIF(T_CLFASCLA=1 ,'S', 'N') as CLFASCLA, "
        l_cmd = l_cmd + "T_CLMAGWIP AS CLMAGWIP, T_CLSEOLFA AS CLSEOLFA, T_FLTSTAOL AS FLTSTAOL, T_FLQTAEVA AS FLQTAEVA, T_FLTPROVE AS FLTPROVE"
        this.w_GSCO_MCL.Exec_Select(this.w_TRSODLTCICL , l_cmd, " ", "T_CLROWORD DESC", " ", " " )     
        this.w_GSCO_MCL.RePos()     
        USE IN SELECT(this.w_TRSODL_CICL)
        SELECT * FROM (this.w_GSCO_AOP.w_Cur_Cic_Odl) INTO CURSOR (this.w_TRSODL_CICL) WHERE 1=0 READWRITE
        SELECT(this.w_TRSODL_CICL)
        APPEND FROM DBF(this.w_TRSODLTCICL)
        INDEX ON CLROWORD TAG CLROWORD
        USE IN SELECT(this.w_TRSODLTCICL)
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Creo cursore vuoto delle risorse dopo lo riempio
        this.w_TRSODL_RISO = SYS(2015)
        this.w_TRSODLTRISO = SYS(2015)
        l_cmd = "I_SRV AS I_SRV, "+cp_ToStrODBC(this.w_OLCODODL)+" as RLCODODL, CPROWNUM, T_CPROWORD as CPROWORD,T_RLCODCLA as RLCODCLA,"
        l_cmd = l_cmd + "T_RL1TIPO AS RL__TIPO, T_RLCODICE as RLCODICE,T_RLTIPTET AS RLTIPTEM, "
        l_cmd = l_cmd + "T_RLQTARIS AS RLQTARIS, T_RLUMTRIS AS RLUMTRIS, T_RLTEMRIS AS RLTEMRIS,"
        l_cmd = l_cmd + "T_RLTEMSEC AS RLTEMSEC, T_RLTPREVS AS RLTPREVS, T_RLTPRRIS AS RLTPRRIS,"
        l_cmd = l_cmd + "T_RLTPRRIU AS RLTPRRIU, T_RLTPSTDS AS RLTPSTDS, T_RLQPSRIS AS RLQPSRIS,"
        l_cmd = l_cmd + "T_RLTPSRIS AS RLTPSRIS, T_RLTPSRIU AS RLTPSRIU, T_RLTCONSS AS RLTCONSS,"
        l_cmd = l_cmd + "T_RLTCORIS AS RLTCORIS, T_RLTCOEXT AS RLTCOEXT, T_RLCODPAD AS RLCODPAD,"
        l_cmd = l_cmd + "T_RLNUMIMP AS RLNUMIMP, T_RLPROORA AS RLPROORA, T_RLTMPCIC AS RLTMPCIC,"
        l_cmd = l_cmd + "T_RLTMPSEC AS RLTMPSEC, T_RLTEMCOD AS RLTEMCOD, "
        l_cmd1 = "' ' as RLINTEST, T_RLMAGWIP AS RLMAGWIP, 999999*0 as RLROWNUM"
        l_cmd2 = l_cmd + l_cmd1
        this.w_GSCO_MRL.MarkPos()     
        this.w_GSCO_MRL.Exec_Select(this.w_TRSODL_RISO, l_cmd2, "1=2", " ", " ", " " )     
        this.w_GSCO_MRL.RePos()     
        =WRCURSOR(this.w_TRSODL_RISO)
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        this.w_GSCO_MCL.MarkPos()     
        this.w_GSCO_MCL.FirstRow()     
        do while Not this.w_GSCO_MCL.Eof_Trs()
          this.w_GSCO_MCL.SetRow()     
          this.w_GSCO_MCL.ChildrenChangeRow()     
          l_cmd3 = "IIF(NVL(T_RLINTEST,0)=1, 'S', 'N')  as RLINTEST, T_RLMAGWIP AS RLMAGWIP, " + cp_ToStrODBC(this.w_GSCO_MCL.w_CPROWNUM) + " as RLROWNUM"
          l_cmd2 = l_cmd + l_cmd3
          this.w_GSCO_MRL.Exec_Select(this.w_TRSODLTRISO, l_cmd2, " ", " ", " ", " " )     
          Select(this.w_TRSODL_RISO)
          APPEND FROM DBF(this.w_TRSODLTRISO)
          USE IN SELECT(this.w_TRSODLTRISO)
          this.w_GSCO_MCL.NextRow()     
        enddo
        this.w_GSCO_MCL.RePos()     
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Ora posso eseguire la routine che sistema le fasi (la eseguo su cursore � pi� veloce) 
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Calcolo checksum del record per vedere se ho modificato qualcosa
        Update (this.w_TRSODL_CICL) SET CLFASCLA = "N" WHERE CLFASCLA <> "S"
        Update (this.w_TRSODL_CICL) SET CLULTFAS = "N" WHERE CLULTFAS <> "S"
        * --- Aggiorno il flag conto lavoro per le fasi esterne
        Update (this.w_TRSODL_CICL) SET i_SRV=IIF(i_SRV="A","A","U"), CLMAGWIP = B.RLMAGWIP, CLLTFASE = B.RLTEMCOD ; 
 FROM (SELECT RLROWNUM, RLMAGWIP, RLTEMCOD FROM (this.w_TRSODL_RISO) WHERE RL__TIPO="CL") B WHERE CPROWNUM=B.RLROWNUM
        Update (this.w_TRSODL_CICL) SET i_SRV=IIF(i_SRV="A","A","U"), CLFASCLA = "S" WHERE CPROWNUM IN ; 
 (SELECT DISTINCT B.RLROWNUM AS RLROWNUM FROM (this.w_TRSODL_RISO) B where RL__TIPO="CL" AND RLINTEST="S")
        if !this.w_OPERAZ = "ALLFASI"
          * --- Aggiorno se ho cambiato il figlio
           Update (this.w_TRSODL_CICL) SET i_SRV=IIF(i_SRV="A","A","U") WHERE CPROWNUM IN ; 
 (SELECT DISTINCT B.RLROWNUM AS RLROWNUM FROM (this.w_TRSODL_RISO) B where i_SRV $ "A-U")
        endif
        GSCITBFO(this,this.w_OLCODODL, this.w_KEYRIF, this.w_TRSODL_CICL, this.w_TRSODL_RISO)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Verifico cosa � stato modificato rispetto al vecchio cursore
        this.w_TRSODLTCICL = SYS(2015)
        if !this.w_OPERAZ = "ALLFASI"
          SELECT Z.* FROM (this.w_TRSODL_CICL) Z INNER JOIN ( ;
          SELECT MIN(ID) AS ID, CLCODODL, CPROWNUM, CRC FROM ( ;
          SELECT "B" AS ID, B.CLCODODL, B.CPROWNUM, Sys(2017, "CLDATINI,CLDATFIN", 1,1) as crc FROM (this.w_TRSODL_CICL) B UNION ALL ;
          SELECT "A" AS ID, C.CLCODODL, C.CPROWNUM, Sys(2017, "CLDATINI,CLDATFIN", 1,1) as crc FROM (this.w_GSCO_AOP.w_Cur_Cic_Odl) C ) AS TMP1 ; 
           GROUP BY CLCODODL, CPROWNUM, CRC HAVING COUNT(*) = 1 AND MAX(ID)="B" ) U ON Z.CLCODODL=U.CLCODODL AND Z.CPROWNUM=U.CPROWNUM ;
          INTO CURSOR (this.w_TRSODLTCICL)
        else
          SELECT Z.* FROM (this.w_TRSODL_CICL) Z INTO CURSOR (this.w_TRSODLTCICL)
        endif
        if !this.w_OPERAZ = "ALLFASI"
          SELECT(this.w_TRSODLTCICL)
          SCAN
          SCATTER MEMVAR
          if INLIST(this.w_OPERAZ , "INPUTFASE" , "ZOOMFASE", "SETWIP")
            * --- Aggiornamento aggiorno solo le fasi modificate/inserite
            *     Le fasi vengono eliminate direttamente  all'F6
            if m.I_SRV= "A"
              * --- Insert into TMPODL_CICL
              i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
              i_commit = .f.
              local bErr_03C098C0
              bErr_03C098C0=bTrsErr
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_CICL_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CLKEYRIF"+",CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CL__FASE"+",CLDESFAS"+",CLFASEVA"+",CLCOUPOI"+",CLFASOUT"+",CLPRIFAS"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLBFRIFE"+",CLDATINI"+",CLDATFIN"+",CLWIPOUT"+",CLCODFAS"+",CLWIPDES"+",CLWIPFPR"+",CLCODERR"+",CLLTFASE"+",CLFASCOS"+",CLFASCLA"+",CLMAGWIP"+",CLSEOLFA"+",FLTSTAOL"+",FLQTAEVA"+",FLTPROVE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_CICL','CLKEYRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_CICL','CLCODODL');
                +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_CICL','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
                +","+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
                +","+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
                +","+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
                +","+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
                +","+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
                +","+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
                +","+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
                +","+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
                +","+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
                +","+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
                +","+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
                +","+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
                +","+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
                +","+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
                +","+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
                +","+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
                +","+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
                +","+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
                +","+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
                +","+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
                +","+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
                +","+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
                +","+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
                +","+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
                +","+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
                +","+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'CLCODODL',this.w_GSCO_AOP.w_OLCODODL,'CPROWNUM',M.CPROWNUM,'CLROWNUM',M.CLROWNUM,'CLROWORD',M.CLROWORD,'CLINDPRE',m.CLINDPRE,'CL__FASE',m.CL__FASE,'CLDESFAS',m.CLDESFAS,'CLFASEVA',m.CLFASEVA,'CLCOUPOI',m.CLCOUPOI,'CLFASOUT',m.CLFASOUT,'CLPRIFAS',m.CLPRIFAS)
                insert into (i_cTable) (CLKEYRIF,CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CL__FASE,CLDESFAS,CLFASEVA,CLCOUPOI,CLFASOUT,CLPRIFAS,CLULTFAS,CLFASSEL,CLCPRIFE,CLBFRIFE,CLDATINI,CLDATFIN,CLWIPOUT,CLCODFAS,CLWIPDES,CLWIPFPR,CLCODERR,CLLTFASE,CLFASCOS,CLFASCLA,CLMAGWIP,CLSEOLFA,FLTSTAOL,FLQTAEVA,FLTPROVE &i_ccchkf. );
                   values (;
                     this.w_GSCO_AOP.w_CLKEYRIF;
                     ,this.w_GSCO_AOP.w_OLCODODL;
                     ,M.CPROWNUM;
                     ,M.CLROWNUM;
                     ,M.CLROWORD;
                     ,m.CLINDPRE;
                     ,m.CL__FASE;
                     ,m.CLDESFAS;
                     ,m.CLFASEVA;
                     ,m.CLCOUPOI;
                     ,m.CLFASOUT;
                     ,m.CLPRIFAS;
                     ,m.CLULTFAS;
                     ,m.CLFASSEL;
                     ,m.CLCPRIFE;
                     ,m.CLBFRIFE;
                     ,m.CLDATINI;
                     ,m.CLDATFIN;
                     ,m.CLWIPOUT;
                     ,m.CLCODFAS;
                     ,m.CLWIPDES;
                     ,m.CLWIPFPR;
                     ,m.CLCODERR;
                     ,m.CLLTFASE;
                     ,m.CLFASCOS;
                     ,m.CLFASCLA;
                     ,m.CLMAGWIP;
                     ,m.CLSEOLFA;
                     ,m.FLTSTAOL;
                     ,m.FLQTAEVA;
                     ,m.FLTPROVE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                bTrsErr=bErr_03C098C0
                * --- Write into TMPODL_CICL
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CLROWNUM ="+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
                  +",CLROWORD ="+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
                  +",CLINDPRE ="+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
                  +",CL__FASE ="+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
                  +",CLDESFAS ="+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
                  +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
                  +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
                  +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
                  +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
                  +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
                  +",CLFASSEL ="+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
                  +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
                  +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
                  +",CLDATINI ="+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
                  +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
                  +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
                  +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
                  +",CLWIPDES ="+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
                  +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
                  +",CLCODERR ="+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
                  +",CLLTFASE ="+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
                  +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
                  +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
                  +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
                  +",CLSEOLFA ="+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
                  +",FLTSTAOL ="+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
                  +",FLQTAEVA ="+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
                  +",FLTPROVE ="+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
                      +i_ccchkf ;
                  +" where ";
                      +"CLKEYRIF = "+cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF);
                      +" and CLCODODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(M.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      CLROWNUM = M.CLROWNUM;
                      ,CLROWORD = M.CLROWORD;
                      ,CLINDPRE = m.CLINDPRE;
                      ,CL__FASE = m.CL__FASE;
                      ,CLDESFAS = m.CLDESFAS;
                      ,CLFASEVA = m.CLFASEVA;
                      ,CLCOUPOI = m.CLCOUPOI;
                      ,CLFASOUT = m.CLFASOUT;
                      ,CLPRIFAS = m.CLPRIFAS;
                      ,CLULTFAS = m.CLULTFAS;
                      ,CLFASSEL = m.CLFASSEL;
                      ,CLCPRIFE = m.CLCPRIFE;
                      ,CLBFRIFE = m.CLBFRIFE;
                      ,CLDATINI = m.CLDATINI;
                      ,CLDATFIN = m.CLDATFIN;
                      ,CLWIPOUT = m.CLWIPOUT;
                      ,CLCODFAS = m.CLCODFAS;
                      ,CLWIPDES = m.CLWIPDES;
                      ,CLWIPFPR = m.CLWIPFPR;
                      ,CLCODERR = m.CLCODERR;
                      ,CLLTFASE = m.CLLTFASE;
                      ,CLFASCOS = m.CLFASCOS;
                      ,CLFASCLA = m.CLFASCLA;
                      ,CLMAGWIP = m.CLMAGWIP;
                      ,CLSEOLFA = m.CLSEOLFA;
                      ,FLTSTAOL = m.FLTSTAOL;
                      ,FLQTAEVA = m.FLQTAEVA;
                      ,FLTPROVE = m.FLTPROVE;
                      &i_ccchkf. ;
                   where;
                      CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF;
                      and CLCODODL = this.w_GSCO_AOP.w_OLCODODL;
                      and CPROWNUM = M.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            else
              * --- Write into TMPODL_CICL
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CLROWNUM ="+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
                +",CLROWORD ="+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
                +",CLINDPRE ="+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
                +",CL__FASE ="+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
                +",CLDESFAS ="+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
                +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
                +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
                +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
                +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
                +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
                +",CLFASSEL ="+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
                +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
                +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
                +",CLDATINI ="+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
                +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
                +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
                +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
                +",CLWIPDES ="+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
                +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
                +",CLCODERR ="+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
                +",CLLTFASE ="+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
                +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
                +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
                +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
                +",CLSEOLFA ="+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
                +",FLTSTAOL ="+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
                +",FLQTAEVA ="+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
                +",FLTPROVE ="+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
                    +i_ccchkf ;
                +" where ";
                    +"CLKEYRIF = "+cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF);
                    +" and CLCODODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
                    +" and CPROWNUM = "+cp_ToStrODBC(M.CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    CLROWNUM = M.CLROWNUM;
                    ,CLROWORD = M.CLROWORD;
                    ,CLINDPRE = m.CLINDPRE;
                    ,CL__FASE = m.CL__FASE;
                    ,CLDESFAS = m.CLDESFAS;
                    ,CLFASEVA = m.CLFASEVA;
                    ,CLCOUPOI = m.CLCOUPOI;
                    ,CLFASOUT = m.CLFASOUT;
                    ,CLPRIFAS = m.CLPRIFAS;
                    ,CLULTFAS = m.CLULTFAS;
                    ,CLFASSEL = m.CLFASSEL;
                    ,CLCPRIFE = m.CLCPRIFE;
                    ,CLBFRIFE = m.CLBFRIFE;
                    ,CLDATINI = m.CLDATINI;
                    ,CLDATFIN = m.CLDATFIN;
                    ,CLWIPOUT = m.CLWIPOUT;
                    ,CLCODFAS = m.CLCODFAS;
                    ,CLWIPDES = m.CLWIPDES;
                    ,CLWIPFPR = m.CLWIPFPR;
                    ,CLCODERR = m.CLCODERR;
                    ,CLLTFASE = m.CLLTFASE;
                    ,CLFASCOS = m.CLFASCOS;
                    ,CLFASCLA = m.CLFASCLA;
                    ,CLMAGWIP = m.CLMAGWIP;
                    ,CLSEOLFA = m.CLSEOLFA;
                    ,FLTSTAOL = m.FLTSTAOL;
                    ,FLQTAEVA = m.FLQTAEVA;
                    ,FLTPROVE = m.FLTPROVE;
                    &i_ccchkf. ;
                 where;
                    CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF;
                    and CLCODODL = this.w_GSCO_AOP.w_OLCODODL;
                    and CPROWNUM = M.CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if i_Rows=0
                * --- Insert into TMPODL_CICL
                i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_CICL_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"CLKEYRIF"+",CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CL__FASE"+",CLDESFAS"+",CLFASEVA"+",CLCOUPOI"+",CLFASOUT"+",CLPRIFAS"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLBFRIFE"+",CLDATINI"+",CLDATFIN"+",CLWIPOUT"+",CLCODFAS"+",CLWIPDES"+",CLWIPFPR"+",CLCODERR"+",CLLTFASE"+",CLFASCOS"+",CLFASCLA"+",CLMAGWIP"+",CLSEOLFA"+",FLTSTAOL"+",FLQTAEVA"+",FLTPROVE"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_CICL','CLKEYRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_CICL','CLCODODL');
                  +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_CICL','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
                  +","+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
                  +","+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
                  +","+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
                  +","+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
                  +","+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'CLCODODL',this.w_GSCO_AOP.w_OLCODODL,'CPROWNUM',M.CPROWNUM,'CLROWNUM',M.CLROWNUM,'CLROWORD',M.CLROWORD,'CLINDPRE',m.CLINDPRE,'CL__FASE',m.CL__FASE,'CLDESFAS',m.CLDESFAS,'CLFASEVA',m.CLFASEVA,'CLCOUPOI',m.CLCOUPOI,'CLFASOUT',m.CLFASOUT,'CLPRIFAS',m.CLPRIFAS)
                  insert into (i_cTable) (CLKEYRIF,CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CL__FASE,CLDESFAS,CLFASEVA,CLCOUPOI,CLFASOUT,CLPRIFAS,CLULTFAS,CLFASSEL,CLCPRIFE,CLBFRIFE,CLDATINI,CLDATFIN,CLWIPOUT,CLCODFAS,CLWIPDES,CLWIPFPR,CLCODERR,CLLTFASE,CLFASCOS,CLFASCLA,CLMAGWIP,CLSEOLFA,FLTSTAOL,FLQTAEVA,FLTPROVE &i_ccchkf. );
                     values (;
                       this.w_GSCO_AOP.w_CLKEYRIF;
                       ,this.w_GSCO_AOP.w_OLCODODL;
                       ,M.CPROWNUM;
                       ,M.CLROWNUM;
                       ,M.CLROWORD;
                       ,m.CLINDPRE;
                       ,m.CL__FASE;
                       ,m.CLDESFAS;
                       ,m.CLFASEVA;
                       ,m.CLCOUPOI;
                       ,m.CLFASOUT;
                       ,m.CLPRIFAS;
                       ,m.CLULTFAS;
                       ,m.CLFASSEL;
                       ,m.CLCPRIFE;
                       ,m.CLBFRIFE;
                       ,m.CLDATINI;
                       ,m.CLDATFIN;
                       ,m.CLWIPOUT;
                       ,m.CLCODFAS;
                       ,m.CLWIPDES;
                       ,m.CLWIPFPR;
                       ,m.CLCODERR;
                       ,m.CLLTFASE;
                       ,m.CLFASCOS;
                       ,m.CLFASCLA;
                       ,m.CLMAGWIP;
                       ,m.CLSEOLFA;
                       ,m.FLTSTAOL;
                       ,m.FLQTAEVA;
                       ,m.FLTPROVE;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            endif
          else
            * --- Inserisco nuovamente tutte le fasi significa che precedentemente era stato svuotato il temporaneo
            * --- Write into TMPODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLROWNUM ="+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
              +",CLROWORD ="+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
              +",CLINDPRE ="+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
              +",CL__FASE ="+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
              +",CLDESFAS ="+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
              +",CLFASEVA ="+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
              +",CLCOUPOI ="+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
              +",CLFASOUT ="+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
              +",CLPRIFAS ="+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
              +",CLULTFAS ="+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
              +",CLFASSEL ="+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
              +",CLCPRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
              +",CLBFRIFE ="+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
              +",CLDATINI ="+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
              +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
              +",CLWIPOUT ="+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
              +",CLCODFAS ="+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
              +",CLWIPDES ="+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
              +",CLWIPFPR ="+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
              +",CLCODERR ="+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
              +",CLLTFASE ="+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
              +",CLFASCOS ="+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
              +",CLFASCLA ="+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
              +",CLMAGWIP ="+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
              +",CLSEOLFA ="+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
              +",FLTSTAOL ="+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
              +",FLQTAEVA ="+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
              +",FLTPROVE ="+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
                  +i_ccchkf ;
              +" where ";
                  +"CLKEYRIF = "+cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF);
                  +" and CLCODODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(M.CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  CLROWNUM = M.CLROWNUM;
                  ,CLROWORD = M.CLROWORD;
                  ,CLINDPRE = m.CLINDPRE;
                  ,CL__FASE = m.CL__FASE;
                  ,CLDESFAS = m.CLDESFAS;
                  ,CLFASEVA = m.CLFASEVA;
                  ,CLCOUPOI = m.CLCOUPOI;
                  ,CLFASOUT = m.CLFASOUT;
                  ,CLPRIFAS = m.CLPRIFAS;
                  ,CLULTFAS = m.CLULTFAS;
                  ,CLFASSEL = m.CLFASSEL;
                  ,CLCPRIFE = m.CLCPRIFE;
                  ,CLBFRIFE = m.CLBFRIFE;
                  ,CLDATINI = m.CLDATINI;
                  ,CLDATFIN = m.CLDATFIN;
                  ,CLWIPOUT = m.CLWIPOUT;
                  ,CLCODFAS = m.CLCODFAS;
                  ,CLWIPDES = m.CLWIPDES;
                  ,CLWIPFPR = m.CLWIPFPR;
                  ,CLCODERR = m.CLCODERR;
                  ,CLLTFASE = m.CLLTFASE;
                  ,CLFASCOS = m.CLFASCOS;
                  ,CLFASCLA = m.CLFASCLA;
                  ,CLMAGWIP = m.CLMAGWIP;
                  ,CLSEOLFA = m.CLSEOLFA;
                  ,FLTSTAOL = m.FLTSTAOL;
                  ,FLQTAEVA = m.FLQTAEVA;
                  ,FLTPROVE = m.FLTPROVE;
                  &i_ccchkf. ;
               where;
                  CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF;
                  and CLCODODL = this.w_GSCO_AOP.w_OLCODODL;
                  and CPROWNUM = M.CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Try
              local bErr_03C0D700
              bErr_03C0D700=bTrsErr
              this.Try_03C0D700()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03C0D700
              * --- End
            endif
          endif
          SELECT(this.w_TRSODLTCICL)
          ENDSCAN
          USE IN SELECT(this.w_TRSODLTCICL)
          SELECT(this.w_TRSODL_RISO)
          SCAN
          SCATTER MEMVAR
          * --- Delete from TMPODL_RISO
          i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CLKEYRIF = "+cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF);
                  +" and RLCODODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
                  +" and RLROWNUM = "+cp_ToStrODBC(M.RLROWNUM);
                  +" and CPROWNUM = "+cp_ToStrODBC(M.CPROWNUM);
                   )
          else
            delete from (i_cTable) where;
                  CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF;
                  and RLCODODL = this.w_GSCO_AOP.w_OLCODODL;
                  and RLROWNUM = M.RLROWNUM;
                  and CPROWNUM = M.CPROWNUM;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Try
          local bErr_03C03F50
          bErr_03C03F50=bTrsErr
          this.Try_03C03F50()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03C03F50
          * --- End
          ENDSCAN
          USE IN SELECT(this.w_TRSODL_RISO)
        else
          SELECT(this.w_TRSODLTCICL)
          SCAN
          SCATTER MEMVAR
          * --- Try
          local bErr_03C2E040
          bErr_03C2E040=bTrsErr
          this.Try_03C2E040()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03C2E040
          * --- End
          SELECT(this.w_TRSODLTCICL)
          ENDSCAN
          USE IN SELECT(this.w_TRSODLTCICL)
          SELECT(this.w_TRSODL_RISO)
          SCAN
          SCATTER MEMVAR
          * --- Try
          local bErr_03C10C40
          bErr_03C10C40=bTrsErr
          this.Try_03C10C40()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03C10C40
          * --- End
          ENDSCAN
          USE IN SELECT(this.w_TRSODL_RISO)
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- -Metto nello storico i dati trattati per confrontarli alla prossima modifica
        SELECT(this.w_GSCO_AOP.w_Cur_Cic_Odl)
        ZAP
        APPEND FROM DBF(this.w_TRSODL_CICL)
        * --- Aggiorno la provenienza della fase (Interna/conto lavoro)
        this.w_CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF
        USE IN SELECT(this.w_TRSODL_CICL)
        USE IN SELECT(this.w_TRSODL_RISO)
        this.w_GSCO_MCL.w_UPDATED = .T.
        this.w_GSCO_AOP.LockScreen = this.w_LockScreen
        this.w_GSCO_AOP.Refresh()     
      endif
    endif
  endproc
  proc Try_03C0D700()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPODL_CICL
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLKEYRIF"+",CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CL__FASE"+",CLDESFAS"+",CLFASEVA"+",CLCOUPOI"+",CLFASOUT"+",CLPRIFAS"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLBFRIFE"+",CLDATINI"+",CLDATFIN"+",CLWIPOUT"+",CLCODFAS"+",CLWIPDES"+",CLWIPFPR"+",CLCODERR"+",CLLTFASE"+",CLFASCOS"+",CLFASCLA"+",CLMAGWIP"+",CLSEOLFA"+",FLTSTAOL"+",FLQTAEVA"+",FLTPROVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_CICL','CLKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_CICL','CLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_CICL','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
      +","+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
      +","+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
      +","+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
      +","+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
      +","+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
      +","+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
      +","+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
      +","+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
      +","+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'CLCODODL',this.w_GSCO_AOP.w_OLCODODL,'CPROWNUM',M.CPROWNUM,'CLROWNUM',M.CLROWNUM,'CLROWORD',M.CLROWORD,'CLINDPRE',m.CLINDPRE,'CL__FASE',m.CL__FASE,'CLDESFAS',m.CLDESFAS,'CLFASEVA',m.CLFASEVA,'CLCOUPOI',m.CLCOUPOI,'CLFASOUT',m.CLFASOUT,'CLPRIFAS',m.CLPRIFAS)
      insert into (i_cTable) (CLKEYRIF,CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CL__FASE,CLDESFAS,CLFASEVA,CLCOUPOI,CLFASOUT,CLPRIFAS,CLULTFAS,CLFASSEL,CLCPRIFE,CLBFRIFE,CLDATINI,CLDATFIN,CLWIPOUT,CLCODFAS,CLWIPDES,CLWIPFPR,CLCODERR,CLLTFASE,CLFASCOS,CLFASCLA,CLMAGWIP,CLSEOLFA,FLTSTAOL,FLQTAEVA,FLTPROVE &i_ccchkf. );
         values (;
           this.w_GSCO_AOP.w_CLKEYRIF;
           ,this.w_GSCO_AOP.w_OLCODODL;
           ,M.CPROWNUM;
           ,M.CLROWNUM;
           ,M.CLROWORD;
           ,m.CLINDPRE;
           ,m.CL__FASE;
           ,m.CLDESFAS;
           ,m.CLFASEVA;
           ,m.CLCOUPOI;
           ,m.CLFASOUT;
           ,m.CLPRIFAS;
           ,m.CLULTFAS;
           ,m.CLFASSEL;
           ,m.CLCPRIFE;
           ,m.CLBFRIFE;
           ,m.CLDATINI;
           ,m.CLDATFIN;
           ,m.CLWIPOUT;
           ,m.CLCODFAS;
           ,m.CLWIPDES;
           ,m.CLWIPFPR;
           ,m.CLCODERR;
           ,m.CLLTFASE;
           ,m.CLFASCOS;
           ,m.CLFASCLA;
           ,m.CLMAGWIP;
           ,m.CLSEOLFA;
           ,m.FLTSTAOL;
           ,m.FLQTAEVA;
           ,m.FLTPROVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03C03F50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPODL_RISO
    i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLKEYRIF"+",RLCODODL"+",RLROWNUM"+",CPROWNUM"+",CPROWORD"+",RLCODCLA"+",RL__TIPO"+",RLCODICE"+",RLQTARIS"+",RLUMTRIS"+",RLTEMRIS"+",RLTEMSEC"+",RLTPREVS"+",RLTPRRIS"+",RLTPRRIU"+",RLTPSTDS"+",RLQPSRIS"+",RLTPSRIS"+",RLTPSRIU"+",RLTCONSS"+",RLTCORIS"+",RLTCOEXT"+",RLCODPAD"+",RLNUMIMP"+",RLPROORA"+",RLTMPCIC"+",RLTMPSEC"+",RLTIPTEM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_RISO','CLKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_RISO','RLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(M.RLROWNUM),'TMPODL_RISO','RLROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_RISO','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWORD),'TMPODL_RISO','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODCLA),'TMPODL_RISO','RLCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(M.RL__TIPO),'TMPODL_RISO','RL__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODICE),'TMPODL_RISO','RLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(M.RLQTARIS),'TMPODL_RISO','RLQTARIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLUMTRIS),'TMPODL_RISO','RLUMTRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTEMRIS),'TMPODL_RISO','RLTEMRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTEMSEC),'TMPODL_RISO','RLTEMSEC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPREVS),'TMPODL_RISO','RLTPREVS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPRRIS),'TMPODL_RISO','RLTPRRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPRRIU),'TMPODL_RISO','RLTPRRIU');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSTDS),'TMPODL_RISO','RLTPSTDS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLQPSRIS),'TMPODL_RISO','RLQPSRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSRIS),'TMPODL_RISO','RLTPSRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSRIU),'TMPODL_RISO','RLTPSRIU');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCONSS),'TMPODL_RISO','RLTCONSS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCORIS),'TMPODL_RISO','RLTCORIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCOEXT),'TMPODL_RISO','RLTCOEXT');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODPAD),'TMPODL_RISO','RLCODPAD');
      +","+cp_NullLink(cp_ToStrODBC(M.RLNUMIMP),'TMPODL_RISO','RLNUMIMP');
      +","+cp_NullLink(cp_ToStrODBC(M.RLPROORA),'TMPODL_RISO','RLPROORA');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTMPCIC),'TMPODL_RISO','RLTMPCIC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTMPSEC),'TMPODL_RISO','RLTMPSEC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTIPTEM),'TMPODL_RISO','RLTIPTEM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'RLCODODL',this.w_GSCO_AOP.w_OLCODODL,'RLROWNUM',M.RLROWNUM,'CPROWNUM',M.CPROWNUM,'CPROWORD',M.CPROWORD,'RLCODCLA',M.RLCODCLA,'RL__TIPO',M.RL__TIPO,'RLCODICE',M.RLCODICE,'RLQTARIS',M.RLQTARIS,'RLUMTRIS',M.RLUMTRIS,'RLTEMRIS',M.RLTEMRIS,'RLTEMSEC',M.RLTEMSEC)
      insert into (i_cTable) (CLKEYRIF,RLCODODL,RLROWNUM,CPROWNUM,CPROWORD,RLCODCLA,RL__TIPO,RLCODICE,RLQTARIS,RLUMTRIS,RLTEMRIS,RLTEMSEC,RLTPREVS,RLTPRRIS,RLTPRRIU,RLTPSTDS,RLQPSRIS,RLTPSRIS,RLTPSRIU,RLTCONSS,RLTCORIS,RLTCOEXT,RLCODPAD,RLNUMIMP,RLPROORA,RLTMPCIC,RLTMPSEC,RLTIPTEM &i_ccchkf. );
         values (;
           this.w_GSCO_AOP.w_CLKEYRIF;
           ,this.w_GSCO_AOP.w_OLCODODL;
           ,M.RLROWNUM;
           ,M.CPROWNUM;
           ,M.CPROWORD;
           ,M.RLCODCLA;
           ,M.RL__TIPO;
           ,M.RLCODICE;
           ,M.RLQTARIS;
           ,M.RLUMTRIS;
           ,M.RLTEMRIS;
           ,M.RLTEMSEC;
           ,M.RLTPREVS;
           ,M.RLTPRRIS;
           ,M.RLTPRRIU;
           ,M.RLTPSTDS;
           ,M.RLQPSRIS;
           ,M.RLTPSRIS;
           ,M.RLTPSRIU;
           ,M.RLTCONSS;
           ,M.RLTCORIS;
           ,M.RLTCOEXT;
           ,M.RLCODPAD;
           ,M.RLNUMIMP;
           ,M.RLPROORA;
           ,M.RLTMPCIC;
           ,M.RLTMPSEC;
           ,M.RLTIPTEM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03C2E040()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPODL_CICL
    i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLKEYRIF"+",CLCODODL"+",CPROWNUM"+",CLROWNUM"+",CLROWORD"+",CLINDPRE"+",CL__FASE"+",CLDESFAS"+",CLFASEVA"+",CLCOUPOI"+",CLFASOUT"+",CLPRIFAS"+",CLULTFAS"+",CLFASSEL"+",CLCPRIFE"+",CLBFRIFE"+",CLDATINI"+",CLDATFIN"+",CLWIPOUT"+",CLCODFAS"+",CLWIPDES"+",CLWIPFPR"+",CLCODERR"+",CLLTFASE"+",CLFASCOS"+",CLFASCLA"+",CLMAGWIP"+",CLSEOLFA"+",FLTSTAOL"+",FLQTAEVA"+",FLTPROVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_CICL','CLKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_CICL','CLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_CICL','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CLROWNUM),'TMPODL_CICL','CLROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CLROWORD),'TMPODL_CICL','CLROWORD');
      +","+cp_NullLink(cp_ToStrODBC(m.CLINDPRE),'TMPODL_CICL','CLINDPRE');
      +","+cp_NullLink(cp_ToStrODBC(m.CL__FASE),'TMPODL_CICL','CL__FASE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDESFAS),'TMPODL_CICL','CLDESFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASEVA),'TMPODL_CICL','CLFASEVA');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCOUPOI),'TMPODL_CICL','CLCOUPOI');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASOUT),'TMPODL_CICL','CLFASOUT');
      +","+cp_NullLink(cp_ToStrODBC(m.CLPRIFAS),'TMPODL_CICL','CLPRIFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLULTFAS),'TMPODL_CICL','CLULTFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASSEL),'TMPODL_CICL','CLFASSEL');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCPRIFE),'TMPODL_CICL','CLCPRIFE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLBFRIFE),'TMPODL_CICL','CLBFRIFE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDATINI),'TMPODL_CICL','CLDATINI');
      +","+cp_NullLink(cp_ToStrODBC(m.CLDATFIN),'TMPODL_CICL','CLDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPOUT),'TMPODL_CICL','CLWIPOUT');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCODFAS),'TMPODL_CICL','CLCODFAS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPDES),'TMPODL_CICL','CLWIPDES');
      +","+cp_NullLink(cp_ToStrODBC(m.CLWIPFPR),'TMPODL_CICL','CLWIPFPR');
      +","+cp_NullLink(cp_ToStrODBC(m.CLCODERR),'TMPODL_CICL','CLCODERR');
      +","+cp_NullLink(cp_ToStrODBC(m.CLLTFASE),'TMPODL_CICL','CLLTFASE');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASCOS),'TMPODL_CICL','CLFASCOS');
      +","+cp_NullLink(cp_ToStrODBC(m.CLFASCLA),'TMPODL_CICL','CLFASCLA');
      +","+cp_NullLink(cp_ToStrODBC(m.CLMAGWIP),'TMPODL_CICL','CLMAGWIP');
      +","+cp_NullLink(cp_ToStrODBC(m.CLSEOLFA),'TMPODL_CICL','CLSEOLFA');
      +","+cp_NullLink(cp_ToStrODBC(m.FLTSTAOL),'TMPODL_CICL','FLTSTAOL');
      +","+cp_NullLink(cp_ToStrODBC(m.FLQTAEVA),'TMPODL_CICL','FLQTAEVA');
      +","+cp_NullLink(cp_ToStrODBC(m.FLTPROVE),'TMPODL_CICL','FLTPROVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'CLCODODL',this.w_GSCO_AOP.w_OLCODODL,'CPROWNUM',M.CPROWNUM,'CLROWNUM',M.CLROWNUM,'CLROWORD',M.CLROWORD,'CLINDPRE',m.CLINDPRE,'CL__FASE',m.CL__FASE,'CLDESFAS',m.CLDESFAS,'CLFASEVA',m.CLFASEVA,'CLCOUPOI',m.CLCOUPOI,'CLFASOUT',m.CLFASOUT,'CLPRIFAS',m.CLPRIFAS)
      insert into (i_cTable) (CLKEYRIF,CLCODODL,CPROWNUM,CLROWNUM,CLROWORD,CLINDPRE,CL__FASE,CLDESFAS,CLFASEVA,CLCOUPOI,CLFASOUT,CLPRIFAS,CLULTFAS,CLFASSEL,CLCPRIFE,CLBFRIFE,CLDATINI,CLDATFIN,CLWIPOUT,CLCODFAS,CLWIPDES,CLWIPFPR,CLCODERR,CLLTFASE,CLFASCOS,CLFASCLA,CLMAGWIP,CLSEOLFA,FLTSTAOL,FLQTAEVA,FLTPROVE &i_ccchkf. );
         values (;
           this.w_GSCO_AOP.w_CLKEYRIF;
           ,this.w_GSCO_AOP.w_OLCODODL;
           ,M.CPROWNUM;
           ,M.CLROWNUM;
           ,M.CLROWORD;
           ,m.CLINDPRE;
           ,m.CL__FASE;
           ,m.CLDESFAS;
           ,m.CLFASEVA;
           ,m.CLCOUPOI;
           ,m.CLFASOUT;
           ,m.CLPRIFAS;
           ,m.CLULTFAS;
           ,m.CLFASSEL;
           ,m.CLCPRIFE;
           ,m.CLBFRIFE;
           ,m.CLDATINI;
           ,m.CLDATFIN;
           ,m.CLWIPOUT;
           ,m.CLCODFAS;
           ,m.CLWIPDES;
           ,m.CLWIPFPR;
           ,m.CLCODERR;
           ,m.CLLTFASE;
           ,m.CLFASCOS;
           ,m.CLFASCLA;
           ,m.CLMAGWIP;
           ,m.CLSEOLFA;
           ,m.FLTSTAOL;
           ,m.FLQTAEVA;
           ,m.FLTPROVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03C10C40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPODL_RISO
    i_nConn=i_TableProp[this.TMPODL_RISO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_RISO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPODL_RISO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLKEYRIF"+",RLCODODL"+",RLROWNUM"+",CPROWNUM"+",CPROWORD"+",RLCODCLA"+",RL__TIPO"+",RLCODICE"+",RLQTARIS"+",RLUMTRIS"+",RLTEMRIS"+",RLTEMSEC"+",RLTPREVS"+",RLTPRRIS"+",RLTPRRIU"+",RLTPSTDS"+",RLQPSRIS"+",RLTPSRIS"+",RLTPSRIU"+",RLTCONSS"+",RLTCORIS"+",RLTCOEXT"+",RLCODPAD"+",RLNUMIMP"+",RLPROORA"+",RLTMPCIC"+",RLTMPSEC"+",RLTIPTEM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF),'TMPODL_RISO','CLKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL),'TMPODL_RISO','RLCODODL');
      +","+cp_NullLink(cp_ToStrODBC(M.RLROWNUM),'TMPODL_RISO','RLROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWNUM),'TMPODL_RISO','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(M.CPROWORD),'TMPODL_RISO','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODCLA),'TMPODL_RISO','RLCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(M.RL__TIPO),'TMPODL_RISO','RL__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODICE),'TMPODL_RISO','RLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(M.RLQTARIS),'TMPODL_RISO','RLQTARIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLUMTRIS),'TMPODL_RISO','RLUMTRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTEMRIS),'TMPODL_RISO','RLTEMRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTEMSEC),'TMPODL_RISO','RLTEMSEC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPREVS),'TMPODL_RISO','RLTPREVS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPRRIS),'TMPODL_RISO','RLTPRRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPRRIU),'TMPODL_RISO','RLTPRRIU');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSTDS),'TMPODL_RISO','RLTPSTDS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLQPSRIS),'TMPODL_RISO','RLQPSRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSRIS),'TMPODL_RISO','RLTPSRIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTPSRIU),'TMPODL_RISO','RLTPSRIU');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCONSS),'TMPODL_RISO','RLTCONSS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCORIS),'TMPODL_RISO','RLTCORIS');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTCOEXT),'TMPODL_RISO','RLTCOEXT');
      +","+cp_NullLink(cp_ToStrODBC(M.RLCODPAD),'TMPODL_RISO','RLCODPAD');
      +","+cp_NullLink(cp_ToStrODBC(M.RLNUMIMP),'TMPODL_RISO','RLNUMIMP');
      +","+cp_NullLink(cp_ToStrODBC(M.RLPROORA),'TMPODL_RISO','RLPROORA');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTMPCIC),'TMPODL_RISO','RLTMPCIC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTMPSEC),'TMPODL_RISO','RLTMPSEC');
      +","+cp_NullLink(cp_ToStrODBC(M.RLTIPTEM),'TMPODL_RISO','RLTIPTEM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLKEYRIF',this.w_GSCO_AOP.w_CLKEYRIF,'RLCODODL',this.w_GSCO_AOP.w_OLCODODL,'RLROWNUM',M.RLROWNUM,'CPROWNUM',M.CPROWNUM,'CPROWORD',M.CPROWORD,'RLCODCLA',M.RLCODCLA,'RL__TIPO',M.RL__TIPO,'RLCODICE',M.RLCODICE,'RLQTARIS',M.RLQTARIS,'RLUMTRIS',M.RLUMTRIS,'RLTEMRIS',M.RLTEMRIS,'RLTEMSEC',M.RLTEMSEC)
      insert into (i_cTable) (CLKEYRIF,RLCODODL,RLROWNUM,CPROWNUM,CPROWORD,RLCODCLA,RL__TIPO,RLCODICE,RLQTARIS,RLUMTRIS,RLTEMRIS,RLTEMSEC,RLTPREVS,RLTPRRIS,RLTPRRIU,RLTPSTDS,RLQPSRIS,RLTPSRIS,RLTPSRIU,RLTCONSS,RLTCORIS,RLTCOEXT,RLCODPAD,RLNUMIMP,RLPROORA,RLTMPCIC,RLTMPSEC,RLTIPTEM &i_ccchkf. );
         values (;
           this.w_GSCO_AOP.w_CLKEYRIF;
           ,this.w_GSCO_AOP.w_OLCODODL;
           ,M.RLROWNUM;
           ,M.CPROWNUM;
           ,M.CPROWORD;
           ,M.RLCODCLA;
           ,M.RL__TIPO;
           ,M.RLCODICE;
           ,M.RLQTARIS;
           ,M.RLUMTRIS;
           ,M.RLTEMRIS;
           ,M.RLTEMSEC;
           ,M.RLTPREVS;
           ,M.RLTPRRIS;
           ,M.RLTPRRIU;
           ,M.RLTPSTDS;
           ,M.RLQPSRIS;
           ,M.RLTPSRIS;
           ,M.RLTPSRIU;
           ,M.RLTCONSS;
           ,M.RLTCORIS;
           ,M.RLTCOEXT;
           ,M.RLCODPAD;
           ,M.RLNUMIMP;
           ,M.RLPROORA;
           ,M.RLTMPCIC;
           ,M.RLTMPSEC;
           ,M.RLTIPTEM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zoom gestione fase di utilizzo
    * --- Carica temporaneo - genera lista materiali ODS
    * --- Zoom fase ODL - azzero le variabili
    this.w_CPROWNUM = 0
    this.w_CLROWNUM = 0
    this.w_CLROWORD = 0
    this.w_FASEPREF = "00"
    this.w_CLINDPRE = "00"
    if this.w_OPERAZ = "ZOOMFASE"
      this.w_CLROWNUM = 0
      this.w_CLROWORD = 0
      vx_exec("..\PRFA\EXE\QUERY\GSCI_AMF.VZM",this)
      this.w_CLROWNUM = this.w_CPROWNUM
      this.oParentObject.w_OLFASRIF = this.w_CLROWORD
    endif
    if Inlist(this.w_GSCO_AOP.cFunction,"Edit","Load")
      * --- Read from TMPODL_CICL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TMPODL_CICL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPODL_CICL_idx,2],.t.,this.TMPODL_CICL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CLROWORD,CLCPRIFE,CLDESFAS,CPROWNUM"+;
          " from "+i_cTable+" TMPODL_CICL where ";
              +"CLKEYRIF = "+cp_ToStrODBC(this.w_GSCO_AOP.w_CLKEYRIF);
              +" and CLCODODL = "+cp_ToStrODBC(this.w_GSCO_AOP.w_OLCODODL);
              +" and CLROWORD = "+cp_ToStrODBC(this.oParentObject.w_OLFASRIF);
              +" and CLINDPRE = "+cp_ToStrODBC(this.w_CLINDPRE);
              +" and CLFASSEL = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CLROWORD,CLCPRIFE,CLDESFAS,CPROWNUM;
          from (i_cTable) where;
              CLKEYRIF = this.w_GSCO_AOP.w_CLKEYRIF;
              and CLCODODL = this.w_GSCO_AOP.w_OLCODODL;
              and CLROWORD = this.oParentObject.w_OLFASRIF;
              and CLINDPRE = this.w_CLINDPRE;
              and CLFASSEL = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_OLFASRIF = NVL(cp_ToDate(_read_.CLROWORD),cp_NullValue(_read_.CLROWORD))
        this.oParentObject.w_OLCPRIFE = NVL(cp_ToDate(_read_.CLCPRIFE),cp_NullValue(_read_.CLCPRIFE))
        this.oParentObject.w_DESFASE = NVL(cp_ToDate(_read_.CLDESFAS),cp_NullValue(_read_.CLDESFAS))
        this.w_CLROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_OPERAZ = "ZOOMFASE"
        if this.oParentObject.w_OLFASRIF <> this.oParentObject.o_OLFASRIF
          this.w_GSCO_MOL.Notifyevent("w_OLFASRIF Changed")     
          if this.oParentObject.w_OLFASRIF = 0
            this.w_GSCO_MOL.SaveDependsOn()     
            SELECT(this.w_GSCO_MOL.cTrsName)
            GATHER FROM this.w_GSCO_MOL.RowData
            this.w_GSCO_MOL.SetRow()     
            this.w_GSCO_MOL.SaveDependsOn()     
            this.w_GSCO_MOL.mEnableControls()     
          endif
          this.bUpdateParentObject = .f.
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if empty(this.w_ORDFAS)
      * --- Fase non collegata a ordini di conto lavoro di fase verifico se � stata dichiarata
      this.w_QTAVER = this.oParentObject.w_CLAVAUM1+this.oParentObject.w_CLSCAUM1
      if (this.oParentObject.w_OLFASRIF <> this.oParentObject.o_OLFASRIF and this.oParentObject.w_OLFASRIF > 0) Or this.w_OPERAZ="CONTROLLI"
        if this.w_QTAVER > 0 Or this.w_CLFASEVA = "S"
          ah_errorMsg("Impossibile associare il materiale alla fase [%1 - %2] la fase � stata dichiarata",48, "", alltrim(str(this.oParentObject.w_OLFASRIF)), alltrim(this.oParentObject.w_DESFASE))
          this.w_GSCO_MOL.SaveDependsOn()     
          SELECT(this.w_GSCO_MOL.cTrsName)
          GATHER FROM this.w_GSCO_MOL.RowData
          this.w_GSCO_MOL.SetRow()     
          this.w_GSCO_MOL.SaveDependsOn()     
          this.w_GSCO_MOL.mEnableControls()     
        endif
      else
        if this.oParentObject.w_OLFASRIF = 0
          ah_errorMsg("Attenzione! Fase non valida.",16, "")
          this.w_GSCO_MOL.SaveDependsOn()     
          SELECT(this.w_GSCO_MOL.cTrsName)
          GATHER FROM this.w_GSCO_MOL.RowData
          this.w_GSCO_MOL.SetRow()     
          this.w_GSCO_MOL.SaveDependsOn()     
          this.w_GSCO_MOL.mEnableControls()     
        endif
      endif
    else
      if this.w_STATOOCL $ "L-F"
        * --- Impossibile associare a fase esterna se � gi� stato generato l'OCL
        this.w_GSCO_MOL.SaveDependsOn()     
        SELECT(this.w_GSCO_MOL.cTrsName)
        GATHER FROM this.w_GSCO_MOL.RowData
        this.w_GSCO_MOL.SetRow()     
        this.w_GSCO_MOL.SaveDependsOn()     
        this.w_GSCO_MOL.mEnableControls()     
        ah_errorMsg("Impossibile associare a fase esterna se � gi� stato generato l'OCL",48)
        i_retcode = 'stop'
        return
      else
        this.GSCO_MOL = this.oParentObject
        this.GSCO_AOP = this.GSCO_MOL.oParentObject
        this.w_GSCO_MCL = this.GSCO_AOP.GSCO_MCL
        this.w_oMess.addMsgPartNL("Attenzione")     
        this.w_oMess.addMsgPartNL("Esistono ordini di contro lavoro di fase collegati che verranno eliminati")     
        this.w_oMess.addMsgPartNL("Si vuole continuare?")     
        this.w_FOUND = .F.
        this.w_L_CHECK = SYS(2015)
        L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
        if !Empty(L_Cur_Del_Ocl ) and Used( L_Cur_Del_Ocl )
          SELECT( L_Cur_Del_Ocl )
          GO TOP
          LOCATE FOR CLCODODL = this.w_ORDFAS
          this.w_FOUND = FOUND()
        endif
        if ! this.w_FOUND
          if this.w_oMess.ah_YesNo()
            this.GSCO_AOP.w_bOclfadadel = True
            if Empty(this.GSCO_AOP.w_Cur_Del_Ocl)
              this.GSCO_AOP.w_Cur_Del_Ocl = Sys(2015)
            endif
            * --- Assegno ad una variabile L_.. poich� devo utilizzare la Macro con il nome del Cursore 
            *     per gli OCL di fase da eliminare
            *     Creo il cursore contenente gli OCL
            L_Cur_Del_Ocl = this.GSCO_AOP.w_Cur_Del_Ocl
            if Not Used( L_Cur_Del_Ocl )
              CREATE CURSOR ( L_Cur_Del_Ocl ) ( CLCODODL C(15) , TIPO C(1), CCHECK C(10) )
              =wrcursor( L_Cur_Del_Ocl )
            endif
            INSERT INTO ( L_Cur_Del_Ocl ) VALUES (this.w_ORDFAS, SPACE(1), this.w_L_CHECK)
            * --- Notifico che posso eliminare la riga alla w_GSCO_MCL
            this.w_GSCO_MCL.w_CANDELROW = True
            * --- A questo punto libero i materiali presenti sull'ordine associati alla fase di conto lavoro
            this.w_LockScreen = this.w_GSCO_AOP.LockScreen
            this.w_GSCO_AOP.LockScreen = .t.
            this.GSCO_MOL.MarkPos()     
            this.GSCO_MOL.FirstRow()     
            do while Not this.GSCO_MOL.Eof_Trs()
              this.GSCO_MOL.SetRow()     
              * --- Storicizzo nelle o_ altrimenti ho i valori dell'ultima riga storicizzata
              this.GSCO_MOL.SaveDependsOn()     
              if this.GSCO_MOL.FullRow() And this.GSCO_MOL.w_OLEVAAUT="S" And this.GSCO_MOL.w_OLFASRIF=this.w_ROWORDC
                * --- Read from ODL_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "OLEVAAUT"+;
                    " from "+i_cTable+" ODL_DETT where ";
                        +"OLCODODL = "+cp_ToStrODBC(this.GSCO_AOP.w_OLCODODL);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.GSCO_MOL.w_CPROWNUM);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    OLEVAAUT;
                    from (i_cTable) where;
                        OLCODODL = this.GSCO_AOP.w_OLCODODL;
                        and CPROWNUM = this.GSCO_MOL.w_CPROWNUM;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_OLEVAAUT = NVL(cp_ToDate(_read_.OLEVAAUT),cp_NullValue(_read_.OLEVAAUT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.GSCO_MOL.w_OLQTAEVA = 0
                this.GSCO_MOL.w_OLQTAEV1 = 0
                this.GSCO_MOL.w_OLFLEVAS = "N"
                this.GSCO_MOL.w_OLEVAAUT = "N"
                this.GSCO_MOL.w_OLQTASAL = this.GSCO_MOL.w_OLQTAUM1
                if this.GSCO_MOL.w_OLFASEXM<>"S" AND this.w_OLEVAAUT="S"
                  this.GSCO_MOL.w_OLFASEXM = "S"
                endif
                this.GSCO_MOL.mCalc(.t.)     
                this.GSCO_MOL.SaveRow()     
                this.GSCO_MOL.SetupdateRow()     
              endif
              this.GSCO_MOL.NextRow()     
            enddo
            this.GSCO_MOL.RePos(.T.)     
            this.w_GSCO_AOP.LockScreen = this.w_LockScreen
            this.w_GSCO_AOP.Refresh()     
          else
            this.w_GSCO_MCL.w_CANDELROW = False
            * --- Impossibile associare a fase esterna se � gi� stato generato l'OCL
            this.w_GSCO_MOL.SaveDependsOn()     
            SELECT(this.w_GSCO_MOL.cTrsName)
            GATHER FROM this.w_GSCO_MOL.RowData
            this.w_GSCO_MOL.SetRow()     
            this.w_GSCO_MOL.SaveDependsOn()     
            this.w_GSCO_MOL.mEnableControls()     
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_OPERAZ)
    this.w_OPERAZ=w_OPERAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='RIS_DETT'
    this.cWorkTables[2]='CIC_DETT'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='PAR_PROD'
    this.cWorkTables[8]='ART_PROD'
    this.cWorkTables[9]='CAM_AGAZ'
    this.cWorkTables[10]='ODL_DETT'
    this.cWorkTables[11]='SALDICOM'
    this.cWorkTables[12]='ODL_CICL'
    this.cWorkTables[13]='CON_MACL'
    this.cWorkTables[14]='*TMPODL_CICL'
    this.cWorkTables[15]='ODL_RISO'
    this.cWorkTables[16]='*TMPODL_RISO'
    this.cWorkTables[17]='ART_TEMP'
    return(this.OpenAllTables(17))

  proc CloseCursors()
    if used('_Curs_TMPODL_RISO')
      use in _Curs_TMPODL_RISO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAZ"
endproc
