* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_mcc                                                        *
*              Classe codice di fase                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_56]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-31                                                      *
* Last revis.: 2017-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsci_mcc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsci_mcc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsci_mcc")
  return

* --- Class definition
define class tgsci_mcc as StdPCForm
  Width  = 612
  Height = 216
  Top    = 10
  Left   = 10
  cComment = "Classe codice di fase"
  cPrg = "gsci_mcc"
  HelpContextID=97125737
  add object cnt as tcgsci_mcc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsci_mcc as PCContext
  w_CFCODICE = space(5)
  w_CFPOSIZI = 0
  w_CFETICHE = space(20)
  w_CFLUNGHE = 0
  w_CF_START = space(35)
  w_CF_START = space(35)
  w_CF__FUNC = space(1)
  w_TOTLUNGHE = 0
  w_HASEVENT = space(1)
  w_HASEVCOP = space(50)
  w_CFPROGRE = 0
  proc Save(i_oFrom)
    this.w_CFCODICE = i_oFrom.w_CFCODICE
    this.w_CFPOSIZI = i_oFrom.w_CFPOSIZI
    this.w_CFETICHE = i_oFrom.w_CFETICHE
    this.w_CFLUNGHE = i_oFrom.w_CFLUNGHE
    this.w_CF_START = i_oFrom.w_CF_START
    this.w_CF_START = i_oFrom.w_CF_START
    this.w_CF__FUNC = i_oFrom.w_CF__FUNC
    this.w_TOTLUNGHE = i_oFrom.w_TOTLUNGHE
    this.w_HASEVENT = i_oFrom.w_HASEVENT
    this.w_HASEVCOP = i_oFrom.w_HASEVCOP
    this.w_CFPROGRE = i_oFrom.w_CFPROGRE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CFCODICE = this.w_CFCODICE
    i_oTo.w_CFPOSIZI = this.w_CFPOSIZI
    i_oTo.w_CFETICHE = this.w_CFETICHE
    i_oTo.w_CFLUNGHE = this.w_CFLUNGHE
    i_oTo.w_CF_START = this.w_CF_START
    i_oTo.w_CF_START = this.w_CF_START
    i_oTo.w_CF__FUNC = this.w_CF__FUNC
    i_oTo.w_TOTLUNGHE = this.w_TOTLUNGHE
    i_oTo.w_HASEVENT = this.w_HASEVENT
    i_oTo.w_HASEVCOP = this.w_HASEVCOP
    i_oTo.w_CFPROGRE = this.w_CFPROGRE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsci_mcc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 612
  Height = 216
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-21"
  HelpContextID=97125737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CCF_DETT_IDX = 0
  cFile = "CCF_DETT"
  cKeySelect = "CFCODICE"
  cKeyWhere  = "CFCODICE=this.w_CFCODICE"
  cKeyDetail  = "CFCODICE=this.w_CFCODICE"
  cKeyWhereODBC = '"CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cKeyDetailWhereODBC = '"CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CCF_DETT.CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCF_DETT.CFPOSIZI'
  cPrg = "gsci_mcc"
  cComment = "Classe codice di fase"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFCODICE = space(5)
  w_CFPOSIZI = 0
  w_CFETICHE = space(20)
  w_CFLUNGHE = 0
  w_CF_START = space(35)
  w_CF_START = space(35)
  w_CF__FUNC = space(1)
  w_TOTLUNGHE = 0
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_CFPROGRE = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_mccPag1","gsci_mcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CCF_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCF_DETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCF_DETT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsci_mcc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CCF_DETT where CFCODICE=KeySet.CFCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2],this.bLoadRecFilter,this.CCF_DETT_IDX,"gsci_mcc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCF_DETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCF_DETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CCF_DETT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
      select * from (i_cTable) CCF_DETT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTLUNGHE = 0
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_CFCODICE = NVL(CFCODICE,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CCF_DETT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTLUNGHE = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CFPOSIZI = NVL(CFPOSIZI,0)
          .w_CFETICHE = NVL(CFETICHE,space(20))
          .w_CFLUNGHE = NVL(CFLUNGHE,0)
          .w_CF_START = NVL(CF_START,space(35))
          .w_CF_START = NVL(CF_START,space(35))
          .w_CF__FUNC = NVL(CF__FUNC,space(1))
          .w_CFPROGRE = NVL(CFPROGRE,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTLUNGHE = .w_TOTLUNGHE+.w_CFLUNGHE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CFCODICE=space(5)
      .w_CFPOSIZI=0
      .w_CFETICHE=space(20)
      .w_CFLUNGHE=0
      .w_CF_START=space(35)
      .w_CF_START=space(35)
      .w_CF__FUNC=space(1)
      .w_TOTLUNGHE=0
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_CFPROGRE=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        .w_CF__FUNC = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(8,10,.f.)
        .w_CFPROGRE = this.oparentobject.w_ULTPROG
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCF_DETT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CCF_DETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODICE,"CFCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CFPOSIZI N(4);
      ,t_CFETICHE C(20);
      ,t_CFLUNGHE N(2);
      ,t_CF_START C(35);
      ,t_CF__FUNC N(3);
      ,CPROWNUM N(10);
      ,t_CFPROGRE N(18);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mccbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1.controlsource=this.cTrsName+'.t_CFPOSIZI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCFETICHE_2_2.controlsource=this.cTrsName+'.t_CFETICHE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCFLUNGHE_2_3.controlsource=this.cTrsName+'.t_CFLUNGHE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.controlsource=this.cTrsName+'.t_CF_START'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.controlsource=this.cTrsName+'.t_CF_START'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.controlsource=this.cTrsName+'.t_CF__FUNC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(51)
    this.AddVLine(211)
    this.AddVLine(250)
    this.AddVLine(486)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2])
      *
      * insert into CCF_DETT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCF_DETT')
        i_extval=cp_InsertValODBCExtFlds(this,'CCF_DETT')
        i_cFldBody=" "+;
                  "(CFCODICE,CFPOSIZI,CFETICHE,CFLUNGHE,CF_START"+;
                  ",CF__FUNC,CFPROGRE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CFCODICE)+","+cp_ToStrODBC(this.w_CFPOSIZI)+","+cp_ToStrODBC(this.w_CFETICHE)+","+cp_ToStrODBC(this.w_CFLUNGHE)+","+cp_ToStrODBC(this.w_CF_START)+;
             ","+cp_ToStrODBC(this.w_CF__FUNC)+","+cp_ToStrODBC(this.w_CFPROGRE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCF_DETT')
        i_extval=cp_InsertValVFPExtFlds(this,'CCF_DETT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CFCODICE',this.w_CFCODICE)
        INSERT INTO (i_cTable) (;
                   CFCODICE;
                  ,CFPOSIZI;
                  ,CFETICHE;
                  ,CFLUNGHE;
                  ,CF_START;
                  ,CF__FUNC;
                  ,CFPROGRE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CFCODICE;
                  ,this.w_CFPOSIZI;
                  ,this.w_CFETICHE;
                  ,this.w_CFLUNGHE;
                  ,this.w_CF_START;
                  ,this.w_CF__FUNC;
                  ,this.w_CFPROGRE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_CFETICHE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CCF_DETT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CCF_DETT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CFETICHE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCF_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CCF_DETT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CFPOSIZI="+cp_ToStrODBC(this.w_CFPOSIZI)+;
                     ",CFETICHE="+cp_ToStrODBC(this.w_CFETICHE)+;
                     ",CFLUNGHE="+cp_ToStrODBC(this.w_CFLUNGHE)+;
                     ",CF_START="+cp_ToStrODBC(this.w_CF_START)+;
                     ",CF__FUNC="+cp_ToStrODBC(this.w_CF__FUNC)+;
                     ",CFPROGRE="+cp_ToStrODBC(this.w_CFPROGRE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CCF_DETT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CFPOSIZI=this.w_CFPOSIZI;
                     ,CFETICHE=this.w_CFETICHE;
                     ,CFLUNGHE=this.w_CFLUNGHE;
                     ,CF_START=this.w_CF_START;
                     ,CF__FUNC=this.w_CF__FUNC;
                     ,CFPROGRE=this.w_CFPROGRE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CFETICHE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CCF_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CFETICHE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCF_DETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCF_DETT_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CFPROGRE with this.w_CFPROGRE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCF_START_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCF_START_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1.value==this.w_CFPOSIZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1.value=this.w_CFPOSIZI
      replace t_CFPOSIZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFETICHE_2_2.value==this.w_CFETICHE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFETICHE_2_2.value=this.w_CFETICHE
      replace t_CFETICHE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFETICHE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFLUNGHE_2_3.value==this.w_CFLUNGHE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFLUNGHE_2_3.value=this.w_CFLUNGHE
      replace t_CFLUNGHE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFLUNGHE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.value==this.w_CF_START)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.value=this.w_CF_START
      replace t_CF_START with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.value==this.w_CF_START)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.value=this.w_CF_START
      replace t_CF_START with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.RadioValue()==this.w_CF__FUNC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.SetRadio()
      replace t_CF__FUNC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'CCF_DETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(Empty(.w_CFETICHE) or  !Empty(.w_CFETICHE) And .w_CFPOSIZI>0) and (NOT EMPTY(.w_CFETICHE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFPOSIZI_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Sequenza non specificata")
        case   not(Empty(.w_CFETICHE) or  !Empty(.w_CFETICHE) And .w_CFLUNGHE>0) and (NOT EMPTY(.w_CFETICHE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCFLUNGHE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Lunghezza non specificata")
        case   not((Len(alltrim(.w_CF_START)) <= .w_CFLUNGHE) and (not empty(.w_CF_START) or .w_CF__FUNC<>'N')) and (NOT EMPTY(.w_CFETICHE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore di partenza non specificato o lunghezza maggiore di quella indicata")
        case   not((Len(alltrim(.w_CF_START)) <= .w_CFLUNGHE)) and (.w_CF__FUNC='P') and (NOT EMPTY(.w_CFETICHE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF_START_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore di partenza non specificato o lunghezza maggiore di quella indicata")
      endcase
      if NOT EMPTY(.w_CFETICHE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CFETICHE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CFPOSIZI=0
      .w_CFETICHE=space(20)
      .w_CFLUNGHE=0
      .w_CF_START=space(35)
      .w_CF_START=space(35)
      .w_CF__FUNC=space(1)
      .w_CFPROGRE=0
      .DoRTCalc(1,6,.f.)
        .w_CF__FUNC = 'N'
      .DoRTCalc(8,10,.f.)
        .w_CFPROGRE = this.oparentobject.w_ULTPROG
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CFPOSIZI = t_CFPOSIZI
    this.w_CFETICHE = t_CFETICHE
    this.w_CFLUNGHE = t_CFLUNGHE
    this.w_CF_START = t_CF_START
    this.w_CF_START = t_CF_START
    this.w_CF__FUNC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.RadioValue(.t.)
    this.w_CFPROGRE = t_CFPROGRE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CFPOSIZI with this.w_CFPOSIZI
    replace t_CFETICHE with this.w_CFETICHE
    replace t_CFLUNGHE with this.w_CFLUNGHE
    replace t_CF_START with this.w_CF_START
    replace t_CF_START with this.w_CF_START
    replace t_CF__FUNC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCF__FUNC_2_6.ToRadio()
    replace t_CFPROGRE with this.w_CFPROGRE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTLUNGHE = .w_TOTLUNGHE-.w_cflunghe
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mccPag1 as StdContainer
  Width  = 608
  height = 216
  stdWidth  = 608
  stdheight = 216
  resizeXpos=418
  resizeYpos=162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=14, top=0, width=589,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CFPOSIZI",Label1="Seq.",Field2="CFETICHE",Label2="Etichetta",Field3="CFLUNGHE",Label3="Lung.",Field4="CF_START",Label4="",Field5="CF_START",Label5="Valore di partenza",Field6="CF__FUNC",Label6="Funzione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218965114

  add object oStr_1_4 as StdString with uid="KKNMUZENYQ",Visible=.t., Left=10, Top=220,;
    Alignment=0, Width=594, Height=18,;
    Caption="Attenzione il campo cf_start � sovrapposto (per immetere il progressivo unicamente come numero)"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=4,top=19,;
    width=585+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=5,top=20,width=584+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsci_mccBodyRow as CPBodyRowCnt
  Width=575
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCFPOSIZI_2_1 as StdTrsField with uid="CHZSUHJVVJ",rtseq=2,rtrep=.t.,;
    cFormVar="w_CFPOSIZI",value=0,;
    ToolTipText = "Sequenza",;
    HelpContextID = 122010257,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Sequenza non specificata",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  func oCFPOSIZI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Empty(.w_CFETICHE) or  !Empty(.w_CFETICHE) And .w_CFPOSIZI>0)
    endwith
    return bRes
  endfunc

  add object oCFETICHE_2_2 as StdTrsField with uid="NFGJWLZPUC",rtseq=3,rtrep=.t.,;
    cFormVar="w_CFETICHE",value=space(20),;
    ToolTipText = "Etichetta componente",;
    HelpContextID = 35558763,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=38, Top=0, InputMask=replicate('X',20)

  add object oCFLUNGHE_2_3 as StdTrsField with uid="RCWLJVVICY",rtseq=4,rtrep=.t.,;
    cFormVar="w_CFLUNGHE",value=0,;
    ToolTipText = "Lunghezza in caratteri",;
    HelpContextID = 108004715,;
    cTotal = "this.Parent.oContained.w_totlunghe", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza non specificata",;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=200, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oCFLUNGHE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Empty(.w_CFETICHE) or  !Empty(.w_CFETICHE) And .w_CFLUNGHE>0)
    endwith
    return bRes
  endfunc

  add object oCF_START_2_4 as StdTrsField with uid="PCPSQYSFJK",rtseq=5,rtrep=.t.,;
    cFormVar="w_CF_START",value=space(35),;
    ToolTipText = "Valore di partenza, o fisso della porzione",;
    HelpContextID = 13579642,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore di partenza non specificato o lunghezza maggiore di quella indicata",;
   bGlobalFont=.t.,;
    Height=17, Width=229, Left=238, Top=0, InputMask=replicate('X',35)

  func oCF_START_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CF__FUNC<>'N')
    endwith
    endif
  endfunc

  func oCF_START_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((Len(alltrim(.w_CF_START)) <= .w_CFLUNGHE) and (not empty(.w_CF_START) or .w_CF__FUNC<>'N'))
    endwith
    return bRes
  endfunc

  add object oCF_START_2_5 as StdTrsField with uid="PYLICREUWO",rtseq=6,rtrep=.t.,;
    cFormVar="w_CF_START",value=space(35),;
    ToolTipText = "Valore di partenza, o fisso della porzione",;
    HelpContextID = 13579642,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore di partenza non specificato o lunghezza maggiore di quella indicata",;
   bGlobalFont=.t.,;
    Height=17, Width=229, Left=238, Top=0, cSayPict=["9999999999999"], cGetPict=["9999999999999"], InputMask=replicate('X',35)

  func oCF_START_2_5.mCond()
    with this.Parent.oContained
      return (.w_CF__FUNC='P')
    endwith
  endfunc

  func oCF_START_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CF__FUNC='N')
    endwith
    endif
  endfunc

  func oCF_START_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((Len(alltrim(.w_CF_START)) <= .w_CFLUNGHE))
    endwith
    return bRes
  endfunc

  add object oCF__FUNC_2_6 as StdTrsCombo with uid="AOWJLFRSND",rtrep=.t.,;
    cFormVar="w_CF__FUNC", RowSource=""+"Articolo,"+"Distinta base,"+"Codice ciclo,"+"Seriale ciclo,"+"Fase,"+"Progressivo,"+"Nessuno" , ;
    ToolTipText = "Tipo di funzione da applicare per costruire la porzione",;
    HelpContextID = 201640599,;
    Height=21, Width=96, Left=474, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCF__FUNC_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CF__FUNC,&i_cF..t_CF__FUNC),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'D',;
    iif(xVal =3,'C',;
    iif(xVal =4,'S',;
    iif(xVal =5,'F',;
    iif(xVal =6,'P',;
    iif(xVal =7,'N',;
    space(1)))))))))
  endfunc
  func oCF__FUNC_2_6.GetRadio()
    this.Parent.oContained.w_CF__FUNC = this.RadioValue()
    return .t.
  endfunc

  func oCF__FUNC_2_6.ToRadio()
    this.Parent.oContained.w_CF__FUNC=trim(this.Parent.oContained.w_CF__FUNC)
    return(;
      iif(this.Parent.oContained.w_CF__FUNC=='A',1,;
      iif(this.Parent.oContained.w_CF__FUNC=='D',2,;
      iif(this.Parent.oContained.w_CF__FUNC=='C',3,;
      iif(this.Parent.oContained.w_CF__FUNC=='S',4,;
      iif(this.Parent.oContained.w_CF__FUNC=='F',5,;
      iif(this.Parent.oContained.w_CF__FUNC=='P',6,;
      iif(this.Parent.oContained.w_CF__FUNC=='N',7,;
      0))))))))
  endfunc

  func oCF__FUNC_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCFPOSIZI_2_1.When()
    return(.t.)
  proc oCFPOSIZI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCFPOSIZI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_mcc','CCF_DETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFCODICE=CCF_DETT.CFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
