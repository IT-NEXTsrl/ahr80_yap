* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bwf                                                        *
*              Elabora costi WIP-fase                                          *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-23                                                      *
* Last revis.: 2017-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bwf",oParentObject)
return(i_retval)

define class tgsci_bwf as StdBatch
  * --- Local variables
  Padre = .NULL.
  w_Listino = space(5)
  w_ESERC = space(4)
  w_NUMINV = space(6)
  w_COCODSAL = space(40)
  w_CLSERIAL = space(10)
  w_CPROWNUM = 0
  w_CORIFDIS = space(41)
  w_CPROWNUM1 = 0
  w_CLCODFAS = space(20)
  w_PRKEYSAL = space(20)
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCSGENE = 0
  w_PRCOSSTA = 0
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCUGENE = 0
  w_PRCMLAVO = 0
  w_PRCMMATE = 0
  w_PRCMGENE = 0
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_PRCLGENE = 0
  w_LOTMED = 0
  w_COEFF = 0
  w_STD = space(1)
  w_UTE = space(1)
  w_ULT = space(1)
  w_MED = space(1)
  w_LST = space(1)
  w_CSLAVO = 0
  w_CSGENE = 0
  w_VARIAR = space(20)
  w_MESS = space(10)
  w_COPREZZO = 0
  w_COCOSSET = 0
  w_VALCONT = space(3)
  w_CAO = 0
  w_CODICE = space(51)
  w_PREZUM = space(1)
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_LOTMEDFE = 0
  w_CLCODDIS = space(41)
  w_PRZSET = 0
  w_ORAUNI = space(1)
  * --- WorkFile variables
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i costi dei codici WIP/fase dei Dati Articolo (da GSCI_KDC)
    if not (this.oParentObject.w_CLISTINO OR this.oParentObject.w_STANDARD OR this.oParentObject.w_MEDIOPON OR this.oParentObject.w_ULTIMO OR this.oParentObject.w_UTENTE)
      ah_errormsg("Impostare almeno un costo da valorizzare",48)
      i_retcode = 'stop'
      return
    endif
    this.Padre = this.oParentObject
    this.oParentObject.w_Msg = ""
    this.Padre.oPgFrm.ActivePage = 2
    this.w_MESS = ""
    do case
      case not this.oParentObject.w_STANDARD and this.oParentObject.w_AGGSTAND="S"
        this.w_MESS = "� stato selezionato il criterio di aggiornamento standard, si vuole continuare?"
      case not this.oParentObject.w_ULTIMO and this.oParentObject.w_AGGSTAND="U"
        this.w_MESS = "� stato selezionato il criterio di aggiornamento ultimo costo, si vuole continuare?"
      case not this.oParentObject.w_MEDIOPON and this.oParentObject.w_AGGSTAND="M"
        this.w_MESS = "� stato selezionato il criterio di aggiornamento medio ponderato, si vuole continuare?"
      case not this.oParentObject.w_CLISTINO and this.oParentObject.w_AGGSTAND="L"
        this.w_MESS = "� stato selezionato il criterio di aggiornamento listino, si vuole continuare?"
    endcase
    if not empty(this.w_MESS) and not ah_YesNo(this.w_MESS)
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_STANDARD = this.oParentObject.w_STANDARD or this.oParentObject.w_AGGSTAND="S"
    this.oParentObject.w_ULTIMO = this.oParentObject.w_ULTIMO or this.oParentObject.w_AGGSTAND="U"
    this.oParentObject.w_MEDIOPON = this.oParentObject.w_MEDIOPON or this.oParentObject.w_AGGSTAND="M"
    this.oParentObject.w_CLISTINO = this.oParentObject.w_CLISTINO or this.oParentObject.w_AGGSTAND="L"
    this.w_STD = iif(this.oParentObject.w_STANDARD, "=", " ")
    this.w_UTE = iif(this.oParentObject.w_UTENTE, "=", " ")
    this.w_ULT = iif(this.oParentObject.w_ULTIMO, "=", " ")
    this.w_MED = iif(this.oParentObject.w_MEDIOPON, "=", " ")
    this.w_LST = iif(this.oParentObject.w_CLISTINO, "=", " ")
    AddMsgNL("Interrogazione dati...",this)
    * --- Cicli di Lavorazione
    vq_exec("..\prfa\exe\query\gsci_bwf", THIS, "Cic_lavo") 
 =wrcursor("Cic_lavo") 
 go bottom
    this.w_CLCODFAS = clcodfas
    this.w_CLSERIAL = clserial
    * --- Riporta i codici wip-fase nelle fasi non output
    do while not bof("Cic_lavo")
      skip -1
      if this.w_CLSERIAL=clserial and empty(clcodfas) and not empty(this.w_CLCODFAS)
        Replace clcodfas with this.w_CLCODFAS
      else
        this.w_CLCODFAS = clcodfas
      endif
      this.w_CLSERIAL = clserial
    enddo
    Select * from Cic_lavo where not empty(clcodfas) order by clcodfas into cursor Cos_lavo nofilter 
 use in Cic_lavo
    this.w_CLCODFAS = space(41)
    * --- Costi materiali di input della fase
    vq_exec("..\prfa\exe\query\gsci_bwf4", THIS, "Par_rior") 
 vq_exec("..\prfa\exe\query\gsci_bwf2", THIS, "Materiali") 
 =wrcursor("Materiali") 
 scan
    this.w_COCODSAL = cocodsal
    if this.w_CLSERIAL=clserial and this.w_CORIFDIS=corifdis and this.w_CPROWNUM1=cprownum1
      * --- Elimina riga perch� duplicata
      delete
    endif
    this.w_CLSERIAL = clserial
    this.w_CORIFDIS = corifdis
    this.w_CPROWNUM1 = cprownum1
    Endscan
    * --- Aggiunge i costi dei materiali
    Select * from Materiali left outer join Par_rior on cocodsal=prkeysal order by clcodfas into cursor CostoMat readwrite nofilter 
 use in Materiali 
 use in Par_rior
    if g_COLA="S"
      * --- -Estare i listini per le fasi di conto lavoro
      * --- - Estrae i materiali forniti per la fase
      vq_exec("..\prfa\exe\query\gsci_bwf6", THIS, "_Listini_") 
 =wrcursor("_Listini_") 
 vq_exec("..\prfa\exe\query\gsci_bwf7", THIS, "_MatFase_") 
 =wrcursor("_MatFase_")
      INDEX ON ALLTRIM(this.w_CLSERIAL)+ALLTRIM(this.w_CLCODFAS) TAG CODICE
      * --- Carica i costi delle lavorazioni esterne dai contratti di conto lavoro
      =wrcursor("Cos_lavo")
      this.w_CLSERIAL = space(10)
      * --- Select from GSCI_BWF5
      do vq_exec with 'GSCI_BWF5',this,'_Curs_GSCI_BWF5','',.f.,.t.
      if used('_Curs_GSCI_BWF5')
        select _Curs_GSCI_BWF5
        locate for 1=1
        do while not(eof())
        if _Curs_gsci_bwf5.CLSERIAL<>this.w_CLSERIAL or _Curs_gsci_bwf5.CLCODFAS<>this.w_CLCODFAS
          this.w_CLSERIAL = _Curs_gsci_bwf5.CLSERIAL
          this.w_CLCODFAS = _Curs_gsci_bwf5.CLCODFAS
          this.w_PREZUM = NVL(_Curs_GSCI_BWF5.CAPREZUM, "N")
          this.w_UNMIS1 = _Curs_GSCI_BWF5.ARUNMIS1
          this.w_OPERAT = nvl(_Curs_GSCI_BWF5.AROPERAT,space(1))
          this.w_MOLTIP = nvl(_Curs_GSCI_BWF5.ARMOLTIP,0)
          this.w_LOTMEDFE = _Curs_GSCI_BWF5.LOTMEDFA
          this.w_CLCODDIS = NVL(_Curs_GSCI_BWF5.CLCODDIS, SPACE(41))
          this.w_CODICE = ALLTRIM(this.w_CLSERIAL)+ALLTRIM(this.w_CLCODFAS)
          this.w_ORAUNI = "U"
          SELECT "_Listini_"
          if nvl(this.w_PREZUM,"N")<>"S"
            * --- Ogni contratto valido va bene
            locate for CLSERIAL=this.w_CLSERIAL and CLCODFAS=this.w_CLCODFAS and CLCODDIS=this.w_CLCODDIS and coquant1>=this.w_LOTMEDFE
          else
            * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. principale articolo, perch�
            *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
            locate for CLSERIAL=this.w_CLSERIAL and CLCODFAS=this.w_CLCODFAS and CLCODDIS=this.w_CLCODDIS and counimis=this.w_UNMIS1 and coquanti >=this.w_LOTMEDFE
          endif
          if Found()
            * --- Marco il listino come utilizzato per la fase
            REPLACE ASSOCIATO WITH "S"
            this.w_VALCONT = _Listini_.cocodval
            this.w_COCOSSET = _Listini_.COCOSSET
            if counimis=this.w_UNMIS1 or this.w_ORAUNI="O"
              * --- Se il costo non � unitario ma orario non lo devo convertire
              this.w_COPREZZO = _Listini_.COPREZZO
            else
              * --- Effettuo le conversioni (ho usato la Seconda Unit� di misura)
              if this.w_OPERAT="/"
                this.w_COPREZZO = cp_ROUND(_Listini_.COPREZZO / this.w_MOLTIP, 5)
              else
                this.w_COPREZZO = cp_ROUND(_Listini_.COPREZZO * this.w_MOLTIP, 5)
              endif
            endif
            if this.w_VALCONT<>g_PERVAL
              * --- Cambio valuta
              this.w_CAO = GETCAM(this.w_VALCONT, i_DATSYS, 0)
              if this.w_CAO<>0
                this.w_COPREZZO = VAL2MON(this.w_COPREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
                this.w_COCOSSET = iif(this.w_COCOSSET=0, 0, VAL2MON(this.w_COCOSSET, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL))
              else
                ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato",48)
              endif
            endif
            * --- Se trovo un cotratto elimino dal cursore dei cicli i costi di setup e warm up
            Select Cos_lavo
            Delete for CLSERIAL=this.w_CLSERIAL and CLCODFAS=this.w_CLCODFAS and CLCODDIS=this.w_CLCODDIS and lrtiptem<>"L"
            update Cos_lavo set rlcossta=this.w_COPREZZO, cocosstp=this.w_COCOSSET ; 
 where CLSERIAL=this.w_CLSERIAL and CLCODFAS=this.w_CLCODFAS and CLCODDIS=this.w_CLCODDIS
            if this.w_ORAUNI="U"
              update Cos_lavo set lrtemora=1 ; 
 where CLSERIAL=this.w_CLSERIAL and CLCODFAS=this.w_CLCODFAS and CLCODDIS=this.w_CLCODDIS
            endif
          endif
        endif
          select _Curs_GSCI_BWF5
          continue
        enddo
        use
      endif
      SELECT("_Listini_")
      GO TOP
      SELECT "_MatFase_"
      GO TOP
      * --- Dal listino segnato come utilizzato mi ricavo i materiali forniti
      Select A.* from _MatFase_ A inner join _Listini_ B on A.MT__GUID=B.DL__GUID AND B.ASSOCIATO="S" into cursor _FornFase_ readwrite
      * --- Non rimuovere le GO TOP sui cursori altrimenti non riesce a fare l'aggiornamento
      SELECT "_FornFase_"
      GO TOP
      SELECT "COSTOMAT"
      GO TOP
      * --- Aggiorno il cursore dei costi dei materiali con il costo dei materiali forniti
      UPDATE COSTOMAT SET PRCSTD=IIF(this.w_STD="=" , MTCOSART, PRCSTD) , PRCULT=IIF(this.w_ULT="=" , MTCOSART, PRCULT),; 
 PRCMPA=IIF(this.w_MED="=" , MTCOSART, PRCMPA), PRCLIS=IIF(this.w_LST="=" , MTCOSART, PRCLIS) FROM _FORNFASE_ ; 
 WHERE COSTOMAT.clserial=_FORNFASE_.clserial AND COSTOMAT.CPROWNUM =_FORNFASE_.CPROWNUM ; 
 AND COSTOMAT.PRKEYSAL=_FORNFASE_.CAKEYSAL
      USE IN SELECT("_MatFase_")
      USE IN SELECT("_FornFase_")
      SELECT "COSTOMAT"
      GO TOP
    endif
    Select Cos_lavo 
 go top
    this.w_CLSERIAL = space(10)
    do while not eof("Cos_lavo")
      Select Cos_lavo
      if this.w_CLSERIAL<>clserial
        * --- Esamina nuovo ciclo: azzera i costi
        this.w_CLSERIAL = clserial
        this.w_PRCSMATE = 0
        this.w_PRCUMATE = 0
        this.w_PRCMMATE = 0
        this.w_PRCLMATE = 0
        this.w_PRCSLAVO = 0
        this.w_PRCULAVO = 0
        this.w_PRCMLAVO = 0
        this.w_PRCLLAVO = 0
        this.w_PRCSGENE = 0
        this.w_PRCUGENE = 0
        this.w_PRCMGENE = 0
        this.w_PRCLGENE = 0
        this.w_Listino = prliscos
        this.w_ESERC = presecos
        this.w_NUMINV = prinvcos
      endif
      this.w_CLCODFAS = clcodfas
      do while this.w_CLCODFAS=clcodfas and not eof("Cos_lavo")
        * --- Costi lavorazione
        this.w_LOTMED = IIF(NVL(RLINTEST, "I") <> "E" , iif(nvl(dplotmed,0)=0,1,dplotmed) , iif(nvl(lotmedfa,0)=0,1,lotmedfa) )
        this.w_CSLAVO = IIF(LRTIPTEM<>"L" , (LRTEMORA/this.w_LOTMED) * rlcossta, lrtemora * rlcossta) + cocosstp/this.w_LOTMED
        this.w_CSGENE = lrtemora * rlcossta * DROVERHE/100
        this.w_PRCSLAVO = this.w_PRCSLAVO + this.w_CSLAVO
        this.w_PRCULAVO = this.w_PRCULAVO + this.w_CSLAVO
        this.w_PRCMLAVO = this.w_PRCMLAVO + this.w_CSLAVO
        this.w_PRCLLAVO = this.w_PRCLLAVO + this.w_CSLAVO
        this.w_PRCSGENE = this.w_PRCSGENE + this.w_CSGENE
        this.w_PRCUGENE = this.w_PRCUGENE + this.w_CSGENE
        this.w_PRCMGENE = this.w_PRCMGENE + this.w_CSGENE
        this.w_PRCLGENE = this.w_PRCLGENE + this.w_CSGENE
        skip
      enddo
      Select CostoMat
      do while this.w_CLCODFAS>clcodfas and not eof("CostoMat")
        * --- Cerca il primo record
        skip
      enddo
      do while this.w_CLCODFAS=clcodfas and not eof("CostoMat")
        * --- Aggiunge i costi dei materiali
        this.w_COEFF = cocoeum1*(1+coscapro*(1-corecsca)+cosfrido*(1-corecsfr)) * iif(this.oParentObject.w_PERRIC="S",1+coperric,1)
        this.w_PRCSMATE = this.w_PRCSMATE + prcstd*this.w_COEFF
        this.w_PRCUMATE = this.w_PRCUMATE + prcult*this.w_COEFF
        this.w_PRCMMATE = this.w_PRCMMATE + prcmpa*this.w_COEFF
        this.w_PRCLMATE = this.w_PRCLMATE + prclis*this.w_COEFF
        skip
      enddo
      * --- Aggiorna il DB
      this.w_PRKEYSAL = this.w_CLCODFAS
      if this.oParentObject.w_UTENTE
        do case
          case this.oParentObject.w_AGGSTAND="S"
            this.w_PRCOSSTA = this.w_PRCSMATE + this.w_PRCSLAVO + this.w_PRCSGENE
          case this.oParentObject.w_AGGSTAND="U"
            this.w_PRCOSSTA = this.w_PRCUMATE + this.w_PRCULAVO + this.w_PRCUGENE
          case this.oParentObject.w_AGGSTAND="M"
            this.w_PRCOSSTA = this.w_PRCMMATE + this.w_PRCMLAVO + this.w_PRCMGENE
          case this.oParentObject.w_AGGSTAND="L"
            this.w_PRCOSSTA = this.w_PRCLMATE + this.w_PRCLLAVO + this.w_PRCLGENE
        endcase
      endif
      * --- Try
      local bErr_03703660
      bErr_03703660=bTrsErr
      this.Try_03703660()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        this.w_MESS = "Errore di scrittura nel database%0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL"
        AddMsgNL("Aggiornamento codice: %1", this,this.w_PRKEYSAL)
      endif
      bTrsErr=bTrsErr or bErr_03703660
      * --- End
    enddo
    if used("Cos_lavo")
      Use in Cos_lavo
    endif
    if used("CostoMat")
      Use in CostoMat
    endif
    if used("_Listini_")
      Use in "_Listini_"
    endif
    AddMsgNL("Aggiornamento completato", this) 
 ah_errormsg("Aggiornamento completato", 64)
  endproc
  proc Try_03703660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    AddMsgNL("Aggiornamento codice: %1", this,this.w_PRKEYSAL)
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_STD,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_ULT,'PRLOTULT','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp11=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
      i_cOp12=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
      i_cOp13=cp_SetTrsOp(this.w_MED,'PRLOTMED','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
      i_cOp15=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
      i_cOp16=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp17=cp_SetTrsOp(this.w_LST,'PRLOTLIS','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
      i_cOp18=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
      i_cOp19=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
      i_cOp21=cp_SetTrsOp(this.w_STD,'PRCSSPGE','this.w_PRCSGENE',this.w_PRCSGENE,'update',i_nConn)
      i_cOp22=cp_SetTrsOp(this.w_ULT,'PRCUSPGE','this.w_PRCUGENE',this.w_PRCUGENE,'update',i_nConn)
      i_cOp23=cp_SetTrsOp(this.w_MED,'PRCMSPGE','this.w_PRCMGENE',this.w_PRCMGENE,'update',i_nConn)
      i_cOp24=cp_SetTrsOp(this.w_LST,'PRCLSPGE','this.w_PRCLGENE',this.w_PRCLGENE,'update',i_nConn)
      i_cOp25=cp_SetTrsOp(this.w_UTE,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVO');
      +",PRCSMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSMATE');
      +",PRLOTSTD ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRLOTSTD');
      +",PRDATCOS ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRDATCOS');
      +",PRCULAVO ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCULAVO');
      +",PRCUMATE ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCUMATE');
      +",PRLOTULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRLOTULT');
      +",PRDATULT ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRDATULT');
      +",PRCMLAVO ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCMLAVO');
      +",PRCMMATE ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRCMMATE');
      +",PRESECOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRINVCOS');
      +",PRLOTMED ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRLOTMED');
      +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
      +",PRCLLAVO ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRCLLAVO');
      +",PRCLMATE ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRCLMATE');
      +",PRLOTLIS ="+cp_NullLink(i_cOp17,'PAR_RIOR','PRLOTLIS');
      +",PRLISCOS ="+cp_NullLink(i_cOp18,'PAR_RIOR','PRLISCOS');
      +",PRDATLIS ="+cp_NullLink(i_cOp19,'PAR_RIOR','PRDATLIS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
      +",PRCSSPGE ="+cp_NullLink(i_cOp21,'PAR_RIOR','PRCSSPGE');
      +",PRCUSPGE ="+cp_NullLink(i_cOp22,'PAR_RIOR','PRCUSPGE');
      +",PRCMSPGE ="+cp_NullLink(i_cOp23,'PAR_RIOR','PRCMSPGE');
      +",PRCLSPGE ="+cp_NullLink(i_cOp24,'PAR_RIOR','PRCLSPGE');
      +",PRCOSSTA ="+cp_NullLink(i_cOp25,'PAR_RIOR','PRCOSSTA');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_PRKEYSAL);
             )
    else
      update (i_cTable) set;
          PRCSLAVO = &i_cOp1.;
          ,PRCSMATE = &i_cOp2.;
          ,PRLOTSTD = &i_cOp3.;
          ,PRDATCOS = &i_cOp4.;
          ,PRCULAVO = &i_cOp5.;
          ,PRCUMATE = &i_cOp6.;
          ,PRLOTULT = &i_cOp7.;
          ,PRDATULT = &i_cOp8.;
          ,PRCMLAVO = &i_cOp9.;
          ,PRCMMATE = &i_cOp10.;
          ,PRESECOS = &i_cOp11.;
          ,PRINVCOS = &i_cOp12.;
          ,PRLOTMED = &i_cOp13.;
          ,PRDATMED = &i_cOp14.;
          ,PRCLLAVO = &i_cOp15.;
          ,PRCLMATE = &i_cOp16.;
          ,PRLOTLIS = &i_cOp17.;
          ,PRLISCOS = &i_cOp18.;
          ,PRDATLIS = &i_cOp19.;
          ,PUUTEELA = i_CODUTE;
          ,PRCSSPGE = &i_cOp21.;
          ,PRCUSPGE = &i_cOp22.;
          ,PRCMSPGE = &i_cOp23.;
          ,PRCLSPGE = &i_cOp24.;
          ,PRCOSSTA = &i_cOp25.;
          ,PRTIPCON = "F";
          &i_ccchkf. ;
       where;
          PRCODART = this.w_PRKEYSAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if  i_rows=0
      this.w_VARIAR = Right(this.w_PRKEYSAL,20)
      * --- Insert into PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODART"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PRKEYSAL),'PAR_RIOR','PRCODART');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_PRKEYSAL)
        insert into (i_cTable) (PRCODART &i_ccchkf. );
           values (;
             this.w_PRKEYSAL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_STD,'PRLOTSTD','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
        i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_ULT,'PRLOTULT','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
        i_cOp11=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
        i_cOp12=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
        i_cOp13=cp_SetTrsOp(this.w_MED,'PRLOTMED','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
        i_cOp15=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
        i_cOp16=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
        i_cOp17=cp_SetTrsOp(this.w_LST,'PRLOTLIS','this.w_LOTMED',this.w_LOTMED,'update',i_nConn)
        i_cOp18=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
        i_cOp19=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.oParentObject.w_DATRIF',this.oParentObject.w_DATRIF,'update',i_nConn)
        i_cOp21=cp_SetTrsOp(this.w_STD,'PRCSSPGE','this.w_PRCSGENE',this.w_PRCSGENE,'update',i_nConn)
        i_cOp22=cp_SetTrsOp(this.w_ULT,'PRCUSPGE','this.w_PRCUGENE',this.w_PRCUGENE,'update',i_nConn)
        i_cOp23=cp_SetTrsOp(this.w_MED,'PRCMSPGE','this.w_PRCMGENE',this.w_PRCMGENE,'update',i_nConn)
        i_cOp24=cp_SetTrsOp(this.w_LST,'PRCLSPGE','this.w_PRCLGENE',this.w_PRCLGENE,'update',i_nConn)
        i_cOp25=cp_SetTrsOp(this.w_STD,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCSLAVO ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSLAVO');
        +",PRCSMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSMATE');
        +",PRLOTSTD ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRLOTSTD');
        +",PRDATCOS ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRDATCOS');
        +",PRCULAVO ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCULAVO');
        +",PRCUMATE ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCUMATE');
        +",PRLOTULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRLOTULT');
        +",PRDATULT ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRDATULT');
        +",PRCMLAVO ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCMLAVO');
        +",PRCMMATE ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRCMMATE');
        +",PRESECOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRESECOS');
        +",PRINVCOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRINVCOS');
        +",PRLOTMED ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRLOTMED');
        +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
        +",PRCLLAVO ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRCLLAVO');
        +",PRCLMATE ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRCLMATE');
        +",PRLOTLIS ="+cp_NullLink(i_cOp17,'PAR_RIOR','PRLOTLIS');
        +",PRLISCOS ="+cp_NullLink(i_cOp18,'PAR_RIOR','PRLISCOS');
        +",PRDATLIS ="+cp_NullLink(i_cOp19,'PAR_RIOR','PRDATLIS');
        +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
        +",PRCSSPGE ="+cp_NullLink(i_cOp21,'PAR_RIOR','PRCSSPGE');
        +",PRCUSPGE ="+cp_NullLink(i_cOp22,'PAR_RIOR','PRCUSPGE');
        +",PRCMSPGE ="+cp_NullLink(i_cOp23,'PAR_RIOR','PRCMSPGE');
        +",PRCLSPGE ="+cp_NullLink(i_cOp24,'PAR_RIOR','PRCLSPGE');
        +",PRCOSSTA ="+cp_NullLink(i_cOp25,'PAR_RIOR','PRCOSSTA');
        +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
        +",PRLOWLEV ="+cp_NullLink(cp_ToStrODBC(-1),'PAR_RIOR','PRLOWLEV');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_PRKEYSAL);
               )
      else
        update (i_cTable) set;
            PRCSLAVO = &i_cOp1.;
            ,PRCSMATE = &i_cOp2.;
            ,PRLOTSTD = &i_cOp3.;
            ,PRDATCOS = &i_cOp4.;
            ,PRCULAVO = &i_cOp5.;
            ,PRCUMATE = &i_cOp6.;
            ,PRLOTULT = &i_cOp7.;
            ,PRDATULT = &i_cOp8.;
            ,PRCMLAVO = &i_cOp9.;
            ,PRCMMATE = &i_cOp10.;
            ,PRESECOS = &i_cOp11.;
            ,PRINVCOS = &i_cOp12.;
            ,PRLOTMED = &i_cOp13.;
            ,PRDATMED = &i_cOp14.;
            ,PRCLLAVO = &i_cOp15.;
            ,PRCLMATE = &i_cOp16.;
            ,PRLOTLIS = &i_cOp17.;
            ,PRLISCOS = &i_cOp18.;
            ,PRDATLIS = &i_cOp19.;
            ,PUUTEELA = i_CODUTE;
            ,PRCSSPGE = &i_cOp21.;
            ,PRCUSPGE = &i_cOp22.;
            ,PRCMSPGE = &i_cOp23.;
            ,PRCLSPGE = &i_cOp24.;
            ,PRCOSSTA = &i_cOp25.;
            ,PRTIPCON = "F";
            ,PRLOWLEV = -1;
            &i_ccchkf. ;
         where;
            PRCODART = this.w_PRKEYSAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_RIOR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCI_BWF5')
      use in _Curs_GSCI_BWF5
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
