* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_acl                                                        *
*              Centri di lavoro                                                *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-29                                                      *
* Last revis.: 2015-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_acl"))

* --- Class definition
define class tgsci_acl as StdForm
  Top    = 4
  Left   = 19

  * --- Standard Properties
  Width  = 629
  Height = 315+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-28"
  HelpContextID=158726807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  RIS_ORSE_IDX = 0
  CENCOST_IDX = 0
  TAB_CALE_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  LIS_MACC_IDX = 0
  LIS_FORN_IDX = 0
  STR_CONF_IDX = 0
  DIS_RISO_IDX = 0
  CAT_RISO_IDX = 0
  TIP_RISO_IDX = 0
  UNIMIS_IDX = 0
  cFile = "RIS_ORSE"
  cKeySelect = "RL__TIPO,RLCODICE"
  cQueryFilter="RL__TIPO='CL'"
  cKeyWhere  = "RL__TIPO=this.w_RL__TIPO and RLCODICE=this.w_RLCODICE"
  cKeyWhereODBC = '"RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cKeyWhereODBCqualified = '"RIS_ORSE.RL__TIPO="+cp_ToStrODBC(this.w_RL__TIPO)';
      +'+" and RIS_ORSE.RLCODICE="+cp_ToStrODBC(this.w_RLCODICE)';

  cPrg = "gsci_acl"
  cComment = "Centri di lavoro"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RL__TIPO = space(2)
  w_UMFLTEMP = space(1)
  w_RLCODICE = space(20)
  w_CODICE = space(20)
  w_TIPOPAD = space(2)
  w_CODPAD = space(20)
  w_RLDESCRI = space(40)
  w_RL__NOTE = space(0)
  w_RLINTEST = space(1)
  w_RLWIPRIS = space(5)
  w_DESMAG = space(30)
  w_RLMAGWIP = space(5)
  w_RLUMTEMP = space(3)
  o_RLUMTEMP = space(3)
  w_RLTEMCOD = 0
  o_RLTEMCOD = 0
  w_RLUMTDEF = space(3)
  w_RLCODCAT = space(5)
  w_RLCENCOS = space(15)
  w_DESCCO = space(40)
  w_FATCON = 0
  w_RLTEMSEC = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DTEMDEF = space(35)
  w_TIPMAG = space(1)
  w_ERECAL = space(5)
  w_DERCAL = space(40)
  w_EREWIP = space(5)
  w_DERMAG = space(30)
  w_DESREP = space(40)
  w_DESCAT = space(40)
  w_RLQTARIS = 0
  w_DURSEC = 0

  * --- Children pointers
  GSCI_MDR = .NULL.
  GSCI_MLF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIS_ORSE','gsci_acl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_aclPag1","gsci_acl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 166109238
      .Pages(2).addobject("oPag","tgsci_aclPag2","gsci_acl",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Disponibilit�")
      .Pages(2).HelpContextID = 223910696
      .Pages(3).addobject("oPag","tgsci_aclPag3","gsci_acl",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Fornitori")
      .Pages(3).HelpContextID = 193227432
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRLCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsci_acl
    * Abilita pagina lavorazioni solo se abilitato C/Lavoro
    This.Pages(3).Enabled = (g_COLA='S')
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='TAB_CALE'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='LIS_MACC'
    this.cWorkTables[6]='LIS_FORN'
    this.cWorkTables[7]='STR_CONF'
    this.cWorkTables[8]='DIS_RISO'
    this.cWorkTables[9]='CAT_RISO'
    this.cWorkTables[10]='TIP_RISO'
    this.cWorkTables[11]='UNIMIS'
    this.cWorkTables[12]='RIS_ORSE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_ORSE_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MDR = CREATEOBJECT('stdDynamicChild',this,'GSCI_MDR',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSCI_MLF = CREATEOBJECT('stdDynamicChild',this,'GSCI_MLF',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MDR)
      this.GSCI_MDR.DestroyChildrenChain()
      this.GSCI_MDR=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.GSCI_MLF)
      this.GSCI_MLF.DestroyChildrenChain()
      this.GSCI_MLF=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MDR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCI_MLF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MDR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCI_MLF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MDR.NewDocument()
    this.GSCI_MLF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCI_MDR.SetKey(;
            .w_RLCODICE,"DRCODRIS";
            ,.w_RL__TIPO,"DRTIPRIS";
            )
      this.GSCI_MLF.SetKey(;
            .w_RLCODICE,"LFCODICE";
            ,.w_RL__TIPO,"LFTIPOCL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCI_MDR.ChangeRow(this.cRowID+'      1',1;
             ,.w_RLCODICE,"DRCODRIS";
             ,.w_RL__TIPO,"DRTIPRIS";
             )
      .GSCI_MLF.ChangeRow(this.cRowID+'      1',1;
             ,.w_RLCODICE,"LFCODICE";
             ,.w_RL__TIPO,"LFTIPOCL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCI_MDR)
        i_f=.GSCI_MDR.BuildFilter()
        if !(i_f==.GSCI_MDR.cQueryFilter)
          i_fnidx=.GSCI_MDR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MDR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MDR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MDR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MDR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCI_MLF)
        i_f=.GSCI_MLF.BuildFilter()
        if !(i_f==.GSCI_MLF.cQueryFilter)
          i_fnidx=.GSCI_MLF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MLF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MLF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MLF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MLF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RL__TIPO = NVL(RL__TIPO,space(2))
      .w_RLCODICE = NVL(RLCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIS_ORSE where RL__TIPO=KeySet.RL__TIPO
    *                            and RLCODICE=KeySet.RLCODICE
    *
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_ORSE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_ORSE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIS_ORSE '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UMFLTEMP = 'S'
        .w_TIPOPAD = 'UP'
        .w_CODPAD = space(20)
        .w_DESMAG = space(30)
        .w_DESCCO = space(40)
        .w_FATCON = 0
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DTEMDEF = space(35)
        .w_TIPMAG = space(1)
        .w_ERECAL = space(5)
        .w_DERCAL = space(40)
        .w_EREWIP = space(5)
        .w_DERMAG = space(30)
        .w_DESREP = space(40)
        .w_DESCAT = space(40)
        .w_DURSEC = 0
        .w_RL__TIPO = NVL(RL__TIPO,space(2))
        .w_RLCODICE = NVL(RLCODICE,space(20))
        .w_CODICE = .w_RLCODICE
          .link_1_4('Load')
          .link_1_6('Load')
        .w_RLDESCRI = NVL(RLDESCRI,space(40))
        .w_RL__NOTE = NVL(RL__NOTE,space(0))
        .w_RLINTEST = NVL(RLINTEST,space(1))
        .w_RLWIPRIS = NVL(RLWIPRIS,space(5))
          if link_1_14_joined
            this.w_RLWIPRIS = NVL(MGCODMAG114,NVL(this.w_RLWIPRIS,space(5)))
            this.w_DESMAG = NVL(MGDESMAG114,space(30))
            this.w_TIPMAG = NVL(MGTIPMAG114,space(1))
          else
          .link_1_14('Load')
          endif
        .w_RLMAGWIP = NVL(RLMAGWIP,space(5))
          * evitabile
          *.link_1_16('Load')
        .w_RLUMTEMP = NVL(RLUMTEMP,space(3))
          .link_1_17('Load')
        .w_RLTEMCOD = NVL(RLTEMCOD,0)
        .w_RLUMTDEF = NVL(RLUMTDEF,space(3))
          .link_1_21('Load')
        .w_RLCODCAT = NVL(RLCODCAT,space(5))
          if link_1_22_joined
            this.w_RLCODCAT = NVL(CRCODCAT122,NVL(this.w_RLCODCAT,space(5)))
            this.w_DESCAT = NVL(CRDESCAT122,space(40))
          else
          .link_1_22('Load')
          endif
        .w_RLCENCOS = NVL(RLCENCOS,space(15))
          if link_1_25_joined
            this.w_RLCENCOS = NVL(CC_CONTO125,NVL(this.w_RLCENCOS,space(15)))
            this.w_DESCCO = NVL(CCDESPIA125,space(40))
          else
          .link_1_25('Load')
          endif
        .w_RLTEMSEC = NVL(RLTEMSEC,0)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
          .link_1_36('Load')
          .link_1_38('Load')
        .w_RLQTARIS = NVL(RLQTARIS,0)
        cp_LoadRecExtFlds(this,'RIS_ORSE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RL__TIPO = space(2)
      .w_UMFLTEMP = space(1)
      .w_RLCODICE = space(20)
      .w_CODICE = space(20)
      .w_TIPOPAD = space(2)
      .w_CODPAD = space(20)
      .w_RLDESCRI = space(40)
      .w_RL__NOTE = space(0)
      .w_RLINTEST = space(1)
      .w_RLWIPRIS = space(5)
      .w_DESMAG = space(30)
      .w_RLMAGWIP = space(5)
      .w_RLUMTEMP = space(3)
      .w_RLTEMCOD = 0
      .w_RLUMTDEF = space(3)
      .w_RLCODCAT = space(5)
      .w_RLCENCOS = space(15)
      .w_DESCCO = space(40)
      .w_FATCON = 0
      .w_RLTEMSEC = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DTEMDEF = space(35)
      .w_TIPMAG = space(1)
      .w_ERECAL = space(5)
      .w_DERCAL = space(40)
      .w_EREWIP = space(5)
      .w_DERMAG = space(30)
      .w_DESREP = space(40)
      .w_DESCAT = space(40)
      .w_RLQTARIS = 0
      .w_DURSEC = 0
      if .cFunction<>"Filter"
        .w_RL__TIPO = 'CL'
        .w_UMFLTEMP = 'S'
          .DoRTCalc(3,3,.f.)
        .w_CODICE = .w_RLCODICE
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODICE))
          .link_1_4('Full')
          endif
        .w_TIPOPAD = 'UP'
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CODPAD))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,8,.f.)
        .w_RLINTEST = 'I'
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_RLWIPRIS))
          .link_1_14('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_RLMAGWIP = iif(empty(.w_RLWIPRIS), .w_EREWIP, .w_RLWIPRIS)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_RLMAGWIP))
          .link_1_16('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_RLUMTEMP))
          .link_1_17('Full')
          endif
        .DoRTCalc(14,15,.f.)
          if not(empty(.w_RLUMTDEF))
          .link_1_21('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_RLCODCAT))
          .link_1_22('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_RLCENCOS))
          .link_1_25('Full')
          endif
          .DoRTCalc(18,19,.f.)
        .w_RLTEMSEC = .w_RLTEMCOD*.w_FATCON
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(22,25,.f.)
          if not(empty(.w_ERECAL))
          .link_1_36('Full')
          endif
        .DoRTCalc(26,27,.f.)
          if not(empty(.w_EREWIP))
          .link_1_38('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_ORSE')
    this.DoRTCalc(28,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRLCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oRLDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oRL__NOTE_1_9.enabled = i_bVal
      .Page1.oPag.oRLINTEST_1_10.enabled_(i_bVal)
      .Page1.oPag.oRLWIPRIS_1_14.enabled = i_bVal
      .Page1.oPag.oRLTEMCOD_1_18.enabled = i_bVal
      .Page1.oPag.oRLUMTDEF_1_21.enabled = i_bVal
      .Page1.oPag.oRLCODCAT_1_22.enabled = i_bVal
      .Page1.oPag.oRLCENCOS_1_25.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRLCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRLCODICE_1_3.enabled = .t.
        .Page1.oPag.oRLDESCRI_1_8.enabled = .t.
      endif
    endwith
    this.GSCI_MDR.SetStatus(i_cOp)
    this.GSCI_MLF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'RIS_ORSE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MDR.SetChildrenStatus(i_cOp)
  *  this.GSCI_MLF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__TIPO,"RL__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODICE,"RLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLDESCRI,"RLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RL__NOTE,"RL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLINTEST,"RLINTEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLWIPRIS,"RLWIPRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLMAGWIP,"RLMAGWIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLUMTEMP,"RLUMTEMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLTEMCOD,"RLTEMCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLUMTDEF,"RLUMTDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCODCAT,"RLCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLCENCOS,"RLCENCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLTEMSEC,"RLTEMSEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RLQTARIS,"RLQTARIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    i_lTable = "RIS_ORSE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIS_ORSE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SCL with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIS_ORSE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIS_ORSE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_ORSE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLINTEST"+;
                  ",RLWIPRIS,RLMAGWIP,RLUMTEMP,RLTEMCOD,RLUMTDEF"+;
                  ",RLCODCAT,RLCENCOS,RLTEMSEC,RLQTARIS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RL__TIPO)+;
                  ","+cp_ToStrODBC(this.w_RLCODICE)+;
                  ","+cp_ToStrODBC(this.w_RLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RL__NOTE)+;
                  ","+cp_ToStrODBC(this.w_RLINTEST)+;
                  ","+cp_ToStrODBCNull(this.w_RLWIPRIS)+;
                  ","+cp_ToStrODBCNull(this.w_RLMAGWIP)+;
                  ","+cp_ToStrODBCNull(this.w_RLUMTEMP)+;
                  ","+cp_ToStrODBC(this.w_RLTEMCOD)+;
                  ","+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
                  ","+cp_ToStrODBCNull(this.w_RLCODCAT)+;
                  ","+cp_ToStrODBCNull(this.w_RLCENCOS)+;
                  ","+cp_ToStrODBC(this.w_RLTEMSEC)+;
                  ","+cp_ToStrODBC(this.w_RLQTARIS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_ORSE')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_ORSE')
        cp_CheckDeletedKey(i_cTable,0,'RL__TIPO',this.w_RL__TIPO,'RLCODICE',this.w_RLCODICE)
        INSERT INTO (i_cTable);
              (RL__TIPO,RLCODICE,RLDESCRI,RL__NOTE,RLINTEST,RLWIPRIS,RLMAGWIP,RLUMTEMP,RLTEMCOD,RLUMTDEF,RLCODCAT,RLCENCOS,RLTEMSEC,RLQTARIS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RL__TIPO;
                  ,this.w_RLCODICE;
                  ,this.w_RLDESCRI;
                  ,this.w_RL__NOTE;
                  ,this.w_RLINTEST;
                  ,this.w_RLWIPRIS;
                  ,this.w_RLMAGWIP;
                  ,this.w_RLUMTEMP;
                  ,this.w_RLTEMCOD;
                  ,this.w_RLUMTDEF;
                  ,this.w_RLCODCAT;
                  ,this.w_RLCENCOS;
                  ,this.w_RLTEMSEC;
                  ,this.w_RLQTARIS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIS_ORSE_IDX,i_nConn)
      *
      * update RIS_ORSE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_ORSE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RLDESCRI="+cp_ToStrODBC(this.w_RLDESCRI)+;
             ",RL__NOTE="+cp_ToStrODBC(this.w_RL__NOTE)+;
             ",RLINTEST="+cp_ToStrODBC(this.w_RLINTEST)+;
             ",RLWIPRIS="+cp_ToStrODBCNull(this.w_RLWIPRIS)+;
             ",RLMAGWIP="+cp_ToStrODBCNull(this.w_RLMAGWIP)+;
             ",RLUMTEMP="+cp_ToStrODBCNull(this.w_RLUMTEMP)+;
             ",RLTEMCOD="+cp_ToStrODBC(this.w_RLTEMCOD)+;
             ",RLUMTDEF="+cp_ToStrODBCNull(this.w_RLUMTDEF)+;
             ",RLCODCAT="+cp_ToStrODBCNull(this.w_RLCODCAT)+;
             ",RLCENCOS="+cp_ToStrODBCNull(this.w_RLCENCOS)+;
             ",RLTEMSEC="+cp_ToStrODBC(this.w_RLTEMSEC)+;
             ",RLQTARIS="+cp_ToStrODBC(this.w_RLQTARIS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_ORSE')
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        UPDATE (i_cTable) SET;
              RLDESCRI=this.w_RLDESCRI;
             ,RL__NOTE=this.w_RL__NOTE;
             ,RLINTEST=this.w_RLINTEST;
             ,RLWIPRIS=this.w_RLWIPRIS;
             ,RLMAGWIP=this.w_RLMAGWIP;
             ,RLUMTEMP=this.w_RLUMTEMP;
             ,RLTEMCOD=this.w_RLTEMCOD;
             ,RLUMTDEF=this.w_RLUMTDEF;
             ,RLCODCAT=this.w_RLCODCAT;
             ,RLCENCOS=this.w_RLCENCOS;
             ,RLTEMSEC=this.w_RLTEMSEC;
             ,RLQTARIS=this.w_RLQTARIS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCI_MDR : Saving
      this.GSCI_MDR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RLCODICE,"DRCODRIS";
             ,this.w_RL__TIPO,"DRTIPRIS";
             )
      this.GSCI_MDR.mReplace()
      * --- GSCI_MLF : Saving
      this.GSCI_MLF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_RLCODICE,"LFCODICE";
             ,this.w_RL__TIPO,"LFTIPOCL";
             )
      this.GSCI_MLF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCI_MDR : Deleting
    this.GSCI_MDR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RLCODICE,"DRCODRIS";
           ,this.w_RL__TIPO,"DRTIPRIS";
           )
    this.GSCI_MDR.mDelete()
    * --- GSCI_MLF : Deleting
    this.GSCI_MLF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_RLCODICE,"LFCODICE";
           ,this.w_RL__TIPO,"LFTIPOCL";
           )
    this.GSCI_MLF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIS_ORSE_IDX,i_nConn)
      *
      * delete RIS_ORSE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RL__TIPO',this.w_RL__TIPO  ,'RLCODICE',this.w_RLCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_CODICE = .w_RLCODICE
          .link_1_4('Full')
        .DoRTCalc(5,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,11,.t.)
            .w_RLMAGWIP = iif(empty(.w_RLWIPRIS), .w_EREWIP, .w_RLWIPRIS)
          .link_1_16('Full')
          .link_1_17('Full')
        .DoRTCalc(14,19,.t.)
        if .o_RLUMTEMP<>.w_RLUMTEMP.or. .o_RLTEMCOD<>.w_RLTEMCOD
            .w_RLTEMSEC = .w_RLTEMCOD*.w_FATCON
        endif
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(21,24,.t.)
          .link_1_36('Full')
        .DoRTCalc(26,26,.t.)
          .link_1_38('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRLINTEST_1_10.enabled_(this.oPgFrm.Page1.oPag.oRLINTEST_1_10.mCond())
    this.GSCI_MLF.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsci_acl
    if g_COLA="S"
       * instanzio il figlio Lista Fornitori per attivare i controlli
       IF Upper(CEVENT)='INIT' and Upper(this.GSCI_MLF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[3].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_CONF_IDX,3]
    i_lTable = "STR_CONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2], .t., this.STR_CONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SCTIPFIG,SCCODFIG,SCCODPAD,SCTIPPAD";
                   +" from "+i_cTable+" "+i_lTable+" where SCCODFIG="+cp_ToStrODBC(this.w_CODICE);
                   +" and SCTIPFIG="+cp_ToStrODBC(this.w_RL__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SCTIPFIG',this.w_RL__TIPO;
                       ,'SCCODFIG',this.w_CODICE)
            select SCTIPFIG,SCCODFIG,SCCODPAD,SCTIPPAD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.SCCODFIG,space(20))
      this.w_CODPAD = NVL(_Link_.SCCODPAD,space(20))
      this.w_TIPOPAD = NVL(_Link_.SCTIPPAD,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(20)
      endif
      this.w_CODPAD = space(20)
      this.w_TIPOPAD = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_CONF_IDX,2])+'\'+cp_ToStr(_Link_.SCTIPFIG,1)+'\'+cp_ToStr(_Link_.SCCODFIG,1)
      cp_ShowWarn(i_cKey,this.STR_CONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPAD
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLMAGWIP,RLCODCAL,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODPAD);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPOPAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPOPAD;
                       ,'RLCODICE',this.w_CODPAD)
            select RL__TIPO,RLCODICE,RLMAGWIP,RLCODCAL,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAD = NVL(_Link_.RLCODICE,space(20))
      this.w_EREWIP = NVL(_Link_.RLMAGWIP,space(5))
      this.w_ERECAL = NVL(_Link_.RLCODCAL,space(5))
      this.w_DESREP = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAD = space(20)
      endif
      this.w_EREWIP = space(5)
      this.w_ERECAL = space(5)
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLWIPRIS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLWIPRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_RLWIPRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_RLWIPRIS))
          select MGCODMAG,MGDESMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLWIPRIS)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_RLWIPRIS)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_RLWIPRIS)+"%");

            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RLWIPRIS) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oRLWIPRIS_1_14'),i_cWhere,'GSAR_AMA',"Magazzini WIP",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLWIPRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RLWIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RLWIPRIS)
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLWIPRIS = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_TIPMAG = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RLWIPRIS = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_TIPMAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAG='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure di tipo diverso da WIP")
        endif
        this.w_RLWIPRIS = space(5)
        this.w_DESMAG = space(30)
        this.w_TIPMAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLWIPRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.MGCODMAG as MGCODMAG114"+ ",link_1_14.MGDESMAG as MGDESMAG114"+ ",link_1_14.MGTIPMAG as MGTIPMAG114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on RIS_ORSE.RLWIPRIS=link_1_14.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLWIPRIS=link_1_14.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLMAGWIP
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLMAGWIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLMAGWIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_RLMAGWIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_RLMAGWIP)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLMAGWIP = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RLMAGWIP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLMAGWIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLUMTEMP
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLUMTEMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLUMTEMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RLUMTEMP);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RLUMTEMP)
            select UMFLTEMP,UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLUMTEMP = NVL(_Link_.UMCODICE,space(3))
      this.w_FATCON = NVL(_Link_.UMDESCRI,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLUMTEMP = space(3)
      endif
      this.w_FATCON = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLUMTEMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLUMTDEF
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLUMTDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RLUMTDEF)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_RLUMTDEF))
          select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLUMTDEF)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLUMTDEF) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oRLUMTDEF_1_21'),i_cWhere,'GSAR_AUM',"",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLUMTDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RLUMTDEF);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_RLUMTDEF)
            select UMFLTEMP,UMCODICE,UMDESCRI,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLUMTDEF = NVL(_Link_.UMCODICE,space(3))
      this.w_DTEMDEF = NVL(_Link_.UMDESCRI,space(35))
      this.w_DURSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_RLUMTDEF = space(3)
      endif
      this.w_DTEMDEF = space(35)
      this.w_DURSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DURSEC > 0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endif
        this.w_RLUMTDEF = space(3)
        this.w_DTEMDEF = space(35)
        this.w_DURSEC = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLUMTDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RLCODCAT
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_RISO_IDX,3]
    i_lTable = "CAT_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2], .t., this.CAT_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAT_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODCAT like "+cp_ToStrODBC(trim(this.w_RLCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODCAT',trim(this.w_RLCODCAT))
          select CRCODCAT,CRDESCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCODCAT)==trim(_Link_.CRCODCAT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_RISO','*','CRCODCAT',cp_AbsName(oSource.parent,'oRLCODCAT_1_22'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODCAT',oSource.xKey(1))
            select CRCODCAT,CRDESCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODCAT,CRDESCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODCAT="+cp_ToStrODBC(this.w_RLCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODCAT',this.w_RLCODCAT)
            select CRCODCAT,CRDESCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCODCAT = NVL(_Link_.CRCODCAT,space(5))
      this.w_DESCAT = NVL(_Link_.CRDESCAT,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCODCAT = space(5)
      endif
      this.w_DESCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])+'\'+cp_ToStr(_Link_.CRCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_RISO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CRCODCAT as CRCODCAT122"+ ",link_1_22.CRDESCAT as CRDESCAT122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on RIS_ORSE.RLCODCAT=link_1_22.CRCODCAT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCODCAT=link_1_22.CRCODCAT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RLCENCOS
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RLCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_RLCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_RLCENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RLCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RLCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oRLCENCOS_1_25'),i_cWhere,'',"Elenco centro di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RLCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_RLCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_RLCENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RLCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCCO = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RLCENCOS = space(15)
      endif
      this.w_DESCCO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RLCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.CC_CONTO as CC_CONTO125"+ ",link_1_25.CCDESPIA as CCDESPIA125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on RIS_ORSE.RLCENCOS=link_1_25.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and RIS_ORSE.RLCENCOS=link_1_25.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ERECAL
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ERECAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ERECAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ERECAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ERECAL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ERECAL = NVL(_Link_.TCCODICE,space(5))
      this.w_DERCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ERECAL = space(5)
      endif
      this.w_DERCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ERECAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EREWIP
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EREWIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EREWIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_EREWIP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_EREWIP)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EREWIP = NVL(_Link_.MGCODMAG,space(5))
      this.w_DERMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_EREWIP = space(5)
      endif
      this.w_DERMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EREWIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRLCODICE_1_3.value==this.w_RLCODICE)
      this.oPgFrm.Page1.oPag.oRLCODICE_1_3.value=this.w_RLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPAD_1_6.value==this.w_CODPAD)
      this.oPgFrm.Page1.oPag.oCODPAD_1_6.value=this.w_CODPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oRLDESCRI_1_8.value==this.w_RLDESCRI)
      this.oPgFrm.Page1.oPag.oRLDESCRI_1_8.value=this.w_RLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRL__NOTE_1_9.value==this.w_RL__NOTE)
      this.oPgFrm.Page1.oPag.oRL__NOTE_1_9.value=this.w_RL__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oRLINTEST_1_10.RadioValue()==this.w_RLINTEST)
      this.oPgFrm.Page1.oPag.oRLINTEST_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRLWIPRIS_1_14.value==this.w_RLWIPRIS)
      this.oPgFrm.Page1.oPag.oRLWIPRIS_1_14.value=this.w_RLWIPRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_15.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_15.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oRLTEMCOD_1_18.value==this.w_RLTEMCOD)
      this.oPgFrm.Page1.oPag.oRLTEMCOD_1_18.value=this.w_RLTEMCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oRLUMTDEF_1_21.value==this.w_RLUMTDEF)
      this.oPgFrm.Page1.oPag.oRLUMTDEF_1_21.value=this.w_RLUMTDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCODCAT_1_22.value==this.w_RLCODCAT)
      this.oPgFrm.Page1.oPag.oRLCODCAT_1_22.value=this.w_RLCODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oRLCENCOS_1_25.value==this.w_RLCENCOS)
      this.oPgFrm.Page1.oPag.oRLCENCOS_1_25.value=this.w_RLCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCO_1_27.value==this.w_DESCCO)
      this.oPgFrm.Page1.oPag.oDESCCO_1_27.value=this.w_DESCCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDTEMDEF_1_32.value==this.w_DTEMDEF)
      this.oPgFrm.Page1.oPag.oDTEMDEF_1_32.value=this.w_DTEMDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oERECAL_1_36.value==this.w_ERECAL)
      this.oPgFrm.Page1.oPag.oERECAL_1_36.value=this.w_ERECAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDERCAL_1_37.value==this.w_DERCAL)
      this.oPgFrm.Page1.oPag.oDERCAL_1_37.value=this.w_DERCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oEREWIP_1_38.value==this.w_EREWIP)
      this.oPgFrm.Page1.oPag.oEREWIP_1_38.value=this.w_EREWIP
    endif
    if not(this.oPgFrm.Page1.oPag.oDERMAG_1_39.value==this.w_DERMAG)
      this.oPgFrm.Page1.oPag.oDERMAG_1_39.value=this.w_DERMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_1_42.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_1_42.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_45.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_45.value=this.w_DESCAT
    endif
    cp_SetControlsValueExtFlds(this,'RIS_ORSE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RLCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_RLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPMAG='W')  and not(empty(.w_RLWIPRIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLWIPRIS_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure di tipo diverso da WIP")
          case   ((empty(.w_RLUMTDEF)) or not(.w_DURSEC > 0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRLUMTDEF_1_21.SetFocus()
            i_bnoObbl = !empty(.w_RLUMTDEF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MDR.CheckForm()
      if i_bres
        i_bres=  .GSCI_MDR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCI_MLF.CheckForm()
      if i_bres
        i_bres=  .GSCI_MLF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsci_acl
      if i_bRes
          GSCI_BCW(this,"QTARISORSA")
          i_bRes = i_bRes .and. nvl(this.w_RLQTARIS,0)>0
          if !i_bRes
               ah_ErrorMsg("Inserire la quantit� risorsa nella scheda disponibilit�",48)
          endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RLUMTEMP = this.w_RLUMTEMP
    this.o_RLTEMCOD = this.w_RLTEMCOD
    * --- GSCI_MDR : Depends On
    this.GSCI_MDR.SaveDependsOn()
    * --- GSCI_MLF : Depends On
    this.GSCI_MLF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsci_aclPag1 as StdContainer
  Width  = 625
  height = 315
  stdWidth  = 625
  stdheight = 315
  resizeXpos=350
  resizeYpos=75
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRLCODICE_1_3 as StdField with uid="GLTFRKVEHA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RLCODICE", cQueryName = "RL__TIPO,RLCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice risorsa / componente",;
    HelpContextID = 150373285,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=96, Top=7, InputMask=replicate('X',20)

  add object oCODPAD_1_6 as StdField with uid="PWHRIWUPNZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODPAD", cQueryName = "CODPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 31100454,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=96, Top=90, InputMask=replicate('X',20), cLinkFile="RIS_ORSE", cZoomOnZoom="GSCI_AUP", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPOPAD", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODPAD"

  func oCODPAD_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oRLDESCRI_1_8 as StdField with uid="OUMTJJWKXX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RLDESCRI", cQueryName = "RLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve",;
    HelpContextID = 235959201,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=256, Top=7, InputMask=replicate('X',40)

  add object oRL__NOTE_1_9 as StdMemo with uid="ABCBEAMSMT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RL__NOTE", cQueryName = "RL__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione estesa",;
    HelpContextID = 38060965,;
   bGlobalFont=.t.,;
    Height=54, Width=296, Left=256, Top=30

  add object oRLINTEST_1_10 as StdRadio with uid="CGTCXPYJQZ",rtseq=9,rtrep=.f.,left=155, top=39, width=82,height=47;
    , ToolTipText = "Tipo centro di lavoro (interno o esterno)";
    , cFormVar="w_RLINTEST", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRLINTEST_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Interno"
      this.Buttons(1).HelpContextID = 200745878
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Esterno"
      this.Buttons(2).HelpContextID = 200745878
      this.Buttons(2).Top=22
      this.SetAll("Width",80)
      this.SetAll("Height",24)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo centro di lavoro (interno o esterno)")
      StdRadio::init()
    endproc

  func oRLINTEST_1_10.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oRLINTEST_1_10.GetRadio()
    this.Parent.oContained.w_RLINTEST = this.RadioValue()
    return .t.
  endfunc

  func oRLINTEST_1_10.SetRadio()
    this.Parent.oContained.w_RLINTEST=trim(this.Parent.oContained.w_RLINTEST)
    this.value = ;
      iif(this.Parent.oContained.w_RLINTEST=='I',1,;
      iif(this.Parent.oContained.w_RLINTEST=='E',2,;
      0))
  endfunc

  func oRLINTEST_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COLA="S")
    endwith
   endif
  endfunc

  add object oRLWIPRIS_1_14 as StdField with uid="RHTZNRILXN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RLWIPRIS", cQueryName = "RLWIPRIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure di tipo diverso da WIP",;
    ToolTipText = "Magazzino WIP del centro di lavoro",;
    HelpContextID = 12893289,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=181, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_RLWIPRIS"

  func oRLWIPRIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLWIPRIS_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLWIPRIS_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oRLWIPRIS_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini WIP",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oRLWIPRIS_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_RLWIPRIS
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_15 as StdField with uid="RDXMLAOBHV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 81294390,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=200, Top=181, InputMask=replicate('X',30)

  add object oRLTEMCOD_1_18 as StdField with uid="ISCVCOUNJN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_RLTEMCOD", cQueryName = "RLTEMCOD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lead time del centro di lavoro (espresso in giorni)",;
    HelpContextID = 26250330,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=135, Top=204, cSayPict='"@Z 99,999,999.999"', cGetPict='"99,999,999.999"'

  add object oRLUMTDEF_1_21 as StdField with uid="GYLEJYPUYS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_RLUMTDEF", cQueryName = "RLUMTDEF",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura non gestita a tempo oppure senza conversione in secondi",;
    ToolTipText = "Unit� di misura di default (proposta in gestione cicli)",;
    HelpContextID = 217539492,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=227, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_RLUMTDEF"

  func oRLUMTDEF_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLUMTDEF_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLUMTDEF_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oRLUMTDEF_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oRLUMTDEF_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_RLUMTDEF
     i_obj.ecpSave()
  endproc

  add object oRLCODCAT_1_22 as StdField with uid="QLUYTEYZJN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_RLCODCAT", cQueryName = "RLCODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria risorsa",;
    HelpContextID = 251036566,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=250, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_RISO", oKey_1_1="CRCODCAT", oKey_1_2="this.w_RLCODCAT"

  func oRLCODCAT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCODCAT_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCODCAT_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_RISO','*','CRCODCAT',cp_AbsName(this.parent,'oRLCODCAT_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oRLCENCOS_1_25 as StdField with uid="LVBNQEBXEN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RLCENCOS", cQueryName = "RLCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 27229289,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=291, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_RLCENCOS"

  func oRLCENCOS_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oRLCENCOS_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRLCENCOS_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oRLCENCOS_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco centro di costo",'',this.parent.oContained
  endproc

  add object oDESCCO_1_27 as StdField with uid="NSDZRGIIVB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCCO", cQueryName = "DESCCO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 216953910,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=271, Top=291, InputMask=replicate('X',40)

  add object oDTEMDEF_1_32 as StdField with uid="QFWBDLZTKK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DTEMDEF", cQueryName = "DTEMDEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 217603274,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=200, Top=227, InputMask=replicate('X',35)


  add object oObj_1_35 as cp_runprogram with uid="XZVWSHCBPT",left=3, top=330, width=208,height=18,;
    caption='GSCI_BCW(NODO)',;
   bGlobalFont=.t.,;
    prg='GSCI_BCW("NODO")',;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 1260739

  add object oERECAL_1_36 as StdField with uid="DTHVAVVGFS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ERECAL", cQueryName = "ERECAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 164471110,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=201, Top=115, InputMask=replicate('X',5), cLinkFile="TAB_CALE", oKey_1_1="TCCODICE", oKey_1_2="this.w_ERECAL"

  func oERECAL_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDERCAL_1_37 as StdField with uid="EBRJHTZDGW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DERCAL", cQueryName = "DERCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 164521014,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=256, Top=115, InputMask=replicate('X',40)

  add object oEREWIP_1_38 as StdField with uid="FJJBGWSLKN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_EREWIP", cQueryName = "EREWIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 241279302,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=201, Top=139, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_EREWIP"

  func oEREWIP_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDERMAG_1_39 as StdField with uid="QSQFXRDILB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DERMAG", cQueryName = "DERMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 81290294,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=256, Top=139, InputMask=replicate('X',30)

  add object oDESREP_1_42 as StdField with uid="FGIRZPWRSF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 236811318,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=256, Top=90, InputMask=replicate('X',40)

  add object oDESCAT_1_45 as StdField with uid="BAECJZAXQI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 238128074,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=200, Top=250, InputMask=replicate('X',40)

  add object oStr_1_7 as StdString with uid="VZFEBWNKHL",Visible=.t., Left=39, Top=7,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="LDWRTMRRSV",Visible=.t., Left=84, Top=39,;
    Alignment=1, Width=67, Height=15,;
    Caption="Tipo centro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KGHOYTACYE",Visible=.t., Left=38, Top=181,;
    Alignment=1, Width=92, Height=15,;
    Caption="Magazzino WIP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OEVZNRNGZT",Visible=.t., Left=68, Top=204,;
    Alignment=1, Width=62, Height=15,;
    Caption="Lead time:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LWYKNIDLNC",Visible=.t., Left=26, Top=154,;
    Alignment=0, Width=80, Height=15,;
    Caption="Gestionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NUBVMLYWTA",Visible=.t., Left=26, Top=266,;
    Alignment=0, Width=58, Height=15,;
    Caption="Analitica"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IOIJDUKIWZ",Visible=.t., Left=28, Top=291,;
    Alignment=1, Width=102, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="EQMPDLHGNW",Visible=.t., Left=35, Top=227,;
    Alignment=1, Width=95, Height=15,;
    Caption="UM tempo pref.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VPBRBEDCNF",Visible=.t., Left=73, Top=115,;
    Alignment=1, Width=124, Height=15,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="RJBLITJLQW",Visible=.t., Left=52, Top=139,;
    Alignment=1, Width=145, Height=15,;
    Caption="Magazzino WIP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JPDQIXVHTT",Visible=.t., Left=45, Top=91,;
    Alignment=1, Width=47, Height=15,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="YDEBNOUASN",Visible=.t., Left=246, Top=204,;
    Alignment=0, Width=37, Height=15,;
    Caption="[giorni]"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="HAHTXITOSY",Visible=.t., Left=73, Top=250,;
    Alignment=1, Width=57, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oBox_1_12 as StdBox with uid="OEGSLKLHGP",left=17, top=171, width=591,height=1

  add object oBox_1_13 as StdBox with uid="EPNOIRJPUI",left=18, top=282, width=591,height=1
enddefine
define class tgsci_aclPag2 as StdContainer
  Width  = 625
  height = 315
  stdWidth  = 625
  stdheight = 315
  resizeXpos=509
  resizeYpos=245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="ZTMTFHNAEH",left=1, top=5, width=623, height=302, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mdr",lower(this.oContained.GSCI_MDR.class))=0
        this.oContained.GSCI_MDR.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsci_aclPag3 as StdContainer
  Width  = 625
  height = 315
  stdWidth  = 625
  stdheight = 315
  resizeXpos=420
  resizeYpos=253
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="XQHISVELHF",left=7, top=8, width=587, height=302, bOnScreen=.t.;


  func oLinkPC_3_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_RLINTEST='E')
      endwith
    endif
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mlf",lower(this.oContained.GSCI_MLF.class))=0
        this.oContained.GSCI_MLF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="RL__TIPO='CL'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsci_acl','RIS_ORSE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RL__TIPO=RIS_ORSE.RL__TIPO";
  +" and "+i_cAliasName2+".RLCODICE=RIS_ORSE.RLCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
