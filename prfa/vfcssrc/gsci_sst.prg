* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_sst                                                        *
*              Stampa layout sist. produttivo                                  *
*                                                                              *
*      Author: ZUCCHETTITAM  SRL                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-22                                                      *
* Last revis.: 2014-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_sst",oParentObject))

* --- Class definition
define class tgsci_sst as StdForm
  Top    = 37
  Left   = 29

  * --- Standard Properties
  Width  = 708
  Height = 153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-23"
  HelpContextID=90834281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  RIS_ORSE_IDX = 0
  PAR_PROD_IDX = 0
  TIP_RISO_IDX = 0
  cPrg = "gsci_sst"
  cComment = "Stampa layout sist. produttivo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PPCODICE = space(2)
  w_CODSTA = space(20)
  w_TPFIL = space(2)
  o_TPFIL = space(2)
  w_CODINI = space(20)
  w_DESINI = space(40)
  w_TIPINI = space(2)
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_TIPFIN = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_sstPag1","gsci_sst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTPFIL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='TIP_RISO'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCI_BST with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE=space(2)
      .w_CODSTA=space(20)
      .w_TPFIL=space(2)
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_TIPINI=space(2)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_TIPFIN=space(2)
        .w_PPCODICE = 'PP'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_TPFIL = 'UP'
        .w_CODINI = iif(.w_TPFIL='ST', .w_CODSTA, space(20))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODINI))
          .link_1_5('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_CODFIN = iif(.w_TPFIL='ST', .w_CODSTA, space(20))
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODFIN))
          .link_1_9('Full')
        endif
    endwith
    this.DoRTCalc(8,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_TPFIL<>.w_TPFIL
            .w_CODINI = iif(.w_TPFIL='ST', .w_CODSTA, space(20))
          .link_1_5('Full')
        endif
        .DoRTCalc(5,6,.t.)
        if .o_TPFIL<>.w_TPFIL
            .w_CODFIN = iif(.w_TPFIL='ST', .w_CODSTA, space(20))
          .link_1_9('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_5.enabled = this.oPgFrm.Page1.oPag.oCODINI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODFIN_1_9.enabled = this.oPgFrm.Page1.oPag.oCODFIN_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCODSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPCODSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CODSTA = NVL(_Link_.PPCODSTA,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_CODSTA = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RLCODICE',trim(this.w_CODINI))
          select RLCODICE,RLDESCRI,RL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RLCODICE',cp_AbsName(oSource.parent,'oCODINI_1_5'),i_cWhere,'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',oSource.xKey(1))
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',this.w_CODINI)
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.RLCODICE,space(20))
      this.w_DESINI = NVL(_Link_.RLDESCRI,space(40))
      this.w_TIPINI = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_TIPINI = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_TIPINI = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RLCODICE',trim(this.w_CODFIN))
          select RLCODICE,RLDESCRI,RL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RLCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_9'),i_cWhere,'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',oSource.xKey(1))
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RLCODICE,RLDESCRI,RL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RLCODICE',this.w_CODFIN)
            select RLCODICE,RLDESCRI,RL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.RLCODICE,space(20))
      this.w_DESFIN = NVL(_Link_.RLDESCRI,space(40))
      this.w_TIPFIN = NVL(_Link_.RL__TIPO,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_TIPFIN = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_TIPFIN = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTPFIL_1_4.RadioValue()==this.w_TPFIL)
      this.oPgFrm.Page1.oPag.oTPFIL_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_5.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_5.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_6.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_6.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPINI_1_7.RadioValue()==this.w_TIPINI)
      this.oPgFrm.Page1.oPag.oTIPINI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_9.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_9.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_10.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_10.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPFIN_1_11.RadioValue()==this.w_TIPFIN)
      this.oPgFrm.Page1.oPag.oTIPFIN_1_11.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and (.w_TPFIL<>'ST')  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and (.w_TPFIL<>'ST')  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TPFIL = this.w_TPFIL
    return

enddefine

* --- Define pages as container
define class tgsci_sstPag1 as StdContainer
  Width  = 704
  height = 153
  stdWidth  = 704
  stdheight = 153
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTPFIL_1_4 as StdCombo with uid="YLPSHUSYBI",rtseq=3,rtrep=.f.,left=132,top=10,width=154,height=21;
    , ToolTipText = "Seleziona il tipo di risorsa";
    , HelpContextID = 6049738;
    , cFormVar="w_TPFIL",RowSource=""+"Unit� produttiva,"+"Reparto,"+"Area,"+"Centro di lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTPFIL_1_4.RadioValue()
    return(iif(this.value =1,'UP',;
    iif(this.value =2,'RE',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'CL',;
    space(2))))))
  endfunc
  func oTPFIL_1_4.GetRadio()
    this.Parent.oContained.w_TPFIL = this.RadioValue()
    return .t.
  endfunc

  func oTPFIL_1_4.SetRadio()
    this.Parent.oContained.w_TPFIL=trim(this.Parent.oContained.w_TPFIL)
    this.value = ;
      iif(this.Parent.oContained.w_TPFIL=='UP',1,;
      iif(this.Parent.oContained.w_TPFIL=='RE',2,;
      iif(this.Parent.oContained.w_TPFIL=='AR',3,;
      iif(this.Parent.oContained.w_TPFIL=='CL',4,;
      0))))
  endfunc

  add object oCODINI_1_5 as StdField with uid="XWQGKTYZQX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice risorsa di inizio selezione",;
    HelpContextID = 147033638,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=132, Top=42, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RLCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TPFIL<>'ST')
    endwith
   endif
  endfunc

  func oCODINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RIS_ORSE','*','RLCODICE',cp_AbsName(this.parent,'oCODINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_6 as StdField with uid="CVEJWTLGSI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 147092534,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=300, Top=42, InputMask=replicate('X',40)


  add object oTIPINI_1_7 as StdCombo with uid="EOWOFYCMGP",rtseq=6,rtrep=.f.,left=591,top=42,width=108,height=21, enabled=.f.;
    , HelpContextID = 147081526;
    , cFormVar="w_TIPINI",RowSource=""+"Unit� produttiva,"+"Reparto,"+"Area,"+"Centro di lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPINI_1_7.RadioValue()
    return(iif(this.value =1,'UP',;
    iif(this.value =2,'RE',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'CL',;
    space(2))))))
  endfunc
  func oTIPINI_1_7.GetRadio()
    this.Parent.oContained.w_TIPINI = this.RadioValue()
    return .t.
  endfunc

  func oTIPINI_1_7.SetRadio()
    this.Parent.oContained.w_TIPINI=trim(this.Parent.oContained.w_TIPINI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPINI=='UP',1,;
      iif(this.Parent.oContained.w_TIPINI=='RE',2,;
      iif(this.Parent.oContained.w_TIPINI=='AR',3,;
      iif(this.Parent.oContained.w_TIPINI=='CL',4,;
      0))))
  endfunc

  add object oCODFIN_1_9 as StdField with uid="OWVSGVTSSA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice risorsa di fine selezione",;
    HelpContextID = 225480230,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=132, Top=72, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RLCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TPFIL<>'ST')
    endwith
   endif
  endfunc

  func oCODFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RIS_ORSE','*','RLCODICE',cp_AbsName(this.parent,'oCODFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oDESFIN_1_10 as StdField with uid="RXSKUDEDXI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225539126,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=300, Top=72, InputMask=replicate('X',40)


  add object oTIPFIN_1_11 as StdCombo with uid="WOKZNCWERN",rtseq=9,rtrep=.f.,left=591,top=72,width=108,height=21, enabled=.f.;
    , HelpContextID = 225528118;
    , cFormVar="w_TIPFIN",RowSource=""+"Stabilimento,"+"Reparto,"+"Area,"+"Centro di lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPFIN_1_11.RadioValue()
    return(iif(this.value =1,'ST',;
    iif(this.value =2,'RE',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'CL',;
    space(2))))))
  endfunc
  func oTIPFIN_1_11.GetRadio()
    this.Parent.oContained.w_TIPFIN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFIN_1_11.SetRadio()
    this.Parent.oContained.w_TIPFIN=trim(this.Parent.oContained.w_TIPFIN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFIN=='ST',1,;
      iif(this.Parent.oContained.w_TIPFIN=='RE',2,;
      iif(this.Parent.oContained.w_TIPFIN=='AR',3,;
      iif(this.Parent.oContained.w_TIPFIN=='CL',4,;
      0))))
  endfunc


  add object oBtn_1_13 as StdButton with uid="AYYXYIQLXB",left=597, top=104, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 111638;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSCI_BST with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="ISFHJFDZQQ",left=647, top=104, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 111638;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="PEXAOBPMEM",Visible=.t., Left=5, Top=42,;
    Alignment=1, Width=124, Height=15,;
    Caption="Da codice risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YABJFUEWHX",Visible=.t., Left=21, Top=72,;
    Alignment=1, Width=108, Height=15,;
    Caption="A codice risorsa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EATTOUUGND",Visible=.t., Left=20, Top=11,;
    Alignment=1, Width=109, Height=15,;
    Caption="Tipo risorsa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_sst','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
