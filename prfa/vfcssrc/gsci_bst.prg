* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bst                                                        *
*              Stampa layuot sist. produttivo                                  *
*                                                                              *
*      Author: ZUCCHETTITAM SRL                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-21                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bst",oParentObject)
return(i_retval)

define class tgsci_bst as StdBatch
  * --- Local variables
  w_cOutCursor = space(10)
  w_cInpCursor = space(10)
  w_cRifTable = space(10)
  w_cRifKey = space(10)
  w_cExpTable = space(10)
  w_cExpKey = space(10)
  w_cRepKey = space(10)
  w_cExpField = space(10)
  w_cOtherField = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Layout Sistema produttivo (da GSCI_SSC)
    do vq_exec WITH "..\PRFA\EXE\QUERY\GSCI_BST", this, "Incursor"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Esegue Esplosione
    this.w_cOutCursor = "_TmpCoSiPr_"
    this.w_cInpCursor = "Incursor"
    this.w_cRifTable = "RIS_ORSE"
    this.w_cRifKey = "RLCODICE,RL__TIPO"
    this.w_cExpTable = "STR_CONF"
    this.w_cExpKey = "SCCODPAD,SCTIPPAD"
    this.w_cRepKey = "SCCODFIG,SCTIPFIG"
    this.w_cExpField = ""
    this.w_cOtherField = ""
    do cp_ExpDB with this.w_cOutCursor,this.w_cInpCursor,this.w_cRifTable,this.w_cRifKey,this.w_cExpTable, ;
    this.w_cExpKey, this.w_cRepKey, this.w_cExpField, this.w_cOtherField
    do vq_exec WITH "..\PRFA\EXE\QUERY\GSCI1BST", this, "Dis_riso"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select *, (len(rtrim(lvlkey))-1)/2 AS livello from (this.w_cOutCursor) left outer join Dis_riso on ;
    rlcodice=drcodris order by lvlkey,drdatini into cursor __tmp__ nofilter
    l_codini = this.oParentObject.w_CODINI
    l_codfin = this.oParentObject.w_CODFIN
    l_tofil = this.oParentObject.w_TPFIL
    cp_chprn("..\PRFA\EXE\QUERY\GSCI_SST","",this.oParentObject)
    * --- Chiude Cursore
    if used(this.w_cOutCursor)
      USE IN (this.w_cOutCursor)
    endif
    if used(this.w_cInpCursor)
      USE IN (this.w_cInpCursor)
    endif
    if used("Dis_riso")
      USE IN Dis_riso
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
