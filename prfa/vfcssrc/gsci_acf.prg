* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_acf                                                        *
*              Codifica fasi lavorazione                                       *
*                                                                              *
*      Author: Zucchetti TAM SpA & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-29                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_acf"))

* --- Class definition
define class tgsci_acf as StdForm
  Top    = 14
  Left   = 8

  * --- Standard Properties
  Width  = 843
  Height = 357+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-29"
  HelpContextID=109708649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  COD_FASI_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "COD_FASI"
  cKeySelect = "CFCODICE"
  cKeyWhere  = "CFCODICE=this.w_CFCODICE"
  cKeyWhereODBC = '"CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cKeyWhereODBCqualified = '"COD_FASI.CFCODICE="+cp_ToStrODBC(this.w_CFCODICE)';

  cPrg = "gsci_acf"
  cComment = "Codifica fasi lavorazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CFCODICE = space(5)
  w_CFDESCRI = space(40)
  w_CFFASCOS = space(1)
  w_CFCOUPOI = space(1)
  o_CFCOUPOI = space(1)
  w_CFOUTPUT = space(1)
  w_CFINDETT = space(1)
  w_CFTIPART = space(2)
  w_CFCODINI = space(20)
  o_CFCODINI = space(20)
  w_CFCODFIN = space(20)
  w_CFFAMAIN = space(5)
  o_CFFAMAIN = space(5)
  w_CFFAMAFI = space(5)
  w_CFGRUINI = space(5)
  o_CFGRUINI = space(5)
  w_CFGRUFIN = space(5)
  w_CFCATINI = space(5)
  o_CFCATINI = space(5)
  w_CFCATFIN = space(5)
  w_CFMAGINI = space(5)
  o_CFMAGINI = space(5)
  w_CFMAGFIN = space(5)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESFAMAI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUI = space(35)
  w_DESGRUF = space(35)
  w_DESCATI = space(35)
  w_DESCATF = space(35)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_CADTOBSO = ctod('  /  /  ')
  w_CADTOBS1 = ctod('  /  /  ')
  w_CFMATINP = space(1)
  o_CFMATINP = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TIPART = space(2)
  w_TIPART1 = space(2)
  w_COCODART = space(20)
  w_COCODAR1 = space(20)

  * --- Children pointers
  GSCI_MDF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COD_FASI','gsci_acf')
    stdPageFrame::Init()
    *set procedure to GSCI_MDF additive
    with this
      .Pages(1).addobject("oPag","tgsci_acfPag1","gsci_acf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codifiche")
      .Pages(1).HelpContextID = 155563230
      .Pages(2).addobject("oPag","tgsci_acfPag2","gsci_acf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni materiali input")
      .Pages(2).HelpContextID = 199304786
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCFCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCI_MDF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='FAM_ARTI'
    this.cWorkTables[3]='GRUMERC'
    this.cWorkTables[4]='CATEGOMO'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='COD_FASI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COD_FASI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COD_FASI_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MDF = CREATEOBJECT('stdDynamicChild',this,'GSCI_MDF',this.oPgFrm.Page1.oPag.oLinkPC_1_9)
    this.GSCI_MDF.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MDF)
      this.GSCI_MDF.DestroyChildrenChain()
      this.GSCI_MDF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_9')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MDF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MDF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MDF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCI_MDF.SetKey(;
            .w_CFCODICE,"DFCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCI_MDF.ChangeRow(this.cRowID+'      1',1;
             ,.w_CFCODICE,"DFCODICE";
             )
      .WriteTo_GSCI_MDF()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCI_MDF)
        i_f=.GSCI_MDF.BuildFilter()
        if !(i_f==.GSCI_MDF.cQueryFilter)
          i_fnidx=.GSCI_MDF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCI_MDF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCI_MDF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCI_MDF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCI_MDF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSCI_MDF()
  if at('gsci_mdf',lower(this.GSCI_MDF.class))<>0
    if this.GSCI_MDF.w_MATINP<>this.w_CFMATINP
      this.GSCI_MDF.w_MATINP = this.w_CFMATINP
      this.GSCI_MDF.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CFCODICE = NVL(CFCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COD_FASI where CFCODICE=KeySet.CFCODICE
    *
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COD_FASI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COD_FASI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COD_FASI '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESINI = space(40)
        .w_DESFIN = space(40)
        .w_DESFAMAI = space(35)
        .w_DESFAMAF = space(35)
        .w_DESGRUI = space(35)
        .w_DESGRUF = space(35)
        .w_DESCATI = space(35)
        .w_DESCATF = space(35)
        .w_DESMAGI = space(30)
        .w_DESMAGF = space(30)
        .w_CADTOBSO = ctod("  /  /  ")
        .w_CADTOBS1 = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_TIPART = space(2)
        .w_TIPART1 = space(2)
        .w_COCODART = space(20)
        .w_COCODAR1 = space(20)
        .w_CFCODICE = NVL(CFCODICE,space(5))
        .w_CFDESCRI = NVL(CFDESCRI,space(40))
        .w_CFFASCOS = NVL(CFFASCOS,space(1))
        .w_CFCOUPOI = NVL(CFCOUPOI,space(1))
        .w_CFOUTPUT = NVL(CFOUTPUT,space(1))
        .w_CFINDETT = NVL(CFINDETT,space(1))
        .w_CFTIPART = NVL(CFTIPART,space(2))
        .w_CFCODINI = NVL(CFCODINI,space(20))
          if link_2_2_joined
            this.w_CFCODINI = NVL(CACODICE202,NVL(this.w_CFCODINI,space(20)))
            this.w_DESINI = NVL(CADESART202,space(40))
            this.w_CADTOBSO = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
            this.w_TIPART = NVL(CA__TIPO202,space(2))
            this.w_COCODART = NVL(CACODART202,space(20))
          else
          .link_2_2('Load')
          endif
        .w_CFCODFIN = NVL(CFCODFIN,space(20))
          if link_2_3_joined
            this.w_CFCODFIN = NVL(CACODICE203,NVL(this.w_CFCODFIN,space(20)))
            this.w_DESFIN = NVL(CADESART203,space(40))
            this.w_CADTOBS1 = NVL(cp_ToDate(CADTOBSO203),ctod("  /  /  "))
            this.w_TIPART1 = NVL(CA__TIPO203,space(2))
            this.w_COCODAR1 = NVL(CACODART203,space(20))
          else
          .link_2_3('Load')
          endif
        .w_CFFAMAIN = NVL(CFFAMAIN,space(5))
          if link_2_4_joined
            this.w_CFFAMAIN = NVL(FACODICE204,NVL(this.w_CFFAMAIN,space(5)))
            this.w_DESFAMAI = NVL(FADESCRI204,space(35))
          else
          .link_2_4('Load')
          endif
        .w_CFFAMAFI = NVL(CFFAMAFI,space(5))
          if link_2_5_joined
            this.w_CFFAMAFI = NVL(FACODICE205,NVL(this.w_CFFAMAFI,space(5)))
            this.w_DESFAMAF = NVL(FADESCRI205,space(35))
          else
          .link_2_5('Load')
          endif
        .w_CFGRUINI = NVL(CFGRUINI,space(5))
          if link_2_6_joined
            this.w_CFGRUINI = NVL(GMCODICE206,NVL(this.w_CFGRUINI,space(5)))
            this.w_DESGRUI = NVL(GMDESCRI206,space(35))
          else
          .link_2_6('Load')
          endif
        .w_CFGRUFIN = NVL(CFGRUFIN,space(5))
          if link_2_7_joined
            this.w_CFGRUFIN = NVL(GMCODICE207,NVL(this.w_CFGRUFIN,space(5)))
            this.w_DESGRUF = NVL(GMDESCRI207,space(35))
          else
          .link_2_7('Load')
          endif
        .w_CFCATINI = NVL(CFCATINI,space(5))
          if link_2_8_joined
            this.w_CFCATINI = NVL(OMCODICE208,NVL(this.w_CFCATINI,space(5)))
            this.w_DESCATI = NVL(OMDESCRI208,space(35))
          else
          .link_2_8('Load')
          endif
        .w_CFCATFIN = NVL(CFCATFIN,space(5))
          if link_2_9_joined
            this.w_CFCATFIN = NVL(OMCODICE209,NVL(this.w_CFCATFIN,space(5)))
            this.w_DESCATF = NVL(OMDESCRI209,space(35))
          else
          .link_2_9('Load')
          endif
        .w_CFMAGINI = NVL(CFMAGINI,space(5))
          if link_2_10_joined
            this.w_CFMAGINI = NVL(MGCODMAG210,NVL(this.w_CFMAGINI,space(5)))
            this.w_DESMAGI = NVL(MGDESMAG210,space(30))
          else
          .link_2_10('Load')
          endif
        .w_CFMAGFIN = NVL(CFMAGFIN,space(5))
          if link_2_11_joined
            this.w_CFMAGFIN = NVL(MGCODMAG211,NVL(this.w_CFMAGFIN,space(5)))
            this.w_DESMAGF = NVL(MGDESMAG211,space(30))
          else
          .link_2_11('Load')
          endif
        .w_CFMATINP = NVL(CFMATINP,space(1))
        .oPgFrm.Page2.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
        cp_LoadRecExtFlds(this,'COD_FASI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CFCODICE = space(5)
      .w_CFDESCRI = space(40)
      .w_CFFASCOS = space(1)
      .w_CFCOUPOI = space(1)
      .w_CFOUTPUT = space(1)
      .w_CFINDETT = space(1)
      .w_CFTIPART = space(2)
      .w_CFCODINI = space(20)
      .w_CFCODFIN = space(20)
      .w_CFFAMAIN = space(5)
      .w_CFFAMAFI = space(5)
      .w_CFGRUINI = space(5)
      .w_CFGRUFIN = space(5)
      .w_CFCATINI = space(5)
      .w_CFCATFIN = space(5)
      .w_CFMAGINI = space(5)
      .w_CFMAGFIN = space(5)
      .w_DESINI = space(40)
      .w_DESFIN = space(40)
      .w_DESFAMAI = space(35)
      .w_DESFAMAF = space(35)
      .w_DESGRUI = space(35)
      .w_DESGRUF = space(35)
      .w_DESCATI = space(35)
      .w_DESCATF = space(35)
      .w_DESMAGI = space(30)
      .w_DESMAGF = space(30)
      .w_CADTOBSO = ctod("  /  /  ")
      .w_CADTOBS1 = ctod("  /  /  ")
      .w_CFMATINP = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_TIPART = space(2)
      .w_TIPART1 = space(2)
      .w_COCODART = space(20)
      .w_COCODAR1 = space(20)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CFFASCOS = "S"
        .w_CFCOUPOI = 'N'
        .w_CFOUTPUT = iif(.w_CFCOUPOI="N" or empty(.w_CFOUTPUT), "N", .w_CFOUTPUT)
        .w_CFINDETT = 'S'
        .w_CFTIPART = 'NE'
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CFCODINI))
          .link_2_2('Full')
          endif
        .w_CFCODFIN = .w_CFCODINI
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CFCODFIN))
          .link_2_3('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CFFAMAIN))
          .link_2_4('Full')
          endif
        .w_CFFAMAFI = .w_CFFAMAIN
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_CFFAMAFI))
          .link_2_5('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CFGRUINI))
          .link_2_6('Full')
          endif
        .w_CFGRUFIN = .w_CFGRUINI
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_CFGRUFIN))
          .link_2_7('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_CFCATINI))
          .link_2_8('Full')
          endif
        .w_CFCATFIN = .w_CFCATINI
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_CFCATFIN))
          .link_2_9('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_CFMAGINI))
          .link_2_10('Full')
          endif
        .w_CFMAGFIN = .w_CFMAGINI
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_CFMAGFIN))
          .link_2_11('Full')
          endif
          .DoRTCalc(18,29,.f.)
        .w_CFMATINP = 'N'
        .oPgFrm.Page2.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
        .w_OBTEST = i_datsys
      endif
    endwith
    cp_BlankRecExtFlds(this,'COD_FASI')
    this.DoRTCalc(32,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCFCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCFDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCFFASCOS_1_5.enabled = i_bVal
      .Page1.oPag.oCFCOUPOI_1_6.enabled = i_bVal
      .Page1.oPag.oCFOUTPUT_1_7.enabled = i_bVal
      .Page1.oPag.oCFINDETT_1_10.enabled = i_bVal
      .Page2.oPag.oCFTIPART_2_1.enabled_(i_bVal)
      .Page2.oPag.oCFCODINI_2_2.enabled = i_bVal
      .Page2.oPag.oCFCODFIN_2_3.enabled = i_bVal
      .Page2.oPag.oCFFAMAIN_2_4.enabled = i_bVal
      .Page2.oPag.oCFFAMAFI_2_5.enabled = i_bVal
      .Page2.oPag.oCFGRUINI_2_6.enabled = i_bVal
      .Page2.oPag.oCFGRUFIN_2_7.enabled = i_bVal
      .Page2.oPag.oCFCATINI_2_8.enabled = i_bVal
      .Page2.oPag.oCFCATFIN_2_9.enabled = i_bVal
      .Page2.oPag.oCFMAGINI_2_10.enabled = i_bVal
      .Page2.oPag.oCFMAGFIN_2_11.enabled = i_bVal
      .Page2.oPag.oCFMATINP_2_34.enabled = i_bVal
      .Page2.oPag.oObj_2_35.enabled = i_bVal
      .Page2.oPag.oObj_2_36.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCFCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCFCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSCI_MDF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'COD_FASI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MDF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODICE,"CFCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFDESCRI,"CFDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFFASCOS,"CFFASCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCOUPOI,"CFCOUPOI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFOUTPUT,"CFOUTPUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFINDETT,"CFINDETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFTIPART,"CFTIPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODINI,"CFCODINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCODFIN,"CFCODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFFAMAIN,"CFFAMAIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFFAMAFI,"CFFAMAFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFGRUINI,"CFGRUINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFGRUFIN,"CFGRUFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCATINI,"CFCATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFCATFIN,"CFCATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFMAGINI,"CFMAGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFMAGFIN,"CFMAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CFMATINP,"CFMATINP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
    i_lTable = "COD_FASI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COD_FASI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COD_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COD_FASI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COD_FASI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COD_FASI')
        i_extval=cp_InsertValODBCExtFlds(this,'COD_FASI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CFCODICE,CFDESCRI,CFFASCOS,CFCOUPOI,CFOUTPUT"+;
                  ",CFINDETT,CFTIPART,CFCODINI,CFCODFIN,CFFAMAIN"+;
                  ",CFFAMAFI,CFGRUINI,CFGRUFIN,CFCATINI,CFCATFIN"+;
                  ",CFMAGINI,CFMAGFIN,CFMATINP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CFCODICE)+;
                  ","+cp_ToStrODBC(this.w_CFDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CFFASCOS)+;
                  ","+cp_ToStrODBC(this.w_CFCOUPOI)+;
                  ","+cp_ToStrODBC(this.w_CFOUTPUT)+;
                  ","+cp_ToStrODBC(this.w_CFINDETT)+;
                  ","+cp_ToStrODBC(this.w_CFTIPART)+;
                  ","+cp_ToStrODBCNull(this.w_CFCODINI)+;
                  ","+cp_ToStrODBCNull(this.w_CFCODFIN)+;
                  ","+cp_ToStrODBCNull(this.w_CFFAMAIN)+;
                  ","+cp_ToStrODBCNull(this.w_CFFAMAFI)+;
                  ","+cp_ToStrODBCNull(this.w_CFGRUINI)+;
                  ","+cp_ToStrODBCNull(this.w_CFGRUFIN)+;
                  ","+cp_ToStrODBCNull(this.w_CFCATINI)+;
                  ","+cp_ToStrODBCNull(this.w_CFCATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_CFMAGINI)+;
                  ","+cp_ToStrODBCNull(this.w_CFMAGFIN)+;
                  ","+cp_ToStrODBC(this.w_CFMATINP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COD_FASI')
        i_extval=cp_InsertValVFPExtFlds(this,'COD_FASI')
        cp_CheckDeletedKey(i_cTable,0,'CFCODICE',this.w_CFCODICE)
        INSERT INTO (i_cTable);
              (CFCODICE,CFDESCRI,CFFASCOS,CFCOUPOI,CFOUTPUT,CFINDETT,CFTIPART,CFCODINI,CFCODFIN,CFFAMAIN,CFFAMAFI,CFGRUINI,CFGRUFIN,CFCATINI,CFCATFIN,CFMAGINI,CFMAGFIN,CFMATINP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CFCODICE;
                  ,this.w_CFDESCRI;
                  ,this.w_CFFASCOS;
                  ,this.w_CFCOUPOI;
                  ,this.w_CFOUTPUT;
                  ,this.w_CFINDETT;
                  ,this.w_CFTIPART;
                  ,this.w_CFCODINI;
                  ,this.w_CFCODFIN;
                  ,this.w_CFFAMAIN;
                  ,this.w_CFFAMAFI;
                  ,this.w_CFGRUINI;
                  ,this.w_CFGRUFIN;
                  ,this.w_CFCATINI;
                  ,this.w_CFCATFIN;
                  ,this.w_CFMAGINI;
                  ,this.w_CFMAGFIN;
                  ,this.w_CFMATINP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COD_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COD_FASI_IDX,i_nConn)
      *
      * update COD_FASI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COD_FASI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CFDESCRI="+cp_ToStrODBC(this.w_CFDESCRI)+;
             ",CFFASCOS="+cp_ToStrODBC(this.w_CFFASCOS)+;
             ",CFCOUPOI="+cp_ToStrODBC(this.w_CFCOUPOI)+;
             ",CFOUTPUT="+cp_ToStrODBC(this.w_CFOUTPUT)+;
             ",CFINDETT="+cp_ToStrODBC(this.w_CFINDETT)+;
             ",CFTIPART="+cp_ToStrODBC(this.w_CFTIPART)+;
             ",CFCODINI="+cp_ToStrODBCNull(this.w_CFCODINI)+;
             ",CFCODFIN="+cp_ToStrODBCNull(this.w_CFCODFIN)+;
             ",CFFAMAIN="+cp_ToStrODBCNull(this.w_CFFAMAIN)+;
             ",CFFAMAFI="+cp_ToStrODBCNull(this.w_CFFAMAFI)+;
             ",CFGRUINI="+cp_ToStrODBCNull(this.w_CFGRUINI)+;
             ",CFGRUFIN="+cp_ToStrODBCNull(this.w_CFGRUFIN)+;
             ",CFCATINI="+cp_ToStrODBCNull(this.w_CFCATINI)+;
             ",CFCATFIN="+cp_ToStrODBCNull(this.w_CFCATFIN)+;
             ",CFMAGINI="+cp_ToStrODBCNull(this.w_CFMAGINI)+;
             ",CFMAGFIN="+cp_ToStrODBCNull(this.w_CFMAGFIN)+;
             ",CFMATINP="+cp_ToStrODBC(this.w_CFMATINP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COD_FASI')
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        UPDATE (i_cTable) SET;
              CFDESCRI=this.w_CFDESCRI;
             ,CFFASCOS=this.w_CFFASCOS;
             ,CFCOUPOI=this.w_CFCOUPOI;
             ,CFOUTPUT=this.w_CFOUTPUT;
             ,CFINDETT=this.w_CFINDETT;
             ,CFTIPART=this.w_CFTIPART;
             ,CFCODINI=this.w_CFCODINI;
             ,CFCODFIN=this.w_CFCODFIN;
             ,CFFAMAIN=this.w_CFFAMAIN;
             ,CFFAMAFI=this.w_CFFAMAFI;
             ,CFGRUINI=this.w_CFGRUINI;
             ,CFGRUFIN=this.w_CFGRUFIN;
             ,CFCATINI=this.w_CFCATINI;
             ,CFCATFIN=this.w_CFCATFIN;
             ,CFMAGINI=this.w_CFMAGINI;
             ,CFMAGFIN=this.w_CFMAGFIN;
             ,CFMATINP=this.w_CFMATINP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCI_MDF : Saving
      this.GSCI_MDF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CFCODICE,"DFCODICE";
             )
      this.GSCI_MDF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCI_MDF : Deleting
    this.GSCI_MDF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CFCODICE,"DFCODICE";
           )
    this.GSCI_MDF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COD_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COD_FASI_IDX,i_nConn)
      *
      * delete COD_FASI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CFCODICE',this.w_CFCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CFCOUPOI<>.w_CFCOUPOI
            .w_CFFASCOS = "S"
        endif
        .DoRTCalc(4,4,.t.)
        if .o_CFCOUPOI<>.w_CFCOUPOI
            .w_CFOUTPUT = iif(.w_CFCOUPOI="N" or empty(.w_CFOUTPUT), "N", .w_CFOUTPUT)
        endif
        if  .o_CFMATINP<>.w_CFMATINP
          .WriteTo_GSCI_MDF()
        endif
        .DoRTCalc(6,8,.t.)
        if .o_CFCODINI<>.w_CFCODINI
            .w_CFCODFIN = .w_CFCODINI
          .link_2_3('Full')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_CFFAMAIN<>.w_CFFAMAIN
            .w_CFFAMAFI = .w_CFFAMAIN
          .link_2_5('Full')
        endif
        .DoRTCalc(12,12,.t.)
        if .o_CFGRUINI<>.w_CFGRUINI
            .w_CFGRUFIN = .w_CFGRUINI
          .link_2_7('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_CFCATINI<>.w_CFCATINI
            .w_CFCATFIN = .w_CFCATINI
          .link_2_9('Full')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_CFMAGINI<>.w_CFMAGINI
            .w_CFMAGFIN = .w_CFMAGINI
          .link_2_11('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_35.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_36.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCFFASCOS_1_5.enabled = this.oPgFrm.Page1.oPag.oCFFASCOS_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCFOUTPUT_1_7.enabled = this.oPgFrm.Page1.oPag.oCFOUTPUT_1_7.mCond()
    this.oPgFrm.Page2.oPag.oCFTIPART_2_1.enabled_(this.oPgFrm.Page2.oPag.oCFTIPART_2_1.mCond())
    this.oPgFrm.Page2.oPag.oCFCODINI_2_2.enabled = this.oPgFrm.Page2.oPag.oCFCODINI_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCFCODFIN_2_3.enabled = this.oPgFrm.Page2.oPag.oCFCODFIN_2_3.mCond()
    this.oPgFrm.Page2.oPag.oCFFAMAIN_2_4.enabled = this.oPgFrm.Page2.oPag.oCFFAMAIN_2_4.mCond()
    this.oPgFrm.Page2.oPag.oCFFAMAFI_2_5.enabled = this.oPgFrm.Page2.oPag.oCFFAMAFI_2_5.mCond()
    this.oPgFrm.Page2.oPag.oCFGRUINI_2_6.enabled = this.oPgFrm.Page2.oPag.oCFGRUINI_2_6.mCond()
    this.oPgFrm.Page2.oPag.oCFGRUFIN_2_7.enabled = this.oPgFrm.Page2.oPag.oCFGRUFIN_2_7.mCond()
    this.oPgFrm.Page2.oPag.oCFCATINI_2_8.enabled = this.oPgFrm.Page2.oPag.oCFCATINI_2_8.mCond()
    this.oPgFrm.Page2.oPag.oCFCATFIN_2_9.enabled = this.oPgFrm.Page2.oPag.oCFCATFIN_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCFMAGINI_2_10.enabled = this.oPgFrm.Page2.oPag.oCFMAGINI_2_10.mCond()
    this.oPgFrm.Page2.oPag.oCFMAGFIN_2_11.enabled = this.oPgFrm.Page2.oPag.oCFMAGFIN_2_11.mCond()
    this.GSCI_MDF.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_35.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CFCODINI
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CFCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CFCODINI))
          select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCFCODINI_2_2'),i_cWhere,'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CFCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CFCODINI)
            select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_CADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.CA__TIPO,space(2))
      this.w_COCODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CFCODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_CADTOBSO = ctod("  /  /  ")
      this.w_TIPART = space(2)
      this.w_COCODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CADTOBSO>i_DATSYS or empty(.w_CADTOBSO)) and .w_TIPART ='R' and  (.w_CFCODINI<=.w_CFCODFIN or empty(.w_CFCODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFCODINI = space(20)
        this.w_DESINI = space(40)
        this.w_CADTOBSO = ctod("  /  /  ")
        this.w_TIPART = space(2)
        this.w_COCODART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CADTOBSO as CADTOBSO202"+ ",link_2_2.CA__TIPO as CA__TIPO202"+ ",link_2_2.CACODART as CACODART202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on COD_FASI.CFCODINI=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and COD_FASI.CFCODINI=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFCODFIN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CFCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CFCODFIN))
          select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCFCODFIN_2_3'),i_cWhere,'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CFCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CFCODFIN)
            select CACODICE,CADESART,CADTOBSO,CA__TIPO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_CADTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_TIPART1 = NVL(_Link_.CA__TIPO,space(2))
      this.w_COCODAR1 = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CFCODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_CADTOBS1 = ctod("  /  /  ")
      this.w_TIPART1 = space(2)
      this.w_COCODAR1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CADTOBS1>i_DATSYS or empty(.w_CADTOBS1)) and .w_TIPART1 ='R' and(.w_CFCODINI<=.w_CFCODFIN or empty(.w_CFCODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFCODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_CADTOBS1 = ctod("  /  /  ")
        this.w_TIPART1 = space(2)
        this.w_COCODAR1 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CADTOBSO as CADTOBSO203"+ ",link_2_3.CA__TIPO as CA__TIPO203"+ ",link_2_3.CACODART as CACODART203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on COD_FASI.CFCODFIN=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and COD_FASI.CFCODFIN=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFFAMAIN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFFAMAIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CFFAMAIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CFFAMAIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFFAMAIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFFAMAIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCFFAMAIN_2_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFFAMAIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CFFAMAIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CFFAMAIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFFAMAIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFFAMAIN = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFFAMAIN <= .w_CFFAMAFI OR EMPTY(.w_CFFAMAFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFFAMAIN = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFFAMAIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.FACODICE as FACODICE204"+ ","+cp_TransLinkFldName('link_2_4.FADESCRI')+" as FADESCRI204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on COD_FASI.CFFAMAIN=link_2_4.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and COD_FASI.CFFAMAIN=link_2_4.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFFAMAFI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFFAMAFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CFFAMAFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CFFAMAFI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFFAMAFI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFFAMAFI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCFFAMAFI_2_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFFAMAFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CFFAMAFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CFFAMAFI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFFAMAFI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFFAMAFI = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFFAMAIN <= .w_CFFAMAFI OR EMPTY(.w_CFFAMAIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFFAMAFI = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFFAMAFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.FACODICE as FACODICE205"+ ",link_2_5.FADESCRI as FADESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on COD_FASI.CFFAMAFI=link_2_5.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and COD_FASI.CFFAMAFI=link_2_5.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFGRUINI
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CFGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CFGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCFGRUINI_2_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CFGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CFGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFGRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFGRUINI <= .w_CFGRUFIN OR EMPTY(.w_CFGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFGRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.GMCODICE as GMCODICE206"+ ","+cp_TransLinkFldName('link_2_6.GMDESCRI')+" as GMDESCRI206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on COD_FASI.CFGRUINI=link_2_6.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and COD_FASI.CFGRUINI=link_2_6.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFGRUFIN
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CFGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CFGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCFGRUFIN_2_7'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CFGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CFGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFGRUINI <= .w_CFGRUFIN OR EMPTY(.w_CFGRUINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFGRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.GMCODICE as GMCODICE207"+ ","+cp_TransLinkFldName('link_2_7.GMDESCRI')+" as GMDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on COD_FASI.CFGRUFIN=link_2_7.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and COD_FASI.CFGRUFIN=link_2_7.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFCATINI
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CFCATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CFCATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFCATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFCATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCFCATINI_2_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CFCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CFCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFCATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFCATINI <= .w_CFCATFIN OR EMPTY(.w_CFCATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFCATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.OMCODICE as OMCODICE208"+ ","+cp_TransLinkFldName('link_2_8.OMDESCRI')+" as OMDESCRI208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on COD_FASI.CFCATINI=link_2_8.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and COD_FASI.CFCATINI=link_2_8.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFCATFIN
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CFCATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CFCATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFCATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCFCATFIN_2_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CFCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CFCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CFCATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFCATINI <= .w_CFCATFIN OR EMPTY(.w_CFCATINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFCATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.OMCODICE as OMCODICE209"+ ","+cp_TransLinkFldName('link_2_9.OMDESCRI')+" as OMDESCRI209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on COD_FASI.CFCATFIN=link_2_9.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and COD_FASI.CFCATFIN=link_2_9.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFMAGINI
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CFMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CFMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCFMAGINI_2_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CFMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CFMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CFMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFMAGINI <= .w_CFMAGFIN OR EMPTY(.w_CFMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.MGCODMAG as MGCODMAG210"+ ",link_2_10.MGDESMAG as MGDESMAG210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on COD_FASI.CFMAGINI=link_2_10.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and COD_FASI.CFMAGINI=link_2_10.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CFMAGFIN
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CFMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CFMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CFMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CFMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CFMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCFMAGFIN_2_11'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CFMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CFMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CFMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CFMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CFMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CFMAGINI <= .w_CFMAGFIN OR EMPTY(.w_CFMAGINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CFMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CFMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.MGCODMAG as MGCODMAG211"+ ",link_2_11.MGDESMAG as MGDESMAG211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on COD_FASI.CFMAGFIN=link_2_11.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and COD_FASI.CFMAGFIN=link_2_11.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value==this.w_CFCODICE)
      this.oPgFrm.Page1.oPag.oCFCODICE_1_1.value=this.w_CFCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDESCRI_1_3.value==this.w_CFDESCRI)
      this.oPgFrm.Page1.oPag.oCFDESCRI_1_3.value=this.w_CFDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCFFASCOS_1_5.RadioValue()==this.w_CFFASCOS)
      this.oPgFrm.Page1.oPag.oCFFASCOS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFCOUPOI_1_6.RadioValue()==this.w_CFCOUPOI)
      this.oPgFrm.Page1.oPag.oCFCOUPOI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFOUTPUT_1_7.RadioValue()==this.w_CFOUTPUT)
      this.oPgFrm.Page1.oPag.oCFOUTPUT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCFINDETT_1_10.RadioValue()==this.w_CFINDETT)
      this.oPgFrm.Page1.oPag.oCFINDETT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCFTIPART_2_1.RadioValue()==this.w_CFTIPART)
      this.oPgFrm.Page2.oPag.oCFTIPART_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCFCODINI_2_2.value==this.w_CFCODINI)
      this.oPgFrm.Page2.oPag.oCFCODINI_2_2.value=this.w_CFCODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCFCODFIN_2_3.value==this.w_CFCODFIN)
      this.oPgFrm.Page2.oPag.oCFCODFIN_2_3.value=this.w_CFCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCFFAMAIN_2_4.value==this.w_CFFAMAIN)
      this.oPgFrm.Page2.oPag.oCFFAMAIN_2_4.value=this.w_CFFAMAIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCFFAMAFI_2_5.value==this.w_CFFAMAFI)
      this.oPgFrm.Page2.oPag.oCFFAMAFI_2_5.value=this.w_CFFAMAFI
    endif
    if not(this.oPgFrm.Page2.oPag.oCFGRUINI_2_6.value==this.w_CFGRUINI)
      this.oPgFrm.Page2.oPag.oCFGRUINI_2_6.value=this.w_CFGRUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCFGRUFIN_2_7.value==this.w_CFGRUFIN)
      this.oPgFrm.Page2.oPag.oCFGRUFIN_2_7.value=this.w_CFGRUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCFCATINI_2_8.value==this.w_CFCATINI)
      this.oPgFrm.Page2.oPag.oCFCATINI_2_8.value=this.w_CFCATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCFCATFIN_2_9.value==this.w_CFCATFIN)
      this.oPgFrm.Page2.oPag.oCFCATFIN_2_9.value=this.w_CFCATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCFMAGINI_2_10.value==this.w_CFMAGINI)
      this.oPgFrm.Page2.oPag.oCFMAGINI_2_10.value=this.w_CFMAGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCFMAGFIN_2_11.value==this.w_CFMAGFIN)
      this.oPgFrm.Page2.oPag.oCFMAGFIN_2_11.value=this.w_CFMAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_14.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_14.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_15.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_15.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_17.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_17.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAF_2_18.value==this.w_DESFAMAF)
      this.oPgFrm.Page2.oPag.oDESFAMAF_2_18.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUI_2_21.value==this.w_DESGRUI)
      this.oPgFrm.Page2.oPag.oDESGRUI_2_21.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUF_2_22.value==this.w_DESGRUF)
      this.oPgFrm.Page2.oPag.oDESGRUF_2_22.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_26.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_26.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATF_2_27.value==this.w_DESCATF)
      this.oPgFrm.Page2.oPag.oDESCATF_2_27.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGI_2_30.value==this.w_DESMAGI)
      this.oPgFrm.Page2.oPag.oDESMAGI_2_30.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGF_2_31.value==this.w_DESMAGF)
      this.oPgFrm.Page2.oPag.oDESMAGF_2_31.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page2.oPag.oCFMATINP_2_34.RadioValue()==this.w_CFMATINP)
      this.oPgFrm.Page2.oPag.oCFMATINP_2_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'COD_FASI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CFCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CFCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CADTOBSO>i_DATSYS or empty(.w_CADTOBSO)) and .w_TIPART ='R' and  (.w_CFCODINI<=.w_CFCODFIN or empty(.w_CFCODFIN)))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFCODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFCODINI_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CADTOBS1>i_DATSYS or empty(.w_CADTOBS1)) and .w_TIPART1 ='R' and(.w_CFCODINI<=.w_CFCODFIN or empty(.w_CFCODINI)))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFCODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFCODFIN_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFFAMAIN <= .w_CFFAMAFI OR EMPTY(.w_CFFAMAFI))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFFAMAIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFFAMAIN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFFAMAIN <= .w_CFFAMAFI OR EMPTY(.w_CFFAMAIN))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFFAMAFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFFAMAFI_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFGRUINI <= .w_CFGRUFIN OR EMPTY(.w_CFGRUFIN))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFGRUINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFGRUINI_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFGRUINI <= .w_CFGRUFIN OR EMPTY(.w_CFGRUINI))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFGRUFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFGRUFIN_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFCATINI <= .w_CFCATFIN OR EMPTY(.w_CFCATFIN))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFCATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFCATINI_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFCATINI <= .w_CFCATFIN OR EMPTY(.w_CFCATINI))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFCATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFCATFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFMAGINI <= .w_CFMAGFIN OR EMPTY(.w_CFMAGFIN))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFMAGINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFMAGINI_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CFMAGINI <= .w_CFMAGFIN OR EMPTY(.w_CFMAGINI))  and (.w_CFINDETT='S' and .w_CFTIPART<>'N')  and not(empty(.w_CFMAGFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCFMAGFIN_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MDF.CheckForm()
      if i_bres
        i_bres=  .GSCI_MDF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CFCOUPOI = this.w_CFCOUPOI
    this.o_CFCODINI = this.w_CFCODINI
    this.o_CFFAMAIN = this.w_CFFAMAIN
    this.o_CFGRUINI = this.w_CFGRUINI
    this.o_CFCATINI = this.w_CFCATINI
    this.o_CFMAGINI = this.w_CFMAGINI
    this.o_CFMATINP = this.w_CFMATINP
    * --- GSCI_MDF : Depends On
    this.GSCI_MDF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsci_acfPag1 as StdContainer
  Width  = 871
  height = 360
  stdWidth  = 871
  stdheight = 360
  resizeXpos=442
  resizeYpos=248
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFCODICE_1_1 as StdField with uid="MHTTYOBJET",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CFCODICE", cQueryName = "CFCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 118060395,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=119, Top=10, InputMask=replicate('X',5)

  add object oCFDESCRI_1_3 as StdField with uid="WXYDGJDHOM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CFDESCRI", cQueryName = "CFDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32474479,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=275, Top=10, InputMask=replicate('X',40)

  add object oCFFASCOS_1_5 as StdCheck with uid="LOOVVDQQPI",rtseq=3,rtrep=.f.,left=119, top=37, caption="Considera per costificazione",;
    ToolTipText = "Considera per costificazione",;
    HelpContextID = 32220537,;
    cFormVar="w_CFFASCOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCFFASCOS_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFFASCOS_1_5.GetRadio()
    this.Parent.oContained.w_CFFASCOS = this.RadioValue()
    return .t.
  endfunc

  func oCFFASCOS_1_5.SetRadio()
    this.Parent.oContained.w_CFFASCOS=trim(this.Parent.oContained.w_CFFASCOS)
    this.value = ;
      iif(this.Parent.oContained.w_CFFASCOS=='S',1,;
      0)
  endfunc

  func oCFFASCOS_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFCOUPOI<>"S")
    endwith
   endif
  endfunc

  add object oCFCOUPOI_1_6 as StdCheck with uid="VOJSACGEOF",rtseq=4,rtrep=.f.,left=332, top=37, caption="Count point",;
    ToolTipText = "Fase Count Point",;
    HelpContextID = 253326703,;
    cFormVar="w_CFCOUPOI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCFCOUPOI_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFCOUPOI_1_6.GetRadio()
    this.Parent.oContained.w_CFCOUPOI = this.RadioValue()
    return .t.
  endfunc

  func oCFCOUPOI_1_6.SetRadio()
    this.Parent.oContained.w_CFCOUPOI=trim(this.Parent.oContained.w_CFCOUPOI)
    this.value = ;
      iif(this.Parent.oContained.w_CFCOUPOI=='S',1,;
      0)
  endfunc

  add object oCFOUTPUT_1_7 as StdCheck with uid="JBOWIFHODS",rtseq=5,rtrep=.f.,left=467, top=37, caption="Output",;
    ToolTipText = "Fase di output",;
    HelpContextID = 252720506,;
    cFormVar="w_CFOUTPUT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCFOUTPUT_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFOUTPUT_1_7.GetRadio()
    this.Parent.oContained.w_CFOUTPUT = this.RadioValue()
    return .t.
  endfunc

  func oCFOUTPUT_1_7.SetRadio()
    this.Parent.oContained.w_CFOUTPUT=trim(this.Parent.oContained.w_CFOUTPUT)
    this.value = ;
      iif(this.Parent.oContained.w_CFOUTPUT=='S',1,;
      0)
  endfunc

  func oCFOUTPUT_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFCOUPOI="S")
    endwith
   endif
  endfunc


  add object oLinkPC_1_9 as stdDynamicChildContainer with uid="ABTYUKZVEA",left=1, top=67, width=836, height=293, bOnScreen=.t.;


  func oLinkPC_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFINDETT='S')
      endwith
    endif
  endfunc

  add object oCFINDETT_1_10 as StdCheck with uid="UWLCUTBPWC",rtseq=6,rtrep=.f.,left=567, top=37, caption="Inserimento dettaglio",;
    HelpContextID = 50910586,;
    cFormVar="w_CFINDETT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCFINDETT_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFINDETT_1_10.GetRadio()
    this.Parent.oContained.w_CFINDETT = this.RadioValue()
    return .t.
  endfunc

  func oCFINDETT_1_10.SetRadio()
    this.Parent.oContained.w_CFINDETT=trim(this.Parent.oContained.w_CFINDETT)
    this.value = ;
      iif(this.Parent.oContained.w_CFINDETT=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="YFYYVABJRS",Visible=.t., Left=59, Top=10,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="QLNUQWXQSK",Visible=.t., Left=185, Top=10,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="LSQBXMHXCM",Visible=.t., Left=7, Top=38,;
    Alignment=1, Width=105, Height=15,;
    Caption="Valori di default:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsci_acfPag2 as StdContainer
  Width  = 871
  height = 360
  stdWidth  = 871
  stdheight = 360
  resizeXpos=472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCFTIPART_2_1 as StdRadio with uid="YOZHOZUYIG",rtseq=7,rtrep=.f.,left=8, top=17, width=865,height=23;
    , ToolTipText = "Tipo articolo";
    , cFormVar="w_CFTIPART", ButtonCount=6, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCFTIPART_2_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Prodotto finito"
      this.Buttons(1).HelpContextID = 264537466
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Prodotto finito","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Semilavorato"
      this.Buttons(2).HelpContextID = 264537466
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Semilavorato","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Materia prima"
      this.Buttons(3).HelpContextID = 264537466
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Materia prima","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="Fantasma"
      this.Buttons(4).HelpContextID = 264537466
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("Fantasma","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="Tutti"
      this.Buttons(5).HelpContextID = 264537466
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.Buttons(6).Caption="Nessuno"
      this.Buttons(6).HelpContextID = 264537466
      this.Buttons(6).Left=i_coord
      this.Buttons(6).Width=(TxtWidth("Nessuno","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(6).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo articolo")
      StdRadio::init()
    endproc

  func oCFTIPART_2_1.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'MP',;
    iif(this.value =4,'PH',;
    iif(this.value =5,'TU',;
    iif(this.value =6,'NE',;
    space(2))))))))
  endfunc
  func oCFTIPART_2_1.GetRadio()
    this.Parent.oContained.w_CFTIPART = this.RadioValue()
    return .t.
  endfunc

  func oCFTIPART_2_1.SetRadio()
    this.Parent.oContained.w_CFTIPART=trim(this.Parent.oContained.w_CFTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_CFTIPART=='PF',1,;
      iif(this.Parent.oContained.w_CFTIPART=='SE',2,;
      iif(this.Parent.oContained.w_CFTIPART=='MP',3,;
      iif(this.Parent.oContained.w_CFTIPART=='PH',4,;
      iif(this.Parent.oContained.w_CFTIPART=='TU',5,;
      iif(this.Parent.oContained.w_CFTIPART=='NE',6,;
      0))))))
  endfunc

  func oCFTIPART_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S')
    endwith
   endif
  endfunc

  add object oCFCODINI_2_2 as StdField with uid="VJUIQNMPMF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CFCODINI", cQueryName = "CFCODINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 150375057,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=64, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CFCODINI"

  func oCFCODINI_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFCODINI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFCODINI_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFCODINI_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCFCODINI_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCFCODINI_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CFCODINI
     i_obj.ecpSave()
  endproc

  add object oCFCODFIN_2_3 as StdField with uid="MSEYIBODRF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CFCODFIN", cQueryName = "CFCODFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 200706700,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=92, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CFCODFIN"

  func oCFCODFIN_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFCODFIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFCODFIN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFCODFIN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCFCODFIN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCFCODFIN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CFCODFIN
     i_obj.ecpSave()
  endproc

  add object oCFFAMAIN_2_4 as StdField with uid="ZYGJEMMUHX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CFFAMAIN", cQueryName = "CFFAMAIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia articolo di inizio selezione",;
    HelpContextID = 7625356,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=96, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CFFAMAIN"

  func oCFFAMAIN_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFFAMAIN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFFAMAIN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFFAMAIN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCFFAMAIN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFFAMAFI_2_5 as StdField with uid="JTWSMHFJVL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CFFAMAFI", cQueryName = "CFFAMAFI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia articolo di fine selezione",;
    HelpContextID = 260810095,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=521, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CFFAMAFI"

  func oCFFAMAFI_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFFAMAFI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFFAMAFI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFFAMAFI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCFFAMAFI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFGRUINI_2_6 as StdField with uid="PPCZSNHRHK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CFGRUINI", cQueryName = "CFGRUINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di inizio selezione",;
    HelpContextID = 132336273,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=96, Top=148, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_CFGRUINI"

  func oCFGRUINI_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFGRUINI_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFGRUINI_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFGRUINI_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCFGRUINI_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFGRUFIN_2_7 as StdField with uid="ZEPUHRYJOE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CFGRUFIN", cQueryName = "CFGRUFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di fine selezione",;
    HelpContextID = 182667916,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=521, Top=148, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_CFGRUFIN"

  func oCFGRUFIN_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFGRUFIN_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFGRUFIN_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFGRUFIN_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCFGRUFIN_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFCATINI_2_8 as StdField with uid="JJZUBYFATH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CFCATINI", cQueryName = "CFCATINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di inizio selezione",;
    HelpContextID = 134515345,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=96, Top=176, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CFCATINI"

  func oCFCATINI_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFCATINI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFCATINI_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFCATINI_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCFCATINI_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFCATFIN_2_9 as StdField with uid="IULOKGMXRT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CFCATFIN", cQueryName = "CFCATFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di fine selezione",;
    HelpContextID = 184846988,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=521, Top=176, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CFCATFIN"

  func oCFCATFIN_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFCATFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFCATFIN_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFCATFIN_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCFCATFIN_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFMAGINI_2_10 as StdField with uid="UXQRMJNVVO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CFMAGINI", cQueryName = "CFMAGINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di inizio selezione",;
    HelpContextID = 148105873,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=96, Top=204, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CFMAGINI"

  func oCFMAGINI_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFMAGINI_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFMAGINI_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFMAGINI_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCFMAGINI_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCFMAGFIN_2_11 as StdField with uid="CJYZIDLNLO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CFMAGFIN", cQueryName = "CFMAGFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di fine selezione",;
    HelpContextID = 198437516,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=521, Top=204, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CFMAGFIN"

  func oCFMAGFIN_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFINDETT='S' and .w_CFTIPART<>'N')
    endwith
   endif
  endfunc

  func oCFMAGFIN_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCFMAGFIN_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCFMAGFIN_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCFMAGFIN_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESINI_2_14 as StdField with uid="RMRSVPWTTW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 128218166,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=64, InputMask=replicate('X',40)

  add object oDESFIN_2_15 as StdField with uid="WNCBVMQXJP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 206664758,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=92, InputMask=replicate('X',40)

  add object oDESFAMAI_2_17 as StdField with uid="SIVYJXCYNN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 181499007,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=153, Top=120, InputMask=replicate('X',35)

  add object oDESFAMAF_2_18 as StdField with uid="NFNOGTJDZS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 181499004,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=578, Top=120, InputMask=replicate('X',35)

  add object oDESGRUI_2_21 as StdField with uid="ZIMMJGAQYV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203262922,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=153, Top=148, InputMask=replicate('X',35)

  add object oDESGRUF_2_22 as StdField with uid="RJTZTHRRIC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203262922,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=578, Top=148, InputMask=replicate('X',35)

  add object oDESCATI_2_26 as StdField with uid="BPCWHRPMRT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 238128074,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=153, Top=176, InputMask=replicate('X',35)

  add object oDESCATF_2_27 as StdField with uid="BGCZHXXLEB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 238128074,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=578, Top=176, InputMask=replicate('X',35)

  add object oDESMAGI_2_30 as StdField with uid="LZCAJPCYGO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 187141066,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=153, Top=204, InputMask=replicate('X',30)

  add object oDESMAGF_2_31 as StdField with uid="WHIVDJTNRD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 81294390,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=578, Top=204, InputMask=replicate('X',30)

  add object oCFMATINP_2_34 as StdCheck with uid="SSPQNCYNBU",rtseq=30,rtrep=.f.,left=96, top=234, caption="Abilita dettaglio materiali di input",;
    ToolTipText = "Abilita dettaglio materiali di input",;
    HelpContextID = 134474378,;
    cFormVar="w_CFMATINP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCFMATINP_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFMATINP_2_34.GetRadio()
    this.Parent.oContained.w_CFMATINP = this.RadioValue()
    return .t.
  endfunc

  func oCFMATINP_2_34.SetRadio()
    this.Parent.oContained.w_CFMATINP=trim(this.Parent.oContained.w_CFMATINP)
    this.value = ;
      iif(this.Parent.oContained.w_CFMATINP=='S',1,;
      0)
  endfunc


  add object oObj_2_35 as cp_runprogram with uid="TLICETRJYG",left=88, top=378, width=202,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCI_BCF('MATINP')",;
    cEvent = "w_CFMATINP Changed",;
    nPag=2;
    , HelpContextID = 68288998


  add object oObj_2_36 as cp_runprogram with uid="LTQXUCIFJM",left=88, top=407, width=202,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCI_BCF('LOAD')",;
    cEvent = "Load,Blank",;
    nPag=2;
    , HelpContextID = 68288998

  add object oStr_2_12 as StdString with uid="QJXAEXWPZQ",Visible=.t., Left=33, Top=64,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="WKGMAYZGSA",Visible=.t., Left=36, Top=92,;
    Alignment=1, Width=59, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="VBDXLMPBNH",Visible=.t., Left=21, Top=120,;
    Alignment=1, Width=74, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="BYUFMJFEXF",Visible=.t., Left=452, Top=120,;
    Alignment=1, Width=67, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="MOKNYUJCHZ",Visible=.t., Left=13, Top=148,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="FCBAZHBPWI",Visible=.t., Left=443, Top=148,;
    Alignment=1, Width=76, Height=18,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="NKAISASGNB",Visible=.t., Left=8, Top=176,;
    Alignment=1, Width=87, Height=18,;
    Caption="Da cat. om.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="MRUCTUZCHJ",Visible=.t., Left=441, Top=176,;
    Alignment=1, Width=78, Height=18,;
    Caption="A cat. om.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="UYXWMFFLNI",Visible=.t., Left=458, Top=204,;
    Alignment=1, Width=61, Height=18,;
    Caption="A magaz.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="CEEQJCTHUY",Visible=.t., Left=16, Top=204,;
    Alignment=1, Width=79, Height=18,;
    Caption="Da magaz.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_acf','COD_FASI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CFCODICE=COD_FASI.CFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
