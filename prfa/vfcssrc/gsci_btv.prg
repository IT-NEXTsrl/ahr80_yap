* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_btv                                                        *
*              Esplode configurazione sistema                                  *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_btv",oParentObject,m.pAzione)
return(i_retval)

define class tgsci_btv as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_CODPAD = space(20)
  w_InCursor = space(10)
  w_GESTIONE = .NULL.
  w_OK = .f.
  w_CONTA = 0
  w_ZOOMCUR = space(10)
  w_MESS = space(100)
  w_MSG = space(100)
  w_NODOSELE = space(20)
  w_NODOTIPO = space(2)
  w_SCUNIPRO = space(20)
  w_SCTIPUNP = space(2)
  w_SCCODPAD = space(20)
  w_SCTIPPAD = space(2)
  w_SCCODFIG = space(20)
  w_SCTIPFIG = space(2)
  w_RL__TIPO = space(2)
  w_TMPC = space(100)
  w_RLCODCAL = space(5)
  w_RLMAGWIP = space(5)
  Padre = .NULL.
  w_AlbeDist = .NULL.
  w_cOutCursor = space(10)
  w_cInpCursor = space(10)
  w_cRifTable = space(10)
  w_cRifKey = space(10)
  w_cExpTable = space(10)
  w_cExpKey = space(10)
  w_cRepKey = space(10)
  w_cExpField = space(10)
  w_cOtherField = space(10)
  * --- WorkFile variables
  PAR_PROD_idx=0
  STR_CONF_idx=0
  RIS_ORSE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cursore con i dati da esplodere
    this.w_InCursor = sys(2015)
    * --- Nodo treeview
    * --- --
    * --- Eredita' dal padre
    this.Padre = this.oParentObject
    do case
      case this.pAzione = "Reload"
        if .F.
          this.w_CODPAD = ""
          * --- Read from PAR_PROD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PPCODSTA"+;
              " from "+i_cTable+" PAR_PROD where ";
                  +"PPCODICE = "+cp_ToStrODBC("PP");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PPCODSTA;
              from (i_cTable) where;
                  PPCODICE = "PP";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODPAD = NVL(cp_ToDate(_read_.PPCODSTA),cp_NullValue(_read_.PPCODSTA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty(this.w_CODPAD)
            this.w_MSG = "Attenzione:%0Codice stabilimento non definito in tabella Parametri produzione"
            ah_ErrorMsg(this.w_Msg,16)
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_SCUNIPRO = this.Padre.w_SCUNIPRO
        this.w_SCTIPUNP = this.Padre.w_SCTIPUNP
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione="Relazione"
        this.w_SCUNIPRO = this.Padre.w_SCUNIPRO
        this.w_SCTIPUNP = this.Padre.w_SCTIPUNP
        this.w_SCCODPAD = this.Padre.w_SCCODPAD
        this.w_SCTIPPAD = this.Padre.w_SCTIPPAD
        do case
          case this.w_SCTIPPAD="UP"
            this.w_RL__TIPO = "RE"
          case this.w_SCTIPPAD="RE"
            this.w_RL__TIPO = "AR"
          case this.w_SCTIPPAD="AR"
            this.w_RL__TIPO = "CL"
          case this.w_SCTIPPAD="CL"
            this.w_RL__TIPO = "MA"
        endcase
        * --- Faccio una query per vedere se il nodo selezionato ha dei figli
        * --- Read from STR_CONF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STR_CONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STR_CONF_idx,2],.t.,this.STR_CONF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SCCODFIG"+;
            " from "+i_cTable+" STR_CONF where ";
                +"SCUNIPRO = "+cp_ToStrODBC(this.w_SCUNIPRO);
                +" and SCTIPUNP = "+cp_ToStrODBC(this.w_SCTIPUNP);
                +" and SCCODPAD = "+cp_ToStrODBC(this.w_SCCODPAD);
                +" and SCTIPPAD = "+cp_ToStrODBC(this.w_SCTIPPAD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SCCODFIG;
            from (i_cTable) where;
                SCUNIPRO = this.w_SCUNIPRO;
                and SCTIPUNP = this.w_SCTIPUNP;
                and SCCODPAD = this.w_SCCODPAD;
                and SCTIPPAD = this.w_SCTIPPAD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TMPC = NVL(cp_ToDate(_read_.SCCODFIG),cp_NullValue(_read_.SCCODFIG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows = 0
          do GSCI_KRR with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Alla chiusura della maschera eseguo il refresh solo se generato un nuovo legame
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_GESTIONE = GSCI_MSC()
          this.w_GESTIONE.ecpFilter()     
          this.w_GESTIONE.w_SCUNIPRO = this.w_SCUNIPRO
          this.w_GESTIONE.w_SCTIPPAD = this.w_SCTIPPAD
          this.w_GESTIONE.w_SCCODPAD = this.w_SCCODPAD
          this.w_GESTIONE.ecpSave()     
          this.w_GESTIONE.Hide()     
          this.w_GESTIONE.Show(1)     
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAzione="Risorsa"
        this.w_NODOSELE = this.Padre.w_Albedist.GetVar("RLCODICE")
        this.w_NODOTIPO = this.Padre.w_Albedist.GetVar("RL__TIPO")
        if !Empty(nvl(this.w_NODOSELE,""))
          do case
            case this.w_NODOTIPO="UP"
              this.w_GESTIONE = GSCI_AUP()
            case this.w_NODOTIPO="RE"
              this.w_GESTIONE = GSCI_ARE()
            case this.w_NODOTIPO="AR"
              this.w_GESTIONE = GSCI_AAR()
            case this.w_NODOTIPO="CL"
              this.w_GESTIONE = GSCI_ACL()
            case this.w_NODOTIPO="MA"
              this.w_GESTIONE = GSCI_AMA()
          endcase
          this.w_GESTIONE.ecpFilter()     
          this.w_GESTIONE.w_RLCODICE = this.w_NODOSELE
          this.w_GESTIONE.ecpSave()     
        endif
      case this.pAzione="CheckDett"
        this.w_OK = .T.
        this.w_SCTIPUNP = this.Padre.w_SCTIPUNP
        this.w_SCUNIPRO = this.Padre.w_SCUNIPRO
        this.w_SCTIPPAD = this.Padre.w_SCTIPPAD
        this.w_SCCODPAD = this.Padre.w_SCCODPAD
        this.w_SCTIPFIG = this.Padre.w_SCTIPFIG
        this.w_SCCODFIG = this.Padre.w_SCCODFIG
        if this.w_OK
          VQ_EXEC("..\PRFA\EXE\QUERY\GSCIDBTV.VQR",this,"CONTA")
          this.w_CONTA = NVL(CONTA.CONTA,0)
          if this.w_CONTA>0
            * --- Componente gi� utilizzato
            this.w_MESS = ah_MsgFormat("La risorsa %1 di tipologia %2 � gi� utilizzato in un altro legame",this.w_SCCODFIG,this.w_SCTIPFIG)
            this.w_OK = .F.
          else
            this.w_RLCODCAL = iif(empty(this.oParentObject.w_RLCALRIS), iif(this.w_SCTIPPAD="UP", this.oParentObject.w_PADCAL, this.oParentObject.w_ERECAL), this.oParentObject.w_RLCALRIS) 
            this.w_RLMAGWIP = iif(empty(this.oParentObject.w_RLWIPRIS), iif(this.w_SCTIPPAD="UP", this.oParentObject.w_PADWIP, this.oParentObject.w_EREWIP), this.oParentObject.w_RLWIPRIS)
            * --- Write into RIS_ORSE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"RLCODCAL ="+cp_NullLink(cp_ToStrODBC(this.w_RLCODCAL),'RIS_ORSE','RLCODCAL');
              +",RLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_RLMAGWIP),'RIS_ORSE','RLMAGWIP');
                  +i_ccchkf ;
              +" where ";
                  +"RLCODICE = "+cp_ToStrODBC(this.w_SCCODFIG);
                  +" and RL__TIPO = "+cp_ToStrODBC(this.w_SCTIPFIG);
                     )
            else
              update (i_cTable) set;
                  RLCODCAL = this.w_RLCODCAL;
                  ,RLMAGWIP = this.w_RLMAGWIP;
                  &i_ccchkf. ;
               where;
                  RLCODICE = this.w_SCCODFIG;
                  and RL__TIPO = this.w_SCTIPFIG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if USED("CONTA")
            SELECT ("CONTA")
            USE
          endif
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pAzione="SalvaDaZoom"
        this.w_ZOOMCUR = this.Padre.w_ZOOM.cCursor
        this.w_SCTIPUNP = this.Padre.w_SCTIPUNP
        this.w_SCUNIPRO = this.Padre.w_SCUNIPRO
        this.w_SCTIPPAD = this.Padre.w_SCTIPPAD
        this.w_SCCODPAD = this.Padre.w_SCCODPAD
        * --- Try
        local bErr_03884AB8
        bErr_03884AB8=bTrsErr
        this.Try_03884AB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=MSG_TRANSACTION_ERROR
          ah_ErrorMsg("Errore durante la scrittura del legame","stop","")
        endif
        bTrsErr=bTrsErr or bErr_03884AB8
        * --- End
    endcase
  endproc
  proc Try_03884AB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select (this.w_ZOOMCUR)
    Go Top
    Count For xChk=1 to this.w_CONTA
    Scan For XChk=1
    this.w_SCTIPFIG = RL__TIPO
    this.w_SCCODFIG = RLCODICE
    this.w_RLCODCAL = iif(empty(nvl(RLCALRIS,space(5))), this.oParentObject.w_ERECAL, RLCALRIS)
    this.w_RLMAGWIP = iif(empty(nvl(RLWIPRIS,space(5))), this.oParentObject.w_EREWIP, RLWIPRIS)
    * --- Write into RIS_ORSE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RLCODCAL ="+cp_NullLink(cp_ToStrODBC(this.w_RLCODCAL),'RIS_ORSE','RLCODCAL');
      +",RLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_RLMAGWIP),'RIS_ORSE','RLMAGWIP');
          +i_ccchkf ;
      +" where ";
          +"RLCODICE = "+cp_ToStrODBC(this.w_SCCODFIG);
          +" and RL__TIPO = "+cp_ToStrODBC(this.w_SCTIPFIG);
             )
    else
      update (i_cTable) set;
          RLCODCAL = this.w_RLCODCAL;
          ,RLMAGWIP = this.w_RLMAGWIP;
          &i_ccchkf. ;
       where;
          RLCODICE = this.w_SCCODFIG;
          and RL__TIPO = this.w_SCTIPFIG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into STR_CONF
    i_nConn=i_TableProp[this.STR_CONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STR_CONF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STR_CONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCUNIPRO"+",SCTIPUNP"+",SCCODPAD"+",SCTIPPAD"+",SCCODFIG"+",SCTIPFIG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SCUNIPRO),'STR_CONF','SCUNIPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPUNP),'STR_CONF','SCTIPUNP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODPAD),'STR_CONF','SCCODPAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPPAD),'STR_CONF','SCTIPPAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCCODFIG),'STR_CONF','SCCODFIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPFIG),'STR_CONF','SCTIPFIG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCUNIPRO',this.w_SCUNIPRO,'SCTIPUNP',this.w_SCTIPUNP,'SCCODPAD',this.w_SCCODPAD,'SCTIPPAD',this.w_SCTIPPAD,'SCCODFIG',this.w_SCCODFIG,'SCTIPFIG',this.w_SCTIPFIG)
      insert into (i_cTable) (SCUNIPRO,SCTIPUNP,SCCODPAD,SCTIPPAD,SCCODFIG,SCTIPFIG &i_ccchkf. );
         values (;
           this.w_SCUNIPRO;
           ,this.w_SCTIPUNP;
           ,this.w_SCCODPAD;
           ,this.w_SCTIPPAD;
           ,this.w_SCCODFIG;
           ,this.w_SCTIPFIG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Select (this.w_ZOOMCUR)
    EndScan
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Legame creato","!","")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea Cursore di Input
    if EMPTY(this.oParentObject.w_CURSTREE)
      this.oParentObject.w_CURSTREE = SYS(2015)
    endif
    this.w_AlbeDist = this.Padre.w_AlbeDist
    if !Empty(this.w_SCUNIPRO)
      * --- Query di input
      do vq_exec with "..\prfa\exe\query\GSCI0BTV", this, this.w_InCursor
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Esegue Esplosione
      this.w_cOutCursor = this.oParentObject.w_CURSTREE
      this.w_cInpCursor = this.w_InCursor
      this.w_cRifTable = "RIS_ORSE"
      this.w_cRifKey = "RLCODICE,RL__TIPO"
      this.w_cExpTable = "STR_CONF"
      this.w_cExpKey = "SCCODPAD,SCTIPPAD"
      this.w_cRepKey = "SCCODFIG,SCTIPFIG"
      this.w_cExpField = ""
      this.w_cOtherField = ""
      cp_ExpDB(this.w_cOutCursor,this.w_cInpCursor,this.w_cRifTable,this.w_cRifKey,this.w_cExpTable,this.w_cExpKey, this.w_cRepKey, this.w_cExpField, this.w_cOtherField)
      * --- Chiude Cursore
      use in select(this.w_cInpCursor)
      * --- Riempio la Treeview
      this.w_AlbeDist.cCursor = this.oParentObject.w_CURSTREE
      this.Padre.NotifyEvent("Reload")     
      this.w_AlbeDist.ExpandAll(.T.)     
      this.w_AlbeDist.SetFocus()     
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='STR_CONF'
    this.cWorkTables[3]='RIS_ORSE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
