* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_mcl                                                        *
*              Cicli di lavorazione                                            *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-27                                                      *
* Last revis.: 2017-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_mcl"))

* --- Class definition
define class tgsci_mcl as StdTrsForm
  Top    = -2
  Left   = 2

  * --- Standard Properties
  Width  = 924
  Height = 553+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-10-18"
  HelpContextID=171309719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CIC_MAST_IDX = 0
  CIC_DETT_IDX = 0
  RIS_DETT_IDX = 0
  DISMBASE_IDX = 0
  KEY_ARTI_IDX = 0
  COD_FASI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  cFile = "CIC_MAST"
  cFileDetail = "CIC_DETT"
  cKeySelect = "CLSERIAL"
  cKeyWhere  = "CLSERIAL=this.w_CLSERIAL"
  cKeyDetail  = "CLSERIAL=this.w_CLSERIAL"
  cKeyWhereODBC = '"CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';

  cKeyDetailWhereODBC = '"CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CIC_DETT.CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CIC_DETT.CPROWORD '
  cPrg = "gsci_mcl"
  cComment = "Cicli di lavorazione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- gsci_mcl
  
  w_CODDIS=space(20)    && Ultima distinta selezionata
  l_CURREC = 0                && Riga selezionata
  w_COMPON='TVCompon'  && Cursore dei componenti della Tree-View
  w_COUTPUT='TVOutput'  && Cursore dei componenti della Tree-View
  * --- Fine Area Manuale

  * --- Local Variables
  w_CLSERIAL = space(10)
  w_DATRIF = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CFUNC = space(10)
  w_CLCODART = space(20)
  w_CLCODDIS = space(20)
  w_DESDIS = space(40)
  w_DBCODART = space(20)
  w_CLCODCIC = space(20)
  w_CLDESCIC = space(40)
  w_CLINDCIC = space(2)
  o_CLINDCIC = space(2)
  w_CHECKFORM = .F.
  w_CPROWORD = 0
  w_CL__FASE = space(5)
  w_CLFASDES = space(40)
  w_CLINDFAS = space(2)
  w_CLINIVAL = ctod('  /  /  ')
  w_CLFINVAL = ctod('  /  /  ')
  w_CLCOUPOI = space(1)
  o_CLCOUPOI = space(1)
  w_CLFASOUT = space(1)
  w_CLCPRIFE = 0
  w_CLARTFAS = space(20)
  w_CLCODFAS = space(66)
  w_DESCFAS = space(40)
  w_CLDESAGG = space(0)
  w_CLFASCOS = space(1)
  w_CADTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CLPRCOST = space(1)
  w_CLPRIFAS = space(1)
  w_CLULTFAS = space(1)
  w_CLFASCLA = space(1)
  w_CLLTFASE = 0
  w_DESART = space(40)
  w_CLFLCMOD = space(1)
  w_CLMAGWIP = space(5)
  w_READPAR = space(10)
  w_PPMGARFS = space(1)
  w_PPPREFAS = space(5)
  w_PPFASSEP = space(1)
  w_PPCLAART = space(5)
  w_CLMGARFS = space(1)
  w_CLPREFAS = space(5)
  w_CLFASSEP = space(1)
  w_PPCLAFAS = space(5)
  w_CLPRECOD = space(5)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CLSERIAL = this.W_CLSERIAL

  * --- Children pointers
  GSCI_MLR = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsci_mcl
  w_CHKDTOBS=0
  * --- Ridefinisce la funzione per avere il controllo delle procedure di CP
  func ah_HasCPEvents(i_cOp)
     if Upper(This.cFunction)<>"QUERY" And upper(i_cop)='ECPF6'
       * disabilita l'F6 quando � selezionata la Tree-view
       return (type("this.ActiveControl")="O" and this.ActiveControl.class<>"Oletree")
       return(.t.)
     else
       return(.t.)
     endif
  endfunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CIC_MAST','gsci_mcl')
    stdPageFrame::Init()
    *set procedure to GSCI_MLR additive
    with this
      .Pages(1).addobject("oPag","tgsci_mclPag1","gsci_mcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ciclo")
      .Pages(1).HelpContextID = 26777638
      .Pages(2).addobject("oPag","tgsci_mclPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note")
      .Pages(2).HelpContextID = 178433750
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCI_MLR
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='RIS_DETT'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='COD_FASI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='PAR_PROD'
    this.cWorkTables[7]='CIC_MAST'
    this.cWorkTables[8]='CIC_DETT'
    * --- Area Manuale = Open Work Table
    * --- gsci_mcl
    
    *Colora la riga selezionata
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
         "IIF(recno()=this.parent.parent.parent.parent.l_CURREC, rgb(224,254,184), rgb(255,255,255))"
    
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CIC_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CIC_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MLR = CREATEOBJECT('stdDynamicChild',this,'GSCI_MLR',this.oPgFrm.Page1.oPag.oLinkPC_2_9)
    this.GSCI_MLR.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MLR)
      this.GSCI_MLR.DestroyChildrenChain()
      this.GSCI_MLR=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_9')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MLR.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MLR.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MLR.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCI_MLR.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CLSERIAL,"LRSERIAL";
             ,.w_CPROWNUM,"LRROWNUM";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CLSERIAL = NVL(CLSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CIC_MAST where CLSERIAL=KeySet.CLSERIAL
    *
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2],this.bLoadRecFilter,this.CIC_MAST_IDX,"gsci_mcl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CIC_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CIC_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CIC_DETT.","CIC_MAST.")
      i_cTable = i_cTable+' CIC_MAST '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DATRIF = i_DATSYS
        .w_OBTEST = i_DATSYS
        .w_DESDIS = space(40)
        .w_DBCODART = space(20)
        .w_CHECKFORM = .f.
        .w_CADTOBSO = ctod("  /  /  ")
        .w_DESART = space(40)
        .w_READPAR = 'PP'
        .w_PPMGARFS = space(1)
        .w_PPPREFAS = space(5)
        .w_PPFASSEP = space(1)
        .w_PPCLAART = space(5)
        .w_PPCLAFAS = space(5)
        .w_CLSERIAL = NVL(CLSERIAL,space(10))
        .op_CLSERIAL = .w_CLSERIAL
        .w_CFUNC = this.cFunction
        .w_CLCODART = NVL(CLCODART,space(20))
          if link_1_5_joined
            this.w_CLCODART = NVL(ARCODART105,NVL(this.w_CLCODART,space(20)))
            this.w_DESART = NVL(ARDESART105,space(40))
          else
          .link_1_5('Load')
          endif
        .w_CLCODDIS = NVL(CLCODDIS,space(20))
          if link_1_6_joined
            this.w_CLCODDIS = NVL(DBCODICE106,NVL(this.w_CLCODDIS,space(20)))
            this.w_DESDIS = NVL(DBDESCRI106,space(40))
          else
          .link_1_6('Load')
          endif
        .w_CLCODCIC = NVL(CLCODCIC,space(20))
        .w_CLDESCIC = NVL(CLDESCIC,space(40))
        .w_CLINDCIC = NVL(CLINDCIC,space(2))
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_CLDESAGG = NVL(CLDESAGG,space(0))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CLPRCOST = NVL(CLPRCOST,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CLFLCMOD = NVL(CLFLCMOD,space(1))
          .link_1_34('Load')
        .w_CLMGARFS = NVL(CLMGARFS,space(1))
        .w_CLPREFAS = NVL(CLPREFAS,space(5))
        .w_CLFASSEP = NVL(CLFASSEP,space(1))
        .w_CLPRECOD = NVL(CLPRECOD,space(5))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CIC_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CIC_DETT where CLSERIAL=KeySet.CLSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CIC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CIC_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CIC_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CIC_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CIC_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
        select * from (i_cTable) CIC_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCFAS = space(40)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CL__FASE = NVL(CL__FASE,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_CLFASDES = NVL(CLFASDES,space(40))
          .w_CLINDFAS = NVL(CLINDFAS,space(2))
          .w_CLINIVAL = NVL(cp_ToDate(CLINIVAL),ctod("  /  /  "))
          .w_CLFINVAL = NVL(cp_ToDate(CLFINVAL),ctod("  /  /  "))
          .w_CLCOUPOI = NVL(CLCOUPOI,space(1))
          .w_CLFASOUT = NVL(CLFASOUT,space(1))
          .w_CLCPRIFE = NVL(CLCPRIFE,0)
          .w_CLARTFAS = NVL(CLARTFAS,space(20))
          .link_2_12('Load')
          .w_CLCODFAS = NVL(CLCODFAS,space(66))
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
          .w_CLFASCOS = NVL(CLFASCOS,space(1))
          .w_CLPRIFAS = NVL(CLPRIFAS,space(1))
          .w_CLULTFAS = NVL(CLULTFAS,space(1))
          .w_CLFASCLA = NVL(CLFASCLA,space(1))
          .w_CLLTFASE = NVL(CLLTFASE,0)
          .w_CLMAGWIP = NVL(CLMAGWIP,space(5))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CFUNC = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CLSERIAL=space(10)
      .w_DATRIF=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CFUNC=space(10)
      .w_CLCODART=space(20)
      .w_CLCODDIS=space(20)
      .w_DESDIS=space(40)
      .w_DBCODART=space(20)
      .w_CLCODCIC=space(20)
      .w_CLDESCIC=space(40)
      .w_CLINDCIC=space(2)
      .w_CHECKFORM=.f.
      .w_CPROWORD=10
      .w_CL__FASE=space(5)
      .w_CLFASDES=space(40)
      .w_CLINDFAS=space(2)
      .w_CLINIVAL=ctod("  /  /  ")
      .w_CLFINVAL=ctod("  /  /  ")
      .w_CLCOUPOI=space(1)
      .w_CLFASOUT=space(1)
      .w_CLCPRIFE=0
      .w_CLARTFAS=space(20)
      .w_CLCODFAS=space(66)
      .w_DESCFAS=space(40)
      .w_CLDESAGG=space(0)
      .w_CLFASCOS=space(1)
      .w_CADTOBSO=ctod("  /  /  ")
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_CLPRCOST=space(1)
      .w_CLPRIFAS=space(1)
      .w_CLULTFAS=space(1)
      .w_CLFASCLA=space(1)
      .w_CLLTFASE=0
      .w_DESART=space(40)
      .w_CLFLCMOD=space(1)
      .w_CLMAGWIP=space(5)
      .w_READPAR=space(10)
      .w_PPMGARFS=space(1)
      .w_PPPREFAS=space(5)
      .w_PPFASSEP=space(1)
      .w_PPCLAART=space(5)
      .w_CLMGARFS=space(1)
      .w_CLPREFAS=space(5)
      .w_CLFASSEP=space(1)
      .w_PPCLAFAS=space(5)
      .w_CLPRECOD=space(5)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_DATRIF = i_DATSYS
        .w_OBTEST = i_DATSYS
        .w_CFUNC = this.cFunction
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CLCODART))
         .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CLCODDIS))
         .link_1_6('Full')
        endif
        .DoRTCalc(7,10,.f.)
        .w_CLINDCIC = '00'
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(12,14,.f.)
        if not(empty(.w_CL__FASE))
         .link_2_2('Full')
        endif
        .DoRTCalc(15,15,.f.)
        .w_CLINDFAS = '00'
        .w_CLINIVAL = i_DATSYS
        .w_CLFINVAL = cp_CharToDate("31-12-2099")
        .w_CLCOUPOI = 'N'
        .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CLARTFAS))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .DoRTCalc(23,25,.f.)
        .w_CLFASCOS = 'S'
        .DoRTCalc(27,31,.f.)
        .w_CLPRCOST = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CLPRIFAS = 'N'
        .w_CLULTFAS = 'N'
        .w_CLFASCLA = 'N'
        .DoRTCalc(36,37,.f.)
        .w_CLFLCMOD = 'N'
        .DoRTCalc(39,39,.f.)
        .w_READPAR = 'PP'
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_READPAR))
         .link_1_34('Full')
        endif
        .DoRTCalc(41,44,.f.)
        .w_CLMGARFS = .w_PPMGARFS
        .w_CLPREFAS = IIF(.w_CLMGARFS='P', .w_PPPREFAS , .w_PPCLAART)
        .w_CLFASSEP = IIF(.w_CLMGARFS='P', .w_PPFASSEP , SPACE(1))
        .DoRTCalc(48,48,.f.)
        .w_CLPRECOD = .w_PPCLAFAS
      endif
    endwith
    cp_BlankRecExtFlds(this,'CIC_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCLSERIAL_1_1.enabled = !i_bVal
      .Page1.oPag.oCLCODART_1_5.enabled = i_bVal
      .Page1.oPag.oCLCODDIS_1_6.enabled = i_bVal
      .Page1.oPag.oCLCODCIC_1_9.enabled = i_bVal
      .Page1.oPag.oCLDESCIC_1_10.enabled = i_bVal
      .Page1.oPag.oCLINDCIC_1_11.enabled = i_bVal
      .Page1.oPag.oCLARTFAS_2_12.enabled = i_bVal
      .Page2.oPag.oCLDESAGG_4_1.enabled = i_bVal
      .Page1.oPag.oCLFASCOS_2_16.enabled = i_bVal
      .Page1.oPag.oCLPRCOST_1_28.enabled = i_bVal
      .Page1.oPag.oCLFLCMOD_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oObj_2_15.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCLCODART_1_5.enabled = .t.
        .Page1.oPag.oCLCODDIS_1_6.enabled = .t.
      endif
    endwith
    this.GSCI_MLR.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CIC_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SECIC","i_codazi,w_CLSERIAL")
      .op_codazi = .w_codazi
      .op_CLSERIAL = .w_CLSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MLR.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLSERIAL,"CLSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODART,"CLCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODDIS,"CLCODDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCODCIC,"CLCODCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDESCIC,"CLDESCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLINDCIC,"CLINDCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDESAGG,"CLDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPRCOST,"CLPRCOST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLFLCMOD,"CLFLCMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLMGARFS,"CLMGARFS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPREFAS,"CLPREFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLFASSEP,"CLFASSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPRECOD,"CLPRECOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
    i_lTable = "CIC_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CIC_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCI_SCI with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CL__FASE C(5);
      ,t_CLFASDES C(40);
      ,t_CLINDFAS C(2);
      ,t_CLINIVAL D(8);
      ,t_CLFINVAL D(8);
      ,t_CLCOUPOI N(3);
      ,t_CLFASOUT N(3);
      ,t_CLARTFAS C(20);
      ,t_CLCODFAS C(66);
      ,t_DESCFAS C(40);
      ,t_CLFASCOS N(3);
      ,t_CLPRIFAS N(3);
      ,t_CLULTFAS N(3);
      ,t_CLFASCLA N(3);
      ,t_CLLTFASE N(12,5);
      ,CPROWNUM N(10);
      ,t_CLCPRIFE N(5);
      ,t_CLMAGWIP C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mclbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.controlsource=this.cTrsName+'.t_CL__FASE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASDES_2_3.controlsource=this.cTrsName+'.t_CLFASDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDFAS_2_4.controlsource=this.cTrsName+'.t_CLINDFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLINIVAL_2_5.controlsource=this.cTrsName+'.t_CLINIVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFINVAL_2_6.controlsource=this.cTrsName+'.t_CLFINVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.controlsource=this.cTrsName+'.t_CLCOUPOI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.controlsource=this.cTrsName+'.t_CLFASOUT'
    this.oPgFRm.Page1.oPag.oCLARTFAS_2_12.controlsource=this.cTrsName+'.t_CLARTFAS'
    this.oPgFRm.Page1.oPag.oCLCODFAS_2_13.controlsource=this.cTrsName+'.t_CLCODFAS'
    this.oPgFRm.Page1.oPag.oDESCFAS_2_14.controlsource=this.cTrsName+'.t_DESCFAS'
    this.oPgFRm.Page1.oPag.oCLFASCOS_2_16.controlsource=this.cTrsName+'.t_CLFASCOS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.controlsource=this.cTrsName+'.t_CLPRIFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.controlsource=this.cTrsName+'.t_CLULTFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.controlsource=this.cTrsName+'.t_CLFASCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_21.controlsource=this.cTrsName+'.t_CLLTFASE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(128)
    this.AddVLine(429)
    this.AddVLine(462)
    this.AddVLine(542)
    this.AddVLine(621)
    this.AddVLine(725)
    this.AddVLine(757)
    this.AddVLine(793)
    this.AddVLine(830)
    this.AddVLine(868)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SECIC","i_codazi,w_CLSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CIC_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CIC_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CIC_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(CLSERIAL,CLCODART,CLCODDIS,CLCODCIC,CLDESCIC"+;
                  ",CLINDCIC,CLDESAGG,UTCC,UTCV,UTDC"+;
                  ",UTDV,CLPRCOST,CLFLCMOD,CLMGARFS,CLPREFAS"+;
                  ",CLFASSEP,CLPRECOD"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CLSERIAL)+;
                    ","+cp_ToStrODBCNull(this.w_CLCODART)+;
                    ","+cp_ToStrODBCNull(this.w_CLCODDIS)+;
                    ","+cp_ToStrODBC(this.w_CLCODCIC)+;
                    ","+cp_ToStrODBC(this.w_CLDESCIC)+;
                    ","+cp_ToStrODBC(this.w_CLINDCIC)+;
                    ","+cp_ToStrODBC(this.w_CLDESAGG)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_CLPRCOST)+;
                    ","+cp_ToStrODBC(this.w_CLFLCMOD)+;
                    ","+cp_ToStrODBC(this.w_CLMGARFS)+;
                    ","+cp_ToStrODBC(this.w_CLPREFAS)+;
                    ","+cp_ToStrODBC(this.w_CLFASSEP)+;
                    ","+cp_ToStrODBC(this.w_CLPRECOD)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CIC_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CIC_MAST')
        cp_CheckDeletedKey(i_cTable,0,'CLSERIAL',this.w_CLSERIAL)
        INSERT INTO (i_cTable);
              (CLSERIAL,CLCODART,CLCODDIS,CLCODCIC,CLDESCIC,CLINDCIC,CLDESAGG,UTCC,UTCV,UTDC,UTDV,CLPRCOST,CLFLCMOD,CLMGARFS,CLPREFAS,CLFASSEP,CLPRECOD &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CLSERIAL;
                  ,this.w_CLCODART;
                  ,this.w_CLCODDIS;
                  ,this.w_CLCODCIC;
                  ,this.w_CLDESCIC;
                  ,this.w_CLINDCIC;
                  ,this.w_CLDESAGG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_CLPRCOST;
                  ,this.w_CLFLCMOD;
                  ,this.w_CLMGARFS;
                  ,this.w_CLPREFAS;
                  ,this.w_CLFASSEP;
                  ,this.w_CLPRECOD;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- gsci_mcl
    
    * Imposta Ultima fase
    GSCI_BCL(this,"UltFase")
    
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CIC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CIC_DETT_IDX,2])
      *
      * insert into CIC_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CLSERIAL,CPROWORD,CL__FASE,CLFASDES,CLINDFAS"+;
                  ",CLINIVAL,CLFINVAL,CLCOUPOI,CLFASOUT,CLCPRIFE"+;
                  ",CLARTFAS,CLCODFAS,CLFASCOS,CLPRIFAS,CLULTFAS"+;
                  ",CLFASCLA,CLLTFASE,CLMAGWIP,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CLSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_CL__FASE)+","+cp_ToStrODBC(this.w_CLFASDES)+","+cp_ToStrODBC(this.w_CLINDFAS)+;
             ","+cp_ToStrODBC(this.w_CLINIVAL)+","+cp_ToStrODBC(this.w_CLFINVAL)+","+cp_ToStrODBC(this.w_CLCOUPOI)+","+cp_ToStrODBC(this.w_CLFASOUT)+","+cp_ToStrODBC(this.w_CLCPRIFE)+;
             ","+cp_ToStrODBCNull(this.w_CLARTFAS)+","+cp_ToStrODBC(this.w_CLCODFAS)+","+cp_ToStrODBC(this.w_CLFASCOS)+","+cp_ToStrODBC(this.w_CLPRIFAS)+","+cp_ToStrODBC(this.w_CLULTFAS)+;
             ","+cp_ToStrODBC(this.w_CLFASCLA)+","+cp_ToStrODBC(this.w_CLLTFASE)+","+cp_ToStrODBC(this.w_CLMAGWIP)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CLSERIAL',this.w_CLSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CLSERIAL,this.w_CPROWORD,this.w_CL__FASE,this.w_CLFASDES,this.w_CLINDFAS"+;
                ",this.w_CLINIVAL,this.w_CLFINVAL,this.w_CLCOUPOI,this.w_CLFASOUT,this.w_CLCPRIFE"+;
                ",this.w_CLARTFAS,this.w_CLCODFAS,this.w_CLFASCOS,this.w_CLPRIFAS,this.w_CLULTFAS"+;
                ",this.w_CLFASCLA,this.w_CLLTFASE,this.w_CLMAGWIP,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CIC_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CIC_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CLCODART="+cp_ToStrODBCNull(this.w_CLCODART)+;
             ",CLCODDIS="+cp_ToStrODBCNull(this.w_CLCODDIS)+;
             ",CLCODCIC="+cp_ToStrODBC(this.w_CLCODCIC)+;
             ",CLDESCIC="+cp_ToStrODBC(this.w_CLDESCIC)+;
             ",CLINDCIC="+cp_ToStrODBC(this.w_CLINDCIC)+;
             ",CLDESAGG="+cp_ToStrODBC(this.w_CLDESAGG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CLPRCOST="+cp_ToStrODBC(this.w_CLPRCOST)+;
             ",CLFLCMOD="+cp_ToStrODBC(this.w_CLFLCMOD)+;
             ",CLMGARFS="+cp_ToStrODBC(this.w_CLMGARFS)+;
             ",CLPREFAS="+cp_ToStrODBC(this.w_CLPREFAS)+;
             ",CLFASSEP="+cp_ToStrODBC(this.w_CLFASSEP)+;
             ",CLPRECOD="+cp_ToStrODBC(this.w_CLPRECOD)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CIC_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
          UPDATE (i_cTable) SET;
              CLCODART=this.w_CLCODART;
             ,CLCODDIS=this.w_CLCODDIS;
             ,CLCODCIC=this.w_CLCODCIC;
             ,CLDESCIC=this.w_CLDESCIC;
             ,CLINDCIC=this.w_CLINDCIC;
             ,CLDESAGG=this.w_CLDESAGG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CLPRCOST=this.w_CLPRCOST;
             ,CLFLCMOD=this.w_CLFLCMOD;
             ,CLMGARFS=this.w_CLMGARFS;
             ,CLPREFAS=this.w_CLPREFAS;
             ,CLFASSEP=this.w_CLFASSEP;
             ,CLPRECOD=this.w_CLPRECOD;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and !Empty(t_CLFASDES)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CIC_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CIC_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSCI_MLR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CLSERIAL,"LRSERIAL";
                     ,this.w_CPROWNUM,"LRROWNUM";
                     )
              this.GSCI_MLR.mDelete()
              *
              * delete from CIC_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CIC_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CL__FASE="+cp_ToStrODBCNull(this.w_CL__FASE)+;
                     ",CLFASDES="+cp_ToStrODBC(this.w_CLFASDES)+;
                     ",CLINDFAS="+cp_ToStrODBC(this.w_CLINDFAS)+;
                     ",CLINIVAL="+cp_ToStrODBC(this.w_CLINIVAL)+;
                     ",CLFINVAL="+cp_ToStrODBC(this.w_CLFINVAL)+;
                     ",CLCOUPOI="+cp_ToStrODBC(this.w_CLCOUPOI)+;
                     ",CLFASOUT="+cp_ToStrODBC(this.w_CLFASOUT)+;
                     ",CLCPRIFE="+cp_ToStrODBC(this.w_CLCPRIFE)+;
                     ",CLARTFAS="+cp_ToStrODBCNull(this.w_CLARTFAS)+;
                     ",CLCODFAS="+cp_ToStrODBC(this.w_CLCODFAS)+;
                     ",CLFASCOS="+cp_ToStrODBC(this.w_CLFASCOS)+;
                     ",CLPRIFAS="+cp_ToStrODBC(this.w_CLPRIFAS)+;
                     ",CLULTFAS="+cp_ToStrODBC(this.w_CLULTFAS)+;
                     ",CLFASCLA="+cp_ToStrODBC(this.w_CLFASCLA)+;
                     ",CLLTFASE="+cp_ToStrODBC(this.w_CLLTFASE)+;
                     ",CLMAGWIP="+cp_ToStrODBC(this.w_CLMAGWIP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CL__FASE=this.w_CL__FASE;
                     ,CLFASDES=this.w_CLFASDES;
                     ,CLINDFAS=this.w_CLINDFAS;
                     ,CLINIVAL=this.w_CLINIVAL;
                     ,CLFINVAL=this.w_CLFINVAL;
                     ,CLCOUPOI=this.w_CLCOUPOI;
                     ,CLFASOUT=this.w_CLFASOUT;
                     ,CLCPRIFE=this.w_CLCPRIFE;
                     ,CLARTFAS=this.w_CLARTFAS;
                     ,CLCODFAS=this.w_CLCODFAS;
                     ,CLFASCOS=this.w_CLFASCOS;
                     ,CLPRIFAS=this.w_CLPRIFAS;
                     ,CLULTFAS=this.w_CLULTFAS;
                     ,CLFASCLA=this.w_CLFASCLA;
                     ,CLLTFASE=this.w_CLLTFASE;
                     ,CLMAGWIP=this.w_CLMAGWIP;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 and !Empty(t_CLFASDES))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSCI_MLR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_CLSERIAL,"LRSERIAL";
             ,this.w_CPROWNUM,"LRROWNUM";
             )
        this.GSCI_MLR.mReplace()
        this.GSCI_MLR.bSaveContext=.f.
      endscan
     this.GSCI_MLR.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsci_mcl
    
    * Imposta Ultima fase
    GSCI_BCL(this,"UltFase")
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and !Empty(t_CLFASDES)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCI_MLR : Deleting
        this.GSCI_MLR.bSaveContext=.f.
        this.GSCI_MLR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CLSERIAL,"LRSERIAL";
               ,this.w_CPROWNUM,"LRROWNUM";
               )
        this.GSCI_MLR.bSaveContext=.t.
        this.GSCI_MLR.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CIC_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CIC_DETT_IDX,2])
        *
        * delete CIC_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
        *
        * delete CIC_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and !Empty(t_CLFASDES)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_CFUNC = this.cFunction
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(5,19,.t.)
        if .o_CLCOUPOI<>.w_CLCOUPOI
          .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
        endif
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .DoRTCalc(21,25,.t.)
        if .o_CLCOUPOI<>.w_CLCOUPOI
          .w_CLFASCOS = 'S'
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(27,39,.t.)
          .link_1_34('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SECIC","i_codazi,w_CLSERIAL")
          .op_CLSERIAL = .w_CLSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(41,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CLCPRIFE with this.w_CLCPRIFE
      replace t_CLMAGWIP with this.w_CLMAGWIP
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLCODART_1_5.enabled = this.oPgFrm.Page1.oPag.oCLCODART_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCLCODDIS_1_6.enabled = this.oPgFrm.Page1.oPag.oCLCODDIS_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCLCODCIC_1_9.enabled = this.oPgFrm.Page1.oPag.oCLCODCIC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCLFLCMOD_1_33.enabled = this.oPgFrm.Page1.oPag.oCLFLCMOD_1_33.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASOUT_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLFASOUT_2_8.mCond()
    this.oPgFrm.Page1.oPag.oCLARTFAS_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCLARTFAS_2_12.mCond()
    this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLFASSEP_1_41.visible=!this.oPgFrm.Page1.oPag.oCLFASSEP_1_41.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsci_mcl
    if !bTrsErr and alltrim(lower(cEvent))=="w_clcoddis changed" and this.cFunction $ "Load Edit"
        if Not (this.w_CADTOBSO>i_DATSYS or empty(this.w_CADTOBSO))
          ah_ErrorMsg("Attenzione%0Il codice di ricerca associato alla distinta � obsoleto", 48)
        endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLCODART
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CLCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CLCODART))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCLCODART_1_5'),i_cWhere,'',"Articolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLCODART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODART = space(20)
      endif
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ARCODART as ARCODART105"+ ",link_1_5.ARDESART as ARDESART105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on CIC_MAST.CLCODART=link_1_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and CIC_MAST.CLCODART=link_1_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODDIS
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_CLCODDIS)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_CLCODDIS))
          select DBCODICE,DBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODDIS)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODDIS) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oCLCODDIS_1_6'),i_cWhere,'',"Distinta base",'GSCI_MCL.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_CLCODDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_CLCODDIS)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODDIS = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODDIS = space(20)
      endif
      this.w_DESDIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DISMBASE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.DBCODICE as DBCODICE106"+ ",link_1_6.DBDESCRI as DBDESCRI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on CIC_MAST.CLCODDIS=link_1_6.DBCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and CIC_MAST.CLCODDIS=link_1_6.DBCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CL__FASE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_FASI_IDX,3]
    i_lTable = "COD_FASI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2], .t., this.COD_FASI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CL__FASE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_ACF',True,'COD_FASI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_CL__FASE)+"%");

          i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFCODICE',trim(this.w_CL__FASE))
          select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CL__FASE)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CL__FASE) and !this.bDontReportError
            deferred_cp_zoom('COD_FASI','*','CFCODICE',cp_AbsName(oSource.parent,'oCL__FASE_2_2'),i_cWhere,'GSCI_ACF',"Codifiche fasi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',oSource.xKey(1))
            select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CL__FASE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_CL__FASE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFCODICE',this.w_CL__FASE)
            select CFCODICE,CFDESCRI,CFCOUPOI,CFOUTPUT,CFFASCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CL__FASE = NVL(_Link_.CFCODICE,space(5))
      this.w_CLFASDES = NVL(_Link_.CFDESCRI,space(40))
      this.w_CLCOUPOI = NVL(_Link_.CFCOUPOI,space(1))
      this.w_CLFASOUT = NVL(_Link_.CFOUTPUT,space(1))
      this.w_CLFASCOS = NVL(_Link_.CFFASCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CL__FASE = space(5)
      endif
      this.w_CLFASDES = space(40)
      this.w_CLCOUPOI = space(1)
      this.w_CLFASOUT = space(1)
      this.w_CLFASCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_FASI_IDX,2])+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_FASI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CL__FASE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLARTFAS
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLARTFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CLARTFAS)+"%");
                   +" and ARCODART="+cp_ToStrODBC(this.w_CLARTFAS);

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART,ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',this.w_CLARTFAS;
                     ,'ARCODART',trim(this.w_CLARTFAS))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART,ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLARTFAS)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLARTFAS) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART,ARCODART',cp_AbsName(oSource.parent,'oCLARTFAS_2_12'),i_cWhere,'GSMA_BZA',"Anagrafica articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLARTFAS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ARCODART="+cp_ToStrODBC(this.w_CLARTFAS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1);
                       ,'ARCODART',oSource.xKey(2))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLARTFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLARTFAS);
                   +" and ARCODART="+cp_ToStrODBC(this.w_CLARTFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLARTFAS;
                       ,'ARCODART',this.w_CLARTFAS)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLARTFAS = NVL(_Link_.ARCODART,space(20))
      this.w_DESCFAS = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CLARTFAS = space(20)
      endif
      this.w_DESCFAS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLARTFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPMGARFS,PPPREFAS,PPFASSEP,PPCLAART,PPCLAFAS";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_READPAR)
            select PPCODICE,PPMGARFS,PPPREFAS,PPFASSEP,PPCLAART,PPCLAFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PPCODICE,space(10))
      this.w_PPMGARFS = NVL(_Link_.PPMGARFS,space(1))
      this.w_PPPREFAS = NVL(_Link_.PPPREFAS,space(5))
      this.w_PPFASSEP = NVL(_Link_.PPFASSEP,space(1))
      this.w_PPCLAART = NVL(_Link_.PPCLAART,space(5))
      this.w_PPCLAFAS = NVL(_Link_.PPCLAFAS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_PPMGARFS = space(1)
      this.w_PPPREFAS = space(5)
      this.w_PPFASSEP = space(1)
      this.w_PPCLAART = space(5)
      this.w_PPCLAFAS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCLSERIAL_1_1.value==this.w_CLSERIAL)
      this.oPgFrm.Page1.oPag.oCLSERIAL_1_1.value=this.w_CLSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODART_1_5.value==this.w_CLCODART)
      this.oPgFrm.Page1.oPag.oCLCODART_1_5.value=this.w_CLCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODDIS_1_6.value==this.w_CLCODDIS)
      this.oPgFrm.Page1.oPag.oCLCODDIS_1_6.value=this.w_CLCODDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIS_1_7.value==this.w_DESDIS)
      this.oPgFrm.Page1.oPag.oDESDIS_1_7.value=this.w_DESDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODCIC_1_9.value==this.w_CLCODCIC)
      this.oPgFrm.Page1.oPag.oCLCODCIC_1_9.value=this.w_CLCODCIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDESCIC_1_10.value==this.w_CLDESCIC)
      this.oPgFrm.Page1.oPag.oCLDESCIC_1_10.value=this.w_CLDESCIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCLINDCIC_1_11.value==this.w_CLINDCIC)
      this.oPgFrm.Page1.oPag.oCLINDCIC_1_11.value=this.w_CLINDCIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCLARTFAS_2_12.value==this.w_CLARTFAS)
      this.oPgFrm.Page1.oPag.oCLARTFAS_2_12.value=this.w_CLARTFAS
      replace t_CLARTFAS with this.oPgFrm.Page1.oPag.oCLARTFAS_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCODFAS_2_13.value==this.w_CLCODFAS)
      this.oPgFrm.Page1.oPag.oCLCODFAS_2_13.value=this.w_CLCODFAS
      replace t_CLCODFAS with this.oPgFrm.Page1.oPag.oCLCODFAS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFAS_2_14.value==this.w_DESCFAS)
      this.oPgFrm.Page1.oPag.oDESCFAS_2_14.value=this.w_DESCFAS
      replace t_DESCFAS with this.oPgFrm.Page1.oPag.oDESCFAS_2_14.value
    endif
    if not(this.oPgFrm.Page2.oPag.oCLDESAGG_4_1.value==this.w_CLDESAGG)
      this.oPgFrm.Page2.oPag.oCLDESAGG_4_1.value=this.w_CLDESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.RadioValue()==this.w_CLFASCOS)
      this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.SetRadio()
      replace t_CLFASCOS with this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCLPRCOST_1_28.RadioValue()==this.w_CLPRCOST)
      this.oPgFrm.Page1.oPag.oCLPRCOST_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_30.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_30.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCLFLCMOD_1_33.RadioValue()==this.w_CLFLCMOD)
      this.oPgFrm.Page1.oPag.oCLFLCMOD_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLMGARFS_1_39.RadioValue()==this.w_CLMGARFS)
      this.oPgFrm.Page1.oPag.oCLMGARFS_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLPREFAS_1_40.value==this.w_CLPREFAS)
      this.oPgFrm.Page1.oPag.oCLPREFAS_1_40.value=this.w_CLPREFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCLFASSEP_1_41.value==this.w_CLFASSEP)
      this.oPgFrm.Page1.oPag.oCLFASSEP_1_41.value=this.w_CLFASSEP
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value==this.w_CL__FASE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value=this.w_CL__FASE
      replace t_CL__FASE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCL__FASE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASDES_2_3.value==this.w_CLFASDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASDES_2_3.value=this.w_CLFASDES
      replace t_CLFASDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASDES_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDFAS_2_4.value==this.w_CLINDFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDFAS_2_4.value=this.w_CLINDFAS
      replace t_CLINDFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDFAS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINIVAL_2_5.value==this.w_CLINIVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINIVAL_2_5.value=this.w_CLINIVAL
      replace t_CLINIVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINIVAL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFINVAL_2_6.value==this.w_CLFINVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFINVAL_2_6.value=this.w_CLFINVAL
      replace t_CLFINVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFINVAL_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.RadioValue()==this.w_CLCOUPOI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.SetRadio()
      replace t_CLCOUPOI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.RadioValue()==this.w_CLFASOUT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.SetRadio()
      replace t_CLFASOUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.RadioValue()==this.w_CLPRIFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.SetRadio()
      replace t_CLPRIFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.RadioValue()==this.w_CLULTFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.SetRadio()
      replace t_CLULTFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.RadioValue()==this.w_CLFASCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.SetRadio()
      replace t_CLFASCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_21.value==this.w_CLLTFASE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_21.value=this.w_CLLTFASE
      replace t_CLLTFASE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLLTFASE_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'CIC_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CLCODART))  and (.w_CFUNC<>"Edit")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLCODART_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CLCODART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLCODDIS))  and (.w_CFUNC<>"Edit")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLCODDIS_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CLCODDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLINDCIC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLINDCIC_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CLINDCIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPPREFAS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPPPREFAS_1_36.SetFocus()
            i_bnoObbl = !empty(.w_PPPREFAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPFASSEP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPPFASSEP_1_37.SetFocus()
            i_bnoObbl = !empty(.w_PPFASSEP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPCLAART))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPPCLAART_1_38.SetFocus()
            i_bnoObbl = !empty(.w_PPCLAART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLPREFAS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLPREFAS_1_40.SetFocus()
            i_bnoObbl = !empty(.w_CLPREFAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLFASSEP))  and not(.w_CLMGARFS<>'P')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLFASSEP_1_41.SetFocus()
            i_bnoObbl = !empty(.w_CLFASSEP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPCLAFAS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPPCLAFAS_1_42.SetFocus()
            i_bnoObbl = !empty(.w_PPCLAFAS)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsci_mcl
      
      .w_CHECKFORM=.T.
      GSCI_BCL(this,"ChkRighe")
      if not .w_CHECKFORM
         i_bRes=.F.
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (t_CPROWORD<>0 and !Empty(t_CLFASDES));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CLINDFAS) and (.w_CPROWORD<>0 and !Empty(.w_CLFASDES))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINDFAS_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_CLINIVAL<=.w_CLFINVAL) and (.w_CPROWORD<>0 and !Empty(.w_CLFASDES))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLINIVAL_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di inizio validit� maggiore della data di fine validit�")
        case   not(.w_CLINIVAL<=.w_CLFINVAL) and (.w_CPROWORD<>0 and !Empty(.w_CLFASDES))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFINVAL_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di inizio validit� maggiore della data di fine validit�")
      endcase
      i_bRes = i_bRes .and. .GSCI_MLR.CheckForm()
      if .w_CPROWORD<>0 and !Empty(.w_CLFASDES)
        * --- Area Manuale = Check Row
        * --- gsci_mcl
        * Colora riga
        thisform.l_currec=recno()
        * instanzio il figlio della seconda pagina immediatamente
        * viene richiamato per i calcoli
        if Upper(thisform.gsci_mlr.gsci_mmi.class)='STDDYNAMICCHILD'
            thisform.gsci_mlr.oPgfrm.Pages[2].opag.uienable(.T.)
        Endif
        
        thisform.gsci_mlr.gsci_mmi.l_currec=1
        
        if Upper(thisform.gsci_mlr.gsci_mmo.class)='STDDYNAMICCHILD'
            thisform.gsci_mlr.oPgfrm.Pages[3].opag.uienable(.T.)
        Endif
        
        thisform.gsci_mlr.gsci_mmo.l_currec=1
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLINDCIC = this.w_CLINDCIC
    this.o_CLCOUPOI = this.w_CLCOUPOI
    * --- GSCI_MLR : Depends On
    this.GSCI_MLR.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and !Empty(t_CLFASDES))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CL__FASE=space(5)
      .w_CLFASDES=space(40)
      .w_CLINDFAS=space(2)
      .w_CLINIVAL=ctod("  /  /  ")
      .w_CLFINVAL=ctod("  /  /  ")
      .w_CLCOUPOI=space(1)
      .w_CLFASOUT=space(1)
      .w_CLCPRIFE=0
      .w_CLARTFAS=space(20)
      .w_CLCODFAS=space(66)
      .w_DESCFAS=space(40)
      .w_CLFASCOS=space(1)
      .w_CLPRIFAS=space(1)
      .w_CLULTFAS=space(1)
      .w_CLFASCLA=space(1)
      .w_CLLTFASE=0
      .w_CLMAGWIP=space(5)
      .DoRTCalc(1,14,.f.)
      if not(empty(.w_CL__FASE))
        .link_2_2('Full')
      endif
      .DoRTCalc(15,15,.f.)
        .w_CLINDFAS = '00'
        .w_CLINIVAL = i_DATSYS
        .w_CLFINVAL = cp_CharToDate("31-12-2099")
        .w_CLCOUPOI = 'N'
        .w_CLFASOUT = iif(.w_CLCOUPOI='S', .w_CLFASOUT, 'N')
      .DoRTCalc(21,22,.f.)
      if not(empty(.w_CLARTFAS))
        .link_2_12('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_15.Calculate()
      .DoRTCalc(23,25,.f.)
        .w_CLFASCOS = 'S'
      .DoRTCalc(27,32,.f.)
        .w_CLPRIFAS = 'N'
        .w_CLULTFAS = 'N'
        .w_CLFASCLA = 'N'
    endwith
    this.DoRTCalc(36,49,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CL__FASE = t_CL__FASE
    this.w_CLFASDES = t_CLFASDES
    this.w_CLINDFAS = t_CLINDFAS
    this.w_CLINIVAL = t_CLINIVAL
    this.w_CLFINVAL = t_CLFINVAL
    this.w_CLCOUPOI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.RadioValue(.t.)
    this.w_CLFASOUT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.RadioValue(.t.)
    this.w_CLCPRIFE = t_CLCPRIFE
    this.w_CLARTFAS = t_CLARTFAS
    this.w_CLCODFAS = t_CLCODFAS
    this.w_DESCFAS = t_DESCFAS
    this.w_CLFASCOS = this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.RadioValue(.t.)
    this.w_CLPRIFAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.RadioValue(.t.)
    this.w_CLULTFAS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.RadioValue(.t.)
    this.w_CLFASCLA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.RadioValue(.t.)
    this.w_CLLTFASE = t_CLLTFASE
    this.w_CLMAGWIP = t_CLMAGWIP
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CL__FASE with this.w_CL__FASE
    replace t_CLFASDES with this.w_CLFASDES
    replace t_CLINDFAS with this.w_CLINDFAS
    replace t_CLINIVAL with this.w_CLINIVAL
    replace t_CLFINVAL with this.w_CLFINVAL
    replace t_CLCOUPOI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCOUPOI_2_7.ToRadio()
    replace t_CLFASOUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASOUT_2_8.ToRadio()
    replace t_CLCPRIFE with this.w_CLCPRIFE
    replace t_CLARTFAS with this.w_CLARTFAS
    replace t_CLCODFAS with this.w_CLCODFAS
    replace t_DESCFAS with this.w_DESCFAS
    replace t_CLFASCOS with this.oPgFrm.Page1.oPag.oCLFASCOS_2_16.ToRadio()
    replace t_CLPRIFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLPRIFAS_2_17.ToRadio()
    replace t_CLULTFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLULTFAS_2_18.ToRadio()
    replace t_CLFASCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLFASCLA_2_19.ToRadio()
    replace t_CLLTFASE with this.w_CLLTFASE
    replace t_CLMAGWIP with this.w_CLMAGWIP
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mclPag1 as StdContainer
  Width  = 920
  height = 553
  stdWidth  = 920
  stdheight = 553
  resizeXpos=397
  resizeYpos=160
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLSERIAL_1_1 as StdField with uid="SAHBRAOJDO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CLSERIAL", cQueryName = "CLSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Chiave primaria",;
    HelpContextID = 123700366,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=76, Left=675, Top=16, InputMask=replicate('X',10)

  add object oCLCODART_1_5 as StdField with uid="JRVHZWEHJM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CLCODART", cQueryName = "CLCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta base",;
    HelpContextID = 3572870,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=138, Left=98, Top=16, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CLCODART"

  func oCLCODART_1_5.mCond()
    with this.Parent.oContained
      return (.w_CFUNC<>"Edit")
    endwith
  endfunc

  func oCLCODART_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODART_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCODART_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCLCODART_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articolo",'',this.parent.oContained
  endproc

  add object oCLCODDIS_1_6 as StdField with uid="ISMGUQLMOZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CLCODDIS", cQueryName = "CLCODDIS",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta base",;
    HelpContextID = 46758777,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=138, Left=98, Top=42, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_CLCODDIS"

  func oCLCODDIS_1_6.mCond()
    with this.Parent.oContained
      return (.w_CFUNC<>"Edit")
    endwith
  endfunc

  func oCLCODDIS_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODDIS_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLCODDIS_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oCLCODDIS_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Distinta base",'GSCI_MCL.DISMBASE_VZM',this.parent.oContained
  endproc

  add object oDESDIS_1_7 as StdField with uid="BAGMBHZXWL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 233868234,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=238, Top=42, InputMask=replicate('X',40)

  add object oCLCODCIC_1_9 as StdField with uid="WCRXDEHTHC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CLCODCIC", cQueryName = "CLCODCIC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo",;
    HelpContextID = 29981545,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=138, Left=98, Top=67, InputMask=replicate('X',20)

  func oCLCODCIC_1_9.mCond()
    with this.Parent.oContained
      return (.w_CFUNC<>"Edit")
    endwith
  endfunc

  add object oCLDESCIC_1_10 as StdField with uid="POSWHZHJZC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CLDESCIC", cQueryName = "CLDESCIC",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione breve ciclo",;
    HelpContextID = 45058921,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=238, Top=67, InputMask=replicate('X',40)

  add object oCLINDCIC_1_11 as StdField with uid="GGLKNXJVQY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLINDCIC", cQueryName = "CLINDCIC",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Indice di preferenza ciclo ( 00= preferenziale )",;
    HelpContextID = 29940585,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=675, Top=42, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)


  add object oObj_1_14 as cp_runprogram with uid="NNRNUDIOAB",left=270, top=565, width=156,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("SOTTO")',;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , ToolTipText = "Controlli univocita";
    , HelpContextID = 187563546


  add object oObj_1_15 as cp_runprogram with uid="FRHCYSVVLC",left=-2, top=590, width=400,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("FUORI")',;
    cEvent = "w_CLCODDIS Changed,w_CLGRUPPO Changed,w_CLCODCIC Changed",;
    nPag=1;
    , ToolTipText = "Controlli univocita";
    , HelpContextID = 187563546


  add object oObj_1_17 as cp_runprogram with uid="MVXIJNNNCI",left=161, top=565, width=105,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("Refresh")',;
    cEvent = "Load,Blank",;
    nPag=1;
    , HelpContextID = 187563546


  add object oObj_1_19 as cp_runprogram with uid="UGOIVYHARY",left=430, top=565, width=105,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("DONE")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 187563546


  add object oObj_1_20 as cp_runprogram with uid="TFHNFSZXNT",left=539, top=565, width=105,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("ENDEDIT")',;
    cEvent = "Record Inserted,Record Updated,Edit Aborted",;
    nPag=1;
    , HelpContextID = 187563546


  add object oObj_1_21 as cp_runprogram with uid="JKIZYADCUM",left=648, top=565, width=105,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("STARTEDIT")',;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 187563546


  add object oObj_1_22 as cp_runprogram with uid="HVHUPQSRAR",left=404, top=590, width=171,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCI_BGC('C' , .t., w_CLSERIAL)",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 187563546

  add object oCLPRCOST_1_28 as StdCheck with uid="QPIDCTUZGB",rtseq=32,rtrep=.f.,left=730, top=42, caption="Costificazione preventiva",;
    ToolTipText = "Se attiva: il ciclo di lavorazione viene considerato dalla costificazione preventiva",;
    HelpContextID = 37926022,;
    cFormVar="w_CLPRCOST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLPRCOST_1_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLPRCOST,&i_cF..t_CLPRCOST),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLPRCOST_1_28.GetRadio()
    this.Parent.oContained.w_CLPRCOST = this.RadioValue()
    return .t.
  endfunc

  func oCLPRCOST_1_28.ToRadio()
    this.Parent.oContained.w_CLPRCOST=trim(this.Parent.oContained.w_CLPRCOST)
    return(;
      iif(this.Parent.oContained.w_CLPRCOST=='S',1,;
      0))
  endfunc

  func oCLPRCOST_1_28.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=97, width=903,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=12,Field1="CPROWORD",Label1="Fase",Field2="CL__FASE",Label2="Codice",Field3="CLFASDES",Label3="Descrizione",Field4="CLINDFAS",Label4="Pref.",Field5="CLINIVAL",Label5="Inizio val.",Field6="CLFINVAL",Label6="Fine val.",Field7="CLLTFASE",Label7="L.T. fase",Field8="CLCOUPOI",Label8="C.P.",Field9="CLFASOUT",Label9="Out",Field10="CLPRIFAS",Label10="1� fase",Field11="CLULTFAS",Label11="Ult.fase",Field12="CLFASCLA",Label12="C/L",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49470342

  add object oDESART_1_30 as StdField with uid="ZIWGGMKVDX",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207850442,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=238, Top=16, InputMask=replicate('X',40)

  add object oCLFLCMOD_1_33 as StdCheck with uid="COEFAJURSU",rtseq=38,rtrep=.f.,left=585, top=67, caption="Abilita modifica ciclo di lavoro",;
    ToolTipText = "Se attivo: consente di modificare il ciclo di lavoro sull'ODL",;
    HelpContextID = 196520810,;
    cFormVar="w_CLFLCMOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLFLCMOD_1_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFLCMOD,&i_cF..t_CLFLCMOD),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFLCMOD_1_33.GetRadio()
    this.Parent.oContained.w_CLFLCMOD = this.RadioValue()
    return .t.
  endfunc

  func oCLFLCMOD_1_33.ToRadio()
    this.Parent.oContained.w_CLFLCMOD=trim(this.Parent.oContained.w_CLFLCMOD)
    return(;
      iif(this.Parent.oContained.w_CLFLCMOD=='S',1,;
      0))
  endfunc

  func oCLFLCMOD_1_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFLCMOD_1_33.mCond()
    with this.Parent.oContained
      return (Inlist(.cFunction , 'Load', 'Edit'))
    endwith
  endfunc


  add object oCLMGARFS_1_39 as StdCombo with uid="IOQTKUCMOD",rtseq=45,rtrep=.f.,left=624,top=255,width=146,height=22, enabled=.f.;
    , ToolTipText = "Modalit� di generazione dell'articolo di fase";
    , HelpContextID = 258860167;
    , cFormVar="w_CLMGARFS",RowSource=""+"Prefisso Articolo,"+"Classe codifica articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLMGARFS_1_39.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLMGARFS,&i_cF..t_CLMGARFS),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oCLMGARFS_1_39.GetRadio()
    this.Parent.oContained.w_CLMGARFS = this.RadioValue()
    return .t.
  endfunc

  func oCLMGARFS_1_39.ToRadio()
    this.Parent.oContained.w_CLMGARFS=trim(this.Parent.oContained.w_CLMGARFS)
    return(;
      iif(this.Parent.oContained.w_CLMGARFS=='P',1,;
      iif(this.Parent.oContained.w_CLMGARFS=='C',2,;
      0)))
  endfunc

  func oCLMGARFS_1_39.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLPREFAS_1_40 as StdField with uid="HJKRIOHCBT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CLPREFAS", cQueryName = "CLPREFAS",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso/codifica articolo fase",;
    HelpContextID = 186823815,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=775, Top=255, InputMask=replicate('X',5)

  add object oCLFASSEP_1_41 as StdField with uid="WQJEWGROHR",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CLFASSEP", cQueryName = "CLFASSEP",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Separatore articolo di fase",;
    HelpContextID = 223630474,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=841, Top=255, InputMask=replicate('X',1)

  func oCLFASSEP_1_41.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLMGARFS<>'P')
    endwith
    endif
  endfunc

  add object oStr_1_12 as StdString with uid="WXZWFZUNRW",Visible=.t., Left=2, Top=43,;
    Alignment=1, Width=93, Height=15,;
    Caption="Distinta base:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="TANBSVXRXP",Visible=.t., Left=560, Top=43,;
    Alignment=1, Width=114, Height=18,;
    Caption="Indice pref. ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DGPKEHBAYS",Visible=.t., Left=22, Top=69,;
    Alignment=1, Width=73, Height=15,;
    Caption="Codice ciclo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="RXNOIJRORK",Visible=.t., Left=2, Top=19,;
    Alignment=1, Width=93, Height=18,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="LLJMXEXNFZ",Visible=.t., Left=581, Top=19,;
    Alignment=1, Width=93, Height=18,;
    Caption="Codice seriale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mlr",lower(this.oContained.GSCI_MLR.class))=0
        this.oContained.GSCI_MLR.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=116,;
    width=898+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=117,width=897+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COD_FASI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCLARTFAS_2_12.Refresh()
      this.Parent.oCLCODFAS_2_13.Refresh()
      this.Parent.oDESCFAS_2_14.Refresh()
      this.Parent.oCLFASCOS_2_16.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COD_FASI'
        oDropInto=this.oBodyCol.oRow.oCL__FASE_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_9 as stdDynamicChildContainer with uid="HUUWHVJVIW",bOnScreen=.t.,width=913,height=245,;
   left=4, top=305;


  add object oCLARTFAS_2_12 as StdTrsField with uid="OZMXGUABIE",rtseq=22,rtrep=.t.,;
    cFormVar="w_CLARTFAS",value=space(20),;
    HelpContextID = 171156615,;
    cTotal="", bFixedPos=.t., cQueryName = "CLARTFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=107, Top=255, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CLARTFAS", oKey_2_1="ARCODART", oKey_2_2="this.w_CLARTFAS"

  func oCLARTFAS_2_12.mCond()
    with this.Parent.oContained
      return (1=2)
    endwith
  endfunc

  func oCLARTFAS_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
      if .not. empty(.w_CLARTFAS)
        bRes2=.link_2_12('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCLARTFAS_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCLARTFAS_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ART_ICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ARCODART="+cp_ToStrODBC(this.Parent.oContained.w_CLARTFAS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ARCODART="+cp_ToStr(this.Parent.oContained.w_CLARTFAS)
    endif
    do cp_zoom with 'ART_ICOL','*','ARCODART,ARCODART',cp_AbsName(this.parent,'oCLARTFAS_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Anagrafica articoli",'',this.parent.oContained
  endproc
  proc oCLARTFAS_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ARCODART=w_CLARTFAS
     i_obj.w_ARCODART=this.parent.oContained.w_CLARTFAS
    i_obj.ecpSave()
  endproc

  add object oCLCODFAS_2_13 as StdTrsField with uid="SZJFJZKVDA",rtseq=23,rtrep=.t.,;
    cFormVar="w_CLCODFAS",value=space(66),enabled=.f.,;
    ToolTipText = "Codifica fase di output",;
    HelpContextID = 188122247,;
    cTotal="", bFixedPos=.t., cQueryName = "CLCODFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=475, Left=107, Top=279, InputMask=replicate('X',66)

  add object oDESCFAS_2_14 as StdTrsField with uid="YEBTRMLPBX",rtseq=24,rtrep=.t.,;
    cFormVar="w_DESCFAS",value=space(40),enabled=.f.,;
    HelpContextID = 2198474,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=278, Top=255, InputMask=replicate('X',40)

  add object oObj_2_15 as cp_runprogram with uid="WYRNNPRFFC",width=171,height=20,;
   left=584, top=589,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCI_BCL("COD_FASI")',;
    cEvent = "w_CL__FASE Changed",;
    nPag=2;
    , HelpContextID = 187563546

  add object oCLFASCOS_2_16 as StdTrsCheck with uid="CHDVSWYNRC",rtrep=.t.,;
    cFormVar="w_CLFASCOS",  caption="Costificazione consuntiva",;
    ToolTipText = "Se attivo: considera per costificazione",;
    HelpContextID = 44804985,;
    Left=655, Top=280,;
    cTotal="", cQueryName = "CLFASCOS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oCLFASCOS_2_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASCOS,&i_cF..t_CLFASCOS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASCOS_2_16.GetRadio()
    this.Parent.oContained.w_CLFASCOS = this.RadioValue()
    return .t.
  endfunc

  func oCLFASCOS_2_16.ToRadio()
    this.Parent.oContained.w_CLFASCOS=trim(this.Parent.oContained.w_CLFASCOS)
    return(;
      iif(this.Parent.oContained.w_CLFASCOS=='S',1,;
      0))
  endfunc

  func oCLFASCOS_2_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFASCOS_2_16.mCond()
    with this.Parent.oContained
      return (.w_CLCOUPOI<>'S')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="EVJHUVQJYJ",Visible=.t., Left=17, Top=281,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codifica fase:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="JFJNGZTFFW",Visible=.t., Left=14, Top=259,;
    Alignment=1, Width=91, Height=18,;
    Caption="Articolo di fase:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsci_mclPag2 as StdContainer
    Width  = 920
    height = 553
    stdWidth  = 920
    stdheight = 553
  resizeXpos=756
  resizeYpos=404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLDESAGG_4_1 as StdMemo with uid="LUAAVCOLEP",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CLDESAGG", cQueryName = "CLDESAGG",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 256930963,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=466, Width=802, Left=4, Top=6
enddefine

* --- Defining Body row
define class tgsci_mclBodyRow as CPBodyRowCnt
  Width=888
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="XJXEYAFFJQ",rtseq=13,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17141910,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["@Z 999"], cGetPict=["999"], backstyle=0

  add object oCL__FASE_2_2 as StdTrsField with uid="RDHPGADSSS",rtseq=14,rtrep=.t.,;
    cFormVar="w_CL__FASE",value=space(5),;
    ToolTipText = "Codifica fase",;
    HelpContextID = 312469,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=49, Top=0, cSayPict=[repl("X",5)], cGetPict=[repl("X",5)], InputMask=replicate('X',5), bHasZoom = .t. , backstyle=0, cLinkFile="COD_FASI", cZoomOnZoom="GSCI_ACF", oKey_1_1="CFCODICE", oKey_1_2="this.w_CL__FASE"

  func oCL__FASE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCL__FASE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCL__FASE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_FASI','*','CFCODICE',cp_AbsName(this.parent,'oCL__FASE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_ACF',"Codifiche fasi",'',this.parent.oContained
  endproc
  proc oCL__FASE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCI_ACF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CFCODICE=this.parent.oContained.w_CL__FASE
    i_obj.ecpSave()
  endproc

  add object oCLFASDES_2_3 as StdTrsField with uid="WIQBKABTJY",rtseq=15,rtrep=.t.,;
    cFormVar="w_CLFASDES",value=space(40),;
    ToolTipText = "Descrizione breve fase",;
    HelpContextID = 206853255,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=119, Top=0, cSayPict=[repl("X",40)], cGetPict=[repl("X",40)], InputMask=replicate('X',40), backstyle=0

  add object oCLINDFAS_2_4 as StdTrsField with uid="OMEDKWRGDC",rtseq=16,rtrep=.t.,;
    cFormVar="w_CLINDFAS",value=space(2),nZero=2,;
    ToolTipText = "Indice di preferenza fase",;
    HelpContextID = 188163207,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=24, Left=420, Top=0, cSayPict=["99"], cGetPict=["99"], InputMask=replicate('X',2), backstyle=0

  add object oCLINIVAL_2_5 as StdTrsField with uid="GCFMTONNJG",rtseq=17,rtrep=.t.,;
    cFormVar="w_CLINIVAL",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio validit� della fase",;
    HelpContextID = 182920334,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio validit� maggiore della data di fine validit�",;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=454, Top=0, backstyle=0

  func oCLINIVAL_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CLINIVAL<=.w_CLFINVAL)
    endwith
    return bRes
  endfunc

  add object oCLFINVAL_2_6 as StdTrsField with uid="FZSPZRRZOF",rtseq=18,rtrep=.t.,;
    cFormVar="w_CLFINVAL",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine validit� della fase",;
    HelpContextID = 178017422,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio validit� maggiore della data di fine validit�",;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=533, Top=0, backstyle=0

  func oCLFINVAL_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CLINIVAL<=.w_CLFINVAL)
    endwith
    return bRes
  endfunc

  add object oCLCOUPOI_2_7 as StdTrsCheck with uid="IEKLAJSTUX",rtrep=.t.,;
    cFormVar="w_CLCOUPOI",  caption="",;
    ToolTipText = "Se attivo la fase � count point",;
    HelpContextID = 265911151,;
    Left=717, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLCOUPOI_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLCOUPOI,&i_cF..t_CLCOUPOI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLCOUPOI_2_7.GetRadio()
    this.Parent.oContained.w_CLCOUPOI = this.RadioValue()
    return .t.
  endfunc

  func oCLCOUPOI_2_7.ToRadio()
    this.Parent.oContained.w_CLCOUPOI=trim(this.Parent.oContained.w_CLCOUPOI)
    return(;
      iif(this.Parent.oContained.w_CLCOUPOI=='S',1,;
      0))
  endfunc

  func oCLCOUPOI_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLFASOUT_2_8 as StdTrsCheck with uid="UIGNZZKTNN",rtrep=.t.,;
    cFormVar="w_CLFASOUT",  caption="",;
    ToolTipText = "Se attivo: la procedura gestisce nel WIP l'output di fase",;
    HelpContextID = 22303878,;
    Left=753, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLFASOUT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASOUT,&i_cF..t_CLFASOUT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASOUT_2_8.GetRadio()
    this.Parent.oContained.w_CLFASOUT = this.RadioValue()
    return .t.
  endfunc

  func oCLFASOUT_2_8.ToRadio()
    this.Parent.oContained.w_CLFASOUT=trim(this.Parent.oContained.w_CLFASOUT)
    return(;
      iif(this.Parent.oContained.w_CLFASOUT=='S',1,;
      0))
  endfunc

  func oCLFASOUT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFASOUT_2_8.mCond()
    with this.Parent.oContained
      return (.w_CLCOUPOI='S')
    endwith
  endfunc

  add object oCLPRIFAS_2_17 as StdTrsCheck with uid="XEXNPATLNB",rtrep=.t.,;
    cFormVar="w_CLPRIFAS", enabled=.f., caption="",;
    ToolTipText = "Se attivo Indica che � la prima fase del ciclo",;
    HelpContextID = 182629511,;
    Left=789, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLPRIFAS_2_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLPRIFAS,&i_cF..t_CLPRIFAS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLPRIFAS_2_17.GetRadio()
    this.Parent.oContained.w_CLPRIFAS = this.RadioValue()
    return .t.
  endfunc

  func oCLPRIFAS_2_17.ToRadio()
    this.Parent.oContained.w_CLPRIFAS=trim(this.Parent.oContained.w_CLPRIFAS)
    return(;
      iif(this.Parent.oContained.w_CLPRIFAS=='S',1,;
      0))
  endfunc

  func oCLPRIFAS_2_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLULTFAS_2_18 as StdTrsCheck with uid="DQGYRNACHC",rtrep=.t.,;
    cFormVar="w_CLULTFAS", enabled=.f., caption="",;
    ToolTipText = "Se attivo Indica che � l'ultima fase del ciclo",;
    HelpContextID = 171467911,;
    Left=827, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLULTFAS_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLULTFAS,&i_cF..t_CLULTFAS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLULTFAS_2_18.GetRadio()
    this.Parent.oContained.w_CLULTFAS = this.RadioValue()
    return .t.
  endfunc

  func oCLULTFAS_2_18.ToRadio()
    this.Parent.oContained.w_CLULTFAS=trim(this.Parent.oContained.w_CLULTFAS)
    return(;
      iif(this.Parent.oContained.w_CLULTFAS=='S',1,;
      0))
  endfunc

  func oCLULTFAS_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLFASCLA_2_19 as StdTrsCheck with uid="PKUCPTGFBF",rtrep=.t.,;
    cFormVar="w_CLFASCLA", enabled=.f., caption="",;
    ToolTipText = "Se attivo la fase � conto lavoro",;
    HelpContextID = 44804967,;
    Left=864, Top=0, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., backstyle=0;
   , bGlobalFont=.t.


  func oCLFASCLA_2_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFASCLA,&i_cF..t_CLFASCLA),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCLFASCLA_2_19.GetRadio()
    this.Parent.oContained.w_CLFASCLA = this.RadioValue()
    return .t.
  endfunc

  func oCLFASCLA_2_19.ToRadio()
    this.Parent.oContained.w_CLFASCLA=trim(this.Parent.oContained.w_CLFASCLA)
    return(;
      iif(this.Parent.oContained.w_CLFASCLA=='S',1,;
      0))
  endfunc

  func oCLFASCLA_2_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLLTFASE_2_21 as StdTrsField with uid="ISCVCOUNJN",rtseq=36,rtrep=.t.,;
    cFormVar="w_CLLTFASE",value=0,;
    ToolTipText = "Lead time del centro di lavoro (espresso in giorni)",;
    HelpContextID = 1111189,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=611, Top=0, cSayPict=["@Z 99,999,999.999"], cGetPict=["99,999,999.999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_mcl','CIC_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CLSERIAL=CIC_MAST.CLSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
