* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_scs                                                        *
*              Stampa classi risorse                                           *
*                                                                              *
*      Author: CF                                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-22                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_scs",oParentObject))

* --- Class definition
define class tgsci_scs as StdForm
  Top    = 20
  Left   = 8

  * --- Standard Properties
  Width  = 517
  Height = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-29"
  HelpContextID=90834281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  CLR_MAST_IDX = 0
  cPrg = "gsci_scs"
  cComment = "Stampa classi risorse"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(5)
  o_CODINI = space(5)
  w_CODFIN = space(5)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_TIPO = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_scsPag1","gsci_scs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLR_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(5)
      .w_CODFIN=space(5)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_TIPO=space(2)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
          .DoRTCalc(3,4,.f.)
        .w_TIPO = ''
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_2('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCLA',trim(this.w_CODINI))
          select CSCODCLA,CSDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CLR_MAST','*','CSCODCLA',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'',"Classi risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',oSource.xKey(1))
            select CSCODCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_CODINI)
            select CSCODCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CSCODCLA,space(5))
      this.w_DESINI = NVL(_Link_.CSDESCLA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(5)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CODFIN).or.(.w_CODFIN>=.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(5)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCLA',trim(this.w_CODFIN))
          select CSCODCLA,CSDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CLR_MAST','*','CSCODCLA',cp_AbsName(oSource.parent,'oCODFIN_1_2'),i_cWhere,'',"Classi risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',oSource.xKey(1))
            select CSCODCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_CODFIN)
            select CSCODCLA,CSDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CSCODCLA,space(5))
      this.w_DESFIN = NVL(_Link_.CSDESCLA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(5)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CODINI).or.(.w_CODFIN>=.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(5)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_2.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_2.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_5.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_5.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_6.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_6.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_12.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_12.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_CODFIN).or.(.w_CODFIN>=.w_CODINI))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_CODINI).or.(.w_CODFIN>=.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    return

enddefine

* --- Define pages as container
define class tgsci_scsPag1 as StdContainer
  Width  = 513
  height = 184
  stdWidth  = 513
  stdheight = 184
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="FMFTPIAEPC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe di inizio selezione",;
    HelpContextID = 121401818,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=96, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_MAST", oKey_1_1="CSCODCLA", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLR_MAST','*','CSCODCLA',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi risorse",'',this.parent.oContained
  endproc

  add object oCODFIN_1_2 as StdField with uid="XUQKJMIMCP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe di fine selezione",;
    HelpContextID = 42955226,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=96, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_MAST", oKey_1_1="CSCODCLA", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLR_MAST','*','CSCODCLA',cp_AbsName(this.parent,'oCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi risorse",'',this.parent.oContained
  endproc

  add object oDESINI_1_5 as StdField with uid="ALUTCBZNQA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121342922,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=185, Top=12, InputMask=replicate('X',40)

  add object oDESFIN_1_6 as StdField with uid="RPOETGFPVQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42896330,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=185, Top=40, InputMask=replicate('X',40)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=96, top=99, width=403,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87163366


  add object oBtn_1_9 as StdButton with uid="AUGRCIKLWO",left=402, top=132, width=48,height=45,;
    CpPicture="BMP\STAMPA.bmp", caption="", nPag=1;
    , ToolTipText = "Stampa selezione";
    , HelpContextID = 50955302;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="VEEALGGJEA",left=452, top=132, width=48,height=45,;
    CpPicture="BMP\Esc.bmp", caption="", nPag=1;
    , ToolTipText = "Uscita dalla maschera";
    , HelpContextID = 54895430;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPO_1_12 as StdCombo with uid="FUPYUSHWTN",value=1,rtseq=5,rtrep=.f.,left=96,top=68,width=108,height=21;
    , HelpContextID = 85309130;
    , cFormVar="w_TIPO",RowSource=""+"Tutti,"+"Centri di lavoro,"+"Squadre,"+"Attrezzature,"+"Spazi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_12.RadioValue()
    return(iif(this.value =1,'',;
    iif(this.value =2,'CL',;
    iif(this.value =3,'SQ',;
    iif(this.value =4,'AT',;
    iif(this.value =5,'SP',;
    space(2)))))))
  endfunc
  func oTIPO_1_12.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_12.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='',1,;
      iif(this.Parent.oContained.w_TIPO=='CL',2,;
      iif(this.Parent.oContained.w_TIPO=='SQ',3,;
      iif(this.Parent.oContained.w_TIPO=='AT',4,;
      iif(this.Parent.oContained.w_TIPO=='SP',5,;
      0)))))
  endfunc

  add object oStr_1_3 as StdString with uid="FSFMFVPKPO",Visible=.t., Left=23, Top=12,;
    Alignment=1, Width=68, Height=18,;
    Caption="Da classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="SMCXMPTNWR",Visible=.t., Left=26, Top=40,;
    Alignment=1, Width=65, Height=18,;
    Caption="A classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QUZEGTPOVU",Visible=.t., Left=11, Top=99,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="DEYHMVMFVT",Visible=.t., Left=23, Top=68,;
    Alignment=1, Width=68, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_scs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
