* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci1bgp                                                        *
*              Selezione zoom                                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-10                                                      *
* Last revis.: 2015-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci1bgp",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsci1bgp as StdBatch
  * --- Local variables
  pAZIONE = space(1)
  w_BLOCCO = space(1)
  * --- WorkFile variables
  PAR_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pAZIONE="Z"
        NC= this.oParentObject.w_ODL.cCursor
        if used(NC)
          do case
            case this.oParentObject.w_SELEZI="S"
              * --- Seleziona tutte le righe dello zoom
              UPDATE (NC) SET xChk=1
            case this.oParentObject.w_SELEZI="D"
              * --- Deseleziona tutte le righe dello zoom
              UPDATE (NC) SET xChk=0
            otherwise
              * --- Inverte Selezione
              UPDATE (NC) SET xChk=abs(xChk-1)
          endcase
        endif
      case this.pAZIONE="I"
        * --- Try
        local bErr_0108F7B8
        bErr_0108F7B8=bTrsErr
        this.Try_0108F7B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0108F7B8
        * --- End
      case this.pAZIONE="BLOCCO"
        * --- Read from PAR_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPBLOMRP"+;
            " from "+i_cTable+" PAR_PROD where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPBLOMRP;
            from (i_cTable) where;
                PPCODICE = "PP";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BLOCCO = NVL(cp_ToDate(_read_.PPBLOMRP),cp_NullValue(_read_.PPBLOMRP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_BLOCCO,"N")="S"
          if g_UseProgBar
            GesProgBar("Q", this)
          endif
          ah_errormsg("Flag di blocco pianificazione conto lavoro attivato: elaborazione abortita!",16)
          this.oParentObject.opgfrm.Page2.enabled=.f. 
 this.oParentObject.w_ELABORA="N"
          i_retcode = 'stop'
          return
        else
          * --- Write into PAR_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PPBLOMRP ="+cp_NullLink(cp_ToStrODBC("S"),'PAR_PROD','PPBLOMRP');
                +i_ccchkf ;
            +" where ";
                +"PPCODICE = "+cp_ToStrODBC("PP");
                   )
          else
            update (i_cTable) set;
                PPBLOMRP = "S";
                &i_ccchkf. ;
             where;
                PPCODICE = "PP";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.oParentObject.w_ELABORA="S"
        endif
      case this.pAZIONE="SBLOCCO"
        * --- Write into PAR_PROD
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PPBLOMRP ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_PROD','PPBLOMRP');
              +i_ccchkf ;
          +" where ";
              +"PPCODICE = "+cp_ToStrODBC("PP");
                 )
        else
          update (i_cTable) set;
              PPBLOMRP = "N";
              &i_ccchkf. ;
           where;
              PPCODICE = "PP";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc
  proc Try_0108F7B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Ritempifica tutti gli ODL con fasi esterne 
    * --- begin transaction
    cp_BeginTrs()
    GSCO_BTF(this," ")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_PROD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
