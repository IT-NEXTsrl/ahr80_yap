* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_mlr                                                        *
*              Lista risorse                                                   *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-21                                                      *
* Last revis.: 2015-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsci_mlr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsci_mlr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsci_mlr")
  return

* --- Class definition
define class tgsci_mlr as StdPCForm
  Width  = 913
  Height = 215+35
  Top    = 10
  Left   = 22
  cComment = "Lista risorse"
  cPrg = "gsci_mlr"
  HelpContextID=214566249
  add object cnt as tcgsci_mlr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsci_mlr as PCContext
  w_LRSERIAL = space(10)
  w_LRROWNUM = 0
  w_CLCODDIS = space(41)
  w_CLINIVAL = space(8)
  w_CLINDFAS = space(2)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_CSFLAPRE = space(1)
  w_CPROWORD = 0
  w_LRCODCLA = space(5)
  w_LRCODCLA = space(5)
  w_CLARIS = space(20)
  w_LR__TIPO = space(2)
  w_LRCODRIS = space(20)
  w_LRTIPTEM = space(1)
  w_QTARIS = 0
  w_UMTDEF = space(3)
  w_LRQTARIS = 0
  w_LRUMTRIS = space(3)
  w_CONSEC = 0
  w_LRTEMRIS = 0
  w_LRNUMIMP = 0
  w_LRTMPCIC = 0
  w_LRTMPSEC = 0
  w_LRPROORA = 0
  w_DESRIS = space(40)
  w_RLINTEST = space(1)
  w_DESCLA = space(40)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_LRISTLAV = space(10)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_UMFLTEMP = space(1)
  w_LRTEMSEC = 0
  w_LR1TIPO = space(2)
  proc Save(i_oFrom)
    this.w_LRSERIAL = i_oFrom.w_LRSERIAL
    this.w_LRROWNUM = i_oFrom.w_LRROWNUM
    this.w_CLCODDIS = i_oFrom.w_CLCODDIS
    this.w_CLINIVAL = i_oFrom.w_CLINIVAL
    this.w_CLINDFAS = i_oFrom.w_CLINDFAS
    this.w_NUMFAS = i_oFrom.w_NUMFAS
    this.w_DESFAS = i_oFrom.w_DESFAS
    this.w_CSFLAPRE = i_oFrom.w_CSFLAPRE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_LRCODCLA = i_oFrom.w_LRCODCLA
    this.w_LRCODCLA = i_oFrom.w_LRCODCLA
    this.w_CLARIS = i_oFrom.w_CLARIS
    this.w_LR__TIPO = i_oFrom.w_LR__TIPO
    this.w_LRCODRIS = i_oFrom.w_LRCODRIS
    this.w_LRTIPTEM = i_oFrom.w_LRTIPTEM
    this.w_QTARIS = i_oFrom.w_QTARIS
    this.w_UMTDEF = i_oFrom.w_UMTDEF
    this.w_LRQTARIS = i_oFrom.w_LRQTARIS
    this.w_LRUMTRIS = i_oFrom.w_LRUMTRIS
    this.w_CONSEC = i_oFrom.w_CONSEC
    this.w_LRTEMRIS = i_oFrom.w_LRTEMRIS
    this.w_LRNUMIMP = i_oFrom.w_LRNUMIMP
    this.w_LRTMPCIC = i_oFrom.w_LRTMPCIC
    this.w_LRTMPSEC = i_oFrom.w_LRTMPSEC
    this.w_LRPROORA = i_oFrom.w_LRPROORA
    this.w_DESRIS = i_oFrom.w_DESRIS
    this.w_RLINTEST = i_oFrom.w_RLINTEST
    this.w_DESCLA = i_oFrom.w_DESCLA
    this.w_NUMFAS = i_oFrom.w_NUMFAS
    this.w_DESFAS = i_oFrom.w_DESFAS
    this.w_LRISTLAV = i_oFrom.w_LRISTLAV
    this.w_NUMFAS = i_oFrom.w_NUMFAS
    this.w_DESFAS = i_oFrom.w_DESFAS
    this.w_UMFLTEMP = i_oFrom.w_UMFLTEMP
    this.w_LRTEMSEC = i_oFrom.w_LRTEMSEC
    this.w_LR1TIPO = i_oFrom.w_LR1TIPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_LRSERIAL = this.w_LRSERIAL
    i_oTo.w_LRROWNUM = this.w_LRROWNUM
    i_oTo.w_CLCODDIS = this.w_CLCODDIS
    i_oTo.w_CLINIVAL = this.w_CLINIVAL
    i_oTo.w_CLINDFAS = this.w_CLINDFAS
    i_oTo.w_NUMFAS = this.w_NUMFAS
    i_oTo.w_DESFAS = this.w_DESFAS
    i_oTo.w_CSFLAPRE = this.w_CSFLAPRE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_LRCODCLA = this.w_LRCODCLA
    i_oTo.w_LRCODCLA = this.w_LRCODCLA
    i_oTo.w_CLARIS = this.w_CLARIS
    i_oTo.w_LR__TIPO = this.w_LR__TIPO
    i_oTo.w_LRCODRIS = this.w_LRCODRIS
    i_oTo.w_LRTIPTEM = this.w_LRTIPTEM
    i_oTo.w_QTARIS = this.w_QTARIS
    i_oTo.w_UMTDEF = this.w_UMTDEF
    i_oTo.w_LRQTARIS = this.w_LRQTARIS
    i_oTo.w_LRUMTRIS = this.w_LRUMTRIS
    i_oTo.w_CONSEC = this.w_CONSEC
    i_oTo.w_LRTEMRIS = this.w_LRTEMRIS
    i_oTo.w_LRNUMIMP = this.w_LRNUMIMP
    i_oTo.w_LRTMPCIC = this.w_LRTMPCIC
    i_oTo.w_LRTMPSEC = this.w_LRTMPSEC
    i_oTo.w_LRPROORA = this.w_LRPROORA
    i_oTo.w_DESRIS = this.w_DESRIS
    i_oTo.w_RLINTEST = this.w_RLINTEST
    i_oTo.w_DESCLA = this.w_DESCLA
    i_oTo.w_NUMFAS = this.w_NUMFAS
    i_oTo.w_DESFAS = this.w_DESFAS
    i_oTo.w_LRISTLAV = this.w_LRISTLAV
    i_oTo.w_NUMFAS = this.w_NUMFAS
    i_oTo.w_DESFAS = this.w_DESFAS
    i_oTo.w_UMFLTEMP = this.w_UMFLTEMP
    i_oTo.w_LRTEMSEC = this.w_LRTEMSEC
    i_oTo.w_LR1TIPO = this.w_LR1TIPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsci_mlr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 913
  Height = 215+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-22"
  HelpContextID=214566249
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  RIS_MAST_IDX = 0
  RIS_DETT_IDX = 0
  RIS_ORSE_IDX = 0
  CIC_MAIN_IDX = 0
  CLR_MAST_IDX = 0
  CLR_DETT_IDX = 0
  UNIMIS_IDX = 0
  TIP_RISO_IDX = 0
  cFile = "RIS_MAST"
  cFileDetail = "RIS_DETT"
  cKeySelect = "LRSERIAL,LRROWNUM"
  cKeyWhere  = "LRSERIAL=this.w_LRSERIAL and LRROWNUM=this.w_LRROWNUM"
  cKeyDetail  = "LRSERIAL=this.w_LRSERIAL and LRROWNUM=this.w_LRROWNUM"
  cKeyWhereODBC = '"LRSERIAL="+cp_ToStrODBC(this.w_LRSERIAL)';
      +'+" and LRROWNUM="+cp_ToStrODBC(this.w_LRROWNUM)';

  cKeyDetailWhereODBC = '"LRSERIAL="+cp_ToStrODBC(this.w_LRSERIAL)';
      +'+" and LRROWNUM="+cp_ToStrODBC(this.w_LRROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"RIS_DETT.LRSERIAL="+cp_ToStrODBC(this.w_LRSERIAL)';
      +'+" and RIS_DETT.LRROWNUM="+cp_ToStrODBC(this.w_LRROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RIS_DETT.CPROWORD '
  cPrg = "gsci_mlr"
  cComment = "Lista risorse"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LRSERIAL = space(10)
  w_LRROWNUM = 0
  w_CLCODDIS = space(41)
  w_CLINIVAL = ctod('  /  /  ')
  w_CLINDFAS = space(2)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_CSFLAPRE = space(1)
  w_CPROWORD = 0
  w_LRCODCLA = space(5)
  o_LRCODCLA = space(5)
  w_LRCODCLA = space(5)
  w_CLARIS = space(20)
  w_LR__TIPO = space(2)
  w_LRCODRIS = space(20)
  o_LRCODRIS = space(20)
  w_LRTIPTEM = space(1)
  w_QTARIS = 0
  w_UMTDEF = space(3)
  w_LRQTARIS = 0
  w_LRUMTRIS = space(3)
  o_LRUMTRIS = space(3)
  w_CONSEC = 0
  w_LRTEMRIS = 0
  o_LRTEMRIS = 0
  w_LRNUMIMP = 0
  o_LRNUMIMP = 0
  w_LRTMPCIC = 0
  o_LRTMPCIC = 0
  w_LRTMPSEC = 0
  w_LRPROORA = 0
  w_DESRIS = space(40)
  w_RLINTEST = space(1)
  w_DESCLA = space(40)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_LRISTLAV = space(0)
  w_NUMFAS = 0
  w_DESFAS = space(40)
  w_UMFLTEMP = space(1)
  w_LRTEMSEC = 0
  w_LR1TIPO = space(2)

  * --- Children pointers
  GSCI_MMI = .NULL.
  GSCI_MMO = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_mlrPag1","gsci_mlr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Risorse")
      .Pages(2).addobject("oPag","tgsci_mlrPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Materiali della fase")
      .Pages(3).addobject("oPag","tgsci_mlrPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Materiali di output")
      .Pages(4).addobject("oPag","tgsci_mlrPag4")
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note della fase")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='CIC_MAIN'
    this.cWorkTables[3]='CLR_MAST'
    this.cWorkTables[4]='CLR_DETT'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='TIP_RISO'
    this.cWorkTables[7]='RIS_MAST'
    this.cWorkTables[8]='RIS_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIS_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIS_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MMI = CREATEOBJECT('stdDynamicChild',this,'GSCI_MMI',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    this.GSCI_MMO = CREATEOBJECT('stdDynamicChild',this,'GSCI_MMO',this.oPgFrm.Page3.oPag.oLinkPC_5_1)
    this.GSCI_MMO.createrealchild()
    return

    procedure NewContext()
      return(createobject('tsgsci_mlr'))

    function DestroyChildrenChain()
      this.oParentObject=.NULL.
      if !ISNULL(this.GSCI_MMI)
        this.GSCI_MMI.DestroyChildrenChain()
      endif
      if !ISNULL(this.GSCI_MMO)
        this.GSCI_MMO.DestroyChildrenChain()
      endif
      return

    function HideChildrenChain()
      *this.Hide()
      this.bOnScreen = .f.
      this.GSCI_MMI.HideChildrenChain()
      this.GSCI_MMO.HideChildrenChain()
      return

    function ShowChildrenChain()
      this.bOnScreen=.t.
      this.GSCI_MMI.ShowChildrenChain()
      this.GSCI_MMO.ShowChildrenChain()
      DoDefault()
      return
  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MMI)
      this.GSCI_MMI.DestroyChildrenChain()
      this.GSCI_MMI=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSCI_MMO)
      this.GSCI_MMO.DestroyChildrenChain()
      this.GSCI_MMO=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_5_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MMI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCI_MMO.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MMI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCI_MMO.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MMI.NewDocument()
    this.GSCI_MMO.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCI_MMI.ChangeRow(this.cRowID+'      1',1;
             ,.w_LRSERIAL,"MISERIAL";
             ,.w_LRROWNUM,"MIROWNUM";
             )
      .GSCI_MMO.ChangeRow(this.cRowID+'      1',1;
             ,.w_LRSERIAL,"MOSERIAL";
             ,.w_LRROWNUM,"MOROWNUM";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from RIS_MAST where LRSERIAL=KeySet.LRSERIAL
    *                            and LRROWNUM=KeySet.LRROWNUM
    *
    i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_MAST_IDX,2],this.bLoadRecFilter,this.RIS_MAST_IDX,"gsci_mlr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIS_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIS_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"RIS_DETT.","RIS_MAST.")
      i_cTable = i_cTable+' RIS_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LRSERIAL',this.w_LRSERIAL  ,'LRROWNUM',this.w_LRROWNUM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .w_CSFLAPRE = 'S'
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .w_UMFLTEMP = 'S'
        .w_LRSERIAL = NVL(LRSERIAL,space(10))
        .w_LRROWNUM = NVL(LRROWNUM,0)
        .w_CLCODDIS = .oParentObject.w_CLCODDIS
        .w_CLINIVAL = .oParentObject.w_CLINIVAL
        .w_CLINDFAS = .oParentObject.w_CLINDFAS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_LRISTLAV = NVL(LRISTLAV,space(0))
        cp_LoadRecExtFlds(this,'RIS_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from RIS_DETT where LRSERIAL=KeySet.LRSERIAL
      *                            and LRROWNUM=KeySet.LRROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.RIS_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('RIS_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "RIS_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" RIS_DETT"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'LRSERIAL',this.w_LRSERIAL  ,'LRROWNUM',this.w_LRROWNUM  )
        select * from (i_cTable) RIS_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CLARIS = space(20)
          .w_QTARIS = 0
          .w_UMTDEF = space(3)
          .w_CONSEC = 0
          .w_DESRIS = space(40)
          .w_RLINTEST = space(1)
          .w_DESCLA = space(40)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_LRCODCLA = NVL(LRCODCLA,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_LRCODCLA = NVL(LRCODCLA,space(5))
          if link_2_3_joined
            this.w_LRCODCLA = NVL(CSCODCLA203,NVL(this.w_LRCODCLA,space(5)))
            this.w_DESCLA = NVL(CSDESCLA203,space(40))
            this.w_LR__TIPO = NVL(CSTIPCLA203,space(2))
          else
          .link_2_3('Load')
          endif
          .w_LR__TIPO = NVL(LR__TIPO,space(2))
          .w_LRCODRIS = NVL(LRCODRIS,space(20))
          .link_2_6('Load')
          .w_LRTIPTEM = NVL(LRTIPTEM,space(1))
          .w_LRQTARIS = NVL(LRQTARIS,0)
          .w_LRUMTRIS = NVL(LRUMTRIS,space(3))
          .link_2_11('Load')
          .w_LRTEMRIS = NVL(LRTEMRIS,0)
          .w_LRNUMIMP = NVL(LRNUMIMP,0)
          .w_LRTMPCIC = NVL(LRTMPCIC,0)
          .w_LRTMPSEC = NVL(LRTMPSEC,0)
          .w_LRPROORA = NVL(LRPROORA,0)
          .w_LRTEMSEC = NVL(LRTEMSEC,0)
        .w_LR1TIPO = .w_LR__TIPO
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CLCODDIS = .oParentObject.w_CLCODDIS
        .w_CLINIVAL = .oParentObject.w_CLINIVAL
        .w_CLINDFAS = .oParentObject.w_CLINDFAS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page2.oPag.oBtn_4_5.enabled = .oPgFrm.Page2.oPag.oBtn_4_5.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LRSERIAL=space(10)
      .w_LRROWNUM=0
      .w_CLCODDIS=space(41)
      .w_CLINIVAL=ctod("  /  /  ")
      .w_CLINDFAS=space(2)
      .w_NUMFAS=0
      .w_DESFAS=space(40)
      .w_CSFLAPRE=space(1)
      .w_CPROWORD=10
      .w_LRCODCLA=space(5)
      .w_LRCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_LR__TIPO=space(2)
      .w_LRCODRIS=space(20)
      .w_LRTIPTEM=space(1)
      .w_QTARIS=0
      .w_UMTDEF=space(3)
      .w_LRQTARIS=0
      .w_LRUMTRIS=space(3)
      .w_CONSEC=0
      .w_LRTEMRIS=0
      .w_LRNUMIMP=0
      .w_LRTMPCIC=0
      .w_LRTMPSEC=0
      .w_LRPROORA=0
      .w_DESRIS=space(40)
      .w_RLINTEST=space(1)
      .w_DESCLA=space(40)
      .w_NUMFAS=0
      .w_DESFAS=space(40)
      .w_LRISTLAV=space(0)
      .w_NUMFAS=0
      .w_DESFAS=space(40)
      .w_UMFLTEMP=space(1)
      .w_LRTEMSEC=0
      .w_LR1TIPO=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CLCODDIS = .oParentObject.w_CLCODDIS
        .w_CLINIVAL = .oParentObject.w_CLINIVAL
        .w_CLINDFAS = .oParentObject.w_CLINDFAS
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .w_CSFLAPRE = 'S'
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_LRCODCLA))
         .link_2_2('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_LRCODCLA))
         .link_2_3('Full')
        endif
        .DoRTCalc(12,12,.f.)
        .w_LR__TIPO = 'CL'
        .w_LRCODRIS = iif(empty(.w_CLARIS),.w_LRCODRIS,.w_CLARIS)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_LRCODRIS))
         .link_2_6('Full')
        endif
        .w_LRTIPTEM = 'L'
        .DoRTCalc(16,19,.f.)
        if not(empty(.w_LRUMTRIS))
         .link_2_11('Full')
        endif
        .DoRTCalc(20,20,.f.)
        .w_LRTEMRIS = iif(.w_LRTMPCIC > 0 , .w_LRTMPCIC / .w_LRNUMIMP , 0)
        .w_LRNUMIMP = 1
        .w_LRTMPCIC = iif(.w_LRTEMRIS>0 , .w_LRTEMRIS * .w_LRNUMIMP, 0)
        .w_LRTMPSEC = .w_LRTMPCIC*.w_CONSEC
        .w_LRPROORA = iif(.w_LRTMPSEC=0 and .w_LRTMPSEC=0 , 0, 3600/IIF(.w_LRTMPSEC<>0 , .w_LRTMPSEC, .w_LRTMPSEC)) *.w_LRNUMIMP
        .DoRTCalc(26,28,.f.)
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(31,31,.f.)
        .w_NUMFAS = this.oParentObject .w_CPROWORD
        .w_DESFAS = this.oParentObject .w_CLFASDES
        .w_UMFLTEMP = 'S'
        .w_LRTEMSEC = .w_LRTEMRIS*.w_CONSEC
        .w_LR1TIPO = .w_LR__TIPO
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIS_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page2.oPag.oBtn_4_5.enabled = this.oPgFrm.Page2.oPag.oBtn_4_5.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLRNUMIMP_2_14.enabled = i_bVal
      .Page1.oPag.oLRTMPCIC_2_15.enabled = i_bVal
      .Page4.oPag.oLRISTLAV_6_1.enabled = i_bVal
      .Page2.oPag.oBtn_4_5.enabled = .Page2.oPag.oBtn_4_5.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCI_MMI.SetStatus(i_cOp)
    this.GSCI_MMO.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'RIS_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MMI.SetChildrenStatus(i_cOp)
  *  this.GSCI_MMO.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LRSERIAL,"LRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LRROWNUM,"LRROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LRISTLAV,"LRISTLAV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_LRCODCLA C(5);
      ,t_LR__TIPO N(3);
      ,t_LRCODRIS C(20);
      ,t_LRTIPTEM N(3);
      ,t_LRQTARIS N(8,2);
      ,t_LRUMTRIS C(3);
      ,t_CONSEC N(9,5);
      ,t_LRTEMRIS N(18,4);
      ,t_LRNUMIMP N(4);
      ,t_LRTMPCIC N(18,4);
      ,t_LRTMPSEC N(18,4);
      ,t_LRPROORA N(18,4);
      ,t_DESRIS C(40);
      ,t_RLINTEST N(3);
      ,t_DESCLA C(40);
      ,CPROWNUM N(10);
      ,t_CLARIS C(20);
      ,t_QTARIS N(12,5);
      ,t_UMTDEF C(3);
      ,t_LRTEMSEC N(18,4);
      ,t_LR1TIPO C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mlrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODCLA_2_2.controlsource=this.cTrsName+'.t_LRCODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.controlsource=this.cTrsName+'.t_LR__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODRIS_2_6.controlsource=this.cTrsName+'.t_LRCODRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.controlsource=this.cTrsName+'.t_LRTIPTEM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRQTARIS_2_10.controlsource=this.cTrsName+'.t_LRQTARIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRUMTRIS_2_11.controlsource=this.cTrsName+'.t_LRUMTRIS'
    this.oPgFRm.Page1.oPag.oCONSEC_2_12.controlsource=this.cTrsName+'.t_CONSEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLRTEMRIS_2_13.controlsource=this.cTrsName+'.t_LRTEMRIS'
    this.oPgFRm.Page1.oPag.oLRNUMIMP_2_14.controlsource=this.cTrsName+'.t_LRNUMIMP'
    this.oPgFRm.Page1.oPag.oLRTMPCIC_2_15.controlsource=this.cTrsName+'.t_LRTMPCIC'
    this.oPgFRm.Page1.oPag.oLRTMPSEC_2_16.controlsource=this.cTrsName+'.t_LRTMPSEC'
    this.oPgFRm.Page1.oPag.oLRPROORA_2_17.controlsource=this.cTrsName+'.t_LRPROORA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_20.controlsource=this.cTrsName+'.t_DESRIS'
    this.oPgFRm.Page1.oPag.oRLINTEST_2_21.controlsource=this.cTrsName+'.t_RLINTEST'
    this.oPgFRm.Page1.oPag.oDESCLA_2_23.controlsource=this.cTrsName+'.t_DESCLA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.AddVLine(112)
    this.AddVLine(233)
    this.AddVLine(389)
    this.AddVLine(602)
    this.AddVLine(723)
    this.AddVLine(762)
    this.AddVLine(804)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIS_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIS_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'RIS_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(LRSERIAL,LRROWNUM,LRISTLAV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_LRSERIAL)+;
                    ","+cp_ToStrODBC(this.w_LRROWNUM)+;
                    ","+cp_ToStrODBC(this.w_LRISTLAV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIS_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'RIS_MAST')
        cp_CheckDeletedKey(i_cTable,0,'LRSERIAL',this.w_LRSERIAL,'LRROWNUM',this.w_LRROWNUM)
        INSERT INTO (i_cTable);
              (LRSERIAL,LRROWNUM,LRISTLAV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_LRSERIAL;
                  ,this.w_LRROWNUM;
                  ,this.w_LRISTLAV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIS_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_DETT_IDX,2])
      *
      * insert into RIS_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(LRSERIAL,LRROWNUM,CPROWORD,LRCODCLA,LR__TIPO"+;
                  ",LRCODRIS,LRTIPTEM,LRQTARIS,LRUMTRIS,LRTEMRIS"+;
                  ",LRNUMIMP,LRTMPCIC,LRTMPSEC,LRPROORA,LRTEMSEC,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LRSERIAL)+","+cp_ToStrODBC(this.w_LRROWNUM)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_LRCODCLA)+","+cp_ToStrODBC(this.w_LR__TIPO)+;
             ","+cp_ToStrODBCNull(this.w_LRCODRIS)+","+cp_ToStrODBC(this.w_LRTIPTEM)+","+cp_ToStrODBC(this.w_LRQTARIS)+","+cp_ToStrODBCNull(this.w_LRUMTRIS)+","+cp_ToStrODBC(this.w_LRTEMRIS)+;
             ","+cp_ToStrODBC(this.w_LRNUMIMP)+","+cp_ToStrODBC(this.w_LRTMPCIC)+","+cp_ToStrODBC(this.w_LRTMPSEC)+","+cp_ToStrODBC(this.w_LRPROORA)+","+cp_ToStrODBC(this.w_LRTEMSEC)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LRSERIAL',this.w_LRSERIAL,'LRROWNUM',this.w_LRROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_LRSERIAL,this.w_LRROWNUM,this.w_CPROWORD,this.w_LRCODCLA,this.w_LR__TIPO"+;
                ",this.w_LRCODRIS,this.w_LRTIPTEM,this.w_LRQTARIS,this.w_LRUMTRIS,this.w_LRTEMRIS"+;
                ",this.w_LRNUMIMP,this.w_LRTMPCIC,this.w_LRTMPSEC,this.w_LRPROORA,this.w_LRTEMSEC,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update RIS_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'RIS_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " LRISTLAV="+cp_ToStrODBC(this.w_LRISTLAV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'RIS_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'LRSERIAL',this.w_LRSERIAL  ,'LRROWNUM',this.w_LRROWNUM  )
          UPDATE (i_cTable) SET;
              LRISTLAV=this.w_LRISTLAV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and !Empty(t_LR__TIPO) and !Empty(t_LRCODRIS) and t_LRTMPSEC>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.RIS_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.RIS_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from RIS_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RIS_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",LRCODCLA="+cp_ToStrODBCNull(this.w_LRCODCLA)+;
                     ",LR__TIPO="+cp_ToStrODBC(this.w_LR__TIPO)+;
                     ",LRCODRIS="+cp_ToStrODBCNull(this.w_LRCODRIS)+;
                     ",LRTIPTEM="+cp_ToStrODBC(this.w_LRTIPTEM)+;
                     ",LRQTARIS="+cp_ToStrODBC(this.w_LRQTARIS)+;
                     ",LRUMTRIS="+cp_ToStrODBCNull(this.w_LRUMTRIS)+;
                     ",LRTEMRIS="+cp_ToStrODBC(this.w_LRTEMRIS)+;
                     ",LRNUMIMP="+cp_ToStrODBC(this.w_LRNUMIMP)+;
                     ",LRTMPCIC="+cp_ToStrODBC(this.w_LRTMPCIC)+;
                     ",LRTMPSEC="+cp_ToStrODBC(this.w_LRTMPSEC)+;
                     ",LRPROORA="+cp_ToStrODBC(this.w_LRPROORA)+;
                     ",LRTEMSEC="+cp_ToStrODBC(this.w_LRTEMSEC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,LRCODCLA=this.w_LRCODCLA;
                     ,LR__TIPO=this.w_LR__TIPO;
                     ,LRCODRIS=this.w_LRCODRIS;
                     ,LRTIPTEM=this.w_LRTIPTEM;
                     ,LRQTARIS=this.w_LRQTARIS;
                     ,LRUMTRIS=this.w_LRUMTRIS;
                     ,LRTEMRIS=this.w_LRTEMRIS;
                     ,LRNUMIMP=this.w_LRNUMIMP;
                     ,LRTMPCIC=this.w_LRTMPCIC;
                     ,LRTMPSEC=this.w_LRTMPSEC;
                     ,LRPROORA=this.w_LRPROORA;
                     ,LRTEMSEC=this.w_LRTEMSEC;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSCI_MMI : Saving
      this.GSCI_MMI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_LRSERIAL,"MISERIAL";
             ,this.w_LRROWNUM,"MIROWNUM";
             )
      this.GSCI_MMI.mReplace()
      * --- GSCI_MMO : Saving
      this.GSCI_MMO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_LRSERIAL,"MOSERIAL";
             ,this.w_LRROWNUM,"MOROWNUM";
             )
      this.GSCI_MMO.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSCI_MMI : Deleting
    this.GSCI_MMI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_LRSERIAL,"MISERIAL";
           ,this.w_LRROWNUM,"MIROWNUM";
           )
    this.GSCI_MMI.mDelete()
    * --- GSCI_MMO : Deleting
    this.GSCI_MMO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_LRSERIAL,"MOSERIAL";
           ,this.w_LRROWNUM,"MOROWNUM";
           )
    this.GSCI_MMO.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and !Empty(t_LR__TIPO) and !Empty(t_LRCODRIS) and t_LRTMPSEC>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.RIS_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.RIS_DETT_IDX,2])
        *
        * delete RIS_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.RIS_MAST_IDX,2])
        *
        * delete RIS_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and !Empty(t_LR__TIPO) and !Empty(t_LRCODRIS) and t_LRTMPSEC>0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIS_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_CLCODDIS = .oParentObject.w_CLCODDIS
          .w_CLINIVAL = .oParentObject.w_CLINIVAL
          .w_CLINDFAS = .oParentObject.w_CLINDFAS
        .DoRTCalc(6,10,.t.)
        if .o_LRCODCLA<>.w_LRCODCLA
          .link_2_3('Full')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_LRCODCLA<>.w_LRCODCLA.or. .o_LRCODRIS<>.w_LRCODRIS
          .w_LRCODRIS = iif(empty(.w_CLARIS),.w_LRCODRIS,.w_CLARIS)
          .link_2_6('Full')
        endif
        .DoRTCalc(15,20,.t.)
        if .o_LRTMPCIC<>.w_LRTMPCIC.or. .o_LRNUMIMP<>.w_LRNUMIMP
          .w_LRTEMRIS = iif(.w_LRTMPCIC > 0 , .w_LRTMPCIC / .w_LRNUMIMP , 0)
        endif
        .DoRTCalc(22,22,.t.)
        if .o_LRTEMRIS<>.w_LRTEMRIS.or. .o_LRNUMIMP<>.w_LRNUMIMP
          .w_LRTMPCIC = iif(.w_LRTEMRIS>0 , .w_LRTEMRIS * .w_LRNUMIMP, 0)
        endif
        if .o_LRTEMRIS<>.w_LRTEMRIS.or. .o_LRTMPCIC<>.w_LRTMPCIC.or. .o_LRUMTRIS<>.w_LRUMTRIS
          .w_LRTMPSEC = .w_LRTMPCIC*.w_CONSEC
        endif
          .w_LRPROORA = iif(.w_LRTMPSEC=0 and .w_LRTMPSEC=0 , 0, 3600/IIF(.w_LRTMPSEC<>0 , .w_LRTMPSEC, .w_LRTMPSEC)) *.w_LRNUMIMP
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(26,34,.t.)
          .w_LRTEMSEC = .w_LRTEMRIS*.w_CONSEC
          .w_LR1TIPO = .w_LR__TIPO
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_LRCODCLA with this.w_LRCODCLA
      replace t_CLARIS with this.w_CLARIS
      replace t_QTARIS with this.w_QTARIS
      replace t_UMTDEF with this.w_UMTDEF
      replace t_LRTEMSEC with this.w_LRTEMSEC
      replace t_LR1TIPO with this.w_LR1TIPO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oBtn_4_5.enabled = this.oPgFrm.Page2.oPag.oBtn_4_5.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLRUMTRIS_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLRUMTRIS_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLRTEMRIS_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oLRTEMRIS_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oBtn_4_5.visible=!this.oPgFrm.Page2.oPag.oBtn_4_5.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LRCODCLA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_DETT_IDX,3]
    i_lTable = "CLR_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2], .t., this.CLR_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LRCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_LRCODCLA)+"%");
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);

          i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSFLAPRE,CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSFLAPRE',this.w_CSFLAPRE;
                     ,'CSCODCLA',trim(this.w_LRCODCLA))
          select CSFLAPRE,CSCODCLA,CSCODRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSFLAPRE,CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LRCODCLA)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LRCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(oSource.parent,'oLRCODCLA_2_2'),i_cWhere,'',"",'GSCI_MCS.CLR_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CSFLAPRE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',oSource.xKey(1);
                       ,'CSCODCLA',oSource.xKey(2))
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LRCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_LRCODCLA);
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',this.w_CSFLAPRE;
                       ,'CSCODCLA',this.w_LRCODCLA)
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LRCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_LRCODRIS = NVL(_Link_.CSCODRIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LRCODCLA = space(5)
      endif
      this.w_LRCODRIS = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CSFLAPRE,1)+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LRCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LRCODCLA
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LRCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LRCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSDESCLA,CSTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_LRCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_LRCODCLA)
            select CSCODCLA,CSDESCLA,CSTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LRCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_DESCLA = NVL(_Link_.CSDESCLA,space(40))
      this.w_LR__TIPO = NVL(_Link_.CSTIPCLA,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_LRCODCLA = space(5)
      endif
      this.w_DESCLA = space(40)
      this.w_LR__TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LRCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLR_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CSCODCLA as CSCODCLA203"+ ",link_2_3.CSDESCLA as CSDESCLA203"+ ",link_2_3.CSTIPCLA as CSTIPCLA203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on RIS_DETT.LRCODCLA=link_2_3.CSCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and RIS_DETT.LRCODCLA=link_2_3.CSCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LRCODRIS
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LRCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_LRCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_LR__TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_LR__TIPO;
                     ,'RLCODICE',trim(this.w_LRCODRIS))
          select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LRCODRIS)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLUMTDEF like "+cp_ToStrODBC(trim(this.w_LRCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_LR__TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLUMTDEF like "+cp_ToStr(trim(this.w_LRCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_LR__TIPO);

            select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LRCODRIS) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oLRCODRIS_2_6'),i_cWhere,'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LR__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_LR__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LRCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_LRCODRIS);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_LR__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_LR__TIPO;
                       ,'RLCODICE',this.w_LRCODRIS)
            select RL__TIPO,RLCODICE,RLUMTDEF,RLDESCRI,RLQTARIS,RLINTEST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LRCODRIS = NVL(_Link_.RLCODICE,space(20))
      this.w_UMTDEF = NVL(_Link_.RLUMTDEF,space(3))
      this.w_DESRIS = NVL(_Link_.RLDESCRI,space(40))
      this.w_QTARIS = NVL(_Link_.RLQTARIS,0)
      this.w_RLINTEST = NVL(_Link_.RLINTEST,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LRCODRIS = space(20)
      endif
      this.w_UMTDEF = space(3)
      this.w_DESRIS = space(40)
      this.w_QTARIS = 0
      this.w_RLINTEST = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LRCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LRUMTRIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LRUMTRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_LRUMTRIS)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_LRUMTRIS))
          select UMFLTEMP,UMCODICE,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LRUMTRIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LRUMTRIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oLRUMTRIS_2_11'),i_cWhere,'GSAR_AUM',"Unit� di misura tempo",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LRUMTRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_LRUMTRIS);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_LRUMTRIS)
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LRUMTRIS = NVL(_Link_.UMCODICE,space(3))
      this.w_CONSEC = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_LRUMTRIS = space(3)
      endif
      this.w_CONSEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CONSEC > 0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endif
        this.w_LRUMTRIS = space(3)
        this.w_CONSEC = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LRUMTRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oNUMFAS_1_7.value==this.w_NUMFAS)
      this.oPgFrm.Page1.oPag.oNUMFAS_1_7.value=this.w_NUMFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAS_1_8.value==this.w_DESFAS)
      this.oPgFrm.Page1.oPag.oDESFAS_1_8.value=this.w_DESFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSEC_2_12.value==this.w_CONSEC)
      this.oPgFrm.Page1.oPag.oCONSEC_2_12.value=this.w_CONSEC
      replace t_CONSEC with this.oPgFrm.Page1.oPag.oCONSEC_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLRNUMIMP_2_14.value==this.w_LRNUMIMP)
      this.oPgFrm.Page1.oPag.oLRNUMIMP_2_14.value=this.w_LRNUMIMP
      replace t_LRNUMIMP with this.oPgFrm.Page1.oPag.oLRNUMIMP_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLRTMPCIC_2_15.value==this.w_LRTMPCIC)
      this.oPgFrm.Page1.oPag.oLRTMPCIC_2_15.value=this.w_LRTMPCIC
      replace t_LRTMPCIC with this.oPgFrm.Page1.oPag.oLRTMPCIC_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLRTMPSEC_2_16.value==this.w_LRTMPSEC)
      this.oPgFrm.Page1.oPag.oLRTMPSEC_2_16.value=this.w_LRTMPSEC
      replace t_LRTMPSEC with this.oPgFrm.Page1.oPag.oLRTMPSEC_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLRPROORA_2_17.value==this.w_LRPROORA)
      this.oPgFrm.Page1.oPag.oLRPROORA_2_17.value=this.w_LRPROORA
      replace t_LRPROORA with this.oPgFrm.Page1.oPag.oLRPROORA_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRLINTEST_2_21.RadioValue()==this.w_RLINTEST)
      this.oPgFrm.Page1.oPag.oRLINTEST_2_21.SetRadio()
      replace t_RLINTEST with this.oPgFrm.Page1.oPag.oRLINTEST_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_2_23.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_2_23.value=this.w_DESCLA
      replace t_DESCLA with this.oPgFrm.Page1.oPag.oDESCLA_2_23.value
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMFAS_4_3.value==this.w_NUMFAS)
      this.oPgFrm.Page2.oPag.oNUMFAS_4_3.value=this.w_NUMFAS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAS_4_4.value==this.w_DESFAS)
      this.oPgFrm.Page2.oPag.oDESFAS_4_4.value=this.w_DESFAS
    endif
    if not(this.oPgFrm.Page4.oPag.oLRISTLAV_6_1.value==this.w_LRISTLAV)
      this.oPgFrm.Page4.oPag.oLRISTLAV_6_1.value=this.w_LRISTLAV
    endif
    if not(this.oPgFrm.Page3.oPag.oNUMFAS_5_3.value==this.w_NUMFAS)
      this.oPgFrm.Page3.oPag.oNUMFAS_5_3.value=this.w_NUMFAS
    endif
    if not(this.oPgFrm.Page3.oPag.oDESFAS_5_4.value==this.w_DESFAS)
      this.oPgFrm.Page3.oPag.oDESFAS_5_4.value=this.w_DESFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODCLA_2_2.value==this.w_LRCODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODCLA_2_2.value=this.w_LRCODCLA
      replace t_LRCODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODCLA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.RadioValue()==this.w_LR__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.SetRadio()
      replace t_LR__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODRIS_2_6.value==this.w_LRCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODRIS_2_6.value=this.w_LRCODRIS
      replace t_LRCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRCODRIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.RadioValue()==this.w_LRTIPTEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.SetRadio()
      replace t_LRTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRQTARIS_2_10.value==this.w_LRQTARIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRQTARIS_2_10.value=this.w_LRQTARIS
      replace t_LRQTARIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRQTARIS_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRUMTRIS_2_11.value==this.w_LRUMTRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRUMTRIS_2_11.value=this.w_LRUMTRIS
      replace t_LRUMTRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRUMTRIS_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTEMRIS_2_13.value==this.w_LRTEMRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTEMRIS_2_13.value=this.w_LRTEMRIS
      replace t_LRTEMRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTEMRIS_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_20.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_20.value=this.w_DESRIS
      replace t_DESRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESRIS_2_20.value
    endif
    cp_SetControlsValueExtFlds(this,'RIS_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MMI.CheckForm()
      if i_bres
        i_bres=  .GSCI_MMI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCI_MMO.CheckForm()
      if i_bres
        i_bres=  .GSCI_MMO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_LRQTARIS<=.w_QTARIS) and (.w_CPROWORD<>0 and !Empty(.w_LR__TIPO) and !Empty(.w_LRCODRIS) and .w_LRTMPSEC>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRQTARIS_2_10
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_CONSEC > 0) and (.w_LRQTARIS>0) and not(empty(.w_LRUMTRIS)) and (.w_CPROWORD<>0 and !Empty(.w_LR__TIPO) and !Empty(.w_LRCODRIS) and .w_LRTMPSEC>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRUMTRIS_2_11
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
      endcase
      if .w_CPROWORD<>0 and !Empty(.w_LR__TIPO) and !Empty(.w_LRCODRIS) and .w_LRTMPSEC>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LRCODCLA = this.w_LRCODCLA
    this.o_LRCODRIS = this.w_LRCODRIS
    this.o_LRUMTRIS = this.w_LRUMTRIS
    this.o_LRTEMRIS = this.w_LRTEMRIS
    this.o_LRNUMIMP = this.w_LRNUMIMP
    this.o_LRTMPCIC = this.w_LRTMPCIC
    * --- GSCI_MMI : Depends On
    this.GSCI_MMI.SaveDependsOn()
    * --- GSCI_MMO : Depends On
    this.GSCI_MMO.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and !Empty(t_LR__TIPO) and !Empty(t_LRCODRIS) and t_LRTMPSEC>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_LRCODCLA=space(5)
      .w_LRCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_LR__TIPO=space(2)
      .w_LRCODRIS=space(20)
      .w_LRTIPTEM=space(1)
      .w_QTARIS=0
      .w_UMTDEF=space(3)
      .w_LRQTARIS=0
      .w_LRUMTRIS=space(3)
      .w_CONSEC=0
      .w_LRTEMRIS=0
      .w_LRNUMIMP=0
      .w_LRTMPCIC=0
      .w_LRTMPSEC=0
      .w_LRPROORA=0
      .w_DESRIS=space(40)
      .w_RLINTEST=space(1)
      .w_DESCLA=space(40)
      .w_LRTEMSEC=0
      .w_LR1TIPO=space(2)
      .DoRTCalc(1,10,.f.)
      if not(empty(.w_LRCODCLA))
        .link_2_2('Full')
      endif
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_LRCODCLA))
        .link_2_3('Full')
      endif
      .DoRTCalc(12,12,.f.)
        .w_LR__TIPO = 'CL'
        .w_LRCODRIS = iif(empty(.w_CLARIS),.w_LRCODRIS,.w_CLARIS)
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_LRCODRIS))
        .link_2_6('Full')
      endif
        .w_LRTIPTEM = 'L'
      .DoRTCalc(16,19,.f.)
      if not(empty(.w_LRUMTRIS))
        .link_2_11('Full')
      endif
      .DoRTCalc(20,20,.f.)
        .w_LRTEMRIS = iif(.w_LRTMPCIC > 0 , .w_LRTMPCIC / .w_LRNUMIMP , 0)
        .w_LRNUMIMP = 1
        .w_LRTMPCIC = iif(.w_LRTEMRIS>0 , .w_LRTEMRIS * .w_LRNUMIMP, 0)
        .w_LRTMPSEC = .w_LRTMPCIC*.w_CONSEC
        .w_LRPROORA = iif(.w_LRTMPSEC=0 and .w_LRTMPSEC=0 , 0, 3600/IIF(.w_LRTMPSEC<>0 , .w_LRTMPSEC, .w_LRTMPSEC)) *.w_LRNUMIMP
      .DoRTCalc(26,34,.f.)
        .w_LRTEMSEC = .w_LRTEMRIS*.w_CONSEC
        .w_LR1TIPO = .w_LR__TIPO
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_LRCODCLA = t_LRCODCLA
    this.w_LRCODCLA = t_LRCODCLA
    this.w_CLARIS = t_CLARIS
    this.w_LR__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.RadioValue(.t.)
    this.w_LRCODRIS = t_LRCODRIS
    this.w_LRTIPTEM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.RadioValue(.t.)
    this.w_QTARIS = t_QTARIS
    this.w_UMTDEF = t_UMTDEF
    this.w_LRQTARIS = t_LRQTARIS
    this.w_LRUMTRIS = t_LRUMTRIS
    this.w_CONSEC = t_CONSEC
    this.w_LRTEMRIS = t_LRTEMRIS
    this.w_LRNUMIMP = t_LRNUMIMP
    this.w_LRTMPCIC = t_LRTMPCIC
    this.w_LRTMPSEC = t_LRTMPSEC
    this.w_LRPROORA = t_LRPROORA
    this.w_DESRIS = t_DESRIS
    this.w_RLINTEST = this.oPgFrm.Page1.oPag.oRLINTEST_2_21.RadioValue(.t.)
    this.w_DESCLA = t_DESCLA
    this.w_LRTEMSEC = t_LRTEMSEC
    this.w_LR1TIPO = t_LR1TIPO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_LRCODCLA with this.w_LRCODCLA
    replace t_LRCODCLA with this.w_LRCODCLA
    replace t_CLARIS with this.w_CLARIS
    replace t_LR__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLR__TIPO_2_5.ToRadio()
    replace t_LRCODRIS with this.w_LRCODRIS
    replace t_LRTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLRTIPTEM_2_7.ToRadio()
    replace t_QTARIS with this.w_QTARIS
    replace t_UMTDEF with this.w_UMTDEF
    replace t_LRQTARIS with this.w_LRQTARIS
    replace t_LRUMTRIS with this.w_LRUMTRIS
    replace t_CONSEC with this.w_CONSEC
    replace t_LRTEMRIS with this.w_LRTEMRIS
    replace t_LRNUMIMP with this.w_LRNUMIMP
    replace t_LRTMPCIC with this.w_LRTMPCIC
    replace t_LRTMPSEC with this.w_LRTMPSEC
    replace t_LRPROORA with this.w_LRPROORA
    replace t_DESRIS with this.w_DESRIS
    replace t_RLINTEST with this.oPgFrm.Page1.oPag.oRLINTEST_2_21.ToRadio()
    replace t_DESCLA with this.w_DESCLA
    replace t_LRTEMSEC with this.w_LRTEMSEC
    replace t_LR1TIPO with this.w_LR1TIPO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mlrPag1 as StdContainer
  Width  = 909
  height = 215
  stdWidth  = 909
  stdheight = 215
  resizeXpos=225
  resizeYpos=130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMFAS_1_7 as StdField with uid="IOBVWUDSPG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMFAS", cQueryName = "NUMFAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fase selezionata",;
    HelpContextID = 91151146,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=125, Top=6, cSayPict='"99999"', cGetPict='"99999"'

  add object oDESFAS_1_8 as StdField with uid="DEKMYCBLTV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESFAS", cQueryName = "DESFAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 91130826,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=174, Top=6, InputMask=replicate('X',40)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=31, width=899,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Posiz.",Field2="LRCODCLA",Label2="Classe",Field3="LR__TIPO",Label3="Tipo Risorsa",Field4="LRCODRIS",Label4="Risorsa",Field5="DESRIS",Label5="Descrizione",Field6="LRTIPTEM",Label6="Tipologia",Field7="LRQTARIS",Label7="Q.t�",Field8="LRUMTRIS",Label8="U.M.",Field9="LRTEMRIS",Label9="Tempo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101524602

  add object oStr_1_6 as StdString with uid="LFRVRENAKN",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=115, Height=15,;
    Caption="Fase selezionata:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=50,;
    width=894+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3100000000000001)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=51,width=893+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3100000000000001)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CLR_DETT|RIS_ORSE|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCONSEC_2_12.Refresh()
      this.Parent.oLRNUMIMP_2_14.Refresh()
      this.Parent.oLRTMPCIC_2_15.Refresh()
      this.Parent.oLRTMPSEC_2_16.Refresh()
      this.Parent.oLRPROORA_2_17.Refresh()
      this.Parent.oRLINTEST_2_21.Refresh()
      this.Parent.oDESCLA_2_23.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CLR_DETT'
        oDropInto=this.oBodyCol.oRow.oLRCODCLA_2_2
      case cFile='RIS_ORSE'
        oDropInto=this.oBodyCol.oRow.oLRCODRIS_2_6
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oLRUMTRIS_2_11
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCONSEC_2_12 as StdTrsField with uid="MGMAKWWPDJ",rtseq=20,rtrep=.t.,;
    cFormVar="w_CONSEC",value=0,enabled=.f.,;
    ToolTipText = "Coeff. conversione",;
    HelpContextID = 86102490,;
    cTotal="", bFixedPos=.t., cQueryName = "CONSEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=90, Top=193, cSayPict=["99999999.99999"], cGetPict=["99999999.99999"]

  add object oLRNUMIMP_2_14 as StdTrsField with uid="UJDMQNUYIR",rtseq=22,rtrep=.t.,;
    cFormVar="w_LRNUMIMP",value=0,;
    ToolTipText = "Numero impronte dell'attrezzatura",;
    HelpContextID = 245353978,;
    cTotal="", bFixedPos=.t., cQueryName = "LRNUMIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=539, Top=193, cSayPict=["@Z 9999"], cGetPict=["9999"]

  add object oLRTMPCIC_2_15 as StdTrsField with uid="RGDDLKHAPS",rtseq=23,rtrep=.t.,;
    cFormVar="w_LRTMPCIC",value=0,;
    ToolTipText = "Tempo ciclo totale (per tutte le impronte)",;
    HelpContextID = 193499641,;
    cTotal="", bFixedPos=.t., cQueryName = "LRTMPCIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=581, Top=193

  add object oLRTMPSEC_2_16 as StdTrsField with uid="EJJUPKTXVI",rtseq=24,rtrep=.t.,;
    cFormVar="w_LRTMPSEC",value=0,enabled=.f.,;
    ToolTipText = "Tempo espresso in secondi",;
    HelpContextID = 193499641,;
    cTotal="", bFixedPos=.t., cQueryName = "LRTMPSEC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=289, Top=193, cSayPict=["9999999999999.9999"], cGetPict=["9999999999999.9999"]

  add object oLRPROORA_2_17 as StdTrsField with uid="NUQRJHKBPH",rtseq=25,rtrep=.t.,;
    cFormVar="w_LRPROORA",value=0,enabled=.f.,;
    ToolTipText = "Produzione oraria della risorsa",;
    HelpContextID = 125653495,;
    cTotal="", bFixedPos=.t., cQueryName = "LRPROORA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=793, Top=193, cSayPict=["@Z 9999999999.9999"], cGetPict=["@Z 9999999999.9999"]

  add object oRLINTEST_2_21 as StdTrsCheck with uid="PDUQVLZNNN",rtrep=.t.,;
    cFormVar="w_RLINTEST", enabled=.f., caption="Centro esterno",;
    HelpContextID = 231267434,;
    Left=539, Top=167,;
    cTotal="", cQueryName = "RLINTEST",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oRLINTEST_2_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RLINTEST,&i_cF..t_RLINTEST),this.value)
    return(iif(xVal =1,'E',;
    'I'))
  endfunc
  func oRLINTEST_2_21.GetRadio()
    this.Parent.oContained.w_RLINTEST = this.RadioValue()
    return .t.
  endfunc

  func oRLINTEST_2_21.ToRadio()
    this.Parent.oContained.w_RLINTEST=trim(this.Parent.oContained.w_RLINTEST)
    return(;
      iif(this.Parent.oContained.w_RLINTEST=='E',1,;
      0))
  endfunc

  func oRLINTEST_2_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESCLA_2_23 as StdTrsField with uid="VMHSJHRMME",rtseq=28,rtrep=.t.,;
    cFormVar="w_DESCLA",value=space(40),enabled=.f.,;
    HelpContextID = 113347530,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCLA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=90, Top=169, InputMask=replicate('X',40)

  add object oStr_2_18 as StdString with uid="UGWAJPCAWG",Visible=.t., Left=453, Top=195,;
    Alignment=1, Width=85, Height=18,;
    Caption="Num. impronte:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="LJNEHBYSJT",Visible=.t., Left=696, Top=195,;
    Alignment=1, Width=96, Height=18,;
    Caption="Prod. oraria:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="LLBIGPPLSH",Visible=.t., Left=31, Top=171,;
    Alignment=1, Width=58, Height=18,;
    Caption="Classe:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="FGOCLMCRBX",Visible=.t., Left=4, Top=195,;
    Alignment=1, Width=85, Height=18,;
    Caption="Conv. Sec.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="XICKNAMHRU",Visible=.t., Left=205, Top=195,;
    Alignment=1, Width=81, Height=18,;
    Caption="Secondi:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsci_mlrPag2 as StdContainer
    Width  = 909
    height = 215
    stdWidth  = 909
    stdheight = 215
  resizeXpos=243
  resizeYpos=155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="WRDJUGIHDV",left=0, top=32, width=806, height=159, bOnScreen=.t.;


  add object oNUMFAS_4_3 as StdField with uid="EXWYNCYBKU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NUMFAS", cQueryName = "NUMFAS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fase selezionata",;
    HelpContextID = 91151146,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=127, Top=6, cSayPict='"99999"', cGetPict='"99999"'

  add object oDESFAS_4_4 as StdField with uid="TUDQOLSZEE",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESFAS", cQueryName = "DESFAS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 91130826,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=176, Top=6, InputMask=replicate('X',40)


  add object oBtn_4_5 as StdButton with uid="MLEQPFNYXG",left=925, top=-49, width=48,height=45,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per importare materiali";
    , HelpContextID = 63088578;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      do gsci_klr with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_5.mCond()
    with this.Parent.oContained
      return ((.cFunction='Load' OR .cFunction='Edit'))
    endwith
  endfunc

  func oBtn_4_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Query' and .t.)
    endwith
   endif
  endfunc

  add object oStr_4_2 as StdString with uid="PSEUJLVHEB",Visible=.t., Left=6, Top=6,;
    Alignment=1, Width=115, Height=15,;
    Caption="Fase selezionata:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mmi",lower(this.oContained.GSCI_MMI.class))=0
        this.oContained.GSCI_MMI.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsci_mlrPag3 as StdContainer
    Width  = 909
    height = 215
    stdWidth  = 909
    stdheight = 215
  resizeXpos=425
  resizeYpos=152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="XPRWECGRBA",left=6, top=31, width=800, height=160, bOnScreen=.t.;


  add object oNUMFAS_5_3 as StdField with uid="SOEKMWDVHX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NUMFAS", cQueryName = "NUMFAS",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fase selezionata",;
    HelpContextID = 91151146,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=127, Top=6, cSayPict='"99999"', cGetPict='"99999"'

  add object oDESFAS_5_4 as StdField with uid="EBFEFWYSLG",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESFAS", cQueryName = "DESFAS",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 91130826,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=176, Top=6, InputMask=replicate('X',40)

  add object oStr_5_2 as StdString with uid="SLCMBBTUZO",Visible=.t., Left=6, Top=6,;
    Alignment=1, Width=115, Height=15,;
    Caption="Fase selezionata:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsci_mlrPag4 as StdContainer
    Width  = 909
    height = 215
    stdWidth  = 909
    stdheight = 215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLRISTLAV_6_1 as StdMemo with uid="ZDGZZKXUTD",rtseq=31,rtrep=.f.,;
    cFormVar = "w_LRISTLAV", cQueryName = "LRISTLAV",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Istruzioni di lavorazione della fase",;
    HelpContextID = 187833844,;
   bGlobalFont=.t.,;
    Height=189, Width=909, Left=1, Top=4
enddefine

* --- Defining Body row
define class tgsci_mlrBodyRow as CPBodyRowCnt
  Width=884
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3100000000000001)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="LFTPDXGZZN",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Posizione",;
    HelpContextID = 133853034,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oLRCODCLA_2_2 as StdTrsField with uid="MRBTJIDYNA",rtseq=10,rtrep=.t.,;
    cFormVar="w_LRCODCLA",value=space(5),;
    ToolTipText = "Classe risorsa",;
    HelpContextID = 87457289,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=42, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_DETT", oKey_1_1="CSFLAPRE", oKey_1_2="this.w_CSFLAPRE", oKey_2_1="CSCODCLA", oKey_2_2="this.w_LRCODCLA"

  func oLRCODCLA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oLRCODCLA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLRCODCLA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLR_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStrODBC(this.Parent.oContained.w_CSFLAPRE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStr(this.Parent.oContained.w_CSFLAPRE)
    endif
    do cp_zoom with 'CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(this.parent,'oLRCODCLA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_MCS.CLR_DETT_VZM',this.parent.oContained
  endproc

  add object oLR__TIPO_2_5 as stdTrsTableCombo with uid="OAVZYSZPBF",rtrep=.t.,;
    cFormVar="w_LR__TIPO", tablefilter="" , ;
    ToolTipText = "Tipologia risorsa",;
    HelpContextID = 237288955,;
    Height=22, Width=118, Left=107, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Seleziona il tipo';
, bIsInHeader=.f.;
    , cTable='..\PRFA\EXE\QUERY\GSCI_QLT.vqr',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.



  func oLR__TIPO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_LRCODRIS)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oLRCODRIS_2_6 as StdTrsField with uid="XFHUUICCDL",rtseq=14,rtrep=.t.,;
    cFormVar="w_LRCODRIS",value=space(20),;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 164200969,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=227, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_LR__TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_LRCODRIS"

  func oLRCODRIS_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLRCODRIS_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLRCODRIS_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_LR__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_LR__TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oLRCODRIS_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZLR.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oLRTIPTEM_2_7 as StdTrsCombo with uid="FYILZRKTDY",rtrep=.t.,;
    cFormVar="w_LRTIPTEM", RowSource=""+"Lavorazione,"+"Setup,"+"Warm Up" , ;
    ToolTipText = "Tipo di tempo della risorsa",;
    HelpContextID = 210014723,;
    Height=22, Width=116, Left=598, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oLRTIPTEM_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LRTIPTEM,&i_cF..t_LRTIPTEM),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'S',;
    iif(xVal =3,'W',;
    space(1)))))
  endfunc
  func oLRTIPTEM_2_7.GetRadio()
    this.Parent.oContained.w_LRTIPTEM = this.RadioValue()
    return .t.
  endfunc

  func oLRTIPTEM_2_7.ToRadio()
    this.Parent.oContained.w_LRTIPTEM=trim(this.Parent.oContained.w_LRTIPTEM)
    return(;
      iif(this.Parent.oContained.w_LRTIPTEM=='L',1,;
      iif(this.Parent.oContained.w_LRTIPTEM=='S',2,;
      iif(this.Parent.oContained.w_LRTIPTEM=='W',3,;
      0))))
  endfunc

  func oLRTIPTEM_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLRQTARIS_2_10 as StdTrsField with uid="CKHONRDQHA",rtseq=18,rtrep=.t.,;
    cFormVar="w_LRQTARIS",value=0,;
    ToolTipText = "Quantita risorsa",;
    HelpContextID = 161440265,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=33, Left=719, Top=0, cSayPict=["@Z 999.99"], cGetPict=["99999.99"]

  func oLRQTARIS_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LRQTARIS<=.w_QTARIS)
    endwith
    return bRes
  endfunc

  add object oLRUMTRIS_2_11 as StdTrsField with uid="XXRDIHSZJN",rtseq=19,rtrep=.t.,;
    cFormVar="w_LRUMTRIS",value=space(3),;
    ToolTipText = "UM tempi",;
    HelpContextID = 180920841,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura non gestita a tempo oppure senza conversione in secondi",;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=36, Left=758, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_LRUMTRIS"

  proc oLRUMTRIS_2_11.mDefault
    with this.Parent.oContained
      if empty(.w_LRUMTRIS)
        .w_LRUMTRIS = .w_UMTDEF
      endif
    endwith
  endproc

  func oLRUMTRIS_2_11.mCond()
    with this.Parent.oContained
      return (.w_LRQTARIS>0)
    endwith
  endfunc

  func oLRUMTRIS_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oLRUMTRIS_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLRUMTRIS_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oLRUMTRIS_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura tempo",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oLRUMTRIS_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_LRUMTRIS
    i_obj.ecpSave()
  endproc

  add object oLRTEMRIS_2_13 as StdTrsField with uid="DSSDNOXEGI",rtseq=21,rtrep=.t.,;
    cFormVar="w_LRTEMRIS",value=0,;
    ToolTipText = "Tempo risorsa",;
    HelpContextID = 173052425,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=800, Top=0, cSayPict=["@Z 99,999.999"], cGetPict=["@Z 99999.999"]

  func oLRTEMRIS_2_13.mCond()
    with this.Parent.oContained
      return (.w_LRQTARIS>0 and not empty(.w_LRUMTRIS))
    endwith
  endfunc

  add object oDESRIS_2_20 as StdTrsField with uid="RURJNZQHGC",rtseq=26,rtrep=.t.,;
    cFormVar="w_DESRIS",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione risorsa",;
    HelpContextID = 81955786,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=209, Left=384, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_mlr','RIS_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LRSERIAL=RIS_MAST.LRSERIAL";
  +" and "+i_cAliasName2+".LRROWNUM=RIS_MAST.LRROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
