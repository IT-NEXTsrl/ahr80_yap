* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_acv                                                        *
*              Causali versamento                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-17                                                      *
* Last revis.: 2015-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsci_acv"))

* --- Class definition
define class tgsci_acv as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 707
  Height = 237+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-16"
  HelpContextID=109708649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  CAU_VERS_IDX = 0
  CAM_AGAZ_IDX = 0
  TIP_DOCU_IDX = 0
  UNIMIS_IDX = 0
  cFile = "CAU_VERS"
  cKeySelect = "CVCODICE"
  cKeyWhere  = "CVCODICE=this.w_CVCODICE"
  cKeyWhereODBC = '"CVCODICE="+cp_ToStrODBC(this.w_CVCODICE)';

  cKeyWhereODBCqualified = '"CAU_VERS.CVCODICE="+cp_ToStrODBC(this.w_CVCODICE)';

  cPrg = "gsci_acv"
  cComment = "Causali versamento"
  icon = "anag.ico"
  cAutoZoom = 'GSCI_ACV'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CVCODICE = space(5)
  w_CVFLDEFA = space(1)
  w_CVTATTIV = space(1)
  o_CVTATTIV = space(1)
  w_CVFLPBSC = space(1)
  w_CVCAUCAR = space(5)
  w_CVCAUSCA = space(5)
  w_CVCAUSER = space(5)
  w_UMFLTEMP = space(1)
  w_CVUMTEMP = space(3)
  w_DESCVAR = space(30)
  w_DESCSCA = space(30)
  w_DESCTMP = space(30)
  w_CVDESCRI = space(40)
  w_CAUCAR = space(5)
  w_CAUSCA = space(5)
  w_CHKCSCA = space(1)
  w_CHKCCAR = space(1)
  w_DTEMDEF = space(40)
  w_CVCAUMOU = space(5)
  w_DESCMOU = space(30)
  w_CAUMOU = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_VERS','gsci_acv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_acvPag1","gsci_acv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causali")
      .Pages(1).HelpContextID = 67063770
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCVCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='CAU_VERS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_VERS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_VERS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CVCODICE = NVL(CVCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_VERS where CVCODICE=KeySet.CVCODICE
    *
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_VERS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_VERS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_VERS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CVCODICE',this.w_CVCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_UMFLTEMP = 'S'
        .w_DESCVAR = space(30)
        .w_DESCSCA = space(30)
        .w_DESCTMP = space(30)
        .w_CAUCAR = space(5)
        .w_CAUSCA = space(5)
        .w_CHKCSCA = space(1)
        .w_CHKCCAR = space(1)
        .w_DTEMDEF = space(40)
        .w_DESCMOU = space(30)
        .w_CAUMOU = space(5)
        .w_CVCODICE = NVL(CVCODICE,space(5))
        .w_CVFLDEFA = NVL(CVFLDEFA,space(1))
        .w_CVTATTIV = NVL(CVTATTIV,space(1))
        .w_CVFLPBSC = NVL(CVFLPBSC,space(1))
        .w_CVCAUCAR = NVL(CVCAUCAR,space(5))
          .link_1_9('Load')
        .w_CVCAUSCA = NVL(CVCAUSCA,space(5))
          .link_1_10('Load')
        .w_CVCAUSER = NVL(CVCAUSER,space(5))
          .link_1_11('Load')
        .w_CVUMTEMP = NVL(CVUMTEMP,space(3))
          .link_1_13('Load')
        .w_CVDESCRI = NVL(CVDESCRI,space(40))
        .w_CVCAUMOU = NVL(CVCAUMOU,space(5))
          .link_1_27('Load')
        cp_LoadRecExtFlds(this,'CAU_VERS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CVCODICE = space(5)
      .w_CVFLDEFA = space(1)
      .w_CVTATTIV = space(1)
      .w_CVFLPBSC = space(1)
      .w_CVCAUCAR = space(5)
      .w_CVCAUSCA = space(5)
      .w_CVCAUSER = space(5)
      .w_UMFLTEMP = space(1)
      .w_CVUMTEMP = space(3)
      .w_DESCVAR = space(30)
      .w_DESCSCA = space(30)
      .w_DESCTMP = space(30)
      .w_CVDESCRI = space(40)
      .w_CAUCAR = space(5)
      .w_CAUSCA = space(5)
      .w_CHKCSCA = space(1)
      .w_CHKCCAR = space(1)
      .w_DTEMDEF = space(40)
      .w_CVCAUMOU = space(5)
      .w_DESCMOU = space(30)
      .w_CAUMOU = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CVTATTIV = 'E'
        .w_CVFLPBSC = iif(.w_CVTATTIV="T","N",iif(not empty(.w_CVFLPBSC),.w_CVFLPBSC,"E"))
        .w_CVCAUCAR = iif(.w_CVTATTIV='T',space(5),.w_CVCAUCAR)
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CVCAUCAR))
          .link_1_9('Full')
          endif
        .w_CVCAUSCA = iif(.w_CVTATTIV='T',space(5),.w_CVCAUSCA)
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CVCAUSCA))
          .link_1_10('Full')
          endif
        .w_CVCAUSER = iif(.w_CVTATTIV='Q',space(5),.w_CVCAUSER)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CVCAUSER))
          .link_1_11('Full')
          endif
        .w_UMFLTEMP = 'S'
        .w_CVUMTEMP = iif(.w_CVTATTIV='Q',space(3),.w_CVUMTEMP)
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CVUMTEMP))
          .link_1_13('Full')
          endif
          .DoRTCalc(10,18,.f.)
        .w_CVCAUMOU = iif(.w_CVTATTIV='T',space(5),.w_CVCAUMOU)
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_CVCAUMOU))
          .link_1_27('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_VERS')
    this.DoRTCalc(20,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCVCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCVFLDEFA_1_4.enabled = i_bVal
      .Page1.oPag.oCVTATTIV_1_5.enabled = i_bVal
      .Page1.oPag.oCVFLPBSC_1_6.enabled = i_bVal
      .Page1.oPag.oCVCAUCAR_1_9.enabled = i_bVal
      .Page1.oPag.oCVCAUSCA_1_10.enabled = i_bVal
      .Page1.oPag.oCVCAUSER_1_11.enabled = i_bVal
      .Page1.oPag.oCVUMTEMP_1_13.enabled = i_bVal
      .Page1.oPag.oCVDESCRI_1_21.enabled = i_bVal
      .Page1.oPag.oCVCAUMOU_1_27.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCVCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCVCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAU_VERS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCODICE,"CVCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVFLDEFA,"CVFLDEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVTATTIV,"CVTATTIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVFLPBSC,"CVFLPBSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCAUCAR,"CVCAUCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCAUSCA,"CVCAUSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCAUSER,"CVCAUSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVUMTEMP,"CVUMTEMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVDESCRI,"CVDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCAUMOU,"CVCAUMOU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
    i_lTable = "CAU_VERS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_VERS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_VERS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_VERS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_VERS')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_VERS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CVCODICE,CVFLDEFA,CVTATTIV,CVFLPBSC,CVCAUCAR"+;
                  ",CVCAUSCA,CVCAUSER,CVUMTEMP,CVDESCRI,CVCAUMOU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CVCODICE)+;
                  ","+cp_ToStrODBC(this.w_CVFLDEFA)+;
                  ","+cp_ToStrODBC(this.w_CVTATTIV)+;
                  ","+cp_ToStrODBC(this.w_CVFLPBSC)+;
                  ","+cp_ToStrODBCNull(this.w_CVCAUCAR)+;
                  ","+cp_ToStrODBCNull(this.w_CVCAUSCA)+;
                  ","+cp_ToStrODBCNull(this.w_CVCAUSER)+;
                  ","+cp_ToStrODBCNull(this.w_CVUMTEMP)+;
                  ","+cp_ToStrODBC(this.w_CVDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_CVCAUMOU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_VERS')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_VERS')
        cp_CheckDeletedKey(i_cTable,0,'CVCODICE',this.w_CVCODICE)
        INSERT INTO (i_cTable);
              (CVCODICE,CVFLDEFA,CVTATTIV,CVFLPBSC,CVCAUCAR,CVCAUSCA,CVCAUSER,CVUMTEMP,CVDESCRI,CVCAUMOU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CVCODICE;
                  ,this.w_CVFLDEFA;
                  ,this.w_CVTATTIV;
                  ,this.w_CVFLPBSC;
                  ,this.w_CVCAUCAR;
                  ,this.w_CVCAUSCA;
                  ,this.w_CVCAUSER;
                  ,this.w_CVUMTEMP;
                  ,this.w_CVDESCRI;
                  ,this.w_CVCAUMOU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_VERS_IDX,i_nConn)
      *
      * update CAU_VERS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_VERS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CVFLDEFA="+cp_ToStrODBC(this.w_CVFLDEFA)+;
             ",CVTATTIV="+cp_ToStrODBC(this.w_CVTATTIV)+;
             ",CVFLPBSC="+cp_ToStrODBC(this.w_CVFLPBSC)+;
             ",CVCAUCAR="+cp_ToStrODBCNull(this.w_CVCAUCAR)+;
             ",CVCAUSCA="+cp_ToStrODBCNull(this.w_CVCAUSCA)+;
             ",CVCAUSER="+cp_ToStrODBCNull(this.w_CVCAUSER)+;
             ",CVUMTEMP="+cp_ToStrODBCNull(this.w_CVUMTEMP)+;
             ",CVDESCRI="+cp_ToStrODBC(this.w_CVDESCRI)+;
             ",CVCAUMOU="+cp_ToStrODBCNull(this.w_CVCAUMOU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_VERS')
        i_cWhere = cp_PKFox(i_cTable  ,'CVCODICE',this.w_CVCODICE  )
        UPDATE (i_cTable) SET;
              CVFLDEFA=this.w_CVFLDEFA;
             ,CVTATTIV=this.w_CVTATTIV;
             ,CVFLPBSC=this.w_CVFLPBSC;
             ,CVCAUCAR=this.w_CVCAUCAR;
             ,CVCAUSCA=this.w_CVCAUSCA;
             ,CVCAUSER=this.w_CVCAUSER;
             ,CVUMTEMP=this.w_CVUMTEMP;
             ,CVDESCRI=this.w_CVDESCRI;
             ,CVCAUMOU=this.w_CVCAUMOU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsci_acv
    if not(bTrsErr)
      with this
        if .bUpdated
          do case
            case .w_CVFLDEFA='S' AND (.cFunction='Load' or .cFunction='Edit')
            vq_exec("..\PRFA\EXE\QUERY\GSCI_ACV", this, "PREDEF")
            if used("PREDEF") 
              if reccount("PREDEF")>1
                bTrsErr=.t.
                .bTrsDontDisplayErr=.t.
               i_TrsMsg=Ah_MsgFormat("Non pu� esistere pi� di una causale predefinita!")
               ah_ErrorMsg(i_TrsMsg,,'')
              endif
              use in select("predef")
            endif
          endcase
        endif
      endwith
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_VERS_IDX,i_nConn)
      *
      * delete CAU_VERS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CVCODICE',this.w_CVCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_VERS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_VERS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVFLPBSC = iif(.w_CVTATTIV="T","N",iif(not empty(.w_CVFLPBSC),.w_CVFLPBSC,"E"))
        endif
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVCAUCAR = iif(.w_CVTATTIV='T',space(5),.w_CVCAUCAR)
          .link_1_9('Full')
        endif
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVCAUSCA = iif(.w_CVTATTIV='T',space(5),.w_CVCAUSCA)
          .link_1_10('Full')
        endif
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVCAUSER = iif(.w_CVTATTIV='Q',space(5),.w_CVCAUSER)
          .link_1_11('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVUMTEMP = iif(.w_CVTATTIV='Q',space(3),.w_CVUMTEMP)
          .link_1_13('Full')
        endif
        .DoRTCalc(10,18,.t.)
        if .o_CVTATTIV<>.w_CVTATTIV
            .w_CVCAUMOU = iif(.w_CVTATTIV='T',space(5),.w_CVCAUMOU)
          .link_1_27('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCVTATTIV_1_5.enabled = this.oPgFrm.Page1.oPag.oCVTATTIV_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCVFLPBSC_1_6.enabled = this.oPgFrm.Page1.oPag.oCVFLPBSC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCVCAUCAR_1_9.enabled = this.oPgFrm.Page1.oPag.oCVCAUCAR_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCVCAUSCA_1_10.enabled = this.oPgFrm.Page1.oPag.oCVCAUSCA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCVCAUSER_1_11.enabled = this.oPgFrm.Page1.oPag.oCVCAUSER_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCVUMTEMP_1_13.enabled = this.oPgFrm.Page1.oPag.oCVUMTEMP_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCVCAUMOU_1_27.enabled = this.oPgFrm.Page1.oPag.oCVCAUMOU_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CVCAUCAR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CVCAUCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CVCAUCAR))
          select TDTIPDOC,TDDESDOC,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCAUCAR)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCAUCAR) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCVCAUCAR_1_9'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCI_ZCC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CVCAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CVCAUCAR)
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCAUCAR = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCVAR = NVL(_Link_.TDDESDOC,space(30))
      this.w_CAUCAR = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CVCAUCAR = space(5)
      endif
      this.w_DESCVAR = space(30)
      this.w_CAUCAR = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUCAR)='+'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire una causale di carico valida")
        endif
        this.w_CVCAUCAR = space(5)
        this.w_DESCVAR = space(30)
        this.w_CAUCAR = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CVCAUSCA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CVCAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CVCAUSCA))
          select TDTIPDOC,TDDESDOC,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCAUSCA)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCAUSCA) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCVCAUSCA_1_10'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCI_ZCS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CVCAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CVCAUSCA)
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCAUSCA = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCSCA = NVL(_Link_.TDDESDOC,space(30))
      this.w_CAUSCA = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CVCAUSCA = space(5)
      endif
      this.w_DESCSCA = space(30)
      this.w_CAUSCA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUSCA)='-'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire una causale di scarico valida")
        endif
        this.w_CVCAUSCA = space(5)
        this.w_DESCSCA = space(30)
        this.w_CAUSCA = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CVCAUSER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCAUSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CVCAUSER)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CVCAUSER))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCAUSER)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCAUSER) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCVCAUSER_1_11'),i_cWhere,'GSVE_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCAUSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CVCAUSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CVCAUSER)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCAUSER = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCTMP = NVL(_Link_.TDDESDOC,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CVCAUSER = space(5)
      endif
      this.w_DESCTMP = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCAUSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CVUMTEMP
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVUMTEMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CVUMTEMP)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_CVUMTEMP))
          select UMFLTEMP,UMCODICE,UMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVUMTEMP)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVUMTEMP) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oCVUMTEMP_1_13'),i_cWhere,'GSAR_AUM',"UM tempi",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVUMTEMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CVUMTEMP);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_CVUMTEMP)
            select UMFLTEMP,UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVUMTEMP = NVL(_Link_.UMCODICE,space(3))
      this.w_DTEMDEF = NVL(_Link_.UMDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CVUMTEMP = space(3)
      endif
      this.w_DTEMDEF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVUMTEMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CVCAUMOU
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCAUMOU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CVCAUMOU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CVCAUMOU))
          select TDTIPDOC,TDDESDOC,TDCAUMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCAUMOU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCAUMOU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCVCAUMOU_1_27'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSCI_ZCC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCAUMOU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CVCAUMOU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CVCAUMOU)
            select TDTIPDOC,TDDESDOC,TDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCAUMOU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCMOU = NVL(_Link_.TDDESDOC,space(30))
      this.w_CAUMOU = NVL(_Link_.TDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CVCAUMOU = space(5)
      endif
      this.w_DESCMOU = space(30)
      this.w_CAUMOU = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUMOU)='+'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire una causale di carico valida")
        endif
        this.w_CVCAUMOU = space(5)
        this.w_DESCMOU = space(30)
        this.w_CAUMOU = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCAUMOU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCVCODICE_1_1.value==this.w_CVCODICE)
      this.oPgFrm.Page1.oPag.oCVCODICE_1_1.value=this.w_CVCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCVFLDEFA_1_4.RadioValue()==this.w_CVFLDEFA)
      this.oPgFrm.Page1.oPag.oCVFLDEFA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCVTATTIV_1_5.RadioValue()==this.w_CVTATTIV)
      this.oPgFrm.Page1.oPag.oCVTATTIV_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCVFLPBSC_1_6.RadioValue()==this.w_CVFLPBSC)
      this.oPgFrm.Page1.oPag.oCVFLPBSC_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCAUCAR_1_9.value==this.w_CVCAUCAR)
      this.oPgFrm.Page1.oPag.oCVCAUCAR_1_9.value=this.w_CVCAUCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCAUSCA_1_10.value==this.w_CVCAUSCA)
      this.oPgFrm.Page1.oPag.oCVCAUSCA_1_10.value=this.w_CVCAUSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCAUSER_1_11.value==this.w_CVCAUSER)
      this.oPgFrm.Page1.oPag.oCVCAUSER_1_11.value=this.w_CVCAUSER
    endif
    if not(this.oPgFrm.Page1.oPag.oCVUMTEMP_1_13.value==this.w_CVUMTEMP)
      this.oPgFrm.Page1.oPag.oCVUMTEMP_1_13.value=this.w_CVUMTEMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCVAR_1_17.value==this.w_DESCVAR)
      this.oPgFrm.Page1.oPag.oDESCVAR_1_17.value=this.w_DESCVAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCSCA_1_18.value==this.w_DESCSCA)
      this.oPgFrm.Page1.oPag.oDESCSCA_1_18.value=this.w_DESCSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTMP_1_19.value==this.w_DESCTMP)
      this.oPgFrm.Page1.oPag.oDESCTMP_1_19.value=this.w_DESCTMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCVDESCRI_1_21.value==this.w_CVDESCRI)
      this.oPgFrm.Page1.oPag.oCVDESCRI_1_21.value=this.w_CVDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDTEMDEF_1_26.value==this.w_DTEMDEF)
      this.oPgFrm.Page1.oPag.oDTEMDEF_1_26.value=this.w_DTEMDEF
    endif
    if not(this.oPgFrm.Page1.oPag.oCVCAUMOU_1_27.value==this.w_CVCAUMOU)
      this.oPgFrm.Page1.oPag.oCVCAUMOU_1_27.value=this.w_CVCAUMOU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCMOU_1_29.value==this.w_DESCMOU)
      this.oPgFrm.Page1.oPag.oDESCMOU_1_29.value=this.w_DESCMOU
    endif
    cp_SetControlsValueExtFlds(this,'CAU_VERS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CVCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CVCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CVCAUCAR)) or not(looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUCAR)='+'))  and (.w_CVTATTIV<>'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCAUCAR_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CVCAUCAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una causale di carico valida")
          case   ((empty(.w_CVCAUSCA)) or not(looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUSCA)='-'))  and (.w_CVTATTIV<>'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCAUSCA_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CVCAUSCA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una causale di scarico valida")
          case   (empty(.w_CVCAUSER))  and (.w_CVTATTIV<>'Q')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCAUSER_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CVCAUSER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una causale valida")
          case   ((empty(.w_CVCAUMOU)) or not(looktab('CAM_AGAZ','CMFLCASC','CMCODICE',.w_CAUMOU)='+'))  and (.w_CVTATTIV<>'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCVCAUMOU_1_27.SetFocus()
            i_bnoObbl = !empty(.w_CVCAUMOU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una causale di carico valida")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CVTATTIV = this.w_CVTATTIV
    return

enddefine

* --- Define pages as container
define class tgsci_acvPag1 as StdContainer
  Width  = 703
  height = 237
  stdWidth  = 703
  stdheight = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCVCODICE_1_1 as StdField with uid="ZJUNEGCBIC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CVCODICE", cQueryName = "CVCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale",;
    HelpContextID = 118064491,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=55, Left=129, Top=17, InputMask=replicate('X',5)

  add object oCVFLDEFA_1_4 as StdCheck with uid="YUOEGDEGNZ",rtseq=2,rtrep=.f.,left=456, top=37, caption="Causale preferenziale",;
    ToolTipText = "Causale di versamento preferenziale",;
    HelpContextID = 50771303,;
    cFormVar="w_CVFLDEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCVFLDEFA_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCVFLDEFA_1_4.GetRadio()
    this.Parent.oContained.w_CVFLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oCVFLDEFA_1_4.SetRadio()
    this.Parent.oContained.w_CVFLDEFA=trim(this.Parent.oContained.w_CVFLDEFA)
    this.value = ;
      iif(this.Parent.oContained.w_CVFLDEFA=='S',1,;
      0)
  endfunc


  add object oCVTATTIV_1_5 as StdCombo with uid="VPFSMOZLUP",rtseq=3,rtrep=.f.,left=129,top=64,width=125,height=21;
    , ToolTipText = "Tipo attivit�";
    , HelpContextID = 218327684;
    , cFormVar="w_CVTATTIV",RowSource=""+"Solo quantit�,"+"Solo tempi,"+"Quantit� e tempi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCVTATTIV_1_5.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'T',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oCVTATTIV_1_5.GetRadio()
    this.Parent.oContained.w_CVTATTIV = this.RadioValue()
    return .t.
  endfunc

  func oCVTATTIV_1_5.SetRadio()
    this.Parent.oContained.w_CVTATTIV=trim(this.Parent.oContained.w_CVTATTIV)
    this.value = ;
      iif(this.Parent.oContained.w_CVTATTIV=='Q',1,;
      iif(this.Parent.oContained.w_CVTATTIV=='T',2,;
      iif(this.Parent.oContained.w_CVTATTIV=='E',3,;
      0)))
  endfunc

  func oCVTATTIV_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(.cFunction)="LOAD")
    endwith
   endif
  endfunc


  add object oCVFLPBSC_1_6 as StdCombo with uid="OXIRPHNMBN",rtseq=4,rtrep=.f.,left=129,top=88,width=125,height=21;
    , ToolTipText = "Pezzi buoni/scarti/entrambi";
    , HelpContextID = 13022569;
    , cFormVar="w_CVFLPBSC",RowSource=""+"Nessuna,"+"Quantit� buona,"+"Quantit� scarta,"+"Entrambe", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCVFLPBSC_1_6.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"B",;
    iif(this.value =3,"S",;
    iif(this.value =4,"E",;
    space(1))))))
  endfunc
  func oCVFLPBSC_1_6.GetRadio()
    this.Parent.oContained.w_CVFLPBSC = this.RadioValue()
    return .t.
  endfunc

  func oCVFLPBSC_1_6.SetRadio()
    this.Parent.oContained.w_CVFLPBSC=trim(this.Parent.oContained.w_CVFLPBSC)
    this.value = ;
      iif(this.Parent.oContained.w_CVFLPBSC=="N",1,;
      iif(this.Parent.oContained.w_CVFLPBSC=="B",2,;
      iif(this.Parent.oContained.w_CVFLPBSC=="S",3,;
      iif(this.Parent.oContained.w_CVFLPBSC=="E",4,;
      0))))
  endfunc

  func oCVFLPBSC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (upper(.cFunction)="LOAD" and .w_CVTATTIV<>"T")
    endwith
   endif
  endfunc

  add object oCVCAUCAR_1_9 as StdField with uid="YZEVAVIXNP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CVCAUCAR", cQueryName = "CVCAUCAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una causale di carico valida",;
    ToolTipText = "Causale di carico",;
    HelpContextID = 34309496,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=129, Top=112, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CVCAUCAR"

  func oCVCAUCAR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CVTATTIV<>'T')
    endwith
   endif
  endfunc

  func oCVCAUCAR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCAUCAR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCAUCAR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCVCAUCAR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCI_ZCC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCVCAUCAR_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CVCAUCAR
     i_obj.ecpSave()
  endproc

  add object oCVCAUSCA_1_10 as StdField with uid="JGZZZENOLC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CVCAUSCA", cQueryName = "CVCAUSCA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una causale di scarico valida",;
    ToolTipText = "Causale di scarico",;
    HelpContextID = 34309479,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=129, Top=137, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CVCAUSCA"

  func oCVCAUSCA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CVTATTIV<>'T')
    endwith
   endif
  endfunc

  func oCVCAUSCA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCAUSCA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCAUSCA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCVCAUSCA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCI_ZCS.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCVCAUSCA_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CVCAUSCA
     i_obj.ecpSave()
  endproc

  add object oCVCAUSER_1_11 as StdField with uid="PCCUEDSORJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CVCAUSER", cQueryName = "CVCAUSER",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una causale valida",;
    ToolTipText = "Causale tempi",;
    HelpContextID = 34309496,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=129, Top=162, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CVCAUSER"

  func oCVCAUSER_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CVTATTIV<>'Q')
    endwith
   endif
  endfunc

  func oCVCAUSER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCAUSER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCAUSER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCVCAUSER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oCVCAUSER_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CVCAUSER
     i_obj.ecpSave()
  endproc

  add object oCVUMTEMP_1_13 as StdField with uid="QBXWTZYUEI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CVUMTEMP", cQueryName = "CVUMTEMP",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "U.M. tempo",;
    HelpContextID = 200759946,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=129, Top=187, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_CVUMTEMP"

  func oCVUMTEMP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CVTATTIV<>'Q')
    endwith
   endif
  endfunc

  func oCVUMTEMP_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVUMTEMP_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVUMTEMP_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oCVUMTEMP_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"UM tempi",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCVUMTEMP_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UMFLTEMP=w_UMFLTEMP
     i_obj.w_UMCODICE=this.parent.oContained.w_CVUMTEMP
     i_obj.ecpSave()
  endproc

  add object oDESCVAR_1_17 as StdField with uid="YCQWLGGNYV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCVAR", cQueryName = "DESCVAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 1995830,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=177, Top=112, InputMask=replicate('X',30)

  add object oDESCSCA_1_18 as StdField with uid="FWMURWGCLS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCSCA", cQueryName = "DESCSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 32404534,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=177, Top=137, InputMask=replicate('X',30)

  add object oDESCTMP_1_19 as StdField with uid="FUUNLMZWXL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCTMP", cQueryName = "DESCTMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 201225270,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=177, Top=162, InputMask=replicate('X',30)

  add object oCVDESCRI_1_21 as StdField with uid="LWANEHNANF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CVDESCRI", cQueryName = "CVDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 32478575,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=129, Top=40, InputMask=replicate('X',40)

  add object oDTEMDEF_1_26 as StdField with uid="CBTPWFZRZF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DTEMDEF", cQueryName = "DTEMDEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 50832182,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=177, Top=187, InputMask=replicate('X',40)

  add object oCVCAUMOU_1_27 as StdField with uid="ZTXLOSVDUP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CVCAUMOU", cQueryName = "CVCAUMOU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una causale di carico valida",;
    HelpContextID = 202081659,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=129, Top=212, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CVCAUMOU"

  func oCVCAUMOU_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CVTATTIV<>'T')
    endwith
   endif
  endfunc

  func oCVCAUMOU_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCAUMOU_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCVCAUMOU_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCVCAUMOU_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSCI_ZCC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCVCAUMOU_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CVCAUMOU
     i_obj.ecpSave()
  endproc

  add object oDESCMOU_1_29 as StdField with uid="XKZPOGQMBX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCMOU", cQueryName = "DESCMOU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 227439670,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=177, Top=212, InputMask=replicate('X',30)

  add object oStr_1_2 as StdString with uid="UTBYQSSMNU",Visible=.t., Left=46, Top=43,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="WLCKRRYZQR",Visible=.t., Left=36, Top=20,;
    Alignment=1, Width=92, Height=18,;
    Caption="Codice causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZBQWWWDREZ",Visible=.t., Left=-15, Top=90,;
    Alignment=1, Width=143, Height=18,;
    Caption="Quantit� buona/scarta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="AAZTHRFNXI",Visible=.t., Left=58, Top=66,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="EUTMWAZWEH",Visible=.t., Left=20, Top=112,;
    Alignment=1, Width=108, Height=18,;
    Caption="Causale di carico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PQQSUTNTYP",Visible=.t., Left=12, Top=137,;
    Alignment=1, Width=116, Height=18,;
    Caption="Causale di scarico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TZQMJWJSQV",Visible=.t., Left=12, Top=162,;
    Alignment=1, Width=116, Height=18,;
    Caption="Causale tempi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WIMXACPGHW",Visible=.t., Left=25, Top=187,;
    Alignment=1, Width=103, Height=18,;
    Caption="U.M. tempo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="IETBYFNAAN",Visible=.t., Left=-51, Top=212,;
    Alignment=1, Width=179, Height=18,;
    Caption="Causale mat.output:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_acv','CAU_VERS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CVCODICE=CAU_VERS.CVCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
