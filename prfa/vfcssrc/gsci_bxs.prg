* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_bxs                                                        *
*              Esplosione struttura                                            *
*                                                                              *
*      Author: Zucchetti TAM srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-20                                                      *
* Last revis.: 2014-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODRIS,w_TIPOWC,w_NEWMC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_bxs",oParentObject,m.w_CODRIS,m.w_TIPOWC,m.w_NEWMC)
return(i_retval)

define class tgsci_bxs as StdBatch
  * --- Local variables
  w_CODRIS = space(20)
  w_TIPOWC = space(1)
  w_NEWMC = space(5)
  w_RLTIPO = space(2)
  w_RLCODICE = space(20)
  * --- WorkFile variables
  RIS_ORSE_idx=0
  DIS_RISO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di esplosione della struttura aziedale per WIP e Calen. ereditati
    * --- Codice Risorsa
    * --- Tipo: Wip o Calendario (W/C)
    * --- Nuovo Valore Wip o Calendario
    if not empty(this.w_NEWMC)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorna i Figli
      select Nodi
      scan
      this.w_RLTIPO = rl__tipo
      this.w_RLCODICE = rlcodice
      if this.w_TIPOWC="C"
        if this.w_RLTIPO="CL"
          * --- Select from GSCI3BXS
          do vq_exec with 'GSCI3BXS',this,'_Curs_GSCI3BXS','',.f.,.t.
          if used('_Curs_GSCI3BXS')
            select _Curs_GSCI3BXS
            locate for 1=1
            do while not(eof())
            * --- Aggiorna il calendario nella DisponibilitÓ risorsa
            * --- Write into DIS_RISO
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_RISO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_RISO_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_RISO_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DRCODCAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWMC),'DIS_RISO','DRCODCAL');
                  +i_ccchkf ;
              +" where ";
                  +"DRCODRIS = "+cp_ToStrODBC(this.w_RLCODICE);
                  +" and DRDATINI = "+cp_ToStrODBC(_Curs_GSCI3BXS.DRDATINI);
                     )
            else
              update (i_cTable) set;
                  DRCODCAL = this.w_NEWMC;
                  &i_ccchkf. ;
               where;
                  DRCODRIS = this.w_RLCODICE;
                  and DRDATINI = _Curs_GSCI3BXS.DRDATINI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
              select _Curs_GSCI3BXS
              continue
            enddo
            use
          endif
        else
          * --- Aggiorna il calendario della risorsa
          * --- Write into RIS_ORSE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RLCODCAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWMC),'RIS_ORSE','RLCODCAL');
                +i_ccchkf ;
            +" where ";
                +"RLCODICE = "+cp_ToStrODBC(this.w_RLCODICE);
                   )
          else
            update (i_cTable) set;
                RLCODCAL = this.w_NEWMC;
                &i_ccchkf. ;
             where;
                RLCODICE = this.w_RLCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        * --- Aggiorna il mag. WIP della risorsa
        * --- Write into RIS_ORSE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RIS_ORSE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ORSE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIS_ORSE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_NEWMC),'RIS_ORSE','RLMAGWIP');
              +i_ccchkf ;
          +" where ";
              +"RLCODICE = "+cp_ToStrODBC(this.w_RLCODICE);
                 )
        else
          update (i_cTable) set;
              RLMAGWIP = this.w_NEWMC;
              &i_ccchkf. ;
           where;
              RLCODICE = this.w_RLCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      endscan
    endif
    if used("Nodi")
      use in Nodi
    endif
    if used("Risorse")
      use in Risorse
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione
    if not empty(this.w_CODRIS)
      ah_msg("Esplosione struttura in corso...")
      fldnull = iif(this.w_TIPOWC="W", "rlwipris", "rlcalris")
      * --- Risorse aziendali e relativi "figli"
      do vq_exec WITH "..\PRFA\Exe\Query\GSCI_BXS", this, "Risorse"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Esplode i figli della Risorsa
      select rlcodice, rl__tipo from Risorse where sccodpad=this.w_CODRIS and isnull( &fldnull ) into cursor nodi
      =wrcursor("Nodi")
      select * from Nodi into cursor exptmp
      this.w_RLTIPO = rl__tipo
      * --- Esplosione ricorsiva
      do while (this.w_TIPOWC="W" and this.w_RLTIPO<>"MA") or (this.w_TIPOWC="C" and this.w_RLTIPO<>"CL")
        select rlcodice, rl__tipo from Risorse where isnull( &fldnull ) and ;
        sccodpad in (select rlcodice from exptmp) into cursor nextlvl
        if RecCount("nextlvl")>0
          select * from nextlvl into array figli
          select Nodi
          append from array figli
          release figli
          select * from nextlvl into cursor exptmp
          this.w_RLTIPO = rl__tipo
        else
          this.w_RLTIPO = iif(this.w_TIPOWC="W", "MA", "CL")
        endif
      enddo
      if used("exptmp")
        use in exptmp
      endif
      if used("nextlvl")
        use in nextlvl
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_CODRIS,w_TIPOWC,w_NEWMC)
    this.w_CODRIS=w_CODRIS
    this.w_TIPOWC=w_TIPOWC
    this.w_NEWMC=w_NEWMC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RIS_ORSE'
    this.cWorkTables[2]='DIS_RISO'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCI3BXS')
      use in _Curs_GSCI3BXS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODRIS,w_TIPOWC,w_NEWMC"
endproc
