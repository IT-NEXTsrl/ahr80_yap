* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_btf                                                        *
*              Tempificazione fasi                                             *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][60]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-08-23                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODODL,pDATINI,pDATFIN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_btf",oParentObject,m.pCODODL,m.pDATINI,m.pDATFIN)
return(i_retval)

define class tgsci_btf as StdBatch
  * --- Local variables
  pCODODL = space(15)
  pDATINI = ctod("  /  /  ")
  pDATFIN = ctod("  /  /  ")
  w_OLCODODL = space(15)
  w_oPOS = 0
  w_OLDODL = space(15)
  w_TmpN = ctod("  /  /  ")
  w_TmpN1 = ctod("  /  /  ")
  w_ODLCUR = space(15)
  w_OKODL = .f.
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_LTFASE = 0
  w_DATVUOTA = ctod("  /  /  ")
  w_OCLCOLL = space(15)
  * --- WorkFile variables
  ODL_MAST_idx=0
  ODL_DETT_idx=0
  PAR_RIOR_idx=0
  SALDIART_idx=0
  PAR_PROD_idx=0
  CAM_AGAZ_idx=0
  KEY_ARTI_idx=0
  ODL_CICL_idx=0
  PRD_ERRO_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Data inizio produzione
    * --- --Data fine produzione 
    * --- Variabili locali
    * --- Verifica se si deve fermare subito ...
    if not( g_PRFA="S" and g_CICLILAV="S" )
      i_retcode = 'stop'
      return
    endif
    * --- Messaggio ...
    ah_msg("Tempificazione fasi in corso...")
    * --- Estrae elenco fasi da tempificare
    this.w_DATVUOTA = cp_CharToDate("  -  -  ")
    this.w_OLCODODL = this.pCODODL
    this.w_OLDODL = repl("-",15)
    * --- Select from GSCI2BTF
    do vq_exec with 'GSCI2BTF',this,'_Curs_GSCI2BTF','',.f.,.t.
    if used('_Curs_GSCI2BTF')
      select _Curs_GSCI2BTF
      locate for 1=1
      do while not(eof())
      * --- --Nuovo calcolo della data, � stata variata sull'odl
      if not empty(this.pDATFIN)
        this.w_ODLCUR = _Curs_GSCI2BTF.OLCODODL
        if this.w_ODLCUR <> this.w_OLDODL
          this.w_OLDODL = this.w_ODLCUR
          this.w_DATFIN = this.pDATFIN
        else
          this.w_DATFIN = this.w_DATINI
        endif
        * --- --OCCORRE RICALCOLARE LE DATE
        * --- LT Fase
        this.w_LTFASE = _Curs_GSCI2BTF.RLTEMCOD
        * --- Calcola data di inizio della fase
        this.w_DATINI = COCALCLT(this.w_DATFIN,this.w_LTFASE,"I"," ")
        * --- Aggiorna dettaglio ODL
        * --- Write into ODL_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'ODL_CICL','CLDATINI');
          +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'ODL_CICL','CLDATFIN');
              +i_ccchkf ;
          +" where ";
              +"CLCODODL = "+cp_ToStrODBC(_Curs_GSCI2BTF.OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCI2BTF.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CLDATINI = this.w_DATINI;
              ,CLDATFIN = this.w_DATFIN;
              &i_ccchkf. ;
           where;
              CLCODODL = _Curs_GSCI2BTF.OLCODODL;
              and CPROWNUM = _Curs_GSCI2BTF.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Cambio ODL
        this.w_ODLCUR = _Curs_GSCI2BTF.OLCODODL
        if this.w_ODLCUR <> this.w_OLDODL
          this.w_OLDODL = this.w_ODLCUR
          * --- Verifica se c'� qualche fase gi� tempificata
          select _Curs_GSCI2BTF
          this.w_oPOS = recno()
          this.w_TmpN = CLDATINI
          this.w_TmpN1 = CLDATFIN
          if this.w_oPOS<>0
            goto this.w_oPOS
          endif
          * --- Non ritempifico gli ODL gi� tempificati.
          this.w_OKODL = (empty(nvl(CLDATINI,""))and empty(nvl(CLDATFIN,"")))
          if this.w_OKODL
            * --- Pulizia vecchie date
            * --- Write into ODL_CICL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_CICL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CLDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATVUOTA),'ODL_CICL','CLDATINI');
              +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATVUOTA),'ODL_CICL','CLDATFIN');
                  +i_ccchkf ;
              +" where ";
                  +"CLCODODL = "+cp_ToStrODBC(_Curs_GSCI2BTF.OLCODODL);
                     )
            else
              update (i_cTable) set;
                  CLDATINI = this.w_DATVUOTA;
                  ,CLDATFIN = this.w_DATVUOTA;
                  &i_ccchkf. ;
               where;
                  CLCODODL = _Curs_GSCI2BTF.OLCODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- La data di fine dell'ultima fase coincide con la data di consegna dell'ODL
            this.w_DATFIN = _Curs_GSCI2BTF.OLTDTRIC
          endif
        else
          * --- La data di fine delle altre fasi concide con la data di inzio della precedente
          this.w_DATFIN = this.w_DATINI
        endif
        if this.w_OKODL
          * --- LT Fase
          this.w_LTFASE = _Curs_GSCI2BTF.RLTEMCOD
          * --- Calcola data di inizio della fase
          this.w_DATINI = COCALCLT(this.w_DATFIN,this.w_LTFASE,"I"," ")
          * --- Aggiorna dettaglio ODL
          * --- Write into ODL_CICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_CICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_CICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_CICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'ODL_CICL','CLDATINI');
            +",CLDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'ODL_CICL','CLDATFIN');
                +i_ccchkf ;
            +" where ";
                +"CLCODODL = "+cp_ToStrODBC(_Curs_GSCI2BTF.OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCI2BTF.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CLDATINI = this.w_DATINI;
                ,CLDATFIN = this.w_DATFIN;
                &i_ccchkf. ;
             where;
                CLCODODL = _Curs_GSCI2BTF.OLCODODL;
                and CPROWNUM = _Curs_GSCI2BTF.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_GSCI2BTF
        continue
      enddo
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCODODL,pDATINI,pDATFIN)
    this.pCODODL=pCODODL
    this.pDATINI=pDATINI
    this.pDATFIN=pDATFIN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ODL_MAST'
    this.cWorkTables[2]='ODL_DETT'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='PAR_PROD'
    this.cWorkTables[6]='CAM_AGAZ'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='ODL_CICL'
    this.cWorkTables[9]='PRD_ERRO'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_GSCI2BTF')
      use in _Curs_GSCI2BTF
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODODL,pDATINI,pDATFIN"
endproc
