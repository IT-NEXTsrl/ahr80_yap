* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_mdf                                                        *
*              Dettaglio codifica fasi                                         *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-28                                                      *
* Last revis.: 2018-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsci_mdf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsci_mdf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsci_mdf")
  return

* --- Class definition
define class tgsci_mdf as StdPCForm
  Width  = 797
  Height = 254+35
  Top    = 3
  Left   = 6
  cComment = "Dettaglio codifica fasi"
  cPrg = "gsci_mdf"
  HelpContextID=80348521
  add object cnt as tcgsci_mdf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsci_mdf as PCContext
  w_DFCODICE = space(5)
  w_CPROWORD = 0
  w_DFCODCLA = space(5)
  w_DFCODCLA = space(5)
  w_CLARIS = space(20)
  w_DF__TIPO = space(2)
  w_DFCODRIS = space(20)
  w_DFTIPTEM = space(1)
  w_DFQTARIS = 0
  w_DFUMTRIS = space(5)
  w_DFTEMRIS = 0
  w_UMTDEF = space(5)
  w_QTARIS = 0
  w_CONLAV = 0
  w_DFTMPSEC = 0
  w_DFNUMIMP = 0
  w_CSFLAPRE = space(1)
  w_DFINTEST = space(1)
  w_DESRIS = space(40)
  w_MATINP = space(1)
  w_TPFIL = space(10)
  w_UMFLTEMP = space(1)
  w_DFISTLAV = space(10)
  proc Save(i_oFrom)
    this.w_DFCODICE = i_oFrom.w_DFCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DFCODCLA = i_oFrom.w_DFCODCLA
    this.w_DFCODCLA = i_oFrom.w_DFCODCLA
    this.w_CLARIS = i_oFrom.w_CLARIS
    this.w_DF__TIPO = i_oFrom.w_DF__TIPO
    this.w_DFCODRIS = i_oFrom.w_DFCODRIS
    this.w_DFTIPTEM = i_oFrom.w_DFTIPTEM
    this.w_DFQTARIS = i_oFrom.w_DFQTARIS
    this.w_DFUMTRIS = i_oFrom.w_DFUMTRIS
    this.w_DFTEMRIS = i_oFrom.w_DFTEMRIS
    this.w_UMTDEF = i_oFrom.w_UMTDEF
    this.w_QTARIS = i_oFrom.w_QTARIS
    this.w_CONLAV = i_oFrom.w_CONLAV
    this.w_DFTMPSEC = i_oFrom.w_DFTMPSEC
    this.w_DFNUMIMP = i_oFrom.w_DFNUMIMP
    this.w_CSFLAPRE = i_oFrom.w_CSFLAPRE
    this.w_DFINTEST = i_oFrom.w_DFINTEST
    this.w_DESRIS = i_oFrom.w_DESRIS
    this.w_MATINP = i_oFrom.w_MATINP
    this.w_TPFIL = i_oFrom.w_TPFIL
    this.w_UMFLTEMP = i_oFrom.w_UMFLTEMP
    this.w_DFISTLAV = i_oFrom.w_DFISTLAV
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DFCODICE = this.w_DFCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DFCODCLA = this.w_DFCODCLA
    i_oTo.w_DFCODCLA = this.w_DFCODCLA
    i_oTo.w_CLARIS = this.w_CLARIS
    i_oTo.w_DF__TIPO = this.w_DF__TIPO
    i_oTo.w_DFCODRIS = this.w_DFCODRIS
    i_oTo.w_DFTIPTEM = this.w_DFTIPTEM
    i_oTo.w_DFQTARIS = this.w_DFQTARIS
    i_oTo.w_DFUMTRIS = this.w_DFUMTRIS
    i_oTo.w_DFTEMRIS = this.w_DFTEMRIS
    i_oTo.w_UMTDEF = this.w_UMTDEF
    i_oTo.w_QTARIS = this.w_QTARIS
    i_oTo.w_CONLAV = this.w_CONLAV
    i_oTo.w_DFTMPSEC = this.w_DFTMPSEC
    i_oTo.w_DFNUMIMP = this.w_DFNUMIMP
    i_oTo.w_CSFLAPRE = this.w_CSFLAPRE
    i_oTo.w_DFINTEST = this.w_DFINTEST
    i_oTo.w_DESRIS = this.w_DESRIS
    i_oTo.w_MATINP = this.w_MATINP
    i_oTo.w_TPFIL = this.w_TPFIL
    i_oTo.w_UMFLTEMP = this.w_UMFLTEMP
    i_oTo.w_DFISTLAV = this.w_DFISTLAV
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsci_mdf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 797
  Height = 254+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-04-16"
  HelpContextID=80348521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_FASI_IDX = 0
  CLR_DETT_IDX = 0
  RIS_ORSE_IDX = 0
  TIP_RISO_IDX = 0
  UNIMIS_IDX = 0
  CLR_MAST_IDX = 0
  cFile = "DET_FASI"
  cKeySelect = "DFCODICE"
  cKeyWhere  = "DFCODICE=this.w_DFCODICE"
  cKeyDetail  = "DFCODICE=this.w_DFCODICE"
  cKeyWhereODBC = '"DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';

  cKeyDetailWhereODBC = '"DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DET_FASI.DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DET_FASI.CPROWORD '
  cPrg = "gsci_mdf"
  cComment = "Dettaglio codifica fasi"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODICE = space(5)
  w_CPROWORD = 0
  w_DFCODCLA = space(5)
  o_DFCODCLA = space(5)
  w_DFCODCLA = space(5)
  w_CLARIS = space(20)
  w_DF__TIPO = space(2)
  w_DFCODRIS = space(20)
  o_DFCODRIS = space(20)
  w_DFTIPTEM = space(1)
  w_DFQTARIS = 0
  w_DFUMTRIS = space(5)
  w_DFTEMRIS = 0
  w_UMTDEF = space(5)
  w_QTARIS = 0
  w_CONLAV = 0
  w_DFTMPSEC = 0
  w_DFNUMIMP = 0
  w_CSFLAPRE = space(1)
  w_DFINTEST = space(1)
  w_DESRIS = space(40)
  w_MATINP = space(1)
  w_TPFIL = space(10)
  w_UMFLTEMP = space(1)
  w_DFISTLAV = space(0)

  * --- Children pointers
  GSCI_MIN = .NULL.
  GSCI_MOU = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsci_mdfPag1","gsci_mdf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio codifica fasi")
      .Pages(2).addobject("oPag","tgsci_mdfPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Materiali input")
      .Pages(3).addobject("oPag","tgsci_mdfPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Materiali output")
      .Pages(4).addobject("oPag","tgsci_mdfPag4")
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note fase")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CLR_DETT'
    this.cWorkTables[2]='RIS_ORSE'
    this.cWorkTables[3]='TIP_RISO'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='CLR_MAST'
    this.cWorkTables[6]='DET_FASI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_FASI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_FASI_IDX,3]
  return

  function CreateChildren()
    this.GSCI_MIN = CREATEOBJECT('stdDynamicChild',this,'GSCI_MIN',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    this.GSCI_MIN.createrealchild()
    this.GSCI_MOU = CREATEOBJECT('stdDynamicChild',this,'GSCI_MOU',this.oPgFrm.Page3.oPag.oLinkPC_5_1)
    return

  procedure NewContext()
    return(createobject('tsgsci_mdf'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCI_MIN)
      this.GSCI_MIN.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSCI_MOU)
      this.GSCI_MOU.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCI_MIN.HideChildrenChain()
    this.GSCI_MOU.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCI_MIN.ShowChildrenChain()
    this.GSCI_MOU.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCI_MIN)
      this.GSCI_MIN.DestroyChildrenChain()
      this.GSCI_MIN=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSCI_MOU)
      this.GSCI_MOU.DestroyChildrenChain()
      this.GSCI_MOU=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_5_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCI_MIN.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCI_MOU.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCI_MIN.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCI_MOU.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCI_MIN.NewDocument()
    this.GSCI_MOU.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCI_MIN.ChangeRow(this.cRowID+'      1',1;
             ,.w_DFCODICE,"DICODICE";
             )
      .GSCI_MOU.ChangeRow(this.cRowID+'      1',1;
             ,.w_DFCODICE,"DMCODICE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_FASI where DFCODICE=KeySet.DFCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2],this.bLoadRecFilter,this.DET_FASI_IDX,"gsci_mdf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_FASI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_FASI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_FASI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCODICE',this.w_DFCODICE  )
      select * from (i_cTable) DET_FASI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MATINP = space(1)
        .w_UMFLTEMP = 'S'
        .w_DFCODICE = NVL(DFCODICE,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_DFISTLAV = NVL(DFISTLAV,space(0))
        cp_LoadRecExtFlds(this,'DET_FASI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CLARIS = space(20)
          .w_UMTDEF = space(5)
          .w_QTARIS = 0
          .w_CONLAV = 0
        .w_CSFLAPRE = 'S'
          .w_DFINTEST = space(1)
          .w_DESRIS = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DFCODCLA = NVL(DFCODCLA,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_DFCODCLA = NVL(DFCODCLA,space(5))
          * evitabile
          *.link_2_3('Load')
          .w_DF__TIPO = NVL(DF__TIPO,space(2))
          .w_DFCODRIS = NVL(DFCODRIS,space(20))
          .link_2_6('Load')
          .w_DFTIPTEM = NVL(DFTIPTEM,space(1))
          .w_DFQTARIS = NVL(DFQTARIS,0)
          .w_DFUMTRIS = NVL(DFUMTRIS,space(5))
          .link_2_9('Load')
          .w_DFTEMRIS = NVL(DFTEMRIS,0)
          .w_DFTMPSEC = NVL(DFTMPSEC,0)
          .w_DFNUMIMP = NVL(DFNUMIMP,0)
        .w_TPFIL = .w_DF__TIPO
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DFCODICE=space(5)
      .w_CPROWORD=10
      .w_DFCODCLA=space(5)
      .w_DFCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_DF__TIPO=space(2)
      .w_DFCODRIS=space(20)
      .w_DFTIPTEM=space(1)
      .w_DFQTARIS=0
      .w_DFUMTRIS=space(5)
      .w_DFTEMRIS=0
      .w_UMTDEF=space(5)
      .w_QTARIS=0
      .w_CONLAV=0
      .w_DFTMPSEC=0
      .w_DFNUMIMP=0
      .w_CSFLAPRE=space(1)
      .w_DFINTEST=space(1)
      .w_DESRIS=space(40)
      .w_MATINP=space(1)
      .w_TPFIL=space(10)
      .w_UMFLTEMP=space(1)
      .w_DFISTLAV=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_DFCODCLA))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_DFCODCLA))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,5,.f.)
        .w_DF__TIPO = 'CL'
        .w_DFCODRIS = iif(empty(.w_CLARIS),.w_DFCODRIS,.w_CLARIS)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DFCODRIS))
         .link_2_6('Full')
        endif
        .w_DFTIPTEM = 'L'
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_DFUMTRIS))
         .link_2_9('Full')
        endif
        .DoRTCalc(11,14,.f.)
        .w_DFTMPSEC = .w_DFTEMRIS*.w_CONLAV
        .w_DFNUMIMP = 1
        .w_CSFLAPRE = 'S'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(18,20,.f.)
        .w_TPFIL = .w_DF__TIPO
        .w_UMFLTEMP = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_FASI')
    this.DoRTCalc(23,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDFNUMIMP_2_15.enabled = i_bVal
      .Page4.oPag.oDFISTLAV_6_1.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCI_MIN.SetStatus(i_cOp)
    this.GSCI_MOU.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DET_FASI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCI_MIN.SetChildrenStatus(i_cOp)
  *  this.GSCI_MOU.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_FASI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODICE,"DFCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFISTLAV,"DFISTLAV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DFCODCLA C(5);
      ,t_DF__TIPO N(3);
      ,t_DFCODRIS C(20);
      ,t_DFTIPTEM N(3);
      ,t_DFQTARIS N(8,2);
      ,t_DFUMTRIS C(5);
      ,t_DFTEMRIS N(18,4);
      ,t_DFNUMIMP N(4);
      ,CPROWNUM N(10);
      ,t_CLARIS C(20);
      ,t_UMTDEF C(5);
      ,t_QTARIS N(12,5);
      ,t_CONLAV N(12,3);
      ,t_DFTMPSEC N(18,4);
      ,t_CSFLAPRE C(1);
      ,t_DFINTEST C(1);
      ,t_DESRIS C(40);
      ,t_TPFIL C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsci_mdfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODCLA_2_2.controlsource=this.cTrsName+'.t_DFCODCLA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.controlsource=this.cTrsName+'.t_DF__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_6.controlsource=this.cTrsName+'.t_DFCODRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.controlsource=this.cTrsName+'.t_DFTIPTEM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFQTARIS_2_8.controlsource=this.cTrsName+'.t_DFQTARIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFUMTRIS_2_9.controlsource=this.cTrsName+'.t_DFUMTRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFTEMRIS_2_10.controlsource=this.cTrsName+'.t_DFTEMRIS'
    this.oPgFRm.Page1.oPag.oDFNUMIMP_2_15.controlsource=this.cTrsName+'.t_DFNUMIMP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(112)
    this.AddVLine(235)
    this.AddVLine(407)
    this.AddVLine(523)
    this.AddVLine(569)
    this.AddVLine(629)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2])
      *
      * insert into DET_FASI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_FASI')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_FASI')
        i_cFldBody=" "+;
                  "(DFCODICE,CPROWORD,DFCODCLA,DF__TIPO,DFCODRIS"+;
                  ",DFTIPTEM,DFQTARIS,DFUMTRIS,DFTEMRIS,DFTMPSEC"+;
                  ",DFNUMIMP,DFISTLAV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DFCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DFCODCLA)+","+cp_ToStrODBC(this.w_DF__TIPO)+","+cp_ToStrODBCNull(this.w_DFCODRIS)+;
             ","+cp_ToStrODBC(this.w_DFTIPTEM)+","+cp_ToStrODBC(this.w_DFQTARIS)+","+cp_ToStrODBCNull(this.w_DFUMTRIS)+","+cp_ToStrODBC(this.w_DFTEMRIS)+","+cp_ToStrODBC(this.w_DFTMPSEC)+;
             ","+cp_ToStrODBC(this.w_DFNUMIMP)+","+cp_ToStrODBC(this.w_DFISTLAV)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_FASI')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_FASI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DFCODICE',this.w_DFCODICE)
        INSERT INTO (i_cTable) (;
                   DFCODICE;
                  ,CPROWORD;
                  ,DFCODCLA;
                  ,DF__TIPO;
                  ,DFCODRIS;
                  ,DFTIPTEM;
                  ,DFQTARIS;
                  ,DFUMTRIS;
                  ,DFTEMRIS;
                  ,DFTMPSEC;
                  ,DFNUMIMP;
                  ,DFISTLAV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DFCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_DFCODCLA;
                  ,this.w_DF__TIPO;
                  ,this.w_DFCODRIS;
                  ,this.w_DFTIPTEM;
                  ,this.w_DFQTARIS;
                  ,this.w_DFUMTRIS;
                  ,this.w_DFTEMRIS;
                  ,this.w_DFTMPSEC;
                  ,this.w_DFNUMIMP;
                  ,this.w_DFISTLAV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) ) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_FASI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DFISTLAV="+cp_ToStrODBC(this.w_DFISTLAV)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_FASI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DFISTLAV=this.w_DFISTLAV;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_FASI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_FASI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DFCODCLA="+cp_ToStrODBCNull(this.w_DFCODCLA)+;
                     ",DF__TIPO="+cp_ToStrODBC(this.w_DF__TIPO)+;
                     ",DFCODRIS="+cp_ToStrODBCNull(this.w_DFCODRIS)+;
                     ",DFTIPTEM="+cp_ToStrODBC(this.w_DFTIPTEM)+;
                     ",DFQTARIS="+cp_ToStrODBC(this.w_DFQTARIS)+;
                     ",DFUMTRIS="+cp_ToStrODBCNull(this.w_DFUMTRIS)+;
                     ",DFTEMRIS="+cp_ToStrODBC(this.w_DFTEMRIS)+;
                     ",DFTMPSEC="+cp_ToStrODBC(this.w_DFTMPSEC)+;
                     ",DFNUMIMP="+cp_ToStrODBC(this.w_DFNUMIMP)+;
                     ",DFISTLAV="+cp_ToStrODBC(this.w_DFISTLAV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_FASI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DFCODCLA=this.w_DFCODCLA;
                     ,DF__TIPO=this.w_DF__TIPO;
                     ,DFCODRIS=this.w_DFCODRIS;
                     ,DFTIPTEM=this.w_DFTIPTEM;
                     ,DFQTARIS=this.w_DFQTARIS;
                     ,DFUMTRIS=this.w_DFUMTRIS;
                     ,DFTEMRIS=this.w_DFTEMRIS;
                     ,DFTMPSEC=this.w_DFTMPSEC;
                     ,DFNUMIMP=this.w_DFNUMIMP;
                     ,DFISTLAV=this.w_DFISTLAV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask footer and header children to save themselves
      * --- GSCI_MIN : Saving
      this.GSCI_MIN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DFCODICE,"DICODICE";
             )
      this.GSCI_MIN.mReplace()
      * --- GSCI_MOU : Saving
      this.GSCI_MOU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DFCODICE,"DMCODICE";
             )
      this.GSCI_MOU.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsci_mdf
    * --- Controlli sul ciclo di lavorazione
    GSCI_BCF(this,"ChkFase")
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSCI_MIN : Deleting
    this.GSCI_MIN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DFCODICE,"DICODICE";
           )
    this.GSCI_MIN.mDelete()
    * --- GSCI_MOU : Deleting
    this.GSCI_MOU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DFCODICE,"DMCODICE";
           )
    this.GSCI_MOU.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_FASI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_FASI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) ) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_FASI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_FASI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_DFCODCLA<>.w_DFCODCLA
          .link_2_3('Full')
        endif
        .DoRTCalc(5,6,.t.)
        if .o_DFCODCLA<>.w_DFCODCLA.or. .o_DFCODRIS<>.w_DFCODRIS
          .w_DFCODRIS = iif(empty(.w_CLARIS),.w_DFCODRIS,.w_CLARIS)
          .link_2_6('Full')
        endif
        .DoRTCalc(8,14,.t.)
          .w_DFTMPSEC = .w_DFTEMRIS*.w_CONLAV
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(16,20,.t.)
          .w_TPFIL = .w_DF__TIPO
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DFCODCLA with this.w_DFCODCLA
      replace t_CLARIS with this.w_CLARIS
      replace t_UMTDEF with this.w_UMTDEF
      replace t_QTARIS with this.w_QTARIS
      replace t_CONLAV with this.w_CONLAV
      replace t_DFTMPSEC with this.w_DFTMPSEC
      replace t_CSFLAPRE with this.w_CSFLAPRE
      replace t_DFINTEST with this.w_DFINTEST
      replace t_DESRIS with this.w_DESRIS
      replace t_TPFIL with this.w_TPFIL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFQTARIS_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFQTARIS_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFUMTRIS_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFUMTRIS_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFTEMRIS_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFTEMRIS_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DFCODCLA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_DETT_IDX,3]
    i_lTable = "CLR_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2], .t., this.CLR_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CLR_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCLA like "+cp_ToStrODBC(trim(this.w_DFCODCLA)+"%");
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);

          i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSFLAPRE,CSCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSFLAPRE',this.w_CSFLAPRE;
                     ,'CSCODCLA',trim(this.w_DFCODCLA))
          select CSFLAPRE,CSCODCLA,CSCODRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSFLAPRE,CSCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFCODCLA)==trim(_Link_.CSCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(oSource.parent,'oDFCODCLA_2_2'),i_cWhere,'',"Classi di risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CSFLAPRE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',oSource.xKey(1);
                       ,'CSCODCLA',oSource.xKey(2))
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSFLAPRE,CSCODCLA,CSCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_DFCODCLA);
                   +" and CSFLAPRE="+cp_ToStrODBC(this.w_CSFLAPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSFLAPRE',this.w_CSFLAPRE;
                       ,'CSCODCLA',this.w_DFCODCLA)
            select CSFLAPRE,CSCODCLA,CSCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_DFCODRIS = NVL(_Link_.CSCODRIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DFCODCLA = space(5)
      endif
      this.w_DFCODRIS = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_DETT_IDX,2])+'\'+cp_ToStr(_Link_.CSFLAPRE,1)+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFCODCLA
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLR_MAST_IDX,3]
    i_lTable = "CLR_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2], .t., this.CLR_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCLA,CSTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCLA="+cp_ToStrODBC(this.w_DFCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCLA',this.w_DFCODCLA)
            select CSCODCLA,CSTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCODCLA = NVL(_Link_.CSCODCLA,space(5))
      this.w_DF__TIPO = NVL(_Link_.CSTIPCLA,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DFCODCLA = space(5)
      endif
      this.w_DF__TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLR_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLR_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFCODRIS
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_DF__TIPO);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_DF__TIPO;
                     ,'RLCODICE',trim(this.w_DFCODRIS))
          select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFCODRIS)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RLUMTDEF like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_DF__TIPO);

            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RLUMTDEF like "+cp_ToStr(trim(this.w_DFCODRIS)+"%");
                   +" and RL__TIPO="+cp_ToStr(this.w_DF__TIPO);

            select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DFCODRIS) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oDFCODRIS_2_6'),i_cWhere,'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DF__TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo risorsa non valido oppure risorsa inestente.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_DF__TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_DFCODRIS);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_DF__TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_DF__TIPO;
                       ,'RLCODICE',this.w_DFCODRIS)
            select RL__TIPO,RLCODICE,RLUMTDEF,RLQTARIS,RLINTEST,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCODRIS = NVL(_Link_.RLCODICE,space(20))
      this.w_UMTDEF = NVL(_Link_.RLUMTDEF,space(5))
      this.w_QTARIS = NVL(_Link_.RLQTARIS,0)
      this.w_DFINTEST = NVL(_Link_.RLINTEST,space(1))
      this.w_DESRIS = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DFCODRIS = space(20)
      endif
      this.w_UMTDEF = space(5)
      this.w_QTARIS = 0
      this.w_DFINTEST = space(1)
      this.w_DESRIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFUMTRIS
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFUMTRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DFUMTRIS)+"%");
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);

          i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMFLTEMP,UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMFLTEMP',this.w_UMFLTEMP;
                     ,'UMCODICE',trim(this.w_DFUMTRIS))
          select UMFLTEMP,UMCODICE,UMDURSEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMFLTEMP,UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFUMTRIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFUMTRIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(oSource.parent,'oDFUMTRIS_2_9'),i_cWhere,'',"",'GSCI_ZUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_UMFLTEMP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',oSource.xKey(1);
                       ,'UMCODICE',oSource.xKey(2))
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFUMTRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMFLTEMP,UMCODICE,UMDURSEC";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DFUMTRIS);
                   +" and UMFLTEMP="+cp_ToStrODBC(this.w_UMFLTEMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMFLTEMP',this.w_UMFLTEMP;
                       ,'UMCODICE',this.w_DFUMTRIS)
            select UMFLTEMP,UMCODICE,UMDURSEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFUMTRIS = NVL(_Link_.UMCODICE,space(5))
      this.w_CONLAV = NVL(_Link_.UMDURSEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_DFUMTRIS = space(5)
      endif
      this.w_CONLAV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CONLAV > 0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
        endif
        this.w_DFUMTRIS = space(5)
        this.w_CONLAV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMFLTEMP,1)+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFUMTRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDFNUMIMP_2_15.value==this.w_DFNUMIMP)
      this.oPgFrm.Page1.oPag.oDFNUMIMP_2_15.value=this.w_DFNUMIMP
      replace t_DFNUMIMP with this.oPgFrm.Page1.oPag.oDFNUMIMP_2_15.value
    endif
    if not(this.oPgFrm.Page4.oPag.oDFISTLAV_6_1.value==this.w_DFISTLAV)
      this.oPgFrm.Page4.oPag.oDFISTLAV_6_1.value=this.w_DFISTLAV
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODCLA_2_2.value==this.w_DFCODCLA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODCLA_2_2.value=this.w_DFCODCLA
      replace t_DFCODCLA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODCLA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.RadioValue()==this.w_DF__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.SetRadio()
      replace t_DF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_6.value==this.w_DFCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_6.value=this.w_DFCODRIS
      replace t_DFCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.RadioValue()==this.w_DFTIPTEM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.SetRadio()
      replace t_DFTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFQTARIS_2_8.value==this.w_DFQTARIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFQTARIS_2_8.value=this.w_DFQTARIS
      replace t_DFQTARIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFQTARIS_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFUMTRIS_2_9.value==this.w_DFUMTRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFUMTRIS_2_9.value=this.w_DFUMTRIS
      replace t_DFUMTRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFUMTRIS_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTEMRIS_2_10.value==this.w_DFTEMRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTEMRIS_2_10.value=this.w_DFTEMRIS
      replace t_DFTEMRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTEMRIS_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_FASI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCI_MIN.CheckForm()
      if i_bres
        i_bres=  .GSCI_MIN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCI_MOU.CheckForm()
      if i_bres
        i_bres=  .GSCI_MOU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DFQTARIS<=.w_QTARIS) and (! .w_DF__TIPO $ 'MA-AR-RE') and (not(Empty(.w_CPROWORD)) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFQTARIS_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_CONLAV > 0) and (.w_DFQTARIS>0) and not(empty(.w_DFUMTRIS)) and (not(Empty(.w_CPROWORD)) )
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFUMTRIS_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura non gestita a tempo oppure senza conversione in secondi")
      endcase
      if not(Empty(.w_CPROWORD)) 
        * --- Area Manuale = Check Row
        * --- gsci_mdf
        If i_bRes and .w_DFTEMRIS=0 and ! .w_DF__TIPO $ 'MA-AR-RE'
         i_bRes=.f.
         i_bnoChk = .f.		
         i_cErrorMsg = Ah_MsgFormat("Sono presenti risorse senza tempi di attrezzaggio, avviamento o lavorazione")
        Endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DFCODCLA = this.w_DFCODCLA
    this.o_DFCODRIS = this.w_DFCODRIS
    * --- GSCI_MIN : Depends On
    this.GSCI_MIN.SaveDependsOn()
    * --- GSCI_MOU : Depends On
    this.GSCI_MOU.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DFCODCLA=space(5)
      .w_DFCODCLA=space(5)
      .w_CLARIS=space(20)
      .w_DF__TIPO=space(2)
      .w_DFCODRIS=space(20)
      .w_DFTIPTEM=space(1)
      .w_DFQTARIS=0
      .w_DFUMTRIS=space(5)
      .w_DFTEMRIS=0
      .w_UMTDEF=space(5)
      .w_QTARIS=0
      .w_CONLAV=0
      .w_DFTMPSEC=0
      .w_DFNUMIMP=0
      .w_CSFLAPRE=space(1)
      .w_DFINTEST=space(1)
      .w_DESRIS=space(40)
      .w_TPFIL=space(10)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_DFCODCLA))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_DFCODCLA))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,5,.f.)
        .w_DF__TIPO = 'CL'
        .w_DFCODRIS = iif(empty(.w_CLARIS),.w_DFCODRIS,.w_CLARIS)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_DFCODRIS))
        .link_2_6('Full')
      endif
        .w_DFTIPTEM = 'L'
      .DoRTCalc(9,10,.f.)
      if not(empty(.w_DFUMTRIS))
        .link_2_9('Full')
      endif
      .DoRTCalc(11,14,.f.)
        .w_DFTMPSEC = .w_DFTEMRIS*.w_CONLAV
        .w_DFNUMIMP = 1
        .w_CSFLAPRE = 'S'
      .DoRTCalc(18,20,.f.)
        .w_TPFIL = .w_DF__TIPO
    endwith
    this.DoRTCalc(22,23,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DFCODCLA = t_DFCODCLA
    this.w_DFCODCLA = t_DFCODCLA
    this.w_CLARIS = t_CLARIS
    this.w_DF__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.RadioValue(.t.)
    this.w_DFCODRIS = t_DFCODRIS
    this.w_DFTIPTEM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.RadioValue(.t.)
    this.w_DFQTARIS = t_DFQTARIS
    this.w_DFUMTRIS = t_DFUMTRIS
    this.w_DFTEMRIS = t_DFTEMRIS
    this.w_UMTDEF = t_UMTDEF
    this.w_QTARIS = t_QTARIS
    this.w_CONLAV = t_CONLAV
    this.w_DFTMPSEC = t_DFTMPSEC
    this.w_DFNUMIMP = t_DFNUMIMP
    this.w_CSFLAPRE = t_CSFLAPRE
    this.w_DFINTEST = t_DFINTEST
    this.w_DESRIS = t_DESRIS
    this.w_TPFIL = t_TPFIL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DFCODCLA with this.w_DFCODCLA
    replace t_DFCODCLA with this.w_DFCODCLA
    replace t_CLARIS with this.w_CLARIS
    replace t_DF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_5.ToRadio()
    replace t_DFCODRIS with this.w_DFCODRIS
    replace t_DFTIPTEM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPTEM_2_7.ToRadio()
    replace t_DFQTARIS with this.w_DFQTARIS
    replace t_DFUMTRIS with this.w_DFUMTRIS
    replace t_DFTEMRIS with this.w_DFTEMRIS
    replace t_UMTDEF with this.w_UMTDEF
    replace t_QTARIS with this.w_QTARIS
    replace t_CONLAV with this.w_CONLAV
    replace t_DFTMPSEC with this.w_DFTMPSEC
    replace t_DFNUMIMP with this.w_DFNUMIMP
    replace t_CSFLAPRE with this.w_CSFLAPRE
    replace t_DFINTEST with this.w_DFINTEST
    replace t_DESRIS with this.w_DESRIS
    replace t_TPFIL with this.w_TPFIL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsci_mdfPag1 as StdContainer
  Width  = 793
  height = 254
  stdWidth  = 793
  stdheight = 254
  resizeXpos=222
  resizeYpos=126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=8, width=779,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Posiz.",Field2="DFCODCLA",Label2="Classe",Field3="DF__TIPO",Label3="Tipo ris.",Field4="DFCODRIS",Label4="Risorsa",Field5="DFTIPTEM",Label5="Tipo",Field6="DFQTARIS",Label6="Q.t�",Field7="DFUMTRIS",Label7="U.M.",Field8="DFTEMRIS",Label8="Tempo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235742330

  add object oStr_1_2 as StdString with uid="LOIJXCTJSJ",Visible=.t., Left=552, Top=228,;
    Alignment=1, Width=85, Height=18,;
    Caption="Num. impronte:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=2,top=27,;
    width=775+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=3,top=28,width=774+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CLR_DETT|RIS_ORSE|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDFNUMIMP_2_15.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CLR_DETT'
        oDropInto=this.oBodyCol.oRow.oDFCODCLA_2_2
      case cFile='RIS_ORSE'
        oDropInto=this.oBodyCol.oRow.oDFCODRIS_2_6
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oDFUMTRIS_2_9
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDFNUMIMP_2_15 as StdTrsField with uid="EZDKVGOTIQ",rtseq=16,rtrep=.t.,;
    cFormVar="w_DFNUMIMP",value=0,;
    HelpContextID = 111139450,;
    cTotal="", bFixedPos=.t., cQueryName = "DFNUMIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=642, Top=226, cSayPict=["9999"], cGetPict=["9999"]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsci_mdfPag2 as StdContainer
    Width  = 793
    height = 254
    stdWidth  = 793
    stdheight = 254
  resizeXpos=700
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="LQTTJPYGZJ",left=3, top=3, width=610, height=248, bOnScreen=.t.;

enddefine

  define class tgsci_mdfPag3 as StdContainer
    Width  = 793
    height = 254
    stdWidth  = 793
    stdheight = 254
  resizeXpos=772
  resizeYpos=177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="CZPHZATXKO",left=1, top=2, width=563, height=247, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsci_mou",lower(this.oContained.GSCI_MOU.class))=0
        this.oContained.GSCI_MOU.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsci_mdfPag4 as StdContainer
    Width  = 793
    height = 254
    stdWidth  = 793
    stdheight = 254
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFISTLAV_6_1 as StdMemo with uid="HLAGZVMVLO",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DFISTLAV", cQueryName = "DFISTLAV",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 214816140,;
   bGlobalFont=.t.,;
    Height=251, Width=793, Left=-1, Top=4
enddefine

* --- Defining Body row
define class tgsci_mdfBodyRow as CPBodyRowCnt
  Width=765
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="FWELVOFWDW",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Posizione",;
    HelpContextID = 268070762,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oDFCODCLA_2_2 as StdTrsField with uid="OACPVCQQTQ",rtseq=3,rtrep=.t.,;
    cFormVar="w_DFCODCLA",value=space(5),;
    ToolTipText = "Classe risorse",;
    HelpContextID = 221678217,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=34, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLR_DETT", oKey_1_1="CSFLAPRE", oKey_1_2="this.w_CSFLAPRE", oKey_2_1="CSCODCLA", oKey_2_2="this.w_DFCODCLA"

  func oDFCODCLA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFCODCLA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDFCODCLA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLR_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStrODBC(this.Parent.oContained.w_CSFLAPRE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSFLAPRE="+cp_ToStr(this.Parent.oContained.w_CSFLAPRE)
    endif
    do cp_zoom with 'CLR_DETT','*','CSFLAPRE,CSCODCLA',cp_AbsName(this.parent,'oDFCODCLA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Classi di risorse",'',this.parent.oContained
  endproc

  add object oDF__TIPO_2_5 as stdTrsTableCombo with uid="LSLBTWXVCF",rtrep=.t.,;
    cFormVar="w_DF__TIPO", tablefilter="" , ;
    ToolTipText = "Tipologia risorsa",;
    HelpContextID = 165361029,;
    Height=22, Width=118, Left=101, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Seleziona il tipo';
, bIsInHeader=.f.;
    , cTable='..\PRFA\EXE\QUERY\GSCI_QLT.vqr',cKey='TRCODICE',cValue='TRDESCRI',cOrderBy='',xDefault=space(2);
  , bGlobalFont=.t.



  func oDF__TIPO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DFCODRIS)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDFCODRIS_2_6 as StdTrsField with uid="FCLZXQBCOW",rtseq=7,rtrep=.t.,;
    cFormVar="w_DFCODRIS",value=space(20),;
    ToolTipText = "Codice risorsa",;
    HelpContextID = 238455415,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Tipo risorsa non valido oppure risorsa inestente.",;
   bGlobalFont=.t.,;
    Height=17, Width=168, Left=223, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="RIS_ORSE", oKey_1_1="RL__TIPO", oKey_1_2="this.w_DF__TIPO", oKey_2_1="RLCODICE", oKey_2_2="this.w_DFCODRIS"

  func oDFCODRIS_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFCODRIS_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDFCODRIS_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_DF__TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_DF__TIPO)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oDFCODRIS_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse",'GSCI_ZST.RIS_ORSE_VZM',this.parent.oContained
  endproc

  add object oDFTIPTEM_2_7 as StdTrsCombo with uid="ZQIKTPFBRG",rtrep=.t.,;
    cFormVar="w_DFTIPTEM", RowSource=""+"Lavorazione,"+"Setup,"+"Warm Up" , ;
    ToolTipText = "Tipo di tempo della risorsa",;
    HelpContextID = 75793795,;
    Height=22, Width=111, Left=396, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDFTIPTEM_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFTIPTEM,&i_cF..t_DFTIPTEM),this.value)
    return(iif(xVal =1,'L',;
    iif(xVal =2,'S',;
    iif(xVal =3,'W',;
    space(1)))))
  endfunc
  func oDFTIPTEM_2_7.GetRadio()
    this.Parent.oContained.w_DFTIPTEM = this.RadioValue()
    return .t.
  endfunc

  func oDFTIPTEM_2_7.ToRadio()
    this.Parent.oContained.w_DFTIPTEM=trim(this.Parent.oContained.w_DFTIPTEM)
    return(;
      iif(this.Parent.oContained.w_DFTIPTEM=='L',1,;
      iif(this.Parent.oContained.w_DFTIPTEM=='S',2,;
      iif(this.Parent.oContained.w_DFTIPTEM=='W',3,;
      0))))
  endfunc

  func oDFTIPTEM_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDFQTARIS_2_8 as StdTrsField with uid="MTOKYJHNVV",rtseq=9,rtrep=.t.,;
    cFormVar="w_DFQTARIS",value=0,;
    HelpContextID = 241216119,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=512, Top=0, cSayPict=["@Z 999.99"], cGetPict=["99999.99"]

  func oDFQTARIS_2_8.mCond()
    with this.Parent.oContained
      return (! .w_DF__TIPO $ 'MA-AR-RE')
    endwith
  endfunc

  func oDFQTARIS_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DFQTARIS<=.w_QTARIS)
    endwith
    return bRes
  endfunc

  add object oDFUMTRIS_2_9 as StdTrsField with uid="NKTHKPIPRE",rtseq=10,rtrep=.t.,;
    cFormVar="w_DFUMTRIS",value=space(5),;
    ToolTipText = "UM tempi",;
    HelpContextID = 221735543,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura non gestita a tempo oppure senza conversione in secondi",;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=561, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMFLTEMP", oKey_1_2="this.w_UMFLTEMP", oKey_2_1="UMCODICE", oKey_2_2="this.w_DFUMTRIS"

  proc oDFUMTRIS_2_9.mDefault
    with this.Parent.oContained
      if empty(.w_DFUMTRIS)
        .w_DFUMTRIS = .w_UMTDEF
      endif
    endwith
  endproc

  func oDFUMTRIS_2_9.mCond()
    with this.Parent.oContained
      return (.w_DFQTARIS>0)
    endwith
  endfunc

  func oDFUMTRIS_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFUMTRIS_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDFUMTRIS_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UNIMIS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStrODBC(this.Parent.oContained.w_UMFLTEMP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UMFLTEMP="+cp_ToStr(this.Parent.oContained.w_UMFLTEMP)
    endif
    do cp_zoom with 'UNIMIS','*','UMFLTEMP,UMCODICE',cp_AbsName(this.parent,'oDFUMTRIS_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCI_ZUM.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oDFTEMRIS_2_10 as StdTrsField with uid="QGNTQMRSQM",rtseq=11,rtrep=.t.,;
    cFormVar="w_DFTEMRIS",value=0,;
    ToolTipText = "Tempo risorsa",;
    HelpContextID = 229603959,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=621, Top=0, cSayPict=["@Z 99999.999"], cGetPict=["@Z 99999.999"]

  func oDFTEMRIS_2_10.mCond()
    with this.Parent.oContained
      return (.w_DFQTARIS>0 and not empty(.w_DFUMTRIS))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsci_mdf','DET_FASI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCODICE=DET_FASI.DFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
