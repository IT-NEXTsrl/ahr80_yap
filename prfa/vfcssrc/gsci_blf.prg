* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsci_blf                                                        *
*              Controlli lista fornitori                                       *
*                                                                              *
*      Author: Zucchetti TAM SpA (SM)                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-21                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsci_blf",oParentObject)
return(i_retval)

define class tgsci_blf as StdBatch
  * --- Local variables
  w_MESSAGE = space(100)
  w_PUNPAD = .NULL.
  w_cCURSOR = space(10)
  w_cPOS = 0
  w_cROWS = 0
  * --- WorkFile variables
  LIS_FORN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se esiste un solo fornitore preferenziale
    this.w_MESSAGE = "z"
    this.w_PUNPAD = this.oParentObject
    this.w_cCURSOR = this.w_PUNPAD.cTrsName
    this.w_cPOS = 0
    this.w_cROWS = 0
    if this.w_PUNPAD.oParentObject.w_RLINTEST="E"
      * --- Memorizza posizione
      select (this.w_cCURSOR)
      this.w_cPOS = recno()
      * --- Conta preferenziali
      select * from (this.w_cCURSOR) where t_LFINDPRE=0 and not empty(t_LFCODFOR) into cursor __Conta__
      if used("__Conta__")
        this.w_cROWS = _TALLY
        USE IN __Conta__
      else
        this.w_cROWS = -1
      endif
      * --- Ripristina posizione
      if this.w_cPOS>0
        select (this.w_cCURSOR)
        goto this.w_cPOS
      endif
      do case
        case this.w_cROWS=0
          this.w_MESSAGE = "Occorre indicare un fornitore preferenziale (indice 0)"
        case this.w_cROWS>1
          this.w_MESSAGE = "Occorre indicare un solo fornitore preferenziale (indice 0)"
      endcase
      * --- Messaggio errore ?
      if this.w_MESSAGE <>"z"
        ah_ErrorMsg(this.w_MESSAGE,48)
        this.oParentObject.w_CHECKFORM = .F.
      endif
    endif
    * --- Non esegue mcalc sul padre
     this.bUpdateParentObject = .f.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIS_FORN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
