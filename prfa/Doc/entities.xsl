<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link dei tag PlanItem -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Plan"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <HTML>
  <HEAD>
    <STYLE>
      BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
      TABLE {font-family:Times New Roman; color:black; background:white}
      A:hover {color:red}
    </STYLE>
  </HEAD>
  <BODY>
    <P ALIGN="CENTER"><FONT COLOR="#000080" SIZE="6"><B>Entities</B></FONT></P>
    <xsl:apply-templates select="PlanItemlist/PlanItem[ItemObj!='Group']">
      <xsl:sort select="ItemObj"/>
      <xsl:sort select="ItemName"/>
    </xsl:apply-templates>
    <DIV ALIGN="CENTER">
      <A HREF="{PlanName}_Main{$default_extension}">Project overview</A>
    </DIV>
  </BODY>
  </HTML>
</xsl:template>


<xsl:template match="PlanItem[ItemDesc]">
  <P>
  <TABLE BORDER="0" CELLSPACING ="0">
    <xsl:variable name="table_link">
      <xsl:value-of select="/Plan/PlanName"/>
      <xsl:text>_TablesSummary</xsl:text>
      <xsl:value-of select="$default_extension"/>
      <xsl:text>#Table_</xsl:text>
    </xsl:variable>

    <TR>
      <TD WIDTH="40" ALIGN="LEFT" VALIGN="MIDDLE">
        <xsl:choose>
          <xsl:when test="ItemObj='Table'">
            <A HREF="{$table_link}{ItemTable/TableName}">
              <IMG BORDER="0" SRC="ds_Tbl.gif"
                   ALT="Show Table" WIDTH="40" HEIGHT="40"/>
            </A>
          </xsl:when>

          <xsl:when test="ItemObj='DataTable'">
            <A HREF="{$table_link}{ItemTable/TableName}">
              <xsl:choose>
                <xsl:when test="ItemDesc/ItemDescLinkable='TRUE'">
                  <IMG BORDER="0" SRC="ds_StorageExtLink.gif"
                       ALT="Show Database Table" WIDTH="40" HEIGHT="40"/>
                </xsl:when>
                <xsl:otherwise>
                  <IMG BORDER="0" SRC="ds_Storage.gif"
                       ALT="Show Database Table" WIDTH="40" HEIGHT="40"/>
                </xsl:otherwise>
              </xsl:choose>
            </A>
          </xsl:when>

          <xsl:when test="ItemObj='Master'">
            <A HREF="{$table_link}{ItemTable/TableName}">
              <xsl:choose>
                <xsl:when test="ItemDesc/ItemDescLinkable='TRUE'">
                  <IMG BORDER="0" SRC="ds_MstExtLink.gif"
                       ALT="Show Master" WIDTH="40" HEIGHT="40"/>
                </xsl:when>
                <xsl:otherwise>
                  <IMG BORDER="0" SRC="ds_Mst.gif"
                       ALT="Show Master" WIDTH="40" HEIGHT="40"/>
                </xsl:otherwise>
              </xsl:choose>
            </A>
          </xsl:when>

          <xsl:when test="ItemObj='MasterDetail'">
            <A HREF="{$table_link}{ItemTable/TableName}">
              <xsl:choose>
                <xsl:when test="ItemDesc/ItemDescLinkable='TRUE'">
                  <IMG BORDER="0" SRC="ds_MDtExtLink.gif"
                       ALT="Show Master Detail" WIDTH="40" HEIGHT="40"/>
                </xsl:when>
                <xsl:otherwise>
                <IMG BORDER="0" SRC="ds_MDt.gif"
                     ALT="Show Master Detail" WIDTH="40" HEIGHT="40"/>
                </xsl:otherwise>
              </xsl:choose>
            </A>
          </xsl:when>

          <xsl:when test="ItemObj='Detail'">
            <A HREF="{$table_link}{ItemTable/TableName}">
              <xsl:choose>
                <xsl:when test="ItemDesc/ItemDescLinkable='TRUE'">
                  <IMG BORDER="0" SRC="ds_DtlExtLink.gif"
                       ALT="Show Detail" WIDTH="40" HEIGHT="40"/>
                </xsl:when>
                <xsl:otherwise>
                  <IMG BORDER="0" SRC="ds_Dtl.gif"
                       ALT="Show Detail" WIDTH="40" HEIGHT="40"/>
                </xsl:otherwise>
              </xsl:choose>
            </A>
          </xsl:when>

          <xsl:when test="ItemObj='Batch'">
            <IMG BORDER="0" SRC="ds_Btc.gif" ALT="Routine" WIDTH="40" HEIGHT="40"/>
          </xsl:when>

          <xsl:when test="ItemObj='Mask'">
            <IMG BORDER="0" SRC="ds_Msk.gif" ALT="Mask" WIDTH="40" HEIGHT="40"/>
          </xsl:when>

          <xsl:when test="ItemObj='Report'">
            <IMG BORDER="0" SRC="ds_Rpt.gif" ALT="Report" WIDTH="40" HEIGHT="40"/>
          </xsl:when>

          <xsl:when test="ItemObj='Output'">
            <IMG BORDER="0" SRC="ds_Output.gif" ALT="Output" WIDTH="40" HEIGHT="40"/>
          </xsl:when>

          <xsl:when test="ItemObj='Page'">
            <IMG BORDER="0" SRC="ds_Page.gif" ALT="Page" WIDTH="40" HEIGHT="40"/>
          </xsl:when>
        </xsl:choose>
      </TD>

      <TD ALIGN="LEFT" VALIGN="MIDDLE">
        <A>
          <xsl:if test="ItemObj='DataTable' or ItemObj='Master' or ItemObj='Detail' or ItemObj='Table'">
            <xsl:attribute name="HREF">
              <xsl:value-of select="$table_link"/>
              <xsl:value-of select="ItemTable/TableName"/>
            </xsl:attribute>
          </xsl:if>
          <FONT SIZE="5"><B><xsl:value-of select="ItemDesc/ItemDescName"/></B></FONT>
        </A>
      </TD>

      <xsl:if test="ItemDesc/ItemDescExternal='TRUE'">
        <TD VALIGN="MIDDLE">&#160;(External entity)</TD>
      </xsl:if>

      <xsl:if test="ItemObj='MasterDetail'">
        <TD>
          <TABLE BORDER="0" CELLSPACING="2">
            <TR><TD>
              <A HREF="{$table_link}{ItemTable/TableName[@Type='Master']}">Master</A>
            </TD></TR>
            <TR><TD>
              <A HREF="{$table_link}{ItemTable/TableName[@Type='Detail']}">Detail</A>
            </TD></TR>
          </TABLE>
        </TD>
      </xsl:if>
      <A NAME="{ItemDesc/ItemDescName}"/>
    </TR>
  </TABLE>

  <TABLE WIDTH="100%">
    <TR>
      <TD COLSPAN="3">&#160;</TD>
    </TR>
    <TR>
      <TD WIDTH="5%">&#160;</TD>
      <TD><TABLE CELLSPACING="0">
        <TR>
          <TD WIDTH="16%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;" ><B>Program</B></TD>
          <xsl:if test="ItemDesc/ItemDescTemplate!=''">
            <TD WIDTH="15%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;"><B>Template</B></TD>
          </xsl:if>
          <TD WIDTH="13%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;"><B>Created</B></TD>
          <TD WIDTH="13%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;"><B>Revised</B></TD>
          <TD WIDTH="5%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;" ALIGN="CENTER"><B>Menu</B></TD>
          <TD WIDTH="5%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;" ALIGN="CENTER"><B>Prototype</B></TD>
          <xsl:if test="ItemDesc/ItemDescTmpTable!=''">
            <TD WIDTH="5%"  STYLE="BORDER-BOTTOM: 1px SOLID #000000;" ALIGN="CENTER"><B>Tmp Table</B></TD>
          </xsl:if>
          <xsl:if test="ItemDesc/ItemDescPublic!=''">
            <TD WIDTH="5%" STYLE="BORDER-BOTTOM: 1px SOLID #000000;" ALIGN="CENTER"><B>Public</B></TD>
          </xsl:if>
        </TR>
        <TR>
          <TD WIDTH="16%"><xsl:value-of select="ItemDesc/ItemDescProcedure"/></TD>
          <xsl:if test="ItemDesc/ItemDescTemplate!=''">
            <TD WIDTH="15%"><xsl:value-of select="ItemDesc/ItemDescTemplate"/></TD>
          </xsl:if>
          <TD WIDTH="13%"><xsl:value-of select="ItemDesc/ItemDescCreated"/></TD>
          <TD WIDTH="13%"><xsl:value-of select="ItemDesc/ItemDescRevised"/></TD>
          <TD WIDTH="5%" ALIGN="CENTER"><xsl:apply-templates select="ItemDesc/ItemDescMenu"/></TD>
          <TD WIDTH="5%" ALIGN="CENTER"><xsl:apply-templates select="ItemDesc/ItemDescPrototype"/></TD>
          <xsl:if test="ItemDesc/ItemDescTmpTable!=''">
            <TD WIDTH="5%" ALIGN="CENTER"><xsl:apply-templates select="ItemDesc/ItemDescTmpTable"/></TD>
          </xsl:if>
          <xsl:if test="ItemDesc/ItemDescPublic!=''">
            <TD WIDTH="5%" ALIGN="CENTER"><xsl:apply-templates select="ItemDesc/ItemDescPublic"/></TD>
          </xsl:if>
        </TR>
      </TABLE></TD>
      <TD WIDTH="5%">&#160;</TD>
    </TR>
  </TABLE>

  <xsl:if test="ItemDesc/ItemDescNote!=''">
    <TABLE BORDER="0" WIDTH="100%">
      <TR>
        <TD COLSPAN="3">&#160;</TD>
      </TR>
      <TR>
        <TD WIDTH="5%">&#160;</TD>
        <TD><TABLE WIDTH="100%">
          <TR>
            <TD ALIGN="CENTER"><B>Notes</B></TD>
          </TR>
          <TR>
            <TD STYLE="BORDER-TOP: 2px SOLID #000000;">
              <xsl:apply-templates select="ItemDesc/ItemDescNote"/>
            </TD>
          </TR>
        </TABLE></TD>
        <TD WIDTH="5%">&#160;</TD>
      </TR>
    </TABLE>
  </xsl:if>

  <xsl:if test="ItemLinks/Link">
    <TABLE BORDER="0" WIDTH="100%">
      <TR>
        <TD COLSPAN="3">&#160;</TD>
      </TR>
      <TR>
        <TD WIDTH="5%">&#160;</TD>
        <TD><TABLE CELLSPACING="0" BORDER="0" WIDTH="100%">
          <TR>
            <TD COLSPAN="4" ALIGN="CENTER" STYLE="BORDER-BOTTOM: 2px SOLID #000000;">
              <B>Relationships</B>
            </TD>
          </TR>
          <TR>
            <TD VALIGN="TOP">
              <TABLE WIDTH="100%" BORDER="0" cellspacing="0" cellpadding="2">
                <xsl:for-each select="ItemLinks/Link">
                  <xsl:sort select="Type"/>
                  <xsl:if test="not(preceding-sibling::node()/Type=Type)">
                    <TR><TD HEIGHT="25" COLSPAN="2" VALIGN="BOTTOM"
                            STYLE="BORDER-BOTTOM: 1px SOLID #000000;">
                      <B><xsl:value-of select="Type"/></B>
                    </TD></TR>
                  </xsl:if>
                  <TR>
                    <TD WIDTH="30%" VALIGN="MIDDLE">
                      <xsl:variable name="linked"
                                    select="/Plan/PlanItemlist/PlanItem
                                            [ItemUID=current()/UID]/ItemName"/>
                      <A HREF="#{$linked}">
                        <xsl:value-of select="$linked"/>
                      </A>
                    </TD>
                    <TD WIDTH="70%" VALIGN="MIDDLE"
                        STYLE="BORDER-LEFT: 1px SOLID #000000;">
                      <xsl:apply-templates select="Note"/>
                    </TD>
                  </TR>
                </xsl:for-each>
              </TABLE>
            </TD>
          </TR>
        </TABLE></TD>
        <TD WIDTH="5%">&#160;</TD>
      </TR>
    </TABLE>
  </xsl:if>
  </P>
</xsl:template>


<!-- Gestione checkbox TRUE/FALSE -->

<xsl:template match="ItemDescMenu | ItemDescPrototype | ItemDescTmpTable | ItemDescPublic">
  <xsl:call-template name="LogicalCheckBox"/>
</xsl:template>


</xsl:stylesheet>
