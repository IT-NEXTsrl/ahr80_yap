* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bpf                                                        *
*              Elabora fattibilitą                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2000-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bpf",oParentObject)
return(i_retval)

define class tgsds_bpf as StdBatch
  * --- Local variables
  w_COMDIS = space(1)
  w_SERPIA = space(10)
  w_NUMREG = 0
  w_ALFREG = space(2)
  w_DATREG = ctod("  /  /  ")
  w_DESCRI = space(40)
  w_DATSTA = ctod("  /  /  ")
  w_CODMAG = space(5)
  w_CODAZI = space(5)
  w_FLVERI = space(1)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_ONUME = 0
  w_ODES = space(50)
  w_OPRG = space(30)
  * --- WorkFile variables
  OUT_PUTS_idx=0
  PAR_DISB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Fattibilita' (da GSDS_MPP)
    if EMPTY(this.oParentObject.w_PPSERIAL)
      ah_ErrorMsg("Nessun piano di produzione selezionato",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Variabili da Passare al Batch di Elaborazione/Stampa
    * --- Inizializza
    this.w_SERPIA = this.oParentObject.w_PPSERIAL
    this.w_NUMREG = this.oParentObject.w_PPNUMREG
    this.w_ALFREG = this.oParentObject.w_PPALFREG
    this.w_DATREG = this.oParentObject.w_PPDATREG
    this.w_DESCRI = this.oParentObject.w_PPDESCRI
    this.w_DATSTA = i_DATSYS
    this.w_CODMAG = SPACE(5)
    this.w_OPRG = LEFT("GSDS_SPP"+ SPACE(30),30)
    this.w_CODAZI = i_CODAZI
    this.w_FLVERI = " "
    this.w_COMDIS = " "
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2],.t.,this.PAR_DISB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDFLVERI"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDFLVERI;
        from (i_cTable) where;
            PDCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLVERI = NVL(cp_ToDate(_read_.PDFLVERI),cp_NullValue(_read_.PDFLVERI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ONUME = IIF(this.w_FLVERI="C", 4, 2)
    this.w_OREP = " "
    this.w_OQRY = " "
    this.w_ODES = " "
    * --- Read from OUT_PUTS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OUNOMQUE,OUNOMREP,OUDESCRI"+;
        " from "+i_cTable+" OUT_PUTS where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_OPRG);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_ONUME);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OUNOMQUE,OUNOMREP,OUDESCRI;
        from (i_cTable) where;
            OUNOMPRG = this.w_OPRG;
            and OUROWNUM = this.w_ONUME;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OQRY = NVL(cp_ToDate(_read_.OUNOMQUE),cp_NullValue(_read_.OUNOMQUE))
      this.w_OREP = NVL(cp_ToDate(_read_.OUNOMREP),cp_NullValue(_read_.OUNOMREP))
      this.w_ODES = NVL(cp_ToDate(_read_.OUDESCRI),cp_NullValue(_read_.OUDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lancia il Batch (Attenzione, viene eseguito anche dalla Maschera: GSDS_SPP)
    do gsds_bpp with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OUT_PUTS'
    this.cWorkTables[2]='PAR_DISB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
