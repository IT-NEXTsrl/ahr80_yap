* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds2bdc                                                        *
*              Eventi da determinazione costo standard                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-04                                                      *
* Last revis.: 2014-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds2bdc",oParentObject,m.pParam)
return(i_retval)

define class tgsds2bdc as StdBatch
  * --- Local variables
  pParam = space(0)
  CurDis = space(10)
  w_MESS = space(10)
  w_zCurs = space(10)
  CurDis = space(10)
  Padre = .NULL.
  w_cQuery = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contiene query originale dello zoom in caso di errore mi serve per ripristinare lo zoom
    * --- Variabile che contiene il il percorso+nome della query da utilizzare come filtro
    * --- Zoom di Selezione
    this.Padre = this.oParentObject
    do case
      case this.pParam="ANTEPRIMA"
        L_CurDis = SPACE(10)
        if !Empty(this.oParentObject.w_OUNOMQUE)
          * --- Controllo sul risultato della query immessa dall'utente per quanto riguarda
          *     i filtri di selezione impostati sulla distinta base
          this.CurDis = SYS(2015)
          this.w_cQuery = ADDBS(JUSTPATH(Alltrim(this.oParentObject.w_OUNOMQUE)))+JUSTSTEM(Alltrim(this.oParentObject.w_OUNOMQUE))
          vq_exec(Alltrim(this.oParentObject.w_OUNOMQUE), this, this.CurDis)
          if Used (this.CurDis )
            L_CurDis = this.CurDis
            if Type("&L_CurDis..DISTINTA") <> "U" And Type("&L_CurDis..DESCRIZIONE") <> "U"
              if Type("&L_CurDis..DISTINTA") <> "C" Or Type("&L_CurDis..DESCRIZIONE") <> "C" 
                this.w_MESS = ah_msgformat("Il tipo di dato contenuto nel campo distinta deve essere di tipo carattere")
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                i_retcode = 'stop'
                return
              endif
            else
              this.w_MESS = ah_msgformat("L'alias del campo che contiene il valore relativo al codice della distinta base si deve chiamare %1", "'DISTINTA'")
              ah_ErrorMsg(this.w_MESS, 16)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
          else
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Raise
            i_Error=AH_MsgFormat("Errore esecuzione query")
            return
          endif
          if USED( L_CurDis )
            use in ( L_CurDis )
          endif
          this.w_cQuery = ADDBS(JUSTPATH(Alltrim(this.oParentObject.w_OUNOMQUE)))+JUSTSTEM(Alltrim(this.oParentObject.w_OUNOMQUE))
          this.oParentObject.w_SelDis.cCpQueryName = this.w_cQuery
          this.oParentObject.w_SelDis.cWhere = ""
          this.Padre.Notifyevent("Interroga")     
          Private l_olderr
          l_olderr=on("ERROR")
          on error=.t.
          this.Padre.oPgfrm.Page2.Enabled = .t.
          this.Padre.oPgfrm.Activepage = 2
          this.oParentObject.w_SelDis.grd.setfocus()     
          on error &l_olderr
          this.Padre.Refresh()     
          if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
            this.Padre.Notifyevent("Interroga")     
            this.oParentObject.w_SELEZI = "S"
            this.Padre.Notifyevent("w_SELEZI Changed")     
          endif
        else
          this.w_cQuery = ADDBS(JUSTPATH(Alltrim(this.oParentObject.w_ORIQUERY)))+JUSTSTEM(Alltrim(this.oParentObject.w_ORIQUERY))
          this.oParentObject.w_SelDis.cCpQueryName = this.w_cQuery
          this.oParentObject.w_SelDis.cWhere = "1=0"
          this.Padre.Notifyevent("Interroga")     
          this.oParentObject.w_SELEZI = "D"
          this.Padre.oPgfrm.Page2.Enabled = .f.
          this.Padre.oPgfrm.Activepage = 1
          this.Padre.Refresh()     
        endif
      case this.pParam="SELDESEL"
        this.w_zCurs = this.oParentObject.w_SelDis.cCursor
        do case
          case this.oParentObject.w_SELEZI="S"
            Update (this.w_zCurs ) set xchk=1
          case this.oParentObject.w_SELEZI="D"
            Update (this.w_zCurs ) set xchk=0
          case this.oParentObject.w_SELEZI="Z"
            Update (this.w_zCurs ) set xchk=iif(xchk=0, 1, 0)
        endcase
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Appare messaggio di errore e resetta le impostazioni originali dello zoom
    ah_ErrorMsg(this.w_MESS, 16)
    * --- Ripristino Impostazioni originali dello zoom
    this.oParentObject.w_OUNOMQUE = SPACE(0)
    this.w_cQuery = ADDBS(JUSTPATH(Alltrim(this.oParentObject.w_ORIQUERY)))+JUSTSTEM(Alltrim(this.oParentObject.w_ORIQUERY))
    this.oParentObject.w_SelDis.cCpQueryName = this.w_cQuery
    this.oParentObject.w_SelDis.cWhere = "1=0"
    this.Padre.Notifyevent("Interroga")     
    this.Padre.oPgfrm.Page2.Enabled = .f.
    this.Padre.oPgfrm.Activepage = 1
    this.Padre.Refresh()     
    if USED( L_CurDis )
      use in ( L_CurDis )
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
