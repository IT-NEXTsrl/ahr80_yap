* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bim                                                        *
*              Importa ordini sul piano di produzione                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-23                                                      *
* Last revis.: 2000-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bim",oParentObject,m.pOper)
return(i_retval)

define class tgsds_bim as StdBatch
  * --- Local variables
  pOper = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lettura Ordini da Importare nel Piano di Produzione
    ND = this.oParentObject.w_ZoomDoc.cCursor
    do case
      case this.pOper="R"
        * --- Ricerca
        vq_exec("..\DISB\EXE\QUERY\GSDS2KIM.VQR", this,"SALDIART")
        this.oParentObject.NotifyEvent("Ricerca")
        * --- Filtra solo gli articoli non Disponibili
        SELECT &ND..CODCOM AS CODCOM, &ND..CODART AS CODART, ; 
 MAX(&ND..DESART) AS DESART, MAX(&ND..UNMIS1) AS UNMIS1, ; 
 MAX(NVL(SALDIART.DISMIN, 0)-NVL(SALDIART.DISCON, 0)) AS QTAUM1, ; 
 MAX(&ND..OPERAT) AS OPERAT, MAX(&ND..MOLTIP) AS MOLTIP, MAX(&ND..UNMIS2) AS UNMIS2, ; 
 MAX(&ND..CODDIS) AS CODDIS, ; 
 MAX(&ND..UNMIS3) AS UNMIS3, MAX(&ND..OPERA3) AS OPERA3, MAX(&ND..MOLTI3) AS MOLTI3, ; 
 MAX(&ND..MVSERIAL) AS MVSERIAL,MAX(&ND..MVNUMRIF) AS MVNUMRIF,; 
 MAX(&ND..XCHK) AS XCHK ;
        FROM ((ND) INNER JOIN SALDIART ON &ND..CODART = SALDIART.CODART) ;
        GROUP BY &ND..CODCOM, &ND..CODART ORDER BY 1 INTO CURSOR APPODOC
        if USED("SALDIART")
          SELECT SALDIART
          USE
        endif
        * --- Riaggiorna il cursore dello Zoom
        SELECT (ND)
        DELETE ALL
        SELECT APPODOC
        GO TOP
        SCAN
        SCATTER MEMVAR
        SELECT (ND)
        APPEND BLANK
        GATHER MEMVAR
        ENDSCAN
        if USED("APPODOC")
          SELECT APPODOC
          USE
        endif
        SELECT (ND)
        GO TOP
        this.oParentObject.w_ZoomDoc.refresh()
      case this.pOper="S"
        * --- Seleziona /Deseleziona
        * --- Evento w_SELEZI Changed
        if USED(this.oParentObject.w_ZoomDoc.cCursor)
          if this.oParentObject.w_SELEZI = "S"
            * --- Seleziona Tutto
            UPDATE (ND) SET XCHK=1
          else
            * --- deseleziona Tutto
            UPDATE (ND) SET XCHK=0
          endif
          SELECT (ND)
          GO TOP
          this.oParentObject.w_ZoomDoc.refresh()
        endif
      case this.pOper="U"
        * --- Conferma Import
        if USED(this.oParentObject.w_ZoomDoc.cCursor)
          SELECT (ND)
          GO TOP
          SELECT * FROM (ND) WHERE XCHK=1 INTO CURSOR RigheOrd
          this.oParentObject.oParentObject.NotifyEvent("AggiornaDaOrdini")
        endif
      case this.pOper="A"
        * --- Abbandona Import (chiude Cursore di Appoggio)
        if USED("RigheOrd")
          SELECT RigheOrd
          USE
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
