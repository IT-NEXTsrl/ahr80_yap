* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds1bdc                                                        *
*              Determinazione costo standard                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-18                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds1bdc",oParentObject,m.w_PARAM1)
return(i_retval)

define class tgsds1bdc as StdBatch
  * --- Local variables
  w_PARAM1 = space(10)
  w_DISPRO = space(1)
  w_FLCOSINV = space(1)
  w_FLDETTMAG = space(1)
  w_TIPCOS = space(1)
  w_FLDESART = space(1)
  w_FLPROV = space(1)
  w_DATSTA = ctod("  /  /  ")
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_Analisi = space(1)
  w_TIPOARTI = space(1)
  w_QUANTI = 0
  w_PERRIC = space(1)
  w_TipoValo = space(1)
  w_Listino = space(5)
  w_ESERC = space(4)
  w_NUMINV = space(6)
  w_VALLIS = space(3)
  w_VALESE = space(3)
  w_SCDRSR = space(1)
  w_TIPOLN = space(1)
  w_CAOVAL = 0
  w_CAOLIS = 0
  w_CODMAG = space(5)
  w_INNUMPRE = space(6)
  w_CAOESE = 0
  w_INDATINV = ctod("  /  /  ")
  w_PRDATINV = ctod("  /  /  ")
  w_PDCOSPAR = space(0)
  w_LOTTOMED = space(1)
  w_TIPVALCIC = space(2)
  w_ESFINESE = ctod("  /  /  ")
  w_STANDARD = .f.
  w_UTENTE = .f.
  w_ULTIMO = .f.
  w_MEDIOPON = .f.
  w_CLISTINO = .f.
  w_AGGSTAND = space(1)
  w_AGGMPOND = space(1)
  w_FLCHKLP = space(1)
  w_NOLIVFIN = space(1)
  w_OUNOMQUE = space(254)
  w_TipoValoOut = space(2)
  w_ListinoMo = space(5)
  w_VALLISMO = space(3)
  w_NUMINVMO = space(6)
  w_ESERCMO = space(4)
  w_maxlvl = 0
  w_codiartico = space(40)
  w_VARIAR = space(20)
  w_mykey = space(10)
  w_CODIAR = space(20)
  w_i = 0
  w_qtaparzin = 0
  w_qtaparzil = 0
  w_qtalmstp = 0
  w_cosselez = 0
  w_cossta = 0
  w_cosmed = 0
  w_cosult = 0
  w_COPERRIC = 0
  w_CODTMP = space(5)
  w_CODLEN = 0
  w_TROVATO = .f.
  w_COECONV = 0
  OutCursor = space(10)
  w_KEYRIF = space(10)
  w_LenLvl = 0
  w_LIVFIN = space(1)
  TipoGestione = space(1)
  w_PARZ = space(1)
  w_PREZUM = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_LOTPRO = 0
  w_CMNETSEL = 0
  w_CMSCASEL = 0
  w_CMRICSEL = 0
  w_CAO = 0
  w_CMNETSTD = 0
  w_CMSCASTD = 0
  w_CMRICSTD = 0
  w_PREZZO = 0
  w_CMNETMED = 0
  w_CMSCAMED = 0
  w_CMRICMED = 0
  w_ERRO = .f.
  w_CMNETULT = 0
  w_CMSCAULT = 0
  w_CMRICULT = 0
  w_CSNETSEL = 0
  w_CSSCASEL = 0
  w_CSRICSEL = 0
  w_CSOVHSEL = 0
  w_PARELAB = .f.
  w_MESS = space(10)
  w_codiartic1 = space(40)
  w_CONTRATTO = .f.
  w_RECADDED = .f.
  w_COFLLOTP = space(1)
  w_CODFOR = space(15)
  w_CODFAS = space(41)
  w_ORAUNI = space(1)
  w_VALCONT = space(3)
  w_PRZSET = 0
  w_CAKEYSAL = space(40)
  w_QTAUM1 = 0
  w_CODARTI = space(20)
  w_TEMP = space(0)
  w_MAGRAG = space(5)
  w_cCoeffImp = space(1)
  w_NCURS = 0
  w_CURSNAME = space(10)
  CurDis = space(10)
  w_NRCURDIS = 0
  w_zCurs = space(10)
  w_ZFILTERS = .f.
  w_PARENTOBJ = .NULL.
  w_CLASS = space(10)
  w_CICLI = .f.
  w_INCONPUN = space(1)
  w_INESCONT = space(1)
  w_COSTILOG = space(1)
  w_NCURS = space(1)
  w_CURSNAME = space(10)
  w_DateTimeStart = space(15)
  w_DateTimeEnd = space(15)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_SMATOU = space(1)
  w_FLCOST = space(1)
  w_UM_ORE = space(3)
  w_VALCOM = space(1)
  w_MAXLEVEL = 0
  w_AGCOSSTD = space(1)
  w_KEYRIFSTO = space(10)
  w_NUMRIGSTO = 0
  w_L_STANDARD = .f.
  w_L_ULTIMO = .f.
  w_L_MEDIOPON = .f.
  w_L_CLISTINO = .f.
  w_bDispMessInv = .f.
  w_bDispMessDatInv = .f.
  w_SELEZ = space(200)
  w_ZFILTERS = .f.
  w_TIPOSTAM = space(1)
  w_LTSRV = space(1)
  w_OLTSRV = space(1)
  w_NUMDIST = 0
  w_PATHDBF = space(5)
  w_MultiRep = .NULL.
  w_VALCLLIS = space(3)
  w_FIRSTCUR = .f.
  w_PRSERIAL = space(10)
  w_OPELOG = space(5)
  w_oMRPLOG = .NULL.
  w_LogTitle = space(10)
  hf_FILE_LOG = 0
  w_FileName = space(50)
  w_LogFileName = space(10)
  w_CursErrorLog = space(10)
  w_cMsg = space(0)
  w_MSGERR = space(250)
  w_DCTINIZ = ctot("")
  w_DCTFINE = ctot("")
  w_DCTTOTA = 0
  w_DateTimeStart = space(15)
  w_DateTimeEnd = space(15)
  w_GENINIZ = ctot("")
  w_GENFINE = ctot("")
  w_GENTOTA = 0
  * --- WorkFile variables
  ART_TEMP_idx=0
  TMPCAMBI_idx=0
  CAM_BI_idx=0
  VALUTE_idx=0
  STOCOSTA_idx=0
  TMPEXPDB_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa costificata della Distinta Base (da GSDB_SDC)
    * --- Variabili utilizzate per l'analisi dei costi
    this.w_PARENTOBJ = this.oParentObject
    * --- Controllo la gestione che chiama la costificazione
    this.w_CLASS = ALLTRIM(UPPER(this.w_PARENTOBJ.CLASS))
    do case
      case this.w_CLASS = "TGSDS_SDC"
        * --- Stampa distinta base Cicli semplificati
        this.w_CICLI = .F.
      case this.w_CLASS = "TGSCI_SDC"
        * --- Stampa distinta base con Cicli di lavorazione
        this.w_CICLI = g_PRFA="S"
      case this.w_CLASS = "TGSDS_KCS"
        * --- Determinazione costo standard (Determina da sola se utilizzare cicli semplificati oppure cicli di lavorazione)
        this.w_CICLI = g_PRFA="S" AND g_CICLILAV="S"
      otherwise
        i_retcode = 'stop'
        return
    endcase
    if FALSE
      * --- Non rimuovere questa istruzione (che non viene eseguita) altrimenti 
      *     il painterr non riconosce che la tabella TMPEXPDB � una tabella temporanea
      * --- Create temporary table TMPEXPDB
      i_nIdx=cp_AddTableDef('TMPEXPDB') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPEXPDB_proto';
            )
      this.TMPEXPDB_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_ZFILTERS = .F.
    this.w_DateTimeStart = TTOC(DATETIME())
    this.OutCursor = SYS(2015)
    this.w_KEYRIF = SYS(2015)
    this.w_LenLvl = 6
    this.w_LIVFIN = " "
    this.w_PARZ = " "
    this.w_PARELAB = type("this.w_PARAM1")="C" and this.w_PARAM1="ELABORA"
    this.w_NOLIVFIN = "N"
    this.w_OUNOMQUE = SPACE(254)
    * --- ----------------------------------------------------------------------------------------------
    * --- Inizializzo le variabili
    this.w_DISPRO = this.w_PARENTOBJ.w_DISPRO
    if !this.w_PARELAB
      this.w_OREP = this.w_PARENTOBJ.w_OREP
      this.w_OQRY = this.w_PARENTOBJ.w_OQRY
    endif
    this.w_FLCOSINV = this.w_PARENTOBJ.w_FLCOSINV
    this.w_FLDETTMAG = this.w_PARENTOBJ.w_FLDETTMAG
    this.w_MAGRAG = this.w_PARENTOBJ.w_MAGRAG
    this.w_cCoeffImp = this.w_PARENTOBJ.w_PDCOECOS
    * --- --
    this.w_FLCOST = "N"
    if !this.w_CICLI or this.w_PARELAB
      this.w_FLCOST = this.w_PARENTOBJ.w_FLCOST
    else
      this.w_UM_ORE = this.w_PARENTOBJ.w_UM_ORE
    endif
    this.w_VALCOM = "S"
    this.w_TIPCOS = this.w_PARENTOBJ.w_TIPCOS
    this.w_MAXLEVEL = IIF(this.w_TIPCOS="S", 99, 999)
    this.w_FLDESART = this.w_PARENTOBJ.w_FLDESART
    this.w_FLPROV = " "
    this.w_DATSTA = this.w_PARENTOBJ.w_DATSTA
    this.w_CODINI = this.w_PARENTOBJ.w_CODINI
    this.w_CODFIN = this.w_PARENTOBJ.w_CODFIN
    this.w_FAMAINI = this.w_PARENTOBJ.w_FAMAINI
    this.w_FAMAFIN = this.w_PARENTOBJ.w_FAMAFIN
    this.w_GRUINI = this.w_PARENTOBJ.w_GRUINI
    this.w_GRUFIN = this.w_PARENTOBJ.w_GRUFIN
    this.w_CATINI = this.w_PARENTOBJ.w_CATINI
    this.w_CATFIN = this.w_PARENTOBJ.w_CATFIN
    this.w_Analisi = this.w_PARENTOBJ.w_Analisi
    this.w_TIPOARTI = this.w_PARENTOBJ.w_TIPOARTI
    this.w_QUANTI = this.w_PARENTOBJ.w_QUANTI
    this.w_PERRIC = this.w_PARENTOBJ.w_PERRIC
    this.w_TipoValo = this.w_PARENTOBJ.w_TipoValo
    this.w_SMATOU = this.w_PARENTOBJ.w_SMATOU
    this.w_Listino = this.w_PARENTOBJ.w_Listino
    this.w_ESERC = this.w_PARENTOBJ.w_ESERC
    this.w_NUMINV = this.w_PARENTOBJ.w_NUMINV
    this.w_VALLIS = this.w_PARENTOBJ.w_VALLIS
    this.w_VALESE = this.w_PARENTOBJ.w_VALESE
    this.w_SCDRSR = this.w_PARENTOBJ.w_SCDRSR
    this.w_TIPOLN = this.w_PARENTOBJ.w_TIPOLN
    this.w_CAOVAL = this.w_PARENTOBJ.w_CAOVAL
    this.w_CAOLIS = this.w_PARENTOBJ.w_CAOLIS
    this.w_CODMAG = this.w_PARENTOBJ.w_CODMAG
    this.w_INNUMPRE = this.w_PARENTOBJ.w_INNUMPRE
    this.w_CAOESE = this.w_PARENTOBJ.w_CAOESE
    this.w_INDATINV = this.w_PARENTOBJ.w_INDATINV
    this.w_PRDATINV = this.w_PARENTOBJ.w_PRDATINV
    this.w_ESFINESE = this.w_PARENTOBJ.w_ESFINESE
    this.w_PDCOSPAR = this.w_PARENTOBJ.w_PDCOSPAR
    this.w_LOTTOMED = this.w_PARENTOBJ.w_LOTTOMED
    this.w_OUNOMQUE = this.w_PARENTOBJ.w_OUNOMQUE
    this.w_AGGMPOND = this.w_PARENTOBJ.w_AGGMPOND
    this.w_COSTILOG = this.w_PARENTOBJ.w_COSTILOG
    this.w_FLCHKLP = this.w_PARENTOBJ.w_FLCHKLP
    this.w_INESCONT = this.w_PARENTOBJ.w_INESCONT
    this.w_TIPVALCIC = this.w_PARENTOBJ.w_TIPVALCIC
    * --- ----------------------------------------------------------------------------------------------
    * --- Materiali di output
    * --- ----------------------------------------------------------------------------------------------
    if !this.w_CLASS = "TGSDS_SDC"
      this.w_TipoValoOut = this.w_PARENTOBJ.w_TipoValoOut
      this.w_ListinoMo = this.w_PARENTOBJ.w_ListinoMo
      this.w_VALLISMO = this.w_PARENTOBJ.w_VALLISMO
      this.w_NUMINVMO = this.w_PARENTOBJ.w_NUMINVMO
      this.w_ESERCMO = this.w_PARENTOBJ.w_ESERCMO
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- ------ variabili solo in GSDB_KDC
    if this.w_PARELAB
      this.w_STANDARD = this.w_PARENTOBJ.w_STANDARD
      this.w_UTENTE = this.w_PARENTOBJ.w_UTENTE
      this.w_ULTIMO = this.w_PARENTOBJ.w_ULTIMO
      this.w_MEDIOPON = this.w_PARENTOBJ.w_MEDIOPON
      this.w_CLISTINO = this.w_PARENTOBJ.w_CLISTINO
      this.w_AGGSTAND = this.w_PARENTOBJ.w_AGGSTAND
      this.w_NOLIVFIN = this.w_PARENTOBJ.w_NOLIVFIN
      this.w_AGCOSSTD = this.w_PARENTOBJ.w_AGCOSSTD
      * --- ----------------------------------------------------------------------------------------------
      if this.w_AGCOSSTD="S"
        * --- Nella GSDB_KDC ho acceso il flag w_AGCOSSTD (aggiorna storico costi prodotto)
        this.w_KEYRIFSTO = SYS(2015)
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIFSTO);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIFSTO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Variabile per la gestione del numero di riga dello storico
        this.w_NUMRIGSTO = 0
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- ------ variabili solo in GSDB_KDC
    if this.w_PARELAB
      this.w_L_STANDARD = this.w_STANDARD<>"N" or this.w_AGGSTAND="S"
      this.w_L_ULTIMO = this.w_ULTIMO<>"N" or this.w_AGGSTAND="U"
      this.w_L_MEDIOPON = this.w_MEDIOPON or this.w_AGGSTAND="M"
      this.w_L_CLISTINO = this.w_CLISTINO or this.w_AGGSTAND="L"
      this.w_TipoValo = iif(this.w_L_CLISTINO,"L", IIF(this.w_STANDARD="N" , "S" , this.w_STANDARD))
    endif
    * --- Controlli preliminari
    * --- ----------------------------------------------------------------------------------------------
    if this.w_COSTILOG="S"
      this.w_OPELOG = "CREATE"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_PARELAB
      * --- Cursore di sistema mi serve per l'elaborazionme
      *     Lo elimino prima di iniziare
      USE IN SELECT("_TAMEXPDB_")
      this.w_MESS = ""
      do case
        case this.w_STANDARD<>"N" and this.w_AGGSTAND="S"
          do case
            case this.w_AGGSTAND="S"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Standard', si vuole continuare?"
            case this.w_AGGSTAND="X"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Ultimo costo standard (articolo)', si vuole continuare?"
          endcase
        case this.w_ULTIMO<>"N"
          do case
            case this.w_AGGSTAND="U"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Ultimo Costo', si vuole continuare?"
            case this.w_AGGSTAND="A"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Ultimo costo dei saldi (articolo)', si vuole continuare?"
          endcase
        case Not this.w_MEDIOPON
          do case
            case this.w_AGGSTAND="M"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Medio ponderato esercizio', si vuole continuare?"
            case this.w_AGGSTAND="P"
              this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Medio ponderato periodo', si vuole continuare?"
          endcase
        case not this.w_CLISTINO and this.w_AGGSTAND="L"
          this.w_MESS = "E' stato selezionato il criterio di aggiornamento 'Listino', si vuole continuare?"
      endcase
      if not empty(this.w_MESS) and not ah_YesNo(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
      if (this.w_MEDIOPON or this.w_ULTIMO="U" or this.w_STANDARD="S") and empty(this.w_NUMINV)
        this.w_bDispMessInv = .T.
      endif
      if (this.w_TipoValo $ "S-M-U-P" or this.w_Analisi<>"N" )
        this.w_bDispMessDatInv = .T.
      endif
    else
      if (this.w_TipoValo $ "S-M-U-P" or this.w_Analisi<>"N") and empty(this.w_NUMINV)
        this.w_bDispMessInv = .T.
      endif
      if (this.w_TipoValo $ "S-M-U-P")
        this.w_bDispMessDatInv = .T.
      endif
    endif
    if this.w_bDispMessInv
      ah_ErrorMsg("Inserire il numero inventario",16)
      i_retcode = 'stop'
      return
    endif
    if this.w_TipoValo = "L" and empty(this.w_Listino)
      ah_ErrorMsg("Inserire il listino",16)
      i_retcode = 'stop'
      return
    endif
    if this.w_TipoValo="A" and Empty(this.w_CODMAG)
      ah_ErrorMsg("Codice magazzino non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if this.w_bDispMessInv
      if this.w_FLCOSINV<>"S"
        if this.w_INDATINV < this.w_PRDATINV or this.w_INDATINV > this.w_ESFINESE
          ah_ErrorMsg("La data di stampa deve essere maggiore della data di elaborazione dell'inventario%0e minore della data di fine esercizio (esercizio:%1)","stop","", alltrim(this.w_ESERC) )
          zObj = .NULL.
          zObj = this.oparentobject.GetCtrl("w_DATSTA")
          zObj.SetFocus()
          release zObj 
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Controllo i filtri di selezione
    this.w_SELEZ = this.w_CODINI+this.w_CODFIN
    this.w_SELEZ = this.w_SELEZ+this.w_FAMAINI+this.w_FAMAFIN+this.w_GRUINI+this.w_GRUFIN+this.w_CATINI+this.w_CATFIN
    this.w_ZFILTERS = !Empty(this.w_SELEZ) Or !Empty(this.w_OUNOMQUE)
    if !Empty(this.w_OUNOMQUE)
      this.w_zCurs = this.oParentObject.w_SelDis.cCursor
      L_CurDis = this.w_zCurs
      this.w_NRCURDIS = RECCOUNT(this.w_zCurs)
      if this.w_NRCURDIS>0
        SELECT ( this.w_zCurs )
        GO TOP
        this.w_NRCURDIS = 0
        COUNT FOR &L_CurDis..xchk=1 AND !DELETED() TO this.w_NRCURDIS
        SELECT ( this.w_zCurs )
        GO TOP
      endif
      this.w_ZFILTERS = this.w_ZFILTERS OR this.w_NRCURDIS > 0
      if !this.w_ZFILTERS
        ah_ErrorMsg("Attenzione: selezione vuota", 48)
        i_retcode = 'stop'
        return
      endif
    endif
    if this.w_ZFILTERS
      USE IN SELECT("incursor")
      if this.w_ZFILTERS And !Empty(this.w_OUNOMQUE)
        if Used (this.w_zCurs )
          SELECT DISTINCT articolo as cacodice from (this.w_zCurs) into cursor ArtDist nofilter where xchk=1
        endif
      else
        do vq_exec with "..\DISB\EXE\QUERY\GSDS8KCS", this, "InCursor"
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT DISTINCT dbcodart as cacodice from InCursor INTO cursor ArtDist nofilter
      endif
      if Used("ArtDist")
        if RecCount("ArtDist")=0
          ah_ErrorMsg("Attenzione: selezione vuota", 48)
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Se nel cursore non � specificato il campo distinta allora esco
      if Used("ArtDist")
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
        this.w_VARIAR = "ArtDist.cacodice"
        * --- Esplodo i nodi
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        USE IN SELECT("InCursor")
        vq_exec("..\DISB\EXE\QUERY\GSDS7BDC", this, "InCursor")
        Use in Select("ArtDist")
        if Not(this.w_PARELAB and this.w_ZFILTERS And this.w_NOLIVFIN="S")
          * --- Delete from ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                   )
          else
            delete from (i_cTable) where;
                  CAKEYRIF = this.w_KEYRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    endif
    if this.w_PARELAB and this.w_ZFILTERS And this.w_NOLIVFIN="S"
      * --- ----------------------------------------------------------------------------------------------
      this.w_GENINIZ = DATETIME()
      * --- ----------------------------------------------------------------------------------------------
      * --- Implosione livelli finali sui quali devo eseguire l'elaborazione.
      this.w_TIPOSTAM = "D"
      do GSDS_BSI with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- ----------------------------------------------------------------------------------------------
      if this.w_COSTILOG="S"
        this.w_MSGERR = "Tempo implosione livelli finali  %1"
        this.w_OPELOG = "ADDMSG"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- ----------------------------------------------------------------------------------------------
    else
      this.w_PARZ = "S"
    endif
    * --- La variabile lato sever pu� assumere 3 valori
    *     'S' = Lato Server
    *     'P' = Esplosione lato server e elaborazione lato client
    *     'M' = tutto lato client tamexpdb
    this.w_LTSRV = IIF(this.w_CICLI, "P", "O")
    this.w_OLTSRV = this.w_LTSRV
    if this.w_PARELAB and this.w_PARZ<>"S" Or this.w_PARELAB and this.w_ZFILTERS
      * --- Sono sicuro di dover fare una nuova esplosione caso di filtri oppure implosione livelli finali
      *     Dico alla procedura GSDS_BEX di non restituirmi il risultato
      this.w_LTSRV = "S"
    endif
    * --- Dichiara le varibili private per le stampe
    private l_CODINI,l_CODFIN,l_DATSTA,l_tipges,l_fldige,l_dispro,l_disman,l_flgpre, l_chkLoop,L_bUnionCurs, l_MaxLevel, l_chkDibaNoleg, l_cCoeffImp 
 l_CODINI = this.w_CODINI 
 l_CODFIN = this.w_CODFIN 
 l_DATSTA = this.w_DATSTA 
 l_dispro = this.w_dispro 
 l_tipart = this.w_TIPOARTI 
 l_costo = this.w_TipoValo 
 l_costomp = this.w_AGGMPOND+this.w_INESCONT+this.w_FLCOSINV 
 l_listino=this.w_Listino 
 l_numinv =this.w_NUMINV 
 l_eserc =this.w_ESERC 
 l_perric = this.w_PERRIC 
 l_Analisi = this.w_Analisi 
 L_OUT = this.OutCursor 
 l_chkLoop = this.w_FLCHKLP 
 l_fldesart = this.w_FLDESART 
 l_chkDibaNoleg="N" 
 L_bUnionCurs="N" 
 L_MaxLevel = this.w_MAXLEVEL 
 l_LatoServer = this.w_LTSRV 
 l_cCoeffImp = this.w_cCoeffImp
    * --- Usare le variabili private per chiamare GSDS_BEX, altrimenti non c'e' abbastanza spazio per generare il codice)
    * --- ----------------------------------------------------------------------------------------------
    this.w_GENINIZ = DATETIME()
    * --- ----------------------------------------------------------------------------------------------
    if ((this.w_ZFILTERS And this.w_PARZ="S") Or (this.w_LIVFIN<>"S" and this.w_PARZ<>"S") Or (!Empty(this.w_OUNOMQUE) And this.w_NRCURDIS > 0))
      GSDS_BEX(this,"V","InCursor", l_cCoeffImp, " ", " ", l_TIPART, L_Out, l_DISPRO, "S", l_DATSTA,this.w_QUANTI, l_MaxLevel, l_chkLoop, l_fldesart,L_bUnionCurs, l_chkDibaNoleg, l_LatoServer)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      GSDS_BEX(this,"V", "", l_cCoeffImp , l_CODINI,l_CODFIN,l_TIPART, L_Out, l_DISPRO, "S", l_DATSTA,this.w_QUANTI, l_MaxLevel, l_chkLoop, l_fldesart,L_bUnionCurs, l_chkDibaNoleg, l_LatoServer)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_COSTILOG="S"
      this.w_MSGERR = "Tempo esplosione distinte base %1"
      this.w_OPELOG = "ADDMSG"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- Controllo quanti cursore devo elaborare
    if !cp_ExistTableDef("TMPEXPDB")
      ah_ErrorMsg("Attenzione: selezione vuota", 48)
      i_retcode = 'stop'
      return
    endif
    this.TMPEXPDB_idx = cp_OpenTable("TMPEXPDB")
    * --- Select from TMPEXPDB
    i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2],.t.,this.TMPEXPDB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(NUMDIST) AS NUMDIST  from "+i_cTable+" TMPEXPDB ";
           ,"_Curs_TMPEXPDB")
    else
      select MAX(NUMDIST) AS NUMDIST from (i_cTable);
        into cursor _Curs_TMPEXPDB
    endif
    if used('_Curs_TMPEXPDB')
      select _Curs_TMPEXPDB
      locate for 1=1
      do while not(eof())
      this.w_NUMDIST = NVL(_Curs_TMPEXPDB.NUMDIST, 0)
        select _Curs_TMPEXPDB
        continue
      enddo
      use
    endif
    if this.w_NUMDIST=0
      ah_ErrorMsg("Attenzione: selezione vuota", 48)
      i_retcode = 'stop'
      return
    endif
    if this.w_PARELAB and this.w_PARZ<>"S" Or this.w_PARELAB and this.w_ZFILTERS
      * --- ----------------------------------------------------------------------------------------------
      this.w_GENINIZ = DATETIME()
      * --- ----------------------------------------------------------------------------------------------
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS1BDC2.VQR",this.ART_TEMP_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      vq_exec("..\DISB\EXE\QUERY\GSDS5BDC", this, "incursor")
      this.w_LTSRV = this.w_OLTSRV
      l_LatoServer = this.w_LTSRV
      GSDS_BEX(this,"V","InCursor", l_cCoeffImp , " ", " ", l_TIPART, L_Out, l_DISPRO, "S", l_DATSTA,this.w_QUANTI, l_MaxLevel, l_chkLoop, l_fldesart,L_bUnionCurs, l_chkDibaNoleg,l_LatoServer)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      this.TMPEXPDB_idx = cp_OpenTable("TMPEXPDB")
      * --- Select from TMPEXPDB
      i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2],.t.,this.TMPEXPDB_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(NUMDIST) AS NUMDIST  from "+i_cTable+" TMPEXPDB ";
             ,"_Curs_TMPEXPDB")
      else
        select MAX(NUMDIST) AS NUMDIST from (i_cTable);
          into cursor _Curs_TMPEXPDB
      endif
      if used('_Curs_TMPEXPDB')
        select _Curs_TMPEXPDB
        locate for 1=1
        do while not(eof())
        this.w_NUMDIST = NVL(_Curs_TMPEXPDB.NUMDIST, 0)
          select _Curs_TMPEXPDB
          continue
        enddo
        use
      endif
      if this.w_NUMDIST=0
        ah_ErrorMsg("Attenzione: selezione vuota", 48)
        i_retcode = 'stop'
        return
      endif
      * --- ----------------------------------------------------------------------------------------------
      if this.w_COSTILOG="S"
        this.w_MSGERR = "Tempo seconda esplosione distinte base con filtri (no livelli finali) %1"
        this.w_OPELOG = "ADDMSG"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- ----------------------------------------------------------------------------------------------
    endif
    SELECT (this.OutCursor)
    * --- ----------------------------------------------------------------------------------------------
    this.w_GENINIZ = DATETIME()
    * --- ----------------------------------------------------------------------------------------------
    * --- Elaboro Costi Articolo
    * --- Faccio la Insert direttamente dalla tabella TMPEXPDB
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS1BDC1.VQR",this.ART_TEMP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elaborazione costi materiali. Ritorna il Cursore PrzArt
    GSDSCBDC (this,this.w_KEYRIF, l_DATSTA, l_costo , l_Analisi, l_ESERC, l_NUMINV, l_Listino, l_costomp)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_COSTILOG="S"
      this.w_MSGERR = "Tempo totale elaborazione costi articolo (materie prime) %1"
      this.w_OPELOG = "ADDMSG"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if Not this.w_PARELAB
      if this.w_FLCOST<>"S"
        this.w_MultiRep=createobject("Multireport")
      endif
    endif
    * --- Preparo i cambi aggiornati per convertire i listini di conto lavoro
    * --- Create temporary table TMPCAMBI
    i_nIdx=cp_AddTableDef('TMPCAMBI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CAM_BI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_BI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"CGCODVAL, CGCAMBIO "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPCAMBI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from VALUTE
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DISTINCT VACODVAL  from "+i_cTable+" VALUTE ";
           ,"_Curs_VALUTE")
    else
      select DISTINCT VACODVAL from (i_cTable);
        into cursor _Curs_VALUTE
    endif
    if used('_Curs_VALUTE')
      select _Curs_VALUTE
      locate for 1=1
      do while not(eof())
      this.w_VALCLLIS = _Curs_VALUTE.VACODVAL
      this.w_CAO = GETCAM(this.w_VALCLLIS, i_DATSYS, 0)
      * --- Insert into TMPCAMBI
      i_nConn=i_TableProp[this.TMPCAMBI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPCAMBI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCAMBI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CGCODVAL"+",CGCAMBIO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_VALCLLIS),'TMPCAMBI','CGCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAO),'TMPCAMBI','CGCAMBIO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CGCODVAL',this.w_VALCLLIS,'CGCAMBIO',this.w_CAO)
        insert into (i_cTable) (CGCODVAL,CGCAMBIO &i_ccchkf. );
           values (;
             this.w_VALCLLIS;
             ,this.w_CAO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_VALUTE
        continue
      enddo
      use
    endif
    * --- Devo controllare se la procedura ha spezzato il cursore per evitare l'errore nel caso superi i 2gb
    if L_bUnionCurs<>"S" and Used("_tamexpdb_")
      this.w_NCURS = RECCOUNT("_tamexpdb_")
    else
      this.w_NCURS = 1
    endif
    this.w_FIRSTCUR = .T.
    if this.w_NCURS > 1
      SELECT (this.OutCursor) 
 zap
      SELECT "_tamexpdb_"
      go top
      do while !Eof("_tamexpdb_") AND this.w_NCURS>1
        SELECT "_tamexpdb_"
        this.w_PATHDBF = ALLTRIM(_TAMEXPDB_.CALIAS)
        this.w_CURSNAME = SYS(2015)
        USE (this.w_PATHDBF) IN 0 ALIAS (this.w_CURSNAME)
        if USED(this.w_CURSNAME)
          * --- ----------------------------------------------------------------------------------------------
          * --- Gestione log
          this.w_GENINIZ = DATETIME()
          * --- ----------------------------------------------------------------------------------------------
          if this.w_CICLI
            GSCI_BDC(this,this.w_PARAM1, this.w_CURSNAME)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            GSDS_BDC(this,this.w_PARAM1, this.w_CURSNAME)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_FIRSTCUR = .F.
          if FILE(this.w_PATHDBF)
            delete file (this.w_PATHDBF)
          endif
          SELECT "_tamexpdb_"
          * --- ----------------------------------------------------------------------------------------------
          if this.w_COSTILOG="S"
            this.w_MSGERR = "Tempo costificazione blocco "+alltrim(str(recno()))+" %1 - "
            this.w_OPELOG = "ADDMSG"
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- ----------------------------------------------------------------------------------------------
        endif
        SELECT "_tamexpdb_"
        skip
      enddo
    else
      * --- ----------------------------------------------------------------------------------------------
      * --- Gestione log
      this.w_GENINIZ = DATETIME()
      * --- ----------------------------------------------------------------------------------------------
      this.w_CURSNAME = this.OutCursor
      if this.w_CICLI
        GSCI_BDC(this,this.w_PARAM1, this.w_CURSNAME)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        GSDS_BDC(this,this.w_PARAM1, this.w_CURSNAME)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- ----------------------------------------------------------------------------------------------
      if this.w_COSTILOG="S"
        this.w_MSGERR = "Tempo costificazione dati %1"
        this.w_OPELOG = "ADDMSG"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- ----------------------------------------------------------------------------------------------
    endif
    * --- Drop temporary table TMPCAMBI
    i_nIdx=cp_GetTableDefIdx('TMPCAMBI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCAMBI')
    endif
    if this.w_PARELAB AND this.w_AGCOSSTD="S"
      * --- Nella GSDB_KDC ho acceso il flag w_AGCOSSTD (aggiorna storico costi prodotto)
      * --- Determinazione costo standard con flag aggiorna storico costi
      this.w_PRSERIAL = SPACE(10)
      * --- Prende il progressivo per la chiave primaria
      this.w_PRSERIAL = cp_GetProg("STOCOSTA", "SERCST", this.w_PRSERIAL, i_CODAZI)
      * --- Try
      local bErr_044805B8
      bErr_044805B8=bTrsErr
      this.Try_044805B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = "Errore di scrittura nel database%0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL"
        ah_ErrorMsg(this.w_MESS, 48, "")
      endif
      bTrsErr=bTrsErr or bErr_044805B8
      * --- End
    endif
    if Not this.w_PARELAB AND this.w_FLCOST<>"S"
      * --- Mostra il report di stampa
      CP_CHPRN(this.w_MultiRep, " ", this.w_PARENTOBJ)
    endif
    USE IN SELECT("__bsi__")
    USE IN SELECT("PrzArt")
    USE IN SELECT("_tamexpdb_")
    * --- ----------------------------------------------------------------------------------------------
    if this.w_COSTILOG="S"
      this.w_OPELOG = "PUTLOG"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_FLCOST="S"
      this.w_DateTimeEnd = TTOC(DATETIME())
      ah_ErrorMsg("Elaborazione terminata", 64)
    endif
  endproc
  proc Try_044805B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STOCOSTA
    i_nConn=i_TableProp[this.STOCOSTA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STOCOSTA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS20BDS.VQR",this.STOCOSTA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare w_VARIAR per definire il cursore e il campo dei codici da passare
    * --- Il campo cakeysal ha il nome fisso (questo campo non ha indice)
    if at(".", this.w_VARIAR)>0
      cCurstmp = left(this.w_VARIAR ,at(".", this.w_VARIAR)-1) 
 SELECT (cCurstmp)
      * --- begin transaction
      cp_BeginTrs()
      if (type(cCurstmp+".cakeysal") = "C")
        SCAN
        this.w_CODIAR = padr(eval(this.w_VARIAR),20)
        this.w_CAKEYSAL = eval(cCurstmp+".cakeysal")
        this.w_CODARTI = SPACE(20)
        if (type(cCurstmp+".cacodart")="C")
          this.w_CODARTI = LEFT(eval(cCurstmp+".cacodart")+SPACE(20) , 20)
        endif
        if Empty(this.w_CODARTI)
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CPROWNUM',0,'CACODDIS',SPACE(20))
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CACODDIS &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,0;
                 ,SPACE(20);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTI),'ART_TEMP','CACODART');
            +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CACODART',this.w_CODARTI,'CPROWNUM',0,'CACODDIS',SPACE(20))
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CPROWNUM,CACODDIS &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,this.w_CODARTI;
                 ,0;
                 ,SPACE(20);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        ENDSCAN
      else
        SCAN
        this.w_CODIAR = padr(eval(this.w_VARIAR),20)
        * --- Insert into ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CAKEYRIF"+",CACODRIC"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
          +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CPROWNUM',0,'CACODDIS',SPACE(20))
          insert into (i_cTable) (CAKEYRIF,CACODRIC,CPROWNUM,CACODDIS &i_ccchkf. );
             values (;
               this.w_KEYRIF;
               ,this.w_CODIAR;
               ,0;
               ,SPACE(20);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        ENDSCAN
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili relative al log elaborazione
    * --- ----------------------------------------------------------------------------------------------
    do case
      case this.w_OPELOG="CREATE"
        * --- CURSORE PER I MESSAGGI
        this.w_DCTINIZ = DATETIME()
        this.w_DateTimeStart = TTOC(DATETIME())
        this.w_FileName = "Determinazione."+dtos(date())+strtran(time(),":","")+".log"
        this.w_LogTitle = "Log Determinazione costo standard"
        this.w_oMRPLOG=createobject("AH_ERRORLOG")
        this.w_oMRPLOG.ADDMSGLOG(this.w_LogTitle, g_VERSION, this.w_LogTitle)     
        this.w_oMRPLOG.ADDMSGLOG("================================================================================")     
        this.w_oMRPLOG.ADDMSGLOG("Inizio Determinazione costo standard [%1]", ALLTRIM(TTOC(DATETIME())))     
      case this.w_OPELOG="ADDMSG"
        this.w_GENFINE = DATETIME()
        this.w_GENTOTA = this.w_GENFINE-this.w_GENINIZ
        this.w_oMRPLOG.ADDMSGLOG(this.w_MSGERR, this.TimeMRP(this.w_GENTOTA))     
      case this.w_OPELOG="PUTLOG"
        this.w_DCTFINE = DATETIME()
        this.w_DCTTOTA = this.w_DCTFINE-this.w_DCTINIZ
        this.w_DateTimeEnd = TTOC(DATETIME())
        this.w_oMRPLOG.ADDMSGLOG("Tempo totale elaborazione %1", this.TimeMRP(this.w_DCTTOTA))     
        this.w_oMRPLOG.ADDMSGLOG("================================================================================")     
        this.w_oMRPLOG.ADDMSGLOG("Fine determinazione costo standard [%1]", ALLTRIM(TTOC(DATETIME())))     
        if this.w_oMRPLOG.ISFULLLOG()
          this.w_LogFileName = LEFT(this.w_FileName, LEN(this.w_filename)-4)+".LOG"
          this.w_CursErrorLog = this.w_oMRPLOG.cCursMessErr
          l_ErrCurs = this.w_oMRPLOG.cCursMessErr
          SELECT(l_ErrCurs) 
 GO TOP
          this.hf_FILE_LOG = fcreate(this.w_LogFileName)
          if this.hf_FILE_LOG>=0
            do while not eof()
              this.w_cMsg = &l_ErrCurs..Msg
              * --- Scrive Log
              this.w_TEMP = fPuts(this.hf_FILE_LOG, this.w_cMsg)
              SELECT (l_ErrCurs)
              skip +1
            enddo
            =fCLOSE(this.hf_FILE_LOG)
          endif
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM1)
    this.w_PARAM1=w_PARAM1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='*TMPCAMBI'
    this.cWorkTables[3]='CAM_BI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='STOCOSTA'
    this.cWorkTables[6]='*TMPEXPDB'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_TMPEXPDB')
      use in _Curs_TMPEXPDB
    endif
    if used('_Curs_TMPEXPDB')
      use in _Curs_TMPEXPDB
    endif
    if used('_Curs_VALUTE')
      use in _Curs_VALUTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsds1bdc
  Function TimeMRP(w_second)
  local timemessage
  if w_second<60
    if int(w_second)=w_second
      timemessage=alltrim(str(w_second,10))+space(1)+ah_msgformat('secondi')
    else
      timemessage=alltrim(str(w_second,10,3))+space(1)+ah_msgformat('secondi')
    endif
  else
    if w_second/60<60
      if int(w_second/60)=1
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuto')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')    
      else
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    else
      if int(w_second/60/60)=1
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ora')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      else
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ore')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    endif
  endif
  return timemessage
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM1"
endproc
