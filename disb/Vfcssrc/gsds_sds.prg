* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_sds                                                        *
*              Stampa distinta base                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-03                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_sds",oParentObject))

* --- Class definition
define class tgsds_sds as StdForm
  Top    = 36
  Left   = 37

  * --- Standard Properties
  Width  = 575
  Height = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=74015849
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsds_sds"
  cComment = "Stampa distinta base"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_DATSTA = ctod('  /  /  ')
  w_FLPROV = space(1)
  w_FLCICL = space(1)
  w_NOESPL = space(1)
  w_FLUM1 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  o_ONUME = 0
  w_DISINI = space(20)
  w_DISFIN = space(20)
  w_FLDESART = space(1)
  w_FLDESCRI = space(1)
  w_DIS1OB = ctod('  /  /  ')
  w_DIS2OB = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_sdsPag1","gsds_sds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsds_sds
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_DATSTA=ctod("  /  /  ")
      .w_FLPROV=space(1)
      .w_FLCICL=space(1)
      .w_NOESPL=space(1)
      .w_FLUM1=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_DISINI=space(20)
      .w_DISFIN=space(20)
      .w_FLDESART=space(1)
      .w_FLDESCRI=space(1)
      .w_DIS1OB=ctod("  /  /  ")
      .w_DIS2OB=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_DATSTA = i_datsys
        .w_FLPROV = ' '
          .DoRTCalc(7,8,.f.)
        .w_FLUM1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_OBTEST = i_INIDAT
        .DoRTCalc(11,13,.f.)
        if not(empty(.w_DISINI))
          .link_1_20('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DISFIN))
          .link_1_21('Full')
        endif
        .w_FLDESART = ' '
        .w_FLDESCRI = 'N'
    endwith
    this.DoRTCalc(17,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_3('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(4,12,.t.)
          .link_1_20('Full')
          .link_1_21('Full')
        .DoRTCalc(15,15,.t.)
        if .o_ONUME<>.w_ONUME
            .w_FLDESCRI = 'N'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLCICL_1_7.enabled = this.oPgFrm.Page1.oPag.oFLCICL_1_7.mCond()
    this.oPgFrm.Page1.oPag.oNOESPL_1_8.enabled = this.oPgFrm.Page1.oPag.oNOESPL_1_8.mCond()
    this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.enabled = this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLCICL_1_7.visible=!this.oPgFrm.Page1.oPag.oFLCICL_1_7.mHide()
    this.oPgFrm.Page1.oPag.oNOESPL_1_8.visible=!this.oPgFrm.Page1.oPag.oNOESPL_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFLDESART_1_22.visible=!this.oPgFrm.Page1.oPag.oFLDESART_1_22.mHide()
    this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.visible=!this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DISINI = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DISINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODINI) OR NOT EMPTY(.w_DISINI)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DISINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DISFIN = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DISFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DISFIN)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DISFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISINI
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DISINI)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISINI = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS1OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DISINI = space(20)
      endif
      this.w_DIS1OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISFIN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DISFIN)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISFIN = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS2OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DISFIN = space(20)
      endif
      this.w_DIS2OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_5.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_5.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_6.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCICL_1_7.RadioValue()==this.w_FLCICL)
      this.oPgFrm.Page1.oPag.oFLCICL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOESPL_1_8.RadioValue()==this.w_NOESPL)
      this.oPgFrm.Page1.oPag.oNOESPL_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLUM1_1_9.RadioValue()==this.w_FLUM1)
      this.oPgFrm.Page1.oPag.oFLUM1_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESART_1_22.RadioValue()==this.w_FLDESART)
      this.oPgFrm.Page1.oPag.oFLDESART_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.RadioValue()==this.w_FLDESCRI)
      this.oPgFrm.Page1.oPag.oFLDESCRI_1_23.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODINI) OR NOT EMPTY(.w_DISINI)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI))))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale")
          case   not((EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DISFIN)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale")
          case   ((empty(.w_DATSTA)) or not((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice distinta di inizio o fine selezione obsoleto alla data selezionata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_ONUME = this.w_ONUME
    return

enddefine

* --- Define pages as container
define class tgsds_sdsPag1 as StdContainer
  Width  = 571
  height = 237
  stdWidth  = 571
  stdheight = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="YLDHOMMPET",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale",;
    ToolTipText = "Articolo (con distinta base) di inizio selezione (spazio=no selezione)",;
    HelpContextID = 104583386,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=108, Top=10, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_2 as StdField with uid="YJQMLZHVQA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104524490,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=10, InputMask=replicate('X',40)

  add object oCODFIN_1_3 as StdField with uid="VXFHEBELNK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo obsoleto, senza distinta o codice iniziale pi� grande del codice finale",;
    ToolTipText = "Articolo (con distinta base) di fine selezione (spazio=no selezione)",;
    HelpContextID = 26136794,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=108, Top=42, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_4 as StdField with uid="DESEQAHWJU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26077898,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=42, InputMask=replicate('X',40)

  add object oDATSTA_1_5 as StdField with uid="WHTHOLGNAQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Codice distinta di inizio o fine selezione obsoleto alla data selezionata",;
    ToolTipText = "Data di stampa",;
    HelpContextID = 231792330,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=108, Top=74

  func oDATSTA_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB)))
    endwith
    return bRes
  endfunc

  add object oFLPROV_1_6 as StdCheck with uid="DBCAKBJFBW",rtseq=6,rtrep=.f.,left=195, top=74, caption="Solo confermate",;
    ToolTipText = "Se attivo: stampa solo le distinte confermate, altrimenti anche quelle provvisorie",;
    HelpContextID = 153228202,;
    cFormVar="w_FLPROV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPROV_1_6.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func oFLPROV_1_6.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_6.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='P',1,;
      0)
  endfunc

  add object oFLCICL_1_7 as StdCheck with uid="FJHERZVUFT",rtseq=7,rtrep=.f.,left=375, top=74, caption="Cicli semplificati",;
    ToolTipText = "Se attivo stampa anche i dati relativi ai cicli semplificati",;
    HelpContextID = 65790890,;
    cFormVar="w_FLCICL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCICL_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLCICL_1_7.GetRadio()
    this.Parent.oContained.w_FLCICL = this.RadioValue()
    return .t.
  endfunc

  func oFLCICL_1_7.SetRadio()
    this.Parent.oContained.w_FLCICL=trim(this.Parent.oContained.w_FLCICL)
    this.value = ;
      iif(this.Parent.oContained.w_FLCICL=='S',1,;
      0)
  endfunc

  func oFLCICL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=3 OR .w_ONUME=4)
    endwith
   endif
  endfunc

  func oFLCICL_1_7.mHide()
    with this.Parent.oContained
      return (NOT(.w_ONUME=3 OR .w_ONUME=4))
    endwith
  endfunc

  add object oNOESPL_1_8 as StdCheck with uid="CNPTTSEHVU",rtseq=8,rtrep=.f.,left=375, top=74, caption="Solo prodotti finiti",;
    ToolTipText = "Se attivo: ignora i componenti da esplodere (considera solo prodotti finiti)",;
    HelpContextID = 51494954,;
    cFormVar="w_NOESPL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOESPL_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNOESPL_1_8.GetRadio()
    this.Parent.oContained.w_NOESPL = this.RadioValue()
    return .t.
  endfunc

  func oNOESPL_1_8.SetRadio()
    this.Parent.oContained.w_NOESPL=trim(this.Parent.oContained.w_NOESPL)
    this.value = ;
      iif(this.Parent.oContained.w_NOESPL=='S',1,;
      0)
  endfunc

  func oNOESPL_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=5)
    endwith
   endif
  endfunc

  func oNOESPL_1_8.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>5)
    endwith
  endfunc

  add object oFLUM1_1_9 as StdCheck with uid="IHKXRZGYVS",rtseq=9,rtrep=.f.,left=195, top=101, caption="Qta componenti su 1^U.M.",;
    ToolTipText = "Se attivo: la stampa delle quantit� dei componenti sar� riferita alla prima unit� di misura",;
    HelpContextID = 17220522,;
    cFormVar="w_FLUM1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLUM1_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLUM1_1_9.GetRadio()
    this.Parent.oContained.w_FLUM1 = this.RadioValue()
    return .t.
  endfunc

  func oFLUM1_1_9.SetRadio()
    this.Parent.oContained.w_FLUM1=trim(this.Parent.oContained.w_FLUM1)
    this.value = ;
      iif(this.Parent.oContained.w_FLUM1=='S',1,;
      0)
  endfunc


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=159, width=452,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103981798


  add object oBtn_1_11 as StdButton with uid="TYEVPKXNEQ",left=461, top=185, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 67773734;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do GSDS_BSD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="BSJPZGRESF",left=511, top=185, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66698426;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLDESART_1_22 as StdCheck with uid="XZKQHNRHPE",rtseq=15,rtrep=.f.,left=375, top=101, caption="Descr. componente variante",;
    ToolTipText = "Se attivo stampa la descrizione del componente variante",;
    HelpContextID = 34614442,;
    cFormVar="w_FLDESART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDESART_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDESART_1_22.GetRadio()
    this.Parent.oContained.w_FLDESART = this.RadioValue()
    return .t.
  endfunc

  func oFLDESART_1_22.SetRadio()
    this.Parent.oContained.w_FLDESART=trim(this.Parent.oContained.w_FLDESART)
    this.value = ;
      iif(this.Parent.oContained.w_FLDESART=='S',1,;
      0)
  endfunc

  func oFLDESART_1_22.mHide()
    with this.Parent.oContained
      return (.w_ONUME=1 OR .w_ONUME=2 OR .w_ONUME=3)
    endwith
  endfunc

  add object oFLDESCRI_1_23 as StdCheck with uid="LCQHMKBGBE",rtseq=16,rtrep=.f.,left=195, top=132, caption="Stampa note distinta base",;
    ToolTipText = "Se attivo: stampa le note della distinta base",;
    HelpContextID = 68168863,;
    cFormVar="w_FLDESCRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDESCRI_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDESCRI_1_23.GetRadio()
    this.Parent.oContained.w_FLDESCRI = this.RadioValue()
    return .t.
  endfunc

  func oFLDESCRI_1_23.SetRadio()
    this.Parent.oContained.w_FLDESCRI=trim(this.Parent.oContained.w_FLDESCRI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDESCRI=='S',1,;
      0)
  endfunc

  func oFLDESCRI_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=4)
    endwith
   endif
  endfunc

  func oFLDESCRI_1_23.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>4)
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="GIGJHPMORF",Visible=.t., Left=5, Top=159,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="AZAYVNOACS",Visible=.t., Left=37, Top=10,;
    Alignment=1, Width=69, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YBUTHVTDPZ",Visible=.t., Left=42, Top=42,;
    Alignment=1, Width=64, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LPGIBLLYAF",Visible=.t., Left=54, Top=74,;
    Alignment=1, Width=52, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_sds','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
