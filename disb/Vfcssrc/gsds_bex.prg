* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bex                                                        *
*              Procedura esplosione distinte base lato server                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-13                                                      *
* Last revis.: 2018-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,cOperazione,InCursor,cCoeffImp,DistintaINI,DistintaFIN,TipoArticolo,OutCursor,cDisPro,cExplMP,cDatRif,cQtaExp,nMaxLevel,cChkLoop,cFldesart,cUnionCurs,cChkDibaNoLegami,cLatoServer
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bex",oParentObject,m.cOperazione,m.InCursor,m.cCoeffImp,m.DistintaINI,m.DistintaFIN,m.TipoArticolo,m.OutCursor,m.cDisPro,m.cExplMP,m.cDatRif,m.cQtaExp,m.nMaxLevel,m.cChkLoop,m.cFldesart,m.cUnionCurs,m.cChkDibaNoLegami,m.cLatoServer)
return(i_retval)

define class tgsds_bex as StdBatch
  * --- Local variables
  cOperazione = space(1)
  InCursor = space(10)
  cCoeffImp = space(1)
  DistintaINI = space(20)
  DistintaFIN = space(20)
  TipoArticolo = space(2)
  OutCursor = space(15)
  cDisPro = space(1)
  cExplMP = space(1)
  cDatRif = space(1)
  cQtaExp = 0
  nMaxLevel = 0
  cChkLoop = .f.
  cFldesart = space(1)
  cUnionCurs = space(1)
  cChkDibaNoLegami = 0
  cLatoServer = space(1)
  w_LIVELLO = 0
  w_ROWS = 0
  w_ROWSLOOP = 0
  w_ROWSDBNOLG = 0
  w_DateTimeStart = space(15)
  w_DateTimeEnd = space(15)
  w_EXIT = .f.
  w_DATRIF = ctod("  /  /  ")
  cTipGes = space(1)
  w_cDisPro = space(1)
  w_CoeffImp = space(1)
  w_cExplMP = space(1)
  w_Operazione = space(1)
  w_nMaxLevel = 0
  w_ChkLoop = space(1)
  w_ChkDibaNoLegami = space(1)
  w_FLDESART = space(1)
  w_UnionCurs = space(1)
  w_LatoServer = space(1)
  w_oEXPDBLOG = .NULL.
  w_EXPDBLOG = .f.
  w_LogTitle = space(10)
  hf_FILE_LOG = 0
  w_FileName = space(50)
  w_LogFileName = space(10)
  w_CursErrorLog = space(10)
  w_cMsg = space(0)
  w_LIVINIZ = 0
  w_LIVFIN = 0
  w_LIVTOT = 0
  w_FILINIZ = 0
  w_FILFIN = 0
  w_FILTOT = 0
  w_AGLINIZ = 0
  w_AGLFIN = 0
  w_AGLTOT = 0
  w_TEMP = space(10)
  w_DIMLVLKEY = space(10)
  w_SQLCollateColLvlKey = space(20)
  w_cQtaExp = 0
  w_CODAZI = space(5)
  w_PDMODVAR = space(1)
  w_TVPADSUPREMO = .f.
  w_ContaPadri = 0
  w_LevelKey = space(200)
  w_nRec = 0
  w_nRecTot = 0
  w_FRASE = space(1)
  w_iNConn = 0
  w_NOFILCURS = .f.
  w_CODICEDIBA = space(20)
  w_DBARTDIS = space(20)
  w_ROWSCOIM = 0
  w_ROWSCOIMV = space(10)
  w_ERR = space(1)
  w_OLDLVLKEY = space(200)
  w_LVLKEY = space(200)
  w_DESCRITV = space(1)
  w_MSGERRORE = space(1)
  w_PRIMOGIRO = .f.
  w_NUMDIST = 0
  w_CURSORLIMIT = 0
  w_CURSORNUMBER = space(5)
  w_DADISTINTA = 0
  w_ADISTINTA = 0
  w_CURSORCOUNTER = 0
  w_NAMECURS = space(50)
  w_OQRY = space(200)
  * --- WorkFile variables
  TMPEXPDBS_idx=0
  TMPDBNOLEG_idx=0
  TMPDISMBASE_idx=0
  TMPDIS_COMP_idx=0
  TMPEXPDB_idx=0
  PAR_DISB_idx=0
  TMPCOE_IMP_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --------------------- Parametri --------------------------
    * --- Tipo di operazione da eseguire 
    * --- Cursore di input che contiene i codici da esplodere
    * --- Gestione Coeff. Impiego in distinta base
    * --- Codice Distinta Iniziale
    * --- Codice Distinta Finale
    * --- Tipo Articolo
    * --- Nome del cursore di output
    * --- Distinta provvisorie
    * --- esplosione distinte materiali
    * --- Data esplosione delle distinte base
    * --- Quantit� da esplodere
    * --- Numero massimo di livelli da esplodere
    * --- Flag che abilita il controllo del loop in distinta base
    * --- ----------------------------------------------------------------------------------------------
    * --- Variabili relative all'esplosione della distinta base
    * --- ----------------------------------------------------------------------------------------------
    * --- Variabili relative al log elaborazione
    * --- ----------------------------------------------------------------------------------------------
    * --- Legge parametro relativo alla modalit� di definizione ed utilizzo delle varianti)
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDMODVAR"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDMODVAR;
        from (i_cTable) where;
            PDCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PDMODVAR = NVL(cp_ToDate(_read_.PDMODVAR),cp_NullValue(_read_.PDMODVAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TVPADSUPREMO = (this.w_PDMODVAR="S")
    * --- ----------------------------------------------------------------------------------------------
    * --- CURSORE PER I MESSAGGI
    this.w_EXPDBLOG = vartype(g_ATTIVAEXPDBLOG)="L" and g_ATTIVAEXPDBLOG
    if this.w_EXPDBLOG
      this.w_FileName = "Esplosione."+dtos(date())+strtran(time(),":","")+".log"
      this.w_LogTitle = "Log esplosione distinta base"
      this.w_oEXPDBLOG=createobject("AH_ERRORLOG")
      this.w_oEXPDBLOG.ADDMSGLOG(this.w_LogTitle, g_VERSION, this.w_LogTitle)     
      this.w_oEXPDBLOG.ADDMSGLOG("================================================================================")     
      this.w_oEXPDBLOG.ADDMSGLOG("Inizio Esplosione [%1]", ALLTRIM(TTOC(DATETIME())))     
    endif
    * --- ----------------------------------------------------------------------------------------------
    this.w_DateTimeStart = TTOC(DATETIME())
    this.w_FILINIZ = seconds()
    this.w_EXIT = .F.
    ah_msg("Esplosione livello 0 in corso ...", .t., .t.)
    this.w_ROWS = 1
    * --- ----------------------------------------------------------------------------------------------
    * --- Inizializzo le variabili in base ai parametri
    this.w_DATRIF = this.cDatRif
    * --- Distinte provvisorie
    this.w_cDisPro = iif(type("This.cDisPro")="C", this.cDisPro, "N" )
    * --- esplosione distinte materiali
    this.w_cExplMP = iif(type("This.cExplMP")="C", this.cExplMP, "N" )
    this.w_Operazione = iif(type("this.cOperazione")="C", this.cOperazione, "V" )
    * --- Controllo loop in distinta
    this.w_ChkLoop = iif(type("this.cChkLoop")="C", this.cChkLoop, "S")
    this.w_nMaxLevel = iif(type("this.nMaxLevel")="N", this.nMaxLevel, 999)
    this.w_FLDESART = iif(type("this.cChkLoop")="C", this.cFldesart, "S")
    this.w_UnionCurs = iif(type("this.cUnionCurs")="C", this.cUnionCurs, "S")
    this.w_ChkDibaNoLegami = iif(type("this.cChkDibaNoLegami")="C", this.cChkDibaNoLegami, "S")
    this.w_LatoServer = iif(type("this.cLatoServer")="C", this.cLatoServer, "N")
    * --- ----------------------------------------------------------------------------------------------
    this.w_cQtaExp = iif(type("This.cQtaExp")="N", this.cQtaExp, 1 )
    * --- ----------------------------------------------------------------------------------------------
    this.w_DIMLVLKEY = "CHAR(200)"
    * --- ----------------------------------------------------------------------------------------------
    this.w_CoeffImp = iif(type("this.cCoeffImp")="C", this.cCoeffImp, "N")
    * --- ----------------------------------------------------------------------------------------------
    * --- Controllo se sono un utente schedulatore abilito sempre il controllo del loop
    if type("g_SCHEDULER")="C" AND g_SCHEDULER="S"
      this.w_ChkLoop = "S"
    else
      this.w_ChkLoop = iif(this.cOperazione<>"V","S", this.w_ChkLoop)
    endif
    * --- ----------------------------------------------------------------------------------------------
    if UPPER(CP_DBTYPE)="SQLSERVER"
      this.w_SQLCollateColLvlKey = IIF(varType(g_SQLCollateColLvlKey)="C" And !Empty(g_SQLCollateColLvlKey) , g_SQLCollateColLvlKey, "Latin1_General_BIN")
    endif
    * --- ----------------------------------------------------------------------------------------------
    this.w_NOFILCURS = .T.
    if this.w_EXPDBLOG
      this.w_LIVINIZ = Seconds()
    endif
    if !Empty(this.InCursor)
      if USED(this.InCursor)
        this.w_NOFILCURS = .F.
        * --- ----------------------------------------------------------------------------------------------
        * --- Sto passando un cursore come filtro
        * --- ----------------------------------------------------------------------------------------------
        * --- Creo tabella temporanea con elenco distinte da esplodere
        * --- Create temporary table TMPEXPDBS
        i_nIdx=cp_AddTableDef('TMPEXPDBS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\DISB\EXE\QUERY\GSDSTBEX',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPEXPDBS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if UPPER(CP_DBTYPE)="SQLSERVER"
          this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN LVLKEY " + this.w_DIMLVLKEY + " COLLATE  " + this.w_SQLCollateColLvlKey
          this.w_iNConn = i_TableProp[ i_nIdx ,3]
          w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
        endif
        * --- ----------------------------------------------------------------------------------------------
        L_Input = this.InCursor
        this.w_ContaPadri = -1
        this.w_nRec = 0
        this.w_nRecTot = RECCOUNT(this.InCursor)
        SELECT (this.InCursor)
        GO TOP
        do while !Eof(this.InCursor) 
          this.w_ContaPadri = this.w_ContaPadri + 1
          this.w_LevelKey = LEFT(this.cp_bintoc(this.w_ContaPadri), 5)
          this.w_nRec = this.w_nRec + 1
          if UPPER(CP_DBTYPE)="SQLSERVER"
            * --- Recupero la versione di SQLServer
            * --- La versione mi serve per sapere se posso utilizzare la ROW_NUMBER nel calcolo del lvl key
            *     che � stata introdotta con la versione 2005 di SQL Server oppure per Oracle
            if Int(Val(Left(SQLXGetDatabaseVersion(1), At("." , SQLXGetDatabaseVersion(1))-1))) < 9
              this.w_LevelKey = this.w_LevelKey+"##"
            endif
          endif
          if MOD(this.w_nRec ,1*iif(this.w_nRecTot>1000,10,1)*iif(this.w_nRecTot>10000,50,1))=0
            ah_msg("Preparazione dati... analisi distinta %1 di %2 in corso...",.t.,.f.,.f.,alltrim(str(this.w_nRec)), alltrim(str(this.w_nRecTot)) )
          endif
          SELECT (this.InCursor)
          this.w_DBARTDIS = &L_Input..DBCODART
          this.w_CODICEDIBA = &L_Input..DBCODICE
          * --- Insert into TMPEXPDBS
          i_nConn=i_TableProp[this.TMPEXPDBS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDBS_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPEXPDBS_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"NUMDIST"+",DBARTDIS"+",DBCODICE"+",CODICEDIBA"+",LVLKEY"+",CPROWNUM"+",CPROWORD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_nRec),'TMPEXPDBS','NUMDIST');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DBARTDIS),'TMPEXPDBS','DBARTDIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODICEDIBA),'TMPEXPDBS','DBCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODICEDIBA),'TMPEXPDBS','CODICEDIBA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LevelKey),'TMPEXPDBS','LVLKEY');
            +","+cp_NullLink(cp_ToStrODBC(0),'TMPEXPDBS','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(0),'TMPEXPDBS','CPROWORD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'NUMDIST',this.w_nRec,'DBARTDIS',this.w_DBARTDIS,'DBCODICE',this.w_CODICEDIBA,'CODICEDIBA',this.w_CODICEDIBA,'LVLKEY',this.w_LevelKey,'CPROWNUM',0,'CPROWORD',0)
            insert into (i_cTable) (NUMDIST,DBARTDIS,DBCODICE,CODICEDIBA,LVLKEY,CPROWNUM,CPROWORD &i_ccchkf. );
               values (;
                 this.w_nRec;
                 ,this.w_DBARTDIS;
                 ,this.w_CODICEDIBA;
                 ,this.w_CODICEDIBA;
                 ,this.w_LevelKey;
                 ,0;
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT (this.InCursor)
          if !Eof(this.InCursor) 
            skip
          endif
        enddo
        Use in (this.InCursor)
      else
        cp_ErrorMsg(cp_msgformat(MSG_CURSOR__IS_NOT_OPENED_F,this.cInpCursor),48,"",.f.)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- Drop temporary table TMPEXPDB
    i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEXPDB')
    endif
    * --- Create temporary table TMPEXPDB
    i_nIdx=cp_AddTableDef('TMPEXPDB') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPEXPDB_proto';
          )
    this.TMPEXPDB_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if UPPER(CP_DBTYPE)="SQLSERVER"
      this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN LVLKEY " + this.w_DIMLVLKEY + " COLLATE  " + this.w_SQLCollateColLvlKey
      this.w_iNConn = i_TableProp[ i_nIdx ,3]
      w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
    endif
    if !this.w_NOFILCURS
      * --- Insert into TMPEXPDB
      i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS1BEX",this.TMPEXPDB_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Create temporary table TMPEXPDBS
      i_nIdx=cp_AddTableDef('TMPEXPDBS') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\DISB\EXE\QUERY\GSDSTBEXA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPEXPDBS_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Faccio una insert direttamente da query
      * --- Insert into TMPEXPDB
      i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS1BEXA",this.TMPEXPDB_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Elimino tabella temporanea di appoggio dei codici distinta base da esplodere
    * --- Drop temporary table TMPEXPDBS
    i_nIdx=cp_GetTableDefIdx('TMPEXPDBS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPEXPDBS')
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- Create temporary table TMPDISMBASE
    i_nIdx=cp_AddTableDef('TMPDISMBASE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPDISMBASE_proto';
          )
    this.TMPDISMBASE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if UPPER(CP_DBTYPE)="SQLSERVER"
      this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN LVLKEY " + this.w_DIMLVLKEY + " COLLATE " + this.w_SQLCollateColLvlKey
      this.w_iNConn = i_TableProp[ i_nIdx ,3]
      w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
    endif
    * --- Create temporary table TMPDIS_COMP
    i_nIdx=cp_AddTableDef('TMPDIS_COMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPDIS_COMP_proto';
          )
    this.TMPDIS_COMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ----------------------------------------------------------------------------------------------
    if this.w_ChkDibaNoLegami = "S"
      * --- Creo tabella temporanea che contiene distinta base senza legami validi
      * --- Create temporary table TMPDBNOLEG
      i_nIdx=cp_AddTableDef('TMPDBNOLEG') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPDBNOLEG_proto';
            )
      this.TMPDBNOLEG_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if UPPER(CP_DBTYPE)="SQLSERVER"
        this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN LVLKEY " + this.w_DIMLVLKEY + " COLLATE " + this.w_SQLCollateColLvlKey
        this.w_iNConn = i_TableProp[ i_nIdx ,3]
        w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
      endif
    endif
    if this.w_EXPDBLOG
      this.w_LIVFIN = Seconds()
      this.w_LIVTOT = this.w_LIVFIN - this.w_LIVINIZ
      * --- Aggiorno il log ogni volta che ho terminato l'operazione di esplosione del livello
      this.w_oEXPDBLOG.ADDMSGLOG("Tempo preparazione dati... %1" , this.TimeEXP(this.w_LIVTOT), alltrim(str(this.w_ROWS)))     
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_CoeffImp="S"
      * --- Create temporary table TMPCOE_IMP
      i_nIdx=cp_AddTableDef('TMPCOE_IMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_WSEUOLKNSE[1]
      indexes_WSEUOLKNSE[1]='LVLKEY'
      vq_exec('..\DISB\EXE\QUERY\GSDS6BCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_WSEUOLKNSE,.f.)
      this.TMPCOE_IMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if UPPER(CP_DBTYPE)="SQLSERVER"
        this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN LVLKEY " + this.w_DIMLVLKEY + " COLLATE " + this.w_SQLCollateColLvlKey
        this.w_iNConn = i_TableProp[ i_nIdx ,3]
        w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    * --- Esplodo fino a che scrivo almeno un componente
    do while this.w_ROWS > 0
      * --- Recupero codici distinta abse del livello N
      if this.w_EXPDBLOG
        this.w_LIVINIZ = Seconds()
      endif
      * --- Insert into TMPDISMBASE
      i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDSMBEX",this.TMPDISMBASE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_ROWSCOIM = 0
      this.w_ROWSCOIMV = 0
      if this.w_CoeffImp="S"
        USE IN SELECT("APPCOE2")
        * --- Insert into TMPCOE_IMP
        i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS6BCI",this.TMPCOE_IMP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ROWSCOIM = i_Rows
        if this.w_TVPADSUPREMO
          * --- Insert into TMPCOE_IMP
          i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS8BCI",this.TMPCOE_IMP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ROWSCOIMV = i_Rows
        else
          * --- Insert into TMPCOE_IMP
          i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS9BCI",this.TMPCOE_IMP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ROWSCOIMV = i_Rows
        endif
        if this.w_ROWSCOIM+this.w_ROWSCOIMV > 0
          if this.w_LIVELLO > 0
            * --- Se il livello � maggiore di zero verifico se devo aggiornare il coeff con i valori impostati precedentemente 
            * --- Write into TMPCOE_IMP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="LVLKEYS,CODCOM,COEIMP,LIVELLO"
              do vq_exec with 'GSDS4BCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCOE_IMP_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.CODCOM = _t2.CODCOM";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAMOL = _t2.QTAMOL";
                  +",DAAGGIO = _t2.DAAGGIO";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPCOE_IMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.CODCOM = _t2.CODCOM";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP, "+i_cQueryTable+" _t2 set ";
                  +"TMPCOE_IMP.QTAMOL = _t2.QTAMOL";
                  +",TMPCOE_IMP.DAAGGIO = _t2.DAAGGIO";
                  +Iif(Empty(i_ccchkf),"",",TMPCOE_IMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="TMPCOE_IMP.LVLKEYS = t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.CODCOM = t2.CODCOM";
                      +" and "+"TMPCOE_IMP.COEIMP = t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = t2.LIVELLO";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP set (";
                  +"QTAMOL,";
                  +"DAAGGIO";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.QTAMOL,";
                  +"t2.DAAGGIO";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.CODCOM = _t2.CODCOM";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP set ";
                  +"QTAMOL = _t2.QTAMOL";
                  +",DAAGGIO = _t2.DAAGGIO";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".LVLKEYS = "+i_cQueryTable+".LVLKEYS";
                      +" and "+i_cTable+".CODCOM = "+i_cQueryTable+".CODCOM";
                      +" and "+i_cTable+".COEIMP = "+i_cQueryTable+".COEIMP";
                      +" and "+i_cTable+".LIVELLO = "+i_cQueryTable+".LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAMOL = (select QTAMOL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",DAAGGIO = (select DAAGGIO from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Write into TMPCOE_IMP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="LVLKEYS,COEIMP,LIVELLO"
              do vq_exec with 'GSDS3BCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCOE_IMP_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAMOL = _t2.QTAMOL";
                  +",DAAGGIO = _t2.DAAGGIO";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPCOE_IMP, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP, "+i_cQueryTable+" _t2 set ";
                  +"TMPCOE_IMP.QTAMOL = _t2.QTAMOL";
                  +",TMPCOE_IMP.DAAGGIO = _t2.DAAGGIO";
                  +Iif(Empty(i_ccchkf),"",",TMPCOE_IMP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="TMPCOE_IMP.LVLKEYS = t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.COEIMP = t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = t2.LIVELLO";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP set (";
                  +"QTAMOL,";
                  +"DAAGGIO";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.QTAMOL,";
                  +"t2.DAAGGIO";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="TMPCOE_IMP.LVLKEYS = _t2.LVLKEYS";
                      +" and "+"TMPCOE_IMP.COEIMP = _t2.COEIMP";
                      +" and "+"TMPCOE_IMP.LIVELLO = _t2.LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCOE_IMP set ";
                  +"QTAMOL = _t2.QTAMOL";
                  +",DAAGGIO = _t2.DAAGGIO";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".LVLKEYS = "+i_cQueryTable+".LVLKEYS";
                      +" and "+i_cTable+".COEIMP = "+i_cQueryTable+".COEIMP";
                      +" and "+i_cTable+".LIVELLO = "+i_cQueryTable+".LIVELLO";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"QTAMOL = (select QTAMOL from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",DAAGGIO = (select DAAGGIO from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          VQ_EXEC( "..\DISB\EXE\QUERY\GSDS1BCI.VQR", this, "APPCOE")
          GSDS_BCI(this,.T.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          USE IN SELECT("APPCOE")
        endif
      endif
      if this.w_EXPDBLOG
        this.w_LIVFIN = Seconds()
        this.w_LIVTOT = this.w_LIVFIN - this.w_LIVINIZ
        * --- Aggiorno il log ogni volta che ho terminato l'operazione di esplosione del livello
        this.w_oEXPDBLOG.ADDMSGLOG("Esplosione livello %1, tempo %2, num righe %3", alltrim(str(this.w_LIVELLO)), this.TimeEXP(this.w_LIVTOT), alltrim(str(this.w_ROWS)))     
        this.w_LIVINIZ = Seconds()
      endif
      * --- Try
      local bErr_049168C0
      bErr_049168C0=bTrsErr
      this.Try_049168C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_ERR = MESSAGE()
        this.w_EXIT = .T.
        if this.w_EXPDBLOG
          this.w_oEXPDBLOG.ADDMSGLOG("Errore: %1", this.w_ERR)     
        endif
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_049168C0
      * --- End
      * --- Livello successivo
      * --- ----------------------------------------------------------------------------------------------
      this.w_AGLINIZ = seconds()
      * --- Aumento il livello
      this.w_LIVELLO = this.w_LIVELLO + 1
      * --- Componenti distinta base livello N+1
      if this.w_ChkDibaNoLegami = "S"
        * --- Insert into TMPDIS_COMP
        i_nConn=i_TableProp[this.TMPDIS_COMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDIS_COMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDSDBEX",this.TMPDIS_COMP_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Inserisco nella tabella le distinte senza legami validi
        * --- Insert into TMPDBNOLEG
        i_nConn=i_TableProp[this.TMPDBNOLEG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDBNOLEG_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS5BEX",this.TMPDBNOLEG_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ROWSDBNOLG = this.w_ROWSDBNOLG + i_ROWS
        * --- Delete from TMPDIS_COMP
        i_nConn=i_TableProp[this.TMPDIS_COMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPDIS_COMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Delete from TMPDISMBASE
      i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      this.w_AGLFIN = seconds()
      this.w_AGLTOT = this.w_AGLFIN-this.w_AGLINIZ
      if this.w_EXPDBLOG
        if this.w_ChkDibaNoLegami = "S"
          this.w_oEXPDBLOG.ADDMSGLOG("Controllo  errore (Distinta senza legami validi) livello %1 Tempo %2", ALLTRIM(STR(this.w_LIVELLO)) , this.TimeEXP(this.w_AGLTOT))     
        else
          this.w_oEXPDBLOG.ADDMSGLOG("Pulizia tabella TMPDISMBASE livello %1 Tempo %2", ALLTRIM(STR(this.w_LIVELLO)) , this.TimeEXP(this.w_AGLTOT))     
        endif
      endif
      * --- ----------------------------------------------------------------------------------------------
      * --- Se � attivo il check loop in distinta effetuo il controllo altrimenti passo oltre
      if this.w_ChkLoop="S"
        if this.w_ROWS > 0
          * --- Per ogni livello esploso controllo se esiste un loop in distinta in modo da non esploderlo pi�
          * --- w_LIVELLO  � gi� aggiornato
          this.w_AGLINIZ = seconds()
          * --- Aggiungo i codici distnta base di livello inferiore per controllo loop
          * --- Insert into TMPDISMBASE
          i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDSMBEX1",this.TMPDISMBASE_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Write into TMPEXPDB
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="LVLKEY"
            do vq_exec with 'GSDSLBEX1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPEXPDB_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="TMPEXPDB.LVLKEY = _t2.LVLKEY";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOOP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPEXPDB','LOOP');
                +",MSGERRORE = _t2.MSGERRORE";
                +i_ccchkf;
                +" from "+i_cTable+" TMPEXPDB, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="TMPEXPDB.LVLKEY = _t2.LVLKEY";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPEXPDB, "+i_cQueryTable+" _t2 set ";
            +"TMPEXPDB.LOOP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPEXPDB','LOOP');
                +",TMPEXPDB.MSGERRORE = _t2.MSGERRORE";
                +Iif(Empty(i_ccchkf),"",",TMPEXPDB.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="TMPEXPDB.LVLKEY = t2.LVLKEY";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPEXPDB set (";
                +"LOOP,";
                +"MSGERRORE";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC("S"),'TMPEXPDB','LOOP')+",";
                +"t2.MSGERRORE";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="TMPEXPDB.LVLKEY = _t2.LVLKEY";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPEXPDB set ";
            +"LOOP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPEXPDB','LOOP');
                +",MSGERRORE = _t2.MSGERRORE";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".LVLKEY = "+i_cQueryTable+".LVLKEY";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOOP ="+cp_NullLink(cp_ToStrODBC("S"),'TMPEXPDB','LOOP');
                +",MSGERRORE = (select MSGERRORE from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_ROWSLOOP = i_ROWS
          * --- Delete from TMPDISMBASE
          i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"1 = "+cp_ToStrODBC(1);
                   )
          else
            delete from (i_cTable) where;
                  1 = 1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_AGLFIN = seconds()
          this.w_AGLTOT = this.w_AGLFIN-this.w_AGLINIZ
          if this.w_EXPDBLOG
            this.w_oEXPDBLOG.ADDMSGLOG("Tempo controllo loop %1", this.TimeEXP(this.w_AGLTOT))     
          endif
        endif
      endif
      * --- Sicurezza al livello 40 esco dal ciclo
      if this.w_LIVELLO = 40
        this.w_EXIT = .T.
      endif
      if this.w_EXIT Or bTrsErr
        exit
      endif
    enddo
    * --- ----------------------------------------------------------------------------------------------
    if this.w_CoeffImp="S"
      * --- Drop temporary table TMPCOE_IMP
      i_nIdx=cp_GetTableDefIdx('TMPCOE_IMP')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPCOE_IMP')
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if this.w_ChkDibaNoLegami = "S" And this.w_ROWSDBNOLG > 0
      * --- Aggiornamento messaggistica errore (Distinta senza legami validi)
      this.w_AGLINIZ = seconds()
      * --- Delete from TMPDISMBASE
      i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where 1=1")
      else
        delete from (i_cTable) where 1=1
        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Insert into TMPDISMBASE
      i_nConn=i_TableProp[this.TMPDISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPDISMBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DIBA\EXE\QUERY\GSDBMBEX2",this.TMPDISMBASE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_PRIMOGIRO = .T.
      * --- Select from GSDB6BEX
      do vq_exec with 'GSDB6BEX',this,'_Curs_GSDB6BEX','',.f.,.t.
      if used('_Curs_GSDB6BEX')
        select _Curs_GSDB6BEX
        locate for 1=1
        do while not(eof())
        this.w_LVLKEY = _Curs_GSDB6BEX.LVLKEY
        if this.w_PRIMOGIRO
          this.w_OLDLVLKEY = this.w_LVLKEY
          this.w_PRIMOGIRO = .F.
        endif
        if this.w_OLDLVLKEY<>this.w_LVLKEY
          this.w_DESCRITV = LEFT(alltrim(this.w_DESCRITV) + SPACE(2) + this.w_MSGERRORE, 200)
          * --- Write into TMPEXPDB
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPEXPDB_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DESCRITV ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRITV),'TMPEXPDB','DESCRITV');
                +i_ccchkf ;
            +" where ";
                +"LVLKEY = "+cp_ToStrODBC(this.w_LVLKEY);
                   )
          else
            update (i_cTable) set;
                DESCRITV = this.w_DESCRITV;
                &i_ccchkf. ;
             where;
                LVLKEY = this.w_LVLKEY;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_MSGERRORE = " "
          this.w_OLDLVLKEY = this.w_LVLKEY
        endif
        this.w_DESCRITV = ALLTRIM(NVL(_Curs_GSDB6BEX.DESCRITV, ""))
        this.w_MSGERRORE = this.w_MSGERRORE + SPACE(2)+ ALLTRIM(NVL(_Curs_GSDB6BEX.MSGERRORE, ""))
          select _Curs_GSDB6BEX
          continue
        enddo
        use
      endif
      this.w_DESCRITV = LEFT(alltrim(this.w_DESCRITV) + SPACE(2) + this.w_MSGERRORE, 200)
      * --- Write into TMPEXPDB
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPEXPDB_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DESCRITV ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRITV),'TMPEXPDB','DESCRITV');
            +i_ccchkf ;
        +" where ";
            +"LVLKEY = "+cp_ToStrODBC(this.w_LVLKEY);
               )
      else
        update (i_cTable) set;
            DESCRITV = this.w_DESCRITV;
            &i_ccchkf. ;
         where;
            LVLKEY = this.w_LVLKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_AGLFIN = seconds()
      this.w_AGLTOT = this.w_AGLFIN-this.w_AGLINIZ
      if this.w_EXPDBLOG
        this.w_oEXPDBLOG.ADDMSGLOG("Tempo aggiornamento msg. errore (distinta base senza legami validi)  %1", this.TimeEXP(this.w_AGLTOT))     
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    do case
      case this.w_LatoServer<>"S"
        this.w_OQRY = IIF( this.w_LatoServer="O" , "..\DISB\EXE\QUERY\GSDS7BEX1.VQR", "..\DISB\EXE\QUERY\GSDS7BEX.VQR")
        * --- Controllo il numero di distinte che ho elaborato
        * --- Select from TMPEXPDB
        i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(NUMDIST) AS NUMDIST  from "+i_cTable+" TMPEXPDB ";
               ,"_Curs_TMPEXPDB")
        else
          select MAX(NUMDIST) AS NUMDIST from (i_cTable);
            into cursor _Curs_TMPEXPDB
        endif
        if used('_Curs_TMPEXPDB')
          select _Curs_TMPEXPDB
          locate for 1=1
          do while not(eof())
          this.w_NUMDIST = NVL(_Curs_TMPEXPDB.NUMDIST, 0)
            select _Curs_TMPEXPDB
            continue
          enddo
          use
        endif
        this.w_CURSORLIMIT = iif(vartype(g_EXPDIBACURSORLIMIT)="N", g_EXPDIBACURSORLIMIT, 5000)
        this.w_CURSORNUMBER = INT(this.w_NUMDIST / this.w_CURSORLIMIT) + 1
        if this.w_UnionCurs <> "S" And this.w_CURSORNUMBER > 1
          USE IN SELECT("_TAMEXPDB_")
          CREATE CURSOR _TAMEXPDB_ (cAlias C(200))
          * --- Non rimuovere la SELECT 0 altrimenti va in errore la query perch� nel cursore 
          *     � presente un campo con il nome cAlias e lo utilizza come parametro errando il nome dell'alias della query temporanea
          SELECT 0
          this.w_DADISTINTA = 0
          this.w_ADISTINTA = 0
          this.w_CURSORCOUNTER = 1
          do while this.w_ADISTINTA < this.w_NUMDIST
            * --- ----------------------------------------------------------------------------------------------
            this.w_AGLINIZ = seconds()
            * --- ----------------------------------------------------------------------------------------------
            this.w_DADISTINTA = IIF(this.w_DADISTINTA=0, 1, this.w_ADISTINTA + 1)
            this.w_ADISTINTA = this.w_ADISTINTA+this.w_CURSORLIMIT
            * --- Non rimuovere la SELECT 0 altrimenti va in errore la query perch� nel cursore 
            *     � presente un campo con il nome cAlias e lo utilizza come parametro errando il nome dell'alias della query temporanea
            SELECT 0
            VQ_EXEC( this.w_OQRY, this, this.OutCursor)
            SELECT (this.OutCursor)
            INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
            SET ORDER TO
            * --- Nome cursore che viene salvato su disco
            this.w_NAMECURS = tempadhoc()+"\"+sys(2015)+"_"+alltrim(this.OutCursor)+"_"+padl(alltrim(Str(this.w_CURSORCOUNTER,3,0)),3,"0")
            l_AliasName=rtrim(this.w_NAMECURS)+".dbf"
            SELECT (this.OutCursor)
            * --- Salvo su disco il cursore
            COPY TO (l_AliasName)
            INSERT INTO _TAMEXPDB_ (cAlias) VALUES (l_AliasName)
            * --- ----------------------------------------------------------------------------------------------
            this.w_AGLFIN = seconds()
            this.w_AGLTOT = this.w_AGLFIN-this.w_AGLINIZ
            if this.w_EXPDBLOG
              this.w_oEXPDBLOG.ADDMSGLOG("Tempo totale scrittura blocco dati %1 su disco  %2", alltrim(str(this.w_CURSORCOUNTER)), this.TimeEXP(this.w_AGLTOT))     
            endif
            * --- ----------------------------------------------------------------------------------------------
            this.w_CURSORCOUNTER = this.w_CURSORCOUNTER + 1
          enddo
        else
          * --- ----------------------------------------------------------------------------------------------
          this.w_AGLINIZ = seconds()
          * --- ----------------------------------------------------------------------------------------------
          this.w_DADISTINTA = 0
          this.w_ADISTINTA = 0
          USE IN SELECT("_TAMEXPDB_")
          * --- Non rimuovere la SELECT 0 altrimenti va in errore la query perch� nel cursore 
          *     � presente un campo con il nome cAlias e lo utilizza come parametro errando il nome dell'alias della query temporanea
          SELECT 0
          VQ_EXEC( this.w_OQRY, this, this.OutCursor)
          SELECT (this.OutCursor)
          INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
          SET ORDER TO
          * --- ----------------------------------------------------------------------------------------------
          this.w_AGLFIN = seconds()
          this.w_AGLTOT = this.w_AGLFIN-this.w_AGLINIZ
          if this.w_EXPDBLOG
            this.w_oEXPDBLOG.ADDMSGLOG("Tempo totale scrittura dati su disco  %1", this.TimeEXP(this.w_AGLTOT))     
          endif
          * --- ----------------------------------------------------------------------------------------------
        endif
    endcase
    * --- ----------------------------------------------------------------------------------------------
    this.w_DateTimeEnd = TTOC(DATETIME())
    this.w_FILFIN = seconds()
    this.w_FILTOT = this.w_FILFIN-this.w_FILINIZ
    if this.w_EXPDBLOG
      this.w_oEXPDBLOG.ADDMSGLOG("Tempo totale elaborazione %1", this.TimeEXP(this.w_FILTOT))     
      this.w_oEXPDBLOG.ADDMSGLOG("================================================================================")     
      this.w_oEXPDBLOG.ADDMSGLOG("Fine Esplosione [%1]", ALLTRIM(TTOC(DATETIME())))     
      if this.w_oEXPDBLOG.ISFULLLOG()
        * --- Scrivo il log nella cartella temporanea dell'utente
        this.w_LogFileName = tempadhoc()+"\"+LEFT(this.w_FileName, LEN(this.w_filename)-4)+".LOG"
        this.w_CursErrorLog = this.w_oEXPDBLOG.cCursMessErr
        l_ErrCurs = this.w_oEXPDBLOG.cCursMessErr
        SELECT(l_ErrCurs) 
 GO TOP
        this.hf_FILE_LOG = fcreate(this.w_LogFileName)
        if this.hf_FILE_LOG>=0
          do while not eof()
            this.w_cMsg = &l_ErrCurs..Msg
            * --- Scrive Log
            this.w_TEMP = fPuts(this.hf_FILE_LOG, this.w_cMsg)
            SELECT (l_ErrCurs)
            skip +1
          enddo
          =fCLOSE(this.hf_FILE_LOG)
        endif
      endif
    endif
    * --- Elimino tabelle temporaneee
    wait clear
    if this.w_LatoServer $ "O-P"
      * --- In questo caso non elimino la tabella temporanea
    else
      if this.w_LatoServer<>"S"
        * --- Drop temporary table TMPEXPDB
        i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPEXPDB')
        endif
      endif
    endif
    * --- Drop temporary table TMPDBNOLEG
    i_nIdx=cp_GetTableDefIdx('TMPDBNOLEG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDBNOLEG')
    endif
    * --- Drop temporary table TMPDISMBASE
    i_nIdx=cp_GetTableDefIdx('TMPDISMBASE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDISMBASE')
    endif
    * --- Drop temporary table TMPDIS_COMP
    i_nIdx=cp_GetTableDefIdx('TMPDIS_COMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDIS_COMP')
    endif
  endproc
  proc Try_049168C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_Operazione = "L"
        * --- Controllo loop in distinta l'esplosione sar� pi� light perch� servono meno informazioni
        * --- Insert into TMPEXPDB
        i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS3BEXL",this.TMPEXPDB_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ROWS = i_ROWS
        if this.w_EXPDBLOG
          this.w_LIVFIN = Seconds()
          this.w_LIVTOT = this.w_LIVFIN - this.w_LIVINIZ
          * --- Aggiorno il log ogni volta che ho terminato l'operazione di esplosione del livello
          this.w_oEXPDBLOG.ADDMSGLOG("Esplosione livello %1, tempo %2, num righe %3", alltrim(str(this.w_LIVELLO)), this.TimeEXP(this.w_LIVTOT), alltrim(str(this.w_ROWS)))     
          this.w_LIVINIZ = Seconds()
        endif
      otherwise
        * --- Altrimenti esplodo le distinte leggendo dalle distinte in linea
        * --- Query su DIS_COMP
        if this.w_Operazione = "R"
          * --- Configuratore di caratteristiche
          * --- Insert into TMPEXPDB
          i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS3BEXR",this.TMPEXPDB_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_ROWS = i_ROWS
        else
          if this.w_CoeffImp="S" AND this.w_ROWSCOIM > 0
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS3BEX1",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS3BEX",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          this.w_ROWS = i_ROWS
        endif
        * --- Leggo i componenti a variante in base alla modalit� di utilizzo delle varianti (padre supremo/padre di livello)
        if this.w_CoeffImp="S" AND this.w_ROWSCOIMV > 0
          if this.w_TVPADSUPREMO
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS32BEX1",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS31BEX1",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        else
          if this.w_TVPADSUPREMO
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS32BEX",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPEXPDB
            i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS31BEX",this.TMPEXPDB_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        this.w_ROWS = this.w_ROWS + i_ROWS
        if this.w_EXPDBLOG
          this.w_LIVFIN = Seconds()
          this.w_LIVTOT = this.w_LIVFIN - this.w_LIVINIZ
          * --- Aggiorno il log ogni volta che ho terminato l'operazione di esplosione del livello
          this.w_oEXPDBLOG.ADDMSGLOG("Esplosione (distinta in linea) livello %1, tempo %2, num righe %3", alltrim(str(this.w_LIVELLO)), this.TimeEXP(this.w_LIVTOT), alltrim(str(this.w_ROWS)))     
          this.w_LIVINIZ = Seconds()
        endif
    endcase
    if this.w_ROWS <= 0
      this.w_EXIT = .T.
    endif
    ah_msg("Esplosione livello %1 %0 Righe esplose %2...",.t.,.t.,.f.,alltrim(str(this.w_LIVELLO+1)) , alltrim(str(this.w_ROWS)) )
    return


  proc Init(oParentObject,cOperazione,InCursor,cCoeffImp,DistintaINI,DistintaFIN,TipoArticolo,OutCursor,cDisPro,cExplMP,cDatRif,cQtaExp,nMaxLevel,cChkLoop,cFldesart,cUnionCurs,cChkDibaNoLegami,cLatoServer)
    this.cOperazione=cOperazione
    this.InCursor=InCursor
    this.cCoeffImp=cCoeffImp
    this.DistintaINI=DistintaINI
    this.DistintaFIN=DistintaFIN
    this.TipoArticolo=TipoArticolo
    this.OutCursor=OutCursor
    this.cDisPro=cDisPro
    this.cExplMP=cExplMP
    this.cDatRif=cDatRif
    this.cQtaExp=cQtaExp
    this.nMaxLevel=nMaxLevel
    this.cChkLoop=cChkLoop
    this.cFldesart=cFldesart
    this.cUnionCurs=cUnionCurs
    this.cChkDibaNoLegami=cChkDibaNoLegami
    this.cLatoServer=cLatoServer
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='*TMPEXPDBS'
    this.cWorkTables[2]='*TMPDBNOLEG'
    this.cWorkTables[3]='*TMPDISMBASE'
    this.cWorkTables[4]='*TMPDIS_COMP'
    this.cWorkTables[5]='*TMPEXPDB'
    this.cWorkTables[6]='PAR_DISB'
    this.cWorkTables[7]='*TMPCOE_IMP'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSDB6BEX')
      use in _Curs_GSDB6BEX
    endif
    if used('_Curs_TMPEXPDB')
      use in _Curs_TMPEXPDB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsds_bex
  func cp_bintoc(i_cNum)
    local i_p1,i_p2,i_p3,i_base,i_TmpN
    store 0 to i_p1,i_p2,i_p3
    i_base=256-asc('A')
  
    * prima cifra
    i_TmpN=i_cNum
    i_p3=mod(i_TmpN,i_base)
    * seconda cifra
    i_TmpN=int(i_TmpN/i_Base)
    if i_TmpN>0
      i_p2=mod(i_TmpN,i_base)
      * Terza cifra
      i_TmpN=int(i_TmpN/i_Base)
      if i_TmpN>0
        i_p1=mod(i_TmpN,i_base)
      endif
    endif
    *
    i_p1= i_p1+asc('A')
    i_p2= i_p2+asc('A')
    i_p3= i_p3+asc('A')
    *
    return chr(i_p1)+chr(i_p2)+chr(i_p3)
  
  Function TimeEXP(w_second)
  local timemessage
  if w_second<60
    if int(w_second)=w_second
      timemessage=alltrim(str(w_second,10))+space(1)+ah_msgformat('secondi')
    else
      timemessage=alltrim(str(w_second,10,3))+space(1)+ah_msgformat('secondi')
    endif
  else
    if w_second/60<60
      if int(w_second/60)=1
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuto')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')    
      else
        timemessage=alltrim(str(int(w_second/60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    else
      if int(w_second/60/60)=1
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ora')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      else
        timemessage=alltrim(str(int(w_second/60/60),10))+space(1)+ah_msgformat('ore')+space(1)+alltrim(str(mod(int(w_second/60),60),10))+space(1)+ah_msgformat('minuti')+space(1)+alltrim(str(mod(w_second,60),10))+space(1)+ah_msgformat('secondi')
      endif
    endif
  endif
  return timemessage
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="cOperazione,InCursor,cCoeffImp,DistintaINI,DistintaFIN,TipoArticolo,OutCursor,cDisPro,cExplMP,cDatRif,cQtaExp,nMaxLevel,cChkLoop,cFldesart,cUnionCurs,cChkDibaNoLegami,cLatoServer"
endproc
