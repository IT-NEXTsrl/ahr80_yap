* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_kda                                                        *
*              Visualizza associazione distinta/articoli                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-08                                                      *
* Last revis.: 2015-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_kda",oParentObject))

* --- Class definition
define class tgsds_kda as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 717
  Height = 523
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-02-06"
  HelpContextID=186030999
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  DISTBASE_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsds_kda"
  cComment = "Visualizza associazione distinta/articoli"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DBCODICI = space(20)
  w_DBCODICF = space(20)
  w_CODART = space(20)
  o_CODART = space(20)
  w_CODART2 = space(20)
  w_ARPROPRE = space(1)
  w_FLARNODB = space(1)
  w_DBDESCRI = space(40)
  w_DBDESCRF = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESART = space(40)
  w_DESART2 = space(40)
  w_TIPART = space(2)
  w_DBCODICE = space(20)
  w_ARCODART = space(20)
  w_ARTIPART = space(10)
  w_TIPART2 = space(2)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_kdaPag1","gsds_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDBCODICI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='DISTBASE'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DBCODICI=space(20)
      .w_DBCODICF=space(20)
      .w_CODART=space(20)
      .w_CODART2=space(20)
      .w_ARPROPRE=space(1)
      .w_FLARNODB=space(1)
      .w_DBDESCRI=space(40)
      .w_DBDESCRF=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_DESART2=space(40)
      .w_TIPART=space(2)
      .w_DBCODICE=space(20)
      .w_ARCODART=space(20)
      .w_ARTIPART=space(10)
      .w_TIPART2=space(2)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_DBCODICI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_DBCODICF))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODART))
          .link_1_3('Full')
        endif
        .w_CODART2 = .w_CODART
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODART2))
          .link_1_4('Full')
        endif
        .w_ARPROPRE = 'T'
        .w_FLARNODB = 'D'
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(7,8,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(10,13,.f.)
        .w_DBCODICE = NVL(.w_ZOOM.GETVAR('DBCODICE') , SPACE(20))
        .w_ARCODART = NVL(.w_ZOOM.GETVAR('ARCODART') , SPACE(20))
        .w_ARTIPART = NVL(.w_ZOOM.GETVAR('ARTIPART') , SPACE(2))
    endwith
    this.DoRTCalc(17,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CODART<>.w_CODART
            .w_CODART2 = .w_CODART
          .link_1_4('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(5,13,.t.)
            .w_DBCODICE = NVL(.w_ZOOM.GETVAR('DBCODICE') , SPACE(20))
            .w_ARCODART = NVL(.w_ZOOM.GETVAR('ARCODART') , SPACE(20))
            .w_ARTIPART = NVL(.w_ZOOM.GETVAR('ARTIPART') , SPACE(2))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsds_kda
    if cEvent == "InterrogaArticolo"
        if This.w_ARTIPART = 'CC' and g_CCAR='S'
               OpenGest('A', 'GSCR_AAR', 'ARCODART' , This.w_ARCODART)
         else
               OpenGest('A', 'GSMA_AAR', 'ARCODART' , This.w_ARCODART)
         endif
    
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DBCODICI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODICI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_DBCODICI)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_DBCODICI))
          select DBCODICE,DBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODICI)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODICI) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oDBCODICI_1_1'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODICI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODICI)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODICI = NVL(_Link_.DBCODICE,space(20))
      this.w_DBDESCRI = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODICI = space(20)
      endif
      this.w_DBDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODICI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODICF
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODICF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_DBCODICF)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_DBCODICF))
          select DBCODICE,DBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODICF)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODICF) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oDBCODICF_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODICF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODICF)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODICF = NVL(_Link_.DBCODICE,space(20))
      this.w_DBDESCRF = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODICF = space(20)
      endif
      this.w_DBDESCRF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODICF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_3'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-CC'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART2)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART2))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART2)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART2)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART2)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART2) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART2_1_4'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART2)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART2 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART2 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART2 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART2 = space(20)
      endif
      this.w_DESART2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART2 $ 'PF-SE-MP-PH-MC-MA-IM-CC' AND  ((UPPER(.w_CODART) <= UPPER(.w_CODART2)) or (empty(.w_CODART2)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART2 = space(20)
        this.w_DESART2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDBCODICI_1_1.value==this.w_DBCODICI)
      this.oPgFrm.Page1.oPag.oDBCODICI_1_1.value=this.w_DBCODICI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODICF_1_2.value==this.w_DBCODICF)
      this.oPgFrm.Page1.oPag.oDBCODICF_1_2.value=this.w_DBCODICF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_3.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_3.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART2_1_4.value==this.w_CODART2)
      this.oPgFrm.Page1.oPag.oCODART2_1_4.value=this.w_CODART2
    endif
    if not(this.oPgFrm.Page1.oPag.oARPROPRE_1_5.RadioValue()==this.w_ARPROPRE)
      this.oPgFrm.Page1.oPag.oARPROPRE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARNODB_1_6.RadioValue()==this.w_FLARNODB)
      this.oPgFrm.Page1.oPag.oFLARNODB_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_11.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_11.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRF_1_15.value==this.w_DBDESCRF)
      this.oPgFrm.Page1.oPag.oDBDESCRF_1_15.value=this.w_DBDESCRF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_18.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_18.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART2_1_19.value==this.w_DESART2)
      this.oPgFrm.Page1.oPag.oDESART2_1_19.value=this.w_DESART2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-CC')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPART2 $ 'PF-SE-MP-PH-MC-MA-IM-CC' AND  ((UPPER(.w_CODART) <= UPPER(.w_CODART2)) or (empty(.w_CODART2))))  and not(empty(.w_CODART2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    return

enddefine

* --- Define pages as container
define class tgsds_kdaPag1 as StdContainer
  Width  = 713
  height = 523
  stdWidth  = 713
  stdheight = 523
  resizeXpos=276
  resizeYpos=215
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDBCODICI_1_1 as StdField with uid="QFDBXBQHAZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DBCODICI", cQueryName = "DBCODICI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base inesistente",;
    HelpContextID = 123071873,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=9, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODICI"

  func oDBCODICI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODICI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODICI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oDBCODICI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDBCODICF_1_2 as StdField with uid="MIRCUHWYAM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DBCODICF", cQueryName = "DBCODICF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base inesistente",;
    HelpContextID = 123071876,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=34, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODICF"

  func oDBCODICF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODICF_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODICF_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oDBCODICF_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODART_1_3 as StdField with uid="HLUYFRZCXR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 193188058,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=61, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oCODART2_1_4 as StdField with uid="OEPPCSVAVH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODART2", cQueryName = "CODART2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 193188058,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=107, Top=85, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART2"

  func oCODART2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART2
     i_obj.ecpSave()
  endproc


  add object oARPROPRE_1_5 as StdCombo with uid="WWUGTAUJOB",rtseq=5,rtrep=.f.,left=107,top=114,width=153,height=21;
    , HelpContextID = 262278581;
    , cFormVar="w_ARPROPRE",RowSource=""+"Interna,"+"Esterna,"+"C/lavoro,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPROPRE_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oARPROPRE_1_5.GetRadio()
    this.Parent.oContained.w_ARPROPRE = this.RadioValue()
    return .t.
  endfunc

  func oARPROPRE_1_5.SetRadio()
    this.Parent.oContained.w_ARPROPRE=trim(this.Parent.oContained.w_ARPROPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ARPROPRE=='I',1,;
      iif(this.Parent.oContained.w_ARPROPRE=='E',2,;
      iif(this.Parent.oContained.w_ARPROPRE=='L',3,;
      iif(this.Parent.oContained.w_ARPROPRE=='T',4,;
      0))))
  endfunc


  add object oFLARNODB_1_6 as StdCombo with uid="JUDAPHANWM",rtseq=6,rtrep=.f.,left=359,top=114,width=198,height=21;
    , HelpContextID = 256703640;
    , cFormVar="w_FLARNODB",RowSource=""+"Distinta associata ad articolo,"+"Distinta non associata ad articolo,"+"Articoli senza distinta associata,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLARNODB_1_6.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oFLARNODB_1_6.GetRadio()
    this.Parent.oContained.w_FLARNODB = this.RadioValue()
    return .t.
  endfunc

  func oFLARNODB_1_6.SetRadio()
    this.Parent.oContained.w_FLARNODB=trim(this.Parent.oContained.w_FLARNODB)
    this.value = ;
      iif(this.Parent.oContained.w_FLARNODB=='D',1,;
      iif(this.Parent.oContained.w_FLARNODB=='N',2,;
      iif(this.Parent.oContained.w_FLARNODB=='A',3,;
      iif(this.Parent.oContained.w_FLARNODB=='T',4,;
      0))))
  endfunc


  add object oBtn_1_7 as StdButton with uid="MZXVNEFMNO",left=654, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'associazione distinta base/articolo";
    , HelpContextID = 173917674;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .Notifyevent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="MJNXVZGNOX",left=654, top=472, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193348422;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_zoombox with uid="POIEQWYSZF",left=7, top=140, width=704,height=332,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ART_ICOL",cZoomFile="GSDS_KDA",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 172842266

  add object oDBDESCRI_1_11 as StdField with uid="ZSHZCEUWBR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208657793,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=264, Top=9, InputMask=replicate('X',40)

  add object oDBDESCRF_1_15 as StdField with uid="YEITLXYYOF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DBDESCRF", cQueryName = "DBDESCRF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208657796,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=264, Top=34, InputMask=replicate('X',40)

  add object oDESART_1_18 as StdField with uid="DHPYTCRITK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193129162,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=264, Top=61, InputMask=replicate('X',40)

  add object oDESART2_1_19 as StdField with uid="YXMLRSMWQS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESART2", cQueryName = "DESART2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193129162,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=264, Top=85, InputMask=replicate('X',40)


  add object oBtn_1_24 as StdButton with uid="ADCPXDWAYE",left=586, top=472, width=48,height=45,;
    CpPicture="bmp\kit.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'articolo";
    , HelpContextID = 256421259;
    , caption='\<Articolo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      this.parent.oContained.NotifyEvent("InterrogaArticolo")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="ISRNWLUDKR",left=533, top=472, width=48,height=45,;
    CpPicture="BMP\verifica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla distinta";
    , HelpContextID = 2242967;
    , caption='\<Distinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSDS_MDB', 'DBCODICE' , .w_DBCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DBCODICE))
      endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="TVUMRBNTHK",Visible=.t., Left=31, Top=13,;
    Alignment=1, Width=72, Height=18,;
    Caption="Da distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ECZKDXFIMD",Visible=.t., Left=29, Top=115,;
    Alignment=1, Width=73, Height=18,;
    Caption="Prov. articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HIJHVUAULC",Visible=.t., Left=284, Top=115,;
    Alignment=1, Width=73, Height=18,;
    Caption="Crit. selez.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RZFKBKKZBH",Visible=.t., Left=31, Top=35,;
    Alignment=1, Width=72, Height=18,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="XFMLZQKRGB",Visible=.t., Left=41, Top=63,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="OSDTYNEQSK",Visible=.t., Left=37, Top=86,;
    Alignment=1, Width=66, Height=18,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
