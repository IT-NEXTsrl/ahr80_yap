* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bcc                                                        *
*              Valorizza p.finiti dai componenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_416]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-06                                                      *
* Last revis.: 2007-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bcc",oParentObject)
return(i_retval)

define class tgsds_bcc as StdBatch
  * --- Local variables
  w_ULTLIV = 0
  w_CODRIS = space(15)
  w_ARTCOM = space(20)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_RECSCA = 0
  w_PERRIC = 0
  w_FLESPL = space(1)
  w_COSCIC = 0
  w_PREZZO = 0
  w_COSTOT = 0
  w_APPO = 0
  w_CODVAL = space(3)
  w_SETUP = space(1)
  w_QTANET = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di Calcolo del valore dei Prodotti Finiti in Base ai Componenti + Cicli (da GSDS_BCA)
    * --- Procedura derivata dalla Stampa Distinta Costificata
    this.w_ULTLIV = 0
    if USED("TMP2EXP")
      SELECT TMP2EXP
      USE
    endif
    if  used("TMPEXP")
      SELECT * FROM TMPEXP WHERE ROWPAD=this.oParentObject.w_ROWPAD INTO TABLE TMP2EXP 
      * --- Calcola i Costi Unitari e Globali
      SELECT TMP2EXP
      if RECCOUNT()>0
        * --- A questo punto riposiziono sul Cursore principale e ciclo sui Componenti (foglie)
        GO TOP
        SCAN FOR FLESPL<>"S" AND VAL(NUMLEV)>0 AND QTAMOV<>0
        this.w_ULTLIV = MAX(this.w_ULTLIV, VAL(NUMLEV))
        this.w_ARTCOM = ARTCOM
        this.w_RECSCA = RECSCA
        this.w_PERRIC = PERRIC
        this.w_QTAMOV = QTAMOV
        * --- Leggo il Costo Unitario calcolato in Precedenza 
        this.w_PREZZO = 0
        SELECT TMPFIN
        GO TOP
        LOCATE FOR ROWPAD=this.oParentObject.w_ROWPAD AND ARTCOM=this.w_ARTCOM 
        if FOUND()
          this.w_PREZZO = cp_ROUND(NVL(PREZZO, 0), g_PERPUL)
          * --- Costo Unitario Complessivo (considerati Recupero e Ricarico)
          if this.w_PERRIC<>0 OR this.w_RECSCA<>0
            * --- % Recupero + % Ricarico
            this.w_PREZZO = cp_ROUND((this.w_PREZZO * (100 + this.w_PERRIC)) / 100, 6)
            this.w_PREZZO = cp_ROUND((this.w_PREZZO * (100 + this.w_RECSCA)) / 100, 6)
          endif
        endif
        this.w_COSTOT = cp_ROUND(this.w_PREZZO * this.w_QTAMOV, 6)
        * --- Aggiorno l'Importo Totale e le % di Recupero per i Componenti Foglie 
        SELECT TMP2EXP
        REPLACE PREZZO WITH this.w_PREZZO, COSTOT WITH this.w_COSTOT
        ENDSCAN 
      endif
      * --- In base al Tipo di valorizzazione ...
      * --- Torna un Cursore contenete tutti gli Articoli associati al Listino/Inventario con i loro Valori 
      * --- Calcolo i Cicli Semplificati e i Valori ad essi Associati
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ULTLIV>0
        * --- Alla fine devo aggiornare in Costi dei Semilavorati/Distinta calcolati sulla Somma dei rispettivi Componenti (foglie) piu' i vari Scarti/Recuperi/Ricarichi.
        this.w_PREZZO = 0
        this.w_COSTOT = 0
        FOR LIV=this.w_ULTLIV TO 1 STEP -1
        SELECT TMP2EXP
        GO BOTTOM
        * --- Ciclo sui Componenti la Distinta partendo dal livello piu' Basso, aggiornando progressivamente il livello immediatamente superiore
        do while NOT BOF()
          this.w_ARTCOM = ARTCOM
          this.w_RECSCA = RECSCA
          this.w_PERRIC = PERRIC
          this.w_QTAMOV = QTAMOV
          this.w_QTAUM1 = QTAUM1
          this.w_COSCIC = COSCIC
          this.w_PREZZO = PREZZO
          if VAL(NUMLEV)=LIV AND FLESPL<>"S"
            * --- Componente 'Foglia' calcolo i Totali Importi (L'importo unitario verra' poi diviso per le Quantita' del Semilavorato)
            this.w_COSTOT = this.w_COSTOT + cp_ROUND((this.w_QTAMOV * this.w_PREZZO), 6)
          endif
          if VAL(NUMLEV) = (LIV-1) AND (FLESPL="S" OR VAL(NUMLEV)=0)
            * --- Componente Semilavorato o Distinta, Aggiorno considerando anche gli Scarti/Recuperi/Ricarichi del Semilavorato.
            this.w_PREZZO = cp_ROUND(this.w_COSTOT/this.w_QTAMOV, g_PERPUL)
            * --- % Ricarico
            * --- Costo Unitario Complessivo (considerati Recupero e Ricarico)
            if this.w_PERRIC<>0 OR this.w_RECSCA<>0
              * --- % Recupero + % Ricarico
              this.w_PREZZO = cp_ROUND((this.w_PREZZO * (100 + this.w_PERRIC)) / 100, 6)
              this.w_PREZZO = cp_ROUND((this.w_PREZZO * (100 + this.w_RECSCA)) / 100, 6)
            endif
            * --- Aggiunge eventuale Ciclo di lavorazione (Unitario)
            this.w_PREZZO = this.w_PREZZO + this.w_COSCIC
            this.w_COSTOT = cp_ROUND(this.w_PREZZO * this.w_QTAMOV, 6)
            * --- Aggiorna gli Importi del Semilavorato/P.F.
            REPLACE PREZZO WITH this.w_PREZZO, COSTOT WITH this.w_COSTOT
            * --- Trucco:
            * --- Toglie il Flag Esplosione al Semilavorato in modo da considerarlo "componente foglia" rispetto al Semilavorato/Prod.Finito di livello superiore
            REPLACE FLESPL WITH " "
            if VAL(NUMLEV)=0
              * --- Aggiorna gli Importi del Prodotto Finito
              this.oParentObject.w_PRZFIN = cp_ROUND(this.w_COSTOT / this.w_QTAMOV, g_PERPUL)
            endif
            * --- Riazzera
            this.w_PREZZO = 0
            this.w_COSTOT = 0
          endif
          SKIP -1
        enddo
        ENDFOR 
      endif
    endif
    if USED("TMP2EXP")
      SELECT TMP2EXP
      USE
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Cicli Semlificati associati alla Distinta Base ed ai suoi Componenti.
    if USED("TMPRIS")
      SELECT TMPRIS
      if RECCOUNT()>0
        GO TOP
        SCAN FOR ROWPAD=this.oParentObject.w_ROWPAD AND NOT EMPTY(NVL(CODRIS,""))
        this.w_CODRIS = CODRIS
        this.w_COSCIC = 0
        this.w_QTANET = 0
        this.w_QTAMOV = QTAUM1
        * --- Per ciascun Ciclo eseguo la query di Ricerca delle Risorse associate
        vq_exec("..\DISB\EXE\QUERY\GSDSCBEA.VQR", this,"APPORIS")
        if USED("APPORIS")
          SELECT APPORIS
          GO TOP
          SCAN FOR NOT EMPTY(NVL(CSCODICE, "")) AND NVL(RIPREZZO, 0)<>0
          this.w_UNIMIS = NVL(UNIMIS, "  ")
          this.w_CODVAL = NVL(RICODVAL, g_PERVAL)
          this.w_PREZZO = NVL(RIPREZZO, 0)
          this.w_SETUP = NVL(CS_SETUP, " ")
          this.w_QTANET = NVL(QTAMOV, 0)
          this.w_QTAUM1 = IIF(this.w_SETUP="S", 1, this.w_QTAMOV) * this.w_QTANET
          if this.w_CODVAL<>g_PERVAL AND NOT EMPTY(this.w_CODVAL)
            * --- Allinea alla Valuta di Conto
            this.w_APPO = GETCAM(this.w_CODVAL, this.oParentObject.w_DATFIL)
            this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_APPO, g_CAOVAL, this.oParentObject.w_DATFIL, g_PERPUL)
            SELECT APPORIS
          endif
          if this.w_UNIMIS<>NVL(RIUNMIS1,"   ") AND NVL(RIMOLTIP, 0)<>0 AND NOT EMPTY(NVL(RIOPERAT," "))
            this.w_PREZZO = cp_ROUND((this.w_PREZZO * IIF(RIOPERAT="*", 1, RIMOLTIP))/IIF(RIOPERAT="*", RIMOLTIP, 1), g_PERPUL)
          endif
          * --- Costo Totale
          * --- Costo Unitario di Lavorazione del Semilavorato/Prod.Finito 
          this.w_COSCIC = this.w_COSCIC + cp_ROUND(this.w_PREZZO * this.w_QTAUM1, 5)
          SELECT APPORIS
          ENDSCAN
          * --- Chiude il Cursore della Query 
          SELECT APPORIS
          USE
          if this.w_COSCIC<>0
            * --- Alla Fine il Totale del Costo viene scritto sul Campo Importo Totale del Componente associato
            SELECT TMPRIS
            this.w_ARTCOM = ARTCOM
            this.w_CODRIS = CODRIS
            SELECT TMP2EXP
            LOCATE FOR ARTCOM=this.w_ARTCOM AND CODRIS=this.w_CODRIS AND (FLESPL="S" OR VAL(NUMLEV)=0)
            if FOUND()
              this.w_QTAMOV = QTAMOV
              if this.w_QTAMOV<>0
                * --- Riporta il Valore Unitario
                this.w_COSCIC = cp_ROUND(this.w_COSCIC / this.w_QTAMOV, 5)
              endif
              REPLACE COSCIC WITH this.w_COSCIC
            endif
          endif
        endif
        SELECT TMPRIS
        ENDSCAN 
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
