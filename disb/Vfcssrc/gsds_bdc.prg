* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bdc                                                        *
*              Batch stampa distinta base costificata                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1391]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2018-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM1,w_cCursor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bdc",oParentObject,m.w_PARAM1,m.w_cCursor)
return(i_retval)

define class tgsds_bdc as StdBatch
  * --- Local variables
  w_PARAM1 = space(10)
  w_cCursor = space(10)
  w_PDCOSPAR = space(0)
  w_Analisi = space(1)
  w_QUANTI = 0
  w_PERRIC = space(1)
  w_TipoValo = space(1)
  w_TipoValoOld = space(1)
  w_Listino = space(5)
  w_NUMINV = space(6)
  w_VALLIS = space(3)
  w_VALESE = space(3)
  w_SCDRSR = space(1)
  w_TIPOLN = space(1)
  w_CAOVAL = 0
  w_CODMAG = space(5)
  w_DATSTA = ctod("  /  /  ")
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_FLPROV = space(1)
  w_CAOESE = 0
  w_OQRY = space(50)
  w_CAOLIS = 0
  w_FLDESART = space(1)
  w_ESERC = space(4)
  w_DATFIL = ctod("  /  /  ")
  w_REP = space(50)
  w_QRY = space(50)
  QTC = 0
  QTN = 0
  QTR = 0
  w_OREP = space(50)
  w_TIPCOS = space(1)
  w_LOTTOMED = .f.
  w_VARIAR = space(20)
  w_CODIAR = space(20)
  w_CAKEYSAL = space(40)
  w_KEYRIF = space(10)
  w_LenLvl = 0
  w_maxlvl = 0
  w_MESS = space(10)
  w_CODTMP = space(5)
  w_CODLEN = 0
  w_i = 0
  w_ERRO = .f.
  w_DecStampa = 0
  w_PARAM = space(1)
  w_INNUMPRE = space(6)
  w_oldkey2 = space(10)
  w_STAMPA = .f.
  w_AGGCSSTD = .f.
  w_GetCollateLvlKey = space(20)
  w_FRASE = space(1)
  w_iNConn = 0
  w_iRows = 0
  w_DIMLVLKEY = space(10)
  Padre = .NULL.
  w_ClsName = space(10)
  w_LatoServer = space(1)
  w_LIVFIN = space(1)
  w_PARZ = space(1)
  w_NOLIVFIN = space(1)
  w_PARELAB = .f.
  w_ClsName1 = space(10)
  w_AGCOSSTD = space(1)
  Objmask = .NULL.
  w_VALCOM = space(1)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_APPO = 0
  w_CODDIS = space(20)
  w_CODART = space(20)
  w_DISPAD = space(20)
  w_ARTPAD = space(20)
  w_CODRIS = space(15)
  w_ORDINE = space(1)
  w_NUMLEV = 0
  w_NUMERO = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_ARTCOM = space(20)
  w_DESPAD = space(15)
  w_UNIMIS = space(3)
  w_QTANET = 0
  w_QTANE1 = 0
  w_QTALOR = 0
  w_QTALO1 = 0
  w_PERSCA = 0
  w_RECSCA = 0
  w_PERSFR = 0
  w_RECSFR = 0
  w_FLVARI = space(1)
  w_FLESPL = space(1)
  w_FLCOMP = space(1)
  w_cosuni = 0
  w_coscic = 0
  w_PROPRE = space(1)
  w_NUMRIG = 0
  w_QTAFIN = 0
  w_QTAFIN1 = 0
  w_PERREC = 0
  w_QTAREC = 0
  w_QTARE1 = 0
  w_QTAPARZ = 0
  w_QTAPAR1 = 0
  i_cNum = 0
  w_LVLKEY = space(200)
  w_OLDKEY = space(200)
  w_APPOLW = space(200)
  w_OLDLEVEL = 0
  w_NUMLEW = 0
  w_OLDQTAFIN = 0
  w_OLDQTAFIN1 = 0
  w_TMPCUR = space(10)
  w_MultiReportVarEnv = .NULL.
  w_CosRec = 0
  w_CMNETSEL = 0
  w_CMSCASEL = 0
  w_CMRICSEL = 0
  w_CMNETSTD = 0
  w_CMSCASTD = 0
  w_CMRICSTD = 0
  w_CMNETMED = 0
  w_CMSCAMED = 0
  w_CMRICMED = 0
  w_CMNETULT = 0
  w_CMSCAULT = 0
  w_CMRICULT = 0
  w_CSNETSEL = 0
  w_CSSCASEL = 0
  w_CSRICSEL = 0
  w_cossta = 0
  w_cosmed = 0
  w_cosult = 0
  w_cosselez = 0
  w_TROVATO = .f.
  w_qtaparzin = 0
  w_qtaparzil = 0
  w_COECONV = 0
  w_COPERRIC = 0
  w_CONTRATTO = .f.
  w_cosappo = 0
  w_RECADDED = .f.
  w_CMRSCSTD = 0
  w_CMRSCSEL = 0
  w_CMRSCMED = 0
  w_CMRSCULT = 0
  w_cossetup = 0
  w_CSELSETU = 0
  w_CSSCSETU = 0
  w_CSRSSETU = 0
  w_cosunist = 0
  w_cosunime = 0
  w_cosuniul = 0
  w_qtaparzif = 0
  w_CSRSCSEL = 0
  w_costotcic = 0
  w_FORNITO = .f.
  w_qtalmstp = 0
  w_csapposetu = 0
  w_FLCOMPPAD = space(1)
  w_CALVLKEY = space(200)
  w_CAUNMISU = space(3)
  w_CAQTAMOV = 0
  w_CAQTAMO1 = 0
  w_ROWNUM = 0
  w_bNoUseRowNum = .f.
  w_Tmp = space(20)
  w_TmpQuanti = 0
  w_DECTOT = 0
  w_CODVAL = space(3)
  w_LIPREZZO = 0
  w_MAQTA1UM = 0
  w_UNMIS1 = space(3)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_CODLIS = space(0)
  w_SCOLIS = space(1)
  w_CALPRZ = 0
  w_UMLIPREZZO = space(3)
  w_UNIMISLI = space(3)
  w_QTALIS = 0
  w_INNUMPRE = space(6)
  w_PRCSLAVO = 0
  w_PRCSMATE = 0
  w_PRCOSSTA = 0
  w_PRCULAVO = 0
  w_PRCUMATE = 0
  w_PRCMLAVO = 0
  w_PRCMMATE = 0
  w_PRCLLAVO = 0
  w_PRCLMATE = 0
  w_STD = space(1)
  w_UTE = space(1)
  w_ULT = space(1)
  w_MED = space(1)
  w_LST = space(1)
  w_PRKEYSAL = space(40)
  w_PRKEYSAL = space(40)
  w_CODFOR = space(15)
  w_CODFAS = space(41)
  w_VALCONT = space(3)
  w_PREZZO = 0
  w_PRZSET = 0
  w_CAO = 0
  w_FORNITO = .f.
  w_mykey = space(10)
  w_codiartic1 = space(20)
  w_codiartico = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = space(1)
  w_MOLTI3 = space(1)
  w_RIQTAMOV = 0
  w_DPCOSSTA = 0
  w_DPCOSSTA = 0
  w_PRENAZ = 0
  w_PREUM = 0
  w_OBTEST = ctod("  /  /  ")
  w_DELROW = .f.
  w_SRQTARIS = 0
  w_Recno = 0
  w_oldkey = space(10)
  w_CONUMERO = space(15)
  w_INDATINV = ctod("  /  /  ")
  w_INCODESE = space(4)
  w_INESEPRE = space(4)
  w_INCODMAG = space(5)
  w_INCATOMO = space(5)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INMAGCOL = space(1)
  w_PRDATINV = ctod("  /  /  ")
  w_MAGRAG = space(5)
  w_ESCONT = space(1)
  w_DECESE = 0
  w_CAOPRE = 0
  w_FlaVal = space(10)
  w_Codice = space(40)
  w_CriVal = space(10)
  w_CodArt = space(20)
  w_PreQtaEsi = 0
  w_CodVar = space(20)
  w_PreQtaVen = 0
  w_QtaEsi = 0
  w_PreQtaAcq = 0
  w_QtaVen = 0
  w_PrePrzMpa = 0
  w_QtaAcq = 0
  w_PreCosMpa = 0
  w_PreCosMpp = 0
  w_PrzMpa = 0
  w_PreCosUlt = 0
  w_PrzMpp = 0
  w_ElenMag = space(10)
  w_PrzUlt = 0
  w_RecElab = 0
  w_CosMpa = 0
  w_PqtMpp = 0
  w_CosMpp = 0
  w_QtaScagl = 0
  w_CqtMpp = 0
  w_CpRowNum = 0
  w_CosUlt = 0
  w_IsDatMov = ctod("  /  /  ")
  w_CosSta = 0
  w_IsDatFin = ctod("  /  /  ")
  w_CosLco = 0
  w_IsQtaEsi = ctod("  /  /  ")
  w_CosLsc = 0
  w_IsValUni = 0
  w_CosFco = 0
  w_DataPrzUlt = ctod("  /  /  ")
  w_UltimoArt = space(40)
  w_DataCosUlt = ctod("  /  /  ")
  w_TIPOVALOr = space(1)
  w_ValQta = space(1)
  w_TipMagaz = space(1)
  w_ValTip = space(1)
  w_Segnalato = .f.
  w_DatReg = space(10)
  w_Trec = 0
  w_Tart = 0
  w_PrzMpaWrite = 0
  w_CosMpaWrite = 0
  w_NEWREC = space(10)
  w_QtaEsiUno = 0
  w_QtaVenUno = 0
  w_QtaAcqUno = 0
  w_QtaEsiWrite = 0
  w_QtaVenWrite = 0
  w_QtaAcqWrite = 0
  w_PrePreUlt = 0
  w_PreQtaEsr = 0
  w_PreQtaVer = 0
  w_PreQtaAcr = 0
  w_QtaEsr = 0
  w_QtaVer = 0
  w_QtaAcr = 0
  w_QtaVerWrite = 0
  w_QtaAcrWrite = 0
  w_Maga = space(5)
  w_Vendite = 0
  w_Acquisti = 0
  w_SLCODART = space(20)
  w_CARVAL = 0
  w_SCAVAL = 0
  w_PreCarVal = 0
  w_PreScaVal = 0
  w_ULTDATPRZ = ctod("  /  /  ")
  w_ULTVALPRZ = 0
  w_ULTMAGPRZ = space(5)
  w_ULTMATPRZ = space(5)
  w_ULTAGGPRZ = space(2)
  w_ULTDATCOS = ctod("  /  /  ")
  w_ULTVALCOS = 0
  w_ULTMAGCOS = space(5)
  w_ULTMAGCOS = space(5)
  w_ULTMATCOS = space(5)
  w_ULTAGGCOS = space(2)
  w_COSMPAMOV = 0
  w_ESIMPAMOV = 0
  w_COSMPPMOV = 0
  w_VarValoAcq = 0
  w_VarValoVen = 0
  w_Old_CosMpp = 0
  w_NUMERO = 0
  w_CI = 0
  w_RECSEL = 0
  w_LIVELLO = space(41)
  w_CODDIS = space(41)
  w_CJ = 0
  w_CODART = space(20)
  * --- WorkFile variables
  PAR_RIOR_idx=0
  PAR_RIMA_idx=0
  SALDIART_idx=0
  ART_TEMP_idx=0
  MAGAZZIN_idx=0
  KEY_ARTI_idx=0
  INVENTAR_idx=0
  ESERCIZI_idx=0
  TMPART_TEMP_idx=0
  INVEDETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Stampa Distinta Base Costificata (da GSDS_SDC,GSDS_BEA,GSDS_BCA)
    * --- Variabili passate dalla maschera
    DIMENSION QTC[99]
    DIMENSION QTN[99]
    DIMENSION QTR[99]
    this.Padre = this.oParentObject
    this.w_ClsName = upper(alltrim(this.Padre.class))
    this.w_PARELAB = .F.
    this.w_LatoServer = "N"
    this.w_STAMPA = .T.
    this.w_AGGCSSTD = .F.
    this.w_MAXLEVEL = IIF(this.w_TIPCOS="S", 99, 999)
    this.w_DecStampa = 10
    this.w_bNoUseRowNum = .T.
    * --- Cursore di Elaborazione
    ElabCursor = SYS(2015)
    * --- Stringa Utilizzata come filtro per prendere in considerazioni 
    *     solo gli articoli della elaborazione corrente presenti in ART_TEMP
    this.w_KEYRIF = SYS(2015)
    * --- Indica la Dimensione del lvlkey
    this.w_LenLvl = 4
    this.w_LIVFIN = " "
    this.w_PARZ = " "
    this.w_NOLIVFIN = "N"
    this.w_LOTTOMED = .F.
    * --- ----------------------------------------------------------------------------------------------
    if this.w_ClsName = "TGSDS1BDC"
      this.w_LatoServer = "O"
      * --- Controlli preliminari
      this.w_PARELAB = type("this.w_PARAM1")="C" and this.w_PARAM1="ELABORA"
      this.w_cCursor = iif(type("this.w_cCursor")="C", this.w_cCursor, SPACE(10))
      if Empty(this.w_cCursor)
        i_retcode = 'stop'
        return
      endif
      ElabCursor = this.w_cCursor
      * --- Determino se devo effettuare la stampa l'aggiornamento del costo oppure entrambi
      this.w_STAMPA = this.oParentObject.w_FLCOST<>"S"
      this.w_AGGCSSTD = this.oParentObject.w_FLCOST $ "E-S"
      * --- ----------------------------------------------------------------------------------------------
      this.w_PARAM = "A"
      this.w_VALCOM = "S"
      this.w_TIPCOS = this.Padre.w_TIPCOS
      this.w_FLDESART = this.Padre.w_FLDESART
      if Upper(this.Padre.oParentObject.Class) = "TGSDS_SDC"
        this.w_OREP = this.Padre.w_OREP
        this.w_OQRY = this.Padre.w_OQRY
      endif
      this.w_FLPROV = " "
      this.w_DATSTA = this.Padre.w_DATSTA
      this.w_CODINI = this.Padre.w_CODINI
      this.w_CODFIN = this.Padre.w_CODFIN
      this.w_FAMAINI = this.Padre.w_FAMAINI
      this.w_FAMAFIN = this.Padre.w_FAMAFIN
      this.w_GRUINI = this.Padre.w_GRUINI
      this.w_GRUFIN = this.Padre.w_GRUFIN
      this.w_CATINI = this.Padre.w_CATINI
      this.w_CATFIN = this.Padre.w_CATFIN
      this.w_Analisi = this.Padre.w_Analisi
      this.w_QUANTI = this.Padre.w_QUANTI
      this.w_PERRIC = this.Padre.w_PERRIC
      this.w_TipoValo = this.Padre.w_TipoValo
      this.w_Listino = this.Padre.w_Listino
      this.w_ESERC = this.Padre.w_ESERC
      this.w_NUMINV = this.Padre.w_NUMINV
      this.w_VALLIS = this.Padre.w_VALLIS
      this.w_VALESE = this.Padre.w_VALESE
      this.w_SCDRSR = this.Padre.w_SCDRSR
      this.w_TIPOLN = this.Padre.w_TIPOLN
      this.w_CAOVAL = this.Padre.w_CAOVAL
      this.w_CAOLIS = this.Padre.w_CAOLIS
      this.w_CODMAG = this.Padre.w_CODMAG
      this.w_INNUMPRE = this.Padre.w_INNUMPRE
      this.w_CAOESE = this.Padre.w_CAOESE
      this.w_INDATINV = this.Padre.w_INDATINV
      this.w_PRDATINV = this.Padre.w_PRDATINV
      this.w_PDCOSPAR = this.Padre.w_PDCOSPAR
      this.w_MAGRAG = this.Padre.w_MAGRAG
      this.w_LOTTOMED = this.Padre.w_LOTTOMED
      if this.w_PARELAB
        this.w_AGCOSSTD = this.Padre.w_AGCOSSTD
      endif
    else
      do case
        case this.w_ClsName = "TGSAR_BEA" 
          this.w_PARAM = "C"
          this.w_TIPCOS = this.Padre.w_TIPCOS
          this.w_FLDESART = "N"
          this.w_OREP = " "
          this.w_OQRY = " "
          this.w_FLPROV = " "
          this.w_DATSTA = i_DATSYS
          this.w_INDATINV = this.w_DATSTA
          this.w_CODINI = this.Padre.w_ARTPAD
          this.w_CODFIN = this.Padre.w_ARTPAD
          this.w_VALCOM = IIF(this.Padre.w_VALCOM="N","N","S")
          this.w_Analisi = "N"
          this.w_PERRIC = "S"
          this.w_QUANTI = this.Padre.w_QUANTI
          this.w_TipoValo = this.Padre.w_DITIPVAL
          this.w_Listino = this.Padre.w_MMTCOLIS
          this.w_ESERC = this.Padre.w_DICODESE
          this.w_NUMINV = this.Padre.w_DINUMINV
          this.w_VALLIS = this.Padre.w_MVCODVAL 
          this.w_VALESE = this.Padre.w_MVCODVAL
          this.w_SCDRSR = "N"
          this.w_TIPOLN = this.Padre.w_TIPOLN
          this.w_CAOVAL = this.Padre.w_MVCAOVAL
          this.w_CAOLIS = this.Padre.w_MVCAOVAL
          this.w_CODMAG = this.Padre.w_MAGCOD
          this.w_INNUMPRE = this.w_NUMINV
          this.w_CAOESE = this.Padre.w_MVCAOVAL
        case this.w_ClsName = "TGSDS_BCA"
          this.w_PARAM = "S"
          this.Objmask = this.oParentObject.Oparentobject
          this.w_VALCOM = this.Objmask.w_DIFLVACO
          this.w_TIPCOS = this.Objmask.w_TIPCOS
          this.w_FLDESART = "N"
          this.w_OREP = " "
          this.w_OQRY = " "
          this.w_FLPROV = " "
          this.w_DATSTA = i_DATSYS
          this.w_INDATINV = this.w_DATSTA
          this.w_CODINI = this.Padre.w_PPARTCOM
          this.w_CODFIN = this.Padre.w_PPARTCOM
          this.w_Analisi = "N"
          this.w_PERRIC = "S"
          this.w_QUANTI = this.Padre.w_QUANTI
          this.w_TipoValo = this.Objmask.w_DITIPVAL
          this.w_Listino = this.Padre.w_MMTCOLIS
          this.w_ESERC = this.Objmask.w_DICODESE
          this.w_NUMINV = this.Objmask.w_DINUMINV
          this.w_VALLIS = this.Padre.w_MMCODVAL 
          this.w_VALESE = this.Padre.w_MMCODVAL
          this.w_SCDRSR = "N"
          this.w_TIPOLN = this.Objmask.w_TIPOLN
          this.w_CAOVAL = this.Padre.w_MMCAOVAL
          this.w_CAOLIS = this.Padre.w_MMCAOVAL
          if VarType( this.Objmask.w_DIMAGSCA )="C"
            this.w_CODMAG = this.Objmask.w_DIMAGSCA
          else
            this.w_CODMAG = Space(5)
          endif
          this.w_INNUMPRE = this.w_NUMINV
          this.w_CAOESE = this.Padre.w_MMCAOVAL
        otherwise
          i_retcode = 'stop'
          return
      endcase
      this.w_MAXLEVEL = IIF(this.w_TIPCOS="S", 99, 999)
      this.w_DecStampa = 10
      * --- Cursore di Elaborazione
      ElabCursor = SYS(2015)
      * --- Stringa Utilizzata come filtro per prendere in considerazioni 
      *     solo gli articoli della elaborazione corrente presenti in ART_TEMP
      this.w_KEYRIF = SYS(2015)
      * --- Controlli preliminari
      if (this.w_TipoValo $ "S-M-U-P" or this.w_Analisi="S") and empty(this.w_NUMINV)
        ah_ErrorMsg("Inserire il numero inventario","STOP","")
        i_retcode = 'stop'
        return
      endif
      if this.w_TipoValo = "L" and empty(this.w_Listino)
        ah_ErrorMsg("Inserire il listino","STOP","")
        i_retcode = 'stop'
        return
      endif
      if this.w_TipoValo="A" and Empty(this.w_CODMAG)
        ah_ErrorMsg("Codice magazzino non definito",,"")
        i_retcode = 'stop'
        return
      endif
      L_FLCOSINV = this.oParentObject.w_FLCOSINV
      L_PDCOSPAR = this.w_PDCOSPAR
      L_Analisi = this.w_Analisi
      L_DATSTA=this.w_DATSTA
      L_CODINI=this.w_CODINI
      L_CODFIN=this.w_CODFIN
      L_FLPROV=this.w_FLPROV
      L_FLCICL=this.w_SCDRSR
      L_QUANTI=this.w_QUANTI
      L_FLRICA=this.w_PERRIC
      L_CODLIS=this.w_Listino
      L_NUMINV=this.w_NUMINV
      l_lotmed = this.w_LOTTOMED
      L_CODESE=this.w_ESERC
      L_TIPVAL=this.w_TipoValo
      L_CODMAG=this.w_CODMAG
      L_TIPCOS=this.w_TIPCOS
      this.w_QRY = ALLTRIM(this.w_OQRY)
      this.w_REP = ALLTRIM(this.w_OREP)
      * --- Parametri per Batch di Esplosione (Indentata / Sommarizzata)
      this.w_DBCODINI = this.w_CODINI
      this.w_DBCODFIN = IIF(EMPTY(this.w_CODFIN), REPL("z", 20), this.w_CODFIN)
      this.w_VERIFICA = "S C"
      this.w_FILSTAT = this.w_FLPROV
      this.w_DATFIL = this.w_DATSTA
      if used("TES_PLOS")
        select TES_PLOS
        use
      endif
      * --- Lancia la Funzione di Esecuzione dell'esplosione Distinta Base
      gsar_bde(this,this.w_PARAM)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if not used("TES_PLOS")
        i_retcode = 'stop'
        return
      else
        if reccount("TES_PLOS")=0
          ah_ErrorMsg("Attenzione: selezione vuota!",16)
          i_retcode = 'stop'
          return
        endif
      endif
      if used("TES_PLOS")
        * --- Crea Cursore di Appoggio
        * --- Cursore di Elaborazione/Stampa
        CREATE CURSOR (ElabCursor) ;
        (CODART C(20), NUMRIG N(10), DISPAD C(20), ARTPAD C(20), ORDINE C(1), CODRES C(20), NUMERO N(5,0), CODCOM C(20), NUMLEV C(4), DESCOM C(40), UNIMIS C(3), ;
        QTANET N(15,6), QTANET1 N(15,6), PERSCA N(6,2), PERSFR N(6,2), QTALOR N(15,6), QTALO1 N(15,6), QTAFIN N(15,6), QTAFIN1 N(15,6), ;
        RECSCA N(15,6), RECSFR N(15,6), PERRIC N(15,6), ;
        FLVARI C(1), DESPAD C(40), FLESPL C(1), CODDIS C(20), ARTCOM C(20), PROPRE C(1), PERCOS N(6,2), CODRIS C(15), FLCOMP C(1),;
        FLSETU C(1), LVLKEY C(200), COSTO N(14,8), COSREC N(14,8), COSUNISE N(14,8), COSUNIST N(14,8), COSUNIME N(14,8), COSUNIUL N(14,8), ;
        COSCIC N(14,8), COSSETU N(14,8), LIVELLO N(4), CSTANDR N(14,8), CMEDIO N(14,8), CULTIM N(14,8), EVASOCL C(1), COSTOCL N(14,8), RAGGRUPP N(2))
        SELECT TES_PLOS
        GO TOP
        this.w_CODART = CODART
        this.w_NUMERO = 0
        * --- Inizializzo le variabili per il calcolo del LVLKY
        this.w_LVLKEY = "000"
        this.w_OLDKEY = "000"
        this.w_APPOLW = "000"
        this.w_OLDLEVEL = 0
        * --- Cicla sulle Distinte Base Esplose
        SCAN FOR NOT EMPTY(DISPAD)
        this.w_CODART = CODART
        this.w_DISPAD = DISPAD
        this.w_CODRIS = CODRIS
        * --- ORDINE: Se '0' = Riga Componente ; '1' = Riga Ciclo Semplificato
        this.w_ORDINE = "0"
        this.w_NUMLEV = NUMLEV
        this.w_DESCOM = DESCOM
        * --- Codice Articolo Associato al Componente
        this.w_ARTCOM = ARTCOM
        this.w_PROPRE = PROPRE
        this.w_NUMRIG = NUMRIG
        if VAL(this.w_NUMLEV)>0
          * --- Rileggo la descrizione dei codici di ricerca nel caso abbia attivato il flag sulla maschera
          if this.w_FLDESART="S"
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_ARTCOM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESART;
                from (i_cTable) where;
                    CACODICE = this.w_ARTCOM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCOM = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.w_UNIMIS = UNMIS0
        this.w_QTANET = QTANET
        this.w_QTANE1 = IIF(QTANE1=0, QTANET, QTANE1)
        this.w_QTALOR = QTAMOV
        this.w_QTALO1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
        this.w_QTAFIN = QTAREC
        this.w_QTAFIN1 = IIF(QTARE1=0, QTAREC, QTARE1)
        this.w_PERSCA = PERSCA
        this.w_RECSCA = RECSCA
        this.w_PERSFR = PERSFR
        this.w_RECSFR = RECSFR
        this.w_COPERRIC = IIF(this.w_PERRIC="S", PERRIC, 0)
        this.w_FLVARI = FLVARI
        this.w_FLESPL = FLESPL
        this.w_ARTPAD = NVL(ARTPAD, SPACE(20))
        * --- % Recupero Scarto e Sfrido (Attenzione al calcolo del Costo Totale: i valori sono normalmente Negativi)
        if this.w_RECSFR<>0 AND this.w_PERSFR<>0 or this.w_RECSCA<>0 AND this.w_PERSCA<>0
          this.w_PERREC = cp_ROUND((this.w_RECSCA+this.w_RECSFR)/100, 6)
          this.w_PERREC = (this.w_RECSCA+this.w_RECSFR)/100
        else
          this.w_PERREC = 0
        endif
        * --- Testa se l'Articolo Componente e' un Prodotto Finito (S) o un Semilavorato
        this.w_FLCOMP = IIF(VAL(this.w_NUMLEV)=0 OR this.w_FLESPL="S", IIF(PROPRE<>"E", " ", "S"), "S")
        if VAL(this.w_NUMLEV)=0
          * --- Inizia una nuova Distinta Base, inizia da qui a calcolare le qta dei Componenti 
          this.w_CODCOM = ALLTRIM(DISPAD)
          this.w_ARTCOM = ALLTRIM(CODART)
          this.w_CODDIS = ALLTRIM(DISPAD)
          this.w_NUMERO = 0
          this.w_QTANET = this.w_QUANTI
          this.w_QTANE1 = this.w_QUANTI
          this.w_QTALOR = this.w_QUANTI
          this.w_QTALO1 = this.w_QUANTI
          this.w_QTAFIN = this.w_QUANTI
          this.w_QTAFIN1 = this.w_QUANTI
          FOR L_i = 1 TO 99
          QTC[L_i] = 0
          QTN[L_i] = 0
          QTR[L_i] = 0
          ENDFOR
        else
          * --- Componente: Prodotto Finito o Semilavorato
          this.w_NUMERO = this.w_NUMERO + 1
          this.w_CODCOM = ALLTRIM(STR(VAL(this.w_NUMLEV)))+" " + ALLTRIM(CODCOM)
          this.w_CODCOM = REPL(". ", IIF(VAL(this.w_NUMLEV)<10, VAL(this.w_NUMLEV)-1, 9))+this.w_CODCOM
          this.w_CODCOM = ALLTRIM(CODCOM)
          this.w_CODDIS = NVL(CODDIS, SPACE(20))
          * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
          QTN[VAL(this.w_NUMLEV)] = this.w_QTANE1
          this.w_QTANET = this.w_QUANTI
          this.w_QTANE1 = this.w_QUANTI
          QTC[VAL(this.w_NUMLEV)] = this.w_QTALO1
          this.w_QTALOR = this.w_QUANTI
          this.w_QTALO1 = this.w_QUANTI
          QTR[VAL(this.w_NUMLEV)] = this.w_QTAFIN1
          this.w_QTAFIN = this.w_QUANTI
          this.w_QTAFIN1 = this.w_QUANTI
          FOR L_i = 1 TO VAL(this.w_NUMLEV)
          * --- Solo la riga del componente viene moltiplicato per l'effettivo valore, gli altri calcoli vanno riportati alla 1^UM
          this.w_QTANET = this.w_QTANET * IIF(L_i=VAL(this.w_NUMLEV), QTANET, QTN[L_i])
          this.w_QTANE1 = this.w_QTANE1 * QTN[L_i]
          this.w_QTALOR = this.w_QTALOR * IIF(L_i=VAL(this.w_NUMLEV), QTAMOV, QTC[L_i])
          this.w_QTALO1 = this.w_QTALO1 * QTC[L_i]
          this.w_QTAFIN = this.w_QTAFIN * IIF(L_i=VAL(this.w_NUMLEV), QTAREC, QTR[L_i])
          this.w_QTAFIN1 = this.w_QTAFIN1 * QTR[L_i]
          ENDFOR
        endif
        * --- Mentre Creo il cursore di elaborazione in base al livello che
        *     st� elaborando costruisco il lvlkey
        this.w_NUMLEW = NVL(VAL(NUMLEV), 0)
        if this.w_NUMLEW = 0
          this.w_LVLKEY = cp_bintoc(this.i_cNum)
          this.w_OLDLEVEL = this.w_NUMLEW
          this.i_cNum = this.i_cNum + 1
        else
          this.w_APPOLW = VAL(SUBSTR(this.w_LVLKEY, this.w_NUMLEW * 4 + 1, 3))+1
          if this.w_NUMLEW > this.w_OLDLEVEL
            this.w_LVLKEY = rTrim(this.w_LVLKEY) + "." + str(this.w_APPOLW,3,0)
          else
            this.w_LVLKEY = left(this.w_LVLKEY, this.w_NUMLEW * 4) + str(this.w_APPOLW,3,0)
          endif
        endif
        this.w_OLDLEVEL = this.w_NUMLEW
        * --- Scrivo il Temporaneo
        INSERT INTO (ElabCursor) ;
        (CODART, NUMRIG, DISPAD, ARTPAD, ORDINE, NUMERO, CODCOM, NUMLEV, DESCOM, UNIMIS, QTANET, QTANET1, PERSCA, PERSFR, QTALOR, QTALO1, QTAFIN, QTAFIN1, ;
        RECSCA, RECSFR, PERRIC, FLVARI, FLESPL, CODDIS, ARTCOM, PROPRE, PERCOS, CODRIS, FLCOMP, FLSETU, ;
        LVLKEY, COSTO, COSREC, COSUNISE, COSUNIST, COSUNIME, COSUNIUL, COSCIC, COSSETU, ;
        LIVELLO, CSTANDR, CMEDIO, CULTIM, EVASOCL, COSTOCL, RAGGRUPP) ;
        VALUES (this.w_CODART, this.w_NUMRIG, this.w_DISPAD, this.w_ARTPAD, this.w_ORDINE, this.w_NUMERO, this.w_CODCOM, this.w_NUMLEV, this.w_DESCOM, this.w_UNIMIS, ;
        this.w_QTANET,this.w_QTANE1, this.w_PERSCA, this.w_PERSFR, this.w_QTALOR, this.w_QTALO1, this.w_QTAFIN, this.w_QTAFIN1, this.w_RECSCA, this.w_RECSFR, this.w_COPERRIC,;
        this.w_FLVARI, this.w_FLESPL, this.w_CODDIS, this.w_ARTCOM, this.w_PROPRE, 0, this.w_CODRIS, this.w_FLCOMP, " ", this.w_LVLKEY, 0,0,0,0,0,0,0,0, this.w_NUMLEW, 0, 0, 0, "N", 0, 1)
        SELECT TES_PLOS
        ENDSCAN
        * --- Chiude il Cursore generato dalla Procedura di Esplosione
        select TES_PLOS
        use
        this.w_PROPRE = Space(1)
      endif
    endif
    * --- ----------------------------------------------------------------------------------------------
    if USED (ElabCursor) 
      Select * from (ElabCursor) into cursor appogg nofilter
      Use in select(ElabCursor)
       Select * from appogg into cursor (ElabCursor) nofilter 
 =wrcursor(ElabCursor) 
 INDEX ON lvlkey TAG lvlkey collate "MACHINE" 
 =FLOCK() 
 UPDATE (ElabCursor) SET livello = (1+len(rtrim(lvlkey)))/this.w_LenLvl-1
      if this.w_Analisi="S"
        Use in select("appogg")
        * --- Aggiunge i record vuoti necessari per l'analisi dei costi
        Select lvlkey FROM (ElabCursor) WHERE livello=0 INTO CURSOR appogg nofilter
        SCAN
        this.w_CODTMP = lvlkey
        Select (ElabCursor)
        SEEK this.w_CODTMP ORDER lvlkey
        SCATTER MEMVAR
        this.w_CODTMP = left(lvlkey,this.w_lenlvl-1)+chr(255)
        this.w_CODLEN = len(lvlkey)
        raggrupp=7
        livello=-1
        ordine="3"
        this.w_i = 8
        do while this.w_i>0
          lvlkey = padr(this.w_CODTMP+str(this.w_i,2,0),this.w_CODLEN)
          INSERT INTO (ElabCursor) FROM MEMVAR
          this.w_i = this.w_i -1
        enddo
        Select appogg
        ENDSCAN
      endif
      SELECT max(livello) AS massimo FROM (ElabCursor) INTO CURSOR maxlevel
      this.w_maxlvl = maxlevel.massimo
      USE IN maxlevel
      select (ElabCursor)
      * --- Elabora i Cicli semplificati e i Contratti di C/Lavoro
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ClsName <> "TGSDS1BDC"
        * --- Se la routine chiamante � GSDS1BDC i costi articoli sono calcolati dall GSDSCBDC
        * --- Elabora i Costi dei Componenti
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Se esiste tmp ordina e Stampa
      SELECT (ElabCursor) 
      if RECCOUNT()>0
        if this.w_PARELAB OR this.w_AGGCSSTD
          * --- Se prevede Aggiornamento del Costo Standard Aggiorna i Dati Articolo/Magazzino
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_STAMPA
          * --- Cancella i figli dei SL forniti da terzista
          delete from (ElabCursor) where evasocl="X" 
          lenlvlkey=len(&ElabCursor..lvlkey)
          * --- Se effettuo la stampa costificata arrivo dal GSDS1BDC potrei avere l'elaborazione spezzata in pi� cursori
          this.w_TMPCUR = IIF(this.w_ClsName = "TGSDS1BDC" , SYS(2015) , "__tmp__")
          L_FLCOSINV = this.oParentObject.w_FLCOSINV
          L_PDCOSPAR = this.w_PDCOSPAR
          L_Analisi = this.w_Analisi
          L_DATSTA=this.w_DATSTA
          L_CODINI=this.w_CODINI
          L_CODFIN=this.w_CODFIN
          L_FLPROV=this.w_FLPROV
          L_FLCICL=this.w_SCDRSR
          L_QUANTI=this.w_QUANTI
          L_FLRICA=this.w_PERRIC
          L_CODLIS=this.w_Listino
          L_NUMINV=this.w_NUMINV
          l_lotmed = this.w_LOTTOMED
          L_CODESE=this.w_ESERC
          L_TIPVAL=this.w_TipoValo
          L_CODMAG=this.w_CODMAG
          L_TIPCOS=this.w_TIPCOS
          this.w_MultiReportVarEnv=createobject("ReportEnvironment")
          this.w_MultiReportVarEnv.AddVariable("L_FLCOSINV", L_FLCOSINV)     
          this.w_MultiReportVarEnv.AddVariable("L_PDCOSPAR", L_PDCOSPAR)     
          this.w_MultiReportVarEnv.AddVariable("l_Analisi" , l_Analisi)     
          this.w_MultiReportVarEnv.AddVariable("L_DATSTA" , L_DATSTA)     
          this.w_MultiReportVarEnv.AddVariable("l_CODINI" , l_CODINI)     
          this.w_MultiReportVarEnv.AddVariable("l_CODFIN" , l_CODFIN)     
          this.w_MultiReportVarEnv.AddVariable("L_FLPROV" , L_FLPROV)     
          this.w_MultiReportVarEnv.AddVariable("L_FLCICL" , L_FLCICL)     
          this.w_MultiReportVarEnv.AddVariable("L_QUANTI", L_QUANTI)     
          this.w_MultiReportVarEnv.AddVariable("L_FLRICA", L_FLRICA)     
          this.w_MultiReportVarEnv.AddVariable("L_CODLIS", L_CODLIS)     
          this.w_MultiReportVarEnv.AddVariable("L_NUMINV", L_NUMINV)     
          this.w_MultiReportVarEnv.AddVariable("l_lotmed ", l_lotmed )     
          this.w_MultiReportVarEnv.AddVariable("L_CODESE", L_CODESE)     
          this.w_MultiReportVarEnv.AddVariable("L_TIPVAL", L_TIPVAL)     
          this.w_MultiReportVarEnv.AddVariable("L_CODMAG", L_CODMAG)     
          this.w_MultiReportVarEnv.AddVariable("L_TIPCOS", L_TIPCOS)     
          this.w_MultiReportVarEnv.AddVariable("lenlvlkey" , lenlvlkey)     
          if this.w_SCDRSR="N"
            if this.w_ClsName = "TGSDS1BDC" And !this.w_PARELAB
              SELECT 1 AS PRLOTMED, CODART, DISPAD, ORDINE, NUMERO, left(CODCOM+SPACE(20),20) AS CODCOM, NUMLEV,; 
 left(DESCOM+space(40),40) as DESCOM , Space(40) as DESFAS, UNIMIS, UNIMIS AS UMDIS, QTANET, QTANET1, PERSCA, PERSFR,; 
 QTALOR, QTALO1, QTAFIN, QTAFIN1,RECSCA, RECSFR, PERRIC, FLVARI, FLESPL, CODDIS, ARTCOM,; 
 PERCOS, LEFT(CODRES+SPACE(20),20) AS CODRES, FLCOMP, FLSETU, LVLKEY, PROPRE, COSTO,; 
 COSREC, LIVELLO, COSUNISE, COSCIC, COSSETU, CSTANDR, CMEDIO, CULTIM, EVASOCL, COSTOCL, ; 
 SPACE(15) AS LFCODFOR, RAGGRUPP, ; 
 LEFT(DESPAD+SPACE(40),40) AS DESPAD FROM (ElabCursor); 
 ORDER BY lvlkey into cursor (this.w_TMPCUR)
            else
              select CODART, ARTCOM, COSTO, IIF(Empty(Nvl(CODDIS," ")),COSUNISE,COSTO/QTAFIN) as COSUNI, COSTO/QTAFIN1 as COSUNI1, UNIMIS, UNIMIS AS UMDIS, QTANET, QTANET1, QTALOR, QTALO1, NUMLEV,NUMLEV,NUMERO*0 AS RIGA; 
 from (ElabCursor) where Raggrupp=1 into cursor TempValo
            endif
          else
            * --- Aggiunge nel cursore di stampa i Cicli Semplificati
            Select * from (ElabCursor) WHERE not Empty(Nvl(coddis,"")) and (!Empty(Nvl(codris,"")) or propre ="L") and flcomp <> "S" and evasocl="N" and raggrupp=1 ; 
 INTO CURSOR _TmpDis_ readwrite
            index on numrig tag numrig
            index on lvlkey tag lvlkey collate "MACHINE"
            Select distinct _Tmpdis_.codart as CODART, LEFT(codris+SPACE(20),20) as DISPAD, artpad as ARTPAD, "1" as ORDINE,; 
 LEFT(RIDESCRI+SPACE(40),40) as DESPAD, cacodric AS CODRES, srcodris as CODCOM, srdessch as DESCOM,; 
 SRDESOPE as desfas, space(4) as NUMLEV, scheris.cproword as NUMERO, unimiric AS UNIMIS, _Tmpdis_.unimis as umdis, ; 
 srqtaris * IIF(SRUNATAN<>"S" , cp_ROUND(_Tmpdis_.qtanet1/_Tmpdis_.qtanet, this.w_DecStampa) , 1) AS QTANET,; 
 persca as persca, persfr as PERSFR, qtanet1 as QTANET1, recsca as RECSCA, recsfr as RECSFR, perric as PERRIC,; 
 flvari as FLVARI, flespl as FLESPL, DBCODICE AS DBCODICE, cacodric as ARTCOM, percos as PERCOS, flcomp as FLCOMP, NVL(srunatan, "N") as FLSETU, ; 
 left(left(lvlkey, this.w_LenLvl-1)+chr(254)+chr(livello)+chr( _Tmpdis_.raggrupp)+cacodric+str(numero)+space(lenlvlkey),lenlvlkey) AS LVLKEY, ; 
 lfcodfor as LFCODFOR, scheris.arpropre as PROPRE, QTALOR as QTALOR, ; 
 qtalo1 as QTALO1, qtafin as qtafin, qtafin1 as qtafin1, evasocl as EVASOCL, ; 
 dpcossta AS COSTO, 999999999999999.99999*0 AS cosrec, livello as LIVELLO, ; 
 999999999999999.99999*0 AS cstandr, 999999999999999.99999*0 AS cmedio, ; 
 999999999999999.99999*0 AS cultim, 999999999999999.99999*0 as COSTOCL, scheris.raggrupp AS RAGGRUPP, PRLOTMED ; 
 from Scheris INNER JOIN _Tmpdis_ ON (_Tmpdis_.numrig = scheris.numrig and _Tmpdis_.lvlkey=scheris.lvlkey1); 
 WHERE not isnull(srcodris) INTO CURSOR Appogg NOFILTER order by lvlkey, numero
            use in select("_TmpDis_")
            SELECT CODART, DISPAD, ORDINE, NUMERO, left(CODCOM+SPACE(20),20) AS CODCOM, NUMLEV,; 
 left(DESCOM+space(40),40) as DESCOM, Space(40) as DESFAS , UNIMIS, UNIMIS AS UMDIS, QTANET, QTANET1, PERSCA, PERSFR,; 
 QTALOR, QTALO1, QTAFIN, QTAFIN1,RECSCA, RECSFR, PERRIC, FLVARI, FLESPL, CODDIS, ARTCOM,; 
 PERCOS, LEFT(CODRES+SPACE(20),20) AS CODRES, FLCOMP, FLSETU, LVLKEY, PROPRE, ; 
 COSTO AS COSTO, COSREC, LIVELLO, COSUNISE, ; 
 COSCIC, COSSETU, CSTANDR, CMEDIO, CULTIM, EVASOCL, COSTOCL, SPACE(15) AS LFCODFOR, RAGGRUPP, QTALOR*0 AS PRLOTMED , ; 
 LEFT(DESPAD+SPACE(40),40) AS DESPAD FROM (ElabCursor); 
 UNION ALL; 
 SELECT CODART, DISPAD, ORDINE, NUMERO, LEFT(CODCOM+SPACE(20),20) AS CODCOM, NUMLEV, DESCOM, DESFAS, UNIMIS, UMDIS, QTANET,; 
 QTANET1, PERSCA, PERSFR, QTALOR, QTALO1, QTAFIN, QTAFIN1, RECSCA, RECSFR, PERRIC, ; 
 FLVARI, FLESPL, DBCODICE AS CODDIS, left(CODRES+space(20),20) as ARTCOM, PERCOS,; 
 LEFT(ARTCOM+space(20),20) AS CODRES, FLCOMP, FLSETU, LVLKEY, PROPRE, COSTO, ; 
 COSREC, LIVELLO, 0 AS COSUNISE,; 
 0 AS COSCIC, 0 AS COSSETU, CSTANDR, CMEDIO, CULTIM, EVASOCL, COSTOCL, LFCODFOR, RAGGRUPP, PRLOTMED, DESPAD FROM ; 
 Appogg into cursor (this.w_TMPCUR) order by 30, 4
            * --- 34 = lvlkey <--> 25 coriffas
          endif
          if this.w_ClsName = "TGSDS1BDC" And !this.w_PARELAB
            this.oParentObject.w_MultiRep.AddNewReport(this.w_OREP , this.oParentObject.oParentObject.Caption , this.w_TMPCUR, .null. , this.w_MultiReportVarEnv , .t., this.oParentObject.w_FIRSTCUR , .f.,"","","",1, .t.)     
          endif
        endif
      endif
    endif
    WAIT CLEAR
    * --- Rimuovo i cursori
    Use in select("TES_PLOS")
    Use in select("__Tmp__")
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if !this.w_ClsName = "TGSDS1BDC" and !this.w_ClsName = "TGSAR_BEA" and !this.w_ClsName = "TGSDS_BCA"
      ah_ErrorMsg("Elaborazione terminata", 64)
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione dati
    * --- La Variabile w_DecStampa mi serve per effettuare gli arrotondamenti
    *     altrimenti il Fox dopo aver effettuato alcuni conteggi considera il
    *     valore come Float e non effettua giuste le operazioni
    * --- Netto
    * --- Scarto
    * --- Recupero
    do while this.w_maxlvl>=0
      wait wind "" timeout 0.001
      ah_Msg("Elaborazione livello %1 in corso...",.T.,.F.,.F., alltrim(str(this.w_maxlvl)) )
      SELECT (ElabCursor)
      SCAN FOR livello=this.w_maxlvl
      if qtanet1=0 or qtanet=0
        ah_ErrorMsg("Nella distinta base %1 riga %2 � stato impostato un coefficiente d'impiego uguale a zero%0Si raccomanda di correggere l'errore%0%0Calcolo della costificazione sospeso","STOP","", alltrim(dispad), alltrim(artcom) )
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      this.w_COECONV = cp_ROUND(qtanet1/qtanet, this.w_DecStampa)
      this.w_CMRSCSTD = 0
      this.w_CMRSCSEL = 0
      this.w_CMRSCMED = 0
      this.w_CMRSCULT = 0
      this.w_CSRSCSEL = 0
      this.w_CSELSETU = 0
      this.w_CSSCSETU = 0
      this.w_CSRSSETU = 0
      this.w_CMNETSEL = 0
      this.w_CMSCASEL = 0
      this.w_CMRICSEL = 0
      this.w_CMNETSTD = 0
      this.w_CMSCASTD = 0
      this.w_CMRICSTD = 0
      this.w_CMNETMED = 0
      this.w_CMSCAMED = 0
      this.w_CMRICMED = 0
      this.w_CMNETULT = 0
      this.w_CMSCAULT = 0
      this.w_CMRICULT = 0
      this.w_CSNETSEL = 0
      this.w_CSSCASEL = 0
      this.w_CSRICSEL = 0
      this.w_costotcic = 0
      this.w_coscic = 0
      this.w_cosuni = 0
      this.w_cosunist = 0
      this.w_cosunime = 0
      this.w_cosuniul = 0
      this.w_cossetup = 0
      this.w_CosRec = 0
      this.w_csapposetu = 0
      this.w_FORNITO = (evasocl="N")
      this.w_costotcic = coscic
      this.w_cosuni = cosunise
      this.w_cosunist = cosunist
      this.w_cosunime = cosunime
      this.w_cosuniul = cosuniul
      if flcomp="S" OR NOT this.w_FORNITO OR this.w_VALCOM<>"S"
        * --- Per gli articoli esterni (es. materia prima) cerca il prezzo
        this.w_codiartico = ARTCOM
        this.w_QTALOR = cp_ROUND(qtalor, this.w_DecStampa)
        this.w_QTALO1 = cp_ROUND(qtalo1, this.w_DecStampa)
        this.w_QTANET = cp_ROUND(qtanet, this.w_DecStampa)
        this.w_QTAFIN = cp_ROUND(qtafin, this.w_DecStampa)
        this.w_CAUNMISU = NVL(UNIMIS, SPACE(3))
        if this.w_FORNITO
          SELECT przart
          do case
            case this.w_TipoValo="L"
              locate for NVL(przart.cacodric,"  ")=this.w_codiartico and nvl(przart.unimis, space(3))=this.w_CAUNMISU and cp_round(nvl(przart.caqtamov,0), this.w_DecStampa)=cp_round(this.w_qtalor, this.w_DecStampa)
            otherwise
              locate for przart.cacodric=this.w_codiartico
          endcase
          this.w_TROVATO = FOUND()
          if this.w_TROVATO
            do case
              case this.w_TipoValo="S"
                this.w_cosselez = cp_ROUND(przart.dicossta*this.w_COECONV, this.w_DecStampa)
              case this.w_TipoValo="X"
                this.w_cosselez = cp_ROUND(przart.dicossta_ar*this.w_COECONV, this.w_DecStampa)
              case this.w_TipoValo="U"
                this.w_cosselez = cp_ROUND(przart.dicosult*this.w_COECONV, this.w_DecStampa)
              case this.w_TipoValo $ "MP"
                this.w_cosselez = cp_ROUND(przart.dicosmpa*this.w_COECONV, this.w_DecStampa)
              case this.w_TipoValo="L"
                * --- cp_ROUND(przart.laprezzo*w_COECONV, w_DecStampa)
                this.w_cosselez = cp_ROUND(przart.laprezzo , this.w_DecStampa)
                if this.w_cosselez<>0
                  if this.w_VALLIS<>g_PERVAL
                    * --- Se Listino di altra Valuta , Allinea alla Valuta di Conto
                    this.w_cosselez = (this.w_cosselez * g_CAOVAL) / this.w_CAOLIS
                  endif
                  if this.w_TIPOLN="L" AND NVL(IVPERIVA,0)<>0
                    * --- Se Listino al Lordo , Nettifica
                    this.w_cosappo = this.w_cosselez - (this.w_cosselez / (1 + (IVPERIVA / 100)))
                    this.w_cosselez = this.w_cosselez - this.w_cosappo
                    this.w_cosappo = 0
                  endif
                endif
              case this.w_TipoValo="A"
                this.w_cosselez = cp_ROUND(przart.slvaluca*this.w_COECONV, this.w_DecStampa)
              otherwise
                this.w_cosselez = 0
            endcase
            if this.w_PARELAB
              this.w_TipoValoOld = this.w_TipoValo
              if this.oParentObject.w_STANDARD="X"
                this.w_cossta = cp_ROUND(przart.dicossta_ar*this.w_COECONV, this.w_DecStampa)
              else
                this.w_cossta = cp_ROUND(przart.dicossta*this.w_COECONV, this.w_DecStampa)
              endif
              if this.oParentObject.w_ULTIMO="A"
                this.w_cosult = cp_ROUND(przart.slvaluca*this.w_COECONV, this.w_DecStampa)
              else
                this.w_cosult = cp_ROUND(przart.dicosult*this.w_COECONV, this.w_DecStampa)
              endif
              this.w_cosmed = cp_ROUND(przart.dicosmpa*this.w_COECONV, this.w_DecStampa)
            else
              if this.w_Analisi="S"
                this.w_cossta = cp_ROUND(przart.dicossta*this.w_COECONV, this.w_DecStampa)
                this.w_cosmed = cp_ROUND(przart.dicosmpa*this.w_COECONV, this.w_DecStampa)
                this.w_cosult = cp_ROUND(przart.dicosult*this.w_COECONV, this.w_DecStampa)
              endif
            endif
            SELECT (ElabCursor)
          else
            this.w_TROVATO = .F.
            if this.w_TipoValo="L" and this.w_Analisi="S"
              locate for cacodric=this.w_codiartico
              this.w_TROVATO = FOUND()
              if this.w_TROVATO
                this.w_cosselez = 0
                this.w_cossta = cp_ROUND(przart.dicossta*this.w_COECONV, this.w_DecStampa)
                this.w_cosmed = cp_ROUND(przart.dicosmpa*this.w_COECONV, this.w_DecStampa)
                this.w_cosult = cp_ROUND(przart.dicosult*this.w_COECONV, this.w_DecStampa)
              endif
            else
              * --- ERRORE: prezzo non definito!
              this.w_cosselez = 0
              this.w_cossta = 0
              this.w_cosmed = 0
              this.w_cosult = 0
            endif
          endif
        else
          * --- Per gli articoli fornito dal terzista imposta il prezzo del contratto
          this.w_TROVATO = .T.
          this.w_cosselez = cp_ROUND(costocl*this.w_COECONV, this.w_DecStampa)
          if this.w_Analisi="S"
            this.w_cossta = this.w_cosselez
            this.w_cosmed = this.w_cosselez
            this.w_cosult = this.w_cosselez
          endif
        endif
        SELECT (ElabCursor)
        if isnull(this.w_cosselez)
          * --- ERRORE: prezzo non definito!
          this.w_cosselez = 0
          this.w_cosuni = 0
        else
          this.w_cosuni = this.w_cosselez
          if this.w_Analisi="S"
            * --- Analisi Costo Selezionato
            this.w_CMNETSEL = this.w_cosselez * qtanet
            this.w_CMSCASEL = this.w_cosselez * (this.w_qtalor - this.w_qtanet)
            this.w_CMRSCSEL = this.w_cosselez * (this.w_qtalor - this.w_qtafin)
            this.w_CMRICSEL = iif(this.w_PERRIC="S", this.w_cosselez * this.w_qtafin * perric/100 , 0)
          endif
          this.w_cosselez = this.w_cosuni * this.w_QTAFIN
          this.w_CosRec = this.w_cosuni * (this.w_qtalor - this.w_qtafin)
          this.w_cosselez = this.w_cosselez * (1 + iif(this.w_PERRIC="S", perric/100,0))
        endif
        if this.w_Analisi="S"
          if isnull(this.w_cossta)
            this.w_cossta = 0
          else
            * --- Analisi Costo Standard
            this.w_CMNETSTD = cp_ROUND(this.w_cossta * qtanet, this.w_DecStampa)
            this.w_CMSCASTD = cp_ROUND(this.w_cossta * (this.w_qtalor- this.w_qtanet), this.w_DecStampa)
            this.w_CMRSCSTD = cp_ROUND(this.w_cossta * (this.w_qtalor - this.w_qtafin), this.w_DecStampa)
            this.w_CMRICSTD = cp_ROUND(iif(this.w_PERRIC="S", this.w_cossta * (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * perric/100,0), this.w_DecStampa)
            this.w_cosunist = this.w_cossta
            this.w_cossta = cp_ROUND(this.w_cosunist * this.w_qtafin, this.w_DecStampa) && (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * (1 + iif(this.w_PERRIC="S", perric/100,0)), this.w_DecStampa)
          endif
          if isnull(this.w_cosmed)
            this.w_cosmed = 0
          else
            * --- Analisi Costo Medio
            this.w_CMNETMED = cp_ROUND(this.w_cosmed * this.w_qtanet, this.w_DecStampa)
            this.w_CMSCAMED = cp_ROUND(this.w_cosmed * (this.w_qtalor - this.w_qtanet), this.w_DecStampa)
            this.w_CMRSCMED = cp_ROUND(this.w_cosmed * (this.w_qtalor - this.w_qtafin), this.w_DecStampa)
            this.w_CMRICMED = cp_ROUND(iif(this.w_PERRIC="S", this.w_cosmed * (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * perric/100,0), this.w_DecStampa) && qtalot tolto
            this.w_cosunime = this.w_cosmed
            this.w_cosmed = cp_ROUND(this.w_cosunime * (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * (1 + iif(this.w_PERRIC="S", perric/100,0)), this.w_DecStampa)
          endif
          if isnull(this.w_cosult)
            this.w_cosult = 0
          else
            * --- Analisi Costo Ultimo
            this.w_CMNETULT = cp_ROUND(this.w_cosult * this.w_qtanet, this.w_DecStampa)
            this.w_CMSCAULT = cp_ROUND(this.w_cosult * (this.w_qtalor - this.w_qtanet), this.w_DecStampa)
            this.w_CMRSCULT = cp_ROUND(this.w_cosult * (this.w_qtalor - this.w_qtafin), this.w_DecStampa)
            this.w_CMRICULT = cp_ROUND(iif(this.w_PERRIC="S", this.w_cosult * (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * perric/100,0), this.w_DecStampa)
            this.w_cosuniul = this.w_cosult
            this.w_cosult = cp_ROUND(this.w_cosuniul * (this.w_qtanet+(this.w_qtalor-this.w_qtafin)) * (1 + iif(this.w_PERRIC="S", perric/100,0)), this.w_DecStampa)
          endif
          replace COSREC with this.w_CosRec
          REPLACE costo WITH this.w_cosselez,cstandr WITH this.w_cossta, ;
          cmedio WITH this.w_cosmed, cultim WITH this.w_cosult
          REPLACE cosunise WITH cosunise+this.w_cosuni, cosunist WITH cosunist+this.w_cosunist, cosunime WITH cosunime+this.w_cosunime, ; 
 cosuniul WITH cosuniul+this.w_cosuniul
        else
          REPLACE costo WITH this.w_cosselez
          REPLACE cosunise WITH this.w_cosuni
          replace COSREC with this.w_CosRec
        endif
      else
        * --- Per gli articoli interni, nodi dell'albero, calcola il costo del ciclo semplificato
        this.w_LVLKEY = LVLKEY
        this.w_codiartico = iif(livello=0, artcom, codcom)
        this.w_codiartic1 = CODDIS
        this.w_TROVATO = .T.
        this.w_qtaparzin = cp_ROUND(qtanet*this.w_COECONV, this.w_DecStampa)
        this.w_qtaparzil = cp_ROUND(qtalor*this.w_COECONV, this.w_DecStampa)
        this.w_qtaparzif = cp_ROUND(qtafin*this.w_COECONV, this.w_DecStampa)
        this.w_cosselez = 0
        this.w_cossetup = 0
        this.w_CONTRATTO = (g_COLA="S")
        SELECT scheris
        SCAN FOR scheris.cacodric=this.w_codiartico and scheris.caunmisu=&ElabCursor..unimis and cp_round(nvl(scheris.caqtamov,0), this.w_DecStampa)=cp_round(&elabcursor..qtalor, this.w_DecStampa) and scheris.lvlkey1=this.w_lvlkey
        this.w_CONTRATTO = False
        this.w_qtalmstp = IIF(this.w_PARELAB or this.w_LOTTOMED or this.w_maxlvl>0, this.w_qtaparzil / nvl(prlotmed,1), 1)
        do case
          case scheris.srunatan="S"
            this.w_csapposetu = scheris.srqtaris * scheris.dpcossta * this.w_qtalmstp
            * --- Azzero la variabile di appoggio costi Lavorazioni
            *     in modo da non moltiplicare il valore
            this.w_cosappo = 0
          otherwise
            * --- Nel caso sia gestito applico il recupero Costi sulla lavorazione
            this.w_cosappo = scheris.srqtaris * scheris.dpcossta * this.w_qtaparzil
            * --- Azzero il costo di Setup per non farlo moltiplicare
            this.w_csapposetu = 0
        endcase
        if this.w_Analisi="S"
          if scheris.srunatan = "S"
            this.w_CSELSETU = cp_ROUND(this.w_CSELSETU + this.w_csapposetu, this.w_DecStampa)
          else
            this.w_CSNETSEL = cp_ROUND(this.w_CSNETSEL + scheris.srqtaris * scheris.dpcossta * this.w_qtaparzin, this.w_DecStampa)
            this.w_CSSCASEL = cp_ROUND(this.w_CSSCASEL + scheris.srqtaris * scheris.dpcossta * (this.w_qtaparzil - this.w_qtaparzin), this.w_DecStampa)
            this.w_CSRSCSEL = cp_ROUND(this.w_CSRSCSEL + scheris.srqtaris * scheris.dpcossta * (this.w_qtaparzil - this.w_qtaparzif), this.w_DecStampa)
          endif
        endif
        this.w_costotcic = this.w_costotcic + iif(scheris.srunatan <> "S", scheris.srqtaris * scheris.dpcossta, 0)
        this.w_coscic = this.w_coscic + iif(scheris.srunatan <> "S", scheris.srqtaris * scheris.dpcossta, 0)
        this.w_cossetup = this.w_cossetup + this.w_csapposetu
        this.w_cosselez = cp_ROUND(this.w_cosselez + this.w_cosappo, this.w_DecStampa)
        ENDSCAN
        SELECT (ElabCursor)
        this.w_COPERRIC = iif(this.w_PERRIC="S", 1+ perric/100, 1)
        if isnull(this.w_cosselez)
          * --- ERRORE: prezzo non definito!
          this.w_cosselez = 0
          this.w_costotcic = 0
        endif
        this.w_cosselez = this.w_cosselez + this.w_cossetup
        this.w_cossetup = cp_round(this.w_cossetup + cossetu, this.w_DecStampa)
        if this.w_Analisi="S"
          * --- Recupero i Costi delle Lavorazioni
          this.w_cossta = cp_ROUND(cstandr + cosunist *(this.w_qtaparzil- this.w_qtaparzif) + this.w_cosselez, this.w_DecStampa)
          this.w_cosmed = cp_ROUND(cmedio + cosunime * (this.w_qtaparzil- this.w_qtaparzif) + this.w_cosselez, this.w_DecStampa)
          this.w_cosult = cp_ROUND(cultim + cosuniul * (this.w_qtaparzil- this.w_qtaparzif) + this.w_cosselez, this.w_DecStampa)
          this.w_cosselez = costo + this.w_cosselez - cp_ROUND(this.w_coscic * (this.w_qtaparzil - this.w_qtaparzif), this.w_DecStampa)
          this.w_CosRec = cp_Round(this.w_coscic * (this.w_qtaparzil - this.w_qtaparzif), this.w_DecStampa)
          REPLACE cosrec with this.w_cosrec
          REPLACE costo WITH this.w_cosselez, cstandr WITH this.w_cossta, cmedio WITH this.w_cosmed, cultim WITH this.w_cosult
          REPLACE coscic WITH this.w_costotcic,cossetu WITH cossetu+this.w_cossetup, cosrec with this.w_cosrec
        else
          this.w_cosselez = this.w_cosselez + costo - this.w_coscic * (this.w_qtaparzil - this.w_qtaparzif)
          this.w_CosRec = cp_Round(this.w_coscic * (this.w_qtaparzil - this.w_qtaparzif) , this.w_DecStampa)
          REPLACE costo WITH this.w_cosselez, cosrec with this.w_cosrec
          REPLACE coscic WITH this.w_costotcic, cossetu WITH cossetu+this.w_cossetup
        endif
        if this.w_Analisi="S"
          * --- Ricarico Costi di Produzione
          this.w_CSNETSEL = nvl(this.w_CSNETSEL,0)
          this.w_CSSCASEL = nvl(this.w_CSSCASEL,0)
          this.w_CSRSCSEL = nvl(this.w_CSRSCSEL,0)
          this.w_CSRICSEL = nvl(this.w_CSRICSEL,0)
          if this.w_PERRIC="S"
            this.w_CMRICSEL = cp_ROUND(this.w_cosselez * (this.w_COPERRIC-1), this.w_DecStampa)
            this.w_CMRICSTD = cp_ROUND(this.w_cossta * (this.w_COPERRIC-1), this.w_DecStampa)
            this.w_CMRICMED = cp_ROUND(this.w_cosmed * (this.w_COPERRIC-1), this.w_DecStampa)
            this.w_CMRICULT = cp_ROUND(this.w_cosult * (this.w_COPERRIC-1), this.w_DecStampa)
          endif
          this.w_cosselez = cp_ROUND(this.w_cosselez * this.w_COPERRIC, this.w_DecStampa)
          this.w_cossta = cp_ROUND(this.w_cossta * this.w_COPERRIC, this.w_DecStampa)
          this.w_cosmed = cp_ROUND(this.w_cosmed* this.w_COPERRIC, this.w_DecStampa)
          this.w_cosult = cp_ROUND(this.w_cosult * this.w_COPERRIC, this.w_DecStampa)
          REPLACE costo WITH this.w_cosselez, cstandr WITH this.w_cossta, cmedio WITH this.w_cosmed, cultim WITH this.w_cosult
        else
          this.w_cosselez = cp_ROUND(this.w_cosselez * this.w_COPERRIC, this.w_DecStampa)
          REPLACE costo WITH this.w_cosselez
        endif
      endif
      this.w_FLCOMPPAD = SPACE(1)
      SELECT (ElabCursor)
      this.w_mykey = lvlkey
      if this.w_maxlvl>0 and this.w_TROVATO AND this.w_VALCOM="S"
        * --- Aggiorna il padre
        SEEK left(this.w_mykey, this.w_maxlvl*this.w_LenLvl-1) ORDER lvlkey
        this.w_FLCOMPPAD = NVL(FLCOMP, SPACE(1))
        if this.w_FLCOMPPAD<>"S"
          if this.w_Analisi="S"
            REPLACE costo WITH costo+this.w_cosselez, cstandr WITH cstandr+this.w_cossta, ;
            cmedio WITH cmedio+this.w_cosmed, cultim WITH cultim+this.w_cosult
            REPLACE cosunise WITH cosunise+this.w_cosuni, cosunist WITH cosunist+this.w_cosunist, cosunime WITH cosunime+this.w_cosunime, ; 
 cosuniul WITH cosuniul+this.w_cosuniul, coscic WITH coscic+this.w_costotcic, cossetu WITH cossetu+this.w_cossetup
          else
            REPLACE costo WITH costo+this.w_cosselez
            REPLACE cosunise WITH cosunise+this.w_cosuni, coscic WITH coscic+this.w_costotcic, cossetu WITH cossetu+this.w_cossetup
          endif
        endif
        SEEK this.w_mykey ORDER lvlkey
      endif
      if this.w_FLCOMPPAD<>"S"
        if this.w_Analisi="S" and this.w_TROVATO
          this.w_CODIAR = left(this.w_mykey, this.w_LenLvl-1) + chr(255)
          if flcomp="S" or not this.w_FORNITO
            * --- Costo materiali netto
            this.w_CODTMP = left(this.w_CODIAR + " 1" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo+ this.w_Cmnetsel, cstandr WITH cstandr+ this.w_Cmnetstd, ;
            cmedio WITH cmedio+ this.w_Cmnetmed, cultim WITH cultim+ this.w_Cmnetult
            * --- Costo materiali scarto
            this.w_CODTMP = left(this.w_CODIAR + " 2" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo+ this.w_Cmscasel, cstandr WITH cstandr+ this.w_Cmscastd, ;
            cmedio WITH cmedio+ this.w_Cmscamed, cultim WITH cultim+ this.w_Cmscault
            * --- Recupero Costo materiali
            this.w_CODTMP = left(this.w_CODIAR + " 3" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo-this.w_Cmrscsel, cstandr WITH cstandr-this.w_Cmrscstd, ;
            cmedio WITH cmedio-this.w_Cmrscmed, cultim WITH cultim-this.w_Cmrscult
            if this.w_PERRIC="S"
              * --- Costo materiali ricarico
              this.w_CODTMP = left(this.w_CODIAR + " 4" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
              cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
            endif
          else
            if this.w_PERRIC="S"
              * --- Costo materiali ricarico
              this.w_CODTMP = left(this.w_CODIAR + " 4" + space(this.w_CODLEN),this.w_CODLEN)
              SEEK this.w_CODTMP ORDER lvlkey
              REPLACE costo WITH costo+ this.w_Cmricsel, cstandr WITH cstandr+ this.w_Cmricstd, ;
              cmedio WITH cmedio+ this.w_Cmricmed, cultim WITH cultim+ this.w_Cmricult
            endif
            * --- Recupero Costo materiali
            this.w_CODTMP = left(this.w_CODIAR + " 3" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo-this.w_Cmrscsel, cstandr WITH cstandr-this.w_Cmrscstd, ;
            cmedio WITH cmedio-this.w_Cmrscmed, cultim WITH cultim-this.w_Cmrscult
            * --- Costo lavorazioni netto
            this.w_CODTMP = left(this.w_CODIAR + " 5" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo+ this.w_Csnetsel, cstandr WITH cstandr+ this.w_Csnetsel, ;
            cmedio WITH cmedio+ this.w_Csnetsel, cultim WITH cultim+ this.w_Csnetsel
            * --- Costo lavorazioni scarto
            this.w_CODTMP = left(this.w_CODIAR + " 6" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo+ this.w_Csscasel, cstandr WITH cstandr+ this.w_Csscasel, ;
            cmedio WITH cmedio+ this.w_Csscasel, cultim WITH cultim+ this.w_Csscasel
            * --- Recupero Costo Lavorazioni
            this.w_CODTMP = left(this.w_CODIAR + " 7" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo- this.w_Csrscsel, cstandr WITH cstandr- this.w_Csrscsel, ;
            cmedio WITH cmedio- this.w_Csrscsel, cultim WITH cultim- this.w_Csrscsel
            * --- Netto Setup
            this.w_CODTMP = left(this.w_CODIAR + " 8" + space(this.w_CODLEN),this.w_CODLEN)
            SEEK this.w_CODTMP ORDER lvlkey
            REPLACE costo WITH costo+ this.w_CSELSETU, cstandr WITH cstandr+ this.w_CSELSETU, ;
            cmedio WITH cmedio+ this.w_CSELSETU, cultim WITH cultim+ this.w_CSELSETU
          endif
          SEEK this.w_mykey ORDER lvlkey
        endif
      endif
      SELECT (ElabCursor)
      ENDSCAN
      this.w_maxlvl = this.w_maxlvl-1
    enddo
    if this.w_PARELAB
      this.w_TipoValo = this.w_TipoValoOld
    endif
    if this.w_RECADDED
      * --- Questo trucco non fa perdere l'ultimo record aggiunto dai contratti
      Select Scheris 
 append blank 
 delete
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare w_VARIAR per definire il cursore e il campo dei codici da passare
    * --- Il campo cakeysal ha il nome fisso (questo campo non ha indicer)
    if at(".", this.w_VARIAR)>0
      cCurstmp = left(this.w_VARIAR ,at(".", this.w_VARIAR)-1) 
 SELECT (cCurstmp)
      * --- begin transaction
      cp_BeginTrs()
      do case
        case (type(cCurstmp+".counimis") = "C") and (type(cCurstmp+".cocoeimp") = "N")
          SCAN
          this.w_CODIAR = padr(eval(this.w_VARIAR),20)
          this.w_CAKEYSAL = eval(cCurstmp+".cakeysal")
          this.w_ROWNUM = eval(cCurstmp+".CPROWNUM")
          this.w_CAUNMISU = eval(cCurstmp+".counimis")
          this.w_CAQTAMOV = eval(cCurstmp+".cocoeimp")
          this.w_CAQTAMO1 = eval(cCurstmp+".cocoeum1")
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CAUNMISU"+",CAQTAMOV"+",CAQTAMO1"+",CACODDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAUNMISU),'ART_TEMP','CAUNMISU');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMOV),'ART_TEMP','CAQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMO1),'ART_TEMP','CAQTAMO1');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CPROWNUM',this.w_ROWNUM,'CAUNMISU',this.w_CAUNMISU,'CAQTAMOV',this.w_CAQTAMOV,'CAQTAMO1',this.w_CAQTAMO1,'CACODDIS',SPACE(20))
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CAUNMISU,CAQTAMOV,CAQTAMO1,CACODDIS &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,this.w_ROWNUM;
                 ,this.w_CAUNMISU;
                 ,this.w_CAQTAMOV;
                 ,this.w_CAQTAMO1;
                 ,SPACE(20);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
        case (type(cCurstmp+".cakeysal") = "C")
          SCAN
          this.w_CODIAR = padr(eval(this.w_VARIAR),20)
          this.w_CAKEYSAL = eval(cCurstmp+".cakeysal")
          if this.w_bNoUseRowNum
            this.w_ROWNUM = 0
          else
            this.w_ROWNUM = eval(cCurstmp+".CPROWNUM")
          endif
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYSAL),'ART_TEMP','CAKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_CAKEYSAL,'CPROWNUM',this.w_ROWNUM,'CACODDIS',SPACE(20))
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CACODDIS &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_CAKEYSAL;
                 ,this.w_ROWNUM;
                 ,SPACE(20);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
          this.w_bNoUseRowNum = .T.
        otherwise
          SCAN
          this.w_CODIAR = padr(eval(this.w_VARIAR),20)
          this.w_ROWNUM = 0
          * --- Insert into ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CAKEYRIF"+",CACODRIC"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CPROWNUM',this.w_ROWNUM,'CACODDIS',SPACE(20))
            insert into (i_cTable) (CAKEYRIF,CACODRIC,CPROWNUM,CACODDIS &i_ccchkf. );
               values (;
                 this.w_KEYRIF;
                 ,this.w_CODIAR;
                 ,this.w_ROWNUM;
                 ,SPACE(20);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
      endcase
      * --- commit
      cp_EndTrs(.t.)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costi Articoli
    PRIVATE cField,cJoin 
 cField = "cacodric" 
 cJoin = "ArtDist "
    * --- Inserisce nella tabella ART_TEMP gli articoli esterni (foglie) della distinta
    if this.w_TipoValo="L"
      SELECT DISTINCT &ElabCursor..artcom as cacodric, ; 
 LEFT(&ElabCursor..artcom+SPACE(20), 20)+"####################" AS cakeysal, ; 
 NVL(&ElabCursor..unimis, space(3)) as counimis, ; 
 cp_round(NVL(&ElabCursor..qtalor, 0), this.w_DecStampa) as cocoeimp, ; 
 cp_round(NVL(&ElabCursor..qtalo1, 0), this.w_DecStampa) as cocoeum1, ; 
 recno() as CPROWNUM ; 
 from (ElabCursor) where IIF(this.w_VALCOM="S", nvl(flcomp,"X")="S", 1=1) INTO cursor ArtDist nofilter
    else
      SELECT DISTINCT &ElabCursor..artcom as cacodric, ; 
 LEFT(&ElabCursor..artcom+SPACE(20), 20)+"####################" AS cakeysal, ; 
 space(3) as counimis, 0 as cocoeimp, 0 as cocoeum1, 0 as CPROWNUM ; 
 from (ElabCursor) where IIF(this.w_VALCOM="S", nvl(flcomp,"X")="S", 1=1) INTO cursor ArtDist nofilter
    endif
    this.w_VARIAR = "ArtDist.cacodric"
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_TipoValo="L"
      this.w_LIPREZZO = 0
      * --- Calcola Prezzo e Sconti
      * --- Azzero l'Array che verr� riempito dalla Funzione
      * --- Prezzi di listino
      USE IN SELECT("przlist")
      vq_exec("..\DISB\EXE\QUERY\GSDSLBDC.VQR", this, "przlist")
      SELECT "przlist"
      GO TOP
      SCAN FOR !EMPTY(LICODLIS)
      this.w_LIPREZZO = 0
      this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
      this.w_OPERAT = NVL(AROPERAT , "*" )
      this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
      this.w_OPERA3 = NVL(CAOPERAT , "*" )
      this.w_MOLTI3 = NVL(CAMOLTIP, 0)
      this.w_PREZUM = ARPREZUM
      this.w_CAUNMISU = NVL(CAUNMISU, SPACE(3))
      this.w_CAQTAMOV = NVL(CAQTAMOV, 0)
      this.w_CAQTAMO1 = NVL(CAQTAMO1, 0)
      this.w_CODIAR = left(nvl(LICODART, space(20))+space(20) , 20)
      this.w_CODVAL = nvl(LSVALLIS, g_PERVAL)
      DECLARE ARRCALC (16)
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
      this.w_QTAUM2 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_CAUNMISU 
 pArrUm [3] = this.w_CAQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_CAQTAMO1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      this.w_SCOLIS = "S"
      this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.w_Listino , this.w_CODIAR , " " , this.w_CAQTAMOV , this.w_VALLIS , this.w_CAOLIS , this.w_DATSTA , " " , " ", g_PERVAL , " ", " ", " ", this.w_SCOLIS, 0,"M", @ARRCALC, " ", " ","N",@pArrUm )
      this.w_DECTOT = getvalut(this.w_VALESE,"VADECUNI")
      this.w_CLUNIMIS = ARRCALC(16)
      this.w_LIPREZZO = CAVALRIG(ARRCALC(5),1, ARRCALC(1),ARRCALC(2),ARRCALC(3),ARRCALC(4),this.w_DECTOT)
      if this.w_CAUNMISU<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
        this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_CAQTAMO1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
        this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_CAQTAMOV, this.w_QTALIS, "N", 0, this.w_DECTOT), this.w_DECTOT)
      endif
      if this.w_VALLIS<>g_PERVAL
        * --- Cambio valuta
        this.w_CAO = GETCAM(this.w_VALLIS, i_DATSYS, 0)
        if this.w_CAO<>0
          this.w_LIPREZZO = VAL2MON(this.w_LIPREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
        else
          ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato",48)
          this.w_ERRO = .T.
        endif
      endif
      SELECT "przlist"
      REPLACE LAPREZZO WITH this.w_LIPREZZO
      ENDSCAN
      if used("przlist")
         =WRCURSOR("przlist") 
 select przlist
      else
      endif
      if this.w_ERRO
        this.w_ERRO = .F.
      else
        cField = cField + ", nvl(caunmisu,  space(3)) as unimis, nvl(caqtamov, 000000000000.00000) as caqtamov"
        cField = cField + ", nvl(laprezzo, 000000000000.00000) as laprezzo, ivperiva"
        cJoin = cJoin +" left outer join przlist ON przlist.licodart=ArtDist.cacodric "
      endif
    endif
    if this.w_PARELAB or this.w_TipoValo="A"
      * --- Ultimo Costo Saldi Articoli
      vq_exec("..\DISB\EXE\QUERY\GSDSABDC", this, "przultsa")
      cField = cField + ",nvl(slcodvaa, g_PERVAL) AS slcodvaa, nvl(slvaluca,000000000000.00000) as slvaluca"
      cJoin = cJoin +" left outer join przultsa ON przultsa.slcodice=ArtDist.cacodric "
    endif
    if this.w_Analisi="S" or this.w_TipoValo $ "SMUP"
      if Empty(this.w_INNUMPRE) 
        * --- Determinazione Costo
        cField = cField + ", 000000000000,00000 as dicosmpp, 000000000000,00000 as dicosmpa" 
 cField = cField + ", 000000000000,00000 as dicosult, 000000000000,00000 as dicossta"
      else
        if this.w_PARELAB
          this.w_TipoValoOld = this.w_TipoValo
          this.w_TipoValo = IIF(this.oParentObject.w_AGGMPOND="N" , "M", this.oParentObject.w_AGGMPOND)
        endif
        * --- Costo medio ponderato e Costo Ultimo (da inventario)
        vq_exec("..\DISB\EXE\QUERY\GSDSIBDC.VQR", this, "przinve")
        =WRCURSOR("przinve")
        * --- Read from INVENTAR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INVENTAR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM"+;
            " from "+i_cTable+" INVENTAR where ";
                +"INNUMINV = "+cp_ToStrODBC(this.w_NUMINV);
                +" and INCODESE = "+cp_ToStrODBC(this.w_ESERC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
            from (i_cTable) where;
                INNUMINV = this.w_NUMINV;
                and INCODESE = this.w_ESERC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INCODMAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
          this.w_INMAGCOL = NVL(cp_ToDate(_read_.INMAGCOL),cp_NullValue(_read_.INMAGCOL))
          this.w_PRDATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
          this.w_INCATOMO = NVL(cp_ToDate(_read_.INCATOMO),cp_NullValue(_read_.INCATOMO))
          this.w_INGRUMER = NVL(cp_ToDate(_read_.INGRUMER),cp_NullValue(_read_.INGRUMER))
          this.w_INCODFAM = NVL(cp_ToDate(_read_.INCODFAM),cp_NullValue(_read_.INCODFAM))
          this.w_MAGRAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGMAGRAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_INCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGMAGRAG;
            from (i_cTable) where;
                MGCODMAG = this.w_INCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MAGRAG = IIF (EMPTY(NVL(this.w_MAGRAG,"")),this.w_INCODMAG,this.w_MAGRAG)
        if this.oParentObject.w_FLCOSINV<>"S"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_PARELAB
          this.w_TipoValo = this.w_TipoValoOld
        endif
        if this.w_VALESE<>g_PERVAL
          * --- Cambio valuta
          this.w_CAO = GETCAM(this.w_VALESE, i_DATSYS, 0)
          if this.w_CAO<>0
            select przinve
            scan
            this.w_PREZZO = dicosmpa
            this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
            replace dicosmpa with this.w_PREZZO
            endscan
          else
            ah_ErrorMsg("Impossibile fare il cambio di valuta per l'inventario selezionato","!","")
            this.w_ERRO = .T.
          endif
        endif
        if this.w_ERRO
          this.w_ERRO = .F.
        else
          cField = cField + ", dicosmpp,dicosmpa,dicosult,dicossta"
          cJoin = cJoin +" left outer join przinve ON przinve.cakeysal=ArtDist.cakeysal "
        endif
      endif
    endif
    if this.w_Analisi="S" or this.w_TipoValo="X"
      * --- Costo standard
      vq_exec("..\DISB\EXE\QUERY\GSDSSBDC", this, "przstan")
      cField = cField + ",prcossta AS dicossta_ar"
      cJoin = cJoin +" left outer join przstan ON przstan.prcodart=ArtDist.cacodric "
    endif
    * --- Cursore dei costi
    SELECT DISTINCT &cField FROM &cJoin ORDER BY cacodric INTO CURSOR przart 
 =WRCURSOR("przart") 
 INDEX ON cacodric TAG cacodric
    if used("ArtDist")
      use in ArtDist
    endif
    if used("przulti")
      use in przulti
    endif
    if used("przinve")
      use in przinve
    endif
    if used("przstan")
      use in przstan
    endif
    if used("przultsa")
      use in przultsa
    endif
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il Costo Standard
    * --- Aggiorna Costi
    if !this.w_PARELAB
      * --- Aggiorna il Costo Standard
      ah_Msg("Aggiornamento dei costi standard...",.T.)
      select CODART, ARTCOM, COSTO, COSTO/QTALOR as COSUNI, COSTO/QTALO1 as COSUNI1, QTANET, QTANET1, QTALOR, QTALO1, NUMLEV; 
 from (ElabCursor) where Raggrupp=1 and FLCOMP<>"S" and (NUMLEV="0000" and !empty(CODDIS)) into cursor TmpUpd
      SELECT TmpUpd
      GO TOP
      * --- Cicla Sugli Articoli Distinte (da valorizzare)
      SCAN
      this.w_ARTCOM = IIF(NUMLEV="0000", CODART, ARTCOM)
      * --- Calcola partendo dal Costo totale in quanto il Costo Unitario non Comprende la % Ricarico e i Cicli di Lavorazione
      this.w_cosuni = cp_ROUND(COSTO / QTALO1, g_PERPUL)
      * --- Aggiorna i Dati Articoli x Valorizzazione
      * --- Try
      local bErr_049AF3F8
      bErr_049AF3F8=bTrsErr
      this.Try_049AF3F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_049AF3F8
      * --- End
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_COSUNI),'PAR_RIOR','PRCOSSTA');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
               )
      else
        update (i_cTable) set;
            PRCOSSTA = this.w_COSUNI;
            &i_ccchkf. ;
         where;
            PRCODART = this.w_ARTCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Dati Singolo Magazzino 
      if this.oParentObject.w_FLDETTMAG="S" AND NOT EMPTY(this.w_MAGRAG) AND this.w_TipoValo $ "SMUP"
        * --- Try
        local bErr_049A2408
        bErr_049A2408=bTrsErr
        this.Try_049A2408()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_049A2408
        * --- End
        * --- Write into PAR_RIMA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIMA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_COSUNI),'PAR_RIMA','PRCOSSTA');
              +i_ccchkf ;
          +" where ";
              +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
              +" and PRCODMAG = "+cp_ToStrODBC(this.w_MAGRAG);
                 )
        else
          update (i_cTable) set;
              PRCOSSTA = this.w_COSUNI;
              &i_ccchkf. ;
           where;
              PRCODART = this.w_ARTCOM;
              and PRCODMAG = this.w_MAGRAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      SELECT TmpUpd
      ENDSCAN
      wait clear
      if used("TmpUpd")
        use in TmpUpd
      endif
    else
      this.w_STD = iif(this.oParentObject.w_STANDARD<>"N", "=", " ")
      this.w_UTE = iif(this.oParentObject.w_UTENTE, "=", " ")
      this.w_ULT = iif(this.oParentObject.w_ULTIMO<>"N", "=", " ")
      this.w_MED = iif(this.oParentObject.w_MEDIOPON, "=", " ")
      this.w_LST = iif(this.oParentObject.w_CLISTINO, "=", " ")
      ah_Msg("Aggiornamento dei costi standard...",.T.)
       Select 1 as tipocos, CODART, SUM(COSTO) AS COSTO , SUM(COSTO/QTALOR) as COSUNI, SUM(COSTO/QTALO1) as COSUNI1, QTANET, QTANET1, QTALOR, QTALO1, NUMLEV ,; 
 SUM(cstandr) as cstandr , SUM(cmedio) as cmedio, SUM(cultim) as cultim from (ElabCursor) where livello=-1 and flcomp<>"S" GROUP BY 1,2 order by codart,tipocos UNION ; 
 Select 2 as tipocos, CODART, sum(nvl(dpcossta * iif(srunatan="S", srqtaris * ((qtalor*(qtanet1/qtanet)/nvl(prlotmed,1))), srqtaris * (qtanet1/qtanet)),0)) as costo , COSTO/QTALOR as COSUNI,; 
 COSTO/QTALO1 as COSUNI1, QTANET, QTANET1, QTALOR, QTALO1, NUMLEV , ; 
 costo as CSTANDR, costo as CMEDIO, costo as CULTIM from ScheRis right outer join (ElabCursor) ; 
 ON cacodric=artcom and caunmisu=&ElabCursor..unimis and cp_round(nvl(caqtamov,0), this.w_DecStampa)=cp_round(&elabcursor..qtalor, this.w_DecStampa) and lvlkey1=lvlkey ; 
 where &ElabCursor..livello=0 and flcomp<>"S" group by tipocos, codart INTO CURSOR tmpupd
      Select tmpupd 
 scan
      do case
        case tipocos=1
          * --- Costi totali (materiali + lavorazioni)
          this.w_PRCSMATE = cstandr
          this.w_PRCUMATE = cultim
          this.w_PRCMMATE = cmedio
          this.w_PRCLMATE = costo
        case tipocos=2
          * --- Scorpora i costi delle lavorazioni
          this.w_ARTCOM = CODART
          this.w_PRCSMATE = this.w_PRCSMATE - costo
          this.w_PRCUMATE = this.w_PRCUMATE - costo
          this.w_PRCMMATE = this.w_PRCMMATE - costo
          this.w_PRCLMATE = this.w_PRCLMATE - costo
          this.w_PRCSLAVO = costo
          this.w_PRCULAVO = costo
          this.w_PRCMLAVO = costo
          this.w_PRCLLAVO = costo
          if this.oParentObject.w_UTENTE
            do case
              case this.oParentObject.w_AGGSTAND="S"
                this.w_PRCOSSTA = this.w_PRCSMATE + this.w_PRCSLAVO
              case this.oParentObject.w_AGGSTAND="U"
                this.w_PRCOSSTA = this.w_PRCUMATE + this.w_PRCULAVO
              case this.oParentObject.w_AGGSTAND="M"
                this.w_PRCOSSTA = this.w_PRCMMATE + this.w_PRCMLAVO
              case this.oParentObject.w_AGGSTAND="L"
                this.w_PRCOSSTA = this.w_PRCLMATE + this.w_PRCLLAVO
            endcase
          endif
          * --- Aggiorna i Dati Articoli x Valorizzazione
          * --- Try
          local bErr_04A0A300
          bErr_04A0A300=bTrsErr
          this.Try_04A0A300()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore di scrittura nel database %0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL")
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04A0A300
          * --- End
          * --- Aggiorna Dati Singolo Magazzino 
          if this.oParentObject.w_FLDETTMAG="S" AND NOT EMPTY(this.w_MAGRAG) AND this.w_TipoValo $ "SMUP"
            * --- Try
            local bErr_04A004F0
            bErr_04A004F0=bTrsErr
            this.Try_04A004F0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04A004F0
            * --- End
            * --- Write into PAR_RIMA
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIMA_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_PRCOSSTA),'PAR_RIMA','PRCOSSTA');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
                  +" and PRCODMAG = "+cp_ToStrODBC(this.w_MAGRAG);
                     )
            else
              update (i_cTable) set;
                  PRCOSSTA = this.w_PRCOSSTA;
                  &i_ccchkf. ;
               where;
                  PRCODART = this.w_ARTCOM;
                  and PRCODMAG = this.w_MAGRAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.w_AGCOSSTD="S"
            * --- Variabile di appoggio per il calcolo del numero di riga
            this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO + 1
            * --- Try
            local bErr_049FD130
            bErr_049FD130=bTrsErr
            this.Try_049FD130()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO - 1
            endif
            bTrsErr=bTrsErr or bErr_049FD130
            * --- End
          endif
      endcase
      endscan 
 use in tmpupd
      * --- Aggiorna i costi delle Materie prime
      Select distinct przart.*,ARTCOM from przart inner join (ElabCursor) ; 
 on cacodric=artcom AND flcomp="S" AND raggrupp=1 into cursor Matpri 
 Select Matpri 
 scan
      this.w_ARTCOM = ARTCOM
      this.w_PRCSMATE = 0
      do case
        case this.oParentObject.w_STANDARD="S"
          this.w_PRCSMATE = nvl(dicossta,0)
        case this.oParentObject.w_STANDARD="X"
          this.w_PRCSMATE = nvl(dicossta_ar, 0)
      endcase
      this.w_PRCUMATE = 0
      do case
        case this.oParentObject.w_ULTIMO="U"
          this.w_PRCUMATE = nvl(dicosult,0)
        case this.oParentObject.w_ULTIMO="A"
          this.w_PRCUMATE = nvl(slvaluca, 0)
      endcase
      this.w_PRCMMATE = iif(this.oParentObject.w_MEDIOPON, nvl(dicosmpa,0), 0)
      this.w_PRCLMATE = iif(this.oParentObject.w_CLISTINO, nvl(laprezzo,0), 0)
      * --- Try
      local bErr_049C9AC8
      bErr_049C9AC8=bTrsErr
      this.Try_049C9AC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Errore di scrittura nel database %0Verificare se � stata eseguita la procedura di aggiornamento del campo xxKEYSAL")
      endif
      bTrsErr=bTrsErr or bErr_049C9AC8
      * --- End
      if this.w_AGCOSSTD="S"
        * --- Variabile di appoggio per il calcolo del numero di riga
        this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO + 1
        * --- Try
        local bErr_049CB928
        bErr_049CB928=bTrsErr
        this.Try_049CB928()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_NUMRIGSTO = this.oParentObject.w_NUMRIGSTO - 1
        endif
        bTrsErr=bTrsErr or bErr_049CB928
        * --- End
      endif
      Select Matpri 
 Endscan 
 use in Matpri
    endif
  endproc
  proc Try_049AF3F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIOR','PRCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM)
      insert into (i_cTable) (PRCODART &i_ccchkf. );
         values (;
           this.w_ARTCOM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_049A2408()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIMA
    i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIMA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+",PRCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIMA','PRCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRAG),'PAR_RIMA','PRCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM,'PRCODMAG',this.w_MAGRAG)
      insert into (i_cTable) (PRCODART,PRCODMAG &i_ccchkf. );
         values (;
           this.w_ARTCOM;
           ,this.w_MAGRAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A0A300()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_UTE,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp11=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp12=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
      i_cOp13=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
      i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp15=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
      i_cOp16=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCOSSTA ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCOSSTA');
      +",PRCSLAVO ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSLAVO');
      +",PRCSMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCSMATE');
      +",PRCULAVO ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCULAVO');
      +",PRCUMATE ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCUMATE');
      +",PRCMLAVO ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCMLAVO');
      +",PRCMMATE ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRCMMATE');
      +",PRCLLAVO ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRCLLAVO');
      +",PRCLMATE ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCLMATE');
      +",PRDATCOS ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATCOS');
      +",PRDATULT ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRDATULT');
      +",PRESECOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRINVCOS');
      +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
      +",PRLISCOS ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRLISCOS');
      +",PRDATLIS ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRDATLIS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
             )
    else
      update (i_cTable) set;
          PRCOSSTA = &i_cOp1.;
          ,PRCSLAVO = &i_cOp2.;
          ,PRCSMATE = &i_cOp3.;
          ,PRCULAVO = &i_cOp4.;
          ,PRCUMATE = &i_cOp5.;
          ,PRCMLAVO = &i_cOp6.;
          ,PRCMMATE = &i_cOp7.;
          ,PRCLLAVO = &i_cOp8.;
          ,PRCLMATE = &i_cOp9.;
          ,PRDATCOS = &i_cOp10.;
          ,PRDATULT = &i_cOp11.;
          ,PRESECOS = &i_cOp12.;
          ,PRINVCOS = &i_cOp13.;
          ,PRDATMED = &i_cOp14.;
          ,PRLISCOS = &i_cOp15.;
          ,PRDATLIS = &i_cOp16.;
          ,PUUTEELA = i_CODUTE;
          ,PRTIPCON = "F";
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARTCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_rows=0
      * --- Insert into PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODART"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIOR','PRCODART');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM)
        insert into (i_cTable) (PRCODART &i_ccchkf. );
           values (;
             this.w_ARTCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_UTE,'PRCOSSTA','this.w_PRCOSSTA',this.w_PRCOSSTA,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_STD,'PRCSLAVO','this.w_PRCSLAVO',this.w_PRCSLAVO,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_ULT,'PRCULAVO','this.w_PRCULAVO',this.w_PRCULAVO,'update',i_nConn)
        i_cOp5=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_MED,'PRCMLAVO','this.w_PRCMLAVO',this.w_PRCMLAVO,'update',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_LST,'PRCLLAVO','this.w_PRCLLAVO',this.w_PRCLLAVO,'update',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp11=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp12=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
        i_cOp13=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
        i_cOp14=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp15=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
        i_cOp16=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCOSSTA ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCOSSTA');
        +",PRCSLAVO ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCSLAVO');
        +",PRCSMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCSMATE');
        +",PRCULAVO ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCULAVO');
        +",PRCUMATE ="+cp_NullLink(i_cOp5,'PAR_RIOR','PRCUMATE');
        +",PRCMLAVO ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRCMLAVO');
        +",PRCMMATE ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRCMMATE');
        +",PRCLLAVO ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRCLLAVO');
        +",PRCLMATE ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRCLMATE');
        +",PRDATCOS ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATCOS');
        +",PRDATULT ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRDATULT');
        +",PRESECOS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRESECOS');
        +",PRINVCOS ="+cp_NullLink(i_cOp13,'PAR_RIOR','PRINVCOS');
        +",PRDATMED ="+cp_NullLink(i_cOp14,'PAR_RIOR','PRDATMED');
        +",PRLISCOS ="+cp_NullLink(i_cOp15,'PAR_RIOR','PRLISCOS');
        +",PRDATLIS ="+cp_NullLink(i_cOp16,'PAR_RIOR','PRDATLIS');
        +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
        +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
               )
      else
        update (i_cTable) set;
            PRCOSSTA = &i_cOp1.;
            ,PRCSLAVO = &i_cOp2.;
            ,PRCSMATE = &i_cOp3.;
            ,PRCULAVO = &i_cOp4.;
            ,PRCUMATE = &i_cOp5.;
            ,PRCMLAVO = &i_cOp6.;
            ,PRCMMATE = &i_cOp7.;
            ,PRCLLAVO = &i_cOp8.;
            ,PRCLMATE = &i_cOp9.;
            ,PRDATCOS = &i_cOp10.;
            ,PRDATULT = &i_cOp11.;
            ,PRESECOS = &i_cOp12.;
            ,PRINVCOS = &i_cOp13.;
            ,PRDATMED = &i_cOp14.;
            ,PRLISCOS = &i_cOp15.;
            ,PRDATLIS = &i_cOp16.;
            ,PUUTEELA = i_CODUTE;
            ,PRTIPCON = "F";
            &i_ccchkf. ;
         where;
            PRCODART = this.w_ARTCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_04A004F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_RIMA
    i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIMA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODART"+",PRCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIMA','PRCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGRAG),'PAR_RIMA','PRCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM,'PRCODMAG',this.w_MAGRAG)
      insert into (i_cTable) (PRCODART,PRCODMAG &i_ccchkf. );
         values (;
           this.w_ARTCOM;
           ,this.w_MAGRAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_049FD130()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PRKEYSAL = Left(this.w_ARTCOM+space(20),20) + Repl("#",20)
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIFSTO),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRKEYSAL),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO))),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIFSTO,'CACODRIC',this.w_ARTCOM,'CAKEYSAL',this.w_PRKEYSAL,'CACODART',ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO)),'CACODDIS',SPACE(20),'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIFSTO;
           ,this.w_ARTCOM;
           ,this.w_PRKEYSAL;
           ,ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO));
           ,SPACE(20);
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_049C9AC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PAR_RIOR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_cOp11=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
      i_cOp12=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCSMATE ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSMATE');
      +",PRCUMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCUMATE');
      +",PRCMMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATE');
      +",PRCLMATE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLMATE');
      +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
      +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
      +",PRDATULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRDATULT');
      +",PRESECOS ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRESECOS');
      +",PRINVCOS ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRINVCOS');
      +",PRDATMED ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATMED');
      +",PRLISCOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRLISCOS');
      +",PRDATLIS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRDATLIS');
      +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
          +i_ccchkf ;
      +" where ";
          +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
             )
    else
      update (i_cTable) set;
          PRCSMATE = &i_cOp1.;
          ,PRCUMATE = &i_cOp2.;
          ,PRCMMATE = &i_cOp3.;
          ,PRCLMATE = &i_cOp4.;
          ,PRTIPCON = "F";
          ,PRDATCOS = &i_cOp6.;
          ,PRDATULT = &i_cOp7.;
          ,PRESECOS = &i_cOp8.;
          ,PRINVCOS = &i_cOp9.;
          ,PRDATMED = &i_cOp10.;
          ,PRLISCOS = &i_cOp11.;
          ,PRDATLIS = &i_cOp12.;
          ,PUUTEELA = i_CODUTE;
          &i_ccchkf. ;
       where;
          PRCODART = this.w_ARTCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_rows=0
      * --- Insert into PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODART"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'PAR_RIOR','PRCODART');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_ARTCOM)
        insert into (i_cTable) (PRCODART &i_ccchkf. );
           values (;
             this.w_ARTCOM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into PAR_RIOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_STD,'PRCSMATE','this.w_PRCSMATE',this.w_PRCSMATE,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_ULT,'PRCUMATE','this.w_PRCUMATE',this.w_PRCUMATE,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MED,'PRCMMATE','this.w_PRCMMATE',this.w_PRCMMATE,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_LST,'PRCLMATE','this.w_PRCLMATE',this.w_PRCLMATE,'update',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_STD,'PRDATCOS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_ULT,'PRDATULT','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_MED,'PRESECOS','this.w_ESERC',this.w_ESERC,'update',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_MED,'PRINVCOS','this.w_NUMINV',this.w_NUMINV,'update',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_MED,'PRDATMED','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
        i_cOp11=cp_SetTrsOp(this.w_LST,'PRLISCOS','this.w_Listino',this.w_Listino,'update',i_nConn)
        i_cOp12=cp_SetTrsOp(this.w_LST,'PRDATLIS','this.w_DATSTA',this.w_DATSTA,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PRCSMATE ="+cp_NullLink(i_cOp1,'PAR_RIOR','PRCSMATE');
        +",PRCUMATE ="+cp_NullLink(i_cOp2,'PAR_RIOR','PRCUMATE');
        +",PRCMMATE ="+cp_NullLink(i_cOp3,'PAR_RIOR','PRCMMATE');
        +",PRCLMATE ="+cp_NullLink(i_cOp4,'PAR_RIOR','PRCLMATE');
        +",PRTIPCON ="+cp_NullLink(cp_ToStrODBC("F"),'PAR_RIOR','PRTIPCON');
        +",PRDATCOS ="+cp_NullLink(i_cOp6,'PAR_RIOR','PRDATCOS');
        +",PRDATULT ="+cp_NullLink(i_cOp7,'PAR_RIOR','PRDATULT');
        +",PRESECOS ="+cp_NullLink(i_cOp8,'PAR_RIOR','PRESECOS');
        +",PRINVCOS ="+cp_NullLink(i_cOp9,'PAR_RIOR','PRINVCOS');
        +",PRDATMED ="+cp_NullLink(i_cOp10,'PAR_RIOR','PRDATMED');
        +",PRLISCOS ="+cp_NullLink(i_cOp11,'PAR_RIOR','PRLISCOS');
        +",PRDATLIS ="+cp_NullLink(i_cOp12,'PAR_RIOR','PRDATLIS');
        +",PUUTEELA ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_RIOR','PUUTEELA');
            +i_ccchkf ;
        +" where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_ARTCOM);
               )
      else
        update (i_cTable) set;
            PRCSMATE = &i_cOp1.;
            ,PRCUMATE = &i_cOp2.;
            ,PRCMMATE = &i_cOp3.;
            ,PRCLMATE = &i_cOp4.;
            ,PRTIPCON = "F";
            ,PRDATCOS = &i_cOp6.;
            ,PRDATULT = &i_cOp7.;
            ,PRESECOS = &i_cOp8.;
            ,PRINVCOS = &i_cOp9.;
            ,PRDATMED = &i_cOp10.;
            ,PRLISCOS = &i_cOp11.;
            ,PRDATLIS = &i_cOp12.;
            ,PUUTEELA = i_CODUTE;
            &i_ccchkf. ;
         where;
            PRCODART = this.w_ARTCOM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_049CB928()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_PRKEYSAL = Left(this.w_ARTCOM+space(20),20) + Repl("#",20)
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODART"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIFSTO),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTCOM),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRKEYSAL),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO))),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIFSTO,'CACODRIC',this.w_ARTCOM,'CAKEYSAL',this.w_PRKEYSAL,'CACODART',ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO)),'CACODDIS',SPACE(20),'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODART,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIFSTO;
           ,this.w_ARTCOM;
           ,this.w_PRKEYSAL;
           ,ALLTRIM(STR(this.oParentObject.w_NUMRIGSTO));
           ,SPACE(20);
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiude i cursori
    if used("appogg")
      USE IN appogg
    endif
    if USED("ScheRis")
      USE IN ScheRis
    endif
    if !this.w_ClsName = "TGSDS1BDC"
      if used("PrzArt")
        USE IN PrzArt
      endif
    endif
    if used("PrzList")
      USE IN PrzList
    endif
    if used(ElabCursor)
      USE IN (ElabCursor)
    endif
    if USED("Contratti")
      use in Contratti
    endif
    if USED("Terzisti")
      use in Terzisti
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
    wait wind "" timeout 0.001
    ah_Msg("Analisi cicli semplificati e contratti di conto lavoro in corso...",.T.)
    SELECT DISTINCT lvlkey as calvlkey, &ElabCursor..artcom as cacodric, ; 
 LEFT(&ElabCursor..artcom+SPACE(20), 20)+"####################" AS cakeysal ,; 
 NVL(&ElabCursor..unimis, space(3)) as caunmisu, ; 
 cp_ROUND(NVL(&ElabCursor..qtalor, 0), this.w_DecStampa) as caqtamov, ; 
 cp_ROUND(NVL(&ElabCursor..qtalo1, 0), this.w_DecStampa) as caqtamo1, ; 
 NUMRIG AS CANUMRIG from (ElabCursor) ; 
 where flcomp <> "S" and !empty(coddis) and livello=0 INTO cursor ArtDist nofilter ;
    union ;
    SELECT DISTINCT lvlkey as calvlkey, &ElabCursor..codcom as cacodric, ; 
 LEFT(&ElabCursor..artcom+SPACE(20), 20)+"####################" AS cakeysal ,; 
 NVL(&ElabCursor..unimis, space(3)) as caunmisu, ; 
 cp_ROUND(NVL(&ElabCursor..qtalor, 0), this.w_DecStampa) as caqtamov, ; 
 cp_ROUND(NVL(&ElabCursor..qtalo1, 0), this.w_DecStampa) as caqtamo1, ; 
 NUMRIG AS CANUMRIG from (ElabCursor) ; 
 where flcomp <> "S" and !empty(coddis) and livello>0 
    clvlkey =" CAST(' ' AS CHAR(200)) "
    * --- Create temporary table TMPART_TEMP
    i_nIdx=cp_AddTableDef('TMPART_TEMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_QAZQAQVINM[1]
    indexes_QAZQAQVINM[1]='CACODRIC,CALVLKEY'
    i_nConn=i_TableProp[this.ART_TEMP_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"&clvlkey as CALVLKEY, CPROWNUM as CANUMRIG, CACODRIC,CAKEYSAL,CAUNMISU,CAQTAMOV,CAQTAMO1 "," from "+i_cTable;
          +" where 1=0";
          ,.f.,@indexes_QAZQAQVINM)
    this.TMPART_TEMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if UPPER(CP_DBTYPE)="SQLSERVER"
      * --- Devo cambiare collate sul campo lvlkey
      this.w_DIMLVLKEY = "CHAR(200)"
      this.w_GetCollateLvlKey = IIF(varType(g_SQLCollateColLvlKey)="C" And !Empty(g_SQLCollateColLvlKey) , g_SQLCollateColLvlKey, "Latin1_General_BIN")
      this.w_FRASE = "ALTER TABLE " + i_cTempTable + " ALTER COLUMN CALVLKEY " + this.w_DIMLVLKEY + " COLLATE  " + this.w_GetCollateLvlKey
      this.w_iNConn = i_TableProp[ i_nIdx ,3]
      if vartype (g_ACTIVATEPROFILER) = "N" AND g_ACTIVATEPROFILER>0
        this.w_iRows = cp_sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
      else
        this.w_iRows = sqlexec(this.w_iNConn, rtrim(this.w_FRASE))
      endif
    endif
    * --- Popolo la tabella temporanea
    this.w_iRows = INSCRTAB("ArtDist", "TMPART_TEMP")
    USE IN SELECT("ArtDist")
    * --- Carica in memoria i Cicli Semplificati con i costi delle risorse
    * --- La Query Estrae anche le distinte che non hanno ciclo semplificato
    *     altrimenti non veniva aggiornato il Contratto di C/Lavoro
    vq_exec("..\DISB\EXE\QUERY\GSDS4BDC", this, "scheris")
    Select Scheris 
 index on numrig tag numrig 
 index on lvlkey1 tag lvlkey1 collate "MACHINE"
    * --- Drop temporary table TMPART_TEMP
    i_nIdx=cp_GetTableDefIdx('TMPART_TEMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPART_TEMP')
    endif
    Select Scheris 
 scan for !isnull(srcodris) and !isnull(dbcodice)
    this.w_UNIMIS = NVL(UNIMIRIC, SPACE(3))
    this.w_UNMIS1 = NVL(SRUNIMIS, SPACE(3))
    this.w_UNMIS2 = NVL(RIUNMIS2, SPACE(3))
    this.w_UNMIS3 = SPACE(3)
    this.w_OPERAT = NVL(RIOPERAT, SPACE(1))
    this.w_OPERA3 = SPACE(1)
    this.w_MOLTIP = RIMOLTIP
    this.w_MOLTI3 = SPACE(1)
    this.w_RIQTAMOV = cp_ROUND(CSQTAMOV, this.w_DecStampa)
    this.w_VALCONT = RICODVAL
    this.w_PREZZO = RIPREZZO
    this.w_CAO = GETCAM(this.w_VALCONT, i_DATSYS, 0)
    this.w_PRENAZ = IIF(this.w_VALCONT=g_PERVAL, this.w_PREZZO, VAL2MON(this.w_PREZZO,this.w_CAO,g_CAOVAL,i_DATSYS))
    this.w_DPCOSSTA = CALMMLIS(this.w_PRENAZ, this.w_UNMIS1+this.w_UNMIS2+"   "+this.w_UNIMIS+this.w_OPERAT+"  P"+ALLTRIM(STR(this.w_DecStampa)),this.w_MOLTIP,0, 0)
    Select Scheris
    replace SRQTARIS with this.w_RIQTAMOV
    replace DPCOSSTA with this.w_DPCOSSTA
    Select Scheris
    ENDSCAN
    if g_COLA="S"
      this.w_OBTEST = i_datsys
      * --- Carica Contratti e calcola Medio Pond. e Ultimo delle lavorazioni esterne
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      SELECT DISTINCT artcom as cacodric, artcom AS cakeysal, NUMRIG as CPROWNUM from (ElabCursor) ; 
 where propre = "L" and flcomp <> "S" and !empty(coddis) and livello=0 INTO cursor ArtDist nofilter; 
 union; 
 SELECT DISTINCT codcom as cacodric, artcom AS cakeysal, NUMRIG as CPROWNUM from (ElabCursor) ; 
 where propre = "L" and flcomp <> "S" and !empty(coddis) and livello > 0
      this.w_VARIAR = "ArtDist.cacodric"
      this.w_bNoUseRowNum = .F.
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      USE IN SELECT("ArtDist")
      =wrcursor("Scheris") 
 vq_exec("..\COLA\EXE\QUERY\GSCO1SDC", this, "Contratti") 
 vq_exec("..\COLA\EXE\QUERY\GSCO2SDC", this, "Terzisti") 
 vq_exec("..\COLA\EXE\QUERY\GSCO3SDC", this, "Matforn")
      wait wind "" timeout 0.001
      ah_Msg("Aggiornamento costi per gli articoli di Conto Lavoro in corso...",.T.)
      * --- Aggiorna i Costi degli articoli di Conto Lavoro
      Select Scheris 
 scan for arpropre="L"
      if this.w_DELROW and this.w_LVLKEY=LVLKEY1 and this.w_CODFAS=cacodric and this.w_CODDIS=DBCODICE and this.w_CAUNMISU=CAUNMISU and cp_ROUND(this.w_CAQTAMOV, 3)=CAQTAMOV
        * --- Cancella Risorse del Ciclo
        delete
      else
        this.w_LVLKEY = LVLKEY1
        this.w_CODDIS = DBCODICE
        this.w_CODFAS = CACODRIC
        this.w_CODART = CACODART
        this.w_DELROW = FALSE
        * --- Dati che servono per la ricerca del contratto in base a ARPREZUM (anagrafica articolo)
        this.w_PREZUM = nvl(ARPREZUM,space(1))
        this.w_UNMIS1 = nvl(ARUNMIS1,space(3))
        this.w_UNMIS2 = nvl(ARUNMIS2,space(3))
        this.w_OPERAT = nvl(AROPERAT,space(1))
        this.w_MOLTIP = nvl(ARMOLTIP,0)
        this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
        this.w_OPERA3 = NVL(CAOPERAT , "*" )
        this.w_MOLTI3 = NVL(CAMOLTIP, 0)
        * --- Leggo  U.M utilizzata e quantit�
        this.w_CAUNMISU = NVL(CAUNMISU, SPACE(3))
        this.w_CAQTAMOV = NVL(CAQTAMOV, 0)
        this.w_CAQTAMO1 = NVL(CAQTAMO1, 0)
        * --- Effettuo le conversioni
        this.w_QTAUM3 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
        this.w_QTAUM2 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
        * --- Costo Standard o Listino => Contratti di Conto Lavoro
        Select Terzisti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and dbcodice=this.w_coddis
        if found() and not empty(prcodfor)
          * --- Cerca un contratto valido per l'articolo ed il fornitore abituale
          this.w_CODFOR = prcodfor
          * --- Visto e considerato che la distinta base � sempre nella prima unit� di misura
          do case
            case this.w_PREZUM="N"
              * --- Sempre U.M Principale
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR And COUNIMIS=this.w_UNMIS1 And COQUANTI >= this.w_CAQTAMO1
            case this.w_PREZUM="S"
              * --- U.M, Filtrata
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR And COUNIMIS=this.w_CAUNMISU And COQUANTI >= this.w_CAQTAMO1
            otherwise
              * --- U.M Converti
              * --- La cerco nell'unit� di misura presente in distinta
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR and COUNIMIS=this.w_CAUNMISU And COQUANTI >= this.w_CAQTAMOV
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS1
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR and COUNIMIS=this.w_UNMIS1 And COQUANTI >= this.w_CAQTAMO1
              endif
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS2
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR and COUNIMIS=this.w_UNMIS2 And COQUANTI >= this.w_QTAUM2
              endif
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS3
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and cocodclf=this.w_CODFOR and COUNIMIS=this.w_UNMIS3 And COQUANTI >= this.w_QTAUM3
              endif
          endcase
        else
          * --- Cerca un contratto valido per l'articolo
          * --- Visto e considerato che la distinta base � sempre nella prima unit� di misura
          do case
            case this.w_PREZUM="N"
              * --- Sempre U.M Principale
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_UNMIS1 And COQUANTI >= this.w_CAQTAMO1
            case this.w_PREZUM="S"
              * --- U.M, Filtrata
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_CAUNMISU And COQUANTI >= this.w_CAQTAMO1
            otherwise
              * --- U.M Converti
              Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_CAUNMISU And COQUANTI >= this.w_CAQTAMOV
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS1
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_UNMIS1 And COQUANTI >= this.w_CAQTAMO1
              endif
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS2
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_UNMIS2 And COQUANTI >= this.w_QTAUM2
              endif
              if Not Found() and this.w_CAUNMISU<>this.w_UNMIS3
                * --- Non ho trovato il contratto nell'unit� di misura che non � la prima, allora lo cerco nella prima UM
                Select Contratti 
 locate for cacodart=this.w_CODART and cacodric=this.w_CODFAS and COUNIMIS=this.w_UNMIS3 And COQUANTI >= this.w_QTAUM3
              endif
          endcase
          * --- Leggo il fornitore
          this.w_CODFOR = cocodclf
        endif
        if found()
          this.w_CODDIS = DBCODICE
          this.w_CLUNIMIS = COUNIMIS
          this.w_PREZZO = coprezzo
          this.w_VALCONT = cocodval
          if this.w_VALCONT<>g_PERVAL
            * --- Cambio valuta
            this.w_CAO = GETCAM(this.w_VALCONT, i_DATSYS, 0)
            if this.w_CAO<>0
              this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
            else
              ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato","!","")
            endif
          endif
          * --- La quantit� del contratto � sempre uno
          *     Eventualmente la converto
          this.w_SRQTARIS = 1
          if this.w_CLUNIMIS <> this.w_CAUNMISU
            do case
              case this.w_CLUNIMIS = this.w_UNMIS1
              case this.w_CLUNIMIS=this.w_UNMIS2
                if this.w_OPERAT="/"
                  this.w_SRQTARIS = cp_ROUND(this.w_SRQTARIS / this.w_MOLTIP, 5)
                else
                  this.w_SRQTARIS = cp_ROUND(this.w_SRQTARIS * this.w_MOLTIP, 5)
                endif
              case this.w_CLUNIMIS=this.w_UNMIS3
                if this.w_OPERAT="/"
                  this.w_SRQTARIS = cp_ROUND(this.w_SRQTARIS / this.w_MOLTI3, 5)
                else
                  this.w_SRQTARIS = cp_ROUND(this.w_SRQTARIS * this.w_MOLTI3, 5)
                endif
            endcase
          endif
          SELECT Scheris 
 replace dbcodice with this.w_CODDIS 
 replace dpcossta with this.w_PREZZO, srdesope with Contratti.codescon 
 replace srcodris with Contratti.conumero, lfcodfor with this.w_CODFOR 
 replace srunimis with this.w_CLUNIMIS, unimiric with this.w_CLUNIMIS 
 replace srqtaris with this.w_SRQTARIS, srunatan with "N", raggrupp with 3 
 replace srdessch with ah_Msgformat("Contratto C/lavoro: %1",Contratti.conumero)
          this.w_Recno = recno()
          DELETE FOR RAGGRUPP=2 AND LVLKEY1==this.w_LVLKEY
          SELECT Scheris
          GO this.w_Recno
        endif
      endif
      SELECT Scheris 
 ENDSCAN
      if true
        wait wind "" timeout 0.001
        ah_Msg("Segna i materiali forniti dal terzista",.T.)
        * --- Segna i materiali forniti dal terzista
        Select (ElabCursor) 
 Scan
        if livello<=0
          this.w_coddis = iif(not empty(coddis), coddis, dispad)
          this.w_mykey = rtrim(lvlkey)
          this.w_oldkey = this.w_mykey
          this.w_oldkey2 = this.w_oldkey
          this.w_FORNITO = .F.
        else
          * --- Ricerca nei contratti i materiali forniti dal terzista
          this.w_mykey = rtrim(lvlkey)
          this.w_coddis = dispad
          this.w_codiartic1 = artpad
          this.w_codiartico = artcom
          if this.w_FORNITO and this.w_mykey = this.w_oldkey
            * --- Aggiorna anche tutti i discendenti
            replace evasocl with "X"
          else
            this.w_CONUMERO = SPACE(15)
            Select Scheris
            locate for lvlkey1=this.w_oldkey2 and dbcodice=this.w_coddis and cacodart=this.w_codiartic1
            if found()
              this.w_CONUMERO = SCHERIS.SRCODRIS
            endif
            * --- Ricerca nei contratti i materiali forniti dal terzista (salto codice)
            Select Terzisti 
 locate for cacodart=this.w_codiartic1 and dbcodice=this.w_CODDIS
            if found() and not empty(prcodfor)
              * --- Cerca un contratto valido per l'articolo ed il fornitore abituale
              Select Matforn 
 locate for conumero=this.w_conumero and cacodart=this.w_codiartic1 and cocodclf=Terzisti.prcodfor and mccodice=this.w_codiartico and dbcodice=this.w_coddis
            else
              * --- Cerca un contratto valido per l'articolo
              Select Matforn 
 locate for conumero=this.w_conumero and cacodart=this.w_codiartic1 and mccodice=this.w_codiartico and dbcodice=this.w_coddis
            endif
            this.w_FORNITO = found()
            this.w_PREZZO = mccosart
            if this.w_FORNITO
              * --- Segna la riga
              SELECT (ElabCursor)
              replace evasocl with "S", costocl with this.w_PREZZO
              this.w_oldkey = this.w_mykey
            endif
          endif
        endif
        SELECT (ElabCursor)
        ENDSCAN
      endif
      if used("matforn")
        use in matforn
      endif
    endif
    SELECT SCHERIS 
 APPEND BLANK
    * --- Elimino le righe che non servono
    delete from Scheris where Empty(nvl(srcodris,""))
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    Select Scheris
    INDEX ON cacodric TAG cacodric
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costo Ultimo e Medio Ponderato per Articoli esterni - derivato da GSMA_BIN
    * --- Variabili locali inizializzate con il valore dei campi dell'anagrafica
    * --- Vengono inizializzate in modo diverso dal solito perche' devono essere letti i valori dei campi
    * --- dell'anagrafica al momento della chiamata del batch. Questo e' necessario perche' nelle righe
    * --- successive viene richiamato l'evento di salvataggio dell'anagrafica che, dopo aver memorizzato
    * --- i records, ripulisce il contenuto dei campi.
    this.w_INCODESE = this.w_ESERC
    this.w_INESEPRE = this.w_ESERC
    * --- Flag continuo su OFF
    this.w_ESCONT = "N"
    this.w_DECESE = g_PERPUL
    this.w_CAOPRE = this.w_CAOESE
    * --- Variabili locali
    this.w_FlaVal = ""
    this.w_Codice = ""
    this.w_CriVal = ""
    this.w_CodArt = ""
    this.w_PreQtaEsi = 0
    this.w_CodVar = ""
    this.w_PreQtaVen = 0
    this.w_QtaEsi = 0
    this.w_PreQtaAcq = 0
    this.w_QtaVen = 0
    this.w_PrePrzMpa = 0
    this.w_QtaAcq = 0
    this.w_PreCosMpa = 0
    this.w_PreCosMpp = 0
    this.w_PrzMpa = 0
    this.w_PreCosUlt = 0
    this.w_PrzMpp = 0
    this.w_ElenMag = ""
    this.w_PrzUlt = 0
    this.w_RecElab = 0
    this.w_CosMpa = 0
    this.w_PqtMpp = 0
    this.w_CosMpp = 0
    this.w_QtaScagl = 0
    this.w_CqtMpp = 0
    this.w_CpRowNum = 0
    this.w_CosUlt = 0
    this.w_IsDatMov = cp_CharToDate("  -  -  ")
    this.w_CosSta = 0
    this.w_IsDatFin = cp_CharToDate("  -  -  ")
    this.w_CosLco = 0
    this.w_IsQtaEsi = 0
    this.w_CosLsc = 0
    this.w_IsValUni = 0
    this.w_CosFco = 0
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_UltimoArt = ""
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    this.w_TIPOVALOr = ""
    this.w_ValQta = 0
    this.w_TipMagaz = ""
    this.w_ValTip = 0
    this.w_Segnalato = .F.
    this.w_DatReg = cp_CharToDate("  -  -  ")
    this.w_Trec = 0
    this.w_Tart = 0
    this.w_QtaEsiUno = 0
    this.w_QtaVenUno = 0
    this.w_QtaAcqUno = 0
    this.w_QtaEsiWrite = 0
    this.w_QtaVenWrite = 0
    this.w_QtaAcqWrite = 0
    this.w_PrePreUlt = 0
    * --- Variabili relative al raggruppamento fiscale
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_QtaEsr = 0
    this.w_QtaVer = 0
    this.w_QtaAcr = 0
    this.w_QtaVerWrite = 0
    this.w_QtaAcrWrite = 0
    this.w_CARVAL = 0
    this.w_SCAVAL = 0
    this.w_PreCarVal = 0
    this.w_PreScaVal = 0
    ah_Msg("Calcolo valorizzazioni per analisi di dettaglio in corso...",.T.)
    * --- Creazione cursore articoli compresi in inventario
    create cursor AppMov ;
    (CACODART C(20), MMCODMAG C(5), CMCRIVAL C(2), CMFLAVAL C(1), CMAGGVAL C(2), CMFLCASC C(1), ;
    VALORE N(18,5), MMDATREG D(8),MMCODMAT C(5))
    * --- Queste query servono solo per la determinazione dell'ultimo costo (righe 'Z' del cursore di lavoro)
    * --- ================================
    this.w_NEWREC = Repl("Z", 30)
    this.w_ULTVALPRZ = 0
    this.w_ULTVALCOS = 0
    this.w_COSMPAMOV = 0
    this.w_COSMPPMOV = 0
    * --- Select from GSDSIBIN
    do vq_exec with 'GSDSIBIN',this,'_Curs_GSDSIBIN','',.f.,.t.
    if used('_Curs_GSDSIBIN')
      select _Curs_GSDSIBIN
      locate for 1=1
      do while not(eof())
      * --- Al cambio articolo...
      if this.w_NEWREC <>_Curs_GSDSIBIN.MMCODART
        * --- Messaggio a Video
        ah_msg("Elabora ultimo costo/prezzo articolo: %1",.t.,,, ALLTRIM(_Curs_GSDSIBIN.MMCODART) + chr(13) ) 
        if this.w_ULTVALPRZ<>0
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
        endif
        if this.w_ULTVALCOS<>0
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
        endif
        this.w_ULTVALPRZ = 0
        this.w_ULTVALCOS = 0
        * --- Se inventario con calcolo costo medio esercizio per movimento
        *     devo, partendo dall'inventario precedente, calcolare il costo medio
        *     non come somma dei valori di carico / somma delle quantit�
        *     di carico ma movimento per movimento come costo medio attuale per 
        *     esistenza + valore movimento / esistenza + quantit� movimento...
        if this.w_ESCONT="M"
          if this.w_NEWREC<>Repl("Z", 30)
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
          endif
          this.w_CODART = _Curs_GSDSIBIN.MMCODART
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_COSMPAMOV = this.w_PreCosMpa
          this.w_COSMPPMOV = this.w_PrePrzMpa
          this.w_ESIMPAMOV = this.w_PreQtaEsr
        endif
        this.w_NEWREC = _Curs_GSDSIBIN.MMCODART
      endif
      if _Curs_GSDSIBIN.CMFLAVAL$"V|S" And _Curs_GSDSIBIN.CMFLCASC="-" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATPRZ = _Curs_GSDSIBIN.MMDATREG
        this.w_ULTVALPRZ = _Curs_GSDSIBIN.ValUni
        this.w_ULTMAGPRZ = _Curs_GSDSIBIN.MMCODMAG
        this.w_ULTMATPRZ = NVL( _Curs_GSDSIBIN.MMCODMAT , "     " )
        this.w_ULTAGGPRZ = _Curs_GSDSIBIN.CMAGGVAL
      endif
      if _Curs_GSDSIBIN.CMFLAVAL$"A|C" And _Curs_GSDSIBIN.CMFLCASC="+" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATCOS = _Curs_GSDSIBIN.MMDATREG
        this.w_ULTVALCOS = _Curs_GSDSIBIN.ValUni
        this.w_ULTMAGCOS = _Curs_GSDSIBIN.MMCODMAG
        this.w_ULTMATCOS = NVL( _Curs_GSDSIBIN.MMCODMAT , "     " )
        this.w_ULTAGGCOS = _Curs_GSDSIBIN.CMAGGVAL
      endif
      if this.w_ESCONT="M" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        * --- Se costo medio esercizio calcolato sui movimenti possono accadere tre cose:
        *     a) � un movimento di carico
        *     b) � un movimento di scarico
        *     c) � un movimento di storno valore
        *     Nel caso a) ricalcolo il costo medio utilizzando il totalizzatore costo ed esistenze
        *     ed i dati sul movimento andando anche a variare il totalizzatore esistenza
        *     Nel caso b) vario solo il totalizzatore Esistenza mentre nel caso c) ridetermino
        *     il totalizzatore del costo medio ponderato con la formula:
        *     
        *     Analogamente per il prezzo...
        *     (totalizzatore costo medio * totalizzatore esistenza - variazione a valore )/esistenza
        do case
          case _Curs_GSDSIBIN.CMFLAVAL$"A|C"
            if _Curs_GSDSIBIN.CMFLCASC$"X"
              * --- Variazione di valore...
              this.w_COSMPAMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPAMOV = (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ ( _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPAMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_GSDSIBIN.Esistenza
            endif
          case _Curs_GSDSIBIN.CMFLAVAL$"V|S"
            if _Curs_GSDSIBIN.CMFLCASC$"X"
              this.w_COSMPPMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPPMOV = (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ ( _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPPMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_GSDSIBIN.Esistenza
            endif
        endcase
      endif
        select _Curs_GSDSIBIN
        continue
      enddo
      use
    endif
    if this.w_ULTVALPRZ<>0 And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
    endif
    if this.w_ULTVALCOS<>0 And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
    endif
    if this.w_ESCONT="M" And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
    endif
    * --- =======================================
    * --- 1) La select seguente crea piu' records di totali per ogni articolo distinti per Tipo Movimento e Criterio di Valorizzazione.
    *      2) Rispetto alla visual query raggruppa record con le stesse caratteristiche che erano rimasti separati perche' provenienti
    *           da archivi differenti (TipoRiga: A=Articoli, M=Mov.Magazzino, V=Vendite).
    *      3) Per le righe di tipo articolo (TipoRiga=A) la data di registrazione non deve essere considerata.
    vq_exec("..\DISB\EXE\QUERY\GSDS1BIN",this, "elabinve1")
     select cacodart, mmcodmag, "A" as Ordine, ; 
 max(cp_CharToDate("  -  -  ")) as mmdatreg, ; 
 sum(iif(cauprival $ "AC", MagPriCar-MagPriSca, MagPriCar*0)) as Acquisti, ; 
 sum(iif(cauprival $ "VS", MagPriSca-MagPriCar, MagPriSca*0)) as Vendite, ; 
 sum(iif(cauprival $ "AC", Valore, Valore*0)) as ValAcqu, ; 
 sum(iif(cauprival $ "VS", Valore, Valore*0)) as ValVend, ; 
 CauPriVal as CauPriVal, ; 
 CauAggVal as CauAggVal, ; 
 CauPriEsi as CauPriEsi, ; 
 sum(999999999999.99999*0) as Valore , nvl(mmcodmat, space(5)) as mmcodmat, ; 
 sum(iif(cauprival $ "AC", MAGPRICAV-MAGPRISCV, MAGPRICAV*0)) as CARVAL, ; 
 sum(iif(cauprival $ "VS", MAGPRISCV-MAGPRICAV, MAGPRISCV*0)) as SCAVAL, ; 
 LEFT(cacodart+SPACE(20), 20)+"####################" AS cakeysal; 
 from elabinve1 ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi, mmcodmat ; 
 union ; 
 select cacodart, mmcodmag, "Z" as Ordine, ; 
 mmdatreg as mmdatreg, ; 
 0 as Acquisti, ; 
 0 as Vendite, ; 
 0 as ValAcqu, ; 
 0 as ValVend, ; 
 CMFLAVAL as CauPriVal , ; 
 CMAGGVAL as CauAggVal , ; 
 CMFLCASC as CauPriEsi, ; 
 VALORE as Valore , MMCODMAT as mmcodmat, ; 
 0 as CARVAL, ; 
 0 as SCAVAL, ; 
 LEFT(cacodart+SPACE(20), 20)+"####################" AS cakeysal; 
 from AppMov ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi,mmcodmat ; 
 order by 1,2,3 ; 
 into cursor elabinve 
 use in elabinve1 
 use in Appmov
    * --- Imposta il codice dell'ultimo articolo elaborato
    this.w_UltimoArt = "<BOF>"
    * --- Imposta le data fino alle quali sono stati considerati i movimenti di prezzo e costo
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    do while Not Eof("elabinve")
      * --- Lettura dati record corrente
      this.w_Codice = elabinve.cakeysal
      * --- Cambio articolo
      if this.w_Codice <> this.w_UltimoArt
        * --- Messaggio a Video
        * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
        *     ATTENZIONE: la pag.9 viene eseguita sull'art. precedente (ovvero DOPO che sono state eseguite le valorizz. di periodo a pag.5)!!
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Azzeramento totalizzatori articolo
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_QtaEsi = 0
        this.w_PreQtaAcq = 0
        this.w_QtaVen = 0
        this.w_PrePrzMpa = 0
        this.w_QtaAcq = 0
        this.w_PreCosMpa = 0
        this.w_PreCosMpp = 0
        this.w_PrzMpa = 0
        this.w_PreCosUlt = 0
        this.w_PrzMpp = 0
        this.w_ElenMag = ""
        this.w_PrzUlt = 0
        this.w_RecElab = 0
        this.w_CosMpa = 0
        this.w_PqtMpp = 0
        this.w_CosMpp = 0
        this.w_QtaScagl = 0
        this.w_CqtMpp = 0
        this.w_CpRowNum = 0
        this.w_CosUlt = 0
        this.w_IsDatMov = cp_CharToDate("  -  -  ")
        this.w_CosSta = 0
        this.w_IsDatFin = cp_CharToDate("  -  -  ")
        this.w_CosLco = 0
        this.w_IsQtaEsi = 0
        this.w_CosLsc = 0
        this.w_IsValUni = 0
        this.w_CosFco = 0
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_UltimoArt = ""
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_TIPOVALOr = ""
        this.w_ValQta = 0
        this.w_TipMagaz = ""
        this.w_ValTip = 0
        this.w_Segnalato = .F.
        this.w_DatReg = cp_CharToDate("  -  -  ")
        this.w_Trec = 0
        this.w_QtaEsiUno = 0
        this.w_PrePreUlt = 0
        this.w_QtaVenUno = 0
        this.w_PreQtaEsr = 0
        this.w_QtaAcqUno = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_QtaEsiWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaVenWrite = 0
        this.w_QtaVer = 0
        this.w_QtaAcqWrite = 0
        this.w_QtaAcr = 0
        this.w_QtaVerWrite = 0
        this.w_QtaAcrWrite = 0
        this.w_Vendite = 0
        this.w_Acquisti = 0
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_PreQtaAcq = 0
        this.w_PreQtaEsr = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_PrePrzMpa = 0
        this.w_PreCosMpa = 0
        this.w_PreCosMpp = 0
        this.w_PreCosUlt = 0
        this.w_PrePreUlt = 0
        this.w_PrzMpaWrite = 0
        this.w_CosMpaWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaEsi = 0
        this.w_PrzUlt = 0
        this.w_QtaEsiUno = 0
        this.w_QtaVer = 0
        this.w_QtaVerWrite = 0
        this.w_QtaVen = 0
        this.w_CosMpa = 0
        this.w_QtaVenUno = 0
        this.w_QtaAcr = 0
        this.w_QtaAcrWrite = 0
        this.w_QtaAcq = 0
        this.w_CosMpp = 0
        this.w_QtaAcqUno = 0
        this.w_PrzMpa = 0
        this.w_CosUlt = 0
        this.w_PqtMpp = 0
        this.w_PrzMpp = 0
        this.w_CosSta = 0
        this.w_CqtMpp = 0
        * --- Preparo i buffer relativi ad acquisti e vendite, (aggiornati, se del caso, a pag.5) da utilizzare in pag.7
        this.w_Vendite = 0
        this.w_Acquisti = 0
        * --- Memorizza il codice dell'ultimo articolo elaborato
        this.w_UltimoArt = this.w_Codice
        Select Elabinve
        this.w_CodArt = elabinve.cacodart
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_Segnalato = .F.
        this.w_VarValoAcq = 0
        this.w_VarValoVen = 0
      endif
      * --- Il seguente assegnamento va messo fuori dall'if, altrimenti non viene aggiornato quando cambia il magazzino, ma non cambia l'art
      this.w_Maga = NVL(elabinve.mmcodmag,space(5))
      * --- Estraggo il raggruppamento fiscale del magazzino collegato (quello del maga principale � in w_MAGRAG)
      if NOT EMPTY(NVL(elabinve.mmcodmat,"")) AND((elabinve.mmcodmat $ this.w_ElenMag AND elabinve.CauAggVal="SS" ) OR elabinve.CauAggVal="NN" OR ((NOT elabinve.mmcodmat $ this.w_ElenMag) AND elabinve.CauAggVal="NS" ))
        * --- Aggiorna solo Esistenza
        this.w_QtaEsi = this.w_QtaEsi + (elabinve.Acquisti - elabinve.Vendite)
        this.w_QtaEsr = this.w_QtaEsr + (elabinve.Acquisti - elabinve.Vendite)
        if this.w_Maga=this.w_INCODMAG
          this.w_QtaEsiUno = this.w_QtaEsiUno + (elabinve.Acquisti - elabinve.Vendite)
        endif
      else
        * --- Calcolo Valorizzazioni Periodo
        * --- La procedura richiama questa pagina piu' volte per ogni articolo e calcola tutte le valorizzazioni di periodo.
        select elabinve
        * --- Inizializzazione dei parametri di selezione delle visual query richiamate in questa pagina
        this.w_DatReg = mmdatreg
        * --- Quantita'
        * --- Esistenza
        this.w_QtaEsi = this.w_QtaEsi + (Acquisti - Vendite)
        this.w_QtaEsr = this.w_QtaEsr + (Acquisti - Vendite)
        * --- Quantita' Venduta
        * --- CauPriVal = Combo valore da Aggiornare sulle causali di magazzino
        *     V:  Venduto
        *     S: Altri Scarichi
        *     A: Acquistato
        *     C: Altri Carichi
        *     
        *     CauAggVal = Check Aggiornamento Valori
        *     S: Aggiorna valori 
        *     N: Non aggiorna valori
        *     la prima lettera riguarda il magazzino principale la seconda quello collegato
        *     Es: 
        *     'SS' entrambi i magazzini aggiornano i valori
        *     'SN' il primo magazzino aggiorna, quello collegato No
        if CauPriVal$"V|S" AND Vendite<>0
          this.w_QtaVen = this.w_QtaVen + Vendite
          this.w_QtaVerWrite = this.w_QtaVerWrite + Vendite
          if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
            this.w_QtaVer = this.w_QtaVer + Vendite
            * --- Solo se Altri Scarichi/Venduto e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata),  aggiorno il campo Scarichi valorizzati
            this.w_SCAVAL = this.w_SCAVAL + SCAVAL
          endif
        endif
        * --- Quantita' Acquistata
        if CauPriVal$"A|C" AND Acquisti<>0
          this.w_QtaAcq = this.w_QtaAcq + Acquisti
          this.w_QtaAcrWrite = this.w_QtaAcrWrite + Acquisti
          if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
            this.w_QtaAcr = this.w_QtaAcr + Acquisti
            * --- Solo se Altri Carichi/Acquistato e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata), aggiorno il campo Carichi valorizzati
            this.w_CARVAL = this.w_CARVAL + CARVAL
          endif
        endif
        if this.w_Maga=this.w_INCODMAG
          * --- Salvo i dati parziali relativi al solo magazzino specificato
          * --- Esistenza
          this.w_QtaEsiUno = this.w_QtaEsiUno + (Acquisti - Vendite)
          * --- Quantita' Venduta
          if CauPriVal$"V|S" AND Vendite<>0
            this.w_QtaVenUno = this.w_QtaVenUno + Vendite
          endif
          * --- Quantita' Acquistata
          if CauPriVal$"A|C" AND Acquisti<>0
            this.w_QtaAcqUno = this.w_QtaAcqUno + Acquisti
          endif
        endif
        * --- VALORIZZAZIONI
        this.w_VarValoAcq = 0
        this.w_VarValoVen = 0
        * --- La valorizzazione NON viene effettuata solo nel caso in cui
        * --- 1) esista una causale collegata (movimento di trasferimento);
        * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
        * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
        if (CauAggVal="SN" OR CauAggVal="NS") OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
          * --- Prezzi
          * --- Prezzo Medio Ponderato Periodo
          * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
          * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
          if CauPriVal$"V|S" AND NVL(Vendite,0)<>0
            this.w_PrzMpp = this.w_PrzMpp + (ValVend * IIF(Vendite<0, -1, 1))
            this.w_PqtMpp = this.w_PqtMpp + Vendite
          endif
          * --- Costi
          * --- Costo Medio Ponderato Periodo
          * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
          * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
          if CauPriVal$"A|C" AND NVL(Acquisti,0)<>0
            this.w_CosMpp = this.w_CosMpp + (ValAcqu * IIF(Acquisti<0, -1, 1))
            this.w_CqtMpp = this.w_CqtMpp + Acquisti
          endif
          if nvl(Ordine," ")="Z"
            * --- Calcola Ultimo Prezzo/Ultimo Costo
            * --- Verifica se deve eseguire la valorizzazione a Ult.Prezzo o Ult.Costo o Costo Standard
            if this.w_DatReg>this.w_DataPrzUlt AND CauPriVal$"V|S" AND nvl(Valore, 0)<>0 and CauPriEsi="-"
              * --- Ultimo Prezzo
              this.w_PrzUlt = NVL(Valore, 0)
              this.w_DataPrzUlt = this.w_DatReg
            endif
            if this.w_DatReg>this.w_DataCosUlt AND CauPriVal$"A|C" AND CauPriEsi<>"-" AND nvl(Valore, 0)<>0 and CauPriEsi="+"
              * --- Ultimo Costo
              * --- Considera i movimenti dall'inizio del periodo
              * --- ATTENZIONE! Se e' zero prende quello dell'inventario precedente nella pagina delle valorizzazioni d'esercizio.
              this.w_CosUlt = NVL(Valore, 0)
              this.w_DataCosUlt = this.w_DatReg
            endif
          else
            * --- Bufferizzo i dati relativi ad acquisti e vendite, da utilizzare in pag.7
            this.w_Vendite = elabinve.vendite+this.w_Vendite
            this.w_Acquisti = elabinve.acquisti+this.w_Acquisti
          endif
        endif
      endif
      * --- Passaggio al record successivo
      if Not Eof ("elabinve")
        Select elabinve
        Skip
      endif
    enddo
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select Przinve 
 Append blank
    if used("elabinve")
      use in elabinve
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valori dell'Esercizio di partenza e aggiornamento cursore
    * --- Calcolo Valorizzazioni Esercizio
    * --- La procedura richiama questa pagina una sola volte per ogni articolo e calcola tutte le valorizzazioni d'esercizio.
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    * --- Variabili relative all'inventario precedente
    this.w_PreQtaEsi = 0
    this.w_PreQtaVen = 0
    this.w_PreQtaAcq = 0
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_PrePrzMpa = 0
    this.w_PreCosMpa = 0
    this.w_PreCosMpp = 0
    this.w_PreCosUlt = 0
    this.w_PrePreUlt = 0
    this.w_PrzMpaWrite = 0
    this.w_CosMpaWrite = 0
    this.w_PreCarVal = 0
    this.w_PreScaVal = 0
    if this.w_UltimoArt <> "<BOF>"
      * --- Legge le valorizzazioni dell'inventario precedente
      if NOT EMPTY(this.w_INNUMPRE)
        select przinve
        go top
        if reccount() > 0
          locate for cakeysal=this.w_UltimoArt
          if found()
            this.w_PreQtaEsi = DIQTAESI
            this.w_PreQtaEsr = DIQTAESR
            if this.w_INCODESE=this.w_INESEPRE
              * --- Vengono estratte le qta totali di raggruppamento
              * --- per eseguire correttamente i calcoli a partire da un inventario precedente
              this.w_PreQtaVen = DIQTAVEN
              this.w_PreQtaAcq = DIQTAACQ
              this.w_PreQtaVer = DIQTAVER
              this.w_PreQtaAcr = DIQTAACR
              this.w_PreCarVal = DIQTACVR 
              this.w_PreScaVal = DIQTASVR
            endif
            this.w_PrePrzMpa = DIPRZMPA
            this.w_PreCosMpa = DICOSMPA
            this.w_PreCosMpp = DICOSMPP
            this.w_PreCosUlt = DICOSULT
            this.w_PrePreUlt = DIPRZULT
            if this.w_CAOPRE<>this.w_CAOESE
              * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
              this.w_PrePrzMpa = cp_ROUND(VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosMpa = cp_ROUND(VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosMpp = cp_ROUND(VAL2MON(this.w_PreCosMpp,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosUlt = cp_ROUND(VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PrePreUlt = cp_ROUND(VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
            endif
          endif
        endif
        select elabinve
      endif
      * --- Quantita'
      * --- Esistenza
      * --- L'esistenza e' data dalla somma della quantita' presente nel periodo precedente con la differenza fra
      * --- gli acquisti e le vendite del periodo (calcolata nella pagina delle valorizzazioni di periodo).
      this.w_QtaEsi = this.w_PreQtaEsi + this.w_QtaEsi
      this.w_QtaEsr = this.w_PreQtaEsr+this.w_QtaEsr
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaEsiUno = this.w_PreQtaEsi + this.w_QtaEsiUno
      endif
      * --- VALORIZZAZIONI
      * --- Cerco eventuali variazioni a valore
      * --- La valorizzazione NON viene effettuata solo nel caso in cui
      * --- 1) esista una causale collegata (movimento di trasferimento);
      * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
      * --- (in quel caso non calcolo neanche gli scaglioni)
      * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      this.w_PrzMpp = iif( empty( this.w_PqtMpp ) , 0 , ( this.w_PrzMpp + this.w_VarValoVen )/ this.w_PqtMpp )
      * --- Prezzo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente corrisponde al valore di periodo.
      if this.w_QtaVer+this.w_PreQtaVer<>0
        * --- w_PqtMpp e w_Qtaver contengono il solito valore...
        this.w_PrzMpa = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaVer)) / (this.w_QtaVer+this.w_PreQtaVer)
      else
        * --- Se la somma delle quantit� � zero due casi
        *     a) entrambe le qt� sono a zero come prezzo medio utilizzo quello
        *     dell'esercizio precedente
        *     b) le quantit� si annullano, prezzo medio a zero...
        if this.w_PreQtaVer=0
          this.w_PrzMpa = this.w_PrePrzMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_PrzMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          * --- Vedi calcolo sopra...
          if this.w_QtaVer+this.w_PreQtaEsr<>0
            this.w_PrzMpaWrite = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaEsr)) / (this.w_QtaVer+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_PrzMpaWrite = this.w_PrePrzMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_PrzMpaWrite = 0
            endif
          endif
        case this.w_ESCONT$"NM"
          this.w_PrzMpaWrite = this.w_PrzMpa
      endcase
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      * --- Devo sapere se nel periodo ho avuto movimenti "validi" o meno.
      *     Se li ho avuti allora l'eventuale presenza di variazioni di valore
      *     non vanno ri applicate al dato proveniente dall'esercizio precedente,
      *     altrimenti ( se non ho movimenti validi ) devo applicarle (quindi nel periodo
      *     ho solo la variazione a valore)
      *     - CASO LIMITE NON GESTITO SOLA VARIAZIONE A VALORE 
      *     SENZA SALDI ARTICOLI, L'ARTICOLO NON SAREBBE PRESENTE 
      *     NELL'ELABORAZIONE INVENTARIO E QUINDI NON ENTREREBBE
      *     NELLA SCAN IN CUI SIAMO
      this.w_Old_CosMpp = iif( this.w_CqtMpp=0 , 0 ,this.w_CosMpp )
      this.w_CosMpp = iif( this.w_CqtMpp=0 , 0 , ( this.w_CosMpp + this.w_VarValoAcq ) / this.w_CqtMpp )
      * --- Costo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente o non � dello stesso Esercizio corrisponde al valore di periodo.
      if this.w_QtaAcr+this.w_PreCarVal<>0
        * --- Se il valore del periodo � zero devo comunque considerare le variazioni a valore del periodo
        this.w_CosMpa = (iif( this.w_Old_CosMpp=0 , this.w_VarValoAcq , (this.w_CosMpp*this.w_QtaAcr)) + (this.w_PreCosMpa*this.w_PreCarVal)) / (this.w_QtaAcr+this.w_PreCarVal)
      else
        if this.w_PreQtaAcr=0 or (this.w_ESCONT="N" and this.w_PreCosMpa<>0)
          * --- Nel caso in cui nell'inventario di riferimento (stesso esercizio dell'inventario da elaborare)
          *     il tot. carichi sia valorizzato ma � una rettifica d'acquisto (mvprezzo a 0) 
          *     l'inventario da elaborare prende sempre il costo Medio ponderato dell'esercizio precedente
          this.w_CosMpa = this.w_PreCosMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_CosMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          if this.w_QtaAcr+ this.w_PreQtaEsr<>0
            this.w_CosMpaWrite = ((this.w_CosMpp*this.w_QtaAcr) + (this.w_PreCosMpa*this.w_PreQtaEsr)) / (this.w_QtaAcr+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_CosMpaWrite = this.w_PreCosMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_CosMpaWrite = 0
            endif
          endif
        case this.w_ESCONT="N"
          this.w_CosMpaWrite = this.w_CosMpa
        case this.w_ESCONT="M"
          * --- Se ho trovato tra i record del temporaneo una entrata con il costo
          *     calcolato sui movimenti lo utilizzo, altrimenti (articolo non movimentato
          *     nel periodo) riporto il prezzo presente nell'eventuale inventario 
          *     precedente, altrimenti 0...
          if w_bMovCosMpaWrite
            this.w_CosMpaWrite = w_MovCosMpaWrite
          else
            this.w_CosMpaWrite = this.w_PreCosMpa
          endif
      endcase
      * --- Ultimo Costo
      * --- Se e' zero l'ultimo costo del periodo prende quello dell'inventario precedente
      if empty( this.w_CosUlt ) .and. (.not. empty(this.w_PreCosUlt))
        this.w_CosUlt = this.w_PreCosUlt
      endif
      * --- Ultimo Prezzo
      * --- Se e' zero l'ultimo prezzo del periodo prende quello dell'inventario precedente
      if empty( this.w_PrzUlt ) .and. (.not. empty(this.w_PrePreUlt))
        this.w_PrzUlt = this.w_PrePreUlt
      endif
      * --- OLD:  IF NOT EMPTY(w_INNUMPRE) AND w_INESEPRE=w_INCODESE
      this.w_QtaAcq = this.w_QtaAcq + this.w_PreQtaAcq
      this.w_QtaVen = this.w_QtaVen + this.w_PreQtaVen
      this.w_QtaAcr = this.w_QtaAcr + this.w_PreQtaAcr
      this.w_QtaVer = this.w_QtaVer + this.w_PreQtaVer
      this.w_QtaAcrWrite = this.w_QtaAcrWrite + this.w_PreQtaAcr
      this.w_QtaVerWrite = this.w_QtaVerWrite + this.w_PreQtaVer
      this.w_CARVAL = this.w_CARVAL + this.w_PreCarVal
      this.w_SCAVAL = this.w_SCAVAL + this.w_PreScaVal
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaAcqUno = this.w_QtaAcqUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaAcq)
        this.w_QtaVenUno = this.w_QtaVenUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaVen)
      endif
    endif
    * --- Scrittura archivi inventario - GSMA_BIN - PAG6
    if this.w_UltimoArt <> "<BOF>"
      * --- Arrotondamenti in base ai decimali della valuta di conto
      this.w_CosMpaWrite = cp_round( this.w_CosMpaWrite , this.w_DECESE )
      if this.w_TipoValo="P"
        this.w_CosMpaWrite = cp_round( this.w_CosMpp , this.w_DECESE )
      endif
      this.w_CosUlt = cp_round( this.w_CosUlt , this.w_DECESE )
      select przinve
      locate for cakeysal=this.w_UltimoArt
      if found()
        replace dicosmpa with this.w_CosMpaWrite, dicosult with this.w_CosUlt
      else
        Insert into przinve (cakeysal, dicosmpa, dicosult) ; 
 values ; 
 (this.w_UltimoArt, this.w_CosMpaWrite, this.w_CosUlt)
      endif
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura ricorsiva di implosione distinte (Ripresa da GSDB_BIM).
    this.w_CI = 0
    * --- Metto in un cursore tutti gli articoli presenti in DIS_STOR in considerazione dei filtri inseriti sulla maschera GSDB_KDC.
    if w_ZFILTERS And !Empty(w_OUNOMQUE)
      vq_exec("..\DISB\EXE\QUERY\GSDS4KCS", this, "selezione")
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      vq_exec("..\DISB\EXE\QUERY\GSDS_KCS", this, "selezione")
    endif
    this.w_RECSEL = reccount("selezione")
    * --- Il numero massimo di distinte controllabili dall'implosione � 9.999.999
    if this.w_RECSEL>0 and this.w_RECSEL<10000000
      select selezione 
 go top 
 scan for PROCESSA="N"
      this.w_LIVELLO = selezione.lvlkey
      * --- Calcolo il livello della distinta
      if empty(this.w_LIVELLO)
        this.w_CI = this.w_CI + 1
        this.w_LIVELLO = right("000000"+alltrim(str(this.w_CI)),7)
        replace lvlkey with this.w_LIVELLO
      endif
      * --- per ahr devo prendere codice articolo e filtrare su dbartcom in distbase
      this.w_CODDIS = selezione.arcodart
      riga=recno()
      ah_msg("Analisi componente %1 in corso",.t.,,,alltrim(this.w_coddis))
      vq_exec("..\DISB\EXE\QUERY\GSDS2KCS", this, "implosione")
      * --- Il numero massimo di livelli di implosione considerabili � 30: (250+1/8)=31, uno lo elimino per sicurezza
      if reccount("implosione")>0
        this.w_CJ = 0
        select implosione 
 go top 
 scan
        this.w_CJ = this.w_CJ + 1
        replace lvlkey with (alltrim(this.w_LIVELLO)+"."+right("000000"+alltrim(str(this.w_CJ)),7))
        scatter memvar 
 m.livellod=occurs(".",m.lvlkey) 
 insert into selezione from memvar 
 endscan
        if m.livellod+1>30
          * --- Superato il limite gestito - potrebbe anche esserci un loop.
          ah_ErrorMsg("Ci potrebbe essere un loop sul codice distinta %1 %0Oppure tale distinta potrebbe superare il numero di sottolivelli massimo gestito (30)",48,"",alltrim(this.w_CODDIS))
          i_retcode = 'stop'
          return
        endif
        select selezione 
 go riga
        * --- se implosione contiene almeno un record significa che questo non � un livello finale,scrivo 'N' su PSUPREMO e 
        *     segno la riga processata mettendo 'S' a PROCESSA
        replace PSUPREMO with "N", PROCESSA with "S"
      else
        select selezione 
 go riga
        * --- questo � un livello finale, lascio 'S' su PSUPREMO e segno la riga processata mettendo 'S' a PROCESSA
        replace PROCESSA with "S"
      endif
      endscan
      * --- Cancello i record dove PSUPREMO<>'S'
      delete for PSUPREMO<>"S"
      * --- Preparo il cursore di input a BSDB_BTV
      if w_ZFILTERS And !Empty(w_OUNOMQUE)
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
        SELECT DISTINCT dbcodart as cacodart from InCursor INTO cursor ArtDist nofilter
        this.w_VARIAR = "ArtDist.cacodart"
        * --- Esplodo i nodi
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        vq_exec("..\DISB\EXE\QUERY\GSDS7KCS", this, "selezione1")
        if used("ArtDist")
          use in ArtDist
        endif
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        vq_exec("..\DISB\EXE\QUERY\GSDS1KCS", this, "selezione1")
      endif
      SELECT * FROM SELEZIONE UNION SELECT * FROM SELEZIONE1 INTO CURSOR ELABREC
    else
      if this.w_RECSEL>9999999
        ah_ErrorMsg("Il numero massimo di distinte verificabili nella selezione (9.999.999) � stato superato")
        i_retcode = 'stop'
        return
      else
        SELECT DISTINCT * from InCursor INTO cursor ELABREC nofilter
        USE IN SELECT("incursor")
        * --- Potrebbero non esistere dati da aggiornare per la selezione effettuata, ma � anche possibile che tutti gli articoli dell'intervallo selezionato siano livelli finali.
        *     In questo caso non passo a GSAR_BDE un cursore ma direttamente l'intervallo di selezione.
        this.w_LIVFIN = "S"
      endif
    endif
    if used("Selezione")
      USE IN "Selezione"
    endif
    if used("Implosione")
      USE IN "Implosione"
    endif
    if used("Selezione1")
      USE IN "Selezione1"
    endif
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura dati inventario precedente
    if NOT EMPTY(this.w_INNUMPRE)
      * --- Read from INVEDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVEDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR"+;
          " from "+i_cTable+" INVEDETT where ";
              +"DINUMINV = "+cp_ToStrODBC(this.w_INNUMPRE);
              +" and DICODESE = "+cp_ToStrODBC(this.w_INESEPRE );
              +" and DICODICE = "+cp_ToStrODBC(this.w_CODART );
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR;
          from (i_cTable) where;
              DINUMINV = this.w_INNUMPRE;
              and DICODESE = this.w_INESEPRE ;
              and DICODICE = this.w_CODART ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PreQtaEsi = NVL(cp_ToDate(_read_.DIQTAESI),cp_NullValue(_read_.DIQTAESI))
        this.w_PreQtaEsr = NVL(cp_ToDate(_read_.DIQTAESR),cp_NullValue(_read_.DIQTAESR))
        this.w_PrePrzMpa = NVL(cp_ToDate(_read_.DIPRZMPA),cp_NullValue(_read_.DIPRZMPA))
        this.w_PreCosMpa = NVL(cp_ToDate(_read_.DICOSMPA),cp_NullValue(_read_.DICOSMPA))
        this.w_PreCosUlt = NVL(cp_ToDate(_read_.DICOSULT),cp_NullValue(_read_.DICOSULT))
        this.w_PrePreUlt = NVL(cp_ToDate(_read_.DIPRZULT),cp_NullValue(_read_.DIPRZULT))
        this.w_PreQtaVen = NVL(cp_ToDate(_read_.DIQTAVEN),cp_NullValue(_read_.DIQTAVEN))
        this.w_PreQtaAcq = NVL(cp_ToDate(_read_.DIQTAACQ),cp_NullValue(_read_.DIQTAACQ))
        this.w_PreQtaVer = NVL(cp_ToDate(_read_.DIQTAVER),cp_NullValue(_read_.DIQTAVER))
        this.w_PreQtaAcr = NVL(cp_ToDate(_read_.DIQTAACR),cp_NullValue(_read_.DIQTAACR))
        this.w_PreCarVal = NVL(cp_ToDate(_read_.DIQTACVR),cp_NullValue(_read_.DIQTACVR))
        this.w_PreScaVal = NVL(cp_ToDate(_read_.DIQTASVR),cp_NullValue(_read_.DIQTASVR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_INCODESE<>this.w_INESEPRE
        * --- Se esercizi diversi i totalizzatori per esercizio li azzero...
        this.w_PreQtaVen = 0
        this.w_PreQtaAcq = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_PreCarVal = 0
        this.w_PreScaVal = 0
      endif
      if this.w_CAOPRE<>this.w_CAOESE
        * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
        this.w_PrePrzMpa = VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosMpa = VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosUlt = VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PrePreUlt = VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM1,w_cCursor)
    this.w_PARAM1=w_PARAM1
    this.w_cCursor=w_cCursor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='PAR_RIOR'
    this.cWorkTables[2]='PAR_RIMA'
    this.cWorkTables[3]='SALDIART'
    this.cWorkTables[4]='ART_TEMP'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='INVENTAR'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='*TMPART_TEMP'
    this.cWorkTables[10]='INVEDETT'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSDSIBIN')
      use in _Curs_GSDSIBIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM1,w_cCursor"
endproc
