* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_spp                                                        *
*              Stampa piano di produzione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_100]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-23                                                      *
* Last revis.: 2016-01-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_spp",oParentObject))

* --- Class definition
define class tgsds_spp as StdForm
  Top    = 28
  Left   = 33

  * --- Standard Properties
  Width  = 575
  Height = 218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-01-05"
  HelpContextID=127310743
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsds_spp"
  cComment = "Stampa piano di produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_NUMREG = 0
  w_ALFREG = space(2)
  w_DATREG = ctod('  /  /  ')
  w_DATFIL = ctod('  /  /  ')
  w_DTOBS1 = ctod('  /  /  ')
  w_CODMAG = space(5)
  w_DATSTA = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  w_DISB = space(20)
  w_DESMAG = space(30)
  w_DESCRI = space(40)
  w_SERPIA = space(10)
  o_SERPIA = space(10)
  w_COMDIS = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_sppPag1","gsds_spp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMREG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='ART_ICOL'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsds_spp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMREG=0
      .w_ALFREG=space(2)
      .w_DATREG=ctod("  /  /  ")
      .w_DATFIL=ctod("  /  /  ")
      .w_DTOBS1=ctod("  /  /  ")
      .w_CODMAG=space(5)
      .w_DATSTA=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_DISB=space(20)
      .w_DESMAG=space(30)
      .w_DESCRI=space(40)
      .w_SERPIA=space(10)
      .w_COMDIS=space(1)
          .DoRTCalc(1,3,.f.)
        .w_DATFIL = CP_TODATE(.w_DATREG)
          .DoRTCalc(5,5,.f.)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODMAG))
          .link_1_6('Full')
        endif
        .w_DATSTA = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_OBTEST = i_datsys
          .DoRTCalc(9,14,.f.)
        .w_COMDIS = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_SERPIA<>.w_SERPIA
            .w_DATFIL = CP_TODATE(.w_DATREG)
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMDIS_1_25.visible=!this.oPgFrm.Page1.oPag.oCOMDIS_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsds_spp
    If Cevent='Stampa'
       Do GSDS_BPP with This
       This.w_NUMREG=0
       This.w_ALFREG=' '
       This.w_DATREG={}
       This.w_DATFIL={}
       This.w_DESCRI=' '
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_6'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_1.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_1.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oALFREG_1_2.value==this.w_ALFREG)
      this.oPgFrm.Page1.oPag.oALFREG_1_2.value=this.w_ALFREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIL_1_4.value==this.w_DATFIL)
      this.oPgFrm.Page1.oPag.oDATFIL_1_4.value=this.w_DATFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_6.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_6.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_7.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_7.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_18.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_18.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_22.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_22.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMDIS_1_25.RadioValue()==this.w_COMDIS)
      this.oPgFrm.Page1.oPag.oCOMDIS_1_25.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERPIA = this.w_SERPIA
    return

enddefine

* --- Define pages as container
define class tgsds_sppPag1 as StdContainer
  Width  = 571
  height = 218
  stdWidth  = 571
  stdheight = 218
  resizeXpos=395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMREG_1_1 as StdField with uid="LLZMCHIINR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero piano di produzione",;
    HelpContextID = 214055466,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=108, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  add object oALFREG_1_2 as StdField with uid="TUGJPFZHSI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ALFREG", cQueryName = "ALFREG",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie del piano di produzione",;
    HelpContextID = 214086650,;
   bGlobalFont=.t.,;
    Height=21, Width=35, Left=210, Top=9, InputMask=replicate('X',2)

  add object oDATFIL_1_4 as StdField with uid="LVRDSSSORH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIL", cQueryName = "DATFIL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del piano di produzione",;
    HelpContextID = 126738122,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=284, Top=9

  add object oCODMAG_1_6 as StdField with uid="XMPGNCFZCF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli da verificare (spazio=verifica per tutti i magazzini)",;
    HelpContextID = 218616026,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=108, Top=69, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDATSTA_1_7 as StdField with uid="WHTHOLGNAQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di elaborazione della stampa",;
    HelpContextID = 30465738,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=108, Top=99


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=140, width=452,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 231562522


  add object oBtn_1_9 as StdButton with uid="TYEVPKXNEQ",left=461, top=167, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 267770586;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      this.parent.oContained.NotifyEvent("Stampa")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERPIA))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="BSJPZGRESF",left=512, top=167, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 134628166;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAG_1_18 as StdField with uid="IGDPZQPCGC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 218557130,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=170, Top=69, InputMask=replicate('X',30)

  add object oDESCRI_1_22 as StdField with uid="WLFLFEARSG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del piano di produzione",;
    HelpContextID = 167832266,;
   bGlobalFont=.t.,;
    Height=21, Width=375, Left=108, Top=39, InputMask=replicate('X',40)

  add object oCOMDIS_1_25 as StdCheck with uid="GSNEGIQXDF",rtseq=15,rtrep=.f.,left=243, top=99, caption="Componenti disponibili",;
    HelpContextID = 9453786,;
    cFormVar="w_COMDIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOMDIS_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCOMDIS_1_25.GetRadio()
    this.Parent.oContained.w_COMDIS = this.RadioValue()
    return .t.
  endfunc

  func oCOMDIS_1_25.SetRadio()
    this.Parent.oContained.w_COMDIS=trim(this.Parent.oContained.w_COMDIS)
    this.value = ;
      iif(this.Parent.oContained.w_COMDIS=='S',1,;
      0)
  endfunc

  func oCOMDIS_1_25.mHide()
    with this.Parent.oContained
      return ((.w_ONUME=1 OR .w_ONUME=3 OR .w_ONUME=5))
    endwith
  endfunc


  add object oBtn_1_26 as StdButton with uid="YXDBGRPKSF",left=374, top=12, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un piano di produzione";
    , HelpContextID = 127511766;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      vx_exec("..\DISB\Exe\Query\Gsds_spp.vzm",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="GIGJHPMORF",Visible=.t., Left=5, Top=140,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QCEJHAMSPS",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=101, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="LPGIBLLYAF",Visible=.t., Left=5, Top=99,;
    Alignment=1, Width=101, Height=18,;
    Caption="Data stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="BMFLQBHNGW",Visible=.t., Left=5, Top=9,;
    Alignment=1, Width=101, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="JKEVEMYLBV",Visible=.t., Left=194, Top=9,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RVZATPIEEP",Visible=.t., Left=248, Top=9,;
    Alignment=1, Width=33, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RUWZWNFODY",Visible=.t., Left=5, Top=39,;
    Alignment=1, Width=101, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_spp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
