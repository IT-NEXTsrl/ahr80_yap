* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_b2d                                                        *
*              Cambia ciclo semplificato                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-27                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_b2d",oParentObject,m.pParam)
return(i_retval)

define class tgsds_b2d as StdBatch
  * --- Local variables
  pParam = space(1)
  w_MESS = space(10)
  GSDS_MCV = .NULL.
  GSDS_MLC = .NULL.
  GSDS_MDB = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pParam = 'R' Controlli al Cambio del Ciclo Semplificato (da GSDS_MDB)
    *     pParam = 'V' Controlli al Cambio del flag variante (da GSDS_MDB)
    this.GSDS_MDB = This.oParentObject
    do case
      case this.pParam="R"
        if this.oParentObject.w_DBCODRIS<>this.oParentObject.o_DBCODRIS AND NOT EMPTY(this.oParentObject.o_DBCODRIS)
          this.w_MESS = "Modificando il codice del ciclo semplificato%0verranno azzerate tutte le fasi associate ai componenti%0Proseguo ugualmente?"
          if ah_YesNo(this.w_MESS)
            * --- Scorre sul Detail
            SELECT (this.oParentObject.cTrsName)
            GO TOP
            SCAN
            * --- Legge i Dati del Temporaneo
            this.oParentObject.WorkFromTrs()
            * --- Azzera Fase
            this.oParentObject.w_DBRIFFAS = 0
            this.oParentObject.w_DESFAS = SPACE(40)
            * --- Carica il Temporaneo dei Dati
            this.oParentObject.TrsFromWork()
            * --- Flag Notifica Riga Variata
            if i_SRV<>"A"
              replace i_SRV with "U"
            endif
            ENDSCAN
            * --- Questa Parte derivata dal Metodo LoadRec
            SELECT (this.oParentObject.cTrsName)
            GO TOP
            With this.oParentObject
            .WorkFromTrs()
            .SetControlsValue()
            .ChildrenChangeRow()
            .oPgFrm.Page1.oPag.oBody.Refresh()
            .oPgFrm.Page1.oPag.oBody.nAbsRow=1
            .oPgFrm.Page1.oPag.oBody.nRelRow=1
            EndWith
          else
            * --- Ripristina il Codice Risorsa
            this.oParentObject.w_DBCODRIS = this.oParentObject.o_DBCODRIS
          endif
        endif
      case this.pParam="V"
        this.GSDS_MCV = this.oParentObject.GSDS_MCV
        if vartype(this.GSDS_MCV.cnt) <> "O"
          this.GSDS_MCV.LinkPcClick()     
          this.GSDS_MCV = this.oParentObject.GSDS_MCV
          this.GSDS_MCV.EcpSave()     
        endif
        NC = this.GSDS_MCV.cnt.CtrsName
        Select (NC) 
 GO TOP 
 DELETE ALL
        this.GSDS_MCV.cnt.InitRow()     
        this.GSDS_MCV.cnt.WorkFromTrs()     
        this.GSDS_MCV.cnt.bUpdated = .t.
      case this.pParam="C"
        if g_CCAR="S"
          this.GSDS_MLC = this.oParentObject.GSDS_MLC
          this.GSDS_MLC.LinkPcClick()     
          this.GSDS_MLC = this.oParentObject.GSDS_MLC
          this.GSDS_MLC.cnt.w_CCCOMPON = this.oParentObject.w_DBCODCOM
          this.GSDS_MLC.cnt.w_CCCODART = this.oParentObject.w_DBARTCOM
          this.GSDS_MLC.cnt.NotifyEvent("w_CCCOMPON Changed")     
          this.GSDS_MLC.cnt.bUpdated = .t.
          this.GSDS_MLC.EcpSave()     
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
