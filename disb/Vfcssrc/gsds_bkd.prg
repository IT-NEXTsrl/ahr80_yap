* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bkd                                                        *
*              Verifica distinta base ricorsiva                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-27                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bkd",oParentObject)
return(i_retval)

define class tgsds_bkd as StdBatch
  * --- Local variables
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_DBCODICE = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_MESS = space(10)
  w_OBJPAD = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se Un Articolo Associato ad un Componente appartiene anche alla stessa Distinta (da GSDS_MDB)
    * --- ATTENZIONE: Il Controllo e' Limitato al Solo Primo Livello (Compresi Componenti Varianti)
    this.w_OBJPAD = this.oParentObject
    with this.oParentObject
    this.w_DBCODINI = .w_DBCODICE
    this.w_DBCODFIN = .w_DBCODICE
    this.w_DBCODICE = .w_DBCODICE
    endwith
    this.w_MAXLEVEL = 99
    this.w_VERIFICA = "TN"
    this.w_FILSTAT = " "
    this.w_DATFIL = i_DATSYS
    this.w_MESS = ah_Msgformat("Operazione abbandonata")
    gsar_bde(this,"D")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OBJPAD.LoadRec()     
    if used("TES_PLOS")
      select TES_PLOS
      use
    endif
    if this.w_VERIFICA="ZZZ"
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
