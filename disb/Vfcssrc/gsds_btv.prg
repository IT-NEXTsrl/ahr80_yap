* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_btv                                                        *
*              Treev.strut.distinta base                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_350]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-24                                                      *
* Last revis.: 2015-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_btv",oParentObject,m.pTipOp)
return(i_retval)

define class tgsds_btv as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_DBCODICE = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_APPO = space(20)
  w_DATFIL = ctod("  /  /  ")
  w_CODICE = space(20)
  w_OBJECT = .NULL.
  w_CODICE = space(20)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  DISMBASE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView
    WITH This.OparentObject
    this.w_DBCODINI = .w_DBCODICE
    this.w_DBCODFIN = .w_DBCODICE
    this.w_DBCODICE = .w_DBCODICE
    this.w_MAXLEVEL = .w_MAXLEVEL
    this.w_VERIFICA = .w_VERIFICA
    this.w_FILSTAT = " "
    this.w_DATFIL = i_DATSYS
    ENDWITH 
    do case
      case (this.pTipop="Vista" OR this.pTipop="Requery") AND NOT EMPTY(this.w_DBCODICE)
        if used("TES_PLOS")
          select TES_PLOS
          use
        endif
        * --- Se non definito definisco il nome del cursore per riempire la Tree View
        if Empty( this.oParentObject.w_CURSORNA )
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        ah_Msg("Fase 1: esplosione distinta base...",.T.)
        gsar_bde(this,"D")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if used("TES_PLOS")
          ah_Msg("Fase 2: costruisce la treeview...",.T.)
          if used( ( this.oParentObject.w_CURSORNA ) )
             
 select ( this.oParentObject.w_CURSORNA ) 
 use
          endif
          if g_PROD="S"
            * --- Gestione bitmap
            if Used("BmpCursor")
              USE IN SELECT("BmpCursor")
            endif
            CREATE CURSOR BmpCursor (ARTIPART C(2), BMPNAME C(200))
            if used("BmpCursor")
              Insert into BmpCursor (ARTIPART, BMPNAME) VALUES ("PF" , "BMP\Tree1.ico")
              Insert into BmpCursor (ARTIPART, BMPNAME) VALUES ("SE" , "BMP\Tree4.ico")
              Insert into BmpCursor (ARTIPART, BMPNAME) VALUES ("MP" , "BMP\Tree2.ico")
              Insert into BmpCursor (ARTIPART, BMPNAME) VALUES ("PH" , "BMP\Tree5.ico")
              Insert into BmpCursor (ARTIPART, BMPNAME) VALUES ("CC" , "BMP\Compone.ico")
            endif
             
 Select *, STR(QTAUM1,12,3) As QTASTR, ; 
 IIF(EMPTY(COEIMP), SPACE(19), "  - "+ ah_Msgformat("Coeff.impiego:")+" ")+COEIMP As COESTR, ; 
 IIF(EMPTY(ARTVAR), SPACE(27), "  - "+ ah_Msgformat("Variante per articolo:")+" ")+ARTVAR As ARTSTR, ; 
 cp_bintoc(0)+SPACE(197) As LVLKEY , NVL(BMPCURSOR.BMPNAME, "BMP\treedefa.ico") AS CPBMPNAME ; 
 From TES_PLOS LEFT OUTER JOIN BMPCURSOR ON TES_PLOS.TIPART=BMPCURSOR.ARTIPART Into Cursor ( this.oParentObject.w_CURSORNA ) Order By NUMRIG NoFilter
          else
            this.oParentObject.w_TREEVIEW.cNodeBmp = "tree1.bmp"
            this.oParentObject.w_TREEVIEW.cLeafBmp = "Tree2.bmp"
            this.oParentObject.w_TREEVIEW.nIndent = 20
             
 Select *, STR(QTAUM1,12,3) As QTASTR, ; 
 IIF(EMPTY(COEIMP), SPACE(19), "  - "+ ah_Msgformat("Coeff.impiego:")+" ")+COEIMP As COESTR, ; 
 IIF(EMPTY(ARTVAR), SPACE(27), "  - "+ ah_Msgformat("Variante per articolo:")+" ")+ARTVAR As ARTSTR, ; 
 cp_bintoc(0)+SPACE(197) As LVLKEY ; 
 From TES_PLOS Into Cursor ( this.oParentObject.w_CURSORNA ) Order By NUMRIG NoFilter
          endif
          if used("TES_PLOS")
             
 select TES_PLOS 
 Use
          endif
          if Used("BmpCursor")
            USE IN SELECT("BmpCursor")
          endif
          WRCURSOR( this.oParentObject.w_CURSORNA )
* --- Area Manuale = Costruzione LVLKEY
* --- gsds_btv
* --- costruisce la LVLKEY in base al valore di NUMLEV
PRIVATE ii, kk, Oliv, Nliv, Okrepl
FOR ii=0 to 99
   Oliv=0
   kk=0
   okrepl=0
   SELECT ( this.oParentObject.w_CURSORNA )
   GO TOP 
   SCAN FOR NOT EMPTY(NUMLEV)
      Nliv = VAL(NUMLEV) 
      IF Nliv = ii
         kk = IIF(Oliv<Nliv, 1, kk + 1)
      ENDIF
      IF Nliv >= ii
         * REPLACE LVLKEY WITH RTRIM(LVLKEY) + IIF(EMPTY(LVLKEY),'','.') + RIGHT('00000'+ALLTRIM(STR(kk)),5)
         REPLACE LVLKEY WITH RTRIM(LVLKEY) + IIF(EMPTY(LVLKEY),'','.') + cp_bintoc(kk)
         okrepl=1
      ENDIF
      Oliv = Nliv
   ENDSCAN
   IF okrepl=0
      EXIT
   ENDIF  
ENDFOR

* --- Fine Area Manuale
           
 Select ( this.oParentObject.w_CURSORNA ) 
 INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
          this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
          this.oParentObject.notifyevent("Esegui")
        endif
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used( ( this.oParentObject.w_CURSORNA ) )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
        if used("TES_PLOS")
          select TES_PLOS
          use
        endif
      case this.pTipop="Distinta"
        if this.oParentObject.cFunction="Query"
          this.w_CODICE = Nvl(this.oParentObject.w_TREEVIEW.GETVAR("CODDIS"),Space(20))
          if EMPTY(NVL(this.w_CODICE, ""))
            * --- Vede se il Componente e' una Distinta
            this.w_APPO = Nvl(this.oParentObject.w_TREEVIEW.GETVAR("DISPAD"),Space(20))
            if NOT EMPTY(NVL(this.w_APPO,""))
              * --- Read from DISMBASE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DISMBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DBCODICE"+;
                  " from "+i_cTable+" DISMBASE where ";
                      +"DBCODICE = "+cp_ToStrODBC(this.w_APPO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DBCODICE;
                  from (i_cTable) where;
                      DBCODICE = this.w_APPO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODICE = NVL(cp_ToDate(_read_.DBCODICE),cp_NullValue(_read_.DBCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          if NOT EMPTY(NVL(this.w_CODICE,""))
            if this.w_CODICE<>this.w_DBCODICE
              this.w_OBJECT = GSDS_MDB()
              * --- Controllo se ha passato il test di accesso
              if !(this.w_OBJECT.bSec1)
                i_retcode = 'stop'
                return
              endif
              this.w_OBJECT.w_DBCODICE = this.w_CODICE
              this.w_OBJECT.QueryKeySet("DBCODICE='"+this.w_CODICE+"'","")     
              this.w_OBJECT.LoadRecWarn()     
            endif
          else
            ah_ErrorMsg("Codice componente non selezionato o inesistente",,"")
          endif
        else
          ah_ErrorMsg("Selezione distinta consentita solo in interrogazione",,"")
        endif
      case this.pTipop="Articolo"
        this.w_CODICE = Nvl(this.oParentObject.w_TREEVIEW.GETVAR("ARTCOM"),Space(20))
        if EMPTY(NVL(this.w_CODICE, ""))
          ah_ErrorMsg("Codice componente non selezionato o inesistente",,"")
        else
          this.w_OBJECT = GSMA_AAR()
          if !(this.w_OBJECT.bSec1)
            i_retcode = 'stop'
            return
          endif
          this.w_OBJECT.w_ARCODART = this.w_CODICE
          this.w_OBJECT.QueryKeySet(cp_BuildWhere("",this.w_CODICE,"ARCODART",1),"")     
          this.w_OBJECT.LoadRecWarn()     
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DISMBASE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
