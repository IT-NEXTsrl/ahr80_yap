* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_mlc                                                        *
*              Caratteristiche del legame                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsds_mlc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsds_mlc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsds_mlc")
  return

* --- Class definition
define class tgsds_mlc as StdPCForm
  Width  = 643
  Height = 527
  Top    = 10
  Left   = 10
  cComment = "Caratteristiche del legame"
  cPrg = "gsds_mlc"
  HelpContextID=214525033
  add object cnt as tcgsds_mlc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsds_mlc as PCContext
  w_CC__DIBA = space(20)
  w_CCROWORD = 0
  w_CCCOMPON = space(20)
  w_CCCODART = space(20)
  w_OBTEST = space(8)
  w_CPROWORD = 0
  w_CCTIPOCA = space(1)
  w_TIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_TIPGES = space(1)
  w_CC__NOTE = space(10)
  w_CCROWPAD = 0
  w_DATINI = space(8)
  w_DATFIN = space(8)
  w_DIFINVAL = space(8)
  w_DINIVAL = space(8)
  w_CCDATINI = space(8)
  w_CCDATFIN = space(8)
  w_DBCODICE = space(20)
  w_COCODCOM = space(20)
  proc Save(i_oFrom)
    this.w_CC__DIBA = i_oFrom.w_CC__DIBA
    this.w_CCROWORD = i_oFrom.w_CCROWORD
    this.w_CCCOMPON = i_oFrom.w_CCCOMPON
    this.w_CCCODART = i_oFrom.w_CCCODART
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CCTIPOCA = i_oFrom.w_CCTIPOCA
    this.w_TIPOCA = i_oFrom.w_TIPOCA
    this.w_CCCODICE = i_oFrom.w_CCCODICE
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_TIPGES = i_oFrom.w_TIPGES
    this.w_CC__NOTE = i_oFrom.w_CC__NOTE
    this.w_CCROWPAD = i_oFrom.w_CCROWPAD
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_DIFINVAL = i_oFrom.w_DIFINVAL
    this.w_DINIVAL = i_oFrom.w_DINIVAL
    this.w_CCDATINI = i_oFrom.w_CCDATINI
    this.w_CCDATFIN = i_oFrom.w_CCDATFIN
    this.w_DBCODICE = i_oFrom.w_DBCODICE
    this.w_COCODCOM = i_oFrom.w_COCODCOM
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CC__DIBA = this.w_CC__DIBA
    i_oTo.w_CCROWORD = this.w_CCROWORD
    i_oTo.w_CCCOMPON = this.w_CCCOMPON
    i_oTo.w_CCCODART = this.w_CCCODART
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CCTIPOCA = this.w_CCTIPOCA
    i_oTo.w_TIPOCA = this.w_TIPOCA
    i_oTo.w_CCCODICE = this.w_CCCODICE
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_TIPGES = this.w_TIPGES
    i_oTo.w_CC__NOTE = this.w_CC__NOTE
    i_oTo.w_CCROWPAD = this.w_CCROWPAD
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_DIFINVAL = this.w_DIFINVAL
    i_oTo.w_DINIVAL = this.w_DINIVAL
    i_oTo.w_CCDATINI = this.w_CCDATINI
    i_oTo.w_CCDATFIN = this.w_CCDATFIN
    i_oTo.w_DBCODICE = this.w_DBCODICE
    i_oTo.w_COCODCOM = this.w_COCODCOM
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsds_mlc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 643
  Height = 527
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-15"
  HelpContextID=214525033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONF_DIS_IDX = 0
  ART_ICOL_IDX = 0
  CONF_CAR_IDX = 0
  DISTBASE_IDX = 0
  DISMBASE_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONF_DIS"
  cKeySelect = "CC__DIBA,CCROWORD"
  cKeyWhere  = "CC__DIBA=this.w_CC__DIBA and CCROWORD=this.w_CCROWORD"
  cKeyDetail  = "CC__DIBA=this.w_CC__DIBA and CCROWORD=this.w_CCROWORD and CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';

  cKeyDetailWhereODBC = '"CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';
      +'+" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CONF_DIS.CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CONF_DIS.CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONF_DIS.CPROWORD'
  cPrg = "gsds_mlc"
  cComment = "Caratteristiche del legame"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CC__DIBA = space(20)
  w_CCROWORD = 0
  w_CCCOMPON = space(20)
  w_CCCODART = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_CPROWORD = 0
  w_CCTIPOCA = space(1)
  o_CCTIPOCA = space(1)
  w_TIPOCA = space(1)
  o_TIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_TIPGES = space(1)
  w_CC__NOTE = space(0)
  w_CCROWPAD = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DIFINVAL = ctod('  /  /  ')
  o_DIFINVAL = ctod('  /  /  ')
  w_DINIVAL = ctod('  /  /  ')
  o_DINIVAL = ctod('  /  /  ')
  w_CCDATINI = ctod('  /  /  ')
  w_CCDATFIN = ctod('  /  /  ')
  w_DBCODICE = space(20)
  w_COCODCOM = space(20)

  * --- Children pointers
  GSDS_MLD = .NULL.
  GSDS_ALP = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsds_mlc
  Proc ecpQuit()
     this.ecpSave()
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSDS_MLD additive
    *set procedure to GSDS_ALP additive
    with this
      .Pages(1).addobject("oPag","tgsds_mlcPag1","gsds_mlc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Caratteristiche")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSDS_MLD
    *release procedure GSDS_ALP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONF_CAR'
    this.cWorkTables[3]='DISTBASE'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='CONF_DIS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONF_DIS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONF_DIS_IDX,3]
  return

  function CreateChildren()
    this.GSDS_MLD = CREATEOBJECT('stdDynamicChild',this,'GSDS_MLD',this.oPgFrm.Page1.oPag.oLinkPC_2_6)
    this.GSDS_MLD.createrealchild()
    this.GSDS_ALP = CREATEOBJECT('stdDynamicChild',this,'GSDS_ALP',this.oPgFrm.Page1.oPag.oLinkPC_2_8)
    this.GSDS_ALP.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsds_mlc'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSDS_MLD)
      this.GSDS_MLD.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSDS_ALP)
      this.GSDS_ALP.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSDS_MLD.HideChildrenChain()
    this.GSDS_ALP.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSDS_MLD.ShowChildrenChain()
    this.GSDS_ALP.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSDS_MLD)
      this.GSDS_MLD.DestroyChildrenChain()
      this.GSDS_MLD=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_6')
    if !ISNULL(this.GSDS_ALP)
      this.GSDS_ALP.DestroyChildrenChain()
      this.GSDS_ALP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSDS_MLD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSDS_ALP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSDS_MLD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSDS_ALP.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSDS_MLD.NewDocument()
    this.GSDS_ALP.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSDS_MLD.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CC__DIBA,"CC__DIBA";
             ,.w_CCROWORD,"CCROWORD";
             ,.w_CCCOMPON,"CCCOMPON";
             ,.w_CCTIPOCA,"CCTIPOCA";
             ,.w_CCCODICE,"CCCODICE";
             )
      .GSDS_ALP.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CC__DIBA,"CC__DIBA";
             ,.w_CCROWORD,"CCROWORD";
             ,.w_CCCOMPON,"CCCOMPON";
             ,.w_CCTIPOCA,"CCTIPOCA";
             ,.w_CCCODICE,"CCCODICE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONF_DIS where CC__DIBA=KeySet.CC__DIBA
    *                            and CCROWORD=KeySet.CCROWORD
    *                            and CCTIPOCA=KeySet.CCTIPOCA
    *                            and CCCODICE=KeySet.CCCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2],this.bLoadRecFilter,this.CONF_DIS_IDX,"gsds_mlc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONF_DIS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONF_DIS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONF_DIS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CC__DIBA',this.w_CC__DIBA  ,'CCROWORD',this.w_CCROWORD  )
      select * from (i_cTable) CONF_DIS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_DBCODICE = space(20)
        .w_CC__DIBA = NVL(CC__DIBA,space(20))
        .w_CCROWORD = NVL(CCROWORD,0)
        .w_CCCOMPON = NVL(CCCOMPON,space(20))
        .w_CCCODART = NVL(CCCODART,space(20))
        .w_CCROWPAD = This.oParentObject .w_CPROWORD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COCODCOM = .w_CCCOMPON
        cp_LoadRecExtFlds(this,'CONF_DIS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CC__NOTE = space(0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
        .w_TIPOCA = .w_CCTIPOCA
          .w_CCCODICE = NVL(CCCODICE,space(5))
          .link_2_4('Load')
          .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .w_TIPGES = 'G'
        .w_DIFINVAL = looktab('CONF_DIS','CCDATFIN','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
        .w_DINIVAL = looktab('CONF_DIS','CCDATINI','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
          .w_CCDATINI = NVL(cp_ToDate(CCDATINI),ctod("  /  /  "))
          .w_CCDATFIN = NVL(cp_ToDate(CCDATFIN),ctod("  /  /  "))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CCTIPOCA with .w_CCTIPOCA
          replace CCCODICE with .w_CCCODICE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CCROWPAD = This.oParentObject .w_CPROWORD
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COCODCOM = .w_CCCOMPON
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CC__DIBA=space(20)
      .w_CCROWORD=0
      .w_CCCOMPON=space(20)
      .w_CCCODART=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_CPROWORD=10
      .w_CCTIPOCA=space(1)
      .w_TIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_TIPGES=space(1)
      .w_CC__NOTE=space(0)
      .w_CCROWPAD=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DIFINVAL=ctod("  /  /  ")
      .w_DINIVAL=ctod("  /  /  ")
      .w_CCDATINI=ctod("  /  /  ")
      .w_CCDATFIN=ctod("  /  /  ")
      .w_DBCODICE=space(20)
      .w_COCODCOM=space(20)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(6,6,.f.)
        .w_CCTIPOCA = "C"
        .w_TIPOCA = .w_CCTIPOCA
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CCCODICE))
         .link_2_4('Full')
        endif
        .DoRTCalc(10,10,.f.)
        .w_TIPGES = 'G'
        .DoRTCalc(12,12,.f.)
        .w_CCROWPAD = This.oParentObject .w_CPROWORD
        .DoRTCalc(14,15,.f.)
        .w_DIFINVAL = looktab('CONF_DIS','CCDATFIN','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
        .w_DINIVAL = looktab('CONF_DIS','CCDATINI','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(18,20,.f.)
        .w_COCODCOM = .w_CCCOMPON
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONF_DIS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsds_mlc
    EndPRoc
    
    Proc EcpQuit()
      This.ecpSave()
    
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSDS_MLD.SetStatus(i_cOp)
    this.GSDS_ALP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONF_DIS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSDS_MLD.SetChildrenStatus(i_cOp)
  *  this.GSDS_ALP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC__DIBA,"CC__DIBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCROWORD,"CCROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCOMPON,"CCCOMPON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODART,"CCCODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_TIPOCA N(3);
      ,t_CCCODICE C(5);
      ,t_CCDESCRI C(40);
      ,t_CC__NOTE M(10);
      ,CCTIPOCA C(1);
      ,CCCODICE C(5);
      ,t_CCTIPOCA C(1);
      ,t_TIPGES C(1);
      ,t_DIFINVAL D(8);
      ,t_DINIVAL D(8);
      ,t_CCDATINI D(8);
      ,t_CCDATFIN D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsds_mlcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.controlsource=this.cTrsName+'.t_TIPOCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.controlsource=this.cTrsName+'.t_CCCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.controlsource=this.cTrsName+'.t_CCDESCRI'
    this.oPgFRm.Page1.oPag.oCC__NOTE_2_10.controlsource=this.cTrsName+'.t_CC__NOTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(67)
    this.AddVLine(181)
    this.AddVLine(261)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2])
      *
      * insert into CONF_DIS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONF_DIS')
        i_extval=cp_InsertValODBCExtFlds(this,'CONF_DIS')
        i_cFldBody=" "+;
                  "(CC__DIBA,CCROWORD,CCCOMPON,CCCODART,CPROWORD"+;
                  ",CCTIPOCA,CCCODICE,CCDESCRI,CCDATINI,CCDATFIN,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CC__DIBA)+","+cp_ToStrODBC(this.w_CCROWORD)+","+cp_ToStrODBC(this.w_CCCOMPON)+","+cp_ToStrODBC(this.w_CCCODART)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_CCTIPOCA)+","+cp_ToStrODBCNull(this.w_CCCODICE)+","+cp_ToStrODBC(this.w_CCDESCRI)+","+cp_ToStrODBC(this.w_CCDATINI)+","+cp_ToStrODBC(this.w_CCDATFIN)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONF_DIS')
        i_extval=cp_InsertValVFPExtFlds(this,'CONF_DIS')
        cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_CC__DIBA,'CCROWORD',this.w_CCROWORD,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable) (;
                   CC__DIBA;
                  ,CCROWORD;
                  ,CCCOMPON;
                  ,CCCODART;
                  ,CPROWORD;
                  ,CCTIPOCA;
                  ,CCCODICE;
                  ,CCDESCRI;
                  ,CCDATINI;
                  ,CCDATFIN;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CC__DIBA;
                  ,this.w_CCROWORD;
                  ,this.w_CCCOMPON;
                  ,this.w_CCCODART;
                  ,this.w_CPROWORD;
                  ,this.w_CCTIPOCA;
                  ,this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CCDATINI;
                  ,this.w_CCDATFIN;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CCCODICE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_DIS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " CCCOMPON="+cp_ToStrODBC(this.w_CCCOMPON)+;
                 ",CCCODART="+cp_ToStrODBC(this.w_CCCODART)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                 " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_DIS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  CCCOMPON=this.w_CCCOMPON;
                 ,CCCODART=this.w_CCCODART;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CCTIPOCA=&i_TN.->CCTIPOCA;
                      and CCCODICE=&i_TN.->CCCODICE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CCCODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSDS_MLD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CC__DIBA,"CC__DIBA";
                     ,this.w_CCROWORD,"CCROWORD";
                     ,this.w_CCCOMPON,"CCCOMPON";
                     ,this.w_CCTIPOCA,"CCTIPOCA";
                     ,this.w_CCCODICE,"CCCODICE";
                     )
              this.GSDS_ALP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CC__DIBA,"CC__DIBA";
                     ,this.w_CCROWORD,"CCROWORD";
                     ,this.w_CCCOMPON,"CCCOMPON";
                     ,this.w_CCTIPOCA,"CCTIPOCA";
                     ,this.w_CCCODICE,"CCCODICE";
                     )
              this.GSDS_MLD.mDelete()
              this.GSDS_ALP.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                            " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CCTIPOCA=&i_TN.->CCTIPOCA;
                            and CCCODICE=&i_TN.->CCCODICE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CCTIPOCA with this.w_CCTIPOCA
              replace CCCODICE with this.w_CCCODICE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONF_DIS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_DIS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CCCOMPON="+cp_ToStrODBC(this.w_CCCOMPON)+;
                     ",CCCODART="+cp_ToStrODBC(this.w_CCCODART)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
                     ",CCDATINI="+cp_ToStrODBC(this.w_CCDATINI)+;
                     ",CCDATFIN="+cp_ToStrODBC(this.w_CCDATFIN)+;
                     ",CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)+;
                     ",CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CCTIPOCA="+cp_ToStrODBC(CCTIPOCA)+;
                             " and CCCODICE="+cp_ToStrODBC(CCCODICE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_DIS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CCCOMPON=this.w_CCCOMPON;
                     ,CCCODART=this.w_CCCODART;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CCDESCRI=this.w_CCDESCRI;
                     ,CCDATINI=this.w_CCDATINI;
                     ,CCDATFIN=this.w_CCDATFIN;
                     ,CCTIPOCA=this.w_CCTIPOCA;
                     ,CCCODICE=this.w_CCCODICE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CCTIPOCA=&i_TN.->CCTIPOCA;
                                      and CCCODICE=&i_TN.->CCCODICE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CCCODICE)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSDS_MLD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CC__DIBA,"CC__DIBA";
               ,this.w_CCROWORD,"CCROWORD";
               ,this.w_CCCOMPON,"CCCOMPON";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
          this.GSDS_MLD.mReplace()
          this.GSDS_ALP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CC__DIBA,"CC__DIBA";
               ,this.w_CCROWORD,"CCROWORD";
               ,this.w_CCCOMPON,"CCCOMPON";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
          this.GSDS_ALP.mReplace()
          this.GSDS_MLD.bSaveContext=.f.
          this.GSDS_ALP.bSaveContext=.f.
        endif
      endscan
     this.GSDS_MLD.bSaveContext=.t.
     this.GSDS_ALP.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsds_mlc
    *--- Inserimento CONF_ART
    This.NotifyEvent('Salvataggio')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CCCODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSDS_MLD : Deleting
        this.GSDS_MLD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CC__DIBA,"CC__DIBA";
               ,this.w_CCROWORD,"CCROWORD";
               ,this.w_CCCOMPON,"CCCOMPON";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
        this.GSDS_MLD.mDelete()
        * --- GSDS_ALP : Deleting
        this.GSDS_ALP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CC__DIBA,"CC__DIBA";
               ,this.w_CCROWORD,"CCROWORD";
               ,this.w_CCCOMPON,"CCCOMPON";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
        this.GSDS_ALP.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONF_DIS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                            " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CCTIPOCA=&i_TN.->CCTIPOCA;
                              and CCCODICE=&i_TN.->CCCODICE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CCCODICE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONF_DIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_DIS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_CCTIPOCA<>.w_CCTIPOCA
          .w_TIPOCA = .w_CCTIPOCA
        endif
        if .o_TIPOCA<>.w_TIPOCA
          .Calculate_LYESVDOCPQ()
        endif
        .DoRTCalc(9,10,.t.)
          .w_TIPGES = 'G'
        .DoRTCalc(12,12,.t.)
          .w_CCROWPAD = This.oParentObject .w_CPROWORD
        .DoRTCalc(14,15,.t.)
          .w_DIFINVAL = looktab('CONF_DIS','CCDATFIN','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
          .w_DINIVAL = looktab('CONF_DIS','CCDATINI','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(18,20,.t.)
          .w_COCODCOM = .w_CCCOMPON
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CCTIPOCA with this.w_CCTIPOCA
      replace t_TIPGES with this.w_TIPGES
      replace t_DIFINVAL with this.w_DIFINVAL
      replace t_DINIVAL with this.w_DINIVAL
      replace t_CCDATINI with this.w_CCDATINI
      replace t_CCDATFIN with this.w_CCDATFIN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_XTLPJCFVQK()
    with this
          * --- w_CCCODICE Changed
     if g_CCAR='S'
          GSCR_BLD(this;
              ,"CCCODICE";
             )
     endif
    endwith
  endproc
  proc Calculate_ORJKQNWYAN()
    with this
          * --- GSCR_BLL -> Modifica
     if g_CCAR='S'
          GSCR_BLL(this;
             )
     endif
    endwith
  endproc
  proc Calculate_RTRPNLTVLS()
    with this
          * --- GSCR_BLC -> w_CCCOMPON Changed
     if g_CCAR='S'
          GSCR_BLC(this;
              ,"CCCOMPON";
             )
     endif
    endwith
  endproc
  proc Calculate_LYESVDOCPQ()
    with this
          * --- Valorizzazione w_CCTIPOCA
          .w_CCTIPOCA = .w_TIPOCA
    endwith
  endproc
  proc Calculate_RPNNPGDOOU()
    with this
          * --- GSCR_BLC -> Salvataggio
     if g_CCAR='S'
          GSCR_BLC(this;
              ,"CONFCAR";
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIPOCA_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIPOCA_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCODICE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCODICE_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_6.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_6.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_2_8.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_8.mHide()
    this.oPgFrm.Page1.oPag.oCC__NOTE_2_10.visible=!this.oPgFrm.Page1.oPag.oCC__NOTE_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CCCODICE Changed")
          .Calculate_XTLPJCFVQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Modifica")
          .Calculate_ORJKQNWYAN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CCCOMPON Changed")
          .Calculate_RTRPNLTVLS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Salvataggio")
          .Calculate_RPNNPGDOOU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODICE
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CCCODICE)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_CCTIPOCA;
                     ,'CCCODICE',trim(this.w_CCCODICE))
          select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODICE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODICE) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oCCCODICE_2_4'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CCTIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_CCTIPOCA;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(40))
      this.w_CC__NOTE = NVL(_Link_.CC__NOTE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(5)
      endif
      this.w_CCDESCRI = space(40)
      this.w_CC__NOTE = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCC__NOTE_2_10.value==this.w_CC__NOTE)
      this.oPgFrm.Page1.oPag.oCC__NOTE_2_10.value=this.w_CC__NOTE
      replace t_CC__NOTE with this.oPgFrm.Page1.oPag.oCC__NOTE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCCROWPAD_1_12.value==this.w_CCROWPAD)
      this.oPgFrm.Page1.oPag.oCCROWPAD_1_12.value=this.w_CCROWPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODICE_1_17.value==this.w_DBCODICE)
      this.oPgFrm.Page1.oPag.oDBCODICE_1_17.value=this.w_DBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCOM_1_18.value==this.w_COCODCOM)
      this.oPgFrm.Page1.oPag.oCOCODCOM_1_18.value=this.w_COCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.RadioValue()==this.w_TIPOCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.SetRadio()
      replace t_TIPOCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value=this.w_CCCODICE
      replace t_CCCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value=this.w_CCDESCRI
      replace t_CCDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'CONF_DIS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CCCODICE) and (.cFuncTion="Load" or empty(.w_CCCODICE)) and (not(Empty(.w_CCCODICE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSDS_MLD.CheckForm()
      i_bRes = i_bRes .and. .GSDS_ALP.CheckForm()
      if not(Empty(.w_CCCODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCTIPOCA = this.w_CCTIPOCA
    this.o_TIPOCA = this.w_TIPOCA
    this.o_DIFINVAL = this.w_DIFINVAL
    this.o_DINIVAL = this.w_DINIVAL
    * --- GSDS_MLD : Depends On
    this.GSDS_MLD.SaveDependsOn()
    * --- GSDS_ALP : Depends On
    this.GSDS_ALP.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CCCODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CCTIPOCA=space(1)
      .w_TIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_TIPGES=space(1)
      .w_CC__NOTE=space(0)
      .w_DIFINVAL=ctod("  /  /  ")
      .w_DINIVAL=ctod("  /  /  ")
      .w_CCDATINI=ctod("  /  /  ")
      .w_CCDATFIN=ctod("  /  /  ")
      .DoRTCalc(1,6,.f.)
        .w_CCTIPOCA = "C"
        .w_TIPOCA = .w_CCTIPOCA
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_CCCODICE))
        .link_2_4('Full')
      endif
      .DoRTCalc(10,10,.f.)
        .w_TIPGES = 'G'
      .DoRTCalc(12,15,.f.)
        .w_DIFINVAL = looktab('CONF_DIS','CCDATFIN','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
        .w_DINIVAL = looktab('CONF_DIS','CCDATINI','CC__DIBA',.w_CC__DIBA,'CCCOMPON',.w_CCCOMPON,'CCROWORD',.w_CCROWORD)
    endwith
    this.DoRTCalc(18,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CCTIPOCA = t_CCTIPOCA
    this.w_TIPOCA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.RadioValue(.t.)
    this.w_CCCODICE = t_CCCODICE
    this.w_CCDESCRI = t_CCDESCRI
    this.w_TIPGES = t_TIPGES
    this.w_CC__NOTE = t_CC__NOTE
    this.w_DIFINVAL = t_DIFINVAL
    this.w_DINIVAL = t_DINIVAL
    this.w_CCDATINI = t_CCDATINI
    this.w_CCDATFIN = t_CCDATFIN
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CCTIPOCA with this.w_CCTIPOCA
    replace t_TIPOCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.ToRadio()
    replace t_CCCODICE with this.w_CCCODICE
    replace t_CCDESCRI with this.w_CCDESCRI
    replace t_TIPGES with this.w_TIPGES
    replace t_CC__NOTE with this.w_CC__NOTE
    replace t_DIFINVAL with this.w_DIFINVAL
    replace t_DINIVAL with this.w_DINIVAL
    replace t_CCDATINI with this.w_CCDATINI
    replace t_CCDATFIN with this.w_CCDATFIN
    if i_srv='A'
      replace CCTIPOCA with this.w_CCTIPOCA
      replace CCCODICE with this.w_CCCODICE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsds_mlcPag1 as StdContainer
  Width  = 639
  height = 527
  stdWidth  = 639
  stdheight = 527
  resizeXpos=336
  resizeYpos=167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCROWPAD_1_12 as StdField with uid="MUSNAKZWOZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CCROWPAD", cQueryName = "CCROWPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 150668138,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=108, Top=53, cSayPict='"99999"', cGetPict='"99999"'


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=78, width=619,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="TIPOCA",Label2="Tipo",Field3="CCCODICE",Label3="Caratteristica",Field4="CCDESCRI",Label4="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101565818

  add object oDBCODICE_1_17 as StdField with uid="QZHVXRAKEM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DBCODICE", cQueryName = "DBCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 13243003,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=108, Top=3, InputMask=replicate('X',20)

  add object oCOCODCOM_1_18 as StdField with uid="VJQFNSNGCQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_COCODCOM", cQueryName = "COCODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 87416973,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=108, Top=28, InputMask=replicate('X',20)

  add object oStr_1_6 as StdString with uid="XUPGSDUYJH",Visible=.t., Left=18, Top=32,;
    Alignment=1, Width=85, Height=18,;
    Caption="Componente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="LRKCMOYHKZ",Visible=.t., Left=10, Top=7,;
    Alignment=1, Width=93, Height=18,;
    Caption="Distinta base:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="JZQPSZXDSZ",Visible=.t., Left=69, Top=54,;
    Alignment=1, Width=34, Height=18,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsds_mld",lower(this.oContained.GSDS_MLD.class))=0
        this.oContained.GSDS_MLD.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsds_alp",lower(this.oContained.GSDS_ALP.class))=0
        this.oContained.GSDS_ALP.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=97,;
    width=615+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=98,width=614+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONF_CAR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCC__NOTE_2_10.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONF_CAR'
        oDropInto=this.oBodyCol.oRow.oCCCODICE_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_6 as stdDynamicChildContainer with uid="DKCALJOHRB",bOnScreen=.t.,width=574,height=286,;
   left=4, top=237;


  func oLinkPC_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"C")
    endwith
   endif
  endfunc

  add object oLinkPC_2_8 as stdDynamicChildContainer with uid="KNIFAUNDXM",bOnScreen=.t.,width=624,height=284,;
   left=4, top=237;


  func oLinkPC_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"M")
    endwith
   endif
  endfunc

  add object oCC__NOTE_2_10 as StdTrsMemo with uid="KZCQYVXKAS",rtseq=12,rtrep=.t.,;
    cFormVar="w_CC__NOTE",value=space(0),enabled=.f.,;
    HelpContextID = 125555563,;
    cTotal="", bFixedPos=.t., cQueryName = "CC__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=283, Width=626, Left=4, Top=237

  func oCC__NOTE_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"D")
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsds_mlcBodyRow as CPBodyRowCnt
  Width=605
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="TOAKAMAILH",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 133894250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oTIPOCA_2_3 as StdTrsCombo with uid="PXZBKJXFMP",rtrep=.t.,;
    cFormVar="w_TIPOCA", RowSource=""+"Caratteristica,"+"Modello,"+"Descrizione" , ;
    HelpContextID = 121968074,;
    Height=21, Width=104, Left=58, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPOCA_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPOCA,&i_cF..t_TIPOCA),this.value)
    return(iif(xVal =1,"C",;
    iif(xVal =2,"M",;
    iif(xVal =3,"D",;
    space(1)))))
  endfunc
  func oTIPOCA_2_3.GetRadio()
    this.Parent.oContained.w_TIPOCA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCA_2_3.ToRadio()
    this.Parent.oContained.w_TIPOCA=trim(this.Parent.oContained.w_TIPOCA)
    return(;
      iif(this.Parent.oContained.w_TIPOCA=="C",1,;
      iif(this.Parent.oContained.w_TIPOCA=="M",2,;
      iif(this.Parent.oContained.w_TIPOCA=="D",3,;
      0))))
  endfunc

  func oTIPOCA_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPOCA_2_3.mCond()
    with this.Parent.oContained
      return (empty(nvl(.w_CCCODICE,"")))
    endwith
  endfunc

  add object oCCCODICE_2_4 as StdTrsField with uid="SHFJHECKFE",rtseq=9,rtrep=.t.,;
    cFormVar="w_CCCODICE",value=space(5),isprimarykey=.t.,;
    HelpContextID = 13243243,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=170, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_CCTIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CCCODICE"

  func oCCCODICE_2_4.mCond()
    with this.Parent.oContained
      return (.cFuncTion="Load" or empty(.w_CCCODICE))
    endwith
  endfunc

  func oCCCODICE_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODICE_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCCODICE_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCCCODICE_2_4.readonly and this.parent.oCCCODICE_2_4.isprimarykey)
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_CCTIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_CCTIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oCCCODICE_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
   endif
  endproc

  add object oCCDESCRI_2_5 as StdTrsField with uid="ZYUEJRUCMX",rtseq=10,rtrep=.t.,;
    cFormVar="w_CCDESCRI",value=space(40),;
    HelpContextID = 196092783,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=347, Left=253, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_mlc','CONF_DIS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CC__DIBA=CONF_DIS.CC__DIBA";
  +" and "+i_cAliasName2+".CCROWORD=CONF_DIS.CCROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsds_mlc
* --- Class definition
define class tgscr_mlc as StdPCForm
  Width  = 643
  Height = 527
  Top    = 10
  Left   = 10
  cComment = "Caratteristiche del legame"
  cPrg = "gscr_mlc"
  HelpContextID=214525033
  add object cnt as tcgscr_mlc

  proc ecpQuit()
    if !this.cnt.bF10
      if !this.cnt.bUpdated
        this.cnt.bDontReportError=.t.
        this.TerminateEdit()
        this.cnt.bUpdated=.f.
      endif
    endif
    *this.cnt.bUpdated=iif(this.cnt.bF10,this.cnt.bUpdated,.f.)
    this.LinkPCClick()
  Endproc

enddefine
* --- Fine Area Manuale
