* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_atd                                                        *
*              Tipologia distinta base                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-30                                                      *
* Last revis.: 2015-01-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_atd"))

* --- Class definition
define class tgsds_atd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 451
  Height = 97+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-15"
  HelpContextID=92890217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  TIP_DIBA_IDX = 0
  cFile = "TIP_DIBA"
  cKeySelect = "TDCODICE"
  cKeyWhere  = "TDCODICE=this.w_TDCODICE"
  cKeyWhereODBC = '"TDCODICE="+cp_ToStrODBC(this.w_TDCODICE)';

  cKeyWhereODBCqualified = '"TIP_DIBA.TDCODICE="+cp_ToStrODBC(this.w_TDCODICE)';

  cPrg = "gsds_atd"
  cComment = "Tipologia distinta base"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TDCODICE = space(4)
  w_TDDESCRI = space(40)
  w_TDDFAULT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIP_DIBA','gsds_atd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_atdPag1","gsds_atd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 34442762
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTDCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIP_DIBA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIP_DIBA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIP_DIBA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TDCODICE = NVL(TDCODICE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIP_DIBA where TDCODICE=KeySet.TDCODICE
    *
    i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIP_DIBA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIP_DIBA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIP_DIBA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TDCODICE',this.w_TDCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TDCODICE = NVL(TDCODICE,space(4))
        .w_TDDESCRI = NVL(TDDESCRI,space(40))
        .w_TDDFAULT = NVL(TDDFAULT,space(1))
        cp_LoadRecExtFlds(this,'TIP_DIBA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TDCODICE = space(4)
      .w_TDDESCRI = space(40)
      .w_TDDFAULT = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIP_DIBA')
    this.DoRTCalc(1,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTDCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTDDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oTDDFAULT_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTDCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTDCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIP_DIBA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDCODICE,"TDCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDESCRI,"TDDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TDDFAULT,"TDDFAULT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
    i_lTable = "TIP_DIBA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIP_DIBA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIP_DIBA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIP_DIBA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIP_DIBA')
        i_extval=cp_InsertValODBCExtFlds(this,'TIP_DIBA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TDCODICE,TDDESCRI,TDDFAULT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TDCODICE)+;
                  ","+cp_ToStrODBC(this.w_TDDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TDDFAULT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIP_DIBA')
        i_extval=cp_InsertValVFPExtFlds(this,'TIP_DIBA')
        cp_CheckDeletedKey(i_cTable,0,'TDCODICE',this.w_TDCODICE)
        INSERT INTO (i_cTable);
              (TDCODICE,TDDESCRI,TDDFAULT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TDCODICE;
                  ,this.w_TDDESCRI;
                  ,this.w_TDDFAULT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIP_DIBA_IDX,i_nConn)
      *
      * update TIP_DIBA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIP_DIBA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TDDESCRI="+cp_ToStrODBC(this.w_TDDESCRI)+;
             ",TDDFAULT="+cp_ToStrODBC(this.w_TDDFAULT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIP_DIBA')
        i_cWhere = cp_PKFox(i_cTable  ,'TDCODICE',this.w_TDCODICE  )
        UPDATE (i_cTable) SET;
              TDDESCRI=this.w_TDDESCRI;
             ,TDDFAULT=this.w_TDDFAULT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIP_DIBA_IDX,i_nConn)
      *
      * delete TIP_DIBA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TDCODICE',this.w_TDCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTDCODICE_1_1.value==this.w_TDCODICE)
      this.oPgFrm.Page1.oPag.oTDCODICE_1_1.value=this.w_TDCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESCRI_1_2.value==this.w_TDDESCRI)
      this.oPgFrm.Page1.oPag.oTDDESCRI_1_2.value=this.w_TDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDFAULT_1_5.RadioValue()==this.w_TDDFAULT)
      this.oPgFrm.Page1.oPag.oTDDFAULT_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TIP_DIBA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsds_atd
      with this
        if .bUpdated
          do case
            case .w_TDDFAULT='S' AND (.cFunction='Load' or .cFunction='Edit')
            vq_exec("..\DISB\EXE\QUERY\GSDS_ATD", this, "PREDEF")
            if used("PREDEF") 
              if reccount("PREDEF")>0
               i_bnoChk = .f.
               i_bRes = .f.
               i_cErrorMsg =Ah_MsgFormat("Non pu� esistere pi� di una tipologia predefinita!")
              endif
              use in predef
            endif
          endcase
        endif
      endwith
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsds_atdPag1 as StdContainer
  Width  = 447
  height = 97
  stdWidth  = 447
  stdheight = 97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTDCODICE_1_1 as StdField with uid="UNQROFKMGY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TDCODICE", cQueryName = "TDCODICE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 134878587,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=147, Top=16, InputMask=replicate('X',4)

  add object oTDDESCRI_1_2 as StdField with uid="GBPIUEZYSS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TDDESCRI", cQueryName = "TDDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49292671,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=147, Top=45, InputMask=replicate('X',40)

  add object oTDDFAULT_1_5 as StdCheck with uid="DFBBBPDIOG",rtseq=3,rtrep=.f.,left=147, top=74, caption="Tipologia predef. per caricamento distinte base",;
    HelpContextID = 204397174,;
    cFormVar="w_TDDFAULT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDDFAULT_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTDDFAULT_1_5.GetRadio()
    this.Parent.oContained.w_TDDFAULT = this.RadioValue()
    return .t.
  endfunc

  func oTDDFAULT_1_5.SetRadio()
    this.Parent.oContained.w_TDDFAULT=trim(this.Parent.oContained.w_TDDFAULT)
    this.value = ;
      iif(this.Parent.oContained.w_TDDFAULT=='S',1,;
      0)
  endfunc

  add object oStr_1_3 as StdString with uid="MFRRGESYNC",Visible=.t., Left=3, Top=16,;
    Alignment=1, Width=140, Height=18,;
    Caption="Tipologia distinta base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="GDEALPQHKY",Visible=.t., Left=3, Top=45,;
    Alignment=1, Width=140, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_atd','TIP_DIBA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TDCODICE=TIP_DIBA.TDCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
