* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_apd                                                        *
*              Parametri distinte                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-28                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_apd"))

* --- Class definition
define class tgsds_apd as StdForm
  Top    = 2
  Left   = 14

  * --- Standard Properties
  Width  = 738
  Height = 560+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=159999081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  PAR_DISB_IDX = 0
  AZIENDA_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  cFile = "PAR_DISB"
  cKeySelect = "PDCODAZI"
  cKeyWhere  = "PDCODAZI=this.w_PDCODAZI"
  cKeyWhereODBC = '"PDCODAZI="+cp_ToStrODBC(this.w_PDCODAZI)';

  cKeyWhereODBCqualified = '"PAR_DISB.PDCODAZI="+cp_ToStrODBC(this.w_PDCODAZI)';

  cPrg = "gsds_apd"
  cComment = "Parametri distinte"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PDCODAZI = space(5)
  w_COAZI = space(5)
  w_RAGAZI = space(40)
  w_PDFLVERI = space(1)
  w_PDCOSPAR = space(1)
  w_PDCOECOS = space(1)
  w_PDCAUIMP = space(5)
  w_PDMAGIMP = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_PDCAUORD = space(5)
  w_PDMAGORD = space(5)
  w_PDCAUCAR = space(5)
  w_CAUCOL3 = space(5)
  w_PDCAUSCA = space(5)
  w_CAUCOL4 = space(5)
  w_PDCAUCA2 = space(5)
  w_PDMAGCAR = space(5)
  w_PDCAUSC2 = space(5)
  w_PDMAGSCA = space(5)
  w_DESC1 = space(35)
  w_FLC1 = space(1)
  w_FL1 = space(1)
  w_DESC2 = space(35)
  w_FLC2 = space(1)
  w_FL2 = space(1)
  w_DESC3 = space(35)
  w_FLC3 = space(1)
  w_FL3 = space(1)
  w_DESC4 = space(35)
  w_FLC4 = space(1)
  w_FL4 = space(1)
  w_DESC5 = space(35)
  w_FLC5 = space(1)
  w_FL5 = space(1)
  w_DESC6 = space(35)
  w_FLC6 = space(1)
  w_FL6 = space(1)
  w_CAUCOL6 = space(5)
  w_CAUCOL5 = space(5)
  w_PDCRIVAL = space(1)
  o_PDCRIVAL = space(1)
  w_PDESEINV = space(4)
  w_PDNUMINV = space(6)
  w_PDCODLIS = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_PDMODVAR = space(1)
  w_PDVERMEM = space(1)
  w_PDRICONF = space(1)
  w_PDBLCFGC = space(1)
  w_PDPROLEN = 0
  * --- Area Manuale = Declare Variables
  * --- gsds_apd
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
          oCpToolBar.b9.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_DISB','gsds_apd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_apdPag1","gsds_apd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri distinte")
      .Pages(1).HelpContextID = 240055718
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsds_apd
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab="T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='INVENTAR'
    this.cWorkTables[7]='PAR_DISB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_DISB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_DISB_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PDCODAZI = NVL(PDCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_65_joined
    link_1_65_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_DISB where PDCODAZI=KeySet.PDCODAZI
    *
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_DISB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_DISB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_DISB '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_65_joined=this.AddJoinedLink_1_65(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PDCODAZI',this.w_PDCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_OBTEST = i_datsys
        .w_CAUCOL3 = space(5)
        .w_CAUCOL4 = space(5)
        .w_DESC1 = space(35)
        .w_FLC1 = space(1)
        .w_FL1 = space(1)
        .w_DESC2 = space(35)
        .w_FLC2 = space(1)
        .w_FL2 = space(1)
        .w_DESC3 = space(35)
        .w_FLC3 = space(1)
        .w_FL3 = space(1)
        .w_DESC4 = space(35)
        .w_FLC4 = space(1)
        .w_FL4 = space(1)
        .w_DESC5 = space(35)
        .w_FLC5 = space(1)
        .w_FL5 = space(1)
        .w_DESC6 = space(35)
        .w_FLC6 = space(1)
        .w_FL6 = space(1)
        .w_CAUCOL6 = space(5)
        .w_CAUCOL5 = space(5)
        .w_DATOBSO = ctod("  /  /  ")
        .w_PDCODAZI = NVL(PDCODAZI,space(5))
          if link_1_1_joined
            this.w_PDCODAZI = NVL(AZCODAZI101,NVL(this.w_PDCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COAZI = .w_PDCODAZI
        .w_PDFLVERI = NVL(PDFLVERI,space(1))
        .w_PDCOSPAR = NVL(PDCOSPAR,space(1))
        .w_PDCOECOS = NVL(PDCOECOS,space(1))
        .w_PDCAUIMP = NVL(PDCAUIMP,space(5))
          if link_1_7_joined
            this.w_PDCAUIMP = NVL(CMCODICE107,NVL(this.w_PDCAUIMP,space(5)))
            this.w_DESC1 = NVL(CMDESCRI107,space(35))
            this.w_FLC1 = NVL(CMFLCLFR107,space(1))
            this.w_FL1 = NVL(CMFLIMPE107,space(1))
          else
          .link_1_7('Load')
          endif
        .w_PDMAGIMP = NVL(PDMAGIMP,space(5))
          * evitabile
          *.link_1_8('Load')
        .w_PDCAUORD = NVL(PDCAUORD,space(5))
          if link_1_13_joined
            this.w_PDCAUORD = NVL(CMCODICE113,NVL(this.w_PDCAUORD,space(5)))
            this.w_DESC2 = NVL(CMDESCRI113,space(35))
            this.w_FLC2 = NVL(CMFLCLFR113,space(1))
            this.w_FL2 = NVL(CMFLORDI113,space(1))
          else
          .link_1_13('Load')
          endif
        .w_PDMAGORD = NVL(PDMAGORD,space(5))
          * evitabile
          *.link_1_14('Load')
        .w_PDCAUCAR = NVL(PDCAUCAR,space(5))
          if link_1_15_joined
            this.w_PDCAUCAR = NVL(CMCODICE115,NVL(this.w_PDCAUCAR,space(5)))
            this.w_DESC3 = NVL(CMDESCRI115,space(35))
            this.w_FLC3 = NVL(CMFLCLFR115,space(1))
            this.w_FL3 = NVL(CMFLCASC115,space(1))
            this.w_CAUCOL3 = NVL(CMCAUCOL115,space(5))
          else
          .link_1_15('Load')
          endif
        .w_PDCAUSCA = NVL(PDCAUSCA,space(5))
          if link_1_17_joined
            this.w_PDCAUSCA = NVL(CMCODICE117,NVL(this.w_PDCAUSCA,space(5)))
            this.w_DESC4 = NVL(CMDESCRI117,space(35))
            this.w_FLC4 = NVL(CMFLCLFR117,space(1))
            this.w_FL4 = NVL(CMFLCASC117,space(1))
            this.w_CAUCOL4 = NVL(CMCAUCOL117,space(5))
          else
          .link_1_17('Load')
          endif
        .w_PDCAUCA2 = NVL(PDCAUCA2,space(5))
          if link_1_19_joined
            this.w_PDCAUCA2 = NVL(CMCODICE119,NVL(this.w_PDCAUCA2,space(5)))
            this.w_DESC5 = NVL(CMDESCRI119,space(35))
            this.w_FLC5 = NVL(CMFLCLFR119,space(1))
            this.w_FL5 = NVL(CMFLCASC119,space(1))
            this.w_CAUCOL5 = NVL(CMCAUCOL119,space(5))
          else
          .link_1_19('Load')
          endif
        .w_PDMAGCAR = NVL(PDMAGCAR,space(5))
          * evitabile
          *.link_1_20('Load')
        .w_PDCAUSC2 = NVL(PDCAUSC2,space(5))
          if link_1_21_joined
            this.w_PDCAUSC2 = NVL(CMCODICE121,NVL(this.w_PDCAUSC2,space(5)))
            this.w_DESC6 = NVL(CMDESCRI121,space(35))
            this.w_FLC6 = NVL(CMFLCLFR121,space(1))
            this.w_FL6 = NVL(CMFLCASC121,space(1))
            this.w_CAUCOL6 = NVL(CMCAUCOL121,space(5))
          else
          .link_1_21('Load')
          endif
        .w_PDMAGSCA = NVL(PDMAGSCA,space(5))
          * evitabile
          *.link_1_22('Load')
        .w_PDCRIVAL = NVL(PDCRIVAL,space(1))
        .w_PDESEINV = NVL(PDESEINV,space(4))
          * evitabile
          *.link_1_63('Load')
        .w_PDNUMINV = NVL(PDNUMINV,space(6))
          * evitabile
          *.link_1_64('Load')
        .w_PDCODLIS = NVL(PDCODLIS,space(5))
          if link_1_65_joined
            this.w_PDCODLIS = NVL(LSCODLIS165,NVL(this.w_PDCODLIS,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(LSDTOBSO165),ctod("  /  /  "))
          else
          .link_1_65('Load')
          endif
        .w_PDMODVAR = NVL(PDMODVAR,space(1))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .w_PDVERMEM = NVL(PDVERMEM,space(1))
        .w_PDRICONF = NVL(PDRICONF,space(1))
        .w_PDBLCFGC = NVL(PDBLCFGC,space(1))
        .w_PDPROLEN = NVL(PDPROLEN,0)
        cp_LoadRecExtFlds(this,'PAR_DISB')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsds_apd
    if this.w_PDCODAZI<>i_codazi and not empty(this.w_PDCODAZI)
     this.blankrec()
     this.w_PDCODAZI=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PDCODAZI = space(5)
      .w_COAZI = space(5)
      .w_RAGAZI = space(40)
      .w_PDFLVERI = space(1)
      .w_PDCOSPAR = space(1)
      .w_PDCOECOS = space(1)
      .w_PDCAUIMP = space(5)
      .w_PDMAGIMP = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_PDCAUORD = space(5)
      .w_PDMAGORD = space(5)
      .w_PDCAUCAR = space(5)
      .w_CAUCOL3 = space(5)
      .w_PDCAUSCA = space(5)
      .w_CAUCOL4 = space(5)
      .w_PDCAUCA2 = space(5)
      .w_PDMAGCAR = space(5)
      .w_PDCAUSC2 = space(5)
      .w_PDMAGSCA = space(5)
      .w_DESC1 = space(35)
      .w_FLC1 = space(1)
      .w_FL1 = space(1)
      .w_DESC2 = space(35)
      .w_FLC2 = space(1)
      .w_FL2 = space(1)
      .w_DESC3 = space(35)
      .w_FLC3 = space(1)
      .w_FL3 = space(1)
      .w_DESC4 = space(35)
      .w_FLC4 = space(1)
      .w_FL4 = space(1)
      .w_DESC5 = space(35)
      .w_FLC5 = space(1)
      .w_FL5 = space(1)
      .w_DESC6 = space(35)
      .w_FLC6 = space(1)
      .w_FL6 = space(1)
      .w_CAUCOL6 = space(5)
      .w_CAUCOL5 = space(5)
      .w_PDCRIVAL = space(1)
      .w_PDESEINV = space(4)
      .w_PDNUMINV = space(6)
      .w_PDCODLIS = space(5)
      .w_DATOBSO = ctod("  /  /  ")
      .w_PDMODVAR = space(1)
      .w_PDVERMEM = space(1)
      .w_PDRICONF = space(1)
      .w_PDBLCFGC = space(1)
      .w_PDPROLEN = 0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PDCODAZI))
          .link_1_1('Full')
          endif
        .w_COAZI = .w_PDCODAZI
          .DoRTCalc(3,5,.f.)
        .w_PDCOECOS = 'S'
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_PDCAUIMP))
          .link_1_7('Full')
          endif
        .w_PDMAGIMP = g_MAGAZI
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PDMAGIMP))
          .link_1_8('Full')
          endif
        .w_OBTEST = i_datsys
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_PDCAUORD))
          .link_1_13('Full')
          endif
        .w_PDMAGORD = g_MAGAZI
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_PDMAGORD))
          .link_1_14('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_PDCAUCAR))
          .link_1_15('Full')
          endif
        .DoRTCalc(13,14,.f.)
          if not(empty(.w_PDCAUSCA))
          .link_1_17('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_PDCAUCA2))
          .link_1_19('Full')
          endif
        .w_PDMAGCAR = g_MAGAZI
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_PDMAGCAR))
          .link_1_20('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_PDCAUSC2))
          .link_1_21('Full')
          endif
        .w_PDMAGSCA = g_MAGAZI
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_PDMAGSCA))
          .link_1_22('Full')
          endif
          .DoRTCalc(20,39,.f.)
        .w_PDCRIVAL = 'S'
        .w_PDESEINV = g_CODESE
        .DoRTCalc(41,41,.f.)
          if not(empty(.w_PDESEINV))
          .link_1_63('Full')
          endif
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_PDNUMINV))
          .link_1_64('Full')
          endif
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_PDCODLIS))
          .link_1_65('Full')
          endif
          .DoRTCalc(44,44,.f.)
        .w_PDMODVAR = 'S'
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .w_PDVERMEM = 'N'
        .w_PDRICONF = 'S'
          .DoRTCalc(48,48,.f.)
        .w_PDPROLEN = 5
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_DISB')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsds_apd
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPDFLVERI_1_4.enabled_(i_bVal)
      .Page1.oPag.oPDCOSPAR_1_5.enabled = i_bVal
      .Page1.oPag.oPDCOECOS_1_6.enabled = i_bVal
      .Page1.oPag.oPDCAUIMP_1_7.enabled = i_bVal
      .Page1.oPag.oPDMAGIMP_1_8.enabled = i_bVal
      .Page1.oPag.oPDCAUORD_1_13.enabled = i_bVal
      .Page1.oPag.oPDMAGORD_1_14.enabled = i_bVal
      .Page1.oPag.oPDCAUCAR_1_15.enabled = i_bVal
      .Page1.oPag.oPDCAUSCA_1_17.enabled = i_bVal
      .Page1.oPag.oPDCAUCA2_1_19.enabled = i_bVal
      .Page1.oPag.oPDMAGCAR_1_20.enabled = i_bVal
      .Page1.oPag.oPDCAUSC2_1_21.enabled = i_bVal
      .Page1.oPag.oPDMAGSCA_1_22.enabled = i_bVal
      .Page1.oPag.oPDCRIVAL_1_62.enabled = i_bVal
      .Page1.oPag.oPDESEINV_1_63.enabled = i_bVal
      .Page1.oPag.oPDNUMINV_1_64.enabled = i_bVal
      .Page1.oPag.oPDCODLIS_1_65.enabled = i_bVal
      .Page1.oPag.oPDMODVAR_1_71.enabled = i_bVal
      .Page1.oPag.oPDVERMEM_1_78.enabled = i_bVal
      .Page1.oPag.oPDRICONF_1_79.enabled = i_bVal
      .Page1.oPag.oPDBLCFGC_1_80.enabled = i_bVal
      .Page1.oPag.oPDPROLEN_1_81.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_DISB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODAZI,"PDCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDFLVERI,"PDFLVERI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCOSPAR,"PDCOSPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCOECOS,"PDCOECOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUIMP,"PDCAUIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDMAGIMP,"PDMAGIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUORD,"PDCAUORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDMAGORD,"PDMAGORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUCAR,"PDCAUCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUSCA,"PDCAUSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUCA2,"PDCAUCA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDMAGCAR,"PDMAGCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCAUSC2,"PDCAUSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDMAGSCA,"PDMAGSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCRIVAL,"PDCRIVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDESEINV,"PDESEINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDNUMINV,"PDNUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODLIS,"PDCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDMODVAR,"PDMODVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDVERMEM,"PDVERMEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDRICONF,"PDRICONF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDBLCFGC,"PDBLCFGC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDPROLEN,"PDPROLEN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    i_lTable = "PAR_DISB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_DISB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_DISB_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_DISB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_DISB')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_DISB')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PDCODAZI,PDFLVERI,PDCOSPAR,PDCOECOS,PDCAUIMP"+;
                  ",PDMAGIMP,PDCAUORD,PDMAGORD,PDCAUCAR,PDCAUSCA"+;
                  ",PDCAUCA2,PDMAGCAR,PDCAUSC2,PDMAGSCA,PDCRIVAL"+;
                  ",PDESEINV,PDNUMINV,PDCODLIS,PDMODVAR,PDVERMEM"+;
                  ",PDRICONF,PDBLCFGC,PDPROLEN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PDCODAZI)+;
                  ","+cp_ToStrODBC(this.w_PDFLVERI)+;
                  ","+cp_ToStrODBC(this.w_PDCOSPAR)+;
                  ","+cp_ToStrODBC(this.w_PDCOECOS)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUIMP)+;
                  ","+cp_ToStrODBCNull(this.w_PDMAGIMP)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUORD)+;
                  ","+cp_ToStrODBCNull(this.w_PDMAGORD)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUCAR)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUSCA)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUCA2)+;
                  ","+cp_ToStrODBCNull(this.w_PDMAGCAR)+;
                  ","+cp_ToStrODBCNull(this.w_PDCAUSC2)+;
                  ","+cp_ToStrODBCNull(this.w_PDMAGSCA)+;
                  ","+cp_ToStrODBC(this.w_PDCRIVAL)+;
                  ","+cp_ToStrODBCNull(this.w_PDESEINV)+;
                  ","+cp_ToStrODBCNull(this.w_PDNUMINV)+;
                  ","+cp_ToStrODBCNull(this.w_PDCODLIS)+;
                  ","+cp_ToStrODBC(this.w_PDMODVAR)+;
                  ","+cp_ToStrODBC(this.w_PDVERMEM)+;
                  ","+cp_ToStrODBC(this.w_PDRICONF)+;
                  ","+cp_ToStrODBC(this.w_PDBLCFGC)+;
                  ","+cp_ToStrODBC(this.w_PDPROLEN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_DISB')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_DISB')
        cp_CheckDeletedKey(i_cTable,0,'PDCODAZI',this.w_PDCODAZI)
        INSERT INTO (i_cTable);
              (PDCODAZI,PDFLVERI,PDCOSPAR,PDCOECOS,PDCAUIMP,PDMAGIMP,PDCAUORD,PDMAGORD,PDCAUCAR,PDCAUSCA,PDCAUCA2,PDMAGCAR,PDCAUSC2,PDMAGSCA,PDCRIVAL,PDESEINV,PDNUMINV,PDCODLIS,PDMODVAR,PDVERMEM,PDRICONF,PDBLCFGC,PDPROLEN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PDCODAZI;
                  ,this.w_PDFLVERI;
                  ,this.w_PDCOSPAR;
                  ,this.w_PDCOECOS;
                  ,this.w_PDCAUIMP;
                  ,this.w_PDMAGIMP;
                  ,this.w_PDCAUORD;
                  ,this.w_PDMAGORD;
                  ,this.w_PDCAUCAR;
                  ,this.w_PDCAUSCA;
                  ,this.w_PDCAUCA2;
                  ,this.w_PDMAGCAR;
                  ,this.w_PDCAUSC2;
                  ,this.w_PDMAGSCA;
                  ,this.w_PDCRIVAL;
                  ,this.w_PDESEINV;
                  ,this.w_PDNUMINV;
                  ,this.w_PDCODLIS;
                  ,this.w_PDMODVAR;
                  ,this.w_PDVERMEM;
                  ,this.w_PDRICONF;
                  ,this.w_PDBLCFGC;
                  ,this.w_PDPROLEN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_DISB_IDX,i_nConn)
      *
      * update PAR_DISB
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_DISB')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PDFLVERI="+cp_ToStrODBC(this.w_PDFLVERI)+;
             ",PDCOSPAR="+cp_ToStrODBC(this.w_PDCOSPAR)+;
             ",PDCOECOS="+cp_ToStrODBC(this.w_PDCOECOS)+;
             ",PDCAUIMP="+cp_ToStrODBCNull(this.w_PDCAUIMP)+;
             ",PDMAGIMP="+cp_ToStrODBCNull(this.w_PDMAGIMP)+;
             ",PDCAUORD="+cp_ToStrODBCNull(this.w_PDCAUORD)+;
             ",PDMAGORD="+cp_ToStrODBCNull(this.w_PDMAGORD)+;
             ",PDCAUCAR="+cp_ToStrODBCNull(this.w_PDCAUCAR)+;
             ",PDCAUSCA="+cp_ToStrODBCNull(this.w_PDCAUSCA)+;
             ",PDCAUCA2="+cp_ToStrODBCNull(this.w_PDCAUCA2)+;
             ",PDMAGCAR="+cp_ToStrODBCNull(this.w_PDMAGCAR)+;
             ",PDCAUSC2="+cp_ToStrODBCNull(this.w_PDCAUSC2)+;
             ",PDMAGSCA="+cp_ToStrODBCNull(this.w_PDMAGSCA)+;
             ",PDCRIVAL="+cp_ToStrODBC(this.w_PDCRIVAL)+;
             ",PDESEINV="+cp_ToStrODBCNull(this.w_PDESEINV)+;
             ",PDNUMINV="+cp_ToStrODBCNull(this.w_PDNUMINV)+;
             ",PDCODLIS="+cp_ToStrODBCNull(this.w_PDCODLIS)+;
             ",PDMODVAR="+cp_ToStrODBC(this.w_PDMODVAR)+;
             ",PDVERMEM="+cp_ToStrODBC(this.w_PDVERMEM)+;
             ",PDRICONF="+cp_ToStrODBC(this.w_PDRICONF)+;
             ",PDBLCFGC="+cp_ToStrODBC(this.w_PDBLCFGC)+;
             ",PDPROLEN="+cp_ToStrODBC(this.w_PDPROLEN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_DISB')
        i_cWhere = cp_PKFox(i_cTable  ,'PDCODAZI',this.w_PDCODAZI  )
        UPDATE (i_cTable) SET;
              PDFLVERI=this.w_PDFLVERI;
             ,PDCOSPAR=this.w_PDCOSPAR;
             ,PDCOECOS=this.w_PDCOECOS;
             ,PDCAUIMP=this.w_PDCAUIMP;
             ,PDMAGIMP=this.w_PDMAGIMP;
             ,PDCAUORD=this.w_PDCAUORD;
             ,PDMAGORD=this.w_PDMAGORD;
             ,PDCAUCAR=this.w_PDCAUCAR;
             ,PDCAUSCA=this.w_PDCAUSCA;
             ,PDCAUCA2=this.w_PDCAUCA2;
             ,PDMAGCAR=this.w_PDMAGCAR;
             ,PDCAUSC2=this.w_PDCAUSC2;
             ,PDMAGSCA=this.w_PDMAGSCA;
             ,PDCRIVAL=this.w_PDCRIVAL;
             ,PDESEINV=this.w_PDESEINV;
             ,PDNUMINV=this.w_PDNUMINV;
             ,PDCODLIS=this.w_PDCODLIS;
             ,PDMODVAR=this.w_PDMODVAR;
             ,PDVERMEM=this.w_PDVERMEM;
             ,PDRICONF=this.w_PDRICONF;
             ,PDBLCFGC=this.w_PDBLCFGC;
             ,PDPROLEN=this.w_PDPROLEN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_DISB_IDX,i_nConn)
      *
      * delete PAR_DISB
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PDCODAZI',this.w_PDCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
            .w_COAZI = .w_PDCODAZI
        .DoRTCalc(3,40,.t.)
        if .o_PDCRIVAL<>.w_PDCRIVAL
          .link_1_63('Full')
        endif
        .DoRTCalc(42,42,.t.)
        if .o_PDCRIVAL<>.w_PDCRIVAL
          .link_1_65('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(44,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPDESEINV_1_63.enabled = this.oPgFrm.Page1.oPag.oPDESEINV_1_63.mCond()
    this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.enabled = this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.mCond()
    this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.enabled = this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.mCond()
    this.oPgFrm.Page1.oPag.oPDVERMEM_1_78.enabled = this.oPgFrm.Page1.oPag.oPDVERMEM_1_78.mCond()
    this.oPgFrm.Page1.oPag.oPDRICONF_1_79.enabled = this.oPgFrm.Page1.oPag.oPDRICONF_1_79.mCond()
    this.oPgFrm.Page1.oPag.oPDBLCFGC_1_80.enabled = this.oPgFrm.Page1.oPag.oPDBLCFGC_1_80.mCond()
    this.oPgFrm.Page1.oPag.oPDPROLEN_1_81.enabled = this.oPgFrm.Page1.oPag.oPDPROLEN_1_81.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPDESEINV_1_63.visible=!this.oPgFrm.Page1.oPag.oPDESEINV_1_63.mHide()
    this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.visible=!this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.mHide()
    this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.visible=!this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PDCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PDCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PDCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PAR_DISB.PDCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCAUIMP
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUIMP))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUIMP)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUIMP) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUIMP_1_7'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUIMP)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUIMP = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC1 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC1 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL1 = NVL(_Link_.CMFLIMPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUIMP = space(5)
      endif
      this.w_DESC1 = space(35)
      this.w_FLC1 = space(1)
      this.w_FL1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUIMP) OR (.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale impegno componenti inesistente o incongruente")
        endif
        this.w_PDCAUIMP = space(5)
        this.w_DESC1 = space(35)
        this.w_FLC1 = space(1)
        this.w_FL1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CMCODICE as CMCODICE107"+ ",link_1_7.CMDESCRI as CMDESCRI107"+ ",link_1_7.CMFLCLFR as CMFLCLFR107"+ ",link_1_7.CMFLIMPE as CMFLIMPE107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on PAR_DISB.PDCAUIMP=link_1_7.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUIMP=link_1_7.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDMAGIMP
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGIMP))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGIMP)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGIMP) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGIMP_1_8'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGIMP)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGIMP = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGIMP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCAUORD
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUORD)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUORD))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUORD)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUORD) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUORD_1_13'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUORD)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUORD = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC2 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL2 = NVL(_Link_.CMFLORDI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUORD = space(5)
      endif
      this.w_DESC2 = space(35)
      this.w_FLC2 = space(1)
      this.w_FL2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUORD) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale ordine prodotti inesistente o incongruente")
        endif
        this.w_PDCAUORD = space(5)
        this.w_DESC2 = space(35)
        this.w_FLC2 = space(1)
        this.w_FL2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.CMCODICE as CMCODICE113"+ ",link_1_13.CMDESCRI as CMDESCRI113"+ ",link_1_13.CMFLCLFR as CMFLCLFR113"+ ",link_1_13.CMFLORDI as CMFLORDI113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on PAR_DISB.PDCAUORD=link_1_13.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUORD=link_1_13.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDMAGORD
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGORD)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGORD))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGORD)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGORD) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGORD_1_14'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGORD)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGORD = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGORD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCAUCAR
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUCAR))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUCAR)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUCAR) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUCAR_1_15'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS1APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUCAR)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUCAR = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC3 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC3 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL3 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUCOL3 = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUCAR = space(5)
      endif
      this.w_DESC3 = space(35)
      this.w_FLC3 = space(1)
      this.w_FL3 = space(1)
      this.w_CAUCOL3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUCAR) OR (.w_FL3 $ '+' AND NOT .w_FLC3 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale carico prodotto + evasione ordine inesistente o incongruente")
        endif
        this.w_PDCAUCAR = space(5)
        this.w_DESC3 = space(35)
        this.w_FLC3 = space(1)
        this.w_FL3 = space(1)
        this.w_CAUCOL3 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.CMCODICE as CMCODICE115"+ ",link_1_15.CMDESCRI as CMDESCRI115"+ ",link_1_15.CMFLCLFR as CMFLCLFR115"+ ",link_1_15.CMFLCASC as CMFLCASC115"+ ",link_1_15.CMCAUCOL as CMCAUCOL115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on PAR_DISB.PDCAUCAR=link_1_15.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUCAR=link_1_15.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCAUSCA
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUSCA))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUSCA)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUSCA) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUSCA_1_17'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS1APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUSCA)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUSCA = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC4 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC4 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL4 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUCOL4 = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUSCA = space(5)
      endif
      this.w_DESC4 = space(35)
      this.w_FLC4 = space(1)
      this.w_FL4 = space(1)
      this.w_CAUCOL4 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUSCA) OR (.w_FL4 $ '-' AND NOT .w_FLC4 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale scarico componenti + evasione impegni inesistente o incongruente")
        endif
        this.w_PDCAUSCA = space(5)
        this.w_DESC4 = space(35)
        this.w_FLC4 = space(1)
        this.w_FL4 = space(1)
        this.w_CAUCOL4 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.CMCODICE as CMCODICE117"+ ",link_1_17.CMDESCRI as CMDESCRI117"+ ",link_1_17.CMFLCLFR as CMFLCLFR117"+ ",link_1_17.CMFLCASC as CMFLCASC117"+ ",link_1_17.CMCAUCOL as CMCAUCOL117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on PAR_DISB.PDCAUSCA=link_1_17.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUSCA=link_1_17.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDCAUCA2
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUCA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUCA2)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUCA2))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUCA2)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUCA2) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUCA2_1_19'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUCA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUCA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUCA2)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUCA2 = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC5 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC5 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL5 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUCOL5 = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUCA2 = space(5)
      endif
      this.w_DESC5 = space(35)
      this.w_FLC5 = space(1)
      this.w_FL5 = space(1)
      this.w_CAUCOL5 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUCA2) OR (.w_FL5 $ '+' AND NOT .w_FLC5 $ 'CF' AND EMPTY(.w_CAUCOL5))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale carico prodotto inesistente o incongruente")
        endif
        this.w_PDCAUCA2 = space(5)
        this.w_DESC5 = space(35)
        this.w_FLC5 = space(1)
        this.w_FL5 = space(1)
        this.w_CAUCOL5 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUCA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.CMCODICE as CMCODICE119"+ ",link_1_19.CMDESCRI as CMDESCRI119"+ ",link_1_19.CMFLCLFR as CMFLCLFR119"+ ",link_1_19.CMFLCASC as CMFLCASC119"+ ",link_1_19.CMCAUCOL as CMCAUCOL119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on PAR_DISB.PDCAUCA2=link_1_19.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUCA2=link_1_19.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDMAGCAR
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGCAR))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGCAR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGCAR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGCAR_1_20'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGCAR)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGCAR = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGCAR = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCAUSC2
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCAUSC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PDCAUSC2)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PDCAUSC2))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCAUSC2)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCAUSC2) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPDCAUSC2_1_21'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCAUSC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PDCAUSC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PDCAUSC2)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCAUSC2 = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC6 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC6 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL6 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUCOL6 = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDCAUSC2 = space(5)
      endif
      this.w_DESC6 = space(35)
      this.w_FLC6 = space(1)
      this.w_FL6 = space(1)
      this.w_CAUCOL6 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PDCAUSC2) OR (.w_FL6 $ '-' AND NOT .w_FLC6 $ 'CF' AND EMPTY(.w_CAUCOL6))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale scarico componenti inesistente o incongruente")
        endif
        this.w_PDCAUSC2 = space(5)
        this.w_DESC6 = space(35)
        this.w_FLC6 = space(1)
        this.w_FL6 = space(1)
        this.w_CAUCOL6 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCAUSC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.CMCODICE as CMCODICE121"+ ",link_1_21.CMDESCRI as CMDESCRI121"+ ",link_1_21.CMFLCLFR as CMFLCLFR121"+ ",link_1_21.CMFLCASC as CMFLCASC121"+ ",link_1_21.CMCAUCOL as CMCAUCOL121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on PAR_DISB.PDCAUSC2=link_1_21.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCAUSC2=link_1_21.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PDMAGSCA
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGSCA))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGSCA)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGSCA) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGSCA_1_22'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGSCA)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGSCA = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGSCA = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDESEINV
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDESEINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PDESEINV)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_PDCODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_PDCODAZI;
                     ,'ESCODESE',trim(this.w_PDESEINV))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDESEINV)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDESEINV) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPDESEINV_1_63'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDCODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_PDCODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDESEINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PDESEINV);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_PDCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_PDCODAZI;
                       ,'ESCODESE',this.w_PDESEINV)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDESEINV = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_PDESEINV = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDESEINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDNUMINV
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDNUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_PDNUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_PDESEINV);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_PDESEINV;
                     ,'INNUMINV',trim(this.w_PDNUMINV))
          select INCODESE,INNUMINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDNUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDNUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oPDNUMINV_1_64'),i_cWhere,'',"Elenco inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDESEINV<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Numero inventario errato o mancante")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_PDESEINV);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDNUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_PDNUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_PDESEINV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_PDESEINV;
                       ,'INNUMINV',this.w_PDNUMINV)
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDNUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_PDESEINV = NVL(_Link_.INCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_PDNUMINV = space(6)
      endif
      this.w_PDESEINV = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDNUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODLIS
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PDCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_PDCODLIS))
          select LSCODLIS,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oPDCODLIS_1_65'),i_cWhere,'GSAR_ALI',"Elenco listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PDCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PDCODLIS)
            select LSCODLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODLIS = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino inesistente o valuta listino obsoleta")
        endif
        this.w_PDCODLIS = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_65(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_65.LSCODLIS as LSCODLIS165"+ ",link_1_65.LSDTOBSO as LSDTOBSO165"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_65 on PAR_DISB.PDCODLIS=link_1_65.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_65"
          i_cKey=i_cKey+'+" and PAR_DISB.PDCODLIS=link_1_65.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOAZI_1_2.value==this.w_COAZI)
      this.oPgFrm.Page1.oPag.oCOAZI_1_2.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_3.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_3.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFLVERI_1_4.RadioValue()==this.w_PDFLVERI)
      this.oPgFrm.Page1.oPag.oPDFLVERI_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOSPAR_1_5.RadioValue()==this.w_PDCOSPAR)
      this.oPgFrm.Page1.oPag.oPDCOSPAR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOECOS_1_6.RadioValue()==this.w_PDCOECOS)
      this.oPgFrm.Page1.oPag.oPDCOECOS_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUIMP_1_7.value==this.w_PDCAUIMP)
      this.oPgFrm.Page1.oPag.oPDCAUIMP_1_7.value=this.w_PDCAUIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGIMP_1_8.value==this.w_PDMAGIMP)
      this.oPgFrm.Page1.oPag.oPDMAGIMP_1_8.value=this.w_PDMAGIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUORD_1_13.value==this.w_PDCAUORD)
      this.oPgFrm.Page1.oPag.oPDCAUORD_1_13.value=this.w_PDCAUORD
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGORD_1_14.value==this.w_PDMAGORD)
      this.oPgFrm.Page1.oPag.oPDMAGORD_1_14.value=this.w_PDMAGORD
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUCAR_1_15.value==this.w_PDCAUCAR)
      this.oPgFrm.Page1.oPag.oPDCAUCAR_1_15.value=this.w_PDCAUCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUCOL3_1_16.value==this.w_CAUCOL3)
      this.oPgFrm.Page1.oPag.oCAUCOL3_1_16.value=this.w_CAUCOL3
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUSCA_1_17.value==this.w_PDCAUSCA)
      this.oPgFrm.Page1.oPag.oPDCAUSCA_1_17.value=this.w_PDCAUSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUCOL4_1_18.value==this.w_CAUCOL4)
      this.oPgFrm.Page1.oPag.oCAUCOL4_1_18.value=this.w_CAUCOL4
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUCA2_1_19.value==this.w_PDCAUCA2)
      this.oPgFrm.Page1.oPag.oPDCAUCA2_1_19.value=this.w_PDCAUCA2
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGCAR_1_20.value==this.w_PDMAGCAR)
      this.oPgFrm.Page1.oPag.oPDMAGCAR_1_20.value=this.w_PDMAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCAUSC2_1_21.value==this.w_PDCAUSC2)
      this.oPgFrm.Page1.oPag.oPDCAUSC2_1_21.value=this.w_PDCAUSC2
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGSCA_1_22.value==this.w_PDMAGSCA)
      this.oPgFrm.Page1.oPag.oPDMAGSCA_1_22.value=this.w_PDMAGSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_29.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_29.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_32.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_32.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC3_1_35.value==this.w_DESC3)
      this.oPgFrm.Page1.oPag.oDESC3_1_35.value=this.w_DESC3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC4_1_38.value==this.w_DESC4)
      this.oPgFrm.Page1.oPag.oDESC4_1_38.value=this.w_DESC4
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC5_1_41.value==this.w_DESC5)
      this.oPgFrm.Page1.oPag.oDESC5_1_41.value=this.w_DESC5
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC6_1_44.value==this.w_DESC6)
      this.oPgFrm.Page1.oPag.oDESC6_1_44.value=this.w_DESC6
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCRIVAL_1_62.RadioValue()==this.w_PDCRIVAL)
      this.oPgFrm.Page1.oPag.oPDCRIVAL_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDESEINV_1_63.value==this.w_PDESEINV)
      this.oPgFrm.Page1.oPag.oPDESEINV_1_63.value=this.w_PDESEINV
    endif
    if not(this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.value==this.w_PDNUMINV)
      this.oPgFrm.Page1.oPag.oPDNUMINV_1_64.value=this.w_PDNUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.value==this.w_PDCODLIS)
      this.oPgFrm.Page1.oPag.oPDCODLIS_1_65.value=this.w_PDCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMODVAR_1_71.RadioValue()==this.w_PDMODVAR)
      this.oPgFrm.Page1.oPag.oPDMODVAR_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDVERMEM_1_78.RadioValue()==this.w_PDVERMEM)
      this.oPgFrm.Page1.oPag.oPDVERMEM_1_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDRICONF_1_79.RadioValue()==this.w_PDRICONF)
      this.oPgFrm.Page1.oPag.oPDRICONF_1_79.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDBLCFGC_1_80.RadioValue()==this.w_PDBLCFGC)
      this.oPgFrm.Page1.oPag.oPDBLCFGC_1_80.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDPROLEN_1_81.value==this.w_PDPROLEN)
      this.oPgFrm.Page1.oPag.oPDPROLEN_1_81.value=this.w_PDPROLEN
    endif
    cp_SetControlsValueExtFlds(this,'PAR_DISB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_PDCAUIMP) OR (.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF'))  and not(empty(.w_PDCAUIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUIMP_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale impegno componenti inesistente o incongruente")
          case   not(EMPTY(.w_PDCAUORD) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF'))  and not(empty(.w_PDCAUORD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUORD_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale ordine prodotti inesistente o incongruente")
          case   not(EMPTY(.w_PDCAUCAR) OR (.w_FL3 $ '+' AND NOT .w_FLC3 $ 'CF'))  and not(empty(.w_PDCAUCAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUCAR_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale carico prodotto + evasione ordine inesistente o incongruente")
          case   not(EMPTY(.w_PDCAUSCA) OR (.w_FL4 $ '-' AND NOT .w_FLC4 $ 'CF'))  and not(empty(.w_PDCAUSCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUSCA_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale scarico componenti + evasione impegni inesistente o incongruente")
          case   not(EMPTY(.w_PDCAUCA2) OR (.w_FL5 $ '+' AND NOT .w_FLC5 $ 'CF' AND EMPTY(.w_CAUCOL5)))  and not(empty(.w_PDCAUCA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUCA2_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale carico prodotto inesistente o incongruente")
          case   not(EMPTY(.w_PDCAUSC2) OR (.w_FL6 $ '-' AND NOT .w_FLC6 $ 'CF' AND EMPTY(.w_CAUCOL6)))  and not(empty(.w_PDCAUSC2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCAUSC2_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale scarico componenti inesistente o incongruente")
          case   (empty(.w_PDESEINV))  and not(NOT .w_PDCRIVAL $ 'SMUP')  and (.w_PDCRIVAL $ 'SMUP')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDESEINV_1_63.SetFocus()
            i_bnoObbl = !empty(.w_PDESEINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_PDCRIVAL<>'L')  and (.w_PDCRIVAL='L')  and not(empty(.w_PDCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODLIS_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino inesistente o valuta listino obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDCRIVAL = this.w_PDCRIVAL
    return

enddefine

* --- Define pages as container
define class tgsds_apdPag1 as StdContainer
  Width  = 734
  height = 560
  stdWidth  = 734
  stdheight = 560
  resizeXpos=438
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_1_2 as StdField with uid="DWXKLHFQDX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 77267162,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=231, Top=13, InputMask=replicate('X',5)

  add object oRAGAZI_1_3 as StdField with uid="PNXYUNMGRK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89936406,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=296, Top=13, InputMask=replicate('X',40)

  add object oPDFLVERI_1_4 as StdRadio with uid="MTWTMPRVDH",rtseq=4,rtrep=.f.,left=410, top=41, width=168,height=35;
    , ToolTipText = "Se attivo: esegue la verifica di fattibilit� sulla disponibilit� contabile anzich� effettiva";
    , cFormVar="w_PDFLVERI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPDFLVERI_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Effettiva"
      this.Buttons(1).HelpContextID = 19350847
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Contabile"
      this.Buttons(2).HelpContextID = 19350847
      this.Buttons(2).Top=16
      this.SetAll("Width",166)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Se attivo: esegue la verifica di fattibilit� sulla disponibilit� contabile anzich� effettiva")
      StdRadio::init()
    endproc

  func oPDFLVERI_1_4.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oPDFLVERI_1_4.GetRadio()
    this.Parent.oContained.w_PDFLVERI = this.RadioValue()
    return .t.
  endfunc

  func oPDFLVERI_1_4.SetRadio()
    this.Parent.oContained.w_PDFLVERI=trim(this.Parent.oContained.w_PDFLVERI)
    this.value = ;
      iif(this.Parent.oContained.w_PDFLVERI=='E',1,;
      iif(this.Parent.oContained.w_PDFLVERI=='C',2,;
      0))
  endfunc

  add object oPDCOSPAR_1_5 as StdCheck with uid="GSINAFYIFM",rtseq=5,rtrep=.f.,left=410, top=76, caption="Costificazione parziale distinta",;
    ToolTipText = "Se attivo: la costificazione avverr� in funzione del check esplosione impostato in distinta",;
    HelpContextID = 200938824,;
    cFormVar="w_PDCOSPAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCOSPAR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPDCOSPAR_1_5.GetRadio()
    this.Parent.oContained.w_PDCOSPAR = this.RadioValue()
    return .t.
  endfunc

  func oPDCOSPAR_1_5.SetRadio()
    this.Parent.oContained.w_PDCOSPAR=trim(this.Parent.oContained.w_PDCOSPAR)
    this.value = ;
      iif(this.Parent.oContained.w_PDCOSPAR=='S',1,;
      0)
  endfunc

  add object oPDCOECOS_1_6 as StdCheck with uid="WHCLBVISGJ",rtseq=6,rtrep=.f.,left=410, top=100, caption="Considera coeff. impiego in fase di costificazione",;
    HelpContextID = 31845047,;
    cFormVar="w_PDCOECOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCOECOS_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPDCOECOS_1_6.GetRadio()
    this.Parent.oContained.w_PDCOECOS = this.RadioValue()
    return .t.
  endfunc

  func oPDCOECOS_1_6.SetRadio()
    this.Parent.oContained.w_PDCOECOS=trim(this.Parent.oContained.w_PDCOECOS)
    this.value = ;
      iif(this.Parent.oContained.w_PDCOECOS=='S',1,;
      0)
  endfunc

  add object oPDCAUIMP_1_7 as StdField with uid="SGHORRHFKB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDCAUIMP", cQueryName = "PDCAUIMP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale impegno componenti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di impegno componenti",;
    HelpContextID = 183757498,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=145, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUIMP"

  func oPDCAUIMP_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUIMP_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUIMP_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUIMP_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUIMP_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUIMP
     i_obj.ecpSave()
  endproc

  add object oPDMAGIMP_1_8 as StdField with uid="CKYQFXUYOM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PDMAGIMP", cQueryName = "PDMAGIMP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di impegno componenti",;
    HelpContextID = 198396602,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=145, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGIMP"

  func oPDMAGIMP_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGIMP_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGIMP_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGIMP_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oPDMAGIMP_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PDMAGIMP
     i_obj.ecpSave()
  endproc

  add object oPDCAUORD_1_13 as StdField with uid="PDSZOLVTKT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PDCAUORD", cQueryName = "PDCAUORD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale ordine prodotti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di ordine prodotto finito",;
    HelpContextID = 185341242,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUORD"

  func oPDCAUORD_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUORD_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUORD_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUORD_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUORD_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUORD
     i_obj.ecpSave()
  endproc

  add object oPDMAGORD_1_14 as StdField with uid="ZOIIXGWJDR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PDMAGORD", cQueryName = "PDMAGORD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di ordine prodotti",;
    HelpContextID = 170702138,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGORD"

  func oPDMAGORD_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGORD_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGORD_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGORD_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oPDMAGORD_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PDMAGORD
     i_obj.ecpSave()
  endproc

  add object oPDCAUCAR_1_15 as StdField with uid="IPUHCUXORH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PDCAUCAR", cQueryName = "PDCAUCAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale carico prodotto + evasione ordine inesistente o incongruente",;
    ToolTipText = "Causale magazzino di carico prodotto e evasione ordine",;
    HelpContextID = 252450120,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUCAR"

  func oPDCAUCAR_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUCAR_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUCAR_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUCAR_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS1APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUCAR_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUCAR
     i_obj.ecpSave()
  endproc

  add object oCAUCOL3_1_16 as StdField with uid="CNODJFACYC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAUCOL3", cQueryName = "CAUCOL3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale collegata di evasione ordine prodotti",;
    HelpContextID = 128921894,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=231, InputMask=replicate('X',5)

  add object oPDCAUSCA_1_17 as StdField with uid="SNPOECUXCY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PDCAUSCA", cQueryName = "PDCAUSCA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale scarico componenti + evasione impegni inesistente o incongruente",;
    ToolTipText = "Causale magazzino di scarico componenti e evasione impegni",;
    HelpContextID = 252450103,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUSCA"

  func oPDCAUSCA_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUSCA_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUSCA_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUSCA_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS1APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUSCA_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUSCA
     i_obj.ecpSave()
  endproc

  add object oCAUCOL4_1_18 as StdField with uid="DOXASMCOOD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CAUCOL4", cQueryName = "CAUCOL4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale collegata di evasione impegno componenti",;
    HelpContextID = 128921894,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=259, InputMask=replicate('X',5)

  add object oPDCAUCA2_1_19 as StdField with uid="PPKYEOGMAK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PDCAUCA2", cQueryName = "PDCAUCA2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale carico prodotto inesistente o incongruente",;
    ToolTipText = "Causale magazzino di carico prodotto",;
    HelpContextID = 252450088,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=317, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUCA2"

  func oPDCAUCA2_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUCA2_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUCA2_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUCA2_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUCA2_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUCA2
     i_obj.ecpSave()
  endproc

  add object oPDMAGCAR_1_20 as StdField with uid="AMNOFAYYVH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PDMAGCAR", cQueryName = "PDMAGCAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di carico prodotti",;
    HelpContextID = 237811016,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=317, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGCAR"

  func oPDMAGCAR_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGCAR_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGCAR_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGCAR_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oPDMAGCAR_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PDMAGCAR
     i_obj.ecpSave()
  endproc

  add object oPDCAUSC2_1_21 as StdField with uid="VNUJQDZHGL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PDCAUSC2", cQueryName = "PDCAUSC2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale scarico componenti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di scarico componenti",;
    HelpContextID = 252450088,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=231, Top=345, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_PDCAUSC2"

  func oPDCAUSC2_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCAUSC2_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCAUSC2_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPDCAUSC2_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPDCAUSC2_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PDCAUSC2
     i_obj.ecpSave()
  endproc

  add object oPDMAGSCA_1_22 as StdField with uid="GZXTGCEXMF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PDMAGSCA", cQueryName = "PDMAGSCA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di scarico componenti",;
    HelpContextID = 237810999,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=649, Top=345, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGSCA"

  func oPDMAGSCA_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGSCA_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGSCA_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGSCA_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oPDMAGSCA_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PDMAGSCA
     i_obj.ecpSave()
  endproc

  add object oDESC1_1_29 as StdField with uid="IHJASYASFK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103869130,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=145, InputMask=replicate('X',35)

  add object oDESC2_1_32 as StdField with uid="DOSKLSEMDQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 102820554,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=173, InputMask=replicate('X',35)

  add object oDESC3_1_35 as StdField with uid="KZHZDGUWOL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESC3", cQueryName = "DESC3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 101771978,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=231, InputMask=replicate('X',35)

  add object oDESC4_1_38 as StdField with uid="EIUTUOCYUL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESC4", cQueryName = "DESC4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100723402,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=259, InputMask=replicate('X',35)

  add object oDESC5_1_41 as StdField with uid="CIBIKROADT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESC5", cQueryName = "DESC5",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99674826,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=317, InputMask=replicate('X',35)

  add object oDESC6_1_44 as StdField with uid="ZNURRYEEKE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESC6", cQueryName = "DESC6",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 98626250,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=296, Top=345, InputMask=replicate('X',35)


  add object oPDCRIVAL_1_62 as StdCombo with uid="YRVEYNBLOI",rtseq=40,rtrep=.f.,left=231,top=410,width=201,height=21;
    , ToolTipText = "Criterio di valorizzazione delle distinte";
    , HelpContextID = 22877506;
    , cFormVar="w_PDCRIVAL",RowSource=""+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Costo di listino,"+"Ultimo costo standard (articolo),"+"Ultimo costo dei saldi (articolo),"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDCRIVAL_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'U',;
    iif(this.value =5,'L',;
    iif(this.value =6,'X',;
    iif(this.value =7,'A',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oPDCRIVAL_1_62.GetRadio()
    this.Parent.oContained.w_PDCRIVAL = this.RadioValue()
    return .t.
  endfunc

  func oPDCRIVAL_1_62.SetRadio()
    this.Parent.oContained.w_PDCRIVAL=trim(this.Parent.oContained.w_PDCRIVAL)
    this.value = ;
      iif(this.Parent.oContained.w_PDCRIVAL=='S',1,;
      iif(this.Parent.oContained.w_PDCRIVAL=='M',2,;
      iif(this.Parent.oContained.w_PDCRIVAL=='P',3,;
      iif(this.Parent.oContained.w_PDCRIVAL=='U',4,;
      iif(this.Parent.oContained.w_PDCRIVAL=='L',5,;
      iif(this.Parent.oContained.w_PDCRIVAL=='X',6,;
      iif(this.Parent.oContained.w_PDCRIVAL=='A',7,;
      iif(this.Parent.oContained.w_PDCRIVAL=='N',8,;
      0))))))))
  endfunc

  add object oPDESEINV_1_63 as StdField with uid="PPCQXSFUWA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PDESEINV", cQueryName = "PDESEINV",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario da considerare per la valorizzazione delle distinte",;
    HelpContextID = 199346868,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=512, Top=410, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_PDCODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PDESEINV"

  func oPDESEINV_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDCRIVAL $ 'SMUP')
    endwith
   endif
  endfunc

  func oPDESEINV_1_63.mHide()
    with this.Parent.oContained
      return (NOT .w_PDCRIVAL $ 'SMUP')
    endwith
  endfunc

  func oPDESEINV_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_63('Part',this)
      if .not. empty(.w_PDNUMINV)
        bRes2=.link_1_64('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPDESEINV_1_63.ecpDrop(oSource)
    this.Parent.oContained.link_1_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDESEINV_1_63.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_PDCODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_PDCODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPDESEINV_1_63'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPDNUMINV_1_64 as StdField with uid="TLVSQNVIAZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PDNUMINV", cQueryName = "PDNUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Numero inventario errato o mancante",;
    ToolTipText = "Inventario da considerare per la valorizzazione delle distinte",;
    HelpContextID = 190790324,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=649, Top=410, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", oKey_1_1="INCODESE", oKey_1_2="this.w_PDESEINV", oKey_2_1="INNUMINV", oKey_2_2="this.w_PDNUMINV"

  func oPDNUMINV_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDCRIVAL $ 'SMUP' AND NOT EMPTY(.w_PDESEINV))
    endwith
   endif
  endfunc

  func oPDNUMINV_1_64.mHide()
    with this.Parent.oContained
      return (NOT .w_PDCRIVAL $ 'SMUP')
    endwith
  endfunc

  func oPDNUMINV_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDNUMINV_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDNUMINV_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_PDESEINV)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_PDESEINV)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oPDNUMINV_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco inventari",'',this.parent.oContained
  endproc

  add object oPDCODLIS_1_65 as StdField with uid="AGOZDBGXAR",rtseq=43,rtrep=.f.,;
    cFormVar = "w_PDCODLIS", cQueryName = "PDCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino inesistente o valuta listino obsoleta",;
    ToolTipText = "Codice listino da considerare per la valorizzazione delle distinte",;
    HelpContextID = 150334135,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=512, Top=410, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PDCODLIS"

  func oPDCODLIS_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDCRIVAL='L')
    endwith
   endif
  endfunc

  func oPDCODLIS_1_65.mHide()
    with this.Parent.oContained
      return (.w_PDCRIVAL<>'L')
    endwith
  endfunc

  func oPDCODLIS_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODLIS_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODLIS_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oPDCODLIS_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Elenco listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oPDCODLIS_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_PDCODLIS
     i_obj.ecpSave()
  endproc


  add object oPDMODVAR_1_71 as StdCombo with uid="UBUPHWAZWP",rtseq=45,rtrep=.f.,left=231,top=460,width=138,height=21;
    , ToolTipText = "Modalit� utilizzo varianti (padre supremo o di livello)";
    , HelpContextID = 17478984;
    , cFormVar="w_PDMODVAR",RowSource=""+"Padre supremo,"+"Padre di livello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDMODVAR_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'L',;
    space(1))))
  endfunc
  func oPDMODVAR_1_71.GetRadio()
    this.Parent.oContained.w_PDMODVAR = this.RadioValue()
    return .t.
  endfunc

  func oPDMODVAR_1_71.SetRadio()
    this.Parent.oContained.w_PDMODVAR=trim(this.Parent.oContained.w_PDMODVAR)
    this.value = ;
      iif(this.Parent.oContained.w_PDMODVAR=='S',1,;
      iif(this.Parent.oContained.w_PDMODVAR=='L',2,;
      0))
  endfunc


  add object oObj_1_74 as cp_runprogram with uid="HVPSLVASAW",left=-2, top=576, width=236,height=19,;
    caption='GSDS_BPD',;
   bGlobalFont=.t.,;
    prg="GSDS_BPD",;
    cEvent = "w_PDMODVAR Changed",;
    nPag=1;
    , HelpContextID = 21089366


  add object oPDVERMEM_1_78 as StdCombo with uid="YLIXCHPQJG",rtseq=46,rtrep=.f.,left=231,top=507,width=152,height=21;
    , ToolTipText = "Determina il tipo di generazione del codice articolo";
    , HelpContextID = 148981059;
    , cFormVar="w_PDVERMEM",RowSource=""+"Legami,"+"Legami+Configurazione,"+"Nessuna verifica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDVERMEM_1_78.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPDVERMEM_1_78.GetRadio()
    this.Parent.oContained.w_PDVERMEM = this.RadioValue()
    return .t.
  endfunc

  func oPDVERMEM_1_78.SetRadio()
    this.Parent.oContained.w_PDVERMEM=trim(this.Parent.oContained.w_PDVERMEM)
    this.value = ;
      iif(this.Parent.oContained.w_PDVERMEM=='N',1,;
      iif(this.Parent.oContained.w_PDVERMEM=='S',2,;
      iif(this.Parent.oContained.w_PDVERMEM=='T',3,;
      0)))
  endfunc

  func oPDVERMEM_1_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CCAR='S')
    endwith
   endif
  endfunc


  add object oPDRICONF_1_79 as StdCombo with uid="PRFJBFABJR",rtseq=47,rtrep=.f.,left=231,top=535,width=62,height=21;
    , ToolTipText = "Richiesta conferma configurazione";
    , HelpContextID = 101382852;
    , cFormVar="w_PDRICONF",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDRICONF_1_79.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPDRICONF_1_79.GetRadio()
    this.Parent.oContained.w_PDRICONF = this.RadioValue()
    return .t.
  endfunc

  func oPDRICONF_1_79.SetRadio()
    this.Parent.oContained.w_PDRICONF=trim(this.Parent.oContained.w_PDRICONF)
    this.value = ;
      iif(this.Parent.oContained.w_PDRICONF=='S',1,;
      iif(this.Parent.oContained.w_PDRICONF=='N',2,;
      0))
  endfunc

  func oPDRICONF_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CCAR='S')
    endwith
   endif
  endfunc

  add object oPDBLCFGC_1_80 as StdCheck with uid="SJZZMSJGFR",rtseq=48,rtrep=.f.,left=512, top=507, caption="Attesa Conf. Caratt.",;
    ToolTipText = " Se attivo: blocca temporaneamente la configurazione",;
    HelpContextID = 16188729,;
    cFormVar="w_PDBLCFGC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDBLCFGC_1_80.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPDBLCFGC_1_80.GetRadio()
    this.Parent.oContained.w_PDBLCFGC = this.RadioValue()
    return .t.
  endfunc

  func oPDBLCFGC_1_80.SetRadio()
    this.Parent.oContained.w_PDBLCFGC=trim(this.Parent.oContained.w_PDBLCFGC)
    this.value = ;
      iif(this.Parent.oContained.w_PDBLCFGC=="S",1,;
      0)
  endfunc

  func oPDBLCFGC_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CCAR='S')
    endwith
   endif
  endfunc

  add object oPDPROLEN_1_81 as StdField with uid="XGAVTEPHES",rtseq=49,rtrep=.f.,;
    cFormVar = "w_PDPROLEN", cQueryName = "PDPROLEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza del progressivo utilizzato per le configurazioni codici articoli",;
    HelpContextID = 129885508,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=649, Top=535

  func oPDPROLEN_1_81.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CCAR='S')
    endwith
   endif
  endfunc

  add object oStr_1_9 as StdString with uid="GQWIDZBQUE",Visible=.t., Left=94, Top=13,;
    Alignment=1, Width=135, Height=15,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="YHNPSTMKVJ",Visible=.t., Left=11, Top=115,;
    Alignment=0, Width=582, Height=18,;
    Caption="Causali impegno componenti/ordine prodotti"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UWMKOEQRMI",Visible=.t., Left=6, Top=145,;
    Alignment=1, Width=223, Height=18,;
    Caption="Impegno componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DGEYKBIZZG",Visible=.t., Left=92, Top=173,;
    Alignment=1, Width=137, Height=18,;
    Caption="Ordine prodotti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HSDDJQBJTS",Visible=.t., Left=92, Top=231,;
    Alignment=1, Width=137, Height=18,;
    Caption="Carico prodotti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="YZQJIRGIGR",Visible=.t., Left=88, Top=259,;
    Alignment=1, Width=141, Height=18,;
    Caption="Scarico componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ABVZLCHICN",Visible=.t., Left=92, Top=317,;
    Alignment=1, Width=137, Height=18,;
    Caption="Carico prodotti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="BUVXWECLOE",Visible=.t., Left=88, Top=345,;
    Alignment=1, Width=141, Height=18,;
    Caption="Scarico componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="XETIJMDUKS",Visible=.t., Left=572, Top=231,;
    Alignment=1, Width=75, Height=18,;
    Caption="Cau.Col.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="SDOCOSQYWG",Visible=.t., Left=572, Top=259,;
    Alignment=1, Width=75, Height=18,;
    Caption="Cau.Col.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="XHRCUCUINA",Visible=.t., Left=11, Top=202,;
    Alignment=0, Width=583, Height=18,;
    Caption="Causali carico prodotti/scarico componenti con evasione ordini/impegni"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="CCSIVRXQER",Visible=.t., Left=11, Top=287,;
    Alignment=0, Width=585, Height=18,;
    Caption="Causali carico prodotti/scarico componenti senza evasione ordini/impegni"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="BLHDULMYIA",Visible=.t., Left=580, Top=145,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="BUNIDBFDJF",Visible=.t., Left=580, Top=317,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="EJZLFDIFUY",Visible=.t., Left=580, Top=345,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="SLGHCCSHGT",Visible=.t., Left=580, Top=173,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="DJMVEBOBMW",Visible=.t., Left=56, Top=41,;
    Alignment=1, Width=349, Height=18,;
    Caption="Verifica di fattibilit� sul piano produzione, disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="EMTRJXJMPG",Visible=.t., Left=11, Top=380,;
    Alignment=0, Width=448, Height=18,;
    Caption="Criterio di valorizzazione per evasione articoli composti"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="CKOEJCKWIO",Visible=.t., Left=151, Top=412,;
    Alignment=1, Width=78, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="CROCMELOIT",Visible=.t., Left=443, Top=410,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (NOT .w_PDCRIVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="HJNIKVJEHE",Visible=.t., Left=440, Top=410,;
    Alignment=1, Width=70, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_PDCRIVAL<>'L')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="XPYKQQZEJP",Visible=.t., Left=580, Top=410,;
    Alignment=1, Width=67, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (NOT .w_PDCRIVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="FBLJJAIEWV",Visible=.t., Left=11, Top=436,;
    Alignment=0, Width=448, Height=17,;
    Caption="Modalit� di definizione delle distinte basi varianti"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="SVDJAJLUHX",Visible=.t., Left=113, Top=460,;
    Alignment=1, Width=116, Height=18,;
    Caption="Varianti riferite a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="HWIYYSIVVH",Visible=.t., Left=11, Top=484,;
    Alignment=0, Width=448, Height=17,;
    Caption="Configuratore di caratteristiche:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="NYMOCNZRGI",Visible=.t., Left=95, Top=538,;
    Alignment=1, Width=134, Height=18,;
    Caption="Richieste conf.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="NFWBIAXCTZ",Visible=.t., Left=77, Top=510,;
    Alignment=1, Width=152, Height=18,;
    Caption="Verifica uguaglianza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="XCULGYHWQX",Visible=.t., Left=481, Top=538,;
    Alignment=1, Width=165, Height=18,;
    Caption="Lunghezza progressivo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="ZHRNEIAIGM",left=11, top=130, width=714,height=2

  add object oBox_1_49 as StdBox with uid="JOZBVAACWW",left=11, top=223, width=714,height=2

  add object oBox_1_51 as StdBox with uid="SEPOUOOKJS",left=11, top=308, width=714,height=2

  add object oBox_1_60 as StdBox with uid="WINXMLZCWY",left=11, top=401, width=714,height=2

  add object oBox_1_75 as StdBox with uid="GNYWVSOVGI",left=11, top=452, width=714,height=2

  add object oBox_1_77 as StdBox with uid="YEZDMTNQVT",left=11, top=500, width=714,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_apd','PAR_DISB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PDCODAZI=PAR_DISB.PDCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
