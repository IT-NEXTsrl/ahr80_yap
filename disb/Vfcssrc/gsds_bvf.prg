* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bvf                                                        *
*              Batch stampa verifica di fattibilitą                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_140]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bvf",oParentObject)
return(i_retval)

define class tgsds_bvf as StdBatch
  * --- Local variables
  w_REP = space(50)
  w_QRY = space(50)
  w_DATFIL = ctod("  /  /  ")
  QTC = 0
  w_CDESART = space(1)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_NUMLEV = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_QTASAL = 0
  w_COEIMP = space(15)
  w_FLVARI = space(1)
  w_FLESPL = space(1)
  w_ARTCOM = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Stampa Verifica Fattibilita' (da GSDS_SVF)
    * --- Variabili passate dalla maschera
    DIMENSION QTC[99]
    this.w_CDESART = this.oParentObject.w_FLDESART
    if EMPTY(this.oParentObject.w_CODDIS)
      ah_ErrorMsg("Codice distinta non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_QUANTI=0
      ah_ErrorMsg("Specificare la quantitą da elaborare",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_OQRY) OR EMPTY(this.oParentObject.w_OREP)
      ah_ErrorMsg("Query/report di stampa non definiti",,"")
      i_retcode = 'stop'
      return
    endif
    L_DATSTA=this.oParentObject.w_DATSTA
    L_CODDIS=this.oParentObject.w_CODDIS
    L_DESDIS=this.oParentObject.w_DESDIS
    L_QUANTI=this.oParentObject.w_QUANTI
    L_CODMAG=this.oParentObject.w_CODMAG
    L_DESMAG=this.oParentObject.w_DESMAG
    L_NOESPL=this.oParentObject.w_NOESPL
    this.w_QRY = ALLTRIM(this.oParentObject.w_OQRY)
    this.w_REP = ALLTRIM(this.oParentObject.w_OREP)
    * --- Parametri per Batch di Esplosione
    this.w_DBCODINI = this.oParentObject.w_CODDIS
    this.w_DBCODFIN = this.oParentObject.w_CODDIS
    this.w_MAXLEVEL = 99
    this.w_VERIFICA = "S"
    this.w_FILSTAT = " "
    this.w_DATFIL = this.oParentObject.w_DATSTA
    if  used("TES_PLOS")
      select TES_PLOS
      use
    endif
    gsar_bde(this,"A")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if  used("TES_PLOS")
      * --- Crea Cursore di Appoggio
      CREATE CURSOR __TMP1__ ;
      (CODCOM C(20), DESCOM C(40), UNIMIS C(3), QTAMOV N(12,3), COEIMP C(15), FLVARI C(1), FLESPL C(1), ARTCOM C(20))
      SELECT TES_PLOS
      GO TOP
      SCAN FOR NOT EMPTY(DISPAD)
      this.w_NUMLEV = NUMLEV
      this.w_DESCOM = DESCOM
      this.w_UNIMIS = UNIMIS
      this.w_QTAUM1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
      * --- In questa Stampa, Le qta sono sempre riferite alla U.M. Principale!
      this.w_QTAMOV = QTAMOV
      this.w_COEIMP = COEIMP
      this.w_FLVARI = FLVARI
      this.w_FLESPL = FLESPL
      this.w_ARTCOM = ARTCOM
      if VAL(this.w_NUMLEV)=0
        FOR L_i = 1 TO 99
        QTC[L_i] = 0
        ENDFOR
      else
        if NOT EMPTY(this.w_ARTCOM) AND this.w_QTAMOV<>this.w_QTAUM1
          * --- Legge la prima U.M.
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_ARTCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1;
              from (i_cTable) where;
                  ARCODART = this.w_ARTCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
        QTC[VAL(this.w_NUMLEV)] = this.w_QTAUM1
        * --- Parte dalle Quantita' da Produrre (Distinta=liv. 0000)
        this.w_QTAUM1 = this.oParentObject.w_QUANTI
        FOR L_i = 1 TO VAL(this.w_NUMLEV)
        * --- Le quantita' vanno tutte riportate alla prima U.M.ri calcoli vanno riportati alla 1^UM
        this.w_QTAUM1 = this.w_QTAUM1 * QTC[L_i]
        ENDFOR
        this.w_CODCOM = CODCOM
        INSERT INTO __TMP1__ ;
        (CODCOM, DESCOM, UNIMIS, QTAMOV, COEIMP, FLVARI, FLESPL, ARTCOM) VALUES ;
        (this.w_CODCOM, this.w_DESCOM, this.w_UNIMIS, cp_ROUND(this.w_QTAUM1, 3), this.w_COEIMP, this.w_FLVARI, this.w_FLESPL, this.w_ARTCOM)
      endif
      SELECT TES_PLOS
      ENDSCAN
      select TES_PLOS
      use
    endif
    if USED("__TMP1__")
      * --- Se esiste tmp
      SELECT __TMP1__ 
      if RECCOUNT()>0
        if this.oParentObject.w_NOESPL="S"
          * --- Considera solo le 'foglie'
          GO TOP
          DELETE FROM __TMP1__ WHERE FLESPL="S"
        endif
        GO TOP
        * --- Sommarizzata, Raggruppa per Componente (Esclude il Livello 0000)
        SELECT CODCOM, MAX(DESCOM) AS DESCOM, MAX(UNIMIS) AS UNIMIS, SUM(QTAMOV) AS QTAMOV, MAX(ARTCOM) AS ARTCOM ;
        FROM __TMP1__ GROUP BY 1 ORDER BY 1 INTO CURSOR __TMP1__
        * --- Rndo Scrivibile per Unirlo con i Saldi
        =WRCURSOR("__TMP1__")
        GO TOP
        * --- A questo Punto Leggo i Saldi delle Disponibilita'
        VQ_EXEC(this.w_QRY, this,"TES_PLOS")
        SELECT __TMP1__.CODCOM AS CODCOM, __TMP1__.DESCOM AS DESCOM, __TMP1__.ARTCOM AS ARTCOM, ;
        __TMP1__.UNIMIS AS UNIMIS, __TMP1__.QTAMOV AS QTAMOV, NVL(TES_PLOS.QTASAL, 0) AS QTASAL, ;
        NVL(TES_PLOS.UNMIS1, "   ") AS UNMIS1, NVL(TES_PLOS.UNMIS2, "   ") AS UNMIS2, NVL(TES_PLOS.UNMIS3, "   ") AS UNMIS3, ;
        NVL(TES_PLOS.OPERAT, " ") AS OPERAT, NVL(TES_PLOS.OPERA3, " ") AS OPERA3, ;
        NVL(TES_PLOS.MOLTIP, 0) AS MOLTIP, NVL(TES_PLOS.MOLTI3, 0) AS MOLTI3 ;
        FROM (__TMP1__ LEFT OUTER JOIN TES_PLOS ON __TMP1__.CODCOM = TES_PLOS.CODCOM AND __TMP1__.ARTCOM = TES_PLOS.CODART) ;
        INTO CURSOR __TMP__ NOFILTER
        if  used("__TMP1__")
          select __TMP1__
          use
        endif
        if  used("TES_PLOS")
          select TES_PLOS
          use
        endif
        if used("__TMP__")
          select __TMP__
          GO TOP
          CP_CHPRN(this.w_REP, " ", this)
        endif
      endif
    endif
    * --- Rimuovo i cursori
    if  used("__TMP1__")
      select __TMP1__
      use
    endif
    if  used("TES_PLOS")
      select TES_PLOS
      use
    endif
    if  used("__Tmp__")
      select __Tmp__
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
