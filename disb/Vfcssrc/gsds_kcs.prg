* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_kcs                                                        *
*              Determinazione costo standard                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_164]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_kcs",oParentObject))

* --- Class definition
define class tgsds_kcs as StdForm
  Top    = 9
  Left   = 17

  * --- Standard Properties
  Width  = 851
  Height = 549+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=99181673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=105

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  ESERCIZI_IDX = 0
  INVENTAR_IDX = 0
  LISTINI_IDX = 0
  ART_ICOL_IDX = 0
  VALUTE_IDX = 0
  PAR_DISB_IDX = 0
  MAGAZZIN_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  cPrg = "gsds_kcs"
  cComment = "Determinazione costo standard"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CICLI = .F.
  w_PARAM = space(1)
  w_CODAZI = space(5)
  w_VALESE = space(3)
  w_AZIENDA = space(5)
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_QUANTI = 0
  w_FLPROV = space(1)
  w_FLCOST = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOLN = space(1)
  w_CAOVAL = 0
  w_DIS1 = space(20)
  w_DIS2 = space(20)
  w_VALESE = space(3)
  w_CAOESE = 0
  w_Analisi = space(1)
  w_PERRIC = space(1)
  w_UTENTE = .F.
  w_SCDRSR = space(1)
  w_TipoValo = space(1)
  o_TipoValo = space(1)
  w_ESERC = space(4)
  o_ESERC = space(4)
  w_NUMINV = space(6)
  o_NUMINV = space(6)
  w_Listino = space(5)
  o_Listino = space(5)
  w_DESLIS = space(40)
  w_PRDATINV = ctod('  /  /  ')
  w_VALLIS = space(3)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INCODMAG = space(5)
  w_INMAGCOL = space(1)
  w_MAGRAG = space(5)
  w_INCATOMO = space(5)
  w_INDATINV = ctod('  /  /  ')
  w_INCODESE = space(5)
  w_INNUMPRE = space(6)
  w_INESEPRE = space(5)
  w_MEDIOPON = .F.
  o_MEDIOPON = .F.
  w_FLDESART = space(1)
  w_CAOLIS = 0
  w_ONUME = 0
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_ESFINESE = ctod('  /  /  ')
  w_FLCOSINV = space(1)
  w_DIS1OB = ctod('  /  /  ')
  w_DIS2OB = ctod('  /  /  ')
  w_DECTOT = 0
  w_STANDARD = space(1)
  o_STANDARD = space(1)
  w_ULTIMO = space(1)
  o_ULTIMO = space(1)
  w_AGGMPOND = space(1)
  o_AGGMPOND = space(1)
  w_CLISTINO = .F.
  w_AGGSTAND = space(1)
  o_AGGSTAND = space(1)
  w_NOLIVFIN = space(1)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_OUNOMQUE = space(254)
  o_OUNOMQUE = space(254)
  w_SELEZI = space(1)
  w_ORIQUERY = space(10)
  w_FLCHKLP = space(1)
  w_COSTILOG = space(1)
  w_AGCOSSTD = space(1)
  w_DISPRO = space(1)
  w_FDBNOLEG = space(1)
  w_TipoValoOut = space(1)
  o_TipoValoOut = space(1)
  w_SMATOU = space(1)
  w_LOTTOMED = .F.
  w_INESCONT = space(1)
  w_TIPOARTI = space(1)
  w_ESERCMO = space(4)
  w_NUMINVMO = space(6)
  w_ListinoMo = space(5)
  o_ListinoMo = space(5)
  w_DESLISMO = space(40)
  w_DATINVMO = ctod('  /  /  ')
  w_CAOLISMO = 0
  w_CODMAGMO = space(5)
  w_DESMAGMO = space(30)
  w_TIPVALCIC = space(2)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_TIPCOS = space(1)
  w_PDCOECOS = space(1)
  w_FLDESART = space(1)
  w_PDCOSPAR = space(1)
  w_UM_ORE = space(3)
  w_VALLISMO = space(3)
  w_TIPOLNMO = space(1)
  w_FLDETTMAG = space(1)
  w_CAOVALMO = 0
  w_PDCOECOP = space(1)
  w_BTNQRY = .NULL.
  w_SelDis = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_kcsPag1","gsds_kcs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsds_kcsPag2","gsds_kcs",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsds_kcs
    This.Pages(2).Enabled=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    this.w_SelDis = this.oPgFrm.Pages(2).oPag.SelDis
    DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      this.w_SelDis = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='INVENTAR'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='PAR_DISB'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='CATEGOMO'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDS1BDC(this,"ELABORA")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLI=.f.
      .w_PARAM=space(1)
      .w_CODAZI=space(5)
      .w_VALESE=space(3)
      .w_AZIENDA=space(5)
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_DATSTA=ctod("  /  /  ")
      .w_QUANTI=0
      .w_FLPROV=space(1)
      .w_FLCOST=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOLN=space(1)
      .w_CAOVAL=0
      .w_DIS1=space(20)
      .w_DIS2=space(20)
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_Analisi=space(1)
      .w_PERRIC=space(1)
      .w_UTENTE=.f.
      .w_SCDRSR=space(1)
      .w_TipoValo=space(1)
      .w_ESERC=space(4)
      .w_NUMINV=space(6)
      .w_Listino=space(5)
      .w_DESLIS=space(40)
      .w_PRDATINV=ctod("  /  /  ")
      .w_VALLIS=space(3)
      .w_INGRUMER=space(5)
      .w_INCODFAM=space(5)
      .w_INCODMAG=space(5)
      .w_INMAGCOL=space(1)
      .w_MAGRAG=space(5)
      .w_INCATOMO=space(5)
      .w_INDATINV=ctod("  /  /  ")
      .w_INCODESE=space(5)
      .w_INNUMPRE=space(6)
      .w_INESEPRE=space(5)
      .w_MEDIOPON=.f.
      .w_FLDESART=space(1)
      .w_CAOLIS=0
      .w_ONUME=0
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_ESFINESE=ctod("  /  /  ")
      .w_FLCOSINV=space(1)
      .w_DIS1OB=ctod("  /  /  ")
      .w_DIS2OB=ctod("  /  /  ")
      .w_DECTOT=0
      .w_STANDARD=space(1)
      .w_ULTIMO=space(1)
      .w_AGGMPOND=space(1)
      .w_CLISTINO=.f.
      .w_AGGSTAND=space(1)
      .w_NOLIVFIN=space(1)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_OUNOMQUE=space(254)
      .w_SELEZI=space(1)
      .w_ORIQUERY=space(10)
      .w_FLCHKLP=space(1)
      .w_COSTILOG=space(1)
      .w_AGCOSSTD=space(1)
      .w_DISPRO=space(1)
      .w_FDBNOLEG=space(1)
      .w_TipoValoOut=space(1)
      .w_SMATOU=space(1)
      .w_LOTTOMED=.f.
      .w_TIPOARTI=space(1)
      .w_ESERCMO=space(4)
      .w_NUMINVMO=space(6)
      .w_ListinoMo=space(5)
      .w_DESLISMO=space(40)
      .w_DATINVMO=ctod("  /  /  ")
      .w_CAOLISMO=0
      .w_CODMAGMO=space(5)
      .w_DESMAGMO=space(30)
      .w_TIPVALCIC=space(2)
      .w_OREP=space(50)
      .w_OQRY=space(50)
      .w_TIPCOS=space(1)
      .w_PDCOECOS=space(1)
      .w_FLDESART=space(1)
      .w_PDCOSPAR=space(1)
      .w_UM_ORE=space(3)
      .w_VALLISMO=space(3)
      .w_TIPOLNMO=space(1)
      .w_FLDETTMAG=space(1)
      .w_CAOVALMO=0
      .w_PDCOECOP=space(1)
        .w_CICLI = g_PRFA="S" and g_CICLILAV="S"
        .w_PARAM = iif(TYPE('this.oParentObject')="O", "G", this.oParentObject)
        .w_CODAZI = i_CODAZI
          .DoRTCalc(4,4,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODINI))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODFIN))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_DATSTA = i_datsys
        .w_QUANTI = 1
        .w_FLPROV = ' '
        .w_FLCOST = 'S'
        .w_OBTEST = i_INIDAT
        .DoRTCalc(15,18,.f.)
        if not(empty(.w_DIS1))
          .link_1_23('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_DIS2))
          .link_1_24('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_VALESE))
          .link_1_25('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_Analisi = IIF(.w_CICLI, "D", "S")
        .w_PERRIC = "S"
        .w_UTENTE = .w_AGGSTAND<>"N"
        .w_SCDRSR = "S"
        .w_TipoValo = iif(.w_CLISTINO,"L", IIF(.w_STANDARD="N" , "S" , .w_STANDARD))
        .w_ESERC = g_CODESE
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_ESERC))
          .link_1_33('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_NUMINV))
          .link_1_34('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_Listino))
          .link_1_36('Full')
        endif
        .DoRTCalc(30,32,.f.)
        if not(empty(.w_VALLIS))
          .link_1_41('Full')
        endif
        .DoRTCalc(33,35,.f.)
        if not(empty(.w_INCODMAG))
          .link_1_44('Full')
        endif
          .DoRTCalc(36,36,.f.)
        .w_MAGRAG = IIF (EMPTY(.w_MAGRAG),.w_INCODMAG,.w_MAGRAG)
          .DoRTCalc(38,38,.f.)
        .w_INDATINV = .w_DATSTA
        .w_INCODESE = .w_ESERC
        .w_INNUMPRE = .w_NUMINV
        .w_INESEPRE = .w_ESERC
        .w_MEDIOPON = .w_AGGMPOND <> 'N'
        .w_FLDESART = 'S'
        .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
          .DoRTCalc(46,46,.f.)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CODMAG))
          .link_1_57('Full')
        endif
          .DoRTCalc(48,49,.f.)
        .w_FLCOSINV = IIF(.w_MEDIOPON or .w_ULTIMO<>"N" or .w_STANDARD='S' or .w_AGGSTAND $ "MU" , "S", "N")
          .DoRTCalc(51,53,.f.)
        .w_STANDARD = "S"
        .w_ULTIMO = "U"
        .w_AGGMPOND = "M"
        .w_CLISTINO = True
        .w_AGGSTAND = "N"
        .w_NOLIVFIN = 'S'
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_77('Full')
        endif
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_78('Full')
        endif
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_GRUINI))
          .link_1_79('Full')
        endif
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_80('Full')
        endif
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_CATINI))
          .link_1_81('Full')
        endif
        .DoRTCalc(65,65,.f.)
        if not(empty(.w_CATFIN))
          .link_1_82('Full')
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Calculate()
      .oPgFrm.Page2.oPag.SelDis.Calculate()
          .DoRTCalc(66,72,.f.)
        .w_SELEZI = 'S'
        .w_ORIQUERY = alltrim(.w_SelDis.cCpQueryName)
        .w_FLCHKLP = g_CHKDIBALOOP
        .w_COSTILOG = "N"
        .w_AGCOSSTD = "N"
        .w_DISPRO = "N"
        .w_FDBNOLEG = 'N'
        .w_TipoValoOut = "NN"
        .w_SMATOU = IIF(.w_CICLI, "S" , "N")
        .w_LOTTOMED = True
        .w_TIPOARTI = ''
        .w_ESERCMO = g_CODESE
        .DoRTCalc(84,84,.f.)
        if not(empty(.w_ESERCMO))
          .link_1_118('Full')
        endif
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_NUMINVMO))
          .link_1_119('Full')
        endif
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_ListinoMo))
          .link_1_121('Full')
        endif
          .DoRTCalc(87,88,.f.)
        .w_CAOLISMO = IIF(.w_TipoValoOut="LI", IIF(.w_CAOVALMO=0, GETCAM(.w_VALLISMO, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLISMO)
        .w_CODMAGMO = g_MAGAZI
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_CODMAGMO))
          .link_1_130('Full')
        endif
          .DoRTCalc(91,91,.f.)
        .w_TIPVALCIC = '00'
          .DoRTCalc(93,94,.f.)
        .w_TIPCOS = .w_PDCOSPAR
        .w_PDCOECOS = .w_PDCOECOP
        .w_FLDESART = 'S'
          .DoRTCalc(98,98,.f.)
        .w_UM_ORE = ah_msgformat('Ore')
        .DoRTCalc(100,100,.f.)
        if not(empty(.w_VALLISMO))
          .link_1_142('Full')
        endif
          .DoRTCalc(101,101,.f.)
        .w_FLDETTMAG = IIF(.w_MEDIOPON or .w_ULTIMO<>"N" or .w_STANDARD='S' or .w_AGGSTAND $ "MU" and !Empty(.w_NUMINV) and !Empty(.w_MAGRAG) , .w_FLDETTMAG, "N")
    endwith
    this.DoRTCalc(103,104,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,7,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_8('Full')
        endif
        .DoRTCalc(9,17,.t.)
          .link_1_23('Full')
          .link_1_24('Full')
          .link_1_25('Full')
        .DoRTCalc(21,23,.t.)
            .w_UTENTE = .w_AGGSTAND<>"N"
        .DoRTCalc(25,25,.t.)
            .w_TipoValo = iif(.w_CLISTINO,"L", IIF(.w_STANDARD="N" , "S" , .w_STANDARD))
        if .o_TipoValo<>.w_TipoValo
          .link_1_33('Full')
        endif
        if .o_ESERC<>.w_ESERC.or. .o_AGGSTAND<>.w_AGGSTAND.or. .o_AGGMPOND<>.w_AGGMPOND.or. .o_STANDARD<>.w_STANDARD
          .link_1_34('Full')
        endif
        .DoRTCalc(29,31,.t.)
          .link_1_41('Full')
        .DoRTCalc(33,34,.t.)
          .link_1_44('Full')
        .DoRTCalc(36,36,.t.)
            .w_MAGRAG = IIF (EMPTY(.w_MAGRAG),.w_INCODMAG,.w_MAGRAG)
        .DoRTCalc(38,38,.t.)
            .w_INDATINV = .w_DATSTA
            .w_INCODESE = .w_ESERC
        if .o_NUMINV<>.w_NUMINV
            .w_INNUMPRE = .w_NUMINV
        endif
            .w_INESEPRE = .w_ESERC
            .w_MEDIOPON = .w_AGGMPOND <> 'N'
        .DoRTCalc(44,44,.t.)
        if .o_DATSTA<>.w_DATSTA.or. .o_Listino<>.w_Listino.or. .o_TipoValo<>.w_TipoValo
            .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
        endif
        .DoRTCalc(46,49,.t.)
        if .o_MEDIOPON<>.w_MEDIOPON.or. .o_ULTIMO<>.w_ULTIMO.or. .o_AGGSTAND<>.w_AGGSTAND
            .w_FLCOSINV = IIF(.w_MEDIOPON or .w_ULTIMO<>"N" or .w_STANDARD='S' or .w_AGGSTAND $ "MU" , "S", "N")
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.SelDis.Calculate()
        if .o_OUNOMQUE<>.w_OUNOMQUE
          .Calculate_TWKEFLENMI()
        endif
        .DoRTCalc(51,82,.t.)
        if .o_NUMINV<>.w_NUMINV
            .w_INESCONT = iif(empty(.w_NUMINV),'N', iif(empty(.w_INESCONT), 'N', .w_INESCONT))
        endif
        .DoRTCalc(84,89,.t.)
        if .o_DATSTA<>.w_DATSTA.or. .o_ListinoMo<>.w_ListinoMo.or. .o_TipoValoOut<>.w_TipoValoOut
            .w_CAOLISMO = IIF(.w_TipoValoOut="LI", IIF(.w_CAOVALMO=0, GETCAM(.w_VALLISMO, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLISMO)
        endif
        .DoRTCalc(91,100,.t.)
          .link_1_142('Full')
        .DoRTCalc(102,102,.t.)
        if .o_MEDIOPON<>.w_MEDIOPON.or. .o_ULTIMO<>.w_ULTIMO.or. .o_AGGSTAND<>.w_AGGSTAND.or. .o_NUMINV<>.w_NUMINV
            .w_FLDETTMAG = IIF(.w_MEDIOPON or .w_ULTIMO<>"N" or .w_STANDARD='S' or .w_AGGSTAND $ "MU" and !Empty(.w_NUMINV) and !Empty(.w_MAGRAG) , .w_FLDETTMAG, "N")
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(104,105,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.SelDis.Calculate()
    endwith
  return

  proc Calculate_WKQUULDECR()
    with this
          * --- Selezione Zoom
          GSDS2BDC(this;
              ,'SELDESEL';
             )
    endwith
  endproc
  proc Calculate_TWKEFLENMI()
    with this
          * --- Eventi damodifica query di selezione
          .w_OUNOMQUE = SYS(2014, .w_OUNOMQUE)
          .w_OUNOMQUE = IIF(FILE(.w_OUNOMQUE) And lower(Alltrim(right(.w_OUNOMQUE,4)))=='.vqr', .w_OUNOMQUE, "")
          .w_CODINI = IIF(Empty(.w_OUNOMQUE), .w_CODINI, "")
          GSDS2BDC(this;
              ,'ANTEPRIMA';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oESERC_1_33.enabled = this.oPgFrm.Page1.oPag.oESERC_1_33.mCond()
    this.oPgFrm.Page1.oPag.oNUMINV_1_34.enabled = this.oPgFrm.Page1.oPag.oNUMINV_1_34.mCond()
    this.oPgFrm.Page1.oPag.oListino_1_36.enabled = this.oPgFrm.Page1.oPag.oListino_1_36.mCond()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_54.enabled = this.oPgFrm.Page1.oPag.oCAOLIS_1_54.mCond()
    this.oPgFrm.Page1.oPag.oCODMAG_1_57.enabled = this.oPgFrm.Page1.oPag.oCODMAG_1_57.mCond()
    this.oPgFrm.Page1.oPag.oFLCOSINV_1_61.enabled = this.oPgFrm.Page1.oPag.oFLCOSINV_1_61.mCond()
    this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.enabled = this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.mCond()
    this.oPgFrm.Page1.oPag.oINESCONT_1_113.enabled = this.oPgFrm.Page1.oPag.oINESCONT_1_113.mCond()
    this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.enabled = this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.mCond()
    this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.enabled = this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.mCond()
    this.oPgFrm.Page1.oPag.oFLDETTMAG_1_144.enabled = this.oPgFrm.Page1.oPag.oFLDETTMAG_1_144.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_54.visible=!this.oPgFrm.Page1.oPag.oCAOLIS_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oCODMAG_1_57.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_59.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.visible=!this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.mHide()
    this.oPgFrm.Page1.oPag.oSMATOU_1_110.visible=!this.oPgFrm.Page1.oPag.oSMATOU_1_110.mHide()
    this.oPgFrm.Page1.oPag.oINESCONT_1_113.visible=!this.oPgFrm.Page1.oPag.oINESCONT_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page1.oPag.oESERCMO_1_118.visible=!this.oPgFrm.Page1.oPag.oESERCMO_1_118.mHide()
    this.oPgFrm.Page1.oPag.oNUMINVMO_1_119.visible=!this.oPgFrm.Page1.oPag.oNUMINVMO_1_119.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oListinoMo_1_121.visible=!this.oPgFrm.Page1.oPag.oListinoMo_1_121.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_122.visible=!this.oPgFrm.Page1.oPag.oStr_1_122.mHide()
    this.oPgFrm.Page1.oPag.oDESLISMO_1_123.visible=!this.oPgFrm.Page1.oPag.oDESLISMO_1_123.mHide()
    this.oPgFrm.Page1.oPag.oDATINVMO_1_124.visible=!this.oPgFrm.Page1.oPag.oDATINVMO_1_124.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.visible=!this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_127.visible=!this.oPgFrm.Page1.oPag.oStr_1_127.mHide()
    this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.visible=!this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_131.visible=!this.oPgFrm.Page1.oPag.oStr_1_131.mHide()
    this.oPgFrm.Page1.oPag.oDESMAGMO_1_132.visible=!this.oPgFrm.Page1.oPag.oDESMAGMO_1_132.mHide()
    this.oPgFrm.Page1.oPag.oTIPVALCIC_1_133.visible=!this.oPgFrm.Page1.oPag.oTIPVALCIC_1_133.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_134.visible=!this.oPgFrm.Page1.oPag.oStr_1_134.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
      .oPgFrm.Page2.oPag.SelDis.Event(cEvent)
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_WKQUULDECR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OUNOMQUE Changed")
          .Calculate_TWKEFLENMI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDCOSPAR,PDCOECOS";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_AZIENDA)
            select PDCODAZI,PDCOSPAR,PDCOECOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.PDCODAZI,space(5))
      this.w_PDCOSPAR = NVL(_Link_.PDCOSPAR,space(1))
      this.w_PDCOECOP = NVL(_Link_.PDCOECOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PDCOSPAR = space(1)
      this.w_PDCOECOP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_6'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DIS1 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DIS1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DIS1 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_8'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DIS2 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DIS2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DIS2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS1
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS1)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS1 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS1OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS1 = space(20)
      endif
      this.w_DIS1OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS2
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS2)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS2 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS2OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS2 = space(20)
      endif
      this.w_DIS2OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERC
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERC)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESERC))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERC)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERC) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERC_1_33'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERC)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERC = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
      this.w_ESFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESERC = space(4)
      endif
      this.w_VALESE = space(3)
      this.w_ESFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINV
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERC;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_1_34'),i_cWhere,'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERC;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_PRDATINV = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Listino
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Listino) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_Listino)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_Listino))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Listino)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Listino) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oListino_1_36'),i_cWhere,'',"Anagrafica listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Listino)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_Listino);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_Listino)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Listino = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_Listino = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Listino Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODMAG
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGMAGRAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_INCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_INCODMAG)
            select MGCODMAG,MGMAGRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_MAGRAG = NVL(_Link_.MGMAGRAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INCODMAG = space(5)
      endif
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_57'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_77'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_78'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_79'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_80(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_80'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_81(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_81'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_82'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCMO
  func Link_1_118(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERCMO)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESERCMO))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERCMO)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERCMO) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERCMO_1_118'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCMO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCMO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCMO = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINVMO
  func Link_1_119(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINVMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINVMO)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERCMO;
                     ,'INNUMINV',trim(this.w_NUMINVMO))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINVMO)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINVMO) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINVMO_1_119'),i_cWhere,'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERCMO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINVMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINVMO);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERCMO;
                       ,'INNUMINV',this.w_NUMINVMO)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINVMO = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_DATINVMO = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINVMO = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_DATINVMO = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINVMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ListinoMo
  func Link_1_121(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ListinoMo) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ListinoMo)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ListinoMo))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ListinoMo)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ListinoMo) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oListinoMo_1_121'),i_cWhere,'',"Anagrafica listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ListinoMo)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ListinoMo);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ListinoMo)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ListinoMo = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLISMO = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLISMO = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLNMO = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ListinoMo = space(5)
      endif
      this.w_DESLISMO = space(40)
      this.w_VALLISMO = space(3)
      this.w_TIPOLNMO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ListinoMo Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAGMO
  func Link_1_130(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAGMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAGMO)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAGMO))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAGMO)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAGMO)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAGMO)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAGMO) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAGMO_1_130'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAGMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAGMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAGMO)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAGMO = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGMO = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAGMO = space(5)
      endif
      this.w_DESMAGMO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAGMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLISMO
  func Link_1_142(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLISMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLISMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLISMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLISMO)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLISMO = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVALMO = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLISMO = space(3)
      endif
      this.w_CAOVALMO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLISMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_6.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_6.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_7.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_7.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_8.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_8.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_9.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_9.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_10.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_10.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oPERRIC_1_28.RadioValue()==this.w_PERRIC)
      this.oPgFrm.Page1.oPag.oPERRIC_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESERC_1_33.value==this.w_ESERC)
      this.oPgFrm.Page1.oPag.oESERC_1_33.value=this.w_ESERC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINV_1_34.value==this.w_NUMINV)
      this.oPgFrm.Page1.oPag.oNUMINV_1_34.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oListino_1_36.value==this.w_Listino)
      this.oPgFrm.Page1.oPag.oListino_1_36.value=this.w_Listino
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_38.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_38.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_39.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_39.value=this.w_PRDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLIS_1_54.value==this.w_CAOLIS)
      this.oPgFrm.Page1.oPag.oCAOLIS_1_54.value=this.w_CAOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_57.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_57.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_59.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_59.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOSINV_1_61.RadioValue()==this.w_FLCOSINV)
      this.oPgFrm.Page1.oPag.oFLCOSINV_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTANDARD_1_66.RadioValue()==this.w_STANDARD)
      this.oPgFrm.Page1.oPag.oSTANDARD_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oULTIMO_1_67.RadioValue()==this.w_ULTIMO)
      this.oPgFrm.Page1.oPag.oULTIMO_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGMPOND_1_68.RadioValue()==this.w_AGGMPOND)
      this.oPgFrm.Page1.oPag.oAGGMPOND_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLISTINO_1_69.RadioValue()==this.w_CLISTINO)
      this.oPgFrm.Page1.oPag.oCLISTINO_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGSTAND_1_70.RadioValue()==this.w_AGGSTAND)
      this.oPgFrm.Page1.oPag.oAGGSTAND_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLIVFIN_1_74.RadioValue()==this.w_NOLIVFIN)
      this.oPgFrm.Page1.oPag.oNOLIVFIN_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_77.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_77.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_78.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_78.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_79.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_79.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_80.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_80.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_81.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_81.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_82.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_82.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_83.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_83.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_84.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_84.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_85.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_85.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_89.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_89.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_90.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_90.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_91.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_91.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMQUE_1_95.value==this.w_OUNOMQUE)
      this.oPgFrm.Page1.oPag.oOUNOMQUE_1_95.value=this.w_OUNOMQUE
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_3.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLP_1_99.RadioValue()==this.w_FLCHKLP)
      this.oPgFrm.Page1.oPag.oFLCHKLP_1_99.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSTILOG_1_102.RadioValue()==this.w_COSTILOG)
      this.oPgFrm.Page1.oPag.oCOSTILOG_1_102.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCOSSTD_1_104.RadioValue()==this.w_AGCOSSTD)
      this.oPgFrm.Page1.oPag.oAGCOSSTD_1_104.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.RadioValue()==this.w_TipoValoOut)
      this.oPgFrm.Page1.oPag.oTipoValoOut_1_109.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSMATOU_1_110.RadioValue()==this.w_SMATOU)
      this.oPgFrm.Page1.oPag.oSMATOU_1_110.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOMED_1_112.RadioValue()==this.w_LOTTOMED)
      this.oPgFrm.Page1.oPag.oLOTTOMED_1_112.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINESCONT_1_113.RadioValue()==this.w_INESCONT)
      this.oPgFrm.Page1.oPag.oINESCONT_1_113.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCMO_1_118.value==this.w_ESERCMO)
      this.oPgFrm.Page1.oPag.oESERCMO_1_118.value=this.w_ESERCMO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINVMO_1_119.value==this.w_NUMINVMO)
      this.oPgFrm.Page1.oPag.oNUMINVMO_1_119.value=this.w_NUMINVMO
    endif
    if not(this.oPgFrm.Page1.oPag.oListinoMo_1_121.value==this.w_ListinoMo)
      this.oPgFrm.Page1.oPag.oListinoMo_1_121.value=this.w_ListinoMo
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLISMO_1_123.value==this.w_DESLISMO)
      this.oPgFrm.Page1.oPag.oDESLISMO_1_123.value=this.w_DESLISMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINVMO_1_124.value==this.w_DATINVMO)
      this.oPgFrm.Page1.oPag.oDATINVMO_1_124.value=this.w_DATINVMO
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.value==this.w_CAOLISMO)
      this.oPgFrm.Page1.oPag.oCAOLISMO_1_126.value=this.w_CAOLISMO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.value==this.w_CODMAGMO)
      this.oPgFrm.Page1.oPag.oCODMAGMO_1_130.value=this.w_CODMAGMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGMO_1_132.value==this.w_DESMAGMO)
      this.oPgFrm.Page1.oPag.oDESMAGMO_1_132.value=this.w_DESMAGMO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVALCIC_1_133.RadioValue()==this.w_TIPVALCIC)
      this.oPgFrm.Page1.oPag.oTIPVALCIC_1_133.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCOS_1_137.RadioValue()==this.w_TIPCOS)
      this.oPgFrm.Page1.oPag.oTIPCOS_1_137.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOECOS_1_138.RadioValue()==this.w_PDCOECOS)
      this.oPgFrm.Page1.oPag.oPDCOECOS_1_138.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDETTMAG_1_144.RadioValue()==this.w_FLDETTMAG)
      this.oPgFrm.Page1.oPag.oFLDETTMAG_1_144.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1))))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not((EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   ((empty(.w_DATSTA)) or not((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice distinta di inizio o fine selezione obsoleto alla data selezionata")
          case   (empty(.w_QUANTI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQUANTI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_QUANTI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("indicare la quantit� da esplodere! (Maggiore di 0)")
          case   (empty(.w_CAOLIS))  and not(.w_CAOVAL<>0 OR Not .w_CLISTINO OR EMPTY(.w_Listino))  and (.w_CAOVAL=0 AND .w_CLISTINO AND NOT EMPTY(.w_Listino))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLIS_1_54.SetFocus()
            i_bnoObbl = !empty(.w_CAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODMAG))  and not(.w_ULTIMO <> 'A')  and (.w_ULTIMO = 'A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_57.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_79.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_80.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_81.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_82.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOLISMO))  and not(.w_CAOVALMO<>0 OR .w_TipoValoOut<>"LI" OR EMPTY(.w_ListinoMo))  and (.w_CAOVALMO=0 AND .w_TipoValoOut="LI" AND NOT EMPTY(.w_ListinoMo))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLISMO_1_126.SetFocus()
            i_bnoObbl = !empty(.w_CAOLISMO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODMAGMO))  and not(.w_ULTIMO <> 'A')  and (.w_ULTIMO = 'A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAGMO_1_130.SetFocus()
            i_bnoObbl = !empty(.w_CODMAGMO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_DATSTA = this.w_DATSTA
    this.o_TipoValo = this.w_TipoValo
    this.o_ESERC = this.w_ESERC
    this.o_NUMINV = this.w_NUMINV
    this.o_Listino = this.w_Listino
    this.o_MEDIOPON = this.w_MEDIOPON
    this.o_STANDARD = this.w_STANDARD
    this.o_ULTIMO = this.w_ULTIMO
    this.o_AGGMPOND = this.w_AGGMPOND
    this.o_AGGSTAND = this.w_AGGSTAND
    this.o_OUNOMQUE = this.w_OUNOMQUE
    this.o_TipoValoOut = this.w_TipoValoOut
    this.o_ListinoMo = this.w_ListinoMo
    return

enddefine

* --- Define pages as container
define class tgsds_kcsPag1 as StdContainer
  Width  = 847
  height = 551
  stdWidth  = 847
  stdheight = 551
  resizeXpos=663
  resizeYpos=496
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_6 as StdField with uid="YLDHOMMPET",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 129749210,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=10, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_7 as StdField with uid="YJQMLZHVQA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 129690314,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=10, InputMask=replicate('X',40)

  add object oCODFIN_1_8 as StdField with uid="VXFHEBELNK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Distinta base di fine selezione (spazio=no selezione)",;
    HelpContextID = 51302618,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=121, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_9 as StdField with uid="DESEQAHWJU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51243722,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=280, Top=36, InputMask=replicate('X',40)

  add object oDATSTA_1_10 as StdField with uid="WHTHOLGNAQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Codice distinta di inizio o fine selezione obsoleto alla data selezionata",;
    ToolTipText = "Data di stampa",;
    HelpContextID = 256958154,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=121, Top=66

  func oDATSTA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="TYEVPKXNEQ",left=738, top=503, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 57613382;
    , Caption='E\<labora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSDS1BDC(this.Parent.oContained,"ELABORA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_CLISTINO OR .w_STANDARD<>"N" OR .w_MEDIOPON OR .w_ULTIMO<>"N" OR .w_UTENTE))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="BSJPZGRESF",left=792, top=503, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 91864250;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPERRIC_1_28 as StdCheck with uid="ITJTQSYEXZ",rtseq=23,rtrep=.f.,left=113, top=294, caption="% Ricarico",;
    ToolTipText = "Aggiunge la percentuale di ricarico nei costi",;
    HelpContextID = 235010570,;
    cFormVar="w_PERRIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERRIC_1_28.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPERRIC_1_28.GetRadio()
    this.Parent.oContained.w_PERRIC = this.RadioValue()
    return .t.
  endfunc

  func oPERRIC_1_28.SetRadio()
    this.Parent.oContained.w_PERRIC=trim(this.Parent.oContained.w_PERRIC)
    this.value = ;
      iif(this.Parent.oContained.w_PERRIC=="S",1,;
      0)
  endfunc

  add object oESERC_1_33 as StdField with uid="QVYDMKMJJA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ESERC", cQueryName = "ESERC",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 23248058,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=112, Top=319, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERC"

  func oESERC_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ "S-M-U-P-A" or .w_Analisi = "S")
    endwith
   endif
  endfunc

  func oESERC_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
      if .not. empty(.w_NUMINV)
        bRes2=.link_1_34('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERC_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESERC_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESERC_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oNUMINV_1_34 as StdField with uid="QRTBDIDHQG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 180042282,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=277, Top=319, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERC", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_STANDARD='S' or .w_MEDIOPON or .w_ULTIMO="U" or .w_AGGSTAND = "M") or (.w_ULTIMO="N" and .w_AGGSTAND="U") or (.w_STANDARD="N" and .w_AGGSTAND="S"))
    endwith
   endif
  endfunc

  func oNUMINV_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERC)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINV_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERC
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINV
     i_obj.ecpSave()
  endproc

  add object oListino_1_36 as StdField with uid="RQFSDHQWGW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_Listino", cQueryName = "Listino",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 14534218,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=112, Top=368, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_Listino"

  func oListino_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLISTINO or .w_AGGSTAND='L')
    endwith
   endif
  endfunc

  func oListino_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oListino_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oListino_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oListino_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica listini",'',this.parent.oContained
  endproc

  add object oDESLIS_1_38 as StdField with uid="WGBDTWDQZP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235399882,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=170, Top=368, InputMask=replicate('X',40)

  add object oPRDATINV_1_39 as StdField with uid="UEIFXOYOKT",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 123980980,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=383, Top=319

  add object oCAOLIS_1_54 as StdField with uid="IQCVTLZTCN",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CAOLIS", cQueryName = "CAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 235417306,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=501, Top=368

  func oCAOLIS_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_CLISTINO AND NOT EMPTY(.w_Listino))
    endwith
   endif
  endfunc

  func oCAOLIS_1_54.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR Not .w_CLISTINO OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oCODMAG_1_57 as StdField with uid="XMPGNCFZCF",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli da verificare (spazio=verifica per tutti i magazzini)",;
    HelpContextID = 176672986,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=112, Top=392, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ULTIMO = 'A')
    endwith
   endif
  endfunc

  func oCODMAG_1_57.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc

  func oCODMAG_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_57.ecpDrop(oSource)
    this.Parent.oContained.link_1_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_57.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_57.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_59 as StdField with uid="IGDPZQPCGC",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 176614090,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=170, Top=391, InputMask=replicate('X',30)

  func oDESMAG_1_59.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc

  add object oFLCOSINV_1_61 as StdCheck with uid="FLKVCBFKWG",rtseq=50,rtrep=.f.,left=682, top=375, caption="Applica costi inventario",;
    HelpContextID = 124117844,;
    cFormVar="w_FLCOSINV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCOSINV_1_61.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCOSINV_1_61.GetRadio()
    this.Parent.oContained.w_FLCOSINV = this.RadioValue()
    return .t.
  endfunc

  func oFLCOSINV_1_61.SetRadio()
    this.Parent.oContained.w_FLCOSINV=trim(this.Parent.oContained.w_FLCOSINV)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOSINV=="S",1,;
      0)
  endfunc

  func oFLCOSINV_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MEDIOPON or .w_ULTIMO<>"N" or .w_AGGSTAND $ "MU")
    endwith
   endif
  endfunc


  add object oSTANDARD_1_66 as StdCombo with uid="LSYQNULYMI",rtseq=54,rtrep=.f.,left=18,top=190,width=176,height=22;
    , ToolTipText = "Aggiorna il costo standard";
    , HelpContextID = 5700246;
    , cFormVar="w_STANDARD",RowSource=""+"Nessuno,"+"Standard,"+"Ultimo costo standard (articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTANDARD_1_66.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"S",;
    iif(this.value =3,"X",;
    space(1)))))
  endfunc
  func oSTANDARD_1_66.GetRadio()
    this.Parent.oContained.w_STANDARD = this.RadioValue()
    return .t.
  endfunc

  func oSTANDARD_1_66.SetRadio()
    this.Parent.oContained.w_STANDARD=trim(this.Parent.oContained.w_STANDARD)
    this.value = ;
      iif(this.Parent.oContained.w_STANDARD=="N",1,;
      iif(this.Parent.oContained.w_STANDARD=="S",2,;
      iif(this.Parent.oContained.w_STANDARD=="X",3,;
      0)))
  endfunc


  add object oULTIMO_1_67 as StdCombo with uid="DFATELJXHM",rtseq=55,rtrep=.f.,left=198,top=190,width=176,height=21;
    , ToolTipText = "Aggiorna l'ultimo costo";
    , HelpContextID = 30069434;
    , cFormVar="w_ULTIMO",RowSource=""+"Nessuno,"+"Ultimo costo,"+"Ultimo costo dei saldi (articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oULTIMO_1_67.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"U",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oULTIMO_1_67.GetRadio()
    this.Parent.oContained.w_ULTIMO = this.RadioValue()
    return .t.
  endfunc

  func oULTIMO_1_67.SetRadio()
    this.Parent.oContained.w_ULTIMO=trim(this.Parent.oContained.w_ULTIMO)
    this.value = ;
      iif(this.Parent.oContained.w_ULTIMO=="N",1,;
      iif(this.Parent.oContained.w_ULTIMO=="U",2,;
      iif(this.Parent.oContained.w_ULTIMO=="A",3,;
      0)))
  endfunc


  add object oAGGMPOND_1_68 as StdCombo with uid="JBJPMXBTCH",rtseq=56,rtrep=.f.,left=378,top=190,width=176,height=21;
    , ToolTipText = "Criterio di aggiornamento del costo standard";
    , HelpContextID = 26716342;
    , cFormVar="w_AGGMPOND",RowSource=""+"Nessuno,"+"Costo medio esercizio,"+"Costo medio periodo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAGGMPOND_1_68.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"M",;
    iif(this.value =3,"P",;
    space(1)))))
  endfunc
  func oAGGMPOND_1_68.GetRadio()
    this.Parent.oContained.w_AGGMPOND = this.RadioValue()
    return .t.
  endfunc

  func oAGGMPOND_1_68.SetRadio()
    this.Parent.oContained.w_AGGMPOND=trim(this.Parent.oContained.w_AGGMPOND)
    this.value = ;
      iif(this.Parent.oContained.w_AGGMPOND=="N",1,;
      iif(this.Parent.oContained.w_AGGMPOND=="M",2,;
      iif(this.Parent.oContained.w_AGGMPOND=="P",3,;
      0)))
  endfunc

  add object oCLISTINO_1_69 as StdCheck with uid="GQLGAZXXYJ",rtseq=57,rtrep=.f.,left=564, top=190, caption="Listino",;
    ToolTipText = "Aggiorna il costo di listino",;
    HelpContextID = 122782603,;
    cFormVar="w_CLISTINO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLISTINO_1_69.RadioValue()
    return(iif(this.value =1,True,;
    False))
  endfunc
  func oCLISTINO_1_69.GetRadio()
    this.Parent.oContained.w_CLISTINO = this.RadioValue()
    return .t.
  endfunc

  func oCLISTINO_1_69.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CLISTINO==True,1,;
      0)
  endfunc


  add object oAGGSTAND_1_70 as StdCombo with uid="YKDHNFATUG",rtseq=58,rtrep=.f.,left=66,top=242,width=188,height=21;
    , ToolTipText = "Criterio di aggiornamento del costo standard";
    , HelpContextID = 257009846;
    , cFormVar="w_AGGSTAND",RowSource=""+"Nessuno,"+"Standard,"+"Ultimo costo,"+"Medio ponderato,"+"Listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAGGSTAND_1_70.RadioValue()
    return(iif(this.value =1,"N",;
    iif(this.value =2,"S",;
    iif(this.value =3,"U",;
    iif(this.value =4,"M",;
    iif(this.value =5,"L",;
    space(1)))))))
  endfunc
  func oAGGSTAND_1_70.GetRadio()
    this.Parent.oContained.w_AGGSTAND = this.RadioValue()
    return .t.
  endfunc

  func oAGGSTAND_1_70.SetRadio()
    this.Parent.oContained.w_AGGSTAND=trim(this.Parent.oContained.w_AGGSTAND)
    this.value = ;
      iif(this.Parent.oContained.w_AGGSTAND=="N",1,;
      iif(this.Parent.oContained.w_AGGSTAND=="S",2,;
      iif(this.Parent.oContained.w_AGGSTAND=="U",3,;
      iif(this.Parent.oContained.w_AGGSTAND=="M",4,;
      iif(this.Parent.oContained.w_AGGSTAND=="L",5,;
      0)))))
  endfunc

  add object oNOLIVFIN_1_74 as StdCheck with uid="KFFOHCBTKQ",rtseq=59,rtrep=.f.,left=219, top=294, caption="Implosione livelli finali",;
    ToolTipText = "Implosione livelli finali",;
    HelpContextID = 96776228,;
    cFormVar="w_NOLIVFIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOLIVFIN_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOLIVFIN_1_74.GetRadio()
    this.Parent.oContained.w_NOLIVFIN = this.RadioValue()
    return .t.
  endfunc

  func oNOLIVFIN_1_74.SetRadio()
    this.Parent.oContained.w_NOLIVFIN=trim(this.Parent.oContained.w_NOLIVFIN)
    this.value = ;
      iif(this.Parent.oContained.w_NOLIVFIN=='S',1,;
      0)
  endfunc

  add object oFAMAINI_1_77 as StdField with uid="BAUZFWPTWC",rtseq=60,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 216838486,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=93, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_78 as StdField with uid="GHDDKOKVIG",rtseq=61,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 138628778,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=532, Top=93, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_78.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_78('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_78.ecpDrop(oSource)
    this.Parent.oContained.link_1_78('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_78.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_78'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_79 as StdField with uid="VYQYSTJFCH",rtseq=62,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 129678746,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_79.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_79('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_79.ecpDrop(oSource)
    this.Parent.oContained.link_1_79('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_79.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_79'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_80 as StdField with uid="BKTBROMCMD",rtseq=63,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 51232154,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=532, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_80.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_80('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_80.ecpDrop(oSource)
    this.Parent.oContained.link_1_80('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_80.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_80'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_81 as StdField with uid="WQNLFQEXDD",rtseq=64,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 129687258,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_81('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_81.ecpDrop(oSource)
    this.Parent.oContained.link_1_81('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_81.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_81'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_82 as StdField with uid="WHQQEZYTLE",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 51240666,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=532, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_82.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_82('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_82.ecpDrop(oSource)
    this.Parent.oContained.link_1_82('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_82.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_82'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_83 as StdField with uid="AOYBYAMKXI",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 76409473,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=189, Top=93, InputMask=replicate('X',35)

  add object oDESGRUI_1_84 as StdField with uid="ZABORREMDH",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 192735946,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=189, Top=118, InputMask=replicate('X',35)

  add object oDESCATI_1_85 as StdField with uid="ZFWYARIWSP",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 227601098,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=189, Top=143, InputMask=replicate('X',35)

  add object oDESFAMAF_1_89 as StdField with uid="WOCDSQXKSA",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 76409476,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=600, Top=93, InputMask=replicate('X',35)

  add object oDESGRUF_1_90 as StdField with uid="PYGQUHDJMJ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 75699510,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=600, Top=118, InputMask=replicate('X',35)

  add object oDESCATF_1_91 as StdField with uid="SXSGGSHCII",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 40834358,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=598, Top=143, InputMask=replicate('X',35)

  add object oOUNOMQUE_1_95 as StdField with uid="QUARTVKHNU",rtseq=72,rtrep=.f.,;
    cFormVar = "w_OUNOMQUE", cQueryName = "OUNOMQUE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Query di filtro delle distinte base",;
    HelpContextID = 3855915,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=112, Top=523, InputMask=replicate('X',254)


  add object BTNQRY as cp_askfile with uid="PJDGLKPWNN",left=562, top=523, width=23,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_OUNOMQUE",cExt="VQR",;
    nPag=1;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 98980650

  add object oFLCHKLP_1_99 as StdCheck with uid="SKYASPWUKP",rtseq=75,rtrep=.f.,left=682, top=441, caption="Loop distinta",;
    ToolTipText = "Se attivo abilita controllo del loop in distinta base",;
    HelpContextID = 82633642,;
    cFormVar="w_FLCHKLP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLP_1_99.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCHKLP_1_99.GetRadio()
    this.Parent.oContained.w_FLCHKLP = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLP_1_99.SetRadio()
    this.Parent.oContained.w_FLCHKLP=trim(this.Parent.oContained.w_FLCHKLP)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLP=="S",1,;
      0)
  endfunc

  add object oCOSTILOG_1_102 as StdCheck with uid="AOSDRGZMLX",rtseq=76,rtrep=.f.,left=682, top=463, caption="Attiva scrittura log",;
    ToolTipText = "Attiva la scrittura del log di elaborazione costi",;
    HelpContextID = 83878035,;
    cFormVar="w_COSTILOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOSTILOG_1_102.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCOSTILOG_1_102.GetRadio()
    this.Parent.oContained.w_COSTILOG = this.RadioValue()
    return .t.
  endfunc

  func oCOSTILOG_1_102.SetRadio()
    this.Parent.oContained.w_COSTILOG=trim(this.Parent.oContained.w_COSTILOG)
    this.value = ;
      iif(this.Parent.oContained.w_COSTILOG=="S",1,;
      0)
  endfunc

  add object oAGCOSSTD_1_104 as StdCheck with uid="FGMBRSXMOS",rtseq=77,rtrep=.f.,left=682, top=243, caption="Storico costi prodotto",;
    ToolTipText = "Aggiorna lo storico costi prodotto",;
    HelpContextID = 43652938,;
    cFormVar="w_AGCOSSTD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCOSSTD_1_104.RadioValue()
    return(iif(this.value =1,"S",;
    space(1)))
  endfunc
  func oAGCOSSTD_1_104.GetRadio()
    this.Parent.oContained.w_AGCOSSTD = this.RadioValue()
    return .t.
  endfunc

  func oAGCOSSTD_1_104.SetRadio()
    this.Parent.oContained.w_AGCOSSTD=trim(this.Parent.oContained.w_AGCOSSTD)
    this.value = ;
      iif(this.Parent.oContained.w_AGCOSSTD=="S",1,;
      0)
  endfunc


  add object oTipoValoOut_1_109 as StdCombo with uid="SYIYAENGVB",rtseq=80,rtrep=.f.,left=450,top=242,width=199,height=21;
    , ToolTipText = "Criterio di aggiornamento del costo standard";
    , HelpContextID = 252394347;
    , cFormVar="w_TipoValoOut",RowSource=""+"Costo standard (dati articolo),"+"Costo standard (inventario),"+"Costo medio ponderato per esercizio,"+"Costo medio ponderato per periodo,"+"Costo ultimo,"+"Listino,"+"Prezzo medio ponderato per esercizio,"+"Prezzo medio ponderato per periodo,"+"Ultimo prezzo,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoValoOut_1_109.RadioValue()
    return(iif(this.value =1,'US',;
    iif(this.value =2,'CS',;
    iif(this.value =3,'CA',;
    iif(this.value =4,'CP',;
    iif(this.value =5,'CU',;
    iif(this.value =6,'LI',;
    iif(this.value =7,'PA',;
    iif(this.value =8,'PP',;
    iif(this.value =9,'UP',;
    iif(this.value =10,"NN",;
    space(1))))))))))))
  endfunc
  func oTipoValoOut_1_109.GetRadio()
    this.Parent.oContained.w_TipoValoOut = this.RadioValue()
    return .t.
  endfunc

  func oTipoValoOut_1_109.SetRadio()
    this.Parent.oContained.w_TipoValoOut=trim(this.Parent.oContained.w_TipoValoOut)
    this.value = ;
      iif(this.Parent.oContained.w_TipoValoOut=='US',1,;
      iif(this.Parent.oContained.w_TipoValoOut=='CS',2,;
      iif(this.Parent.oContained.w_TipoValoOut=='CA',3,;
      iif(this.Parent.oContained.w_TipoValoOut=='CP',4,;
      iif(this.Parent.oContained.w_TipoValoOut=='CU',5,;
      iif(this.Parent.oContained.w_TipoValoOut=='LI',6,;
      iif(this.Parent.oContained.w_TipoValoOut=='PA',7,;
      iif(this.Parent.oContained.w_TipoValoOut=='PP',8,;
      iif(this.Parent.oContained.w_TipoValoOut=='UP',9,;
      iif(this.Parent.oContained.w_TipoValoOut=="NN",10,;
      0))))))))))
  endfunc

  func oTipoValoOut_1_109.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SMATOU = "S")
    endwith
   endif
  endfunc

  func oTipoValoOut_1_109.mHide()
    with this.Parent.oContained
      return (!.w_CICLI)
    endwith
  endfunc

  add object oSMATOU_1_110 as StdCheck with uid="BSHLGPISIO",rtseq=81,rtrep=.f.,left=312, top=242, caption="Materiali di output",;
    ToolTipText = "Stampa i costi dei materiali di output",;
    HelpContextID = 195101146,;
    cFormVar="w_SMATOU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSMATOU_1_110.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSMATOU_1_110.GetRadio()
    this.Parent.oContained.w_SMATOU = this.RadioValue()
    return .t.
  endfunc

  func oSMATOU_1_110.SetRadio()
    this.Parent.oContained.w_SMATOU=trim(this.Parent.oContained.w_SMATOU)
    this.value = ;
      iif(this.Parent.oContained.w_SMATOU=="S",1,;
      0)
  endfunc

  func oSMATOU_1_110.mHide()
    with this.Parent.oContained
      return (!.w_CICLI)
    endwith
  endfunc

  add object oLOTTOMED_1_112 as StdCheck with uid="LUGZPIIGZF",rtseq=82,rtrep=.f.,left=682, top=294, caption="Su lotto medio", enabled=.f.,;
    ToolTipText = "Calcola i costi di fissi in funzione del lotto medio",;
    HelpContextID = 207630330,;
    cFormVar="w_LOTTOMED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLOTTOMED_1_112.RadioValue()
    return(iif(this.value =1,True,;
    False))
  endfunc
  func oLOTTOMED_1_112.GetRadio()
    this.Parent.oContained.w_LOTTOMED = this.RadioValue()
    return .t.
  endfunc

  func oLOTTOMED_1_112.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LOTTOMED==True,1,;
      0)
  endfunc


  add object oINESCONT_1_113 as StdCombo with uid="IYNJNDEZRH",rtseq=83,rtrep=.f.,left=469,top=319,width=156,height=22;
    , ToolTipText = "Metodo di calcolo del costo e prezzo medio esercizio";
    , HelpContextID = 39960870;
    , cFormVar="w_INESCONT",RowSource=""+"Standard,"+"Continuo,"+"Per movimento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINESCONT_1_113.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oINESCONT_1_113.GetRadio()
    this.Parent.oContained.w_INESCONT = this.RadioValue()
    return .t.
  endfunc

  func oINESCONT_1_113.SetRadio()
    this.Parent.oContained.w_INESCONT=trim(this.Parent.oContained.w_INESCONT)
    this.value = ;
      iif(this.Parent.oContained.w_INESCONT=='N',1,;
      iif(this.Parent.oContained.w_INESCONT=='S',2,;
      iif(this.Parent.oContained.w_INESCONT=='M',3,;
      0)))
  endfunc

  func oINESCONT_1_113.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_NUMINV))
    endwith
   endif
  endfunc

  func oINESCONT_1_113.mHide()
    with this.Parent.oContained
      return (.w_Analisi="N" and .w_TipoValo <> "M")
    endwith
  endfunc

  add object oESERCMO_1_118 as StdField with uid="UQEJHJLUJV",rtseq=85,rtrep=.f.,;
    cFormVar = "w_ESERCMO", cQueryName = "ESERCMO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 73579706,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=112, Top=441, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCMO"

  func oESERCMO_1_118.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  func oESERCMO_1_118.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_118('Part',this)
      if .not. empty(.w_NUMINVMO)
        bRes2=.link_1_119('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERCMO_1_118.ecpDrop(oSource)
    this.Parent.oContained.link_1_118('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oNUMINVMO_1_119 as StdField with uid="EZVRCZLAGN",rtseq=86,rtrep=.f.,;
    cFormVar = "w_NUMINVMO", cQueryName = "NUMINVMO",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali di output",;
    HelpContextID = 180042203,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=281, Top=441, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERCMO", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINVMO"

  func oNUMINVMO_1_119.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  func oNUMINVMO_1_119.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_119('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINVMO_1_119.ecpDrop(oSource)
    this.Parent.oContained.link_1_119('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINVMO_1_119.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERCMO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERCMO)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINVMO_1_119'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINVMO_1_119.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERCMO
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINVMO
     i_obj.ecpSave()
  endproc

  add object oListinoMo_1_121 as StdField with uid="YFTLERWREL",rtseq=87,rtrep=.f.,;
    cFormVar = "w_ListinoMo", cQueryName = "ListinoMo",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali di output",;
    HelpContextID = 14532365,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=112, Top=465, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ListinoMo"

  func oListinoMo_1_121.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  func oListinoMo_1_121.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_121('Part',this)
    endwith
    return bRes
  endfunc

  proc oListinoMo_1_121.ecpDrop(oSource)
    this.Parent.oContained.link_1_121('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oListinoMo_1_121.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oListinoMo_1_121'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica listini",'',this.parent.oContained
  endproc

  add object oDESLISMO_1_123 as StdField with uid="KOEYJNGEOP",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESLISMO", cQueryName = "DESLISMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235399803,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=170, Top=465, InputMask=replicate('X',40)

  func oDESLISMO_1_123.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  add object oDATINVMO_1_124 as StdField with uid="AQZCXIFWVE",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DATINVMO", cQueryName = "DATINVMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 180018811,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=386, Top=441

  func oDATINVMO_1_124.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oCAOLISMO_1_126 as StdField with uid="MOTUECDKNJ",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CAOLISMO", cQueryName = "CAOLISMO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 235417227,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=501, Top=465

  func oCAOLISMO_1_126.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVALMO=0 AND .w_TipoValoOut="LI" AND NOT EMPTY(.w_ListinoMo))
    endwith
   endif
  endfunc

  func oCAOLISMO_1_126.mHide()
    with this.Parent.oContained
      return (.w_CAOVALMO<>0 OR .w_TipoValoOut<>"LI" OR EMPTY(.w_ListinoMo))
    endwith
  endfunc

  add object oCODMAGMO_1_130 as StdField with uid="UITLEHGIOC",rtseq=91,rtrep=.f.,;
    cFormVar = "w_CODMAGMO", cQueryName = "CODMAGMO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli da verificare (spazio=verifica per tutti i magazzini)",;
    HelpContextID = 176672907,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=112, Top=490, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAGMO"

  func oCODMAGMO_1_130.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ULTIMO = 'A')
    endwith
   endif
  endfunc

  func oCODMAGMO_1_130.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc

  func oCODMAGMO_1_130.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_130('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAGMO_1_130.ecpDrop(oSource)
    this.Parent.oContained.link_1_130('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAGMO_1_130.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAGMO_1_130'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAGMO_1_130.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAGMO
     i_obj.ecpSave()
  endproc

  add object oDESMAGMO_1_132 as StdField with uid="SLCULEYYGL",rtseq=92,rtrep=.f.,;
    cFormVar = "w_DESMAGMO", cQueryName = "DESMAGMO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 176614011,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=170, Top=489, InputMask=replicate('X',30)

  func oDESMAGMO_1_132.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc


  add object oTIPVALCIC_1_133 as StdCombo with uid="IXIOKXTFBG",rtseq=93,rtrep=.f.,left=597,top=60,width=196,height=21;
    , ToolTipText = "Seleziona il criterio di valorizzazione del ciclo";
    , HelpContextID = 176287407;
    , cFormVar="w_TIPVALCIC",RowSource=""+"Ciclo preferenziale,"+"Da anagrafica ciclo,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPVALCIC_1_133.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'99',;
    iif(this.value =3,'NN',;
    space(2)))))
  endfunc
  func oTIPVALCIC_1_133.GetRadio()
    this.Parent.oContained.w_TIPVALCIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPVALCIC_1_133.SetRadio()
    this.Parent.oContained.w_TIPVALCIC=trim(this.Parent.oContained.w_TIPVALCIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVALCIC=='00',1,;
      iif(this.Parent.oContained.w_TIPVALCIC=='99',2,;
      iif(this.Parent.oContained.w_TIPVALCIC=='NN',3,;
      0)))
  endfunc

  func oTIPVALCIC_1_133.mHide()
    with this.Parent.oContained
      return (!.w_CICLI)
    endwith
  endfunc

  add object oTIPCOS_1_137 as StdCheck with uid="VERNBIWUUK",rtseq=96,rtrep=.f.,left=682, top=320, caption="Costificazione parziale",;
    ToolTipText = "Se attivo: la costificazione avverr� in funzione del check esplosione impostato in distinta",;
    HelpContextID = 229709258,;
    cFormVar="w_TIPCOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCOS_1_137.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCOS_1_137.GetRadio()
    this.Parent.oContained.w_TIPCOS = this.RadioValue()
    return .t.
  endfunc

  func oTIPCOS_1_137.SetRadio()
    this.Parent.oContained.w_TIPCOS=trim(this.Parent.oContained.w_TIPCOS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCOS=='S',1,;
      0)
  endfunc

  add object oPDCOECOS_1_138 as StdCheck with uid="WHCLBVISGJ",rtseq=97,rtrep=.f.,left=682, top=348, caption="Considera coeff. impiego",;
    ToolTipText = "Considera coeff. impiego in fase di costificazione",;
    HelpContextID = 239463095,;
    cFormVar="w_PDCOECOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCOECOS_1_138.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPDCOECOS_1_138.GetRadio()
    this.Parent.oContained.w_PDCOECOS = this.RadioValue()
    return .t.
  endfunc

  func oPDCOECOS_1_138.SetRadio()
    this.Parent.oContained.w_PDCOECOS=trim(this.Parent.oContained.w_PDCOECOS)
    this.value = ;
      iif(this.Parent.oContained.w_PDCOECOS=='S',1,;
      0)
  endfunc

  add object oFLDETTMAG_1_144 as StdCheck with uid="GQTWAXZVRU",rtseq=103,rtrep=.f.,left=278, top=344, caption="Aggiorna costi dettaglio magazzini",;
    HelpContextID = 207605497,;
    cFormVar="w_FLDETTMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDETTMAG_1_144.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLDETTMAG_1_144.GetRadio()
    this.Parent.oContained.w_FLDETTMAG = this.RadioValue()
    return .t.
  endfunc

  func oFLDETTMAG_1_144.SetRadio()
    this.Parent.oContained.w_FLDETTMAG=trim(this.Parent.oContained.w_FLDETTMAG)
    this.value = ;
      iif(this.Parent.oContained.w_FLDETTMAG=="S",1,;
      0)
  endfunc

  func oFLDETTMAG_1_144.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MEDIOPON or .w_ULTIMO<>"N" or .w_STANDARD='S' or .w_AGGSTAND $ "MU" and !Empty(.w_NUMINV) and !Empty(.w_MAGRAG))
    endwith
   endif
  endfunc

  add object oStr_1_18 as StdString with uid="AZAYVNOACS",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=114, Height=15,;
    Caption="Da distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YBUTHVTDPZ",Visible=.t., Left=20, Top=38,;
    Alignment=1, Width=99, Height=15,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LPGIBLLYAF",Visible=.t., Left=20, Top=68,;
    Alignment=1, Width=99, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="WNERIRPNMR",Visible=.t., Left=14, Top=370,;
    Alignment=1, Width=95, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="XROSXMIRDD",Visible=.t., Left=19, Top=320,;
    Alignment=1, Width=90, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="CMQQHAIKKN",Visible=.t., Left=170, Top=320,;
    Alignment=1, Width=104, Height=15,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ZFXOXTIOFY",Visible=.t., Left=347, Top=320,;
    Alignment=1, Width=32, Height=15,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="MACUDGSSKF",Visible=.t., Left=427, Top=370,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR Not .w_CLISTINO OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="QCEJHAMSPS",Visible=.t., Left=14, Top=395,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="LMDTBFEMQW",Visible=.t., Left=19, Top=169,;
    Alignment=0, Width=249, Height=18,;
    Caption="Costi valorizzazione materie prime"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="OGJQPZMBDU",Visible=.t., Left=17, Top=220,;
    Alignment=0, Width=233, Height=18,;
    Caption="Aggiornamento costo standard articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="FRXPGPJUNO",Visible=.t., Left=17, Top=269,;
    Alignment=0, Width=213, Height=18,;
    Caption="Opzioni di valorizzazione materiali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=12, Top=96,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=12, Top=120,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=12, Top=145,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="BETNLUNOYI",Visible=.t., Left=460, Top=96,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=435, Top=120,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=436, Top=145,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="BQPXSTBRUO",Visible=.t., Left=1, Top=526,;
    Alignment=1, Width=109, Height=18,;
    Caption="Query"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="SJOQRQEYXN",Visible=.t., Left=684, Top=418,;
    Alignment=0, Width=71, Height=18,;
    Caption="Controllo"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="SFSJPLKTDE",Visible=.t., Left=684, Top=220,;
    Alignment=0, Width=137, Height=18,;
    Caption="Storicizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="NUGERPDILA",Visible=.t., Left=313, Top=220,;
    Alignment=0, Width=249, Height=18,;
    Caption="Criterio valorizzazione materiali di output"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (!.w_CICLI)
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="TMCYOPGXZF",Visible=.t., Left=401, Top=298,;
    Alignment=0, Width=223, Height=15,;
    Caption="Opzioni costo medio ponderato esercizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="PRFSUKMZOZ",Visible=.t., Left=36, Top=468,;
    Alignment=1, Width=75, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (.w_TipoValoOut<>"LI")
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="EXEPDHPGVR",Visible=.t., Left=38, Top=443,;
    Alignment=1, Width=75, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_122 as StdString with uid="PIYYUNCMCZ",Visible=.t., Left=160, Top=443,;
    Alignment=1, Width=119, Height=18,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_122.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="UNQANDMSCL",Visible=.t., Left=352, Top=443,;
    Alignment=1, Width=31, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValoOut $ "CS-CA-CP-CU-PA-PP-UP"))
    endwith
  endfunc

  add object oStr_1_127 as StdString with uid="OOPLVOKRTK",Visible=.t., Left=427, Top=468,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_127.mHide()
    with this.Parent.oContained
      return (.w_CAOVALMO<>0 OR .w_TipoValoOut<>"L" OR EMPTY(.w_ListinoMo))
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="HUGZCDIZJK",Visible=.t., Left=17, Top=418,;
    Alignment=0, Width=259, Height=18,;
    Caption="Opzioni di valorizzazione materiali di output:"  ;
  , bGlobalFont=.t.

  add object oStr_1_131 as StdString with uid="MGDXNDTTYX",Visible=.t., Left=18, Top=493,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_131.mHide()
    with this.Parent.oContained
      return (.w_ULTIMO <> 'A')
    endwith
  endfunc

  add object oStr_1_134 as StdString with uid="VUNLBHDDVA",Visible=.t., Left=522, Top=62,;
    Alignment=1, Width=74, Height=18,;
    Caption="Lavorazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_134.mHide()
    with this.Parent.oContained
      return (!.w_CICLI)
    endwith
  endfunc

  add object oBox_1_71 as StdBox with uid="HWORLOGRGK",left=15, top=186, width=617,height=1

  add object oBox_1_73 as StdBox with uid="ASSHMQODQL",left=15, top=237, width=241,height=1

  add object oBox_1_76 as StdBox with uid="QWYSVYUMIJ",left=15, top=286, width=617,height=2

  add object oBox_1_101 as StdBox with uid="ZXUPTCHTYW",left=679, top=435, width=142,height=1

  add object oBox_1_105 as StdBox with uid="EWQTFRJNLY",left=679, top=237, width=142,height=1

  add object oBox_1_111 as StdBox with uid="SXZVFEXLUP",left=311, top=237, width=342,height=1

  add object oBox_1_115 as StdBox with uid="EBGDQVQSNS",left=396, top=315, width=234,height=1

  add object oBox_1_129 as StdBox with uid="XMGMNCBGEV",left=15, top=435, width=617,height=2
enddefine
define class tgsds_kcsPag2 as StdContainer
  Width  = 847
  height = 551
  stdWidth  = 847
  stdheight = 551
  resizeXpos=410
  resizeYpos=238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SelDis as cp_szoombox with uid="SHVQPKONZP",left=-4, top=4, width=847,height=496,;
    caption='Browse',;
   bGlobalFont=.t.,;
    cTable="DISMBASE",cZoomFile="GSDSSKCS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 113574422


  add object oBtn_2_2 as StdButton with uid="VIVRYPRXRO",left=793, top=503, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 260199702;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_3 as StdRadio with uid="PGKTVHMOKS",rtseq=73,rtrep=.f.,left=4, top=503, width=139,height=50;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 117397978
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 117397978
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 117397978
      this.Buttons(3).Top=32
      this.SetAll("Width",137)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'Z',;
    space(1)))))
  endfunc
  func oSELEZI_2_3.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_3.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      iif(this.Parent.oContained.w_SELEZI=='Z',3,;
      0)))
  endfunc


  add object oBtn_2_5 as StdButton with uid="ZYQNADMBYV",left=738, top=503, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per elaborare il costo standard";
    , HelpContextID = 57613382;
    , Caption='E\<labora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSDS1BDC(this.Parent.oContained,"ELABORA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_CLISTINO OR .w_STANDARD<>"N" OR .w_MEDIOPON OR .w_ULTIMO<>"N" OR .w_UTENTE))
      endwith
    endif
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_kcs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
