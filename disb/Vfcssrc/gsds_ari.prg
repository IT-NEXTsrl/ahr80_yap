* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_ari                                                        *
*              Tabella risorse                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_22]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-26                                                      *
* Last revis.: 2014-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_ari"))

* --- Class definition
define class tgsds_ari as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 528
  Height = 258+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-21"
  HelpContextID=141990807
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  TAB_RISO_IDX = 0
  UNIMIS_IDX = 0
  VALUTE_IDX = 0
  RIS_ORSE_IDX = 0
  cFile = "TAB_RISO"
  cKeySelect = "RICODICE"
  cKeyWhere  = "RICODICE=this.w_RICODICE"
  cKeyWhereODBC = '"RICODICE="+cp_ToStrODBC(this.w_RICODICE)';

  cKeyWhereODBCqualified = '"TAB_RISO.RICODICE="+cp_ToStrODBC(this.w_RICODICE)';

  cPrg = "gsds_ari"
  cComment = "Tabella risorse"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RICODICE = space(15)
  w_RIDESCRI = space(40)
  w_RITPOREP = space(2)
  w_RIDESSUP = space(0)
  w_RICODREP = space(15)
  w_DESREP = space(40)
  w_RIUNMIS1 = space(3)
  w_RIUNMIS2 = space(3)
  o_RIUNMIS2 = space(3)
  w_RIOPERAT = space(1)
  w_RIMOLTIP = 0
  w_RIPREZZO = 0
  w_RICODVAL = space(3)
  o_RICODVAL = space(3)
  w_DECUNI = 0
  w_DECMAX = 0
  w_CALCPICU = 0
  w_TIPREP = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_RISO','gsds_ari')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_ariPag1","gsds_ari",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tabella risorse")
      .Pages(1).HelpContextID = 104383094
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='TAB_RISO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_RISO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_RISO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RICODICE = NVL(RICODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_20_joined
    link_1_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_RISO where RICODICE=KeySet.RICODICE
    *
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_RISO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_RISO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_RISO '
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESREP = space(40)
        .w_DECUNI = 0
        .w_TIPREP = 'RE'
        .w_RICODICE = NVL(RICODICE,space(15))
        .w_RIDESCRI = NVL(RIDESCRI,space(40))
        .w_RITPOREP = NVL(RITPOREP,space(2))
        .w_RIDESSUP = NVL(RIDESSUP,space(0))
        .w_RICODREP = NVL(RICODREP,space(15))
          .link_1_6('Load')
        .w_RIUNMIS1 = NVL(RIUNMIS1,space(3))
          * evitabile
          *.link_1_8('Load')
        .w_RIUNMIS2 = NVL(RIUNMIS2,space(3))
          * evitabile
          *.link_1_9('Load')
        .w_RIOPERAT = NVL(RIOPERAT,space(1))
        .w_RIMOLTIP = NVL(RIMOLTIP,0)
        .w_RIPREZZO = NVL(RIPREZZO,0)
        .w_RICODVAL = NVL(RICODVAL,space(3))
          if link_1_20_joined
            this.w_RICODVAL = NVL(VACODVAL120,NVL(this.w_RICODVAL,space(3)))
            this.w_DECUNI = NVL(VADECUNI120,0)
          else
          .link_1_20('Load')
          endif
        .w_DECMAX = IIF(.w_DECUNI>5,5,.w_DECUNI)
        .w_CALCPICU = DEFPIU(.w_DECMAX)
        cp_LoadRecExtFlds(this,'TAB_RISO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RICODICE = space(15)
      .w_RIDESCRI = space(40)
      .w_RITPOREP = space(2)
      .w_RIDESSUP = space(0)
      .w_RICODREP = space(15)
      .w_DESREP = space(40)
      .w_RIUNMIS1 = space(3)
      .w_RIUNMIS2 = space(3)
      .w_RIOPERAT = space(1)
      .w_RIMOLTIP = 0
      .w_RIPREZZO = 0
      .w_RICODVAL = space(3)
      .w_DECUNI = 0
      .w_DECMAX = 0
      .w_CALCPICU = 0
      .w_TIPREP = space(2)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_RITPOREP = 'RE'
        .DoRTCalc(4,5,.f.)
          if not(empty(.w_RICODREP))
          .link_1_6('Full')
          endif
        .DoRTCalc(6,7,.f.)
          if not(empty(.w_RIUNMIS1))
          .link_1_8('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_RIUNMIS2))
          .link_1_9('Full')
          endif
        .w_RIOPERAT = '*'
        .w_RIMOLTIP = 0
          .DoRTCalc(11,11,.f.)
        .w_RICODVAL = g_PERVAL
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_RICODVAL))
          .link_1_20('Full')
          endif
          .DoRTCalc(13,13,.f.)
        .w_DECMAX = IIF(.w_DECUNI>5,5,.w_DECUNI)
        .w_CALCPICU = DEFPIU(.w_DECMAX)
        .w_TIPREP = 'RE'
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_RISO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRICODICE_1_1.enabled = i_bVal
      .Page1.oPag.oRIDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oRIDESSUP_1_5.enabled = i_bVal
      .Page1.oPag.oRICODREP_1_6.enabled = i_bVal
      .Page1.oPag.oRIUNMIS1_1_8.enabled = i_bVal
      .Page1.oPag.oRIUNMIS2_1_9.enabled = i_bVal
      .Page1.oPag.oRIOPERAT_1_10.enabled = i_bVal
      .Page1.oPag.oRIMOLTIP_1_11.enabled = i_bVal
      .Page1.oPag.oRIPREZZO_1_12.enabled = i_bVal
      .Page1.oPag.oRICODVAL_1_20.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRICODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRICODICE_1_1.enabled = .t.
        .Page1.oPag.oRIDESCRI_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_RISO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODICE,"RICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDESCRI,"RIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RITPOREP,"RITPOREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDESSUP,"RIDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODREP,"RICODREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIUNMIS1,"RIUNMIS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIUNMIS2,"RIUNMIS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIOPERAT,"RIOPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIMOLTIP,"RIMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIPREZZO,"RIPREZZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODVAL,"RICODVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    i_lTable = "TAB_RISO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_RISO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsds_sri with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_RISO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_RISO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_RISO')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_RISO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RICODICE,RIDESCRI,RITPOREP,RIDESSUP,RICODREP"+;
                  ",RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO"+;
                  ",RICODVAL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RICODICE)+;
                  ","+cp_ToStrODBC(this.w_RIDESCRI)+;
                  ","+cp_ToStrODBC(this.w_RITPOREP)+;
                  ","+cp_ToStrODBC(this.w_RIDESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_RICODREP)+;
                  ","+cp_ToStrODBCNull(this.w_RIUNMIS1)+;
                  ","+cp_ToStrODBCNull(this.w_RIUNMIS2)+;
                  ","+cp_ToStrODBC(this.w_RIOPERAT)+;
                  ","+cp_ToStrODBC(this.w_RIMOLTIP)+;
                  ","+cp_ToStrODBC(this.w_RIPREZZO)+;
                  ","+cp_ToStrODBCNull(this.w_RICODVAL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_RISO')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_RISO')
        cp_CheckDeletedKey(i_cTable,0,'RICODICE',this.w_RICODICE)
        INSERT INTO (i_cTable);
              (RICODICE,RIDESCRI,RITPOREP,RIDESSUP,RICODREP,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RICODICE;
                  ,this.w_RIDESCRI;
                  ,this.w_RITPOREP;
                  ,this.w_RIDESSUP;
                  ,this.w_RICODREP;
                  ,this.w_RIUNMIS1;
                  ,this.w_RIUNMIS2;
                  ,this.w_RIOPERAT;
                  ,this.w_RIMOLTIP;
                  ,this.w_RIPREZZO;
                  ,this.w_RICODVAL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_RISO_IDX,i_nConn)
      *
      * update TAB_RISO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_RISO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RIDESCRI="+cp_ToStrODBC(this.w_RIDESCRI)+;
             ",RITPOREP="+cp_ToStrODBC(this.w_RITPOREP)+;
             ",RIDESSUP="+cp_ToStrODBC(this.w_RIDESSUP)+;
             ",RICODREP="+cp_ToStrODBCNull(this.w_RICODREP)+;
             ",RIUNMIS1="+cp_ToStrODBCNull(this.w_RIUNMIS1)+;
             ",RIUNMIS2="+cp_ToStrODBCNull(this.w_RIUNMIS2)+;
             ",RIOPERAT="+cp_ToStrODBC(this.w_RIOPERAT)+;
             ",RIMOLTIP="+cp_ToStrODBC(this.w_RIMOLTIP)+;
             ",RIPREZZO="+cp_ToStrODBC(this.w_RIPREZZO)+;
             ",RICODVAL="+cp_ToStrODBCNull(this.w_RICODVAL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_RISO')
        i_cWhere = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
        UPDATE (i_cTable) SET;
              RIDESCRI=this.w_RIDESCRI;
             ,RITPOREP=this.w_RITPOREP;
             ,RIDESSUP=this.w_RIDESSUP;
             ,RICODREP=this.w_RICODREP;
             ,RIUNMIS1=this.w_RIUNMIS1;
             ,RIUNMIS2=this.w_RIUNMIS2;
             ,RIOPERAT=this.w_RIOPERAT;
             ,RIMOLTIP=this.w_RIMOLTIP;
             ,RIPREZZO=this.w_RIPREZZO;
             ,RICODVAL=this.w_RICODVAL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_RISO_IDX,i_nConn)
      *
      * delete TAB_RISO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RICODICE',this.w_RICODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_RIUNMIS2<>.w_RIUNMIS2
            .w_RIMOLTIP = 0
        endif
        .DoRTCalc(11,13,.t.)
            .w_DECMAX = IIF(.w_DECUNI>5,5,.w_DECUNI)
        if .o_RICODVAL<>.w_RICODVAL
            .w_CALCPICU = DEFPIU(.w_DECMAX)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRIOPERAT_1_10.enabled = this.oPgFrm.Page1.oPag.oRIOPERAT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oRIMOLTIP_1_11.enabled = this.oPgFrm.Page1.oPag.oRIMOLTIP_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RICODREP
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ARP',True,'RIS_ORSE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RLCODICE like "+cp_ToStrODBC(trim(this.w_RICODREP)+"%");
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPREP);

          i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RL__TIPO,RLCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RL__TIPO',this.w_TIPREP;
                     ,'RLCODICE',trim(this.w_RICODREP))
          select RL__TIPO,RLCODICE,RLDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RL__TIPO,RLCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODREP)==trim(_Link_.RLCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODREP) and !this.bDontReportError
            deferred_cp_zoom('RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(oSource.parent,'oRICODREP_1_6'),i_cWhere,'GSDS_ARP',"Elenco reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPREP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPREP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',oSource.xKey(1);
                       ,'RLCODICE',oSource.xKey(2))
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_RICODREP);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPREP;
                       ,'RLCODICE',this.w_RICODREP)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODREP = NVL(_Link_.RLCODICE,space(15))
      this.w_DESREP = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICODREP = space(15)
      endif
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RIUNMIS1
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIUNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RIUNMIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_RIUNMIS1))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIUNMIS1)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIUNMIS1) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oRIUNMIS1_1_8'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIUNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RIUNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_RIUNMIS1)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIUNMIS1 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_RIUNMIS1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIUNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RIUNMIS2
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIUNMIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_RIUNMIS2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_RIUNMIS2))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RIUNMIS2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RIUNMIS2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oRIUNMIS2_1_9'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIUNMIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_RIUNMIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_RIUNMIS2)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIUNMIS2 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_RIUNMIS2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_RIUNMIS2<>.w_RIUNMIS1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_RIUNMIS2 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIUNMIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICODVAL
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_RICODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_RICODVAL))
          select VACODVAL,VADECUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADECUNI like "+cp_ToStrODBC(trim(this.w_RICODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADECUNI like "+cp_ToStr(trim(this.w_RICODVAL)+"%");

            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RICODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oRICODVAL_1_20'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_RICODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_RICODVAL)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_RICODVAL = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.VACODVAL as VACODVAL120"+ ",link_1_20.VADECUNI as VADECUNI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on TAB_RISO.RICODVAL=link_1_20.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and TAB_RISO.RICODVAL=link_1_20.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRICODICE_1_1.value==this.w_RICODICE)
      this.oPgFrm.Page1.oPag.oRICODICE_1_1.value=this.w_RICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDESCRI_1_2.value==this.w_RIDESCRI)
      this.oPgFrm.Page1.oPag.oRIDESCRI_1_2.value=this.w_RIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDESSUP_1_5.value==this.w_RIDESSUP)
      this.oPgFrm.Page1.oPag.oRIDESSUP_1_5.value=this.w_RIDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODREP_1_6.value==this.w_RICODREP)
      this.oPgFrm.Page1.oPag.oRICODREP_1_6.value=this.w_RICODREP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_1_7.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_1_7.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page1.oPag.oRIUNMIS1_1_8.value==this.w_RIUNMIS1)
      this.oPgFrm.Page1.oPag.oRIUNMIS1_1_8.value=this.w_RIUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oRIUNMIS2_1_9.value==this.w_RIUNMIS2)
      this.oPgFrm.Page1.oPag.oRIUNMIS2_1_9.value=this.w_RIUNMIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oRIOPERAT_1_10.RadioValue()==this.w_RIOPERAT)
      this.oPgFrm.Page1.oPag.oRIOPERAT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRIMOLTIP_1_11.value==this.w_RIMOLTIP)
      this.oPgFrm.Page1.oPag.oRIMOLTIP_1_11.value=this.w_RIMOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oRIPREZZO_1_12.value==this.w_RIPREZZO)
      this.oPgFrm.Page1.oPag.oRIPREZZO_1_12.value=this.w_RIPREZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODVAL_1_20.value==this.w_RICODVAL)
      this.oPgFrm.Page1.oPag.oRICODVAL_1_20.value=this.w_RICODVAL
    endif
    cp_SetControlsValueExtFlds(this,'TAB_RISO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRICODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_RICODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RIUNMIS1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIUNMIS1_1_8.SetFocus()
            i_bnoObbl = !empty(.w_RIUNMIS1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_RIUNMIS2<>.w_RIUNMIS1)  and not(empty(.w_RIUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIUNMIS2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_RIMOLTIP))  and (NOT EMPTY(.w_RIUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRIMOLTIP_1_11.SetFocus()
            i_bnoObbl = !empty(.w_RIMOLTIP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RIUNMIS2 = this.w_RIUNMIS2
    this.o_RICODVAL = this.w_RICODVAL
    return

enddefine

* --- Define pages as container
define class tgsds_ariPag1 as StdContainer
  Width  = 524
  height = 258
  stdWidth  = 524
  stdheight = 258
  resizeXpos=429
  resizeYpos=106
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRICODICE_1_1 as StdField with uid="DLWKUQLFLD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RICODICE", cQueryName = "RICODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice risorsa da utilizzare nei cicli semplificati",;
    HelpContextID = 167110053,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=107, Top=16, InputMask=replicate('X',15)

  add object oRIDESCRI_1_2 as StdField with uid="ZPJAINGFIT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RIDESCRI", cQueryName = "RIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione risorsa",;
    HelpContextID = 15739487,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=107, Top=45, InputMask=replicate('X',40)

  add object oRIDESSUP_1_5 as StdMemo with uid="ZTIHUCIWVC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RIDESSUP", cQueryName = "RIDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 252695962,;
   bGlobalFont=.t.,;
    Height=53, Width=414, Left=107, Top=74

  add object oRICODREP_1_6 as StdField with uid="PNSOUMIZPU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RICODREP", cQueryName = "RICODREP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del reparto associato alla risorsa",;
    HelpContextID = 16115098,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=107, Top=135, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="RIS_ORSE", cZoomOnZoom="GSDS_ARP", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPREP", oKey_2_1="RLCODICE", oKey_2_2="this.w_RICODREP"

  func oRICODREP_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODREP_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODREP_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIS_ORSE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPREP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RL__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPREP)
    endif
    do cp_zoom with 'RIS_ORSE','*','RL__TIPO,RLCODICE',cp_AbsName(this.parent,'oRICODREP_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ARP',"Elenco reparti",'',this.parent.oContained
  endproc
  proc oRICODREP_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.RL__TIPO=w_TIPREP
     i_obj.w_RLCODICE=this.parent.oContained.w_RICODREP
     i_obj.ecpSave()
  endproc

  add object oDESREP_1_7 as StdField with uid="KEGJOBKRTQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220075318,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=229, Top=135, InputMask=replicate('X',40)

  add object oRIUNMIS1_1_8 as StdField with uid="TOTOIMWOXX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RIUNMIS1", cQueryName = "RIUNMIS1",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 157664697,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=107, Top=195, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_RIUNMIS1"

  func oRIUNMIS1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIUNMIS1_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIUNMIS1_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oRIUNMIS1_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oRIUNMIS1_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_RIUNMIS1
     i_obj.ecpSave()
  endproc

  add object oRIUNMIS2_1_9 as StdField with uid="TCEQBUQQAT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RIUNMIS2", cQueryName = "RIUNMIS2",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale seconda unit� di misura",;
    HelpContextID = 157664696,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=229, Top=195, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_RIUNMIS2"

  func oRIUNMIS2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oRIUNMIS2_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRIUNMIS2_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oRIUNMIS2_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oRIUNMIS2_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_RIUNMIS2
     i_obj.ecpSave()
  endproc


  add object oRIOPERAT_1_10 as StdCombo with uid="UAYOKYLNCY",rtseq=9,rtrep=.f.,left=279,top=195,width=35,height=21;
    , ToolTipText = "Operatore (moltip. o divis.) tra la 2^U.M. e la 1^U.M.";
    , HelpContextID = 253483626;
    , cFormVar="w_RIOPERAT",RowSource=""+"X,"+"/", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRIOPERAT_1_10.RadioValue()
    return(iif(this.value =1,'*',;
    iif(this.value =2,'/',;
    space(1))))
  endfunc
  func oRIOPERAT_1_10.GetRadio()
    this.Parent.oContained.w_RIOPERAT = this.RadioValue()
    return .t.
  endfunc

  func oRIOPERAT_1_10.SetRadio()
    this.Parent.oContained.w_RIOPERAT=trim(this.Parent.oContained.w_RIOPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_RIOPERAT=='*',1,;
      iif(this.Parent.oContained.w_RIOPERAT=='/',2,;
      0))
  endfunc

  func oRIOPERAT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_RIUNMIS2))
    endwith
   endif
  endfunc

  add object oRIMOLTIP_1_11 as StdField with uid="ORBHPHKOYU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_RIMOLTIP", cQueryName = "RIMOLTIP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di rapporto tra la 1^U.M. e la 2^U.M.",;
    HelpContextID = 242566554,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=327, Top=195, cSayPict='"99999.9999"', cGetPict='"99999.9999"'

  func oRIMOLTIP_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_RIUNMIS2))
    endwith
   endif
  endfunc

  add object oRIPREZZO_1_12 as StdField with uid="ARYCZMEBTY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RIPREZZO", cQueryName = "RIPREZZO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario della risorsa",;
    HelpContextID = 119401061,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=107, Top=226, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oRICODVAL_1_20 as StdField with uid="CLDWQJXDDM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RICODVAL", cQueryName = "RICODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della valuta nella quale � espresso il costo",;
    HelpContextID = 50993762,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=277, Top=226, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_RICODVAL"

  func oRICODVAL_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODVAL_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODVAL_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oRICODVAL_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oRICODVAL_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_RICODVAL
     i_obj.ecpSave()
  endproc

  add object oStr_1_3 as StdString with uid="DCWUDGDIDD",Visible=.t., Left=20, Top=45,;
    Alignment=1, Width=85, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="SJRTZUMUXI",Visible=.t., Left=18, Top=226,;
    Alignment=1, Width=87, Height=18,;
    Caption="Costo unitario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="NLKOPFNCIN",Visible=.t., Left=327, Top=177,;
    Alignment=0, Width=91, Height=15,;
    Caption="Parametro"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ACMKLISIPI",Visible=.t., Left=274, Top=177,;
    Alignment=2, Width=40, Height=15,;
    Caption="Oper."  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PDDUZGMNIA",Visible=.t., Left=172, Top=195,;
    Alignment=1, Width=53, Height=18,;
    Caption="2^U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ZQKOFUSNXY",Visible=.t., Left=142, Top=195,;
    Alignment=2, Width=28, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="AYJXEXVWNI",Visible=.t., Left=12, Top=195,;
    Alignment=1, Width=93, Height=18,;
    Caption="Un. di misura.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WKWNGBEXKC",Visible=.t., Left=19, Top=16,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="SQNIFMITJE",Visible=.t., Left=258, Top=226,;
    Alignment=1, Width=18, Height=18,;
    Caption="In:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TWOVTJKYRZ",Visible=.t., Left=20, Top=135,;
    Alignment=1, Width=85, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_ari','TAB_RISO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RICODICE=TAB_RISO.RICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
