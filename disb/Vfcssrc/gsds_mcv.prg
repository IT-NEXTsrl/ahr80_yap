* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_mcv                                                        *
*              Componenti variante                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_76]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-26                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsds_mcv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsds_mcv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsds_mcv")
  return

* --- Class definition
define class tgsds_mcv as StdPCForm
  Width  = 649
  Height = 299
  Top    = 121
  Left   = 3
  cComment = "Componenti variante"
  cPrg = "gsds_mcv"
  HelpContextID=97084521
  add object cnt as tcgsds_mcv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsds_mcv as PCContext
  w_CVCODICE = space(20)
  w_CVNUMRIF = 0
  w_CPROWORD = 0
  w_CVCODART = space(20)
  w_TIPART = space(2)
  w_CVCODCOM = space(20)
  w_CVARTCOM = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_DISCOM = space(20)
  w_UNMIS3 = space(3)
  w_DTOBS1 = space(8)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_TIPRIG = space(1)
  w_CVUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_CVQTADIS = 0
  w_CVCOEIMP = space(15)
  w_CVFLESPL = space(1)
  w_RIGA = 0
  w_DESCON = space(40)
  w_OBTEST = space(8)
  w_DESART = space(40)
  w_DESCOM = space(40)
  w_CVCOEUM1 = 0
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_NOFRAZ = space(1)
  proc Save(i_oFrom)
    this.w_CVCODICE = i_oFrom.w_CVCODICE
    this.w_CVNUMRIF = i_oFrom.w_CVNUMRIF
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CVCODART = i_oFrom.w_CVCODART
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_CVCODCOM = i_oFrom.w_CVCODCOM
    this.w_CVARTCOM = i_oFrom.w_CVARTCOM
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_DISCOM = i_oFrom.w_DISCOM
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_DTOBS1 = i_oFrom.w_DTOBS1
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_TIPRIG = i_oFrom.w_TIPRIG
    this.w_CVUNIMIS = i_oFrom.w_CVUNIMIS
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_CVQTADIS = i_oFrom.w_CVQTADIS
    this.w_CVCOEIMP = i_oFrom.w_CVCOEIMP
    this.w_CVFLESPL = i_oFrom.w_CVFLESPL
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DESART = i_oFrom.w_DESART
    this.w_DESCOM = i_oFrom.w_DESCOM
    this.w_CVCOEUM1 = i_oFrom.w_CVCOEUM1
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_NOFRAZ = i_oFrom.w_NOFRAZ
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CVCODICE = this.w_CVCODICE
    i_oTo.w_CVNUMRIF = this.w_CVNUMRIF
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CVCODART = this.w_CVCODART
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_CVCODCOM = this.w_CVCODCOM
    i_oTo.w_CVARTCOM = this.w_CVARTCOM
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_DISCOM = this.w_DISCOM
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_DTOBS1 = this.w_DTOBS1
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_TIPRIG = this.w_TIPRIG
    i_oTo.w_CVUNIMIS = this.w_CVUNIMIS
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_CVQTADIS = this.w_CVQTADIS
    i_oTo.w_CVCOEIMP = this.w_CVCOEIMP
    i_oTo.w_CVFLESPL = this.w_CVFLESPL
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_DESCOM = this.w_DESCOM
    i_oTo.w_CVCOEUM1 = this.w_CVCOEUM1
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_NOFRAZ = this.w_NOFRAZ
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsds_mcv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 649
  Height = 299
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=97084521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  COM_VARI_IDX = 0
  ART_ICOL_IDX = 0
  KEY_ARTI_IDX = 0
  UNIMIS_IDX = 0
  COE_IMPI_IDX = 0
  MAGAZZIN_IDX = 0
  cFile = "COM_VARI"
  cKeySelect = "CVCODICE,CVNUMRIF"
  cKeyWhere  = "CVCODICE=this.w_CVCODICE and CVNUMRIF=this.w_CVNUMRIF"
  cKeyDetail  = "CVCODICE=this.w_CVCODICE and CVNUMRIF=this.w_CVNUMRIF"
  cKeyWhereODBC = '"CVCODICE="+cp_ToStrODBC(this.w_CVCODICE)';
      +'+" and CVNUMRIF="+cp_ToStrODBC(this.w_CVNUMRIF)';

  cKeyDetailWhereODBC = '"CVCODICE="+cp_ToStrODBC(this.w_CVCODICE)';
      +'+" and CVNUMRIF="+cp_ToStrODBC(this.w_CVNUMRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"COM_VARI.CVCODICE="+cp_ToStrODBC(this.w_CVCODICE)';
      +'+" and COM_VARI.CVNUMRIF="+cp_ToStrODBC(this.w_CVNUMRIF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'COM_VARI.CPROWORD '
  cPrg = "gsds_mcv"
  cComment = "Componenti variante"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CVCODICE = space(20)
  w_CVNUMRIF = 0
  w_CPROWORD = 0
  w_CVCODART = space(20)
  w_TIPART = space(2)
  w_CVCODCOM = space(20)
  o_CVCODCOM = space(20)
  w_CVARTCOM = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_DISCOM = space(20)
  w_UNMIS3 = space(3)
  w_DTOBS1 = ctod('  /  /  ')
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_TIPRIG = space(1)
  w_CVUNIMIS = space(3)
  o_CVUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_CVQTADIS = 0
  o_CVQTADIS = 0
  w_CVCOEIMP = space(15)
  w_CVFLESPL = space(1)
  w_RIGA = 0
  w_DESCON = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DESART = space(40)
  w_DESCOM = space(40)
  w_CVCOEUM1 = 0
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_NOFRAZ = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_mcvPag1","gsds_mcv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='COE_IMPI'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='COM_VARI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COM_VARI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COM_VARI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsds_mcv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from COM_VARI where CVCODICE=KeySet.CVCODICE
    *                            and CVNUMRIF=KeySet.CVNUMRIF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.COM_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2],this.bLoadRecFilter,this.COM_VARI_IDX,"gsds_mcv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COM_VARI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COM_VARI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COM_VARI '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CVCODICE',this.w_CVCODICE  ,'CVNUMRIF',this.w_CVNUMRIF  )
      select * from (i_cTable) COM_VARI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CVCODICE = NVL(CVCODICE,space(20))
        .w_CVNUMRIF = NVL(CVNUMRIF,0)
        .w_RIGA = THIS.oParentObject .w_CPROWORD
        .w_DESCON = THIS.oParentObject .w_DBDESCOM
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'COM_VARI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_TIPART = space(2)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_DISCOM = space(20)
          .w_UNMIS3 = space(3)
          .w_DTOBS1 = ctod("  /  /  ")
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_TIPRIG = space(1)
          .w_FLFRAZ = space(1)
          .w_DESART = space(40)
          .w_DESCOM = space(40)
          .w_FLUSEP = space(1)
          .w_MODUM2 = space(1)
          .w_NOFRAZ = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CVCODART = NVL(CVCODART,space(20))
          if link_2_2_joined
            this.w_CVCODART = NVL(ARCODART202,NVL(this.w_CVCODART,space(20)))
            this.w_TIPART = NVL(ARTIPART202,space(2))
            this.w_DESART = NVL(ARDESART202,space(40))
          else
          .link_2_2('Load')
          endif
          .w_CVCODCOM = NVL(CVCODCOM,space(20))
          if link_2_4_joined
            this.w_CVCODCOM = NVL(CACODICE204,NVL(this.w_CVCODCOM,space(20)))
            this.w_DESCOM = NVL(CADESART204,space(40))
            this.w_CVARTCOM = NVL(CACODART204,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS204,space(3))
            this.w_OPERA3 = NVL(CAOPERAT204,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP204,0)
            this.w_TIPRIG = NVL(CA__TIPO204,space(1))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_CVARTCOM = NVL(CVARTCOM,space(20))
          if link_2_5_joined
            this.w_CVARTCOM = NVL(ARCODART205,NVL(this.w_CVARTCOM,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1205,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2205,space(3))
            this.w_OPERAT = NVL(AROPERAT205,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP205,0)
            this.w_DISCOM = NVL(ARCODDIS205,space(20))
            this.w_FLUSEP = NVL(ARFLUSEP205,space(1))
          else
          .link_2_5('Load')
          endif
          .link_2_6('Load')
          .w_CVUNIMIS = NVL(CVUNIMIS,space(3))
          if link_2_16_joined
            this.w_CVUNIMIS = NVL(UMCODICE216,NVL(this.w_CVUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ216,space(1))
          else
          .link_2_16('Load')
          endif
          .w_CVQTADIS = NVL(CVQTADIS,0)
          .w_CVCOEIMP = NVL(CVCOEIMP,space(15))
          * evitabile
          *.link_2_19('Load')
          .w_CVFLESPL = NVL(CVFLESPL,space(1))
          .w_CVCOEUM1 = NVL(CVCOEUM1,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_RIGA = THIS.oParentObject .w_CPROWORD
        .w_DESCON = THIS.oParentObject .w_DBDESCOM
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CVCODICE=space(20)
      .w_CVNUMRIF=0
      .w_CPROWORD=10
      .w_CVCODART=space(20)
      .w_TIPART=space(2)
      .w_CVCODCOM=space(20)
      .w_CVARTCOM=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DISCOM=space(20)
      .w_UNMIS3=space(3)
      .w_DTOBS1=ctod("  /  /  ")
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_TIPRIG=space(1)
      .w_CVUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_CVQTADIS=0
      .w_CVCOEIMP=space(15)
      .w_CVFLESPL=space(1)
      .w_RIGA=0
      .w_DESCON=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_DESCOM=space(40)
      .w_CVCOEUM1=0
      .w_FLUSEP=space(1)
      .w_MODUM2=space(1)
      .w_NOFRAZ=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_CVCODART))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CVCODCOM))
         .link_2_4('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CVARTCOM))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_6('Full')
        endif
        .DoRTCalc(9,17,.f.)
        .w_CVUNIMIS = .w_UNMIS1
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CVUNIMIS))
         .link_2_16('Full')
        endif
        .DoRTCalc(19,20,.f.)
        .w_CVCOEIMP = SPACE(15)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CVCOEIMP))
         .link_2_19('Full')
        endif
        .w_CVFLESPL = IIF(NOT EMPTY(.w_CVCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
        .w_RIGA = THIS.oParentObject .w_CPROWORD
        .w_DESCON = THIS.oParentObject .w_DBDESCOM
        .w_OBTEST = i_DATSYS
        .DoRTCalc(26,27,.f.)
        .w_CVCOEUM1 = CALQTAADV(.w_CVQTADIS,.w_CVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "CVQTADIS")
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'COM_VARI')
    this.DoRTCalc(29,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'COM_VARI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COM_VARI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVCODICE,"CVCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CVNUMRIF,"CVNUMRIF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CVCODART C(20);
      ,t_CVCODCOM C(20);
      ,t_CVUNIMIS C(3);
      ,t_CVQTADIS N(15,6);
      ,t_CVCOEIMP C(15);
      ,t_CVFLESPL N(3);
      ,t_DESART C(40);
      ,t_DESCOM C(40);
      ,CPROWNUM N(10);
      ,t_TIPART C(2);
      ,t_CVARTCOM C(20);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_DISCOM C(20);
      ,t_UNMIS3 C(3);
      ,t_DTOBS1 D(8);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_TIPRIG C(1);
      ,t_FLFRAZ C(1);
      ,t_CVCOEUM1 N(15,6);
      ,t_FLUSEP C(1);
      ,t_MODUM2 C(1);
      ,t_NOFRAZ C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsds_mcvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODART_2_2.controlsource=this.cTrsName+'.t_CVCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODCOM_2_4.controlsource=this.cTrsName+'.t_CVCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVUNIMIS_2_16.controlsource=this.cTrsName+'.t_CVUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVQTADIS_2_18.controlsource=this.cTrsName+'.t_CVQTADIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVCOEIMP_2_19.controlsource=this.cTrsName+'.t_CVCOEIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.controlsource=this.cTrsName+'.t_CVFLESPL'
    this.oPgFRm.Page1.oPag.oDESART_2_21.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oDESCOM_2_22.controlsource=this.cTrsName+'.t_DESCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(209)
    this.AddVLine(359)
    this.AddVLine(397)
    this.AddVLine(477)
    this.AddVLine(593)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COM_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COM_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2])
      *
      * insert into COM_VARI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COM_VARI')
        i_extval=cp_InsertValODBCExtFlds(this,'COM_VARI')
        i_cFldBody=" "+;
                  "(CVCODICE,CVNUMRIF,CPROWORD,CVCODART,CVCODCOM"+;
                  ",CVARTCOM,CVUNIMIS,CVQTADIS,CVCOEIMP,CVFLESPL"+;
                  ",CVCOEUM1,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CVCODICE)+","+cp_ToStrODBC(this.w_CVNUMRIF)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_CVCODART)+","+cp_ToStrODBCNull(this.w_CVCODCOM)+;
             ","+cp_ToStrODBCNull(this.w_CVARTCOM)+","+cp_ToStrODBCNull(this.w_CVUNIMIS)+","+cp_ToStrODBC(this.w_CVQTADIS)+","+cp_ToStrODBCNull(this.w_CVCOEIMP)+","+cp_ToStrODBC(this.w_CVFLESPL)+;
             ","+cp_ToStrODBC(this.w_CVCOEUM1)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COM_VARI')
        i_extval=cp_InsertValVFPExtFlds(this,'COM_VARI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CVCODICE',this.w_CVCODICE,'CVNUMRIF',this.w_CVNUMRIF)
        INSERT INTO (i_cTable) (;
                   CVCODICE;
                  ,CVNUMRIF;
                  ,CPROWORD;
                  ,CVCODART;
                  ,CVCODCOM;
                  ,CVARTCOM;
                  ,CVUNIMIS;
                  ,CVQTADIS;
                  ,CVCOEIMP;
                  ,CVFLESPL;
                  ,CVCOEUM1;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CVCODICE;
                  ,this.w_CVNUMRIF;
                  ,this.w_CPROWORD;
                  ,this.w_CVCODART;
                  ,this.w_CVCODCOM;
                  ,this.w_CVARTCOM;
                  ,this.w_CVUNIMIS;
                  ,this.w_CVQTADIS;
                  ,this.w_CVCOEIMP;
                  ,this.w_CVFLESPL;
                  ,this.w_CVCOEUM1;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.COM_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CVCODART) AND NOT EMPTY(t_CVCODCOM)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'COM_VARI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'COM_VARI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CVCODART) AND NOT EMPTY(t_CVCODCOM)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update COM_VARI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'COM_VARI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CVCODART="+cp_ToStrODBCNull(this.w_CVCODART)+;
                     ",CVCODCOM="+cp_ToStrODBCNull(this.w_CVCODCOM)+;
                     ",CVARTCOM="+cp_ToStrODBCNull(this.w_CVARTCOM)+;
                     ",CVUNIMIS="+cp_ToStrODBCNull(this.w_CVUNIMIS)+;
                     ",CVQTADIS="+cp_ToStrODBC(this.w_CVQTADIS)+;
                     ",CVCOEIMP="+cp_ToStrODBCNull(this.w_CVCOEIMP)+;
                     ",CVFLESPL="+cp_ToStrODBC(this.w_CVFLESPL)+;
                     ",CVCOEUM1="+cp_ToStrODBC(this.w_CVCOEUM1)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'COM_VARI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CVCODART=this.w_CVCODART;
                     ,CVCODCOM=this.w_CVCODCOM;
                     ,CVARTCOM=this.w_CVARTCOM;
                     ,CVUNIMIS=this.w_CVUNIMIS;
                     ,CVQTADIS=this.w_CVQTADIS;
                     ,CVCOEIMP=this.w_CVCOEIMP;
                     ,CVFLESPL=this.w_CVFLESPL;
                     ,CVCOEUM1=this.w_CVCOEUM1;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COM_VARI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CVCODART) AND NOT EMPTY(t_CVCODCOM)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete COM_VARI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CVCODART) AND NOT EMPTY(t_CVCODCOM)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COM_VARI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_VARI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CVCODCOM<>.w_CVCODCOM
          .link_2_5('Full')
        endif
          .link_2_6('Full')
        .DoRTCalc(9,17,.t.)
        if .o_CVCODCOM<>.w_CVCODCOM
          .w_CVUNIMIS = .w_UNMIS1
          .link_2_16('Full')
        endif
        .DoRTCalc(19,20,.t.)
        if .o_CVCODCOM<>.w_CVCODCOM
          .w_CVCOEIMP = SPACE(15)
          .link_2_19('Full')
        endif
        if .o_CVCODCOM<>.w_CVCODCOM
          .w_CVFLESPL = IIF(NOT EMPTY(.w_CVCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
        endif
          .w_RIGA = THIS.oParentObject .w_CPROWORD
          .w_DESCON = THIS.oParentObject .w_DBDESCOM
          .w_OBTEST = i_DATSYS
        .DoRTCalc(26,27,.t.)
        if .o_CVQTADIS<>.w_CVQTADIS.or. .o_CVUNIMIS<>.w_CVUNIMIS
          .w_CVCOEUM1 = CALQTAADV(.w_CVQTADIS,.w_CVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "CVQTADIS")
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPART with this.w_TIPART
      replace t_CVARTCOM with this.w_CVARTCOM
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_DISCOM with this.w_DISCOM
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_DTOBS1 with this.w_DTOBS1
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_TIPRIG with this.w_TIPRIG
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_CVCOEUM1 with this.w_CVCOEUM1
      replace t_FLUSEP with this.w_FLUSEP
      replace t_MODUM2 with this.w_MODUM2
      replace t_NOFRAZ with this.w_NOFRAZ
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVUNIMIS_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVUNIMIS_2_16.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVQTADIS_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVQTADIS_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVCOEIMP_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVCOEIMP_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVFLESPL_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCVFLESPL_2_20.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CVCODART
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CVCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CVCODART))
          select ARCODART,ARTIPART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCVCODART_2_2'),i_cWhere,'GSMA_AAR',"Elenco articoli",'GSDS_MCV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARTIPART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CVCODART)
            select ARCODART,ARTIPART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CVCODART = space(20)
      endif
      this.w_TIPART = space(2)
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM' AND .w_CVCODART<>.w_CVARTCOM
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente o uguale al componente")
        endif
        this.w_CVCODART = space(20)
        this.w_TIPART = space(2)
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.ARCODART as ARCODART202"+ ",link_2_2.ARTIPART as ARTIPART202"+ ",link_2_2.ARDESART as ARDESART202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on COM_VARI.CVCODART=link_2_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and COM_VARI.CVCODART=link_2_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCODCOM
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CVCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CVCODCOM))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCODCOM)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCODCOM) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCVCODCOM_2_4'),i_cWhere,'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CVCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CVCODCOM)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCODCOM = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOM = NVL(_Link_.CADESART,space(40))
      this.w_CVARTCOM = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CVCODCOM = space(20)
      endif
      this.w_DESCOM = space(40)
      this.w_CVARTCOM = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_TIPRIG = space(1)
      this.w_DTOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG='R' AND (.w_DTOBS1>.w_OBTEST OR EMPTY(.w_DTOBS1)) AND .w_CVCODART<>.w_CVARTCOM
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice componente incongruente o obsoleto o uguale al codice articolo")
        endif
        this.w_CVCODCOM = space(20)
        this.w_DESCOM = space(40)
        this.w_CVARTCOM = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_TIPRIG = space(1)
        this.w_DTOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CACODICE as CACODICE204"+ ",link_2_4.CADESART as CADESART204"+ ",link_2_4.CACODART as CACODART204"+ ",link_2_4.CAUNIMIS as CAUNIMIS204"+ ",link_2_4.CAOPERAT as CAOPERAT204"+ ",link_2_4.CAMOLTIP as CAMOLTIP204"+ ",link_2_4.CA__TIPO as CA__TIPO204"+ ",link_2_4.CADTOBSO as CADTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on COM_VARI.CVCODCOM=link_2_4.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and COM_VARI.CVCODCOM=link_2_4.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVARTCOM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVARTCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVARTCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODDIS,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CVARTCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CVARTCOM)
            select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODDIS,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVARTCOM = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_DISCOM = NVL(_Link_.ARCODDIS,space(20))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CVARTCOM = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_DISCOM = space(20)
      this.w_FLUSEP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVARTCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ARCODART as ARCODART205"+ ",link_2_5.ARUNMIS1 as ARUNMIS1205"+ ",link_2_5.ARUNMIS2 as ARUNMIS2205"+ ",link_2_5.AROPERAT as AROPERAT205"+ ",link_2_5.ARMOLTIP as ARMOLTIP205"+ ",link_2_5.ARCODDIS as ARCODDIS205"+ ",link_2_5.ARFLUSEP as ARFLUSEP205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on COM_VARI.CVARTCOM=link_2_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and COM_VARI.CVARTCOM=link_2_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CVUNIMIS
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CVUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CVUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCVUNIMIS_2_16'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSDS_MDB.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CVUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CVUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CVUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_CVUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_CVUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.UMCODICE as UMCODICE216"+ ",link_2_16.UMFLFRAZ as UMFLFRAZ216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on COM_VARI.CVUNIMIS=link_2_16.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and COM_VARI.CVUNIMIS=link_2_16.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CVCOEIMP
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COE_IMPI_IDX,3]
    i_lTable = "COE_IMPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2], .t., this.COE_IMPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CVCOEIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ACI',True,'COE_IMPI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CICODICE like "+cp_ToStrODBC(trim(this.w_CVCOEIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CICODICE',trim(this.w_CVCOEIMP))
          select CICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CVCOEIMP)==trim(_Link_.CICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CVCOEIMP) and !this.bDontReportError
            deferred_cp_zoom('COE_IMPI','*','CICODICE',cp_AbsName(oSource.parent,'oCVCOEIMP_2_19'),i_cWhere,'GSDS_ACI',"Coefficienti di impiego",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODICE',oSource.xKey(1))
            select CICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CVCOEIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CICODICE="+cp_ToStrODBC(this.w_CVCOEIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODICE',this.w_CVCOEIMP)
            select CICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CVCOEIMP = NVL(_Link_.CICODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CVCOEIMP = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2])+'\'+cp_ToStr(_Link_.CICODICE,1)
      cp_ShowWarn(i_cKey,this.COE_IMPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CVCOEIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRIGA_1_3.value==this.w_RIGA)
      this.oPgFrm.Page1.oPag.oRIGA_1_3.value=this.w_RIGA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_5.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_5.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_2_21.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_2_21.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oDESART_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_2_22.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_2_22.value=this.w_DESCOM
      replace t_DESCOM with this.oPgFrm.Page1.oPag.oDESCOM_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODART_2_2.value==this.w_CVCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODART_2_2.value=this.w_CVCODART
      replace t_CVCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODART_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODCOM_2_4.value==this.w_CVCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODCOM_2_4.value=this.w_CVCODCOM
      replace t_CVCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODCOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVUNIMIS_2_16.value==this.w_CVUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVUNIMIS_2_16.value=this.w_CVUNIMIS
      replace t_CVUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVUNIMIS_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVQTADIS_2_18.value==this.w_CVQTADIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVQTADIS_2_18.value=this.w_CVQTADIS
      replace t_CVQTADIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVQTADIS_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCOEIMP_2_19.value==this.w_CVCOEIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCOEIMP_2_19.value=this.w_CVCOEIMP
      replace t_CVCOEIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCOEIMP_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.RadioValue()==this.w_CVFLESPL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.SetRadio()
      replace t_CVFLESPL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.value
    endif
    cp_SetControlsValueExtFlds(this,'COM_VARI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM' AND .w_CVCODART<>.w_CVARTCOM) and not(empty(.w_CVCODART)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CVCODART) AND NOT EMPTY(.w_CVCODCOM))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODART_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente o uguale al componente")
        case   not(.w_TIPRIG='R' AND (.w_DTOBS1>.w_OBTEST OR EMPTY(.w_DTOBS1)) AND .w_CVCODART<>.w_CVARTCOM) and not(empty(.w_CVCODCOM)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CVCODART) AND NOT EMPTY(.w_CVCODCOM))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVCODCOM_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice componente incongruente o obsoleto o uguale al codice articolo")
        case   (empty(.w_CVUNIMIS) or not(CHKUNIMI(.w_CVUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (NOT EMPTY(.w_CVCODCOM)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CVCODART) AND NOT EMPTY(.w_CVCODCOM))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVUNIMIS_2_16
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_CVQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_CVQTADIS=INT(.w_CVQTADIS))) and (NOT EMPTY(.w_CVCODCOM)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CVCODART) AND NOT EMPTY(.w_CVCODCOM))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVQTADIS_2_18
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
      endcase
      if not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CVCODART) AND NOT EMPTY(.w_CVCODCOM)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CVCODCOM = this.w_CVCODCOM
    this.o_CVUNIMIS = this.w_CVUNIMIS
    this.o_CVQTADIS = this.w_CVQTADIS
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CVCODART) AND NOT EMPTY(t_CVCODCOM))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CVCODART=space(20)
      .w_TIPART=space(2)
      .w_CVCODCOM=space(20)
      .w_CVARTCOM=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DISCOM=space(20)
      .w_UNMIS3=space(3)
      .w_DTOBS1=ctod("  /  /  ")
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_TIPRIG=space(1)
      .w_CVUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_CVQTADIS=0
      .w_CVCOEIMP=space(15)
      .w_CVFLESPL=space(1)
      .w_DESART=space(40)
      .w_DESCOM=space(40)
      .w_CVCOEUM1=0
      .w_FLUSEP=space(1)
      .w_MODUM2=space(1)
      .w_NOFRAZ=space(1)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_CVCODART))
        .link_2_2('Full')
      endif
      .DoRTCalc(5,6,.f.)
      if not(empty(.w_CVCODCOM))
        .link_2_4('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CVARTCOM))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_6('Full')
      endif
      .DoRTCalc(9,17,.f.)
        .w_CVUNIMIS = .w_UNMIS1
      .DoRTCalc(18,18,.f.)
      if not(empty(.w_CVUNIMIS))
        .link_2_16('Full')
      endif
      .DoRTCalc(19,20,.f.)
        .w_CVCOEIMP = SPACE(15)
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_CVCOEIMP))
        .link_2_19('Full')
      endif
        .w_CVFLESPL = IIF(NOT EMPTY(.w_CVCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
      .DoRTCalc(23,27,.f.)
        .w_CVCOEUM1 = CALQTAADV(.w_CVQTADIS,.w_CVUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "CVQTADIS")
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
    endwith
    this.DoRTCalc(29,31,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CVCODART = t_CVCODART
    this.w_TIPART = t_TIPART
    this.w_CVCODCOM = t_CVCODCOM
    this.w_CVARTCOM = t_CVARTCOM
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_DISCOM = t_DISCOM
    this.w_UNMIS3 = t_UNMIS3
    this.w_DTOBS1 = t_DTOBS1
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_TIPRIG = t_TIPRIG
    this.w_CVUNIMIS = t_CVUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_CVQTADIS = t_CVQTADIS
    this.w_CVCOEIMP = t_CVCOEIMP
    this.w_CVFLESPL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.RadioValue(.t.)
    this.w_DESART = t_DESART
    this.w_DESCOM = t_DESCOM
    this.w_CVCOEUM1 = t_CVCOEUM1
    this.w_FLUSEP = t_FLUSEP
    this.w_MODUM2 = t_MODUM2
    this.w_NOFRAZ = t_NOFRAZ
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CVCODART with this.w_CVCODART
    replace t_TIPART with this.w_TIPART
    replace t_CVCODCOM with this.w_CVCODCOM
    replace t_CVARTCOM with this.w_CVARTCOM
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_DISCOM with this.w_DISCOM
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_TIPRIG with this.w_TIPRIG
    replace t_CVUNIMIS with this.w_CVUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_CVQTADIS with this.w_CVQTADIS
    replace t_CVCOEIMP with this.w_CVCOEIMP
    replace t_CVFLESPL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCVFLESPL_2_20.ToRadio()
    replace t_DESART with this.w_DESART
    replace t_DESCOM with this.w_DESCOM
    replace t_CVCOEUM1 with this.w_CVCOEUM1
    replace t_FLUSEP with this.w_FLUSEP
    replace t_MODUM2 with this.w_MODUM2
    replace t_NOFRAZ with this.w_NOFRAZ
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsds_mcvPag1 as StdContainer
  Width  = 645
  height = 299
  stdWidth  = 645
  stdheight = 299
  resizeXpos=293
  resizeYpos=176
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRIGA_1_3 as StdField with uid="CXNGIAYLUZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_RIGA", cQueryName = "RIGA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92513770,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=86, Top=9, cSayPict='"99999"', cGetPict='"99999"'

  add object oDESCON_1_5 as StdField with uid="RSNXRYQMOK",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225383734,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=226, Top=9, InputMask=replicate('X',40)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=40, width=623,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Posiz.",Field2="CVCODART",Label2="Articolo",Field3="CVCODCOM",Label3="Componente",Field4="CVUNIMIS",Label4="U.M.",Field5="CVQTADIS",Label5="Quantit�",Field6="CVCOEIMP",Label6="Coeff. impiego",Field7="CVFLESPL",Label7="Espl.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219006330

  add object oStr_1_4 as StdString with uid="MZLGPHTGNQ",Visible=.t., Left=144, Top=9,;
    Alignment=1, Width=80, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="AHSSFZWQRI",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=80, Height=18,;
    Caption="Riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KHKJIGSGHH",Visible=.t., Left=9, Top=268,;
    Alignment=1, Width=206, Height=18,;
    Caption="Descrizione componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="YFDIJQMSSV",Visible=.t., Left=9, Top=239,;
    Alignment=1, Width=206, Height=18,;
    Caption="Descrizione articolo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=59,;
    width=619+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=60,width=618+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ART_ICOL|KEY_ARTI|UNIMIS|COE_IMPI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESART_2_21.Refresh()
      this.Parent.oDESCOM_2_22.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oCVCODART_2_2
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oCVCODCOM_2_4
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oCVUNIMIS_2_16
      case cFile='COE_IMPI'
        oDropInto=this.oBodyCol.oRow.oCVCOEIMP_2_19
    endcase
    return(oDropInto)
  EndFunc


  add object oDESART_2_21 as StdTrsField with uid="PYWIVYRTKD",rtseq=26,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione componente",;
    HelpContextID = 60626230,;
    cTotal="", bFixedPos=.t., cQueryName = "DESART",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=239, InputMask=replicate('X',40)

  add object oDESCOM_2_22 as StdTrsField with uid="AXSTYGWFIQ",rtseq=27,rtrep=.t.,;
    cFormVar="w_DESCOM",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione componente",;
    HelpContextID = 208606518,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=268, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsds_mcvBodyRow as CPBodyRowCnt
  Width=609
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="DRSSATDJPS",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251334762,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCVCODART_2_2 as StdTrsField with uid="XEZTDVIPKD",rtseq=4,rtrep=.t.,;
    cFormVar="w_CVCODART",value=space(20),;
    ToolTipText = "Codice articolo variante",;
    HelpContextID = 264906362,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente o uguale al componente",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=50, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_CVCODART"

  func oCVCODART_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCODART_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCVCODART_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCVCODART_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Elenco articoli",'GSDS_MCV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCVCODART_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CVCODART
    i_obj.ecpSave()
  endproc

  add object oCVCODCOM_2_4 as StdTrsField with uid="RRFIWMCJTD",rtseq=6,rtrep=.t.,;
    cFormVar="w_CVCODCOM",value=space(20),;
    ToolTipText = "Codice componente legato all'articolo variante",;
    HelpContextID = 30025331,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice componente incongruente o obsoleto o uguale al codice articolo",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=201, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CVCODCOM"

  func oCVCODCOM_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCODCOM_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCVCODCOM_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCVCODCOM_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCVCODCOM_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CVCODCOM
    i_obj.ecpSave()
  endproc

  add object oCVUNIMIS_2_16 as StdTrsField with uid="UMGKKCMIZB",rtseq=18,rtrep=.t.,;
    cFormVar="w_CVUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura del componente",;
    HelpContextID = 65386887,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=350, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_CVUNIMIS"

  func oCVUNIMIS_2_16.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CVCODCOM))
    endwith
  endfunc

  func oCVUNIMIS_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVUNIMIS_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCVUNIMIS_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCVUNIMIS_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSDS_MDB.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCVUNIMIS_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_CVUNIMIS
    i_obj.ecpSave()
  endproc

  add object oCVQTADIS_2_18 as StdTrsField with uid="JVXUEUIRRI",rtseq=20,rtrep=.t.,;
    cFormVar="w_CVQTADIS",value=0,;
    ToolTipText = "Coefficiente di impiego",;
    HelpContextID = 224393607,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=388, Top=0, cSayPict=[v_PQ(32)], cGetPict=[v_GQ(32)]

  func oCVQTADIS_2_18.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CVCODCOM))
    endwith
  endfunc

  func oCVQTADIS_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CVQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_CVQTADIS=INT(.w_CVQTADIS)))
    endwith
    return bRes
  endfunc

  add object oCVCOEIMP_2_19 as StdTrsField with uid="TQILEWKWXZ",rtseq=21,rtrep=.t.,;
    cFormVar="w_CVCOEIMP",value=space(15),;
    ToolTipText = "Eventuale codice formula coefficiente di impiego associata al componente",;
    HelpContextID = 136698250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=469, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COE_IMPI", cZoomOnZoom="GSDS_ACI", oKey_1_1="CICODICE", oKey_1_2="this.w_CVCOEIMP"

  func oCVCOEIMP_2_19.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CVCODCOM))
    endwith
  endfunc

  func oCVCOEIMP_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCVCOEIMP_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCVCOEIMP_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COE_IMPI','*','CICODICE',cp_AbsName(this.parent,'oCVCOEIMP_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ACI',"Coefficienti di impiego",'',this.parent.oContained
  endproc
  proc oCVCOEIMP_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ACI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CICODICE=this.parent.oContained.w_CVCOEIMP
    i_obj.ecpSave()
  endproc

  add object oCVFLESPL_2_20 as StdTrsCheck with uid="BAZMFWVVAF",rtrep=.t.,;
    cFormVar="w_CVFLESPL",  caption="",;
    ToolTipText = "Se attivo esplode componenti della distinta associata all'articolo componente",;
    HelpContextID = 30889586,;
    Left=584, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCVFLESPL_2_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CVFLESPL,&i_cF..t_CVFLESPL),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCVFLESPL_2_20.GetRadio()
    this.Parent.oContained.w_CVFLESPL = this.RadioValue()
    return .t.
  endfunc

  func oCVFLESPL_2_20.ToRadio()
    this.Parent.oContained.w_CVFLESPL=trim(this.Parent.oContained.w_CVFLESPL)
    return(;
      iif(this.Parent.oContained.w_CVFLESPL=='S',1,;
      0))
  endfunc

  func oCVFLESPL_2_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCVFLESPL_2_20.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CVCODCOM) AND NOT EMPTY(.w_DISCOM))
    endwith
  endfunc

  add object oObj_2_24 as cp_runprogram with uid="FQLGKMUUHI",width=288,height=26,;
   left=279, top=247,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSDS_BVP('V')",;
    cEvent = "w_CVCODCOM Changed",;
    nPag=2;
    , HelpContextID = 80913126
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_mcv','COM_VARI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CVCODICE=COM_VARI.CVCODICE";
  +" and "+i_cAliasName2+".CVNUMRIF=COM_VARI.CVNUMRIF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
