* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bds                                                        *
*              Duplicazione cicli semplificati                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_52]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-27                                                      *
* Last revis.: 2000-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bds",oParentObject)
return(i_retval)

define class tgsds_bds as StdBatch
  * --- Local variables
  w_MESS = space(0)
  w_APPO = space(15)
  w_SOVRAS = space(1)
  w_NUMRIG = 0
  * --- WorkFile variables
  TABMCICL_idx=0
  TAB_CICL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplicazione Cicli Semplificati (da GSDS_KDC)
    if EMPTY(this.oParentObject.w_CODRIF)
      ah_ErrorMsg("Ciclo di origine non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_CSCODICE)
      ah_ErrorMsg("Ciclo da generare non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_CODRIF=this.oParentObject.w_CSCODICE
      ah_ErrorMsg("Cicli di origine e destinazione coincidenti; impossibile generare",,"")
      i_retcode = 'stop'
      return
    endif
    this.w_APPO = SPACE(15)
    this.w_SOVRAS = "N"
    * --- Read from TABMCICL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TABMCICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2],.t.,this.TABMCICL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CSCODICE"+;
        " from "+i_cTable+" TABMCICL where ";
            +"CSCODICE = "+cp_ToStrODBC(this.oParentObject.w_CSCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CSCODICE;
        from (i_cTable) where;
            CSCODICE = this.oParentObject.w_CSCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO = NVL(cp_ToDate(_read_.CSCODICE),cp_NullValue(_read_.CSCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_APPO=this.oParentObject.w_CSCODICE
      if ah_YesNo("Ciclo da generare gi� esistente; sovrascrivo?")
        this.w_SOVRAS = "S"
      else
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Inizia a Procedere
    ah_Msg("Inizio duplicazione cicli: %1",.T.,.F.,.F., ALLTRIM(this.oParentObject.w_CSCODICE) )
    * --- Try
    local bErr_038743C0
    bErr_038743C0=bTrsErr
    this.Try_038743C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if NOT EMPTY(this.w_MESS)
        ah_ErrorMsg("%1%0Operazione abbandonata",,"", this.w_MESS)
      else
        ah_ErrorMsg("Errore durante l'elaborazione%0Operazione abbandonata",,"")
      endif
    endif
    bTrsErr=bTrsErr or bErr_038743C0
    * --- End
  endproc
  proc Try_038743C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_SOVRAS="S"
      * --- Elimina i Dati del Ciclo di Origine
      * --- Try
      local bErr_037A9728
      bErr_037A9728=bTrsErr
      this.Try_037A9728()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile eliminare il ciclo da sovrascrivere (tabella: dettaglio)")
      endif
      bTrsErr=bTrsErr or bErr_037A9728
      * --- End
      * --- Try
      local bErr_037A8228
      bErr_037A8228=bTrsErr
      this.Try_037A8228()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile modificare il ciclo da sovrascrivere (tabella: testata)")
      endif
      bTrsErr=bTrsErr or bErr_037A8228
      * --- End
    else
      * --- Try
      local bErr_037AA868
      bErr_037AA868=bTrsErr
      this.Try_037AA868()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile inserire il ciclo da sovrascrivere (tabella: testata)")
      endif
      bTrsErr=bTrsErr or bErr_037AA868
      * --- End
    endif
    * --- Select from TAB_CICL
    i_nConn=i_TableProp[this.TAB_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2],.t.,this.TAB_CICL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_CICL ";
          +" where CSCODICE="+cp_ToStrODBC(this.oParentObject.w_CODRIF)+"";
          +" order by CPROWORD";
           ,"_Curs_TAB_CICL")
    else
      select * from (i_cTable);
       where CSCODICE=this.oParentObject.w_CODRIF;
       order by CPROWORD;
        into cursor _Curs_TAB_CICL
    endif
    if used('_Curs_TAB_CICL')
      select _Curs_TAB_CICL
      locate for 1=1
      do while not(eof())
      * --- Try
      local bErr_037A8EE8
      bErr_037A8EE8=bTrsErr
      this.Try_037A8EE8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile inserire il ciclo da sovrascrivere (tabella: dettaglio)")
      endif
      bTrsErr=bTrsErr or bErr_037A8EE8
      * --- End
        select _Curs_TAB_CICL
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Duplicazione cicli completata",,"")
    return
  proc Try_037A9728()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from TAB_CICL
    i_nConn=i_TableProp[this.TAB_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CSCODICE = "+cp_ToStrODBC(this.oParentObject.w_CSCODICE);
             )
    else
      delete from (i_cTable) where;
            CSCODICE = this.oParentObject.w_CSCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_037A8228()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TABMCICL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TABMCICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TABMCICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CSDESCRI),'TABMCICL','CSDESCRI');
          +i_ccchkf ;
      +" where ";
          +"CSCODICE = "+cp_ToStrODBC(this.oParentObject.w_CSCODICE);
             )
    else
      update (i_cTable) set;
          CSDESCRI = this.oParentObject.w_CSDESCRI;
          &i_ccchkf. ;
       where;
          CSCODICE = this.oParentObject.w_CSCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_037AA868()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TABMCICL
    i_nConn=i_TableProp[this.TABMCICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TABMCICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODICE"+",CSDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CSCODICE),'TABMCICL','CSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CSDESCRI),'TABMCICL','CSDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODICE',this.oParentObject.w_CSCODICE,'CSDESCRI',this.oParentObject.w_CSDESCRI)
      insert into (i_cTable) (CSCODICE,CSDESCRI &i_ccchkf. );
         values (;
           this.oParentObject.w_CSCODICE;
           ,this.oParentObject.w_CSDESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_037A8EE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TAB_CICL
    i_nConn=i_TableProp[this.TAB_CICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_CICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODICE"+",CPROWNUM"+",CPROWORD"+",CSCODRIS"+",CSUNIMIS"+",CSQTAMOV"+",CSDESFAS"+",CS_SETUP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CSCODICE),'TAB_CICL','CSCODICE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CPROWNUM),'TAB_CICL','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CPROWORD),'TAB_CICL','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CSCODRIS),'TAB_CICL','CSCODRIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CSUNIMIS),'TAB_CICL','CSUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CSQTAMOV),'TAB_CICL','CSQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CSDESFAS),'TAB_CICL','CSDESFAS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_TAB_CICL.CS_SETUP),'TAB_CICL','CS_SETUP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODICE',this.oParentObject.w_CSCODICE,'CPROWNUM',_Curs_TAB_CICL.CPROWNUM,'CPROWORD',_Curs_TAB_CICL.CPROWORD,'CSCODRIS',_Curs_TAB_CICL.CSCODRIS,'CSUNIMIS',_Curs_TAB_CICL.CSUNIMIS,'CSQTAMOV',_Curs_TAB_CICL.CSQTAMOV,'CSDESFAS',_Curs_TAB_CICL.CSDESFAS,'CS_SETUP',_Curs_TAB_CICL.CS_SETUP)
      insert into (i_cTable) (CSCODICE,CPROWNUM,CPROWORD,CSCODRIS,CSUNIMIS,CSQTAMOV,CSDESFAS,CS_SETUP &i_ccchkf. );
         values (;
           this.oParentObject.w_CSCODICE;
           ,_Curs_TAB_CICL.CPROWNUM;
           ,_Curs_TAB_CICL.CPROWORD;
           ,_Curs_TAB_CICL.CSCODRIS;
           ,_Curs_TAB_CICL.CSUNIMIS;
           ,_Curs_TAB_CICL.CSQTAMOV;
           ,_Curs_TAB_CICL.CSDESFAS;
           ,_Curs_TAB_CICL.CS_SETUP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TABMCICL'
    this.cWorkTables[2]='TAB_CICL'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TAB_CICL')
      use in _Curs_TAB_CICL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
