* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bib                                                        *
*              Calcolo formule coefficienti di impiego                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_37]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bib",oParentObject,m.pOper)
return(i_retval)

define class tgsds_bib as StdBatch
  * --- Local variables
  pOper = space(5)
  w_OCCUR = 0
  w_LUNGVAR = 0
  w_INDOCC = 0
  w_PVCODICE = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato per il calcolo delle Formule associate ai Coefficienti di Impiego (da GSDS_ACI)
    do case
      case INLIST(alltrim(this.pOper),"0","1","2","3","4","5","6","7","8","9")
        this.oParentObject.w_CIFORMUL = alltrim(this.oParentObject.w_CIFORMUL)+alltrim(this.pOper)
      case INLIST(alltrim(this.pOper),"+","-","*","/","(",".",")")
        this.oParentObject.w_CIFORMUL = alltrim(this.oParentObject.w_CIFORMUL)+alltrim(this.pOper)
      case alltrim(this.pOper)="CE"
        this.w_OCCUR = 0
        DIMENSION OCCURV(6)
        OCCURV(1) = RATC("+",this.oParentObject.w_CIFORMUL)
        OCCURV(2) = RATC("-",this.oParentObject.w_CIFORMUL)
        OCCURV(3) = RATC("*",this.oParentObject.w_CIFORMUL)
        OCCURV(4) = RATC("/",this.oParentObject.w_CIFORMUL)
        OCCURV(5) = RATC("(",this.oParentObject.w_CIFORMUL)
        OCCURV(6) = RATC(")",this.oParentObject.w_CIFORMUL)
        this.w_LUNGVAR = LEN(ALLTRIM(this.oParentObject.w_CIFORMUL))
        FOR this.w_INDOCC=1 TO 6
        this.w_OCCUR = IIF(this.w_OCCUR < OCCURV(this.w_INDOCC), IIF(this.w_LUNGVAR=OCCURV(this.w_INDOCC),OCCURV(this.w_INDOCC)-1,OCCURV(this.w_INDOCC)), this.w_OCCUR)
        ENDFOR
        this.oParentObject.w_CIFORMUL = SUBSTR(ALLTRIM(this.oParentObject.w_CIFORMUL),1,this.w_OCCUR)
      case alltrim(this.pOper)="CZ"
        this.oParentObject.w_CIFORMUL = ""
      case alltrim(this.pOper)="VARIABILI"
        this.w_PVCODICE = SPACE(5)
        vx_exec("..\DISB\EXE\QUERY\GSDS_BIB.VZM",this)
        if NOT EMPTY(NVL(this.w_PVCODICE, " "))
          this.oParentObject.w_CIFORMUL = alltrim(this.oParentObject.w_CIFORMUL) + "<" + alltrim(this.w_PVCODICE) + ">"
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
