* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_sdc                                                        *
*              Stampa distinta base costificata                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_164]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-03                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_sdc",oParentObject))

* --- Class definition
define class tgsds_sdc as StdForm
  Top    = 9
  Left   = 17

  * --- Standard Properties
  Width  = 860
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=74015849
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=81

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  ESERCIZI_IDX = 0
  INVENTAR_IDX = 0
  LISTINI_IDX = 0
  ART_ICOL_IDX = 0
  VALUTE_IDX = 0
  PAR_DISB_IDX = 0
  MAGAZZIN_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  cPrg = "gsds_sdc"
  cComment = "Stampa distinta base costificata"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_CODAZI = space(5)
  w_VALESE = space(3)
  w_AZIENDA = space(5)
  w_PDCOSPAR = space(1)
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_DATSTA = ctod('  /  /  ')
  o_DATSTA = ctod('  /  /  ')
  w_QUANTI = 0
  w_FLPROV = space(1)
  w_FLCOST = space(1)
  o_FLCOST = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOLN = space(1)
  w_CAOVAL = 0
  w_DIS1 = space(20)
  w_DIS2 = space(20)
  w_VALESE = space(3)
  w_CAOESE = 0
  w_Analisi = space(1)
  w_PERRIC = space(1)
  w_LOTTOMED = .F.
  w_SCDRSR = space(1)
  w_TIPCOS = space(1)
  w_PDCOECOS = space(1)
  w_TipoValo = space(1)
  o_TipoValo = space(1)
  w_ESERC = space(4)
  o_ESERC = space(4)
  w_NUMINV = space(6)
  o_NUMINV = space(6)
  w_Listino = space(5)
  o_Listino = space(5)
  w_DESLIS = space(40)
  w_PRDATINV = ctod('  /  /  ')
  w_VALLIS = space(3)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INCODMAG = space(5)
  w_INMAGCOL = space(1)
  w_MAGRAG = space(5)
  o_MAGRAG = space(5)
  w_INCATOMO = space(5)
  w_INDATINV = ctod('  /  /  ')
  w_INCODESE = space(5)
  w_INNUMPRE = space(6)
  w_INESEPRE = space(5)
  w_FLDESART = space(1)
  w_CAOLIS = 0
  w_ONUME = 0
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_ESFINESE = ctod('  /  /  ')
  w_FLCOSINV = space(1)
  w_DIS1OB = ctod('  /  /  ')
  w_DIS2OB = ctod('  /  /  ')
  w_DECTOT = 0
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_SELEZI = space(1)
  w_ORIQUERY = space(10)
  w_OUNOMQUE = space(254)
  o_OUNOMQUE = space(254)
  w_DISPRO = space(1)
  w_FLCHKLP = space(1)
  w_COSTILOG = space(1)
  w_TIPOARTI = space(1)
  w_INESCONT = space(1)
  w_AGGMPOND = space(1)
  w_FDBNOLEG = space(1)
  w_TIPVALCIC = space(2)
  w_TipoValoOut = space(1)
  w_SMATOU = space(1)
  w_FLDETTMAG = space(1)
  w_PDCOECOP = space(1)
  w_SelDis = .NULL.
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_sdcPag1","gsds_sdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsds_sdcPag2","gsds_sdc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsds_sdc
    This.Pages(2).Enabled=.f.
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SelDis = this.oPgFrm.Pages(2).oPag.SelDis
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    DoDefault()
    proc Destroy()
      this.w_SelDis = .NULL.
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='INVENTAR'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='PAR_DISB'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='CATEGOMO'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSDS1BDC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_CODAZI=space(5)
      .w_VALESE=space(3)
      .w_AZIENDA=space(5)
      .w_PDCOSPAR=space(1)
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_DATSTA=ctod("  /  /  ")
      .w_QUANTI=0
      .w_FLPROV=space(1)
      .w_FLCOST=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOLN=space(1)
      .w_CAOVAL=0
      .w_DIS1=space(20)
      .w_DIS2=space(20)
      .w_VALESE=space(3)
      .w_CAOESE=0
      .w_Analisi=space(1)
      .w_PERRIC=space(1)
      .w_LOTTOMED=.f.
      .w_SCDRSR=space(1)
      .w_TIPCOS=space(1)
      .w_PDCOECOS=space(1)
      .w_TipoValo=space(1)
      .w_ESERC=space(4)
      .w_NUMINV=space(6)
      .w_Listino=space(5)
      .w_DESLIS=space(40)
      .w_PRDATINV=ctod("  /  /  ")
      .w_VALLIS=space(3)
      .w_INGRUMER=space(5)
      .w_INCODFAM=space(5)
      .w_INCODMAG=space(5)
      .w_INMAGCOL=space(1)
      .w_MAGRAG=space(5)
      .w_INCATOMO=space(5)
      .w_INDATINV=ctod("  /  /  ")
      .w_INCODESE=space(5)
      .w_INNUMPRE=space(6)
      .w_INESEPRE=space(5)
      .w_FLDESART=space(1)
      .w_CAOLIS=0
      .w_ONUME=0
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_ESFINESE=ctod("  /  /  ")
      .w_FLCOSINV=space(1)
      .w_DIS1OB=ctod("  /  /  ")
      .w_DIS2OB=ctod("  /  /  ")
      .w_DECTOT=0
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_SELEZI=space(1)
      .w_ORIQUERY=space(10)
      .w_OUNOMQUE=space(254)
      .w_DISPRO=space(1)
      .w_FLCHKLP=space(1)
      .w_COSTILOG=space(1)
      .w_TIPOARTI=space(1)
      .w_AGGMPOND=space(1)
      .w_FDBNOLEG=space(1)
      .w_TIPVALCIC=space(2)
      .w_TipoValoOut=space(1)
      .w_SMATOU=space(1)
      .w_FLDETTMAG=space(1)
      .w_PDCOECOP=space(1)
        .w_PARAM = iif(TYPE('this.oParentObject')="O", "G", this.oParentObject)
        .w_CODAZI = i_CODAZI
          .DoRTCalc(3,3,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CODINI))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODFIN))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_DATSTA = i_datsys
        .w_QUANTI = 1
        .w_FLPROV = ' '
        .w_FLCOST = 'N'
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .w_OBTEST = i_INIDAT
        .DoRTCalc(15,18,.f.)
        if not(empty(.w_DIS1))
          .link_1_28('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_DIS2))
          .link_1_29('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_VALESE))
          .link_1_30('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_Analisi = "S"
        .w_PERRIC = "S"
        .w_LOTTOMED = True
        .w_SCDRSR = "S"
        .w_TIPCOS = .w_PDCOSPAR
        .w_PDCOECOS = .w_PDCOECOP
        .w_TipoValo = "S"
        .w_ESERC = g_CODESE
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_ESERC))
          .link_1_41('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_NUMINV))
          .link_1_42('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_Listino))
          .link_1_44('Full')
        endif
        .DoRTCalc(32,34,.f.)
        if not(empty(.w_VALLIS))
          .link_1_49('Full')
        endif
        .DoRTCalc(35,37,.f.)
        if not(empty(.w_INCODMAG))
          .link_1_52('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_MAGRAG = IIF (EMPTY(.w_MAGRAG),.w_INCODMAG,.w_MAGRAG)
          .DoRTCalc(40,40,.f.)
        .w_INDATINV = .w_DATSTA
        .w_INCODESE = .w_ESERC
        .w_INNUMPRE = .w_NUMINV
        .w_INESEPRE = .w_ESERC
        .w_FLDESART = 'S'
        .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
          .DoRTCalc(47,47,.f.)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_CODMAG))
          .link_1_64('Full')
        endif
          .DoRTCalc(49,50,.f.)
        .w_FLCOSINV = IIF(.w_TipoValo $ 'SMUP' , "S", "N")
        .DoRTCalc(52,55,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_72('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_73('Full')
        endif
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_GRUINI))
          .link_1_74('Full')
        endif
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_75('Full')
        endif
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_CATINI))
          .link_1_76('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CATFIN))
          .link_1_77('Full')
        endif
      .oPgFrm.Page2.oPag.SelDis.Calculate()
          .DoRTCalc(61,66,.f.)
        .w_SELEZI = 'S'
        .w_ORIQUERY = alltrim(.w_SelDis.cCpQueryName)
      .oPgFrm.Page1.oPag.BTNQRY.Calculate()
          .DoRTCalc(69,69,.f.)
        .w_DISPRO = "N"
        .w_FLCHKLP = "N"
        .w_COSTILOG = "N"
        .w_TIPOARTI = ''
        .w_AGGMPOND = .w_TipoValo
        .w_FDBNOLEG = 'N'
        .w_TIPVALCIC = "00"
        .w_TipoValoOut = "NN"
        .w_SMATOU = "N"
        .w_FLDETTMAG = IIF(.w_TipoValo $ 'SMUP' and .w_FLCOST $ 'E-S' and !Empty(.w_MAGRAG) , "S", "N")
    endwith
    this.DoRTCalc(80,80,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_8('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .DoRTCalc(9,17,.t.)
          .link_1_28('Full')
          .link_1_29('Full')
          .link_1_30('Full')
        .DoRTCalc(21,28,.t.)
        if .o_TipoValo<>.w_TipoValo
          .link_1_41('Full')
        endif
        if .o_ESERC<>.w_ESERC
          .link_1_42('Full')
        endif
        .DoRTCalc(31,33,.t.)
          .link_1_49('Full')
        .DoRTCalc(35,36,.t.)
          .link_1_52('Full')
        .DoRTCalc(38,38,.t.)
            .w_MAGRAG = IIF (EMPTY(.w_MAGRAG),.w_INCODMAG,.w_MAGRAG)
        .DoRTCalc(40,40,.t.)
            .w_INDATINV = .w_DATSTA
            .w_INCODESE = .w_ESERC
        if .o_NUMINV<>.w_NUMINV
            .w_INNUMPRE = .w_NUMINV
        endif
            .w_INESEPRE = .w_ESERC
        .DoRTCalc(45,45,.t.)
        if .o_DATSTA<>.w_DATSTA.or. .o_Listino<>.w_Listino.or. .o_TipoValo<>.w_TipoValo
            .w_CAOLIS = IIF(.w_TipoValo='L', IIF(.w_CAOVAL=0, GETCAM(.w_VALLIS, .w_DATSTA, 7), .w_CAOVAL), .w_CAOLIS)
        endif
        .DoRTCalc(47,50,.t.)
        if .o_TipoValo<>.w_TipoValo
            .w_FLCOSINV = IIF(.w_TipoValo $ 'SMUP' , "S", "N")
        endif
        if .o_OUNOMQUE<>.w_OUNOMQUE
          .Calculate_TWKEFLENMI()
        endif
        .oPgFrm.Page2.oPag.SelDis.Calculate()
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .DoRTCalc(52,73,.t.)
        if .o_NUMINV<>.w_NUMINV
            .w_INESCONT = iif(empty(.w_NUMINV),'N', iif(empty(.w_INESCONT), 'N', .w_INESCONT))
        endif
            .w_AGGMPOND = .w_TipoValo
        .DoRTCalc(76,79,.t.)
        if .o_TipoValo<>.w_TipoValo.or. .o_MAGRAG<>.w_MAGRAG.or. .o_FLCOST<>.w_FLCOST
            .w_FLDETTMAG = IIF(.w_TipoValo $ 'SMUP' and .w_FLCOST $ 'E-S' and !Empty(.w_MAGRAG) , "S", "N")
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(81,81,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page2.oPag.SelDis.Calculate()
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_TWKEFLENMI()
    with this
          * --- Eventi damodifica query di selezione
          .w_OUNOMQUE = SYS(2014, .w_OUNOMQUE)
          .w_OUNOMQUE = IIF(FILE(.w_OUNOMQUE) And lower(Alltrim(right(.w_OUNOMQUE,4)))=='.vqr', .w_OUNOMQUE, "")
          .w_CODINI = IIF(Empty(.w_OUNOMQUE), .w_CODINI, "")
          GSDS2BDC(this;
              ,'ANTEPRIMA';
             )
    endwith
  endproc
  proc Calculate_DNQOHTLVII()
    with this
          * --- Eventi modifica Dettaglio costi prodotto
          GSDS2BDC(this;
              ,"AGDETCOS";
             )
    endwith
  endproc
  proc Calculate_THWHPQCJWC()
    with this
          * --- Eventi modifica storico costi prodotto
          GSDS2BDC(this;
              ,"AGSTOCOS";
             )
    endwith
  endproc
  proc Calculate_WKQUULDECR()
    with this
          * --- Selezione Zoom
          GSDS2BDC(this;
              ,'SELDESEL';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oESERC_1_41.enabled = this.oPgFrm.Page1.oPag.oESERC_1_41.mCond()
    this.oPgFrm.Page1.oPag.oNUMINV_1_42.enabled = this.oPgFrm.Page1.oPag.oNUMINV_1_42.mCond()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_61.enabled = this.oPgFrm.Page1.oPag.oCAOLIS_1_61.mCond()
    this.oPgFrm.Page1.oPag.oCODMAG_1_64.enabled = this.oPgFrm.Page1.oPag.oCODMAG_1_64.mCond()
    this.oPgFrm.Page1.oPag.oFLCOSINV_1_68.enabled = this.oPgFrm.Page1.oPag.oFLCOSINV_1_68.mCond()
    this.oPgFrm.Page1.oPag.oTIPOARTI_1_104.enabled = this.oPgFrm.Page1.oPag.oTIPOARTI_1_104.mCond()
    this.oPgFrm.Page1.oPag.oINESCONT_1_106.enabled = this.oPgFrm.Page1.oPag.oINESCONT_1_106.mCond()
    this.oPgFrm.Page1.oPag.oFLDETTMAG_1_116.enabled = this.oPgFrm.Page1.oPag.oFLDETTMAG_1_116.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oESERC_1_41.visible=!this.oPgFrm.Page1.oPag.oESERC_1_41.mHide()
    this.oPgFrm.Page1.oPag.oNUMINV_1_42.visible=!this.oPgFrm.Page1.oPag.oNUMINV_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oListino_1_44.visible=!this.oPgFrm.Page1.oPag.oListino_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_46.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_46.mHide()
    this.oPgFrm.Page1.oPag.oPRDATINV_1_47.visible=!this.oPgFrm.Page1.oPag.oPRDATINV_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oCAOLIS_1_61.visible=!this.oPgFrm.Page1.oPag.oCAOLIS_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oCODMAG_1_64.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_66.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_66.mHide()
    this.oPgFrm.Page1.oPag.oINESCONT_1_106.visible=!this.oPgFrm.Page1.oPag.oINESCONT_1_106.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
        if lower(cEvent)==lower("w_OUNOMQUE Changed")
          .Calculate_TWKEFLENMI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGDETCOS Changed")
          .Calculate_DNQOHTLVII()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGSTOCOS Changed")
          .Calculate_THWHPQCJWC()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.SelDis.Event(cEvent)
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_WKQUULDECR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDCOSPAR,PDCOECOS";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_AZIENDA)
            select PDCODAZI,PDCOSPAR,PDCOECOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.PDCODAZI,space(5))
      this.w_PDCOSPAR = NVL(_Link_.PDCOSPAR,space(1))
      this.w_PDCOECOP = NVL(_Link_.PDCOECOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PDCOSPAR = space(1)
      this.w_PDCOECOP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_6'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DIS1 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DIS1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DIS1 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_8'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DIS2 = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DIS2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DIS2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS1
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS1)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS1 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS1OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS1 = space(20)
      endif
      this.w_DIS1OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIS2
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DIS2)
            select DBCODICE,DBDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIS2 = NVL(_Link_.DBCODICE,space(20))
      this.w_DIS2OB = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIS2 = space(20)
      endif
      this.w_DIS2OB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERC
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERC)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESERC))
          select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERC)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERC) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERC_1_41'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERC)
            select ESCODAZI,ESCODESE,ESVALNAZ,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERC = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
      this.w_ESFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESERC = space(4)
      endif
      this.w_VALESE = space(3)
      this.w_ESFINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINV
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AIN',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERC;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_1_42'),i_cWhere,'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERC;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV,INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM,INESCONT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INMAGCOL = NVL(_Link_.INMAGCOL,space(1))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INCATOMO = NVL(_Link_.INCATOMO,space(5))
      this.w_INGRUMER = NVL(_Link_.INGRUMER,space(5))
      this.w_INCODFAM = NVL(_Link_.INCODFAM,space(5))
      this.w_MAGRAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INESCONT = NVL(_Link_.INESCONT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
      this.w_INCODMAG = space(5)
      this.w_INMAGCOL = space(1)
      this.w_PRDATINV = ctod("  /  /  ")
      this.w_INCATOMO = space(5)
      this.w_INGRUMER = space(5)
      this.w_INCODFAM = space(5)
      this.w_MAGRAG = space(5)
      this.w_INESCONT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Listino
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Listino) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_Listino)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_Listino))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Listino)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Listino) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oListino_1_44'),i_cWhere,'',"Anagrafica listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Listino)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_Listino);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_Listino)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Listino = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_Listino = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Listino Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALLIS
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALLIS)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALLIS = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALLIS = space(3)
      endif
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODMAG
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGMAGRAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_INCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_INCODMAG)
            select MGCODMAG,MGMAGRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_MAGRAG = NVL(_Link_.MGMAGRAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INCODMAG = space(5)
      endif
      this.w_MAGRAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_64'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_72'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_73'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_74'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_75'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_76'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_77'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_6.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_6.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_7.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_7.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_8.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_8.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_9.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_9.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_10.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_10.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oQUANTI_1_11.value==this.w_QUANTI)
      this.oPgFrm.Page1.oPag.oQUANTI_1_11.value=this.w_QUANTI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOST_1_13.RadioValue()==this.w_FLCOST)
      this.oPgFrm.Page1.oPag.oFLCOST_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnalisi_1_32.RadioValue()==this.w_Analisi)
      this.oPgFrm.Page1.oPag.oAnalisi_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERRIC_1_33.RadioValue()==this.w_PERRIC)
      this.oPgFrm.Page1.oPag.oPERRIC_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTOMED_1_34.RadioValue()==this.w_LOTTOMED)
      this.oPgFrm.Page1.oPag.oLOTTOMED_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCDRSR_1_35.RadioValue()==this.w_SCDRSR)
      this.oPgFrm.Page1.oPag.oSCDRSR_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCOS_1_36.RadioValue()==this.w_TIPCOS)
      this.oPgFrm.Page1.oPag.oTIPCOS_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCOECOS_1_37.RadioValue()==this.w_PDCOECOS)
      this.oPgFrm.Page1.oPag.oPDCOECOS_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoValo_1_38.RadioValue()==this.w_TipoValo)
      this.oPgFrm.Page1.oPag.oTipoValo_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESERC_1_41.value==this.w_ESERC)
      this.oPgFrm.Page1.oPag.oESERC_1_41.value=this.w_ESERC
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINV_1_42.value==this.w_NUMINV)
      this.oPgFrm.Page1.oPag.oNUMINV_1_42.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oListino_1_44.value==this.w_Listino)
      this.oPgFrm.Page1.oPag.oListino_1_44.value=this.w_Listino
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_46.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_46.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_47.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_47.value=this.w_PRDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESART_1_60.RadioValue()==this.w_FLDESART)
      this.oPgFrm.Page1.oPag.oFLDESART_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOLIS_1_61.value==this.w_CAOLIS)
      this.oPgFrm.Page1.oPag.oCAOLIS_1_61.value=this.w_CAOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_64.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_64.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_66.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_66.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOSINV_1_68.RadioValue()==this.w_FLCOSINV)
      this.oPgFrm.Page1.oPag.oFLCOSINV_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_72.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_72.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_73.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_73.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_74.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_74.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_75.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_75.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_76.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_76.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_77.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_77.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_78.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_78.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_79.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_79.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_80.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_80.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_84.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_84.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_85.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_85.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_86.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_86.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_3.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMQUE_1_93.value==this.w_OUNOMQUE)
      this.oPgFrm.Page1.oPag.oOUNOMQUE_1_93.value=this.w_OUNOMQUE
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPRO_1_97.RadioValue()==this.w_DISPRO)
      this.oPgFrm.Page1.oPag.oDISPRO_1_97.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLP_1_100.RadioValue()==this.w_FLCHKLP)
      this.oPgFrm.Page1.oPag.oFLCHKLP_1_100.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSTILOG_1_103.RadioValue()==this.w_COSTILOG)
      this.oPgFrm.Page1.oPag.oCOSTILOG_1_103.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOARTI_1_104.RadioValue()==this.w_TIPOARTI)
      this.oPgFrm.Page1.oPag.oTIPOARTI_1_104.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINESCONT_1_106.RadioValue()==this.w_INESCONT)
      this.oPgFrm.Page1.oPag.oINESCONT_1_106.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDETTMAG_1_116.RadioValue()==this.w_FLDETTMAG)
      this.oPgFrm.Page1.oPag.oFLDETTMAG_1_116.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODINI) OR NOT EMPTY(.w_DIS1)) AND (empty(.w_CODFIN) OR .w_CODINI<=.w_CODFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS1))))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not((EMPTY(.w_CODFIN) OR NOT EMPTY(.w_DIS2)) AND (.w_CODFIN>=.w_CODINI or empty(.w_CODFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DIS2))))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   ((empty(.w_DATSTA)) or not((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice distinta di inizio o fine selezione obsoleto alla data selezionata")
          case   (empty(.w_QUANTI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQUANTI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_QUANTI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("indicare la quantit� da esplodere! (Maggiore di 0)")
          case   (empty(.w_CAOLIS))  and not(.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))  and (.w_CAOVAL=0 AND .w_TipoValo="L" AND NOT EMPTY(.w_Listino))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOLIS_1_61.SetFocus()
            i_bnoObbl = !empty(.w_CAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODMAG))  and not(.w_TipoValo <> 'A')  and (.w_TipoValo = 'A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_64.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_DATSTA = this.w_DATSTA
    this.o_FLCOST = this.w_FLCOST
    this.o_TipoValo = this.w_TipoValo
    this.o_ESERC = this.w_ESERC
    this.o_NUMINV = this.w_NUMINV
    this.o_Listino = this.w_Listino
    this.o_MAGRAG = this.w_MAGRAG
    this.o_OUNOMQUE = this.w_OUNOMQUE
    return

enddefine

* --- Define pages as container
define class tgsds_sdcPag1 as StdContainer
  Width  = 856
  height = 532
  stdWidth  = 856
  stdheight = 532
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_6 as StdField with uid="YLDHOMMPET",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 104583386,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=123, Top=10, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_7 as StdField with uid="YJQMLZHVQA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104524490,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=10, InputMask=replicate('X',40)

  add object oCODFIN_1_8 as StdField with uid="VXFHEBELNK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Distinta base di fine selezione (spazio=no selezione)",;
    HelpContextID = 26136794,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=123, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_9 as StdField with uid="DESEQAHWJU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26077898,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=36, InputMask=replicate('X',40)

  add object oDATSTA_1_10 as StdField with uid="WHTHOLGNAQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Codice distinta di inizio o fine selezione obsoleto alla data selezionata",;
    ToolTipText = "Data di stampa",;
    HelpContextID = 231792330,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=123, Top=62

  func oDATSTA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DIS1OB>.w_DATSTA or empty(.w_DIS1OB)) and (.w_DIS2OB>.w_DATSTA or empty(.w_DIS2OB)))
    endwith
    return bRes
  endfunc

  add object oQUANTI_1_11 as StdField with uid="ATRFNFHQBW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_QUANTI", cQueryName = "QUANTI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "indicare la quantit� da esplodere! (Maggiore di 0)",;
    ToolTipText = "Quantit� da esplodere",;
    HelpContextID = 97974778,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=356, Top=62, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"


  add object oFLCOST_1_13 as StdCombo with uid="TYQNNXBBPH",rtseq=13,rtrep=.f.,left=543,top=88,width=153,height=21;
    , ToolTipText = "Imposta modalit� di aggiornamento e stampa";
    , HelpContextID = 182838186;
    , cFormVar="w_FLCOST",RowSource=""+"Nessun aggiornamento,"+"Aggiornamento e stampa,"+"Solo Aggiornamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLCOST_1_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLCOST_1_13.GetRadio()
    this.Parent.oContained.w_FLCOST = this.RadioValue()
    return .t.
  endfunc

  func oFLCOST_1_13.SetRadio()
    this.Parent.oContained.w_FLCOST=trim(this.Parent.oContained.w_FLCOST)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOST=='N',1,;
      iif(this.Parent.oContained.w_FLCOST=='E',2,;
      iif(this.Parent.oContained.w_FLCOST=='S',3,;
      0)))
  endfunc


  add object oObj_1_14 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=489, width=485,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103981798


  add object oBtn_1_15 as StdButton with uid="TYEVPKXNEQ",left=749, top=484, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 67773734;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do GSDS1BDC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="BSJPZGRESF",left=800, top=484, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66698426;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAnalisi_1_32 as StdCheck with uid="AZEHPXYLAW",rtseq=22,rtrep=.f.,left=33, top=266, caption="Analisi",;
    ToolTipText = "Stampa l'analisi dei costi dei prodotti",;
    HelpContextID = 174514682,;
    cFormVar="w_Analisi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnalisi_1_32.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oAnalisi_1_32.GetRadio()
    this.Parent.oContained.w_Analisi = this.RadioValue()
    return .t.
  endfunc

  func oAnalisi_1_32.SetRadio()
    this.Parent.oContained.w_Analisi=trim(this.Parent.oContained.w_Analisi)
    this.value = ;
      iif(this.Parent.oContained.w_Analisi=="S",1,;
      0)
  endfunc

  add object oPERRIC_1_33 as StdCheck with uid="ITJTQSYEXZ",rtseq=23,rtrep=.f.,left=126, top=266, caption="% Ricarico",;
    ToolTipText = "Aggiunge la percentuale di ricarico nei costi",;
    HelpContextID = 209844746,;
    cFormVar="w_PERRIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERRIC_1_33.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPERRIC_1_33.GetRadio()
    this.Parent.oContained.w_PERRIC = this.RadioValue()
    return .t.
  endfunc

  func oPERRIC_1_33.SetRadio()
    this.Parent.oContained.w_PERRIC=trim(this.Parent.oContained.w_PERRIC)
    this.value = ;
      iif(this.Parent.oContained.w_PERRIC=="S",1,;
      0)
  endfunc

  add object oLOTTOMED_1_34 as StdCheck with uid="XJGAFWFWWO",rtseq=24,rtrep=.f.,left=269, top=266, caption="Su lotto medio",;
    ToolTipText = "Calcola i costi di fissi in funzione del lotto medio",;
    HelpContextID = 232796154,;
    cFormVar="w_LOTTOMED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLOTTOMED_1_34.RadioValue()
    return(iif(this.value =1,True,;
    False))
  endfunc
  func oLOTTOMED_1_34.GetRadio()
    this.Parent.oContained.w_LOTTOMED = this.RadioValue()
    return .t.
  endfunc

  func oLOTTOMED_1_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LOTTOMED==True,1,;
      0)
  endfunc

  add object oSCDRSR_1_35 as StdCheck with uid="KVRSJMUCSJ",rtseq=25,rtrep=.f.,left=430, top=266, caption="Ciclo semplificato",;
    ToolTipText = "Stampa i costi delle risorse",;
    HelpContextID = 216194010,;
    cFormVar="w_SCDRSR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCDRSR_1_35.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oSCDRSR_1_35.GetRadio()
    this.Parent.oContained.w_SCDRSR = this.RadioValue()
    return .t.
  endfunc

  func oSCDRSR_1_35.SetRadio()
    this.Parent.oContained.w_SCDRSR=trim(this.Parent.oContained.w_SCDRSR)
    this.value = ;
      iif(this.Parent.oContained.w_SCDRSR=="S",1,;
      0)
  endfunc

  add object oTIPCOS_1_36 as StdCheck with uid="RFKZVDJUYK",rtseq=26,rtrep=.f.,left=658, top=266, caption="Costificazione parziale",;
    ToolTipText = "Se attivo: la costificazione avverr� in funzione del check esplosione impostato in distinta",;
    HelpContextID = 204543434,;
    cFormVar="w_TIPCOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCOS_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCOS_1_36.GetRadio()
    this.Parent.oContained.w_TIPCOS = this.RadioValue()
    return .t.
  endfunc

  func oTIPCOS_1_36.SetRadio()
    this.Parent.oContained.w_TIPCOS=trim(this.Parent.oContained.w_TIPCOS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCOS=='S',1,;
      0)
  endfunc

  add object oPDCOECOS_1_37 as StdCheck with uid="WHCLBVISGJ",rtseq=27,rtrep=.f.,left=658, top=295, caption="Considera coeff. impiego",;
    ToolTipText = "Considera coeff. impiego in fase di costificazione",;
    HelpContextID = 214297271,;
    cFormVar="w_PDCOECOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDCOECOS_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPDCOECOS_1_37.GetRadio()
    this.Parent.oContained.w_PDCOECOS = this.RadioValue()
    return .t.
  endfunc

  func oPDCOECOS_1_37.SetRadio()
    this.Parent.oContained.w_PDCOECOS=trim(this.Parent.oContained.w_PDCOECOS)
    this.value = ;
      iif(this.Parent.oContained.w_PDCOECOS=='S',1,;
      0)
  endfunc


  add object oTipoValo_1_38 as StdCombo with uid="UTJKFXOHYK",rtseq=28,rtrep=.f.,left=173,top=312,width=201,height=21;
    , ToolTipText = "Criterio di valorizzazione dei materiali d'aquisto";
    , HelpContextID = 227734875;
    , cFormVar="w_TipoValo",RowSource=""+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Ultimo costo dei saldi (articolo),"+"Ultimo costo standard (articolo),"+"Costo di listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoValo_1_38.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"M",;
    iif(this.value =3,"P",;
    iif(this.value =4,"U",;
    iif(this.value =5,"A",;
    iif(this.value =6,"X",;
    iif(this.value =7,"L",;
    space(1)))))))))
  endfunc
  func oTipoValo_1_38.GetRadio()
    this.Parent.oContained.w_TipoValo = this.RadioValue()
    return .t.
  endfunc

  func oTipoValo_1_38.SetRadio()
    this.Parent.oContained.w_TipoValo=trim(this.Parent.oContained.w_TipoValo)
    this.value = ;
      iif(this.Parent.oContained.w_TipoValo=="S",1,;
      iif(this.Parent.oContained.w_TipoValo=="M",2,;
      iif(this.Parent.oContained.w_TipoValo=="P",3,;
      iif(this.Parent.oContained.w_TipoValo=="U",4,;
      iif(this.Parent.oContained.w_TipoValo=="A",5,;
      iif(this.Parent.oContained.w_TipoValo=="X",6,;
      iif(this.Parent.oContained.w_TipoValo=="L",7,;
      0)))))))
  endfunc

  add object oESERC_1_41 as StdField with uid="QVYDMKMJJA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ESERC", cQueryName = "ESERC",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 266517690,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=113, Top=342, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERC"

  func oESERC_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ "S-M-U-P-A" or .w_Analisi = "S")
    endwith
   endif
  endfunc

  func oESERC_1_41.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  func oESERC_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
      if .not. empty(.w_NUMINV)
        bRes2=.link_1_42('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERC_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESERC_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESERC_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oNUMINV_1_42 as StdField with uid="QRTBDIDHQG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 154876458,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=278, Top=342, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="GSMA_AIN", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERC", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_TipoValo $ 'SMUP' or .w_Analisi='S') AND NOT EMPTY(.w_ESERC))
    endwith
   endif
  endfunc

  func oNUMINV_1_42.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  func oNUMINV_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERC)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AIN',"Inventari",'GSDB_SDC.INVENTAR_VZM',this.parent.oContained
  endproc
  proc oNUMINV_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AIN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERC
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINV
     i_obj.ecpSave()
  endproc

  add object oListino_1_44 as StdField with uid="RQFSDHQWGW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_Listino", cQueryName = "Listino",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino di riferimento per valorizzare i materiali d'aquisto",;
    HelpContextID = 10631606,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=113, Top=399, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_Listino"

  func oListino_1_44.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc

  func oListino_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oListino_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oListino_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oListino_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica listini",'',this.parent.oContained
  endproc

  add object oDESLIS_1_46 as StdField with uid="WGBDTWDQZP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210234058,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=171, Top=399, InputMask=replicate('X',40)

  func oDESLIS_1_46.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc

  add object oPRDATINV_1_47 as StdField with uid="UEIFXOYOKT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 98815156,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=384, Top=342

  func oPRDATINV_1_47.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oFLDESART_1_60 as StdCheck with uid="JEIRFKUIPF",rtseq=45,rtrep=.f.,left=543, top=61, caption="Descrizione componente variante",;
    ToolTipText = "Se attivo stampa la descrizione del componente variante",;
    HelpContextID = 34614442,;
    cFormVar="w_FLDESART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDESART_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDESART_1_60.GetRadio()
    this.Parent.oContained.w_FLDESART = this.RadioValue()
    return .t.
  endfunc

  func oFLDESART_1_60.SetRadio()
    this.Parent.oContained.w_FLDESART=trim(this.Parent.oContained.w_FLDESART)
    this.value = ;
      iif(this.Parent.oContained.w_FLDESART=='S',1,;
      0)
  endfunc

  add object oCAOLIS_1_61 as StdField with uid="IQCVTLZTCN",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CAOLIS", cQueryName = "CAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 210251482,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=502, Top=399

  func oCAOLIS_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_TipoValo="L" AND NOT EMPTY(.w_Listino))
    endwith
   endif
  endfunc

  func oCAOLIS_1_61.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oCODMAG_1_64 as StdField with uid="XMPGNCFZCF",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di appartenenza degli articoli da verificare (spazio=verifica per tutti i magazzini)",;
    HelpContextID = 151507162,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=113, Top=399, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo = 'A')
    endwith
   endif
  endfunc

  func oCODMAG_1_64.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  func oCODMAG_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_64.ecpDrop(oSource)
    this.Parent.oContained.link_1_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_64.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_66 as StdField with uid="IGDPZQPCGC",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 151448266,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=171, Top=399, InputMask=replicate('X',30)

  func oDESMAG_1_66.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  add object oFLCOSINV_1_68 as StdCheck with uid="FLKVCBFKWG",rtseq=51,rtrep=.f.,left=113, top=369, caption="Applica costi inventario",;
    HelpContextID = 98952020,;
    cFormVar="w_FLCOSINV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCOSINV_1_68.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCOSINV_1_68.GetRadio()
    this.Parent.oContained.w_FLCOSINV = this.RadioValue()
    return .t.
  endfunc

  func oFLCOSINV_1_68.SetRadio()
    this.Parent.oContained.w_FLCOSINV=trim(this.Parent.oContained.w_FLCOSINV)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOSINV=="S",1,;
      0)
  endfunc

  func oFLCOSINV_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ 'SMUP')
    endwith
   endif
  endfunc

  add object oFAMAINI_1_72 as StdField with uid="BAUZFWPTWC",rtseq=55,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 242004310,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=114, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_72('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_72.ecpDrop(oSource)
    this.Parent.oContained.link_1_72('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_72.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_72'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_73 as StdField with uid="GHDDKOKVIG",rtseq=56,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 113462954,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=543, Top=114, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_73.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_73.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_73'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_74 as StdField with uid="VYQYSTJFCH",rtseq=57,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 104512922,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=139, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_74('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_74.ecpDrop(oSource)
    this.Parent.oContained.link_1_74('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_74.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_74'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_75 as StdField with uid="BKTBROMCMD",rtseq=58,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 26066330,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=543, Top=139, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_75.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_75('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_75.ecpDrop(oSource)
    this.Parent.oContained.link_1_75('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_75.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_75'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_76 as StdField with uid="WQNLFQEXDD",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 104521434,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=163, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_77 as StdField with uid="WHQQEZYTLE",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 26074842,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=543, Top=163, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_77.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_78 as StdField with uid="AOYBYAMKXI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51243649,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=190, Top=114, InputMask=replicate('X',35)

  add object oDESGRUI_1_79 as StdField with uid="ZABORREMDH",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167570122,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=190, Top=139, InputMask=replicate('X',35)

  add object oDESCATI_1_80 as StdField with uid="ZFWYARIWSP",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202435274,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=190, Top=163, InputMask=replicate('X',35)

  add object oDESFAMAF_1_84 as StdField with uid="WOCDSQXKSA",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51243652,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=611, Top=114, InputMask=replicate('X',35)

  add object oDESGRUF_1_85 as StdField with uid="PYGQUHDJMJ",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100865334,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=611, Top=139, InputMask=replicate('X',35)

  add object oDESCATF_1_86 as StdField with uid="SXSGGSHCII",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 66000182,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=611, Top=163, InputMask=replicate('X',35)

  add object oOUNOMQUE_1_93 as StdField with uid="QUARTVKHNU",rtseq=69,rtrep=.f.,;
    cFormVar = "w_OUNOMQUE", cQueryName = "OUNOMQUE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Query di filtro delle distinte base",;
    HelpContextID = 29021739,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=115, Top=436, InputMask=replicate('X',254)


  add object BTNQRY as cp_askfile with uid="PJDGLKPWNN",left=564, top=436, width=23,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_OUNOMQUE",cExt="VQR",;
    nPag=1;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 73814826

  add object oDISPRO_1_97 as StdCheck with uid="IDWOFYGGQK",rtseq=70,rtrep=.f.,left=14, top=217, caption="Provvisorie",;
    ToolTipText = "Stampa la distinta provvisoria",;
    HelpContextID = 267642570,;
    cFormVar="w_DISPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDISPRO_1_97.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDISPRO_1_97.GetRadio()
    this.Parent.oContained.w_DISPRO = this.RadioValue()
    return .t.
  endfunc

  func oDISPRO_1_97.SetRadio()
    this.Parent.oContained.w_DISPRO=trim(this.Parent.oContained.w_DISPRO)
    this.value = ;
      iif(this.Parent.oContained.w_DISPRO=="S",1,;
      0)
  endfunc

  add object oFLCHKLP_1_100 as StdCheck with uid="SKYASPWUKP",rtseq=71,rtrep=.f.,left=269, top=217, caption="Loop distinta",;
    ToolTipText = "Se attivo abilita controllo del loop in distinta base",;
    HelpContextID = 57467818,;
    cFormVar="w_FLCHKLP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLP_1_100.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCHKLP_1_100.GetRadio()
    this.Parent.oContained.w_FLCHKLP = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLP_1_100.SetRadio()
    this.Parent.oContained.w_FLCHKLP=trim(this.Parent.oContained.w_FLCHKLP)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLP=="S",1,;
      0)
  endfunc

  add object oCOSTILOG_1_103 as StdCheck with uid="AOSDRGZMLX",rtseq=72,rtrep=.f.,left=543, top=217, caption="Attiva scrittura log elaborazione",;
    ToolTipText = "Attiva la scrittura del log di elaborazione costi",;
    HelpContextID = 58712211,;
    cFormVar="w_COSTILOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOSTILOG_1_103.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCOSTILOG_1_103.GetRadio()
    this.Parent.oContained.w_COSTILOG = this.RadioValue()
    return .t.
  endfunc

  func oCOSTILOG_1_103.SetRadio()
    this.Parent.oContained.w_COSTILOG=trim(this.Parent.oContained.w_COSTILOG)
    this.value = ;
      iif(this.Parent.oContained.w_COSTILOG=="S",1,;
      0)
  endfunc


  add object oTIPOARTI_1_104 as StdCombo with uid="YQKLJLUZOY",value=4,rtseq=73,rtrep=.f.,left=123,top=88,width=226,height=21;
    , ToolTipText = "Seleziona il tipo articolo legato alla distinta base";
    , HelpContextID = 33221247;
    , cFormVar="w_TIPOARTI",RowSource=""+"Prodotto finito,"+"Semilavorato,"+"Fanstasma,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOARTI_1_104.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'PH',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oTIPOARTI_1_104.GetRadio()
    this.Parent.oContained.w_TIPOARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPOARTI_1_104.SetRadio()
    this.Parent.oContained.w_TIPOARTI=trim(this.Parent.oContained.w_TIPOARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOARTI=='PF',1,;
      iif(this.Parent.oContained.w_TIPOARTI=='SE',2,;
      iif(this.Parent.oContained.w_TIPOARTI=='PH',3,;
      iif(this.Parent.oContained.w_TIPOARTI=='',4,;
      0))))
  endfunc

  func oTIPOARTI_1_104.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PROD='S')
    endwith
   endif
  endfunc


  add object oINESCONT_1_106 as StdCombo with uid="LNVAXODUBN",rtseq=74,rtrep=.f.,left=679,top=342,width=156,height=21;
    , ToolTipText = "Metodo di calcolo del costo e prezzo medio esercizio";
    , HelpContextID = 14795046;
    , cFormVar="w_INESCONT",RowSource=""+"Standard,"+"Continuo,"+"Per movimento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINESCONT_1_106.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oINESCONT_1_106.GetRadio()
    this.Parent.oContained.w_INESCONT = this.RadioValue()
    return .t.
  endfunc

  func oINESCONT_1_106.SetRadio()
    this.Parent.oContained.w_INESCONT=trim(this.Parent.oContained.w_INESCONT)
    this.value = ;
      iif(this.Parent.oContained.w_INESCONT=='N',1,;
      iif(this.Parent.oContained.w_INESCONT=='S',2,;
      iif(this.Parent.oContained.w_INESCONT=='M',3,;
      0)))
  endfunc

  func oINESCONT_1_106.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_NUMINV))
    endwith
   endif
  endfunc

  func oINESCONT_1_106.mHide()
    with this.Parent.oContained
      return (.w_Analisi="N" and .w_TipoValo <> "M")
    endwith
  endfunc

  add object oFLDETTMAG_1_116 as StdCheck with uid="GQTWAXZVRU",rtseq=80,rtrep=.f.,left=384, top=369, caption="Aggiorna costi dettaglio magazzini",;
    HelpContextID = 182439673,;
    cFormVar="w_FLDETTMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDETTMAG_1_116.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLDETTMAG_1_116.GetRadio()
    this.Parent.oContained.w_FLDETTMAG = this.RadioValue()
    return .t.
  endfunc

  func oFLDETTMAG_1_116.SetRadio()
    this.Parent.oContained.w_FLDETTMAG=trim(this.Parent.oContained.w_FLDETTMAG)
    this.value = ;
      iif(this.Parent.oContained.w_FLDETTMAG=="S",1,;
      0)
  endfunc

  func oFLDETTMAG_1_116.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoValo $ 'SMUP' and .w_FLCOST $ 'E-S')
    endwith
   endif
  endfunc

  add object oStr_1_17 as StdString with uid="GIGJHPMORF",Visible=.t., Left=10, Top=490,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="AZAYVNOACS",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=114, Height=15,;
    Caption="Da distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YBUTHVTDPZ",Visible=.t., Left=22, Top=38,;
    Alignment=1, Width=99, Height=15,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LPGIBLLYAF",Visible=.t., Left=22, Top=64,;
    Alignment=1, Width=99, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WXBVMGPNMM",Visible=.t., Left=255, Top=64,;
    Alignment=1, Width=99, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AUJZFEKPFU",Visible=.t., Left=9, Top=243,;
    Alignment=0, Width=390, Height=18,;
    Caption="Criterio di valorizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="FRXPGPJUNO",Visible=.t., Left=6, Top=314,;
    Alignment=1, Width=162, Height=15,;
    Caption="Criterio di valorizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="WNERIRPNMR",Visible=.t., Left=15, Top=401,;
    Alignment=1, Width=95, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_TipoValo<>"L")
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="XROSXMIRDD",Visible=.t., Left=20, Top=343,;
    Alignment=1, Width=90, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="CMQQHAIKKN",Visible=.t., Left=171, Top=343,;
    Alignment=1, Width=104, Height=15,;
    Caption="Numero inventario:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="ZFXOXTIOFY",Visible=.t., Left=348, Top=343,;
    Alignment=1, Width=32, Height=15,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return ((! .w_TipoValo $ "S-M-U-P") AND .w_Analisi = "N")
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="MACUDGSSKF",Visible=.t., Left=428, Top=401,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_TipoValo<>"L" OR EMPTY(.w_Listino))
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="QCEJHAMSPS",Visible=.t., Left=15, Top=401,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_TipoValo <> 'A')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=14, Top=117,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=14, Top=142,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=14, Top=165,;
    Alignment=1, Width=107, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="BETNLUNOYI",Visible=.t., Left=471, Top=117,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=446, Top=142,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=449, Top=165,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="BQPXSTBRUO",Visible=.t., Left=3, Top=439,;
    Alignment=1, Width=109, Height=18,;
    Caption="Query"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="KYGOPRNZRY",Visible=.t., Left=404, Top=90,;
    Alignment=1, Width=137, Height=18,;
    Caption="Aggiorna costo standard:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="HZOBJMFNIA",Visible=.t., Left=12, Top=195,;
    Alignment=0, Width=113, Height=15,;
    Caption="Stato distinte"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="SJOQRQEYXN",Visible=.t., Left=269, Top=195,;
    Alignment=0, Width=71, Height=18,;
    Caption="Controllo"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="HLSCXIZNMW",Visible=.t., Left=24, Top=90,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="PGPCUPGELN",Visible=.t., Left=611, Top=321,;
    Alignment=0, Width=223, Height=15,;
    Caption="Opzioni costo medio ponderato esercizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="ITSFXNBZJR",Visible=.t., Left=8, Top=463,;
    Alignment=0, Width=135, Height=15,;
    Caption="Selezione stampa"  ;
  , bGlobalFont=.t.

  add object oBox_1_27 as StdBox with uid="RCGFMPXTYM",left=10, top=260, width=838,height=1

  add object oBox_1_99 as StdBox with uid="FDAQBTOJIF",left=4, top=212, width=234,height=1

  add object oBox_1_102 as StdBox with uid="ZXUPTCHTYW",left=260, top=212, width=190,height=1

  add object oBox_1_111 as StdBox with uid="EBGDQVQSNS",left=606, top=338, width=234,height=1

  add object oBox_1_112 as StdBox with uid="HZZUJLZEMD",left=10, top=477, width=835,height=1
enddefine
define class tgsds_sdcPag2 as StdContainer
  Width  = 856
  height = 532
  stdWidth  = 856
  stdheight = 532
  resizeXpos=335
  resizeYpos=216
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SelDis as cp_szoombox with uid="SHVQPKONZP",left=-4, top=3, width=858,height=476,;
    caption='Browse',;
   bGlobalFont=.t.,;
    cTable="DISMBASE",cZoomFile="GSDSSKCS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 138740246


  add object oBtn_2_2 as StdButton with uid="VIVRYPRXRO",left=800, top=484, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 16930070;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_3 as StdRadio with uid="PGKTVHMOKS",rtseq=67,rtrep=.f.,left=4, top=484, width=139,height=50;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 92232154
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 92232154
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 92232154
      this.Buttons(3).Top=32
      this.SetAll("Width",137)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'Z',;
    space(1)))))
  endfunc
  func oSELEZI_2_3.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_3.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      iif(this.Parent.oContained.w_SELEZI=='Z',3,;
      0)))
  endfunc


  add object oBtn_2_5 as StdButton with uid="ZYQNADMBYV",left=748, top=484, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per elaborare il costo standard";
    , HelpContextID = 16930070;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        do GSDS1BDC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_sdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
