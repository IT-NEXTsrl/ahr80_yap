* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bap                                                        *
*              Aggorna piano produzione da ordini                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-23                                                      *
* Last revis.: 2000-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bap",oParentObject)
return(i_retval)

define class tgsds_bap as StdBatch
  * --- Local variables
  w_CNT = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  UNIMIS_idx=0
  DISMBASE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Alla Conferma Ordini, Aggiorna Piano Produzione (ds GSDS_MPP)
    this.w_PADRE = this.oParentObject
    if USED("RigheOrd")
      * --- Carica Tmp Detail Piano di Produzione
      this.w_PADRE.MarkPos()     
      this.w_CNT = 0
      SELECT RigheOrd
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODCOM,"")) AND NOT EMPTY(NVL(CODART,"")) AND NOT EMPTY(NVL(CODDIS,"")) AND NVL(QTAUM1, 0)>0
      SELECT (this.w_PADRE.cTrsName)
      this.w_PADRE.AddRow()     
      SELECT RigheOrd
      this.oParentObject.w_PPCODCOM = CODCOM
      this.oParentObject.w_PPARTCOM = CODART
      this.oParentObject.w_PPUNIMIS = NVL(UNMIS1, "   ")
      if NOT EMPTY(this.oParentObject.w_PPUNIMIS)
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_PPUNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.oParentObject.w_PPUNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT RigheOrd
      endif
      this.oParentObject.w_PPQTAMOV = NVL(QTAUM1, 0)
      this.oParentObject.w_PPQTAUM1 = NVL(QTAUM1, 0)
      this.oParentObject.w_DESART = DESART
      this.oParentObject.w_UNMIS1 = NVL(UNMIS1, "   ")
      this.oParentObject.w_UNMIS2 = NVL(UNMIS2, "   ")
      this.oParentObject.w_OPERAT = NVL(OPERAT, " ")
      this.oParentObject.w_MOLTIP = NVL(MOLTIP, 0)
      this.oParentObject.w_UNMIS3 = NVL(UNMIS3, "   ")
      this.oParentObject.w_OPERA3 = NVL(OPERA3, " ")
      this.oParentObject.w_MOLTI3 = NVL(MOLTI3, 0)
      this.oParentObject.w_DISCOM = CODDIS
      this.oParentObject.w_STATUS = " "
      if NOT EMPTY(this.oParentObject.w_DISCOM)
        * --- Read from DISMBASE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DISMBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DBFLSTAT"+;
            " from "+i_cTable+" DISMBASE where ";
                +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DISCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DBFLSTAT;
            from (i_cTable) where;
                DBCODICE = this.oParentObject.w_DISCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_STATUS = NVL(cp_ToDate(_read_.DBFLSTAT),cp_NullValue(_read_.DBFLSTAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT RigheOrd
      endif
      this.oParentObject.w_TIPRIG = "R"
      * --- Carica il Temporaneo
      this.w_CNT = this.w_CNT + 1
      ah_Msg("Inserimento riga: %1",.T.,.F.,.F., ALLTRIM(STR(this.w_CNT)) )
      this.w_PADRE.TrsFromWork()     
      SELECT RigheOrd
      ENDSCAN
      WAIT CLEAR
      * --- Chiude il Cursore
      SELECT RigheOrd
      USE
      * --- Questa Parte derivata dal Metodo LoadRec
      this.w_PADRE.RePos(.T.)     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='DISMBASE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
