* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdscbdc                                                        *
*              Elaborazione costi articolo                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-05                                                      *
* Last revis.: 2015-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pKEYRIF,pDATRIF,pTipoValo,pAnalisi,pESERC,pNUMINV,pListino,pAGGMPOND,pLatoServer
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdscbdc",oParentObject,m.pKEYRIF,m.pDATRIF,m.pTipoValo,m.pAnalisi,m.pESERC,m.pNUMINV,m.pListino,m.pAGGMPOND,m.pLatoServer)
return(i_retval)

define class tgsdscbdc as StdBatch
  * --- Local variables
  pKEYRIF = space(10)
  pDATRIF = ctod("  /  /  ")
  pTipoValo = space(1)
  pAnalisi = space(1)
  pESERC = space(4)
  pNUMINV = space(6)
  pListino = space(5)
  pAGGMPOND = space(3)
  pLatoServer = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_Analisi = space(1)
  w_TipoValo = space(1)
  w_Listino = space(5)
  w_ESERC = space(4)
  w_NUMINV = space(6)
  w_VALESE = space(3)
  w_CAOESE = 0
  w_AGGMPOND = space(1)
  w_KEYRIF = space(10)
  w_ERRO = .f.
  w_PARELAB = .f.
  OutCursor = space(10)
  w_FLCOSINV = space(1)
  w_INESCONT = space(1)
  w_PREZZO = 0
  w_DecStampa = 0
  w_Tmp = space(41)
  w_DECTOT = 0
  w_CODVAL = space(3)
  w_LIPREZZO = 0
  w_MAQTA1UM = 0
  w_UNMIS1 = space(3)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_CODLIS = space(0)
  w_SCOLIS = space(1)
  w_CAUNMISU = space(3)
  w_CAQTAMOV = 0
  w_CAQTAMO1 = 0
  w_CODIAR = space(20)
  w_VALLIS = space(3)
  w_CALPRZ = 0
  w_UMLIPREZZO = space(3)
  w_UNIMISLI = space(3)
  w_CAO = 0
  w_CAOLIS = 0
  w_QTALIS = 0
  w_INNUMPRE = space(6)
  w_TipoValoOld = space(1)
  w_INDATINV = ctod("  /  /  ")
  w_INCODESE = space(4)
  w_INESEPRE = space(4)
  w_INCODMAG = space(5)
  w_MAGRAG = space(5)
  w_INCATOMO = space(5)
  w_INGRUMER = space(5)
  w_INCODFAM = space(5)
  w_INMAGCOL = space(1)
  w_PRDATINV = ctod("  /  /  ")
  w_ESCONT = space(1)
  w_DECESE = 0
  w_CAOPRE = 0
  w_FlaVal = space(10)
  w_Codice = space(40)
  w_CriVal = space(10)
  w_CodArt = space(20)
  w_PreQtaEsi = 0
  w_CodVar = space(20)
  w_PreQtaVen = 0
  w_QtaEsi = 0
  w_PreQtaAcq = 0
  w_QtaVen = 0
  w_PrePrzMpa = 0
  w_QtaAcq = 0
  w_PreCosMpa = 0
  w_PreCosMpp = 0
  w_PrzMpa = 0
  w_PreCosUlt = 0
  w_PrzMpp = 0
  w_ElenMag = space(10)
  w_PrzUlt = 0
  w_RecElab = 0
  w_CosMpa = 0
  w_PqtMpp = 0
  w_CosMpp = 0
  w_QtaScagl = 0
  w_CqtMpp = 0
  w_CpRowNum = 0
  w_CosUlt = 0
  w_IsDatMov = ctod("  /  /  ")
  w_CosSta = 0
  w_IsDatFin = ctod("  /  /  ")
  w_CosLco = 0
  w_IsQtaEsi = ctod("  /  /  ")
  w_CosLsc = 0
  w_IsValUni = 0
  w_CosFco = 0
  w_DataPrzUlt = ctod("  /  /  ")
  w_UltimoArt = space(40)
  w_DataCosUlt = ctod("  /  /  ")
  w_TIPOVALOr = space(1)
  w_ValQta = space(1)
  w_TipMagaz = space(1)
  w_ValTip = space(1)
  w_Segnalato = .f.
  w_DatReg = space(10)
  w_Trec = 0
  w_Tart = 0
  w_PrzMpaWrite = 0
  w_CosMpaWrite = 0
  w_NEWREC = space(10)
  w_QtaEsiUno = 0
  w_QtaVenUno = 0
  w_QtaAcqUno = 0
  w_QtaEsiWrite = 0
  w_QtaVenWrite = 0
  w_QtaAcqWrite = 0
  w_PrePreUlt = 0
  w_PreQtaEsr = 0
  w_PreQtaVer = 0
  w_PreQtaAcr = 0
  w_QtaEsr = 0
  w_QtaVer = 0
  w_QtaAcr = 0
  w_QtaVerWrite = 0
  w_QtaAcrWrite = 0
  w_Maga = space(5)
  w_Vendite = 0
  w_Acquisti = 0
  w_SLCODART = space(20)
  w_CARVAL = 0
  w_SCAVAL = 0
  w_PreCarVal = 0
  w_PreScaVal = 0
  w_ULTDATPRZ = ctod("  /  /  ")
  w_ULTVALPRZ = 0
  w_ULTMAGPRZ = space(5)
  w_ULTMATPRZ = space(5)
  w_ULTAGGPRZ = space(2)
  w_ULTDATCOS = ctod("  /  /  ")
  w_ULTVALCOS = 0
  w_ULTMAGCOS = space(5)
  w_ULTMAGCOS = space(5)
  w_ULTMATCOS = space(5)
  w_ULTAGGCOS = space(2)
  w_COSMPAMOV = 0
  w_ESIMPAMOV = 0
  w_COSMPPMOV = 0
  w_VarValoAcq = 0
  w_VarValoVen = 0
  w_Old_CosMpp = 0
  w_NUMERO = 0
  w_CODART = space(20)
  * --- WorkFile variables
  ART_TEMP_idx=0
  MAGAZZIN_idx=0
  VALUTE_idx=0
  INVENTAR_idx=0
  ESERCIZI_idx=0
  LISTINI_idx=0
  INVEDETT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro che indica se sta effettuando la determinazione costo standard
    * --- data riferimento per elaborazione
    * --- Tipo Valorizzazione
    * --- Flag Analisi
    * --- Esercizio
    * --- Inventario di riferimento
    * --- Listino per valorizzazione
    * --- Tipo aggiornamento costo standard
    * --- Parametro che indica se la procedura di esplosione deve rilasciare il cursore alla fine dell'elaborazione
    *     oppure lasciare viva la tabella temporanea con i dati
    * --- Controllo i parametri
    this.w_KEYRIF = IIF(type("this.pKEYRIF")="C" , this.pKEYRIF , SPACE(10) )
    this.pESERC = IIF(type("this.pESERC")="C" , this.pESERC , g_CODESE )
    this.pNUMINV = IIF(type("this.pNUMINV")<>"C" , SPACE(1) , this.pNUMINV )
    this.pTipoValo = IIF(type("this.pTipoValo")<>"C" , SPACE(6) , this.pTipoValo )
    this.pAnalisi = IIF(type("this.pAnalisi")<>"C" , SPACE(1) , this.pAnalisi )
    this.pListino = IIF(type("this.pListino")<>"C" , SPACE(5) , this.pListino )
    this.pDATRIF = IIF(type("this.pDATRIF")<>"D" , CTOD("  -  -  ") , this.pDATRIF )
    this.pAGGMPOND = IIF(Type("this.pAGGMPOND")<>"C" Or empty(nvl(this.pAGGMPOND, "")), "NNN", this.pAGGMPOND)
    this.pLatoServer = iif(type("this.pLatoServer")="C", this.pLatoServer, "N")
    * --- ------------------------------------------------------------------
    this.OutCursor = "__bsi__"
    * --- Inizializzo e controllo i parametri
    this.w_TipoValo = this.pTipoValo
    this.w_Analisi = this.pAnalisi
    this.w_ESERC = this.pESERC
    this.w_NUMINV = this.pNUMINV
    this.w_Listino = this.pListino
    this.w_DATRIF = this.pDATRIF
    this.w_AGGMPOND = LEFT(this.pAGGMPOND, 1)
    this.w_INESCONT = SUBSTR(this.pAGGMPOND, 2 , 1)
    this.w_FLCOSINV = RIGHT(this.pAGGMPOND, 1)
    this.w_INDATINV = this.pDATRIF
    PRIVATE cField,cJoin 
 cField = "cacodric" 
 cJoin = "ArtDist "
    if Empty(this.w_DATRIF)
      ah_ErrorMsg("Inserire la data di riferimento",16)
      i_retcode = 'stop'
      return
    endif
    * --- Costi Articoli
    if !Empty(this.w_NUMINV)
      * --- Leggo dati dall'inventario
      * --- Read from INVENTAR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVENTAR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM"+;
          " from "+i_cTable+" INVENTAR where ";
              +"INNUMINV = "+cp_ToStrODBC(this.w_NUMINV);
              +" and INCODESE = "+cp_ToStrODBC(this.w_ESERC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
          from (i_cTable) where;
              INNUMINV = this.w_NUMINV;
              and INCODESE = this.w_ESERC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INCODMAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
        this.w_INMAGCOL = NVL(cp_ToDate(_read_.INMAGCOL),cp_NullValue(_read_.INMAGCOL))
        this.w_PRDATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
        this.w_INCATOMO = NVL(cp_ToDate(_read_.INCATOMO),cp_NullValue(_read_.INCATOMO))
        this.w_INGRUMER = NVL(cp_ToDate(_read_.INGRUMER),cp_NullValue(_read_.INGRUMER))
        this.w_INCODFAM = NVL(cp_ToDate(_read_.INCODFAM),cp_NullValue(_read_.INCODFAM))
        this.w_MAGRAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGMAGRAG"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_INCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGMAGRAG;
          from (i_cTable) where;
              MGCODMAG = this.w_INCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MAGRAG = IIF (EMPTY(NVL(this.w_MAGRAG,"")),this.w_INCODMAG,this.w_MAGRAG)
    endif
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
            +" and ESCODESE = "+cp_ToStrODBC(this.w_ESERC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = i_CODAZI;
            and ESCODESE = this.w_ESERC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALESE = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOESE = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DecStampa = 10
    if this.w_TipoValo="L"
      this.w_LIPREZZO = 0
      * --- Calcola Prezzo e Sconti
      * --- Azzero l'Array che verr� riempito dalla Funzione
      * --- Prezzi di listino
      USE IN SELECT("przlist")
      vq_exec("..\DISB\EXE\QUERY\GSDSLBDC.VQR", this, "przlist")
      SELECT "przlist"
      GO TOP
      SCAN FOR !EMPTY(LICODLIS)
      this.w_LIPREZZO = 0
      this.w_UNMIS1 = NVL(ARUNMIS1, SPACE(3))
      this.w_MOLTIP = NVL(ARMOLTIP, 0)
      this.w_UNMIS2 = NVL(ARUNMIS2, SPACE(3))
      this.w_OPERAT = NVL(AROPERAT , "*" )
      this.w_UNMIS3 = NVL(CAUNIMIS, SPACE(3))
      this.w_OPERA3 = NVL(CAOPERAT , "*" )
      this.w_MOLTI3 = NVL(CAMOLTIP, 0)
      this.w_PREZUM = ARPREZUM
      this.w_CAUNMISU = NVL(CAUNMISU, SPACE(3))
      this.w_CAQTAMOV = NVL(CAQTAMOV, 0)
      this.w_CAQTAMO1 = NVL(CAQTAMO1, 0)
      this.w_CODIAR = left(nvl(LICODART, space(20))+space(20) , 20)
      this.w_CODVAL = nvl(LSVALLIS, g_PERVAL)
      this.w_VALLIS = nvl(LSVALLIS, g_PERVAL)
      DECLARE ARRCALC (16)
      ARRCALC(1)=0
      this.w_QTAUM3 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
      this.w_QTAUM2 = CALQTA( this.w_CAQTAMO1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3, this.w_DecStampa)
      DIMENSION pArrUm[9]
      pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_CAUNMISU 
 pArrUm [3] = this.w_CAQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_CAQTAMO1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
      this.w_SCOLIS = "S"
      this.w_CAOLIS = GETCAM(this.w_VALLIS, i_DATSYS, 0)
      this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.w_Listino , this.w_CODIAR , " " , this.w_CAQTAMO1 , this.w_VALLIS , this.w_CAOLIS , this.w_DATRIF , " " , " ", g_PERVAL , " ", " ", " ", this.w_SCOLIS, 0,"M", @ARRCALC, " ", " ","N",@pArrUm )
      this.w_DECTOT = getvalut(this.w_VALESE,"VADECUNI")
      this.w_CLUNIMIS = ARRCALC(16)
      this.w_LIPREZZO = CAVALRIG(ARRCALC(5),1, ARRCALC(1),ARRCALC(2),ARRCALC(3),ARRCALC(4),this.w_DECTOT)
      if this.w_CAUNMISU<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
        this.w_QTALIS = IIF(this.w_CLUNIMIS=this.w_UNMIS1,this.w_CAQTAMO1, IIF(this.w_CLUNIMIS=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
        this.w_LIPREZZO = cp_Round(CALMMPZ(this.w_LIPREZZO, this.w_CAQTAMOV, this.w_QTALIS, "N", 0, this.w_DECTOT), this.w_DECTOT)
      endif
      if this.w_VALLIS<>g_PERVAL
        * --- Cambio valuta
        this.w_CAO = GETCAM(this.w_VALLIS, i_DATSYS, 0)
        if this.w_CAO<>0
          this.w_LIPREZZO = VAL2MON(this.w_LIPREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
        else
          ah_ErrorMsg("Impossibile fare il cambio di valuta per il listino selezionato",48)
          this.w_ERRO = .T.
        endif
      endif
      SELECT "przlist"
      REPLACE LAPREZZO WITH this.w_LIPREZZO
      ENDSCAN
      if used("przlist")
         =WRCURSOR("przlist") 
 select przlist
      else
      endif
      if this.w_ERRO
        this.w_ERRO = .F.
      else
        cField = cField + ", nvl(caunmisu,  space(3)) as unimis, nvl(caqtamov, 000000000000.00000) as caqtamov"
        cField = cField + ", nvl(laprezzo, 000000000000.00000) as laprezzo, ivperiva"
        cJoin = cJoin +" left outer join przlist ON przlist.licodart=ArtDist.cacodric "
      endif
    endif
    if this.w_Analisi<>"N" or this.w_TipoValo="A"
      * --- Ultimo Costo Saldi Articoli
      vq_exec("..\DISB\EXE\QUERY\GSDSABDC", this, "przultsa")
      cField = cField + ",nvl(slcodvaa, g_PERVAL) AS slcodvaa, nvl(slvaluca,000000000000.00000) as slvaluca"
      cJoin = cJoin +" left outer join przultsa ON przultsa.slcodice=ArtDist.cacodric "
    endif
    this.w_INNUMPRE = this.w_NUMINV
    if this.w_Analisi<>"N" or this.w_TipoValo $ "SMUP"
      if Empty(this.w_INNUMPRE) 
        * --- Determinazione Costo
        cField = cField + ", 000000000000,00000 as dicosmpp, 000000000000,00000 as dicosmpa" 
 cField = cField + ", 000000000000,00000 as dicosult, 000000000000,00000 as dicossta"
      else
        this.w_TipoValoOld = this.w_TipoValo
        this.w_TipoValo = IIF(this.w_AGGMPOND="N" , "M", this.w_AGGMPOND)
        * --- Costo medio ponderato e Costo Ultimo (da inventario)
        vq_exec("..\DISB\EXE\QUERY\GSDSIBDC.VQR", this, "przinve")
        =WRCURSOR("przinve")
        if this.w_FLCOSINV<>"S"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_TipoValo = this.w_TipoValoOld
        if this.w_VALESE<>g_PERVAL
          * --- Cambio valuta
          this.w_CAO = GETCAM(this.w_VALESE, i_DATSYS, 0)
          if this.w_CAO<>0
            select przinve
            scan
            this.w_PREZZO = dicosmpa
            this.w_PREZZO = VAL2MON(this.w_PREZZO, this.w_CAO, g_CAOVAL, i_DATSYS, g_PERVAL)
            replace dicosmpa with this.w_PREZZO
            endscan
          else
            ah_ErrorMsg("Impossibile fare il cambio di valuta per l'inventario selezionato","!","")
            this.w_ERRO = .T.
          endif
        endif
        if this.w_ERRO
          this.w_ERRO = .F.
        else
          cField = cField + ", dicosmpp,dicosmpa,dicosult,dicossta"
          cJoin = cJoin +" left outer join przinve ON przinve.cakeysal=ArtDist.cakeysal "
        endif
      endif
    endif
    if this.w_Analisi<>"N" or this.w_TipoValo="X"
      * --- Costo standard
      vq_exec("..\DISB\EXE\QUERY\GSDSSBDC", this, "przstan")
      cField = cField + ",prcossta AS dicossta_ar"
      cJoin = cJoin +" left outer join przstan ON przstan.prcodart=ArtDist.cacodric "
    endif
    * --- Cursore dei costi
    VQ_EXEC("..\DISB\EXE\QUERY\GSDS6BDC", this, "ArtDist")
    SELECT DISTINCT &cField FROM &cJoin ORDER BY cacodric INTO CURSOR przart 
 =WRCURSOR("przart") 
 INDEX ON cacodric TAG cacodric
    Use in Select("ArtDist")
    Use in Select("przulti")
    Use in Select("przinve")
    Use in Select("przstan")
    Use in Select("przultsa")
    * --- Elimino i dati da art_temp
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costo Ultimo e Medio Ponderato per Articoli esterni - derivato da GSMA_BIN
    * --- Variabili locali inizializzate con il valore dei campi dell'anagrafica
    * --- Vengono inizializzate in modo diverso dal solito perche' devono essere letti i valori dei campi
    * --- dell'anagrafica al momento della chiamata del batch. Questo e' necessario perche' nelle righe
    * --- successive viene richiamato l'evento di salvataggio dell'anagrafica che, dopo aver memorizzato
    * --- i records, ripulisce il contenuto dei campi.
    * --- Read from INVENTAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM"+;
        " from "+i_cTable+" INVENTAR where ";
            +"INNUMINV = "+cp_ToStrODBC(this.w_NUMINV);
            +" and INCODESE = "+cp_ToStrODBC(this.w_ESERC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INCODMAG,INMAGCOL,INDATINV,INCATOMO,INGRUMER,INCODFAM;
        from (i_cTable) where;
            INNUMINV = this.w_NUMINV;
            and INCODESE = this.w_ESERC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INCODMAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
      this.w_INMAGCOL = NVL(cp_ToDate(_read_.INMAGCOL),cp_NullValue(_read_.INMAGCOL))
      this.w_PRDATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
      this.w_INCATOMO = NVL(cp_ToDate(_read_.INCATOMO),cp_NullValue(_read_.INCATOMO))
      this.w_INGRUMER = NVL(cp_ToDate(_read_.INGRUMER),cp_NullValue(_read_.INGRUMER))
      this.w_INCODFAM = NVL(cp_ToDate(_read_.INCODFAM),cp_NullValue(_read_.INCODFAM))
      this.w_MAGRAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_INCODESE = this.w_ESERC
    this.w_INESEPRE = this.w_ESERC
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGMAGRAG"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_INCODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGMAGRAG;
        from (i_cTable) where;
            MGCODMAG = this.w_INCODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MAGRAG = NVL(cp_ToDate(_read_.MGMAGRAG),cp_NullValue(_read_.MGMAGRAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MAGRAG = IIF (EMPTY(NVL(this.w_MAGRAG,"")),this.w_INCODMAG,this.w_MAGRAG)
    * --- Flag continuo su OFF
    this.w_ESCONT = this.w_INESCONT
    this.w_DECESE = g_PERPUL
    this.w_CAOPRE = this.w_CAOESE
    * --- Variabili locali
    this.w_FlaVal = ""
    this.w_Codice = ""
    this.w_CriVal = ""
    this.w_CodArt = ""
    this.w_PreQtaEsi = 0
    this.w_CodVar = ""
    this.w_PreQtaVen = 0
    this.w_QtaEsi = 0
    this.w_PreQtaAcq = 0
    this.w_QtaVen = 0
    this.w_PrePrzMpa = 0
    this.w_QtaAcq = 0
    this.w_PreCosMpa = 0
    this.w_PreCosMpp = 0
    this.w_PrzMpa = 0
    this.w_PreCosUlt = 0
    this.w_PrzMpp = 0
    this.w_ElenMag = ""
    this.w_PrzUlt = 0
    this.w_RecElab = 0
    this.w_CosMpa = 0
    this.w_PqtMpp = 0
    this.w_CosMpp = 0
    this.w_QtaScagl = 0
    this.w_CqtMpp = 0
    this.w_CpRowNum = 0
    this.w_CosUlt = 0
    this.w_IsDatMov = cp_CharToDate("  -  -  ")
    this.w_CosSta = 0
    this.w_IsDatFin = cp_CharToDate("  -  -  ")
    this.w_CosLco = 0
    this.w_IsQtaEsi = 0
    this.w_CosLsc = 0
    this.w_IsValUni = 0
    this.w_CosFco = 0
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_UltimoArt = ""
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    this.w_TIPOVALOr = ""
    this.w_ValQta = 0
    this.w_TipMagaz = ""
    this.w_ValTip = 0
    this.w_Segnalato = .F.
    this.w_DatReg = cp_CharToDate("  -  -  ")
    this.w_Trec = 0
    this.w_Tart = 0
    this.w_QtaEsiUno = 0
    this.w_QtaVenUno = 0
    this.w_QtaAcqUno = 0
    this.w_QtaEsiWrite = 0
    this.w_QtaVenWrite = 0
    this.w_QtaAcqWrite = 0
    this.w_PrePreUlt = 0
    * --- Variabili relative al raggruppamento fiscale
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_QtaEsr = 0
    this.w_QtaVer = 0
    this.w_QtaAcr = 0
    this.w_QtaVerWrite = 0
    this.w_QtaAcrWrite = 0
    this.w_CARVAL = 0
    this.w_SCAVAL = 0
    this.w_PreCarVal = 0
    this.w_PreScaVal = 0
    ah_Msg("Calcolo valorizzazioni per analisi di dettaglio in corso...",.T.)
    * --- Creazione cursore articoli compresi in inventario
    create cursor AppMov ;
    (CACODART C(20), MMCODMAG C(5), CMCRIVAL C(2), CMFLAVAL C(1), CMAGGVAL C(2), CMFLCASC C(1), ;
    VALORE N(18,5), MMDATREG D(8),MMCODMAT C(5))
    * --- Queste query servono solo per la determinazione dell'ultimo costo (righe 'Z' del cursore di lavoro)
    * --- ================================
    this.w_NEWREC = Repl("Z", 30)
    this.w_ULTVALPRZ = 0
    this.w_ULTVALCOS = 0
    this.w_COSMPAMOV = 0
    this.w_COSMPPMOV = 0
    * --- Select from GSDSIBIN
    do vq_exec with 'GSDSIBIN',this,'_Curs_GSDSIBIN','',.f.,.t.
    if used('_Curs_GSDSIBIN')
      select _Curs_GSDSIBIN
      locate for 1=1
      do while not(eof())
      * --- Al cambio articolo...
      if this.w_NEWREC <>_Curs_GSDSIBIN.MMCODART
        * --- Messaggio a Video
        ah_msg("Elabora ultimo costo/prezzo articolo: %1",.t.,,, ALLTRIM(_Curs_GSDSIBIN.MMCODART) + chr(13) ) 
        if this.w_ULTVALPRZ<>0
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
        endif
        if this.w_ULTVALCOS<>0
           
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
        endif
        this.w_ULTVALPRZ = 0
        this.w_ULTVALCOS = 0
        * --- Se inventario con calcolo costo medio esercizio per movimento
        *     devo, partendo dall'inventario precedente, calcolare il costo medio
        *     non come somma dei valori di carico / somma delle quantit�
        *     di carico ma movimento per movimento come costo medio attuale per 
        *     esistenza + valore movimento / esistenza + quantit� movimento...
        if this.w_ESCONT="M"
          if this.w_NEWREC<>Repl("Z", 30)
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
             
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
          endif
          this.w_CODART = _Curs_GSDSIBIN.MMCODART
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_COSMPAMOV = this.w_PreCosMpa
          this.w_COSMPPMOV = this.w_PrePrzMpa
          this.w_ESIMPAMOV = this.w_PreQtaEsr
        endif
        this.w_NEWREC = _Curs_GSDSIBIN.MMCODART
      endif
      if _Curs_GSDSIBIN.CMFLAVAL$"V|S" And _Curs_GSDSIBIN.CMFLCASC="-" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATPRZ = _Curs_GSDSIBIN.MMDATREG
        this.w_ULTVALPRZ = _Curs_GSDSIBIN.ValUni
        this.w_ULTMAGPRZ = _Curs_GSDSIBIN.MMCODMAG
        this.w_ULTMATPRZ = NVL( _Curs_GSDSIBIN.MMCODMAT , "     " )
        this.w_ULTAGGPRZ = _Curs_GSDSIBIN.CMAGGVAL
      endif
      if _Curs_GSDSIBIN.CMFLAVAL$"A|C" And _Curs_GSDSIBIN.CMFLCASC="+" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        this.w_ULTDATCOS = _Curs_GSDSIBIN.MMDATREG
        this.w_ULTVALCOS = _Curs_GSDSIBIN.ValUni
        this.w_ULTMAGCOS = _Curs_GSDSIBIN.MMCODMAG
        this.w_ULTMATCOS = NVL( _Curs_GSDSIBIN.MMCODMAT , "     " )
        this.w_ULTAGGCOS = _Curs_GSDSIBIN.CMAGGVAL
      endif
      if this.w_ESCONT="M" And ((_Curs_GSDSIBIN.CMAGGVAL="SN" OR _Curs_GSDSIBIN.CMAGGVAL="NS" OR(_Curs_GSDSIBIN.CMAGGVAL="SS" AND NOT (NVL(_Curs_GSDSIBIN.mmcodmat,"") $ this.w_ElenMag ))))
        * --- Se costo medio esercizio calcolato sui movimenti possono accadere tre cose:
        *     a) � un movimento di carico
        *     b) � un movimento di scarico
        *     c) � un movimento di storno valore
        *     Nel caso a) ricalcolo il costo medio utilizzando il totalizzatore costo ed esistenze
        *     ed i dati sul movimento andando anche a variare il totalizzatore esistenza
        *     Nel caso b) vario solo il totalizzatore Esistenza mentre nel caso c) ridetermino
        *     il totalizzatore del costo medio ponderato con la formula:
        *     
        *     Analogamente per il prezzo...
        *     (totalizzatore costo medio * totalizzatore esistenza - variazione a valore )/esistenza
        do case
          case _Curs_GSDSIBIN.CMFLAVAL$"A|C"
            if _Curs_GSDSIBIN.CMFLCASC$"X"
              * --- Variazione di valore...
              this.w_COSMPAMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPAMOV = (( this.w_COSMPAMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ ( _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPAMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_GSDSIBIN.Esistenza
            endif
          case _Curs_GSDSIBIN.CMFLAVAL$"V|S"
            if _Curs_GSDSIBIN.CMFLCASC$"X"
              this.w_COSMPPMOV = iif( this.w_ESIMPAMOV<>0 , (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ this.w_ESIMPAMOV, 0 )
            else
              if _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV <>0
                this.w_COSMPPMOV = (( this.w_COSMPPMOV * this.w_ESIMPAMOV ) + _Curs_GSDSIBIN.Valore )/ ( _Curs_GSDSIBIN.Esistenza + this.w_ESIMPAMOV )
              else
                this.w_COSMPPMOV = 0
              endif
              this.w_ESIMPAMOV = this.w_ESIMPAMOV + _Curs_GSDSIBIN.Esistenza
            endif
        endcase
      endif
        select _Curs_GSDSIBIN
        continue
      enddo
      use
    endif
    if this.w_ULTVALPRZ<>0 And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGPRZ , this.w_ULTVALPRZ , ; 
 "V", this.w_ULTAGGPRZ , "-", this.w_ULTDATPRZ, this.w_ULTMATPRZ )
    endif
    if this.w_ULTVALCOS<>0 And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , this.w_ULTMAGCOS , this.w_ULTVALCOS , ; 
 "A", this.w_ULTAGGCOS , "+", this.w_ULTDATCOS, this.w_ULTMATCOS)
    endif
    if this.w_ESCONT="M" And this.w_NEWREC<>Repl("Z", 30)
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPAMOV , ; 
 "A", "XX" , "+", i_datsys , Space(5))
       
 INSERT INTO AppMov ; 
 (CACODART, MMCODMAG, VALORE, ; 
 CMFLAVAL, CMAGGVAL ,CMFLCASC, MMDATREG,MMCODMAT) VALUES ; 
 ( this.w_NEWREC , Space(5) , this.w_COSMPPMOV , ; 
 "V", "XX" , "-", i_datsys , Space(5))
    endif
    * --- =======================================
    * --- 1) La select seguente crea piu' records di totali per ogni articolo distinti per Tipo Movimento e Criterio di Valorizzazione.
    *      2) Rispetto alla visual query raggruppa record con le stesse caratteristiche che erano rimasti separati perche' provenienti
    *           da archivi differenti (TipoRiga: A=Articoli, M=Mov.Magazzino, V=Vendite).
    *      3) Per le righe di tipo articolo (TipoRiga=A) la data di registrazione non deve essere considerata.
    vq_exec("..\DISB\EXE\QUERY\GSDS1BIN",this, "elabinve1")
     select cacodart, mmcodmag, "A" as Ordine, ; 
 max(cp_CharToDate("  -  -  ")) as mmdatreg, ; 
 sum(iif(cauprival $ "AC", MagPriCar-MagPriSca, MagPriCar*0)) as Acquisti, ; 
 sum(iif(cauprival $ "VS", MagPriSca-MagPriCar, MagPriSca*0)) as Vendite, ; 
 sum(iif(cauprival $ "AC", Valore, Valore*0)) as ValAcqu, ; 
 sum(iif(cauprival $ "VS", Valore, Valore*0)) as ValVend, ; 
 CauPriVal as CauPriVal, ; 
 CauAggVal as CauAggVal, ; 
 CauPriEsi as CauPriEsi, ; 
 sum(999999999999.99999*0) as Valore , nvl(mmcodmat, space(5)) as mmcodmat, ; 
 sum(iif(cauprival $ "AC", MAGPRICAV-MAGPRISCV, MAGPRICAV*0)) as CARVAL, ; 
 sum(iif(cauprival $ "VS", MAGPRISCV-MAGPRICAV, MAGPRISCV*0)) as SCAVAL, ; 
 LEFT(cacodart+SPACE(20), 20)+"####################" AS cakeysal; 
 from elabinve1 ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi, mmcodmat ; 
 union ; 
 select cacodart, mmcodmag, "Z" as Ordine, ; 
 mmdatreg as mmdatreg, ; 
 0 as Acquisti, ; 
 0 as Vendite, ; 
 0 as ValAcqu, ; 
 0 as ValVend, ; 
 CMFLAVAL as CauPriVal , ; 
 CMAGGVAL as CauAggVal , ; 
 CMFLCASC as CauPriEsi, ; 
 VALORE as Valore , MMCODMAT as mmcodmat, ; 
 0 as CARVAL, ; 
 0 as SCAVAL, ; 
 LEFT(cacodart+SPACE(20), 20)+"####################" AS cakeysal; 
 from AppMov ; 
 group by cacodart, mmcodmag, Ordine, CauPriVal, CauAggVal, CauPriEsi,mmcodmat ; 
 order by 1,2,3 ; 
 into cursor elabinve 
 use in elabinve1 
 use in Appmov
    * --- Imposta il codice dell'ultimo articolo elaborato
    this.w_UltimoArt = "<BOF>"
    * --- Imposta le data fino alle quali sono stati considerati i movimenti di prezzo e costo
    this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
    this.w_DataCosUlt = cp_CharToDate("  -  -  ")
    do while Not Eof("elabinve")
      * --- Lettura dati record corrente
      this.w_Codice = elabinve.cakeysal
      * --- Cambio articolo
      if this.w_Codice <> this.w_UltimoArt
        * --- Messaggio a Video
        * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
        *     ATTENZIONE: la pag.9 viene eseguita sull'art. precedente (ovvero DOPO che sono state eseguite le valorizz. di periodo a pag.5)!!
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Azzeramento totalizzatori articolo
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_QtaEsi = 0
        this.w_PreQtaAcq = 0
        this.w_QtaVen = 0
        this.w_PrePrzMpa = 0
        this.w_QtaAcq = 0
        this.w_PreCosMpa = 0
        this.w_PreCosMpp = 0
        this.w_PrzMpa = 0
        this.w_PreCosUlt = 0
        this.w_PrzMpp = 0
        this.w_ElenMag = ""
        this.w_PrzUlt = 0
        this.w_RecElab = 0
        this.w_CosMpa = 0
        this.w_PqtMpp = 0
        this.w_CosMpp = 0
        this.w_QtaScagl = 0
        this.w_CqtMpp = 0
        this.w_CpRowNum = 0
        this.w_CosUlt = 0
        this.w_IsDatMov = cp_CharToDate("  -  -  ")
        this.w_CosSta = 0
        this.w_IsDatFin = cp_CharToDate("  -  -  ")
        this.w_CosLco = 0
        this.w_IsQtaEsi = 0
        this.w_CosLsc = 0
        this.w_IsValUni = 0
        this.w_CosFco = 0
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_UltimoArt = ""
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_TIPOVALOr = ""
        this.w_ValQta = 0
        this.w_TipMagaz = ""
        this.w_ValTip = 0
        this.w_Segnalato = .F.
        this.w_DatReg = cp_CharToDate("  -  -  ")
        this.w_Trec = 0
        this.w_QtaEsiUno = 0
        this.w_PrePreUlt = 0
        this.w_QtaVenUno = 0
        this.w_PreQtaEsr = 0
        this.w_QtaAcqUno = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_QtaEsiWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaVenWrite = 0
        this.w_QtaVer = 0
        this.w_QtaAcqWrite = 0
        this.w_QtaAcr = 0
        this.w_QtaVerWrite = 0
        this.w_QtaAcrWrite = 0
        this.w_Vendite = 0
        this.w_Acquisti = 0
        this.w_PreQtaEsi = 0
        this.w_PreQtaVen = 0
        this.w_PreQtaAcq = 0
        this.w_PreQtaEsr = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_PrePrzMpa = 0
        this.w_PreCosMpa = 0
        this.w_PreCosMpp = 0
        this.w_PreCosUlt = 0
        this.w_PrePreUlt = 0
        this.w_PrzMpaWrite = 0
        this.w_CosMpaWrite = 0
        this.w_QtaEsr = 0
        this.w_QtaEsi = 0
        this.w_PrzUlt = 0
        this.w_QtaEsiUno = 0
        this.w_QtaVer = 0
        this.w_QtaVerWrite = 0
        this.w_QtaVen = 0
        this.w_CosMpa = 0
        this.w_QtaVenUno = 0
        this.w_QtaAcr = 0
        this.w_QtaAcrWrite = 0
        this.w_QtaAcq = 0
        this.w_CosMpp = 0
        this.w_QtaAcqUno = 0
        this.w_PrzMpa = 0
        this.w_CosUlt = 0
        this.w_PqtMpp = 0
        this.w_PrzMpp = 0
        this.w_CosSta = 0
        this.w_CqtMpp = 0
        * --- Preparo i buffer relativi ad acquisti e vendite, (aggiornati, se del caso, a pag.5) da utilizzare in pag.7
        this.w_Vendite = 0
        this.w_Acquisti = 0
        * --- Memorizza il codice dell'ultimo articolo elaborato
        this.w_UltimoArt = this.w_Codice
        Select Elabinve
        this.w_CodArt = elabinve.cacodart
        this.w_DataPrzUlt = cp_CharToDate("  -  -  ")
        this.w_DataCosUlt = cp_CharToDate("  -  -  ")
        this.w_Segnalato = .F.
        this.w_VarValoAcq = 0
        this.w_VarValoVen = 0
      endif
      * --- Il seguente assegnamento va messo fuori dall'if, altrimenti non viene aggiornato quando cambia il magazzino, ma non cambia l'art
      this.w_Maga = NVL(elabinve.mmcodmag,space(5))
      * --- Estraggo il raggruppamento fiscale del magazzino collegato (quello del maga principale � in w_MAGRAG)
      if NOT EMPTY(NVL(elabinve.mmcodmat,"")) AND((elabinve.mmcodmat $ this.w_ElenMag AND elabinve.CauAggVal="SS" ) OR elabinve.CauAggVal="NN" OR ((NOT elabinve.mmcodmat $ this.w_ElenMag) AND elabinve.CauAggVal="NS" ))
        * --- Aggiorna solo Esistenza
        this.w_QtaEsi = this.w_QtaEsi + (elabinve.Acquisti - elabinve.Vendite)
        this.w_QtaEsr = this.w_QtaEsr + (elabinve.Acquisti - elabinve.Vendite)
        if this.w_Maga=this.w_INCODMAG
          this.w_QtaEsiUno = this.w_QtaEsiUno + (elabinve.Acquisti - elabinve.Vendite)
        endif
      else
        * --- Calcolo Valorizzazioni Periodo
        * --- La procedura richiama questa pagina piu' volte per ogni articolo e calcola tutte le valorizzazioni di periodo.
        select elabinve
        * --- Inizializzazione dei parametri di selezione delle visual query richiamate in questa pagina
        this.w_DatReg = mmdatreg
        * --- Quantita'
        * --- Esistenza
        this.w_QtaEsi = this.w_QtaEsi + (Acquisti - Vendite)
        this.w_QtaEsr = this.w_QtaEsr + (Acquisti - Vendite)
        * --- Quantita' Venduta
        * --- CauPriVal = Combo valore da Aggiornare sulle causali di magazzino
        *     V:  Venduto
        *     S: Altri Scarichi
        *     A: Acquistato
        *     C: Altri Carichi
        *     
        *     CauAggVal = Check Aggiornamento Valori
        *     S: Aggiorna valori 
        *     N: Non aggiorna valori
        *     la prima lettera riguarda il magazzino principale la seconda quello collegato
        *     Es: 
        *     'SS' entrambi i magazzini aggiornano i valori
        *     'SN' il primo magazzino aggiorna, quello collegato No
        if CauPriVal$"V|S" AND Vendite<>0
          this.w_QtaVen = this.w_QtaVen + Vendite
          this.w_QtaVerWrite = this.w_QtaVerWrite + Vendite
          if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
            this.w_QtaVer = this.w_QtaVer + Vendite
            * --- Solo se Altri Scarichi/Venduto e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata),  aggiorno il campo Scarichi valorizzati
            this.w_SCAVAL = this.w_SCAVAL + SCAVAL
          endif
        endif
        * --- Quantita' Acquistata
        if CauPriVal$"A|C" AND Acquisti<>0
          this.w_QtaAcq = this.w_QtaAcq + Acquisti
          this.w_QtaAcrWrite = this.w_QtaAcrWrite + Acquisti
          if CauAggVal="SN" OR CauAggVal="NS" OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
            this.w_QtaAcr = this.w_QtaAcr + Acquisti
            * --- Solo se Altri Carichi/Acquistato e Aggiornamento valori Attivo (pu� essere anche sulla causale collegata), aggiorno il campo Carichi valorizzati
            this.w_CARVAL = this.w_CARVAL + CARVAL
          endif
        endif
        if this.w_Maga=this.w_INCODMAG
          * --- Salvo i dati parziali relativi al solo magazzino specificato
          * --- Esistenza
          this.w_QtaEsiUno = this.w_QtaEsiUno + (Acquisti - Vendite)
          * --- Quantita' Venduta
          if CauPriVal$"V|S" AND Vendite<>0
            this.w_QtaVenUno = this.w_QtaVenUno + Vendite
          endif
          * --- Quantita' Acquistata
          if CauPriVal$"A|C" AND Acquisti<>0
            this.w_QtaAcqUno = this.w_QtaAcqUno + Acquisti
          endif
        endif
        * --- VALORIZZAZIONI
        this.w_VarValoAcq = 0
        this.w_VarValoVen = 0
        * --- La valorizzazione NON viene effettuata solo nel caso in cui
        * --- 1) esista una causale collegata (movimento di trasferimento);
        * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
        * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
        if (CauAggVal="SN" OR CauAggVal="NS") OR(CauAggVal="SS" AND NOT (NVL(mmcodmat,"") $ this.w_ElenMag ))
          * --- Prezzi
          * --- Prezzo Medio Ponderato Periodo
          * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
          * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
          if CauPriVal$"V|S" AND NVL(Vendite,0)<>0
            this.w_PrzMpp = this.w_PrzMpp + (ValVend * IIF(Vendite<0, -1, 1))
            this.w_PqtMpp = this.w_PqtMpp + Vendite
          endif
          * --- Costi
          * --- Costo Medio Ponderato Periodo
          * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
          * --- Considera i movimenti dall'inizio del periodo (se reso diminuisce il Valore)
          if CauPriVal$"A|C" AND NVL(Acquisti,0)<>0
            this.w_CosMpp = this.w_CosMpp + (ValAcqu * IIF(Acquisti<0, -1, 1))
            this.w_CqtMpp = this.w_CqtMpp + Acquisti
          endif
          if nvl(Ordine," ")="Z"
            * --- Calcola Ultimo Prezzo/Ultimo Costo
            * --- Verifica se deve eseguire la valorizzazione a Ult.Prezzo o Ult.Costo o Costo Standard
            if this.w_DatReg>this.w_DataPrzUlt AND CauPriVal$"V|S" AND nvl(Valore, 0)<>0 and CauPriEsi="-"
              * --- Ultimo Prezzo
              this.w_PrzUlt = NVL(Valore, 0)
              this.w_DataPrzUlt = this.w_DatReg
            endif
            if this.w_DatReg>this.w_DataCosUlt AND CauPriVal$"A|C" AND CauPriEsi<>"-" AND nvl(Valore, 0)<>0 and CauPriEsi="+"
              * --- Ultimo Costo
              * --- Considera i movimenti dall'inizio del periodo
              * --- ATTENZIONE! Se e' zero prende quello dell'inventario precedente nella pagina delle valorizzazioni d'esercizio.
              this.w_CosUlt = NVL(Valore, 0)
              this.w_DataCosUlt = this.w_DatReg
            endif
          else
            * --- Bufferizzo i dati relativi ad acquisti e vendite, da utilizzare in pag.7
            this.w_Vendite = elabinve.vendite+this.w_Vendite
            this.w_Acquisti = elabinve.acquisti+this.w_Acquisti
          endif
        endif
      endif
      * --- Passaggio al record successivo
      if Not Eof ("elabinve")
        Select elabinve
        Skip
      endif
    enddo
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select Przinve 
 Append blank
    if used("elabinve")
      use in elabinve
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valori dell'Esercizio di partenza e aggiornamento cursore
    * --- Calcolo Valorizzazioni Esercizio
    * --- La procedura richiama questa pagina una sola volte per ogni articolo e calcola tutte le valorizzazioni d'esercizio.
    * --- Calcolo delle valorizzazioni che considerano i movimenti dall'inizio dell'esercizio
    * --- Variabili relative all'inventario precedente
    this.w_PreQtaEsi = 0
    this.w_PreQtaVen = 0
    this.w_PreQtaAcq = 0
    this.w_PreQtaEsr = 0
    this.w_PreQtaVer = 0
    this.w_PreQtaAcr = 0
    this.w_PrePrzMpa = 0
    this.w_PreCosMpa = 0
    this.w_PreCosMpp = 0
    this.w_PreCosUlt = 0
    this.w_PrePreUlt = 0
    this.w_PrzMpaWrite = 0
    this.w_CosMpaWrite = 0
    this.w_PreCarVal = 0
    this.w_PreScaVal = 0
    if this.w_UltimoArt <> "<BOF>"
      * --- Legge le valorizzazioni dell'inventario precedente
      if NOT EMPTY(this.w_INNUMPRE)
        select przinve
        go top
        if reccount() > 0
          locate for cakeysal=this.w_UltimoArt
          if found()
            this.w_PreQtaEsi = DIQTAESI
            this.w_PreQtaEsr = DIQTAESR
            if this.w_INCODESE=this.w_INESEPRE
              * --- Vengono estratte le qta totali di raggruppamento
              * --- per eseguire correttamente i calcoli a partire da un inventario precedente
              this.w_PreQtaVen = DIQTAVEN
              this.w_PreQtaAcq = DIQTAACQ
              this.w_PreQtaVer = DIQTAVER
              this.w_PreQtaAcr = DIQTAACR
              this.w_PreCarVal = DIQTACVR 
              this.w_PreScaVal = DIQTASVR
            endif
            this.w_PrePrzMpa = DIPRZMPA
            this.w_PreCosMpa = DICOSMPA
            this.w_PreCosMpp = DICOSMPP
            this.w_PreCosUlt = DICOSULT
            this.w_PrePreUlt = DIPRZULT
            if this.w_CAOPRE<>this.w_CAOESE
              * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
              this.w_PrePrzMpa = cp_ROUND(VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosMpa = cp_ROUND(VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosMpp = cp_ROUND(VAL2MON(this.w_PreCosMpp,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PreCosUlt = cp_ROUND(VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
              this.w_PrePreUlt = cp_ROUND(VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV), this.w_DECESE)
            endif
          endif
        endif
        select elabinve
      endif
      * --- Quantita'
      * --- Esistenza
      * --- L'esistenza e' data dalla somma della quantita' presente nel periodo precedente con la differenza fra
      * --- gli acquisti e le vendite del periodo (calcolata nella pagina delle valorizzazioni di periodo).
      this.w_QtaEsi = this.w_PreQtaEsi + this.w_QtaEsi
      this.w_QtaEsr = this.w_PreQtaEsr+this.w_QtaEsr
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaEsiUno = this.w_PreQtaEsi + this.w_QtaEsiUno
      endif
      * --- VALORIZZAZIONI
      * --- Cerco eventuali variazioni a valore
      * --- La valorizzazione NON viene effettuata solo nel caso in cui
      * --- 1) esista una causale collegata (movimento di trasferimento);
      * --- 2) i magazzini appartengano allo stesso raggruppamento fiscale (basta testare questa che � pi� forte)
      * --- (in quel caso non calcolo neanche gli scaglioni)
      * --- 3) Flag Aggiorna Valore associato alla causale di magazzino non attivo
      * --- Prezzi
      * --- Prezzo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      this.w_PrzMpp = iif( empty( this.w_PqtMpp ) , 0 , ( this.w_PrzMpp + this.w_VarValoVen )/ this.w_PqtMpp )
      * --- Prezzo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i prezzi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente corrisponde al valore di periodo.
      if this.w_QtaVer+this.w_PreQtaVer<>0
        * --- w_PqtMpp e w_Qtaver contengono il solito valore...
        this.w_PrzMpa = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaVer)) / (this.w_QtaVer+this.w_PreQtaVer)
      else
        * --- Se la somma delle quantit� � zero due casi
        *     a) entrambe le qt� sono a zero come prezzo medio utilizzo quello
        *     dell'esercizio precedente
        *     b) le quantit� si annullano, prezzo medio a zero...
        if this.w_PreQtaVer=0
          this.w_PrzMpa = this.w_PrePrzMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_PrzMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          * --- Vedi calcolo sopra...
          if this.w_QtaVer+this.w_PreQtaEsr<>0
            this.w_PrzMpaWrite = ((this.w_PrzMpp*this.w_QtaVer) + (this.w_PrePrzMpa*this.w_PreQtaEsr)) / (this.w_QtaVer+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_PrzMpaWrite = this.w_PrePrzMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_PrzMpaWrite = 0
            endif
          endif
        case this.w_ESCONT$"NM"
          this.w_PrzMpaWrite = this.w_PrzMpa
      endcase
      * --- Costi
      * --- Costo Medio Ponderato Periodo
      * --- Adesso che sono stati letti tutti i prezzi e le quantit� dell'articolo � possibile calcolare la media del periodo
      * --- Devo sapere se nel periodo ho avuto movimenti "validi" o meno.
      *     Se li ho avuti allora l'eventuale presenza di variazioni di valore
      *     non vanno ri applicate al dato proveniente dall'esercizio precedente,
      *     altrimenti ( se non ho movimenti validi ) devo applicarle (quindi nel periodo
      *     ho solo la variazione a valore)
      *     - CASO LIMITE NON GESTITO SOLA VARIAZIONE A VALORE 
      *     SENZA SALDI ARTICOLI, L'ARTICOLO NON SAREBBE PRESENTE 
      *     NELL'ELABORAZIONE INVENTARIO E QUINDI NON ENTREREBBE
      *     NELLA SCAN IN CUI SIAMO
      this.w_Old_CosMpp = iif( this.w_CqtMpp=0 , 0 ,this.w_CosMpp )
      this.w_CosMpp = iif( this.w_CqtMpp=0 , 0 , ( this.w_CosMpp + this.w_VarValoAcq ) / this.w_CqtMpp )
      * --- Costo Medio Ponderato Esercizio
      * --- Sommatoria delle quantita' per i costi divisa per la sommatoria delle quantita'.
      * --- Considera i movimenti dall'inizio dell'esercizio
      * --- Se non e' stato specificato l'inventario precedente o non � dello stesso Esercizio corrisponde al valore di periodo.
      if this.w_QtaAcr+this.w_PreCarVal<>0
        * --- Se il valore del periodo � zero devo comunque considerare le variazioni a valore del periodo
        this.w_CosMpa = (iif( this.w_Old_CosMpp=0 , this.w_VarValoAcq , (this.w_CosMpp*this.w_QtaAcr)) + (this.w_PreCosMpa*this.w_PreCarVal)) / (this.w_QtaAcr+this.w_PreCarVal)
      else
        if this.w_PreQtaAcr=0 or (this.w_ESCONT="N" and this.w_PreCosMpa<>0)
          * --- Nel caso in cui nell'inventario di riferimento (stesso esercizio dell'inventario da elaborare)
          *     il tot. carichi sia valorizzato ma � una rettifica d'acquisto (mvprezzo a 0) 
          *     l'inventario da elaborare prende sempre il costo Medio ponderato dell'esercizio precedente
          this.w_CosMpa = this.w_PreCosMpa
        else
          * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
          this.w_CosMpa = 0
        endif
      endif
      * --- Se attivo Flag Eserc Continuo devo sempre considerare l'esercizio precedente
      do case
        case this.w_ESCONT="S"
          if this.w_QtaAcr+ this.w_PreQtaEsr<>0
            this.w_CosMpaWrite = ((this.w_CosMpp*this.w_QtaAcr) + (this.w_PreCosMpa*this.w_PreQtaEsr)) / (this.w_QtaAcr+this.w_PreQtaEsr)
          else
            if this.w_PreQtaEsr=0
              this.w_CosMpaWrite = this.w_PreCosMpa
            else
              * --- Se le quantit� si annullano senza questo default avrei una divisione per zero!
              this.w_CosMpaWrite = 0
            endif
          endif
        case this.w_ESCONT="N"
          this.w_CosMpaWrite = this.w_CosMpa
        case this.w_ESCONT="M"
          * --- Se ho trovato tra i record del temporaneo una entrata con il costo
          *     calcolato sui movimenti lo utilizzo, altrimenti (articolo non movimentato
          *     nel periodo) riporto il prezzo presente nell'eventuale inventario 
          *     precedente, altrimenti 0...
          if w_bMovCosMpaWrite
            this.w_CosMpaWrite = w_MovCosMpaWrite
          else
            this.w_CosMpaWrite = this.w_PreCosMpa
          endif
      endcase
      * --- Ultimo Costo
      * --- Se e' zero l'ultimo costo del periodo prende quello dell'inventario precedente
      if empty( this.w_CosUlt ) .and. (.not. empty(this.w_PreCosUlt))
        this.w_CosUlt = this.w_PreCosUlt
      endif
      * --- Ultimo Prezzo
      * --- Se e' zero l'ultimo prezzo del periodo prende quello dell'inventario precedente
      if empty( this.w_PrzUlt ) .and. (.not. empty(this.w_PrePreUlt))
        this.w_PrzUlt = this.w_PrePreUlt
      endif
      * --- OLD:  IF NOT EMPTY(w_INNUMPRE) AND w_INESEPRE=w_INCODESE
      this.w_QtaAcq = this.w_QtaAcq + this.w_PreQtaAcq
      this.w_QtaVen = this.w_QtaVen + this.w_PreQtaVen
      this.w_QtaAcr = this.w_QtaAcr + this.w_PreQtaAcr
      this.w_QtaVer = this.w_QtaVer + this.w_PreQtaVer
      this.w_QtaAcrWrite = this.w_QtaAcrWrite + this.w_PreQtaAcr
      this.w_QtaVerWrite = this.w_QtaVerWrite + this.w_PreQtaVer
      this.w_CARVAL = this.w_CARVAL + this.w_PreCarVal
      this.w_SCAVAL = this.w_SCAVAL + this.w_PreScaVal
      if this.w_INMAGCOL="N"
        * --- OLD: w_Maga=w_INCODMAG
        this.w_QtaAcqUno = this.w_QtaAcqUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaAcq)
        this.w_QtaVenUno = this.w_QtaVenUno + iif(this.w_INCODESE<>this.w_INESEPRE,0,this.w_PreQtaVen)
      endif
    endif
    * --- Scrittura archivi inventario - GSMA_BIN - PAG6
    if this.w_UltimoArt <> "<BOF>"
      * --- Arrotondamenti in base ai decimali della valuta di conto
      this.w_CosMpaWrite = cp_round( this.w_CosMpaWrite , this.w_DECESE )
      if this.w_TipoValo="P"
        this.w_CosMpaWrite = cp_round( this.w_CosMpp , this.w_DECESE )
      endif
      this.w_CosUlt = cp_round( this.w_CosUlt , this.w_DECESE )
      select przinve
      locate for cakeysal=this.w_UltimoArt
      if found()
        replace dicosmpa with this.w_CosMpaWrite, dicosult with this.w_CosUlt
      else
        Insert into przinve (cakeysal, dicosmpa, dicosult) ; 
 values ; 
 (this.w_UltimoArt, this.w_CosMpaWrite, this.w_CosUlt)
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura dati inventario precedente
    if NOT EMPTY(this.w_INNUMPRE)
      * --- Read from INVEDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVEDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR"+;
          " from "+i_cTable+" INVEDETT where ";
              +"DINUMINV = "+cp_ToStrODBC(this.w_INNUMPRE);
              +" and DICODESE = "+cp_ToStrODBC(this.w_INESEPRE );
              +" and DICODICE = "+cp_ToStrODBC(this.w_CODART );
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIQTAESI,DIQTAESR,DIPRZMPA,DICOSMPA,DICOSULT,DIPRZULT,DIQTAVEN,DIQTAACQ,DIQTAVER,DIQTAACR,DIQTACVR,DIQTASVR;
          from (i_cTable) where;
              DINUMINV = this.w_INNUMPRE;
              and DICODESE = this.w_INESEPRE ;
              and DICODICE = this.w_CODART ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PreQtaEsi = NVL(cp_ToDate(_read_.DIQTAESI),cp_NullValue(_read_.DIQTAESI))
        this.w_PreQtaEsr = NVL(cp_ToDate(_read_.DIQTAESR),cp_NullValue(_read_.DIQTAESR))
        this.w_PrePrzMpa = NVL(cp_ToDate(_read_.DIPRZMPA),cp_NullValue(_read_.DIPRZMPA))
        this.w_PreCosMpa = NVL(cp_ToDate(_read_.DICOSMPA),cp_NullValue(_read_.DICOSMPA))
        this.w_PreCosUlt = NVL(cp_ToDate(_read_.DICOSULT),cp_NullValue(_read_.DICOSULT))
        this.w_PrePreUlt = NVL(cp_ToDate(_read_.DIPRZULT),cp_NullValue(_read_.DIPRZULT))
        this.w_PreQtaVen = NVL(cp_ToDate(_read_.DIQTAVEN),cp_NullValue(_read_.DIQTAVEN))
        this.w_PreQtaAcq = NVL(cp_ToDate(_read_.DIQTAACQ),cp_NullValue(_read_.DIQTAACQ))
        this.w_PreQtaVer = NVL(cp_ToDate(_read_.DIQTAVER),cp_NullValue(_read_.DIQTAVER))
        this.w_PreQtaAcr = NVL(cp_ToDate(_read_.DIQTAACR),cp_NullValue(_read_.DIQTAACR))
        this.w_PreCarVal = NVL(cp_ToDate(_read_.DIQTACVR),cp_NullValue(_read_.DIQTACVR))
        this.w_PreScaVal = NVL(cp_ToDate(_read_.DIQTASVR),cp_NullValue(_read_.DIQTASVR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_INCODESE<>this.w_INESEPRE
        * --- Se esercizi diversi i totalizzatori per esercizio li azzero...
        this.w_PreQtaVen = 0
        this.w_PreQtaAcq = 0
        this.w_PreQtaVer = 0
        this.w_PreQtaAcr = 0
        this.w_PreCarVal = 0
        this.w_PreScaVal = 0
      endif
      if this.w_CAOPRE<>this.w_CAOESE
        * --- Se L'inventario precedente e' di una altra valuta converte i valori alla valuta del nuovo
        this.w_PrePrzMpa = VAL2MON(this.w_PrePrzMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosMpa = VAL2MON(this.w_PreCosMpa,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PreCosUlt = VAL2MON(this.w_PreCosUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
        this.w_PrePreUlt = VAL2MON(this.w_PrePreUlt,this.w_CAOPRE,this.w_CAOESE, this.w_INDATINV, this.w_DECESE)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pKEYRIF,pDATRIF,pTipoValo,pAnalisi,pESERC,pNUMINV,pListino,pAGGMPOND,pLatoServer)
    this.pKEYRIF=pKEYRIF
    this.pDATRIF=pDATRIF
    this.pTipoValo=pTipoValo
    this.pAnalisi=pAnalisi
    this.pESERC=pESERC
    this.pNUMINV=pNUMINV
    this.pListino=pListino
    this.pAGGMPOND=pAGGMPOND
    this.pLatoServer=pLatoServer
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='INVENTAR'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='INVEDETT'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSDSIBIN')
      use in _Curs_GSDSIBIN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pKEYRIF,pDATRIF,pTipoValo,pAnalisi,pESERC,pNUMINV,pListino,pAGGMPOND,pLatoServer"
endproc
