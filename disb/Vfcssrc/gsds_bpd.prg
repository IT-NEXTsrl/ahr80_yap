* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bpd                                                        *
*              Controllo combo modalitÓ varianti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-07                                                      *
* Last revis.: 2004-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bpd",oParentObject)
return(i_retval)

define class tgsds_bpd as StdBatch
  * --- Local variables
  w_TMPC = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato da GSDS_APD.
    *     Utilizzato per dare un messaggio esplicito al cambio del valore del parametro trattato
    * --- Variabili locali
    * --- Messaggio
    if this.oParentObject.w_PDMODVAR="L"
      this.w_TMPC = "Attenzione%0%0Il cambio dell'impostazione attuale (padre supremo) comporterÓ una diversa modalitÓ di gestione delle distinte basi varianti%0Confermi ugualmente?"
    else
      this.w_TMPC = "Attenzione%0%0Il cambio dell'impostazione attuale (padre di livello) comporterÓ una diversa modalitÓ di gestione delle distinte basi varianti%0Confermi ugualmente?"
    endif
    if not ah_YesNo(this.w_TMPC)
      this.oParentObject.w_PDMODVAR = IIF(this.oParentObject.w_PDMODVAR="L", "S", "L")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
