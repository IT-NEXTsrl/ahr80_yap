* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bad                                                        *
*              Sostituzione/variazione associazione distinta/articolo          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-08                                                      *
* Last revis.: 2015-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bad",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsds_bad as StdBatch
  * --- Local variables
  w_KEYRIF = space(10)
  Padre = .NULL.
  w_CURSOR = space(10)
  NC = space(10)
  pAZIONE = space(10)
  w_ROWNUM = 0
  w_ARCODART = space(20)
  w_DBCODICE = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  DISMBASE_idx=0
  ART_TEMP_idx=0
  ART_DIST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato da GSDS_KAD
    this.Padre = this.oParentObject
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAZIONE="INTERROGA"
        * --- Visualizza Zoom
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2 automaticamente
        this.Padre.oPgFrm.ActivePage = 2
      case this.pAZIONE= "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAZIONE="AG"
        this.w_CURSOR = SYS(2015)
        * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
        select distinct arcodart, arcoddis from (this.NC) where xChk=1 into cursor (this.w_CURSOR)
        * --- Try
        local bErr_02BB8FB8
        bErr_02BB8FB8=bTrsErr
        this.Try_02BB8FB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Si sono verificati errori durante l'aggiornamento del database.",48)
        endif
        bTrsErr=bTrsErr or bErr_02BB8FB8
        * --- End
        * --- Visualizza Zoom
        this.Padre.NotifyEvent("Interroga")     
    endcase
  endproc
  proc Try_02BB8FB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_KEYRIF = SYS(2015)
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    USE IN SELECT(this.w_CURSOR)
    if g_PRFA="S"
      * --- Se ho impostato il flag preferenziale in maschera prima storno tutti gli altri
      if this.oParentObject.w_OPERAZ <> "E" AND this.oParentObject.w_DIFLPREF = "S"
        * --- Storno il flag preferenziale da tutte le altre distinte base
        * --- Write into ART_DIST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_DIST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='DICODART'
          cp_CreateTempTable(i_nConn,i_cTempTable,"DISTINCT CACODRIC AS DICODART, 'N' AS DIFLPREF "," from "+i_cQueryTable+" where CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF)+"",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_DIST_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ART_DIST.DICODART = _t2.DICODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DIFLPREF = _t2.DIFLPREF";
              +i_ccchkf;
              +" from "+i_cTable+" ART_DIST, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ART_DIST.DICODART = _t2.DICODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_DIST, "+i_cQueryTable+" _t2 set ";
              +"ART_DIST.DIFLPREF = _t2.DIFLPREF";
              +Iif(Empty(i_ccchkf),"",",ART_DIST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="ART_DIST.DICODART = _t2.DICODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_DIST set ";
              +"DIFLPREF = _t2.DIFLPREF";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DICODART = "+i_cQueryTable+".DICODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DIFLPREF = (select DIFLPREF from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      do case
        case this.oParentObject.w_OPERAZ = "I"
          * --- Insert into ART_DIST
          i_nConn=i_TableProp[this.ART_DIST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\DISB\EXE\QUERY\GSDS3BAD",this.ART_DIST_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        case this.oParentObject.w_OPERAZ = "S"
          * --- Write into ART_DIST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ART_DIST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="DICODART,DICODDIS"
            do vq_exec with 'GSDS4BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_DIST_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ART_DIST.DICODART = _t2.DICODART";
                    +" and "+"ART_DIST.DICODDIS = _t2.DICODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DICODDIS = _t2.DBCODICE";
                +",DITIPDIS = _t2.DITIPDIS";
                +",DIFLPREF = _t2.DIFLPREF";
                +i_ccchkf;
                +" from "+i_cTable+" ART_DIST, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ART_DIST.DICODART = _t2.DICODART";
                    +" and "+"ART_DIST.DICODDIS = _t2.DICODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_DIST, "+i_cQueryTable+" _t2 set ";
                +"ART_DIST.DICODDIS = _t2.DBCODICE";
                +",ART_DIST.DITIPDIS = _t2.DITIPDIS";
                +",ART_DIST.DIFLPREF = _t2.DIFLPREF";
                +Iif(Empty(i_ccchkf),"",",ART_DIST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ART_DIST.DICODART = t2.DICODART";
                    +" and "+"ART_DIST.DICODDIS = t2.DICODDIS";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_DIST set (";
                +"DICODDIS,";
                +"DITIPDIS,";
                +"DIFLPREF";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.DBCODICE,";
                +"t2.DITIPDIS,";
                +"t2.DIFLPREF";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ART_DIST.DICODART = _t2.DICODART";
                    +" and "+"ART_DIST.DICODDIS = _t2.DICODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_DIST set ";
                +"DICODDIS = _t2.DBCODICE";
                +",DITIPDIS = _t2.DITIPDIS";
                +",DIFLPREF = _t2.DIFLPREF";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".DICODART = "+i_cQueryTable+".DICODART";
                    +" and "+i_cTable+".DICODDIS = "+i_cQueryTable+".DICODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DICODDIS = (select DBCODICE from "+i_cQueryTable+" where "+i_cWhere+")";
                +",DITIPDIS = (select DITIPDIS from "+i_cQueryTable+" where "+i_cWhere+")";
                +",DIFLPREF = (select DIFLPREF from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.oParentObject.w_OPERAZ = "E"
          * --- Write into ART_ICOL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ARCODART,ARCODDIS"
            do vq_exec with 'GSDS6BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
                    +" and "+"ART_ICOL.ARCODDIS = _t2.ARCODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
                    +" and "+"ART_ICOL.ARCODDIS = _t2.ARCODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
            +"ART_ICOL.ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ART_ICOL.ARCODART = t2.ARCODART";
                    +" and "+"ART_ICOL.ARCODDIS = t2.ARCODDIS";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set (";
                +"ARCODDIS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
                    +" and "+"ART_ICOL.ARCODDIS = _t2.ARCODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
                    +" and "+i_cTable+".ARCODDIS = "+i_cQueryTable+".ARCODDIS";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Delete from ART_DIST
          i_nConn=i_TableProp[this.ART_DIST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".DICODART = "+i_cQueryTable+".DICODART";
                  +" and "+i_cTable+".DICODDIS = "+i_cQueryTable+".DICODDIS";
          
            do vq_exec with 'GSDS4BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
      endcase
      * --- Se ho messo il flag preferenziale aggiorno la distinta in ARCODDIS tabella ART_ICOL
      if this.oParentObject.w_OPERAZ <> "E" AND this.oParentObject.w_DIFLPREF = "S"
        * --- Storno il flag preferenziale da tutte le altre distinte base
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="ARCODART"
          do vq_exec with 'GSDS5BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARCODDIS = _t2.ARCODDIS";
              +i_ccchkf;
              +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
              +"ART_ICOL.ARCODDIS = _t2.ARCODDIS";
              +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="ART_ICOL.ARCODART = t2.ARCODART";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set (";
              +"ARCODDIS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.ARCODDIS";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
              +"ARCODDIS = _t2.ARCODDIS";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARCODDIS = (select ARCODDIS from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      do case
        case this.oParentObject.w_OPERAZ $ "I-S"
          * --- Write into ART_ICOL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ARCODART"
            do vq_exec with 'GSDS1BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ARCODDIS = _t2.ARCODDIS";
                +i_ccchkf;
                +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
                +"ART_ICOL.ARCODDIS = _t2.ARCODDIS";
                +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ART_ICOL.ARCODART = t2.ARCODART";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set (";
                +"ARCODDIS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.ARCODDIS";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
                +"ARCODDIS = _t2.ARCODDIS";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ARCODDIS = (select ARCODDIS from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.oParentObject.w_OPERAZ = "E"
          * --- Write into ART_ICOL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ARCODART"
            do vq_exec with 'GSDS2BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" from "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL, "+i_cQueryTable+" _t2 set ";
            +"ART_ICOL.ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +Iif(Empty(i_ccchkf),"",",ART_ICOL.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="ART_ICOL.ARCODART = t2.ARCODART";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set (";
                +"ARCODDIS";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="ART_ICOL.ARCODART = _t2.ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ART_ICOL set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ARCODART = "+i_cQueryTable+".ARCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(Null),'ART_ICOL','ARCODDIS');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
    endif
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento completato con successo",64)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ROWNUM = 0
    SELECT (this.w_CURSOR)
    GO TOP
    SCAN
    this.w_ARCODART = NVL(ARCODART, SPACE(20))
    this.w_DBCODICE = NVL(ARCODDIS, SPACE(20))
    if g_PRFA="S"
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DBCODICE),'ART_TEMP','CACODDIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_ARCODART,'CPROWNUM',this.w_ROWNUM,'CACODDIS',this.w_DBCODICE)
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CPROWNUM,CACODDIS &i_ccchkf. );
           values (;
             this.w_KEYRIF;
             ,this.w_ARCODART;
             ,this.w_ROWNUM;
             ,this.w_DBCODICE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_ARCODART,'CPROWNUM',this.w_ROWNUM,'CACODDIS',SPACE(20))
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CPROWNUM,CACODDIS &i_ccchkf. );
           values (;
             this.w_KEYRIF;
             ,this.w_ARCODART;
             ,this.w_ROWNUM;
             ,SPACE(20);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    SELECT (this.w_CURSOR)
    ENDSCAN
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='ART_TEMP'
    this.cWorkTables[4]='ART_DIST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
