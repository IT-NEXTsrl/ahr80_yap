* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_adi                                                        *
*              Impegno componenti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_103]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-28                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_adi"))

* --- Class definition
define class tgsds_adi as StdForm
  Top    = 6
  Left   = 9

  * --- Standard Properties
  Width  = 694
  Height = 366+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=92890217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  DIS_IMPC_IDX = 0
  PIAMPROD_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_DISB_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  cFile = "DIS_IMPC"
  cKeySelect = "DISERIAL"
  cKeyWhere  = "DISERIAL=this.w_DISERIAL"
  cKeyWhereODBC = '"DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cKeyWhereODBCqualified = '"DIS_IMPC.DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cPrg = "gsds_adi"
  cComment = "Impegno componenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DISERIAL = space(10)
  o_DISERIAL = space(10)
  w_DIDATREG = ctod('  /  /  ')
  o_DIDATREG = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_CFUNC = space(10)
  w_READPAR = space(10)
  w_CC1 = space(5)
  w_CC2 = space(5)
  w_CM1 = space(5)
  w_CM2 = space(5)
  w_DIRIFPIA = space(10)
  o_DIRIFPIA = space(10)
  w_NUMREG = 0
  w_ALFREG = space(2)
  w_DATFIL = ctod('  /  /  ')
  w_DIMAXLEV = 0
  w_DIDESSUP = space(40)
  w_DICAUIMP = space(5)
  w_DESC1 = space(35)
  w_DIMAGIMP = space(5)
  w_DICAUORD = space(5)
  w_DESC2 = space(35)
  w_DIMAGORD = space(5)
  w_DITIPVAL = space(1)
  o_DITIPVAL = space(1)
  w_DICODESE = space(4)
  w_DINUMINV = space(6)
  w_DICODLIS = space(5)
  o_DICODLIS = space(5)
  w_DATREG = ctod('  /  /  ')
  w_FL1 = space(1)
  w_FLC1 = space(1)
  w_FL2 = space(1)
  w_FLC2 = space(1)
  w_VALESE = space(3)
  w_VALUTA = space(3)
  w_TIPOLN = space(1)
  w_CAOESE = 0
  w_CAOVAL = 0
  w_MAGINV = space(5)
  w_DICAOLIS = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DATIMP = ctod('  /  /  ')
  w_FLEVAD = space(1)
  w_RESCHK = 0
  w_DECLU = 0
  w_DECLG = 0
  w_DECIU = 0
  w_DECIG = 0
  w_DIRIFORD = space(10)
  w_DIRIFIMP = space(10)
  w_DATCAR = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DISERIAL = this.W_DISERIAL
  * --- Area Manuale = Declare Variables
  * --- gsds_adi
  * ---- Disattiva il metodo Edit (non posso variare)
  proc ecpEdit()
      * ----
  endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_IMPC','gsds_adi')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_adiPag1","gsds_adi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impegno componenti")
      .Pages(1).HelpContextID = 79348612
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='PIAMPROD'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='PAR_DISB'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='INVENTAR'
    this.cWorkTables[9]='DIS_IMPC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_IMPC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_IMPC_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DISERIAL = NVL(DISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_IMPC where DISERIAL=KeySet.DISERIAL
    *
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_IMPC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_IMPC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_IMPC '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR = i_CODAZI
        .w_CC1 = space(5)
        .w_CC2 = space(5)
        .w_CM1 = space(5)
        .w_CM2 = space(5)
        .w_NUMREG = 0
        .w_ALFREG = space(2)
        .w_DESC1 = space(35)
        .w_DESC2 = space(35)
        .w_DATREG = ctod("  /  /  ")
        .w_FL1 = space(1)
        .w_FLC1 = space(1)
        .w_FL2 = space(1)
        .w_FLC2 = space(1)
        .w_VALESE = space(3)
        .w_VALUTA = space(3)
        .w_TIPOLN = space(1)
        .w_CAOESE = 0
        .w_CAOVAL = 0
        .w_MAGINV = space(5)
        .w_OBTEST = i_INIDAT
        .w_DATOBSO = ctod("  /  /  ")
        .w_DATIMP = ctod("  /  /  ")
        .w_FLEVAD = space(1)
        .w_RESCHK = 0
        .w_DECLU = 0
        .w_DECLG = 0
        .w_DECIU = 0
        .w_DECIG = 0
        .w_DATCAR = ctod("  /  /  ")
        .w_DISERIAL = NVL(DISERIAL,space(10))
        .op_DISERIAL = .w_DISERIAL
        .w_DIDATREG = NVL(cp_ToDate(DIDATREG),ctod("  /  /  "))
        .w_CODAZI = i_CODAZI
        .w_CFUNC = this.cFunction
          .link_1_5('Load')
        .w_DIRIFPIA = NVL(DIRIFPIA,space(10))
          if link_1_10_joined
            this.w_DIRIFPIA = NVL(PPSERIAL110,NVL(this.w_DIRIFPIA,space(10)))
            this.w_NUMREG = NVL(PPNUMREG110,0)
            this.w_ALFREG = NVL(PPALFREG110,space(2))
            this.w_DATREG = NVL(cp_ToDate(PPDATREG110),ctod("  /  /  "))
            this.w_FLEVAD = NVL(PPFLEVAD110,space(1))
            this.w_DATIMP = NVL(cp_ToDate(PPDATIMP110),ctod("  /  /  "))
            this.w_DATCAR = NVL(cp_ToDate(PPDATCAR110),ctod("  /  /  "))
          else
          .link_1_10('Load')
          endif
        .w_DATFIL = CP_TODATE(.w_DATREG)
        .w_DIMAXLEV = NVL(DIMAXLEV,0)
        .w_DIDESSUP = NVL(DIDESSUP,space(40))
        .w_DICAUIMP = NVL(DICAUIMP,space(5))
          if link_1_17_joined
            this.w_DICAUIMP = NVL(CMCODICE117,NVL(this.w_DICAUIMP,space(5)))
            this.w_DESC1 = NVL(CMDESCRI117,space(35))
            this.w_FLC1 = NVL(CMFLCLFR117,space(1))
            this.w_FL1 = NVL(CMFLIMPE117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_DIMAGIMP = NVL(DIMAGIMP,space(5))
          * evitabile
          *.link_1_19('Load')
        .w_DICAUORD = NVL(DICAUORD,space(5))
          if link_1_20_joined
            this.w_DICAUORD = NVL(CMCODICE120,NVL(this.w_DICAUORD,space(5)))
            this.w_DESC2 = NVL(CMDESCRI120,space(35))
            this.w_FLC2 = NVL(CMFLCLFR120,space(1))
            this.w_FL2 = NVL(CMFLORDI120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_DIMAGORD = NVL(DIMAGORD,space(5))
          * evitabile
          *.link_1_22('Load')
        .w_DITIPVAL = NVL(DITIPVAL,space(1))
        .w_DICODESE = NVL(DICODESE,space(4))
          .link_1_24('Load')
        .w_DINUMINV = NVL(DINUMINV,space(6))
          .link_1_25('Load')
        .w_DICODLIS = NVL(DICODLIS,space(5))
          if link_1_26_joined
            this.w_DICODLIS = NVL(LSCODLIS126,NVL(this.w_DICODLIS,space(5)))
            this.w_VALUTA = NVL(LSVALLIS126,space(3))
            this.w_TIPOLN = NVL(LSIVALIS126,space(1))
          else
          .link_1_26('Load')
          endif
          .link_1_48('Load')
          .link_1_49('Load')
        .w_DICAOLIS = NVL(DICAOLIS,0)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .w_DIRIFORD = NVL(DIRIFORD,space(10))
        .w_DIRIFIMP = NVL(DIRIFIMP,space(10))
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIS_IMPC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DISERIAL = space(10)
      .w_DIDATREG = ctod("  /  /  ")
      .w_CODAZI = space(5)
      .w_CFUNC = space(10)
      .w_READPAR = space(10)
      .w_CC1 = space(5)
      .w_CC2 = space(5)
      .w_CM1 = space(5)
      .w_CM2 = space(5)
      .w_DIRIFPIA = space(10)
      .w_NUMREG = 0
      .w_ALFREG = space(2)
      .w_DATFIL = ctod("  /  /  ")
      .w_DIMAXLEV = 0
      .w_DIDESSUP = space(40)
      .w_DICAUIMP = space(5)
      .w_DESC1 = space(35)
      .w_DIMAGIMP = space(5)
      .w_DICAUORD = space(5)
      .w_DESC2 = space(35)
      .w_DIMAGORD = space(5)
      .w_DITIPVAL = space(1)
      .w_DICODESE = space(4)
      .w_DINUMINV = space(6)
      .w_DICODLIS = space(5)
      .w_DATREG = ctod("  /  /  ")
      .w_FL1 = space(1)
      .w_FLC1 = space(1)
      .w_FL2 = space(1)
      .w_FLC2 = space(1)
      .w_VALESE = space(3)
      .w_VALUTA = space(3)
      .w_TIPOLN = space(1)
      .w_CAOESE = 0
      .w_CAOVAL = 0
      .w_MAGINV = space(5)
      .w_DICAOLIS = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DATIMP = ctod("  /  /  ")
      .w_FLEVAD = space(1)
      .w_RESCHK = 0
      .w_DECLU = 0
      .w_DECLG = 0
      .w_DECIU = 0
      .w_DECIG = 0
      .w_DIRIFORD = space(10)
      .w_DIRIFIMP = space(10)
      .w_DATCAR = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DIDATREG = i_DATSYS
        .w_CODAZI = i_CODAZI
        .w_CFUNC = this.cFunction
        .w_READPAR = i_CODAZI
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_READPAR))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,10,.f.)
          if not(empty(.w_DIRIFPIA))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,12,.f.)
        .w_DATFIL = CP_TODATE(.w_DATREG)
        .w_DIMAXLEV = 99
          .DoRTCalc(15,15,.f.)
        .w_DICAUIMP = .w_CC1
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_DICAUIMP))
          .link_1_17('Full')
          endif
          .DoRTCalc(17,17,.f.)
        .w_DIMAGIMP = .w_CM1
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_DIMAGIMP))
          .link_1_19('Full')
          endif
        .w_DICAUORD = .w_CC2
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_DICAUORD))
          .link_1_20('Full')
          endif
          .DoRTCalc(20,20,.f.)
        .w_DIMAGORD = .w_CM2
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_DIMAGORD))
          .link_1_22('Full')
          endif
        .w_DITIPVAL = 'N'
        .w_DICODESE = g_CODESE
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_DICODESE))
          .link_1_24('Full')
          endif
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_DINUMINV))
          .link_1_25('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_DICODLIS))
          .link_1_26('Full')
          endif
        .DoRTCalc(26,31,.f.)
          if not(empty(.w_VALESE))
          .link_1_48('Full')
          endif
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_VALUTA))
          .link_1_49('Full')
          endif
          .DoRTCalc(33,36,.f.)
        .w_DICAOLIS = IIF(.w_CAOVAL=0, GETCAM(.w_VALUTA, .w_DIDATREG, 7), .w_CAOVAL)
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
          .DoRTCalc(39,41,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_IMPC')
    this.DoRTCalc(43,49,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDISIMP","i_CODAZI,w_DISERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DISERIAL = .w_DISERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDIDATREG_1_2.enabled = i_bVal
      .Page1.oPag.oNUMREG_1_11.enabled = i_bVal
      .Page1.oPag.oALFREG_1_12.enabled = i_bVal
      .Page1.oPag.oDATFIL_1_13.enabled = i_bVal
      .Page1.oPag.oDIMAXLEV_1_14.enabled = i_bVal
      .Page1.oPag.oDIDESSUP_1_16.enabled = i_bVal
      .Page1.oPag.oDICAUIMP_1_17.enabled = i_bVal
      .Page1.oPag.oDIMAGIMP_1_19.enabled = i_bVal
      .Page1.oPag.oDICAUORD_1_20.enabled = i_bVal
      .Page1.oPag.oDIMAGORD_1_22.enabled = i_bVal
      .Page1.oPag.oDITIPVAL_1_23.enabled = i_bVal
      .Page1.oPag.oDICODESE_1_24.enabled = i_bVal
      .Page1.oPag.oDINUMINV_1_25.enabled = i_bVal
      .Page1.oPag.oDICODLIS_1_26.enabled = i_bVal
      .Page1.oPag.oDICAOLIS_1_57.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_60.enabled = i_bVal
      .Page1.oPag.oBtn_1_61.enabled = .Page1.oPag.oBtn_1_61.mCond()
      .Page1.oPag.oBtn_1_74.enabled = .Page1.oPag.oBtn_1_74.mCond()
      .Page1.oPag.oBtn_1_75.enabled = .Page1.oPag.oBtn_1_75.mCond()
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oDIDATREG_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DIS_IMPC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERIAL,"DISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATREG,"DIDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFPIA,"DIRIFPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAXLEV,"DIMAXLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDESSUP,"DIDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUIMP,"DICAUIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAGIMP,"DIMAGIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUORD,"DICAUORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAGORD,"DIMAGORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPVAL,"DITIPVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODESE,"DICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMINV,"DINUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODLIS,"DICODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAOLIS,"DICAOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFORD,"DIRIFORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFIMP,"DIRIFIMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
    i_lTable = "DIS_IMPC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_IMPC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_IMPC_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDISIMP","i_CODAZI,w_DISERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_IMPC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_IMPC')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_IMPC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DISERIAL,DIDATREG,DIRIFPIA,DIMAXLEV,DIDESSUP"+;
                  ",DICAUIMP,DIMAGIMP,DICAUORD,DIMAGORD,DITIPVAL"+;
                  ",DICODESE,DINUMINV,DICODLIS,DICAOLIS,DIRIFORD"+;
                  ",DIRIFIMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DISERIAL)+;
                  ","+cp_ToStrODBC(this.w_DIDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_DIRIFPIA)+;
                  ","+cp_ToStrODBC(this.w_DIMAXLEV)+;
                  ","+cp_ToStrODBC(this.w_DIDESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_DICAUIMP)+;
                  ","+cp_ToStrODBCNull(this.w_DIMAGIMP)+;
                  ","+cp_ToStrODBCNull(this.w_DICAUORD)+;
                  ","+cp_ToStrODBCNull(this.w_DIMAGORD)+;
                  ","+cp_ToStrODBC(this.w_DITIPVAL)+;
                  ","+cp_ToStrODBCNull(this.w_DICODESE)+;
                  ","+cp_ToStrODBCNull(this.w_DINUMINV)+;
                  ","+cp_ToStrODBCNull(this.w_DICODLIS)+;
                  ","+cp_ToStrODBC(this.w_DICAOLIS)+;
                  ","+cp_ToStrODBC(this.w_DIRIFORD)+;
                  ","+cp_ToStrODBC(this.w_DIRIFIMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_IMPC')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_IMPC')
        cp_CheckDeletedKey(i_cTable,0,'DISERIAL',this.w_DISERIAL)
        INSERT INTO (i_cTable);
              (DISERIAL,DIDATREG,DIRIFPIA,DIMAXLEV,DIDESSUP,DICAUIMP,DIMAGIMP,DICAUORD,DIMAGORD,DITIPVAL,DICODESE,DINUMINV,DICODLIS,DICAOLIS,DIRIFORD,DIRIFIMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DISERIAL;
                  ,this.w_DIDATREG;
                  ,this.w_DIRIFPIA;
                  ,this.w_DIMAXLEV;
                  ,this.w_DIDESSUP;
                  ,this.w_DICAUIMP;
                  ,this.w_DIMAGIMP;
                  ,this.w_DICAUORD;
                  ,this.w_DIMAGORD;
                  ,this.w_DITIPVAL;
                  ,this.w_DICODESE;
                  ,this.w_DINUMINV;
                  ,this.w_DICODLIS;
                  ,this.w_DICAOLIS;
                  ,this.w_DIRIFORD;
                  ,this.w_DIRIFIMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_IMPC_IDX,i_nConn)
      *
      * update DIS_IMPC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_IMPC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DIDATREG="+cp_ToStrODBC(this.w_DIDATREG)+;
             ",DIRIFPIA="+cp_ToStrODBCNull(this.w_DIRIFPIA)+;
             ",DIMAXLEV="+cp_ToStrODBC(this.w_DIMAXLEV)+;
             ",DIDESSUP="+cp_ToStrODBC(this.w_DIDESSUP)+;
             ",DICAUIMP="+cp_ToStrODBCNull(this.w_DICAUIMP)+;
             ",DIMAGIMP="+cp_ToStrODBCNull(this.w_DIMAGIMP)+;
             ",DICAUORD="+cp_ToStrODBCNull(this.w_DICAUORD)+;
             ",DIMAGORD="+cp_ToStrODBCNull(this.w_DIMAGORD)+;
             ",DITIPVAL="+cp_ToStrODBC(this.w_DITIPVAL)+;
             ",DICODESE="+cp_ToStrODBCNull(this.w_DICODESE)+;
             ",DINUMINV="+cp_ToStrODBCNull(this.w_DINUMINV)+;
             ",DICODLIS="+cp_ToStrODBCNull(this.w_DICODLIS)+;
             ",DICAOLIS="+cp_ToStrODBC(this.w_DICAOLIS)+;
             ",DIRIFORD="+cp_ToStrODBC(this.w_DIRIFORD)+;
             ",DIRIFIMP="+cp_ToStrODBC(this.w_DIRIFIMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_IMPC')
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        UPDATE (i_cTable) SET;
              DIDATREG=this.w_DIDATREG;
             ,DIRIFPIA=this.w_DIRIFPIA;
             ,DIMAXLEV=this.w_DIMAXLEV;
             ,DIDESSUP=this.w_DIDESSUP;
             ,DICAUIMP=this.w_DICAUIMP;
             ,DIMAGIMP=this.w_DIMAGIMP;
             ,DICAUORD=this.w_DICAUORD;
             ,DIMAGORD=this.w_DIMAGORD;
             ,DITIPVAL=this.w_DITIPVAL;
             ,DICODESE=this.w_DICODESE;
             ,DINUMINV=this.w_DINUMINV;
             ,DICODLIS=this.w_DICODLIS;
             ,DICAOLIS=this.w_DICAOLIS;
             ,DIRIFORD=this.w_DIRIFORD;
             ,DIRIFIMP=this.w_DIRIFIMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_IMPC_IDX,i_nConn)
      *
      * delete DIS_IMPC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CODAZI = i_CODAZI
        if .o_DISERIAL<>.w_DISERIAL
            .w_CFUNC = this.cFunction
        endif
          .link_1_5('Full')
        .DoRTCalc(6,9,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
            .w_DATFIL = CP_TODATE(.w_DATREG)
        endif
        .DoRTCalc(14,22,.t.)
        if .o_DITIPVAL<>.w_DITIPVAL
          .link_1_24('Full')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_DITIPVAL<>.w_DITIPVAL
          .link_1_26('Full')
        endif
        .DoRTCalc(26,30,.t.)
          .link_1_48('Full')
          .link_1_49('Full')
        .DoRTCalc(33,36,.t.)
        if .o_DIDATREG<>.w_DIDATREG.or. .o_DICODLIS<>.w_DICODLIS
            .w_DICAOLIS = IIF(.w_CAOVAL=0, GETCAM(.w_VALUTA, .w_DIDATREG, 7), .w_CAOVAL)
        endif
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEDISIMP","i_CODAZI,w_DISERIAL")
          .op_DISERIAL = .w_DISERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(38,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMREG_1_11.enabled = this.oPgFrm.Page1.oPag.oNUMREG_1_11.mCond()
    this.oPgFrm.Page1.oPag.oALFREG_1_12.enabled = this.oPgFrm.Page1.oPag.oALFREG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDATFIL_1_13.enabled = this.oPgFrm.Page1.oPag.oDATFIL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDIMAGIMP_1_19.enabled = this.oPgFrm.Page1.oPag.oDIMAGIMP_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDIMAGORD_1_22.enabled = this.oPgFrm.Page1.oPag.oDIMAGORD_1_22.mCond()
    this.oPgFrm.Page1.oPag.oDICODESE_1_24.enabled = this.oPgFrm.Page1.oPag.oDICODESE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oDINUMINV_1_25.enabled = this.oPgFrm.Page1.oPag.oDINUMINV_1_25.mCond()
    this.oPgFrm.Page1.oPag.oDICODLIS_1_26.enabled = this.oPgFrm.Page1.oPag.oDICODLIS_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.enabled = this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDICODESE_1_24.visible=!this.oPgFrm.Page1.oPag.oDICODESE_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDINUMINV_1_25.visible=!this.oPgFrm.Page1.oPag.oDINUMINV_1_25.mHide()
    this.oPgFrm.Page1.oPag.oDICODLIS_1_26.visible=!this.oPgFrm.Page1.oPag.oDICODLIS_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oVALUTA_1_49.visible=!this.oPgFrm.Page1.oPag.oVALUTA_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.visible=!this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_74.visible=!this.oPgFrm.Page1.oPag.oBtn_1_74.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_75.visible=!this.oPgFrm.Page1.oPag.oBtn_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDCAUIMP,PDCAUORD,PDMAGIMP,PDMAGORD";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_READPAR)
            select PDCODAZI,PDCAUIMP,PDCAUORD,PDMAGIMP,PDMAGORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCODAZI,space(10))
      this.w_CC1 = NVL(_Link_.PDCAUIMP,space(5))
      this.w_CC2 = NVL(_Link_.PDCAUORD,space(5))
      this.w_CM1 = NVL(_Link_.PDMAGIMP,space(5))
      this.w_CM2 = NVL(_Link_.PDMAGORD,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_CC1 = space(5)
      this.w_CC2 = space(5)
      this.w_CM1 = space(5)
      this.w_CM2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIRIFPIA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PIAMPROD_IDX,3]
    i_lTable = "PIAMPROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2], .t., this.PIAMPROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIRIFPIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIRIFPIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPSERIAL,PPNUMREG,PPALFREG,PPDATREG,PPFLEVAD,PPDATIMP,PPDATCAR";
                   +" from "+i_cTable+" "+i_lTable+" where PPSERIAL="+cp_ToStrODBC(this.w_DIRIFPIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPSERIAL',this.w_DIRIFPIA)
            select PPSERIAL,PPNUMREG,PPALFREG,PPDATREG,PPFLEVAD,PPDATIMP,PPDATCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIRIFPIA = NVL(_Link_.PPSERIAL,space(10))
      this.w_NUMREG = NVL(_Link_.PPNUMREG,0)
      this.w_ALFREG = NVL(_Link_.PPALFREG,space(2))
      this.w_DATREG = NVL(cp_ToDate(_Link_.PPDATREG),ctod("  /  /  "))
      this.w_FLEVAD = NVL(_Link_.PPFLEVAD,space(1))
      this.w_DATIMP = NVL(cp_ToDate(_Link_.PPDATIMP),ctod("  /  /  "))
      this.w_DATCAR = NVL(cp_ToDate(_Link_.PPDATCAR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIRIFPIA = space(10)
      endif
      this.w_NUMREG = 0
      this.w_ALFREG = space(2)
      this.w_DATREG = ctod("  /  /  ")
      this.w_FLEVAD = space(1)
      this.w_DATIMP = ctod("  /  /  ")
      this.w_DATCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])+'\'+cp_ToStr(_Link_.PPSERIAL,1)
      cp_ShowWarn(i_cKey,this.PIAMPROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIRIFPIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PIAMPROD_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.PPSERIAL as PPSERIAL110"+ ",link_1_10.PPNUMREG as PPNUMREG110"+ ",link_1_10.PPALFREG as PPALFREG110"+ ",link_1_10.PPDATREG as PPDATREG110"+ ",link_1_10.PPFLEVAD as PPFLEVAD110"+ ",link_1_10.PPDATIMP as PPDATIMP110"+ ",link_1_10.PPDATCAR as PPDATCAR110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on DIS_IMPC.DIRIFPIA=link_1_10.PPSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and DIS_IMPC.DIRIFPIA=link_1_10.PPSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICAUIMP
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_DICAUIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_DICAUIMP))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUIMP)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUIMP) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oDICAUIMP_1_17'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_DICAUIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_DICAUIMP)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLIMPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUIMP = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC1 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC1 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL1 = NVL(_Link_.CMFLIMPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUIMP = space(5)
      endif
      this.w_DESC1 = space(35)
      this.w_FLC1 = space(1)
      this.w_FL1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DICAUIMP) OR (.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale impegno componenti inesistente o incongruente")
        endif
        this.w_DICAUIMP = space(5)
        this.w_DESC1 = space(35)
        this.w_FLC1 = space(1)
        this.w_FL1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.CMCODICE as CMCODICE117"+ ",link_1_17.CMDESCRI as CMDESCRI117"+ ",link_1_17.CMFLCLFR as CMFLCLFR117"+ ",link_1_17.CMFLIMPE as CMFLIMPE117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on DIS_IMPC.DICAUIMP=link_1_17.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and DIS_IMPC.DICAUIMP=link_1_17.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIMAGIMP
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIMAGIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DIMAGIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DIMAGIMP))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIMAGIMP)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIMAGIMP) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDIMAGIMP_1_19'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIMAGIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DIMAGIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DIMAGIMP)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIMAGIMP = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DIMAGIMP = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIMAGIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICAUORD
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_DICAUORD)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_DICAUORD))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUORD)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUORD) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oDICAUORD_1_20'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_DICAUORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_DICAUORD)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLORDI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUORD = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC2 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL2 = NVL(_Link_.CMFLORDI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUORD = space(5)
      endif
      this.w_DESC2 = space(35)
      this.w_FLC2 = space(1)
      this.w_FL2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DICAUORD) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale ordine prodotti inesistente o incongruente")
        endif
        this.w_DICAUORD = space(5)
        this.w_DESC2 = space(35)
        this.w_FLC2 = space(1)
        this.w_FL2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.CMCODICE as CMCODICE120"+ ",link_1_20.CMDESCRI as CMDESCRI120"+ ",link_1_20.CMFLCLFR as CMFLCLFR120"+ ",link_1_20.CMFLORDI as CMFLORDI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on DIS_IMPC.DICAUORD=link_1_20.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and DIS_IMPC.DICAUORD=link_1_20.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIMAGORD
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIMAGORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DIMAGORD)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DIMAGORD))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIMAGORD)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIMAGORD) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDIMAGORD_1_22'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIMAGORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DIMAGORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DIMAGORD)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIMAGORD = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DIMAGORD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIMAGORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODESE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_DICODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_DICODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oDICODESE_1_24'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_DICODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DICODESE = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DINUMINV
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DINUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_DINUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_DICODESE;
                     ,'INNUMINV',trim(this.w_DINUMINV))
          select INCODESE,INNUMINV,INCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DINUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DINUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oDINUMINV_1_25'),i_cWhere,'',"Elenco inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DICODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Numero inventario errato o mancante")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DINUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_DINUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_DICODESE;
                       ,'INNUMINV',this.w_DINUMINV)
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DINUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_DICODESE = NVL(_Link_.INCODESE,space(4))
      this.w_MAGINV = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DINUMINV = space(6)
      endif
      this.w_DICODESE = space(4)
      this.w_MAGINV = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DINUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODLIS
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DICODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DICODLIS))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDICODLIS_1_26'),i_cWhere,'GSAR_ALI',"Elenco listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DICODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DICODLIS)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICODLIS = space(5)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.LSCODLIS as LSCODLIS126"+ ",link_1_26.LSVALLIS as LSVALLIS126"+ ",link_1_26.LSIVALIS as LSIVALIS126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on DIS_IMPC.DICODLIS=link_1_26.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and DIS_IMPC.DICODLIS=link_1_26.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECIU = NVL(_Link_.VADECUNI,0)
      this.w_DECIG = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECIU = 0
      this.w_DECIG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VACAOVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECLU = NVL(_Link_.VADECUNI,0)
      this.w_DECLG = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECLU = 0
      this.w_DECLG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDIDATREG_1_2.value==this.w_DIDATREG)
      this.oPgFrm.Page1.oPag.oDIDATREG_1_2.value=this.w_DIDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_11.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_11.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oALFREG_1_12.value==this.w_ALFREG)
      this.oPgFrm.Page1.oPag.oALFREG_1_12.value=this.w_ALFREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIL_1_13.value==this.w_DATFIL)
      this.oPgFrm.Page1.oPag.oDATFIL_1_13.value=this.w_DATFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAXLEV_1_14.value==this.w_DIMAXLEV)
      this.oPgFrm.Page1.oPag.oDIMAXLEV_1_14.value=this.w_DIMAXLEV
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDESSUP_1_16.value==this.w_DIDESSUP)
      this.oPgFrm.Page1.oPag.oDIDESSUP_1_16.value=this.w_DIDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUIMP_1_17.value==this.w_DICAUIMP)
      this.oPgFrm.Page1.oPag.oDICAUIMP_1_17.value=this.w_DICAUIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_18.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_18.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAGIMP_1_19.value==this.w_DIMAGIMP)
      this.oPgFrm.Page1.oPag.oDIMAGIMP_1_19.value=this.w_DIMAGIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUORD_1_20.value==this.w_DICAUORD)
      this.oPgFrm.Page1.oPag.oDICAUORD_1_20.value=this.w_DICAUORD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_21.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_21.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAGORD_1_22.value==this.w_DIMAGORD)
      this.oPgFrm.Page1.oPag.oDIMAGORD_1_22.value=this.w_DIMAGORD
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPVAL_1_23.RadioValue()==this.w_DITIPVAL)
      this.oPgFrm.Page1.oPag.oDITIPVAL_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODESE_1_24.value==this.w_DICODESE)
      this.oPgFrm.Page1.oPag.oDICODESE_1_24.value=this.w_DICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMINV_1_25.value==this.w_DINUMINV)
      this.oPgFrm.Page1.oPag.oDINUMINV_1_25.value=this.w_DINUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODLIS_1_26.value==this.w_DICODLIS)
      this.oPgFrm.Page1.oPag.oDICODLIS_1_26.value=this.w_DICODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_49.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_49.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.value==this.w_DICAOLIS)
      this.oPgFrm.Page1.oPag.oDICAOLIS_1_57.value=this.w_DICAOLIS
    endif
    cp_SetControlsValueExtFlds(this,'DIS_IMPC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DIDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATREG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DIDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DICAUIMP)) or not(EMPTY(.w_DICAUIMP) OR (.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAUIMP_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DICAUIMP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale impegno componenti inesistente o incongruente")
          case   (empty(.w_DIMAGIMP))  and (NOT EMPTY(.w_DICAUIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIMAGIMP_1_19.SetFocus()
            i_bnoObbl = !empty(.w_DIMAGIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DICAUORD) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF'))  and not(empty(.w_DICAUORD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAUORD_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale ordine prodotti inesistente o incongruente")
          case   (empty(.w_DICODESE))  and not(NOT .w_DITIPVAL $ 'SMUP')  and (.w_DITIPVAL $ 'SMUP')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODESE_1_24.SetFocus()
            i_bnoObbl = !empty(.w_DICODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DICAOLIS))  and not(NOT (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS)))  and (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAOLIS_1_57.SetFocus()
            i_bnoObbl = !empty(.w_DICAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsds_adi
      * --- Controlli Finali
      if i_bRes=.t. and .cFunction<>'Query'
         .w_RESCHK=0
          Ah_Msg('Controlli finali...',.T.)
           .NotifyEvent('Controlli')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DISERIAL = this.w_DISERIAL
    this.o_DIDATREG = this.w_DIDATREG
    this.o_DIRIFPIA = this.w_DIRIFPIA
    this.o_DITIPVAL = this.w_DITIPVAL
    this.o_DICODLIS = this.w_DICODLIS
    return

enddefine

* --- Define pages as container
define class tgsds_adiPag1 as StdContainer
  Width  = 690
  height = 366
  stdWidth  = 690
  stdheight = 366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDIDATREG_1_2 as StdField with uid="EQTIIIQIIY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DIDATREG", cQueryName = "DIDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione dell'impegno componenti",;
    HelpContextID = 235132547,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=227, Top=21

  add object oNUMREG_1_11 as StdField with uid="PGRUEHSAMT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 102614486,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=227, Top=49, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  add object oALFREG_1_12 as StdField with uid="QFZWAZWQBX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ALFREG", cQueryName = "ALFREG",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 102583302,;
   bGlobalFont=.t.,;
    Height=21, Width=36, Left=317, Top=49, InputMask=replicate('X',2)

  func oALFREG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  add object oDATFIL_1_13 as StdField with uid="CWHHWSWPVG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATFIL", cQueryName = "DATFIL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 189931830,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=393, Top=49

  func oDATFIL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  add object oDIMAXLEV_1_14 as StdField with uid="XMVJJFLKDX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DIMAXLEV", cQueryName = "DIMAXLEV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo di livelli di esplosione",;
    HelpContextID = 63129204,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=624, Top=49, cSayPict="'99'", cGetPict="'99'"


  add object oBtn_1_15 as StdButton with uid="FNZMXFRUGH",left=475, top=49, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un piano di produzione";
    , HelpContextID = 92689194;
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      vx_exec("..\DISB\Exe\Query\Gsds_adi.vzm",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFUNC<>'Query')
      endwith
    endif
  endfunc

  add object oDIDESSUP_1_16 as StdField with uid="GVDJEHBYVW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DIDESSUP", cQueryName = "DIDESSUP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive allegate ai movimenti di magazzino",;
    HelpContextID = 219141754,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=227, Top=77, InputMask=replicate('X',40)

  add object oDICAUIMP_1_17 as StdField with uid="SGHORRHFKB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DICAUIMP", cQueryName = "DICAUIMP",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale impegno componenti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di impegno componenti",;
    HelpContextID = 151787910,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=227, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_DICAUIMP"

  func oDICAUIMP_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUIMP_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICAUIMP_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oDICAUIMP_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oDICAUIMP_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_DICAUIMP
     i_obj.ecpSave()
  endproc

  add object oDESC1_1_18 as StdField with uid="IHJASYASFK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 36760266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=292, Top=149, InputMask=replicate('X',35)

  add object oDIMAGIMP_1_19 as StdField with uid="CKYQFXUYOM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DIMAGIMP", cQueryName = "DIMAGIMP",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di impegno componenti",;
    HelpContextID = 137148806,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=623, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DIMAGIMP"

  func oDIMAGIMP_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUIMP))
    endwith
   endif
  endfunc

  func oDIMAGIMP_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIMAGIMP_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIMAGIMP_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDIMAGIMP_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oDIMAGIMP_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DIMAGIMP
     i_obj.ecpSave()
  endproc

  add object oDICAUORD_1_20 as StdField with uid="PDSZOLVTKT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DICAUORD", cQueryName = "DICAUORD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale ordine prodotti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di ordine prodotto finito",;
    HelpContextID = 252451194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=227, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_DICAUORD"

  func oDICAUORD_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUORD_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICAUORD_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oDICAUORD_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_APD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oDICAUORD_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_DICAUORD
     i_obj.ecpSave()
  endproc

  add object oDESC2_1_21 as StdField with uid="DOSKLSEMDQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 35711690,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=292, Top=177, InputMask=replicate('X',35)

  add object oDIMAGORD_1_22 as StdField with uid="ZOIIXGWJDR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DIMAGORD", cQueryName = "DIMAGORD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di ordine prodotti",;
    HelpContextID = 237812090,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=623, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DIMAGORD"

  func oDIMAGORD_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUORD))
    endwith
   endif
  endfunc

  func oDIMAGORD_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIMAGORD_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIMAGORD_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDIMAGORD_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oDIMAGORD_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DIMAGORD
     i_obj.ecpSave()
  endproc


  add object oDITIPVAL_1_23 as StdCombo with uid="YRVEYNBLOI",rtseq=22,rtrep=.f.,left=135,top=246,width=201,height=21;
    , ToolTipText = "Criterio di valorizzazione delle distinte";
    , HelpContextID = 96807298;
    , cFormVar="w_DITIPVAL",RowSource=""+"Nessuno,"+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Costo di listino,"+"Ultimo costo standard (articolo),"+"Ultimo costo dei saldi (articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPVAL_1_23.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'M',;
    iif(this.value =4,'P',;
    iif(this.value =5,'U',;
    iif(this.value =6,'L',;
    iif(this.value =7,'X',;
    iif(this.value =8,'A',;
    space(1))))))))))
  endfunc
  func oDITIPVAL_1_23.GetRadio()
    this.Parent.oContained.w_DITIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oDITIPVAL_1_23.SetRadio()
    this.Parent.oContained.w_DITIPVAL=trim(this.Parent.oContained.w_DITIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPVAL=='N',1,;
      iif(this.Parent.oContained.w_DITIPVAL=='S',2,;
      iif(this.Parent.oContained.w_DITIPVAL=='M',3,;
      iif(this.Parent.oContained.w_DITIPVAL=='P',4,;
      iif(this.Parent.oContained.w_DITIPVAL=='U',5,;
      iif(this.Parent.oContained.w_DITIPVAL=='L',6,;
      iif(this.Parent.oContained.w_DITIPVAL=='X',7,;
      iif(this.Parent.oContained.w_DITIPVAL=='A',8,;
      0))))))))
  endfunc

  add object oDICODESE_1_24 as StdField with uid="PPCQXSFUWA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DICODESE", cQueryName = "DICODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 67770747,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=429, Top=246, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_DICODESE"

  func oDICODESE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL $ 'SMUP')
    endwith
   endif
  endfunc

  func oDICODESE_1_24.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  func oDICODESE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
      if .not. empty(.w_DINUMINV)
        bRes2=.link_1_25('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDICODESE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODESE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oDICODESE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDINUMINV_1_25 as StdField with uid="TLVSQNVIAZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DINUMINV", cQueryName = "DINUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Numero inventario errato o mancante",;
    ToolTipText = "Numero dell'inventario da elaborare",;
    HelpContextID = 144755084,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=588, Top=246, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", oKey_1_1="INCODESE", oKey_1_2="this.w_DICODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_DINUMINV"

  func oDINUMINV_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL $ 'SMUP' AND NOT EMPTY(.w_DICODESE))
    endwith
   endif
  endfunc

  func oDINUMINV_1_25.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  func oDINUMINV_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oDINUMINV_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDINUMINV_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_DICODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_DICODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oDINUMINV_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco inventari",'',this.parent.oContained
  endproc

  add object oDICODLIS_1_26 as StdField with uid="AGOZDBGXAR",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DICODLIS", cQueryName = "DICODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 83224183,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=429, Top=246, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DICODLIS"

  func oDICODLIS_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL='L')
    endwith
   endif
  endfunc

  func oDICODLIS_1_26.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L')
    endwith
  endfunc

  func oDICODLIS_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODLIS_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODLIS_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDICODLIS_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Elenco listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oDICODLIS_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_DICODLIS
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_49 as StdField with uid="XFQEBGZCIY",rtseq=32,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 17867350,;
   bGlobalFont=.t.,;
    Height=21, Width=42, Left=588, Top=246, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_49.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L' OR EMPTY(.w_DICODLIS))
    endwith
  endfunc

  func oVALUTA_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDICAOLIS_1_57 as StdField with uid="TYWXLNOQQB",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DICAOLIS", cQueryName = "DICAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 72607351,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=588, Top=279

  func oDICAOLIS_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS))
    endwith
   endif
  endfunc

  func oDICAOLIS_1_57.mHide()
    with this.Parent.oContained
      return (NOT (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS)))
    endwith
  endfunc


  add object oBtn_1_60 as StdButton with uid="QJZVDJKIQR",left=586, top=319, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 92861466;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFUNC='Load')
      endwith
    endif
  endfunc


  add object oBtn_1_61 as StdButton with uid="KZSQCYUNAO",left=637, top=319, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 85572794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_62 as cp_runprogram with uid="XOFORQYQOL",left=13, top=418, width=223,height=19,;
    caption='GSDS_BNU(INIZIO)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNU('INIZIO')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 28073828


  add object oObj_1_63 as cp_runprogram with uid="KDNFFYCJCR",left=254, top=389, width=201,height=19,;
    caption='GSDS_BIC',;
   bGlobalFont=.t.,;
    prg="GSDS_BIC",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 222415959


  add object oObj_1_66 as cp_runprogram with uid="EOVMJGYUSI",left=254, top=418, width=201,height=19,;
    caption='GSDS_BNU(CHECK)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNU('CHECK')",;
    cEvent = "Controlli",;
    nPag=1;
    , HelpContextID = 37222715


  add object oBtn_1_74 as StdButton with uid="KKTWUWGJAF",left=4, top=319, width=48,height=45,;
    CpPicture="bmp\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento di impegno dei componenti";
    , HelpContextID = 257101190;
    , Caption='\<Impegno';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_74.Click()
      with this.Parent.oContained
        GSDS_BNU(this.Parent.oContained,"IMPE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_74.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DIRIFIMP) AND .w_CFUNC<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_74.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DIRIFIMP))
     endwith
    endif
  endfunc


  add object oBtn_1_75 as StdButton with uid="FZPWBPRUQW",left=55, top=319, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento di ordine dei prodotti finiti";
    , HelpContextID = 113660646;
    , Caption='O\<rdine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        GSDS_BNU(this.Parent.oContained,"ORDI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DIRIFORD) AND .w_CFUNC<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_75.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DIRIFORD))
     endwith
    endif
  endfunc


  add object oObj_1_76 as cp_runprogram with uid="TVPDLJWAVK",left=13, top=389, width=223,height=19,;
    caption='GSDS_BNU(ELIMINA)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNU('ELIMINA')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 14433804

  add object oStr_1_28 as StdString with uid="HPFUCYJFQM",Visible=.t., Left=95, Top=49,;
    Alignment=1, Width=131, Height=18,;
    Caption="Piano produzione n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="DPIKMSONLB",Visible=.t., Left=302, Top=49,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="BSUPWQMNDE",Visible=.t., Left=361, Top=49,;
    Alignment=1, Width=29, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="XZARJNGGCG",Visible=.t., Left=95, Top=21,;
    Alignment=1, Width=131, Height=18,;
    Caption="Registrazione del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CYXYCTLBYK",Visible=.t., Left=139, Top=77,;
    Alignment=1, Width=87, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YHNPSTMKVJ",Visible=.t., Left=6, Top=119,;
    Alignment=0, Width=576, Height=18,;
    Caption="Causali impegno componenti/ordine prodotti"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UWMKOEQRMI",Visible=.t., Left=3, Top=149,;
    Alignment=1, Width=223, Height=18,;
    Caption="Impegno componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="DGEYKBIZZG",Visible=.t., Left=96, Top=177,;
    Alignment=1, Width=130, Height=18,;
    Caption="Ordine prodotti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="BLHDULMYIA",Visible=.t., Left=554, Top=149,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="SLGHCCSHGT",Visible=.t., Left=554, Top=177,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="ZNPHNSJTHY",Visible=.t., Left=6, Top=215,;
    Alignment=0, Width=571, Height=18,;
    Caption="Criterio di valorizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="CKOEJCKWIO",Visible=.t., Left=10, Top=246,;
    Alignment=1, Width=124, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="CROCMELOIT",Visible=.t., Left=360, Top=246,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="HJNIKVJEHE",Visible=.t., Left=360, Top=246,;
    Alignment=1, Width=67, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="OJUYXHBXKZ",Visible=.t., Left=539, Top=246,;
    Alignment=1, Width=46, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L' OR EMPTY(.w_DICODLIS))
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="XPYKQQZEJP",Visible=.t., Left=519, Top=246,;
    Alignment=1, Width=66, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="DMKKDPCHQQ",Visible=.t., Left=479, Top=279,;
    Alignment=1, Width=106, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (NOT (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS)))
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="SIYSIOQMRT",Visible=.t., Left=503, Top=49,;
    Alignment=1, Width=117, Height=18,;
    Caption="Numero max livelli:"  ;
  , bGlobalFont=.t.

  add object oBox_1_33 as StdBox with uid="ZHRNEIAIGM",left=6, top=140, width=681,height=2

  add object oBox_1_43 as StdBox with uid="FOKNZYXYPK",left=6, top=236, width=681,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_adi','DIS_IMPC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DISERIAL=DIS_IMPC.DISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
