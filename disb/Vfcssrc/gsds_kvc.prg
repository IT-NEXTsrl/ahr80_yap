* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_kvc                                                        *
*              Sostituzione cicli                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_kvc",oParentObject))

* --- Class definition
define class tgsds_kvc as StdForm
  Top    = 8
  Left   = 8

  * --- Standard Properties
  Width  = 747
  Height = 521+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=48850025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  TABMCICL_IDX = 0
  DISMBASE_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  cPrg = "gsds_kvc"
  cComment = "Sostituzione cicli"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOOPE = space(1)
  o_TIPOOPE = space(1)
  w_CICRIF = space(15)
  w_CICINS = space(15)
  w_STATO = space(1)
  w_DBCODICI = space(20)
  w_DBCODICF = space(20)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_DESRIF = space(40)
  w_DBDESCRI = space(40)
  w_DBDESCRF = space(40)
  w_DESINS = space(40)
  w_SELEZI = space(1)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_DESFAMAI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUI = space(35)
  w_DESGRUF = space(35)
  w_DESCATI = space(35)
  w_DESCATF = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_kvcPag1","gsds_kvc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsds_kvcPag2","gsds_kvc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco distinte")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOOPE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(2).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='TABMCICL'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='GRUMERC'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOOPE=space(1)
      .w_CICRIF=space(15)
      .w_CICINS=space(15)
      .w_STATO=space(1)
      .w_DBCODICI=space(20)
      .w_DBCODICF=space(20)
      .w_DBCODINI=space(20)
      .w_DBCODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_DESRIF=space(40)
      .w_DBDESCRI=space(40)
      .w_DBDESCRF=space(40)
      .w_DESINS=space(40)
      .w_SELEZI=space(1)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_DESFAMAI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUI=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATI=space(35)
      .w_DESCATF=space(35)
      .w_OBTEST=ctod("  /  /  ")
        .w_TIPOOPE = 'I'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CICRIF))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CICINS))
          .link_1_3('Full')
        endif
        .w_STATO = 'T'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DBCODICI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_DBCODICF))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DBCODINI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DBCODFIN))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_GRUINI))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CATINI))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CATFIN))
          .link_1_14('Full')
        endif
      .oPgFrm.Page2.oPag.ZoomSel.Calculate()
          .DoRTCalc(15,18,.f.)
        .w_SELEZI = "D"
      .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
          .DoRTCalc(20,27,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
        if .o_TIPOOPE<>.w_TIPOOPE
          .Calculate_RTVIPFWEXP()
        endif
        .DoRTCalc(1,27,.t.)
            .w_OBTEST = i_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate()
    endwith
  return

  proc Calculate_RTVIPFWEXP()
    with this
          * --- Aggiornamento ciclo da inserire
          .w_CICINS = iif(.w_TIPOOPE<>'S','',.w_CICINS)
          .w_DESINS = iif(.w_TIPOOPE<>'S','',.w_DESINS)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCICRIF_1_2.enabled = this.oPgFrm.Page1.oPag.oCICRIF_1_2.mCond()
    this.oPgFrm.Page1.oPag.oCICINS_1_3.enabled = this.oPgFrm.Page1.oPag.oCICINS_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oCICRIF_1_2
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_5.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CICRIF
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TABMCICL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_CICRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_CICRIF))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICRIF)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CICRIF) and !this.bDontReportError
            deferred_cp_zoom('TABMCICL','*','CSCODICE',cp_AbsName(oSource.parent,'oCICRIF_1_2'),i_cWhere,'',"Elenco cicli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_CICRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_CICRIF)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICRIF = NVL(_Link_.CSCODICE,space(15))
      this.w_DESRIF = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CICRIF = space(15)
      endif
      this.w_DESRIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CICINS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CICINS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TABMCICL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_CICINS)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_CICINS))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CICINS)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CICINS) and !this.bDontReportError
            deferred_cp_zoom('TABMCICL','*','CSCODICE',cp_AbsName(oSource.parent,'oCICINS_1_3'),i_cWhere,'',"Elenco cicli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CICINS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_CICINS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_CICINS)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CICINS = NVL(_Link_.CSCODICE,space(15))
      this.w_DESINS = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CICINS = space(15)
      endif
      this.w_DESINS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CICINS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODICI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODICI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_DBCODICI)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_DBCODICI))
          select DBCODICE,DBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODICI)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODICI) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oDBCODICI_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODICI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODICI)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODICI = NVL(_Link_.DBCODICE,space(20))
      this.w_DBDESCRI = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODICI = space(20)
      endif
      this.w_DBDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODICI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODICF
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODICF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_DBCODICF)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_DBCODICF))
          select DBCODICE,DBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODICF)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODICF) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oDBCODICF_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODICF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODICF)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODICF = NVL(_Link_.DBCODICE,space(20))
      this.w_DBDESCRF = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODICF = space(20)
      endif
      this.w_DBDESCRF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODICF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODINI_1_7'),i_cWhere,'',"CODICI DI RICERCA",'GSDS_KVC.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODFIN_1_8'),i_cWhere,'',"CODICI DI RICERCA",'GSDS_KVC.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_9'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_10'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(_Link_.FADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_11'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_12'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(_Link_.GMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_13'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_14'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(_Link_.OMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOOPE_1_1.RadioValue()==this.w_TIPOOPE)
      this.oPgFrm.Page1.oPag.oTIPOOPE_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCICRIF_1_2.value==this.w_CICRIF)
      this.oPgFrm.Page1.oPag.oCICRIF_1_2.value=this.w_CICRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCICINS_1_3.value==this.w_CICINS)
      this.oPgFrm.Page1.oPag.oCICINS_1_3.value=this.w_CICINS
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_4.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODICI_1_5.value==this.w_DBCODICI)
      this.oPgFrm.Page1.oPag.oDBCODICI_1_5.value=this.w_DBCODICI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODICF_1_6.value==this.w_DBCODICF)
      this.oPgFrm.Page1.oPag.oDBCODICF_1_6.value=this.w_DBCODICF
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODINI_1_7.value==this.w_DBCODINI)
      this.oPgFrm.Page1.oPag.oDBCODINI_1_7.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIN_1_8.value==this.w_DBCODFIN)
      this.oPgFrm.Page1.oPag.oDBCODFIN_1_8.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_9.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_9.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_10.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_10.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_11.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_11.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_12.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_12.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_13.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_13.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_14.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_14.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_17.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_17.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_21.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_21.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRF_1_23.value==this.w_DBDESCRF)
      this.oPgFrm.Page1.oPag.oDBDESCRF_1_23.value=this.w_DBDESCRF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINS_1_29.value==this.w_DESINS)
      this.oPgFrm.Page1.oPag.oDESINS_1_29.value=this.w_DESINS
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_4.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_31.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_31.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_32.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_32.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_33.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_33.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_34.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_34.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_35.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_35.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_36.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_36.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_37.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_37.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_38.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_38.value=this.w_DESCATF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CICRIF) and (.w_TIPOOPE='C'))  and (.w_TIPOOPE<>'I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCICRIF_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CICRIF) or !(.w_TIPOOPE='C')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice ciclo inesistente")
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOOPE = this.w_TIPOOPE
    return

enddefine

* --- Define pages as container
define class tgsds_kvcPag1 as StdContainer
  Width  = 743
  height = 521
  stdWidth  = 743
  stdheight = 521
  resizeXpos=332
  resizeYpos=479
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOOPE_1_1 as StdRadio with uid="RSIMUUVHNV",rtseq=1,rtrep=.f.,left=115, top=10, width=306,height=23;
    , cFormVar="w_TIPOOPE", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPOOPE_1_1.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Inserimento"
      this.Buttons(1).HelpContextID = 39512630
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Inserimento","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Sostituzione"
      this.Buttons(2).HelpContextID = 39512630
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Sostituzione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Cancellazione"
      this.Buttons(3).HelpContextID = 39512630
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Cancellazione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPOOPE_1_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oTIPOOPE_1_1.GetRadio()
    this.Parent.oContained.w_TIPOOPE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOOPE_1_1.SetRadio()
    this.Parent.oContained.w_TIPOOPE=trim(this.Parent.oContained.w_TIPOOPE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOOPE=='I',1,;
      iif(this.Parent.oContained.w_TIPOOPE=='S',2,;
      iif(this.Parent.oContained.w_TIPOOPE=='C',3,;
      0)))
  endfunc

  add object oCICRIF_1_2 as StdField with uid="JUUIXUGDOZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CICRIF", cQueryName = "CICRIF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice ciclo inesistente",;
    ToolTipText = "Codice ciclo di riferimento",;
    HelpContextID = 134407898,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TABMCICL", oKey_1_1="CSCODICE", oKey_1_2="this.w_CICRIF"

  func oCICRIF_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOOPE<>'I')
    endwith
   endif
  endfunc

  func oCICRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  func oCICRIF_1_2.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_TIPOOPE='C'
    endwith
    return i_bres
  endfunc

  proc oCICRIF_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICRIF_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TABMCICL','*','CSCODICE',cp_AbsName(this.parent,'oCICRIF_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco cicli",'',this.parent.oContained
  endproc

  add object oCICINS_1_3 as StdField with uid="HNOOIOPGMH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CICINS", cQueryName = "CICINS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice ciclo inesistente",;
    ToolTipText = "Codice ciclo da sostituire",;
    HelpContextID = 88348966,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=101, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TABMCICL", oKey_1_1="CSCODICE", oKey_1_2="this.w_CICINS"

  func oCICINS_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOOPE<>'C')
    endwith
   endif
  endfunc

  func oCICINS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCICINS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCICINS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TABMCICL','*','CSCODICE',cp_AbsName(this.parent,'oCICINS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco cicli",'',this.parent.oContained
  endproc

  add object oSTATO_1_4 as StdRadio with uid="CYDORZETNB",rtseq=4,rtrep=.f.,left=163, top=162, width=247,height=23;
    , ToolTipText = "Stato distinta";
    , cFormVar="w_STATO", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSTATO_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Provvisorie"
      this.Buttons(1).HelpContextID = 228653786
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Provvisorie","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Confermate"
      this.Buttons(2).HelpContextID = 228653786
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Confermate","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 228653786
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Stato distinta")
      StdRadio::init()
    endproc

  func oSTATO_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSTATO_1_4.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_4.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='P',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='T',3,;
      0)))
  endfunc

  add object oDBCODICI_1_5 as StdField with uid="QFDBXBQHAZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBCODICI", cQueryName = "DBCODICI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base inesistente",;
    ToolTipText = "Codice distinta di inizio selezione",;
    HelpContextID = 178918015,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=162, Top=192, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODICI"

  func oDBCODICI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODICI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODICI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oDBCODICI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDBCODICF_1_6 as StdField with uid="MIRCUHWYAM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DBCODICF", cQueryName = "DBCODICF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base inesistente",;
    ToolTipText = "Codice distinta di fine selezione",;
    HelpContextID = 178918012,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=162, Top=216, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODICF"

  func oDBCODICF_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODICF_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODICF_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oDBCODICF_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDBCODINI_1_7 as StdField with uid="EGVKVVQLEU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Articolo di inizio selezione",;
    HelpContextID = 89517441,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=162, Top=279, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CODICI DI RICERCA",'GSDS_KVC.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDBCODFIN_1_8 as StdField with uid="VCEEWLYQYH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice Articolo di fine selezione",;
    HelpContextID = 139849084,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=162, Top=304, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODFIN"

  proc oDBCODFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_DBCODFIN)
        .w_DBCODFIN = .w_DBCODINI
      endif
    endwith
  endproc

  func oDBCODFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CODICI DI RICERCA",'GSDS_KVC.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_1_9 as StdField with uid="BAUZFWPTWC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Inizio Selezione",;
    HelpContextID = 1265322,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=329, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_10 as StdField with uid="GHDDKOKVIG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Famiglia Articolo di Fine Selezione",;
    HelpContextID = 88297130,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=353, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oGRUINI_1_11 as StdField with uid="VYQYSTJFCH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Inizio Selezione",;
    HelpContextID = 79347098,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=378, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_12 as StdField with uid="BKTBROMCMD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Gruppo Merceologico di Fine Selezione",;
    HelpContextID = 900506,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=401, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_12.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oCATINI_1_13 as StdField with uid="WQNLFQEXDD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Inizio Selezione",;
    HelpContextID = 79355610,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=427, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oCATFIN_1_14 as StdField with uid="WHQQEZYTLE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Categoria Omogenea di Fine Selezione",;
    HelpContextID = 909018,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=162, Top=450, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_14.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc


  add object oBtn_1_15 as StdButton with uid="PPJFABIUST",left=634, top=467, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza lista distinte che soddisfano le selezioni impostate";
    , HelpContextID = 111367421;
    , tabstop=.f., Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSDS_BVC(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="MJNXVZGNOX",left=685, top=467, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41532602;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESRIF_1_17 as StdField with uid="VDRWLZUGCM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione componente da eliminare",;
    HelpContextID = 134343370,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=317, Top=77, InputMask=replicate('X',40)

  add object oDBDESCRI_1_21 as StdField with uid="ZSHZCEUWBR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93332095,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=319, Top=192, InputMask=replicate('X',40)

  add object oDBDESCRF_1_23 as StdField with uid="YEITLXYYOF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DBDESCRF", cQueryName = "DBDESCRF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93332092,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=319, Top=216, InputMask=replicate('X',40)

  add object oDESINS_1_29 as StdField with uid="WXZWPJVPXK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESINS", cQueryName = "DESINS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione componente da eliminare",;
    HelpContextID = 88413494,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=317, Top=101, InputMask=replicate('X',40)

  add object oDESDISI_1_31 as StdField with uid="QSEMNVVIFP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185592522,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=333, Top=279, InputMask=replicate('X',40)

  add object oDESDISF_1_32 as StdField with uid="BLVZKOKGVG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 82842934,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=333, Top=304, InputMask=replicate('X',40)

  add object oDESFAMAI_1_33 as StdField with uid="AOYBYAMKXI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242357631,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=329, InputMask=replicate('X',35)

  add object oDESFAMAF_1_34 as StdField with uid="WOCDSQXKSA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242357628,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=353, InputMask=replicate('X',35)

  add object oDESGRUI_1_35 as StdField with uid="ZABORREMDH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142404298,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=378, InputMask=replicate('X',35)

  add object oDESGRUF_1_36 as StdField with uid="PYGQUHDJMJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126031158,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=401, InputMask=replicate('X',35)

  add object oDESCATI_1_37 as StdField with uid="ZFWYARIWSP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177269450,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=427, InputMask=replicate('X',35)

  add object oDESCATF_1_38 as StdField with uid="SXSGGSHCII",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 91166006,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=218, Top=450, InputMask=replicate('X',35)

  add object oStr_1_18 as StdString with uid="JHRLKDGLYO",Visible=.t., Left=16, Top=77,;
    Alignment=1, Width=140, Height=18,;
    Caption="Ciclo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="AMPUHFHNSW",Visible=.t., Left=17, Top=13,;
    Alignment=1, Width=91, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="TVUMRBNTHK",Visible=.t., Left=86, Top=196,;
    Alignment=1, Width=72, Height=18,;
    Caption="Da distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="RZFKBKKZBH",Visible=.t., Left=86, Top=217,;
    Alignment=1, Width=72, Height=18,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZTNCZZVNMO",Visible=.t., Left=82, Top=165,;
    Alignment=1, Width=74, Height=18,;
    Caption="Stato distinte:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="SQUWQEWLWC",Visible=.t., Left=16, Top=137,;
    Alignment=0, Width=165, Height=18,;
    Caption="Selezione distinte"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BRRBIHDPTI",Visible=.t., Left=17, Top=46,;
    Alignment=0, Width=146, Height=18,;
    Caption="Selezione cicli"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZOYDQQNTSQ",Visible=.t., Left=63, Top=101,;
    Alignment=1, Width=93, Height=18,;
    Caption="Ciclo da inserire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=94, Top=279,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=103, Top=304,;
    Alignment=1, Width=55, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=88, Top=331,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=75, Top=380,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=68, Top=428,;
    Alignment=1, Width=90, Height=18,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="BETNLUNOYI",Visible=.t., Left=93, Top=355,;
    Alignment=1, Width=65, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=93, Top=403,;
    Alignment=1, Width=65, Height=18,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=80, Top=451,;
    Alignment=1, Width=78, Height=18,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PEISJRCHUW",Visible=.t., Left=16, Top=255,;
    Alignment=0, Width=141, Height=16,;
    Caption="Selezioni Articolo"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="FWBDOEHGWZ",left=13, top=154, width=653,height=2

  add object oBox_1_28 as StdBox with uid="THBGJVALIS",left=14, top=63, width=653,height=2

  add object oBox_1_48 as StdBox with uid="BQDUBJBORW",left=13, top=270, width=653,height=1
enddefine
define class tgsds_kvcPag2 as StdContainer
  Width  = 743
  height = 521
  stdWidth  = 743
  stdheight = 521
  resizeXpos=667
  resizeYpos=440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSel as cp_szoombox with uid="AEPLHZWOQV",left=1, top=9, width=740,height=457,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DISMBASE",cZoomFile="GSDS_KVC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 129147622


  add object oBtn_2_2 as StdButton with uid="TYEVPKXNEQ",left=641, top=473, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 48821274;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSDS_BVC(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="BSJPZGRESF",left=691, top=473, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , HelpContextID = 41532602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_2_4 as StdRadio with uid="KCMBJLDROQ",rtseq=19,rtrep=.f.,left=4, top=472, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 67066330
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 67066330
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_4.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_4.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_4.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_2_5 as cp_runprogram with uid="BHANWDYKJR",left=-3, top=549, width=219,height=22,;
    caption='GSDS_BVC',;
   bGlobalFont=.t.,;
    prg="GSDS_BVC('SS')",;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 90059689


  add object oObj_2_6 as cp_runprogram with uid="EBVGCFPGTX",left=223, top=549, width=203,height=22,;
    caption='GSDS_BVC',;
   bGlobalFont=.t.,;
    prg="GSDS_BVC('INTERROGA')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 90059689
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_kvc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
