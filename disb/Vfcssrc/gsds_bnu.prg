* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bnu                                                        *
*              Controlli impegno componenti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_223]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-28                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bnu",oParentObject,m.pOpz)
return(i_retval)

define class tgsds_bnu as StdBatch
  * --- Local variables
  pOpz = space(5)
  w_MMSERIAL = space(10)
  w_MMKEYSAL = space(20)
  w_MMCODMAG = space(5)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMQTAUM1 = 0
  w_PROG = .NULL.
  w_MESS = space(10)
  w_OK = .f.
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_MMCODART = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  PIAMPROD_idx=0
  PIA_PROD_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Controlli in Elaborazione Impegno Componenti (da GSDS_ADI)
    * --- Variabili passate dalla maschera
    this.w_MESS = " "
    this.w_OK = .T.
    do case
      case this.pOpz="CHECK"
        if EMPTY(this.oParentObject.w_DIRIFPIA)
          ah_ErrorMsg("Nessun piano di produzione selezionato",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_FLEVAD="S" AND NOT EMPTY(this.oParentObject.w_DATIMP)
          ah_ErrorMsg("Piano di produzione gi� elaborato (%1)",,"", DTOC(this.oParentObject.w_DATIMP) )
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DICAUIMP)
          ah_ErrorMsg("Nessuna causale magazzino selezionata per impegno componenti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DIMAGIMP)
          ah_ErrorMsg("Nessun magazzino selezionato per impegno componenti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DIMAGORD) AND NOT EMPTY(this.oParentObject.w_DICAUORD)
          ah_ErrorMsg("Nessun magazzino selezionato per ordine prodotti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_DIMAXLEV =0
          ah_ErrorMsg("Attenzione, numero massimo livelli di esplosione non specificato",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        do case
          case this.oParentObject.w_DITIPVAL="L"
            if EMPTY(this.oParentObject.w_DICODLIS)
              ah_ErrorMsg("Nessun listino selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
            if this.oParentObject.w_CAOVAL=0 AND this.oParentObject.w_DICAOLIS=0
              ah_ErrorMsg("Inserire il cambio per la valuta selezionata",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
          case this.oParentObject.w_DITIPVAL $ "SMU"
            if EMPTY(this.oParentObject.w_DICODESE)
              ah_ErrorMsg("Nessun esercizio/inventario selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
            if EMPTY(this.oParentObject.w_DINUMINV)
              ah_ErrorMsg("Nessun inventario selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
        endcase
        if EMPTY(this.oParentObject.w_DICAUORD) AND NOT EMPTY(this.oParentObject.w_DICAUIMP)
          if NOT ah_YesNo("Confermi inserimento dei soli movimenti di impegno componenti?")
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pOpz="IMPE" OR this.pOpz="ORDI"
        this.w_MMSERIAL = IIF(this.pOpz="IMPE", this.oParentObject.w_DIRIFIMP, this.oParentObject.w_DIRIFORD)
        if NOT EMPTY(this.w_MMSERIAL)
          gsar_bzm(this,this.w_MMSERIAL,-10)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOpz="ELIMINA"
        this.w_OK = .T.
        if this.oParentObject.w_FLEVAD="S" AND NOT EMPTY(this.oParentObject.w_DATCAR)
          this.w_MESS = ah_Msgformat("Cancellazione impossibile; occorre prima eliminare la distinta di carico da produzione")
          this.w_OK = .F.
        endif
        if this.w_OK=.T.
          if NOT EMPTY(this.oParentObject.w_DIRIFORD) OR NOT EMPTY(this.oParentObject.w_DIRIFIMP)
            * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
            this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
            if ah_YesNo("Elimino anche i movimenti di magazzino di impegno/ordine associati?")
              if NOT EMPTY(this.oParentObject.w_DIRIFORD)
                ah_Msg("Elimino movimento di ordine prodotti finiti...",.T.)
                * --- Select from MVM_DETT
                i_nConn=i_TableProp[this.MVM_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" MVM_DETT ";
                      +" where MMSERIAL="+cp_ToStrODBC(this.oParentObject.w_DIRIFORD)+"";
                       ,"_Curs_MVM_DETT")
                else
                  select * from (i_cTable);
                   where MMSERIAL=this.oParentObject.w_DIRIFORD;
                    into cursor _Curs_MVM_DETT
                endif
                if used('_Curs_MVM_DETT')
                  select _Curs_MVM_DETT
                  locate for 1=1
                  do while not(eof())
                  if NOT EMPTY(NVL(_Curs_MVM_DETT.MMKEYSAL,"")) AND NOT EMPTY(NVL(_Curs_MVM_DETT.MMCODMAG,"")) AND NVL(_Curs_MVM_DETT.MMQTAUM1,0)<>0
                    this.w_MMKEYSAL = _Curs_MVM_DETT.MMKEYSAL
                    this.w_MMCODART = _Curs_MVM_DETT.MMCODART
                    this.w_MMCODMAG = _Curs_MVM_DETT.MMCODMAG
                    this.w_MMQTAUM1 = _Curs_MVM_DETT.MMQTAUM1
                    this.w_MMFLCASC = NVL(_Curs_MVM_DETT.MMFLCASC," ")
                    this.w_MMFLORDI = NVL(_Curs_MVM_DETT.MMFLORDI, " ")
                    this.w_MMFLIMPE = NVL(_Curs_MVM_DETT.MMFLIMPE, " ")
                    this.w_MMFLRISE = NVL(_Curs_MVM_DETT.MMFLRISE, " ")
                    this.w_MMFLCASC = IIF(this.w_MMFLCASC="+", "-", IIF(this.w_MMFLCASC="-", "+", " "))
                    this.w_MMFLORDI = IIF(this.w_MMFLORDI="+", "-", IIF(this.w_MMFLORDI="-", "+", " "))
                    this.w_MMFLIMPE = IIF(this.w_MMFLIMPE="+", "-", IIF(this.w_MMFLIMPE="-", "+", " "))
                    this.w_MMFLRISE = IIF(this.w_MMFLRISE="+", "-", IIF(this.w_MMFLRISE="-", "+", " "))
                    if NOT EMPTY(this.w_MMFLCASC+this.w_MMFLRISE+this.w_MMFLORDI+this.w_MMFLIMPE)
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                               )
                      else
                        update (i_cTable) set;
                            SLQTAPER = &i_cOp1.;
                            ,SLQTRPER = &i_cOp2.;
                            ,SLQTOPER = &i_cOp3.;
                            ,SLQTIPER = &i_cOp4.;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_MMKEYSAL;
                            and SLCODMAG = this.w_MMCODMAG;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      * --- Read from ART_ICOL
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "ARSALCOM"+;
                          " from "+i_cTable+" ART_ICOL where ";
                              +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          ARSALCOM;
                          from (i_cTable) where;
                              ARCODART = this.w_MMCODART;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_SALCOM="S"
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                          i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTAPER = &i_cOp1.;
                              ,SCQTRPER = &i_cOp2.;
                              ,SCQTOPER = &i_cOp3.;
                              ,SCQTIPER = &i_cOp4.;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_MMKEYSAL;
                              and SCCODMAG = this.w_MMCODMAG;
                              and SCCODCAN = this.w_COMMDEFA;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore Aggiornamento Saldi Commessa'
                          return
                        endif
                      endif
                    endif
                  endif
                    select _Curs_MVM_DETT
                    continue
                  enddo
                  use
                endif
                * --- Delete from MVM_DETT
                i_nConn=i_TableProp[this.MVM_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFORD);
                         )
                else
                  delete from (i_cTable) where;
                        MMSERIAL = this.oParentObject.w_DIRIFORD;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error='Errore in eliminazione movimento di ordine (dettaglio)'
                  return
                endif
                * --- Delete from MVM_MAST
                i_nConn=i_TableProp[this.MVM_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFORD);
                         )
                else
                  delete from (i_cTable) where;
                        MMSERIAL = this.oParentObject.w_DIRIFORD;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error='Errore in eliminazione movimento di ordine (testata)'
                  return
                endif
              endif
              if NOT EMPTY(this.oParentObject.w_DIRIFIMP)
                ah_Msg("Elimino movimento di impegno componenti...",.T.)
                * --- Select from MVM_DETT
                i_nConn=i_TableProp[this.MVM_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" MVM_DETT ";
                      +" where MMSERIAL="+cp_ToStrODBC(this.oParentObject.w_DIRIFIMP)+"";
                       ,"_Curs_MVM_DETT")
                else
                  select * from (i_cTable);
                   where MMSERIAL=this.oParentObject.w_DIRIFIMP;
                    into cursor _Curs_MVM_DETT
                endif
                if used('_Curs_MVM_DETT')
                  select _Curs_MVM_DETT
                  locate for 1=1
                  do while not(eof())
                  if NOT EMPTY(NVL(_Curs_MVM_DETT.MMKEYSAL,"")) AND NOT EMPTY(NVL(_Curs_MVM_DETT.MMCODMAG,"")) AND NVL(_Curs_MVM_DETT.MMQTAUM1,0)<>0
                    this.w_MMKEYSAL = _Curs_MVM_DETT.MMKEYSAL
                    this.w_MMCODART = _Curs_MVM_DETT.MMCODART
                    this.w_MMCODMAG = _Curs_MVM_DETT.MMCODMAG
                    this.w_MMQTAUM1 = _Curs_MVM_DETT.MMQTAUM1
                    this.w_MMFLCASC = NVL(_Curs_MVM_DETT.MMFLCASC," ")
                    this.w_MMFLORDI = NVL(_Curs_MVM_DETT.MMFLORDI, " ")
                    this.w_MMFLIMPE = NVL(_Curs_MVM_DETT.MMFLIMPE, " ")
                    this.w_MMFLRISE = NVL(_Curs_MVM_DETT.MMFLRISE, " ")
                    this.w_MMFLCASC = IIF(this.w_MMFLCASC="+", "-", IIF(this.w_MMFLCASC="-", "+", " "))
                    this.w_MMFLORDI = IIF(this.w_MMFLORDI="+", "-", IIF(this.w_MMFLORDI="-", "+", " "))
                    this.w_MMFLIMPE = IIF(this.w_MMFLIMPE="+", "-", IIF(this.w_MMFLIMPE="-", "+", " "))
                    this.w_MMFLRISE = IIF(this.w_MMFLRISE="+", "-", IIF(this.w_MMFLRISE="-", "+", " "))
                    if NOT EMPTY(this.w_MMFLCASC+this.w_MMFLRISE+this.w_MMFLORDI+this.w_MMFLIMPE)
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                               )
                      else
                        update (i_cTable) set;
                            SLQTAPER = &i_cOp1.;
                            ,SLQTRPER = &i_cOp2.;
                            ,SLQTOPER = &i_cOp3.;
                            ,SLQTIPER = &i_cOp4.;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_MMKEYSAL;
                            and SLCODMAG = this.w_MMCODMAG;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      * --- Read from ART_ICOL
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "ARSALCOM"+;
                          " from "+i_cTable+" ART_ICOL where ";
                              +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          ARSALCOM;
                          from (i_cTable) where;
                              ARCODART = this.w_MMCODART;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if this.w_SALCOM="S"
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                          i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                          i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTAPER = &i_cOp1.;
                              ,SCQTRPER = &i_cOp2.;
                              ,SCQTOPER = &i_cOp3.;
                              ,SCQTIPER = &i_cOp4.;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_MMKEYSAL;
                              and SCCODMAG = this.w_MMCODMAG;
                              and SCCODCAN = this.w_COMMDEFA;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore Aggiornamento Saldi Commessa'
                          return
                        endif
                      endif
                    endif
                  endif
                    select _Curs_MVM_DETT
                    continue
                  enddo
                  use
                endif
                * --- Delete from MVM_DETT
                i_nConn=i_TableProp[this.MVM_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFIMP);
                         )
                else
                  delete from (i_cTable) where;
                        MMSERIAL = this.oParentObject.w_DIRIFIMP;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error='Errore in eiminazione movimento di impegno (dettaglio)'
                  return
                endif
                * --- Delete from MVM_MAST
                i_nConn=i_TableProp[this.MVM_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFIMP);
                         )
                else
                  delete from (i_cTable) where;
                        MMSERIAL = this.oParentObject.w_DIRIFIMP;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error='Errore in eliminazione movimento di impegno (testata)'
                  return
                endif
              endif
            else
              if NOT EMPTY(this.oParentObject.w_DIRIFORD)
                * --- Write into MVM_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MVM_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                      +i_ccchkf ;
                  +" where ";
                      +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFORD);
                         )
                else
                  update (i_cTable) set;
                      MMRIFPRO = SPACE(11);
                      &i_ccchkf. ;
                   where;
                      MMSERIAL = this.oParentObject.w_DIRIFORD;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura movimento di ordine'
                  return
                endif
              endif
              if NOT EMPTY(this.oParentObject.w_DIRIFIMP)
                * --- Write into MVM_MAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MVM_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                      +i_ccchkf ;
                  +" where ";
                      +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFIMP);
                         )
                else
                  update (i_cTable) set;
                      MMRIFPRO = SPACE(11);
                      &i_ccchkf. ;
                   where;
                      MMSERIAL = this.oParentObject.w_DIRIFIMP;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura movimento di impegno'
                  return
                endif
              endif
            endif
          endif
          if this.oParentObject.w_FLEVAD="S"
            * --- Elimina il Riferimento sul Piano di Produzione
            * --- Write into PIAMPROD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PIAMPROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PIAMPROD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PIAMPROD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PPDATIMP ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PIAMPROD','PPDATIMP');
                  +i_ccchkf ;
              +" where ";
                  +"PPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFPIA);
                     )
            else
              update (i_cTable) set;
                  PPDATIMP = cp_CharToDate("  -  -  ");
                  &i_ccchkf. ;
               where;
                  PPSERIAL = this.oParentObject.w_DIRIFPIA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in aggiornamento piano di produzione'
              return
            endif
          endif
        else
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
    endcase
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MVM_DETT'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='PIAMPROD'
    this.cWorkTables[5]='PIA_PROD'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='SALDICOM'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
