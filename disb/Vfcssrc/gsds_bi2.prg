* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bi2                                                        *
*              Verifica formule coefficienti di impiego                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_38]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-27                                                      *
* Last revis.: 2000-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Evento
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bi2",oParentObject,m.Evento)
return(i_retval)

define class tgsds_bi2 as StdBatch
  * --- Local variables
  Evento = space(5)
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_DIFFE = 0
  w_VALORE = 0
  w_CNT = 0
  Func = space(0)
  CODVOC = space(15)
  INDICE = 0
  w_MESS = space(10)
  * --- WorkFile variables
  PAR_VARI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della formula integrata nei Coefficienti di Impiego (da GSDS_ACI)
    Messaggio = "Corretta"
    this.w_MESS = ah_Msgformat("Impossibile salvare sintassi errata")
    this.TmpSave = ALLTRIM(this.oParentObject.w_CIFORMUL)
    * --- Ricerco il segno '<' il quale indica che ho inserito un Parametro Variabile
    this.w_OCCUR = RATC("<",this.TmpSave)
    this.w_OCCUR1 = RATC(">",this.TmpSave)
    this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    this.w_CNT = 0
    do while this.w_OCCUR <> 0
      * --- Ciclo sulla stringa per verificarne la correttezza
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      this.TmpSave1 = this.TmpSave1 + "10"
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      * --- Verifico la correttezza del Parametro Variabile
      this.CODVOC = ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+1,this.w_DIFFE-1))
      * --- Read from PAR_VARI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VARI_idx,2],.t.,this.PAR_VARI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" PAR_VARI where ";
              +"PVCODICE = "+cp_ToStrODBC(this.CODVOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              PVCODICE = this.CODVOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_rows = 0
        Messaggio = "Errata"
      endif
      if Messaggio = "Corretta"
        this.w_CNT = this.w_CNT + 1
      else
        * --- Sostituisco il Parametro Variabile errato mettendolo tra ##
        this.TmpSave1 = SUBSTR(this.oParentObject.w_CIFORMUL,1,this.w_OCCUR)
        this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODVOC) + "#"
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_CIFORMUL,this.w_OCCUR1))
        this.oParentObject.w_CIFORMUL = ALLTRIM(this.TmpSave1)
        this.oParentObject.w_ERRMSG = "T"
        EXIT
      endif
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("<",this.TmpSave)
      this.w_OCCUR1 = RATC(">",this.TmpSave)
      this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    enddo
    if Messaggio = "Corretta"
      this.Func = this.oParentObject.w_CIFORMUL
      this.FuncCalc =  ALLTRIM(this.TmpSave)
      ON ERROR Messaggio = "Errata"
      TmpVal = this.FuncCalc
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      ON ERROR
      if Messaggio="Errata"
        this.oParentObject.w_ERRMSG = "N"
      else
        this.oParentObject.w_ERRMSG = "S"
      endif
    endif
    if Messaggio<>"Errata" AND this.w_CNT>8
      Messaggio = "Errata"
      this.w_MESS = ah_Msgformat("Inserire al massimo 8 parametri variabili")
    endif
    WAIT CLEAR
    if Messaggio = "Errata" AND this.Evento = "Save"
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,Evento)
    this.Evento=Evento
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_VARI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Evento"
endproc
