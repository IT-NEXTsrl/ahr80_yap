* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bsd                                                        *
*              Batch stampa distinta base                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_111]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2011-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bsd",oParentObject)
return(i_retval)

define class tgsds_bsd as StdBatch
  * --- Local variables
  w_REP = space(50)
  w_QRY = space(50)
  w_DATFIL = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_TIPCOS = space(1)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_CODART = space(20)
  w_CODART = space(20)
  w_DISPAD = space(20)
  w_ORDINE = space(1)
  w_NUMLEV = 0
  w_NUMERO = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_QTARIG = 0
  w_COEIMP = space(15)
  w_PERSCA = 0
  w_RECSCA = 0
  w_PERSFR = 0
  w_RECSFR = 0
  w_PERRIC = 0
  w_FLVARI = space(1)
  w_FLESPL = space(1)
  w_RIFFAS = 0
  w_CODVAR = space(20)
  w_DB__NOTE = space(0)
  w_OK = 0
  w_FIELDCOUNT = 0
  w_TmpC = space(20)
  w_FIELDS = space(250)
  w_LOOP = 0
  * --- WorkFile variables
  KEY_ARTI_idx=0
  PAR_DISB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Stampa Distinte base (da GSDS_SDS)
    * --- Variabili passate dalla maschera
    DIMENSION QTC[99]
    if this.oParentObject.w_CODFIN<this.oParentObject.w_CODINI AND NOT EMPTY(this.oParentObject.w_CODFIN)
      ah_ErrorMsg("Codice finale minore del codice iniziale",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_OQRY) OR EMPTY(this.oParentObject.w_OREP)
      ah_ErrorMsg("Query/report di stampa non definiti",,"")
      i_retcode = 'stop'
      return
    endif
    L_DATSTA=this.oParentObject.w_DATSTA
    L_CODINI=this.oParentObject.w_CODINI
    L_CODFIN=this.oParentObject.w_CODFIN
    L_FLPROV=this.oParentObject.w_FLPROV
    L_FLCICL=this.oParentObject.w_FLCICL
    L_FLUM1=this.oParentObject.w_FLUM1
    L_NOESPL= this.oParentObject.w_NOESPL
    L_FLDESCRI=this.oParentObject.w_FLDESCRI
    this.w_QRY = ALLTRIM(this.oParentObject.w_OQRY)
    this.w_REP = ALLTRIM(this.oParentObject.w_OREP)
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2],.t.,this.PAR_DISB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDCOSPAR"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDCOSPAR;
        from (i_cTable) where;
            PDCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPCOS = NVL(cp_ToDate(_read_.PDCOSPAR),cp_NullValue(_read_.PDCOSPAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.oParentObject.w_ONUME=4 OR this.oParentObject.w_ONUME=5
        * --- Parametri per Batch di Esplosione (Indentata / Sommarizzata)
        this.w_DBCODINI = this.oParentObject.w_CODINI
        this.w_DBCODFIN = IIF(EMPTY(this.oParentObject.w_CODFIN), REPL("z", 20), this.oParentObject.w_CODFIN)
        this.w_MAXLEVEL = IIF(this.w_TIPCOS="S", 99, 999)
        this.w_VERIFICA = "S C"
        this.w_FILSTAT = this.oParentObject.w_FLPROV
        this.w_DATFIL = this.oParentObject.w_DATSTA
        if used("TES_PLOS")
          select TES_PLOS
          use
        endif
        gsar_bde(this,"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if used("TES_PLOS")
          * --- Crea Cursore di Appoggio
           
 CREATE CURSOR __TMP__ ; 
 (CODART C(20), DISPAD C(20), ORDINE C(1), NUMERO N(5,0), CODCOM C(40), NUMLEV C(4), DESCOM C(40), UNIMIS C(3), QTAMOV N(15,6), ; 
 COEIMP C(15), PERSCA N(6,2), RECSCA N(6,2), PERSFR N(6,2), RECSFR N(6,2), PERRIC N(6,2), FLVARI C(1), DESPAD C(40), FLESPL C(1), RIFFAS N(4,0), DB__NOTE M)
           
 SELECT TES_PLOS 
 GO TOP
          this.w_CODART = CODART
          this.w_DISPAD = DISPAD
          this.w_NUMERO = 0
          SCAN FOR NOT EMPTY(DISPAD)
          this.w_CODART = CODART
          this.w_CODVAR = CODCOM
          this.w_ORDINE = "0"
          this.w_NUMLEV = NUMLEV
          this.w_DESCOM = DESCOM
          * --- Rileggo la descrizione dei codici di ricerca nel caso abbia attivato il flag sulla maschera
          if this.oParentObject.w_FLDESART="S"
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESART"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CODVAR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESART;
                from (i_cTable) where;
                    CACODICE = this.w_CODVAR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCOM = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_UNIMIS = IIF(this.oParentObject.w_FLUM1="S", UNIMIS, UNMIS0)
          this.w_QTAMOV = QTAMOV
          this.w_QTAUM1 = QTAUM1
          this.w_QTARIG = IIF(this.oParentObject.w_FLUM1="S", this.w_QTAUM1, this.w_QTAMOV)
          this.w_COEIMP = COEIMP
          this.w_PERSCA = PERSCA
          this.w_RECSCA = RECSCA
          this.w_PERSFR = PERSFR
          this.w_RECSFR = RECSFR
          this.w_PERRIC = PERRIC
          this.w_FLVARI = FLVARI
          this.w_FLESPL = FLESPL
          this.w_RIFFAS = RIFFAS
          this.w_DB__NOTE = MVDESSUP
          if VAL(this.w_NUMLEV)=0
            this.w_DISPAD = DISPAD
            this.w_CODCOM = ALLTRIM(DISPAD)
            this.w_NUMERO = 0
            * --- Svuto l'array..
            QTC = 0
          else
            this.w_NUMERO = this.w_NUMERO + 1
            if this.oParentObject.w_ONUME=4
              this.w_CODCOM = ALLTRIM(STR(VAL(this.w_NUMLEV)))+" " + ALLTRIM(CODCOM)
              this.w_CODCOM = REPL(". ", IIF(VAL(this.w_NUMLEV)<10, VAL(this.w_NUMLEV)-1, 9))+this.w_CODCOM
            else
              * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
              QTC[VAL(this.w_NUMLEV)] = this.w_QTAUM1
              this.w_QTARIG = 1
              FOR L_i = 1 TO VAL(this.w_NUMLEV)
              * --- Solo la riga del componente viene moltiplicato per l'effettivo valore, gli altri calcoli vanno riportati alla 1^UM
              this.w_QTARIG = this.w_QTARIG * IIF(L_i=VAL(this.w_NUMLEV) AND this.oParentObject.w_FLUM1<>"S", this.w_QTAMOV, QTC[L_i])
              ENDFOR
              this.w_CODCOM = CODCOM
            endif
          endif
           
 INSERT INTO __TMP__ ; 
 (CODART, DISPAD, ORDINE, NUMERO, CODCOM, NUMLEV, DESCOM, UNIMIS, QTAMOV, COEIMP, ; 
 PERSCA, RECSCA, PERSFR, RECSFR, PERRIC, FLVARI, FLESPL, RIFFAS, DB__NOTE) ; 
 VALUES (this.w_CODART, this.w_DISPAD, this.w_ORDINE, this.w_NUMERO, this.w_CODCOM, this.w_NUMLEV, this.w_DESCOM, this.w_UNIMIS, ; 
 this.w_QTARIG, this.w_COEIMP, this.w_PERSCA, this.w_RECSCA, this.w_PERSFR, this.w_RECSFR, this.w_PERRIC, this.w_FLVARI, this.w_FLESPL, this.w_RIFFAS, this.w_DB__NOTE)
          SELECT TES_PLOS
          ENDSCAN
          select TES_PLOS
          use
          if this.oParentObject.w_FLCICL="S" AND this.oParentObject.w_ONUME=4
            vq_exec(this.w_QRY, this,"TES_PLOS")
            if USED("TES_PLOS")
              select TES_PLOS
              GO TOP
              SCAN FOR NOT EMPTY(NVL(DISPAD, ""))
              this.w_CODART = CODART
              this.w_DISPAD = DISPAD
              this.w_ORDINE = "1"
              this.w_NUMERO = NVL(NUMERO, 0)
              this.w_CODCOM = NVL(CODCOM," ")
              this.w_DESCOM = NVL(DESCOM, " ")
              this.w_UNIMIS = NVL(UNIMIS, "  ")
              this.w_QTAMOV = NVL(QTAMOV, 0)
              this.w_COEIMP = NVL(DESPAD, " ")
              this.w_FLESPL = NVL(CS_SETUP, "  ")
               
 INSERT INTO __TMP__ ; 
 (CODART, DISPAD, ORDINE, NUMERO, CODCOM, DESCOM, UNIMIS, QTAMOV, DESPAD, FLESPL) ; 
 VALUES (this.w_CODART, this.w_DISPAD, this.w_ORDINE, this.w_NUMERO, this.w_CODCOM, this.w_DESCOM, this.w_UNIMIS, this.w_QTAMOV, this.w_COEIMP, this.w_FLESPL)
              SELECT TES_PLOS
              ENDSCAN
              select TES_PLOS
              use
            endif
          endif
        endif
        if USED("__TMP__")
          * --- Se esiste tmp
          SELECT __TMP__ 
          if RECCOUNT()>0
            * --- Recupero le unit� di misura non frazionabili per arrotondare all'intero superiore
            *     le quantit� associata a tali unit� di misura
            vq_exec("QUERY\GSAR_UMI.VQR", this,"UMNOFR")
            if this.oParentObject.w_ONUME=5
              if this.oParentObject.w_NOESPL="S"
                * --- Considera solo le 'foglie'
                GO TOP
                DELETE FROM __TMP__ WHERE FLESPL="S" AND VAL(NUMLEV)<>0
              endif
              * --- Sommarizzata, Raggruppa per Componente e arrotonda le qt� se unti� di
              *     misura frazionabile
               
 Select CODART, DISPAD, IIF(VAL(NUMLEV)=0,"0000","0001") AS NUMLEV, CODCOM, Min("0") AS ORDINE, MIn(0) AS NUMERO, ; 
 MAX(DESCOM) AS DESCOM, UNIMIS, SUM( IIF(Not Isnull(UMCODICE) , cp_ROUND(QTAMOV+.499, 0), QTAMOV ))*1.00000 AS QTAMOV, MAX(DESPAD) AS DESPAD ; 
 From __TMP__ Left Outer Join UMNOFR On Unimis=UMCODICE Group By 1, 2, 3, 4, 8 Order By 1, 2, 3, 4, 8 Into Cursor __TMP__ NoFilter
            else
              * --- Determino l'elenco dei campi per ricalcolare QTAMOV e arrotodarla se
              *     unit� di misura frazionabile
              Select __Tmp__
              this.w_OK = 0
              this.w_FIELDS = ""
              this.w_FIELDCOUNT = AFIELDS(gaMyArray)
              this.w_LOOP = 1
              do while this.w_FIELDCOUNT>=this.w_LOOP
                if Upper ( gaMyArray[ this.w_LOOP , 1] ) ="QTAMOV"
                  this.w_TmpC = "IIF(Not Isnull(UMCODICE) , cp_ROUND(QTAMOV+.499, 0), QTAMOV )*1.00000 As  QTAMOV"
                  this.w_OK = this.w_OK + 1
                else
                  this.w_TmpC = gaMyArray[ this.w_LOOP , 1]
                endif
                * --- La query in Output utente deve contere due campi QTAMOV e UNIMIS altrimenti
                *     segnalo ed esco...
                if Upper ( gaMyArray[ this.w_LOOP , 1] ) ="UNIMIS" 
                  this.w_OK = this.w_OK + 1
                endif
                if Not Empty( this.w_FIELDS )
                  this.w_FIELDS = this.w_FIELDS + " , "+this.w_TmpC
                else
                  this.w_FIELDS = this.w_TmpC
                endif
                this.w_LOOP = this.w_LOOP + 1
              enddo
              if this.w_OK=2
                 
 L_Fields = this.w_FIELDS 
 SELECT &L_Fields FROM __TMP__ Left Outer Join UMNOFR On UNIMIS=UMCODICE ORDER BY 1, 2, 3, 4 INTO CURSOR __TMP__
              else
                ah_ErrorMsg("La query in output utente deve contenere i campi qtamov e unimis. La stampa non pu� essere portata a termine",,"")
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                i_retcode = 'stop'
                return
              endif
              if this.oParentObject.w_FLDESCRI="S" and this.w_OK=2
                SELECT CODART, DISPAD, "2" AS ORDINE, NUMERO, CODCOM, NUMLEV, DESCOM, UNIMIS, QTAMOV, COEIMP,; 
 PERSCA, RECSCA, PERSFR, RECSFR, PERRIC, FLVARI, DESPAD, FLESPL, RIFFAS, DB__NOTE FROM __TMP__ ; 
 INTO CURSOR __TMP1__ WHERE VAL(NUMLEV)=0 AND ORDINE="0"
                SELECT * FROM __TMP__ ORDER BY 1, 2, 3, 4 INTO CURSOR __TMP__ ; 
 UNION ALL ; 
 SELECT * FROM __TMP1__
              endif
            endif
          endif
          CP_CHPRN(this.w_REP, " ", this)
        endif
      otherwise
        * --- Altre Stampe
        VX_EXEC(this.w_QRY + ", " + this.w_REP, this)
    endcase
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo i cursori
    if used("TES_PLOS")
      select TES_PLOS
      use
    endif
    if used("UMNOFR")
      select UMNOFR
      use
    endif
    if used("__Tmp__")
      select __Tmp__
      use
    endif
    if used("__Tmp1__")
      select __Tmp1__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='PAR_DISB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
