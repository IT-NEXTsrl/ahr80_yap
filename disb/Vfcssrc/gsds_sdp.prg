* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_sdp                                                        *
*              Stampa documenti di produzione                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_68]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-19                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_sdp",oParentObject))

* --- Class definition
define class tgsds_sdp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 584
  Height = 470
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=194419607
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsds_sdp"
  cComment = "Stampa documenti di produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOIN = space(5)
  w_datain = ctod('  /  /  ')
  o_datain = ctod('  /  /  ')
  w_datafi = ctod('  /  /  ')
  w_numini = 0
  w_serie1 = space(2)
  w_numfin = 0
  w_FC = space(1)
  o_FC = space(1)
  w_clifor = space(15)
  w_DATREG = ctod('  /  /  ')
  w_serie2 = space(2)
  w_SELEZI = space(1)
  w_dt = space(35)
  w_DQ = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_ONUME = 0
  w_NUMER = 0
  w_SERDOC = space(10)
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_sdpPag1","gsds_sdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOIN=space(5)
      .w_datain=ctod("  /  /  ")
      .w_datafi=ctod("  /  /  ")
      .w_numini=0
      .w_serie1=space(2)
      .w_numfin=0
      .w_FC=space(1)
      .w_clifor=space(15)
      .w_DATREG=ctod("  /  /  ")
      .w_serie2=space(2)
      .w_SELEZI=space(1)
      .w_dt=space(35)
      .w_DQ=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLVEAC=space(1)
      .w_CATDOC=space(2)
      .w_ONUME=0
      .w_NUMER=0
      .w_SERDOC=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_TIPOIN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_numini = 1
          .DoRTCalc(5,5,.f.)
        .w_numfin = 999999
        .w_FC = 'C'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_clifor))
          .link_1_9('Full')
        endif
        .w_DATREG = i_datsys
          .DoRTCalc(10,10,.f.)
        .w_SELEZI = "D"
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(12,13,.f.)
        .w_OBTEST = .w_datain
          .DoRTCalc(15,15,.f.)
        .w_FLVEAC = 'V'
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
          .DoRTCalc(17,18,.f.)
        .w_NUMER = .w_ONUME
        .w_SERDOC = SPAC(10)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_FC<>.w_FC
          .link_1_9('Full')
        endif
            .w_DATREG = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(10,13,.t.)
        if .o_datain<>.w_datain
            .w_OBTEST = .w_datain
        endif
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .DoRTCalc(15,18,.t.)
            .w_NUMER = .w_ONUME
            .w_SERDOC = SPAC(10)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oclifor_1_9.enabled = this.oPgFrm.Page1.oPag.oclifor_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPOIN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPOIN))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPOIN_1_1'),i_cWhere,'GSVE_ATD',"Tipi di documento",'GSDS_TIP.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPOIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPOIN)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOIN = NVL(_Link_.TDTIPDOC,space(5))
      this.w_dt = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOIN = space(5)
      endif
      this.w_dt = space(35)
      this.w_FLVEAC = space(1)
      this.w_CATDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=clifor
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_clifor) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FC;
                     ,'ANCODICE',trim(this.w_clifor))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_clifor)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_FC);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_clifor) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oclifor_1_9'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_clifor)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_clifor);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FC;
                       ,'ANCODICE',this.w_clifor)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_clifor = NVL(_Link_.ANCODICE,space(15))
      this.w_DQ = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_clifor = space(15)
      endif
      this.w_DQ = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_clifor = space(15)
        this.w_DQ = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_clifor Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOIN_1_1.value==this.w_TIPOIN)
      this.oPgFrm.Page1.oPag.oTIPOIN_1_1.value=this.w_TIPOIN
    endif
    if not(this.oPgFrm.Page1.oPag.odatain_1_2.value==this.w_datain)
      this.oPgFrm.Page1.oPag.odatain_1_2.value=this.w_datain
    endif
    if not(this.oPgFrm.Page1.oPag.odatafi_1_4.value==this.w_datafi)
      this.oPgFrm.Page1.oPag.odatafi_1_4.value=this.w_datafi
    endif
    if not(this.oPgFrm.Page1.oPag.onumini_1_5.value==this.w_numini)
      this.oPgFrm.Page1.oPag.onumini_1_5.value=this.w_numini
    endif
    if not(this.oPgFrm.Page1.oPag.oserie1_1_6.value==this.w_serie1)
      this.oPgFrm.Page1.oPag.oserie1_1_6.value=this.w_serie1
    endif
    if not(this.oPgFrm.Page1.oPag.onumfin_1_7.value==this.w_numfin)
      this.oPgFrm.Page1.oPag.onumfin_1_7.value=this.w_numfin
    endif
    if not(this.oPgFrm.Page1.oPag.oFC_1_8.RadioValue()==this.w_FC)
      this.oPgFrm.Page1.oPag.oFC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oclifor_1_9.value==this.w_clifor)
      this.oPgFrm.Page1.oPag.oclifor_1_9.value=this.w_clifor
    endif
    if not(this.oPgFrm.Page1.oPag.oserie2_1_11.value==this.w_serie2)
      this.oPgFrm.Page1.oPag.oserie2_1_11.value=this.w_serie2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odt_1_20.value==this.w_dt)
      this.oPgFrm.Page1.oPag.odt_1_20.value=this.w_dt
    endif
    if not(this.oPgFrm.Page1.oPag.oDQ_1_21.value==this.w_DQ)
      this.oPgFrm.Page1.oPag.oDQ_1_21.value=this.w_DQ
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TIPOIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOIN_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TIPOIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_datain)) or not((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatain_1_2.SetFocus()
            i_bnoObbl = !empty(.w_datain)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatafi_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumini_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumfin_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (NOT EMPTY(NVL(.w_FC,' ')))  and not(empty(.w_clifor))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oclifor_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie2_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_datain = this.w_datain
    this.o_FC = this.w_FC
    return

enddefine

* --- Define pages as container
define class tgsds_sdpPag1 as StdContainer
  Width  = 580
  height = 470
  stdWidth  = 580
  stdheight = 470
  resizeYpos=304
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOIN_1_1 as StdField with uid="OGBVPWQYKG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIPOIN", cQueryName = "TIPOIN",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 25499082,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=121, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPOIN"

  func oTIPOIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOIN_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOIN_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPOIN_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi di documento",'GSDS_TIP.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPOIN_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPOIN
     i_obj.ecpSave()
  endproc

  add object odatain_1_2 as StdField with uid="TLSMGPEYWG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_datain", cQueryName = "datain",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento iniziale selezionata",;
    HelpContextID = 259046602,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=120, Top=40

  func odatain_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc


  add object oBtn_1_3 as StdButton with uid="XAOFRYAIID",left=522, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza documenti che soddisfano le selezioni impostate";
    , HelpContextID = 165529066;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object odatafi_1_4 as StdField with uid="SJPNDJOWBM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_datafi", cQueryName = "datafi",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento finale selezionata",;
    HelpContextID = 77642954,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=120, Top=72

  func odatafi_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc

  add object onumini_1_5 as StdField with uid="CGTAYVCLKJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_numini", cQueryName = "numini",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 68753450,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=296, Top=40, cSayPict='"999999"', cGetPict='"999999"'

  func onumini_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oserie1_1_6 as StdField with uid="IKCHOCFECU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_serie1", cQueryName = "serie1",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 212391898,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=371, Top=40, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oserie1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object onumfin_1_7 as StdField with uid="PPVPLBFEDU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_numfin", cQueryName = "numfin",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 258742314,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=296, Top=72, cSayPict='"999999"', cGetPict='"999999"'

  func onumfin_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc


  add object oFC_1_8 as StdCombo with uid="XQHGUYUKZX",value=3,rtseq=7,rtrep=.f.,left=413,top=71,width=101,height=21;
    , ToolTipText = "Tipo dell'intestatario";
    , HelpContextID = 194437974;
    , cFormVar="w_FC",RowSource=""+"Cliente,"+"Fornitore,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFC_1_8.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oFC_1_8.GetRadio()
    this.Parent.oContained.w_FC = this.RadioValue()
    return .t.
  endfunc

  func oFC_1_8.SetRadio()
    this.Parent.oContained.w_FC=trim(this.Parent.oContained.w_FC)
    this.value = ;
      iif(this.Parent.oContained.w_FC=='C',1,;
      iif(this.Parent.oContained.w_FC=='F',2,;
      iif(this.Parent.oContained.w_FC=='',3,;
      0)))
  endfunc

  func oFC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_clifor)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oclifor_1_9 as StdField with uid="PVMPOOOOIB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_clifor", cQueryName = "clifor",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Cliente selezionato",;
    HelpContextID = 185360858,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=101, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FC", oKey_2_1="ANCODICE", oKey_2_2="this.w_clifor"

  func oclifor_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(NVL(.w_FC,' ')))
    endwith
   endif
  endfunc

  func oclifor_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oclifor_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oclifor_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FC)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oclifor_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'',this.parent.oContained
  endproc
  proc oclifor_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FC
     i_obj.w_ANCODICE=this.parent.oContained.w_clifor
     i_obj.ecpSave()
  endproc

  add object oserie2_1_11 as StdField with uid="SNGKDXOYPG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_serie2", cQueryName = "serie2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 195614682,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=371, Top=71, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oserie2_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oSELEZI_1_12 as StdRadio with uid="IRSHIEQNET",rtseq=11,rtrep=.f.,left=6, top=424, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 92232154
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 92232154
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_1_13 as cp_outputCombo with uid="ZLERXIVPWT",left=119, top=394, width=452,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 164453658


  add object oBtn_1_14 as StdButton with uid="UONXVMSJSP",left=472, top=419, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 200661722;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSDS_BG1(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="KYQESPJULE",left=522, top=419, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201737030;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object odt_1_20 as StdField with uid="HIPUUHZXZF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_dt", cQueryName = "dt",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 194450998,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=186, Top=12, InputMask=replicate('X',35)

  add object oDQ_1_21 as StdField with uid="JUPRVCCYCI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DQ", cQueryName = "DQ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 194441526,;
   bGlobalFont=.t.,;
    Height=21, Width=268, Left=250, Top=101, InputMask=replicate('X',40)


  add object oObj_1_29 as cp_runprogram with uid="FCDBUQPWYA",left=7, top=471, width=260,height=19,;
    caption='GSDS_BG1(SELEZI)',;
   bGlobalFont=.t.,;
    prg="GSDS_BG1('SELEZI')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 41867584


  add object ZoomSel as cp_szoombox with uid="SBJCPVQPFT",left=3, top=127, width=572,height=260,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSDS1QSP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 164453658

  add object oStr_1_15 as StdString with uid="RWHNHUCUAO",Visible=.t., Left=20, Top=40,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="GFGSYQKGMB",Visible=.t., Left=20, Top=72,;
    Alignment=1, Width=95, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=210, Top=41,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=210, Top=74,;
    Alignment=1, Width=85, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VRHESNZKUV",Visible=.t., Left=20, Top=12,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SOIMYBQITY",Visible=.t., Left=20, Top=102,;
    Alignment=1, Width=95, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=358, Top=40,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=358, Top=72,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="GIGJHPMORF",Visible=.t., Left=16, Top=394,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_sdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
