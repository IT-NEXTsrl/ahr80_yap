* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bsc                                                        *
*              Sostituzione componenti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2018-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bsc",oParentObject,m.pAzione)
return(i_retval)

define class tgsds_bsc as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_OK = .f.
  w_MESS = space(10)
  w_CODDIS = space(20)
  w_OLDCOM = space(20)
  w_NUMROW = 0
  w_NUMRIF = 0
  w_FLESPL = space(1)
  w_DATELAB = ctod("  /  /  ")
  w_QTADIS = 0
  w_QTADIS1 = 0
  w_ROWNUM = 0
  w_ROWORD = 0
  w_FLVARI = space(1)
  w_CODCOMS = space(20)
  w_CODARTS = space(20)
  w_NUMRIG = 0
  w_DESCOMS = space(40)
  w_OLDNUMRIF = 0
  w_COMDIS = space(20)
  w_FLCOMP = space(1)
  w_RIFFAS = 0
  w_QTAUM1 = 0
  * --- WorkFile variables
  COM_VARI_idx=0
  DISTBASE_idx=0
  DISMBASE_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sostituzione Componenti Varianti (da GSDS_KSC)
    this.w_OK = .T.
    this.w_DATELAB = i_Datsys
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione="INTERROGA"
        this.w_OK = .t.
        if this.oParentObject.w_OPERAZ = "S" and EMPTY(this.oParentObject.w_COMORI)
          ah_ErrorMsg("Componente di origine non definito",,"")
          this.w_OK = .f.
        endif
        if this.oParentObject.w_OPERAZ<>"D" and EMPTY(this.oParentObject.w_DBCODCOM)
          ah_ErrorMsg("Componente da inserire non definito",,"")
          this.w_OK = .f.
        endif
        if this.oParentObject.w_OPERAZ="D" and EMPTY(this.oParentObject.w_COMORI) and (EMPTY(this.oParentObject.w_DISINI) or EMPTY(this.oParentObject.w_DISFIN))
          Ah_ErrorMsg("Occorre specificare l'intervallo delle distinte",48,"")
          this.w_OK = .f.
        endif
        if this.oparentobject.opgfrm.ActivePage=1 and this.w_OK
          if EMPTY(this.oParentObject.w_DISINI) and EMPTY(this.oParentObject.w_DISFIN) and EMPTY(this.oParentObject.w_STADIT) and EMPTY(this.oParentObject.w_TIPOARTI) and EMPTY(this.oParentObject.w_FAMINI) ; 
 and EMPTY(this.oParentObject.w_FAMFIN) and EMPTY(this.oParentObject.w_GRUINI) and EMPTY(this.oParentObject.w_GRUFIN) and EMPTY(this.oParentObject.w_CATINI) and EMPTY(this.oParentObject.w_CATFIN) ; 
 and (this.oParentObject.w_OPERAZ="I" or (this.oParentObject.w_OPERAZ="D" and EMPTY(this.oParentObject.w_COMORI)))
            if !ah_YesNo("Attenzione, verranno visualizzati in elenco distinte tutti i componenti presenti in archivio.%0Proseguire con la visualizzazione?")
              this.w_OK = .f.
            endif
          endif
        endif
        if this.w_OK
          if this.oParentObject.w_OPERAZ="I"
            this.Padre.w_ZoomSel.cSymFile = "DISMBASE"
            this.Padre.w_ZoomSel.cZoomName = "GSDS_KSC"
            this.Padre.w_ZoomSel.cCpQueryName = "..\disb\exe\query\GSDS3BSC"
          else
            this.Padre.w_ZoomSel.cSymFile = "DISMBASE"
            this.Padre.w_ZoomSel.cZoomName = "GSDS_KSC"
            this.Padre.w_ZoomSel.cCpQueryName = "..\disb\exe\query\GSDS_BSC"
          endif
          * --- Visualizza Zoom distinte
          this.Padre.NotifyEvent("Interroga")     
          * --- Attiva la pagina 2 automaticamente
          this.oParentObject.oPgFrm.ActivePage = 2
          this.oParentObject.w_SELEZI = "D"
          if this.oParentObject.w_OPERAZ="I"
            this.Padre.w_ZoomSel.grd.Columns(4).width = 0
            this.Padre.w_ZoomSel.grd.Columns(5).width = 0
            this.Padre.w_ZoomSel.grd.Columns(6).width = 0
            this.Padre.w_ZoomSel.grd.Columns(7).width = 0
            this.Padre.w_ZoomSel.grd.Columns(8).width = 0
            this.Padre.w_ZoomSel.grd.Columns(3).width = 0
            this.Padre.w_ZoomSel.grd.refresh()     
          endif
        endif
      case this.pAzione="AG"
        * --- Controlli Preliminari
        if this.oParentObject.w_OPERAZ="S" and this.oParentObject.w_TIPSOST="S"
          if this.oParentObject.w_UM1<>this.oParentObject.w_UNMIS1 OR this.oParentObject.w_UM2<>this.oParentObject.w_UNMIS2 OR this.oParentObject.w_UM3<>this.oParentObject.w_UNMIS3
            ah_ErrorMsg("Componenti con unit� di misura non congruenti",,"")
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_nRecSel = 0
        this.w_nRecEla = 0
        SELECT (this.NC)
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel = 0
          this.w_MESS = "Occorre specificare almeno una distinta."
          Ah_ErrorMsg(this.w_MESS,48,"")
        else
          CREATE CURSOR _NOAGG_ ; 
 (DISTINTA C(20),NEWCOMP C(20),OLDCOMP C(20), MESS C(100))
          * --- Inizia Sostituzione Componente
          ah_Msg("Inizio %1 componenti",.T.,.F.,.F.,iif(this.oParentObject.w_OPERAZ="S","sostituzione",iif(this.oParentObject.w_OPERAZ="I","inserimento","disattivazione")))
          * --- Try
          local bErr_011E47C0
          bErr_011E47C0=bTrsErr
          this.Try_011E47C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            if NOT EMPTY(this.w_MESS)
              ah_ErrorMsg("%1%0Operazione abbandonata",,"", this.w_MESS)
            else
              ah_ErrorMsg("Errore durante l'elaborazione%0Operazione abbandonata",,"")
            endif
          endif
          bTrsErr=bTrsErr or bErr_011E47C0
          * --- End
          Select *, this.oParentObject.w_OPERAZ as OPERAZ, this.oParentObject.w_COMORI as COMORI, this.oParentObject.w_DESORI as DESORI, this.oParentObject.w_DBCODCOM as DBCODCOM, this.oParentObject.w_DBDESCOM as DBDESCOM,; 
 this.oParentObject.w_NEWATT as NEWATT, this.oParentObject.w_UNIMIS as UNIMIS, this.oParentObject.w_DBPERSCA as PERSCA, this.oParentObject.w_DBPERSFR as PERSFR, this.oParentObject.w_DBPERRIC as PERRIC,; 
 this.oParentObject.w_DBRECSCA as RECSCA, this.oParentObject.w_DBRECSFR as RECSFR, this.oParentObject.w_QTA as QTA, this.oParentObject.w_DESSUP as DESSUP, this.oParentObject.w_DISINI as DISINI, this.oParentObject.w_DISFIN as DISFIN,; 
 this.oParentObject.w_STADIT as STADIT, this.oParentObject.w_TIPOARTI as TIPOARTI, this.oParentObject.w_FAMINI as FAMINI, this.oParentObject.w_FAMFIN as FAMFIN, this.oParentObject.w_GRUINI as GRUINI, this.oParentObject.w_GRUFIN as GRUFIN,; 
 this.oParentObject.w_CATINI as CATINI, this.oParentObject.w_CATFIN as CATFIN, "S" as AGG, "" as MESS from (this.NC) where XCHK=1 into cursor __tmp__
          cur=wrcursor("__tmp__") 
 select _NOAGG_ 
 scan 
 Cancello=_NOAGG_.DISTINTA 
 rinum=recno() 
 update __tmp__ set agg="N", mess =_NOAGG_.MESS where dbcodice=Cancello 
 select _NOAGG_ 
 go rinum 
 endscan
           Use in Select("_NOAGG_")
          CP_CHPRN("..\DISB\EXE\QUERY\GSDS_KSC","",this.oParentObject)
           Use in Select("__tmp__")
          this.Padre.NotifyEvent("Interroga")     
        endif
    endcase
  endproc
  proc Try_011E47C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT (this.NC)
    GO TOP
    SCAN FOR xChk=1
    this.w_CODDIS = CODDIS
    this.w_NUMROW = CPROWNUM
    this.w_OLDCOM = CODCOM
    this.w_NUMRIF = CVNUMRIF
    this.w_FLVARI = NVL(DBFLVARI,"N")
    this.w_FLESPL = NVL(DBFLESPL," ")
    this.w_RIFFAS = NVL(DBRIFFAS,0)
    if this.oParentObject.w_OPERAZ$"I-S"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODDIS,ARFLCOMP"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_DBARTCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODDIS,ARFLCOMP;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_DBARTCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COMDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        this.w_FLCOMP = NVL(cp_ToDate(_read_.ARFLCOMP),cp_NullValue(_read_.ARFLCOMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_FLESPL = IIF(NOT EMPTY(this.oParentObject.w_DBCODCOM) AND NOT EMPTY(this.w_COMDIS), this.w_FLCOMP, " ")
    endif
    this.w_QTADIS = iif(this.oParentObject.w_QTAORI<>"S" and this.oParentObject.w_OPERAZ<>"D",this.oParentObject.w_QTA,DBQTADIS)
    if this.oParentObject.w_UM1=this.oParentObject.w_UNMIS1
      this.w_QTAUM1 = DBCOEUM1
    else
      this.w_QTAUM1 = CALQTA(this.w_QTADIS,this.oParentObject.w_UNMIS1,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "N", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
    endif
    this.w_QTADIS1 = iif(this.oParentObject.w_QTAORI<>"S" and this.oParentObject.w_OPERAZ<>"D",this.oParentObject.w_QTA1,this.w_QTAUM1)
    ah_Msg("Inizio %1 componenti distinta: %2 - riga: %3",.T.,.F.,.F.,iif(this.oParentObject.w_OPERAZ="S","sostituzione",iif(this.oParentObject.w_OPERAZ="I","inserimento","disattivazione")), ALLTRIM(this.w_CODDIS), ALLTRIM(STR(this.w_NUMROW)) )
    * --- Try
    local bErr_04376550
    bErr_04376550=bTrsErr
    this.Try_04376550()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = Ah_Msgformat("Impossibile inserire i componenti nel dettaglio distinta base")
      INSERT INTO _NOAGG_ (DISTINTA, NEWCOMP, OLDCOMP, MESS) VALUES (this.w_CODDIS, w_DBCODOM,"", message())
    endif
    bTrsErr=bTrsErr or bErr_04376550
    * --- End
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("%1 componenti completata",,"",iif(this.oParentObject.w_OPERAZ="S","Sostituzione",iif(this.oParentObject.w_OPERAZ="I","Inserimento","Disattivazione")))
    return
  proc Try_04376550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_OPERAZ="I"
        * --- Inserimento nuovo componente
        this.w_ROWNUM = NVL(CPROWNUM,0) + 1
        this.w_ROWORD = NVL(CPROWORD,0) + 10
        this.w_FLESPL = IIF(EMPTY(this.w_COMDIS), " ", IIF(this.oParentObject.w_FLAGG="S", this.oParentObject.w_DBFLESPL, this.w_FLESPL))
        * --- Try
        local bErr_0437A0C0
        bErr_0437A0C0=bTrsErr
        this.Try_0437A0C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = Ah_Msgformat("Impossibile inserire i componenti nel dettaglio distinta base")
          INSERT INTO _NOAGG_ (DISTINTA, NEWCOMP, OLDCOMP, MESS) VALUES (this.w_CODDIS, w_DBCODOM,"", message())
        endif
        bTrsErr=bTrsErr or bErr_0437A0C0
        * --- End
      case this.oParentObject.w_OPERAZ="D"
        * --- Disattivazione legame alla data
        * --- Try
        local bErr_0437CE50
        bErr_0437CE50=bTrsErr
        this.Try_0437CE50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = Ah_Msgformat("Impossibile disattivare i componenti del dettaglio distinta base")
          INSERT INTO _NOAGG_ (DISTINTA, NEWCOMP, OLDCOMP, MESS) VALUES (this.w_CODDIS, w_DBCODOM,"", message())
        endif
        bTrsErr=bTrsErr or bErr_0437CE50
        * --- End
      case this.oParentObject.w_OPERAZ="S"
        * --- Try
        local bErr_0437E860
        bErr_0437E860=bTrsErr
        this.Try_0437E860()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = Ah_Msgformat("Impossibile aggiornare i componenti nel dettaglio distinta base")
          INSERT INTO _NOAGG_ (DISTINTA, NEWCOMP, OLDCOMP, MESS) VALUES (this.w_CODDIS, w_DBCODOM,"", message())
        endif
        bTrsErr=bTrsErr or bErr_0437E860
        * --- End
    endcase
    return
  proc Try_0437A0C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBUNIMIS"+",DBQTADIS"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBDATULT"+",DBFLESPL"+",DBARTCOM"+",DBCOEUM1"+",DBINIVAL"+",DBFINVAL"+",DB__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'DISTBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DISTBASE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DISTBASE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLVARI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'DISTBASE','DBCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCOM),'DISTBASE','DBDESCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNIMIS),'DISTBASE','DBUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSCA),'DISTBASE','DBPERSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSCA),'DISTBASE','DBRECSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSFR),'DISTBASE','DBPERSFR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSFR),'DISTBASE','DBRECSFR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERRIC),'DISTBASE','DBPERRIC');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'DISTBASE','DBARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBCOEUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(i_FINDAT),'DISTBASE','DBFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESSUP),'DISTBASE','DB__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODDIS,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'DBFLVARI'," ",'DBCODCOM',this.oParentObject.w_DBCODCOM,'DBDESCOM',this.oParentObject.w_DBDESCOM,'DBUNIMIS',this.oParentObject.w_UNIMIS,'DBQTADIS',this.w_QTADIS,'DBPERSCA',this.oParentObject.w_DBPERSCA,'DBRECSCA',this.oParentObject.w_DBRECSCA,'DBPERSFR',this.oParentObject.w_DBPERSFR,'DBRECSFR',this.oParentObject.w_DBRECSFR)
      insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBUNIMIS,DBQTADIS,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBDATULT,DBFLESPL,DBARTCOM,DBCOEUM1,DBINIVAL,DBFINVAL,DB__NOTE &i_ccchkf. );
         values (;
           this.w_CODDIS;
           ,this.w_ROWNUM;
           ,this.w_ROWORD;
           ," ";
           ,this.oParentObject.w_DBCODCOM;
           ,this.oParentObject.w_DBDESCOM;
           ,this.oParentObject.w_UNIMIS;
           ,this.w_QTADIS;
           ,this.oParentObject.w_DBPERSCA;
           ,this.oParentObject.w_DBRECSCA;
           ,this.oParentObject.w_DBPERSFR;
           ,this.oParentObject.w_DBRECSFR;
           ,this.oParentObject.w_DBPERRIC;
           ,i_DATSYS;
           ,this.w_FLESPL;
           ,this.oParentObject.w_DBARTCOM;
           ,this.w_QTADIS;
           ,this.oParentObject.w_NEWATT;
           ,i_FINDAT;
           ,this.oParentObject.w_DESSUP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0437CE50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DISTBASE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DBFLESPL ="+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
      +",DBPERSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSCA),'DISTBASE','DBPERSCA');
      +",DBRECSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSCA),'DISTBASE','DBRECSCA');
      +",DBPERSFR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSFR),'DISTBASE','DBPERSFR');
      +",DBRECSFR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSFR),'DISTBASE','DBRECSFR');
      +",DBPERRIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERRIC),'DISTBASE','DBPERRIC');
      +",DBFINVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBFINVAL');
      +",DBQTADIS ="+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBQTADIS');
      +",DBDATULT ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
      +",DB__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESSUP),'DISTBASE','DB__NOTE');
          +i_ccchkf ;
      +" where ";
          +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
             )
    else
      update (i_cTable) set;
          DBFLESPL = this.w_FLESPL;
          ,DBPERSCA = this.oParentObject.w_DBPERSCA;
          ,DBRECSCA = this.oParentObject.w_DBRECSCA;
          ,DBPERSFR = this.oParentObject.w_DBPERSFR;
          ,DBRECSFR = this.oParentObject.w_DBRECSFR;
          ,DBPERRIC = this.oParentObject.w_DBPERRIC;
          ,DBFINVAL = this.oParentObject.w_NEWATT;
          ,DBQTADIS = this.w_QTADIS;
          ,DBDATULT = i_DATSYS;
          ,DB__NOTE = this.oParentObject.w_DESSUP;
          &i_ccchkf. ;
       where;
          DBCODICE = this.w_CODDIS;
          and CPROWNUM = this.w_NUMROW;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0437E860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPSOST = "M"
      if this.w_FLVARI="S"
        * --- Disattivazione legame alla data del componente di riferimento
        if this.w_NUMRIF<>this.w_OLDNUMRIF
          * --- Controllo per evitare di inserire pi� volte la riga se devo fare pi� sostituzioni sulla medesima variante
          this.w_OLDNUMRIF = this.w_NUMRIF
          * --- Write into DISTBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBFINVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT-1),'DISTBASE','DBFINVAL');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIF);
                   )
          else
            update (i_cTable) set;
                DBFINVAL = this.oParentObject.w_NEWATT-1;
                &i_ccchkf. ;
             where;
                DBCODICE = this.w_CODDIS;
                and CPROWNUM = this.w_NUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Select from DISTBASE
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select max(cprownum) as cprownum  from "+i_cTable+" DISTBASE ";
                +" where DBCODICE="+cp_ToStrODBC(this.w_CODDIS)+"";
                 ,"_Curs_DISTBASE")
          else
            select max(cprownum) as cprownum from (i_cTable);
             where DBCODICE=this.w_CODDIS;
              into cursor _Curs_DISTBASE
          endif
          if used('_Curs_DISTBASE')
            select _Curs_DISTBASE
            locate for 1=1
            do while not(eof())
            this.w_ROWNUM = NVL(cprownum,0)
              select _Curs_DISTBASE
              continue
            enddo
            use
          endif
          * --- Read from DISTBASE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWORD,DBDESCOM"+;
              " from "+i_cTable+" DISTBASE where ";
                  +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWORD,DBDESCOM;
              from (i_cTable) where;
                  DBCODICE = this.w_CODDIS;
                  and CPROWNUM = this.w_NUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            this.w_DESCOMS = NVL(cp_ToDate(_read_.DBDESCOM),cp_NullValue(_read_.DBDESCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ROWNUM = NVL(this.w_ROWNUM,0)+1
          if this.oParentObject.w_FLAGG="S"
            * --- Insert into DISTBASE
            i_nConn=i_TableProp[this.DISTBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBQTADIS"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBDATULT"+",DBFLESPL"+",DBCOEUM1"+",DBINIVAL"+",DBFINVAL"+",DB__NOTE"+",DBFLVARC"+",DBRIFFAS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'DISTBASE','DBCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DISTBASE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DISTBASE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLVARI),'DISTBASE','DBFLVARI');
              +","+cp_NullLink(cp_ToStrODBC(space(20)),'DISTBASE','DBCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESCOMS),'DISTBASE','DBDESCOM');
              +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBQTADIS');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSCA),'DISTBASE','DBPERSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSCA),'DISTBASE','DBRECSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSFR),'DISTBASE','DBPERSFR');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSFR),'DISTBASE','DBRECSFR');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERRIC),'DISTBASE','DBPERRIC');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
              +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBCOEUM1');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBINIVAL');
              +","+cp_NullLink(cp_ToStrODBC(i_FINDAT),'DISTBASE','DBFINVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESSUP),'DISTBASE','DB__NOTE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLVARI),'DISTBASE','DBFLVARC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'DISTBASE','DBRIFFAS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODDIS,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'DBFLVARI',this.w_FLVARI,'DBCODCOM',space(20),'DBDESCOM',this.w_DESCOMS,'DBQTADIS',0,'DBPERSCA',this.oParentObject.w_DBPERSCA,'DBRECSCA',this.oParentObject.w_DBRECSCA,'DBPERSFR',this.oParentObject.w_DBPERSFR,'DBRECSFR',this.oParentObject.w_DBRECSFR,'DBPERRIC',this.oParentObject.w_DBPERRIC)
              insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBQTADIS,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBDATULT,DBFLESPL,DBCOEUM1,DBINIVAL,DBFINVAL,DB__NOTE,DBFLVARC,DBRIFFAS &i_ccchkf. );
                 values (;
                   this.w_CODDIS;
                   ,this.w_ROWNUM;
                   ,this.w_ROWORD;
                   ,this.w_FLVARI;
                   ,space(20);
                   ,this.w_DESCOMS;
                   ,0;
                   ,this.oParentObject.w_DBPERSCA;
                   ,this.oParentObject.w_DBRECSCA;
                   ,this.oParentObject.w_DBPERSFR;
                   ,this.oParentObject.w_DBRECSFR;
                   ,this.oParentObject.w_DBPERRIC;
                   ,i_DATSYS;
                   ,this.w_FLESPL;
                   ,0;
                   ,this.oParentObject.w_NEWATT;
                   ,i_FINDAT;
                   ,this.oParentObject.w_DESSUP;
                   ,this.w_FLVARI;
                   ,this.w_RIFFAS;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into DISTBASE
            i_nConn=i_TableProp[this.DISTBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBQTADIS"+",DBDATULT"+",DBFLESPL"+",DBCOEUM1"+",DBINIVAL"+",DBFINVAL"+",DBFLVARC"+",DBRIFFAS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'DISTBASE','DBCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DISTBASE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DISTBASE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLVARI),'DISTBASE','DBFLVARI');
              +","+cp_NullLink(cp_ToStrODBC(space(20)),'DISTBASE','DBCODCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESCOMS),'DISTBASE','DBDESCOM');
              +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBQTADIS');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
              +","+cp_NullLink(cp_ToStrODBC(0),'DISTBASE','DBCOEUM1');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBINIVAL');
              +","+cp_NullLink(cp_ToStrODBC(i_FINDAT),'DISTBASE','DBFINVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLVARI),'DISTBASE','DBFLVARC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'DISTBASE','DBRIFFAS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODDIS,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'DBFLVARI',this.w_FLVARI,'DBCODCOM',space(20),'DBDESCOM',this.w_DESCOMS,'DBQTADIS',0,'DBDATULT',i_DATSYS,'DBFLESPL',this.w_FLESPL,'DBCOEUM1',0,'DBINIVAL',this.oParentObject.w_NEWATT,'DBFINVAL',i_FINDAT)
              insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBQTADIS,DBDATULT,DBFLESPL,DBCOEUM1,DBINIVAL,DBFINVAL,DBFLVARC,DBRIFFAS &i_ccchkf. );
                 values (;
                   this.w_CODDIS;
                   ,this.w_ROWNUM;
                   ,this.w_ROWORD;
                   ,this.w_FLVARI;
                   ,space(20);
                   ,this.w_DESCOMS;
                   ,0;
                   ,i_DATSYS;
                   ,this.w_FLESPL;
                   ,0;
                   ,this.oParentObject.w_NEWATT;
                   ,i_FINDAT;
                   ,this.w_FLVARI;
                   ,this.w_RIFFAS;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        * --- Select from COM_VARI
        i_nConn=i_TableProp[this.COM_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2],.t.,this.COM_VARI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" COM_VARI ";
              +" where CVCODICE="+cp_ToStrODBC(this.w_CODDIS)+" and CVNUMRIF="+cp_ToStrODBC(this.w_NUMRIF)+"";
              +" order by CVCODICE,CVNUMRIF,CPROWORD";
               ,"_Curs_COM_VARI")
        else
          select * from (i_cTable);
           where CVCODICE=this.w_CODDIS and CVNUMRIF=this.w_NUMRIF;
           order by CVCODICE,CVNUMRIF,CPROWORD;
            into cursor _Curs_COM_VARI
        endif
        if used('_Curs_COM_VARI')
          select _Curs_COM_VARI
          locate for 1=1
          do while not(eof())
          ah_Msg("Inizio sostituzione componenti varianti: %1 - riga: %2",.T.,.F.,.F., ALLTRIM(this.w_CODDIS), ALLTRIM(STR(this.w_NUMRIF)) )
          this.w_NUMRIG = _Curs_COM_VARI.CPROWNUM
          this.w_FLESPL = NVL(_Curs_COM_VARI.CVFLESPL, " ")
          this.w_CODCOMS = _Curs_COM_VARI.CVCODCOM
          this.w_CODARTS = _Curs_COM_VARI.CVARTCOM
          if this.w_NUMROW=this.w_NUMRIG
            this.w_CODCOMS = this.oParentObject.w_DBCODCOM
            this.w_CODARTS = this.oParentObject.w_DBARTCOM
          endif
          * --- Il nuovo Componente potrebbe non essere piu' una Distinta Base
          this.w_FLESPL = IIF(EMPTY(this.w_COMDIS), " ", IIF(this.oParentObject.w_FLAGG="S", this.oParentObject.w_DBFLESPL, this.w_FLESPL))
          * --- Componente a varianti
          * --- Try
          local bErr_043664B8
          bErr_043664B8=bTrsErr
          this.Try_043664B8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write solo sulla riga da variare (la insert invece la dovevo fare per ogni riga) - caso modifica su pi� righe della medesima variante
            if this.w_NUMRIG=this.w_NUMROW
              * --- Write into COM_VARI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.COM_VARI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_VARI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_CODCOMS),'COM_VARI','CVCODCOM');
                +",CVARTCOM ="+cp_NullLink(cp_ToStrODBC(this.w_CODARTS),'COM_VARI','CVARTCOM');
                +",CVFLESPL ="+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'COM_VARI','CVFLESPL');
                    +i_ccchkf ;
                +" where ";
                    +"CVCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                    +" and CVNUMRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
                       )
              else
                update (i_cTable) set;
                    CVCODCOM = this.w_CODCOMS;
                    ,CVARTCOM = this.w_CODARTS;
                    ,CVFLESPL = this.w_FLESPL;
                    &i_ccchkf. ;
                 where;
                    CVCODICE = this.w_CODDIS;
                    and CVNUMRIF = this.w_ROWNUM;
                    and CPROWNUM = this.w_NUMROW;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          bTrsErr=bTrsErr or bErr_043664B8
          * --- End
          * --- Write into DISMBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISMBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"UTCV ="+cp_NullLink(cp_ToStrODBC(i_codute),'DISMBASE','UTCV');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'DISMBASE','UTDV');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                   )
          else
            update (i_cTable) set;
                UTCV = i_codute;
                ,UTDV = SetInfoDate(g_CALUTD);
                &i_ccchkf. ;
             where;
                DBCODICE = this.w_CODDIS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_COM_VARI
            continue
          enddo
          use
        endif
      else
        * --- Disattivazione legame alla data del componente di riferimento
        this.w_ROWORD = NVL(CPROWORD,10)
        * --- Write into DISTBASE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBFINVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT-1),'DISTBASE','DBFINVAL');
              +i_ccchkf ;
          +" where ";
              +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
                 )
        else
          update (i_cTable) set;
              DBFINVAL = this.oParentObject.w_NEWATT-1;
              &i_ccchkf. ;
           where;
              DBCODICE = this.w_CODDIS;
              and CPROWNUM = this.w_NUMROW;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Select from DISTBASE
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select max(cprownum) as cprownum  from "+i_cTable+" DISTBASE ";
              +" where DBCODICE="+cp_ToStrODBC(this.w_CODDIS)+"";
               ,"_Curs_DISTBASE")
        else
          select max(cprownum) as cprownum from (i_cTable);
           where DBCODICE=this.w_CODDIS;
            into cursor _Curs_DISTBASE
        endif
        if used('_Curs_DISTBASE')
          select _Curs_DISTBASE
          locate for 1=1
          do while not(eof())
          this.w_ROWNUM = NVL(cprownum,0)
            select _Curs_DISTBASE
            continue
          enddo
          use
        endif
        this.w_ROWNUM = NVL(this.w_ROWNUM,0)+1
        * --- Select from DISTBASE
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DBFLESPL  from "+i_cTable+" DISTBASE ";
              +" where DBCODICE="+cp_ToStrODBC(this.w_CODDIS)+" and CPROWNUM="+cp_ToStrODBC(this.w_NUMROW)+"";
               ,"_Curs_DISTBASE")
        else
          select DBFLESPL from (i_cTable);
           where DBCODICE=this.w_CODDIS and CPROWNUM=this.w_NUMROW;
            into cursor _Curs_DISTBASE
        endif
        if used('_Curs_DISTBASE')
          select _Curs_DISTBASE
          locate for 1=1
          do while not(eof())
          this.w_FLESPL = NVL(DBFLESPL," ")
            select _Curs_DISTBASE
            continue
          enddo
          use
        endif
        this.w_FLESPL = IIF(EMPTY(this.w_COMDIS), " ", IIF(this.oParentObject.w_FLAGG="S", this.oParentObject.w_DBFLESPL, this.w_FLESPL))
        if this.oParentObject.w_FLAGG="S"
          * --- Insert into DISTBASE
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBUNIMIS"+",DBQTADIS"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBDATULT"+",DBFLESPL"+",DBARTCOM"+",DBCOEUM1"+",DBINIVAL"+",DBFINVAL"+",DB__NOTE"+",DBRIFFAS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'DISTBASE','DBCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DISTBASE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DISTBASE','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLVARI');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'DISTBASE','DBCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCOM),'DISTBASE','DBDESCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNIMIS),'DISTBASE','DBUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBQTADIS');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSCA),'DISTBASE','DBPERSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSCA),'DISTBASE','DBRECSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSFR),'DISTBASE','DBPERSFR');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSFR),'DISTBASE','DBRECSFR');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERRIC),'DISTBASE','DBPERRIC');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'DISTBASE','DBARTCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS1),'DISTBASE','DBCOEUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBINIVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_FINDAT),'DISTBASE','DBFINVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESSUP),'DISTBASE','DB__NOTE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'DISTBASE','DBRIFFAS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODDIS,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'DBFLVARI'," ",'DBCODCOM',this.oParentObject.w_DBCODCOM,'DBDESCOM',this.oParentObject.w_DBDESCOM,'DBUNIMIS',this.oParentObject.w_UNIMIS,'DBQTADIS',this.w_QTADIS,'DBPERSCA',this.oParentObject.w_DBPERSCA,'DBRECSCA',this.oParentObject.w_DBRECSCA,'DBPERSFR',this.oParentObject.w_DBPERSFR,'DBRECSFR',this.oParentObject.w_DBRECSFR)
            insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBUNIMIS,DBQTADIS,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBDATULT,DBFLESPL,DBARTCOM,DBCOEUM1,DBINIVAL,DBFINVAL,DB__NOTE,DBRIFFAS &i_ccchkf. );
               values (;
                 this.w_CODDIS;
                 ,this.w_ROWNUM;
                 ,this.w_ROWORD;
                 ," ";
                 ,this.oParentObject.w_DBCODCOM;
                 ,this.oParentObject.w_DBDESCOM;
                 ,this.oParentObject.w_UNIMIS;
                 ,this.w_QTADIS;
                 ,this.oParentObject.w_DBPERSCA;
                 ,this.oParentObject.w_DBRECSCA;
                 ,this.oParentObject.w_DBPERSFR;
                 ,this.oParentObject.w_DBRECSFR;
                 ,this.oParentObject.w_DBPERRIC;
                 ,i_DATSYS;
                 ,this.w_FLESPL;
                 ,this.oParentObject.w_DBARTCOM;
                 ,this.w_QTADIS1;
                 ,this.oParentObject.w_NEWATT;
                 ,i_FINDAT;
                 ,this.oParentObject.w_DESSUP;
                 ,this.w_RIFFAS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into DISTBASE
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBFLVARI"+",DBCODCOM"+",DBDESCOM"+",DBUNIMIS"+",DBQTADIS"+",DBDATULT"+",DBFLESPL"+",DBARTCOM"+",DBCOEUM1"+",DBINIVAL"+",DBFINVAL"+",DBRIFFAS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'DISTBASE','DBCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DISTBASE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DISTBASE','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DISTBASE','DBFLVARI');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'DISTBASE','DBCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCOM),'DISTBASE','DBDESCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNIMIS),'DISTBASE','DBUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBQTADIS');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'DISTBASE','DBARTCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTADIS1),'DISTBASE','DBCOEUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWATT),'DISTBASE','DBINIVAL');
            +","+cp_NullLink(cp_ToStrODBC(i_FINDAT),'DISTBASE','DBFINVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFAS),'DISTBASE','DBRIFFAS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODDIS,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'DBFLVARI'," ",'DBCODCOM',this.oParentObject.w_DBCODCOM,'DBDESCOM',this.oParentObject.w_DBDESCOM,'DBUNIMIS',this.oParentObject.w_UNIMIS,'DBQTADIS',this.w_QTADIS,'DBDATULT',i_DATSYS,'DBFLESPL',this.w_FLESPL,'DBARTCOM',this.oParentObject.w_DBARTCOM,'DBCOEUM1',this.w_QTADIS1)
            insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBFLVARI,DBCODCOM,DBDESCOM,DBUNIMIS,DBQTADIS,DBDATULT,DBFLESPL,DBARTCOM,DBCOEUM1,DBINIVAL,DBFINVAL,DBRIFFAS &i_ccchkf. );
               values (;
                 this.w_CODDIS;
                 ,this.w_ROWNUM;
                 ,this.w_ROWORD;
                 ," ";
                 ,this.oParentObject.w_DBCODCOM;
                 ,this.oParentObject.w_DBDESCOM;
                 ,this.oParentObject.w_UNIMIS;
                 ,this.w_QTADIS;
                 ,i_DATSYS;
                 ,this.w_FLESPL;
                 ,this.oParentObject.w_DBARTCOM;
                 ,this.w_QTADIS1;
                 ,this.oParentObject.w_NEWATT;
                 ,i_FINDAT;
                 ,this.w_RIFFAS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    else
      if this.w_FLVARI="S"
        ah_Msg("Inizio sostituzione componenti varianti: %1 - riga: %2",.T.,.F.,.F., ALLTRIM(this.w_CODDIS), ALLTRIM(STR(this.w_NUMRIF)) )
        * --- Componente a varianti
        this.w_FLESPL = IIF(EMPTY(this.oParentObject.w_DISCOM), " ", IIF(this.oParentObject.w_FLAGG="S", this.oParentObject.w_DBFLESPL, this.w_FLESPL))
        * --- Write into COM_VARI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COM_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COM_VARI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'COM_VARI','CVCODCOM');
          +",CVARTCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'COM_VARI','CVARTCOM');
          +",CVFLESPL ="+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'COM_VARI','CVFLESPL');
              +i_ccchkf ;
          +" where ";
              +"CVCODICE = "+cp_ToStrODBC(this.w_CODDIS);
              +" and CVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
                 )
        else
          update (i_cTable) set;
              CVCODCOM = this.oParentObject.w_DBCODCOM;
              ,CVARTCOM = this.oParentObject.w_DBARTCOM;
              ,CVFLESPL = this.w_FLESPL;
              &i_ccchkf. ;
           where;
              CVCODICE = this.w_CODDIS;
              and CVNUMRIF = this.w_NUMRIF;
              and CPROWNUM = this.w_NUMROW;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.oParentObject.w_FLAGG="S"
          * --- Write into DISTBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'DISTBASE','DBCODCOM');
            +",DBDESCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCOM),'DISTBASE','DBDESCOM');
            +",DBFLESPL ="+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
            +",DBARTCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'DISTBASE','DBARTCOM');
            +",DBPERSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSCA),'DISTBASE','DBPERSCA');
            +",DBRECSCA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSCA),'DISTBASE','DBRECSCA');
            +",DBPERSFR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERSFR),'DISTBASE','DBPERSFR');
            +",DBRECSFR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBRECSFR),'DISTBASE','DBRECSFR');
            +",DBPERRIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPERRIC),'DISTBASE','DBPERRIC');
            +",DBQTADIS ="+cp_NullLink(cp_ToStrODBC(this.w_QTADIS),'DISTBASE','DBQTADIS');
            +",DBDATULT ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBDATULT');
            +",DB__NOTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESSUP),'DISTBASE','DB__NOTE');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
                   )
          else
            update (i_cTable) set;
                DBCODCOM = this.oParentObject.w_DBCODCOM;
                ,DBDESCOM = this.oParentObject.w_DBDESCOM;
                ,DBFLESPL = this.w_FLESPL;
                ,DBARTCOM = this.oParentObject.w_DBARTCOM;
                ,DBPERSCA = this.oParentObject.w_DBPERSCA;
                ,DBRECSCA = this.oParentObject.w_DBRECSCA;
                ,DBPERSFR = this.oParentObject.w_DBPERSFR;
                ,DBRECSFR = this.oParentObject.w_DBRECSFR;
                ,DBPERRIC = this.oParentObject.w_DBPERRIC;
                ,DBQTADIS = this.w_QTADIS;
                ,DBDATULT = i_DATSYS;
                ,DB__NOTE = this.oParentObject.w_DESSUP;
                &i_ccchkf. ;
             where;
                DBCODICE = this.w_CODDIS;
                and CPROWNUM = this.w_NUMROW;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into DISTBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODCOM),'DISTBASE','DBCODCOM');
            +",DBDESCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCOM),'DISTBASE','DBDESCOM');
            +",DBFLESPL ="+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'DISTBASE','DBFLESPL');
            +",DBARTCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBARTCOM),'DISTBASE','DBARTCOM');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMROW);
                   )
          else
            update (i_cTable) set;
                DBCODCOM = this.oParentObject.w_DBCODCOM;
                ,DBDESCOM = this.oParentObject.w_DBDESCOM;
                ,DBFLESPL = this.w_FLESPL;
                ,DBARTCOM = this.oParentObject.w_DBARTCOM;
                &i_ccchkf. ;
             where;
                DBCODICE = this.w_CODDIS;
                and CPROWNUM = this.w_NUMROW;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    return
  proc Try_043664B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COM_VARI
    i_nConn=i_TableProp[this.COM_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COM_VARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODICE"+",CVNUMRIF"+",CPROWNUM"+",CPROWORD"+",CVCODART"+",CVCODCOM"+",CVUNIMIS"+",CVQTADIS"+",CVFLESPL"+",CVCOEIMP"+",CVARTCOM"+",CVCOEUM1"+",CVQTAMOV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODDIS),'COM_VARI','CVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'COM_VARI','CVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'COM_VARI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CPROWORD),'COM_VARI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCODART),'COM_VARI','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOMS),'COM_VARI','CVCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVUNIMIS),'COM_VARI','CVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVQTADIS),'COM_VARI','CVQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLESPL),'COM_VARI','CVFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCOEIMP),'COM_VARI','CVCOEIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODARTS),'COM_VARI','CVARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCOEUM1),'COM_VARI','CVCOEUM1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVQTAMOV),'COM_VARI','CVQTAMOV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODICE',this.w_CODDIS,'CVNUMRIF',this.w_ROWNUM,'CPROWNUM',this.w_NUMRIG,'CPROWORD',_Curs_COM_VARI.CPROWORD,'CVCODART',_Curs_COM_VARI.CVCODART,'CVCODCOM',this.w_CODCOMS,'CVUNIMIS',_Curs_COM_VARI.CVUNIMIS,'CVQTADIS',_Curs_COM_VARI.CVQTADIS,'CVFLESPL',this.w_FLESPL,'CVCOEIMP',_Curs_COM_VARI.CVCOEIMP,'CVARTCOM',this.w_CODARTS,'CVCOEUM1',_Curs_COM_VARI.CVCOEUM1)
      insert into (i_cTable) (CVCODICE,CVNUMRIF,CPROWNUM,CPROWORD,CVCODART,CVCODCOM,CVUNIMIS,CVQTADIS,CVFLESPL,CVCOEIMP,CVARTCOM,CVCOEUM1,CVQTAMOV &i_ccchkf. );
         values (;
           this.w_CODDIS;
           ,this.w_ROWNUM;
           ,this.w_NUMRIG;
           ,_Curs_COM_VARI.CPROWORD;
           ,_Curs_COM_VARI.CVCODART;
           ,this.w_CODCOMS;
           ,_Curs_COM_VARI.CVUNIMIS;
           ,_Curs_COM_VARI.CVQTADIS;
           ,this.w_FLESPL;
           ,_Curs_COM_VARI.CVCOEIMP;
           ,this.w_CODARTS;
           ,_Curs_COM_VARI.CVCOEUM1;
           ,_Curs_COM_VARI.CVQTAMOV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='COM_VARI'
    this.cWorkTables[2]='DISTBASE'
    this.cWorkTables[3]='DISMBASE'
    this.cWorkTables[4]='ART_ICOL'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_COM_VARI')
      use in _Curs_COM_VARI
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
