* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_adc                                                        *
*              Carico da produzione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_182]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-29                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_adc"))

* --- Class definition
define class tgsds_adc as StdForm
  Top    = 6
  Left   = 9

  * --- Standard Properties
  Width  = 664
  Height = 412+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=92890217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Constant Properties
  DIS_CARP_IDX = 0
  PIAMPROD_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_DISB_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  DIS_IMPC_IDX = 0
  cFile = "DIS_CARP"
  cKeySelect = "DISERIAL"
  cKeyWhere  = "DISERIAL=this.w_DISERIAL"
  cKeyWhereODBC = '"DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cKeyWhereODBCqualified = '"DIS_CARP.DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cPrg = "gsds_adc"
  cComment = "Carico da produzione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DISERIAL = space(10)
  o_DISERIAL = space(10)
  w_DIDATREG = ctod('  /  /  ')
  o_DIDATREG = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_CFUNC = space(10)
  w_READPAR = space(10)
  w_CC1 = space(5)
  w_CC2 = space(5)
  w_CM1 = space(5)
  w_CM2 = space(5)
  w_CC1A = space(5)
  w_CC2A = space(5)
  w_CM1A = space(5)
  w_CM2A = space(5)
  w_DIRIFPIA = space(10)
  o_DIRIFPIA = space(10)
  w_RIFPIA = space(10)
  w_RIFIMP = space(10)
  w_RIFORD = space(10)
  w_NUMREG = 0
  w_ALFREG = space(2)
  w_DATFIL = ctod('  /  /  ')
  w_DIDESSUP = space(40)
  w_DICAUCAR = space(5)
  o_DICAUCAR = space(5)
  w_DESC1 = space(35)
  w_DIMAGCAR = space(5)
  w_CAUSOR = space(5)
  o_CAUSOR = space(5)
  w_MAGSOR = space(5)
  w_DIFLTICA = space(1)
  w_DIFLVACO = space(1)
  w_DICAUSCA = space(5)
  o_DICAUSCA = space(5)
  w_DIMAGSCA = space(5)
  w_DESC2 = space(35)
  w_CAUSIM = space(5)
  o_CAUSIM = space(5)
  w_MAGSIM = space(5)
  w_DIFLTISC = space(1)
  w_DIMAXLEV = 0
  w_DITIPVAL = space(1)
  o_DITIPVAL = space(1)
  w_DICODESE = space(4)
  w_DINUMINV = space(6)
  w_DICODLIS = space(5)
  o_DICODLIS = space(5)
  w_DATREG = ctod('  /  /  ')
  w_FL1 = space(1)
  w_FLC1 = space(1)
  w_FL2 = space(1)
  w_FLC2 = space(1)
  w_VALESE = space(3)
  w_VALUTA = space(3)
  w_TIPOLN = space(1)
  w_CAOESE = 0
  w_CAOVAL = 0
  w_MAGINV = space(5)
  w_DICAOLIS = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DATIMP = ctod('  /  /  ')
  w_FLEVAD = space(1)
  w_RESCHK = 0
  w_DECLU = 0
  w_DECLG = 0
  w_DECIU = 0
  w_DECIG = 0
  w_DIRIFCAR = space(10)
  w_DIRIFSCA = space(10)
  w_DATCAR = ctod('  /  /  ')
  w_DESC3 = space(35)
  w_DESC4 = space(35)
  w_UBCAR = space(1)
  w_UBSOR = space(1)
  w_UBSIM = space(1)
  w_UBSCA = space(1)
  w_TIPCOS = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DISERIAL = this.W_DISERIAL
  * --- Area Manuale = Declare Variables
  * --- gsds_adc
  * ---- Disattiva il metodo Edit (non posso variare)
  proc ecpEdit()
      * ----
  endproc
  
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_CARP','gsds_adc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_adcPag1","gsds_adc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Carico da produzione")
      .Pages(1).HelpContextID = 68379228
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='PIAMPROD'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='PAR_DISB'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='INVENTAR'
    this.cWorkTables[9]='DIS_IMPC'
    this.cWorkTables[10]='DIS_CARP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_CARP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_CARP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DISERIAL = NVL(DISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_CARP where DISERIAL=KeySet.DISERIAL
    *
    i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_CARP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_CARP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_CARP '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPAR = i_CODAZI
        .w_CC1 = space(5)
        .w_CC2 = space(5)
        .w_CM1 = space(5)
        .w_CM2 = space(5)
        .w_CC1A = space(5)
        .w_CC2A = space(5)
        .w_CM1A = space(5)
        .w_CM2A = space(5)
        .w_RIFIMP = space(10)
        .w_RIFORD = space(10)
        .w_NUMREG = 0
        .w_ALFREG = space(2)
        .w_DESC1 = space(35)
        .w_CAUSOR = space(5)
        .w_DESC2 = space(35)
        .w_CAUSIM = space(5)
        .w_DATREG = ctod("  /  /  ")
        .w_FL1 = space(1)
        .w_FLC1 = space(1)
        .w_FL2 = space(1)
        .w_FLC2 = space(1)
        .w_VALESE = space(3)
        .w_VALUTA = space(3)
        .w_TIPOLN = space(1)
        .w_CAOESE = 0
        .w_CAOVAL = 0
        .w_MAGINV = space(5)
        .w_OBTEST = i_INIDAT
        .w_DATOBSO = ctod("  /  /  ")
        .w_DATIMP = ctod("  /  /  ")
        .w_FLEVAD = space(1)
        .w_RESCHK = 0
        .w_DECLU = 0
        .w_DECLG = 0
        .w_DECIU = 0
        .w_DECIG = 0
        .w_DATCAR = ctod("  /  /  ")
        .w_DESC3 = space(35)
        .w_DESC4 = space(35)
        .w_UBCAR = space(1)
        .w_UBSOR = space(1)
        .w_UBSIM = space(1)
        .w_UBSCA = space(1)
        .w_TIPCOS = space(1)
        .w_DISERIAL = NVL(DISERIAL,space(10))
        .op_DISERIAL = .w_DISERIAL
        .w_DIDATREG = NVL(cp_ToDate(DIDATREG),ctod("  /  /  "))
        .w_CODAZI = i_CODAZI
        .w_CFUNC = this.cFunction
          .link_1_5('Load')
        .w_DIRIFPIA = NVL(DIRIFPIA,space(10))
          if link_1_14_joined
            this.w_DIRIFPIA = NVL(PPSERIAL114,NVL(this.w_DIRIFPIA,space(10)))
            this.w_NUMREG = NVL(PPNUMREG114,0)
            this.w_ALFREG = NVL(PPALFREG114,space(2))
            this.w_DATREG = NVL(cp_ToDate(PPDATREG114),ctod("  /  /  "))
            this.w_FLEVAD = NVL(PPFLEVAD114,space(1))
            this.w_DATIMP = NVL(cp_ToDate(PPDATIMP114),ctod("  /  /  "))
            this.w_DATCAR = NVL(cp_ToDate(PPDATCAR114),ctod("  /  /  "))
          else
          .link_1_14('Load')
          endif
        .w_RIFPIA = .w_DIRIFPIA
          .link_1_15('Load')
        .w_DATFIL = CP_TODATE(.w_DATREG)
        .w_DIDESSUP = NVL(DIDESSUP,space(40))
        .w_DICAUCAR = NVL(DICAUCAR,space(5))
          if link_1_23_joined
            this.w_DICAUCAR = NVL(CMCODICE123,NVL(this.w_DICAUCAR,space(5)))
            this.w_DESC1 = NVL(CMDESCRI123,space(35))
            this.w_FLC1 = NVL(CMFLCLFR123,space(1))
            this.w_FL1 = NVL(CMFLCASC123,space(1))
            this.w_CAUSOR = NVL(CMCAUCOL123,space(5))
          else
          .link_1_23('Load')
          endif
        .w_DIMAGCAR = NVL(DIMAGCAR,space(5))
          if link_1_25_joined
            this.w_DIMAGCAR = NVL(MGCODMAG125,NVL(this.w_DIMAGCAR,space(5)))
            this.w_UBCAR = NVL(MGFLUBIC125,space(1))
          else
          .link_1_25('Load')
          endif
          .link_1_26('Load')
        .w_MAGSOR = IIF(EMPTY(.w_CAUSOR), SPACE(5), .w_CM1A)
          .link_1_27('Load')
        .w_DIFLTICA = NVL(DIFLTICA,space(1))
        .w_DIFLVACO = NVL(DIFLVACO,space(1))
        .w_DICAUSCA = NVL(DICAUSCA,space(5))
          if link_1_30_joined
            this.w_DICAUSCA = NVL(CMCODICE130,NVL(this.w_DICAUSCA,space(5)))
            this.w_DESC2 = NVL(CMDESCRI130,space(35))
            this.w_FLC2 = NVL(CMFLCLFR130,space(1))
            this.w_FL2 = NVL(CMFLCASC130,space(1))
            this.w_CAUSIM = NVL(CMCAUCOL130,space(5))
          else
          .link_1_30('Load')
          endif
        .w_DIMAGSCA = NVL(DIMAGSCA,space(5))
          if link_1_31_joined
            this.w_DIMAGSCA = NVL(MGCODMAG131,NVL(this.w_DIMAGSCA,space(5)))
            this.w_UBSCA = NVL(MGFLUBIC131,space(1))
          else
          .link_1_31('Load')
          endif
          .link_1_33('Load')
        .w_MAGSIM = IIF(EMPTY(.w_CAUSIM), SPACE(5), .w_CM2A)
          .link_1_34('Load')
        .w_DIFLTISC = NVL(DIFLTISC,space(1))
        .w_DIMAXLEV = NVL(DIMAXLEV,0)
        .w_DITIPVAL = NVL(DITIPVAL,space(1))
        .w_DICODESE = NVL(DICODESE,space(4))
          .link_1_38('Load')
        .w_DINUMINV = NVL(DINUMINV,space(6))
          .link_1_39('Load')
        .w_DICODLIS = NVL(DICODLIS,space(5))
          if link_1_40_joined
            this.w_DICODLIS = NVL(LSCODLIS140,NVL(this.w_DICODLIS,space(5)))
            this.w_VALUTA = NVL(LSVALLIS140,space(3))
            this.w_TIPOLN = NVL(LSIVALIS140,space(1))
          else
          .link_1_40('Load')
          endif
          .link_1_62('Load')
          .link_1_63('Load')
        .w_DICAOLIS = NVL(DICAOLIS,0)
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .w_DIRIFCAR = NVL(DIRIFCAR,space(10))
        .w_DIRIFSCA = NVL(DIRIFSCA,space(10))
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIS_CARP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DISERIAL = space(10)
      .w_DIDATREG = ctod("  /  /  ")
      .w_CODAZI = space(5)
      .w_CFUNC = space(10)
      .w_READPAR = space(10)
      .w_CC1 = space(5)
      .w_CC2 = space(5)
      .w_CM1 = space(5)
      .w_CM2 = space(5)
      .w_CC1A = space(5)
      .w_CC2A = space(5)
      .w_CM1A = space(5)
      .w_CM2A = space(5)
      .w_DIRIFPIA = space(10)
      .w_RIFPIA = space(10)
      .w_RIFIMP = space(10)
      .w_RIFORD = space(10)
      .w_NUMREG = 0
      .w_ALFREG = space(2)
      .w_DATFIL = ctod("  /  /  ")
      .w_DIDESSUP = space(40)
      .w_DICAUCAR = space(5)
      .w_DESC1 = space(35)
      .w_DIMAGCAR = space(5)
      .w_CAUSOR = space(5)
      .w_MAGSOR = space(5)
      .w_DIFLTICA = space(1)
      .w_DIFLVACO = space(1)
      .w_DICAUSCA = space(5)
      .w_DIMAGSCA = space(5)
      .w_DESC2 = space(35)
      .w_CAUSIM = space(5)
      .w_MAGSIM = space(5)
      .w_DIFLTISC = space(1)
      .w_DIMAXLEV = 0
      .w_DITIPVAL = space(1)
      .w_DICODESE = space(4)
      .w_DINUMINV = space(6)
      .w_DICODLIS = space(5)
      .w_DATREG = ctod("  /  /  ")
      .w_FL1 = space(1)
      .w_FLC1 = space(1)
      .w_FL2 = space(1)
      .w_FLC2 = space(1)
      .w_VALESE = space(3)
      .w_VALUTA = space(3)
      .w_TIPOLN = space(1)
      .w_CAOESE = 0
      .w_CAOVAL = 0
      .w_MAGINV = space(5)
      .w_DICAOLIS = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DATIMP = ctod("  /  /  ")
      .w_FLEVAD = space(1)
      .w_RESCHK = 0
      .w_DECLU = 0
      .w_DECLG = 0
      .w_DECIU = 0
      .w_DECIG = 0
      .w_DIRIFCAR = space(10)
      .w_DIRIFSCA = space(10)
      .w_DATCAR = ctod("  /  /  ")
      .w_DESC3 = space(35)
      .w_DESC4 = space(35)
      .w_UBCAR = space(1)
      .w_UBSOR = space(1)
      .w_UBSIM = space(1)
      .w_UBSCA = space(1)
      .w_TIPCOS = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DIDATREG = i_DATSYS
        .w_CODAZI = i_CODAZI
        .w_CFUNC = this.cFunction
        .w_READPAR = i_CODAZI
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_READPAR))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,14,.f.)
          if not(empty(.w_DIRIFPIA))
          .link_1_14('Full')
          endif
        .w_RIFPIA = .w_DIRIFPIA
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_RIFPIA))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,19,.f.)
        .w_DATFIL = CP_TODATE(.w_DATREG)
          .DoRTCalc(21,21,.f.)
        .w_DICAUCAR = IIF(EMPTY(.w_DATIMP), .w_CC1A, .w_CC1)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_DICAUCAR))
          .link_1_23('Full')
          endif
          .DoRTCalc(23,23,.f.)
        .w_DIMAGCAR = .w_CM1
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_DIMAGCAR))
          .link_1_25('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_CAUSOR))
          .link_1_26('Full')
          endif
        .w_MAGSOR = IIF(EMPTY(.w_CAUSOR), SPACE(5), .w_CM1A)
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_MAGSOR))
          .link_1_27('Full')
          endif
        .w_DIFLTICA = IIF(NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUCAR) AND NOT EMPTY(.w_RIFORD), 'S', 'N')
        .w_DIFLVACO = ' '
        .w_DICAUSCA = IIF(EMPTY(.w_DATIMP), .w_CC2A, .w_CC2)
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_DICAUSCA))
          .link_1_30('Full')
          endif
        .w_DIMAGSCA = .w_CM2
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_DIMAGSCA))
          .link_1_31('Full')
          endif
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_CAUSIM))
          .link_1_33('Full')
          endif
        .w_MAGSIM = IIF(EMPTY(.w_CAUSIM), SPACE(5), .w_CM2A)
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_MAGSIM))
          .link_1_34('Full')
          endif
        .w_DIFLTISC = IIF(NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUSCA) AND NOT EMPTY(.w_RIFIMP), 'S', 'N')
        .w_DIMAXLEV = 99
        .w_DITIPVAL = 'S'
        .w_DICODESE = g_CODESE
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_DICODESE))
          .link_1_38('Full')
          endif
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_DINUMINV))
          .link_1_39('Full')
          endif
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_DICODLIS))
          .link_1_40('Full')
          endif
        .DoRTCalc(40,45,.f.)
          if not(empty(.w_VALESE))
          .link_1_62('Full')
          endif
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_VALUTA))
          .link_1_63('Full')
          endif
          .DoRTCalc(47,50,.f.)
        .w_DICAOLIS = IIF(.w_CAOVAL=0, GETCAM(.w_VALUTA, .w_DIDATREG, 7), .w_CAOVAL)
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
          .DoRTCalc(53,55,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_CARP')
    this.DoRTCalc(57,70,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDISCAR","i_CODAZI,w_DISERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DISERIAL = .w_DISERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDIDATREG_1_2.enabled = i_bVal
      .Page1.oPag.oNUMREG_1_18.enabled = i_bVal
      .Page1.oPag.oALFREG_1_19.enabled = i_bVal
      .Page1.oPag.oDATFIL_1_20.enabled = i_bVal
      .Page1.oPag.oDIDESSUP_1_22.enabled = i_bVal
      .Page1.oPag.oDICAUCAR_1_23.enabled = i_bVal
      .Page1.oPag.oDIMAGCAR_1_25.enabled = i_bVal
      .Page1.oPag.oMAGSOR_1_27.enabled = i_bVal
      .Page1.oPag.oDIFLTICA_1_28.enabled = i_bVal
      .Page1.oPag.oDIFLVACO_1_29.enabled = i_bVal
      .Page1.oPag.oDICAUSCA_1_30.enabled = i_bVal
      .Page1.oPag.oDIMAGSCA_1_31.enabled = i_bVal
      .Page1.oPag.oMAGSIM_1_34.enabled = i_bVal
      .Page1.oPag.oDIFLTISC_1_35.enabled = i_bVal
      .Page1.oPag.oDIMAXLEV_1_36.enabled = i_bVal
      .Page1.oPag.oDITIPVAL_1_37.enabled = i_bVal
      .Page1.oPag.oDICODESE_1_38.enabled = i_bVal
      .Page1.oPag.oDINUMINV_1_39.enabled = i_bVal
      .Page1.oPag.oDICODLIS_1_40.enabled = i_bVal
      .Page1.oPag.oDICAOLIS_1_70.enabled = i_bVal
      .Page1.oPag.oBtn_1_21.enabled = .Page1.oPag.oBtn_1_21.mCond()
      .Page1.oPag.oBtn_1_73.enabled = i_bVal
      .Page1.oPag.oBtn_1_74.enabled = .Page1.oPag.oBtn_1_74.mCond()
      .Page1.oPag.oBtn_1_87.enabled = .Page1.oPag.oBtn_1_87.mCond()
      .Page1.oPag.oBtn_1_88.enabled = .Page1.oPag.oBtn_1_88.mCond()
      .Page1.oPag.oObj_1_75.enabled = i_bVal
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      .Page1.oPag.oObj_1_79.enabled = i_bVal
      .Page1.oPag.oObj_1_89.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oDIDATREG_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DIS_CARP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERIAL,"DISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATREG,"DIDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFPIA,"DIRIFPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDESSUP,"DIDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUCAR,"DICAUCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAGCAR,"DIMAGCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLTICA,"DIFLTICA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLVACO,"DIFLVACO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUSCA,"DICAUSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAGSCA,"DIMAGSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLTISC,"DIFLTISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIMAXLEV,"DIMAXLEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPVAL,"DITIPVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODESE,"DICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMINV,"DINUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODLIS,"DICODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAOLIS,"DICAOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFCAR,"DIRIFCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFSCA,"DIRIFSCA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
    i_lTable = "DIS_CARP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_CARP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_CARP_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDISCAR","i_CODAZI,w_DISERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_CARP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_CARP')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_CARP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DISERIAL,DIDATREG,DIRIFPIA,DIDESSUP,DICAUCAR"+;
                  ",DIMAGCAR,DIFLTICA,DIFLVACO,DICAUSCA,DIMAGSCA"+;
                  ",DIFLTISC,DIMAXLEV,DITIPVAL,DICODESE,DINUMINV"+;
                  ",DICODLIS,DICAOLIS,DIRIFCAR,DIRIFSCA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DISERIAL)+;
                  ","+cp_ToStrODBC(this.w_DIDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_DIRIFPIA)+;
                  ","+cp_ToStrODBC(this.w_DIDESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_DICAUCAR)+;
                  ","+cp_ToStrODBCNull(this.w_DIMAGCAR)+;
                  ","+cp_ToStrODBC(this.w_DIFLTICA)+;
                  ","+cp_ToStrODBC(this.w_DIFLVACO)+;
                  ","+cp_ToStrODBCNull(this.w_DICAUSCA)+;
                  ","+cp_ToStrODBCNull(this.w_DIMAGSCA)+;
                  ","+cp_ToStrODBC(this.w_DIFLTISC)+;
                  ","+cp_ToStrODBC(this.w_DIMAXLEV)+;
                  ","+cp_ToStrODBC(this.w_DITIPVAL)+;
                  ","+cp_ToStrODBCNull(this.w_DICODESE)+;
                  ","+cp_ToStrODBCNull(this.w_DINUMINV)+;
                  ","+cp_ToStrODBCNull(this.w_DICODLIS)+;
                  ","+cp_ToStrODBC(this.w_DICAOLIS)+;
                  ","+cp_ToStrODBC(this.w_DIRIFCAR)+;
                  ","+cp_ToStrODBC(this.w_DIRIFSCA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_CARP')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_CARP')
        cp_CheckDeletedKey(i_cTable,0,'DISERIAL',this.w_DISERIAL)
        INSERT INTO (i_cTable);
              (DISERIAL,DIDATREG,DIRIFPIA,DIDESSUP,DICAUCAR,DIMAGCAR,DIFLTICA,DIFLVACO,DICAUSCA,DIMAGSCA,DIFLTISC,DIMAXLEV,DITIPVAL,DICODESE,DINUMINV,DICODLIS,DICAOLIS,DIRIFCAR,DIRIFSCA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DISERIAL;
                  ,this.w_DIDATREG;
                  ,this.w_DIRIFPIA;
                  ,this.w_DIDESSUP;
                  ,this.w_DICAUCAR;
                  ,this.w_DIMAGCAR;
                  ,this.w_DIFLTICA;
                  ,this.w_DIFLVACO;
                  ,this.w_DICAUSCA;
                  ,this.w_DIMAGSCA;
                  ,this.w_DIFLTISC;
                  ,this.w_DIMAXLEV;
                  ,this.w_DITIPVAL;
                  ,this.w_DICODESE;
                  ,this.w_DINUMINV;
                  ,this.w_DICODLIS;
                  ,this.w_DICAOLIS;
                  ,this.w_DIRIFCAR;
                  ,this.w_DIRIFSCA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_CARP_IDX,i_nConn)
      *
      * update DIS_CARP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_CARP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DIDATREG="+cp_ToStrODBC(this.w_DIDATREG)+;
             ",DIRIFPIA="+cp_ToStrODBCNull(this.w_DIRIFPIA)+;
             ",DIDESSUP="+cp_ToStrODBC(this.w_DIDESSUP)+;
             ",DICAUCAR="+cp_ToStrODBCNull(this.w_DICAUCAR)+;
             ",DIMAGCAR="+cp_ToStrODBCNull(this.w_DIMAGCAR)+;
             ",DIFLTICA="+cp_ToStrODBC(this.w_DIFLTICA)+;
             ",DIFLVACO="+cp_ToStrODBC(this.w_DIFLVACO)+;
             ",DICAUSCA="+cp_ToStrODBCNull(this.w_DICAUSCA)+;
             ",DIMAGSCA="+cp_ToStrODBCNull(this.w_DIMAGSCA)+;
             ",DIFLTISC="+cp_ToStrODBC(this.w_DIFLTISC)+;
             ",DIMAXLEV="+cp_ToStrODBC(this.w_DIMAXLEV)+;
             ",DITIPVAL="+cp_ToStrODBC(this.w_DITIPVAL)+;
             ",DICODESE="+cp_ToStrODBCNull(this.w_DICODESE)+;
             ",DINUMINV="+cp_ToStrODBCNull(this.w_DINUMINV)+;
             ",DICODLIS="+cp_ToStrODBCNull(this.w_DICODLIS)+;
             ",DICAOLIS="+cp_ToStrODBC(this.w_DICAOLIS)+;
             ",DIRIFCAR="+cp_ToStrODBC(this.w_DIRIFCAR)+;
             ",DIRIFSCA="+cp_ToStrODBC(this.w_DIRIFSCA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_CARP')
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        UPDATE (i_cTable) SET;
              DIDATREG=this.w_DIDATREG;
             ,DIRIFPIA=this.w_DIRIFPIA;
             ,DIDESSUP=this.w_DIDESSUP;
             ,DICAUCAR=this.w_DICAUCAR;
             ,DIMAGCAR=this.w_DIMAGCAR;
             ,DIFLTICA=this.w_DIFLTICA;
             ,DIFLVACO=this.w_DIFLVACO;
             ,DICAUSCA=this.w_DICAUSCA;
             ,DIMAGSCA=this.w_DIMAGSCA;
             ,DIFLTISC=this.w_DIFLTISC;
             ,DIMAXLEV=this.w_DIMAXLEV;
             ,DITIPVAL=this.w_DITIPVAL;
             ,DICODESE=this.w_DICODESE;
             ,DINUMINV=this.w_DINUMINV;
             ,DICODLIS=this.w_DICODLIS;
             ,DICAOLIS=this.w_DICAOLIS;
             ,DIRIFCAR=this.w_DIRIFCAR;
             ,DIRIFSCA=this.w_DIRIFSCA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsds_adc
    this.Notifyevent('Elimina')
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_CARP_IDX,i_nConn)
      *
      * delete DIS_CARP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_CARP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_CARP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CODAZI = i_CODAZI
        if .o_DISERIAL<>.w_DISERIAL
            .w_CFUNC = this.cFunction
        endif
          .link_1_5('Full')
        .DoRTCalc(6,13,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
          .link_1_14('Full')
        endif
        if .o_DIRIFPIA<>.w_DIRIFPIA
            .w_RIFPIA = .w_DIRIFPIA
          .link_1_15('Full')
        endif
        .DoRTCalc(16,19,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
            .w_DATFIL = CP_TODATE(.w_DATREG)
        endif
        .DoRTCalc(21,21,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
            .w_DICAUCAR = IIF(EMPTY(.w_DATIMP), .w_CC1A, .w_CC1)
          .link_1_23('Full')
        endif
        .DoRTCalc(23,24,.t.)
          .link_1_26('Full')
        if .o_CAUSOR<>.w_CAUSOR
            .w_MAGSOR = IIF(EMPTY(.w_CAUSOR), SPACE(5), .w_CM1A)
          .link_1_27('Full')
        endif
        if .o_DIRIFPIA<>.w_DIRIFPIA.or. .o_DICAUCAR<>.w_DICAUCAR
            .w_DIFLTICA = IIF(NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUCAR) AND NOT EMPTY(.w_RIFORD), 'S', 'N')
        endif
        .DoRTCalc(28,28,.t.)
        if .o_DIRIFPIA<>.w_DIRIFPIA
            .w_DICAUSCA = IIF(EMPTY(.w_DATIMP), .w_CC2A, .w_CC2)
          .link_1_30('Full')
        endif
        .DoRTCalc(30,31,.t.)
          .link_1_33('Full')
        if .o_CAUSIM<>.w_CAUSIM
            .w_MAGSIM = IIF(EMPTY(.w_CAUSIM), SPACE(5), .w_CM2A)
          .link_1_34('Full')
        endif
        if .o_DIRIFPIA<>.w_DIRIFPIA.or. .o_DICAUSCA<>.w_DICAUSCA
            .w_DIFLTISC = IIF(NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUSCA) AND NOT EMPTY(.w_RIFIMP), 'S', 'N')
        endif
        .DoRTCalc(35,36,.t.)
        if .o_DITIPVAL<>.w_DITIPVAL
          .link_1_38('Full')
        endif
        .DoRTCalc(38,38,.t.)
        if .o_DITIPVAL<>.w_DITIPVAL
          .link_1_40('Full')
        endif
        .DoRTCalc(40,44,.t.)
          .link_1_62('Full')
          .link_1_63('Full')
        .DoRTCalc(47,50,.t.)
        if .o_DIDATREG<>.w_DIDATREG.or. .o_DICODLIS<>.w_DICODLIS
            .w_DICAOLIS = IIF(.w_CAOVAL=0, GETCAM(.w_VALUTA, .w_DIDATREG, 7), .w_CAOVAL)
        endif
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEDISCAR","i_CODAZI,w_DISERIAL")
          .op_DISERIAL = .w_DISERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(52,70,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_75.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMREG_1_18.enabled = this.oPgFrm.Page1.oPag.oNUMREG_1_18.mCond()
    this.oPgFrm.Page1.oPag.oALFREG_1_19.enabled = this.oPgFrm.Page1.oPag.oALFREG_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDATFIL_1_20.enabled = this.oPgFrm.Page1.oPag.oDATFIL_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDIMAGCAR_1_25.enabled = this.oPgFrm.Page1.oPag.oDIMAGCAR_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMAGSOR_1_27.enabled = this.oPgFrm.Page1.oPag.oMAGSOR_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDIFLTICA_1_28.enabled = this.oPgFrm.Page1.oPag.oDIFLTICA_1_28.mCond()
    this.oPgFrm.Page1.oPag.oDIMAGSCA_1_31.enabled = this.oPgFrm.Page1.oPag.oDIMAGSCA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oMAGSIM_1_34.enabled = this.oPgFrm.Page1.oPag.oMAGSIM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oDIFLTISC_1_35.enabled = this.oPgFrm.Page1.oPag.oDIFLTISC_1_35.mCond()
    this.oPgFrm.Page1.oPag.oDIMAXLEV_1_36.enabled = this.oPgFrm.Page1.oPag.oDIMAXLEV_1_36.mCond()
    this.oPgFrm.Page1.oPag.oDICODESE_1_38.enabled = this.oPgFrm.Page1.oPag.oDICODESE_1_38.mCond()
    this.oPgFrm.Page1.oPag.oDINUMINV_1_39.enabled = this.oPgFrm.Page1.oPag.oDINUMINV_1_39.mCond()
    this.oPgFrm.Page1.oPag.oDICODLIS_1_40.enabled = this.oPgFrm.Page1.oPag.oDICODLIS_1_40.mCond()
    this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.enabled = this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAUSOR_1_26.visible=!this.oPgFrm.Page1.oPag.oCAUSOR_1_26.mHide()
    this.oPgFrm.Page1.oPag.oMAGSOR_1_27.visible=!this.oPgFrm.Page1.oPag.oMAGSOR_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCAUSIM_1_33.visible=!this.oPgFrm.Page1.oPag.oCAUSIM_1_33.mHide()
    this.oPgFrm.Page1.oPag.oMAGSIM_1_34.visible=!this.oPgFrm.Page1.oPag.oMAGSIM_1_34.mHide()
    this.oPgFrm.Page1.oPag.oDICODESE_1_38.visible=!this.oPgFrm.Page1.oPag.oDICODESE_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDINUMINV_1_39.visible=!this.oPgFrm.Page1.oPag.oDINUMINV_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDICODLIS_1_40.visible=!this.oPgFrm.Page1.oPag.oDICODLIS_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oVALUTA_1_63.visible=!this.oPgFrm.Page1.oPag.oVALUTA_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.visible=!this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_87.visible=!this.oPgFrm.Page1.oPag.oBtn_1_87.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_88.visible=!this.oPgFrm.Page1.oPag.oBtn_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oDESC3_1_95.visible=!this.oPgFrm.Page1.oPag.oDESC3_1_95.mHide()
    this.oPgFrm.Page1.oPag.oDESC4_1_96.visible=!this.oPgFrm.Page1.oPag.oDESC4_1_96.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_75.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_89.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDCAUCAR,PDCAUSCA,PDMAGCAR,PDMAGSCA,PDCAUCA2,PDCAUSC2,PDMAGORD,PDMAGIMP,PDCOSPAR";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_READPAR)
            select PDCODAZI,PDCAUCAR,PDCAUSCA,PDMAGCAR,PDMAGSCA,PDCAUCA2,PDCAUSC2,PDMAGORD,PDMAGIMP,PDCOSPAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCODAZI,space(10))
      this.w_CC1 = NVL(_Link_.PDCAUCAR,space(5))
      this.w_CC2 = NVL(_Link_.PDCAUSCA,space(5))
      this.w_CM1 = NVL(_Link_.PDMAGCAR,space(5))
      this.w_CM2 = NVL(_Link_.PDMAGSCA,space(5))
      this.w_CC1A = NVL(_Link_.PDCAUCA2,space(5))
      this.w_CC2A = NVL(_Link_.PDCAUSC2,space(5))
      this.w_CM1A = NVL(_Link_.PDMAGORD,space(5))
      this.w_CM2A = NVL(_Link_.PDMAGIMP,space(5))
      this.w_TIPCOS = NVL(_Link_.PDCOSPAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(10)
      endif
      this.w_CC1 = space(5)
      this.w_CC2 = space(5)
      this.w_CM1 = space(5)
      this.w_CM2 = space(5)
      this.w_CC1A = space(5)
      this.w_CC2A = space(5)
      this.w_CM1A = space(5)
      this.w_CM2A = space(5)
      this.w_TIPCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIRIFPIA
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PIAMPROD_IDX,3]
    i_lTable = "PIAMPROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2], .t., this.PIAMPROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIRIFPIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIRIFPIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPSERIAL,PPNUMREG,PPALFREG,PPDATREG,PPFLEVAD,PPDATIMP,PPDATCAR";
                   +" from "+i_cTable+" "+i_lTable+" where PPSERIAL="+cp_ToStrODBC(this.w_DIRIFPIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPSERIAL',this.w_DIRIFPIA)
            select PPSERIAL,PPNUMREG,PPALFREG,PPDATREG,PPFLEVAD,PPDATIMP,PPDATCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIRIFPIA = NVL(_Link_.PPSERIAL,space(10))
      this.w_NUMREG = NVL(_Link_.PPNUMREG,0)
      this.w_ALFREG = NVL(_Link_.PPALFREG,space(2))
      this.w_DATREG = NVL(cp_ToDate(_Link_.PPDATREG),ctod("  /  /  "))
      this.w_FLEVAD = NVL(_Link_.PPFLEVAD,space(1))
      this.w_DATIMP = NVL(cp_ToDate(_Link_.PPDATIMP),ctod("  /  /  "))
      this.w_DATCAR = NVL(cp_ToDate(_Link_.PPDATCAR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIRIFPIA = space(10)
      endif
      this.w_NUMREG = 0
      this.w_ALFREG = space(2)
      this.w_DATREG = ctod("  /  /  ")
      this.w_FLEVAD = space(1)
      this.w_DATIMP = ctod("  /  /  ")
      this.w_DATCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])+'\'+cp_ToStr(_Link_.PPSERIAL,1)
      cp_ShowWarn(i_cKey,this.PIAMPROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIRIFPIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PIAMPROD_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.PPSERIAL as PPSERIAL114"+ ",link_1_14.PPNUMREG as PPNUMREG114"+ ",link_1_14.PPALFREG as PPALFREG114"+ ",link_1_14.PPDATREG as PPDATREG114"+ ",link_1_14.PPFLEVAD as PPFLEVAD114"+ ",link_1_14.PPDATIMP as PPDATIMP114"+ ",link_1_14.PPDATCAR as PPDATCAR114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on DIS_CARP.DIRIFPIA=link_1_14.PPSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and DIS_CARP.DIRIFPIA=link_1_14.PPSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RIFPIA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_IMPC_IDX,3]
    i_lTable = "DIS_IMPC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2], .t., this.DIS_IMPC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFPIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFPIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DIRIFPIA,DIRIFIMP,DIRIFORD";
                   +" from "+i_cTable+" "+i_lTable+" where DIRIFPIA="+cp_ToStrODBC(this.w_RIFPIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DIRIFPIA',this.w_RIFPIA)
            select DIRIFPIA,DIRIFIMP,DIRIFORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFPIA = NVL(_Link_.DIRIFPIA,space(10))
      this.w_RIFIMP = NVL(_Link_.DIRIFIMP,space(10))
      this.w_RIFORD = NVL(_Link_.DIRIFORD,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_RIFPIA = space(10)
      endif
      this.w_RIFIMP = space(10)
      this.w_RIFORD = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_IMPC_IDX,2])+'\'+cp_ToStr(_Link_.DIRIFPIA,1)
      cp_ShowWarn(i_cKey,this.DIS_IMPC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFPIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICAUCAR
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_DICAUCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_DICAUCAR))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUCAR)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUCAR) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oDICAUCAR_1_23'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_ADC.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_DICAUCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_DICAUCAR)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUCAR = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC1 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC1 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL1 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUSOR = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUCAR = space(5)
      endif
      this.w_DESC1 = space(35)
      this.w_FLC1 = space(1)
      this.w_FL1 = space(1)
      this.w_CAUSOR = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale carico prodotti inesistente o incongruente")
        endif
        this.w_DICAUCAR = space(5)
        this.w_DESC1 = space(35)
        this.w_FLC1 = space(1)
        this.w_FL1 = space(1)
        this.w_CAUSOR = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.CMCODICE as CMCODICE123"+ ",link_1_23.CMDESCRI as CMDESCRI123"+ ",link_1_23.CMFLCLFR as CMFLCLFR123"+ ",link_1_23.CMFLCASC as CMFLCASC123"+ ",link_1_23.CMCAUCOL as CMCAUCOL123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on DIS_CARP.DICAUCAR=link_1_23.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and DIS_CARP.DICAUCAR=link_1_23.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIMAGCAR
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIMAGCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DIMAGCAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DIMAGCAR))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIMAGCAR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIMAGCAR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDIMAGCAR_1_25'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIMAGCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DIMAGCAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DIMAGCAR)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIMAGCAR = NVL(_Link_.MGCODMAG,space(5))
      this.w_UBCAR = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DIMAGCAR = space(5)
      endif
      this.w_UBCAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIMAGCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.MGCODMAG as MGCODMAG125"+ ",link_1_25.MGFLUBIC as MGFLUBIC125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on DIS_CARP.DIMAGCAR=link_1_25.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and DIS_CARP.DIMAGCAR=link_1_25.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAUSOR
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSOR)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSOR = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC3 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSOR = space(5)
      endif
      this.w_DESC3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGSOR
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGSOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGSOR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGSOR))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGSOR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGSOR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGSOR_1_27'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGSOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGSOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGSOR)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGSOR = NVL(_Link_.MGCODMAG,space(5))
      this.w_UBSOR = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGSOR = space(5)
      endif
      this.w_UBSOR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGSOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICAUSCA
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_DICAUSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_DICAUSCA))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUSCA)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUSCA) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oDICAUSCA_1_30'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSDS_ADC.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_DICAUSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_DICAUSCA)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLCASC,CMCAUCOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUSCA = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLC2 = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FL2 = NVL(_Link_.CMFLCASC,space(1))
      this.w_CAUSIM = NVL(_Link_.CMCAUCOL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUSCA = space(5)
      endif
      this.w_DESC2 = space(35)
      this.w_FLC2 = space(1)
      this.w_FL2 = space(1)
      this.w_CAUSIM = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DICAUSCA) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale scarico componenti inesistente o incongruente")
        endif
        this.w_DICAUSCA = space(5)
        this.w_DESC2 = space(35)
        this.w_FLC2 = space(1)
        this.w_FL2 = space(1)
        this.w_CAUSIM = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.CMCODICE as CMCODICE130"+ ",link_1_30.CMDESCRI as CMDESCRI130"+ ",link_1_30.CMFLCLFR as CMFLCLFR130"+ ",link_1_30.CMFLCASC as CMFLCASC130"+ ",link_1_30.CMCAUCOL as CMCAUCOL130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on DIS_CARP.DICAUSCA=link_1_30.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and DIS_CARP.DICAUSCA=link_1_30.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DIMAGSCA
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIMAGSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DIMAGSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DIMAGSCA))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIMAGSCA)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIMAGSCA) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDIMAGSCA_1_31'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIMAGSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DIMAGSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DIMAGSCA)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIMAGSCA = NVL(_Link_.MGCODMAG,space(5))
      this.w_UBSCA = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DIMAGSCA = space(5)
      endif
      this.w_UBSCA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIMAGSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.MGCODMAG as MGCODMAG131"+ ",link_1_31.MGFLUBIC as MGFLUBIC131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on DIS_CARP.DIMAGSCA=link_1_31.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and DIS_CARP.DIMAGSCA=link_1_31.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAUSIM
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUSIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUSIM)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSIM = NVL(_Link_.CMCODICE,space(5))
      this.w_DESC4 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSIM = space(5)
      endif
      this.w_DESC4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGSIM
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGSIM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGSIM)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGSIM))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGSIM)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGSIM) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGSIM_1_34'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGSIM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGSIM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGSIM)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGSIM = NVL(_Link_.MGCODMAG,space(5))
      this.w_UBSIM = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGSIM = space(5)
      endif
      this.w_UBSIM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGSIM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODESE
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_DICODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_DICODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oDICODESE_1_38'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_DICODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DICODESE = space(4)
      endif
      this.w_VALESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DINUMINV
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DINUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_DINUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_DICODESE;
                     ,'INNUMINV',trim(this.w_DINUMINV))
          select INCODESE,INNUMINV,INCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DINUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DINUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oDINUMINV_1_39'),i_cWhere,'',"Elenco inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DICODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Numero inventario errato o mancante")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DINUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_DINUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_DICODESE;
                       ,'INNUMINV',this.w_DINUMINV)
            select INCODESE,INNUMINV,INCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DINUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_DICODESE = NVL(_Link_.INCODESE,space(4))
      this.w_MAGINV = NVL(_Link_.INCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DINUMINV = space(6)
      endif
      this.w_DICODESE = space(4)
      this.w_MAGINV = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DINUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODLIS
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DICODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DICODLIS))
          select LSCODLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDICODLIS_1_40'),i_cWhere,'GSAR_ALI',"Elenco listini",'GSMAPDV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DICODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DICODLIS)
            select LSCODLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPOLN = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICODLIS = space(5)
      endif
      this.w_VALUTA = space(3)
      this.w_TIPOLN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.LSCODLIS as LSCODLIS140"+ ",link_1_40.LSVALLIS as LSVALLIS140"+ ",link_1_40.LSIVALIS as LSIVALIS140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on DIS_CARP.DICODLIS=link_1_40.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and DIS_CARP.DICODLIS=link_1_40.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALESE
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL,VACAOVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOESE = NVL(_Link_.VACAOVAL,0)
      this.w_DECIU = NVL(_Link_.VADECUNI,0)
      this.w_DECIG = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
      this.w_CAOESE = 0
      this.w_DECIU = 0
      this.w_DECIG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VACAOVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECLU = NVL(_Link_.VADECUNI,0)
      this.w_DECLG = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECLU = 0
      this.w_DECLG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDIDATREG_1_2.value==this.w_DIDATREG)
      this.oPgFrm.Page1.oPag.oDIDATREG_1_2.value=this.w_DIDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_18.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_18.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oALFREG_1_19.value==this.w_ALFREG)
      this.oPgFrm.Page1.oPag.oALFREG_1_19.value=this.w_ALFREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIL_1_20.value==this.w_DATFIL)
      this.oPgFrm.Page1.oPag.oDATFIL_1_20.value=this.w_DATFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDESSUP_1_22.value==this.w_DIDESSUP)
      this.oPgFrm.Page1.oPag.oDIDESSUP_1_22.value=this.w_DIDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUCAR_1_23.value==this.w_DICAUCAR)
      this.oPgFrm.Page1.oPag.oDICAUCAR_1_23.value=this.w_DICAUCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_24.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_24.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAGCAR_1_25.value==this.w_DIMAGCAR)
      this.oPgFrm.Page1.oPag.oDIMAGCAR_1_25.value=this.w_DIMAGCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSOR_1_26.value==this.w_CAUSOR)
      this.oPgFrm.Page1.oPag.oCAUSOR_1_26.value=this.w_CAUSOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGSOR_1_27.value==this.w_MAGSOR)
      this.oPgFrm.Page1.oPag.oMAGSOR_1_27.value=this.w_MAGSOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLTICA_1_28.RadioValue()==this.w_DIFLTICA)
      this.oPgFrm.Page1.oPag.oDIFLTICA_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLVACO_1_29.RadioValue()==this.w_DIFLVACO)
      this.oPgFrm.Page1.oPag.oDIFLVACO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUSCA_1_30.value==this.w_DICAUSCA)
      this.oPgFrm.Page1.oPag.oDICAUSCA_1_30.value=this.w_DICAUSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAGSCA_1_31.value==this.w_DIMAGSCA)
      this.oPgFrm.Page1.oPag.oDIMAGSCA_1_31.value=this.w_DIMAGSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_32.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_32.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSIM_1_33.value==this.w_CAUSIM)
      this.oPgFrm.Page1.oPag.oCAUSIM_1_33.value=this.w_CAUSIM
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGSIM_1_34.value==this.w_MAGSIM)
      this.oPgFrm.Page1.oPag.oMAGSIM_1_34.value=this.w_MAGSIM
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLTISC_1_35.RadioValue()==this.w_DIFLTISC)
      this.oPgFrm.Page1.oPag.oDIFLTISC_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMAXLEV_1_36.value==this.w_DIMAXLEV)
      this.oPgFrm.Page1.oPag.oDIMAXLEV_1_36.value=this.w_DIMAXLEV
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPVAL_1_37.RadioValue()==this.w_DITIPVAL)
      this.oPgFrm.Page1.oPag.oDITIPVAL_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODESE_1_38.value==this.w_DICODESE)
      this.oPgFrm.Page1.oPag.oDICODESE_1_38.value=this.w_DICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMINV_1_39.value==this.w_DINUMINV)
      this.oPgFrm.Page1.oPag.oDINUMINV_1_39.value=this.w_DINUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODLIS_1_40.value==this.w_DICODLIS)
      this.oPgFrm.Page1.oPag.oDICODLIS_1_40.value=this.w_DICODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_63.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_63.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.value==this.w_DICAOLIS)
      this.oPgFrm.Page1.oPag.oDICAOLIS_1_70.value=this.w_DICAOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDATIMP_1_77.value==this.w_DATIMP)
      this.oPgFrm.Page1.oPag.oDATIMP_1_77.value=this.w_DATIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC3_1_95.value==this.w_DESC3)
      this.oPgFrm.Page1.oPag.oDESC3_1_95.value=this.w_DESC3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC4_1_96.value==this.w_DESC4)
      this.oPgFrm.Page1.oPag.oDESC4_1_96.value=this.w_DESC4
    endif
    cp_SetControlsValueExtFlds(this,'DIS_CARP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DIDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATREG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DIDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DICAUCAR)) or not(.w_FL1 $ '+-' AND NOT .w_FLC1 $ 'CF'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAUCAR_1_23.SetFocus()
            i_bnoObbl = !empty(.w_DICAUCAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale carico prodotti inesistente o incongruente")
          case   (empty(.w_DIMAGCAR))  and (.w_CFUNC<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIMAGCAR_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DIMAGCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MAGSOR))  and not(EMPTY(.w_CAUSOR))  and (NOT EMPTY(.w_CAUSOR) AND .w_CFUNC<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGSOR_1_27.SetFocus()
            i_bnoObbl = !empty(.w_MAGSOR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino evasione ordini inesistente o incongruente")
          case   not(EMPTY(.w_DICAUSCA) OR (.w_FL2 $ '+-' AND NOT .w_FLC2 $ 'CF'))  and not(empty(.w_DICAUSCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAUSCA_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale scarico componenti inesistente o incongruente")
          case   (empty(.w_MAGSIM))  and not(EMPTY(.w_CAUSIM))  and (NOT EMPTY(.w_CAUSIM) AND .w_CFUNC<>'Query')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGSIM_1_34.SetFocus()
            i_bnoObbl = !empty(.w_MAGSIM)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino evasione impegni inesistente o incongruente")
          case   (empty(.w_DICODESE))  and not(NOT .w_DITIPVAL $ 'SMUP')  and (.w_DITIPVAL $ 'SMUP')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODESE_1_38.SetFocus()
            i_bnoObbl = !empty(.w_DICODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DICAOLIS))  and not(NOT (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS)))  and (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAOLIS_1_70.SetFocus()
            i_bnoObbl = !empty(.w_DICAOLIS)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsds_adc
      * --- Controlli Finali
      if i_bRes=.t. and .cFunction<>'Query'
         .w_RESCHK=0
          ah_Msg('Controlli finali...',.T.)
           .NotifyEvent('Controlli')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DISERIAL = this.w_DISERIAL
    this.o_DIDATREG = this.w_DIDATREG
    this.o_DIRIFPIA = this.w_DIRIFPIA
    this.o_DICAUCAR = this.w_DICAUCAR
    this.o_CAUSOR = this.w_CAUSOR
    this.o_DICAUSCA = this.w_DICAUSCA
    this.o_CAUSIM = this.w_CAUSIM
    this.o_DITIPVAL = this.w_DITIPVAL
    this.o_DICODLIS = this.w_DICODLIS
    return

enddefine

* --- Define pages as container
define class tgsds_adcPag1 as StdContainer
  Width  = 660
  height = 412
  stdWidth  = 660
  stdheight = 412
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDIDATREG_1_2 as StdField with uid="EQTIIIQIIY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DIDATREG", cQueryName = "DIDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione della distinta",;
    HelpContextID = 33302909,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=21

  add object oNUMREG_1_18 as StdField with uid="PGRUEHSAMT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 165820970,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=49, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  add object oALFREG_1_19 as StdField with uid="QFZWAZWQBX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ALFREG", cQueryName = "ALFREG",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 165852154,;
   bGlobalFont=.t.,;
    Height=21, Width=36, Left=239, Top=49, InputMask=replicate('X',2)

  func oALFREG_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  add object oDATFIL_1_20 as StdField with uid="CWHHWSWPVG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATFIL", cQueryName = "DATFIL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 78503626,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=320, Top=49

  func oDATFIL_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="FNZMXFRUGH",left=408, top=49, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare un piano di produzione";
    , HelpContextID = 92689194;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      vx_exec("..\DISB\Exe\Query\Gsds_adc.vzm",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFUNC<>'Query')
      endwith
    endif
  endfunc

  add object oDIDESSUP_1_22 as StdField with uid="GVDJEHBYVW",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DIDESSUP", cQueryName = "DIDESSUP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive allegate ai movimenti di magazzino",;
    HelpContextID = 49293702,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=149, Top=77, InputMask=replicate('X',40)

  add object oDICAUCAR_1_23 as StdField with uid="SGHORRHFKB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DICAUCAR", cQueryName = "DICAUCAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale carico prodotti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di carico prodotti per produzione",;
    HelpContextID = 217310840,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=138, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_DICAUCAR"

  func oDICAUCAR_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUCAR_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICAUCAR_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oDICAUCAR_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_ADC.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oDICAUCAR_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_DICAUCAR
     i_obj.ecpSave()
  endproc

  add object oDESC1_1_24 as StdField with uid="IHJASYASFK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 36760266,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=138, InputMask=replicate('X',35)

  add object oDIMAGCAR_1_25 as StdField with uid="CKYQFXUYOM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DIMAGCAR", cQueryName = "DIMAGCAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di carico prodotti",;
    HelpContextID = 231949944,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=593, Top=138, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DIMAGCAR"

  func oDIMAGCAR_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  func oDIMAGCAR_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIMAGCAR_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIMAGCAR_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDIMAGCAR_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oDIMAGCAR_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DIMAGCAR
     i_obj.ecpSave()
  endproc

  add object oCAUSOR_1_26 as StdField with uid="XPWQUUUQWH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CAUSOR", cQueryName = "CAUSOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale collegata di evasione ordine dei prodotti",;
    HelpContextID = 239128282,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=166, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSOR"

  func oCAUSOR_1_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSOR))
    endwith
  endfunc

  func oCAUSOR_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMAGSOR_1_27 as StdField with uid="GPKPBKNKDR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MAGSOR", cQueryName = "MAGSOR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino evasione ordini inesistente o incongruente",;
    ToolTipText = "Magazzino di evasione ordini prodotti",;
    HelpContextID = 239185466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=593, Top=166, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGSOR"

  func oMAGSOR_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUSOR) AND .w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  func oMAGSOR_1_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSOR))
    endwith
  endfunc

  func oMAGSOR_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGSOR_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGSOR_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGSOR_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oMAGSOR_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGSOR
     i_obj.ecpSave()
  endproc


  add object oDIFLTICA_1_28 as StdCombo with uid="LGBMIJYKYV",rtseq=27,rtrep=.f.,left=149,top=194,width=138,height=21;
    , ToolTipText = "Indica se il movimento di carico da generare, utilizzerà come fonte dati il movimento di ordine o il piano di produzione";
    , HelpContextID = 151472503;
    , cFormVar="w_DIFLTICA",RowSource=""+"Movimento di ordine,"+"Piano produzione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIFLTICA_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    ' ')))
  endfunc
  func oDIFLTICA_1_28.GetRadio()
    this.Parent.oContained.w_DIFLTICA = this.RadioValue()
    return .t.
  endfunc

  func oDIFLTICA_1_28.SetRadio()
    this.Parent.oContained.w_DIFLTICA=trim(this.Parent.oContained.w_DIFLTICA)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLTICA=='S',1,;
      iif(this.Parent.oContained.w_DIFLTICA=='N',2,;
      0))
  endfunc

  func oDIFLTICA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUCAR) AND NOT EMPTY(.w_RIFORD) AND .w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  add object oDIFLVACO_1_29 as StdCheck with uid="PMMWGIYNNT",rtseq=28,rtrep=.f.,left=306, top=194, caption="Valorizza sui componenti",;
    ToolTipText = "Se attivo: valorizza i carichi in base alla somma dei componenti + cicli",;
    HelpContextID = 19351941,;
    cFormVar="w_DIFLVACO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDIFLVACO_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDIFLVACO_1_29.GetRadio()
    this.Parent.oContained.w_DIFLVACO = this.RadioValue()
    return .t.
  endfunc

  func oDIFLVACO_1_29.SetRadio()
    this.Parent.oContained.w_DIFLVACO=trim(this.Parent.oContained.w_DIFLVACO)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLVACO=='S',1,;
      0)
  endfunc

  add object oDICAUSCA_1_30 as StdField with uid="PDSZOLVTKT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DICAUSCA", cQueryName = "DICAUSCA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale scarico componenti inesistente o incongruente",;
    ToolTipText = "Causale magazzino di scarico componenti",;
    HelpContextID = 51124599,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_DICAUSCA"

  func oDICAUSCA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUSCA_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICAUSCA_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oDICAUSCA_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSDS_ADC.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oDICAUSCA_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_DICAUSCA
     i_obj.ecpSave()
  endproc

  add object oDIMAGSCA_1_31 as StdField with uid="ZOIIXGWJDR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DIMAGSCA", cQueryName = "DIMAGSCA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di scarico componenti",;
    HelpContextID = 36485495,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=593, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DIMAGSCA"

  func oDIMAGSCA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUSCA) AND .w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  func oDIMAGSCA_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIMAGSCA_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIMAGSCA_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDIMAGSCA_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oDIMAGSCA_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DIMAGSCA
     i_obj.ecpSave()
  endproc

  add object oDESC2_1_32 as StdField with uid="DOSKLSEMDQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 35711690,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=221, InputMask=replicate('X',35)

  add object oCAUSIM_1_33 as StdField with uid="QDDQKAFNAX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CAUSIM", cQueryName = "CAUSIM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale collegata di evasione impegno dei componenti",;
    HelpContextID = 60870362,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=249, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUSIM"

  func oCAUSIM_1_33.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSIM))
    endwith
  endfunc

  func oCAUSIM_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMAGSIM_1_34 as StdField with uid="JNQKBRZHFW",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MAGSIM", cQueryName = "MAGSIM",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino evasione impegni inesistente o incongruente",;
    ToolTipText = "Magazzino di evasione impegni componenti",;
    HelpContextID = 60927546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=593, Top=249, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGSIM"

  func oMAGSIM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUSIM) AND .w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  func oMAGSIM_1_34.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSIM))
    endwith
  endfunc

  func oMAGSIM_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGSIM_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGSIM_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGSIM_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oMAGSIM_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGSIM
     i_obj.ecpSave()
  endproc


  add object oDIFLTISC_1_35 as StdCombo with uid="LEMOPUILPA",rtseq=34,rtrep=.f.,left=149,top=277,width=138,height=21;
    , ToolTipText = "Indica se il movimento di scarico da generare, utilizzerà come fonte dati il movimento di impegno o l'esplosione dei componenti";
    , HelpContextID = 151472505;
    , cFormVar="w_DIFLTISC",RowSource=""+"Movimento di impegno,"+"Esplosione distinte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIFLTISC_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    ' ')))
  endfunc
  func oDIFLTISC_1_35.GetRadio()
    this.Parent.oContained.w_DIFLTISC = this.RadioValue()
    return .t.
  endfunc

  func oDIFLTISC_1_35.SetRadio()
    this.Parent.oContained.w_DIFLTISC=trim(this.Parent.oContained.w_DIFLTISC)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLTISC=='S',1,;
      iif(this.Parent.oContained.w_DIFLTISC=='N',2,;
      0))
  endfunc

  func oDIFLTISC_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATIMP) AND .w_FLEVAD='S' AND NOT EMPTY(.w_DICAUSCA) AND NOT EMPTY(.w_RIFIMP)  AND .w_CFUNC<>'Query')
    endwith
   endif
  endfunc

  add object oDIMAXLEV_1_36 as StdField with uid="XMVJJFLKDX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DIMAXLEV", cQueryName = "DIMAXLEV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero massimo di livelli di esplosione",;
    HelpContextID = 205306252,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=445, Top=278, cSayPict="'99'", cGetPict="'99'"

  func oDIMAXLEV_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIFLTISC='N')
    endwith
   endif
  endfunc


  add object oDITIPVAL_1_37 as StdCombo with uid="YRVEYNBLOI",rtseq=36,rtrep=.f.,left=113,top=337,width=201,height=21;
    , ToolTipText = "Criterio di valorizzazione delle distinte";
    , HelpContextID = 96807298;
    , cFormVar="w_DITIPVAL",RowSource=""+"Costo standard,"+"Costo medio esercizio,"+"Costo medio periodo,"+"Ultimo costo,"+"Costo di listino,"+"Ultimo costo standard (articolo),"+"Ultimo costo dei saldi (articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPVAL_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'P',;
    iif(this.value =4,'U',;
    iif(this.value =5,'L',;
    iif(this.value =6,'X',;
    iif(this.value =7,'A',;
    space(1)))))))))
  endfunc
  func oDITIPVAL_1_37.GetRadio()
    this.Parent.oContained.w_DITIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oDITIPVAL_1_37.SetRadio()
    this.Parent.oContained.w_DITIPVAL=trim(this.Parent.oContained.w_DITIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPVAL=='S',1,;
      iif(this.Parent.oContained.w_DITIPVAL=='M',2,;
      iif(this.Parent.oContained.w_DITIPVAL=='P',3,;
      iif(this.Parent.oContained.w_DITIPVAL=='U',4,;
      iif(this.Parent.oContained.w_DITIPVAL=='L',5,;
      iif(this.Parent.oContained.w_DITIPVAL=='X',6,;
      iif(this.Parent.oContained.w_DITIPVAL=='A',7,;
      0)))))))
  endfunc

  add object oDICODESE_1_38 as StdField with uid="PPCQXSFUWA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DICODESE", cQueryName = "DICODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio dell'inventario",;
    HelpContextID = 67770747,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=379, Top=337, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_DICODESE"

  func oDICODESE_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL $ 'SMUP')
    endwith
   endif
  endfunc

  func oDICODESE_1_38.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  func oDICODESE_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
      if .not. empty(.w_DINUMINV)
        bRes2=.link_1_39('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDICODESE_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODESE_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oDICODESE_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDINUMINV_1_39 as StdField with uid="TLVSQNVIAZ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DINUMINV", cQueryName = "DINUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Numero inventario errato o mancante",;
    ToolTipText = "Numero dell'inventario da elaborare",;
    HelpContextID = 123680372,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=497, Top=337, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", oKey_1_1="INCODESE", oKey_1_2="this.w_DICODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_DINUMINV"

  func oDINUMINV_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL $ 'SMUP' AND NOT EMPTY(.w_DICODESE))
    endwith
   endif
  endfunc

  func oDINUMINV_1_39.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  func oDINUMINV_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oDINUMINV_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDINUMINV_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_DICODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_DICODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oDINUMINV_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco inventari",'',this.parent.oContained
  endproc

  add object oDICODLIS_1_40 as StdField with uid="AGOZDBGXAR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DICODLIS", cQueryName = "DICODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 185211273,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=379, Top=337, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DICODLIS"

  func oDICODLIS_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPVAL='L')
    endwith
   endif
  endfunc

  func oDICODLIS_1_40.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L')
    endwith
  endfunc

  func oDICODLIS_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODLIS_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODLIS_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDICODLIS_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Elenco listini",'GSMAPDV.LISTINI_VZM',this.parent.oContained
  endproc
  proc oDICODLIS_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_DICODLIS
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_63 as StdField with uid="XFQEBGZCIY",rtseq=46,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta associata al listino",;
    HelpContextID = 250568106,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=497, Top=337, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_63.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L' OR EMPTY(.w_DICODLIS))
    endwith
  endfunc

  func oVALUTA_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDICAOLIS_1_70 as StdField with uid="TYWXLNOQQB",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DICAOLIS", cQueryName = "DICAOLIS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio associato alla valuta listino",;
    HelpContextID = 195828105,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=558, Top=337

  func oDICAOLIS_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS))
    endwith
   endif
  endfunc

  func oDICAOLIS_1_70.mHide()
    with this.Parent.oContained
      return (NOT (.w_CAOVAL=0 AND .w_DITIPVAL='L' AND NOT EMPTY(.w_DICODLIS)))
    endwith
  endfunc


  add object oBtn_1_73 as StdButton with uid="QJZVDJKIQR",left=556, top=363, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 92861466;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_73.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CFUNC='Load')
      endwith
    endif
  endfunc


  add object oBtn_1_74 as StdButton with uid="KZSQCYUNAO",left=607, top=363, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 85572794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_74.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_75 as cp_runprogram with uid="XOFORQYQOL",left=-2, top=564, width=245,height=19,;
    caption='GSDS_BNC(INIZIO)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNC('INIZIO')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 28073810


  add object oObj_1_76 as cp_runprogram with uid="KDNFFYCJCR",left=284, top=536, width=204,height=19,;
    caption='GSDS_BCA',;
   bGlobalFont=.t.,;
    prg="GSDS_BCA",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 46019495

  add object oDATIMP_1_77 as StdField with uid="VGAKJSERBV",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DATIMP", cQueryName = "DATIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di avvenuto impegno dei componenti",;
    HelpContextID = 7003850,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=576, Top=49


  add object oObj_1_79 as cp_runprogram with uid="EOVMJGYUSI",left=284, top=564, width=204,height=19,;
    caption='GSDS_BNC(CHECK)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNC('CHECK')",;
    cEvent = "Controlli",;
    nPag=1;
    , HelpContextID = 231212759


  add object oBtn_1_87 as StdButton with uid="KKTWUWGJAF",left=31, top=363, width=48,height=45,;
    CpPicture="bmp\NewOffe.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento di carico prodotti per produzione";
    , HelpContextID = 1515814;
    , Caption='\<Carico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_87.Click()
      with this.Parent.oContained
        GSDS_BNC(this.Parent.oContained,"CARI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_87.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DIRIFCAR) AND .w_CFUNC<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_87.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DIRIFCAR))
     endwith
    endif
  endfunc


  add object oBtn_1_88 as StdButton with uid="FZPWBPRUQW",left=82, top=363, width=48,height=45,;
    CpPicture="bmp\DocDest.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al movimento di scarico componenti";
    , HelpContextID = 192998362;
    , Caption='\<Scarico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_88.Click()
      with this.Parent.oContained
        GSDS_BNC(this.Parent.oContained,"SCAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_88.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DIRIFSCA) AND .w_CFUNC<>'Load')
      endwith
    endif
  endfunc

  func oBtn_1_88.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DIRIFSCA))
     endwith
    endif
  endfunc


  add object oObj_1_89 as cp_runprogram with uid="TVPDLJWAVK",left=-2, top=536, width=245,height=19,;
    caption='GSDS_BNC(ELIMINA)',;
   bGlobalFont=.t.,;
    prg="GSDS_BNC('ELIMINA')",;
    cEvent = "Record Deleted",;
    nPag=1;
    , HelpContextID = 254001670

  add object oDESC3_1_95 as StdField with uid="BRIMCPZQEG",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESC3", cQueryName = "DESC3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34663114,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=166, InputMask=replicate('X',35)

  func oDESC3_1_95.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSOR))
    endwith
  endfunc

  add object oDESC4_1_96 as StdField with uid="VRXSQVKNQN",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESC4", cQueryName = "DESC4",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33614538,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=214, Top=249, InputMask=replicate('X',35)

  func oDESC4_1_96.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSIM))
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="HPFUCYJFQM",Visible=.t., Left=29, Top=49,;
    Alignment=1, Width=117, Height=18,;
    Caption="Piano produzione n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="DPIKMSONLB",Visible=.t., Left=224, Top=49,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="BSUPWQMNDE",Visible=.t., Left=287, Top=49,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="XZARJNGGCG",Visible=.t., Left=29, Top=21,;
    Alignment=1, Width=117, Height=18,;
    Caption="Registrazione del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="CYXYCTLBYK",Visible=.t., Left=29, Top=77,;
    Alignment=1, Width=117, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="YHNPSTMKVJ",Visible=.t., Left=29, Top=108,;
    Alignment=0, Width=561, Height=18,;
    Caption="Causali carico prodotti / scarico componenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="UWMKOEQRMI",Visible=.t., Left=29, Top=138,;
    Alignment=1, Width=117, Height=18,;
    Caption="Carico prodotti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="DGEYKBIZZG",Visible=.t., Left=5, Top=221,;
    Alignment=1, Width=141, Height=18,;
    Caption="Scarico componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="BLHDULMYIA",Visible=.t., Left=523, Top=138,;
    Alignment=1, Width=68, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="SLGHCCSHGT",Visible=.t., Left=521, Top=221,;
    Alignment=1, Width=70, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="ZNPHNSJTHY",Visible=.t., Left=29, Top=308,;
    Alignment=0, Width=245, Height=18,;
    Caption="Criterio di valorizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="CKOEJCKWIO",Visible=.t., Left=2, Top=337,;
    Alignment=1, Width=108, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="CROCMELOIT",Visible=.t., Left=310, Top=337,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="HJNIKVJEHE",Visible=.t., Left=309, Top=337,;
    Alignment=1, Width=70, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="OJUYXHBXKZ",Visible=.t., Left=444, Top=337,;
    Alignment=1, Width=51, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_DITIPVAL<>'L' OR EMPTY(.w_DICODLIS))
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="XPYKQQZEJP",Visible=.t., Left=443, Top=337,;
    Alignment=1, Width=52, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (NOT .w_DITIPVAL $ 'SMUP')
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="AVCBTSIBQW",Visible=.t., Left=11, Top=166,;
    Alignment=1, Width=135, Height=18,;
    Caption="Evas.ordine prodotti:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSOR))
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="VGZPDWPRAS",Visible=.t., Left=29, Top=249,;
    Alignment=1, Width=117, Height=18,;
    Caption="Evas.componenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSIM))
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="HEVUYIKVGZ",Visible=.t., Left=522, Top=166,;
    Alignment=1, Width=69, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSOR))
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="AFLCIXOVSW",Visible=.t., Left=524, Top=249,;
    Alignment=1, Width=67, Height=18,;
    Caption="Cod.mag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUSIM))
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="FGAUTVBMPB",Visible=.t., Left=465, Top=49,;
    Alignment=1, Width=110, Height=18,;
    Caption="Imp.componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="HEAEDEZROR",Visible=.t., Left=6, Top=279,;
    Alignment=1, Width=140, Height=18,;
    Caption="Scar.componenti su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="YPUKYKDAOO",Visible=.t., Left=32, Top=194,;
    Alignment=1, Width=114, Height=18,;
    Caption="Carico prodotti su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="SIYSIOQMRT",Visible=.t., Left=326, Top=279,;
    Alignment=1, Width=117, Height=18,;
    Caption="Numero max livelli:"  ;
  , bGlobalFont=.t.

  add object oBox_1_47 as StdBox with uid="ZHRNEIAIGM",left=29, top=129, width=629,height=2

  add object oBox_1_57 as StdBox with uid="FOKNZYXYPK",left=29, top=328, width=629,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_adc','DIS_CARP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DISERIAL=DIS_CARP.DISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
