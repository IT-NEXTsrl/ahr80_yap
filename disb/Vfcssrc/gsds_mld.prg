* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_mld                                                        *
*              Caratt. di conf. legami dett                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-08                                                      *
* Last revis.: 2015-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsds_mld")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsds_mld")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsds_mld")
  return

* --- Class definition
define class tgsds_mld as StdPCForm
  Width  = 612
  Height = 287
  Top    = 10
  Left   = 10
  cComment = "Caratt. di conf. legami dett"
  cPrg = "gsds_mld"
  HelpContextID=214525033
  add object cnt as tcgsds_mld
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsds_mld as PCContext
  w_CC__DIBA = space(41)
  w_CCROWORD = 0
  w_CCCOMPON = space(41)
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDETTAG = space(40)
  w_CPROWORD = 0
  w_CC__DEFA = space(1)
  w_CCOPERAT = space(1)
  w_DEFA = 0
  w_CC_COEFF = 0
  w_TOTALE = 0
  w_CCDATINI = space(8)
  w_CCDATFIN = space(8)
  w_CCDESSUP = space(80)
  w_CCFLINIT = space(1)
  proc Save(i_oFrom)
    this.w_CC__DIBA = i_oFrom.w_CC__DIBA
    this.w_CCROWORD = i_oFrom.w_CCROWORD
    this.w_CCCOMPON = i_oFrom.w_CCCOMPON
    this.w_CCTIPOCA = i_oFrom.w_CCTIPOCA
    this.w_CCCODICE = i_oFrom.w_CCCODICE
    this.w_CCDETTAG = i_oFrom.w_CCDETTAG
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CC__DEFA = i_oFrom.w_CC__DEFA
    this.w_CCOPERAT = i_oFrom.w_CCOPERAT
    this.w_DEFA = i_oFrom.w_DEFA
    this.w_CC_COEFF = i_oFrom.w_CC_COEFF
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_CCDATINI = i_oFrom.w_CCDATINI
    this.w_CCDATFIN = i_oFrom.w_CCDATFIN
    this.w_CCDESSUP = i_oFrom.w_CCDESSUP
    this.w_CCFLINIT = i_oFrom.w_CCFLINIT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CC__DIBA = this.w_CC__DIBA
    i_oTo.w_CCROWORD = this.w_CCROWORD
    i_oTo.w_CCCOMPON = this.w_CCCOMPON
    i_oTo.w_CCTIPOCA = this.w_CCTIPOCA
    i_oTo.w_CCCODICE = this.w_CCCODICE
    i_oTo.w_CCDETTAG = this.w_CCDETTAG
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CC__DEFA = this.w_CC__DEFA
    i_oTo.w_CCOPERAT = this.w_CCOPERAT
    i_oTo.w_DEFA = this.w_DEFA
    i_oTo.w_CC_COEFF = this.w_CC_COEFF
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_CCDATINI = this.w_CCDATINI
    i_oTo.w_CCDATFIN = this.w_CCDATFIN
    i_oTo.w_CCDESSUP = this.w_CCDESSUP
    i_oTo.w_CCFLINIT = this.w_CCFLINIT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsds_mld as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 612
  Height = 287
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-06-11"
  HelpContextID=214525033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONFDDIS_IDX = 0
  cFile = "CONFDDIS"
  cKeySelect = "CC__DIBA,CCROWORD,CCTIPOCA,CCCODICE"
  cQueryFilter="CCDATFIN>i_DATSYS"
  cKeyWhere  = "CC__DIBA=this.w_CC__DIBA and CCROWORD=this.w_CCROWORD and CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyDetail  = "CC__DIBA=this.w_CC__DIBA and CCROWORD=this.w_CCROWORD and CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';
      +'+" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyDetailWhereODBC = '"CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';
      +'+" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CONFDDIS.CC__DIBA="+cp_ToStrODBC(this.w_CC__DIBA)';
      +'+" and CONFDDIS.CCROWORD="+cp_ToStrODBC(this.w_CCROWORD)';
      +'+" and CONFDDIS.CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CONFDDIS.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONFDDIS.CPROWORD '
  cPrg = "gsds_mld"
  cComment = "Caratt. di conf. legami dett"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CC__DIBA = space(41)
  w_CCROWORD = 0
  w_CCCOMPON = space(41)
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDETTAG = space(40)
  w_CPROWORD = 0
  w_CC__DEFA = space(1)
  o_CC__DEFA = space(1)
  w_CCOPERAT = space(1)
  w_DEFA = 0
  w_CC_COEFF = 0
  w_TOTALE = 0
  w_CCDATINI = ctod('  /  /  ')
  w_CCDATFIN = ctod('  /  /  ')
  w_CCDESSUP = space(80)
  w_CCFLINIT = space(1)
  o_CCFLINIT = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsds_mld
  func CanAddRow()
    local i_res
    i_res=False
    if !i_res
      *Aggiunta righe non gestita
      *ah_ErrorMsg("Aggiungere nuove righe dalla gestione caratteristiche")
    endif
    return(i_res)
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_mldPag1","gsds_mld",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONFDDIS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONFDDIS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONFDDIS_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsds_mld'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONFDDIS where CC__DIBA=KeySet.CC__DIBA
    *                            and CCROWORD=KeySet.CCROWORD
    *                            and CCTIPOCA=KeySet.CCTIPOCA
    *                            and CCCODICE=KeySet.CCCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2],this.bLoadRecFilter,this.CONFDDIS_IDX,"gsds_mld")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONFDDIS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONFDDIS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONFDDIS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CC__DIBA',this.w_CC__DIBA  ,'CCROWORD',this.w_CCROWORD  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) CONFDDIS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTALE = 0
        .w_CC__DIBA = NVL(CC__DIBA,space(41))
        .w_CCROWORD = NVL(CCROWORD,0)
        .w_CCCOMPON = NVL(CCCOMPON,space(41))
        .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONFDDIS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CCDETTAG = NVL(CCDETTAG,space(40))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CC__DEFA = NVL(CC__DEFA,space(1))
          .w_CCOPERAT = NVL(CCOPERAT,space(1))
        .w_DEFA = IIF(.w_CC__DEFA="S", 1, 0)
          .w_CC_COEFF = NVL(CC_COEFF,0)
          .w_CCDATINI = NVL(cp_ToDate(CCDATINI),ctod("  /  /  "))
          .w_CCDATFIN = NVL(cp_ToDate(CCDATFIN),ctod("  /  /  "))
          .w_CCDESSUP = NVL(CCDESSUP,space(80))
          .w_CCFLINIT = NVL(CCFLINIT,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_DEFA
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CC__DIBA=space(41)
      .w_CCROWORD=0
      .w_CCCOMPON=space(41)
      .w_CCTIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDETTAG=space(40)
      .w_CPROWORD=10
      .w_CC__DEFA=space(1)
      .w_CCOPERAT=space(1)
      .w_DEFA=0
      .w_CC_COEFF=0
      .w_TOTALE=0
      .w_CCDATINI=ctod("  /  /  ")
      .w_CCDATFIN=ctod("  /  /  ")
      .w_CCDESSUP=space(80)
      .w_CCFLINIT=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,7,.f.)
        .w_CC__DEFA = "N"
        .DoRTCalc(9,9,.f.)
        .w_DEFA = IIF(.w_CC__DEFA="S", 1, 0)
        .DoRTCalc(11,15,.f.)
        .w_CCFLINIT = iif(.w_CC__DEFA='N','N',.w_CCFLINIT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONFDDIS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCCDESSUP_2_7.enabled = i_bVal
      .Page1.oPag.oBtn_3_1.enabled = i_bVal
      .Page1.oPag.oBtn_3_2.enabled = i_bVal
      .Page1.oPag.oBtn_3_3.enabled = i_bVal
      .Page1.oPag.oBtn_3_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONFDDIS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC__DIBA,"CC__DIBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCROWORD,"CCROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCOMPON,"CCCOMPON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPOCA,"CCTIPOCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CCDETTAG C(40);
      ,t_CPROWORD N(5);
      ,t_CC__DEFA N(3);
      ,t_CCOPERAT N(3);
      ,t_CC_COEFF N(12,5);
      ,t_CCDESSUP C(80);
      ,t_CCFLINIT N(3);
      ,CPROWNUM N(10);
      ,t_DEFA N(1);
      ,t_CCDATINI D(8);
      ,t_CCDATFIN D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsds_mldbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_1.controlsource=this.cTrsName+'.t_CCDETTAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.controlsource=this.cTrsName+'.t_CC__DEFA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.controlsource=this.cTrsName+'.t_CCOPERAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCC_COEFF_2_6.controlsource=this.cTrsName+'.t_CC_COEFF'
    this.oPgFRm.Page1.oPag.oCCDESSUP_2_7.controlsource=this.cTrsName+'.t_CCDESSUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.controlsource=this.cTrsName+'.t_CCFLINIT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(65)
    this.AddVLine(364)
    this.AddVLine(403)
    this.AddVLine(449)
    this.AddVLine(553)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2])
      *
      * insert into CONFDDIS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONFDDIS')
        i_extval=cp_InsertValODBCExtFlds(this,'CONFDDIS')
        i_cFldBody=" "+;
                  "(CC__DIBA,CCROWORD,CCCOMPON,CCTIPOCA,CCCODICE"+;
                  ",CCDETTAG,CPROWORD,CC__DEFA,CCOPERAT,CC_COEFF"+;
                  ",CCDATINI,CCDATFIN,CCDESSUP,CCFLINIT,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CC__DIBA)+","+cp_ToStrODBC(this.w_CCROWORD)+","+cp_ToStrODBC(this.w_CCCOMPON)+","+cp_ToStrODBC(this.w_CCTIPOCA)+","+cp_ToStrODBC(this.w_CCCODICE)+;
             ","+cp_ToStrODBC(this.w_CCDETTAG)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CC__DEFA)+","+cp_ToStrODBC(this.w_CCOPERAT)+","+cp_ToStrODBC(this.w_CC_COEFF)+;
             ","+cp_ToStrODBC(this.w_CCDATINI)+","+cp_ToStrODBC(this.w_CCDATFIN)+","+cp_ToStrODBC(this.w_CCDESSUP)+","+cp_ToStrODBC(this.w_CCFLINIT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONFDDIS')
        i_extval=cp_InsertValVFPExtFlds(this,'CONFDDIS')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CC__DIBA',this.w_CC__DIBA,'CCROWORD',this.w_CCROWORD,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable) (;
                   CC__DIBA;
                  ,CCROWORD;
                  ,CCCOMPON;
                  ,CCTIPOCA;
                  ,CCCODICE;
                  ,CCDETTAG;
                  ,CPROWORD;
                  ,CC__DEFA;
                  ,CCOPERAT;
                  ,CC_COEFF;
                  ,CCDATINI;
                  ,CCDATFIN;
                  ,CCDESSUP;
                  ,CCFLINIT;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CC__DIBA;
                  ,this.w_CCROWORD;
                  ,this.w_CCCOMPON;
                  ,this.w_CCTIPOCA;
                  ,this.w_CCCODICE;
                  ,this.w_CCDETTAG;
                  ,this.w_CPROWORD;
                  ,this.w_CC__DEFA;
                  ,this.w_CCOPERAT;
                  ,this.w_CC_COEFF;
                  ,this.w_CCDATINI;
                  ,this.w_CCDATFIN;
                  ,this.w_CCDESSUP;
                  ,this.w_CCFLINIT;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CCDATINI<>t_CCDATINI
            i_bUpdAll = .t.
          endif
          if this.w_CCDATFIN<>t_CCDATFIN
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONFDDIS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " CCCOMPON="+cp_ToStrODBC(this.w_CCCOMPON)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONFDDIS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  CCCOMPON=this.w_CCCOMPON;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CCDATINI<>t_CCDATINI
            i_bUpdAll = .t.
          endif
          if this.w_CCDATFIN<>t_CCDATFIN
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_CPROWORD))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONFDDIS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONFDDIS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CCCOMPON="+cp_ToStrODBC(this.w_CCCOMPON)+;
                     ",CCDETTAG="+cp_ToStrODBC(this.w_CCDETTAG)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CC__DEFA="+cp_ToStrODBC(this.w_CC__DEFA)+;
                     ",CCOPERAT="+cp_ToStrODBC(this.w_CCOPERAT)+;
                     ",CC_COEFF="+cp_ToStrODBC(this.w_CC_COEFF)+;
                     ",CCDATINI="+cp_ToStrODBC(this.w_CCDATINI)+;
                     ",CCDATFIN="+cp_ToStrODBC(this.w_CCDATFIN)+;
                     ",CCDESSUP="+cp_ToStrODBC(this.w_CCDESSUP)+;
                     ",CCFLINIT="+cp_ToStrODBC(this.w_CCFLINIT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONFDDIS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CCCOMPON=this.w_CCCOMPON;
                     ,CCDETTAG=this.w_CCDETTAG;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CC__DEFA=this.w_CC__DEFA;
                     ,CCOPERAT=this.w_CCOPERAT;
                     ,CC_COEFF=this.w_CC_COEFF;
                     ,CCDATINI=this.w_CCDATINI;
                     ,CCDATFIN=this.w_CCDATFIN;
                     ,CCDESSUP=this.w_CCDESSUP;
                     ,CCFLINIT=this.w_CCFLINIT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONFDDIS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONFDDIS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONFDDIS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
          .w_TOTALE = .w_TOTALE-.w_defa
          .w_DEFA = IIF(.w_CC__DEFA="S", 1, 0)
          .w_TOTALE = .w_TOTALE+.w_defa
        if .o_CCFLINIT<>.w_CCFLINIT
          .Calculate_FHLSOVWWCJ()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DEFA with this.w_DEFA
      replace t_CCDATINI with this.w_CCDATINI
      replace t_CCDATFIN with this.w_CCDATFIN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_FHLSOVWWCJ()
    with this
          * --- Cambio il valore di INIT
     if g_CCAR='S'
          GSCR1BLD(this;
              ,'CHK';
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCC_COEFF_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCC_COEFF_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CCFLINIT Changed")
          .Calculate_FHLSOVWWCJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCCDESSUP_2_7.value==this.w_CCDESSUP)
      this.oPgFrm.Page1.oPag.oCCDESSUP_2_7.value=this.w_CCDESSUP
      replace t_CCDESSUP with this.oPgFrm.Page1.oPag.oCCDESSUP_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_1.value==this.w_CCDETTAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_1.value=this.w_CCDETTAG
      replace t_CCDETTAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.RadioValue()==this.w_CC__DEFA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.SetRadio()
      replace t_CC__DEFA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.RadioValue()==this.w_CCOPERAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.SetRadio()
      replace t_CCOPERAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC_COEFF_2_6.value==this.w_CC_COEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC_COEFF_2_6.value=this.w_CC_COEFF
      replace t_CC_COEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC_COEFF_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.RadioValue()==this.w_CCFLINIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.SetRadio()
      replace t_CCFLINIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'CONFDDIS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_TOTALE>0)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Selezionare almeno una riga."))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CC__DEFA = this.w_CC__DEFA
    this.o_CCFLINIT = this.w_CCFLINIT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CCDETTAG=space(40)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CC__DEFA=space(1)
      .w_CCOPERAT=space(1)
      .w_DEFA=0
      .w_CC_COEFF=0
      .w_CCDATINI=ctod("  /  /  ")
      .w_CCDATFIN=ctod("  /  /  ")
      .w_CCDESSUP=space(80)
      .w_CCFLINIT=space(1)
      .DoRTCalc(1,7,.f.)
        .w_CC__DEFA = "N"
      .DoRTCalc(9,9,.f.)
        .w_DEFA = IIF(.w_CC__DEFA="S", 1, 0)
      .DoRTCalc(11,15,.f.)
        .w_CCFLINIT = iif(.w_CC__DEFA='N','N',.w_CCFLINIT)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CCDETTAG = t_CCDETTAG
    this.w_CPROWORD = t_CPROWORD
    this.w_CC__DEFA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.RadioValue(.t.)
    this.w_CCOPERAT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.RadioValue(.t.)
    this.w_DEFA = t_DEFA
    this.w_CC_COEFF = t_CC_COEFF
    this.w_CCDATINI = t_CCDATINI
    this.w_CCDATFIN = t_CCDATFIN
    this.w_CCDESSUP = t_CCDESSUP
    this.w_CCFLINIT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CCDETTAG with this.w_CCDETTAG
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CC__DEFA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCC__DEFA_2_3.ToRadio()
    replace t_CCOPERAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCOPERAT_2_4.ToRadio()
    replace t_DEFA with this.w_DEFA
    replace t_CC_COEFF with this.w_CC_COEFF
    replace t_CCDATINI with this.w_CCDATINI
    replace t_CCDATFIN with this.w_CCDATFIN
    replace t_CCDESSUP with this.w_CCDESSUP
    replace t_CCFLINIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCFLINIT_2_8.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_defa
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsds_mldPag1 as StdContainer
  Width  = 608
  height = 287
  stdWidth  = 608
  stdheight = 287
  resizeXpos=290
  resizeYpos=169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=1, width=584,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Posizione",Field2="CCDETTAG",Label2="Dettaglio caratteristica",Field3="CC__DEFA",Label3="Def.",Field4="CCOPERAT",Label4="Oper.",Field5="CC_COEFF",Label5="Coefficiente",Field6="CCFLINIT",Label6="Iniz.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101565818

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=20,;
    width=581+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=21,width=580+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCCDESSUP_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCCDESSUP_2_7 as StdTrsField with uid="KETRVZHASE",rtseq=15,rtrep=.t.,;
    cFormVar="w_CCDESSUP",value=space(80),;
    HelpContextID = 196092790,;
    cTotal="", bFixedPos=.t., cQueryName = "CCDESSUP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=555, Left=7, Top=202, InputMask=replicate('X',80)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oBtn_3_1 as StdButton with uid="BUJGGYAQUX",width=48,height=45,;
   left=411, top=237,;
    CpPicture="bmp\wf_conferma.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per selezionare tutte le righe";
    , HelpContextID = 214524938;
    , Caption='\<Sel.Tutto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_1.Click()
      with this.Parent.oContained
        GSCR_BSS(this.Parent.oContained,"SEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_3_2 as StdButton with uid="TNGHLKHEBJ",width=48,height=45,;
   left=461, top=237,;
    CpPicture="bmp\deselez.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per deselezionare tutte le righe";
    , HelpContextID = 214524938;
    , Caption='\<Des.Tutto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_2.Click()
      with this.Parent.oContained
        GSCR_BSS(this.Parent.oContained,"DES")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_3_3 as StdButton with uid="PXTUZOCHIH",width=48,height=45,;
   left=511, top=237,;
    CpPicture="bmp\offprec.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per invertire la selezione";
    , HelpContextID = 214524938;
    , Caption='\<Inv.Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        GSCR_BSS(this.Parent.oContained,"INV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBtn_3_5 as StdButton with uid="LELQKHMLSS",width=48,height=45,;
   left=361, top=237,;
    CpPicture="BMP\INS_RAP.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per attivare l'inserimento rapido delle caratteristiche";
    , HelpContextID = 214524938;
    , TabStop=.f.,Caption='\<Car.Rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      with this.Parent.oContained
        GSCR_KMC(this.parent.ocontained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

* --- Defining Body row
define class tgsds_mldBodyRow as CPBodyRowCnt
  Width=571
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCCDETTAG_2_1 as StdTrsField with uid="BCQBURCASM",rtseq=6,rtrep=.t.,;
    cFormVar="w_CCDETTAG",value=space(40),;
    HelpContextID = 213918573,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=56, Top=0, InputMask=replicate('X',40)

  add object oCPROWORD_2_2 as StdTrsField with uid="GBNVFZCGBT",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    HelpContextID = 133894250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCC__DEFA_2_3 as StdTrsCheck with uid="HLXJHFQLLV",rtrep=.t.,;
    cFormVar="w_CC__DEFA",  caption="",;
    HelpContextID = 215733095,;
    Left=358, Top=0, Width=28,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCC__DEFA_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CC__DEFA,&i_cF..t_CC__DEFA),this.value)
    return(iif(xVal =1,"S",;
    "N"))
  endfunc
  func oCC__DEFA_2_3.GetRadio()
    this.Parent.oContained.w_CC__DEFA = this.RadioValue()
    return .t.
  endfunc

  func oCC__DEFA_2_3.ToRadio()
    this.Parent.oContained.w_CC__DEFA=trim(this.Parent.oContained.w_CC__DEFA)
    return(;
      iif(this.Parent.oContained.w_CC__DEFA=="S",1,;
      0))
  endfunc

  func oCC__DEFA_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCCOPERAT_2_4 as StdTrsCombo with uid="VIKVFDGAGN",rtrep=.t.,;
    cFormVar="w_CCOPERAT", RowSource=""+"X,"+"/" , ;
    HelpContextID = 165401466,;
    Height=21, Width=36, Left=398, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCCOPERAT_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CCOPERAT,&i_cF..t_CCOPERAT),this.value)
    return(iif(xVal =1,"*",;
    iif(xVal =2,"/",;
    space(1))))
  endfunc
  func oCCOPERAT_2_4.GetRadio()
    this.Parent.oContained.w_CCOPERAT = this.RadioValue()
    return .t.
  endfunc

  func oCCOPERAT_2_4.ToRadio()
    this.Parent.oContained.w_CCOPERAT=trim(this.Parent.oContained.w_CCOPERAT)
    return(;
      iif(this.Parent.oContained.w_CCOPERAT=="*",1,;
      iif(this.Parent.oContained.w_CCOPERAT=="/",2,;
      0)))
  endfunc

  func oCCOPERAT_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCC_COEFF_2_6 as StdTrsField with uid="LDPBYGZJQB",rtseq=11,rtrep=.t.,;
    cFormVar="w_CC_COEFF",value=0,;
    HelpContextID = 225432428,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=442, Top=0, cSayPict=["@z" + v_pq(32)], cGetPict=[v_gq(32)]

  func oCC_COEFF_2_6.mCond()
    with this.Parent.oContained
      return (not empty(.w_CCOPERAT))
    endwith
  endfunc

  add object oCCFLINIT_2_8 as StdTrsCheck with uid="QNEWRTRNHO",rtrep=.t.,;
    cFormVar="w_CCFLINIT",  caption="",;
    ToolTipText = "Flag di inizializzazione per configurazione",;
    HelpContextID = 166247558,;
    Left=546, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCCFLINIT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CCFLINIT,&i_cF..t_CCFLINIT),this.value)
    return(iif(xVal =1,"S",;
    "N"))
  endfunc
  func oCCFLINIT_2_8.GetRadio()
    this.Parent.oContained.w_CCFLINIT = this.RadioValue()
    return .t.
  endfunc

  func oCCFLINIT_2_8.ToRadio()
    this.Parent.oContained.w_CCFLINIT=trim(this.Parent.oContained.w_CCFLINIT)
    return(;
      iif(this.Parent.oContained.w_CCFLINIT=="S",1,;
      0))
  endfunc

  func oCCFLINIT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCCDETTAG_2_1.When()
    return(.t.)
  proc oCCDETTAG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCCDETTAG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="CCDATFIN>i_DATSYS"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".CC__DIBA=CONFDDIS.CC__DIBA";
  +" and "+i_cAliasName+".CCROWORD=CONFDDIS.CCROWORD";
  +" and "+i_cAliasName+".CCTIPOCA=CONFDDIS.CCTIPOCA";
  +" and "+i_cAliasName+".CCCODICE=CONFDDIS.CCCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsds_mld','CONFDDIS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CC__DIBA=CONFDDIS.CC__DIBA";
  +" and "+i_cAliasName2+".CCROWORD=CONFDDIS.CCROWORD";
  +" and "+i_cAliasName2+".CCTIPOCA=CONFDDIS.CCTIPOCA";
  +" and "+i_cAliasName2+".CCCODICE=CONFDDIS.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
