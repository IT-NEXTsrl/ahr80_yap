* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_mdb                                                        *
*              Distinta base                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_375]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-26                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_mdb"))

* --- Class definition
define class tgsds_mdb as StdTrsForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 791
  Height = 450+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-15"
  HelpContextID=80307305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  DISMBASE_IDX = 0
  DISTBASE_IDX = 0
  KEY_ARTI_IDX = 0
  TABMCICL_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  COE_IMPI_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_PROD_IDX = 0
  TAB_CICL_IDX = 0
  TIP_DIBA_IDX = 0
  cFile = "DISMBASE"
  cFileDetail = "DISTBASE"
  cKeySelect = "DBCODICE"
  cQueryFilter="DBDISKIT = 'D'"
  cKeyWhere  = "DBCODICE=this.w_DBCODICE"
  cKeyDetail  = "DBCODICE=this.w_DBCODICE"
  cKeyWhereODBC = '"DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';

  cKeyDetailWhereODBC = '"DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"DISTBASE.DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DISTBASE.CPROWORD '
  cPrg = "gsds_mdb"
  cComment = "Distinta base"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DBCODICE = space(20)
  o_DBCODICE = space(20)
  w_DBDESCRI = space(40)
  w_DBFLSTAT = space(1)
  w_DBCODRIS = space(15)
  o_DBCODRIS = space(15)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_CPROWORD = 0
  w_DBPRIORI = space(4)
  w_DBDTINVA = ctod('  /  /  ')
  w_DBDTOBSO = ctod('  /  /  ')
  w_READPAR = space(1)
  w_TIPDEFA = space(4)
  w_DBTIPDIS = space(4)
  w_UTDV = ctot('')
  w_DBFLVARI = space(1)
  o_DBFLVARI = space(1)
  w_DBCODCOM = space(20)
  o_DBCODCOM = space(20)
  w_OPERA3 = space(1)
  w_DBARTCOM = space(20)
  o_DBARTCOM = space(20)
  w_TIPRIG = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_DESART = space(40)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_DISCOM = space(20)
  w_DBDESCOM = space(40)
  w_DBUNIMIS = space(3)
  o_DBUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_DBQTADIS = 0
  o_DBQTADIS = 0
  w_DBCOEIMP = space(15)
  w_DBFLESPL = space(1)
  w_POSIZ = 0
  w_DBPERSCA = 0
  o_DBPERSCA = 0
  w_DBRECSCA = 0
  w_DBPERSFR = 0
  o_DBPERSFR = 0
  w_DBRECSFR = 0
  w_DBPERRIC = 0
  w_DBDATULT = ctod('  /  /  ')
  w_DB__NOTE = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_DESCIC = space(40)
  w_DBFLVARC = space(1)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_EXPCOART = space(20)
  w_CODDIS = space(20)
  w_DBINIVAL = ctod('  /  /  ')
  w_DBFINVAL = ctod('  /  /  ')
  w_VERDAT = .F.
  w_DBCOEUM1 = 0
  w_NUMLEV = space(4)
  w_DBRIFFAS = 0
  w_DESFAS = space(40)
  w_CODRIS = space(15)
  w_CURSORNA = space(10)
  w_DBDISKIT = space(1)
  w_KITIMB = space(1)
  w_NOFRAZ = space(1)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_CODDISB = space(20)
  w_DESDIS = space(40)
  w_DBNOTAGG = space(0)
  w_DBCORLEA = 0
  w_ARGESCAR = space(1)
  w_ARCMPCAR = space(1)

  * --- Children pointers
  GSDS_MCV = .NULL.
  GSDS_MOU = .NULL.
  GSDS_MLC = .NULL.
  w_TreeView = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DISMBASE','gsds_mdb')
    stdPageFrame::Init()
    *set procedure to GSDS_MCV additive
    *set procedure to GSDS_MLC additive
    with this
      .Pages(1).addobject("oPag","tgsds_mdbPag1","gsds_mdb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta base")
      .Pages(1).HelpContextID = 118205847
      .Pages(2).addobject("oPag","tgsds_mdbPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Materiali di output")
      .Pages(2).HelpContextID = 46504650
      .Pages(3).addobject("oPag","tgsds_mdbPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Vista")
      .Pages(3).HelpContextID = 238929322
      .Pages(4).addobject("oPag","tgsds_mdbPag4")
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note")
      .Pages(4).HelpContextID = 73183274
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDBCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSDS_MCV
    *release procedure GSDS_MLC
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(3).oPag.TreeView
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='TABMCICL'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='COE_IMPI'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='PAR_PROD'
    this.cWorkTables[8]='TAB_CICL'
    this.cWorkTables[9]='TIP_DIBA'
    this.cWorkTables[10]='DISMBASE'
    this.cWorkTables[11]='DISTBASE'
    * --- Area Manuale = Open Work Table
    * --- gsds_mdb
    * colora le righe non pi� valide
    mycolor="RGB(255,127,39)"
        
        This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor=;
           "IIF(empty(t_DBINIVAL) or empty(t_DBFINVAL) or t_DBFINVAL < i_DATSYS, " + mycolor  ;
           + ", RGB(255,255,255))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DISMBASE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DISMBASE_IDX,3]
  return

  function CreateChildren()
    this.GSDS_MCV = CREATEOBJECT('stdLazyChild',this,'GSDS_MCV')
    this.GSDS_MOU = CREATEOBJECT('stdDynamicChild',this,'GSDS_MOU',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    this.GSDS_MLC = CREATEOBJECT('stdLazyChild',this,'GSDS_MLC')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSDS_MCV)
      this.GSDS_MCV.DestroyChildrenChain()
      this.GSDS_MCV=.NULL.
    endif
    if !ISNULL(this.GSDS_MOU)
      this.GSDS_MOU.DestroyChildrenChain()
      this.GSDS_MOU=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSDS_MLC)
      this.GSDS_MLC.DestroyChildrenChain()
      this.GSDS_MLC=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSDS_MCV.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSDS_MOU.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSDS_MLC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSDS_MCV.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSDS_MOU.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSDS_MLC.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSDS_MCV.NewDocument()
    this.GSDS_MOU.NewDocument()
    this.GSDS_MLC.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSDS_MCV.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_DBCODICE,"CVCODICE";
             ,.w_CPROWNUM,"CVNUMRIF";
             )
      .GSDS_MOU.ChangeRow(this.cRowID+'      1',1;
             ,.w_DBCODICE,"MOCODICE";
             ,.w_CPROWNUM,"CPROWNUM";
             )
      .GSDS_MLC.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_DBCODICE,"CC__DIBA";
             ,.w_CPROWNUM,"CCROWORD";
             )
      .WriteTo_GSDS_MLC()
    endwith
    select (i_cOldSel)
    return

procedure WriteTo_GSDS_MLC()
  if at('gsds_mlc',lower(this.GSDS_MLC.class))<>0
    if this.GSDS_MLC.cnt.w_DBCODICE<>this.w_DBCODICE or this.GSDS_MLC.cnt.w_CCCOMPON<>this.w_DBCODCOM or this.GSDS_MLC.cnt.w_CCCODART<>this.w_DBARTCOM
      this.GSDS_MLC.cnt.w_DBCODICE = this.w_DBCODICE
      this.GSDS_MLC.cnt.w_CCCOMPON = this.w_DBCODCOM
      this.GSDS_MLC.cnt.w_CCCODART = this.w_DBARTCOM
      this.GSDS_MLC.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_DBCODICE = NVL(DBCODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from DISMBASE where DBCODICE=KeySet.DBCODICE
    *
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2],this.bLoadRecFilter,this.DISMBASE_IDX,"gsds_mdb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DISMBASE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DISMBASE.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"DISTBASE.","DISMBASE.")
      i_cTable = i_cTable+' DISMBASE '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_READPAR = 'S'
        .w_TIPDEFA = space(4)
        .w_DESCIC = space(40)
        .w_VERDAT = .F.
        .w_CURSORNA = space(10)
        .w_DBCODICE = NVL(DBCODICE,space(20))
        .w_DBDESCRI = NVL(DBDESCRI,space(40))
        .w_DBFLSTAT = NVL(DBFLSTAT,space(1))
        .w_DBCODRIS = NVL(DBCODRIS,space(15))
          if link_1_4_joined
            this.w_DBCODRIS = NVL(CSCODICE104,NVL(this.w_DBCODRIS,space(15)))
            this.w_DESCIC = NVL(CSDESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_DBPRIORI = NVL(DBPRIORI,space(4))
        .w_DBDTINVA = NVL(cp_ToDate(DBDTINVA),ctod("  /  /  "))
        .w_DBDTOBSO = NVL(cp_ToDate(DBDTOBSO),ctod("  /  /  "))
          .link_1_11('Load')
        .w_DBTIPDIS = NVL(DBTIPDIS,space(4))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_OBTEST = .w_UTDC
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page3.oPag.TreeView.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_2.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_3.Calculate()
        .w_CODDIS = Nvl(.w_TREEVIEW.GETVAR('CODDIS'),Space(20))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_NUMLEV = IIF(EMPTY(NVL(.w_TREEVIEW.GETVAR('NUMLEV'),' ')),'0000',.w_TREEVIEW.GETVAR('NUMLEV'))
        .w_CODRIS = .w_DBCODRIS
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .w_DBDISKIT = NVL(DBDISKIT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODDISB = .w_DBCODICE
        .w_DESDIS = .w_DBDESCRI
        .w_DBNOTAGG = NVL(DBNOTAGG,space(0))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        cp_LoadRecExtFlds(this,'DISMBASE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from DISTBASE where DBCODICE=KeySet.DBCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.DISTBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('DISTBASE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "DISTBASE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" DISTBASE"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
        select * from (i_cTable) DISTBASE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_OPERA3 = space(1)
          .w_TIPRIG = space(1)
          .w_DTOBS1 = ctod("  /  /  ")
          .w_DESART = space(40)
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_DISCOM = space(20)
          .w_FLFRAZ = space(1)
          .w_DESFAS = space(40)
          .w_KITIMB = space(1)
          .w_NOFRAZ = space(1)
          .w_MODUM2 = space(1)
          .w_FLUSEP = space(1)
          .w_ARGESCAR = space(1)
          .w_ARCMPCAR = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DBFLVARI = NVL(DBFLVARI,space(1))
          .w_DBCODCOM = NVL(DBCODCOM,space(20))
          if link_2_3_joined
            this.w_DBCODCOM = NVL(CACODICE203,NVL(this.w_DBCODCOM,space(20)))
            this.w_DBARTCOM = NVL(CACODART203,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS203,space(3))
            this.w_OPERA3 = NVL(CAOPERAT203,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP203,0)
            this.w_DESART = NVL(CADESART203,space(40))
            this.w_TIPRIG = NVL(CA__TIPO203,space(1))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_DBARTCOM = NVL(DBARTCOM,space(20))
          if link_2_5_joined
            this.w_DBARTCOM = NVL(ARCODART205,NVL(this.w_DBARTCOM,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1205,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2205,space(3))
            this.w_OPERAT = NVL(AROPERAT205,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP205,0)
            this.w_DISCOM = NVL(ARCODDIS205,space(20))
            this.w_KITIMB = NVL(ARKITIMB205,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP205,space(1))
            this.w_ARCMPCAR = NVL(ARCMPCAR205,space(1))
            this.w_ARGESCAR = NVL(ARGESCAR205,space(1))
          else
          .link_2_5('Load')
          endif
          .link_2_11('Load')
          .w_DBDESCOM = NVL(DBDESCOM,space(40))
          .w_DBUNIMIS = NVL(DBUNIMIS,space(3))
          if link_2_17_joined
            this.w_DBUNIMIS = NVL(UMCODICE217,NVL(this.w_DBUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ217,space(1))
          else
          .link_2_17('Load')
          endif
          .w_DBQTADIS = NVL(DBQTADIS,0)
          .w_DBCOEIMP = NVL(DBCOEIMP,space(15))
          * evitabile
          *.link_2_20('Load')
          .w_DBFLESPL = NVL(DBFLESPL,space(1))
        .w_POSIZ = .w_CPROWORD
          .w_DBPERSCA = NVL(DBPERSCA,0)
          .w_DBRECSCA = NVL(DBRECSCA,0)
          .w_DBPERSFR = NVL(DBPERSFR,0)
          .w_DBRECSFR = NVL(DBRECSFR,0)
          .w_DBPERRIC = NVL(DBPERRIC,0)
          .w_DBDATULT = NVL(cp_ToDate(DBDATULT),ctod("  /  /  "))
          .w_DB__NOTE = NVL(DB__NOTE,space(0))
          .w_DBFLVARC = NVL(DBFLVARC,space(1))
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
          .w_DBINIVAL = NVL(cp_ToDate(DBINIVAL),ctod("  /  /  "))
          .w_DBFINVAL = NVL(cp_ToDate(DBFINVAL),ctod("  /  /  "))
          .w_DBCOEUM1 = NVL(DBCOEUM1,0)
          .w_DBRIFFAS = NVL(DBRIFFAS,0)
          .link_2_36('Load')
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
          .w_DBCORLEA = NVL(DBCORLEA,0)
          select (this.cTrsName)
          append blank
          replace DBCODICE with .w_DBCODICE
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_UTDC
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page3.oPag.TreeView.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_2.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_3.Calculate()
        .w_CODDIS = Nvl(.w_TREEVIEW.GETVAR('CODDIS'),Space(20))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_NUMLEV = IIF(EMPTY(NVL(.w_TREEVIEW.GETVAR('NUMLEV'),' ')),'0000',.w_TREEVIEW.GETVAR('NUMLEV'))
        .w_CODRIS = .w_DBCODRIS
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODDISB = .w_DBCODICE
        .w_DESDIS = .w_DBDESCRI
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page3.oPag.oBtn_5_5.enabled = .oPgFrm.Page3.oPag.oBtn_5_5.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_6.enabled = .oPgFrm.Page3.oPag.oBtn_5_6.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_7.enabled = .oPgFrm.Page3.oPag.oBtn_5_7.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_8.enabled = .oPgFrm.Page3.oPag.oBtn_5_8.mCond()
        .oPgFrm.Page3.oPag.oBtn_5_9.enabled = .oPgFrm.Page3.oPag.oBtn_5_9.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DBCODICE=space(20)
      .w_DBDESCRI=space(40)
      .w_DBFLSTAT=space(1)
      .w_DBCODRIS=space(15)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_CPROWORD=10
      .w_DBPRIORI=space(4)
      .w_DBDTINVA=ctod("  /  /  ")
      .w_DBDTOBSO=ctod("  /  /  ")
      .w_READPAR=space(1)
      .w_TIPDEFA=space(4)
      .w_DBTIPDIS=space(4)
      .w_UTDV=ctot("")
      .w_DBFLVARI=space(1)
      .w_DBCODCOM=space(20)
      .w_OPERA3=space(1)
      .w_DBARTCOM=space(20)
      .w_TIPRIG=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DISCOM=space(20)
      .w_DBDESCOM=space(40)
      .w_DBUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_DBQTADIS=0
      .w_DBCOEIMP=space(15)
      .w_DBFLESPL=space(1)
      .w_POSIZ=0
      .w_DBPERSCA=0
      .w_DBRECSCA=0
      .w_DBPERSFR=0
      .w_DBRECSFR=0
      .w_DBPERRIC=0
      .w_DBDATULT=ctod("  /  /  ")
      .w_DB__NOTE=space(0)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCIC=space(40)
      .w_DBFLVARC=space(1)
      .w_MAXLEVEL=0
      .w_VERIFICA=space(1)
      .w_EXPCOART=space(20)
      .w_CODDIS=space(20)
      .w_DBINIVAL=ctod("  /  /  ")
      .w_DBFINVAL=ctod("  /  /  ")
      .w_VERDAT=.f.
      .w_DBCOEUM1=0
      .w_NUMLEV=space(4)
      .w_DBRIFFAS=0
      .w_DESFAS=space(40)
      .w_CODRIS=space(15)
      .w_CURSORNA=space(10)
      .w_DBDISKIT=space(1)
      .w_KITIMB=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_CODDISB=space(20)
      .w_DESDIS=space(40)
      .w_DBNOTAGG=space(0)
      .w_DBCORLEA=0
      .w_ARGESCAR=space(1)
      .w_ARCMPCAR=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_DBFLSTAT = 'S'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_DBCODRIS))
         .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        .w_UTDC = i_DATSYS
        .DoRTCalc(8,8,.f.)
        .w_DBPRIORI = '0000'
        .DoRTCalc(10,11,.f.)
        .w_READPAR = 'S'
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_READPAR))
         .link_1_11('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_DBTIPDIS = iif(upper(this.cFunction)='LOAD' and empty(.w_DBTIPDIS),.w_TIPDEFA,.w_DBTIPDIS)
        .DoRTCalc(15,16,.f.)
        .w_DBCODCOM = SPACE(20)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DBCODCOM))
         .link_2_3('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_DBARTCOM))
         .link_2_5('Full')
        endif
        .DoRTCalc(20,25,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_11('Full')
        endif
        .DoRTCalc(26,29,.f.)
        .w_DBDESCOM = IIF(.w_DBFLVARI='S', .w_DBDESCOM, .w_DESART)
        .w_DBUNIMIS = .w_UNMIS1
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_DBUNIMIS))
         .link_2_17('Full')
        endif
        .DoRTCalc(32,33,.f.)
        .w_DBCOEIMP = SPACE(15)
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_DBCOEIMP))
         .link_2_20('Full')
        endif
        .w_DBFLESPL = IIF(NOT EMPTY(.w_DBCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
        .w_POSIZ = .w_CPROWORD
        .DoRTCalc(37,37,.f.)
        .w_DBRECSCA = 0
        .DoRTCalc(39,39,.f.)
        .w_DBRECSFR = 0
        .DoRTCalc(41,43,.f.)
        .w_OBTEST = .w_UTDC
        .DoRTCalc(45,45,.f.)
        .w_DBFLVARC = .w_DBFLVARI
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page3.oPag.TreeView.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_2.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_3.Calculate()
        .w_CODDIS = Nvl(.w_TREEVIEW.GETVAR('CODDIS'),Space(20))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
        .w_DBINIVAL = i_DATSYS
        .w_DBFINVAL = i_FINDAT
        .w_VERDAT = .F.
        .w_DBCOEUM1 = CALQTAADV(.w_DBQTADIS,.w_DBUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "DBQTADIS")
        .w_NUMLEV = IIF(EMPTY(NVL(.w_TREEVIEW.GETVAR('NUMLEV'),' ')),'0000',.w_TREEVIEW.GETVAR('NUMLEV'))
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_DBRIFFAS))
         .link_2_36('Full')
        endif
        .DoRTCalc(57,57,.f.)
        .w_CODRIS = .w_DBCODRIS
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .DoRTCalc(59,59,.f.)
        .w_DBDISKIT = 'D'
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(61,64,.f.)
        .w_CODDISB = .w_DBCODICE
        .w_DESDIS = .w_DBDESCRI
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DISMBASE')
    this.DoRTCalc(67,70,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page3.oPag.oBtn_5_5.enabled = this.oPgFrm.Page3.oPag.oBtn_5_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_6.enabled = this.oPgFrm.Page3.oPag.oBtn_5_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_7.enabled = this.oPgFrm.Page3.oPag.oBtn_5_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_8.enabled = this.oPgFrm.Page3.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_9.enabled = this.oPgFrm.Page3.oPag.oBtn_5_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDBCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDBDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oDBFLSTAT_1_3.enabled = i_bVal
      .Page1.oPag.oDBCODRIS_1_4.enabled = i_bVal
      .Page1.oPag.oDBPRIORI_1_8.enabled = i_bVal
      .Page1.oPag.oDBDTINVA_1_9.enabled = i_bVal
      .Page1.oPag.oDBDTOBSO_1_10.enabled = i_bVal
      .Page1.oPag.oDBTIPDIS_1_13.enabled = i_bVal
      .Page1.oPag.oDBPERSCA_2_23.enabled = i_bVal
      .Page1.oPag.oDBRECSCA_2_24.enabled = i_bVal
      .Page1.oPag.oDBPERSFR_2_25.enabled = i_bVal
      .Page1.oPag.oDBRECSFR_2_26.enabled = i_bVal
      .Page1.oPag.oDBPERRIC_2_27.enabled = i_bVal
      .Page1.oPag.oDB__NOTE_2_29.enabled = i_bVal
      .Page1.oPag.oDBINIVAL_2_33.enabled = i_bVal
      .Page1.oPag.oDBFINVAL_2_34.enabled = i_bVal
      .Page1.oPag.oDBRIFFAS_2_36.enabled = i_bVal
      .Page4.oPag.oDBNOTAGG_6_3.enabled = i_bVal
      .Page1.oPag.oDBCORLEA_2_43.enabled = i_bVal
      .Page3.oPag.oBtn_5_5.enabled = .Page3.oPag.oBtn_5_5.mCond()
      .Page3.oPag.oBtn_5_6.enabled = .Page3.oPag.oBtn_5_6.mCond()
      .Page3.oPag.oBtn_5_7.enabled = .Page3.oPag.oBtn_5_7.mCond()
      .Page3.oPag.oBtn_5_8.enabled = .Page3.oPag.oBtn_5_8.mCond()
      .Page3.oPag.oBtn_5_9.enabled = .Page3.oPag.oBtn_5_9.mCond()
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page3.oPag.TreeView.enabled = i_bVal
      .Page3.oPag.oObj_5_2.enabled = i_bVal
      .Page3.oPag.oObj_5_3.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_2_38.enabled = i_bVal
      .Page1.oPag.oObj_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_48.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDBCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDBCODICE_1_1.enabled = .t.
        .Page1.oPag.oDBDESCRI_1_2.enabled = .t.
      endif
    endwith
    this.GSDS_MCV.SetStatus(i_cOp)
    this.GSDS_MOU.SetStatus(i_cOp)
    this.GSDS_MLC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DISMBASE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSDS_MCV.SetChildrenStatus(i_cOp)
  *  this.GSDS_MOU.SetChildrenStatus(i_cOp)
  *  this.GSDS_MLC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODICE,"DBCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDESCRI,"DBDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBFLSTAT,"DBFLSTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODRIS,"DBCODRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBPRIORI,"DBPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDTINVA,"DBDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDTOBSO,"DBDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBTIPDIS,"DBTIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDISKIT,"DBDISKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBNOTAGG,"DBNOTAGG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    i_lTable = "DISMBASE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DISMBASE_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsds_sds with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DBFLVARI N(3);
      ,t_DBCODCOM C(20);
      ,t_DISCOM C(20);
      ,t_DBDESCOM C(40);
      ,t_DBUNIMIS C(3);
      ,t_DBQTADIS N(15,6);
      ,t_DBCOEIMP C(15);
      ,t_DBFLESPL N(3);
      ,t_POSIZ N(5);
      ,t_DBPERSCA N(6,2);
      ,t_DBRECSCA N(6,2);
      ,t_DBPERSFR N(6,2);
      ,t_DBRECSFR N(6,2);
      ,t_DBPERRIC N(6,2);
      ,t_DB__NOTE M(10);
      ,t_DBINIVAL D(8);
      ,t_DBFINVAL D(8);
      ,t_DBRIFFAS N(4);
      ,t_DESFAS C(40);
      ,t_DBCORLEA N(7,2);
      ,DBCODICE C(20);
      ,CPROWNUM N(10);
      ,t_OPERA3 C(1);
      ,t_DBARTCOM C(20);
      ,t_TIPRIG C(1);
      ,t_DTOBS1 D(8);
      ,t_DESART C(40);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_FLFRAZ C(1);
      ,t_DBDATULT D(8);
      ,t_DBFLVARC C(1);
      ,t_DBCOEUM1 N(15,6);
      ,t_KITIMB C(1);
      ,t_NOFRAZ C(1);
      ,t_MODUM2 C(1);
      ,t_FLUSEP C(1);
      ,t_ARGESCAR C(1);
      ,t_ARCMPCAR C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsds_mdbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.controlsource=this.cTrsName+'.t_DBFLVARI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_3.controlsource=this.cTrsName+'.t_DBCODCOM'
    this.oPgFRm.Page1.oPag.oDISCOM_2_15.controlsource=this.cTrsName+'.t_DISCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_16.controlsource=this.cTrsName+'.t_DBDESCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBUNIMIS_2_17.controlsource=this.cTrsName+'.t_DBUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_19.controlsource=this.cTrsName+'.t_DBQTADIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBCOEIMP_2_20.controlsource=this.cTrsName+'.t_DBCOEIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.controlsource=this.cTrsName+'.t_DBFLESPL'
    this.oPgFRm.Page1.oPag.oPOSIZ_2_22.controlsource=this.cTrsName+'.t_POSIZ'
    this.oPgFRm.Page1.oPag.oDBPERSCA_2_23.controlsource=this.cTrsName+'.t_DBPERSCA'
    this.oPgFRm.Page1.oPag.oDBRECSCA_2_24.controlsource=this.cTrsName+'.t_DBRECSCA'
    this.oPgFRm.Page1.oPag.oDBPERSFR_2_25.controlsource=this.cTrsName+'.t_DBPERSFR'
    this.oPgFRm.Page1.oPag.oDBRECSFR_2_26.controlsource=this.cTrsName+'.t_DBRECSFR'
    this.oPgFRm.Page1.oPag.oDBPERRIC_2_27.controlsource=this.cTrsName+'.t_DBPERRIC'
    this.oPgFRm.Page1.oPag.oDB__NOTE_2_29.controlsource=this.cTrsName+'.t_DB__NOTE'
    this.oPgFRm.Page1.oPag.oDBINIVAL_2_33.controlsource=this.cTrsName+'.t_DBINIVAL'
    this.oPgFRm.Page1.oPag.oDBFINVAL_2_34.controlsource=this.cTrsName+'.t_DBFINVAL'
    this.oPgFRm.Page1.oPag.oDBRIFFAS_2_36.controlsource=this.cTrsName+'.t_DBRIFFAS'
    this.oPgFRm.Page1.oPag.oDESFAS_2_37.controlsource=this.cTrsName+'.t_DESFAS'
    this.oPgFRm.Page1.oPag.oDBCORLEA_2_43.controlsource=this.cTrsName+'.t_DBCORLEA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(56)
    this.AddVLine(76)
    this.AddVLine(226)
    this.AddVLine(482)
    this.AddVLine(524)
    this.AddVLine(605)
    this.AddVLine(722)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISMBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DISMBASE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DISMBASE')
        i_extval=cp_InsertValODBCExtFlds(this,'DISMBASE')
        local i_cFld
        i_cFld=" "+;
                  "(DBCODICE,DBDESCRI,DBFLSTAT,DBCODRIS,UTCC"+;
                  ",UTCV,UTDC,DBPRIORI,DBDTINVA,DBDTOBSO"+;
                  ",DBTIPDIS,UTDV,DBDISKIT,DBNOTAGG"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_DBCODICE)+;
                    ","+cp_ToStrODBC(this.w_DBDESCRI)+;
                    ","+cp_ToStrODBC(this.w_DBFLSTAT)+;
                    ","+cp_ToStrODBCNull(this.w_DBCODRIS)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_DBPRIORI)+;
                    ","+cp_ToStrODBC(this.w_DBDTINVA)+;
                    ","+cp_ToStrODBC(this.w_DBDTOBSO)+;
                    ","+cp_ToStrODBC(this.w_DBTIPDIS)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_DBDISKIT)+;
                    ","+cp_ToStrODBC(this.w_DBNOTAGG)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DISMBASE')
        i_extval=cp_InsertValVFPExtFlds(this,'DISMBASE')
        cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_DBCODICE)
        INSERT INTO (i_cTable);
              (DBCODICE,DBDESCRI,DBFLSTAT,DBCODRIS,UTCC,UTCV,UTDC,DBPRIORI,DBDTINVA,DBDTOBSO,DBTIPDIS,UTDV,DBDISKIT,DBNOTAGG &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_DBCODICE;
                  ,this.w_DBDESCRI;
                  ,this.w_DBFLSTAT;
                  ,this.w_DBCODRIS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_DBPRIORI;
                  ,this.w_DBDTINVA;
                  ,this.w_DBDTOBSO;
                  ,this.w_DBTIPDIS;
                  ,this.w_UTDV;
                  ,this.w_DBDISKIT;
                  ,this.w_DBNOTAGG;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISTBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
      *
      * insert into DISTBASE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(DBCODICE,CPROWORD,DBFLVARI,DBCODCOM,DBARTCOM"+;
                  ",DBDESCOM,DBUNIMIS,DBQTADIS,DBCOEIMP,DBFLESPL"+;
                  ",DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC"+;
                  ",DBDATULT,DB__NOTE,DBFLVARC,DBINIVAL,DBFINVAL"+;
                  ",DBCOEUM1,DBRIFFAS,DBCORLEA,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DBCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DBFLVARI)+","+cp_ToStrODBCNull(this.w_DBCODCOM)+","+cp_ToStrODBCNull(this.w_DBARTCOM)+;
             ","+cp_ToStrODBC(this.w_DBDESCOM)+","+cp_ToStrODBCNull(this.w_DBUNIMIS)+","+cp_ToStrODBC(this.w_DBQTADIS)+","+cp_ToStrODBCNull(this.w_DBCOEIMP)+","+cp_ToStrODBC(this.w_DBFLESPL)+;
             ","+cp_ToStrODBC(this.w_DBPERSCA)+","+cp_ToStrODBC(this.w_DBRECSCA)+","+cp_ToStrODBC(this.w_DBPERSFR)+","+cp_ToStrODBC(this.w_DBRECSFR)+","+cp_ToStrODBC(this.w_DBPERRIC)+;
             ","+cp_ToStrODBC(this.w_DBDATULT)+","+cp_ToStrODBC(this.w_DB__NOTE)+","+cp_ToStrODBC(this.w_DBFLVARC)+","+cp_ToStrODBC(this.w_DBINIVAL)+","+cp_ToStrODBC(this.w_DBFINVAL)+;
             ","+cp_ToStrODBC(this.w_DBCOEUM1)+","+cp_ToStrODBCNull(this.w_DBRIFFAS)+","+cp_ToStrODBC(this.w_DBCORLEA)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DBCODICE',this.w_DBCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_DBCODICE,this.w_CPROWORD,this.w_DBFLVARI,this.w_DBCODCOM,this.w_DBARTCOM"+;
                ",this.w_DBDESCOM,this.w_DBUNIMIS,this.w_DBQTADIS,this.w_DBCOEIMP,this.w_DBFLESPL"+;
                ",this.w_DBPERSCA,this.w_DBRECSCA,this.w_DBPERSFR,this.w_DBRECSFR,this.w_DBPERRIC"+;
                ",this.w_DBDATULT,this.w_DB__NOTE,this.w_DBFLVARC,this.w_DBINIVAL,this.w_DBFINVAL"+;
                ",this.w_DBCOEUM1,this.w_DBRIFFAS,this.w_DBCORLEA,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DISMBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update DISMBASE
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'DISMBASE')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " DBDESCRI="+cp_ToStrODBC(this.w_DBDESCRI)+;
             ",DBFLSTAT="+cp_ToStrODBC(this.w_DBFLSTAT)+;
             ",DBCODRIS="+cp_ToStrODBCNull(this.w_DBCODRIS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",DBPRIORI="+cp_ToStrODBC(this.w_DBPRIORI)+;
             ",DBDTINVA="+cp_ToStrODBC(this.w_DBDTINVA)+;
             ",DBDTOBSO="+cp_ToStrODBC(this.w_DBDTOBSO)+;
             ",DBTIPDIS="+cp_ToStrODBC(this.w_DBTIPDIS)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",DBDISKIT="+cp_ToStrODBC(this.w_DBDISKIT)+;
             ",DBNOTAGG="+cp_ToStrODBC(this.w_DBNOTAGG)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'DISMBASE')
          i_cWhere = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
          UPDATE (i_cTable) SET;
              DBDESCRI=this.w_DBDESCRI;
             ,DBFLSTAT=this.w_DBFLSTAT;
             ,DBCODRIS=this.w_DBCODRIS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,DBPRIORI=this.w_DBPRIORI;
             ,DBDTINVA=this.w_DBDTINVA;
             ,DBDTOBSO=this.w_DBDTOBSO;
             ,DBTIPDIS=this.w_DBTIPDIS;
             ,UTDV=this.w_UTDV;
             ,DBDISKIT=this.w_DBDISKIT;
             ,DBNOTAGG=this.w_DBNOTAGG;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_DBCODICE<>DBCODICE
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.DISTBASE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSDS_MCV.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_DBCODICE,"CVCODICE";
                     ,this.w_CPROWNUM,"CVNUMRIF";
                     )
              this.GSDS_MLC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_DBCODICE,"CC__DIBA";
                     ,this.w_CPROWNUM,"CCROWORD";
                     )
              this.GSDS_MCV.mDelete()
              this.GSDS_MLC.mDelete()
              *
              * delete from DISTBASE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DISTBASE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DBFLVARI="+cp_ToStrODBC(this.w_DBFLVARI)+;
                     ",DBCODCOM="+cp_ToStrODBCNull(this.w_DBCODCOM)+;
                     ",DBARTCOM="+cp_ToStrODBCNull(this.w_DBARTCOM)+;
                     ",DBDESCOM="+cp_ToStrODBC(this.w_DBDESCOM)+;
                     ",DBUNIMIS="+cp_ToStrODBCNull(this.w_DBUNIMIS)+;
                     ",DBQTADIS="+cp_ToStrODBC(this.w_DBQTADIS)+;
                     ",DBCOEIMP="+cp_ToStrODBCNull(this.w_DBCOEIMP)+;
                     ",DBFLESPL="+cp_ToStrODBC(this.w_DBFLESPL)+;
                     ",DBPERSCA="+cp_ToStrODBC(this.w_DBPERSCA)+;
                     ",DBRECSCA="+cp_ToStrODBC(this.w_DBRECSCA)+;
                     ",DBPERSFR="+cp_ToStrODBC(this.w_DBPERSFR)+;
                     ",DBRECSFR="+cp_ToStrODBC(this.w_DBRECSFR)+;
                     ",DBPERRIC="+cp_ToStrODBC(this.w_DBPERRIC)+;
                     ",DBDATULT="+cp_ToStrODBC(this.w_DBDATULT)+;
                     ",DB__NOTE="+cp_ToStrODBC(this.w_DB__NOTE)+;
                     ",DBFLVARC="+cp_ToStrODBC(this.w_DBFLVARC)+;
                     ",DBINIVAL="+cp_ToStrODBC(this.w_DBINIVAL)+;
                     ",DBFINVAL="+cp_ToStrODBC(this.w_DBFINVAL)+;
                     ",DBCOEUM1="+cp_ToStrODBC(this.w_DBCOEUM1)+;
                     ",DBRIFFAS="+cp_ToStrODBCNull(this.w_DBRIFFAS)+;
                     ",DBCORLEA="+cp_ToStrODBC(this.w_DBCORLEA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DBFLVARI=this.w_DBFLVARI;
                     ,DBCODCOM=this.w_DBCODCOM;
                     ,DBARTCOM=this.w_DBARTCOM;
                     ,DBDESCOM=this.w_DBDESCOM;
                     ,DBUNIMIS=this.w_DBUNIMIS;
                     ,DBQTADIS=this.w_DBQTADIS;
                     ,DBCOEIMP=this.w_DBCOEIMP;
                     ,DBFLESPL=this.w_DBFLESPL;
                     ,DBPERSCA=this.w_DBPERSCA;
                     ,DBRECSCA=this.w_DBRECSCA;
                     ,DBPERSFR=this.w_DBPERSFR;
                     ,DBRECSFR=this.w_DBRECSFR;
                     ,DBPERRIC=this.w_DBPERRIC;
                     ,DBDATULT=this.w_DBDATULT;
                     ,DB__NOTE=this.w_DB__NOTE;
                     ,DBFLVARC=this.w_DBFLVARC;
                     ,DBINIVAL=this.w_DBINIVAL;
                     ,DBFINVAL=this.w_DBFINVAL;
                     ,DBCOEUM1=this.w_DBCOEUM1;
                     ,DBRIFFAS=this.w_DBRIFFAS;
                     ,DBCORLEA=this.w_DBCORLEA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSDS_MOU : Saving
      this.GSDS_MOU.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DBCODICE,"MOCODICE";
             ,this.w_CPROWNUM,"CPROWNUM";
             )
      this.GSDS_MOU.mReplace()
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM))))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSDS_MCV.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_DBCODICE,"CVCODICE";
             ,this.w_CPROWNUM,"CVNUMRIF";
             )
        this.GSDS_MCV.mReplace()
        this.GSDS_MLC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_DBCODICE,"CC__DIBA";
             ,this.w_CPROWNUM,"CCROWORD";
             )
        this.GSDS_MLC.mReplace()
        this.GSDS_MCV.bSaveContext=.f.
        this.GSDS_MLC.bSaveContext=.f.
      endscan
     this.GSDS_MCV.bSaveContext=.t.
     this.GSDS_MLC.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsds_mdb
    if this.w_VERDAT=.T. and not(bTrsErr)
       * --- Esegue Controlli Finali (deve farlo Sempre!)
       this.NotifyEvent('ControlliFinali')
       select (this.cTrsName)
       go top
       this.WorkFromTrs()
       this.SaveDependsOn()
       this.ChildrenChangeRow()
    endif
    
    
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSDS_MOU : Deleting
    this.GSDS_MOU.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DBCODICE,"MOCODICE";
           ,this.w_CPROWNUM,"CPROWNUM";
           )
    this.GSDS_MOU.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSDS_MCV : Deleting
        this.GSDS_MCV.bSaveContext=.f.
        this.GSDS_MCV.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_DBCODICE,"CVCODICE";
               ,this.w_CPROWNUM,"CVNUMRIF";
               )
        this.GSDS_MCV.bSaveContext=.t.
        this.GSDS_MCV.mDelete()
        * --- GSDS_MLC : Deleting
        this.GSDS_MLC.bSaveContext=.f.
        this.GSDS_MLC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_DBCODICE,"CC__DIBA";
               ,this.w_CPROWNUM,"CCROWORD";
               )
        this.GSDS_MLC.bSaveContext=.t.
        this.GSDS_MLC.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.DISTBASE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
        *
        * delete DISTBASE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.DISMBASE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
        *
        * delete DISMBASE
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
          .link_1_11('Full')
        .DoRTCalc(13,16,.t.)
        if .o_DBFLVARI<>.w_DBFLVARI
          .w_DBCODCOM = SPACE(20)
          .link_2_3('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_DBCODCOM<>.w_DBCODCOM
          .link_2_5('Full')
        endif
        .DoRTCalc(20,24,.t.)
          .link_2_11('Full')
        .DoRTCalc(26,29,.t.)
        if .o_DBFLVARI<>.w_DBFLVARI.or. .o_DBCODCOM<>.w_DBCODCOM
          .w_DBDESCOM = IIF(.w_DBFLVARI='S', .w_DBDESCOM, .w_DESART)
        endif
        if .o_DBCODCOM<>.w_DBCODCOM
          .w_DBUNIMIS = .w_UNMIS1
          .link_2_17('Full')
        endif
        .DoRTCalc(32,33,.t.)
        if .o_DBCODCOM<>.w_DBCODCOM
          .w_DBCOEIMP = SPACE(15)
          .link_2_20('Full')
        endif
        if .o_DBCODCOM<>.w_DBCODCOM
          .w_DBFLESPL = IIF(NOT EMPTY(.w_DBCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
        endif
          .w_POSIZ = .w_CPROWORD
        .DoRTCalc(37,37,.t.)
        if .o_DBPERSCA<>.w_DBPERSCA
          .w_DBRECSCA = 0
        endif
        .DoRTCalc(39,39,.t.)
        if .o_DBPERSFR<>.w_DBPERSFR
          .w_DBRECSFR = 0
        endif
        .DoRTCalc(41,43,.t.)
          .w_OBTEST = .w_UTDC
        .DoRTCalc(45,45,.t.)
          .w_DBFLVARC = .w_DBFLVARI
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
          .w_MAXLEVEL = 999
          .w_VERIFICA = 'S C'
          .w_EXPCOART = SPACE(20)
        .oPgFrm.Page3.oPag.TreeView.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_2.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_3.Calculate()
          .w_CODDIS = Nvl(.w_TREEVIEW.GETVAR('CODDIS'),Space(20))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
        .DoRTCalc(51,53,.t.)
        if .o_DBQTADIS<>.w_DBQTADIS.or. .o_DBUNIMIS<>.w_DBUNIMIS
          .w_DBCOEUM1 = CALQTAADV(.w_DBQTADIS,.w_DBUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "DBQTADIS")
        endif
          .w_NUMLEV = IIF(EMPTY(NVL(.w_TREEVIEW.GETVAR('NUMLEV'),' ')),'0000',.w_TREEVIEW.GETVAR('NUMLEV'))
        .DoRTCalc(56,57,.t.)
        if .o_DBCODRIS<>.w_DBCODRIS
          .w_CODRIS = .w_DBCODRIS
        endif
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(59,64,.t.)
          .w_CODDISB = .w_DBCODICE
          .w_DESDIS = .w_DBDESCRI
        if  .o_DBCODICE<>.w_DBCODICE.or. .o_DBCODCOM<>.w_DBCODCOM.or. .o_DBARTCOM<>.w_DBARTCOM
          .WriteTo_GSDS_MLC()
        endif
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        * --- Area Manuale = Calculate
        * --- gsds_mdb
        *Forzo condizione di Hide perch� non rieseguita in interroga
        if this.cFunction ='Query'
           this.mEnableControls()
        endif
        
        
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(67,70,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OPERA3 with this.w_OPERA3
      replace t_DBARTCOM with this.w_DBARTCOM
      replace t_TIPRIG with this.w_TIPRIG
      replace t_DTOBS1 with this.w_DTOBS1
      replace t_DESART with this.w_DESART
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_DBDATULT with this.w_DBDATULT
      replace t_DBFLVARC with this.w_DBFLVARC
      replace t_DBCOEUM1 with this.w_DBCOEUM1
      replace t_KITIMB with this.w_KITIMB
      replace t_NOFRAZ with this.w_NOFRAZ
      replace t_MODUM2 with this.w_MODUM2
      replace t_FLUSEP with this.w_FLUSEP
      replace t_ARGESCAR with this.w_ARGESCAR
      replace t_ARCMPCAR with this.w_ARCMPCAR
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page3.oPag.TreeView.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_2.Calculate()
        .oPgFrm.Page3.oPag.oObj_5_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
    endwith
  return
  proc Calculate_HZQCKHQIGM()
    with this
          * --- Aggiorna modifca da\il
          .w_UTCV = i_CODUTE
          .w_UTDV = i_DATSYS
          .bHeaderUpdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.enabled = this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_5.enabled = this.oPgFrm.Page3.oPag.oBtn_5_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_6.enabled = this.oPgFrm.Page3.oPag.oBtn_5_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_7.enabled = this.oPgFrm.Page3.oPag.oBtn_5_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_8.enabled = this.oPgFrm.Page3.oPag.oBtn_5_8.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_9.enabled = this.oPgFrm.Page3.oPag.oBtn_5_9.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBCODCOM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBCODCOM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBUNIMIS_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBUNIMIS_2_17.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBQTADIS_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBQTADIS_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBCOEIMP_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBCOEIMP_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBFLESPL_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBFLESPL_2_21.mCond()
    this.oPgFrm.Page1.oPag.oDBRECSCA_2_24.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDBRECSCA_2_24.mCond()
    this.oPgFrm.Page1.oPag.oDBRECSFR_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDBRECSFR_2_26.mCond()
    this.oPgFrm.Page1.oPag.oDBINIVAL_2_33.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDBINIVAL_2_33.mCond()
    this.oPgFrm.Page1.oPag.oDBFINVAL_2_34.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDBFINVAL_2_34.mCond()
    this.oPgFrm.Page1.oPag.oDBRIFFAS_2_36.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oDBRIFFAS_2_36.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_30.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_30.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSDS_MCV.visible")=='L' And this.GSDS_MCV.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_30.enabled
      this.GSDS_MCV.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_46.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_46.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSDS_MLC.visible")=='L' And this.GSDS_MLC.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_46.enabled
      this.GSDS_MLC.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.visible=!this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDESCIC_1_19.visible=!this.oPgFrm.Page1.oPag.oDESCIC_1_19.mHide()
    this.oPgFrm.Page3.oPag.oCODDIS_5_4.visible=!this.oPgFrm.Page3.oPag.oCODDIS_5_4.mHide()
    this.oPgFrm.Page3.oPag.oStr_5_10.visible=!this.oPgFrm.Page3.oPag.oStr_5_10.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDISCOM_2_15.visible=!this.oPgFrm.Page1.oPag.oDISCOM_2_15.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_2_30.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_30.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSDS_MCV.visible")=='L' And this.GSDS_MCV.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_30.visible
      this.GSDS_MCV.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_46.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_46.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSDS_MLC.visible")=='L' And this.GSDS_MLC.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_46.visible
      this.GSDS_MLC.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsds_mdb
    IF Upper(CEVENT)='W_DBFLVARI CHANGED'
      if Upper(this.GSDS_MCV.class)='STDLAZYCHILD'
        this.GSDS_MCV.linkPCClick()
        this.GSDS_MCV.Hide()
        NC = this.GSDS_MCV.cnt.ctrsName
        Select (NC)
        GO TOP
        DELETE ALL
        this.GSDS_MCV.cnt.InitRow()
      Endif  
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page3.oPag.TreeView.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_5_2.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_5_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_38.Event(cEvent)
        if lower(cEvent)==lower("Edit Started")
          .Calculate_HZQCKHQIGM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DBCODRIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_MCS',True,'TABMCICL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_DBCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_DBCODRIS))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODRIS)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODRIS) and !this.bDontReportError
            deferred_cp_zoom('TABMCICL','*','CSCODICE',cp_AbsName(oSource.parent,'oDBCODRIS_1_4'),i_cWhere,'GSDS_MCS',"Cicli semplificati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_DBCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_DBCODRIS)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODRIS = NVL(_Link_.CSCODICE,space(15))
      this.w_DESCIC = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODRIS = space(15)
      endif
      this.w_DESCIC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TABMCICL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.CSCODICE as CSCODICE104"+ ",link_1_4.CSDESCRI as CSDESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on DISMBASE.DBCODRIS=link_1_4.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and DISMBASE.DBCODRIS=link_1_4.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DIBA_IDX,3]
    i_lTable = "TIP_DIBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2], .t., this.TIP_DIBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDDFAULT,TDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TDDFAULT="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDDFAULT',this.w_READPAR)
            select TDDFAULT,TDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.TDDFAULT,space(1))
      this.w_TIPDEFA = NVL(_Link_.TDCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(1)
      endif
      this.w_TIPDEFA = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DIBA_IDX,2])+'\'+cp_ToStr(_Link_.TDDFAULT,1)
      cp_ShowWarn(i_cKey,this.TIP_DIBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODCOM))
          select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODCOM)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODCOM) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODCOM_2_3'),i_cWhere,'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODCOM)
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODCOM = NVL(_Link_.CACODICE,space(20))
      this.w_DBARTCOM = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODCOM = space(20)
      endif
      this.w_DBARTCOM = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DESART = space(40)
      this.w_TIPRIG = space(1)
      this.w_DTOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG='R' AND (.w_DTOBS1>.w_UTDC OR EMPTY(.w_DTOBS1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_DBCODCOM = space(20)
        this.w_DBARTCOM = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DESART = space(40)
        this.w_TIPRIG = space(1)
        this.w_DTOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CAUNIMIS as CAUNIMIS203"+ ",link_2_3.CAOPERAT as CAOPERAT203"+ ",link_2_3.CAMOLTIP as CAMOLTIP203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CA__TIPO as CA__TIPO203"+ ",link_2_3.CADTOBSO as CADTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on DISTBASE.DBCODCOM=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and DISTBASE.DBCODCOM=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DBARTCOM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBARTCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBARTCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODDIS,ARKITIMB,ARFLUSEP,ARCMPCAR,ARGESCAR";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DBARTCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DBARTCOM)
            select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODDIS,ARKITIMB,ARFLUSEP,ARCMPCAR,ARGESCAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBARTCOM = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_DISCOM = NVL(_Link_.ARCODDIS,space(20))
      this.w_KITIMB = NVL(_Link_.ARKITIMB,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_ARCMPCAR = NVL(_Link_.ARCMPCAR,space(1))
      this.w_ARGESCAR = NVL(_Link_.ARGESCAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DBARTCOM = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_DISCOM = space(20)
      this.w_KITIMB = space(1)
      this.w_FLUSEP = space(1)
      this.w_ARCMPCAR = space(1)
      this.w_ARGESCAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBARTCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ARCODART as ARCODART205"+ ",link_2_5.ARUNMIS1 as ARUNMIS1205"+ ",link_2_5.ARUNMIS2 as ARUNMIS2205"+ ",link_2_5.AROPERAT as AROPERAT205"+ ",link_2_5.ARMOLTIP as ARMOLTIP205"+ ",link_2_5.ARCODDIS as ARCODDIS205"+ ",link_2_5.ARKITIMB as ARKITIMB205"+ ",link_2_5.ARFLUSEP as ARFLUSEP205"+ ",link_2_5.ARCMPCAR as ARCMPCAR205"+ ",link_2_5.ARGESCAR as ARGESCAR205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on DISTBASE.DBARTCOM=link_2_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and DISTBASE.DBARTCOM=link_2_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBUNIMIS
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DBUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_DBUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oDBUNIMIS_2_17'),i_cWhere,'GSAR_AUM',"Unit� di misuta",'GSDS_MDB.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DBUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_DBUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DBUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_DBUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_DBUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.UMCODICE as UMCODICE217"+ ",link_2_17.UMFLFRAZ as UMFLFRAZ217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on DISTBASE.DBUNIMIS=link_2_17.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and DISTBASE.DBUNIMIS=link_2_17.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DBCOEIMP
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COE_IMPI_IDX,3]
    i_lTable = "COE_IMPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2], .t., this.COE_IMPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCOEIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ACI',True,'COE_IMPI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CICODICE like "+cp_ToStrODBC(trim(this.w_DBCOEIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CICODICE',trim(this.w_DBCOEIMP))
          select CICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCOEIMP)==trim(_Link_.CICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCOEIMP) and !this.bDontReportError
            deferred_cp_zoom('COE_IMPI','*','CICODICE',cp_AbsName(oSource.parent,'oDBCOEIMP_2_20'),i_cWhere,'GSDS_ACI',"Coefficienti di impiego",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODICE',oSource.xKey(1))
            select CICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCOEIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CICODICE="+cp_ToStrODBC(this.w_DBCOEIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODICE',this.w_DBCOEIMP)
            select CICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCOEIMP = NVL(_Link_.CICODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DBCOEIMP = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COE_IMPI_IDX,2])+'\'+cp_ToStr(_Link_.CICODICE,1)
      cp_ShowWarn(i_cKey,this.COE_IMPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCOEIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBRIFFAS
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CICL_IDX,3]
    i_lTable = "TAB_CICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2], .t., this.TAB_CICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBRIFFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_MCS',True,'TAB_CICL')
        if i_nConn<>0
          i_cWhere = " CPROWORD="+cp_ToStrODBC(this.w_DBRIFFAS);
                   +" and CSCODICE="+cp_ToStrODBC(this.w_CODRIS);

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CPROWORD,CSDESFAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',this.w_CODRIS;
                     ,'CPROWORD',this.w_DBRIFFAS)
          select CSCODICE,CPROWORD,CSDESFAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DBRIFFAS) and !this.bDontReportError
            deferred_cp_zoom('TAB_CICL','*','CSCODICE,CPROWORD',cp_AbsName(oSource.parent,'oDBRIFFAS_2_36'),i_cWhere,'GSDS_MCS',"Fasi di lavorazione",'GSDS_MDB.TAB_CICL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CPROWORD,CSDESFAS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CSCODICE,CPROWORD,CSDESFAS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CPROWORD,CSDESFAS";
                     +" from "+i_cTable+" "+i_lTable+" where CPROWORD="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CSCODICE="+cp_ToStrODBC(this.w_CODRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1);
                       ,'CPROWORD',oSource.xKey(2))
            select CSCODICE,CPROWORD,CSDESFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBRIFFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CPROWORD,CSDESFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWORD="+cp_ToStrODBC(this.w_DBRIFFAS);
                   +" and CSCODICE="+cp_ToStrODBC(this.w_CODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_CODRIS;
                       ,'CPROWORD',this.w_DBRIFFAS)
            select CSCODICE,CPROWORD,CSDESFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBRIFFAS = NVL(_Link_.CPROWORD,0)
      this.w_DESFAS = NVL(_Link_.CSDESFAS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBRIFFAS = 0
      endif
      this.w_DESFAS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)+'\'+cp_ToStr(_Link_.CPROWORD,1)
      cp_ShowWarn(i_cKey,this.TAB_CICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBRIFFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDBCODICE_1_1.value==this.w_DBCODICE)
      this.oPgFrm.Page1.oPag.oDBCODICE_1_1.value=this.w_DBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_2.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_2.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBFLSTAT_1_3.RadioValue()==this.w_DBFLSTAT)
      this.oPgFrm.Page1.oPag.oDBFLSTAT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.value==this.w_DBCODRIS)
      this.oPgFrm.Page1.oPag.oDBCODRIS_1_4.value=this.w_DBCODRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDC_1_7.value==this.w_UTDC)
      this.oPgFrm.Page1.oPag.oUTDC_1_7.value=this.w_UTDC
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPRIORI_1_8.value==this.w_DBPRIORI)
      this.oPgFrm.Page1.oPag.oDBPRIORI_1_8.value=this.w_DBPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDTINVA_1_9.value==this.w_DBDTINVA)
      this.oPgFrm.Page1.oPag.oDBDTINVA_1_9.value=this.w_DBDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDTOBSO_1_10.value==this.w_DBDTOBSO)
      this.oPgFrm.Page1.oPag.oDBDTOBSO_1_10.value=this.w_DBDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oDBTIPDIS_1_13.RadioValue()==this.w_DBTIPDIS)
      this.oPgFrm.Page1.oPag.oDBTIPDIS_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDISCOM_2_15.value==this.w_DISCOM)
      this.oPgFrm.Page1.oPag.oDISCOM_2_15.value=this.w_DISCOM
      replace t_DISCOM with this.oPgFrm.Page1.oPag.oDISCOM_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPOSIZ_2_22.value==this.w_POSIZ)
      this.oPgFrm.Page1.oPag.oPOSIZ_2_22.value=this.w_POSIZ
      replace t_POSIZ with this.oPgFrm.Page1.oPag.oPOSIZ_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPERSCA_2_23.value==this.w_DBPERSCA)
      this.oPgFrm.Page1.oPag.oDBPERSCA_2_23.value=this.w_DBPERSCA
      replace t_DBPERSCA with this.oPgFrm.Page1.oPag.oDBPERSCA_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBRECSCA_2_24.value==this.w_DBRECSCA)
      this.oPgFrm.Page1.oPag.oDBRECSCA_2_24.value=this.w_DBRECSCA
      replace t_DBRECSCA with this.oPgFrm.Page1.oPag.oDBRECSCA_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPERSFR_2_25.value==this.w_DBPERSFR)
      this.oPgFrm.Page1.oPag.oDBPERSFR_2_25.value=this.w_DBPERSFR
      replace t_DBPERSFR with this.oPgFrm.Page1.oPag.oDBPERSFR_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBRECSFR_2_26.value==this.w_DBRECSFR)
      this.oPgFrm.Page1.oPag.oDBRECSFR_2_26.value=this.w_DBRECSFR
      replace t_DBRECSFR with this.oPgFrm.Page1.oPag.oDBRECSFR_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBPERRIC_2_27.value==this.w_DBPERRIC)
      this.oPgFrm.Page1.oPag.oDBPERRIC_2_27.value=this.w_DBPERRIC
      replace t_DBPERRIC with this.oPgFrm.Page1.oPag.oDBPERRIC_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDB__NOTE_2_29.value==this.w_DB__NOTE)
      this.oPgFrm.Page1.oPag.oDB__NOTE_2_29.value=this.w_DB__NOTE
      replace t_DB__NOTE with this.oPgFrm.Page1.oPag.oDB__NOTE_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCIC_1_19.value==this.w_DESCIC)
      this.oPgFrm.Page1.oPag.oDESCIC_1_19.value=this.w_DESCIC
    endif
    if not(this.oPgFrm.Page3.oPag.oCODDIS_5_4.value==this.w_CODDIS)
      this.oPgFrm.Page3.oPag.oCODDIS_5_4.value=this.w_CODDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDBINIVAL_2_33.value==this.w_DBINIVAL)
      this.oPgFrm.Page1.oPag.oDBINIVAL_2_33.value=this.w_DBINIVAL
      replace t_DBINIVAL with this.oPgFrm.Page1.oPag.oDBINIVAL_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBFINVAL_2_34.value==this.w_DBFINVAL)
      this.oPgFrm.Page1.oPag.oDBFINVAL_2_34.value=this.w_DBFINVAL
      replace t_DBFINVAL with this.oPgFrm.Page1.oPag.oDBFINVAL_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDBRIFFAS_2_36.value==this.w_DBRIFFAS)
      this.oPgFrm.Page1.oPag.oDBRIFFAS_2_36.value=this.w_DBRIFFAS
      replace t_DBRIFFAS with this.oPgFrm.Page1.oPag.oDBRIFFAS_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAS_2_37.value==this.w_DESFAS)
      this.oPgFrm.Page1.oPag.oDESFAS_2_37.value=this.w_DESFAS
      replace t_DESFAS with this.oPgFrm.Page1.oPag.oDESFAS_2_37.value
    endif
    if not(this.oPgFrm.Page4.oPag.oCODDISB_6_1.value==this.w_CODDISB)
      this.oPgFrm.Page4.oPag.oCODDISB_6_1.value=this.w_CODDISB
    endif
    if not(this.oPgFrm.Page4.oPag.oDESDIS_6_2.value==this.w_DESDIS)
      this.oPgFrm.Page4.oPag.oDESDIS_6_2.value=this.w_DESDIS
    endif
    if not(this.oPgFrm.Page4.oPag.oDBNOTAGG_6_3.value==this.w_DBNOTAGG)
      this.oPgFrm.Page4.oPag.oDBNOTAGG_6_3.value=this.w_DBNOTAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCORLEA_2_43.value==this.w_DBCORLEA)
      this.oPgFrm.Page1.oPag.oDBCORLEA_2_43.value=this.w_DBCORLEA
      replace t_DBCORLEA with this.oPgFrm.Page1.oPag.oDBCORLEA_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.RadioValue()==this.w_DBFLVARI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.SetRadio()
      replace t_DBFLVARI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_3.value==this.w_DBCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_3.value=this.w_DBCODCOM
      replace t_DBCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_16.value==this.w_DBDESCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_16.value=this.w_DBDESCOM
      replace t_DBDESCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBUNIMIS_2_17.value==this.w_DBUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBUNIMIS_2_17.value=this.w_DBUNIMIS
      replace t_DBUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBUNIMIS_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_19.value==this.w_DBQTADIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_19.value=this.w_DBQTADIS
      replace t_DBQTADIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCOEIMP_2_20.value==this.w_DBCOEIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCOEIMP_2_20.value=this.w_DBCOEIMP
      replace t_DBCOEIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCOEIMP_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.RadioValue()==this.w_DBFLESPL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.SetRadio()
      replace t_DBFLESPL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'DISMBASE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDBCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DBCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_UTDC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oUTDC_1_7.SetFocus()
            i_bnoObbl = !empty(.w_UTDC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBPRIORI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDBPRIORI_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DBPRIORI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBTIPDIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDBTIPDIS_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DBTIPDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSDS_MOU.CheckForm()
      if i_bres
        i_bres=  .GSDS_MOU.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsds_mdb
      this.w_VERDAT=.f.
      if i_bRes = .t. and this.cFunction<>'Query'
        this.w_VERDAT = ah_YESNO('Si vuole la verifica dei componenti inseriti?')
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM))));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPRIG='R' AND (.w_DTOBS1>.w_UTDC OR EMPTY(.w_DTOBS1))) and (.w_DBFLVARI<>'S') and not(empty(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
        case   (empty(.w_DBUNIMIS) or not(CHKUNIMI(.w_DBUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (NOT EMPTY(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBUNIMIS_2_17
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_DBQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_DBQTADIS=INT(.w_DBQTADIS))) and (NOT EMPTY(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_19
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   empty(.w_DBINIVAL) and (.w_DBFLVARI='S' OR NOT EMPTY(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDBINIVAL_2_33
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_DBFINVAL) or not(.w_DBFINVAL>=.w_DBINIVAL)) and (.w_DBFLVARI='S' OR NOT EMPTY(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDBFINVAL_2_34
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di fine validit� minore o uguale alla data di inizio validit�")
      endcase
      i_bRes = i_bRes .and. .GSDS_MCV.CheckForm()
      i_bRes = i_bRes .and. .GSDS_MLC.CheckForm()
      if not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM)))
        * --- Area Manuale = Check Row
        * --- gsds_mdb
        * Controllo che il componente selezionato su riga non
        * abbia associato un kit imballo o un articolo kit
        If g_VEFA='S'And (CHKKITIMB(.w_DISCOM) $'IK' Or .w_KITIMB<>'N') And Not Empty(.w_DBARTCOM)
           i_bRes = .f.
           i_bnoChk = .f.
        	 i_cErrorMsg = Ah_MsgFormat("Impossibile selezionare un componente di tipo imballo o che ha associato un articolo kit o un kit imballo.")
        Endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DBCODICE = this.w_DBCODICE
    this.o_DBCODRIS = this.w_DBCODRIS
    this.o_DBFLVARI = this.w_DBFLVARI
    this.o_DBCODCOM = this.w_DBCODCOM
    this.o_DBARTCOM = this.w_DBARTCOM
    this.o_DBUNIMIS = this.w_DBUNIMIS
    this.o_DBQTADIS = this.w_DBQTADIS
    this.o_DBPERSCA = this.w_DBPERSCA
    this.o_DBPERSFR = this.w_DBPERSFR
    * --- GSDS_MCV : Depends On
    this.GSDS_MCV.SaveDependsOn()
    * --- GSDS_MOU : Depends On
    this.GSDS_MOU.SaveDependsOn()
    * --- GSDS_MLC : Depends On
    this.GSDS_MLC.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DBFLVARI=space(1)
      .w_DBCODCOM=space(20)
      .w_OPERA3=space(1)
      .w_DBARTCOM=space(20)
      .w_TIPRIG=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DISCOM=space(20)
      .w_DBDESCOM=space(40)
      .w_DBUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_DBQTADIS=0
      .w_DBCOEIMP=space(15)
      .w_DBFLESPL=space(1)
      .w_POSIZ=0
      .w_DBPERSCA=0
      .w_DBRECSCA=0
      .w_DBPERSFR=0
      .w_DBRECSFR=0
      .w_DBPERRIC=0
      .w_DBDATULT=ctod("  /  /  ")
      .w_DB__NOTE=space(0)
      .w_DBFLVARC=space(1)
      .w_DBINIVAL=ctod("  /  /  ")
      .w_DBFINVAL=ctod("  /  /  ")
      .w_DBCOEUM1=0
      .w_DBRIFFAS=0
      .w_DESFAS=space(40)
      .w_KITIMB=space(1)
      .w_NOFRAZ=space(1)
      .w_MODUM2=space(1)
      .w_FLUSEP=space(1)
      .w_DBCORLEA=0
      .w_ARGESCAR=space(1)
      .w_ARCMPCAR=space(1)
      .DoRTCalc(1,16,.f.)
        .w_DBCODCOM = SPACE(20)
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_DBCODCOM))
        .link_2_3('Full')
      endif
      .DoRTCalc(18,19,.f.)
      if not(empty(.w_DBARTCOM))
        .link_2_5('Full')
      endif
      .DoRTCalc(20,25,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_11('Full')
      endif
      .DoRTCalc(26,29,.f.)
        .w_DBDESCOM = IIF(.w_DBFLVARI='S', .w_DBDESCOM, .w_DESART)
        .w_DBUNIMIS = .w_UNMIS1
      .DoRTCalc(31,31,.f.)
      if not(empty(.w_DBUNIMIS))
        .link_2_17('Full')
      endif
      .DoRTCalc(32,33,.f.)
        .w_DBCOEIMP = SPACE(15)
      .DoRTCalc(34,34,.f.)
      if not(empty(.w_DBCOEIMP))
        .link_2_20('Full')
      endif
        .w_DBFLESPL = IIF(NOT EMPTY(.w_DBCODCOM) AND NOT EMPTY(.w_DISCOM), 'S', ' ')
        .w_POSIZ = .w_CPROWORD
      .DoRTCalc(37,37,.f.)
        .w_DBRECSCA = 0
      .DoRTCalc(39,39,.f.)
        .w_DBRECSFR = 0
      .DoRTCalc(41,45,.f.)
        .w_DBFLVARC = .w_DBFLVARI
        .oPgFrm.Page1.oPag.oObj_2_32.Calculate(IIF(EMPTY(.w_DISCOM), '', AH_Msgformat('Distinta:')),RGB(0,0,0),IIF(EMPTY(.w_DISCOM), '',RGB(192,192,192)))
      .DoRTCalc(47,50,.f.)
        .w_DBINIVAL = i_DATSYS
        .w_DBFINVAL = i_FINDAT
      .DoRTCalc(53,53,.f.)
        .w_DBCOEUM1 = CALQTAADV(.w_DBQTADIS,.w_DBUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD,,This, "DBQTADIS")
      .DoRTCalc(55,56,.f.)
      if not(empty(.w_DBRIFFAS))
        .link_2_36('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_38.Calculate()
    endwith
    this.DoRTCalc(57,70,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DBFLVARI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.RadioValue(.t.)
    this.w_DBCODCOM = t_DBCODCOM
    this.w_OPERA3 = t_OPERA3
    this.w_DBARTCOM = t_DBARTCOM
    this.w_TIPRIG = t_TIPRIG
    this.w_DTOBS1 = t_DTOBS1
    this.w_DESART = t_DESART
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_DISCOM = t_DISCOM
    this.w_DBDESCOM = t_DBDESCOM
    this.w_DBUNIMIS = t_DBUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_DBQTADIS = t_DBQTADIS
    this.w_DBCOEIMP = t_DBCOEIMP
    this.w_DBFLESPL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.RadioValue(.t.)
    this.w_POSIZ = t_POSIZ
    this.w_DBPERSCA = t_DBPERSCA
    this.w_DBRECSCA = t_DBRECSCA
    this.w_DBPERSFR = t_DBPERSFR
    this.w_DBRECSFR = t_DBRECSFR
    this.w_DBPERRIC = t_DBPERRIC
    this.w_DBDATULT = t_DBDATULT
    this.w_DB__NOTE = t_DB__NOTE
    this.w_DBFLVARC = t_DBFLVARC
    this.w_DBINIVAL = t_DBINIVAL
    this.w_DBFINVAL = t_DBFINVAL
    this.w_DBCOEUM1 = t_DBCOEUM1
    this.w_DBRIFFAS = t_DBRIFFAS
    this.w_DESFAS = t_DESFAS
    this.w_KITIMB = t_KITIMB
    this.w_NOFRAZ = t_NOFRAZ
    this.w_MODUM2 = t_MODUM2
    this.w_FLUSEP = t_FLUSEP
    this.w_DBCORLEA = t_DBCORLEA
    this.w_ARGESCAR = t_ARGESCAR
    this.w_ARCMPCAR = t_ARCMPCAR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DBFLVARI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLVARI_2_2.ToRadio()
    replace t_DBCODCOM with this.w_DBCODCOM
    replace t_OPERA3 with this.w_OPERA3
    replace t_DBARTCOM with this.w_DBARTCOM
    replace t_TIPRIG with this.w_TIPRIG
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_DESART with this.w_DESART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_DISCOM with this.w_DISCOM
    replace t_DBDESCOM with this.w_DBDESCOM
    replace t_DBUNIMIS with this.w_DBUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_DBQTADIS with this.w_DBQTADIS
    replace t_DBCOEIMP with this.w_DBCOEIMP
    replace t_DBFLESPL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBFLESPL_2_21.ToRadio()
    replace t_POSIZ with this.w_POSIZ
    replace t_DBPERSCA with this.w_DBPERSCA
    replace t_DBRECSCA with this.w_DBRECSCA
    replace t_DBPERSFR with this.w_DBPERSFR
    replace t_DBRECSFR with this.w_DBRECSFR
    replace t_DBPERRIC with this.w_DBPERRIC
    replace t_DBDATULT with this.w_DBDATULT
    replace t_DB__NOTE with this.w_DB__NOTE
    replace t_DBFLVARC with this.w_DBFLVARC
    replace t_DBINIVAL with this.w_DBINIVAL
    replace t_DBFINVAL with this.w_DBFINVAL
    replace t_DBCOEUM1 with this.w_DBCOEUM1
    replace t_DBRIFFAS with this.w_DBRIFFAS
    replace t_DESFAS with this.w_DESFAS
    replace t_KITIMB with this.w_KITIMB
    replace t_NOFRAZ with this.w_NOFRAZ
    replace t_MODUM2 with this.w_MODUM2
    replace t_FLUSEP with this.w_FLUSEP
    replace t_DBCORLEA with this.w_DBCORLEA
    replace t_ARGESCAR with this.w_ARGESCAR
    replace t_ARCMPCAR with this.w_ARCMPCAR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsds_mdbPag1 as StdContainer
  Width  = 787
  height = 450
  stdWidth  = 787
  stdheight = 450
  resizeXpos=283
  resizeYpos=294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDBCODICE_1_1 as StdField with uid="KIDIKRLIGV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DBCODICE", cQueryName = "DBCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della distinta base",;
    HelpContextID = 147460731,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=9, InputMask=replicate('X',20)

  add object oDBDESCRI_1_2 as StdField with uid="QWYICCNFWF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della distinta base",;
    HelpContextID = 206560641,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=9, InputMask=replicate('X',40)


  add object oDBFLSTAT_1_3 as StdCombo with uid="HQJNSIFZVE",rtseq=3,rtrep=.f.,left=661,top=11,width=107,height=21;
    , tabstop=.f.;
    , ToolTipText = "Status della distinta: confermata o provvisoria";
    , HelpContextID = 189316470;
    , cFormVar="w_DBFLSTAT",RowSource=""+"Confermata,"+"Provvisoria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDBFLSTAT_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DBFLSTAT,&i_cF..t_DBFLSTAT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'P',;
    space(1))))
  endfunc
  func oDBFLSTAT_1_3.GetRadio()
    this.Parent.oContained.w_DBFLSTAT = this.RadioValue()
    return .t.
  endfunc

  func oDBFLSTAT_1_3.ToRadio()
    this.Parent.oContained.w_DBFLSTAT=trim(this.Parent.oContained.w_DBFLSTAT)
    return(;
      iif(this.Parent.oContained.w_DBFLSTAT=='S',1,;
      iif(this.Parent.oContained.w_DBFLSTAT=='P',2,;
      0)))
  endfunc

  func oDBFLSTAT_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDBCODRIS_1_4 as StdField with uid="UKTPKBISVK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DBCODRIS", cQueryName = "DBCODRIS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo semplificato associato alla distinta",;
    HelpContextID = 30020233,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=106, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TABMCICL", cZoomOnZoom="GSDS_MCS", oKey_1_1="CSCODICE", oKey_1_2="this.w_DBCODRIS"

  func oDBCODRIS_1_4.mCond()
    with this.Parent.oContained
      return (g_CICLILAV<>'S')
    endwith
  endfunc

  func oDBCODRIS_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' and g_CICLILAV='S')
    endwith
    endif
  endfunc

  func oDBCODRIS_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODRIS_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODRIS_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TABMCICL','*','CSCODICE',cp_AbsName(this.parent,'oDBCODRIS_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_MCS',"Cicli semplificati",'',this.parent.oContained
  endproc
  proc oDBCODRIS_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSDS_MCS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_DBCODRIS
    i_obj.ecpSave()
  endproc

  add object oUTDC_1_7 as StdField with uid="WILYXBYQNT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_UTDC", cQueryName = "UTDC",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data di creazione della distinta base",;
    HelpContextID = 75614906,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=692, Top=38, tabstop=.f.

  add object oDBPRIORI_1_8 as StdField with uid="FRMYVEJXIR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DBPRIORI", cQueryName = "DBPRIORI",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Priorit� distinta",;
    HelpContextID = 14818689,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=106, Top=67, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  add object oDBDTINVA_1_9 as StdField with uid="BRDKQNRGIN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DBDTINVA", cQueryName = "DBDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� della distinta base",;
    HelpContextID = 236921463,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=482, Top=67, tabstop=.f.

  add object oDBDTOBSO_1_10 as StdField with uid="VDIQBARXLE",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DBDTOBSO", cQueryName = "DBDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit� della distinta base (vuota=illimitata)",;
    HelpContextID = 41886341,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=692, Top=67, tabstop=.f.


  add object oDBTIPDIS_1_13 as StdTableCombo with uid="WYIOHKEORV",rtseq=14,rtrep=.f.,left=106,top=96,width=153,height=22;
    , cDescEmptyElement='Selezionare una chiave';
    , ToolTipText = "Tipologia distinta";
    , HelpContextID = 75833993;
    , cFormVar="w_DBTIPDIS",tablefilter="", bObbl = .t. , nPag = 1;
    , cTable='TIP_DIBA',cKey='TDCODICE',cValue='TDDESCRI',cOrderBy='',xDefault=space(4);
  , bGlobalFont=.t.


  add object oDESCIC_1_19 as StdField with uid="MSSJIFYLSL",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESCIC", cQueryName = "DESCIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 217115338,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=265, Top=38, InputMask=replicate('X',40)

  func oDESCIC_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' and g_CICLILAV='S')
    endwith
    endif
  endfunc


  add object oObj_1_27 as cp_runprogram with uid="RSHMYAJKXV",left=2, top=469, width=202,height=22,;
    caption='GSDS_BKD',;
   bGlobalFont=.t.,;
    prg="GSDS_BKD",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 209833046


  add object oObj_1_31 as cp_runprogram with uid="ZXOIPZZCYZ",left=221, top=469, width=202,height=22,;
    caption='GSAR_BEL',;
   bGlobalFont=.t.,;
    prg="GSAR_BEL('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 58524594


  add object oObj_1_40 as cp_runprogram with uid="PZISWIOBME",left=466, top=468, width=202,height=22,;
    caption='GSDS_B2D(R)',;
   bGlobalFont=.t.,;
    prg="GSDS_B2D('R')",;
    cEvent = "w_DBCODRIS Changed",;
    nPag=1;
    , HelpContextID = 209643478


  add object oObj_1_43 as cp_runprogram with uid="SNAEBQBSFW",left=481, top=498, width=202,height=22,;
    caption='GSDS_B2D(V)',;
   bGlobalFont=.t.,;
    prg="GSDS_B2D('V')",;
    cEvent = "w_DBFLVARI Changed",;
    nPag=1;
    , HelpContextID = 209642454


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=126, width=756,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Posiz.",Field2="DBFLVARI",Label2="Variante",Field3="DBCODCOM",Label3="Componente",Field4="DBDESCOM",Label4="Descrizione",Field5="DBUNIMIS",Label5="U.M.",Field6="DBQTADIS",Label6="Quantit�",Field7="DBCOEIMP",Label7="Coeff. impiego",Field8="DBFLESPL",Label8="Espl.Com.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235783546


  add object oObj_1_48 as cp_runprogram with uid="NCMXZFOZYY",left=481, top=519, width=202,height=22,;
    caption='GSDS_B2D(C)',;
   bGlobalFont=.t.,;
    prg="GSDS_B2D('C')",;
    cEvent = "w_DBFLVARI Changed,w_DBCODCOM Changed",;
    nPag=1;
    , HelpContextID = 209647318

  add object oStr_1_16 as StdString with uid="HGKCEJRURI",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=101, Height=18,;
    Caption="Distinta:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="NDYKMAZDLI",Visible=.t., Left=587, Top=38,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KAFVVFIGJR",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=101, Height=18,;
    Caption="Codice ciclo:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (g_PRFA='S' and g_CICLILAV='S')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="XXEGPVIFWX",Visible=.t., Left=1, Top=344,;
    Alignment=1, Width=66, Height=18,;
    Caption="Posizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RHTWGWCWQO",Visible=.t., Left=128, Top=344,;
    Alignment=1, Width=83, Height=18,;
    Caption="% Scarto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ICDFWHCYHZ",Visible=.t., Left=119, Top=371,;
    Alignment=1, Width=92, Height=18,;
    Caption="% Rec.scarto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QRCLGIGAKP",Visible=.t., Left=287, Top=344,;
    Alignment=1, Width=102, Height=18,;
    Caption="% Sfrido:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PWSVQDTSEN",Visible=.t., Left=286, Top=371,;
    Alignment=1, Width=103, Height=18,;
    Caption="% Rec.sfrido:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="OBDHHTMIUF",Visible=.t., Left=5, Top=398,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="FCDSTBQZMQ",Visible=.t., Left=461, Top=344,;
    Alignment=1, Width=112, Height=18,;
    Caption="% Ricarico costi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FKAXWMAINA",Visible=.t., Left=382, Top=68,;
    Alignment=1, Width=98, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IVIUDTPVKG",Visible=.t., Left=567, Top=67,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BFESZEKONO",Visible=.t., Left=610, Top=371,;
    Alignment=1, Width=96, Height=18,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="QAXXJFVGFE",Visible=.t., Left=605, Top=398,;
    Alignment=1, Width=101, Height=18,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="MSYHJFXVZX",Visible=.t., Left=604, Top=11,;
    Alignment=1, Width=54, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="AANWKUSPZJ",Visible=.t., Left=264, Top=425,;
    Alignment=1, Width=103, Height=18,;
    Caption="N.fase:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="RCJZFSLMOL",Visible=.t., Left=17, Top=68,;
    Alignment=1, Width=88, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="MEHDPYFNXH",Visible=.t., Left=-1, Top=97,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipologia distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="FVZAZZHZXO",Visible=.t., Left=646, Top=344,;
    Alignment=1, Width=73, Height=18,;
    Caption="Corr. L.T.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=145,;
    width=752+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=146,width=751+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|COE_IMPI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDISCOM_2_15.Refresh()
      this.Parent.oPOSIZ_2_22.Refresh()
      this.Parent.oDBPERSCA_2_23.Refresh()
      this.Parent.oDBRECSCA_2_24.Refresh()
      this.Parent.oDBPERSFR_2_25.Refresh()
      this.Parent.oDBRECSFR_2_26.Refresh()
      this.Parent.oDBPERRIC_2_27.Refresh()
      this.Parent.oDB__NOTE_2_29.Refresh()
      this.Parent.oDBINIVAL_2_33.Refresh()
      this.Parent.oDBFINVAL_2_34.Refresh()
      this.Parent.oDBRIFFAS_2_36.Refresh()
      this.Parent.oDESFAS_2_37.Refresh()
      this.Parent.oDBCORLEA_2_43.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDBCODCOM_2_3
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oDBUNIMIS_2_17
      case cFile='COE_IMPI'
        oDropInto=this.oBodyCol.oRow.oDBCOEIMP_2_20
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDISCOM_2_15 as StdTrsField with uid="YGIJNFTUAT",rtseq=29,rtrep=.t.,;
    cFormVar="w_DISCOM",value=space(20),enabled=.f.,;
    ToolTipText = "Codice distinta base associata",;
    HelpContextID = 43050698,;
    cTotal="", bFixedPos=.t., cQueryName = "DISCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=82, Top=425, InputMask=replicate('X',20)

  func oDISCOM_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DISCOM))
    endwith
    endif
  endfunc

  add object oPOSIZ_2_22 as StdTrsField with uid="QFPCZUPISU",rtseq=36,rtrep=.t.,;
    cFormVar="w_POSIZ",value=0,enabled=.f.,;
    HelpContextID = 249225226,;
    cTotal="", bFixedPos=.t., cQueryName = "POSIZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=70, Top=344, cSayPict=["99999"], cGetPict=["99999"]

  add object oDBPERSCA_2_23 as StdTrsField with uid="ROKTIBALJQ",rtseq=37,rtrep=.t.,;
    cFormVar="w_DBPERSCA",value=0,;
    ToolTipText = "Percentuale di scarto sul componente",;
    HelpContextID = 60875383,;
    cTotal="", bFixedPos=.t., cQueryName = "DBPERSCA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=214, Top=344, cSayPict=["9999.99"], cGetPict=["9999.99"]

  add object oDBRECSCA_2_24 as StdTrsField with uid="JJUJZLVJNR",rtseq=38,rtrep=.t.,;
    cFormVar="w_DBRECSCA",value=0,;
    ToolTipText = "Percentuale di recupero scarto sul componente",;
    HelpContextID = 45154935,;
    cTotal="", bFixedPos=.t., cQueryName = "DBRECSCA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=214, Top=371, cSayPict=["9999.99"], cGetPict=["9999.99"]

  func oDBRECSCA_2_24.mCond()
    with this.Parent.oContained
      return (.w_DBPERSCA<>0)
    endwith
  endfunc

  add object oDBPERSFR_2_25 as StdTrsField with uid="BBNYKMTHJO",rtseq=39,rtrep=.t.,;
    cFormVar="w_DBPERSFR",value=0,;
    ToolTipText = "Percentuale di sfrido sul componente",;
    HelpContextID = 60875400,;
    cTotal="", bFixedPos=.t., cQueryName = "DBPERSFR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=392, Top=344, cSayPict=["9999.99"], cGetPict=["9999.99"]

  add object oDBRECSFR_2_26 as StdTrsField with uid="HKSPVJAXYK",rtseq=40,rtrep=.t.,;
    cFormVar="w_DBRECSFR",value=0,;
    ToolTipText = "Percentuale di recupero sfrido sul componente",;
    HelpContextID = 45154952,;
    cTotal="", bFixedPos=.t., cQueryName = "DBRECSFR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=392, Top=371, cSayPict=["9999.99"], cGetPict=["9999.99"]

  func oDBRECSFR_2_26.mCond()
    with this.Parent.oContained
      return (.w_DBPERSFR<>0)
    endwith
  endfunc

  add object oDBPERRIC_2_27 as StdTrsField with uid="CKBZHQDUCC",rtseq=41,rtrep=.t.,;
    cFormVar="w_DBPERRIC",value=0,;
    ToolTipText = "Percentuale di ricarico costi sul componente",;
    HelpContextID = 44098169,;
    cTotal="", bFixedPos=.t., cQueryName = "DBPERRIC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=576, Top=344, cSayPict=["9999.99"], cGetPict=["9999.99"], tabstop=.f.

  add object oDB__NOTE_2_29 as StdTrsMemo with uid="SLHYGFLBAT",rtseq=43,rtrep=.t.,;
    cFormVar="w_DB__NOTE",value=space(0),;
    ToolTipText = "Eventuali note di riga",;
    HelpContextID = 259773051,;
    cTotal="", bFixedPos=.t., cQueryName = "DB__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=82, Top=398, tabstop=.f.

  add object oLinkPC_2_30 as StdButton with uid="KZHXUJHFDP",width=18,height=20,;
   left=765, top=146,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per accedere all'elenco degli articoli/componenti della variante";
    , HelpContextID = 80106282;
  , bGlobalFont=.t.

    proc oLinkPC_2_30.Click()
      this.Parent.oContained.GSDS_MCV.LinkPCClick()
    endproc

  func oLinkPC_2_30.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI='S' AND NOT EMPTY(.w_DBDESCOM))
    endwith
  endfunc

  func oLinkPC_2_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DBFLVARI<>'S')
    endwith
   endif
  endfunc

  add object oObj_2_32 as cp_calclbl with uid="FULGAYEIRQ",width=75,height=22,;
   left=4, top=425,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 97690342

  add object oDBINIVAL_2_33 as StdTrsField with uid="LWCZNKPDAR",rtseq=51,rtrep=.t.,;
    cFormVar="w_DBINIVAL",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio validit� della singola riga componente",;
    HelpContextID = 166104446,;
    cTotal="", bFixedPos=.t., cQueryName = "DBINIVAL",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=708, Top=371, tabstop=.f.

  func oDBINIVAL_2_33.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI='S' OR NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  add object oDBFINVAL_2_34 as StdTrsField with uid="AAWBANRBVB",rtseq=52,rtrep=.t.,;
    cFormVar="w_DBFINVAL",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine validit� della singola riga componente",;
    HelpContextID = 161201534,;
    cTotal="", bFixedPos=.t., cQueryName = "DBFINVAL",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine validit� minore o uguale alla data di inizio validit�",;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=708, Top=398, tabstop=.f.

  func oDBFINVAL_2_34.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI='S' OR NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  func oDBFINVAL_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DBFINVAL>=.w_DBINIVAL)
    endwith
    return bRes
  endfunc

  add object oDBRIFFAS_2_36 as StdTrsField with uid="DZXNYOHOBX",rtseq=56,rtrep=.t.,;
    cFormVar="w_DBRIFFAS",value=0,;
    ToolTipText = "Numero della fase di lavorazione associata al componente (0=ultima fase del ciclo)",;
    HelpContextID = 169540983,;
    cTotal="", bFixedPos=.t., cQueryName = "DBRIFFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=370, Top=425, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="TAB_CICL", cZoomOnZoom="GSDS_MCS", oKey_1_1="CSCODICE", oKey_1_2="this.w_CODRIS", oKey_2_1="CPROWORD", oKey_2_2="this.w_DBRIFFAS"

  func oDBRIFFAS_2_36.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI='S' OR NOT EMPTY(.w_DBCODCOM) AND NOT EMPTY(.w_DBCODRIS))
    endwith
  endfunc

  func oDBRIFFAS_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBRIFFAS_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oDBRIFFAS_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TAB_CICL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CSCODICE="+cp_ToStr(this.Parent.oContained.w_CODRIS)
    endif
    do cp_zoom with 'TAB_CICL','*','CSCODICE,CPROWORD',cp_AbsName(this.parent,'oDBRIFFAS_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_MCS',"Fasi di lavorazione",'GSDS_MDB.TAB_CICL_VZM',this.parent.oContained
  endproc
  proc oDBRIFFAS_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSDS_MCS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CSCODICE=w_CODRIS
     i_obj.w_CPROWORD=this.parent.oContained.w_DBRIFFAS
    i_obj.ecpSave()
  endproc

  add object oDESFAS_2_37 as StdTrsField with uid="KRCIKZZZHW",rtseq=57,rtrep=.t.,;
    cFormVar="w_DESFAS",value=space(40),enabled=.f.,;
    HelpContextID = 225307338,;
    cTotal="", bFixedPos=.t., cQueryName = "DESFAS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=421, Top=425, InputMask=replicate('X',40)

  add object oObj_2_38 as cp_runprogram with uid="TBPHJAGLWG",width=246,height=19,;
   left=221, top=498,;
    caption='GSDS_BVP(D)',;
   bGlobalFont=.t.,;
    prg="GSDS_BVP('D')",;
    cEvent = "w_DBCODCOM Changed",;
    nPag=2;
    , HelpContextID = 58788406

  add object oDBCORLEA_2_43 as StdTrsField with uid="OCHPTJSPJV",rtseq=68,rtrep=.t.,;
    cFormVar="w_DBCORLEA",value=0,;
    ToolTipText = "Correzione leadtime",;
    HelpContextID = 212472439,;
    cTotal="", bFixedPos=.t., cQueryName = "DBCORLEA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=722, Top=344

  add object oLinkPC_2_46 as StdButton with uid="PRVHRJAJKO",width=18,height=20,;
   left=765, top=146,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per accedere all'elenco degli articoli/componenti della variante";
    , HelpContextID = 80106282;
  , bGlobalFont=.t.

    proc oLinkPC_2_46.Click()
      this.Parent.oContained.GSDS_MLC.LinkPCClick()
      this.Parent.oContained.WriteTo_GSDS_MLC()
    endproc

  func oLinkPC_2_46.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI<>'S' AND NOT EMPTY(.w_DBCODCOM) AND .w_ARCMPCAR='S' AND g_CCAR='S')
    endwith
  endfunc

  func oLinkPC_2_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DBFLVARI='S' or empty(.w_DBCODCOM) or g_CCAR<>'S')
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsds_mdbPag2 as StdContainer
    Width  = 787
    height = 450
    stdWidth  = 787
    stdheight = 450
  resizeXpos=434
  resizeYpos=235
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="EGBTJSZSVV",left=24, top=17, width=743, height=420, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsds_mou",lower(this.oContained.GSDS_MOU.class))=0
        this.oContained.GSDS_MOU.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsds_mdbPag3 as StdContainer
    Width  = 787
    height = 450
    stdWidth  = 787
    stdheight = 450
  resizeXpos=493
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object TreeView as cp_Treeview with uid="GYODZTEHZL",left=2, top=7, width=782,height=373,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeBmp="treedefa.bmp",cLeafBmp="",nIndent=0,cCursor="Vista",cShowFields="ALLTRIM(FLSTAT)+' '+ALLTRIM(CODCOM)+' - '+ALLTRIM(DESCOM)+' - Qta: '+ALLTR(QTASTR)+' '+UNIMIS+ALLTRIM(COESTR)+' '+ALLTRIM(ARTSTR)",cNodeShowField="",cLeafShowField="",;
    cEvent = "Esegui",;
    nPag=5;
    , HelpContextID = 97690342


  add object oObj_5_2 as cp_runprogram with uid="VJBDDJQXEO",left=-4, top=466, width=174,height=22,;
    caption='GSDS_BTV(CLOSE)',;
   bGlobalFont=.t.,;
    prg="GSDS_BTV('Close')",;
    cEvent = "Done",;
    nPag=5;
    , HelpContextID = 33408708


  add object oObj_5_3 as cp_runprogram with uid="HZHTJBPPJM",left=173, top=466, width=206,height=23,;
    caption='GSDS_BTV(VISTA)',;
   bGlobalFont=.t.,;
    prg="GSDS_BTV('Vista')",;
    cEvent = "ActivatePage 3",;
    nPag=5;
    , HelpContextID = 99214276

  add object oCODDIS_5_4 as StdField with uid="ASHTOPMECH",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODDIS", cQueryName = "CODDIS",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta associata al componente",;
    HelpContextID = 217108698,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=627, Top=386, InputMask=replicate('X',20)

  func oCODDIS_5_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(Nvl(.w_CODDIS,Space(20))))
    endwith
    endif
  endfunc


  add object oBtn_5_5 as StdButton with uid="QMINRPEQRT",left=6, top=386, width=48,height=45,;
    CpPicture="BMP\verifica.BMP", caption="", nPag=5;
    , ToolTipText = "Premere per accedere ai dettagli della distinta associata al componente";
    , HelpContextID = 4340119;
    , caption='\<Distinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_5.Click()
      with this.Parent.oContained
        GSDS_BTV(this.Parent.oContained,"Distinta")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_5.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODICE))
    endwith
  endfunc


  add object oBtn_5_6 as StdButton with uid="HWLAFCJVGN",left=55, top=386, width=48,height=45,;
    CpPicture="bmp\kit.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per accedere ai dettagli del componente";
    , HelpContextID = 254324107;
    , caption='\<Articolo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_6.Click()
      with this.Parent.oContained
        GSDS_BTV(this.Parent.oContained,"Articolo")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODICE) AND .w_NUMLEV<>'0000')
    endwith
  endfunc


  add object oBtn_5_7 as StdButton with uid="GJINNPZABI",left=104, top=386, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per rieseguire la lettura della distinta";
    , HelpContextID = 201124330;
    , caption='\<Requery';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_7.Click()
      with this.Parent.oContained
        GSDS_BTV(this.Parent.oContained,"Requery")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODICE))
    endwith
  endfunc


  add object oBtn_5_8 as StdButton with uid="RQSKFAEHAZ",left=153, top=386, width=48,height=45,;
    CpPicture="bmp\esplodi.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per esplodere la treeview";
    , HelpContextID = 157674682;
    , caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_8.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_8.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODICE))
    endwith
  endfunc


  add object oBtn_5_9 as StdButton with uid="POVNMSFBDI",left=202, top=386, width=48,height=45,;
    CpPicture="bmp\implodi.bmp", caption="", nPag=5;
    , ToolTipText = "Premere per implodere la treeview";
    , HelpContextID = 157676154;
    , caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_5_9.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_9.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODICE))
    endwith
  endfunc

  add object oStr_5_10 as StdString with uid="UZSUPCMMKR",Visible=.t., Left=407, Top=386,;
    Alignment=1, Width=219, Height=18,;
    Caption="Distinta componente:"  ;
  , bGlobalFont=.t.

  func oStr_5_10.mHide()
    with this.Parent.oContained
      return (Empty(Nvl(.w_CODDIS,Space(20))))
    endwith
  endfunc
enddefine

  define class tgsds_mdbPag4 as StdContainer
    Width  = 787
    height = 450
    stdWidth  = 787
    stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODDISB_6_1 as StdField with uid="FIIQHEQXNN",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CODDISB", cQueryName = "CODDISB",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 51326758,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=94, Top=8, InputMask=replicate('X',20)

  add object oDESDIS_6_2 as StdField with uid="BIILUXJOMV",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 217049802,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=253, Top=8, InputMask=replicate('X',40)

  add object oDBNOTAGG_6_3 as StdMemo with uid="TCLBLEFAUA",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DBNOTAGG", cQueryName = "DBNOTAGG",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 30065277,;
   bGlobalFont=.t.,;
    Height=398, Width=758, Left=9, Top=37

  add object oStr_6_4 as StdString with uid="FTFALPEDIT",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=88, Height=18,;
    Caption="Distinta:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

* --- Defining Body row
define class tgsds_mdbBodyRow as CPBodyRowCnt
  Width=742
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="HLMPAMRDEF",rtseq=8,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di elaborazione del componente/variante associato alla distinta",;
    HelpContextID = 323478,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], BackStyle=0

  add object oDBFLVARI_2_2 as StdTrsCheck with uid="RPJMROKJEG",rtrep=.t.,;
    cFormVar="w_DBFLVARI",  caption="",;
    ToolTipText = "Se attivo: componente di tipo variante",;
    HelpContextID = 236502401,;
    Left=51, Top=0, Width=16,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., Autosize=.f., BackStyle=0;
   , bGlobalFont=.t.


  func oDBFLVARI_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DBFLVARI,&i_cF..t_DBFLVARI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDBFLVARI_2_2.GetRadio()
    this.Parent.oContained.w_DBFLVARI = this.RadioValue()
    return .t.
  endfunc

  func oDBFLVARI_2_2.ToRadio()
    this.Parent.oContained.w_DBFLVARI=trim(this.Parent.oContained.w_DBFLVARI)
    return(;
      iif(this.Parent.oContained.w_DBFLVARI=='S',1,;
      0))
  endfunc

  func oDBFLVARI_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDBCODCOM_2_3 as StdTrsField with uid="JXLDNFGVBT",rtseq=17,rtrep=.t.,;
    cFormVar="w_DBCODCOM",value=space(20),;
    ToolTipText = "Codice componente della distinta base",;
    HelpContextID = 221638013,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=70, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , BackStyle=0, cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODCOM"

  func oDBCODCOM_2_3.mCond()
    with this.Parent.oContained
      return (.w_DBFLVARI<>'S')
    endwith
  endfunc

  func oDBCODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDBCODCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDBCODCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DBCODCOM
    i_obj.ecpSave()
  endproc

  add object oDBDESCOM_2_16 as StdTrsField with uid="OLKSEPMULL",rtseq=30,rtrep=.t.,;
    cFormVar="w_DBDESCOM",value=space(40),;
    HelpContextID = 206560637,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=251, Left=221, Top=0, InputMask=replicate('X',40), BackStyle=0

  add object oDBUNIMIS_2_17 as StdTrsField with uid="TXVYGJCBWE",rtseq=31,rtrep=.t.,;
    cFormVar="w_DBUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 219820681,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=477, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , BackStyle=0, cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_DBUNIMIS"

  func oDBUNIMIS_2_17.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  func oDBUNIMIS_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBUNIMIS_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDBUNIMIS_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oDBUNIMIS_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misuta",'GSDS_MDB.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oDBUNIMIS_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_DBUNIMIS
    i_obj.ecpSave()
  endproc

  add object oDBQTADIS_2_19 as StdTrsField with uid="SSRZORVMMZ",rtseq=33,rtrep=.t.,;
    cFormVar="w_DBQTADIS",value=0,;
    ToolTipText = "Coefficiente di impiego",;
    HelpContextID = 60813961,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=519, Top=0, cSayPict=[v_PQ(32)], cGetPict=[v_GQ(32)], BackStyle=0

  func oDBQTADIS_2_19.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  func oDBQTADIS_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DBQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_DBQTADIS=INT(.w_DBQTADIS)))
    endwith
    return bRes
  endfunc

  add object oDBCOEIMP_2_20 as StdTrsField with uid="QUPOTMDJXZ",rtseq=34,rtrep=.t.,;
    cFormVar="w_DBCOEIMP",value=space(15),;
    ToolTipText = "Eventuale codice formula coefficiente di impiego associata al componente",;
    HelpContextID = 119926138,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=600, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , BackStyle=0, cLinkFile="COE_IMPI", cZoomOnZoom="GSDS_ACI", oKey_1_1="CICODICE", oKey_1_2="this.w_DBCOEIMP"

  func oDBCOEIMP_2_20.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  func oDBCOEIMP_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCOEIMP_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDBCOEIMP_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COE_IMPI','*','CICODICE',cp_AbsName(this.parent,'oDBCOEIMP_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ACI',"Coefficienti di impiego",'',this.parent.oContained
  endproc
  proc oDBCOEIMP_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ACI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CICODICE=this.parent.oContained.w_DBCOEIMP
    i_obj.ecpSave()
  endproc

  add object oDBFLESPL_2_21 as StdTrsCheck with uid="NYBNMJPOYB",rtrep=.t.,;
    cFormVar="w_DBFLESPL",  caption="",;
    ToolTipText = "Se attivo esplode componenti della distinta associata all'articolo componente",;
    HelpContextID = 220773758,;
    Left=717, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., Autosize=.f.,BackStyle=0;
   , bGlobalFont=.t.


  func oDBFLESPL_2_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DBFLESPL,&i_cF..t_DBFLESPL),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oDBFLESPL_2_21.GetRadio()
    this.Parent.oContained.w_DBFLESPL = this.RadioValue()
    return .t.
  endfunc

  func oDBFLESPL_2_21.ToRadio()
    this.Parent.oContained.w_DBFLESPL=trim(this.Parent.oContained.w_DBFLESPL)
    return(;
      iif(this.Parent.oContained.w_DBFLESPL=='S',1,;
      0))
  endfunc

  func oDBFLESPL_2_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDBFLESPL_2_21.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODCOM) AND NOT EMPTY(.w_DISCOM))
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="DBDISKIT = 'D'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".DBCODICE=DISTBASE.DBCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsds_mdb','DISMBASE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DBCODICE=DISMBASE.DBCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
