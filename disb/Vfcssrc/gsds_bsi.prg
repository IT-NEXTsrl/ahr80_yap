* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bsi                                                        *
*              Stampa implosione distinta base                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-18                                                      *
* Last revis.: 2017-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bsi",oParentObject)
return(i_retval)

define class tgsds_bsi as StdBatch
  * --- Local variables
  w_RECSEL = 0
  w_CODICE = space(41)
  w_LIVELLO = space(250)
  w_CI = 0
  w_CJ = 0
  w_CVCODART = space(20)
  w_CODAZI = space(5)
  w_PDMODVAR = space(1)
  w_TVPADSUPREMO = .f.
  w_QUERY = space(100)
  w_KEYSAL = space(40)
  w_CODIAR = space(20)
  w_ARTICOLO = space(20)
  w_nRIGA = 0
  Trovato = .f.
  conta = 0
  point = 0
  chiaveD = space(30)
  * --- WorkFile variables
  ART_TEMP_idx=0
  PAR_DISB_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Implosione Distinta Base
    *     Da GSDS_SIM - GSDS_KCS
    * --- w_TIPOSTAM
    *      I - Indentata
    *      M - Monolivello
    *      P - Solo lievelli finali
    *      D - Soo livelli finali da determinazione costo standard (GSDS_KCS)
    * --- caller
    * --- locali
    * --- ----------------------------------------------------------------------------------------------
    * --- Variabili da Determinazione costo standard GSDS_KCS
    * --- ----------------------------------------------------------------------------------------------
    * --- ----------------------------------------------------------------------------------------------
    * --- Legge parametro relativo alla modalit� di definizione ed utilizzo delle varianti)
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDMODVAR"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDMODVAR;
        from (i_cTable) where;
            PDCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PDMODVAR = NVL(cp_ToDate(_read_.PDMODVAR),cp_NullValue(_read_.PDMODVAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TVPADSUPREMO = (this.w_PDMODVAR="S")
    local riga 
 store 0 to riga
    * --- ----------------------------------------------------------------------------------------------
    if this.oParentObject.w_TIPOSTAM<>"D"
      * --- Il w_KEYRIF lo valorizzo solo se sto facendo la stampa implosione altrienti lo eredito
      this.oParentObject.w_KEYRIF = SYS(2015)
      if empty(this.oParentObject.w_CODINI) and empty(this.oParentObject.w_CODFIN)
        * --- La preparazione della stampa di tutte le distinte, senza alcun filtro, potrebbe essere estremamente lenta, quindi chiedo conferma.
        if not cp_YesNo("E' stata richiesta l'analisi di tutte le distinte base. Continuo ?")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    do case
      case this.oParentObject.w_TIPOSTAM="M"
        l_codini=this.oParentObject.w_codini 
 l_codfin=this.oParentObject.w_codfin 
 l_datrif=this.oParentObject.w_DATSTA 
 l_FLPROV = this.oParentObject.w_FLPROV 
 l_grumer=this.oParentObject.w_GRUMER 
 l_grumerf=this.oParentObject.w_GRUMERF 
 l_codfama=this.oParentObject.w_CODFAMA 
 l_codfamaf=this.oParentObject.w_CODFAMAF 
 l_codcat=this.oParentObject.w_CODCAT 
 l_codcatf=this.oParentObject.w_CODCATF 
 l_codmag=this.oParentObject.w_CODMAG 
 l_codmagf=this.oParentObject.w_CODMAGF 
 l_coddist=this.oParentObject.w_CODDIST 
 l_coddistf=this.oParentObject.w_CODDISTF 
 
        * --- Stampa Monolivello
        vq_exec("..\disb\exe\query\gsds_qim", this, "__tmp__")
        CP_CHPRN("..\disb\exe\query\GSDS_QIM","",this.oParentObject)
      otherwise
        * --- Stampa Indentata ('I') e Stampa Livelli Finali ('P')
        this.w_CI = 0
        this.w_CVCODART = SPACE(20)
        * --- Metto in un cursore tutti gli articoli presenti in DISTBASE in considerazione dei filtri della maschera.
        this.w_QUERY = "..\DISB\EXE\QUERY\GSDS2SIM"
        if this.oParentObject.w_TIPOSTAM="D"
          if this.oParentObject.w_ZFILTERS And !Empty(this.oParentObject.w_OUNOMQUE)
            this.w_QUERY = "..\DISB\EXE\QUERY\GSDS4KCS"
          endif
        endif
        VQ_EXEC(this.w_QUERY, this, "selezione")
        this.w_RECSEL = reccount("selezione")
        if this.oParentObject.w_TIPOSTAM="D"
          if this.oParentObject.w_ZFILTERS And !Empty(this.oParentObject.w_OUNOMQUE)
            * --- Delete from ART_TEMP
            i_nConn=i_TableProp[this.ART_TEMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                     )
            else
              delete from (i_cTable) where;
                    CAKEYRIF = this.oParentObject.w_KEYRIF;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        endif
        * --- Il numero massimo di distinte controllabili � 9.999.999
        if this.w_RECSEL>0 and this.w_RECSEL<10000000
          select selezione 
 go top 
 scan for PROCESSA="N"
          this.w_LIVELLO = selezione.lvlkey
          * --- Calcolo il livello della distinta
          if empty(this.w_LIVELLO)
            this.w_CI = this.w_CI + 1
            this.w_LIVELLO = right("000000"+alltrim(str(this.w_CI)),7)
            replace lvlkey with this.w_LIVELLO
          endif
          this.w_CODICE = selezione.arcoddis
          qtan=selezione.qtacompn
          if used("Implosione")
            * --- Tengo in considerazione scarto e sfrido su qtacompl
            qtal=selezione.qtacompl*(1+selezione.DBPERSCA/100) * (1+selezione.DBPERSFR/100)
          else
            * --- a livello 0 (radice dell'implosione) non ho scarto n� sfrido da considerare
            qtal=selezione.qtacompl
          endif
          if this.w_TVPADSUPREMO
            this.w_CVCODART = NVL(CVCODART , SPACE(20))
          endif
          riga=recno()
          ah_msg ("Analisi componente %1 in corso",.t.,,,alltrim(this.w_codice))
          vq_exec("..\disb\exe\query\gsds4sim", this, "implosione")
          * --- Il numero massimo di livelli di implosione considerabili � 30: (250+1/8)=31, uno lo elimino per sicurezza
          if reccount("implosione")>0
            this.w_CJ = 0
            select implosione 
 go top 
 scan
            this.w_CJ = this.w_CJ + 1
            replace lvlkey with (alltrim(this.w_LIVELLO)+"."+right("000000"+alltrim(str(this.w_CJ)),7))
            scatter memvar 
 m.livellod=occurs(".",m.lvlkey) 
 m.qtacompn=m.dbcoeum1*qtan 
 m.qtacompl=m.dbcoeum1*qtal
            insert into selezione from memvar 
 endscan
            if m.livellod+1>30
              * --- Superato il limite gestito - potrebbe anche esserci un loop.
              ah_ErrorMsg("Ci potrebbe essere un loop sul codice distinta %1 %0Oppure tale distinta potrebbe superare il numero di sottolivelli massimo gestito (30)",48, "",alltrim(this.w_CODICE))
              i_retcode = 'stop'
              return
            endif
            select selezione 
 go riga
            * --- se implosione contiene almeno un record significa che questo non � un livello finale,scrivo 'N' su PSUPREMO e 
            *     segno la riga processata mettendo 'S' a PROCESSA
            replace PSUPREMO with "N", PROCESSA with "S"
          else
            select selezione 
 go riga
            * --- questo � un livello finale, lascio 'S' su PSUPREMO e segno la riga processata mettendo 'S' a PROCESSA
            replace PROCESSA with "S"
          endif
          endscan
          * --- Se stampo solo i livelli finali cancello i record dove LIVELLO>0 e PSUPREMO<>'S'
          DELETE FOR PSUPREMO="S" and LIVELLOD>0 AND ARCODDIS<>CVCODART and !Empty(CVCODART)
          if this.oParentObject.w_TIPOSTAM $ "D-P"
            delete for PSUPREMO<>"S" and LIVELLOD>0
          endif
          if this.oParentObject.w_TIPOSTAM<>"D"
            l_codini=this.oParentObject.w_codini 
 l_codfin=this.oParentObject.w_codfin 
 l_datrif=this.oParentObject.w_DATSTA 
 l_FLPROV = this.oParentObject.w_FLPROV 
 l_grumer=this.oParentObject.w_GRUMER 
 l_grumerf=this.oParentObject.w_GRUMERF 
 l_codfama=this.oParentObject.w_CODFAMA 
 l_codfamaf=this.oParentObject.w_CODFAMAF 
 l_codcat=this.oParentObject.w_CODCAT 
 l_codcatf=this.oParentObject.w_CODCATF 
 l_codmag=this.oParentObject.w_CODMAG 
 l_codmagf=this.oParentObject.w_CODMAGF 
 l_coddist=this.oParentObject.w_CODDIST 
 l_coddistf=this.oParentObject.w_CODDISTF 
 
            SELECT DISTINCT arcoddis as cacodart, dbcodice as arcodart from selezione INTO CURSOR ArtDist
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- --Eliminazione distinte che non verificano i filtri impostati
            vq_exec("..\disb\exe\query\GSDSIBTV", this, "_distF_")
            SELECT * FROM selezione LEFT OUTER JOIN _distF_ ON (selezione.dbcodice=_distF_.codice and selezione.arcoddis=_distf_.padre );
            INTO CURSOR _appo_ order by lvlkey
            * --- Delete from ART_TEMP
            i_nConn=i_TableProp[this.ART_TEMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                     )
            else
              delete from (i_cTable) where;
                    CAKEYRIF = this.oParentObject.w_KEYRIF;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Preparo il cursore di stampa
            if this.oParentObject.w_TIPOSTAM="P"
              select * from _appo_ into cursor selezione
              * --- Preparo il cursore di stampa-raggruppo i dati e li ordino per Codice articolo.
              select arcoddis, dbcodice,max(dbdescri) as dbdescri, max(dbcodcom) as cocodcom, max(dbunimis) as counimis, sum(dbqtadis) as cocoeimp, ; 
 sum(dbcoeum1) as cocoeum1, sum(qtacompn) as qtacompn, sum(qtacompl) as qtacompl, max(livellod) as livellod, max(Psupremo) as Psupremo,; 
 max(DBPERSFR) as cosfrido, max(DBPERSCA) as coscapro, max(dbinival) as coattges, max(dbfinval) as coadisges, left(lvlkey,7) as chiave, flvari ; 
 from selezione group by arcoddis, dbcodice,chiave order by chiave,Psupremo,dbcodice into cursor __tmp__
              * --- Stampa livelli finali
              CP_CHPRN("..\disb\exe\query\GSDS2SIM","",this.oParentObject)
            else
              select * from _appo_ into cursor __tmp__ order by lvlkey
              * --- Stampa indentata
              CP_CHPRN("..\disb\exe\query\GSDS1SIM","",this.oParentObject)
            endif
          else
            if this.oParentObject.w_ZFILTERS And !Empty(this.oParentObject.w_OUNOMQUE)
              * --- Delete from ART_TEMP
              i_nConn=i_TableProp[this.ART_TEMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                       )
              else
                delete from (i_cTable) where;
                      CAKEYRIF = this.oParentObject.w_KEYRIF;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Inserisce nella tabella ART_TEMP gli articoli interni (nodi) della distinta
              SELECT DISTINCT dbcodart as cacodart, dbcodice as arcodart from InCursor INTO cursor ArtDist nofilter
              this.w_CODIAR = "ArtDist.cacodart"
              * --- Esplodo i nodi
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              vq_exec("..\DISB\EXE\QUERY\GSDS7KCS", this, "selezione1")
              use in select("ArtDist")
              * --- Delete from ART_TEMP
              i_nConn=i_TableProp[this.ART_TEMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                       )
              else
                delete from (i_cTable) where;
                      CAKEYRIF = this.oParentObject.w_KEYRIF;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            else
              vq_exec("..\DISB\EXE\QUERY\GSDS1KCS", this, "selezione1")
            endif
            SELECT ARCODDIS AS DBCODART, DBCODICE FROM SELEZIONE UNION SELECT DBCODART, DBCODICE FROM SELEZIONE1 INTO CURSOR InCursor
          endif
        else
          if this.oParentObject.w_TIPOSTAM<>"D"
            if this.w_RECSEL>9999999
              ah_ErrorMsg("Il numero massimo di distinte verificabili nella selezione (9.999.999) � stato superato")
              i_retcode = 'stop'
              return
            else
              ah_ErrorMsg("Non esistono dati da stampare per la selezione effettuata")
              i_retcode = 'stop'
              return
            endif
          endif
        endif
    endcase
    if used("Selezione")
      USE IN "Selezione"
    endif
    if used("Implosione")
      USE IN "Implosione"
    endif
    if used("_distF_")
      USE IN ("_distF_")
    endif
    if used("_appo_")
      USE IN ("_appo_")
    endif
    if used("__tmp__")
      USE IN "__tmp__"
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare il cursore ArtDist per passare i codici
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    SCAN
    this.w_CODIAR = padr(ArtDist.Cacodart,20)
    this.w_ARTICOLO = padr(ArtDist.arcodart,20)
    this.w_nRIGA = RECNO()
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTICOLO),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_nRIGA),'ART_TEMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_ARTICOLO,'CPROWNUM',this.w_nRIGA,'CACODDIS',SPACE(20))
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CACODDIS &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIF;
           ,this.w_CODIAR;
           ,this.w_ARTICOLO;
           ,this.w_nRIGA;
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    if used("ArtDist")
      USE IN ("ArtDist")
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Eliminazione componenti che non verificano i flitri inseriti dall'utente
    this.conta = 0
    this.point = 0
    Select _appo_ 
 =wrcursor("_appo_") 
 go bottom
    do while not BOF()
      if empty(nvl(codice," "))
        this.point=recno()
        this.chiaveD = alltrim(lvlkey)+"*"
        do while not this.trovato and not EOF()
          if LIKE(this.chiaveD,alltrim(lvlkey)) and not empty(nvl(codice," "))
            this.Trovato = .t.
          endif
          skip
        enddo
        go this.point
        if not this.trovato
          delete
        endif
        skip-1
      else
        skip-1
      endif
      this.Trovato = .f.
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='PAR_DISB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
