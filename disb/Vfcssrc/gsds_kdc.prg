* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_kdc                                                        *
*              Duplicazione cicli semplificati                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_23]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-27                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_kdc",oParentObject))

* --- Class definition
define class tgsds_kdc as StdForm
  Top    = 6
  Left   = 4

  * --- Standard Properties
  Width  = 494
  Height = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=82404457
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  TABMCICL_IDX = 0
  cPrg = "gsds_kdc"
  cComment = "Duplicazione cicli semplificati"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODRIF = space(15)
  o_CODRIF = space(15)
  w_DESRIF = space(40)
  w_CSCODICE = space(15)
  w_CSDESCRI = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_kdcPag1","gsds_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODRIF_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TABMCICL'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODRIF=space(15)
      .w_DESRIF=space(40)
      .w_CSCODICE=space(15)
      .w_CSDESCRI=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODRIF))
          .link_1_3('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CSCODICE = SPACE(15)
        .w_CSDESCRI = .w_DESRIF
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODRIF<>.w_CODRIF
            .w_CSCODICE = SPACE(15)
        endif
        if .o_CODRIF<>.w_CODRIF
            .w_CSDESCRI = .w_DESRIF
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODRIF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_MCS',True,'TABMCICL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_CODRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_CODRIF))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRIF)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODRIF) and !this.bDontReportError
            deferred_cp_zoom('TABMCICL','*','CSCODICE',cp_AbsName(oSource.parent,'oCODRIF_1_3'),i_cWhere,'GSDS_MCS',"Elenco cicli semplificati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_CODRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_CODRIF)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIF = NVL(_Link_.CSCODICE,space(15))
      this.w_DESRIF = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIF = space(15)
      endif
      this.w_DESRIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODRIF_1_3.value==this.w_CODRIF)
      this.oPgFrm.Page1.oPag.oCODRIF_1_3.value=this.w_CODRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_4.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_4.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCSCODICE_1_10.value==this.w_CSCODICE)
      this.oPgFrm.Page1.oPag.oCSCODICE_1_10.value=this.w_CSCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCSDESCRI_1_11.value==this.w_CSDESCRI)
      this.oPgFrm.Page1.oPag.oCSDESCRI_1_11.value=this.w_CSDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODRIF = this.w_CODRIF
    return

enddefine

* --- Define pages as container
define class tgsds_kdcPag1 as StdContainer
  Width  = 490
  height = 182
  stdWidth  = 490
  stdheight = 182
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODRIF_1_3 as StdField with uid="JUUIXUGDOZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODRIF", cQueryName = "CODRIF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo semplificato di riferimento",;
    HelpContextID = 167956698,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=56, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TABMCICL", cZoomOnZoom="GSDS_MCS", oKey_1_1="CSCODICE", oKey_1_2="this.w_CODRIF"

  func oCODRIF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRIF_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRIF_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TABMCICL','*','CSCODICE',cp_AbsName(this.parent,'oCODRIF_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_MCS',"Elenco cicli semplificati",'',this.parent.oContained
  endproc
  proc oCODRIF_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSDS_MCS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_CODRIF
     i_obj.ecpSave()
  endproc

  add object oDESRIF_1_4 as StdField with uid="VDRWLZUGCM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione ciclo origine",;
    HelpContextID = 167897802,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=185, Top=40, InputMask=replicate('X',40)


  add object oBtn_1_8 as StdButton with uid="TYEVPKXNEQ",left=377, top=133, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 82375706;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSDS_BDS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="BSJPZGRESF",left=428, top=133, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 75087034;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCSCODICE_1_10 as StdField with uid="ITVBJSDXTH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CSCODICE", cQueryName = "CSCODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo semplificato da duplicare",;
    HelpContextID = 145367915,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=56, Top=107, InputMask=replicate('X',15)

  add object oCSDESCRI_1_11 as StdField with uid="KKFCUFPSUU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CSDESCRI", cQueryName = "CSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione ciclo da generare",;
    HelpContextID = 59781999,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=185, Top=107, InputMask=replicate('X',40)

  add object oStr_1_1 as StdString with uid="HWRRCFGZRR",Visible=.t., Left=8, Top=8,;
    Alignment=0, Width=469, Height=18,;
    Caption="Ciclo di origine"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="SQUWQEWLWC",Visible=.t., Left=8, Top=73,;
    Alignment=0, Width=465, Height=18,;
    Caption="Ciclo di destinazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JHRLKDGLYO",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QGUROTLLQC",Visible=.t., Left=6, Top=107,;
    Alignment=1, Width=47, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oBox_1_6 as StdBox with uid="KNNESRHNLY",left=5, top=27, width=474,height=2

  add object oBox_1_7 as StdBox with uid="FWBDOEHGWZ",left=5, top=92, width=474,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
