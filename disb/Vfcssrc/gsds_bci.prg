* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bci                                                        *
*              Calcola coefficienti di impiego                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_41]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-01                                                      *
* Last revis.: 2018-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_bLatoServer
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bci",oParentObject,m.w_bLatoServer)
return(i_retval)

define class tgsds_bci as StdBatch
  * --- Local variables
  w_bLatoServer = .f.
  w_PAR1 = space(5)
  w_PAR2 = space(5)
  w_PAR3 = space(5)
  w_PAR4 = space(5)
  w_PAR5 = space(5)
  w_PAR6 = space(5)
  w_PAR7 = space(5)
  w_PAR8 = space(5)
  w_QTPA1 = 0
  w_QTPA2 = 0
  w_QTPA3 = 0
  w_QTPA4 = 0
  w_QTPA5 = 0
  w_QTPA6 = 0
  w_QTPA7 = 0
  w_QTPA8 = 0
  w_CODART = space(20)
  w_CODCOM = space(20)
  w_COEIMP = space(15)
  w_UNIMIS = space(3)
  w_QTAMOL = 0
  w_FORMUL = space(100)
  w_CODVOC = space(15)
  w_CHKFOR = space(1)
  w_OKCONF = space(1)
  w_VALFOR = 0
  w_SALREC = 0
  w_FLGLOB = space(1)
  w_TmpSave = space(0)
  w_OCCUR = 0
  w_OCCUR1 = 0
  w_CNT = 0
  w_LVLKEY = space(240)
  * --- WorkFile variables
  COE_IMPI_idx=0
  PAR_VARI_idx=0
  TMPCOE_IMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Interpreta i coefficienti di Impiego provenienti da una Distinta
    if .F.
      * --- Create temporary table TMPCOE_IMP
      i_nIdx=cp_AddTableDef('TMPCOE_IMP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPCOE_IMP_proto';
            )
      this.TMPCOE_IMP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    if this.w_bLatoServer
      this.TMPCOE_IMP_idx = cp_OpenTable("TMPCOE_IMP")
    endif
    if USED("APPCOE")
      SELECT APPCOE
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODART,"")) AND NOT EMPTY(NVL(CODCOM,"")) AND NOT EMPTY(NVL(COEIMP,"")) AND NVL(QTAMOL,0)=0
      this.w_CODART = CODART
      this.w_CODCOM = CODCOM
      this.w_COEIMP = COEIMP
      this.w_UNIMIS = UNIMIS
      if this.w_bLatoServer
        this.w_LVLKEY = ALLTRIM(LVLKEY)
      endif
      this.w_QTAMOL = 1
      this.w_FORMUL = " "
      this.w_PAR1 = ""
      this.w_PAR2 = ""
      this.w_PAR3 = ""
      this.w_PAR4 = ""
      this.w_PAR5 = ""
      this.w_PAR6 = ""
      this.w_PAR7 = ""
      this.w_PAR8 = ""
      this.w_FLGLOB = " "
      * --- Read from COE_IMPI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COE_IMPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COE_IMPI_idx,2],.t.,this.COE_IMPI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CIFORMUL,CIFLGLOB"+;
          " from "+i_cTable+" COE_IMPI where ";
              +"CICODICE = "+cp_ToStrODBC(this.w_COEIMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CIFORMUL,CIFLGLOB;
          from (i_cTable) where;
              CICODICE = this.w_COEIMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FORMUL = NVL(cp_ToDate(_read_.CIFORMUL),cp_NullValue(_read_.CIFORMUL))
        this.w_FLGLOB = NVL(cp_ToDate(_read_.CIFLGLOB),cp_NullValue(_read_.CIFLGLOB))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_FORMUL)
        this.w_FORMUL = ALLTRIM(this.w_FORMUL)
        this.w_TmpSave = this.w_FORMUL
        this.w_CNT = 0
        * --- Ricerco il segno '<' il quale indica che ho inserito un Parametro Variabile
        this.w_OCCUR = RATC("<", this.w_TmpSave )
        * --- Eseguo una scanzione della formula per indentificare l'elenco dei 
        *     parametri variabile da mostrare nella maschera
        do while this.w_OCCUR <> 0
          * --- Determino la posizione di inizio e fine parametro..
          this.w_OCCUR = RATC("<", this.w_TmpSave )
          this.w_OCCUR1 = RATC(">", this.w_TmpSave )
          * --- Verifico la correttezza del Parametro Variabile
          *     Identifico il nome ...
          this.w_CODVOC = ALLTRIM(SUBSTR(this.w_TmpSave,this.w_OCCUR+1,this.w_OCCUR1-this.w_OCCUR-1))
          * --- Read from PAR_VARI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_VARI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_VARI_idx,2],.t.,this.PAR_VARI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PAR_VARI where ";
                  +"PVCODICE = "+cp_ToStrODBC(this.w_CODVOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  PVCODICE = this.w_CODVOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se Parametro Variabile non trovato lo ignoro...
          if i_rows = 0
            * --- Se parametro non esistente lo sostituisco nella formula con 0.
            *     w_FORMUL � utilizzata in GSDS_BC2 per applicare la formula
            this.w_FORMUL = STRTRAN(this.w_FORMUL, "<"+ALLTRIM(this.w_CODVOC)+">", "(0)")
          else
            * --- Parametro trovato lo aggiungo alla lista dei parametri
            *     da mostrare nella mascherina
            this.w_CNT = this.w_CNT+1
            if this.w_CNT>0 AND this.w_CNT<9
              KK = ALLTRIM(STR(this.w_CNT))
              this.w_PAR&KK = this.w_CODVOC
            endif
          endif
          * --- Rimuovo dalla stringa di appoggio la variabile + le parentesi angolati che la limitano
          this.w_TmpSave = STRTRAN(this.w_TmpSave, "<"+ALLTRIM(this.w_CODVOC)+">", "")
        enddo
        this.w_OKCONF = "N"
        if this.w_CNT<>0
          this.w_CHKFOR = " "
          this.w_VALFOR = 0
          this.w_QTPA1 = 0
          this.w_QTPA2 = 0
          this.w_QTPA3 = 0
          this.w_QTPA4 = 0
          this.w_QTPA5 = 0
          this.w_QTPA6 = 0
          this.w_QTPA7 = 0
          this.w_QTPA8 = 0
          do while this.w_OKCONF="N"
            * --- Cicla fino a quando non seleziono un valore congruente
            do GSDS_KCI with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT APPCOE
            * --- Se Controllo OK w_OKCONF='S'
            GSDS_BC2(this,"B")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT APPCOE
            if this.w_bLatoServer
              this.w_LVLKEY = ALLTRIM(LVLKEY)
            endif
            if this.w_OKCONF="S"
              if this.w_FLGLOB="S"
                * --- Se Globale, riporta la Quantita' su tutti gli eventuali codici con lo stesso Coefficiente della Distinta
                this.w_SALREC = RECNO()
                GO TOP 
 REPLACE QTAMOL WITH this.w_QTAMOL FOR CODART=this.w_CODART AND COEIMP=this.w_COEIMP 
 GOTO this.w_SALREC
                if this.w_bLatoServer
                  * --- Write into TMPCOE_IMP
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCOE_IMP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"QTAMOL ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOL),'TMPCOE_IMP','QTAMOL');
                        +i_ccchkf ;
                    +" where ";
                        +"LVLKEYS = "+cp_ToStrODBC(this.w_LVLKEY);
                        +" and COEIMP = "+cp_ToStrODBC(APPCOE.COEIMP);
                           )
                  else
                    update (i_cTable) set;
                        QTAMOL = this.w_QTAMOL;
                        &i_ccchkf. ;
                     where;
                        LVLKEYS = this.w_LVLKEY;
                        and COEIMP = APPCOE.COEIMP;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                REPLACE QTAMOL WITH this.w_QTAMOL
                if this.w_bLatoServer
                  * --- Write into TMPCOE_IMP
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCOE_IMP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"QTAMOL ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOL),'TMPCOE_IMP','QTAMOL');
                        +i_ccchkf ;
                    +" where ";
                        +"LVLKEYS = "+cp_ToStrODBC(this.w_LVLKEY);
                        +" and CODCOM = "+cp_ToStrODBC(APPCOE.CODCOM);
                        +" and COEIMP = "+cp_ToStrODBC(APPCOE.COEIMP);
                        +" and LIVELLO = "+cp_ToStrODBC(APPCOE.LIVELLO);
                           )
                  else
                    update (i_cTable) set;
                        QTAMOL = this.w_QTAMOL;
                        &i_ccchkf. ;
                     where;
                        LVLKEYS = this.w_LVLKEY;
                        and CODCOM = APPCOE.CODCOM;
                        and COEIMP = APPCOE.COEIMP;
                        and LIVELLO = APPCOE.LIVELLO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          enddo
        else
          * --- Se non ho trovato nessun parametro potrei  sempre avere
          *     una formula composta da valori fissi, lancio cmq il batch di calcolo
          *     della formula
          SELECT APPCOE
          if this.w_bLatoServer
            this.w_LVLKEY = ALLTRIM(LVLKEY)
          endif
          GSDS_BC2(this,"B")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Se tutto Ok aggiorno il moltiplicatore....
          if this.w_OKCONF="S"
            SELECT APPCOE 
 REPLACE QTAMOL WITH this.w_QTAMOL
            if this.w_bLatoServer
              * --- Write into TMPCOE_IMP
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPCOE_IMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPCOE_IMP_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCOE_IMP_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"QTAMOL ="+cp_NullLink(cp_ToStrODBC(this.w_QTAMOL),'TMPCOE_IMP','QTAMOL');
                    +i_ccchkf ;
                +" where ";
                    +"LVLKEYS = "+cp_ToStrODBC(this.w_LVLKEY);
                    +" and CODART = "+cp_ToStrODBC(APPCOE.CODART);
                    +" and CODCOM = "+cp_ToStrODBC(APPCOE.CODCOM);
                    +" and COEIMP = "+cp_ToStrODBC(APPCOE.COEIMP);
                    +" and LIVELLO = "+cp_ToStrODBC(APPCOE.LIVELLO);
                       )
              else
                update (i_cTable) set;
                    QTAMOL = this.w_QTAMOL;
                    &i_ccchkf. ;
                 where;
                    LVLKEYS = this.w_LVLKEY;
                    and CODART = APPCOE.CODART;
                    and CODCOM = APPCOE.CODCOM;
                    and COEIMP = APPCOE.COEIMP;
                    and LIVELLO = APPCOE.LIVELLO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      endif
      SELECT APPCOE
      ENDSCAN 
    endif
  endproc


  proc Init(oParentObject,w_bLatoServer)
    this.w_bLatoServer=w_bLatoServer
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='COE_IMPI'
    this.cWorkTables[2]='PAR_VARI'
    this.cWorkTables[3]='*TMPCOE_IMP'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_bLatoServer"
endproc
