* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_mcs                                                        *
*              Cicli semplificati                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-26                                                      *
* Last revis.: 2014-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_mcs"))

* --- Class definition
define class tgsds_mcs as StdTrsForm
  Top    = 6
  Left   = 8

  * --- Standard Properties
  Width  = 758
  Height = 325+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-21"
  HelpContextID=97084521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TABMCICL_IDX = 0
  TAB_CICL_IDX = 0
  TAB_RISO_IDX = 0
  UNIMIS_IDX = 0
  RIS_ORSE_IDX = 0
  cFile = "TABMCICL"
  cFileDetail = "TAB_CICL"
  cKeySelect = "CSCODICE"
  cKeyWhere  = "CSCODICE=this.w_CSCODICE"
  cKeyDetail  = "CSCODICE=this.w_CSCODICE"
  cKeyWhereODBC = '"CSCODICE="+cp_ToStrODBC(this.w_CSCODICE)';

  cKeyDetailWhereODBC = '"CSCODICE="+cp_ToStrODBC(this.w_CSCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"TAB_CICL.CSCODICE="+cp_ToStrODBC(this.w_CSCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAB_CICL.CPROWORD '
  cPrg = "gsds_mcs"
  cComment = "Cicli semplificati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CSCODICE = space(15)
  w_CSDESCRI = space(40)
  w_CPROWORD = 0
  w_CSDESFAS = space(40)
  w_CS_SETUP = space(1)
  w_CSCODRIS = space(15)
  o_CSCODRIS = space(15)
  w_DESRIS = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CODVAL = space(3)
  w_PREZZO = 0
  w_CAOVAL = 0
  w_PRENAZ = 0
  w_CSUNIMIS = space(3)
  o_CSUNIMIS = space(3)
  w_CSQTAMOV = 0
  w_PREUM = 0
  w_TOTDAR = 0
  w_TOTALE = 0
  w_CALCPICT = 0
  w_CODREP = space(15)
  w_DESREP = space(40)
  w_TIPREP = space(2)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TABMCICL','gsds_mcs')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_mcsPag1","gsds_mcs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Cicli semplificati")
      .Pages(1).HelpContextID = 251010820
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCSCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TAB_RISO'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='RIS_ORSE'
    this.cWorkTables[4]='TABMCICL'
    this.cWorkTables[5]='TAB_CICL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TABMCICL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TABMCICL_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CSCODICE = NVL(CSCODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TABMCICL where CSCODICE=KeySet.CSCODICE
    *
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2],this.bLoadRecFilter,this.TABMCICL_IDX,"gsds_mcs")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TABMCICL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TABMCICL.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"TAB_CICL.","TABMCICL.")
      i_cTable = i_cTable+' TABMCICL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TOTALE = 0
        .w_TIPREP = 'RE'
        .w_CSCODICE = NVL(CSCODICE,space(15))
        .w_CSDESCRI = NVL(CSDESCRI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate(AH_Msgformat('Costo totale (%1):',alltrim(g_PERVAL+ ' '+ g_VALSIM)),RGB(0,0,0))
        .w_CALCPICT = DEFPIU(g_PERPUL)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TABMCICL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from TAB_CICL where CSCODICE=KeySet.CSCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.TAB_CICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('TAB_CICL')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "TAB_CICL.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" TAB_CICL"
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
        select * from (i_cTable) TAB_CICL where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_DESRIS = space(40)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_CODVAL = space(3)
          .w_PREZZO = 0
          .w_CODREP = space(15)
          .w_DESREP = space(40)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CSDESFAS = NVL(CSDESFAS,space(40))
          .w_CS_SETUP = NVL(CS_SETUP,space(1))
          .w_CSCODRIS = NVL(CSCODRIS,space(15))
          if link_2_4_joined
            this.w_CSCODRIS = NVL(RICODICE204,NVL(this.w_CSCODRIS,space(15)))
            this.w_DESRIS = NVL(RIDESCRI204,space(40))
            this.w_UNMIS1 = NVL(RIUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(RIUNMIS2204,space(3))
            this.w_OPERAT = NVL(RIOPERAT204,space(1))
            this.w_MOLTIP = NVL(RIMOLTIP204,0)
            this.w_PREZZO = NVL(RIPREZZO204,0)
            this.w_CODVAL = NVL(RICODVAL204,space(3))
            this.w_CODREP = NVL(RICODREP204,space(15))
          else
          .link_2_4('Load')
          endif
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
          .w_CSUNIMIS = NVL(CSUNIMIS,space(3))
          * evitabile
          *.link_2_14('Load')
          .w_CSQTAMOV = NVL(CSQTAMOV,0)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_CSUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_CSQTAMOV,G_PERPUL)
          .link_2_18('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_TOTDAR
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate(AH_Msgformat('Costo totale (%1):',alltrim(g_PERVAL+ ' '+ g_VALSIM)),RGB(0,0,0))
        .w_CALCPICT = DEFPIU(g_PERPUL)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CSCODICE=space(15)
      .w_CSDESCRI=space(40)
      .w_CPROWORD=10
      .w_CSDESFAS=space(40)
      .w_CS_SETUP=space(1)
      .w_CSCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_CSUNIMIS=space(3)
      .w_CSQTAMOV=0
      .w_PREUM=0
      .w_TOTDAR=0
      .w_TOTALE=0
      .w_CALCPICT=0
      .w_CODREP=space(15)
      .w_DESREP=space(40)
      .w_TIPREP=space(2)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        if not(empty(.w_CSCODRIS))
         .link_2_4('Full')
        endif
        .DoRTCalc(7,13,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_CSUNIMIS = .w_UNMIS1
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CSUNIMIS))
         .link_2_14('Full')
        endif
        .DoRTCalc(17,17,.f.)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_CSUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_CSQTAMOV,G_PERPUL)
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate(AH_Msgformat('Costo totale (%1):',alltrim(g_PERVAL+ ' '+ g_VALSIM)),RGB(0,0,0))
        .DoRTCalc(20,20,.f.)
        .w_CALCPICT = DEFPIU(g_PERPUL)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODREP))
         .link_2_18('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(23,23,.f.)
        .w_TIPREP = 'RE'
      endif
    endwith
    cp_BlankRecExtFlds(this,'TABMCICL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCSCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCSDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCSCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCSCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TABMCICL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSCODICE,"CSCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CSDESCRI,"CSDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    i_lTable = "TABMCICL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TABMCICL_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsds_scs with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_CSDESFAS C(40);
      ,t_CS_SETUP N(3);
      ,t_CSCODRIS C(15);
      ,t_DESRIS C(40);
      ,t_CSUNIMIS C(3);
      ,t_CSQTAMOV N(10,3);
      ,t_TOTDAR N(18,5);
      ,t_CODREP C(15);
      ,t_DESREP C(40);
      ,CPROWNUM N(10);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_CODVAL C(3);
      ,t_PREZZO N(18,5);
      ,t_CAOVAL N(12,7);
      ,t_PRENAZ N(18,5);
      ,t_PREUM N(18,5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsds_mcsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSDESFAS_2_2.controlsource=this.cTrsName+'.t_CSDESFAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.controlsource=this.cTrsName+'.t_CS_SETUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODRIS_2_4.controlsource=this.cTrsName+'.t_CSCODRIS'
    this.oPgFRm.Page1.oPag.oDESRIS_2_5.controlsource=this.cTrsName+'.t_DESRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSUNIMIS_2_14.controlsource=this.cTrsName+'.t_CSUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCSQTAMOV_2_15.controlsource=this.cTrsName+'.t_CSQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTOTDAR_2_17.controlsource=this.cTrsName+'.t_TOTDAR'
    this.oPgFRm.Page1.oPag.oCODREP_2_18.controlsource=this.cTrsName+'.t_CODREP'
    this.oPgFRm.Page1.oPag.oDESREP_2_19.controlsource=this.cTrsName+'.t_DESREP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(306)
    this.AddVLine(339)
    this.AddVLine(468)
    this.AddVLine(506)
    this.AddVLine(586)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TABMCICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TABMCICL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TABMCICL')
        i_extval=cp_InsertValODBCExtFlds(this,'TABMCICL')
        local i_cFld
        i_cFld=" "+;
                  "(CSCODICE,CSDESCRI"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CSCODICE)+;
                    ","+cp_ToStrODBC(this.w_CSDESCRI)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TABMCICL')
        i_extval=cp_InsertValVFPExtFlds(this,'TABMCICL')
        cp_CheckDeletedKey(i_cTable,0,'CSCODICE',this.w_CSCODICE)
        INSERT INTO (i_cTable);
              (CSCODICE,CSDESCRI &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CSCODICE;
                  ,this.w_CSDESCRI;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_CICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])
      *
      * insert into TAB_CICL
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CSCODICE,CPROWORD,CSDESFAS,CS_SETUP,CSCODRIS"+;
                  ",CSUNIMIS,CSQTAMOV,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CSCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CSDESFAS)+","+cp_ToStrODBC(this.w_CS_SETUP)+","+cp_ToStrODBCNull(this.w_CSCODRIS)+;
             ","+cp_ToStrODBCNull(this.w_CSUNIMIS)+","+cp_ToStrODBC(this.w_CSQTAMOV)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CSCODICE',this.w_CSCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CSCODICE,this.w_CPROWORD,this.w_CSDESFAS,this.w_CS_SETUP,this.w_CSCODRIS"+;
                ",this.w_CSUNIMIS,this.w_CSQTAMOV,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TABMCICL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TABMCICL
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TABMCICL')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CSDESCRI="+cp_ToStrODBC(this.w_CSDESCRI)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TABMCICL')
          i_cWhere = cp_PKFox(i_cTable  ,'CSCODICE',this.w_CSCODICE  )
          UPDATE (i_cTable) SET;
              CSDESCRI=this.w_CSDESCRI;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CSCODRIS) AND t_CSQTAMOV<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.TAB_CICL_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from TAB_CICL
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAB_CICL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CSDESFAS="+cp_ToStrODBC(this.w_CSDESFAS)+;
                     ",CS_SETUP="+cp_ToStrODBC(this.w_CS_SETUP)+;
                     ",CSCODRIS="+cp_ToStrODBCNull(this.w_CSCODRIS)+;
                     ",CSUNIMIS="+cp_ToStrODBCNull(this.w_CSUNIMIS)+;
                     ",CSQTAMOV="+cp_ToStrODBC(this.w_CSQTAMOV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CSDESFAS=this.w_CSDESFAS;
                     ,CS_SETUP=this.w_CS_SETUP;
                     ,CSCODRIS=this.w_CSCODRIS;
                     ,CSUNIMIS=this.w_CSUNIMIS;
                     ,CSQTAMOV=this.w_CSQTAMOV;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CSCODRIS) AND t_CSQTAMOV<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.TAB_CICL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TAB_CICL_IDX,2])
        *
        * delete TAB_CICL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TABMCICL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
        *
        * delete TABMCICL
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CSCODRIS) AND t_CSQTAMOV<>0) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,13,.t.)
        if .o_CSCODRIS<>.w_CSCODRIS
          .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        endif
        if .o_CSCODRIS<>.w_CSCODRIS
          .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        endif
        if .o_CSCODRIS<>.w_CSCODRIS
          .w_CSUNIMIS = .w_UNMIS1
          .link_2_14('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_CSCODRIS<>.w_CSCODRIS.or. .o_CSUNIMIS<>.w_CSUNIMIS
          .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_CSUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        endif
          .w_TOTALE = .w_TOTALE-.w_totdar
          .w_TOTDAR = cp_ROUND(.w_PREUM*.w_CSQTAMOV,G_PERPUL)
          .w_TOTALE = .w_TOTALE+.w_totdar
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate(AH_Msgformat('Costo totale (%1):',alltrim(g_PERVAL+ ' '+ g_VALSIM)),RGB(0,0,0))
        .DoRTCalc(20,20,.t.)
          .w_CALCPICT = DEFPIU(g_PERPUL)
          .link_2_18('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_CODVAL with this.w_CODVAL
      replace t_PREZZO with this.w_PREZZO
      replace t_CAOVAL with this.w_CAOVAL
      replace t_PRENAZ with this.w_PRENAZ
      replace t_PREUM with this.w_PREUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate(AH_Msgformat('Costo totale (%1):',alltrim(g_PERVAL+ ' '+ g_VALSIM)),RGB(0,0,0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCSUNIMIS_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCSUNIMIS_2_14.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CSCODRIS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_RISO_IDX,3]
    i_lTable = "TAB_RISO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2], .t., this.TAB_RISO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CSCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_ARI',True,'TAB_RISO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_CSCODRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_CSCODRIS))
          select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CSCODRIS)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CSCODRIS) and !this.bDontReportError
            deferred_cp_zoom('TAB_RISO','*','RICODICE',cp_AbsName(oSource.parent,'oCSCODRIS_2_4'),i_cWhere,'GSDS_ARI',"Elenco risorse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CSCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_CSCODRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_CSCODRIS)
            select RICODICE,RIDESCRI,RIUNMIS1,RIUNMIS2,RIOPERAT,RIMOLTIP,RIPREZZO,RICODVAL,RICODREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CSCODRIS = NVL(_Link_.RICODICE,space(15))
      this.w_DESRIS = NVL(_Link_.RIDESCRI,space(40))
      this.w_UNMIS1 = NVL(_Link_.RIUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.RIUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.RIOPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.RIMOLTIP,0)
      this.w_PREZZO = NVL(_Link_.RIPREZZO,0)
      this.w_CODVAL = NVL(_Link_.RICODVAL,space(3))
      this.w_CODREP = NVL(_Link_.RICODREP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CSCODRIS = space(15)
      endif
      this.w_DESRIS = space(40)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_PREZZO = 0
      this.w_CODVAL = space(3)
      this.w_CODREP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_RISO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CSCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_RISO_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_RISO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.RICODICE as RICODICE204"+ ",link_2_4.RIDESCRI as RIDESCRI204"+ ",link_2_4.RIUNMIS1 as RIUNMIS1204"+ ",link_2_4.RIUNMIS2 as RIUNMIS2204"+ ",link_2_4.RIOPERAT as RIOPERAT204"+ ",link_2_4.RIMOLTIP as RIMOLTIP204"+ ",link_2_4.RIPREZZO as RIPREZZO204"+ ",link_2_4.RICODVAL as RICODVAL204"+ ",link_2_4.RICODREP as RICODREP204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on TAB_CICL.CSCODRIS=link_2_4.RICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and TAB_CICL.CSCODRIS=link_2_4.RICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CSUNIMIS
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CSUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CSUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CSUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CSUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CSUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCSUNIMIS_2_14'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CSUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CSUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CSUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CSUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CSUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_CSUNIMIS, .w_UNMIS1, .w_UNMIS2, '   ')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_CSUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CSUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODREP
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIS_ORSE_IDX,3]
    i_lTable = "RIS_ORSE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2], .t., this.RIS_ORSE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RL__TIPO,RLCODICE,RLDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RLCODICE="+cp_ToStrODBC(this.w_CODREP);
                   +" and RL__TIPO="+cp_ToStrODBC(this.w_TIPREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RL__TIPO',this.w_TIPREP;
                       ,'RLCODICE',this.w_CODREP)
            select RL__TIPO,RLCODICE,RLDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODREP = NVL(_Link_.RLCODICE,space(15))
      this.w_DESREP = NVL(_Link_.RLDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODREP = space(15)
      endif
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIS_ORSE_IDX,2])+'\'+cp_ToStr(_Link_.RL__TIPO,1)+'\'+cp_ToStr(_Link_.RLCODICE,1)
      cp_ShowWarn(i_cKey,this.RIS_ORSE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCSCODICE_1_1.value==this.w_CSCODICE)
      this.oPgFrm.Page1.oPag.oCSCODICE_1_1.value=this.w_CSCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value==this.w_CSDESCRI)
      this.oPgFrm.Page1.oPag.oCSDESCRI_1_2.value=this.w_CSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIS_2_5.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oDESRIS_2_5.value=this.w_DESRIS
      replace t_DESRIS with this.oPgFrm.Page1.oPag.oDESRIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODREP_2_18.value==this.w_CODREP)
      this.oPgFrm.Page1.oPag.oCODREP_2_18.value=this.w_CODREP
      replace t_CODREP with this.oPgFrm.Page1.oPag.oCODREP_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_2_19.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_2_19.value=this.w_DESREP
      replace t_DESREP with this.oPgFrm.Page1.oPag.oDESREP_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSDESFAS_2_2.value==this.w_CSDESFAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSDESFAS_2_2.value=this.w_CSDESFAS
      replace t_CSDESFAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSDESFAS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.RadioValue()==this.w_CS_SETUP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.SetRadio()
      replace t_CS_SETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODRIS_2_4.value==this.w_CSCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODRIS_2_4.value=this.w_CSCODRIS
      replace t_CSCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSCODRIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSUNIMIS_2_14.value==this.w_CSUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSUNIMIS_2_14.value=this.w_CSUNIMIS
      replace t_CSUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSUNIMIS_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSQTAMOV_2_15.value==this.w_CSQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSQTAMOV_2_15.value=this.w_CSQTAMOV
      replace t_CSQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSQTAMOV_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTDAR_2_17.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTDAR_2_17.value=this.w_TOTDAR
      replace t_TOTDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTDAR_2_17.value
    endif
    cp_SetControlsValueExtFlds(this,'TABMCICL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_CSUNIMIS) or not(CHKUNIMI(.w_CSUNIMIS, .w_UNMIS1, .w_UNMIS2, '   '))) and (NOT EMPTY(.w_CSCODRIS)) and (not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CSCODRIS) AND .w_CSQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCSUNIMIS_2_14
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      if not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_CSCODRIS) AND .w_CSQTAMOV<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CSCODRIS = this.w_CSCODRIS
    this.o_CSUNIMIS = this.w_CSUNIMIS
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND NOT EMPTY(t_CSCODRIS) AND t_CSQTAMOV<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_CSDESFAS=space(40)
      .w_CS_SETUP=space(1)
      .w_CSCODRIS=space(15)
      .w_DESRIS=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_CODVAL=space(3)
      .w_PREZZO=0
      .w_CAOVAL=0
      .w_PRENAZ=0
      .w_CSUNIMIS=space(3)
      .w_CSQTAMOV=0
      .w_PREUM=0
      .w_TOTDAR=0
      .w_CODREP=space(15)
      .w_DESREP=space(40)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_CSCODRIS))
        .link_2_4('Full')
      endif
      .DoRTCalc(7,13,.f.)
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 0)
        .w_PRENAZ = IIF(.w_CODVAL=g_PERVAL, .w_PREZZO, VAL2MON(.w_PREZZO,.w_CAOVAL,g_CAOVAL,i_DATSYS))
        .w_CSUNIMIS = .w_UNMIS1
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_CSUNIMIS))
        .link_2_14('Full')
      endif
      .DoRTCalc(17,17,.f.)
        .w_PREUM = CALMMLIS(.w_PRENAZ, .w_UNMIS1+.w_UNMIS2+'   '+.w_CSUNIMIS+.w_OPERAT+'  P'+ALLTRIM(STR(g_PERPUL)),.w_MOLTIP,0, 0)
        .w_TOTDAR = cp_ROUND(.w_PREUM*.w_CSQTAMOV,G_PERPUL)
      .DoRTCalc(20,22,.f.)
      if not(empty(.w_CODREP))
        .link_2_18('Full')
      endif
    endwith
    this.DoRTCalc(23,24,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CSDESFAS = t_CSDESFAS
    this.w_CS_SETUP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.RadioValue(.t.)
    this.w_CSCODRIS = t_CSCODRIS
    this.w_DESRIS = t_DESRIS
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_CODVAL = t_CODVAL
    this.w_PREZZO = t_PREZZO
    this.w_CAOVAL = t_CAOVAL
    this.w_PRENAZ = t_PRENAZ
    this.w_CSUNIMIS = t_CSUNIMIS
    this.w_CSQTAMOV = t_CSQTAMOV
    this.w_PREUM = t_PREUM
    this.w_TOTDAR = t_TOTDAR
    this.w_CODREP = t_CODREP
    this.w_DESREP = t_DESREP
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CSDESFAS with this.w_CSDESFAS
    replace t_CS_SETUP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCS_SETUP_2_3.ToRadio()
    replace t_CSCODRIS with this.w_CSCODRIS
    replace t_DESRIS with this.w_DESRIS
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_CODVAL with this.w_CODVAL
    replace t_PREZZO with this.w_PREZZO
    replace t_CAOVAL with this.w_CAOVAL
    replace t_PRENAZ with this.w_PRENAZ
    replace t_CSUNIMIS with this.w_CSUNIMIS
    replace t_CSQTAMOV with this.w_CSQTAMOV
    replace t_PREUM with this.w_PREUM
    replace t_TOTDAR with this.w_TOTDAR
    replace t_CODREP with this.w_CODREP
    replace t_DESREP with this.w_DESREP
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_totdar
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsds_mcsPag1 as StdContainer
  Width  = 754
  height = 325
  stdWidth  = 754
  stdheight = 325
  resizeXpos=276
  resizeYpos=198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCSCODICE_1_1 as StdField with uid="DZOCQOBBEK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CSCODICE", cQueryName = "CSCODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 130687851,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=60, Top=19, InputMask=replicate('X',15)

  add object oCSDESCRI_1_2 as StdField with uid="GIBONDGQGA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CSDESCRI", cQueryName = "CSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45101935,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=184, Top=19, InputMask=replicate('X',40)


  add object oObj_1_4 as cp_calclbl with uid="HUBIDDYKUA",left=361, top=301, width=225,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 80913126


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=56, width=740,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Fase",Field2="CSDESFAS",Label2="Descrizione operazione",Field3="CS_SETUP",Label3="Setup",Field4="CSCODRIS",Label4="Risorsa",Field5="CSUNIMIS",Label5="U.M.",Field6="CSQTAMOV",Label6="Quantit�",Field7="TOTDAR",Label7="AH_Msgformat('Importo totale (%1)',alltrim(g_PERVAL+ ' ' +g_VALSIM))",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219006330

  add object oStr_1_3 as StdString with uid="SHEWKIUXFD",Visible=.t., Left=3, Top=19,;
    Alignment=1, Width=56, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="XHBVODZMXB",Visible=.t., Left=1, Top=272,;
    Alignment=1, Width=93, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QUZFPBCTSL",Visible=.t., Left=1, Top=301,;
    Alignment=1, Width=93, Height=18,;
    Caption="Des.risorsa:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=76,;
    width=736+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=77,width=735+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TAB_RISO|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESRIS_2_5.Refresh()
      this.Parent.oCODREP_2_18.Refresh()
      this.Parent.oDESREP_2_19.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TAB_RISO'
        oDropInto=this.oBodyCol.oRow.oCSCODRIS_2_4
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oCSUNIMIS_2_14
    endcase
    return(oDropInto)
  EndFunc


  add object oDESRIS_2_5 as StdTrsField with uid="ILSXFZEPEV",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESRIS",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione risorsa",;
    HelpContextID = 232909514,;
    cTotal="", bFixedPos=.t., cQueryName = "DESRIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=99, Top=301, InputMask=replicate('X',40)

  add object oCODREP_2_18 as StdTrsField with uid="NMAGVORIYH",rtseq=22,rtrep=.t.,;
    cFormVar="w_CODREP",value=space(15),enabled=.f.,;
    ToolTipText = "Codice reparto associato alla risorsa",;
    HelpContextID = 19058906,;
    cTotal="", bFixedPos=.t., cQueryName = "CODREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=99, Top=272, InputMask=replicate('X',15), cLinkFile="RIS_ORSE", cZoomOnZoom="GSDB_ARL", oKey_1_1="RL__TIPO", oKey_1_2="this.w_TIPREP", oKey_2_1="RLCODICE", oKey_2_2="this.w_CODREP"

  func oCODREP_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESREP_2_19 as StdTrsField with uid="VOHRRCCFNX",rtseq=23,rtrep=.t.,;
    cFormVar="w_DESREP",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione reparto",;
    HelpContextID = 19000010,;
    cTotal="", bFixedPos=.t., cQueryName = "DESREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=221, Top=272, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="DOFEMETEME",rtseq=20,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Importo totale del ciclo semplificato",;
    HelpContextID = 197316554,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=588, Top=301, cSayPict=[v_PU(40+VVU)], cGetPict=[v_GU(40+VVU)]
enddefine

* --- Defining Body row
define class tgsds_mcsBodyRow as CPBodyRowCnt
  Width=726
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="YIKODDOLMS",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza temporale di elaborazione del ciclo semplificato",;
    HelpContextID = 251334762,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oCSDESFAS_2_2 as StdTrsField with uid="OBPICASBEI",rtseq=4,rtrep=.t.,;
    cFormVar="w_CSDESFAS",value=space(40),;
    HelpContextID = 173001863,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=251, Left=43, Top=0, InputMask=replicate('X',40)

  add object oCS_SETUP_2_3 as StdTrsCheck with uid="YKIEGKTGBO",rtrep=.t.,;
    cFormVar="w_CS_SETUP",  caption="",;
    ToolTipText = "Se attivo: la fase si intende a costo fisso",;
    HelpContextID = 48227190,;
    Left=303, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oCS_SETUP_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CS_SETUP,&i_cF..t_CS_SETUP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCS_SETUP_2_3.GetRadio()
    this.Parent.oContained.w_CS_SETUP = this.RadioValue()
    return .t.
  endfunc

  func oCS_SETUP_2_3.ToRadio()
    this.Parent.oContained.w_CS_SETUP=trim(this.Parent.oContained.w_CS_SETUP)
    return(;
      iif(this.Parent.oContained.w_CS_SETUP=='S',1,;
      0))
  endfunc

  func oCS_SETUP_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCSCODRIS_2_4 as StdTrsField with uid="FMJVHAAQYN",rtseq=6,rtrep=.t.,;
    cFormVar="w_CSCODRIS",value=space(15),;
    ToolTipText = "Codice della risorsa impegnata",;
    HelpContextID = 255188103,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=340, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TAB_RISO", cZoomOnZoom="GSDS_ARI", oKey_1_1="RICODICE", oKey_1_2="this.w_CSCODRIS"

  func oCSCODRIS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCSCODRIS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCSCODRIS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_RISO','*','RICODICE',cp_AbsName(this.parent,'oCSCODRIS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_ARI',"Elenco risorse",'',this.parent.oContained
  endproc
  proc oCSCODRIS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSDS_ARI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RICODICE=this.parent.oContained.w_CSCODRIS
    i_obj.ecpSave()
  endproc

  add object oCSUNIMIS_2_14 as StdTrsField with uid="NNLDJJETXR",rtseq=16,rtrep=.t.,;
    cFormVar="w_CSUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura associata alla risorsa",;
    HelpContextID = 203047801,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=462, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , Autosize=.f., cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_CSUNIMIS"

  func oCSUNIMIS_2_14.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_CSCODRIS))
    endwith
  endfunc

  func oCSUNIMIS_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCSUNIMIS_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCSUNIMIS_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCSUNIMIS_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSDS_MCS.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCSUNIMIS_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_CSUNIMIS
    i_obj.ecpSave()
  endproc

  add object oCSQTAMOV_2_15 as StdTrsField with uid="PTAACKZZQF",rtseq=17,rtrep=.t.,;
    cFormVar="w_CSQTAMOV",value=0,;
    HelpContextID = 73399428,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=500, Top=0, cSayPict=[v_PQ(10)], cGetPict=[v_GQ(10)]

  add object oTOTDAR_2_17 as StdTrsField with uid="JQHUIECZCX",rtseq=19,rtrep=.t.,;
    cFormVar="w_TOTDAR",value=0,enabled=.f.,;
    ToolTipText = "Importo totale di riga",;
    HelpContextID = 258985930,;
    cTotal = "this.Parent.oContained.w_totale", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=141, Left=580, Top=-1
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_mcs','TABMCICL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CSCODICE=TABMCICL.CSCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
