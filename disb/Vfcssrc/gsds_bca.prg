* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bca                                                        *
*              Elabora carico da produzione                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_419]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-29                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bca",oParentObject)
return(i_retval)

define class tgsds_bca as StdBatch
  * --- Local variables
  w_NUMLEV = 0
  w_PPCODCOM = space(20)
  w_PPARTCOM = space(20)
  w_PPUNIMIS = space(3)
  w_PPQTAMOV = 0
  w_PPQTAUM1 = 0
  w_CODCOM = space(20)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_QTASAL = 0
  w_COEIMP = space(15)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_FLESPL = space(1)
  w_ARTCOM = space(20)
  w_UNMIS1 = space(3)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_FLFRAZ = space(1)
  w_APPO = 0
  w_DIRIFCAR = space(10)
  w_DIRIFSCA = space(10)
  w_DATFIL = ctod("  /  /  ")
  w_MOVIMP = space(10)
  w_MOVORD = space(10)
  w_MESS = space(10)
  w_ORIGIN = space(1)
  w_ACCEDI = .f.
  QTC = 0
  w_ONLYVAL = .f.
  w_FLCOSINV = space(1)
  w_QUANTI = 0
  w_PRGPAD = 0
  w_ROWPAD = 0
  w_ULTLIV = 0
  w_CODRIS = space(15)
  w_RECSCA = 0
  w_RECSFR = 0
  w_PERRIC = 0
  w_ULTLIV = 0
  w_MMSERIAL = space(10)
  w_MMCODESE = space(4)
  w_MMVALNAZ = space(3)
  w_MMCODUTE = 0
  w_MMNUMREG = 0
  w_MMDATREG = ctod("  /  /  ")
  w_MMTCAMAG = space(5)
  w_MMTCOLIS = space(5)
  w_MMNUMDOC = 0
  w_MMALFDOC = space(10)
  w_MMDATDOC = ctod("  /  /  ")
  w_MMDESSUP = space(40)
  w_MMCODVAL = space(3)
  w_MMCAOVAL = 0
  w_MMRIFPRO = space(11)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_CPROWNUM = 0
  w_MMNUMRIF = 0
  w_CPROWORD = 0
  w_MMCODICE = space(20)
  w_MMCODART = space(20)
  w_MMKEYSAL = space(20)
  w_MMUNIMIS = space(3)
  w_MMQTAMOV = 0
  w_MMQTAUM1 = 0
  w_MMCODMAG = space(5)
  w_MMCAUMAG = space(5)
  w_MMCODLIS = space(5)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMCAUCOL = space(5)
  w_MMCODMAT = space(5)
  w_MMF2CASC = space(1)
  w_MMF2ORDI = space(1)
  w_MMF2IMPE = space(1)
  w_MMF2RISE = space(1)
  w_MMFLLOTT = space(1)
  w_MMF2LOTT = space(1)
  w_FLAVA1 = space(1)
  w_MMFLELGM = space(1)
  w_MMPREZZO = 0
  w_MMVALMAG = 0
  w_MMIMPNAZ = 0
  w_MMFLULCA = space(1)
  w_MMFLULPV = space(1)
  w_MMVALULT = 0
  w_FLLOTT = space(1)
  w_PRZFIN = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  DIS_CARP_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  PIAMPROD_idx=0
  PIA_PROD_idx=0
  SALDIART_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Elaborazione Carichi  Prodotti (da GSDS_ADC)
    * --- Variabili passate dalla maschera
    DIMENSION QTC[99]
    this.w_MAXLEVEL = IIF(this.oParentObject.w_DIMAXLEV>0,this.oParentObject.w_DIMAXLEV,99)
    this.w_VERIFICA = "S"
    this.w_FILSTAT = " "
    this.w_DATFIL = this.oParentObject.w_DIDATREG
    this.w_MMCODESE = g_CODESE
    this.w_MMVALNAZ = g_PERVAL
    this.w_MMDATREG = this.oParentObject.w_DIDATREG
    this.w_MMTCOLIS = IIF(this.oParentObject.w_DITIPVAL="L", this.oParentObject.w_DICODLIS, SPACE(5))
    this.w_MMNUMDOC = this.oParentObject.w_NUMREG
    this.w_MMALFDOC = this.oParentObject.w_ALFREG
    this.w_MMDATDOC = this.oParentObject.w_DATREG
    this.w_MMCODVAL = IIF(this.oParentObject.w_DITIPVAL="L", this.oParentObject.w_VALUTA, this.oParentObject.w_VALESE)
    this.w_MMCAOVAL = IIF(this.oParentObject.w_DITIPVAL="L", IIF(this.oParentObject.w_CAOVAL=0, this.oParentObject.w_DICAOLIS, this.oParentObject.w_CAOVAL), this.oParentObject.w_CAOESE)
    this.w_MMCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    this.w_MMDESSUP = this.oParentObject.w_DIDESSUP
    this.w_DIRIFCAR = SPACE(10)
    this.w_DIRIFSCA = SPACE(10)
    this.w_FLCOSINV = "N"
    if EMPTY(this.oParentObject.w_DIRIFPIA)
      ah_ErrorMsg("Nessun piano di produzione selezionato",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_DICAUCAR)
      ah_ErrorMsg("Nessuna causale magazzino selezionata per carico prodotti",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_DIMAGCAR)
      ah_ErrorMsg("Nessun magazzino selezionato per carico prodotti",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_RIFORD) AND NOT EMPTY(this.oParentObject.w_CAUSOR)
      ah_ErrorMsg("Non esiste un riferimento a ordine di prodotti finiti%0Impossibile eseguire l'evasione dell'ordinato",,"")
      i_retcode = 'stop'
      return
    endif
    if Not Empty(CHKCONS("M",this.w_MMDATREG,"B","S"))
      i_retcode = 'stop'
      return
    endif
    * --- Riferimento al Movimento di Impegno per Inserimento Scarico Componenti 
    this.w_MOVIMP = SPACE(10)
    this.w_ONLYVAL = .T.
    if this.oParentObject.w_DIFLTISC="S" AND NOT EMPTY(this.oParentObject.w_DICAUSCA) AND this.oParentObject.w_FLEVAD="S" AND NOT EMPTY(this.oParentObject.w_DATIMP) AND NOT EMPTY(this.oParentObject.w_RIFIMP)
      * --- Read from MVM_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MVM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MMSERIAL"+;
          " from "+i_cTable+" MVM_MAST where ";
              +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RIFIMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MMSERIAL;
          from (i_cTable) where;
              MMSERIAL = this.oParentObject.w_RIFIMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MOVIMP = NVL(cp_ToDate(_read_.MMSERIAL),cp_NullValue(_read_.MMSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_MOVIMP)
        ah_ErrorMsg("Non esiste nessun riferimento a movimenti di impegno componenti",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_MOVORD = SPACE(10)
    if this.oParentObject.w_DIFLTICA="S" AND NOT EMPTY(this.oParentObject.w_DICAUCAR) AND this.oParentObject.w_FLEVAD="S" AND NOT EMPTY(this.oParentObject.w_DATIMP) AND NOT EMPTY(this.oParentObject.w_RIFORD)
      * --- Read from MVM_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MVM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MMSERIAL"+;
          " from "+i_cTable+" MVM_MAST where ";
              +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RIFORD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MMSERIAL;
          from (i_cTable) where;
              MMSERIAL = this.oParentObject.w_RIFORD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MOVORD = NVL(cp_ToDate(_read_.MMSERIAL),cp_NullValue(_read_.MMSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_MOVORD)
        ah_ErrorMsg("Non esiste nessun riferimento a movimenti di ordine prodotti",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_ACCEDI = .F.
    * --- Crea Cursore di Appoggio Ordine Prodotti
    CREATE CURSOR TMP_CARI ;
    (ROWPAD N(4,0), PPCODCOM C(20), PPARTCOM C(20), PPUNIMIS C(3), PPQTAMOV N(12, 3), PPQTAUM1 N(12, 3))
    * --- Crea Cursore di Appoggio Ordine Prodotti
    CREATE CURSOR TMP_SCAR ;
    (ROWPAD N(4,0), CODCOM C(20), ARTCOM C(20), UNIMIS C(3), QTAMOV N(15, 6), QTAUM1 N(15, 6), COEIMP C(15), FLESPL C(1), ORIGIN C(1))
    * --- Legge U.M. Non Frazionabili e Dati Articoli x Valorizzazioni
    ah_Msg("Lettura dati articoli per valorizzazioni...",.T.)
    vq_exec("QUERY\GSAR_UMI.VQR", this,"UMNOFR")
    vq_exec("..\DISB\EXE\QUERY\GSDS_BIC.VQR", this,"APPOART")
    * --- Calcola i Costi Unitari e Globali, In base al Tipo di valorizzazione ...
    * --- Calcola i Costi Unitari e Globali, In base al Tipo di valorizzazione ...
    * --- Se valorizzo in base ai componenti sfrutto il risultato del gsds_bdc
    do case
      case this.oParentObject.w_DITIPVAL="L"
        * --- Calcola su Listino
        WAIT WIND "Lettura Dati Listini Associati..." NOWAIT
        vq_exec("..\DISB\EXE\QUERY\GSDSLQIC.VQR", this,"APPOVALO")
      case this.oParentObject.w_DITIPVAL="X"
        * --- Calcola su Ultimo Costo Standard Articolo
        vq_exec("..\DISB\EXE\QUERY\GSDSXQIC.VQR", this,"APPOVALO")
      case this.oParentObject.w_DITIPVAL $ "SMUP"
        * --- Eseguo successivamente il GDSD_BDC
      case this.oParentObject.w_DITIPVAL="A"
        this.w_ACCEDI = .T.
    endcase
    if EMPTY(this.w_MOVORD)
      * --- Carichi da Piano Produzione
      ah_Msg("Lettura dati piano di produzione...",.T.)
      this.w_PRGPAD = 0
      * --- Select from PIA_PROD
      i_nConn=i_TableProp[this.PIA_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PIA_PROD_idx,2],.t.,this.PIA_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PIA_PROD ";
            +" where PPSERIAL="+cp_ToStrODBC(this.oParentObject.w_DIRIFPIA)+"";
             ,"_Curs_PIA_PROD")
      else
        select * from (i_cTable);
         where PPSERIAL=this.oParentObject.w_DIRIFPIA;
          into cursor _Curs_PIA_PROD
      endif
      if used('_Curs_PIA_PROD')
        select _Curs_PIA_PROD
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_PIA_PROD.PPCODCOM,"")) AND NOT EMPTY(NVL(_Curs_PIA_PROD.PPARTCOM,"")) AND NVL(_Curs_PIA_PROD.PPQTAUM1,0)<>0
          this.w_ROWPAD = 0
          this.w_PPCODCOM = _Curs_PIA_PROD.PPCODCOM
          this.w_PPARTCOM = _Curs_PIA_PROD.PPARTCOM
          this.w_PPUNIMIS = _Curs_PIA_PROD.PPUNIMIS
          this.w_PPQTAMOV = _Curs_PIA_PROD.PPQTAMOV
          this.w_PPQTAUM1 = _Curs_PIA_PROD.PPQTAUM1
          * --- Aggiorna TMP Ordini Prodotti
          this.w_PRGPAD = this.w_PRGPAD + 1
          this.w_ROWPAD = IIF(this.oParentObject.w_DIFLVACO="S", this.w_PRGPAD, 0)
          if this.oParentObject.w_DIFLVACO="S" or this.oParentObject.w_DITIPVAL $ "SMUP"
            * --- Eseguo costificazione da Distinta Costificata
            this.w_QUANTI = this.w_PPQTAUM1
            this.w_MAXLEVEL = IIF( this.oParentObject.w_DIFLVACO="S" OR this.oParentObject.w_TIPCOS<>"S", 999, this.oParentObject.w_DIMAXLEV)
            do GSDS_BDC with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if Used("APPCOE")
               
 UPDATE APPCOE SET CPROWNUM=this.w_PRGPAD WHERE 1=1
              if Used("TOTCOE")
                * --- Devo valorizzare riferimento riga in TOTCOE contenuto 
                *     in w_CPROWNUM
                 
 INSERT INTO TOTCOE SELECT * FROM APPCOE 
 
              else
                 
 Select * from APPCOE into cursor TOTCOE 
 Wrcursor("TOTCOE")
              endif
              Select APPCOE 
 Use
            endif
            if Used("TEMPVALO")
               
 Wrcursor("TEMPVALO") 
 UPDATE TEMPVALO SET RIGA=this.w_PRGPAD WHERE 1=1
              if Used("TOTVALO")
                * --- Devo valorizzare riferimento riga in TOTCOE contenuto 
                *     in w_CPROWNUM
                 
 INSERT INTO TOTVALO SELECT * FROM TEMPVALO 
 
              else
                 
 Select * from TEMPVALO into cursor TOTVALO 
 Wrcursor("TOTVALO")
              endif
              Select TEMPVALO 
 Use
            endif
          endif
          INSERT INTO TMP_CARI (ROWPAD, PPCODCOM, PPARTCOM, PPUNIMIS, PPQTAMOV, PPQTAUM1) ;
          VALUES (this.w_ROWPAD, this.w_PPCODCOM, this.w_PPARTCOM, this.w_PPUNIMIS, this.w_PPQTAMOV, this.w_PPQTAUM1)
        endif
          select _Curs_PIA_PROD
          continue
        enddo
        use
      endif
    else
      * --- Carichi da movimento di Ordine Prodotti Finiti
      ah_Msg("Lettura dati movimento di ordine...",.T.)
      this.w_PRGPAD = 0
      * --- Select from MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MVM_DETT ";
            +" where MMSERIAL="+cp_ToStrODBC(this.w_MOVORD)+"";
            +" order by MMCODICE";
             ,"_Curs_MVM_DETT")
      else
        select * from (i_cTable);
         where MMSERIAL=this.w_MOVORD;
         order by MMCODICE;
          into cursor _Curs_MVM_DETT
      endif
      if used('_Curs_MVM_DETT')
        select _Curs_MVM_DETT
        locate for 1=1
        do while not(eof())
        this.w_PPCODCOM = NVL(_Curs_MVM_DETT.MMCODICE, SPACE(20))
        this.w_PPARTCOM = NVL(_Curs_MVM_DETT.MMCODART, SPACE(20))
        this.w_PPUNIMIS = NVL(_Curs_MVM_DETT.MMUNIMIS, 0)
        this.w_PPQTAMOV = NVL(_Curs_MVM_DETT.MMQTAMOV, 0)
        this.w_PPQTAUM1 = NVL(_Curs_MVM_DETT.MMQTAUM1, 0)
        this.w_COEIMP = SPACE(15)
        this.w_FLESPL = " "
        if NOT EMPTY(this.w_PPCODCOM) AND NOT EMPTY(this.w_PPARTCOM) AND this.w_PPQTAMOV<>0
          this.w_ROWPAD = 0
          this.w_PRGPAD = this.w_PRGPAD + 1
          this.w_ROWPAD = IIF(this.oParentObject.w_DIFLVACO="S", this.w_PRGPAD, 0)
          if this.oParentObject.w_DIFLVACO="S" or this.oParentObject.w_DITIPVAL $ "SMUP"
            * --- Eseguo costificazione da Distinta Costificata
            this.w_QUANTI = this.w_PPQTAUM1
            this.w_MAXLEVEL = IIF( this.oParentObject.w_DIFLVACO="S" OR this.oParentObject.w_TIPCOS<>"S", 999, this.oParentObject.w_DIMAXLEV)
            do GSDS_BDC with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if Used("APPCOE")
               
 UPDATE APPCOE SET CPROWNUM=this.w_PRGPAD WHERE 1=1
              if Used("TOTCOE")
                * --- Devo valorizzare riferimento riga in TOTCOE contenuto 
                *     in w_CPROWNUM
                 
 INSERT INTO TOTCOE SELECT * FROM APPCOE 
 
              else
                 
 Select * from APPCOE into cursor TOTCOE 
 Wrcursor("TOTCOE")
              endif
              Select APPCOE 
 Use
            endif
            if Used("TEMPVALO")
               
 Wrcursor("TEMPVALO") 
 UPDATE TEMPVALO SET RIGA=this.w_PRGPAD WHERE 1=1
              if Used("TOTVALO")
                * --- Devo valorizzare riferimento riga in TOTCOE contenuto 
                *     in w_CPROWNUM
                 
 INSERT INTO TOTVALO SELECT * FROM TEMPVALO 
 
              else
                 
 Select * from TEMPVALO into cursor TOTVALO 
 Wrcursor("TOTVALO")
              endif
              Select TEMPVALO 
 Use
            endif
          endif
          INSERT INTO TMP_CARI (ROWPAD, PPCODCOM, PPARTCOM, PPUNIMIS, PPQTAMOV, PPQTAUM1) ;
          VALUES (this.w_ROWPAD, this.w_PPCODCOM, this.w_PPARTCOM, this.w_PPUNIMIS, this.w_PPQTAMOV, this.w_PPQTAUM1)
        endif
          select _Curs_MVM_DETT
          continue
        enddo
        use
      endif
    endif
    if EMPTY(this.w_MOVIMP) OR this.oParentObject.w_DIFLVACO="S"
      * --- Scarichi da Esplosione distinta Base
      * --- Se valorizza i carichi in base ai componenti, deve calcolare le valorizzazioni generando TMP_SCAR
      * --- Se gli scarichi saranno generati dai movimenti TMP_SCAR poi verra' eliminato
      this.w_PRGPAD = 0
      if USED("TMP_CARI")
        SELECT TMP_CARI 
        GO TOP
        SCAN FOR NOT EMPTY(PPARTCOM)
        this.w_PPARTCOM = PPARTCOM
        this.w_PPQTAMOV = PPQTAMOV
        this.w_PPQTAUM1 = PPQTAUM1
        this.w_ROWPAD = ROWPAD
        * --- Parametri per Batch di Esplosione
        this.w_DBCODINI = this.w_PPARTCOM
        this.w_DBCODFIN = this.w_PPARTCOM
        this.w_PRGPAD = this.w_PRGPAD + 1
        if used("TES_PLOS")
          select TES_PLOS
          use
        endif
        * --- Esplode Distinta
        this.w_VERIFICA = IIF(this.w_ROWPAD<>0, "S C", "S")
        if Not empty(this.w_MOVIMP)
          this.w_MAXLEVEL = IIF(this.w_ROWPAD<>0 AND this.oParentObject.w_TIPCOS<>"S", 999, this.w_MAXLEVEL)
        else
          this.w_MAXLEVEL = this.oParentObject.w_DIMAXLEV
        endif
        if this.oParentObject.w_DIFLVACO="S" or this.oParentObject.w_DITIPVAL $ "SMUP"
          * --- Non devo ricalcolare i coefficenti ho gi� il cursore con i calcoli generale
          if Used("TOTCOE")
             
 Select * from TOTCOE where CPROWNUM=this.w_PRGPAD into Cursor APPCOE
          endif
          gsar_bde(this,"T")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          gsar_bde(this,"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if used("TES_PLOS")
          select TES_PLOS
          LOCATE FOR NVL(FLSTAT, " ")="*"
          if FOUND()
            ah_ErrorMsg("Nel piano elaborato sono presenti distinte provvisorie; impossibile proseguire",,"")
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          else
            * --- Esegue valorizzazione cursori costificazione solo se esiste movimento 
            *     di impegnato
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        SELECT TMP_CARI 
        ENDSCAN 
        if USED("TMP_SCAR") AND EMPTY(this.w_MOVIMP)
          * --- Se esiste tmp
          SELECT TMP_SCAR
          if RECCOUNT()>0
            * --- Considera solo le 'foglie'
            GO TOP
            DELETE FROM TMP_SCAR WHERE FLESPL="S"
            GO TOP
            * --- Raggruppa per Distinta+Componente
            SELECT ROWPAD, CODCOM, MAX(ARTCOM) AS ARTCOM, UNIMIS, ;
            SUM(QTAMOV) AS QTAMOV, SUM(QTAUM1) AS QTAUM1, MAX(ORIGIN) AS ORIGIN ;
            FROM TMP_SCAR GROUP BY 1, 2, 4 ORDER BY 1, 2 INTO CURSOR TMP_SCAR
            if used("TES_PLOS")
              select TES_PLOS
              use
            endif
          endif
        endif
      endif
    endif
    if NOT EMPTY(this.w_MOVIMP)
      * --- Legge il movimento di Impegno Componenti di Origine
      if this.oParentObject.w_DIFLVACO="S" AND USED("TMP_SCAR")
        * --- Chiude il cursore generato in precedenza per i calcoli delle valoizzazioni carichi
        SELECT TMP_SCAR
        USE
        CREATE CURSOR TMP_SCAR ;
        (ROWPAD N(4,0), CODCOM C(20), ARTCOM C(20), UNIMIS C(3), QTAMOV N(15, 6), QTAUM1 N(15, 6), COEIMP C(15), FLESPL C(1), ORIGIN C(1))
      endif
      * --- Select from MVM_DETT
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" MVM_DETT ";
            +" where MMSERIAL="+cp_ToStrODBC(this.w_MOVIMP)+"";
            +" order by MMCODICE";
             ,"_Curs_MVM_DETT")
      else
        select * from (i_cTable);
         where MMSERIAL=this.w_MOVIMP;
         order by MMCODICE;
          into cursor _Curs_MVM_DETT
      endif
      if used('_Curs_MVM_DETT')
        select _Curs_MVM_DETT
        locate for 1=1
        do while not(eof())
        this.w_CODCOM = NVL(_Curs_MVM_DETT.MMCODICE, SPACE(20))
        this.w_ARTCOM = NVL(_Curs_MVM_DETT.MMCODART, SPACE(20))
        this.w_UNIMIS = NVL(_Curs_MVM_DETT.MMUNIMIS, "   ")
        this.w_QTAMOV = NVL(_Curs_MVM_DETT.MMQTAMOV, 0)
        this.w_QTAUM1 = NVL(_Curs_MVM_DETT.MMQTAUM1, 0)
        this.w_COEIMP = SPACE(15)
        this.w_FLESPL = " "
        if NOT EMPTY(this.w_CODCOM) AND NOT EMPTY(this.w_ARTCOM) AND this.w_QTAMOV<>0
          INSERT INTO TMP_SCAR (ROWPAD, CODCOM, ARTCOM, UNIMIS, QTAMOV, QTAUM1, COEIMP, FLESPL, ORIGIN) ;
          VALUES (0, this.w_CODCOM, this.w_ARTCOM, this.w_UNIMIS, this.w_QTAMOV, this.w_QTAUM1, this.w_COEIMP, this.w_FLESPL, "D")
        endif
          select _Curs_MVM_DETT
          continue
        enddo
        use
      endif
    endif
    if used("TMP_CARI")
      * --- Aggiorna Carico Prodotti
      SELECT TMP_CARI
      if RECCOUNT()>0
        ah_Msg("Scrittura movimento di carico da produzione...",.T.)
        * --- Try
        local bErr_02B0CC50
        bErr_02B0CC50=bTrsErr
        this.Try_02B0CC50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if NOT EMPTY(this.w_MESS)
            ah_ErrorMsg("%1%0Operazione abbandonata",,"",this.w_MESS)
          else
            ah_ErrorMsg("Errore durante l'elaborazione%0Operazione abbandonata",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_02B0CC50
        * --- End
      else
        ah_ErrorMsg("Non esistono movimenti da generare",,"")
      endif
    endif
    * --- Rimuovo i cursori
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_02B0CC50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if USED("TMP_SCAR") AND NOT EMPTY(this.oParentObject.w_DICAUSCA) AND NOT EMPTY(this.oParentObject.w_DIMAGSCA)
      * --- Aggiorna Scarico Componenti (se Previsto)
      select TMP_SCAR
      if RECCOUNT()>0
        ah_Msg("Scrittura movimento di scarico componenti",.T.)
        * --- Inserisce il Master
        this.w_MMSERIAL = SPACE(10)
        this.w_MMNUMREG = 0
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
        this.w_MMTCAMAG = this.oParentObject.w_DICAUSCA
        this.w_MMCAUMAG = this.oParentObject.w_DICAUSCA
        this.w_MMFLCASC = " "
        this.w_MMFLORDI = " "
        this.w_MMFLIMPE = " "
        this.w_MMFLRISE = " "
        this.w_MMFLELGM = " "
        this.w_FLAVA1 = " "
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MMTCAMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
            from (i_cTable) where;
                CMCODICE = this.w_MMTCAMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MMCODMAG = this.oParentObject.w_DIMAGSCA
        this.w_FLUBIC = this.oParentObject.w_UBSCA
        * --- Eventuale Evasione Impegno Componenti
        this.w_MMF2CASC = " "
        this.w_MMF2ORDI = " "
        this.w_MMF2IMPE = " "
        this.w_MMF2RISE = " "
        this.w_MMCAUCOL = IIF(EMPTY(this.oParentObject.w_MAGSIM), SPACE(5), this.oParentObject.w_CAUSIM)
        this.w_MMCODMAT = IIF(EMPTY(this.w_MMCAUCOL), SPACE(5), this.oParentObject.w_MAGSIM)
        this.w_F2UBIC = IIF(EMPTY(this.w_MMCAUCOL), SPACE(5), this.oParentObject.w_UBSIM)
        if NOT EMPTY(this.w_MMCAUCOL)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUCOL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
              from (i_cTable) where;
                  CMCODICE = this.w_MMCAUCOL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MMF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_MMF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_MMF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_MMF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_MMRIFPRO = "S"+this.oParentObject.w_DISERIAL
        i_Conn=i_TableProp[this.MVM_MAST_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEMVM", "i_codazi,w_MMSERIAL")
        cp_NextTableProg(this, i_Conn, "PRMVM", "i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
        * --- Try
        local bErr_040BB658
        bErr_040BB658=bTrsErr
        this.Try_040BB658()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = Ah_Msgformat("Impossibile inserire impegno componenti (tabella master)")
        endif
        bTrsErr=bTrsErr or bErr_040BB658
        * --- End
        this.w_DIRIFSCA = this.w_MMSERIAL
        select TMP_SCAR
        GO TOP
        SCAN FOR NOT EMPTY(CODCOM) AND QTAMOV>0
        this.w_MMCODICE = CODCOM
        this.w_MMCODART = ARTCOM
        this.w_MMUNIMIS = UNIMIS
        this.w_MMQTAMOV = QTAMOV
        this.w_MMQTAUM1 = QTAUM1
        this.w_ROWPAD = ROWPAD
        this.w_ORIGIN = ORIGIN
        ah_Msg("Scrittura movimento di scarico componenti; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART) )
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        select TMP_SCAR
        ENDSCAN 
      endif
    endif
    * --- Inserisce il Master
    this.w_MMSERIAL = SPACE(10)
    this.w_MMNUMREG = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_MMTCAMAG = this.oParentObject.w_DICAUCAR
    this.w_MMCAUMAG = this.oParentObject.w_DICAUCAR
    this.w_MMFLCASC = " "
    this.w_MMFLORDI = " "
    this.w_MMFLIMPE = " "
    this.w_MMFLRISE = " "
    this.w_MMFLELGM = " "
    this.w_FLAVA1 = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_MMTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM;
        from (i_cTable) where;
            CMCODICE = this.w_MMTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MMCODMAG = this.oParentObject.w_DIMAGCAR
    this.w_FLUBIC = this.oParentObject.w_UBCAR
    * --- Eventuale Evasione Ordine
    this.w_MMF2CASC = " "
    this.w_MMF2ORDI = " "
    this.w_MMF2IMPE = " "
    this.w_MMF2RISE = " "
    this.w_MMCAUCOL = IIF(EMPTY(this.oParentObject.w_MAGSOR), SPACE(5), this.oParentObject.w_CAUSOR)
    this.w_MMCODMAT = IIF(EMPTY(this.w_MMCAUCOL), SPACE(5), this.oParentObject.w_MAGSOR)
    this.w_F2UBIC = IIF(EMPTY(this.w_MMCAUCOL), " ", this.oParentObject.w_UBSOR)
    if NOT EMPTY(this.w_MMCAUCOL)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
          from (i_cTable) where;
              CMCODICE = this.w_MMCAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MMF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MMF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MMF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MMF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MMRIFPRO = "C"+this.oParentObject.w_DISERIAL
    i_Conn=i_TableProp[this.MVM_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEMVM", "i_codazi,w_MMSERIAL")
    cp_NextTableProg(this, i_Conn, "PRMVM", "i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
    * --- Try
    local bErr_02B0B4B0
    bErr_02B0B4B0=bTrsErr
    this.Try_02B0B4B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = ah_Msgformat("Impossibile inserire impegno componenti (tabella master)")
    endif
    bTrsErr=bTrsErr or bErr_02B0B4B0
    * --- End
    this.w_DIRIFCAR = this.w_MMSERIAL
    select TMP_CARI
    GO TOP
    SCAN FOR NOT EMPTY(PPCODCOM) AND PPQTAMOV>0
    this.w_MMCODICE = PPCODCOM
    this.w_MMCODART = PPARTCOM
    this.w_MMUNIMIS = PPUNIMIS
    this.w_MMQTAMOV = PPQTAMOV
    this.w_MMQTAUM1 = PPQTAUM1
    this.w_ROWPAD = ROWPAD
    this.w_ORIGIN = "C"
    ah_Msg("Scrittura movimento di carico da produzione; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART) )
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    select TMP_CARI
    ENDSCAN 
    * --- Aggiorna Riferimenti ai Movimenti Magazzino sulla Distinta Carichi
    if NOT EMPTY(this.w_DIRIFCAR)
      * --- Try
      local bErr_02B0E450
      bErr_02B0E450=bTrsErr
      this.Try_02B0E450()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Errore aggiornamento riferimenti ai movimenti in distinta carichi")
      endif
      bTrsErr=bTrsErr or bErr_02B0E450
      * --- End
    endif
    * --- Aggiorna Riferimenti Avvenuto Carico sul Piano Produzione
    if NOT EMPTY(this.oParentObject.w_DIRIFPIA)
      * --- Try
      local bErr_02B087E0
      bErr_02B087E0=bTrsErr
      this.Try_02B087E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = Ah_Msgformat("Errore aggiornamento riferimenti sul piano produzione")
      endif
      bTrsErr=bTrsErr or bErr_02B087E0
      * --- End
    endif
    * --- commit
    cp_EndTrs(.t.)
    WAIT CLEAR
    ah_ErrorMsg("Generazione movimenti completata",,"")
    return
  proc Try_040BB658()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOLIS"+",MMNUMDOC"+",MMALFDOC"+",MMDATDOC"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",MMSCOCL1"+",MMSCOCL2"+",MMSCOPAG"+",MMFLGIOM"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",MMRIFPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMALFDOC),'MVM_MAST','MMALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATDOC),'MVM_MAST','MMDATDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'MVM_MAST','MMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMRIFPRO),'MVM_MAST','MMRIFPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOLIS',this.w_MMTCOLIS,'MMNUMDOC',this.w_MMNUMDOC,'MMALFDOC',this.w_MMALFDOC,'MMDATDOC',this.w_MMDATDOC,'MMFLCLFR',"N")
      insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOLIS,MMNUMDOC,MMALFDOC,MMDATDOC,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,MMSCOCL1,MMSCOCL2,MMSCOPAG,MMFLGIOM,UTCC,UTDC,UTCV,UTDV,MMRIFPRO &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_MMCODESE;
           ,this.w_MMVALNAZ;
           ,this.w_MMCODUTE;
           ,this.w_MMNUMREG;
           ,this.w_MMDATREG;
           ,this.w_MMTCAMAG;
           ,this.w_MMTCOLIS;
           ,this.w_MMNUMDOC;
           ,this.w_MMALFDOC;
           ,this.w_MMDATDOC;
           ,"N";
           ," ";
           ,SPACE(15);
           ,this.w_MMDESSUP;
           ,this.w_MMCODVAL;
           ,this.w_MMCAOVAL;
           ,0;
           ,0;
           ,0;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_MMRIFPRO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02B0B4B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_MAST
    i_nConn=i_TableProp[this.MVM_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",MMCODESE"+",MMVALNAZ"+",MMCODUTE"+",MMNUMREG"+",MMDATREG"+",MMTCAMAG"+",MMTCOLIS"+",MMNUMDOC"+",MMALFDOC"+",MMDATDOC"+",MMFLCLFR"+",MMTIPCON"+",MMCODCON"+",MMDESSUP"+",MMCODVAL"+",MMCAOVAL"+",MMSCOCL1"+",MMSCOCL2"+",MMSCOPAG"+",MMFLGIOM"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",MMRIFPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_MAST','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODESE),'MVM_MAST','MMCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALNAZ),'MVM_MAST','MMVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODUTE),'MVM_MAST','MMCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMREG),'MVM_MAST','MMNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'MVM_MAST','MMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCAMAG),'MVM_MAST','MMTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMTCOLIS),'MVM_MAST','MMTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMDOC),'MVM_MAST','MMNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMALFDOC),'MVM_MAST','MMALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATDOC),'MVM_MAST','MMDATDOC');
      +","+cp_NullLink(cp_ToStrODBC("N"),'MVM_MAST','MMFLCLFR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'MVM_MAST','MMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDESSUP),'MVM_MAST','MMDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODVAL),'MVM_MAST','MMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAOVAL),'MVM_MAST','MMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOCL2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','MMSCOPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_MAST','MMFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MVM_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MVM_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MVM_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMRIFPRO),'MVM_MAST','MMRIFPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'MMCODESE',this.w_MMCODESE,'MMVALNAZ',this.w_MMVALNAZ,'MMCODUTE',this.w_MMCODUTE,'MMNUMREG',this.w_MMNUMREG,'MMDATREG',this.w_MMDATREG,'MMTCAMAG',this.w_MMTCAMAG,'MMTCOLIS',this.w_MMTCOLIS,'MMNUMDOC',this.w_MMNUMDOC,'MMALFDOC',this.w_MMALFDOC,'MMDATDOC',this.w_MMDATDOC,'MMFLCLFR',"N")
      insert into (i_cTable) (MMSERIAL,MMCODESE,MMVALNAZ,MMCODUTE,MMNUMREG,MMDATREG,MMTCAMAG,MMTCOLIS,MMNUMDOC,MMALFDOC,MMDATDOC,MMFLCLFR,MMTIPCON,MMCODCON,MMDESSUP,MMCODVAL,MMCAOVAL,MMSCOCL1,MMSCOCL2,MMSCOPAG,MMFLGIOM,UTCC,UTDC,UTCV,UTDV,MMRIFPRO &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_MMCODESE;
           ,this.w_MMVALNAZ;
           ,this.w_MMCODUTE;
           ,this.w_MMNUMREG;
           ,this.w_MMDATREG;
           ,this.w_MMTCAMAG;
           ,this.w_MMTCOLIS;
           ,this.w_MMNUMDOC;
           ,this.w_MMALFDOC;
           ,this.w_MMDATDOC;
           ,"N";
           ," ";
           ,SPACE(15);
           ,this.w_MMDESSUP;
           ,this.w_MMCODVAL;
           ,this.w_MMCAOVAL;
           ,0;
           ,0;
           ,0;
           ," ";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_MMRIFPRO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02B0E450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DIS_CARP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_CARP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_CARP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_CARP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIRIFCAR ="+cp_NullLink(cp_ToStrODBC(this.w_DIRIFCAR),'DIS_CARP','DIRIFCAR');
      +",DIRIFSCA ="+cp_NullLink(cp_ToStrODBC(this.w_DIRIFSCA),'DIS_CARP','DIRIFSCA');
          +i_ccchkf ;
      +" where ";
          +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
             )
    else
      update (i_cTable) set;
          DIRIFCAR = this.w_DIRIFCAR;
          ,DIRIFSCA = this.w_DIRIFSCA;
          &i_ccchkf. ;
       where;
          DISERIAL = this.oParentObject.w_DISERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_02B087E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PIAMPROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PIAMPROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PIAMPROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PIAMPROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPDATCAR ="+cp_NullLink(cp_ToStrODBC(this.w_MMDATREG),'PIAMPROD','PPDATCAR');
          +i_ccchkf ;
      +" where ";
          +"PPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFPIA);
             )
    else
      update (i_cTable) set;
          PPDATCAR = this.w_MMDATREG;
          &i_ccchkf. ;
       where;
          PPSERIAL = this.oParentObject.w_DIRIFPIA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il temporaneo di Elaborazione
    if used("TES_PLOS")
      SELECT TES_PLOS
      GO TOP
      SCAN FOR NOT EMPTY(DISPAD)
      this.w_NUMLEV = NUMLEV
      this.w_UNIMIS = UNMIS0
      this.w_QTAMOV = QTAMOV
      this.w_QTAUM1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
      this.w_COEIMP = COEIMP
      this.w_FLESPL = FLESPL
      this.w_ARTCOM = ARTCOM
      * --- Eseguo Valorizzazione se:
      *     - Numero di Cicli Massimo oppure Se Non devo Esplodere distinta per Costificazione
      if this.w_MAXLEVEL <> 999 
        * --- Eseguo Valorizzazione Cursore Documenti di Scarico
        if VAL(this.w_NUMLEV)=0
          this.w_QTAMOV = this.w_PPQTAMOV
          this.w_QTAUM1 = this.w_PPQTAUM1
          FOR L_i = 1 TO 99
          QTC[L_i] = 0
          ENDFOR
        else
          * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
          QTC[VAL(this.w_NUMLEV)] = this.w_QTAUM1
          * --- Parte dalle Quantita' da Produrre (Distinta=liv. 0000)
          this.w_QTAMOV = this.w_PPQTAUM1
          this.w_QTAUM1 = this.w_PPQTAUM1
          FOR L_i = 1 TO VAL(this.w_NUMLEV)
          * --- Solo la riga del componente viene moltiplicato per l'effettivo valore, gli altri calcoli vanno riportati alla 1^UM
          this.w_QTAMOV = this.w_QTAMOV * IIF(L_i=VAL(this.w_NUMLEV), QTAMOV, QTC[L_i])
          this.w_QTAUM1 = this.w_QTAUM1 * QTC[L_i]
          ENDFOR
          this.w_CODCOM = CODCOM
          INSERT INTO TMP_SCAR (ROWPAD, CODCOM, ARTCOM, UNIMIS, QTAMOV, QTAUM1, COEIMP, FLESPL, ORIGIN) ;
          VALUES (this.w_ROWPAD, this.w_CODCOM, this.w_ARTCOM, this.w_UNIMIS, this.w_QTAMOV, this.w_QTAUM1, this.w_COEIMP, this.w_FLESPL, "E")
        endif
      endif
      SELECT TES_PLOS
      ENDSCAN
      select TES_PLOS
      use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i Movimenti di Magazzino
    * --- Master
    * --- Detail
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    this.w_MMNUMRIF = -10
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_MMCODLIS = this.w_MMTCOLIS
    this.w_MMKEYSAL = this.w_MMCODART
    this.w_MMFLULCA = " "
    this.w_MMFLULPV = " "
    * --- Verifica le UM Frazionabili
    if this.w_ORIGIN="E" AND this.w_MMQTAMOV>0
      * --- Solo se Scarico Componenti da Esplosione Distinta Base
      this.w_QTAMOV = this.w_MMQTAMOV
      this.w_QTAUM1 = this.w_MMQTAUM1
      if used("UMNOFR")
        SELECT UMNOFR
        GO TOP
        LOCATE FOR UMCODICE=this.w_MMUNIMIS
        if FOUND()
          * --- Se U.M. Non Frazionabile aggiorna la Qta all'Intero Superiore
          this.w_MMQTAMOV = cp_ROUND(this.w_MMQTAMOV+.499, 0)
        endif
      endif
      this.w_MMQTAMOV = cp_ROUND(this.w_MMQTAMOV, g_PERPQT)
      * --- Se Variata Qta
      if this.w_QTAMOV<>this.w_MMQTAMOV
        this.w_MMQTAUM1 = (this.w_MMQTAMOV * this.w_QTAUM1) / this.w_QTAMOV
      endif
    endif
    * --- Verifica Data Ultimo Costo/Prezzo
    if this.w_MMFLCASC $ "+-"
      SELECT APPOART
      GO TOP
      LOCATE FOR NVL(CODART, SPACE(20))=this.w_MMCODART AND NVL(CODMAG, SPACE(5))=this.w_MMCODMAG
      if FOUND()
        this.w_MMFLULCA = IIF(this.w_FLAVA1="A" AND this.w_MMFLCASC="+" and this.w_MMDATREG>=CP_TODATE(DATUCA), "=", " ")
        this.w_MMFLULPV = IIF(this.w_FLAVA1="V" AND this.w_MMFLCASC="-" and this.w_MMDATREG>=CP_TODATE(DATUPV), "=", " ")
      endif
    endif
    this.w_MMPREZZO = 0
    if Used("TOTVALO") and (this.oParentObject.w_DIFLVACO="S" or this.oParentObject.w_DITIPVAL $ "SMUP" ) 
      * --- Nei componenti non apllico filtro di riga perch� cmq la valorizzazione del 
      *     componente � uguale nelle eventuali righe replicate nel caso di pi�
      *     righe dello stesso prodotto finito
      Select * from TOTVALO where (RIGA=this.w_CPROWNUM OR this.w_ORIGIN<>"C") into Cursor TEMPVALO
    endif
    if this.w_ORIGIN="C" AND this.oParentObject.w_DIFLVACO="S" AND this.w_ROWPAD<>0
      * --- Calcola il Prezzo in Base al Totale Componenti
      this.w_PRZFIN = 0
      if used("tempValo")
        select tempValo
        locate for codart=this.w_MMCODART AND Nvl(UNIMIS,"   ")=this.w_MMUNIMIS
        if found()
          this.w_PRZFIN = NVL(COSUNI, 0)
        endif
      endif
      this.w_MMPREZZO = this.w_PRZFIN
      if this.w_MMQTAUM1<>this.w_MMQTAMOV AND this.w_MMPREZZO<>0 and Not (this.oParentObject.w_DITIPVAL $ "SMUP" )
        * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
        this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1)/this.w_MMQTAMOV, IIF(this.oParentObject.w_DITIPVAL="L", this.oParentObject.w_DECLU, this.oParentObject.w_DECIU))
      endif
    else
      * --- Calcola il Prezzo
      if USED("APPOVALO") 
        SELECT APPOVALO
        if RECCOUNT()>0
          GO TOP
          this.w_ACCEDI = .T.
        endif
      endif
      if USED("TEMPVALO") 
        SELECT TEMPVALO
        if RECCOUNT()>0
          GO TOP
          this.w_ACCEDI = .T.
        endif
      endif
      if this.w_ACCEDI
        if this.oParentObject.w_DIFLVACO<>"S" OR this.w_ORIGIN <>"C"
          do case
            case this.oParentObject.w_DITIPVAL="L"
              * --- Leggo il Listino Qta Scaglione e Data di Attivaziione piu' Prossimi
              SELECT APPOVALO
              LOCATE FOR NVL(LICODART,"  ")=this.w_MMCODART AND NVL(LIQUANTI,0)>=this.w_MMQTAUM1
              if FOUND()
                this.w_MMPREZZO = NVL(LIPREZZO, 0)
                if this.w_MMPREZZO<>0 AND this.oParentObject.w_TIPOLN="L" AND NVL(IVPERIVA,0)<>0
                  * --- Se Listino al Lordo , Nettifica
                  this.w_APPO = cp_ROUND(this.w_MMPREZZO - (this.w_MMPREZZO / (1 + (IVPERIVA / 100))), this.oParentObject.w_DECLU)
                  this.w_MMPREZZO = this.w_MMPREZZO - this.w_APPO
                endif
              endif
            case this.oParentObject.w_DITIPVAL="X"
              * --- Cerco il Costo dell'Articolo nei Dati Articoli (Prima sul Magazzino)
              SELECT APPOVALO
              LOCATE FOR NVL(PRCODART,"  ")=this.w_MMCODART AND NVL(PRCODMAG, SPACE(5))=this.w_MMCODMAG
              if FOUND()
                this.w_MMPREZZO = NVL(PRCOSSTA, 0)
              else
                * --- ...Se non esiste sul Magazzino nei Dati generici
                GO TOP
                LOCATE FOR NVL(PRCODART,"  ")=this.w_MMCODART AND NVL(PRCODMAG, SPACE(5))="#####"
                if FOUND()
                  this.w_MMPREZZO = NVL(PRCOSSTA, 0)
                endif
              endif
            case this.oParentObject.w_DITIPVAL $ "SMUP"
              * --- Cerco il Costo dell'Articolo nel Cursore calcolato sull'Inventario
              this.w_PRZFIN = 0
              if used("tempValo")
                select tempValo
                locate for ARTCOM=this.w_MMCODART AND Nvl(UNIMIS,"   ")=this.w_MMUNIMIS
                if found()
                  this.w_MMPREZZO = NVL(COSUNI, 0)
                endif
              endif
            case this.oParentObject.w_DITIPVAL="A"
              * --- Read from SALDIART
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SLVALUCA"+;
                  " from "+i_cTable+" SALDIART where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_MMCODART);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SLVALUCA;
                  from (i_cTable) where;
                      SLCODICE = this.w_MMCODART;
                      and SLCODMAG = this.w_MMCODMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MMPREZZO = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
          if this.w_MMQTAUM1<>this.w_MMQTAMOV AND this.w_MMPREZZO<>0 and Not (this.oParentObject.w_DITIPVAL $ "SMUP" )
            * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
            *     tranne nel caso di criteri inventariali dove viene gi� eseguita nel GSDS_BDC
            this.w_MMPREZZO = cp_ROUND((this.w_MMPREZZO * this.w_MMQTAUM1)/this.w_MMQTAMOV, IIF(this.oParentObject.w_DITIPVAL="L", this.oParentObject.w_DECLU, this.oParentObject.w_DECIU))
          endif
        else
          this.w_PRZFIN = 0
          if used("tempValo")
            select tempValo
            locate for ARTCOM=this.w_MMCODART AND Nvl(UNIMIS,"   ")=this.w_MMUNIMIS
            if found()
              this.w_MMPREZZO = NVL(COSUNI, 0)
            endif
          endif
        endif
      endif
    endif
    if this.w_MMPREZZO=0
      * --- Se il prezzo � 0 non devo aggiornare i valori sui saldi
      this.w_MMFLULCA = " "
      this.w_MMFLULPV = " "
    endif
    this.w_MMVALMAG = cp_ROUND((this.w_MMQTAMOV*this.w_MMPREZZO), IIF(this.oParentObject.w_DITIPVAL="L", this.oParentObject.w_DECLG, this.oParentObject.w_DECIG))
    this.w_MMIMPNAZ = IIF(this.w_MMVALNAZ=this.w_MMCODVAL, this.w_MMVALMAG, VAL2MON(this.w_MMVALMAG, this.w_MMCAOVAL, g_CAOVAL, this.w_MMDATREG))
    this.w_MMVALULT = IIF(this.w_MMQTAUM1=0, 0, cp_ROUND(this.w_MMIMPNAZ/this.w_MMQTAUM1, 2))
    * --- Flag di Gestione Lotti/Ubicazioni (Lotti/Ubicazioni da inserire in Differita)
    this.w_MMFLLOTT = " "
    this.w_MMF2LOTT = " "
    this.w_FLLOTT = " "
    if g_PERLOT="S" AND NOT EMPTY(this.w_MMCODART)
      * --- Se Articolo Gestito a Lotti legge Flag
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLLOTT"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLLOTT;
          from (i_cTable) where;
              ARCODART = this.w_MMCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if (g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_FLUBIC="S")
      this.w_MMFLLOTT = LEFT(ALLTRIM(this.w_MMFLCASC)+IIF(this.w_MMFLRISE="+", "-", IIF(this.w_MMFLRISE="-", "+", " ")), 1)
    endif
    if (g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_F2UBIC="S")
      this.w_MMF2LOTT = LEFT(ALLTRIM(this.w_MMF2CASC)+IIF(this.w_MMF2RISE="+", "-", IIF(this.w_MMF2RISE="-", "+", " ")), 1)
    endif
    * --- Aggiorna Detail Movimento di Magazzino
    * --- Try
    local bErr_042C07C8
    bErr_042C07C8=bTrsErr
    this.Try_042C07C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = ah_Msgformat("Impossibile inserire dettaglio movimenti di magazzino")
    endif
    bTrsErr=bTrsErr or bErr_042C07C8
    * --- End
    * --- Aggiorna Saldi
    if this.w_MMQTAUM1<>0 AND NOT EMPTY(this.w_MMKEYSAL) AND NOT EMPTY(this.w_MMCODMAG)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MMCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_MMCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_MMFLCASC+this.w_MMFLRISE+this.w_MMFLORDI+this.w_MMFLIMPE)
        * --- Try
        local bErr_042B76A8
        bErr_042B76A8=bTrsErr
        this.Try_042B76A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_042B76A8
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_MMFLULCA,'SLVALUCA','this.w_MMVALULT',this.w_MMVALULT,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_MMFLULCA,'SLDATUCA','this.w_MMDATREG',this.w_MMDATREG,'update',i_nConn)
          i_cOp7=cp_SetTrsOp(this.w_MMFLULCA,'SLCODVAA','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',i_nConn)
          i_cOp8=cp_SetTrsOp(this.w_MMFLULPV,'SLVALUPV','this.w_MMVALULT',this.w_MMVALULT,'update',i_nConn)
          i_cOp9=cp_SetTrsOp(this.w_MMFLULPV,'SLDATUPV','this.w_MMDATREG',this.w_MMDATREG,'update',i_nConn)
          i_cOp10=cp_SetTrsOp(this.w_MMFLULPV,'SLCODVAV','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +",SLVALUCA ="+cp_NullLink(i_cOp5,'SALDIART','SLVALUCA');
          +",SLDATUCA ="+cp_NullLink(i_cOp6,'SALDIART','SLDATUCA');
          +",SLCODVAA ="+cp_NullLink(i_cOp7,'SALDIART','SLCODVAA');
          +",SLVALUPV ="+cp_NullLink(i_cOp8,'SALDIART','SLVALUPV');
          +",SLDATUPV ="+cp_NullLink(i_cOp9,'SALDIART','SLDATUPV');
          +",SLCODVAV ="+cp_NullLink(i_cOp10,'SALDIART','SLCODVAV');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              ,SLVALUCA = &i_cOp5.;
              ,SLDATUCA = &i_cOp6.;
              ,SLCODVAA = &i_cOp7.;
              ,SLVALUPV = &i_cOp8.;
              ,SLDATUPV = &i_cOp9.;
              ,SLCODVAV = &i_cOp10.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MMKEYSAL;
              and SLCODMAG = this.w_MMCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_SALCOM="S"
          * --- Try
          local bErr_042E7560
          bErr_042E7560=bTrsErr
          this.Try_042E7560()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_042E7560
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MMKEYSAL;
                and SCCODMAG = this.w_MMCODMAG;
                and SCCODCAN = this.w_COMMDEFA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
      if NOT EMPTY(this.w_MMCODMAT) AND NOT EMPTY(this.w_MMF2CASC+this.w_MMF2RISE+this.w_MMF2ORDI+this.w_MMF2IMPE)
        * --- Try
        local bErr_042C34C8
        bErr_042C34C8=bTrsErr
        this.Try_042C34C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_042C34C8
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MMCODMAT);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MMKEYSAL;
              and SLCODMAG = this.w_MMCODMAT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_SALCOM="S"
          * --- Try
          local bErr_042C4C68
          bErr_042C4C68=bTrsErr
          this.Try_042C4C68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_042C4C68
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SCQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SCQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SCQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SCQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MMKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MMCODMAT);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MMKEYSAL;
                and SCCODMAG = this.w_MMCODMAT;
                and SCCODCAN = this.w_COMMDEFA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
    endif
  endproc
  proc Try_042C07C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MVM_DETT
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MMSERIAL"+",CPROWNUM"+",MMNUMRIF"+",CPROWORD"+",MMCAUMAG"+",MMCAUCOL"+",MMCODMAG"+",MMCODMAT"+",MMCODLIS"+",MMCODICE"+",MMCODART"+",MMUNIMIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMVALMAG"+",MMIMPNAZ"+",MMKEYSAL"+",MMFLELGM"+",MMFLCASC"+",MMFLORDI"+",MMFLIMPE"+",MMFLRISE"+",MMF2CASC"+",MMF2ORDI"+",MMF2IMPE"+",MMF2RISE"+",MMFLOMAG"+",MMCODCOM"+",MMCODATT"+",MMTIPATT"+",MMCODCOS"+",MMFLORCO"+",MMFLCOCO"+",MMIMPCOM"+",MMFLULCA"+",MMFLULPV"+",MMVALULT"+",MMFLLOTT"+",MMF2LOTT"+",MMCODLOT"+",MMCODUBI"+",MMCODUB2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMSERIAL),'MVM_DETT','MMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MVM_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMNUMRIF),'MVM_DETT','MMNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MVM_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUMAG),'MVM_DETT','MMCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUCOL),'MVM_DETT','MMCAUCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'MVM_DETT','MMCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'MVM_DETT','MMCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODLIS),'MVM_DETT','MMCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'MVM_DETT','MMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'MVM_DETT','MMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'MVM_DETT','MMUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'MVM_DETT','MMQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'MVM_DETT','MMQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMPREZZO),'MVM_DETT','MMPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALMAG),'MVM_DETT','MMVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMIMPNAZ),'MVM_DETT','MMIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'MVM_DETT','MMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLELGM),'MVM_DETT','MMFLELGM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLCASC),'MVM_DETT','MMFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLORDI),'MVM_DETT','MMFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLIMPE),'MVM_DETT','MMFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLRISE),'MVM_DETT','MMFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMF2CASC),'MVM_DETT','MMF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMF2ORDI),'MVM_DETT','MMF2ORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMF2IMPE),'MVM_DETT','MMF2IMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMF2RISE),'MVM_DETT','MMF2RISE');
      +","+cp_NullLink(cp_ToStrODBC("X"),'MVM_DETT','MMFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODATT');
      +","+cp_NullLink(cp_ToStrODBC("A"),'MVM_DETT','MMTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMCODCOS');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLORCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MVM_DETT','MMFLCOCO');
      +","+cp_NullLink(cp_ToStrODBC(0),'MVM_DETT','MMIMPCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLULCA),'MVM_DETT','MMFLULCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLULPV),'MVM_DETT','MMFLULPV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMVALULT),'MVM_DETT','MMVALULT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMFLLOTT),'MVM_DETT','MMFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMF2LOTT),'MVM_DETT','MMF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'MVM_DETT','MMCODUB2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL,'CPROWNUM',this.w_CPROWNUM,'MMNUMRIF',this.w_MMNUMRIF,'CPROWORD',this.w_CPROWORD,'MMCAUMAG',this.w_MMCAUMAG,'MMCAUCOL',this.w_MMCAUCOL,'MMCODMAG',this.w_MMCODMAG,'MMCODMAT',this.w_MMCODMAT,'MMCODLIS',this.w_MMCODLIS,'MMCODICE',this.w_MMCODICE,'MMCODART',this.w_MMCODART,'MMUNIMIS',this.w_MMUNIMIS)
      insert into (i_cTable) (MMSERIAL,CPROWNUM,MMNUMRIF,CPROWORD,MMCAUMAG,MMCAUCOL,MMCODMAG,MMCODMAT,MMCODLIS,MMCODICE,MMCODART,MMUNIMIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMVALMAG,MMIMPNAZ,MMKEYSAL,MMFLELGM,MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMFLOMAG,MMCODCOM,MMCODATT,MMTIPATT,MMCODCOS,MMFLORCO,MMFLCOCO,MMIMPCOM,MMFLULCA,MMFLULPV,MMVALULT,MMFLLOTT,MMF2LOTT,MMCODLOT,MMCODUBI,MMCODUB2 &i_ccchkf. );
         values (;
           this.w_MMSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MMNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MMCAUMAG;
           ,this.w_MMCAUCOL;
           ,this.w_MMCODMAG;
           ,this.w_MMCODMAT;
           ,this.w_MMCODLIS;
           ,this.w_MMCODICE;
           ,this.w_MMCODART;
           ,this.w_MMUNIMIS;
           ,this.w_MMQTAMOV;
           ,this.w_MMQTAUM1;
           ,this.w_MMPREZZO;
           ,0;
           ,0;
           ,0;
           ,0;
           ,this.w_MMVALMAG;
           ,this.w_MMIMPNAZ;
           ,this.w_MMKEYSAL;
           ,this.w_MMFLELGM;
           ,this.w_MMFLCASC;
           ,this.w_MMFLORDI;
           ,this.w_MMFLIMPE;
           ,this.w_MMFLRISE;
           ,this.w_MMF2CASC;
           ,this.w_MMF2ORDI;
           ,this.w_MMF2IMPE;
           ,this.w_MMF2RISE;
           ,"X";
           ," ";
           ," ";
           ,"A";
           ," ";
           ," ";
           ," ";
           ,0;
           ,this.w_MMFLULCA;
           ,this.w_MMFLULPV;
           ,this.w_MMVALULT;
           ,this.w_MMFLLOTT;
           ,this.w_MMF2LOTT;
           ,SPACE(20);
           ,SPACE(20);
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042B76A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042E7560()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MMKEYSAL,'SCCODMAG',this.w_MMCODMAG,'SCCODCAN',this.w_COMMDEFA,'SCCODART',this.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAG;
           ,this.w_COMMDEFA;
           ,this.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042C34C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAT)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_042C4C68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MMKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MMKEYSAL,'SCCODMAG',this.w_MMCODMAT,'SCCODCAN',this.w_COMMDEFA,'SCCODART',this.w_MMCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MMKEYSAL;
           ,this.w_MMCODMAT;
           ,this.w_COMMDEFA;
           ,this.w_MMCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiude i Cursori
    if used("TMP_SCAR")
      select TMP_SCAR
      use
    endif
    if used("TMP_CARI")
      select TMP_CARI
      use
    endif
    if used("APPOART")
      select APPOART
      use
    endif
    if used("UMNOFR")
      select UMNOFR
      use
    endif
    if used("TES_PLOS")
      select TES_PLOS
      use
    endif
    if USED("tempvalo")
      SELECT tempvalo
      use
    endif
    if USED("APPCOE")
      SELECT APPCOE
      use
    endif
    if Used("TOTCOE")
       
 Select TOTCOE 
 Use
    endif
    if Used("TOTVALO")
       
 Select TOTVALO 
 Use
    endif
    if used("APPOVALO")
      select APPOVALO
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='DIS_CARP'
    this.cWorkTables[4]='MVM_DETT'
    this.cWorkTables[5]='MVM_MAST'
    this.cWorkTables[6]='PIAMPROD'
    this.cWorkTables[7]='PIA_PROD'
    this.cWorkTables[8]='SALDIART'
    this.cWorkTables[9]='SALDICOM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_PIA_PROD')
      use in _Curs_PIA_PROD
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    if used('_Curs_MVM_DETT')
      use in _Curs_MVM_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
