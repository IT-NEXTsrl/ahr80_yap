* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bg1                                                        *
*              Elaborazione stampa documenti da piano                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_221]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2006-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bg1",oParentObject,m.pAzione)
return(i_retval)

define class tgsds_bg1 as StdBatch
  * --- Local variables
  pAzione = space(6)
  NC = space(10)
  w_oMultiReport = .NULL.
  w_GRPDEF = space(5)
  w_PRG = space(30)
  w_ODESCRI = space(50)
  w_OREP = space(254)
  w_OQRY = space(254)
  w_OBAT = space(254)
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSDS_SDP e da GSVE_BSD
    * --- parametro
    * --- Nome cursore collegato allo zoom
    * --- Test parametro
    do case
      case this.pAzione = "SELEZI"
        this.NC = this.oParentObject.w_ZoomSel.cCursor
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "STAMPA"
        this.w_PRG = LEFT("GSDS_SDP" + SPACE(30),30)
        * --- Read from OUT_PUTS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "OUNOMQUE,OUNOMREP,OUDESPRG,OUNOMBAT"+;
            " from "+i_cTable+" OUT_PUTS where ";
                +"OUNOMPRG = "+cp_ToStrODBC(this.w_PRG);
                +" and OUROWNUM = "+cp_ToStrODBC(this.oParentObject.w_NUMER);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            OUNOMQUE,OUNOMREP,OUDESPRG,OUNOMBAT;
            from (i_cTable) where;
                OUNOMPRG = this.w_PRG;
                and OUROWNUM = this.oParentObject.w_NUMER;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OQRY = NVL(cp_ToDate(_read_.OUNOMQUE),cp_NullValue(_read_.OUNOMQUE))
          this.w_OREP = NVL(cp_ToDate(_read_.OUNOMREP),cp_NullValue(_read_.OUNOMREP))
          this.w_ODESCRI = NVL(cp_ToDate(_read_.OUDESPRG),cp_NullValue(_read_.OUDESPRG))
          this.w_OBAT = NVL(cp_ToDate(_read_.OUNOMBAT),cp_NullValue(_read_.OUNOMBAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_oMultiReport=createobject("MultiReport")
        this.w_oMultiReport.AddReport(This.oParentObject, this.w_OREP, this.w_ODESCRI, this.w_OQRY, this.w_OBAT,.F.,.T.)     
        CP_CHPRN(this.w_oMultiReport, , this)
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
