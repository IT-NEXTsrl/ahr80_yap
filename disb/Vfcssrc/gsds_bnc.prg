* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bnc                                                        *
*              Controlli carico produzione                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_268]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-28                                                      *
* Last revis.: 2000-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bnc",oParentObject,m.pOpz)
return(i_retval)

define class tgsds_bnc as StdBatch
  * --- Local variables
  pOpz = space(5)
  w_MMSERIAL = space(10)
  w_MMKEYSAL = space(20)
  w_MMCODMAG = space(5)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMCODMAT = space(5)
  w_MMF2CASC = space(1)
  w_MMF2ORDI = space(1)
  w_MMF2IMPE = space(1)
  w_MMF2RISE = space(1)
  w_MMQTAUM1 = 0
  w_FLGIOMC = space(1)
  w_FLGIOMS = space(1)
  w_PROG = .NULL.
  w_MESS = space(10)
  w_OK = .f.
  w_OKIMP = .f.
  w_SERIAL = space(10)
  * --- WorkFile variables
  ART_ICOL_idx=0
  MVM_DETT_idx=0
  MVM_MAST_idx=0
  PIAMPROD_idx=0
  PIA_PROD_idx=0
  SALDIART_idx=0
  DIS_CARP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch Controlli in Elaborazione Carico da Produzione (da GSDS_ADC)
    * --- Variabili passate dalla maschera
    this.w_MESS = " "
    this.w_OK = .T.
    do case
      case this.pOpz="CHECK"
        if EMPTY(this.oParentObject.w_DIRIFPIA)
          ah_ErrorMsg("Nessun piano di produzione selezionato",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_FLEVAD="S" AND NOT EMPTY(this.oParentObject.w_DATCAR)
          ah_ErrorMsg("Piano di produzione gi� elaborato (%1)",,"", DTOC(this.oParentObject.w_DATCAR) )
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DICAUCAR)
          ah_ErrorMsg("Nessuna causale magazzino selezionata per carico prodotti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DIMAGCAR)
          ah_ErrorMsg("Nessun magazzino selezionato per carico prodotti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_MAGSOR) AND NOT EMPTY(this.oParentObject.w_CAUSOR)
          ah_ErrorMsg("Nessun magazzino selezionato per evasione ordine prodotti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_DIMAGSCA) AND NOT EMPTY(this.oParentObject.w_DICAUSCA)
          ah_ErrorMsg("Nessun magazzino selezionato per scarico componenti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_MAGSIM) AND NOT EMPTY(this.oParentObject.w_CAUSIM)
          ah_ErrorMsg("Nessun magazzino selezionato per evasione impegni componenti",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_RIFORD) AND NOT EMPTY(this.oParentObject.w_CAUSOR)
          this.w_MESS = "Non esiste un riferimento a ordine di prodotti finiti%0Impossibile eseguire l'evasione dell'ordinato"
          ah_ErrorMsg(this.w_MESS,,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_DIMAXLEV =0
          ah_ErrorMsg("Attenzione, numero massimo livelli di esplosione non specificato",,"")
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        do case
          case this.oParentObject.w_DITIPVAL="L"
            if EMPTY(this.oParentObject.w_DICODLIS)
              ah_ErrorMsg("Nessun listino selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
            if this.oParentObject.w_CAOVAL=0 AND this.oParentObject.w_DICAOLIS=0
              ah_ErrorMsg("Inserire il cambio per la valuta selezionata",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
          case this.oParentObject.w_DITIPVAL $ "SMU"
            if EMPTY(this.oParentObject.w_DICODESE)
              ah_ErrorMsg("Nessun esercizio/inventario selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
            if EMPTY(this.oParentObject.w_DINUMINV)
              ah_ErrorMsg("Nessun inventario selezionato",,"")
              this.oParentObject.w_RESCHK = -1
              i_retcode = 'stop'
              return
            endif
        endcase
        if EMPTY(this.oParentObject.w_DICAUSCA) AND NOT EMPTY(this.oParentObject.w_DICAUCAR)
          if NOT ah_YesNo("Confermi inserimento dei soli carichi prodotti?")
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_OKIMP = .T.
        do case
          case NOT EMPTY(this.oParentObject.w_DATIMP) AND ((EMPTY(this.oParentObject.w_CAUSOR) AND NOT EMPTY(this.oParentObject.w_RIFORD)) OR (NOT EMPTY(this.oParentObject.w_CAUSOR) AND EMPTY(this.oParentObject.w_RIFORD)))
            * --- Causale Collegata di Evasione Ordine dei Prodotti piena e non ho  l'ordine nell'impegno componenti
            *     Causale Collegata di Evasione Ordine dei Prodotti vuota e  ho  l'ordine nell'impegno componenti
            this.w_OKIMP = .F.
          case NOT EMPTY(this.oParentObject.w_DATIMP) AND ((EMPTY(this.oParentObject.w_CAUSIM) AND NOT EMPTY(this.oParentObject.w_RIFIMP)) OR (NOT EMPTY(this.oParentObject.w_CAUSIM) AND EMPTY(this.oParentObject.w_RIFIMP)))
            this.w_OKIMP = .F.
            * --- Causale Collegata di Evasione Impegno dei Componentii piena e non ho  l'impegno nell'impegno componenti
            *     Causale Collegata di Evasione Impegno dei Componenti vuota e  ho  l'impegno nell'impegno componenti
        endcase
        if Not this.w_OKIMP
          this.w_MESS = "ATTENZIONE%0Il piano di produzione ha associato un impegno dei componenti%0Le causali selezionate non prevedono l'evasione di tali movimenti%0Si intende comunque procedere?"
          if NOT ah_YesNo(this.w_MESS)
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pOpz="CARI" OR this.pOpz="SCAR"
        this.w_MMSERIAL = IIF(this.pOpz="CARI", this.oParentObject.w_DIRIFCAR, this.oParentObject.w_DIRIFSCA)
        if NOT EMPTY(this.w_MMSERIAL)
          gsar_bzm(this,this.w_MMSERIAL,-10)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOpz="ELIMINA" 
        if (Not EMPTY(this.oParentObject.w_DIRIFSCA) or Not EMPTY(this.oParentObject.w_DIRIFCAR))
          if ah_YesNo("Elimino anche i movimenti di magazzino di carico/scarico associati?")
            this.w_PROG = GSMA_MVM()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              i_retcode = 'stop'
              return
            endif
            if NOT EMPTY(this.oParentObject.w_DIRIFSCA)
              * --- Write into MVM_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MVM_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                    +i_ccchkf ;
                +" where ";
                    +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFSCA);
                       )
              else
                update (i_cTable) set;
                    MMRIFPRO = SPACE(11);
                    &i_ccchkf. ;
                 where;
                    MMSERIAL = this.oParentObject.w_DIRIFSCA;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura movimento di scarico'
                return
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_DIRIFSCA)
              this.w_SERIAL = this.oParentObject.w_DIRIFSCA
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_DIRIFCAR)
              * --- Write into MVM_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MVM_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                    +i_ccchkf ;
                +" where ";
                    +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFCAR);
                       )
              else
                update (i_cTable) set;
                    MMRIFPRO = SPACE(11);
                    &i_ccchkf. ;
                 where;
                    MMSERIAL = this.oParentObject.w_DIRIFCAR;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura movimento di carico'
                return
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_DIRIFCAR) 
              this.w_SERIAL = this.oParentObject.w_DIRIFCAR
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_PROG.Ecpquit()     
          else
            if NOT EMPTY(this.oParentObject.w_DIRIFCAR)
              * --- Write into MVM_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MVM_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                    +i_ccchkf ;
                +" where ";
                    +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFCAR);
                       )
              else
                update (i_cTable) set;
                    MMRIFPRO = SPACE(11);
                    &i_ccchkf. ;
                 where;
                    MMSERIAL = this.oParentObject.w_DIRIFCAR;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura movimento di carico'
                return
              endif
            endif
            if NOT EMPTY(this.oParentObject.w_DIRIFSCA)
              * --- Write into MVM_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MVM_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MMRIFPRO ="+cp_NullLink(cp_ToStrODBC(SPACE(11)),'MVM_MAST','MMRIFPRO');
                    +i_ccchkf ;
                +" where ";
                    +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFSCA);
                       )
              else
                update (i_cTable) set;
                    MMRIFPRO = SPACE(11);
                    &i_ccchkf. ;
                 where;
                    MMSERIAL = this.oParentObject.w_DIRIFSCA;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura movimento di scarico'
                return
              endif
            endif
          endif
          if this.oParentObject.w_FLEVAD="S"
            * --- Elimina il Riferimento sul Piano di Produzione
            * --- Write into PIAMPROD
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PIAMPROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PIAMPROD_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PIAMPROD_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PPDATCAR ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PIAMPROD','PPDATCAR');
                  +i_ccchkf ;
              +" where ";
                  +"PPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DIRIFPIA);
                     )
            else
              update (i_cTable) set;
                  PPDATCAR = cp_CharToDate("  -  -  ");
                  &i_ccchkf. ;
               where;
                  PPSERIAL = this.oParentObject.w_DIRIFPIA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in aggiornamento piano di produzione'
              return
            endif
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PROG.w_MMSERIAL = this.w_SERIAL
    * --- creo il curosre delle solo chiavi
    this.w_PROG.QueryKeySet("MMSERIAL='"+this.w_SERIAL+ "'","")     
    * --- mi metto in interrogazione
    this.w_PROG.LoadRecWarn()     
    * --- Questo assegnamento serve per aggiornare lo zoom al record inserted della primanota
    this.w_PROG.w_DELCARPRO = This.OparentObject
    * --- Verifico se posso eliminare la registrazione
    if this.w_PROG.HAsCpEvents("EcpDelete")
      this.w_PROG.ECPDELETE()     
    endif
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MVM_DETT'
    this.cWorkTables[3]='MVM_MAST'
    this.cWorkTables[4]='PIAMPROD'
    this.cWorkTables[5]='PIA_PROD'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='DIS_CARP'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
