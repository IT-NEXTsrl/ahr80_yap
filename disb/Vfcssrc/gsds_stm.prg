* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_stm                                                        *
*              Tracciabilit� matricole magazzino produzione                    *
*                                                                              *
*      Author: ZUCCHETTI SPA (CF)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_173]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-17                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsds_stm",oParentObject))

* --- Class definition
define class tgsds_stm as StdForm
  Top    = 2
  Left   = 7

  * --- Standard Properties
  Width  = 589
  Height = 256+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-22"
  HelpContextID=194419607
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  MATRICOL_IDX = 0
  LOTTIART_IDX = 0
  KEY_ARTI_IDX = 0
  DOC_MAST_IDX = 0
  ART_ICOL_IDX = 0
  TIP_DOCU_IDX = 0
  PIAMPROD_IDX = 0
  cPrg = "gsds_stm"
  cComment = "Tracciabilit� matricole magazzino produzione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_DESINI = space(40)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_DESFIN = space(40)
  w_MATINI = space(40)
  o_MATINI = space(40)
  w_MATFIN = space(40)
  o_MATFIN = space(40)
  w_LOTINI = space(20)
  o_LOTINI = space(20)
  w_LOTFIN = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_COCINI = space(20)
  o_COCINI = space(20)
  w_COCFIN = space(20)
  o_COCFIN = space(20)
  w_MACINI = space(40)
  o_MACINI = space(40)
  w_MACFIN = space(40)
  o_MACFIN = space(40)
  w_LOCINI = space(20)
  o_LOCINI = space(20)
  w_TIPSEL = space(1)
  o_TIPSEL = space(1)
  w_CAUINI = space(5)
  w_CAUFIN = space(5)
  w_NUMINI = 0
  o_NUMINI = 0
  w_NUMFIN = 0
  w_ALFINI = space(2)
  w_ALFFIN = space(2)
  w_DTDFIN = ctod('  /  /  ')
  w_DTDINI = ctod('  /  /  ')
  o_DTDINI = ctod('  /  /  ')
  w_DICINI = 0
  o_DICINI = 0
  w_DICFIN = 0
  w_DTPINI = ctod('  /  /  ')
  o_DTPINI = ctod('  /  /  ')
  w_DTPFIN = ctod('  /  /  ')
  w_LOCFIN = space(20)
  w_DECINI = space(40)
  w_DECFIN = space(40)
  w_RIFINI = space(10)
  w_RIFFIN = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsds_stmPag1","gsds_stm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni sui prodotti finiti")
      .Pages(2).addobject("oPag","tgsds_stmPag2","gsds_stm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni sui componenti")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MATRICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='PIAMPROD'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_DESINI=space(40)
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_MATINI=space(40)
      .w_MATFIN=space(40)
      .w_LOTINI=space(20)
      .w_LOTFIN=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_COCINI=space(20)
      .w_COCFIN=space(20)
      .w_MACINI=space(40)
      .w_MACFIN=space(40)
      .w_LOCINI=space(20)
      .w_TIPSEL=space(1)
      .w_CAUINI=space(5)
      .w_CAUFIN=space(5)
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_ALFINI=space(2)
      .w_ALFFIN=space(2)
      .w_DTDFIN=ctod("  /  /  ")
      .w_DTDINI=ctod("  /  /  ")
      .w_DICINI=0
      .w_DICFIN=0
      .w_DTPINI=ctod("  /  /  ")
      .w_DTPFIN=ctod("  /  /  ")
      .w_LOCFIN=space(20)
      .w_DECINI=space(40)
      .w_DECFIN=space(40)
      .w_RIFINI=space(10)
      .w_RIFFIN=space(10)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_MATINI = space(40)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MATINI))
          .link_1_5('Full')
        endif
        .w_MATFIN = .w_MATINI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MATFIN))
          .link_1_6('Full')
        endif
        .w_LOTINI = space(20)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_LOTINI))
          .link_1_7('Full')
        endif
        .w_LOTFIN = .w_LOTINI
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_LOTFIN))
          .link_1_8('Full')
        endif
        .w_OBTEST = i_datsys
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_COCINI))
          .link_2_1('Full')
        endif
        .w_COCFIN = .w_COCINI
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_COCFIN))
          .link_2_2('Full')
        endif
        .w_MACINI = space(40)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MACINI))
          .link_2_3('Full')
        endif
        .w_MACFIN = .w_MACINI
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MACFIN))
          .link_2_4('Full')
        endif
        .w_LOCINI = space(20)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_LOCINI))
          .link_2_5('Full')
        endif
        .w_TIPSEL = 'E'
        .w_CAUINI = Space(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CAUINI))
          .link_2_7('Full')
        endif
        .w_CAUFIN = Space(5)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CAUFIN))
          .link_2_8('Full')
        endif
        .w_NUMINI = 0
        .w_NUMFIN = .w_NUMINI
        .w_ALFINI = ''
        .w_ALFFIN = ''
        .w_DTDFIN = .w_DTDINI
        .w_DTDINI = cp_CharToDate('  -  -    ')
        .w_DICINI = 0
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_DICINI))
          .link_2_15('Full')
        endif
        .w_DICFIN = .w_DICINI
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_DICFIN))
          .link_2_16('Full')
        endif
        .w_DTPINI = cp_CharToDate('  -  -    ')
        .w_DTPFIN = .w_DTPINI
        .w_LOCFIN = .w_LOCINI
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_LOCFIN))
          .link_2_19('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
    this.DoRTCalc(29,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN
            .w_MATINI = space(40)
          .link_1_5('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI
            .w_MATFIN = .w_MATINI
          .link_1_6('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI.or. .o_MATFIN<>.w_MATFIN
            .w_LOTINI = space(20)
          .link_1_7('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_CODFIN<>.w_CODFIN.or. .o_MATINI<>.w_MATINI.or. .o_MATFIN<>.w_MATFIN.or. .o_LOTINI<>.w_LOTINI
            .w_LOTFIN = .w_LOTINI
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_COCINI<>.w_COCINI
            .w_COCFIN = .w_COCINI
          .link_2_2('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN
            .w_MACINI = space(40)
          .link_2_3('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACINI<>.w_MACINI
            .w_MACFIN = .w_MACINI
          .link_2_4('Full')
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACFIN<>.w_MACFIN.or. .o_MACINI<>.w_MACINI
            .w_LOCINI = space(20)
          .link_2_5('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_TIPSEL<>.w_TIPSEL
            .w_CAUINI = Space(5)
          .link_2_7('Full')
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_CAUFIN = Space(5)
          .link_2_8('Full')
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_NUMINI = 0
        endif
        if .o_NUMINI<>.w_NUMINI.or. .o_TIPSEL<>.w_TIPSEL
            .w_NUMFIN = .w_NUMINI
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_ALFINI = ''
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_ALFFIN = ''
        endif
        if .o_DTDINI<>.w_DTDINI.or. .o_TIPSEL<>.w_TIPSEL
            .w_DTDFIN = .w_DTDINI
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_DTDINI = cp_CharToDate('  -  -    ')
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_DICINI = 0
          .link_2_15('Full')
        endif
        if .o_DICINI<>.w_DICINI.or. .o_TIPSEL<>.w_TIPSEL
            .w_DICFIN = .w_DICINI
          .link_2_16('Full')
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_DTPINI = cp_CharToDate('  -  -    ')
        endif
        if .o_DTPINI<>.w_DTPINI.or. .o_TIPSEL<>.w_TIPSEL
            .w_DTPFIN = .w_DTPINI
        endif
        if .o_COCINI<>.w_COCINI.or. .o_COCFIN<>.w_COCFIN.or. .o_MACINI<>.w_MACINI.or. .o_MACFIN<>.w_MACFIN.or. .o_LOCINI<>.w_LOCINI
            .w_LOCFIN = .w_LOCINI
          .link_2_19('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTINI_1_7.enabled = this.oPgFrm.Page1.oPag.oLOTINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oLOTFIN_1_8.enabled = this.oPgFrm.Page1.oPag.oLOTFIN_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oCAUINI_2_7.visible=!this.oPgFrm.Page2.oPag.oCAUINI_2_7.mHide()
    this.oPgFrm.Page2.oPag.oCAUFIN_2_8.visible=!this.oPgFrm.Page2.oPag.oCAUFIN_2_8.mHide()
    this.oPgFrm.Page2.oPag.oNUMINI_2_9.visible=!this.oPgFrm.Page2.oPag.oNUMINI_2_9.mHide()
    this.oPgFrm.Page2.oPag.oNUMFIN_2_10.visible=!this.oPgFrm.Page2.oPag.oNUMFIN_2_10.mHide()
    this.oPgFrm.Page2.oPag.oALFINI_2_11.visible=!this.oPgFrm.Page2.oPag.oALFINI_2_11.mHide()
    this.oPgFrm.Page2.oPag.oALFFIN_2_12.visible=!this.oPgFrm.Page2.oPag.oALFFIN_2_12.mHide()
    this.oPgFrm.Page2.oPag.oDTDFIN_2_13.visible=!this.oPgFrm.Page2.oPag.oDTDFIN_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDTDINI_2_14.visible=!this.oPgFrm.Page2.oPag.oDTDINI_2_14.mHide()
    this.oPgFrm.Page2.oPag.oDICINI_2_15.visible=!this.oPgFrm.Page2.oPag.oDICINI_2_15.mHide()
    this.oPgFrm.Page2.oPag.oDICFIN_2_16.visible=!this.oPgFrm.Page2.oPag.oDICFIN_2_16.mHide()
    this.oPgFrm.Page2.oPag.oDTPINI_2_17.visible=!this.oPgFrm.Page2.oPag.oDTPINI_2_17.mHide()
    this.oPgFrm.Page2.oPag.oDTPFIN_2_18.visible=!this.oPgFrm.Page2.oPag.oDTPFIN_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_33.visible=!this.oPgFrm.Page2.oPag.oStr_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_38.visible=!this.oPgFrm.Page2.oPag.oStr_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'',"Codici di ricerca",'TRACPRO2.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATINI))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATINI_1_5'),i_cWhere,'',"Matricole",'gsco1ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATINI)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MATINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATFIN))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_1_6'),i_cWhere,'',"Matricole",'gsco1ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATFIN)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MATFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTINI)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODINI;
                     ,'LOCODICE',trim(this.w_LOTINI))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTINI_1_7'),i_cWhere,'',"Lotti",'gsco3ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTINI);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODINI;
                       ,'LOCODICE',this.w_LOTINI)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTINI = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTINI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOTINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTFIN)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODINI;
                     ,'LOCODICE',trim(this.w_LOTFIN))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTFIN_1_8'),i_cWhere,'',"Lotti",'gsco3ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTFIN);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODINI;
                       ,'LOCODICE',this.w_LOTFIN)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTFIN = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTFIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOTFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCINI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COCINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_COCINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_COCINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOCINI_2_1'),i_cWhere,'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COCINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCINI = NVL(_Link_.CACODICE,space(20))
      this.w_DECINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCINI = space(20)
      endif
      this.w_DECINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COCINI<=.w_COCFIN or empty(.w_COCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_COCINI = space(20)
        this.w_DECINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCFIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COCFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_COCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_COCFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOCFIN_2_2'),i_cWhere,'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COCFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DECFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCFIN = space(20)
      endif
      this.w_DECFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COCINI<=.w_COCFIN or empty(.w_COCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_COCFIN = space(20)
        this.w_DECFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACINI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MACINI)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MACINI))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACINI)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACINI) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMACINI_2_3'),i_cWhere,'',"Matricole",'gsco2ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MACINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MACINI)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACINI = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MACINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MACINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MACFIN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MACFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MACFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MACFIN))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MACFIN)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MACFIN) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMACFIN_2_4'),i_cWhere,'',"Matricole",'gsco2ztm.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MACFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MACFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MACFIN)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MACFIN = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MACFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MACFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MACFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCINI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_LOCINI))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOCINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOCINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oLOCINI_2_5'),i_cWhere,'',"Lotti",'gsco4ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_LOCINI)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCINI = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOCINI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOCINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUINI
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUINI))
          select TDTIPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUINI)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUINI) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUINI_2_7'),i_cWhere,'GSAC_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUINI)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUINI = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUINI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUFIN
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUFIN))
          select TDTIPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUFIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUFIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUFIN_2_8'),i_cWhere,'GSAC_ATD',"Causali documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUFIN)
            select TDTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUFIN = NVL(_Link_.TDTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUFIN = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICINI
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PIAMPROD_IDX,3]
    i_lTable = "PIAMPROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2], .t., this.PIAMPROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PIAMPROD')
        if i_nConn<>0
          i_cWhere = " PPNUMREG="+cp_ToStrODBC(this.w_DICINI);

          i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPNUMREG',this.w_DICINI)
          select PPNUMREG,PPSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DICINI) and !this.bDontReportError
            deferred_cp_zoom('PIAMPROD','*','PPNUMREG',cp_AbsName(oSource.parent,'oDICINI_2_15'),i_cWhere,'',"Piano di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where PPNUMREG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPNUMREG',oSource.xKey(1))
            select PPNUMREG,PPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where PPNUMREG="+cp_ToStrODBC(this.w_DICINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPNUMREG',this.w_DICINI)
            select PPNUMREG,PPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICINI = NVL(_Link_.PPNUMREG,0)
      this.w_RIFINI = NVL(_Link_.PPSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DICINI = 0
      endif
      this.w_RIFINI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICINI = 0
        this.w_RIFINI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])+'\'+cp_ToStr(_Link_.PPNUMREG,1)
      cp_ShowWarn(i_cKey,this.PIAMPROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICFIN
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PIAMPROD_IDX,3]
    i_lTable = "PIAMPROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2], .t., this.PIAMPROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PIAMPROD')
        if i_nConn<>0
          i_cWhere = " PPNUMREG="+cp_ToStrODBC(this.w_DICFIN);

          i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPNUMREG',this.w_DICFIN)
          select PPNUMREG,PPSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_DICFIN) and !this.bDontReportError
            deferred_cp_zoom('PIAMPROD','*','PPNUMREG',cp_AbsName(oSource.parent,'oDICFIN_2_16'),i_cWhere,'',"Piano di produzione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where PPNUMREG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPNUMREG',oSource.xKey(1))
            select PPNUMREG,PPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPNUMREG,PPSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where PPNUMREG="+cp_ToStrODBC(this.w_DICFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPNUMREG',this.w_DICFIN)
            select PPNUMREG,PPSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICFIN = NVL(_Link_.PPNUMREG,0)
      this.w_RIFFIN = NVL(_Link_.PPSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_DICFIN = 0
      endif
      this.w_RIFFIN = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DICFIN = 0
        this.w_RIFFIN = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PIAMPROD_IDX,2])+'\'+cp_ToStr(_Link_.PPNUMREG,1)
      cp_ShowWarn(i_cKey,this.PIAMPROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCFIN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_LOCFIN))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOCFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOCFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oLOCFIN_2_19'),i_cWhere,'',"Lotti",'gsco4ztm.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_LOCFIN)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCFIN = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOCFIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_LOCFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_1_5.value==this.w_MATINI)
      this.oPgFrm.Page1.oPag.oMATINI_1_5.value=this.w_MATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_1_6.value==this.w_MATFIN)
      this.oPgFrm.Page1.oPag.oMATFIN_1_6.value=this.w_MATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTINI_1_7.value==this.w_LOTINI)
      this.oPgFrm.Page1.oPag.oLOTINI_1_7.value=this.w_LOTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTFIN_1_8.value==this.w_LOTFIN)
      this.oPgFrm.Page1.oPag.oLOTFIN_1_8.value=this.w_LOTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCINI_2_1.value==this.w_COCINI)
      this.oPgFrm.Page2.oPag.oCOCINI_2_1.value=this.w_COCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOCFIN_2_2.value==this.w_COCFIN)
      this.oPgFrm.Page2.oPag.oCOCFIN_2_2.value=this.w_COCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMACINI_2_3.value==this.w_MACINI)
      this.oPgFrm.Page2.oPag.oMACINI_2_3.value=this.w_MACINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMACFIN_2_4.value==this.w_MACFIN)
      this.oPgFrm.Page2.oPag.oMACFIN_2_4.value=this.w_MACFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCINI_2_5.value==this.w_LOCINI)
      this.oPgFrm.Page2.oPag.oLOCINI_2_5.value=this.w_LOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPSEL_2_6.RadioValue()==this.w_TIPSEL)
      this.oPgFrm.Page2.oPag.oTIPSEL_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUINI_2_7.value==this.w_CAUINI)
      this.oPgFrm.Page2.oPag.oCAUINI_2_7.value=this.w_CAUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUFIN_2_8.value==this.w_CAUFIN)
      this.oPgFrm.Page2.oPag.oCAUFIN_2_8.value=this.w_CAUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMINI_2_9.value==this.w_NUMINI)
      this.oPgFrm.Page2.oPag.oNUMINI_2_9.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMFIN_2_10.value==this.w_NUMFIN)
      this.oPgFrm.Page2.oPag.oNUMFIN_2_10.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oALFINI_2_11.value==this.w_ALFINI)
      this.oPgFrm.Page2.oPag.oALFINI_2_11.value=this.w_ALFINI
    endif
    if not(this.oPgFrm.Page2.oPag.oALFFIN_2_12.value==this.w_ALFFIN)
      this.oPgFrm.Page2.oPag.oALFFIN_2_12.value=this.w_ALFFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDTDFIN_2_13.value==this.w_DTDFIN)
      this.oPgFrm.Page2.oPag.oDTDFIN_2_13.value=this.w_DTDFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDTDINI_2_14.value==this.w_DTDINI)
      this.oPgFrm.Page2.oPag.oDTDINI_2_14.value=this.w_DTDINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDICINI_2_15.value==this.w_DICINI)
      this.oPgFrm.Page2.oPag.oDICINI_2_15.value=this.w_DICINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDICFIN_2_16.value==this.w_DICFIN)
      this.oPgFrm.Page2.oPag.oDICFIN_2_16.value=this.w_DICFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDTPINI_2_17.value==this.w_DTPINI)
      this.oPgFrm.Page2.oPag.oDTPINI_2_17.value=this.w_DTPINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDTPFIN_2_18.value==this.w_DTPFIN)
      this.oPgFrm.Page2.oPag.oDTPFIN_2_18.value=this.w_DTPFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCFIN_2_19.value==this.w_LOCFIN)
      this.oPgFrm.Page2.oPag.oLOCFIN_2_19.value=this.w_LOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDECINI_2_20.value==this.w_DECINI)
      this.oPgFrm.Page2.oPag.oDECINI_2_20.value=this.w_DECINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDECFIN_2_21.value==this.w_DECFIN)
      this.oPgFrm.Page2.oPag.oDECFIN_2_21.value=this.w_DECFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATFIN))  and not(empty(.w_MATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MATINI <= .w_MATFIN OR EMPTY(.w_MATINI))  and not(empty(.w_MATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMATFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTFIN))  and (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)  and not(empty(.w_LOTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOTINI <= .w_LOTFIN OR EMPTY(.w_LOTINI))  and (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)  and not(empty(.w_LOTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COCINI<=.w_COCFIN or empty(.w_COCFIN))  and not(empty(.w_COCINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCINI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_COCINI<=.w_COCFIN or empty(.w_COCINI))  and not(empty(.w_COCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOCFIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACFIN))  and not(empty(.w_MACINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMACINI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MACINI <= .w_MACFIN OR EMPTY(.w_MACINI))  and not(empty(.w_MACFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMACFIN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCFIN))  and not(empty(.w_LOCINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLOCINI_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NUMINI <= .w_NUMFIN OR EMPTY(.w_NUMFIN))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMINI_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NUMINI <= .w_numfin OR EMPTY(.w_NUMINI))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMFIN_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_ALFFIN)) OR  (.w_ALFINI<=.w_ALFFIN))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oALFINI_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oALFFIN_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDINI))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTDFIN_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDFIN))  and not(.w_TIPSEL<>'D')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTDINI_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICFIN))  and not(.w_TIPSEL<>'P')  and not(empty(.w_DICINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDICINI_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DICINI <= .w_DICFIN OR EMPTY(.w_DICINI))  and not(.w_TIPSEL<>'P')  and not(empty(.w_DICFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDICFIN_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTPINI <= .w_DTPFIN OR EMPTY(.w_DTPFIN))  and not(.w_TIPSEL<>'P')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTPINI_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DTPINI <= .w_DTPFIN OR EMPTY(.w_DTPINI))  and not(.w_TIPSEL<>'P')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDTPFIN_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LOCINI <= .w_LOCFIN OR EMPTY(.w_LOCINI))  and not(empty(.w_LOCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLOCFIN_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_MATINI = this.w_MATINI
    this.o_MATFIN = this.w_MATFIN
    this.o_LOTINI = this.w_LOTINI
    this.o_COCINI = this.w_COCINI
    this.o_COCFIN = this.w_COCFIN
    this.o_MACINI = this.w_MACINI
    this.o_MACFIN = this.w_MACFIN
    this.o_LOCINI = this.w_LOCINI
    this.o_TIPSEL = this.w_TIPSEL
    this.o_NUMINI = this.w_NUMINI
    this.o_DTDINI = this.w_DTDINI
    this.o_DICINI = this.w_DICINI
    this.o_DTPINI = this.w_DTPINI
    return

enddefine

* --- Define pages as container
define class tgsds_stmPag1 as StdContainer
  Width  = 585
  height = 256
  stdWidth  = 585
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="SZEKJHCVBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 104583386,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=105, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_LOTINI)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_LOTFIN)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACPRO2.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_2 as StdField with uid="UWDLPXXTHQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104524490,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=268, Top=17, InputMask=replicate('X',40)

  add object oCODFIN_1_3 as StdField with uid="KAOZXLFOUP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 26136794,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=105, Top=43, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACPROD.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESFIN_1_4 as StdField with uid="MYLSJYYKJE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26077898,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=268, Top=43, InputMask=replicate('X',40)

  add object oMATINI_1_5 as StdField with uid="BAUZFWPTWC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MATINI", cQueryName = "MATINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di inizio selezione",;
    HelpContextID = 104521274,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=105, Top=69, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATINI"

  func oMATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco1ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oMATFIN_1_6 as StdField with uid="GHDDKOKVIG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MATFIN", cQueryName = "MATFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di fine selezione",;
    HelpContextID = 26074682,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=105, Top=95, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATFIN"

  func oMATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco1ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oLOTINI_1_7 as StdField with uid="VYQYSTJFCH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LOTINI", cQueryName = "LOTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di inizio selezione",;
    HelpContextID = 104517706,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=105, Top=122, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTINI"

  func oLOTINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)
    endwith
   endif
  endfunc

  func oLOTINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco3ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oLOTFIN_1_8 as StdField with uid="BKTBROMCMD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LOTFIN", cQueryName = "LOTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 26071114,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=394, Top=122, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_CODINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTFIN"

  func oLOTFIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODINI) AND NOT EMPTY(.w_CODFIN) AND .w_CODINI=.w_CODFIN)
    endwith
   endif
  endfunc

  func oLOTFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco3ztm.LOTTIART_VZM',this.parent.oContained
  endproc


  add object oBtn_1_17 as StdButton with uid="HZUGTMEYRQ",left=480, top=210, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare movimenti matricole";
    , HelpContextID = 200661722;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oObj_1_18 as cp_outputCombo with uid="ZLERXIVPWT",left=105, top=165, width=414,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 164453658


  add object oBtn_1_19 as StdButton with uid="FRSAGQFIZA",left=531, top=210, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 201737030;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_9 as StdString with uid="JHACHFBEZX",Visible=.t., Left=5, Top=17,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="CGICWDOWMU",Visible=.t., Left=5, Top=43,;
    Alignment=1, Width=96, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=5, Top=122,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BETNLUNOYI",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=96, Height=15,;
    Caption="A matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=337, Top=122,;
    Alignment=1, Width=53, Height=15,;
    Caption="A lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="PGZKNKALVF",Visible=.t., Left=5, Top=165,;
    Alignment=1, Width=96, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsds_stmPag2 as StdContainer
  Width  = 585
  height = 256
  stdWidth  = 585
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCINI_2_1 as StdField with uid="IQFGQMWCZO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COCINI", cQueryName = "COCINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 104587482,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=116, Top=17, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_COCINI"

  func oCOCINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCINI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCINI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOCINI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCOCFIN_2_2 as StdField with uid="TBUXLZLSFY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COCFIN", cQueryName = "COCFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 26140890,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=116, Top=41, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_COCFIN"

  func oCOCFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCFIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCFIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOCFIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'TRACMATR.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oMACINI_2_3 as StdField with uid="MXBHNACTEW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MACINI", cQueryName = "MACINI",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di inizio selezione",;
    HelpContextID = 104590906,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=116, Top=65, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MACINI"

  func oMACINI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACINI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMACINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco2ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oMACFIN_2_4 as StdField with uid="EXIOWJGCBL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MACFIN", cQueryName = "MACFIN",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola di fine selezione",;
    HelpContextID = 26144314,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=116, Top=89, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", oKey_1_1="AMCODICE", oKey_1_2="this.w_MACFIN"

  func oMACFIN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMACFIN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMACFIN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMACFIN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Matricole",'gsco2ztm.MATRICOL_VZM',this.parent.oContained
  endproc

  add object oLOCINI_2_5 as StdField with uid="YGCZPUZYWL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_LOCINI", cQueryName = "LOCINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di inizio selezione",;
    HelpContextID = 104587338,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=116, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODICE", oKey_1_2="this.w_LOCINI"

  func oLOCINI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCINI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCINI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oLOCINI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco4ztm.LOTTIART_VZM',this.parent.oContained
  endproc


  add object oTIPSEL_2_6 as StdCombo with uid="YYWVRDSRQT",rtseq=15,rtrep=.f.,left=116,top=138,width=196,height=21;
    , ToolTipText = "Tipo di selezione matricole";
    , HelpContextID = 62985674;
    , cFormVar="w_TIPSEL",RowSource=""+"Da piano produzione,"+"Da documenti di produzione,"+"Entrambi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPSEL_2_6.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oTIPSEL_2_6.GetRadio()
    this.Parent.oContained.w_TIPSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPSEL_2_6.SetRadio()
    this.Parent.oContained.w_TIPSEL=trim(this.Parent.oContained.w_TIPSEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSEL=='P',1,;
      iif(this.Parent.oContained.w_TIPSEL=='D',2,;
      iif(this.Parent.oContained.w_TIPSEL=='E',3,;
      0)))
  endfunc

  add object oCAUINI_2_7 as StdField with uid="XBKNFLCIMB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CAUINI", cQueryName = "CAUINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 104517338,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=83, Top=200, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUINI"

  func oCAUINI_2_7.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oCAUINI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUINI_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUINI_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUINI_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oCAUINI_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CAUINI
     i_obj.ecpSave()
  endproc

  add object oCAUFIN_2_8 as StdField with uid="HFGGGGSOKX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CAUFIN", cQueryName = "CAUFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 26070746,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=83, Top=222, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUFIN"

  func oCAUFIN_2_8.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oCAUFIN_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUFIN_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUFIN_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUFIN_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'',this.parent.oContained
  endproc
  proc oCAUFIN_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CAUFIN
     i_obj.ecpSave()
  endproc

  add object oNUMINI_2_9 as StdField with uid="HQHVDGLJKG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di inizio selezione",;
    HelpContextID = 104544810,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=272, Top=200, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_2_9.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oNUMINI_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI <= .w_NUMFIN OR EMPTY(.w_NUMFIN))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_2_10 as StdField with uid="ETHPIEMRQR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 26098218,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=272, Top=222, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMFIN_2_10.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oNUMFIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI <= .w_numfin OR EMPTY(.w_NUMINI))
    endwith
    return bRes
  endfunc

  add object oALFINI_2_11 as StdField with uid="HVPVSEHHGU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ALFINI", cQueryName = "ALFINI",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Alfa del documento iniziale selezionato",;
    HelpContextID = 104575994,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=368, Top=200, InputMask=replicate('X',2)

  func oALFINI_2_11.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oALFINI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_ALFFIN)) OR  (.w_ALFINI<=.w_ALFFIN))
    endwith
    return bRes
  endfunc

  add object oALFFIN_2_12 as StdField with uid="LHPNDMBVHJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ALFFIN", cQueryName = "ALFFIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Alfa del documento finale selezionato",;
    HelpContextID = 26129402,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=368, Top=222, InputMask=replicate('X',2)

  func oALFFIN_2_12.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oALFFIN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
    endwith
    return bRes
  endfunc

  add object oDTDFIN_2_13 as StdField with uid="UOJAWAEHVO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DTDFIN", cQueryName = "DTDFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 26135498,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=496, Top=221

  func oDTDFIN_2_13.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oDTDFIN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDINI))
    endwith
    return bRes
  endfunc

  add object oDTDINI_2_14 as StdField with uid="AUCHYPCWKM",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DTDINI", cQueryName = "DTDINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 104582090,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=496, Top=199

  func oDTDINI_2_14.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  func oDTDINI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTDINI <= .w_DTDFIN OR EMPTY(.w_DTDFIN))
    endwith
    return bRes
  endfunc

  add object oDICINI_2_15 as StdField with uid="WQNLFQEXDD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DICINI", cQueryName = "DICINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero piano di produzione di inizio selezione",;
    HelpContextID = 104589002,;
   bGlobalFont=.t.,;
    Height=21, Width=54, Left=133, Top=200, bHasZoom = .t. , cLinkFile="PIAMPROD", oKey_1_1="PPNUMREG", oKey_1_2="this.w_DICINI"

  func oDICINI_2_15.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  func oDICINI_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICINI_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICINI_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PIAMPROD','*','PPNUMREG',cp_AbsName(this.parent,'oDICINI_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Piano di produzione",'',this.parent.oContained
  endproc

  add object oDICFIN_2_16 as StdField with uid="WHQQEZYTLE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DICFIN", cQueryName = "DICFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero piano di produzione di fine selezione",;
    HelpContextID = 26142410,;
   bGlobalFont=.t.,;
    Height=21, Width=54, Left=133, Top=222, bHasZoom = .t. , cLinkFile="PIAMPROD", oKey_1_1="PPNUMREG", oKey_1_2="this.w_DICFIN"

  func oDICFIN_2_16.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  func oDICFIN_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICFIN_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICFIN_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PIAMPROD','*','PPNUMREG',cp_AbsName(this.parent,'oDICFIN_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Piano di produzione",'',this.parent.oContained
  endproc

  add object oDTPINI_2_17 as StdField with uid="GDSDVFEHKK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DTPINI", cQueryName = "DTPINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data piano di inizio selezione",;
    HelpContextID = 104532938,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=305, Top=200

  func oDTPINI_2_17.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  func oDTPINI_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTPINI <= .w_DTPFIN OR EMPTY(.w_DTPFIN))
    endwith
    return bRes
  endfunc

  add object oDTPFIN_2_18 as StdField with uid="EYSAEQKSHL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DTPFIN", cQueryName = "DTPFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data piano di fine selezione",;
    HelpContextID = 26086346,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=305, Top=222

  func oDTPFIN_2_18.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  func oDTPFIN_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DTPINI <= .w_DTPFIN OR EMPTY(.w_DTPINI))
    endwith
    return bRes
  endfunc

  add object oLOCFIN_2_19 as StdField with uid="TGZPADMXNT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_LOCFIN", cQueryName = "LOCFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 26140746,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=405, Top=113, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODICE", oKey_1_2="this.w_LOCFIN"

  func oLOCFIN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCFIN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCFIN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oLOCFIN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'gsco4ztm.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oDECINI_2_20 as StdField with uid="ANDVBHJWDX",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DECINI", cQueryName = "DECINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104590026,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=279, Top=17, InputMask=replicate('X',40)

  add object oDECFIN_2_21 as StdField with uid="NWKNSBGIQH",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DECFIN", cQueryName = "DECFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26143434,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=279, Top=41, InputMask=replicate('X',40)

  add object oStr_2_22 as StdString with uid="HGLLLTOCVE",Visible=.t., Left=6, Top=17,;
    Alignment=1, Width=106, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="CTZMIIHWUH",Visible=.t., Left=6, Top=41,;
    Alignment=1, Width=106, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="VKKHKJCAEV",Visible=.t., Left=6, Top=65,;
    Alignment=1, Width=106, Height=15,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="INDZOCTWXR",Visible=.t., Left=341, Top=113,;
    Alignment=1, Width=60, Height=15,;
    Caption="A lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="ICHJGVJLAW",Visible=.t., Left=6, Top=89,;
    Alignment=1, Width=106, Height=15,;
    Caption="A matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="PNYCXUFOTG",Visible=.t., Left=6, Top=113,;
    Alignment=1, Width=106, Height=15,;
    Caption="Da lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=7, Top=197,;
    Alignment=1, Width=122, Height=18,;
    Caption="Da piano produzione:"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=6, Top=219,;
    Alignment=1, Width=123, Height=18,;
    Caption="A piano di produzione:"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="CSUDQKLXHQ",Visible=.t., Left=392, Top=199,;
    Alignment=1, Width=100, Height=18,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_31 as StdString with uid="QVXSTVYWUB",Visible=.t., Left=392, Top=221,;
    Alignment=1, Width=100, Height=18,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="LTENIIBGWH",Visible=.t., Left=192, Top=200,;
    Alignment=1, Width=76, Height=15,;
    Caption="Da doc.int:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="IKFKEWCTKN",Visible=.t., Left=200, Top=222,;
    Alignment=1, Width=68, Height=15,;
    Caption="A doc.int.:"  ;
  , bGlobalFont=.t.

  func oStr_2_33.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="WMRGDURNBL",Visible=.t., Left=6, Top=170,;
    Alignment=0, Width=219, Height=18,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="LVVBINNLET",Visible=.t., Left=361, Top=202,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_37 as StdString with uid="PKVCFMTCZS",Visible=.t., Left=361, Top=226,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_38 as StdString with uid="YBSXPFSDKK",Visible=.t., Left=5, Top=198,;
    Alignment=1, Width=75, Height=18,;
    Caption="Da causale:"  ;
  , bGlobalFont=.t.

  func oStr_2_38.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="MVOZPMDDVS",Visible=.t., Left=12, Top=222,;
    Alignment=1, Width=68, Height=18,;
    Caption="A causale:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'D')
    endwith
  endfunc

  add object oStr_2_40 as StdString with uid="FMMKAPKQOT",Visible=.t., Left=6, Top=138,;
    Alignment=1, Width=106, Height=18,;
    Caption="Tipo selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="NGCCCSJEIH",Visible=.t., Left=216, Top=200,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da data piano:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="BTKAXHPMFK",Visible=.t., Left=225, Top=222,;
    Alignment=1, Width=77, Height=18,;
    Caption="A data piano:"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'P')
    endwith
  endfunc

  add object oBox_2_35 as StdBox with uid="NJTNMJACPG",left=1, top=191, width=582,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsds_stm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
