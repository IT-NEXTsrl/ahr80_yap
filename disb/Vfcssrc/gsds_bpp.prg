* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bpp                                                        *
*              Batch stampa piano di produzione                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_233]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2016-01-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bpp",oParentObject)
return(i_retval)

define class tgsds_bpp as StdBatch
  * --- Local variables
  w_CODDIS = space(20)
  w_DESDIS = space(40)
  w_QUANTI = 0
  w_REP = space(50)
  w_QRY = space(50)
  w_DATFIL = ctod("  /  /  ")
  QTC = 0
  w_NUMLEV = 0
  w_CODCOM = space(20)
  w_DESCOM = space(40)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTASAL = 0
  w_QTPROD = 0
  w_UMPROD = space(3)
  w_COEIMP = space(15)
  w_FLVARI = space(1)
  w_FLESPL = space(1)
  w_ARTCOM = space(20)
  w_UNMIS1 = space(3)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_FLFRAZ = space(1)
  w_CODI_DIST = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  PIA_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili passate dalla maschera
    * --- Batch Stampa Piano di Produzione lanciato da :
    *     GSDS_SPP 
    *     GSDS_BPF 
    DIMENSION QTC[99]
    this.w_MAXLEVEL = 99
    this.w_VERIFICA = "S"
    this.w_FILSTAT = " "
    this.w_DATFIL = this.oParentObject.w_DATSTA
    this.w_QRY = ALLTRIM(this.oParentObject.w_OQRY)
    this.w_REP = ALLTRIM(this.oParentObject.w_OREP)
    if EMPTY(this.oParentObject.w_SERPIA)
      ah_ErrorMsg("Nessun piano di produzione selezionato",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_OQRY) OR EMPTY(this.oParentObject.w_OREP)
      ah_ErrorMsg("Query/report di stampa non definiti",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_ONUME=1 OR this.oParentObject.w_ONUME=2 OR this.oParentObject.w_ONUME=4 OR this.oParentObject.w_ONUME=5
      * --- Crea Cursore di Appoggio
      CREATE CURSOR __TMP1__ (CODDIS C(20), DESDIS C(40), UMPROD C(3), QTPROD N(12,3), ;
      CODCOM C(20), DESCOM C(40), UNIMIS C(3), QTAMOV N(12,3), COEIMP C(15), FLVARI C(1), FLESPL C(1), ARTCOM C(20), DISPAD C(20))
      * --- Select from PIA_PROD
      i_nConn=i_TableProp[this.PIA_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PIA_PROD_idx,2],.t.,this.PIA_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PIA_PROD ";
            +" where PPSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERPIA)+"";
             ,"_Curs_PIA_PROD")
      else
        select * from (i_cTable);
         where PPSERIAL=this.oParentObject.w_SERPIA;
          into cursor _Curs_PIA_PROD
      endif
      if used('_Curs_PIA_PROD')
        select _Curs_PIA_PROD
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(_Curs_PIA_PROD.PPARTCOM,"")) AND NVL(_Curs_PIA_PROD.PPQTAUM1,0)<>0
          this.w_CODDIS = _Curs_PIA_PROD.PPARTCOM
          this.w_QUANTI = _Curs_PIA_PROD.PPQTAUM1
          * --- Qta da Produrre e U.M. (solo da stampare, in realta' i calcoli sono considerati sulla UM Principale)
          this.w_QTPROD = _Curs_PIA_PROD.PPQTAMOV
          this.w_UMPROD = _Curs_PIA_PROD.PPUNIMIS
          this.w_DESDIS = SPACE(40)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODDIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = this.w_CODDIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESDIS = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Parametri per Batch di Esplosione
          this.w_DBCODINI = this.w_CODDIS
          this.w_DBCODFIN = this.w_CODDIS
          if used("TES_PLOS")
            select TES_PLOS
            use
          endif
          gsar_bde(this,"A")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
          select _Curs_PIA_PROD
          continue
        enddo
        use
      endif
      if USED("__TMP1__")
        * --- Se esiste tmp
        SELECT __TMP1__ 
        if RECCOUNT()>0
          * --- Considera solo le 'foglie'
          GO TOP
          DELETE FROM __TMP1__ WHERE FLESPL="S"
          GO TOP
          * --- Sommarizzata, Raggruppa per Componente (Esclude il Livello 0000)
          if this.oParentObject.w_ONUME=2 OR this.oParentObject.w_ONUME=4
            * --- Raggruppa sui soli Componenti
            SELECT CODCOM, MAX(CODDIS) AS CODDIS, MAX(DESDIS) AS DESDIS, MAX(UMPROD) AS UMPROD, MAX(QTPROD) AS QTPROD, ;
            MAX(DESCOM) AS DESCOM, MAX(UNIMIS) AS UNIMIS, SUM(QTAMOV) AS QTAMOV, MAX(ARTCOM) AS ARTCOM ;
            FROM __TMP1__ GROUP BY 1 ORDER BY 1 INTO CURSOR __TMP1__
          else
            if this.oParentObject.w_ONUME=5
              SELECT CODDIS, CODCOM, MAX(DESDIS) AS DESDIS, MAX(UMPROD) AS UMPROD, MAX(QTPROD) AS QTPROD, ;
              MAX(DESCOM) AS DESCOM, MAX(UNIMIS) AS UNIMIS, SUM(QTAMOV) AS QTAMOV, MAX(ARTCOM) AS ARTCOM, DISPAD ;
              FROM __TMP1__ GROUP BY 10, 1, 2 ORDER BY 10, 1, 2 INTO CURSOR __TMP1__
            else
              SELECT CODDIS, CODCOM, MAX(DESDIS) AS DESDIS, MAX(UMPROD) AS UMPROD, MAX(QTPROD) AS QTPROD, ;
              MAX(DESCOM) AS DESCOM, MAX(UNIMIS) AS UNIMIS, SUM(QTAMOV) AS QTAMOV, MAX(ARTCOM) AS ARTCOM ;
              FROM __TMP1__ GROUP BY 1, 2 ORDER BY 1, 2 INTO CURSOR __TMP1__
            endif
          endif
          * --- Rndo Scrivibile per Unirlo con i Saldi
          =WRCURSOR("__TMP1__")
          GO TOP
          * --- A questo Punto Leggo i Saldi delle Disponibilita'
          VQ_EXEC(this.w_QRY, this,"TES_PLOS")
          if this.oParentObject.w_ONUME=2 OR this.oParentObject.w_ONUME=4
            if this.oParentObject.w_COMDIS="S"
              * --- Stampa anche i Componenti Disponibili
              SELECT __TMP1__.CODDIS AS CODDIS, __TMP1__.CODCOM AS CODCOM, __TMP1__.DESDIS AS DESDIS, ;
              __TMP1__.UMPROD AS UMPROD, __TMP1__.QTPROD AS QTPROD, __TMP1__.DESCOM AS DESCOM, __TMP1__.ARTCOM AS ARTCOM, ;
              __TMP1__.UNIMIS AS UNIMIS, __TMP1__.QTAMOV AS QTAMOV, NVL(TES_PLOS.QTASAL, 0) AS QTASAL, ;
              NVL(TES_PLOS.UNMIS1, "   ") AS UNMIS1, NVL(TES_PLOS.FLFRAZ, " ") AS FLFRAZ ;
              FROM (__TMP1__ LEFT OUTER JOIN TES_PLOS ON __TMP1__.CODCOM = TES_PLOS.CODCOM AND __TMP1__.ARTCOM = TES_PLOS.CODART) ;
              ORDER BY 1, 2 INTO CURSOR __TMP__ NOFILTER
            else
              * --- Toglie i Componenti Disponibili
              SELECT __TMP1__.CODDIS AS CODDIS, __TMP1__.CODCOM AS CODCOM, __TMP1__.DESDIS AS DESDIS, ;
              __TMP1__.UMPROD AS UMPROD, __TMP1__.QTPROD AS QTPROD, __TMP1__.DESCOM AS DESCOM, __TMP1__.ARTCOM AS ARTCOM, ;
              __TMP1__.UNIMIS AS UNIMIS, __TMP1__.QTAMOV AS QTAMOV, NVL(TES_PLOS.QTASAL, 0) AS QTASAL, ;
              NVL(TES_PLOS.UNMIS1, "   ") AS UNMIS1, NVL(TES_PLOS.FLFRAZ, " ") AS FLFRAZ ;
              FROM (__TMP1__ LEFT OUTER JOIN TES_PLOS ON __TMP1__.CODCOM = TES_PLOS.CODCOM AND __TMP1__.ARTCOM = TES_PLOS.CODART) ;
              WHERE __TMP1__.QTAMOV > NVL(TES_PLOS.QTASAL, 0) ORDER BY 1, 2 INTO CURSOR __TMP__ NOFILTER
            endif
          else
            if this.oParentObject.w_ONUME=5
              SELECT __TMP1__.CODDIS AS CODDIS, __TMP1__.CODCOM AS CODCOM, __TMP1__.DESDIS AS DESDIS, ;
              __TMP1__.UMPROD AS UMPROD, __TMP1__.QTPROD AS QTPROD, __TMP1__.DESCOM AS DESCOM, __TMP1__.ARTCOM AS ARTCOM, ;
              __TMP1__.UNIMIS AS UNIMIS, __TMP1__.QTAMOV AS QTAMOV, NVL(TES_PLOS.QTASAL, 0) AS QTASAL, __TMP1__.DISPAD, ;
              NVL(TES_PLOS.UNMIS1, "   ") AS UNMIS1, NVL(TES_PLOS.FLFRAZ, " ") AS FLFRAZ, TES_PLOS.DBNOTAGG, TES_PLOS.DB__NOTE, TES_PLOS.DBCODICE, TES_PLOS.CPROWORD AS CPROWORD;
              FROM (__TMP1__ LEFT OUTER JOIN TES_PLOS ON __TMP1__.CODCOM = TES_PLOS.CODCOM AND __TMP1__.ARTCOM = TES_PLOS.CODART AND __TMP1__.DISPAD=TES_PLOS.DBCODICE) ;
              ORDER BY CODDIS, TES_PLOS.CPROWORD INTO CURSOR __TMP__ NOFILTER
            else
              SELECT __TMP1__.CODDIS AS CODDIS, __TMP1__.CODCOM AS CODCOM, __TMP1__.DESDIS AS DESDIS, ;
              __TMP1__.UMPROD AS UMPROD, __TMP1__.QTPROD AS QTPROD, __TMP1__.DESCOM AS DESCOM, __TMP1__.ARTCOM AS ARTCOM, ;
              __TMP1__.UNIMIS AS UNIMIS, __TMP1__.QTAMOV AS QTAMOV, NVL(TES_PLOS.QTASAL, 0) AS QTASAL, ;
              NVL(TES_PLOS.UNMIS1, "   ") AS UNMIS1, NVL(TES_PLOS.FLFRAZ, " ") AS FLFRAZ ;
              FROM (__TMP1__ LEFT OUTER JOIN TES_PLOS ON __TMP1__.CODCOM = TES_PLOS.CODCOM AND __TMP1__.ARTCOM = TES_PLOS.CODART) ;
              ORDER BY 1, 2 INTO CURSOR __TMP__ NOFILTER
            endif
          endif
          if used("__TMP1__")
            select __TMP1__
            use
          endif
          if used("TES_PLOS")
            select TES_PLOS
            use
          endif
          if used("__TMP__")
            =WRCURSOR("__TMP__")
            * --- Verifica le Quantita' se riferite alla 1^UM
            select __TMP__
            GO TOP
            SCAN FOR NOT EMPTY(CODCOM)
            this.w_UNIMIS = UNIMIS
            this.w_UNMIS1 = NVL(UNMIS1, "   ")
            this.w_FLFRAZ = NVL(FLFRAZ, " ")
            this.w_QTAMOV = NVL(QTAMOV, 0)
            if this.w_UNIMIS<>this.w_UNMIS1 AND this.w_QTAMOV<>0 AND NOT EMPTY(this.w_UNMIS1)
              REPLACE UNIMIS WITH this.w_UNMIS1
            endif
            * --- Se U.M. Non Frazionabile aggiorna la Qta all'Intero Superiore
            if this.w_FLFRAZ="S" AND this.w_QTAMOV<>INT(this.w_QTAMOV)
              REPLACE QTAMOV WITH cp_ROUND(this.w_QTAMOV+.499, 0)
            endif
            select __TMP__
            ENDSCAN 
          endif
        else
          ah_ErrorMsg("Per le selezioni impostate non esistono dati da stampare",,"")
        endif
      endif
    else
      VQ_EXEC(this.w_QRY, this,"__TMP__")
    endif
    L_NUMREG=this.oParentObject.w_NUMREG
    L_ALFREG=this.oParentObject.w_ALFREG
    L_DATREG=this.oParentObject.w_DATREG
    L_DESCRI=this.oParentObject.w_DESCRI
    L_DATSTA=this.oParentObject.w_DATSTA
    L_CODMAG=this.oParentObject.w_CODMAG
    L_ONUME=this.oParentObject.w_ONUME
    L_ODES=this.oParentObject.w_ODES
    if USED("__TMP__")
      select __TMP__
      if RECCOUNT()>0
        GO TOP
        CP_CHPRN(this.w_REP, " ", this)
      else
        ah_ErrorMsg("Tutti i componenti sono sufficienti alla produzione",,"")
      endif
    endif
    * --- Rimuovo i cursori
    if used("__TMP1__")
      select __TMP1__
      use
    endif
    if used("TES_PLOS")
      select TES_PLOS
      use
    endif
    if used("__Tmp__")
      select __Tmp__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il temporaneo di Elaborazione
    if used("TES_PLOS")
      SELECT TES_PLOS
      GO TOP
      SCAN FOR NOT EMPTY(DISPAD)
      this.w_NUMLEV = NUMLEV
      this.w_DESCOM = DESCOM
      this.w_UNIMIS = UNIMIS
      this.w_QTAMOV = IIF(QTAUM1=0, QTAMOV, QTAUM1)
      this.w_COEIMP = COEIMP
      this.w_FLVARI = FLVARI
      this.w_FLESPL = FLESPL
      this.w_ARTCOM = ARTCOM
      this.w_CODI_DIST = DISPAD
      if VAL(this.w_NUMLEV)=0
        FOR L_i = 1 TO 99
        QTC[L_i] = 0
        ENDFOR
      else
        * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
        QTC[VAL(this.w_NUMLEV)] = this.w_QTAMOV
        * --- Parte dalle Quantita' da Produrre (Distinta=liv. 0000)
        this.w_QTAMOV = this.w_QUANTI
        FOR L_i = 1 TO VAL(this.w_NUMLEV)
        this.w_QTAMOV = this.w_QTAMOV * QTC[L_i]
        ENDFOR
        this.w_CODCOM = CODCOM
        INSERT INTO __TMP1__ ;
        (CODDIS, DESDIS, UMPROD, QTPROD, CODCOM, DESCOM, UNIMIS, QTAMOV, COEIMP, FLVARI, FLESPL, ARTCOM, DISPAD) VALUES ;
        (this.w_CODDIS, this.w_DESDIS, this.w_UMPROD, this.w_QTPROD, this.w_CODCOM, this.w_DESCOM, this.w_UNIMIS, cp_ROUND(this.w_QTAMOV, 3), ;
        this.w_COEIMP, this.w_FLVARI, this.w_FLESPL, this.w_ARTCOM, this.w_CODI_DIST)
      endif
      SELECT TES_PLOS
      ENDSCAN
      select TES_PLOS
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='PIA_PROD'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PIA_PROD')
      use in _Curs_PIA_PROD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
