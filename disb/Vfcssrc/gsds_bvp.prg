* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bvp                                                        *
*              Calcola coefficienti di impiego                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_65]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-01                                                      *
* Last revis.: 2000-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bvp",oParentObject,m.pOpz)
return(i_retval)

define class tgsds_bvp as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_CODART = space(20)
  w_cKey = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge il Post-In associato al Codice di Ricerca (richiamato da varie gestioni)
    * --- pOpz: 'D' = GSDS_MDB ; 'V' = GSDS_MCV ; 'P' = GSDS_MPP 
    * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
    this.w_CODART = SPACE(20)
    do case
      case this.pOpz="D"
        * --- Distinta Base
        this.w_CODART = this.oParentObject.w_DBARTCOM
      case this.pOpz="V"
        * --- Componenti Varianti
        this.w_CODART = this.oParentObject.w_CVARTCOM
      case this.pOpz="P"
        * --- Piano di Produzione
        this.w_CODART = this.oParentObject.w_PPARTCOM
    endcase
    if NOT EMPTY(this.w_CODART)
      this.w_cKey = cp_setAzi(i_TableProp[this.oParentObject.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.w_CODART, 1)
      cp_ShowWarn(this.w_cKey, this.oParentObject.ART_ICOL_IDX)
    endif
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
