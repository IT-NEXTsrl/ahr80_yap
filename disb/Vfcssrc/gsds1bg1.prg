* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds1bg1                                                        *
*              Stampa produzione                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRS_161][VRS_3]                                                *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-05                                                      *
* Last revis.: 2012-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR,w_MultiReportVarEnv,w_NUMREP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds1bg1",oParentObject,m.w_CURSOR,m.w_MultiReportVarEnv,m.w_NUMREP)
return(i_retval)

define class tgsds1bg1 as StdBatch
  * --- Local variables
  w_CURSOR = space(254)
  w_MultiReportVarEnv = .NULL.
  w_NUMREP = 0
  w_ZOOM = space(10)
  w_SERIAL = space(10)
  w_TEST = space(1)
  w_NUMDOC = 0
  w_DATEVA = ctod("  /  /  ")
  w_TIPDOC = space(5)
  w_CODICE = space(20)
  w_QTAMOV = 0
  w_CODART = space(20)
  w_ARTICOLO = space(20)
  w_DESART = space(40)
  w_DESCOMP = space(40)
  w_UNIMIS = space(3)
  w_UNIMISP = space(3)
  w_QTAPROD = 0
  w_MVDESSUP = space(0)
  w_MVDESSU1 = space(0)
  w_ARTICOLO = space(20)
  w_ESISAL = 0
  w_ORDSAL = 0
  w_IMPSAL = 0
  w_RISSAL = 0
  w_ROWORD = 0
  w_CODCOM = space(15)
  w_COMDES = space(100)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  * --- WorkFile variables
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di interfaccia per GSDS_BG1
     
 Create Cursor TmpElab (CODART C(20),DESART C(40),CODDIS C(20), UNIMIS C(3), ; 
 QTAPROD N(12,3),CODCOMP C(20) , DESCOMP C(40),QTAMOV N(12,3) ,UNIMISP C(3),; 
 DATEVA D(8), MVDESSUP M(10), CPROWORD N(4),CODCOM C(15), COMDES C(100),TIPCON C(1), CODCON C(15), MVSERIAL C(10))
    if NOT EMPTY(this.oParentObject.w_SERDOC)
      * --- Lancio stampa in automatico al salvataggio del documento
      if this.oParentObject.w_SERDOC = "XXXXXXXXXX"
        * --- Da ristampa documenti
        vq_exec("..\DISB\EXE\QUERY\GSDS_BRD.vqr",this,"TEMP")
      else
        vq_exec("..\DISB\EXE\QUERY\GSDS0QSP.vqr",this,"TEMP")
      endif
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Seleziono i Documenti Selezionati nello Zoom della Stampa
      this.w_ZOOM = this.oParentObject.w_ZOOMSEL
      select * from (this.w_ZOOM.cCursor) where XCHK=1 into cursor MASTER
      SELECT MASTER
      GO TOP
      SCAN
      this.oParentObject.w_SERDOC = MASTER.MVSERIAL
      if reccount("MASTER")=0
        ah_ErrorMsg("Selezionare almeno un documento",,"")
        i_retcode = 'stop'
        return
      endif
      * --- Determino le righe del dettaglio Documento.
      if this.w_NUMREP=7
        vq_exec("..\DISB\EXE\QUERY\GSDS07QSP.vqr",this,"TEMP")
      else
        vq_exec("..\DISB\EXE\QUERY\GSDS0QSP.vqr",this,"TEMP")
      endif
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT MASTER
      ENDSCAN
      if used ("MASTER")
        select MASTER
        Use
      endif
    endif
    * --- Variabili passate al report
    this.w_MultiReportVarEnv.AddVariable("L_NUMINI", this.oParentObject.w_NUMINI)     
    this.w_MultiReportVarEnv.AddVariable("L_NUMFIN", this.oParentObject.w_NUMFIN)     
    this.w_MultiReportVarEnv.AddVariable("L_DATAIN", this.oParentObject.w_DATAIN)     
    this.w_MultiReportVarEnv.AddVariable("L_DATAFI", this.oParentObject.w_DATAFI)     
    this.w_MultiReportVarEnv.AddVariable("L_DT", this.oParentObject.w_DT)     
    this.w_MultiReportVarEnv.AddVariable("L_SERIE1", this.oParentObject.w_SERIE1)     
    this.w_MultiReportVarEnv.AddVariable("L_SERIE2", this.oParentObject.w_SERIE2)     
    this.w_MultiReportVarEnv.AddVariable("L_CLIFOR", this.oParentObject.w_CLIFOR)     
    if this.w_NUMREP=1 OR this.w_NUMREP=7
      ah_Msg("Stampa componenti in corso...")
    else
      ah_Msg("Stampa fattibilitą in corso...")
    endif
    do case
      case this.w_NUMREP=1 OR this.w_NUMREP=7
        * --- Stampa Documenti di Produzione     
        select * from TmpElab ORDER BY CODCOM,CODDIS INTO CURSOR (this.w_CURSOR)
      case this.w_NUMREP=2
        * --- Verifica Fattibilitą Sommarizzata
         
 SEL="codart as codcomp,sum(qtamov) as qtamov,DATEVA,MAX(CODDIS) AS CODDIS,Max(codcon) as CODCON,Max(codcom) as CODCOM" 
 RAGGRUP="CODCOMP,DATEVA" 
 ORDINE="CODCOMP,DATEVA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
         
 ORDINE="CODCOMP,TIPPROV DESC,DATEVA" 
 select * from MOVIART order by &ORDINE INTO CURSOR (this.w_CURSOR)
      case this.w_NUMREP=3
        * --- Stampa Documenti di Produzione + Note    
        select * from TmpElab ORDER BY CODDIS, DATEVA, CPROWORD INTO CURSOR (this.w_CURSOR)
      case this.w_NUMREP=4
        * --- Stampa Fattibilitą Sommarizzata per Magazzino     
         
 
 SEL="codart as codcomp,sum(qtamov) as qtamov,DATEVA,MAX(CODDIS) AS CODDIS,Max(codcon) as CODCON,Max(codcom) as CODCOM" 
 RAGGRUP="CODCOMP,DATEVA" 
 ORDINE="CODCOMP,DATEVA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        select * from MOVIART order by ARTICOLO,TIPPROV DESC,MAGAZZINO,MVDATEVA INTO CURSOR (this.w_CURSOR)
      case this.w_NUMREP=5
        * --- Stampa Fattibilitą  per Prodotto Finito   
         
 SEL="codart as codcomp,sum(qtamov) as qtamov,DATEVA,CODDIS,Max(codcon) as CODCON,Max(codcom) as CODCOM" 
 RAGGRUP="CODDIS,CODCOMP,DATEVA" 
 ORDINE="CODDIS,CODCOMP,DATEVA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ORDINE="CODCOMP,TIPPROV DESC,DATEVA" 
 select * from MOVIART order by &ORDINE INTO CURSOR (this.w_CURSOR)
      case this.w_NUMREP=6
        * --- Stampa Fattibilitą per Prodotto Finito e Magazzino
         
 SEL="codart as codcomp,sum(qtamov) as qtamov,DATEVA,CODDIS,Max(codcon) as CODCON,Max(codcom) as CODCOM" 
 RAGGRUP="coddis,CODCOMP,DATEVA" 
 ORDINE="coddis,CODCOMP,DATEVA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
         
 ORDINE="CODDIS,ARTICOLO,TIPPROV DESC,MAGAZZINO,MVDATEVA" 
 select * from MOVIART order by &ORDINE INTO CURSOR (this.w_CURSOR)
    endcase
    if used ("Temp")
      select Temp
      Use
    endif
    if used ("Tempor")
      select Tempor
      Use
    endif
    if used ("TmpElab")
      select TmpElab
      Use
    endif
    if used ("MOVIART")
      select MOVIART
      Use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo cursore con elenco componenti e relative qta 
     
 SELECT &SEL from tmpelab group by &RAGGRUP Order By &ORDINE into cursor temp
    * --- Creo tabella temporanea utilizzata in join con elenco sotto documenti generati
    *     per fattibilitą
    CurToTab("temp","TMPVEND2")
    * --- Creo tabella temporanea con dati completi per componenti  dei prodotti finit
    *     da utilizzare come filtro nella gsdsosft e in union nella stessa
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\disb\EXE\QUERY\gsdsFsft.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\disb\EXE\QUERY\gsds0sft.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPVEND2
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND2_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ARTICOLO"
      do vq_exec with 'gsdsssft',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND2_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND2.ARTICOLO = _t2.ARTICOLO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = _t2.ESISAL";
          +",SLQTRPER = _t2.RISSAL";
      +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTIPER');
      +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTOPER');
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND2.ARTICOLO = _t2.ARTICOLO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND2.SLQTAPER = _t2.ESISAL";
          +",TMPVEND2.SLQTRPER = _t2.RISSAL";
      +",TMPVEND2.SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTIPER');
      +",TMPVEND2.SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTOPER');
          +Iif(Empty(i_ccchkf),"",",TMPVEND2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPVEND2.ARTICOLO = t2.ARTICOLO";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set (";
          +"SLQTAPER,";
          +"SLQTRPER,";
          +"SLQTIPER,";
          +"SLQTOPER";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ESISAL,";
          +"t2.RISSAL,";
          +cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTIPER')+",";
          +cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTOPER')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND2.ARTICOLO = _t2.ARTICOLO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND2 set ";
          +"SLQTAPER = _t2.ESISAL";
          +",SLQTRPER = _t2.RISSAL";
      +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTIPER');
      +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTOPER');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ARTICOLO = "+i_cQueryTable+".ARTICOLO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER = (select ESISAL from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SLQTRPER = (select RISSAL from "+i_cQueryTable+" where "+i_cWhere+")";
      +",SLQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTIPER');
      +",SLQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND2','SLQTOPER');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    vq_exec("..\disb\EXE\QUERY\gsdstsft.vqr",this,"moviart")
    * --- Drop temporary table TMPVEND2
    i_nIdx=cp_GetTableDefIdx('TMPVEND2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND2')
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziono  le Righe del Documento.
    SELECT TEMP 
    GO TOP
    SCAN
    this.w_SERIAL = TEMP.MVSERIAL
    this.w_NUMDOC = TEMP.MVNUMDOC
    this.w_TIPDOC = TEMP.MVTIPDOC
    this.w_DESART = TEMP.MVDESART
    this.w_UNIMIS = TEMP.MVUNIMIS
    this.w_QTAPROD = TEMP.QTAPROD
    this.w_CODART = NVL(temp.MVCODART,SPACE(20))
    if this.w_NUMREP=7
      this.w_CODCOM = NVL(temp.MVCODCOM,SPACE(15))
      this.w_COMDES = NVL(temp.COMDES,SPACE(100))
    endif
    this.w_MVDESSUP = TEMP.MVDESSUP
    Select Tmpelab
    go top
    if this.w_NUMREP=7
      Locate For this.w_CODCOM=CODCOM AND this.w_CODART=CODDIS and this.w_SERIAL=TmpElab.MVSERIAL
    else
      Locate For this.w_CODART=CODDIS and this.w_SERIAL=TmpElab.MVSERIAL
    endif
    if found()
      REPLACE QTAPROD with this.w_QTAPROD+QTAPROD
    endif
    * --- Ricavo i componenti della distinta associata
    do case
      case this.w_NUMREP=3
        vq_exec("..\DISB\EXE\QUERY\GSDS3QSP.vqr",this,"tempor")
      case this.w_NUMREP=7
        vq_exec("..\DISB\EXE\QUERY\GSDS7QSP.vqr",this,"tempor")
      otherwise
        vq_exec("..\DISB\EXE\QUERY\GSDS_QSP.vqr",this,"tempor")
    endcase
    Select tempor
    go top
    scan 
    this.w_CODICE = NVL(tempor.MVCODICE,SPACE(20))
    this.w_ARTICOLO = NVL(tempor.ARTICOLO,SPACE(20))
    this.w_DESCOMP = NVL(tempor.DESCOMP,SPACE(40))
    this.w_QTAMOV = NVL(tempor.MVQTAMOV,0)
    this.w_DATEVA = NVL(TEMPOR.DATEVA,cp_CharToDate("  -  -  "))
    this.w_UNIMISP = NVL(tempor.UNIMIS,"   ")
    this.w_CODCON = NVL(tempor.MVCODCON,SPACE(15))
    this.w_TIPCON = NVL(tempor.MVTIPCON,SPACE(1))
    this.w_MVDESSUP = NVL(tempor.MVDESSUP, SPACE(10))
    if this.w_NUMREP=3
      this.w_ROWORD = NVL(TEMPOR.CPROWORD, 0)
    endif
    Select Tmpelab
    go top
    if this.w_NUMREP=7
      Locate For this.w_CODCOM=TmpElab.CODCOM AND this.w_CODART=TmpElab.CODDIS AND this.w_CODICE =TmpElab.CODCOMP and this.w_SERIAL=TmpElab.MVSERIAL
    else
      Locate For this.w_CODART=TmpElab.CODDIS AND this.w_CODICE =TmpElab.CODCOMP and this.w_SERIAL=TmpElab.MVSERIAL
    endif
    if found()
      REPLACE QTAMOV with QTAMOV+this.w_QTAMOV
    else
       
 APPEND BLANK 
 REPLACE CODDIS with this.w_CODART 
 REPLACE DESART with this.w_DESART 
 REPLACE UNIMIS with this.w_UNIMIS 
 REPLACE CODCOMP with this.w_CODICE 
 REPLACE DESCOMP with this.w_DESCOMP 
 REPLACE QTAMOV with this.w_QTAMOV 
 REPLACE CODART with this.w_ARTICOLO 
 REPLACE QTAPROD with this.w_QTAPROD 
 REPLACE UNIMISP with this.w_UNIMISP 
 REPLACE DATEVA with this.w_DATEVA 
 REPLACE MVDESSUP with this.w_MVDESSUP 
 REPLACE CODCON with this.w_CODCON 
 REPLACE TIPCON with this.w_TIPCON 
 REPLACE CODCOM with this.w_CODCOM 
 REPLACE COMDES with this.w_COMDES 
 REPLACE MVSERIAL with this.w_SERIAL
      if this.w_NUMREP=3
        REPLACE CPROWORD with this.w_ROWORD
      endif
    endif
    SELECT TEMPOR
    ENDSCAN
    SELECT TEMP 
    ENDSCAN
  endproc


  proc Init(oParentObject,w_CURSOR,w_MultiReportVarEnv,w_NUMREP)
    this.w_CURSOR=w_CURSOR
    this.w_MultiReportVarEnv=w_MultiReportVarEnv
    this.w_NUMREP=w_NUMREP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPVEND1'
    this.cWorkTables[2]='*TMPVEND2'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR,w_MultiReportVarEnv,w_NUMREP"
endproc
