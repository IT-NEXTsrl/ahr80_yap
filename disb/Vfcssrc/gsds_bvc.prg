* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bvc                                                        *
*              Sostituzione cicli                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_719]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-07                                                      *
* Last revis.: 2010-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bvc",oParentObject,m.pAzione)
return(i_retval)

define class tgsds_bvc as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_nRecEla = 0
  w_OPERAZ = space(2)
  w_ERRORE = .f.
  TmpC = space(100)
  w_MESS = space(50)
  w_CODICE = space(20)
  * --- WorkFile variables
  DISMBASE_idx=0
  DISTBASE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di sostituzione cicli (GSDS_KVC)
    * --- Variabili Cursore
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione="SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione="INTERROGA"
        * --- Visualizza Zoom distinte
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
        this.oParentObject.w_SELEZI = "D"
      case this.pAzione="AG"
        * --- Controlli Preliminari
        this.w_nRecSel = 0
        this.w_nRecEla = 0
        SELECT (this.NC)
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel = 0
          this.w_MESS = "Occorre specificare almeno una distinta."
        else
          if this.oParentObject.w_TIPOOPE$"S-C" and empty(this.oParentObject.w_CICRIF)
            this.w_MESS = "Occorre specificare il ciclo di riferimento."
          else
            if this.oParentObject.w_TIPOOPE="S" and (empty(this.oParentObject.w_CICRIF) or empty(this.oParentObject.w_CICINS)) 
              this.w_MESS = "Occorre specificare il ciclo di riferimento ed il ciclo da inserire."
            endif
          endif
        endif
        if !empty(this.w_MESS)
          Ah_ErrorMsg(this.w_MESS,48,"")
        else
          * --- Legge cursore di selezione ...
          * --- Cicla sui codici selezionati ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR xChk=1
          this.w_CODICE = NVL(DBCODICE,"")
          do case
            case this.oParentObject.w_TIPOOPE="I"
              * --- Write into DISMBASE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DISMBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DBCODRIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CICINS),'DISMBASE','DBCODRIS');
                    +i_ccchkf ;
                +" where ";
                    +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
                       )
              else
                update (i_cTable) set;
                    DBCODRIS = this.oParentObject.w_CICINS;
                    &i_ccchkf. ;
                 where;
                    DBCODICE = this.w_CODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.oParentObject.w_TIPOOPE="S"
              * --- Write into DISMBASE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DISMBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DBCODRIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CICINS),'DISMBASE','DBCODRIS');
                    +i_ccchkf ;
                +" where ";
                    +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
                       )
              else
                update (i_cTable) set;
                    DBCODRIS = this.oParentObject.w_CICINS;
                    &i_ccchkf. ;
                 where;
                    DBCODICE = this.w_CODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Write into DISTBASE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DISTBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DBRIFFAS ="+cp_NullLink(cp_ToStrODBC(""),'DISTBASE','DBRIFFAS');
                    +i_ccchkf ;
                +" where ";
                    +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
                       )
              else
                update (i_cTable) set;
                    DBRIFFAS = "";
                    &i_ccchkf. ;
                 where;
                    DBCODICE = this.w_CODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            otherwise
              * --- Write into DISMBASE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DISMBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DBCODRIS ="+cp_NullLink(cp_ToStrODBC(""),'DISMBASE','DBCODRIS');
                    +i_ccchkf ;
                +" where ";
                    +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
                       )
              else
                update (i_cTable) set;
                    DBCODRIS = "";
                    &i_ccchkf. ;
                 where;
                    DBCODICE = this.w_CODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
          this.w_nRecEla = this.w_nRecEla + 1
          ENDSCAN
          Ah_ErrorMsg("Aggiornamento eseguito con successo su %1 distinte.",,"",this.w_nRecEla)
          * --- Refresh nuovo Progressivo Ordine e Zoom
          this.Padre.NotifyEvent("Interroga")     
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Errori
    * --- Incrementa numero errori
    this.w_LNumErr = this.w_LNumErr + 1
    * --- Scrive LOG
    if EMPTY(this.w_LTesMes)
      this.w_LTesMes = "Message()= "+message()
    endif
    INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
    (this.w_LNumErr, w_LOggErr, w_LErrore, this.w_LTesMes)
    this.w_LTesMes = ""
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='DISTBASE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
