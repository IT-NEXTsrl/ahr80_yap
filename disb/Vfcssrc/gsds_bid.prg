* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bid                                                        *
*              Import distinta base                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_65]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-13                                                      *
* Last revis.: 2007-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bid",oParentObject)
return(i_retval)

define class tgsds_bid as StdBatch
  * --- Local variables
  w_PPSERIAL = space(10)
  w_CODAZI = space(5)
  w_TIPCON = space(1)
  w_UNMIS1 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  ChiavePrec = space(1)
  * --- WorkFile variables
  TAB_RISO_idx=0
  TABMCICL_idx=0
  TAB_CICL_idx=0
  DISMBASE_idx=0
  DISTBASE_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  PIAMPROD_idx=0
  PIA_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla routine GSIM_BAC per l'importazione della Distinta Base
    * --- Modello di segnalazione
    * --- Modello di segnalazione
    * --- Contatore delle righe scritte
    * --- Tipo segnalazione
    * --- Dettaglio segnalazione
    * --- Numero errori riscontrati
    * --- Record corretto S/N
    * --- Sigla archivio destinazione
    * --- Codice utente
    * --- Data importazione
    * --- Chiave corrente Cicli Semplificati
    * --- Chiave corrente Distinta Base
    * --- *** Variabili per gestione rottura Piano di Produzione ***
    * --- Chiave corrente movimentazione
    * --- Ultima chiave di rottura movimentazione
    * --- Ultimo serial
    * --- Ultimo numero registrazione
    * --- Ultimo numero di riga
    * --- Messaggio a video - Elaborazione archivio ...
    * --- Serial Piano di Produzione
    this.w_PPSERIAL = space(10)
    * --- Codice Azienda
    this.w_CODAZI = i_CODAZI
    * --- Tipo conto (valorizzata a G=Generico) per controllo Conto Manutenzione
    * --- Variabili per coefficiente di impiego
    * --- Aggiornamento archivi
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento database
    do case
      case this.oParentObject.w_Destinaz $ this.oParentObject.oParentObject.w_Gestiti
        * --- Inserimento nuovi records
        * --- Try
        local bErr_03A950E0
        bErr_03A950E0=bTrsErr
        this.Try_03A950E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03A950E0
        * --- End
        * --- Aggiornamento records
        i_rows = 0
        * --- Try
        local bErr_037A72C8
        bErr_037A72C8=bTrsErr
        this.Try_037A72C8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_Corretto = .F.
        endif
        bTrsErr=bTrsErr or bErr_037A72C8
        * --- End
        * --- Se fallisce la insert (trigger failed) la procedura prosegue e poi le write non vengono fatte perch� il serial non esiste
        if i_rows=0
          this.oParentObject.w_Corretto = .F.
        endif
        if this.oParentObject.w_Corretto .and. this.oParentObject.w_ResoDett <> ah_Msgformat("<SENZA RESOCONTO>")
          * --- Scrittura resoconto
          this.oParentObject.w_ResoMode = "SCRITTURA"
          * --- Aggiornamento archivio resoconti
          this.oParentObject.Pag4()
        endif
      case .T.
        * --- Scrittura resoconto per archivi non supportati
        this.oParentObject.w_ResoMode = "NONSUPP"
        * --- Aggiornamento archivio resoconti
        this.oParentObject.Pag4()
    endcase
  endproc
  proc Try_03A950E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz = "DB"
        * --- Distinta Base
        w_DBCODICE = left( ltrim(w_DBCODICE), 20)
        if !w_DBCODICE==this.oParentObject.ULT_DBMOV
          this.ChiavePrec = this.oParentObject.w_Destinaz + this.oParentObject.ULT_DBMOV
          do GSIM_BAM with this, this.oparentobject.oparentobject.w_IMAZZERA, this.oParentObject.w_Destinaz, this.ChiavePrec , w_DBCODICE , "" , ""
          * --- Try
          local bErr_03AD4690
          bErr_03AD4690=bTrsErr
          this.Try_03AD4690()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03AD4690
          * --- End
          this.oParentObject.w_CPCHIAVE = 1
          this.oParentObject.w_CPORDINE = 10
          this.oParentObject.ULT_DBMOV = w_DBCODICE
        else
          this.oParentObject.w_CPCHIAVE = this.oParentObject.w_CPCHIAVE+1
          this.oParentObject.w_CPORDINE = this.oParentObject.w_CPORDINE+10
        endif
        * --- Insert into DISTBASE
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DBCODICE"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_DBCODICE),'DISTBASE','DBCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPCHIAVE),'DISTBASE','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DBCODICE',w_DBCODICE,'CPROWNUM',this.oParentObject.w_CPCHIAVE)
          insert into (i_cTable) (DBCODICE,CPROWNUM &i_ccchkf. );
             values (;
               w_DBCODICE;
               ,this.oParentObject.w_CPCHIAVE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "RI"
        * --- Tabella Risorse
        w_RICODICE = left( ltrim(w_RICODICE), 15)
        * --- Insert into TAB_RISO
        i_nConn=i_TableProp[this.TAB_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_RISO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RICODICE"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_RICODICE),'TAB_RISO','RICODICE');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RICODICE',w_RICODICE)
          insert into (i_cTable) (RICODICE &i_ccchkf. );
             values (;
               w_RICODICE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz = "SE"
        * --- Cicli Semplificati
        w_CSCODICE = left( ltrim(w_CSCODICE), 15)
        if !w_CSCODICE==this.oParentObject.ULT_CSMOV
          do GSIM_BAM with this, this.oparentobject.oparentobject.w_IMAZZERA, this.oParentObject.w_Destinaz, this.oParentObject.w_Destinaz + this.oParentObject.ULT_CSMOV , w_CSCODICE , "" , ""
          * --- Try
          local bErr_03AE71C0
          bErr_03AE71C0=bTrsErr
          this.Try_03AE71C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03AE71C0
          * --- End
          this.oParentObject.w_CPCHIAVE = 1
          this.oParentObject.w_CPORDINE = 10
          this.oParentObject.ULT_CSMOV = w_CSCODICE
        else
          this.oParentObject.w_CPCHIAVE = this.oParentObject.w_CPCHIAVE+1
          this.oParentObject.w_CPORDINE = this.oParentObject.w_CPORDINE+10
        endif
        * --- Insert into TAB_CICL
        i_nConn=i_TableProp[this.TAB_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TAB_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CSCODICE"+",CPROWNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(w_CSCODICE),'TAB_CICL','CSCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPCHIAVE),'TAB_CICL','CPROWNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CSCODICE',w_CSCODICE,'CPROWNUM',this.oParentObject.w_CPCHIAVE)
          insert into (i_cTable) (CSCODICE,CPROWNUM &i_ccchkf. );
             values (;
               w_CSCODICE;
               ,this.oParentObject.w_CPCHIAVE;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
    endcase
    return
  proc Try_037A72C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_Destinaz="DB"
        * --- Distinta Base
        this.oParentObject.w_ResoDett = ah_Msgformat("Distinta: %1 componente: %2", trim(w_DBCODICE), trim(w_DBCODCOM) )
        * --- Lettura dall'anagrafica della descrizione della Distinta Base
        if EMPTY(ALLTRIM(w_DBDESCRI)) AND NOT EMPTY(w_DBCODICE)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(w_DBCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = w_DBCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DBDESCRI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Write into DISMBASE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DISMBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DBDESCRI ="+cp_NullLink(cp_ToStrODBC(w_DBDESCRI),'DISMBASE','DBDESCRI');
          +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_codute),'DISMBASE','UTCC');
          +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DISMBASE','UTDC');
          +",DBDTINVA ="+cp_NullLink(cp_ToStrODBC(w_DBDTINVA),'DISMBASE','DBDTINVA');
          +",DBDTOBSO ="+cp_NullLink(cp_ToStrODBC(w_DBDTOBSO),'DISMBASE','DBDTOBSO');
          +",DBNOTAGG ="+cp_NullLink(cp_ToStrODBC(w_DBNOTAGG),'DISMBASE','DBNOTAGG');
          +",DBCODRIS ="+cp_NullLink(cp_ToStrODBC(w_DBCODRIS),'DISMBASE','DBCODRIS');
          +",DBFLSTAT ="+cp_NullLink(cp_ToStrODBC(w_DBFLSTAT),'DISMBASE','DBFLSTAT');
              +i_ccchkf ;
          +" where ";
              +"DBCODICE = "+cp_ToStrODBC(w_DBCODICE);
                 )
        else
          update (i_cTable) set;
              DBDESCRI = w_DBDESCRI;
              ,UTCC = i_codute;
              ,UTDC = SetInfoDate( g_CALUTD );
              ,DBDTINVA = w_DBDTINVA;
              ,DBDTOBSO = w_DBDTOBSO;
              ,DBNOTAGG = w_DBNOTAGG;
              ,DBCODRIS = w_DBCODRIS;
              ,DBFLSTAT = w_DBFLSTAT;
              &i_ccchkf. ;
           where;
              DBCODICE = w_DBCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Lettura dall'anagrafica della descrizione dell'articolo
        if EMPTY(w_DBDESCOM) AND NOT EMPTY(w_DBCODCOM)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(w_DBCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESART;
              from (i_cTable) where;
                  ARCODART = w_DBCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DBDESCOM = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Lettura dall'anagrafica dell'unit� di misura principale per calcolo coeff. di impiego
        if EMPTY(w_DBCOEUM1) AND NOT EMPTY(w_DBCODCOM) AND NOT EMPTY(w_DBUNIMIS) AND NOT EMPTY (w_DBQTADIS)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARUNMIS1,AROPERAT,ARMOLTIP"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(w_DBCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARUNMIS1,AROPERAT,ARMOLTIP;
              from (i_cTable) where;
                  ARCODART = w_DBCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_UNMIS1=w_DBUNIMIS
            * --- Se l'unit� di misura � uguale all'unit� di misura principale dell'articolo allora il coefficiente di impiego � uguale alla quantit� movimentata
            w_DBCOEUM1 = w_DBQTADIS
          else
            * --- altrimenti il coefficiente di impiego � ottenuto convertendo la quantit� movimentata
            do case
              case this.w_OPERAT="/"
                w_DBCOEUM1 = w_DBQTADIS*this.w_MOLTIP
              case this.w_OPERAT="*"
                w_DBCOEUM1 = w_DBQTADIS/this.w_MOLTIP
            endcase
          endif
        endif
        * --- Write into DISTBASE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPORDINE),'DISTBASE','CPROWORD');
          +",DBFLVARI ="+cp_NullLink(cp_ToStrODBC(w_DBFLVARI),'DISTBASE','DBFLVARI');
          +",DBCODCOM ="+cp_NullLink(cp_ToStrODBC(w_DBCODCOM),'DISTBASE','DBCODCOM');
          +",DBDESCOM ="+cp_NullLink(cp_ToStrODBC(w_DBDESCOM),'DISTBASE','DBDESCOM');
          +",DB__NOTE ="+cp_NullLink(cp_ToStrODBC(w_DB__NOTE),'DISTBASE','DB__NOTE');
          +",DBUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_DBUNIMIS),'DISTBASE','DBUNIMIS');
          +",DBQTADIS ="+cp_NullLink(cp_ToStrODBC(w_DBQTADIS),'DISTBASE','DBQTADIS');
          +",DBPERSCA ="+cp_NullLink(cp_ToStrODBC(w_DBPERSCA),'DISTBASE','DBPERSCA');
          +",DBRECSCA ="+cp_NullLink(cp_ToStrODBC(w_DBRECSCA),'DISTBASE','DBRECSCA');
          +",DBPERSFR ="+cp_NullLink(cp_ToStrODBC(w_DBPERSFR),'DISTBASE','DBPERSFR');
          +",DBRECSFR ="+cp_NullLink(cp_ToStrODBC(w_DBRECSFR),'DISTBASE','DBRECSFR');
          +",DBPERRIC ="+cp_NullLink(cp_ToStrODBC(w_DBPERRIC),'DISTBASE','DBPERRIC');
          +",DBDATULT ="+cp_NullLink(cp_ToStrODBC(w_DBDATULT),'DISTBASE','DBDATULT');
          +",DBFLESPL ="+cp_NullLink(cp_ToStrODBC(w_DBFLESPL),'DISTBASE','DBFLESPL');
          +",DBCOEIMP ="+cp_NullLink(cp_ToStrODBC(w_DBCOEIMP),'DISTBASE','DBCOEIMP');
          +",DBFLVARC ="+cp_NullLink(cp_ToStrODBC(w_DBFLVARC),'DISTBASE','DBFLVARC');
          +",DBINIVAL ="+cp_NullLink(cp_ToStrODBC(w_DBINIVAL),'DISTBASE','DBINIVAL');
          +",DBARTCOM ="+cp_NullLink(cp_ToStrODBC(w_DBARTCOM),'DISTBASE','DBARTCOM');
          +",DBCOEUM1 ="+cp_NullLink(cp_ToStrODBC(w_DBCOEUM1),'DISTBASE','DBCOEUM1');
          +",DBRIFFAS ="+cp_NullLink(cp_ToStrODBC(w_DBRIFFAS),'DISTBASE','DBRIFFAS');
          +",DBFINVAL ="+cp_NullLink(cp_ToStrODBC(w_DBFINVAL),'DISTBASE','DBFINVAL');
          +",DBQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_DBQTAMOV),'DISTBASE','DBQTAMOV');
              +i_ccchkf ;
          +" where ";
              +"DBCODICE = "+cp_ToStrODBC(w_DBCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPCHIAVE);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.oParentObject.w_CPORDINE;
              ,DBFLVARI = w_DBFLVARI;
              ,DBCODCOM = w_DBCODCOM;
              ,DBDESCOM = w_DBDESCOM;
              ,DB__NOTE = w_DB__NOTE;
              ,DBUNIMIS = w_DBUNIMIS;
              ,DBQTADIS = w_DBQTADIS;
              ,DBPERSCA = w_DBPERSCA;
              ,DBRECSCA = w_DBRECSCA;
              ,DBPERSFR = w_DBPERSFR;
              ,DBRECSFR = w_DBRECSFR;
              ,DBPERRIC = w_DBPERRIC;
              ,DBDATULT = w_DBDATULT;
              ,DBFLESPL = w_DBFLESPL;
              ,DBCOEIMP = w_DBCOEIMP;
              ,DBFLVARC = w_DBFLVARC;
              ,DBINIVAL = w_DBINIVAL;
              ,DBARTCOM = w_DBARTCOM;
              ,DBCOEUM1 = w_DBCOEUM1;
              ,DBRIFFAS = w_DBRIFFAS;
              ,DBFINVAL = w_DBFINVAL;
              ,DBQTAMOV = w_DBQTAMOV;
              &i_ccchkf. ;
           where;
              DBCODICE = w_DBCODICE;
              and CPROWNUM = this.oParentObject.w_CPCHIAVE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="RI"
        * --- Tabella Risorse
        this.oParentObject.w_ResoDett = trim(w_RICODICE)
        * --- Write into TAB_RISO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_RISO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_RISO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RIDESCRI ="+cp_NullLink(cp_ToStrODBC(w_RIDESCRI),'TAB_RISO','RIDESCRI');
          +",RIDESSUP ="+cp_NullLink(cp_ToStrODBC(w_RIDESSUP),'TAB_RISO','RIDESSUP');
          +",RIUNMIS1 ="+cp_NullLink(cp_ToStrODBC(w_RIUNMIS1),'TAB_RISO','RIUNMIS1');
          +",RIUNMIS2 ="+cp_NullLink(cp_ToStrODBC(w_RIUNMIS2),'TAB_RISO','RIUNMIS2');
          +",RIOPERAT ="+cp_NullLink(cp_ToStrODBC(w_RIOPERAT),'TAB_RISO','RIOPERAT');
          +",RIMOLTIP ="+cp_NullLink(cp_ToStrODBC(w_RIMOLTIP),'TAB_RISO','RIMOLTIP');
          +",RIPREZZO ="+cp_NullLink(cp_ToStrODBC(w_RIPREZZO),'TAB_RISO','RIPREZZO');
          +",RICODVAL ="+cp_NullLink(cp_ToStrODBC(w_RICODVAL),'TAB_RISO','RICODVAL');
          +",RICODREP ="+cp_NullLink(cp_ToStrODBC(w_RICODREP),'TAB_RISO','RICODREP');
              +i_ccchkf ;
          +" where ";
              +"RICODICE = "+cp_ToStrODBC(w_RICODICE);
                 )
        else
          update (i_cTable) set;
              RIDESCRI = w_RIDESCRI;
              ,RIDESSUP = w_RIDESSUP;
              ,RIUNMIS1 = w_RIUNMIS1;
              ,RIUNMIS2 = w_RIUNMIS2;
              ,RIOPERAT = w_RIOPERAT;
              ,RIMOLTIP = w_RIMOLTIP;
              ,RIPREZZO = w_RIPREZZO;
              ,RICODVAL = w_RICODVAL;
              ,RICODREP = w_RICODREP;
              &i_ccchkf. ;
           where;
              RICODICE = w_RICODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="SE"
        * --- Cicli Semplificati
        this.oParentObject.w_ResoDett = ah_Msgformat("Ciclo: %1 fase: %2", trim(w_CSCODICE), str(this.oParentObject.w_CPORDINE,4) )
        * --- Write into TABMCICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TABMCICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TABMCICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSDESCRI ="+cp_NullLink(cp_ToStrODBC(w_CSDESCRI),'TABMCICL','CSDESCRI');
              +i_ccchkf ;
          +" where ";
              +"CSCODICE = "+cp_ToStrODBC(w_CSCODICE);
                 )
        else
          update (i_cTable) set;
              CSDESCRI = w_CSDESCRI;
              &i_ccchkf. ;
           where;
              CSCODICE = w_CSCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Lettura dall'anagrafica dell'unit� di misura
        if EMPTY(w_CSUNIMIS) AND NOT EMPTY(w_CSCODRIS)
          * --- Read from TAB_RISO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TAB_RISO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TAB_RISO_idx,2],.t.,this.TAB_RISO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RIUNMIS1"+;
              " from "+i_cTable+" TAB_RISO where ";
                  +"RICODICE = "+cp_ToStrODBC(w_CSCODRIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RIUNMIS1;
              from (i_cTable) where;
                  RICODICE = w_CSCODRIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CSUNIMIS = NVL(cp_ToDate(_read_.RIUNMIS1),cp_NullValue(_read_.RIUNMIS1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Write into TAB_CICL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TAB_CICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAB_CICL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TAB_CICL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPORDINE),'TAB_CICL','CPROWORD');
          +",CSCODRIS ="+cp_NullLink(cp_ToStrODBC(w_CSCODRIS),'TAB_CICL','CSCODRIS');
          +",CSUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_CSUNIMIS),'TAB_CICL','CSUNIMIS');
          +",CSQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_CSQTAMOV),'TAB_CICL','CSQTAMOV');
          +",CSDESFAS ="+cp_NullLink(cp_ToStrODBC(w_CSDESFAS),'TAB_CICL','CSDESFAS');
          +",CS_SETUP ="+cp_NullLink(cp_ToStrODBC(w_CS_SETUP),'TAB_CICL','CS_SETUP');
              +i_ccchkf ;
          +" where ";
              +"CSCODICE = "+cp_ToStrODBC(w_CSCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPCHIAVE);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.oParentObject.w_CPORDINE;
              ,CSCODRIS = w_CSCODRIS;
              ,CSUNIMIS = w_CSUNIMIS;
              ,CSQTAMOV = w_CSQTAMOV;
              ,CSDESFAS = w_CSDESFAS;
              ,CS_SETUP = w_CS_SETUP;
              &i_ccchkf. ;
           where;
              CSCODICE = w_CSCODICE;
              and CPROWNUM = this.oParentObject.w_CPCHIAVE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="AB"
        * --- Aggiorna Articoli Distinta Base
        w_ARCODART = left( ltrim(w_ARCODART), 20)
        w_ARCODDIS = left( ltrim(w_ARCODDIS), 20)
        this.oParentObject.w_ResoDett = trim(w_ARCODART)
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(w_ARCODDIS),'ART_ICOL','ARCODDIS');
          +",ARFLCOMP ="+cp_NullLink(cp_ToStrODBC(w_ARFLCOMP),'ART_ICOL','ARFLCOMP');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARCODDIS = w_ARCODDIS;
              ,ARFLCOMP = w_ARFLCOMP;
              &i_ccchkf. ;
           where;
              ARCODART = w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_Destinaz="PR"
        if this.oParentObject.ULT_CHIMOV <> this.oParentObject.w_ChiaveRow
          * --- Nuova Registrazione
          w_PPDATREG = iif( empty(w_PPDATREG), i_datsys, w_PPDATREG )
          this.w_PPSERIAL = space(10)
          i_Conn=i_TableProp[this.PIAMPROD_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEPIAPRO", "i_codazi,w_PPSERIAL")
          * --- Salva dati progressivi
          this.oParentObject.ULT_SERI = this.w_PPSERIAL
          this.oParentObject.ULT_NREG = w_PPNUMREG
          this.oParentObject.ULT_RNUM = 1
          * --- Scrittura Master Piano di Produzione
          ah_Msg(this.oParentObject.w_MSGINFO,.T.)
          this.oParentObject.w_ResoDett = ah_Msgformat("Seriale %1 numero registrazione %2 del %3 riga %4", trim(this.w_PPSERIAL), alltrim(str(w_PPNUMREG,6,0)), dtos(w_PPDATREG), str(this.oParentObject.ULT_RNUM,4,0) )
          * --- Insert into PIAMPROD
          i_nConn=i_TableProp[this.PIAMPROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PIAMPROD_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PIAMPROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PPSERIAL"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PPSERIAL),'PIAMPROD','PPSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'PIAMPROD','UTCC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'PIAMPROD','UTDC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PPSERIAL',this.w_PPSERIAL,'UTCC',this.oParentObject.w_CreVarUte,'UTDC',this.oParentObject.w_CreVarDat)
            insert into (i_cTable) (PPSERIAL,UTCC,UTDC &i_ccchkf. );
               values (;
                 this.w_PPSERIAL;
                 ,this.oParentObject.w_CreVarUte;
                 ,this.oParentObject.w_CreVarDat;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Write into PIAMPROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PIAMPROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PIAMPROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PIAMPROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PPNUMREG ="+cp_NullLink(cp_ToStrODBC(w_PPNUMREG),'PIAMPROD','PPNUMREG');
            +",PPALFREG ="+cp_NullLink(cp_ToStrODBC(w_PPALFREG),'PIAMPROD','PPALFREG');
            +",PPDATREG ="+cp_NullLink(cp_ToStrODBC(w_PPDATREG),'PIAMPROD','PPDATREG');
            +",PPDESCRI ="+cp_NullLink(cp_ToStrODBC(w_PPDESCRI),'PIAMPROD','PPDESCRI');
            +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarUte),'PIAMPROD','UTCV');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CreVarDat),'PIAMPROD','UTDV');
            +",PPFLEVAD ="+cp_NullLink(cp_ToStrODBC(w_PPFLEVAD),'PIAMPROD','PPFLEVAD');
            +",PPDATIMP ="+cp_NullLink(cp_ToStrODBC(w_PPDATIMP),'PIAMPROD','PPDATIMP');
            +",PPDATCAR ="+cp_NullLink(cp_ToStrODBC(w_PPDATCAR),'PIAMPROD','PPDATCAR');
                +i_ccchkf ;
            +" where ";
                +"PPSERIAL = "+cp_ToStrODBC(this.w_PPSERIAL);
                   )
          else
            update (i_cTable) set;
                PPNUMREG = w_PPNUMREG;
                ,PPALFREG = w_PPALFREG;
                ,PPDATREG = w_PPDATREG;
                ,PPDESCRI = w_PPDESCRI;
                ,UTCV = this.oParentObject.w_CreVarUte;
                ,UTDV = this.oParentObject.w_CreVarDat;
                ,PPFLEVAD = w_PPFLEVAD;
                ,PPDATIMP = w_PPDATIMP;
                ,PPDATCAR = w_PPDATCAR;
                &i_ccchkf. ;
             where;
                PPSERIAL = this.w_PPSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Stessa registrazione
          this.w_PPSERIAL = this.oParentObject.ULT_SERI
          w_PPNUMREG = this.oParentObject.ULT_NREG
          this.oParentObject.w_ResoDett = ah_Msgformat("Seriale %1 numero registrazione %2 del %3 riga %4", trim(this.w_PPSERIAL), alltrim(str(w_PPNUMREG,6,0)), dtos(w_PPDATREG), str(this.oParentObject.ULT_RNUM,4,0) )
          if w_EXNUMRIG=0 .or. this.oParentObject.ULT_CHIRIG <> this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
            this.oParentObject.ULT_RNUM = this.oParentObject.ULT_RNUM + 1
          endif
        endif
        if NOT EMPTY(w_PPCODCOM)
          * --- Lettura Articolo Componente
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODART"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(w_PPCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODART;
              from (i_cTable) where;
                  CACODICE = w_PPCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PPARTCOM = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Inserimento record detail
        if w_EXNUMRIG=0 .or. this.oParentObject.ULT_CHIRIG <> this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
          w_CPROWORD = this.oParentObject.ULT_RNUM * 10
          * --- Try
          local bErr_03AABE90
          bErr_03AABE90=bTrsErr
          this.Try_03AABE90()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03AABE90
          * --- End
          * --- Scrittura riga di dettaglio
          * --- Resoconto
          this.oParentObject.w_ResoDett = ah_Msgformat("Seriale %1 numero registrazione %2 del %3 riga %4", trim(this.w_PPSERIAL), alltrim(str(w_PPNUMREG,6,0)), dtos(w_PPDATREG), str(this.oParentObject.ULT_RNUM,4,0) )
          * --- Write into PIA_PROD
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PIA_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PIA_PROD_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PIA_PROD_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PPCODCOM ="+cp_NullLink(cp_ToStrODBC(w_PPCODCOM),'PIA_PROD','PPCODCOM');
            +",PPARTCOM ="+cp_NullLink(cp_ToStrODBC(w_PPARTCOM),'PIA_PROD','PPARTCOM');
            +",PPUNIMIS ="+cp_NullLink(cp_ToStrODBC(w_PPUNIMIS),'PIA_PROD','PPUNIMIS');
            +",PPQTAMOV ="+cp_NullLink(cp_ToStrODBC(w_PPQTAMOV),'PIA_PROD','PPQTAMOV');
            +",PPQTAUM1 ="+cp_NullLink(cp_ToStrODBC(w_PPQTAUM1),'PIA_PROD','PPQTAUM1');
                +i_ccchkf ;
            +" where ";
                +"PPSERIAL = "+cp_ToStrODBC(this.w_PPSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.ULT_RNUM);
                   )
          else
            update (i_cTable) set;
                PPCODCOM = w_PPCODCOM;
                ,PPARTCOM = w_PPARTCOM;
                ,PPUNIMIS = w_PPUNIMIS;
                ,PPQTAMOV = w_PPQTAMOV;
                ,PPQTAUM1 = w_PPQTAUM1;
                &i_ccchkf. ;
             where;
                PPSERIAL = this.w_PPSERIAL;
                and CPROWNUM = this.oParentObject.ULT_RNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Memorizza la nuova chiave di rottura della primanota
        this.oParentObject.ULT_CHIMOV = this.oParentObject.w_ChiaveRow
        this.oParentObject.ULT_CHIRIG = this.oParentObject.w_ChiaveRow + str(w_EXNUMRIG,5,0)
    endcase
    return
  proc Try_03AD4690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISMBASE
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISMBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_DBCODICE),'DISMBASE','DBCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',w_DBCODICE)
      insert into (i_cTable) (DBCODICE &i_ccchkf. );
         values (;
           w_DBCODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03AE71C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TABMCICL
    i_nConn=i_TableProp[this.TABMCICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TABMCICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TABMCICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CSCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(w_CSCODICE),'TABMCICL','CSCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CSCODICE',w_CSCODICE)
      insert into (i_cTable) (CSCODICE &i_ccchkf. );
         values (;
           w_CSCODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03AABE90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PIA_PROD
    i_nConn=i_TableProp[this.PIA_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PIA_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PIA_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPSERIAL"+",CPROWNUM"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PPSERIAL),'PIA_PROD','PPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.ULT_RNUM),'PIA_PROD','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(w_CPROWORD),'PIA_PROD','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPSERIAL',this.w_PPSERIAL,'CPROWNUM',this.oParentObject.ULT_RNUM,'CPROWORD',w_CPROWORD)
      insert into (i_cTable) (PPSERIAL,CPROWNUM,CPROWORD &i_ccchkf. );
         values (;
           this.w_PPSERIAL;
           ,this.oParentObject.ULT_RNUM;
           ,w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='TAB_RISO'
    this.cWorkTables[2]='TABMCICL'
    this.cWorkTables[3]='TAB_CICL'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DISTBASE'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='PIAMPROD'
    this.cWorkTables[9]='PIA_PROD'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
