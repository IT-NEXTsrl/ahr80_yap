* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsds_bc2                                                        *
*              Calcola coefficienti di impiego                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_62]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-01                                                      *
* Last revis.: 2000-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsds_bc2",oParentObject,m.pOpz)
return(i_retval)

define class tgsds_bc2 as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_FORMUL = space(100)
  w_APPO = space(10)
  w_APPO1 = 0
  FuncCalc = space(0)
  w_VALORE = 0
  w_MESS = space(10)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Interpreta i coefficienti di Impiego provenienti da una Distinta
    Messaggio = "Corretta"
    this.w_VALORE = 0
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    if this.oParentObject.w_CHKFOR="S"
      * --- Valore Forzato
      this.w_VALORE = this.oParentObject.w_VALFOR
      this.w_oPart = this.w_oMess.AddMsgPartNL("Risultato: %1")
      this.w_oPart.AddParam(ALLTRIM(STR(this.w_VALORE,12,3))+"("+ALLTRIM(this.oParentObject.w_UNIMIS)+")")     
    else
      if this.pOpz="M"
        this.w_FORMUL = this.oParentObject.oParentObject.w_FORMUL
      else
        this.w_FORMUL = this.oParentObject.w_FORMUL
      endif
      FOR KK = 1 TO 8
      ZZ = ALLTRIM(STR(KK))
      this.w_APPO = this.oParentObject.w_PAR&ZZ
      this.w_APPO1 = this.oParentObject.w_QTPA&ZZ
      if NOT EMPTY(this.w_APPO)
        this.w_FORMUL = STRTRAN(this.w_FORMUL, "<"+ALLTRIM(this.w_APPO)+">", "("+ALLTRIM(STR(this.w_APPO1,12,3))+")")
      endif
      ENDFOR 
      * --- Valutazione del Risultato
      this.w_FORMUL = STRTRAN(this.w_FORMUL, ",", ".")
      this.w_oPart = this.w_oMess.AddMsgPartNL("Formula:%0%1")
      this.w_oPart.AddParam(this.w_FORMUL)     
      this.FuncCalc = this.w_FORMUL
      ON ERROR Messaggio = "Errata"
      TmpVal = this.FuncCalc
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      ON ERROR
      if Messaggio="Errata" OR ABS(this.w_VALORE)>99999999999999
        this.w_VALORE = 0
        this.w_oMess.AddMsgPartNL("%0Risultato: errato o incongruente")     
      else
        this.w_oPart = this.w_oMess.AddMsgPartNL("%0Risultato:%1")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_VALORE,12,3))+"("+ALLTRIM(this.oParentObject.w_UNIMIS)+")")     
      endif
    endif
    if this.pOpz="B"
      if Messaggio="Errata" OR this.w_VALORE=0
        this.w_oMess.AddMsgPartNL("Ignoro il coefficiente di impiego dai calcoli?")     
        if this.w_oMess.ah_YesNo()
          this.oParentObject.w_OKCONF = "S"
          this.oParentObject.w_QTAMOL = 1
        endif
      else
        this.oParentObject.w_OKCONF = "S"
        this.oParentObject.w_QTAMOL = this.w_VALORE
      endif
    else
      this.w_oMess.Ah_ErrorMsg()     
    endif
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
