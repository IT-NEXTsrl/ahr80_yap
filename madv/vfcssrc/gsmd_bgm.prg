* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bgm                                                        *
*              Gestione matricole                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_445]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-09                                                      *
* Last revis.: 2016-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pCLASSE,pARFLCOMM,pFLAUTO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bgm",oParentObject,m.pTipo,m.pCLASSE,m.pARFLCOMM,m.pFLAUTO)
return(i_retval)

define class tgsmd_bgm as StdBatch
  * --- Local variables
  pTipo = space(10)
  pCLASSE = space(5)
  pARFLCOMM = space(1)
  pFLAUTO = space(1)
  w_LODATSCA = ctod("  /  /  ")
  w_CODICE = space(40)
  w_CHIAVE = space(10)
  w_NUMROW = 0
  w_NUMRIF = space(4)
  w_SECODUBI = space(20)
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_AGGRIGA = .f.
  w_FILTROLIKE = space(41)
  w_MESS = space(100)
  w_MTKEYSAL = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_ESIRIS = space(1)
  w_UNIMIS1 = space(3)
  w_CODMAG = space(5)
  w_QTAUM1 = 0
  w_FLLOTT = space(1)
  w_MGUBIC = space(1)
  w_MGUBICAR = space(1)
  w_MGUBISCA = space(1)
  w_CODFOR = space(15)
  w_DATREG = ctod("  /  /  ")
  w_MTMAGCAR = space(5)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_MTFLSCAR = space(1)
  w_PUNPAD = .NULL.
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_MAGUBI = space(5)
  w_CODMAT = space(40)
  w_AM_PRODU = 0
  w_CLASSE = space(5)
  w_FLAUTO = space(1)
  w_MAGCAR = space(5)
  w_MAGSCA = space(5)
  w_MAGUBI1 = space(5)
  w_MGUBICA2 = space(1)
  w_QTACA1 = 0
  w_QTASC1 = 0
  w_FLORI = space(1)
  w_AMKEYSAL = space(20)
  w_NUMMATRICOLA = 0
  w_LCODLOT = space(20)
  w_LCODUBI = space(20)
  w_LCODMAT = space(40)
  w_LSERRIF = space(10)
  w_LROWRIF = 0
  w_LRIFNUM = 0
  w_LLOTTES = space(20)
  w_LUBITES = space(20)
  w_LSTALOT = space(1)
  w_LCODMAG = space(5)
  w_SCRIS = space(1)
  w_MTSERIAL = space(10)
  w_MTROWNUM = 0
  w_MTNUMRIF = 0
  w_CODCOM = space(15)
  w_ARFLCOMM = space(1)
  w_LCODCOM = space(15)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  w_CODAZI = space(5)
  w_SCAMAT = space(1)
  w_FLGSCA = 0
  w_NOESMAT = .f.
  w_UNIMAT = space(1)
  w_CLAMAT = space(5)
  w_MLCODICE = space(20)
  w_LOCODICE = space(20)
  w_LOCODUBI = space(20)
  w_FLCASC = space(1)
  w_OKSCA = space(1)
  w_OLD_MTCODLOT = space(20)
  w_OLD_CODUBI = space(20)
  w_SALDO = 0
  w_PADRE = .NULL.
  w_FLCARI = space(1)
  * --- WorkFile variables
  MOVIMATR_idx=0
  MATRICOL_idx=0
  LOTTIART_idx=0
  ART_ICOL_idx=0
  MAGAZZIN_idx=0
  CMT_MAST_idx=0
  SALLOTUBI_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Zoom Matricole (Zoom - lancia lo zoom , Changed al changed della variabile w_MTCODMAT
    * --- Variabili lette dagli Zoom o Utilizzate nel Caricamento Rapido
    * --- Utilizzato per filtro stile zoom con la Like
    * --- Per Consentire il passaggio dei dati dalla Movimentazione GSVE_MMT alla
    *     Maschera di Caricamento rapido GSMD_KCR dichiariamo locali le variabili
    *     da passare ed effettuiamo l'assegnamento alle variabili del padre. 
    this.w_PUNPAD = this.oParentObject
    this.w_PUNPAD.MarkPos()     
    this.w_MTKEYSAL = this.w_PUNPAD.w_MTKEYSAL
    this.w_CODART = this.w_PUNPAD.w_CODART
    if vartype(this.pCLASSE)="C" and not empty(this.pCLASSE)
      this.w_CLASSE = this.pCLASSE
      this.w_ARFLCOMM = this.pARFLCOMM
      this.w_FLAUTO = this.pFLAUTO
    else
      * --- Leggo Classe Matricola 
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCLAMAT,ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCLAMAT,ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLASSE = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
        this.w_ARFLCOMM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Tipo Aggiornamento (Automatico, Manuale)
      * --- Read from CMT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CMT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLAUTO"+;
          " from "+i_cTable+" CMT_MAST where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CLASSE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLAUTO;
          from (i_cTable) where;
              CMCODICE = this.w_CLASSE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLAUTO = NVL(cp_ToDate(_read_.CMFLAUTO),cp_NullValue(_read_.CMFLAUTO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Tipo Aggiornamento (Automatico, Manuale)
    this.w_CODAZI = i_CODAZI
    if vartype(g_AZSCAMAT)="C"
      this.w_SCAMAT = g_AZSCAMAT
    else
      public g_AZSCAMAT
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSCAMAT"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSCAMAT;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SCAMAT = NVL(cp_ToDate(_read_.AZSCAMAT),cp_NullValue(_read_.AZSCAMAT))
        g_AZSCAMAT = NVL(cp_ToDate(_read_.AZSCAMAT),cp_NullValue(_read_.AZSCAMAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_FLGSCA = IIF(this.w_SCAMAT="S",-1,0)
    this.w_DESART = this.w_PUNPAD.w_DESART
    this.w_ESIRIS = this.w_PUNPAD.w_ESIRIS
    if upper(this.w_PUNPAD.Class)="TGSAR_MPG"
      * --- Da Caricamento Rapido Penna Ottica
      this.w_CODMAG = this.w_PUNPAD.w_GPCODMAG
      this.w_UNIMIS1 = this.w_PUNPAD.w_UNMIS1
      this.w_MGUBIC = this.w_PUNPAD.w_FLUBIC
      this.w_SERRIF = this.w_PUNPAD.w_GPSERRIF
      this.w_ROWRIF = this.w_PUNPAD.w_GPROWRIF
      this.w_FILTROLIKE = IIF( Not Empty( this.w_PUNPAD.w_GPCODMAT ), Rtrim( this.w_PUNPAD.w_GPCODMAT)+"%","")
      this.w_FLORI = IIF(NOT EMPTY(this.w_CODMAG),"N","S")
      if this.pTipo="CHANGED" OR this.pTipo="DICHCHANGED" OR this.pTipo="ZOOM" OR this.pTipo="DICHZOOM" or this.pTipo="CHGZOOM"
        this.w_LCODLOT = this.w_PUNPAD.w_GPCODLOT
        this.w_LCODUBI = this.w_PUNPAD.w_GPCODUBI
        this.w_LLOTTES = this.w_PUNPAD.w_GPCODLOT
        this.w_LUBITES = this.w_PUNPAD.w_GPCODUBI
        this.w_LCODMAT = this.w_PUNPAD.w_GPCODMAT
        this.w_LSTALOT = this.w_PUNPAD.w_FLSTAT
      endif
      * --- consente la visualizzazione e quindi  scarico (GSMD_ZMS) immediato del riservato nel caricamento rapido
      *     per eseguire un successiva importazione 
      this.w_SCRIS = "R"
    else
      this.w_CODMAG = this.w_PUNPAD.w_CODMAG
      this.w_UNIMIS1 = this.w_PUNPAD.w_UNIMIS1
      this.w_QTAUM1 = this.w_PUNPAD.w_QTAUM1 - IIF( Type("This.oParentObject.w_TOTALE")="U",0 , this.w_PUNPAD.w_TOTALE)
      this.w_MGUBIC = this.w_PUNPAD.w_MGUBIC
      this.w_CODFOR = this.w_PUNPAD.w_CODFOR
      this.w_SERRIF = this.w_PUNPAD.w_SERRIF
      this.w_ROWRIF = this.w_PUNPAD.w_ROWRIF
      this.w_FLORI = "N"
      this.w_FILTROLIKE = IIF( Not Empty( this.w_PUNPAD.w_MTCODMAT ), Rtrim( this.w_PUNPAD.w_MTCODMAT)+"%","")
      if this.pTipo="CHANGED" OR this.pTipo="DICHCHANGED" OR this.pTipo="ZOOM" OR this.pTipo="DICHZOOM" or this.pTipo="CHGZOOM"
        this.w_LCODLOT = this.w_PUNPAD.w_MTCODLOT
        this.w_LCODUBI = this.w_PUNPAD.w_MTCODUBI
        this.w_LCODMAT = this.w_PUNPAD.w_MTCODMAT
        this.w_LSERRIF = this.w_PUNPAD.w_MTSERRIF
        this.w_LROWRIF = this.w_PUNPAD.w_MTROWRIF
        this.w_LRIFNUM = this.w_PUNPAD.w_MTRIFNUM
        this.w_LLOTTES = this.w_PUNPAD.w_CODLOTTES
        this.w_LUBITES = this.w_PUNPAD.w_CODUBITES
        this.w_LSTALOT = this.w_PUNPAD.w_STALOT
        this.w_LCODCOM = this.w_PUNPAD.w_MTCODCOM
        this.w_MTSERIAL = this.w_PUNPAD.w_MTSERIAL
        this.w_MTROWNUM = this.w_PUNPAD.w_MTROWNUM
        this.w_MTNUMRIF = this.w_PUNPAD.w_MTNUMRIF
        * --- Eseguo controllo modifica record matricola gi� salvato attraverso lo zoom
        * --- Read from MOVIMATR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MTSERIAL"+;
            " from "+i_cTable+" MOVIMATR where ";
                +"MTSERIAL = "+cp_ToStrODBC(this.w_MTSERIAL);
                +" and MTROWNUM = "+cp_ToStrODBC(this.w_MTROWNUM);
                +" and MTNUMRIF = "+cp_ToStrODBC(this.w_MTNUMRIF);
                +" and MTKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
                +" and MTCODMAT = "+cp_ToStrODBC(this.w_LCODMAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MTSERIAL;
            from (i_cTable) where;
                MTSERIAL = this.w_MTSERIAL;
                and MTROWNUM = this.w_MTROWNUM;
                and MTNUMRIF = this.w_MTNUMRIF;
                and MTKEYSAL = this.w_MTKEYSAL;
                and MTCODMAT = this.w_LCODMAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MTSERIAL = NVL(cp_ToDate(_read_.MTSERIAL),cp_NullValue(_read_.MTSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS<>0
          ah_ErrorMsg("Matricola gi� salvata impossibile variare","!","")
          i_retcode = 'stop'
          return
        endif
      endif
      * --- In questa casisitca tale assegnamento rende ininfluente il relativo 
      *     filtro nella GSMD_ZMS
      this.w_SCRIS = "N"
    endif
    this.w_QTACA1 = this.w_QTAUM1
    this.w_QTASC1 = 0
    this.w_FLLOTT = this.w_PUNPAD.w_FLLOTT
    this.w_DATREG = this.w_PUNPAD.w_DATREG
    this.w_MTMAGCAR = this.w_PUNPAD.w_MTMAGCAR
    this.w_MAGUBI = this.w_PUNPAD.w_MAGUBI
    if type("this.oParentObject.w_MAGSCA")="C" and not empty(this.w_PUNPAD.w_MAGSCA) and upper(this.w_PUNPAD.Class)<>"TGSMD_KCR"
      * --- Magazzino Scarti delle dichiarazioni di produzione
      this.w_MAGCAR = this.w_PUNPAD.w_MAGCAR
      this.w_MAGSCA = this.w_PUNPAD.w_MAGSCA
      this.w_MAGUBI1 = this.w_PUNPAD.w_MAGUBI1
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGSCA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_MAGSCA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MGUBICA2 = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MTMAGCAR = iif(empty(this.w_MAGCAR), this.w_MAGSCA, this.w_MAGCAR)
      this.w_QTACA1 = this.w_PUNPAD.w_QTACA1 - this.w_PUNPAD.w_TOTCAR
      this.w_QTASC1 = this.w_PUNPAD.w_QTASC1 - this.w_PUNPAD.w_TOTSCA
    endif
    if Not Empty(this.w_MTMAGCAR)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MTMAGCAR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_MTMAGCAR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MGUBICAR = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MTMAGSCA = this.w_PUNPAD.w_MTMAGSCA
    if Not Empty(this.w_MTMAGSCA)
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_MTMAGSCA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_MTMAGSCA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MGUBISCA = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MTFLCARI = this.w_PUNPAD.w_MTFLCARI
    this.w_MTFLSCAR = this.w_PUNPAD.w_MTFLSCAR
    this.w_AGGRIGA = .F.
    do case
      case this.pTipo="ZOOM" OR this.pTipo="DICHZOOM" or this.pTipo="CHGZOOM"
        this.w_MTMAGCAR = this.w_PUNPAD.w_MTMAGCAR
        this.w_SECODUBI = this.w_LUBITES
        * --- La seconda condizione per contemplare il caso di trasferimenti sul
        *     solito magazzino
        if this.w_ARFLCOMM<>"S"
          this.w_COMSCA = SPACE(15)
        else
          this.w_COMSCA = this.w_PUNPAD.w_COMSCA
        endif
        this.w_COMCAR = this.w_PUNPAD.w_COMCAR
        if this.pTipo="ZOOM"
          this.w_FILTROLIKE = Space(40)
        endif
        if this.pTipo="DICHZOOM" OR this.w_CODMAG=this.w_MTMAGCAR And this.w_MTMAGCAR<>this.w_MTMAGSCA
          * --- Carico
          this.w_AM_PRODU = iif(this.pTipo="DICHZOOM", 1, 0) 
          vx_exec("..\MADV\EXE\QUERY\GSMD_ZMC.VZM",this)
        else
          * --- Scarico
          vx_exec("..\MADV\EXE\QUERY\GSMD_ZMS.VZM",this)
        endif
        if Not Empty( this.w_CODICE )
          this.w_LCODMAT = this.w_CODICE
          this.w_LSERRIF = Nvl( this.w_CHIAVE , Space(10) )
          this.w_LROWRIF = Nvl ( this.w_NUMROW ,0 )
          this.w_LRIFNUM = Nvl ( this.w_NUMRIF , 0 )
          this.w_LCODMAG = Nvl ( this.w_MTMAGCAR , 0 )
          this.w_LCODCOM = Nvl ( this.w_CODCOM , space(15) )
          * --- Se non primo movimento di carico attivo il meccanismo di storno
          this.oParentObject.w_MT__FLAG = IIF( Empty( this.w_LSERRIF ) , " " , "+" )
          * --- Se gestisco i lotti
          if g_PERLOT="S" 
            if (this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )) AND upper(this.w_PUNPAD.Class)<>"TGSAR_MPG"
              this.w_LCODLOT = this.w_LLOTTES
            else
              this.w_LCODLOT = Nvl ( this.w_CODLOT , Space(20) )
            endif
          endif
          * --- Se gestisco le ubicazioni
          if g_PERUBI="S" 
            * --- Se movimento l'esitenza ed ho un carico (trasferimento) imposto come ubicazione quella di testata
            if upper(this.w_PUNPAD.Class)="TGSAR_MPG"
              this.w_LCODUBI = Nvl ( this.w_CODUBI , Space(20))
            else
              if this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )
                this.w_LCODUBI = this.w_LUBITES
              else
                this.w_LCODUBI = iif( this.w_CODMAG=this.w_MAGUBI , Nvl ( this.w_CODUBI , Space(20) ) , Space(20) )
              endif
            endif
          endif
          if this.pTipo="DICHZOOM"
            * --- Da Dichiarazioni di produzione
            this.w_LCODLOT = this.w_LLOTTES
            this.w_LCODUBI = iif(this.w_MAGCAR=this.w_MTMAGCAR, this.w_LUBITES, this.w_LCODUBI)
          endif
          * --- Leggo Articolo e Variante lotto per non far fallire il check
          if Not Empty( this.w_LCODLOT )
            * --- Read from LOTTIART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LOFLSTAT"+;
                " from "+i_cTable+" LOTTIART where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_LCODLOT);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LOFLSTAT;
                from (i_cTable) where;
                    LOCODICE = this.w_LCODLOT;
                    and LOCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LSTALOT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AGGRIGA = .T.
        else
          * --- Se non sceglie niente sbianco il campo (se matricola non esistente)
          *     Nel caso l'utente preme F9 su una matricola gi� valorizzata e non
          *     seleziona niente
          if Not Empty( this.w_LCODMAT )
            * --- Se la trovo non la cambio se non la trovo la sbianca
            * --- Read from MATRICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MATRICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AMCODICE"+;
                " from "+i_cTable+" MATRICOL where ";
                    +"AMKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
                    +" and AMCODICE = "+cp_ToStrODBC(this.w_LCODMAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AMCODICE;
                from (i_cTable) where;
                    AMKEYSAL = this.w_MTKEYSAL;
                    and AMCODICE = this.w_LCODMAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LCODMAT = NVL(cp_ToDate(_read_.AMCODICE),cp_NullValue(_read_.AMCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      case this.pTipo="CHANGED" OR this.pTipo="DICHCHANGED"
        * --- Azzero parametro per consentire cmq la digitazione della matricola gi� scaricata
        this.w_FLGSCA = 0
        * --- Verifico la correttezza della matricola immessa
        this.w_NUMMATRICOLA = 0
        this.w_CODMAT = this.w_LCODMAT
        if this.w_ARFLCOMM<>"S"
          this.w_COMSCA = SPACE(15)
        else
          this.w_COMSCA = this.w_PUNPAD.w_COMSCA
        endif
        this.w_COMCAR = this.w_PUNPAD.w_COMCAR
        if (this.w_CODMAG=this.w_MTMAGCAR And this.w_MTMAGCAR<>this.w_MTMAGSCA) OR this.pTipo="DICHCHANGED"
          * --- Cerco un carico scaricabile, se esiste non posso effettuare lo scarico
          this.w_AM_PRODU = iif(this.pTipo="DICHCHANGED", 1, 0) 
          * --- Select from GSMD_ZMC
          do vq_exec with 'GSMD_ZMC',this,'_Curs_GSMD_ZMC','',.f.,.t.
          if used('_Curs_GSMD_ZMC')
            select _Curs_GSMD_ZMC
            locate for 1=1
            do while not(eof())
            this.w_NUMMATRICOLA = this.w_NUMMATRICOLA + 1
            if this.w_NUMMATRICOLA=2
              * --- Esco dal ciclo l'utente ha selezionato un prefisso che identifica pi� di una matricola
              Exit
            else
              this.w_LCODMAT = _Curs_GSMD_ZMC.CODICE
              this.w_LSERRIF = Nvl ( _Curs_GSMD_ZMC.CHIAVE , Space(10) )
              this.w_LROWRIF = Nvl ( _Curs_GSMD_ZMC.NUMROW , 0 )
              this.w_LRIFNUM = Nvl ( _Curs_GSMD_ZMC.NUMRIF , 0 )
              this.w_LCODCOM = Nvl ( _Curs_GSMD_ZMC.CODCOM , space(15) )
              * --- Se matricola movimenata la prima volta non ho riferimenti
              this.oParentObject.w_MT__FLAG = iif( Empty( this.w_LSERRIF )," ", "+")
              * --- Imposto Lotto e ubicazione dalla testata se movimento l'esistenza
              * --- Se gestisco i lotti
              if g_PERLOT="S" 
                if this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )
                  this.w_LCODLOT = this.w_LLOTTES
                else
                  this.w_LCODLOT = Nvl ( _Curs_GSMD_ZMC.CODLOT , Space(20) )
                endif
              endif
              * --- Se gestisco le ubicazioni
              if g_PERUBI="S" AND this.w_MGUBIC="S" 
                if upper(this.w_PUNPAD.Class)="TGSAR_MPG"
                  this.w_LCODUBI = Nvl (_Curs_GSMD_ZMC.CODUBI, Space(20) )
                else
                  if this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )
                    this.w_LCODUBI = this.w_LUBITES
                  else
                    this.w_LCODUBI = iif( this.w_CODMAG=this.w_MAGUBI , Nvl ( _Curs_GSMD_ZMC.CODUBI , Space(20) ) , Space(20) )
                  endif
                endif
              endif
            endif
              select _Curs_GSMD_ZMC
              continue
            enddo
            use
          endif
        else
          * --- Cerco di leggere un carico scaricabile (w_MT_SALDO=0) se non esiste matricola non valida
          * --- Select from GSMD_ZMS
          do vq_exec with 'GSMD_ZMS',this,'_Curs_GSMD_ZMS','',.f.,.t.
          if used('_Curs_GSMD_ZMS')
            select _Curs_GSMD_ZMS
            locate for 1=1
            do while not(eof())
            this.w_NUMMATRICOLA = this.w_NUMMATRICOLA + 1
            if this.w_NUMMATRICOLA=2
              * --- Esco dal ciclo l'utente ha selezionato un prefisso che identifica pi� di una matricola
              Exit
            else
              this.w_AMKEYSAL = _Curs_GSMD_ZMS.AMKEYSAL
              this.w_LCODMAT = _Curs_GSMD_ZMS.CODICE
              this.w_LSERRIF = _Curs_GSMD_ZMS.CHIAVE
              this.w_LROWRIF = _Curs_GSMD_ZMS.NUMROW
              this.w_LRIFNUM = _Curs_GSMD_ZMS.NUMRIF
              this.w_LCODCOM = Nvl ( _Curs_GSMD_ZMS.CODCOM , space(15) )
              this.w_LCODMAG = _Curs_GSMD_ZMS.MTMAGCAR
              this.w_FLCARI = _Curs_GSMD_ZMS.FLCARI
              this.oParentObject.w_MT__FLAG = "+"
              * --- Imposto Lotto e ubicazione dalla testata se movimento l'esistenza
              * --- Se gestisco i lotti
              if g_PERLOT="S" 
                if (this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )) AND upper(this.w_PUNPAD.Class)<>"TGSAR_MPG"
                  this.w_LCODLOT = this.w_LLOTTES
                else
                  this.w_LCODLOT = Nvl ( _Curs_GSMD_ZMS.CODLOT , Space(20) )
                endif
              endif
              * --- Se gestisco le ubicazioni
              if g_PERUBI="S" 
                if upper(this.w_PUNPAD.Class)="TGSAR_MPG"
                  this.w_LCODUBI = Nvl (_Curs_GSMD_ZMS.CODUBI, Space(20) )
                else
                  if this.w_MGUBIC="S" 
                    if this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0 And ( this.w_CODMAG=this.w_MTMAGCAR Or (Not Empty(this.w_MTMAGCAR) And Not Empty(this.w_MTMAGSCA)) )
                      this.w_LCODUBI = this.w_LUBITES
                    else
                      this.w_LCODUBI = IIF( this.w_CODMAG=this.w_MAGUBI , Nvl (_Curs_GSMD_ZMS.CODUBI, Space(20) ) , Space(20) )
                    endif
                  endif
                endif
              endif
            endif
              select _Curs_GSMD_ZMS
              continue
            enddo
            use
          endif
        endif
        if this.w_NUMMATRICOLA>1 AND this.oParentObject.w_NOMSG
          * --- Ho pi� di una matricola buona lancio lo zoom per selezionarla
          *     Rimetto nella Matricola il valore digitato in origine dall'utente
          this.w_LCODMAT = this.w_CODMAT
          if this.pTipo="DICHCHANGED"
            GSMD_BGM(This.oParentObject , "DICHZOOM" )
          else
            GSMD_BGM(This.oParentObject , "CHGZOOM" )
          endif
          i_retcode = 'stop'
          return
        else
          this.w_AGGRIGA = ( this.w_NUMMATRICOLA=1 )
          if this.w_AGGRIGA
            * --- Leggo Articolo lotto per non far fallire il check
            if Not Empty( this.w_LCODLOT )
              * --- Read from LOTTIART
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LOTTIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LOFLSTAT"+;
                  " from "+i_cTable+" LOTTIART where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_LCODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LOFLSTAT;
                  from (i_cTable) where;
                      LOCODICE = this.w_LCODLOT;
                      and LOCODART = this.w_CODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LSTALOT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- Se matricola non valida e non vuota
          if Not this.w_AGGRIGA And Not Empty( this.w_LCODMAT )
            * --- Matricola inesistente, se st� caricando allora chiedo se
            *     eseguire il caricamento della matricola!
            if this.w_CODMAG=this.w_MTMAGCAR And this.w_ESIRIS="E" And this.oParentObject.w_MT_SALDO=0
              * --- Verifico se la matricola immessa � gia presente
              if g_UNIMAT="A"
                * --- Read from MATRICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.MATRICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "*"+;
                    " from "+i_cTable+" MATRICOL where ";
                        +"AMKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
                        +" and AMCODICE = "+cp_ToStrODBC(this.w_LCODMAT);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    *;
                    from (i_cTable) where;
                        AMKEYSAL = this.w_MTKEYSAL;
                        and AMCODICE = this.w_LCODMAT;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_NOESMAT = i_Rows=0
              else
                * --- E' possibile che l'utente si attardi a confermare il messaggio sotto
                *     e per cui si riesca a generare due matricole identiche sebbene vietato.
                *     
                *     Se si vuole evitare occorre introdudre una transazione con all'interno
                *     ripetuta la UNIVOCMAT e la INSERT INTO MOVIMATR
                * --- Leggo il flag sulla classe
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARCLAMAT"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARCLAMAT;
                    from (i_cTable) where;
                        ARCODART = this.w_CODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from CMT_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CMT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CMUNIMAT"+;
                    " from "+i_cTable+" CMT_MAST where ";
                        +"CMCODICE = "+cp_ToStrODBC(this.w_CLAMAT);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CMUNIMAT;
                    from (i_cTable) where;
                        CMCODICE = this.w_CLAMAT;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_UNIMAT = NVL(cp_ToDate(_read_.CMUNIMAT),cp_NullValue(_read_.CMUNIMAT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_NOESMAT = Not UNIVOMAT( this.w_LCODMAT , this.w_UNIMAT , this.w_MTKEYSAL, this.w_CLAMAT )
              endif
              this.w_AGGRIGA = .T.
              * --- Se la matricola non esiste chiedo se posso crearla
              if this.w_NOESMAT and Not Empty ( this.w_MTKEYSAL )
                if this.oParentObject.w_NOMSG
                  if ah_YesNo("La matricola %1 non esiste, la si vuole caricare come nuova matricola?","", alltrim(this.w_LCODMAT))
                    * --- Leggo la classe dall'articolo
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARCLAMAT"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARCLAMAT;
                        from (i_cTable) where;
                            ARCODART = this.w_CODART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_CLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Insert into MATRICOL
                    i_nConn=i_TableProp[this.MATRICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MATRICOL_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"AMKEYSAL"+",AMCODICE"+",AMCODART"+",AMCLAMAT"+",AMDATCRE"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_MTKEYSAL),'MATRICOL','AMKEYSAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODMAT),'MATRICOL','AMCODICE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'MATRICOL','AMCODART');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CLAMAT),'MATRICOL','AMCLAMAT');
                      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MATRICOL','AMDATCRE');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'AMKEYSAL',this.w_MTKEYSAL,'AMCODICE',this.w_LCODMAT,'AMCODART',this.w_CODART,'AMCLAMAT',this.w_CLAMAT,'AMDATCRE',i_DATSYS)
                      insert into (i_cTable) (AMKEYSAL,AMCODICE,AMCODART,AMCLAMAT,AMDATCRE &i_ccchkf. );
                         values (;
                           this.w_MTKEYSAL;
                           ,this.w_LCODMAT;
                           ,this.w_CODART;
                           ,this.w_CLAMAT;
                           ,i_DATSYS;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error='Errore inserimento matricola'
                      return
                    endif
                    * --- Metto lotto e ubicazioni di Default + Variabili per evitare
                    *     fallimento check
                    this.w_LCODLOT = this.w_LLOTTES
                    this.w_LCODUBI = this.w_LUBITES
                  else
                    this.w_LCODMAT = Space(40)
                  endif
                else
                  this.w_LCODMAT = Space(40)
                endif
              else
                this.w_LCODMAT = Space(40)
                if this.oParentObject.w_NOMSG
                  ah_ErrorMsg("Matricola incongruente, non esistente, non associata all'articolo di riga o gi� presente","!","")
                endif
              endif
            else
              this.w_LCODMAT = Space(40)
              this.w_AGGRIGA = .T.
              if this.oParentObject.w_NOMSG
                ah_ErrorMsg("Matricola incongruente, non esistente, non associata all'articolo di riga o gi� presente","!","")
              endif
            endif
          endif
        endif
      case this.pTipo="LOTTIZOOM" Or this.pTipo="LOTTIZOOM_FILTRO" OR this.pTipo="LOTTIZOOM_VALORE" OR this.pTipo="LOTTITES"
        this.w_FLCASC = IIF( Not Empty( this.w_PUNPAD.w_MTFLSCAR) , "-" , "+")
        this.w_SERRIF = SPACE(10)
        this.w_ROWRIF = 0
        this.w_MLCODICE = Space ( 20 )
        this.w_LOCODICE = SPACE(20)
        this.w_LOCODUBI = SPACE(20)
        this.w_LODATSCA = cp_CharToDate("  -  -  ")
        this.w_OKSCA = "T"
        if this.w_FLCASC = "+"
          * --- Carica il Lotto
          vx_exec("..\MADV\EXE\QUERY\GSMD_QLC.VZM",this)
        else
          * --- Scarica il Lotto
          vx_exec("..\MADV\EXE\QUERY\GSMD_QLO.VZM",this)
        endif
        if NOT EMPTY(NVL(this.w_LOCODICE," "))
          * --- Due Zoom Lotto in testata oppure lotto di riga
          if this.pTipo="LOTTIZOOM_FILTRO" OR this.pTipo="LOTTIZOOM_VALORE"
            this.w_OLD_MTCODLOT = this.oParentObject.w_MTCODLOT
            this.w_OLD_CODUBI = this.oparentobect.w_CODUBI
          endif
          if this.pTipo="LOTTITES"
            this.oParentObject.w_ARTLOTTES = Space(20)
            this.oParentObject.w_CODLOTTES = this.w_LOCODICE
            * --- Read from LOTTIART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LOCODART,LOFLSTAT"+;
                " from "+i_cTable+" LOTTIART where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_LOCODICE);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LOCODART,LOFLSTAT;
                from (i_cTable) where;
                    LOCODICE = this.w_LOCODICE;
                    and LOCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_ARTLOTTES = NVL(cp_ToDate(_read_.LOCODART),cp_NullValue(_read_.LOCODART))
              this.oParentObject.w_STALOT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.oParentObject.w_STALOT = SPACE(20)
            if this.pTipo="LOTTIZOOM"
              this.oParentObject.w_MTCODLOT = this.w_LOCODICE
            endif
            * --- Read from LOTTIART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LOFLSTAT"+;
                " from "+i_cTable+" LOTTIART where ";
                    +"LOCODICE = "+cp_ToStrODBC(this.w_LOCODICE);
                    +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LOFLSTAT;
                from (i_cTable) where;
                    LOCODICE = this.w_LOCODICE;
                    and LOCODART = this.w_CODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_STALOT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
            this.oParentObject.w_LOTZOOM = .T.
          endif
          if this.pTipo="LOTTIZOOM"
            * --- Notifica Riga Variata
            SELECT (this.oParentObject.cTrsName)
            if I_SRV=" " AND NOT DELETED()
              REPLACE i_SRV WITH "U"
            endif
          endif
          if NOT EMPTY(NVL(this.w_LODATSCA, cp_CharToDate("  -  -  "))) AND this.w_LODATSCA<this.w_DATREG
            ah_ErrorMsg("Il lotto selezionato � scaduto",,"")
          endif
        else
          * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
          this.oParentObject.w_LOTZOOM = .F.
        endif
        * --- Zoom da inserimento rapido (GSMD_KCR). O dal campo lotto utilizzato come filtro oppure da quello utilizzato per aggiornare il valore
        if NOT EMPTY(this.w_LOCODICE) and (this.pTipo="LOTTIZOOM_FILTRO" OR this.pTipo="LOTTIZOOM_VALORE")
          if this.pTipo="LOTTIZOOM_FILTRO"
            This.oParentObject.w_SECODLOT=this.w_LOCODICE
            if g_PERUBI="S" AND this.w_MGUBIC="S" AND this.w_CODMAG=this.w_MTMAGSCA
              This.oParentObject.w_SECODUBI=this.w_LOCODUBI
            else
              This.oParentObject.w_SECODUBI=Space(20)
            endif
            * --- Rimetto i vecchi valori nei campi valorizzati dalla zoom in automatico
            this.oParentObject.w_MTCODLOT = this.w_OLD_MTCODLOT
            This.oParentObject.w_CODUBI=this.w_OLD_CODUBI
          else
            this.oParentObject.w_MTCODLOT = this.w_LOCODICE
            * --- Prendo la matricola dallo zoom se il magazino di carico  � uguale
            *     al magazzino passato alla query (w_CODMAG)
            if g_PERUBI="S" AND this.w_MGUBICAR="S" AND this.w_CODMAG=this.w_MTMAGCAR
              This.oParentObject.w_CODUBI=this.w_LOCODUBI
            else
              This.oParentObject.w_CODUBI=Space(20)
            endif
          endif
        endif
      case (this.pTipo="DELETEROW" Or this.pTipo="UPDATEROW" Or this.pTipo="INSERTROW") And Not bTrsErr
        * --- Check cancellazione/ modifica Riga
        if this.oParentObject.w_MT_SALDO<>0 And ( this.pTipo="DELETEROW" Or this.pTipo="UPDATEROW" )
          Local L_Area 
 L_area=Select() 
 SELECT (this.oParentObject.cTrsName)
          this.w_MESS = ah_Msgformat("Impossibile eliminare / modificare matricola. (%1) dell'articolo %2 [ riga %3] perch� movimento gi� stornato", ALLTRIM(MTCODMAT), alltrim(Left(MTKEYSAL,20)), Alltrim(Str( this.oParentObject.w_ROWORD )) )
          Select ( L_Area )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        else
          if this.pTipo="DELETEROW"
            ah_Msg("Elimino matricola %1",.T.,.F.,.F., this.oParentObject.w_MTCODMAT)
          endif
        endif
        * --- Verifico all'inserimento che la matricola non venga utilizzata in contemporanea
        *     da un altro utente.
        *     Due casi
        *     a) La matricola � stata movimentata in precedenza (MTRIFNUM � pieno)
        *     b) Primo carico
        if this.pTipo="INSERTROW"
          this.w_SALDO = -1
          if Not Empty( this.oParentObject.w_MTRIFNUM ) OR Not Empty( this.oParentObject.w_MTRIFSTO )
            * --- Leggo in MOVIMATR il valore del saldo se a uno tutto OK - Evento scatenato dopo l'Update da saldo
            *     automatico
            if Not Empty( this.oParentObject.w_MTRIFNUM ) 
              * --- Verifico Presenza Ultimo carico nei saldi Lotti
              * --- Read from MOVIMATR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MOVIMATR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MT_SALDO"+;
                  " from "+i_cTable+" MOVIMATR where ";
                      +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MTSERRIF);
                      +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MTROWRIF);
                      +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MTRIFNUM);
                      +" and MTKEYSAL = "+cp_ToStrODBC(this.w_MTKEYSAL);
                      +" and MTCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MT_SALDO;
                  from (i_cTable) where;
                      MTSERIAL = this.oParentObject.w_MTSERRIF;
                      and MTROWNUM = this.oParentObject.w_MTROWRIF;
                      and MTNUMRIF = this.oParentObject.w_MTRIFNUM;
                      and MTKEYSAL = this.w_MTKEYSAL;
                      and MTCODMAT = this.oParentObject.w_MTCODMAT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALDO = NVL(cp_ToDate(_read_.MT_SALDO),cp_NullValue(_read_.MT_SALDO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Read from SALLOTUBI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2],.t.,this.SALLOTUBI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SU_SALDO"+;
                  " from "+i_cTable+" SALLOTUBI where ";
                      +"SUSERRIF = "+cp_ToStrODBC(this.oParentObject.w_MTSERRIF);
                      +" and SUROWRIF = "+cp_ToStrODBC(this.oParentObject.w_MTROWRIF);
                      +" and SUNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MTRIFSTO);
                      +" and SUCODICE = "+cp_ToStrODBC(this.w_MTKEYSAL);
                      +" and SUCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SU_SALDO;
                  from (i_cTable) where;
                      SUSERRIF = this.oParentObject.w_MTSERRIF;
                      and SUROWRIF = this.oParentObject.w_MTROWRIF;
                      and SUNUMRIF = this.oParentObject.w_MTRIFSTO;
                      and SUCODICE = this.w_MTKEYSAL;
                      and SUCODMAT = this.oParentObject.w_MTCODMAT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALDO = NVL(cp_ToDate(_read_.SU_SALDO),cp_NullValue(_read_.SU_SALDO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          else
            * --- Nel caso di primo carico verifico la presenza della matricola in MOVIMATR
            * --- Select from MOVIMATR
            i_nConn=i_TableProp[this.MOVIMATR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" MOVIMATR ";
                  +" where MTCODMAT= "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT)+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+"";
                   ,"_Curs_MOVIMATR")
            else
              select Count(*) As Conta from (i_cTable);
               where MTCODMAT= this.oParentObject.w_MTCODMAT and MTKEYSAL=this.w_MTKEYSAL;
                into cursor _Curs_MOVIMATR
            endif
            if used('_Curs_MOVIMATR')
              select _Curs_MOVIMATR
              locate for 1=1
              do while not(eof())
              this.w_SALDO = Nvl ( _Curs_MOVIMATR.CONTA , 0 )
                select _Curs_MOVIMATR
                continue
              enddo
              use
            endif
            if this.w_SALDO=0
              * --- Nel caso di primo carico verifico la presenza della matricola in SALLOTUBI
              * --- Select from SALLOTUBI
              i_nConn=i_TableProp[this.SALLOTUBI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALLOTUBI_idx,2],.t.,this.SALLOTUBI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" SALLOTUBI ";
                    +" where SUCODMAT= "+cp_ToStrODBC(this.oParentObject.w_MTCODMAT)+" and SUCODICE="+cp_ToStrODBC(this.w_MTKEYSAL)+"";
                     ,"_Curs_SALLOTUBI")
              else
                select Count(*) As Conta from (i_cTable);
                 where SUCODMAT= this.oParentObject.w_MTCODMAT and SUCODICE=this.w_MTKEYSAL;
                  into cursor _Curs_SALLOTUBI
              endif
              if used('_Curs_SALLOTUBI')
                select _Curs_SALLOTUBI
                locate for 1=1
                do while not(eof())
                this.w_SALDO = Nvl ( _Curs_SALLOTUBI.CONTA , 0 )
                  select _Curs_SALLOTUBI
                  continue
                enddo
                use
              endif
            endif
            * --- Se non entro nella SELECT allora matricola gia movimentata
            this.w_SALDO = iif( this.w_SALDO=0, 1 , 0)
          endif
          if this.w_SALDO<>1
            Local L_Area 
 L_area=Select() 
 SELECT (this.oParentObject.cTrsName)
            this.w_MESS = AH_Msgformat("Impossibile utilizzare matricola. (%1) dell'articolo %2 [riga %3]. Gi� utilizzata", ALLTRIM(MTCODMAT), Alltrim(Left(this.w_MTKEYSAL,20)), Alltrim(Str( this.oParentObject.w_ROWORD )) )
            Select ( L_Area )
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          else
            ah_Msg("Inserimento matricola %1",.T.,.F.,.F., this.oParentObject.w_MTCODMAT)
          endif
        endif
      case this.pTipo="CARRAPIDO" OR this.pTipo="DICHCARRAPIDO"
        this.w_SECODUBI = this.oParentObject.w_CODUBITES
        this.w_CODLOT = this.oParentObject.w_CODLOTTES
        this.w_CODUBI = this.w_PUNPAD.w_CODUBI
        this.w_CODICE = this.w_PUNPAD.w_CODICE
        this.w_NUMRIF = this.w_PUNPAD.w_NUMRIF
        this.w_MAGUBI = this.w_PUNPAD.w_MAGUBI
        if this.w_ARFLCOMM<>"S"
          this.w_COMSCA = SPACE(15)
        else
          this.w_COMSCA = this.w_PUNPAD.w_COMSCA
        endif
        this.w_COMCAR = this.w_PUNPAD.w_COMCAR
        this.w_AM_PRODU = iif(this.pTipo="DICHCARRAPIDO", 1, 0) 
        do GSMD_KCR with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="CARMAT" OR this.pTIPO="DICHCARMAT"
        * --- --Chiudo la movimentazione Movimenti Matricola (se dall'inserimento rapido ho selezionato tutte le matricole che mi servono)
        if this.pTipo="DICHCARMAT"
          this.w_QTACA1 = this.w_PUNPAD.w_QTACA1
          this.w_QTASC1 = this.w_PUNPAD.w_QTASC1
          this.w_MAGCAR = this.w_PUNPAD.w_MAGCAR
          this.w_MAGSCA = this.w_PUNPAD.w_MAGSCA
        endif
        if (_TALLY>0)
          this.w_PADRE = this.oParentObject
          this.w_PADRE.MarkPos()     
          SELECT (this.w_PADRE.cTrsName)
          * --- MI sposto al termine del transitorio
          Go Bottom
          do while not eof ("Carica")
            * --- Verifico se la matricola � gi� presente nel dettaglio
            Local L_Posizione,L_Trovato 
 Select (this.oParentObject.cTrsName) 
 L_Posizione=IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) ) 
 LOCATE FOR t_MTCODMAT = carica.MtCodMat And Not Deleted() 
 L_Trovato=Found() 
 
 * mi riposiziono nella riga di partenza 
 Select (this.oParentObject.cTrsName) 
 Go L_Posizione 
 
 Select Carica
            if Not L_Trovato
              SELECT (this.oParentObject.cTrsName)
              if not(Empty(t_MTCODMAT))
                * --- Se non sono su una riga di Append, scrivo nuova riga
                this.oParentObject.InitRow()
              else
                this.oParentObject.WorkFromTrs
              endif
              Select Carica
              this.oParentObject.w_MTCODMAT = MTCODMAT
              ah_Msg("Inserimento matricola %1",.T.,.F.,.F., this.oParentObject.w_MTCODMAT)
              this.oParentObject.w_MTCODLOT = Nvl( CODLOT , Space(20) )
              this.oParentObject.w_STALOT = SPACE(20)
              if not Empty(this.oParentObject.w_MTCODLOT)
                * --- Read from LOTTIART
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "LOFLSTAT,LOCODART"+;
                    " from "+i_cTable+" LOTTIART where ";
                        +"LOCODICE = "+cp_ToStrODBC(this.oParentObject.w_MTCODLOT);
                        +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    LOFLSTAT,LOCODART;
                    from (i_cTable) where;
                        LOCODICE = this.oParentObject.w_MTCODLOT;
                        and LOCODART = this.w_CODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_STALOT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
                  this.oParentObject.w_ARTLOT = NVL(cp_ToDate(_read_.LOCODART),cp_NullValue(_read_.LOCODART))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.oParentObject.w_MTCODCOM = NVL(CODCOM, iif(this.w_MTFLCARI="E",this.w_PADRE.w_COMCAR,iif(this.w_MTFLSCAR="E",this.w_PADRE.w_COMSCA,space(15))))
              this.oParentObject.w_MTCODUBI = iif(upper(this.w_PUNPAD.Class)="TCGSAC_MCM",nvl(CODUBI,""),this.w_PUNPAD.w_CODUBI)
              this.oParentObject.w_MTSERRIF = nvl(CHIAVE, Space(10))
              this.oParentObject.w_MTROWRIF = nvl(NUMROW,0)
              this.oParentObject.w_MTRIFNUM = nvl(NUMRIF, 0)
              this.oParentObject.w_MTRIFSTO = nvl(NUMRIF, 0)
              this.oParentObject.w_MT__FLAG = IIF( Empty( this.oParentObject.w_MTSERRIF ) , " " , "+" )
              * --- MT_SALDO eventualmente impostato ad 1 nel gsve_mmt Write in MTRIFNUM
              this.oParentObject.w_MT_SALDO = 0
              this.oParentObject.w_RIGA = 1
              if this.pTIPO="DICHCARMAT"
                * --- Dichiarazioni di Produzione
                if empty(NVL(MTMAGCAR, "")) or nvl(MTMAGCAR,"")=this.w_MAGCAR AND this.oParentObject.w_TOTCAR<this.w_QTACA1
                  if not empty(this.w_MAGCAR)
                    this.w_PUNPAD.w_MTMAGCAR = this.w_MAGCAR
                  endif
                  this.oParentObject.w_RIGACAR = 1
                  this.oParentObject.w_TOTCAR = this.oParentObject.w_TOTCAR+1
                endif
                if not empty(NVL(MTMAGCAR, "")) and MTMAGCAR=this.w_MAGSCA and this.oParentObject.w_TOTSCA<this.w_QTASC1
                  this.w_PUNPAD.w_MTMAGCAR = this.w_MAGSCA
                  this.oParentObject.w_RIGASCA = 1
                  this.oParentObject.w_TOTSCA = this.oParentObject.w_TOTSCA+1
                endif
              endif
              this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + 1
              SELECT (this.oParentObject.cTrsName)
              this.oParentObject.TrsFromWork()
            else
              ah_ErrorMsg("La matricola � gi� stata utilizzata nel dettaglio matricole (%1)",,"", Alltrim(carica.mtcodmat) )
            endif
            Select Carica
            if Not Eof("Carica")
              Skip
            endif
          enddo
          Wait Clear
          * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
          this.w_PADRE.RePos(.F.)     
        endif
        if this.oParentObject.w_QTAUM1 < this.oParentObject.w_TOTALE
          this.w_PUNPAD.EcpSave()     
        endif
    endcase
    if this.pTipo="CHANGED" OR this.pTipo="DICHCHANGED" OR this.pTipo="ZOOM" OR this.pTipo="DICHZOOM" or this.pTipo="CHGZOOM"
      * --- Ripristino valore nelle variabili caller
      if upper(this.w_PUNPAD.Class)="TGSAR_MPG"
        if g_UNIMAT="M" 
          * --- Se univocit� per matricola assegno codice articolo ricavandolo dall'Anagrafica matricole
          if NOT EMPTY(this.w_LCODMAT)
            * --- Read from MATRICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MATRICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2],.t.,this.MATRICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AMKEYSAL"+;
                " from "+i_cTable+" MATRICOL where ";
                    +"AMCODICE = "+cp_ToStrODBC(this.w_LCODMAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AMKEYSAL;
                from (i_cTable) where;
                    AMCODICE = this.w_LCODMAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_GPCODICE = NVL(cp_ToDate(_read_.AMKEYSAL),cp_NullValue(_read_.AMKEYSAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          if EMPTY(this.oParentObject.w_GPCODICE)
            this.oParentObject.w_GPCODICE = this.w_AMKEYSAL
          endif
        endif
        this.oParentObject.w_GPQTAMOV = 1
        this.oParentObject.w_GPCODLOT = this.w_LCODLOT
        this.oParentObject.w_GPCODUBI = this.w_LCODUBI
        this.oParentObject.w_GPCODMAT = this.w_LCODMAT
        if NOT EMPTY(this.w_LCODMAG)
          this.oParentObject.w_GPCODMAG = this.w_LCODMAG
          this.oParentObject.w_COMAG = this.w_LCODMAG
          this.oParentObject.w_OMAG = this.w_LCODMAG
          this.oParentObject.w_MAGPRE = this.w_LCODMAG
          this.oParentObject.w_MAGTER = this.w_LCODMAG
        endif
        this.oParentObject.w_FLSTAT = this.w_LSTALOT
        this.oParentObject.w_FLAGRIS = this.w_FLCARI
      else
        this.oParentObject.w_MTCODLOT = this.w_LCODLOT
        this.oParentObject.w_MTCODUBI = this.w_LCODUBI
        this.oParentObject.w_MTCODMAT = this.w_LCODMAT
        this.oParentObject.w_STALOT = this.w_LSTALOT
        this.oParentObject.w_MTCODCOM = this.w_LCODCOM
      endif
      this.oParentObject.w_MTSERRIF = this.w_LSERRIF
      this.oParentObject.w_MTROWRIF = this.w_LROWRIF
      this.oParentObject.w_MTRIFNUM = this.w_LRIFNUM
      this.oParentObject.w_MTRIFSTO = this.w_LRIFNUM
    endif
    if this.w_AGGRIGA and this.oParentObject.w_MOVMAT
      * --- Notifica Riga Variata
      SELECT (this.w_PUNPAD.cTrsName)
      if I_SRV=" " AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
  endproc


  proc Init(oParentObject,pTipo,pCLASSE,pARFLCOMM,pFLAUTO)
    this.pTipo=pTipo
    this.pCLASSE=pCLASSE
    this.pARFLCOMM=pARFLCOMM
    this.pFLAUTO=pFLAUTO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='MOVIMATR'
    this.cWorkTables[2]='MATRICOL'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='CMT_MAST'
    this.cWorkTables[7]='SALLOTUBI'
    this.cWorkTables[8]='AZIENDA'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_GSMD_ZMC')
      use in _Curs_GSMD_ZMC
    endif
    if used('_Curs_GSMD_ZMS')
      use in _Curs_GSMD_ZMS
    endif
    if used('_Curs_MOVIMATR')
      use in _Curs_MOVIMATR
    endif
    if used('_Curs_SALLOTUBI')
      use in _Curs_SALLOTUBI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pCLASSE,pARFLCOMM,pFLAUTO"
endproc
