* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bsl                                                        *
*              Visualizzazione schede lotti/ubicazioni                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-26                                                      *
* Last revis.: 2002-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bsl",oParentObject,m.pPROV)
return(i_retval)

define class tgsmd_bsl as StdBatch
  * --- Local variables
  pPROV = space(1)
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Eventi da Visualizzazione Schede Lotti/Ubicazioni (da GSMD_SZL, GSMD_SZU)
    do case
      case this.pPROV="L"
        this.w_ZOOM = this.oParentObject.w_ZoomSlo
        if EMPTY(this.oParentObject.w_LOTTO)
          ah_ErrorMsg("Inserire un codice lotto",,"")
          i_retcode = 'stop'
          return
        endif
        * --- w_FLARSTO='S' Considera anche i Movimenti Storicizzati
        if this.oParentObject.w_FLARSTO = "S"
          this.w_ZOOM.cCpQueryName = "..\MADV\EXE\QUERY\GSMDSSZL"
        else
          this.w_ZOOM.cCpQueryName = "..\MADV\EXE\QUERY\GSMD_SZL"
        endif
      case this.pPROV="U"
        this.w_ZOOM = this.oParentObject.w_ZoomSub
        if EMPTY(this.oParentObject.w_CODUBI)
          ah_ErrorMsg("Inserire un codice ubicazione",,"")
          i_retcode = 'stop'
          return
        endif
        * --- w_FLARSTO='S' Considera anche i Movimenti Storicizzati
        if this.oParentObject.w_FLARSTO = "S"
          this.w_ZOOM.cCpQueryName = "..\MADV\EXE\QUERY\GSMDSSZU"
        else
          this.w_ZOOM.cCpQueryName = "..\MADV\EXE\QUERY\GSMD_SZU"
        endif
    endcase
    NC = this.w_Zoom.cCursor
    This.OparentObject.NotifyEvent("Esegui")
    this.oParentObject.w_TOTCAR = 0
    this.oParentObject.w_TOTSCA = 0
    * --- Legge Totali Carichi, Scarichi
    SELECT &NC
    GO TOP
    SUM QTACAR, QTASCA TO this.oParentObject.w_TOTCAR, this.oParentObject.w_TOTSCA
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
