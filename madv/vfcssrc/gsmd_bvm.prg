* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bvm                                                        *
*              Aggiornamento differito lotti/ubicazioni                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_166]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2016-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bvm",oParentObject)
return(i_retval)

define class tgsmd_bvm as StdBatch
  * --- Local variables
  w_LFLVEAC = space(1)
  w_LCLADOC = space(2)
  w_SERIAL = space(10)
  w_MESS = space(10)
  w_CODODL = space(15)
  w_TROV = .f.
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  w_MVCODART = space(20)
  w_MVDESART = space(40)
  w_MVUNIMIS = space(3)
  w_MVQTAIMP = 0
  w_MVQTAIM1 = 0
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2RISE = space(1)
  w_MVFLUBIC = space(1)
  w_MVFLUBI2 = space(1)
  w_MVARTLOT = space(1)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVRIFRIG = 0
  w_MVTIPRIG = space(1)
  w_MVCAUMAG = space(5)
  w_MVCATCON = space(5)
  w_MVCONTRA = space(15)
  w_MVDESSUP = space(0)
  w_MVINICOM = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVCODLIS = space(5)
  w_MVCODCLA = space(3)
  w_MVPREZZO = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVIMPACC = 0
  w_MVIMPSCO = 0
  w_MVIMPNAZ = 0
  w_MVVALMAG = 0
  w_MVVALRIG = 0
  w_MVPESNET = 0
  w_MVFLRAGG = space(1)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVCODIVA = space(5)
  w_MVCONIND = space(15)
  w_MVNOMENC = space(8)
  w_MVFLTRAS = space(1)
  w_MVMOLSUP = 0
  w_MVCAUCOL = space(5)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  w_MVCODODL = space(15)
  w_MVF2ORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLELGM = space(1)
  w_MVFLIMPE = space(1)
  w_MVKEYSAL = space(20)
  w_MVTIPATT = space(1)
  w_MVRIGMAT = 0
  w_MVVOCCEN = space(15)
  w_CONFERMA = .f.
  w_DATDOC = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CAUDOC = space(5)
  w_DESDOC = space(40)
  w_OLDROW = 0
  w_OLDART = space(20)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_DATREG = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_MVFLELAN = space(1)
  w_MV_SEGNO = space(1)
  w_ROWMAT = 0
  w_CODVAL = space(3)
  w_DECTOT = 0
  w_MVTIPPRO = space(2)
  w_MVTIPPR2 = space(2)
  w_MVPERPRO = 0
  w_MVPROCAP = 0
  w_MVIMPPRO = 0
  w_MVCONTRO = space(15)
  w_MVLOTMAG = space(5)
  w_MVLOTMAT = space(5)
  w_AGGRIGSAL = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_FLARIF = space(1)
  w_ROWLOT = 0
  w_SERDOCORI = space(15)
  w_ROWMATORI = 0
  w_ROWORDORI = 0
  w_CODMAGORI = space(3)
  * --- WorkFile variables
  AGG_LOTT_idx=0
  MVM_MAST_idx=0
  DOC_MAST_idx=0
  CAM_AGAZ_idx=0
  TIP_DOCU_idx=0
  DOC_DETT_idx=0
  MVM_DETT_idx=0
  SPL_LOTT_idx=0
  AGG_MATR_idx=0
  MOVIMATR_idx=0
  VALUTE_idx=0
  LOTTIART_idx=0
  AVA_LOT_idx=0
  MAT_PROD_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSMA_SZM)
    this.w_LFLVEAC = this.oParentObject.w_FLVEAC
    this.w_LCLADOC = this.oParentObject.w_CLADOC
    if EMPTY(this.oParentObject.w_SERIALE)
      this.w_MESS = "Selezionare un documento"
      ah_ErrorMsg(this.w_MESS,,"")
      i_retcode = 'stop'
      return
    endif
    this.w_SERIAL = this.oParentObject.w_SERIALE
    this.w_DATDOC = cp_CharToDate("  -  -  ")
    this.w_DATREG = cp_CharToDate("  -  -  ")
    this.w_NUMDOC = 0
    this.w_ALFDOC = Space(10)
    this.w_CAUDOC = SPACE(5)
    this.w_DESDOC = SPACE(40)
    this.w_TIPCON = " "
    this.w_CODCON = SPACE(15)
    if this.oParentObject.w_CLADOC="MM"
      vq_exec("..\MADV\EXE\QUERY\GSMD_QMM.VQR", this,"AGGIORNA")
      * --- Read from MVM_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MVM_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MMDATREG,MMNUMREG,MMTCAMAG,MMTIPCON,MMCODCON,MMCODVAL"+;
          " from "+i_cTable+" MVM_MAST where ";
              +"MMSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MMDATREG,MMNUMREG,MMTCAMAG,MMTIPCON,MMCODCON,MMCODVAL;
          from (i_cTable) where;
              MMSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATDOC = NVL(cp_ToDate(_read_.MMDATREG),cp_NullValue(_read_.MMDATREG))
        this.w_NUMDOC = NVL(cp_ToDate(_read_.MMNUMREG),cp_NullValue(_read_.MMNUMREG))
        this.w_CAUDOC = NVL(cp_ToDate(_read_.MMTCAMAG),cp_NullValue(_read_.MMTCAMAG))
        this.w_TIPCON = NVL(cp_ToDate(_read_.MMTIPCON),cp_NullValue(_read_.MMTIPCON))
        this.w_CODCON = NVL(cp_ToDate(_read_.MMCODCON),cp_NullValue(_read_.MMCODCON))
        this.w_CODVAL = NVL(cp_ToDate(_read_.MMCODVAL),cp_NullValue(_read_.MMCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMDESCRI"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CAUDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMDESCRI;
          from (i_cTable) where;
              CMCODICE = this.w_CAUDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESDOC = NVL(cp_ToDate(_read_.CMDESCRI),cp_NullValue(_read_.CMDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DATREG = this.w_DATDOC
    else
      vq_exec("..\MADV\EXE\QUERY\GSMD_QDO.VQR", this,"AGGIORNA")
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON,MVCODVAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON,MVCODVAL;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_CAUDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
        this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_CODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDDESDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDDESDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_CAUDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESDOC = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_TROV = .F.
    if USED("AGGIORNA")
      * --- Azzerro Archivi di Appoggio
      * --- Delete from SPL_LOTT
      i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              SPSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione SPL_LOTT'
        return
      endif
      * --- Delete from AGG_LOTT
      i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione AGG_LOTT'
        return
      endif
      * --- Delete from AGG_MATR
      i_nConn=i_TableProp[this.AGG_MATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGG_MATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              MTSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione AGG_MATR'
        return
      endif
      * --- Inserisco Dati in Archivi di Appoggio
      AddMsgNL("FASE 1: inserimento dati...",this)
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0
        this.w_CPROWNUM = CPROWNUM
        this.w_MVNUMRIF = MVNUMRIF
        this.w_CPROWORD = CPROWORD
        this.w_MVCODICE = NVL(MVCODICE," ")
        this.w_MVDESART = NVL(MVDESART," ")
        this.w_MVUNIMIS = NVL(MVUNIMIS," ")
        this.w_MVQTAMOV = NVL(MVQTAMOV, 0)
        this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
        this.w_MVCODMAG = NVL(MVCODMAG," ")
        this.w_MVCODMAT = NVL(MVCODMAT," ")
        this.w_MVCODLOT = NVL(MVCODLOT," ")
        this.w_MVCODUBI = NVL(MVCODUBI," ")
        this.w_MVCODUB2 = NVL(MVCODUB2," ")
        this.w_MVFLCASC = NVL(MVFLCASC," ")
        this.w_MVFLRISE = NVL(MVFLRISE," ")
        this.w_MVF2CASC = NVL(MVF2CASC," ")
        this.w_MVF2RISE = NVL(MVF2RISE," ")
        this.w_MVFLUBIC = NVL(FLUBIC, " ")
        this.w_MVFLUBI2 = NVL(FLUBI2, " ")
        this.w_MVARTLOT = NVL(FLLOTT, " ")
        this.w_MVCODART = NVL(MVCODART, " ")
        this.w_MVSERRIF = NVL(MVSERRIF, " ")
        this.w_MVROWRIF = NVL(MVROWRIF, 0)
        this.w_MVFLLOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBIC="S"), LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " ")
        this.w_MVF2LOTT = IIF((g_PERLOT="S" AND this.w_MVARTLOT$ "SC") OR (g_PERUBI="S" AND this.w_MVFLUBI2="S"), LEFT(ALLTRIM(this.w_MVF2CASC)+IIF(this.w_MVF2RISE="+", "-", IIF(this.w_MVF2RISE="-", "+", " ")), 1), " ")
        this.w_MVFLELAN = NVL(MVFLELAN," ")
        this.w_MV_SEGNO = NVL(MV_SEGNO," ")
        this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
        if NOT EMPTY(this.w_MVCODICE) AND this.w_MVQTAUM1>0
          * --- Try
          local bErr_048D5458
          bErr_048D5458=bTrsErr
          this.Try_048D5458()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_048D5458
          * --- End
        endif
        SELECT AGGIORNA
        ENDSCAN
      endif
      if this.w_TROV
        * --- Lancia la Manutenzione
        this.w_CONFERMA = .F.
        if (g_PERLOT="S" OR g_PERUBI="S")
          do GSMD_MLU with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CONFERMA
            * --- Try
            local bErr_0306F028
            bErr_0306F028=bTrsErr
            this.Try_0306F028()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg(i_ErrMsg,,"")
            endif
            bTrsErr=bTrsErr or bErr_0306F028
            * --- End
          endif
          * --- Chiude i Cursori che non servono piu'
          if USED("AGGIORNA")
            SELECT AGGIORNA
            USE
          endif
        else
          do GSMD_MAT with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AddMsgNL("FASE 2: aggiornamento matricole...",this)
          * --- Aggoiungo righe matricole alle righe documento aggiornate
          if this.w_CONFERMA
            * --- Try
            local bErr_0306E968
            bErr_0306E968=bTrsErr
            this.Try_0306E968()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg(i_ErrMsg,,"")
            endif
            bTrsErr=bTrsErr or bErr_0306E968
            * --- End
          endif
        endif
        * --- Ripulisce le tabelle temporanee
        * --- Delete from SPL_LOTT
        i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                SPSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione SPL_LOTT'
          return
        endif
        * --- Delete from AGG_LOTT
        i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione AGG_LOTT'
          return
        endif
        * --- Delete from AGG_MATR
        i_nConn=i_TableProp[this.AGG_MATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGG_MATR_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MTSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore in cancellazione AGG_MATR'
          return
        endif
      endif
      if USED("AGGIORNA")
        SELECT AGGIORNA
        USE
      endif
    endif
    if this.w_CONFERMA
      This.OparentObject.NotifyEvent("Esegui")
    endif
    This.oparentobject.oPgFrm.ActivePage=2
  endproc
  proc Try_048D5458()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into AGG_LOTT
    i_nConn=i_TableProp[this.AGG_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AGG_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AGG_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODMAG"+",MVCODMAT"+",MVCODLOT"+",MVCODUBI"+",MVCODUB2"+",MVFLLOTT"+",MVF2LOTT"+",MVFLUBIC"+",MVFLUBI2"+",MVARTLOT"+",MVCODART"+",MVSERRIF"+",MVROWRIF"+",MVFLCASC"+",MVFLRISE"+",MVF2CASC"+",MVF2RISE"+",MVCODCOM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'AGG_LOTT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'AGG_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'AGG_LOTT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'AGG_LOTT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'AGG_LOTT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'AGG_LOTT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'AGG_LOTT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'AGG_LOTT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'AGG_LOTT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'AGG_LOTT','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'AGG_LOTT','MVCODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AGG_LOTT','MVCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'AGG_LOTT','MVCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'AGG_LOTT','MVCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'AGG_LOTT','MVFLLOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'AGG_LOTT','MVF2LOTT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBIC),'AGG_LOTT','MVFLUBIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLUBI2),'AGG_LOTT','MVFLUBI2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVARTLOT),'AGG_LOTT','MVARTLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'AGG_LOTT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'AGG_LOTT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'AGG_LOTT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'AGG_LOTT','MVFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'AGG_LOTT','MVFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'AGG_LOTT','MVF2CASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'AGG_LOTT','MVF2RISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'AGG_LOTT','MVCODCOM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_SERIALE,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_CPROWORD,'MVCODICE',this.w_MVCODICE,'MVDESART',this.w_MVDESART,'MVUNIMIS',this.w_MVUNIMIS,'MVQTAMOV',this.w_MVQTAMOV,'MVQTAUM1',this.w_MVQTAUM1,'MVCODMAG',this.w_MVCODMAG,'MVCODMAT',this.w_MVCODMAT,'MVCODLOT',this.w_MVCODLOT)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVCODICE,MVDESART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODMAG,MVCODMAT,MVCODLOT,MVCODUBI,MVCODUB2,MVFLLOTT,MVF2LOTT,MVFLUBIC,MVFLUBI2,MVARTLOT,MVCODART,MVSERRIF,MVROWRIF,MVFLCASC,MVFLRISE,MVF2CASC,MVF2RISE,MVCODCOM &i_ccchkf. );
         values (;
           this.oParentObject.w_SERIALE;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.w_CPROWORD;
           ,this.w_MVCODICE;
           ,this.w_MVDESART;
           ,this.w_MVUNIMIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVCODMAG;
           ,this.w_MVCODMAT;
           ,this.w_MVCODLOT;
           ,this.w_MVCODUBI;
           ,this.w_MVCODUB2;
           ,this.w_MVFLLOTT;
           ,this.w_MVF2LOTT;
           ,this.w_MVFLUBIC;
           ,this.w_MVFLUBI2;
           ,this.w_MVARTLOT;
           ,this.w_MVCODART;
           ,this.w_MVSERRIF;
           ,this.w_MVROWRIF;
           ,this.w_MVFLCASC;
           ,this.w_MVFLRISE;
           ,this.w_MVF2CASC;
           ,this.w_MVF2RISE;
           ,this.w_MVCODCOM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento AGG_LOTT'
      return
    endif
    * --- Precarico primo record da splittare
    * --- Insert into SPL_LOTT
    i_nConn=i_TableProp[this.SPL_LOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SPL_LOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SPL_LOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SPSERIAL"+",SPROWORD"+",CPROWNUM"+",SPCODLOT"+",SPCODUBI"+",SPCODUB2"+",SPQTAMOV"+",SPQTAUM1"+",SPNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'SPL_LOTT','SPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SPL_LOTT','SPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(1),'SPL_LOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'SPL_LOTT','SPCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'SPL_LOTT','SPCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'SPL_LOTT','SPCODUB2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'SPL_LOTT','SPQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'SPL_LOTT','SPQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'SPL_LOTT','SPNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SPSERIAL',this.oParentObject.w_SERIALE,'SPROWORD',this.w_CPROWNUM,'CPROWNUM',1,'SPCODLOT',this.w_MVCODLOT,'SPCODUBI',this.w_MVCODUBI,'SPCODUB2',this.w_MVCODUB2,'SPQTAMOV',this.w_MVQTAMOV,'SPQTAUM1',this.w_MVQTAUM1,'SPNUMRIF',this.w_MVNUMRIF)
      insert into (i_cTable) (SPSERIAL,SPROWORD,CPROWNUM,SPCODLOT,SPCODUBI,SPCODUB2,SPQTAMOV,SPQTAUM1,SPNUMRIF &i_ccchkf. );
         values (;
           this.oParentObject.w_SERIALE;
           ,this.w_CPROWNUM;
           ,1;
           ,this.w_MVCODLOT;
           ,this.w_MVCODUBI;
           ,this.w_MVCODUB2;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVNUMRIF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento SPL_LOTT'
      return
    endif
    this.w_TROV = .T.
    return
  proc Try_0306F028()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- begin transaction
    cp_BeginTrs()
    AddMsgNL("FASE 2: aggiornamento righe documento...",this)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento completato",,"")
    AddMsgNL("FASE 3: aggiornamento completato",this)
    return
  proc Try_0306E968()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento completato",,"")
    AddMsgNL("FASE 3: aggiornamento completato",this)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Documenti di Evasione
    * --- Carica Temporaneo Evasioni
    vq_exec("..\MADV\EXE\QUERY\GSMD1QDO.VQR", this,"AGGIORNA")
    if USED("AGGIORNA")
      SELECT AGGIORNA
      if RECCOUNT()>0
        GO TOP
        SCAN FOR NVL(CPROWNUM, 0)>0 AND NVL(MVQTAMOV, 0)>0
        * --- Esegue Update su riga gi� presente.
        this.w_CPROWNUM = CPROWNUM
        if this.w_OLDROW<>this.w_CPROWNUM
          * --- Informazioni Mantenute dalla riga originaria
          * --- Assegno lo stesso CPROWORD per avere le righe consecutive
          this.w_ROWORD = NVL(CPROWORD,0)
          this.w_MVDESART = NVL(MVDESART, " ")
          this.w_MVCODART = NVL(MVCODART, " ")
          this.w_MVCODICE = NVL(MVCODICE, " ")
          this.w_MVCODMAG = NVL(MVCODMAG, " ")
          this.w_MVCODMAT = NVL(MVCODMAT, " ")
          this.w_MVFLLOTT = NVL(MVFLLOTT, " ")
          this.w_MVF2LOTT = NVL(MVF2LOTT, " ")
          this.w_MVARTLOT = NVL(MVARTLOT, " ")
          this.w_MVFLUBIC = NVL(MVFLUBIC, " ")
          this.w_MVFLUBI2 = NVL(MVFLUBI2, " ")
          this.w_MVCODLOT = SPACE(20)
          this.w_MVCODUBI = SPACE(20)
          this.w_MVCODUB2 = SPACE(20)
          this.w_MVCODMAG = NVL(MVCODMAG,SPACE(5))
          this.w_MVCODMAT = NVL(MVCODMAT,SPACE(5))
          this.w_MVFLCASC = NVL(MVFLCASC, " ")
          this.w_MVFLRISE = NVL(MVFLRISE, " ")
          this.w_MVF2CASC = NVL(MVF2CASC, " ")
          this.w_MVF2RISE = NVL(MVF2RISE, " ")
          this.w_MVSERRIF = NVL(MVSERRIF,SPACE(10))
          this.w_MVROWRIF = NVL(MVROWRIF,0)
          this.w_MVTIPRIG = NVL(MVTIPRIG," ")
          this.w_MVCAUMAG = NVL(MVCAUMAG,SPACE(5))
          this.w_MVCATCON = NVL(MVCATCON,SPACE(5))
          this.w_MVCONTRA = NVL(MVCONTRA,SPACE(15))
          this.w_MVDESSUP = NVL(MVDESSUP,SPACE(10))
          this.w_MVCODLIS = NVL(MVCODLIS,SPACE(5))
          this.w_MVCODCLA = NVL(MVCODCLA,SPACE(5))
          this.w_MVPREZZO = NVL(MVPREZZO,0)
          this.w_MVSCONT1 = NVL(MVSCONT1,0)
          this.w_MVSCONT2 = NVL(MVSCONT2,0)
          this.w_MVSCONT3 = NVL(MVSCONT3,0)
          this.w_MVSCONT4 = NVL(MVSCONT4,0)
          this.w_MVFLOMAG = NVL(MVFLOMAG," ")
          this.w_MVIMPACC = 0
          this.w_MVIMPSCO = 0
          this.w_MVPESNET = NVL(MVPESNET,0)
          this.w_MVFLRAGG = NVL(MVFLRAGG," ")
          this.w_MVDATEVA = CP_TODATE(MVDATEVA)
          this.w_MVCODIVA = NVL(MVCODIVA,SPACE(5))
          this.w_MVCONIND = NVL(MVCONIND,SPACE(15))
          this.w_MVNOMENC = NVL(MVNOMENC,SPACE(8))
          this.w_MVFLTRAS = NVL(MVFLTRAS,SPACE(1))
          this.w_MVMOLSUP = NVL(MVMOLSUP,0)
          this.w_MVCAUCOL = NVL(MVCAUCOL,SPACE(5))
          this.w_MVCODATT = NVL(MVCODATT,SPACE(15))
          this.w_MVCODCEN = NVL(MVCODCEN,SPACE(15))
          this.w_MVCODCOM = NVL(MVCODCOM,SPACE(15))
          this.w_MVCODODL = NVL(MVCODODL,SPACE(15))
          this.w_MVF2ORDI = NVL(MVF2ORDI," ")
          this.w_MVF2IMPE = NVL(MVF2IMPE," ")
          this.w_MVFLORDI = NVL(MVFLORDI," ")
          this.w_MVFLELGM = NVL(MVFLELGM," ")
          this.w_MVFLIMPE = NVL(MVFLIMPE," ")
          this.w_MVKEYSAL = NVL(MVKEYSAL,SPACE(20))
          this.w_MVTIPATT = NVL(MVTIPATT," ")
          this.w_MVRIGMAT = NVL(MVRIGMAT,0)
          this.w_MVVOCCEN = NVL(MVVOCCEN,SPACE(15))
          this.w_MVIMPNAZ = 0
          this.w_MVVALMAG = 0
          this.w_MVVALRIG = 0
          this.w_MVINICOM = CP_TODATE(MVINICOM)
          this.w_MVFINCOM = CP_TODATE(MVFINCOM)
          this.w_MVTIPPRO = NVL(MVTIPPRO,"  ")
          this.w_MVTIPPR2 = NVL(MVTIPPR2,"  ")
          this.w_MVPERPRO = NVL(MVPERPRO,0)
          this.w_MVPROCAP = NVL(MVPROCAP,0)
          this.w_MVIMPPRO = 0
          this.w_MVCONTRO = NVL(MVCONTRO,SPACE(15) )
          this.w_MVQTAIMP = NVL(MVQTAIMP,0)
          this.w_MVQTAIM1 = NVL(MVQTAIM1,0)
        endif
        this.w_MVQTAMOV = NVL(MVQTAMOV,0)
        this.w_MVQTAUM1 = NVL(MVQTAUM1,0)
        if g_PERLOT="S" AND this.w_MVARTLOT$ "SC" AND (this.w_MVFLLOTT $ "+-" OR this.w_MVF2LOTT $ "+-")
          this.w_MVCODLOT = NVL(MVCODLOT, SPACE(20))
        endif
        if g_PERUBI="S" AND NOT EMPTY(this.w_MVCODMAG) AND this.w_MVFLUBIC="S" AND this.w_MVFLLOTT $ "+-"
          this.w_MVCODUBI = NVL(MVCODUBI, SPACE(20))
        endif
        if NOT EMPTY(this.w_MVCODMAT) AND g_PERUBI="S" AND this.w_MVFLUBI2="S" AND this.w_MVF2LOTT $ "+-"
          this.w_MVCODUB2 = NVL(MVCODUB2, SPACE(20))
        endif
        this.w_MVUNIMIS = NVL(MVUNIMIS," ")
        this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
        this.w_ROWMAT = SPROWNUM
        this.w_FLARIF = IIF(Not Empty(this.w_MVSERRIF),MVFLARIF," ")
        this.w_MVLOTMAG = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUBI ) , Space(5) , this.w_MVCODMAG )
        this.w_MVLOTMAT = Iif( Empty( this.w_MVCODLOT ) And Empty( this.w_MVCODUB2 ) , Space(5) , this.w_MVCODMAT )
        * --- Riferimento di riga dello split del lotto passato come parametro alla 
        *     query per la determinazione delle matricole da inserire.
        *     Nel caso di righe aggiuntive la variabile w_ROWNUM viene passato come 
        *     parametro alla query senza filtro poich� sar� effettivamente l'espressione del 
        *     campo MTROWNUM da inserire in MOVIMATR
        if this.oParentObject.w_CLADOC="MM"
          this.w_MVNUMRIF = -10
          if this.w_OLDROW<>this.w_CPROWNUM
            * --- Write into MVM_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'MVM_DETT','MMCODLOT');
              +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'MVM_DETT','MMCODUBI');
              +",MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'MVM_DETT','MMCODUB2');
              +",MMQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'MVM_DETT','MMQTAUM1');
              +",MMQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'MVM_DETT','MMQTAMOV');
              +",MMVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'MVM_DETT','MMVALMAG');
              +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'MVM_DETT','MMLOTMAG');
              +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'MVM_DETT','MMLOTMAT');
                  +i_ccchkf ;
              +" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                  +" and MMNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MMCODLOT = this.w_MVCODLOT;
                  ,MMCODUBI = this.w_MVCODUBI;
                  ,MMCODUB2 = this.w_MVCODUB2;
                  ,MMQTAUM1 = this.w_MVQTAUM1;
                  ,MMQTAMOV = this.w_MVQTAMOV;
                  ,MMVALMAG = this.w_MVVALRIG;
                  ,MMLOTMAG = this.w_MVLOTMAG;
                  ,MMLOTMAT = this.w_MVLOTMAT;
                  &i_ccchkf. ;
               where;
                  MMSERIAL = this.w_SERIAL;
                  and CPROWNUM = this.w_CPROWNUM;
                  and MMNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_ROWNUM = this.w_CPROWNUM
            this.w_AGGRIGSAL = this.w_CPROWNUM
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Determino il Max Numero di riga del documento
            vq_exec("..\MADV\EXE\QUERY\GSMDMQDO.VQR", this,"MAXRIG")
            SELECT MAXRIG
            this.w_ROWNUM = NVL(MAXRIG.NUMRIG,0)
          else
            * --- Inserisco Righe nuove
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Insert into MVM_DETT
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MVM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MMSERIAL"+",CPROWNUM"+",MMNUMRIF"+",CPROWORD"+",MMCODICE"+",MMCODART"+",MMUNIMIS"+",MMCAUMAG"+",MMCODLIS"+",MMQTAMOV"+",MMQTAUM1"+",MMPREZZO"+",MMSCONT1"+",MMSCONT2"+",MMSCONT3"+",MMSCONT4"+",MMFLOMAG"+",MMVALMAG"+",MMLOTMAG"+",MMLOTMAT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'MVM_DETT','MMSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MVM_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'MVM_DETT','MMNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'MVM_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'MVM_DETT','MMCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'MVM_DETT','MMCODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'MVM_DETT','MMUNIMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'MVM_DETT','MMCAUMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'MVM_DETT','MMCODLIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'MVM_DETT','MMQTAMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'MVM_DETT','MMQTAUM1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'MVM_DETT','MMPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'MVM_DETT','MMSCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'MVM_DETT','MMSCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'MVM_DETT','MMSCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'MVM_DETT','MMSCONT4');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'MVM_DETT','MMFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'MVM_DETT','MMVALMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'MVM_DETT','MMLOTMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'MVM_DETT','MMLOTMAT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.oParentObject.w_SERIALE,'CPROWNUM',this.w_ROWNUM,'MMNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_ROWORD,'MMCODICE',this.w_MVCODICE,'MMCODART',this.w_MVCODART,'MMUNIMIS',this.w_MVUNIMIS,'MMCAUMAG',this.w_MVCAUMAG,'MMCODLIS',this.w_MVCODLIS,'MMQTAMOV',this.w_MVQTAMOV,'MMQTAUM1',this.w_MVQTAUM1,'MMPREZZO',this.w_MVPREZZO)
              insert into (i_cTable) (MMSERIAL,CPROWNUM,MMNUMRIF,CPROWORD,MMCODICE,MMCODART,MMUNIMIS,MMCAUMAG,MMCODLIS,MMQTAMOV,MMQTAUM1,MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4,MMFLOMAG,MMVALMAG,MMLOTMAG,MMLOTMAT &i_ccchkf. );
                 values (;
                   this.oParentObject.w_SERIALE;
                   ,this.w_ROWNUM;
                   ,this.w_MVNUMRIF;
                   ,this.w_ROWORD;
                   ,this.w_MVCODICE;
                   ,this.w_MVCODART;
                   ,this.w_MVUNIMIS;
                   ,this.w_MVCAUMAG;
                   ,this.w_MVCODLIS;
                   ,this.w_MVQTAMOV;
                   ,this.w_MVQTAUM1;
                   ,this.w_MVPREZZO;
                   ,this.w_MVSCONT1;
                   ,this.w_MVSCONT2;
                   ,this.w_MVSCONT3;
                   ,this.w_MVSCONT4;
                   ,this.w_MVFLOMAG;
                   ,this.w_MVVALRIG;
                   ,this.w_MVLOTMAG;
                   ,this.w_MVLOTMAT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento MVM_DETT'
              return
            endif
            * --- Write into MVM_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MMCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'MVM_DETT','MMCAUCOL');
              +",MMCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'MVM_DETT','MMCODATT');
              +",MMCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'MVM_DETT','MMCODCOM');
              +",MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'MVM_DETT','MMCODLOT');
              +",MMCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'MVM_DETT','MMCODMAG');
              +",MMCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'MVM_DETT','MMCODMAT');
              +",MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'MVM_DETT','MMCODUB2');
              +",MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'MVM_DETT','MMCODUBI');
              +",MMF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'MVM_DETT','MMF2CASC');
              +",MMF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2IMPE),'MVM_DETT','MMF2IMPE');
              +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'MVM_DETT','MMF2LOTT');
              +",MMF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2ORDI),'MVM_DETT','MMF2ORDI');
              +",MMF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'MVM_DETT','MMF2RISE');
              +",MMFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'MVM_DETT','MMFLCASC');
              +",MMFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELGM),'MVM_DETT','MMFLELGM');
              +",MMFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'MVM_DETT','MMFLIMPE');
              +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'MVM_DETT','MMFLLOTT');
              +",MMFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'MVM_DETT','MMFLORDI');
              +",MMFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'MVM_DETT','MMFLRISE');
              +",MMKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'MVM_DETT','MMKEYSAL');
              +",MMTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'MVM_DETT','MMTIPATT');
                  +i_ccchkf ;
              +" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and MMNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MMCAUCOL = this.w_MVCAUCOL;
                  ,MMCODATT = this.w_MVCODATT;
                  ,MMCODCOM = this.w_MVCODCOM;
                  ,MMCODLOT = this.w_MVCODLOT;
                  ,MMCODMAG = this.w_MVCODMAG;
                  ,MMCODMAT = this.w_MVCODMAT;
                  ,MMCODUB2 = this.w_MVCODUB2;
                  ,MMCODUBI = this.w_MVCODUBI;
                  ,MMF2CASC = this.w_MVF2CASC;
                  ,MMF2IMPE = this.w_MVF2IMPE;
                  ,MMF2LOTT = this.w_MVF2LOTT;
                  ,MMF2ORDI = this.w_MVF2ORDI;
                  ,MMF2RISE = this.w_MVF2RISE;
                  ,MMFLCASC = this.w_MVFLCASC;
                  ,MMFLELGM = this.w_MVFLELGM;
                  ,MMFLIMPE = this.w_MVFLIMPE;
                  ,MMFLLOTT = this.w_MVFLLOTT;
                  ,MMFLORDI = this.w_MVFLORDI;
                  ,MMFLRISE = this.w_MVFLRISE;
                  ,MMKEYSAL = this.w_MVKEYSAL;
                  ,MMTIPATT = this.w_MVTIPATT;
                  &i_ccchkf. ;
               where;
                  MMSERIAL = this.oParentObject.w_SERIALE;
                  and CPROWNUM = this.w_ROWNUM;
                  and MMNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura MVM_DETT'
              return
            endif
            this.w_AGGRIGSAL = this.w_ROWNUM
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          this.w_MVNUMRIF = -20
          if this.w_MVQTAIMP-this.w_MVQTAMOV<0
            this.w_QTAIMP = IIF(Not Empty(this.w_MVSERRIF),this.w_MVQTAIMP,0)
          else
            this.w_QTAIMP = IIF(Not Empty(this.w_MVSERRIF),this.w_MVQTAMOV,0)
            this.w_MVQTAIMP = this.w_MVQTAIMP-this.w_MVQTAMOV
          endif
          if this.w_MVQTAIM1-this.w_MVQTAUM1<0
            this.w_QTAIM1 = IIF(Not Empty(this.w_MVSERRIF),this.w_MVQTAIM1,0)
          else
            this.w_QTAIM1 = IIF(Not Empty(this.w_MVSERRIF),this.w_MVQTAUM1,0)
            this.w_MVQTAIM1 = this.w_MVQTAIM1-this.w_MVQTAUM1
          endif
          if this.w_OLDROW<>this.w_CPROWNUM
            * --- Modifico riga gi� esistente
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
              +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
              +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
              +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
              +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
              +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
              +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
              +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'DOC_DETT','MVQTAIMP');
              +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIM1),'DOC_DETT','MVQTAIM1');
              +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
              +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVCODLOT = this.w_MVCODLOT;
                  ,MVCODUBI = this.w_MVCODUBI;
                  ,MVCODUB2 = this.w_MVCODUB2;
                  ,MVQTAUM1 = this.w_MVQTAUM1;
                  ,MVQTASAL = this.w_MVQTAUM1;
                  ,MVQTAMOV = this.w_MVQTAMOV;
                  ,MVVALRIG = this.w_MVVALRIG;
                  ,MVQTAIMP = this.w_QTAIMP;
                  ,MVQTAIM1 = this.w_QTAIM1;
                  ,MVLOTMAG = this.w_MVLOTMAG;
                  ,MVLOTMAT = this.w_MVLOTMAT;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERIAL;
                  and CPROWNUM = this.w_CPROWNUM;
                  and MVNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_AGGRIGSAL = this.w_CPROWNUM
            * --- Aggiorno Righe Piano Produzione collegato
            if g_PROD="S"
              Gsmd_bap(this,"A")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if g_PERLOT="S"
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Determino il Max Numero di riga del documento
            this.w_ROWNUM = this.w_CPROWNUM
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            vq_exec("..\MADV\EXE\QUERY\GSMD2QDO.VQR", this,"MAXRIG")
            SELECT MAXRIG
            this.w_ROWNUM = NVL(MAXRIG.NUMRIG,0)
          else
            * --- Inserisco Righe nuove
            this.w_MVCODODL = IIF(EMPTY(this.w_MVCODODL),this.w_CODODL,this.w_MVCODODL)
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Insert into DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVDESART"+",MVDESSUP"+",MVUNIMIS"+",MVCATCON"+",MVCONTRO"+",MVCAUMAG"+",MVCODCLA"+",MVCONTRA"+",MVCODLIS"+",MVQTAMOV"+",MVQTAUM1"+",MVPREZZO"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVFLOMAG"+",MVCODIVA"+",MVCONIND"+",MVVALRIG"+",MVIMPACC"+",MVIMPSCO"+",MVVALMAG"+",MVIMPNAZ"+",MVPERPRO"+",MVIMPPRO"+",MVPESNET"+",MVNOMENC"+",MVMOLSUP"+",MVNUMCOL"+",MVFLTRAS"+",MVFLRAGG"+",MVSERRIF"+",MVROWRIF"+",MVFLARIF"+",MVQTASAL"+",MVLOTMAG"+",MVLOTMAT"+",MVTIPPRO"+",MVTIPPR2"+",MVPROCAP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'DOC_DETT','MVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DOC_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRO),'DOC_DETT','MVCONTRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
              +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLRAGG),'DOC_DETT','MVFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLARIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAG),'DOC_DETT','MVLOTMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVLOTMAT),'DOC_DETT','MVLOTMAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_SERIALE,'CPROWNUM',this.w_ROWNUM,'MVNUMRIF',this.w_MVNUMRIF,'CPROWORD',this.w_ROWORD,'MVTIPRIG',this.w_MVTIPRIG,'MVCODICE',this.w_MVCODICE,'MVCODART',this.w_MVCODART,'MVDESART',this.w_MVDESART,'MVDESSUP',this.w_MVDESSUP,'MVUNIMIS',this.w_MVUNIMIS,'MVCATCON',this.w_MVCATCON,'MVCONTRO',this.w_MVCONTRO)
              insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVDESART,MVDESSUP,MVUNIMIS,MVCATCON,MVCONTRO,MVCAUMAG,MVCODCLA,MVCONTRA,MVCODLIS,MVQTAMOV,MVQTAUM1,MVPREZZO,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVFLOMAG,MVCODIVA,MVCONIND,MVVALRIG,MVIMPACC,MVIMPSCO,MVVALMAG,MVIMPNAZ,MVPERPRO,MVIMPPRO,MVPESNET,MVNOMENC,MVMOLSUP,MVNUMCOL,MVFLTRAS,MVFLRAGG,MVSERRIF,MVROWRIF,MVFLARIF,MVQTASAL,MVLOTMAG,MVLOTMAT,MVTIPPRO,MVTIPPR2,MVPROCAP &i_ccchkf. );
                 values (;
                   this.oParentObject.w_SERIALE;
                   ,this.w_ROWNUM;
                   ,this.w_MVNUMRIF;
                   ,this.w_ROWORD;
                   ,this.w_MVTIPRIG;
                   ,this.w_MVCODICE;
                   ,this.w_MVCODART;
                   ,this.w_MVDESART;
                   ,this.w_MVDESSUP;
                   ,this.w_MVUNIMIS;
                   ,this.w_MVCATCON;
                   ,this.w_MVCONTRO;
                   ,this.w_MVCAUMAG;
                   ,this.w_MVCODCLA;
                   ,this.w_MVCONTRA;
                   ,this.w_MVCODLIS;
                   ,this.w_MVQTAMOV;
                   ,this.w_MVQTAUM1;
                   ,this.w_MVPREZZO;
                   ,this.w_MVSCONT1;
                   ,this.w_MVSCONT2;
                   ,this.w_MVSCONT3;
                   ,this.w_MVSCONT4;
                   ,this.w_MVFLOMAG;
                   ,this.w_MVCODIVA;
                   ,this.w_MVCONIND;
                   ,this.w_MVVALRIG;
                   ,this.w_MVIMPACC;
                   ,this.w_MVIMPSCO;
                   ,this.w_MVVALMAG;
                   ,this.w_MVIMPNAZ;
                   ,this.w_MVPERPRO;
                   ,this.w_MVIMPPRO;
                   ,this.w_MVPESNET;
                   ,this.w_MVNOMENC;
                   ,this.w_MVMOLSUP;
                   ,0;
                   ,this.w_MVFLTRAS;
                   ,this.w_MVFLRAGG;
                   ,this.w_MVSERRIF;
                   ,this.w_MVROWRIF;
                   ," ";
                   ,this.w_MVQTAUM1;
                   ,this.w_MVLOTMAG;
                   ,this.w_MVLOTMAT;
                   ,this.w_MVTIPPRO;
                   ,this.w_MVTIPPR2;
                   ,this.w_MVPROCAP;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore in inserimento DOC_DETT'
              return
            endif
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLARIF ="+cp_NullLink(cp_ToStrODBC(this.w_FLARIF),'DOC_DETT','MVFLARIF');
              +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
              +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLERIF');
              +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
              +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
              +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
              +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
              +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
              +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
              +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
              +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
              +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
              +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
              +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
              +",MVCODODL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODODL),'DOC_DETT','MVCODODL');
              +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
              +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
              +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
              +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'DOC_DETT','MVF2CASC');
              +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2IMPE),'DOC_DETT','MVF2IMPE');
              +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
              +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2ORDI),'DOC_DETT','MVF2ORDI');
              +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'DOC_DETT','MVF2RISE');
              +",MVFINCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFINCOM),'DOC_DETT','MVFINCOM');
              +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'DOC_DETT','MVFLCASC');
              +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELGM),'DOC_DETT','MVFLELGM');
              +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
              +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
              +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'DOC_DETT','MVFLORDI');
              +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'DOC_DETT','MVFLRISE');
              +",MVINICOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVINICOM),'DOC_DETT','MVINICOM');
              +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
              +",MVRIGMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGMAT),'DOC_DETT','MVRIGMAT');
              +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
              +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
              +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
              +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
              +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'DOC_DETT','MVQTAIMP');
              +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIM1),'DOC_DETT','MVQTAIM1');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVFLARIF = this.w_FLARIF;
                  ,MVFLRIPA = " ";
                  ,MVFLEVAS = " ";
                  ,MVFLERIF = " ";
                  ,MVQTAEVA = 0;
                  ,MVQTAEV1 = 0;
                  ,MVIMPEVA = 0;
                  ,MVCAUCOL = this.w_MVCAUCOL;
                  ,MVCAUMAG = this.w_MVCAUMAG;
                  ,MVCODATT = this.w_MVCODATT;
                  ,MVCODCEN = this.w_MVCODCEN;
                  ,MVCODCOM = this.w_MVCODCOM;
                  ,MVCODLOT = this.w_MVCODLOT;
                  ,MVCODMAG = this.w_MVCODMAG;
                  ,MVCODMAT = this.w_MVCODMAT;
                  ,MVCODODL = this.w_MVCODODL;
                  ,MVCODUB2 = this.w_MVCODUB2;
                  ,MVCODUBI = this.w_MVCODUBI;
                  ,MVDATEVA = this.w_MVDATEVA;
                  ,MVF2CASC = this.w_MVF2CASC;
                  ,MVF2IMPE = this.w_MVF2IMPE;
                  ,MVF2LOTT = this.w_MVF2LOTT;
                  ,MVF2ORDI = this.w_MVF2ORDI;
                  ,MVF2RISE = this.w_MVF2RISE;
                  ,MVFINCOM = this.w_MVFINCOM;
                  ,MVFLCASC = this.w_MVFLCASC;
                  ,MVFLELGM = this.w_MVFLELGM;
                  ,MVFLIMPE = this.w_MVFLIMPE;
                  ,MVFLLOTT = this.w_MVFLLOTT;
                  ,MVFLORDI = this.w_MVFLORDI;
                  ,MVFLRISE = this.w_MVFLRISE;
                  ,MVINICOM = this.w_MVINICOM;
                  ,MVKEYSAL = this.w_MVKEYSAL;
                  ,MVRIGMAT = this.w_MVRIGMAT;
                  ,MVTIPATT = this.w_MVTIPATT;
                  ,MVVOCCEN = this.w_MVVOCCEN;
                  ,MV_SEGNO = this.w_MV_SEGNO;
                  ,MVFLELAN = this.w_MVFLELAN;
                  ,MVQTAIMP = this.w_QTAIMP;
                  ,MVQTAIM1 = this.w_QTAIM1;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.oParentObject.w_SERIALE;
                  and CPROWNUM = this.w_ROWNUM;
                  and MVNUMRIF = this.w_MVNUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura DOC_DETT'
              return
            endif
            this.w_AGGRIGSAL = this.w_ROWNUM
            * --- Inserisce nuova riga nel piano produzione
            if g_PROD="S"
              Gsmd_bap(this,"B")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if g_PERLOT="S"
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.w_OLDROW = this.w_CPROWNUM
        * --- Aggiorna anagrafica saldi/lotti
        if g_PERUBI="S" OR g_PERLOT="S"
          GSMD_BRL (this, this.oParentObject.w_SERIALE, IIF(this.oParentObject.w_CLADOC="MM", "M","V"), "" , this.w_AGGRIGSAL ,.T. )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        SELECT AGGIORNA
        ENDSCAN 
        * --- Try
        local bErr_049606A8
        bErr_049606A8=bTrsErr
        this.Try_049606A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          AddMsgNL("Errore aggiornamento status lortti...",this)
        endif
        bTrsErr=bTrsErr or bErr_049606A8
        * --- End
        if this.oParentObject.w_CLADOC<>"MM"
          * --- Rieseguo ricalcoli Totali documenti ed eventuali Ripartizioni (Sconti,Spese Accessorie)
          gsar_brd(this,this.oParentObject.w_SERIALE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
  endproc
  proc Try_049606A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into LOTTIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="LOCODICE,LOCODART"
      do vq_exec with 'GSMDSUBI_0',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
              +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LOFLSTAT = _t2.LOFLSTAT";
          +i_ccchkf;
          +" from "+i_cTable+" LOTTIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
              +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART, "+i_cQueryTable+" _t2 set ";
          +"LOTTIART.LOFLSTAT = _t2.LOFLSTAT";
          +Iif(Empty(i_ccchkf),"",",LOTTIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="LOTTIART.LOCODICE = t2.LOCODICE";
              +" and "+"LOTTIART.LOCODART = t2.LOCODART";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART set (";
          +"LOFLSTAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.LOFLSTAT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
              +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART set ";
          +"LOFLSTAT = _t2.LOFLSTAT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".LOCODICE = "+i_cQueryTable+".LOCODICE";
              +" and "+i_cTable+".LOCODART = "+i_cQueryTable+".LOCODART";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LOFLSTAT = (select LOFLSTAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo Matricole nel documento selezionato
    if (g_PERLOT="S" OR g_PERUBI="S")
      * --- Insert into MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMD_QAM.vqr",this.MOVIMATR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento matricole'
        return
      endif
    else
      * --- Insert into MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMD1QAM.vqr",this.MOVIMATR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore inserimento matricole'
        return
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce dettaglio lotti
    this.w_SERDOCORI = NVL(MVSERDDT,SPACE(15))
    this.w_ROWORDORI = NVL(MVROWDDT,0)
    if NOT EMPTY(this.w_SERDOCORI)
      * --- Select from MAT_PROD
      i_nConn=i_TableProp[this.MAT_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" MAT_PROD ";
            +" where MPSERIAL="+cp_ToStrODBC(this.w_SERDOCORI)+" AND MPROWDOC="+cp_ToStrODBC(this.w_ROWORDORI)+" AND MPNUMRIF=-30 and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+"";
             ,"_Curs_MAT_PROD")
      else
        select CPROWNUM from (i_cTable);
         where MPSERIAL=this.w_SERDOCORI AND MPROWDOC=this.w_ROWORDORI AND MPNUMRIF=-30 and CPROWNUM=this.w_CPROWNUM;
          into cursor _Curs_MAT_PROD
      endif
      if used('_Curs_MAT_PROD')
        select _Curs_MAT_PROD
        locate for 1=1
        do while not(eof())
        this.w_ROWMATORI = NVL(_Curs_MAT_PROD.CPROWNUM,0)
          select _Curs_MAT_PROD
          continue
        enddo
        use
      endif
      * --- Select from AVA_LOT
      i_nConn=i_TableProp[this.AVA_LOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2],.t.,this.AVA_LOT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" AVA_LOT ";
            +" where CLDICHIA="+cp_ToStrODBC(this.w_SERDOCORI)+" AND CLROWCNM="+cp_ToStrODBC(this.w_ROWMATORI)+" AND CLNUMRIF=-30 and CLROWDOC="+cp_ToStrODBC(this.w_ROWORDORI)+"";
             ,"_Curs_AVA_LOT")
      else
        select * from (i_cTable);
         where CLDICHIA=this.w_SERDOCORI AND CLROWCNM=this.w_ROWMATORI AND CLNUMRIF=-30 and CLROWDOC=this.w_ROWORDORI;
          into cursor _Curs_AVA_LOT
      endif
      if used('_Curs_AVA_LOT')
        select _Curs_AVA_LOT
        locate for 1=1
        if not(eof())
        do while not(eof())
        this.w_ROWLOT = NVL(_Curs_AVA_LOT.CPROWNUM,0) +1
          select _Curs_AVA_LOT
          continue
        enddo
        else
          this.w_ROWLOT = 1
          select _Curs_AVA_LOT
        endif
        use
      endif
      if this.w_ROWMATORI>0 AND NOT EMPTY(this.w_MVCODLOT) 
        * --- Try
        local bErr_049F34F0
        bErr_049F34F0=bTrsErr
        this.Try_049F34F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Write into AVA_LOT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.AVA_LOT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.AVA_LOT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CLFLCASC ="+cp_NullLink(cp_ToStrODBC("S"),'AVA_LOT','CLFLCASC');
            +",CLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'AVA_LOT','CLCODMAG');
            +",CLLOTART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AVA_LOT','CLLOTART');
            +",CLCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'AVA_LOT','CLCODUBI');
            +",CLCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AVA_LOT','CLCODLOT');
            +",CLQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'AVA_LOT','CLQTAMOV');
                +i_ccchkf ;
            +" where ";
                +"CLDICHIA = "+cp_ToStrODBC(this.w_SERDOCORI);
                +" and CLROWCNM = "+cp_ToStrODBC(this.w_ROWMATORI);
                +" and CLNUMRIF = "+cp_ToStrODBC(-30);
                +" and CLROWDOC = "+cp_ToStrODBC(this.w_ROWORDORI);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWLOT);
                   )
          else
            update (i_cTable) set;
                CLFLCASC = "S";
                ,CLCODMAG = this.w_MVCODMAG;
                ,CLLOTART = this.w_MVCODLOT;
                ,CLCODUBI = this.w_MVCODUBI;
                ,CLCODLOT = this.w_MVCODLOT;
                ,CLQTAMOV = this.w_MVQTAMOV;
                &i_ccchkf. ;
             where;
                CLDICHIA = this.w_SERDOCORI;
                and CLROWCNM = this.w_ROWMATORI;
                and CLNUMRIF = -30;
                and CLROWDOC = this.w_ROWORDORI;
                and CPROWNUM = this.w_ROWLOT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_049F34F0
        * --- End
      endif
    endif
  endproc
  proc Try_049F34F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into AVA_LOT
    i_nConn=i_TableProp[this.AVA_LOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AVA_LOT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AVA_LOT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CLDICHIA"+",CLROWCNM"+",CLNUMRIF"+",CLROWDOC"+",CPROWNUM"+",CLFLCASC"+",CLCODMAG"+",CLLOTART"+",CLCODUBI"+",CLCODLOT"+",CLQTAMOV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERDOCORI),'AVA_LOT','CLDICHIA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWMATORI),'AVA_LOT','CLROWCNM');
      +","+cp_NullLink(cp_ToStrODBC(-30),'AVA_LOT','CLNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORDORI),'AVA_LOT','CLROWDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWLOT),'AVA_LOT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("S"),'AVA_LOT','CLFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'AVA_LOT','CLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AVA_LOT','CLLOTART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'AVA_LOT','CLCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'AVA_LOT','CLCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'AVA_LOT','CLQTAMOV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CLDICHIA',this.w_SERDOCORI,'CLROWCNM',this.w_ROWMATORI,'CLNUMRIF',-30,'CLROWDOC',this.w_ROWORDORI,'CPROWNUM',this.w_ROWLOT,'CLFLCASC',"S",'CLCODMAG',this.w_MVCODMAG,'CLLOTART',this.w_MVCODLOT,'CLCODUBI',this.w_MVCODUBI,'CLCODLOT',this.w_MVCODLOT,'CLQTAMOV',this.w_MVQTAMOV)
      insert into (i_cTable) (CLDICHIA,CLROWCNM,CLNUMRIF,CLROWDOC,CPROWNUM,CLFLCASC,CLCODMAG,CLLOTART,CLCODUBI,CLCODLOT,CLQTAMOV &i_ccchkf. );
         values (;
           this.w_SERDOCORI;
           ,this.w_ROWMATORI;
           ,-30;
           ,this.w_ROWORDORI;
           ,this.w_ROWLOT;
           ,"S";
           ,this.w_MVCODMAG;
           ,this.w_MVCODLOT;
           ,this.w_MVCODUBI;
           ,this.w_MVCODLOT;
           ,this.w_MVQTAMOV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='AGG_LOTT'
    this.cWorkTables[2]='MVM_MAST'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='CAM_AGAZ'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='MVM_DETT'
    this.cWorkTables[8]='SPL_LOTT'
    this.cWorkTables[9]='AGG_MATR'
    this.cWorkTables[10]='MOVIMATR'
    this.cWorkTables[11]='VALUTE'
    this.cWorkTables[12]='LOTTIART'
    this.cWorkTables[13]='AVA_LOT'
    this.cWorkTables[14]='MAT_PROD'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    if used('_Curs_AVA_LOT')
      use in _Curs_AVA_LOT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
