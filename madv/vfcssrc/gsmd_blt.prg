* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_blt                                                        *
*              Lancia tracciabilita matricole                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-23                                                      *
* Last revis.: 2002-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_blt",oParentObject)
return(i_retval)

define class tgsmd_blt as StdBatch
  * --- Local variables
  w_OGG = .NULL.
  w_CODICE = space(41)
  w_DESCRI = space(50)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia tracciabilità matricole (Da GSMD_KME e GSMD_AMT)
    if Not empty( this.oParentObject.w_AMCODICE )
      * --- Leggo un codice di ricerca che abbia Keysal come la matricola
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART,CADESART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODART = "+cp_ToStrODBC(this.oParentObject.w_AMKEYSAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART,CADESART;
          from (i_cTable) where;
              CACODART = this.oParentObject.w_AMKEYSAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODICE = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_DESCRI = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- apro la maschera della tracciabilita e riempio i campi
      this.w_OGG = GSMD_KTM()
      this.w_OGG.w_CODINI = this.w_CODICE
      this.w_OGG.w_DESINI = this.w_DESCRI
      this.w_OGG.w_KEYSINI = this.oParentObject.w_AMKEYSAL
      this.w_OGG.w_ARTINI = this.oParentObject.w_AMCODART
      this.w_OGG.w_CODFIN = this.w_CODICE
      this.w_OGG.w_DESFIN = this.w_DESCRI
      this.w_OGG.w_KEYSFIN = this.oParentObject.w_AMKEYSAL
      this.w_OGG.w_MATINI = this.oParentObject.w_AMCODICE
      this.w_OGG.w_MATFIN = this.oParentObject.w_AMCODICE
      if g_UNIMAT="M"
        this.w_OGG.w_MATINI_A = this.oParentObject.w_AMCODICE
        this.w_OGG.w_MATFIN_A = this.oParentObject.w_AMCODICE
      else
        this.w_OGG.w_MATINI_M = this.oParentObject.w_AMCODICE
        this.w_OGG.w_MATFIN_M = this.oParentObject.w_AMCODICE
      endif
      this.w_OGG.SetControlsValue()     
      this.w_OGG.mEnableControls()     
      this.w_OGG.NotifyEvent("Esegui")     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
