* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bmu                                                        *
*              Situazione matricole da schede magazzino                        *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-04                                                      *
* Last revis.: 2002-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bmu",oParentObject)
return(i_retval)

define class tgsmd_bmu as StdBatch
  * --- Local variables
  w_MTKEYSAL = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_ESIRIS = space(1)
  w_UNIMIS1 = space(3)
  w_CODMAG = space(5)
  w_QTAUM1 = 0
  w_FLLOTT = space(1)
  w_MGUBIC = space(1)
  w_CODFOR = space(15)
  w_DATREG = ctod("  /  /  ")
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_MTMAGCAR = space(5)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_MTFLSCAR = space(1)
  w_MTCODMAT = space(40)
  w_CODICE = space(40)
  w_NUMRIF = 0
  w_PUNPAD = .NULL.
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_MAGUBI = space(5)
  w_UNIMIS1 = space(3)
  w_MVFLRISE = space(1)
  w_MVF2RISE = space(1)
  w_MVFLCASC = space(1)
  w_MVF2CASC = space(1)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_FLRISE = space(1)
  w_CODICE = space(41)
  * --- WorkFile variables
  MVM_DETT_idx=0
  DOC_DETT_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'inserimento rapido matricole
    this.w_PUNPAD = This.oParentObject
    this.w_NUMRIF = this.w_PUNPAD.w_NUMRIF
    this.w_CODART = this.oParentObject.w_CODARTMAT
    this.w_TIPCON = Nvl ( this.oParentObject.w_ZOOMMAG.GetVar("TIPCON") , "" )
    this.w_CODCON = Nvl ( this.oParentObject.w_ZOOMMAG.GetVar("CODCON") , "" )
    this.w_DESART = this.oParentObject.w_DESARTMAT
    this.w_UNIMIS1 = this.w_PUNPAD.w_UNIMIS1
    * --- Leggo informazioni dalla riga per determinare il magazzino di carico / scarico
    if this.w_NUMRIF=-10
      * --- Read from MVM_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MVM_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2],.t.,this.MVM_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MMCODMAG,MMCODMAT,MMFLRISE,MMFLCASC,MMF2CASC,MMF2RISE"+;
          " from "+i_cTable+" MVM_DETT where ";
              +"MMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_NUMROW);
              +" and MMNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MMCODMAG,MMCODMAT,MMFLRISE,MMFLCASC,MMF2CASC,MMF2RISE;
          from (i_cTable) where;
              MMSERIAL = this.oParentObject.w_SERIALE;
              and CPROWNUM = this.oParentObject.w_NUMROW;
              and MMNUMRIF = this.w_NUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODMAG = NVL(cp_ToDate(_read_.MMCODMAG),cp_NullValue(_read_.MMCODMAG))
        this.w_MVCODMAT = NVL(cp_ToDate(_read_.MMCODMAT),cp_NullValue(_read_.MMCODMAT))
        this.w_MVFLRISE = NVL(cp_ToDate(_read_.MMFLRISE),cp_NullValue(_read_.MMFLRISE))
        this.w_MVFLCASC = NVL(cp_ToDate(_read_.MMFLCASC),cp_NullValue(_read_.MMFLCASC))
        this.w_MVF2CASC = NVL(cp_ToDate(_read_.MMF2CASC),cp_NullValue(_read_.MMF2CASC))
        this.w_MVF2RISE = NVL(cp_ToDate(_read_.MMF2RISE),cp_NullValue(_read_.MMF2RISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODMAG,MVCODMAT,MVFLRISE,MVFLCASC,MVF2CASC,MVSERRIF,MVROWRIF,MVNUMRIF,MVF2RISE"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_NUMROW);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODMAG,MVCODMAT,MVFLRISE,MVFLCASC,MVF2CASC,MVSERRIF,MVROWRIF,MVNUMRIF,MVF2RISE;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_SERIALE;
              and CPROWNUM = this.oParentObject.w_NUMROW;
              and MVNUMRIF = this.w_NUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        this.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
        this.w_MVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
        this.w_MVFLCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
        this.w_MVF2CASC = NVL(cp_ToDate(_read_.MVF2CASC),cp_NullValue(_read_.MVF2CASC))
        this.w_SERRIF = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
        this.w_ROWRIF = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
        this.w_NUMRIF = NVL(cp_ToDate(_read_.MVNUMRIF),cp_NullValue(_read_.MVNUMRIF))
        this.w_MVF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se documento che evade verifico se storno riservato
      if Not Empty( this.w_SERRIF )
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVFLRISE"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVFLRISE;
            from (i_cTable) where;
                MVSERIAL = this.w_SERRIF;
                and CPROWNUM = this.w_ROWRIF;
                and MVNUMRIF = this.w_NUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- La quantit� di matricole da movimentare � data dal campo DIffer cambiato di segno
    this.w_QTAUM1 = - Nvl ( this.oParentObject.w_ZOOMMAG.GetVar("DIFFER") , 0 )
    * --- Se esistono pi� matricole della qt� di riga devo cancellare delle matricole
    *     L'utente deve farlo dalla gestione!
    if this.w_QTAUM1<=0
      ah_ErrorMsg("Matricole movimentate superiori rispetto alla quantit� di riga. Accedere al documento per modificare le matricole in eccedenza","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Se non storno il riservato allora svuoto i campi per interrogare l'eventuale 
    *     documento di origine
    if this.w_FLRISE<>"+"
      this.w_SERRIF = Space(10)
      this.w_ROWRIF = 0
      this.w_NUMRIF = 0
    endif
    if this.w_MVFLCASC="+" Or this.w_MVFLRISE="-" Or this.w_MVF2CASC="+" Or this.w_MVF2RISE="-"
      this.w_MTMAGCAR = IIF(this.w_MVFLCASC="+" Or this.w_MVFLRISE="-", this.w_MVCODMAG,this.w_MVCODMAT)
      this.w_MTFLCARI = IIF( this.w_MVFLCASC="+" Or this.w_MVF2CASC="+","E","R")
    endif
    if this.w_MVFLCASC="-" Or this.w_MVFLRISE="+" Or this.w_MVF2CASC="-" Or this.w_MVF2RISE="+"
      this.w_MTMAGSCA = IIF(this.w_MVFLCASC="-" Or this.w_MVFLRISE="+",this.w_MVCODMAG,this.w_MVCODMAT)
      this.w_MTFLSCAR = IIF(this.w_MVFLCASC="-" Or this.w_MVF2CASC="-","E","R")
    endif
    this.w_CODMAG = IIF( Not Empty( this.w_MTMAGSCA) , this.w_MTMAGSCA , this.w_MTMAGCAR )
    * --- Flag Ubicazioni
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGFLUBIC"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGFLUBIC;
        from (i_cTable) where;
            MGCODMAG = this.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MGUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CODMAG=this.w_MTMAGCAR And this.w_TIPCON="F"
      this.w_CODFOR = this.w_CODCON
    endif
    if Not Empty( this.w_MTMAGCAR )
      this.w_MAGUBI = IIF( this.w_MTMAGCAR = this.w_MVCODMAT , this.w_MVCODMAT , this.w_MVCODMAG )
    else
      this.w_MAGUBI = IIF( this.w_MTMAGSCA = this.w_MVCODMAT , this.w_MVCODMAT , this.w_MVCODMAG )
    endif
    * --- Articolo gestito a lotti
    this.w_FLLOTT = this.oParentObject.w_FLLOTMAT
    this.w_CODICE = this.w_PUNPAD.w_CODICE
    this.w_ESIRIS =  IIF( Not Empty( this.w_MTFLSCAR) , this.w_MTFLSCAR , this.w_MTFLCARI )
    this.w_MTKEYSAL = this.w_CODART
    GSVE_BGM (this,"CARRAPIDO")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MVM_DETT'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='MAGAZZIN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
