* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bif                                                        *
*              Elabora inventario fisico                                       *
*                                                                              *
*      Author: TAM Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_165]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-19                                                      *
* Last revis.: 2008-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bif",oParentObject)
return(i_retval)

define class tgsmd_bif as StdBatch
  * --- Local variables
  w_MESS = space(254)
  w_TOTREC = 0
  * --- WorkFile variables
  TMPINVFIS_idx=0
  TMPSTAMFIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Inventario Fisico da GSMD_SIF
    ah_Msg("Elaborazione inventario fisico di magazzino...",.T.)
    this.oParentObject.w_CLASSE = iif( Empty( this.oParentObject.w_CLASSE ) ,"N" , this.oParentObject.w_CLASSE )
    if this.oParentObject.w_SELLOT<>"S" .and. this.oParentObject.w_SELUBI<>"S" 
      * --- Estrazione Dati: Inventario fisico senza ubicazioni e lotti
      if this.oParentObject.w_SELMAT="S"
        * --- Create temporary table TMPINVFIS
        i_nIdx=cp_AddTableDef('TMPINVFIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\MADV\EXE\QUERY\GSMDMSIF_0',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPINVFIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPINVFIS
        i_nIdx=cp_AddTableDef('TMPINVFIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSMD_SIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPINVFIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      * --- Controllo se � stato inserito il magazzino nel caso di lotti oppure ubicazioni
      if EMPTY(this.oParentObject.w_CODMAG)
        ah_ErrorMsg("Il codice magazzino non � stato inserito","!","")
        i_retcode = 'stop'
        return
      endif
      * --- Estrazione Dati: Inventario fisico con ubicazioni e lotti
      * --- Create temporary table TMPSTAMFIS
      i_nIdx=cp_AddTableDef('TMPSTAMFIS') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSMD_SIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSTAMFIS_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.oParentObject.w_SELMAT="S"
        * --- Create temporary table TMPINVFIS
        i_nIdx=cp_AddTableDef('TMPINVFIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\MADV\EXE\QUERY\GSMDMSIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPINVFIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPINVFIS
        i_nIdx=cp_AddTableDef('TMPINVFIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\MADV\EXE\QUERY\GSMD4SIF_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPINVFIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Drop temporary table TMPSTAMFIS
      i_nIdx=cp_GetTableDefIdx('TMPSTAMFIS')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPSTAMFIS')
      endif
    endif
    * --- Controllo esistenza dati da stampare
    * --- Select from TMPINVFIS
    i_nConn=i_TableProp[this.TMPINVFIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPINVFIS_idx,2],.t.,this.TMPINVFIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COUNT(*) AS TOTREC  from "+i_cTable+" TMPINVFIS ";
           ,"_Curs_TMPINVFIS")
    else
      select COUNT(*) AS TOTREC from (i_cTable);
        into cursor _Curs_TMPINVFIS
    endif
    if used('_Curs_TMPINVFIS')
      select _Curs_TMPINVFIS
      locate for 1=1
      do while not(eof())
      this.w_TOTREC = NVL(TOTREC,0)
        select _Curs_TMPINVFIS
        continue
      enddo
      use
    endif
    if this.w_TOTREC=0
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da stampare","!","")
      * --- Drop temporary table TMPINVFIS
      i_nIdx=cp_GetTableDefIdx('TMPINVFIS')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPINVFIS')
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Se Flag Ubicazione non attivo sbianco il campo Codiubic
    *     Se Flag Lotti non attivo sbianco il campo Codilott
    *     Se entrambi i flag non sono attivi i campi sono gi� vuoti
    *     Se No Flag Matricole non � sbiancare codice matricola
    do case
      case this.oParentObject.w_SELUBI<>"S" AND this.oParentObject.w_SELLOT ="S" 
        * --- Write into TMPINVFIS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPINVFIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPINVFIS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPINVFIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODIUBIC ="+cp_NullLink(cp_ToStrODBC("                    "),'TMPINVFIS','CODIUBIC');
              +i_ccchkf ;
                 )
        else
          update (i_cTable) set;
              CODIUBIC = "                    ";
              &i_ccchkf. ;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.oParentObject.w_SELLOT<>"S" AND this.oParentObject.w_SELUBI ="S" 
        * --- Write into TMPINVFIS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPINVFIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPINVFIS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPINVFIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CODILOTT ="+cp_NullLink(cp_ToStrODBC("                    "),'TMPINVFIS','CODILOTT');
              +i_ccchkf ;
                 )
        else
          update (i_cTable) set;
              CODILOTT = "                    ";
              &i_ccchkf. ;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    if this.oParentObject.w_SELLOT="S" OR this.oParentObject.w_SELUBI="S"
      * --- Create temporary table TMPSTAMFIS
      i_nIdx=cp_AddTableDef('TMPSTAMFIS') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\MADV\EXE\QUERY\GSMD12SIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSTAMFIS_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPSTAMFIS
      i_nIdx=cp_AddTableDef('TMPSTAMFIS') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('..\MADV\EXE\QUERY\GSMD13SIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSTAMFIS_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Costruisco il campo CACODICE visibile nelle stampe
    CODMAG = this.oParentObject.w_CODMAG 
 ARTINI = this.oParentObject.w_ARTINI 
 CODINI = this.oParentObject.w_CODINI 
 CODFIN = this.oParentObject.w_CODFIN 
 UBICAZ = this.oParentObject.w_UBICAZ 
 ARTFIN = this.oParentObject.w_ARTFIN 
 CODFAM = this.oParentObject.w_CODFAM 
 CODGRU = this.oParentObject.w_CODGRU 
 SELUBI = this.oParentObject.w_SELUBI 
 SELLOT = this.oParentObject.w_SELLOT 
 CODCAT = this.oParentObject.w_CODCAT 
 CODMAR = this.oParentObject.w_CODMAR 
 DATASTAM = this.oParentObject.w_DATASTAM 
 CODESE = g_CODESE 
 SELORD = this.oParentObject.w_SELORD 
 INVORA = this.oParentObject.w_INVORA 
 SELTOT = this.oParentObject.w_SELTOT 
 INVMIN = this.oParentObject.w_INVMIN 
 SELGNEG=" "
     
 L_FILLOT=this.oParentObject.w_FILLOT 
 L_FLSALOT=this.oParentObject.w_FLSALOT 
 L_FILMAT=this.oParentObject.w_FILMAT 
 L_SELMAT=this.oParentObject.w_SELMAT 
 L_TIPESI=this.oParentObject.w_TIPESI 
 L_CODPRO=this.oParentObject.w_CODPRO 
 L_CLASSE=this.oParentObject.w_CLASSE
    if this.oParentObject.w_CLASSE="S"
      * --- Se voglio una stampa che discrimina per la classe ABC allora lancio il report solo se non ci sono situazioni anomale
      *     Altrimenti lancio un report che evidenzia tali situazioni (classe ABC non valorizzata)
      vq_exec("..\MADV\EXE\\QUERY\GSMD17SIF",this,"__TMP__")
      if RECCOUNT("__TMP__")>0
        this.w_MESS = "Impossibile eseguire la stampa: esistono articoli con classe ABC non definita%0Si vuole eseguire una stampa delle anomalie riscontrate?"
        if ah_YesNo(this.w_MESS)
          cp_chprn("..\MADV\EXE\QUERY\gsmd6sif.FRX", " ", this)
        endif
        * --- Drop temporary table TMPINVFIS
        i_nIdx=cp_GetTableDefIdx('TMPINVFIS')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPINVFIS')
        endif
        * --- Drop temporary table TMPSTAMFIS
        i_nIdx=cp_GetTableDefIdx('TMPSTAMFIS')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPSTAMFIS')
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_FILLOT="S"
      * --- eseguo filtro solo articoli gestiti a lotti
      * --- Delete from TMPSTAMFIS
      i_nConn=i_TableProp[this.TMPSTAMFIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMFIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ARFLLOTT = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              ARFLLOTT = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_FILMAT="S"
      * --- eseguo filtro solo articoli gestiti a matricole
      * --- Delete from TMPSTAMFIS
      i_nConn=i_TableProp[this.TMPSTAMFIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMFIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".CACODICE = "+i_cQueryTable+".CACODICE";
      
        do vq_exec with 'GSMDDBIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_SELLOT="S" AND this.oParentObject.w_FLSALOT<>"T"
      * --- Delete from TMPSTAMFIS
      i_nConn=i_TableProp[this.TMPSTAMFIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTAMFIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
              +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
      
        do vq_exec with 'GSMDLSIF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Cursore di Stampa
    vq_exec("..\MADV\EXE\query\GSMD18SIF",this,"__TMP__")
    do case
      case this.oParentObject.w_SelOrd = "A" AND this.oParentObject.w_Selubi<>"S" AND this.oParentObject.w_Sellot<>"S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd_sif.FRX", " ", this)
      case this.oParentObject.w_SelOrd = "A" AND this.oParentObject.w_Selubi<>"S" AND this.oParentObject.w_Sellot="S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd3sif.FRX", " ", this)
      case this.oParentObject.w_SelOrd = "A" AND this.oParentObject.w_Selubi="S" AND this.oParentObject.w_Sellot<>"S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd4sif.FRX", " ", this)
      case this.oParentObject.w_SelOrd = "A" AND this.oParentObject.w_Selubi="S" AND this.oParentObject.w_Sellot="S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd5sif.FRX", " ", this)
      case this.oParentObject.w_SelOrd = "U" AND this.oParentObject.w_Sellot<>"S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd0sif.FRX", " ", this)
      case this.oParentObject.w_SelOrd = "U" AND this.oParentObject.w_Sellot="S"
        cp_chprn("..\MADV\EXE\QUERY\gsmd1sif.FRX", " ", this)
    endcase
    * --- Drop temporary table TMPINVFIS
    i_nIdx=cp_GetTableDefIdx('TMPINVFIS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPINVFIS')
    endif
    * --- Drop temporary table TMPSTAMFIS
    i_nIdx=cp_GetTableDefIdx('TMPSTAMFIS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSTAMFIS')
    endif
    if used("__tmp__")
      select __tmp__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPINVFIS'
    this.cWorkTables[2]='*TMPSTAMFIS'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_TMPINVFIS')
      use in _Curs_TMPINVFIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
