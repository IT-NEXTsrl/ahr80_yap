* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_apm                                                        *
*              Progressivo matricola/lotto                                     *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_18]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-17                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_apm"))

* --- Class definition
define class tgsmd_apm as StdForm
  Top    = 6
  Left   = 10

  * --- Standard Properties
  Width  = 650
  Height = 77+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=108377239
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  PROGMAT_IDX = 0
  CMT_MAST_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "PROGMAT"
  cKeySelect = "PRKEYSAL,PRCODICE,PRCLAMAT"
  cKeyWhere  = "PRKEYSAL=this.w_PRKEYSAL and PRCODICE=this.w_PRCODICE and PRCLAMAT=this.w_PRCLAMAT"
  cKeyWhereODBC = '"PRKEYSAL="+cp_ToStrODBC(this.w_PRKEYSAL)';
      +'+" and PRCODICE="+cp_ToStrODBC(this.w_PRCODICE)';
      +'+" and PRCLAMAT="+cp_ToStrODBC(this.w_PRCLAMAT)';

  cKeyWhereODBCqualified = '"PROGMAT.PRKEYSAL="+cp_ToStrODBC(this.w_PRKEYSAL)';
      +'+" and PROGMAT.PRCODICE="+cp_ToStrODBC(this.w_PRCODICE)';
      +'+" and PROGMAT.PRCLAMAT="+cp_ToStrODBC(this.w_PRCLAMAT)';

  cPrg = "gsmd_apm"
  cComment = "Progressivo matricola/lotto"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRKEYSAL = space(40)
  w_PRCODICE = space(41)
  w_DESART = space(40)
  w_PRCLAMAT = space(5)
  w_CLDESCRI = space(30)
  w_PRCHKMUL = 0
  w_PR_PROGR = 0
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROGMAT','gsmd_apm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_apmPag1","gsmd_apm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Progressivo matricola/lotto")
      .Pages(1).HelpContextID = 259633494
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CMT_MAST'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='PROGMAT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROGMAT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROGMAT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PRKEYSAL = NVL(PRKEYSAL,space(40))
      .w_PRCODICE = NVL(PRCODICE,space(41))
      .w_PRCLAMAT = NVL(PRCLAMAT,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PROGMAT where PRKEYSAL=KeySet.PRKEYSAL
    *                            and PRCODICE=KeySet.PRCODICE
    *                            and PRCLAMAT=KeySet.PRCLAMAT
    *
    i_nConn = i_TableProp[this.PROGMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROGMAT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROGMAT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROGMAT '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRKEYSAL',this.w_PRKEYSAL  ,'PRCODICE',this.w_PRCODICE  ,'PRCLAMAT',this.w_PRCLAMAT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESART = space(40)
        .w_CLDESCRI = space(30)
        .w_OBTEST = i_INIDAT
        .w_PRKEYSAL = NVL(PRKEYSAL,space(40))
        .w_PRCODICE = NVL(PRCODICE,space(41))
          if link_1_2_joined
            this.w_PRCODICE = NVL(CACODICE102,NVL(this.w_PRCODICE,space(41)))
            this.w_DESART = NVL(CADESART102,space(40))
            this.w_PRKEYSAL = NVL(CACODART102,space(40))
          else
          .link_1_2('Load')
          endif
        .w_PRCLAMAT = NVL(PRCLAMAT,space(5))
          if link_1_5_joined
            this.w_PRCLAMAT = NVL(CMCODICE105,NVL(this.w_PRCLAMAT,space(5)))
            this.w_CLDESCRI = NVL(CMDESCRI105,space(30))
          else
          .link_1_5('Load')
          endif
        .w_PRCHKMUL = NVL(PRCHKMUL,0)
        .w_PR_PROGR = NVL(PR_PROGR,0)
        cp_LoadRecExtFlds(this,'PROGMAT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRKEYSAL = space(40)
      .w_PRCODICE = space(41)
      .w_DESART = space(40)
      .w_PRCLAMAT = space(5)
      .w_CLDESCRI = space(30)
      .w_PRCHKMUL = 0
      .w_PR_PROGR = 0
      .w_OBTEST = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_PRCODICE))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_PRCLAMAT))
          .link_1_5('Full')
          endif
          .DoRTCalc(5,7,.f.)
        .w_OBTEST = i_INIDAT
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROGMAT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oPRCLAMAT_1_5.enabled = i_bVal
      .Page1.oPag.oPR_PROGR_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPRCODICE_1_2.enabled = .f.
        .Page1.oPag.oPRCLAMAT_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPRCODICE_1_2.enabled = .t.
        .Page1.oPag.oPRCLAMAT_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PROGMAT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROGMAT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRKEYSAL,"PRKEYSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODICE,"PRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCLAMAT,"PRCLAMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCHKMUL,"PRCHKMUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR_PROGR,"PR_PROGR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROGMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
    i_lTable = "PROGMAT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROGMAT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROGMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PROGMAT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROGMAT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROGMAT')
        i_extval=cp_InsertValODBCExtFlds(this,'PROGMAT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRKEYSAL,PRCODICE,PRCLAMAT,PRCHKMUL,PR_PROGR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PRKEYSAL)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_PRCLAMAT)+;
                  ","+cp_ToStrODBC(this.w_PRCHKMUL)+;
                  ","+cp_ToStrODBC(this.w_PR_PROGR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROGMAT')
        i_extval=cp_InsertValVFPExtFlds(this,'PROGMAT')
        cp_CheckDeletedKey(i_cTable,0,'PRKEYSAL',this.w_PRKEYSAL,'PRCODICE',this.w_PRCODICE,'PRCLAMAT',this.w_PRCLAMAT)
        INSERT INTO (i_cTable);
              (PRKEYSAL,PRCODICE,PRCLAMAT,PRCHKMUL,PR_PROGR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRKEYSAL;
                  ,this.w_PRCODICE;
                  ,this.w_PRCLAMAT;
                  ,this.w_PRCHKMUL;
                  ,this.w_PR_PROGR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PROGMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PROGMAT_IDX,i_nConn)
      *
      * update PROGMAT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PROGMAT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRCHKMUL="+cp_ToStrODBC(this.w_PRCHKMUL)+;
             ",PR_PROGR="+cp_ToStrODBC(this.w_PR_PROGR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PROGMAT')
        i_cWhere = cp_PKFox(i_cTable  ,'PRKEYSAL',this.w_PRKEYSAL  ,'PRCODICE',this.w_PRCODICE  ,'PRCLAMAT',this.w_PRCLAMAT  )
        UPDATE (i_cTable) SET;
              PRCHKMUL=this.w_PRCHKMUL;
             ,PR_PROGR=this.w_PR_PROGR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROGMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PROGMAT_IDX,i_nConn)
      *
      * delete PROGMAT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRKEYSAL',this.w_PRKEYSAL  ,'PRCODICE',this.w_PRCODICE  ,'PRCLAMAT',this.w_PRCLAMAT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROGMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGMAT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PRCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PRCODICE))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPRCODICE_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PRCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PRCODICE)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODICE = NVL(_Link_.CACODICE,space(41))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_PRKEYSAL = NVL(_Link_.CACODART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODICE = space(41)
      endif
      this.w_DESART = space(40)
      this.w_PRKEYSAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.CACODICE as CACODICE102"+ ",link_1_2.CADESART as CADESART102"+ ",link_1_2.CACODART as CACODART102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on PROGMAT.PRCODICE=link_1_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and PROGMAT.PRCODICE=link_1_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCLAMAT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCLAMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PRCLAMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PRCLAMAT))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCLAMAT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCLAMAT) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oPRCLAMAT_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCLAMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PRCLAMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PRCLAMAT)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCLAMAT = NVL(_Link_.CMCODICE,space(5))
      this.w_CLDESCRI = NVL(_Link_.CMDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PRCLAMAT = space(5)
      endif
      this.w_CLDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCLAMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CMT_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.CMCODICE as CMCODICE105"+ ",link_1_5.CMDESCRI as CMDESCRI105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PROGMAT.PRCLAMAT=link_1_5.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PROGMAT.PRCLAMAT=link_1_5.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRCODICE_1_2.value==this.w_PRCODICE)
      this.oPgFrm.Page1.oPag.oPRCODICE_1_2.value=this.w_PRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_3.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_3.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCLAMAT_1_5.value==this.w_PRCLAMAT)
      this.oPgFrm.Page1.oPag.oPRCLAMAT_1_5.value=this.w_PRCLAMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDESCRI_1_7.value==this.w_CLDESCRI)
      this.oPgFrm.Page1.oPag.oCLDESCRI_1_7.value=this.w_CLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPR_PROGR_1_9.value==this.w_PR_PROGR)
      this.oPgFrm.Page1.oPag.oPR_PROGR_1_9.value=this.w_PR_PROGR
    endif
    cp_SetControlsValueExtFlds(this,'PROGMAT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PRCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_PRCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRCLAMAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCLAMAT_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PRCLAMAT)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsmd_apmPag1 as StdContainer
  Width  = 646
  height = 77
  stdWidth  = 646
  stdheight = 77
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRCODICE_1_2 as StdField with uid="QBROBNNMLJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PRCODICE", cQueryName = "PRKEYSAL,PRCODICE",;
    bObbl = .t. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    HelpContextID = 200721349,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=112, Top=14, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_PRCODICE"

  func oPRCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPRCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESART_1_3 as StdField with uid="DZOJCGBEDM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 2347466,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=407, Top=14, InputMask=replicate('X',40)

  add object oPRCLAMAT_1_5 as StdField with uid="LUVJYISSZQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PRCLAMAT", cQueryName = "PRKEYSAL,PRCODICE,PRCLAMAT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 136954806,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=47, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", oKey_1_1="CMCODICE", oKey_1_2="this.w_PRCLAMAT"

  func oPRCLAMAT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCLAMAT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCLAMAT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oPRCLAMAT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCLDESCRI_1_7 as StdField with uid="AGJGOTATID",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CLDESCRI", cQueryName = "CLDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 17873553,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=176, Top=47, InputMask=replicate('X',30)

  add object oPR_PROGR_1_9 as StdField with uid="UUHLFAFWDX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PR_PROGR", cQueryName = "PR_PROGR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 183237704,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=502, Top=47, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

  add object oStr_1_4 as StdString with uid="IDROPNRCVS",Visible=.t., Left=11, Top=18,;
    Alignment=1, Width=99, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="UKPEOUABRS",Visible=.t., Left=2, Top=51,;
    Alignment=1, Width=108, Height=18,;
    Caption="Classe matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IPVCVGCZDA",Visible=.t., Left=408, Top=51,;
    Alignment=1, Width=90, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_apm','PROGMAT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRKEYSAL=PROGMAT.PRKEYSAL";
  +" and "+i_cAliasName2+".PRCODICE=PROGMAT.PRCODICE";
  +" and "+i_cAliasName2+".PRCLAMAT=PROGMAT.PRCLAMAT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
