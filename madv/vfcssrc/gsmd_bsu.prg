* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bsu                                                        *
*              Stampa SSCC                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-10                                                      *
* Last revis.: 2004-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bsu",oParentObject)
return(i_retval)

define class tgsmd_bsu as StdBatch
  * --- Local variables
  w_UL__SSCC = space(18)
  w_ULCODART = space(20)
  * --- WorkFile variables
  TMPSTAMFIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Etichette Unit� Logistica (da GSMD_SUL)
    * --- Sbianco le variabili affinch� non facciano filtro
    this.w_UL__SSCC = ""
    this.w_ULCODART = ""
    * --- Moltiplico le etichette
    * --- Create temporary table TMPSTAMFIS
    i_nIdx=cp_AddTableDef('TMPSTAMFIS') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\MADV\EXE\QUERY\GSMD_SUL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSTAMFIS_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Controllo dimensione MOLTIPXN
    CHKMOLTIPXN(this,this.oParentObject.w_NUMCOPY)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Eseguo la join tra TMPSTAMFIS e MOLTIPXN per effettuare la duplicazione
    vx_exec(""+alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_OREP)+"",this)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPSTAMFIS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
