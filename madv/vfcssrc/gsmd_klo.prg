* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_klo                                                        *
*              Carica nuovo lotto                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_27]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-14                                                      *
* Last revis.: 2007-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_klo",oParentObject))

* --- Class definition
define class tgsmd_klo as StdForm
  Top    = 198
  Left   = 78

  * --- Standard Properties
  Width  = 525
  Height = 264
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-11-19"
  HelpContextID=51754135
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  CMT_MAST_IDX = 0
  PROGMAT_IDX = 0
  cPrg = "gsmd_klo"
  cComment = "Carica nuovo lotto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LOTIPCON = space(1)
  w_LOCODICE = space(20)
  w_LODATSCA = ctod('  /  /  ')
  w_LOCODCON = space(15)
  w_DESFOR = space(40)
  w_LOLOTFOR = space(20)
  w_LO__NOTE = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_LOCODART = space(20)
  w_CMCODICE = space(5)
  w_CODICE = space(20)
  w_KEYSAL = space(5)
  w_CODART = space(5)
  w_INCREM = 0
  w_QTAUM1 = 0
  w_ULTPROG = 0
  w_PROG = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_kloPag1","gsmd_klo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLOCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CMT_MAST'
    this.cWorkTables[4]='PROGMAT'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LOTIPCON=space(1)
      .w_LOCODICE=space(20)
      .w_LODATSCA=ctod("  /  /  ")
      .w_LOCODCON=space(15)
      .w_DESFOR=space(40)
      .w_LOLOTFOR=space(20)
      .w_LO__NOTE=space(0)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_LOCODART=space(20)
      .w_CMCODICE=space(5)
      .w_CODICE=space(20)
      .w_KEYSAL=space(5)
      .w_CODART=space(5)
      .w_INCREM=0
      .w_QTAUM1=0
      .w_ULTPROG=0
      .w_PROG=0
      .w_LOTIPCON=oParentObject.w_LOTIPCON
      .w_LOCODICE=oParentObject.w_LOCODICE
      .w_LODATSCA=oParentObject.w_LODATSCA
      .w_LOCODCON=oParentObject.w_LOCODCON
      .w_LOLOTFOR=oParentObject.w_LOLOTFOR
      .w_LO__NOTE=oParentObject.w_LO__NOTE
      .w_LOCODART=oParentObject.w_LOCODART
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_LOCODCON))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,7,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_LOCODART))
          .link_1_16('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CMCODICE))
          .link_1_18('Full')
        endif
        .w_CODICE = .w_LOCODART
        .w_KEYSAL = .w_LOCODART
        .w_CODART = .w_LOCODART
          .DoRTCalc(15,15,.f.)
        .w_QTAUM1 = 1
        .w_ULTPROG = 0
        .w_PROG = 0
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_LOTIPCON=.w_LOTIPCON
      .oParentObject.w_LOCODICE=.w_LOCODICE
      .oParentObject.w_LODATSCA=.w_LODATSCA
      .oParentObject.w_LOCODCON=.w_LOCODCON
      .oParentObject.w_LOLOTFOR=.w_LOLOTFOR
      .oParentObject.w_LO__NOTE=.w_LO__NOTE
      .oParentObject.w_LOCODART=.w_LOCODART
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
          .link_1_16('Full')
          .link_1_18('Full')
            .w_CODICE = .w_LOCODART
            .w_KEYSAL = .w_LOCODART
            .w_CODART = .w_LOCODART
        .DoRTCalc(15,15,.t.)
            .w_QTAUM1 = 1
            .w_ULTPROG = 0
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_PEJAPFTRDJ()
    with this
          * --- Calcolo lotto (gsmd_bcm)
          GSMD_BCM(this;
              ,'L';
              ,.w_CODICE;
              ,1;
             )
    endwith
  endproc
  proc Calculate_GEQEGJQNOA()
    with this
          * --- Aggiorna progressio all esc
          .w_PROG = IIF(.w_PROG>=0,CALPRMAT(.w_KEYSAL,.w_CODICE,.w_CMCODICE,.w_PROG,.w_INCREM*-1),0)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_PEJAPFTRDJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_GEQEGJQNOA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LOCODCON
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_LOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LOTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_LOTIPCON;
                     ,'ANCODICE',trim(this.w_LOCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_LOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LOTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_LOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_LOTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LOCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oLOCODCON_1_4'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LOTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_LOTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LOCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LOTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LOTIPCON;
                       ,'ANCODICE',this.w_LOCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LOCODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_LOCODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOCODART
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARCLALOT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_LOCODART);
                   +" and ARCODART="+cp_ToStrODBC(this.w_LOCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_LOCODART;
                       ,'ARCODART',this.w_LOCODART)
            select ARCODART,ARCLALOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOCODART = NVL(_Link_.ARCODART,space(20))
      this.w_CMCODICE = NVL(_Link_.ARCLALOT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_LOCODART = space(20)
      endif
      this.w_CMCODICE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CMCODICE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMINCREM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CMCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CMCODICE)
            select CMCODICE,CMINCREM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMCODICE = NVL(_Link_.CMCODICE,space(5))
      this.w_INCREM = NVL(_Link_.CMINCREM,0)
    else
      if i_cCtrl<>'Load'
        this.w_CMCODICE = space(5)
      endif
      this.w_INCREM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLOCODICE_1_2.value==this.w_LOCODICE)
      this.oPgFrm.Page1.oPag.oLOCODICE_1_2.value=this.w_LOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oLODATSCA_1_3.value==this.w_LODATSCA)
      this.oPgFrm.Page1.oPag.oLODATSCA_1_3.value=this.w_LODATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCODCON_1_4.value==this.w_LOCODCON)
      this.oPgFrm.Page1.oPag.oLOCODCON_1_4.value=this.w_LOCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_5.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_5.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLOLOTFOR_1_6.value==this.w_LOLOTFOR)
      this.oPgFrm.Page1.oPag.oLOLOTFOR_1_6.value=this.w_LOLOTFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLO__NOTE_1_7.value==this.w_LO__NOTE)
      this.oPgFrm.Page1.oPag.oLO__NOTE_1_7.value=this.w_LO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLOCODART_1_16.value==this.w_LOCODART)
      this.oPgFrm.Page1.oPag.oLOCODART_1_16.value=this.w_LOCODART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(NOT CHKESILO(.w_LOCODICE,.w_LOCODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOCODICE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lotto gi� esistente")
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(empty(.w_LOCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOCODCON_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   (empty(.w_LOCODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOCODART_1_16.SetFocus()
            i_bnoObbl = !empty(.w_LOCODART)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_klo
      IF EMPTY(.w_LOCODICE)
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=Ah_MsgFormat("Inserire nuovo codice lotto")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsmd_kloPag1 as StdContainer
  Width  = 521
  height = 264
  stdWidth  = 521
  stdheight = 264
  resizeXpos=310
  resizeYpos=179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLOCODICE_1_2 as StdField with uid="LHHTHBFCJZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LOCODICE", cQueryName = "LOCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lotto gi� esistente",;
    ToolTipText = "Codice o numero del lotto da caricare",;
    HelpContextID = 257345285,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=121, Top=39, cSayPict="p_LOT", cGetPict="p_LOT", InputMask=replicate('X',20)

  func oLOCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT CHKESILO(.w_LOCODICE,.w_LOCODART))
    endwith
    return bRes
  endfunc

  add object oLODATSCA_1_3 as StdField with uid="VSMRHWTGVI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LODATSCA", cQueryName = "LODATSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza validit� del lotto",;
    HelpContextID = 73709321,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=430, Top=10

  add object oLOCODCON_1_4 as StdField with uid="PAKMTQRLJV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LOCODCON", cQueryName = "LOCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Codice del fornitore di riferimento",;
    HelpContextID = 89573116,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=121, Top=68, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LOTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_LOCODCON"

  func oLOCODCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOCODCON_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOCODCON_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_LOTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_LOTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oLOCODCON_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oLOCODCON_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_LOTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_LOCODCON
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_5 as StdField with uid="ONEJNQJMBJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 95343050,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=262, Top=68, InputMask=replicate('X',40)

  add object oLOLOTFOR_1_6 as StdField with uid="YORESSPRQM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LOLOTFOR", cQueryName = "LOLOTFOR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Lotto del fornitore di riferimento",;
    HelpContextID = 22427384,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=121, Top=97, InputMask=replicate('X',20)

  add object oLO__NOTE_1_7 as StdMemo with uid="RRORGXEUXH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LO__NOTE", cQueryName = "LO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni aggiuntive sul lotto",;
    HelpContextID = 145032965,;
   bGlobalFont=.t.,;
    Height=84, Width=497, Left=12, Top=126


  add object oBtn_1_14 as StdButton with uid="GHEVIDFOUV",left=408, top=213, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere caricare nuovo lotto";
    , HelpContextID = 51782886;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="GNIBZHELYY",left=459, top=213, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 59071558;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLOCODART_1_16 as StdField with uid="GBNWOZTEON",rtseq=10,rtrep=.f.,;
    cFormVar = "w_LOCODART", cQueryName = "LOCODART",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 123127542,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=122, Top=11, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_LOCODART", oKey_2_1="ARCODART", oKey_2_2="this.w_LOCODART"

  func oLOCODART_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_LOCODART)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oStr_1_8 as StdString with uid="KDJGLHSSXK",Visible=.t., Left=42, Top=39,;
    Alignment=1, Width=74, Height=15,;
    Caption="Codice lotto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="YYNHMHRBPL",Visible=.t., Left=351, Top=10,;
    Alignment=1, Width=73, Height=15,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="PNUZJWSNPH",Visible=.t., Left=55, Top=68,;
    Alignment=1, Width=61, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FMAIRGIFVW",Visible=.t., Left=11, Top=97,;
    Alignment=1, Width=105, Height=15,;
    Caption="Rif.lotto fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FSEHKDCNCV",Visible=.t., Left=27, Top=11,;
    Alignment=1, Width=89, Height=18,;
    Caption="Codice articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_klo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
