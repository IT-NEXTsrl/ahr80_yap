* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_kcm                                                        *
*              Generazione codici matricole                                    *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_125]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-14                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_kcm",oParentObject))

* --- Class definition
define class tgsmd_kcm as StdForm
  Top    = 8
  Left   = 14

  * --- Standard Properties
  Width  = 622
  Height = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=169194647
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CMT_MAST_IDX = 0
  cPrg = "gsmd_kcm"
  cComment = "Generazione codici matricole"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PADRE = space(10)
  w_CODICE = space(41)
  o_CODICE = space(41)
  w_CODART = space(20)
  w_KEYSAL = space(40)
  w_TIPART = space(1)
  w_CMCODICE = space(5)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_QTAUM1 = 0
  w_INCREM = space(10)
  w_OBTEST = space(10)
  w_ULTPROG = 0
  w_DTELAB = ctod('  /  /  ')
  w_INIMATR = space(40)
  w_CODMATR = space(40)
  w_Msg = space(0)
  w_UNIMAT = space(1)
  w_ZoomMat = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_kcmPag1","gsmd_kcm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMat = this.oPgFrm.Pages(1).oPag.ZoomMat
    DoDefault()
    proc Destroy()
      this.w_ZoomMat = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CMT_MAST'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PADRE=space(10)
      .w_CODICE=space(41)
      .w_CODART=space(20)
      .w_KEYSAL=space(40)
      .w_TIPART=space(1)
      .w_CMCODICE=space(5)
      .w_DESART=space(40)
      .w_UNIMIS1=space(3)
      .w_QTAUM1=0
      .w_INCREM=space(10)
      .w_OBTEST=space(10)
      .w_ULTPROG=0
      .w_DTELAB=ctod("  /  /  ")
      .w_INIMATR=space(40)
      .w_CODMATR=space(40)
      .w_Msg=space(0)
      .w_UNIMAT=space(1)
        .w_PADRE = this.oParentObject
        .w_CODICE = .w_CODICE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICE))
          .link_1_2('Full')
        endif
        .w_CODART = .w_CODICE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODART))
          .link_1_3('Full')
        endif
        .w_KEYSAL = .w_CODICE
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CMCODICE))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,10,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(12,12,.f.)
        .w_DTELAB = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(14,14,.f.)
        .w_CODMATR = .w_ZoomMat.getVar('AMCODICE')
      .oPgFrm.Page1.oPag.ZoomMat.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
    this.DoRTCalc(16,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsmd_kcm
    * valorizzo il codice e la qt� se chiamata dalla
    * maschera di caricamento rapido
    if type('this.w_PADRE')='O'
       if upper(THIS.oParentObject.class)="TGSMD_BCR"
         This.w_CODICE=oParentObject.oParentObject.w_CODART
         This.w_QTAUM1=oParentObject.oParentObject.w_QTAUM1
         * se da caricamento rapido calcolo ultimo numero
       Else
         This.w_CODICE=oParentObject.w_CODART
         This.w_QTAUM1=oParentObject.w_GPQTAMOV
         * se da caricamento rapido calcolo ultimo numero
       endif
       This.NotifyEvent('w_CODICE Changed')
       This.mCalc(.t.)
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODICE = .w_CODICE
          .link_1_2('Full')
        endif
        if .o_CODICE<>.w_CODICE
            .w_CODART = .w_CODICE
          .link_1_3('Full')
        endif
        if .o_CODICE<>.w_CODICE
            .w_KEYSAL = .w_CODICE
        endif
        .DoRTCalc(5,5,.t.)
          .link_1_6('Full')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(7,14,.t.)
            .w_CODMATR = .w_ZoomMat.getVar('AMCODICE')
        .oPgFrm.Page1.oPag.ZoomMat.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.ZoomMat.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODICE_1_2.enabled = this.oPgFrm.Page1.oPag.oCODICE_1_2.mCond()
    this.oPgFrm.Page1.oPag.oQTAUM1_1_12.enabled = this.oPgFrm.Page1.oPag.oQTAUM1_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomMat.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODICE))
          select ARCODART,ARTIPART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODICE_1_2'),i_cWhere,'GSMA_BZA',"Codici di articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARTIPART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODICE)
            select ARCODART,ARTIPART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ARCODART,space(41))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(1))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(41)
      endif
      this.w_TIPART = space(1)
      this.w_DESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKARTI('M',.w_CODICE) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido oppure obsoleto")
        endif
        this.w_CODICE = space(41)
        this.w_TIPART = space(1)
        this.w_DESART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARCLAMAT,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARCLAMAT,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_CMCODICE = NVL(_Link_.ARCLAMAT,space(5))
      this.w_UNIMIS1 = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_CMCODICE = space(5)
      this.w_UNIMIS1 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CMCODICE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMINCREM,CMUNIMAT";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CMCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CMCODICE)
            select CMCODICE,CMINCREM,CMUNIMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMCODICE = NVL(_Link_.CMCODICE,space(5))
      this.w_INCREM = NVL(_Link_.CMINCREM,space(10))
      this.w_UNIMAT = NVL(_Link_.CMUNIMAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CMCODICE = space(5)
      endif
      this.w_INCREM = space(10)
      this.w_UNIMAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_2.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_2.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS1_1_10.value==this.w_UNIMIS1)
      this.oPgFrm.Page1.oPag.oUNIMIS1_1_10.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAUM1_1_12.value==this.w_QTAUM1)
      this.oPgFrm.Page1.oPag.oQTAUM1_1_12.value=this.w_QTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPROG_1_17.value==this.w_ULTPROG)
      this.oPgFrm.Page1.oPag.oULTPROG_1_17.value=this.w_ULTPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_25.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_25.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKARTI('M',.w_CODICE) AND .w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM')  and (Type('This..w_PADRE')<>'O')  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido oppure obsoleto")
          case   (empty(.w_QTAUM1))  and (Not Empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oQTAUM1_1_12.SetFocus()
            i_bnoObbl = !empty(.w_QTAUM1)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    return

enddefine

* --- Define pages as container
define class tgsmd_kcmPag1 as StdContainer
  Width  = 618
  height = 450
  stdWidth  = 618
  stdheight = 450
  resizeXpos=491
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_2 as StdField with uid="XCBYWDJYVV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido oppure obsoleto",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 208451546,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=58, Top=11, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODICE"

  func oCODICE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Type('This..w_PADRE')<>'O')
    endwith
   endif
  endfunc

  func oCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici di articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oDESART_1_7 as StdField with uid="ZLXWOVIZXZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209965514,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=353, Top=10, InputMask=replicate('X',40)

  add object oUNIMIS1_1_10 as StdField with uid="CXTKOINJOE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 235431866,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=58, Top=38, InputMask=replicate('X',3)

  add object oQTAUM1_1_12 as StdField with uid="RTHDOFNRAY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_QTAUM1", cQueryName = "QTAUM1",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� movimentata nella prima unit� di misura",;
    HelpContextID = 4136454,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=134, Top=38, cSayPict='"999999999999"', cGetPict='"999999999999"'

  func oQTAUM1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODICE))
    endwith
   endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="BRVGHGZAYB",left=558, top=35, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 86803386;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSMD_BCM(this.Parent.oContained,"M" , .w_CODICE , .w_QTAUM1 )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CODICE))
      endwith
    endif
  endfunc

  add object oULTPROG_1_17 as StdField with uid="NQOKSCDMNH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ULTPROG", cQueryName = "ULTPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore iniziale",;
    HelpContextID = 24426938,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=355, Top=38, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'


  add object oObj_1_20 as cp_runprogram with uid="IKTKTRDRQJ",left=399, top=474, width=202,height=24,;
    caption='GSMD_BSZ',;
   bGlobalFont=.t.,;
    prg="GSMD_BSZ('C')",;
    cEvent = "w_zoommat selected",;
    nPag=1;
    , HelpContextID = 229712704


  add object ZoomMat as cp_zoombox with uid="CZSVCBIDGT",left=13, top=85, width=593,height=257,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MATRICOL",cZoomFile="GSMD_KCM",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSMD_AMT",bRetriveAllRows=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 189678618


  add object oBtn_1_23 as StdButton with uid="FRSAGQFIZA",left=563, top=396, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176512070;
    , Caption='E\<sci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_24 as cp_runprogram with uid="RQRBLVNJFO",left=0, top=475, width=202,height=24,;
    caption='GSMD_BSZ',;
   bGlobalFont=.t.,;
    prg="GSMD_BSZ('A')",;
    cEvent = "w_CODICE Changed",;
    nPag=1;
    , HelpContextID = 229712704

  add object oMsg_1_25 as StdMemo with uid="ATBBCHAJSX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 169647302,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=95, Width=512, Left=11, Top=344, tabstop = .f., readonly = .t.

  add object oStr_1_8 as StdString with uid="SOYPQWKRXQ",Visible=.t., Left=9, Top=13,;
    Alignment=1, Width=45, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="MZWIFWFMHD",Visible=.t., Left=27, Top=42,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OSJFPVSUNF",Visible=.t., Left=96, Top=42,;
    Alignment=1, Width=35, Height=18,;
    Caption="Qt�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YDPVSAQQJT",Visible=.t., Left=257, Top=42,;
    Alignment=1, Width=93, Height=18,;
    Caption="Valore iniziale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_kcm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
