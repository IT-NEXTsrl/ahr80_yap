* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bml                                                        *
*              Controlli matricole                                             *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-15                                                      *
* Last revis.: 2002-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_NUMRIF,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bml",oParentObject,m.w_SERIAL,m.w_NUMRIF,m.pTipo)
return(i_retval)

define class tgsmd_bml as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_NUMRIF = 0
  pTipo = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se il documento contiene la stessa matricola due volte (cosa vietata)
    * --- Se ci� avviene la variabile caller w_OK viene posta a false
    this.oParentObject.w_OK = .T.
    * --- Select from GSMD_BML
    do vq_exec with 'GSMD_BML',this,'_Curs_GSMD_BML','',.f.,.t.
    if used('_Curs_GSMD_BML')
      select _Curs_GSMD_BML
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_OK = ( _Curs_GSMD_BML.CODMAT<=1 )
      Exit
        select _Curs_GSMD_BML
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_SERIAL,w_NUMRIF,pTipo)
    this.w_SERIAL=w_SERIAL
    this.w_NUMRIF=w_NUMRIF
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSMD_BML')
      use in _Curs_GSMD_BML
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_NUMRIF,pTipo"
endproc
