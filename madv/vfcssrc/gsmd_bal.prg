* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bal                                                        *
*              Aggiornamento lotti\ubicazioni                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_44]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-01                                                      *
* Last revis.: 2016-04-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bal",oParentObject,m.pEXEC)
return(i_retval)

define class tgsmd_bal as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_FLRISE = space(1)
  w_FLCASC = space(1)
  w_LOCODESE = space(4)
  w_LOSERIAL = space(10)
  w_DESCR1 = space(40)
  * --- WorkFile variables
  MVM_DETT_idx=0
  DOC_DETT_idx=0
  LOTTIART_idx=0
  UBICAZIO_idx=0
  MOVIMATR_idx=0
  CORDRISP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla maschera Gsmd_Kal di aggiornameto lotti e Gsmd_Kub  di aggiornamento ubicazioni
    do case
      case this.pEXEC="L"
        if NOT EMPTY(this.oParentObject.w_CODLOT) 
          if NOT CP_YESNO("La modifica dei lotti sui documenti e movimenti di magazzino pu� richiedere molto tempo. Vuoi proseguire?")
            this.oparentobject.oparentobject.w_TEST=.F.
            i_retcode = 'stop'
            return
          endif
          * --- Carica Nuovo Lotto
          if CHKESILO(this.oParentObject.w_CODLOT, this.oParentObject.w_CODART)
            ah_ErrorMsg("Lotto gi� esistente",,"")
            This.oparentobject.oparentobject.w_TEST=.F.
            i_retcode = 'stop'
            return
          else
            * --- Try
            local bErr_02B3AAC8
            bErr_02B3AAC8=bTrsErr
            this.Try_02B3AAC8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Errore aggiornamento lotto sui movimenti articolo",,"")
              This.oparentobject.oparentobject.w_TEST=.F.
              i_retcode = 'stop'
              return
            endif
            bTrsErr=bTrsErr or bErr_02B3AAC8
            * --- End
          endif
        else
          i_retcode = 'stop'
          return
        endif
      case this.pEXEC="U"
        * --- Carico Ubicazione
        if NOT EMPTY(this.oParentObject.w_CODUBI)
          if NOT CP_YESNO("La modifica delle ubicazioni sui documenti e movimenti di magazzino pu� richiedere molto tempo. Vuoi proseguire?")
            this.oparentobject.oparentobject.w_TEST=.F.
            i_retcode = 'stop'
            return
          endif
          * --- Try
          local bErr_02B3A078
          bErr_02B3A078=bTrsErr
          this.Try_02B3A078()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore aggiornamento lotto sui movimenti articolo",,"")
            This.oparentobject.oparentobject.w_TEST=.F.
            i_retcode = 'stop'
            return
          endif
          bTrsErr=bTrsErr or bErr_02B3A078
          * --- End
        else
          i_retcode = 'stop'
          return
        endif
    endcase
    * --- Aggiornamento massivo saldi commessa lotti ubicazione
    GSMD_BRL (this, , , , , .T. , )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_02B3AAC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_LOSERIAL = SPACE(10)
    this.w_LOCODESE = g_CODESE
    i_Conn=i_TableProp[this.LOTTIART_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRLOT", "i_codazi,w_LOCODESE,w_LOSERIAL")
    * --- Insert into LOTTIART
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOTTIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LOCODICE"+",LOCODESE"+",LOSERIAL"+",LOCODART"+",LODATCRE"+",LOTIPCON"+",LOFLSTAT"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'LOTTIART','LOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCODESE),'LOTTIART','LOCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOSERIAL),'LOTTIART','LOSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LOTTIART','LOCODART');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'LOTTIART','LODATCRE');
      +","+cp_NullLink(cp_ToStrODBC("F"),'LOTTIART','LOTIPCON');
      +","+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'LOTTIART','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'LOTTIART','UTDC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LOCODICE',this.oParentObject.w_CODLOT,'LOCODESE',this.w_LOCODESE,'LOSERIAL',this.w_LOSERIAL,'LOCODART',this.oParentObject.w_CODART,'LODATCRE',i_datsys,'LOTIPCON',"F",'LOFLSTAT',"D",'UTCC',i_codute,'UTDC',SetInfoDate( g_CALUTD ))
      insert into (i_cTable) (LOCODICE,LOCODESE,LOSERIAL,LOCODART,LODATCRE,LOTIPCON,LOFLSTAT,UTCC,UTDC &i_ccchkf. );
         values (;
           this.oParentObject.w_CODLOT;
           ,this.w_LOCODESE;
           ,this.w_LOSERIAL;
           ,this.oParentObject.w_CODART;
           ,i_datsys;
           ,"F";
           ,"D";
           ,i_codute;
           ,SetInfoDate( g_CALUTD );
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Aggiornamento ciclo documentale",.T.)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD1QAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'DOC_DETT','MVCODLOT');
          +",MVFLLOTT = _t2.MVFLLOTT";
          +",MVF2LOTT = _t2.MVF2LOTT";
          +",MVLOTMAG = _t2.MVLOTMAG";
          +",MVLOTMAT = _t2.MVLOTMAT";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'DOC_DETT','MVCODLOT');
          +",DOC_DETT.MVFLLOTT = _t2.MVFLLOTT";
          +",DOC_DETT.MVF2LOTT = _t2.MVF2LOTT";
          +",DOC_DETT.MVLOTMAG = _t2.MVLOTMAG";
          +",DOC_DETT.MVLOTMAT = _t2.MVLOTMAT";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVCODLOT,";
          +"MVFLLOTT,";
          +"MVF2LOTT,";
          +"MVLOTMAG,";
          +"MVLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'DOC_DETT','MVCODLOT')+",";
          +"t2.MVFLLOTT,";
          +"t2.MVF2LOTT,";
          +"t2.MVLOTMAG,";
          +"t2.MVLOTMAT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'DOC_DETT','MVCODLOT');
          +",MVFLLOTT = _t2.MVFLLOTT";
          +",MVF2LOTT = _t2.MVF2LOTT";
          +",MVLOTMAG = _t2.MVLOTMAG";
          +",MVLOTMAT = _t2.MVLOTMAT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'DOC_DETT','MVCODLOT');
          +",MVFLLOTT = (select MVFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVF2LOTT = (select MVF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVLOTMAG = (select MVLOTMAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVLOTMAT = (select MVLOTMAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT'
      return
    endif
    ah_Msg("Aggiornamento movimenti di magazzino",.T.)
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD_QAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MVM_DETT','MMCODLOT');
          +",MMFLLOTT = _t2.MMFLLOTT";
          +",MMF2LOTT = _t2.MMF2LOTT";
          +",MMLOTMAG = _t2.MMLOTMAG";
          +",MMLOTMAT = _t2.MMLOTMAT";
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
      +"MVM_DETT.MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MVM_DETT','MMCODLOT');
          +",MVM_DETT.MMFLLOTT = _t2.MMFLLOTT";
          +",MVM_DETT.MMF2LOTT = _t2.MMF2LOTT";
          +",MVM_DETT.MMLOTMAG = _t2.MMLOTMAG";
          +",MVM_DETT.MMLOTMAT = _t2.MMLOTMAT";
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMCODLOT,";
          +"MMFLLOTT,";
          +"MMF2LOTT,";
          +"MMLOTMAG,";
          +"MMLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MVM_DETT','MMCODLOT')+",";
          +"t2.MMFLLOTT,";
          +"t2.MMF2LOTT,";
          +"t2.MMLOTMAG,";
          +"t2.MMLOTMAT";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
      +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MVM_DETT','MMCODLOT');
          +",MMFLLOTT = _t2.MMFLLOTT";
          +",MMF2LOTT = _t2.MMF2LOTT";
          +",MMLOTMAG = _t2.MMLOTMAG";
          +",MMLOTMAT = _t2.MMLOTMAT";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MVM_DETT','MMCODLOT');
          +",MMFLLOTT = (select MMFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMF2LOTT = (select MMF2LOTT from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMLOTMAG = (select MMLOTMAG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MMLOTMAT = (select MMLOTMAT from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura MVM_DETT'
      return
    endif
    * --- Aggiorno Righe Matricole
    if g_GPOS="S"
      ah_Msg("Aggiornamento vendite negozio",.T.)
      * --- Write into CORDRISP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CORDRISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MDSERIAL,CPROWNUM"
        do vq_exec with '..\MADV\EXE\QUERY\GSMD2QAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MDCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'CORDRISP','MDCODLOT');
            +",MDFLLOTT = _t2.MDFLLOTT";
            +",MDLOTMAG = _t2.MDLOTMAG";
            +i_ccchkf;
            +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
        +"CORDRISP.MDCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'CORDRISP','MDCODLOT');
            +",CORDRISP.MDFLLOTT = _t2.MDFLLOTT";
            +",CORDRISP.MDLOTMAG = _t2.MDLOTMAG";
            +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
            +"MDCODLOT,";
            +"MDFLLOTT,";
            +"MDLOTMAG";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'CORDRISP','MDCODLOT')+",";
            +"t2.MDFLLOTT,";
            +"t2.MDLOTMAG";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
        +"MDCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'CORDRISP','MDCODLOT');
            +",MDFLLOTT = _t2.MDFLLOTT";
            +",MDLOTMAG = _t2.MDLOTMAG";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MDCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'CORDRISP','MDCODLOT');
            +",MDFLLOTT = (select MDFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MDLOTMAG = (select MDLOTMAG from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore in scrittura CORDRISP'
        return
      endif
    endif
    ah_Msg("Aggiornamento movimenti matricole",.T.)
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MTCODLOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MOVIMATR','MTCODLOT');
          +i_ccchkf ;
      +" where ";
          +"MTKEYSAL = "+cp_ToStrODBC(this.oParentObject.w_CODART);
             )
    else
      update (i_cTable) set;
          MTCODLOT = this.oParentObject.w_CODLOT;
          &i_ccchkf. ;
       where;
          MTKEYSAL = this.oParentObject.w_CODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_02B3A078()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from UBICAZIO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UBICAZIO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UBICAZIO_idx,2],.t.,this.UBICAZIO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UBDESCRI"+;
        " from "+i_cTable+" UBICAZIO where ";
            +"UBCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
            +" and UBCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODUBI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UBDESCRI;
        from (i_cTable) where;
            UBCODMAG = this.oParentObject.w_CODMAG;
            and UBCODICE = this.oParentObject.w_CODUBI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESCR1 = NVL(cp_ToDate(_read_.UBDESCRI),cp_NullValue(_read_.UBDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      * --- Inserisco Nuova Ubicazione
      * --- Insert into UBICAZIO
      i_nConn=i_TableProp[this.UBICAZIO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UBICAZIO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UBICAZIO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"UBCODMAG"+",UBCODICE"+",UBDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'UBICAZIO','UBCODMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'UBICAZIO','UBCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCRI),'UBICAZIO','UBDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'UBCODMAG',this.oParentObject.w_CODMAG,'UBCODICE',this.oParentObject.w_CODUBI,'UBDESCRI',this.oParentObject.w_DESCRI)
        insert into (i_cTable) (UBCODMAG,UBCODICE,UBDESCRI &i_ccchkf. );
           values (;
             this.oParentObject.w_CODMAG;
             ,this.oParentObject.w_CODUBI;
             ,this.oParentObject.w_DESCRI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    ah_Msg("Aggiornamento ubicazione",.T.)
    * --- Aggiorno codice lotto nei Documenti dI Carico
    this.w_FLRISE = "-"
    this.w_FLCASC = "+"
    ah_Msg("Aggiornamento carichi",.T.)
    * --- Aggiorno codice Ubicazione nei Documenti 
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD1QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",DOC_DETT.MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVFLLOTT');
      +",DOC_DETT.MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVCODUBI,";
          +"MVFLLOTT,";
          +"MVLOTMAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI')+",";
          +cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVFLLOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT aggiornamento ubicazione'
      return
    endif
    * --- Aggiorno codice Ubicazione nei Movimenti di Magazzino 
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD_QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
      +"MVM_DETT.MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MVM_DETT.MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMFLLOTT');
      +",MVM_DETT.MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMCODUBI,";
          +"MMFLLOTT,";
          +"MMLOTMAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI')+",";
          +cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMFLLOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura MVM_DETT aggiornamento ubicazione '
      return
    endif
    ah_Msg("Aggiornamento ubicazione collegata",.T.)
    * --- Aggiorno codice Ubicazione Collegata nei Documenti
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD3QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",DOC_DETT.MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVF2LOTT');
      +",DOC_DETT.MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVCODUB2,";
          +"MVF2LOTT,";
          +"MVLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2')+",";
          +cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVF2LOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT collegata carico'
      return
    endif
    * --- Aggiorno codice Ubicazione Collegata nei Movimenti di Magazzino 
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD2QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
      +"MVM_DETT.MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MVM_DETT.MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMF2LOTT');
      +",MVM_DETT.MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMCODUB2,";
          +"MMF2LOTT,";
          +"MMLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2')+",";
          +cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMF2LOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("+"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura MVM_DETT collegata carico'
      return
    endif
    * --- Aggiorno codice lotto nei Documenti dI Scarico
    ah_Msg("Aggiornamento scarichi",.T.)
    this.w_FLRISE = "+"
    this.w_FLCASC = "-"
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD1QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",DOC_DETT.MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVFLLOTT');
      +",DOC_DETT.MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVCODUBI,";
          +"MVFLLOTT,";
          +"MVLOTMAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI')+",";
          +cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVFLLOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUBI');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVFLLOTT');
      +",MVLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAG');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT aggiornamento ubicazione'
      return
    endif
    * --- Aggiorno codice Ubicazione nei Movimenti di Magazzino 
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD_QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
      +"MVM_DETT.MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MVM_DETT.MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMFLLOTT');
      +",MVM_DETT.MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMCODUBI,";
          +"MMFLLOTT,";
          +"MMLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI')+",";
          +cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMFLLOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUBI');
      +",MMFLLOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMFLLOTT');
      +",MMLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura MVM_DETT aggiornamento ubicazione '
      return
    endif
    * --- Aggiorno codice Ubicazione Collegata nei Documenti
    ah_Msg("Aggiornamento ubicazione collegata",.T.)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD3QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",DOC_DETT.MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVF2LOTT');
      +",DOC_DETT.MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVCODUB2,";
          +"MVF2LOTT,";
          +"MVLOTMAT";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2')+",";
          +cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVF2LOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'DOC_DETT','MVCODUB2');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'DOC_DETT','MVF2LOTT');
      +",MVLOTMAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'DOC_DETT','MVLOTMAT');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT collegata carico'
      return
    endif
    * --- Aggiorno codice Ubicazione Collegata nei Movimenti di Magazzino 
    * --- Write into MVM_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MVM_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MMSERIAL,CPROWNUM,MMNUMRIF"
      do vq_exec with '..\MADV\EXE\QUERY\GSMD2QAU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" from "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT, "+i_cQueryTable+" _t2 set ";
      +"MVM_DETT.MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MVM_DETT.MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMF2LOTT');
      +",MVM_DETT.MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +Iif(Empty(i_ccchkf),"",",MVM_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MVM_DETT.MMSERIAL = t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = t2.MMNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set (";
          +"MMCODUB2,";
          +"MMF2LOTT,";
          +"MMLOTMAG";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2')+",";
          +cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMF2LOTT')+",";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MVM_DETT.MMSERIAL = _t2.MMSERIAL";
              +" and "+"MVM_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"MVM_DETT.MMNUMRIF = _t2.MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MVM_DETT set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MMSERIAL = "+i_cQueryTable+".MMSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MMNUMRIF = "+i_cQueryTable+".MMNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MMCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MVM_DETT','MMCODUB2');
      +",MMF2LOTT ="+cp_NullLink(cp_ToStrODBC("-"),'MVM_DETT','MMF2LOTT');
      +",MMLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MVM_DETT','MMLOTMAG');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura MVM_DETT collegata carico'
      return
    endif
    * --- Aggiorno Righe Matricole
    ah_Msg("Aggiornamento movimenti matricole",.T.)
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MTCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MOVIMATR','MTCODUBI');
          +i_ccchkf ;
      +" where ";
          +"MTMAGSCA = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             )
    else
      update (i_cTable) set;
          MTCODUBI = this.oParentObject.w_CODUBI;
          &i_ccchkf. ;
       where;
          MTMAGSCA = this.oParentObject.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MTCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MOVIMATR','MTCODUBI');
          +i_ccchkf ;
      +" where ";
          +"MTMAGCAR = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             )
    else
      update (i_cTable) set;
          MTCODUBI = this.oParentObject.w_CODUBI;
          &i_ccchkf. ;
       where;
          MTMAGCAR = this.oParentObject.w_CODMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_GPOS="S"
      * --- Aggiorno le vendite negozio
      ah_Msg("Aggiornamento vendite negozio",.T.)
      * --- Write into CORDRISP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CORDRISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CORDRISP_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MDSERIAL,CPROWNUM"
        do vq_exec with '..\MADV\EXE\QUERY\GSMD3QAL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CORDRISP_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MDCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'CORDRISP','MDCODUBI');
            +",MDFLLOTT = _t2.MDFLLOTT";
        +",MDLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDLOTMAG');
        +",MDMAGRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDMAGRIG');
            +i_ccchkf;
            +" from "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP, "+i_cQueryTable+" _t2 set ";
        +"CORDRISP.MDCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'CORDRISP','MDCODUBI');
            +",CORDRISP.MDFLLOTT = _t2.MDFLLOTT";
        +",CORDRISP.MDLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDLOTMAG');
        +",CORDRISP.MDMAGRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDMAGRIG');
            +Iif(Empty(i_ccchkf),"",",CORDRISP.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="CORDRISP.MDSERIAL = t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set (";
            +"MDCODUBI,";
            +"MDFLLOTT,";
            +"MDLOTMAG,";
            +"MDMAGRIG";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'CORDRISP','MDCODUBI')+",";
            +"t2.MDFLLOTT,";
            +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDLOTMAG')+",";
            +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDMAGRIG')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="CORDRISP.MDSERIAL = _t2.MDSERIAL";
                +" and "+"CORDRISP.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CORDRISP set ";
        +"MDCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'CORDRISP','MDCODUBI');
            +",MDFLLOTT = _t2.MDFLLOTT";
        +",MDLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDLOTMAG');
        +",MDMAGRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDMAGRIG');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MDSERIAL = "+i_cQueryTable+".MDSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MDCODUBI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'CORDRISP','MDCODUBI');
            +",MDFLLOTT = (select MDFLLOTT from "+i_cQueryTable+" where "+i_cWhere+")";
        +",MDLOTMAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDLOTMAG');
        +",MDMAGRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'CORDRISP','MDMAGRIG');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore in scrittura CORDRISP'
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='MVM_DETT'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='UBICAZIO'
    this.cWorkTables[5]='MOVIMATR'
    this.cWorkTables[6]='CORDRISP'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
