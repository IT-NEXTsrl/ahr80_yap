* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bdc                                                        *
*              Controllo eliminazione lotti                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-04                                                      *
* Last revis.: 2008-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bdc",oParentObject)
return(i_retval)

define class tgsmd_bdc as StdBatch
  * --- Local variables
  w_CODART = space(20)
  w_CODLOT = space(20)
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il lotto � utilizzato nell anagrafica saldi lotti allora se ne impedisce la cancellazione
    this.w_CODART = this.oparentobject.w_LOCODART
    this.w_CODLOT = this.oparentobject.w_LOCODICE
    * --- Select from gsar3bks
    do vq_exec with 'gsar3bks',this,'_Curs_gsar3bks','',.f.,.t.
    if used('_Curs_gsar3bks')
      select _Curs_gsar3bks
      locate for 1=1
      do while not(eof())
      if CONTA>0
        this.w_MESS = "Lotto usato nell anagrafica saldi/lotti%0Impossibile eliminare!"
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat(this.w_MESS)
      endif
        select _Curs_gsar3bks
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gsar3bks')
      use in _Curs_gsar3bks
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
