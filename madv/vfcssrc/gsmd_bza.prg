* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bza                                                        *
*              Zoom unita logistiche                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_3]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-25                                                      *
* Last revis.: 2004-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bza",oParentObject)
return(i_retval)

define class tgsmd_bza as StdBatch
  * --- Local variables
  w_UL__SSCC = space(18)
  w_CODART = space(20)
  w_CODLOT = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esamino le righe selezionate per condizionare lo zoom delle unit� logistiche
    *     (da GSMD_KAT)
    NC = this.oParentObject.w_ZOOMDET.cCursor
    select distinct MVCODART, MVCODLOT from (NC) where XCHK=1 into cursor APPO1
    if RECCOUNT("APPO1") = 1
      * --- Selezionato un solo articolo
      this.w_CODART = MVCODART
      this.w_CODLOT = MVCODLOT
    endif
    * --- Lancio lo zoom
    vx_exec("..\MADV\EXE\QUERY\GSMD_QUL.VZM",this)
    if NOT EMPTY(this.w_UL__SSCC)
      this.oParentObject.w_CODUNLOG = this.w_UL__SSCC
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
