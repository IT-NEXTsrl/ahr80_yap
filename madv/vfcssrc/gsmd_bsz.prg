* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bsz                                                        *
*              Controlli gestione matricole                                    *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-20                                                      *
* Last revis.: 2011-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bsz",oParentObject,m.pPROV)
return(i_retval)

define class tgsmd_bsz as StdBatch
  * --- Local variables
  pPROV = space(1)
  w_KEY = space(254)
  w_OBJMOV = .NULL.
  w_FLAUTO = space(1)
  w_MESS = space(10)
  w_UNIMAT = space(1)
  * --- WorkFile variables
  PROGMAT_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  CMT_DETT_idx=0
  CMT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pPROV='C'=Carica il record selezionato da GSMD_KCM
    *     pPROV='A'=Valorizza Ultimo Numero da GSMD_ACM, GSMD_KCM
    * --- Caricamento Anagrafica Matricole
    * --- Calcolo Ultimo Progressivo 
    do case
      case this.pPROV="C"
        * --- Oggetto Anagrafica Matricole
        this.w_OBJMOV = GSMD_AMT()
        * --- Locale per sringa da passare alla querykeyset
        this.w_KEY = "AMCODICE='"+this.oParentObject.w_CODMATR+ "'and  AMKEYSAL='"+this.oParentObject.w_KEYSAL+"'"
        * --- Testo se l'utente pu� aprire la gestione
        if not (this.w_OBJMOV.bSec1)
          ah_ErrorMsg("L'utente non � abilitato ad accedere alla gestione","!","")
          i_retcode = 'stop'
          return
        endif
        * --- Lancio l'oggetto in interrogazione
        this.w_OBJMOV.cFunction = "Query"
        this.w_OBJMOV.QueryKeySet(this.w_KEY,"")     
        this.w_OBJMOV.LoadRecWarn()     
        oCpToolBar.SetQuery()
      case this.pPROV="A"
        * --- Calcola l'Ultimo Numero : l'utilizzo della query consente di inibire tramite remove filter il filtro
        *     w_KEYSAL: Chiave Saldo di Magazzino. Il filtro � necessario in GSMD_KCM(Generazione
        *     Codici) mentre in GSMD_ACM (Classe Matricole) interessa l'Ultimo Progressivo associato alla Classe
        *     in assoluto (rileggo Keysal dopo la Changed non ha ancora eseguito il Link)
        if UPPER(This.OparentObject.Class)="TGSMD_KCM"
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CACODART"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CACODART;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_CODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_KEYSAL = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            this.oParentObject.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Leggo la classe
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCLAMAT"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCLAMAT;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_CMCODICE = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Solo Classi con tipo Aggiornamento automatico
          * --- Read from CMT_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CMT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLAUTO,CMUNIMAT"+;
              " from "+i_cTable+" CMT_MAST where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CMCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLAUTO,CMUNIMAT;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CMCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLAUTO = NVL(cp_ToDate(_read_.CMFLAUTO),cp_NullValue(_read_.CMFLAUTO))
            this.w_UNIMAT = NVL(cp_ToDate(_read_.CMUNIMAT),cp_NullValue(_read_.CMUNIMAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if g_UNIMAT$"ME" OR (g_UNIMAT="C" AND this.w_UNIMAT="S")
            this.oParentObject.w_KEYSAL = SPACE(40)
          endif
          if this.w_FLAUTO="M"
            this.w_MESS = "L'articolo selezionato ha associato una classe matricola con aggiornamento manuale"
            ah_ErrorMsg(this.w_MESS,"!","")
            this.oParentObject.w_CODICE = SPACE(41)
            this.oParentObject.w_DESART = SPACE(40)
            this.oParentObject.w_ULTPROG = 0
            this.oParentObject.w_CODART = SPACE(20)
            this.oParentObject.w_KEYSAL = SPACE(40)
            this.oParentObject.w_QTAUM1 = 0
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Select from gsmd_bsz
        do vq_exec with 'gsmd_bsz',this,'_Curs_gsmd_bsz','',.f.,.t.
        if used('_Curs_gsmd_bsz')
          select _Curs_gsmd_bsz
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_ULTPROG = NVL(PR_PROGR,0)
            select _Curs_gsmd_bsz
            continue
          enddo
          use
        endif
      case this.pPROV="P"
        * --- Verifico che esista almeno una riga tipo progressivo o dettaglio vuoto
        * --- Read from CMT_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CMT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CMT_DETT_idx,2],.t.,this.CMT_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" CMT_DETT where ";
                +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CMCODICE);
                +" and CM__FUNC = "+cp_ToStrODBC("P");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                CMCODICE = this.oParentObject.w_CMCODICE;
                and CM__FUNC = "P";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          * --- non ho trovato il progressivo verifico se il dettaglio � vuoto
          * --- Read from CMT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CMT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CMT_DETT_idx,2],.t.,this.CMT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CMT_DETT where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CMCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_CMCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0
            * --- non ho trovato il progressivo verifico e il dettaglio non � vuoto
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=Ah_Msgformat("Occorre specificare una riga di tipo progressivo")
          else
            * --- Non ho righe di dettaglio
            if this.oParentObject.w_CMFLAUTO="A"
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=ah_Msgformat("Attenzione, classe senza righe di dettaglio")
            endif
          endif
          i_retcode = 'stop'
          return
        endif
        if i_Rows>1
          * --- Non sono gestite matricole con pi� di un progressivo
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_Msgformat("Non � possibile utilizzare due progressivi nella stessa classe")
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PROGMAT'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='CMT_DETT'
    this.cWorkTables[5]='CMT_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gsmd_bsz')
      use in _Curs_gsmd_bsz
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
