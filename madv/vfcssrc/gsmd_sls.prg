* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_sls                                                        *
*              Stampa lotti in scadenza                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_78]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-25                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_sls",oParentObject))

* --- Class definition
define class tgsmd_sls as StdForm
  Top    = 31
  Left   = 34

  * --- Standard Properties
  Width  = 504
  Height = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=208292713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  LOTTIART_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsmd_sls"
  cComment = "Stampa lotti in scadenza"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOSEL = space(1)
  o_TIPOSEL = space(1)
  w_DATARIF = ctod('  /  /  ')
  o_DATARIF = ctod('  /  /  ')
  w_LOFLSTAT = space(1)
  w_GIORSUC = 0
  o_GIORSUC = 0
  w_CODINI = space(20)
  w_CODARIN = space(20)
  w_CODFIN = space(20)
  w_DESCARTI = space(40)
  w_CODARFI = space(20)
  w_DESCARTF = space(40)
  w_DATACALC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SELARLO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_slsPag1","gsmd_sls",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOSEL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOSEL=space(1)
      .w_DATARIF=ctod("  /  /  ")
      .w_LOFLSTAT=space(1)
      .w_GIORSUC=0
      .w_CODINI=space(20)
      .w_CODARIN=space(20)
      .w_CODFIN=space(20)
      .w_DESCARTI=space(40)
      .w_CODARFI=space(20)
      .w_DESCARTF=space(40)
      .w_DATACALC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SELARLO=space(1)
        .w_TIPOSEL = 'L'
        .w_DATARIF = I_DATSYS
        .w_LOFLSTAT = 'D'
          .DoRTCalc(4,4,.f.)
        .w_CODINI = IIF(.w_TIPOSEL='A',SPACE(20),.w_CODINI)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODINI))
          .link_1_5('Full')
        endif
        .w_CODARIN = IIF(.w_TIPOSEL='L',SPACE(20),.w_CODARIN)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODARIN))
          .link_1_6('Full')
        endif
        .w_CODFIN = IIF(.w_TIPOSEL='A',SPACE(20),.w_CODFIN)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODFIN))
          .link_1_7('Full')
        endif
        .w_DESCARTI = IIF(.w_TIPOSEL='L',SPACE(30),.w_DESCARTI)
        .w_CODARFI = IIF(.w_TIPOSEL='L',SPACE(20),.w_CODARFI)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODARFI))
          .link_1_9('Full')
        endif
        .w_DESCARTF = IIF(.w_TIPOSEL='L',SPACE(30),.w_DESCARTF)
        .w_DATACALC = .w_DATARIF + .w_GIORSUC
        .w_OBTEST = .w_DATARIF
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPOSEL<>.w_TIPOSEL
            .w_CODINI = IIF(.w_TIPOSEL='A',SPACE(20),.w_CODINI)
          .link_1_5('Full')
        endif
        if .o_TIPOSEL<>.w_TIPOSEL
            .w_CODARIN = IIF(.w_TIPOSEL='L',SPACE(20),.w_CODARIN)
          .link_1_6('Full')
        endif
        if .o_TIPOSEL<>.w_TIPOSEL
            .w_CODFIN = IIF(.w_TIPOSEL='A',SPACE(20),.w_CODFIN)
          .link_1_7('Full')
        endif
            .w_DESCARTI = IIF(.w_TIPOSEL='L',SPACE(30),.w_DESCARTI)
        if .o_TIPOSEL<>.w_TIPOSEL
            .w_CODARFI = IIF(.w_TIPOSEL='L',SPACE(20),.w_CODARFI)
          .link_1_9('Full')
        endif
            .w_DESCARTF = IIF(.w_TIPOSEL='L',SPACE(30),.w_DESCARTF)
        if .o_GIORSUC<>.w_GIORSUC.or. .o_DATARIF<>.w_DATARIF
            .w_DATACALC = .w_DATARIF + .w_GIORSUC
        endif
        if .o_DATARIF<>.w_DATARIF
            .w_OBTEST = .w_DATARIF
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_5.enabled = this.oPgFrm.Page1.oPag.oCODINI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODARIN_1_6.enabled = this.oPgFrm.Page1.oPag.oCODARIN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODFIN_1_7.enabled = this.oPgFrm.Page1.oPag.oCODFIN_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODARFI_1_9.enabled = this.oPgFrm.Page1.oPag.oCODARFI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_5.visible=!this.oPgFrm.Page1.oPag.oCODINI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCODARIN_1_6.visible=!this.oPgFrm.Page1.oPag.oCODARIN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCODFIN_1_7.visible=!this.oPgFrm.Page1.oPag.oCODFIN_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCARTI_1_8.visible=!this.oPgFrm.Page1.oPag.oDESCARTI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODARFI_1_9.visible=!this.oPgFrm.Page1.oPag.oCODARFI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESCARTF_1_10.visible=!this.oPgFrm.Page1.oPag.oDESCARTF_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_CODINI))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oCODINI_1_5'),i_cWhere,'GSMD_ALO',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_CODINI)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_codfin) OR  .w_codini <= .w_codfin
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARIN))
          select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARIN_1_6'),i_cWhere,'GSMA_BZA',"Articoli",'GSMD_ALO.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARIN)
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESCARTI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_SELARLO = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODARIN = space(20)
      endif
      this.w_DESCARTI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_SELARLO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_codarfi) OR .w_codarin <= .w_codarfi) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST <= .w_DATOBSO)  AND .w_SELARLO $ 'SC'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore di quello finale o l'articolo � obsoleto o non � gestito a lotti.")
        endif
        this.w_CODARIN = space(20)
        this.w_DESCARTI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_SELARLO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODICE',trim(this.w_CODFIN))
          select LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_7'),i_cWhere,'GSMD_ALO',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',oSource.xKey(1))
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODICE',this.w_CODFIN)
            select LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_codini <= .w_codfin or empty(.w_codini)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore di quello iniziale")
        endif
        this.w_CODFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARFI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARFI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARFI))
          select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARFI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARFI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARFI_1_9'),i_cWhere,'GSMA_BZA',"Articoli",'GSMD_ALO.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARFI)
            select ARCODART,ARDESART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARFI = NVL(_Link_.ARCODART,space(20))
      this.w_DESCARTF = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_SELARLO = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODARFI = space(20)
      endif
      this.w_DESCARTF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_SELARLO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_codarin <= .w_codarfi or empty(.w_codarfi))  AND (EMPTY(.w_DATOBSO) OR .w_OBTEST <= .w_DATOBSO) AND .w_SELARLO $ 'SC'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale � minore di quello iniziale o l'articolo � obsoleto o non � gestito a lotti.")
        endif
        this.w_CODARFI = space(20)
        this.w_DESCARTF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_SELARLO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOSEL_1_1.RadioValue()==this.w_TIPOSEL)
      this.oPgFrm.Page1.oPag.oTIPOSEL_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATARIF_1_2.value==this.w_DATARIF)
      this.oPgFrm.Page1.oPag.oDATARIF_1_2.value=this.w_DATARIF
    endif
    if not(this.oPgFrm.Page1.oPag.oLOFLSTAT_1_3.RadioValue()==this.w_LOFLSTAT)
      this.oPgFrm.Page1.oPag.oLOFLSTAT_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORSUC_1_4.value==this.w_GIORSUC)
      this.oPgFrm.Page1.oPag.oGIORSUC_1_4.value=this.w_GIORSUC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_5.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_5.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARIN_1_6.value==this.w_CODARIN)
      this.oPgFrm.Page1.oPag.oCODARIN_1_6.value=this.w_CODARIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_7.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_7.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCARTI_1_8.value==this.w_DESCARTI)
      this.oPgFrm.Page1.oPag.oDESCARTI_1_8.value=this.w_DESCARTI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARFI_1_9.value==this.w_CODARFI)
      this.oPgFrm.Page1.oPag.oCODARFI_1_9.value=this.w_CODARFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCARTF_1_10.value==this.w_DESCARTF)
      this.oPgFrm.Page1.oPag.oDESCARTF_1_10.value=this.w_DESCARTF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_codfin) OR  .w_codini <= .w_codfin)  and not(.w_TIPOSEL='A')  and (.w_TIPOSEL<>'A')  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale")
          case   not((empty(.w_codarfi) OR .w_codarin <= .w_codarfi) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST <= .w_DATOBSO)  AND .w_SELARLO $ 'SC')  and not(.w_TIPOSEL='L')  and (.w_TIPOSEL<>'L')  and not(empty(.w_CODARIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore di quello finale o l'articolo � obsoleto o non � gestito a lotti.")
          case   not(.w_codini <= .w_codfin or empty(.w_codini))  and not(.w_TIPOSEL='A')  and (.w_TIPOSEL<>'A')  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore di quello iniziale")
          case   not((.w_codarin <= .w_codarfi or empty(.w_codarfi))  AND (EMPTY(.w_DATOBSO) OR .w_OBTEST <= .w_DATOBSO) AND .w_SELARLO $ 'SC')  and not(.w_TIPOSEL='L')  and (.w_TIPOSEL<>'L')  and not(empty(.w_CODARFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARFI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale � minore di quello iniziale o l'articolo � obsoleto o non � gestito a lotti.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOSEL = this.w_TIPOSEL
    this.o_DATARIF = this.w_DATARIF
    this.o_GIORSUC = this.w_GIORSUC
    return

enddefine

* --- Define pages as container
define class tgsmd_slsPag1 as StdContainer
  Width  = 500
  height = 221
  stdWidth  = 500
  stdheight = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOSEL_1_1 as StdCombo with uid="RQWOKYIRNI",rtseq=1,rtrep=.f.,left=106,top=23,width=76,height=21;
    , ToolTipText = "Imposta il parametro di ricerca.";
    , HelpContextID = 31849674;
    , cFormVar="w_TIPOSEL",RowSource=""+"Lotti,"+"Articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSEL_1_1.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTIPOSEL_1_1.GetRadio()
    this.Parent.oContained.w_TIPOSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSEL_1_1.SetRadio()
    this.Parent.oContained.w_TIPOSEL=trim(this.Parent.oContained.w_TIPOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSEL=='L',1,;
      iif(this.Parent.oContained.w_TIPOSEL=='A',2,;
      0))
  endfunc

  add object oDATARIF_1_2 as StdField with uid="DYUYKYXMGQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATARIF", cQueryName = "DATARIF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona gli articoli la cui data di scadenza � ad essa anteriore.",;
    HelpContextID = 33307190,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=319, Top=23


  add object oLOFLSTAT_1_3 as StdCombo with uid="BGNKVREQEB",rtseq=3,rtrep=.f.,left=106,top=54,width=98,height=21;
    , HelpContextID = 219572490;
    , cFormVar="w_LOFLSTAT",RowSource=""+"Disponibile,"+"Sospeso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLOFLSTAT_1_3.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oLOFLSTAT_1_3.GetRadio()
    this.Parent.oContained.w_LOFLSTAT = this.RadioValue()
    return .t.
  endfunc

  func oLOFLSTAT_1_3.SetRadio()
    this.Parent.oContained.w_LOFLSTAT=trim(this.Parent.oContained.w_LOFLSTAT)
    this.value = ;
      iif(this.Parent.oContained.w_LOFLSTAT=='D',1,;
      iif(this.Parent.oContained.w_LOFLSTAT=='S',2,;
      0))
  endfunc

  add object oGIORSUC_1_4 as StdField with uid="NVTXWAHIFB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GIORSUC", cQueryName = "GIORSUC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica il numero di giorni per l'intervallo di ricerca.",;
    HelpContextID = 236778086,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=319, Top=53, cSayPict='"999"', cGetPict='"999"'

  add object oCODINI_1_5 as StdField with uid="CTEKAKABCR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Codice lotto di inizio selezione",;
    HelpContextID = 238860250,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=84, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOSEL<>'A')
    endwith
   endif
  endfunc

  func oCODINI_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='A')
    endwith
  endfunc

  func oCODINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oCODINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'',this.parent.oContained
  endproc
  proc oCODINI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LOCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODARIN_1_6 as StdField with uid="EKFNJTBTDH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODARIN", cQueryName = "CODARIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore di quello finale o l'articolo � obsoleto o non � gestito a lotti.",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 235190234,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=85, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARIN"

  func oCODARIN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOSEL<>'L')
    endwith
   endif
  endfunc

  func oCODARIN_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='L')
    endwith
  endfunc

  func oCODARIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMD_ALO.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODARIN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARIN
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_7 as StdField with uid="XZUTBFYFKE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore di quello iniziale",;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 160413658,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=110, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOSEL<>'A')
    endwith
   endif
  endfunc

  func oCODFIN_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='A')
    endwith
  endfunc

  func oCODFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LOTTIART','*','LOCODICE',cp_AbsName(this.parent,'oCODFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'',this.parent.oContained
  endproc
  proc oCODFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LOCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCARTI_1_8 as StdField with uid="PFYKITTYLP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCARTI", cQueryName = "DESCARTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 166604415,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=264, Top=85, InputMask=replicate('X',40)

  func oDESCARTI_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='L')
    endwith
  endfunc

  add object oCODARFI_1_9 as StdField with uid="KEMWPOQHRR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODARFI", cQueryName = "CODARFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale � minore di quello iniziale o l'articolo � obsoleto o non � gestito a lotti.",;
    ToolTipText = "Codice lotto di fine selezione",;
    HelpContextID = 251349030,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=111, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARFI"

  func oCODARFI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOSEL<>'L')
    endwith
   endif
  endfunc

  func oCODARFI_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='L')
    endwith
  endfunc

  func oCODARFI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARFI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARFI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARFI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMD_ALO.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODARFI_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODARFI
     i_obj.ecpSave()
  endproc

  add object oDESCARTF_1_10 as StdField with uid="MPIGRJOZTW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCARTF", cQueryName = "DESCARTF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 166604412,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=264, Top=113, InputMask=replicate('X',40)

  func oDESCARTF_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='L')
    endwith
  endfunc


  add object oObj_1_24 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=143, width=386,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 238140390


  add object oBtn_1_25 as StdButton with uid="GHEVIDFOUV",left=392, top=169, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 201932326;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="GNIBZHELYY",left=443, top=169, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 200975290;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="IXSBCEPEUL",Visible=.t., Left=19, Top=89,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL = 'A')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="JMEAXLYTOP",Visible=.t., Left=19, Top=117,;
    Alignment=1, Width=83, Height=18,;
    Caption="A lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='A')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="RWIYQLVWDV",Visible=.t., Left=58, Top=58,;
    Alignment=1, Width=44, Height=17,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KUHNGVFQIK",Visible=.t., Left=16, Top=145,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EUCDHDDUHI",Visible=.t., Left=201, Top=24,;
    Alignment=1, Width=114, Height=18,;
    Caption="Data di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HWRXAPEDAS",Visible=.t., Left=243, Top=57,;
    Alignment=1, Width=74, Height=18,;
    Caption="Giorni succ.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="YNAGADSUPR",Visible=.t., Left=12, Top=25,;
    Alignment=1, Width=90, Height=17,;
    Caption="Tipo selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VVLXEPEGCW",Visible=.t., Left=19, Top=88,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL = 'L')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="UFSGPQSVXU",Visible=.t., Left=19, Top=116,;
    Alignment=1, Width=83, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOSEL='L')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_sls','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
