* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bzl                                                        *
*              Seleziona lotto da inserire                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_57]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-06                                                      *
* Last revis.: 2011-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bzl",oParentObject)
return(i_retval)

define class tgsmd_bzl as StdBatch
  * --- Local variables
  w_FLRISE = space(1)
  w_CODFOR = space(15)
  w_DATREG = ctod("  /  /  ")
  w_LOCODICE = space(20)
  w_CODART = space(20)
  w_LOCODUBI = space(20)
  w_LOQTAESI = 0
  w_CODMAG = space(5)
  w_CODLOT = space(20)
  w_CODUBI = space(20)
  w_LODATSCA = ctod("  /  /  ")
  w_SERRIF = space(10)
  w_MLCODICE = space(20)
  w_ROWRIF = 0
  w_FLLOTT = space(1)
  w_F2LOTT = space(1)
  w_UTDC = ctod("  /  /  ")
  w_QTAMOV = 0
  w_CAUCOL = space(5)
  w_CAUMAG = space(5)
  w_FLAVAL = space(1)
  w_OKSCA = space(1)
  * --- WorkFile variables
  LOTTIART_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona il Lotto da Inserire (da GSMA_MVM, GSVE_MDV,GSAC_MDV, GSCO_MLO)
    this.w_FLRISE = " "
    this.w_SERRIF = SPACE(10)
    this.w_ROWRIF = 0
    this.w_MLCODICE = Space ( 20 )
    * --- Filtro Data Scadenza Lotto Se Movimento di Reso (documentale) 
    *     -Movimento Venduto e Carico
    *     -Movimento Acquistato e Scarico
    this.w_OKSCA = "F"
    WITH this.oParentObject
    do case
      case this.oParentObject.w_FLPRG="M"
        * --- Siamo entrati dai Movimenti di Magazzino
        this.w_CODART = .w_MMCODART
        this.w_CODFOR = IIF(.w_MMTIPCON="F" And Empty(.w_MMCODMAT), .w_MMCODCON, SPACE(15))
        this.w_DATREG = .w_MMDATREG
        this.w_CODMAG = .w_MMCODMAG
        this.w_CODLOT = .w_MMCODLOT
        this.w_CODUBI = .w_MMCODUBI
        this.w_FLLOTT = .w_MMFLCASC
        this.w_F2LOTT = .w_MMF2CASC
        this.w_CAUMAG = .w_MMCAUMAG
        this.w_CAUCOL = .w_MMCAUCOL
      case this.oParentObject.w_FLPRG="D"
        * --- Siamo entrati dai Documenti
        this.w_CODART = .w_MVCODART
        this.w_FLRISE = .w_FLRRIF
        this.w_SERRIF = .w_MVSERRIF
        this.w_ROWRIF = .w_MVROWRIF
        this.w_CODFOR = IIF(.w_MVTIPCON="F" And Empty(.w_MVCODMAT),.w_MVCODCON, SPACE(15))
        this.w_DATREG = MAX(.w_MVDATREG, .w_MVDATEVA)
        this.w_CODMAG = .w_MVCODMAG
        this.w_CODLOT = .w_MVCODLOT
        this.w_CODUBI = .w_MVCODUBI
        this.w_FLLOTT = .w_MVFLCASC
        this.w_F2LOTT = .w_MVF2CASC
        this.w_CAUMAG = .w_MVCAUMAG
        this.w_CAUCOL = .w_MVCAUCOL
      case this.oParentObject.w_FLPRG="A"
        * --- Siamo entrati dall'Aggiornamento differito
        this.w_CODART = .w_CODART
        this.w_FLRISE = .w_SFLRRIF
        this.w_SERRIF = .w_SSERRIF
        this.w_ROWRIF = .w_SROWRIF
        this.w_CODFOR = IIF(.w_STIPCON="F" And Empty(.w_SLMAT),.w_SCODCON, SPACE(15))
        this.w_DATREG = .w_SDATREG
        this.w_CODMAG = .w_SLMAG
        this.w_CODLOT = .w_SPCODLOT
        this.w_CODUBI = .w_SPCODUBI
        this.w_FLLOTT = .w_SFLLOTT
        this.w_F2LOTT = .w_SF2LOTT
      case this.oParentObject.w_FLPRG="P"
        * --- Siamo entrati nell'inserimento da dispositivo
        this.w_CODART = .w_CODART
        this.w_FLRISE = .w_FLRRIF
        this.w_SERRIF = .w_GPSERRIF
        this.w_ROWRIF = .w_GPROWRIF
        this.w_CODFOR = .w_CODCON
        this.w_DATREG = .w_DATREG
        this.w_CODMAG = .w_GPCODMAG
        this.w_CODLOT = .w_GPCODLOT
        this.w_CODUBI = .w_GPCODUBI
        this.w_FLLOTT = .w_MxFLLOTT
        this.w_F2LOTT = " "
        this.w_CAUMAG = .w_CAUMAG
        this.w_CAUCOL = .w_CAUCOL
      case this.oParentObject.w_FLPRG="V"
        * --- Siamo entrati dal POS
        this.w_CODART = .w_MDCODART
        * --- Da POS Flrise � MDFLCASC Carico/Scarico che � sempre '-' tranne nel caso sia un Reso
        this.w_FLRISE = .w_MDFLCASC
        this.w_CODFOR = .w_CODCLI
        this.w_DATREG = .w_MDDATREG
        this.w_CODMAG = .w_MDCODMAG
        this.w_CODLOT = .w_MDCODLOT
        this.w_CODUBI = .w_MDCODUBI
        this.w_FLLOTT = .w_MDFLCASC
        this.w_CAUMAG = Space(5)
        this.w_CAUCOL = Space(5)
        * --- Lanciata da POS considero sempre causale di Scarico se non � un RESO
        this.w_OKSCA = IIF(this.w_FLRISE="+","T","F")
      case this.oParentObject.w_FLPRG="L"
        * --- Consuntivazione materiali da dichiarazioni di produzione
        this.w_CODART = .w_CODART
        this.w_CODFOR = this.w_CODFOR
        this.w_DATREG = .w_DATREG
        this.w_CODMAG = .w_CLCODMAG
        this.w_CODLOT = .w_CLCODLOT
        this.w_CODUBI = .w_CLCODUBI
        this.w_FLLOTT = iif(.w_CLFLCASC="S","-","+")
    endcase
    ENDWITH
    this.w_LOCODICE = SPACE(20)
    this.w_LOCODUBI = SPACE(20)
    this.w_LOQTAESI = 0
    this.w_LODATSCA = cp_CharToDate("  -  -  ")
    this.w_UTDC = cp_CharToDate("  -  -  ")
    * --- Seleziona Viste del Form
    if Not Empty(this.w_CAUMAG)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLAVAL"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLAVAL;
          from (i_cTable) where;
              CMCODICE = this.w_CAUMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OKSCA = IIF((this.w_FLAVAL="A" and this.w_FLLOTT="-") or (this.w_FLAVAL="V" and this.w_FLLOTT="+") ,"T","F")
    endif
    if Not Empty(this.w_CAUCOL) AND this.w_OKSCA="F"
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLAVAL"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLAVAL;
          from (i_cTable) where;
              CMCODICE = this.w_CAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OKSCA = IIF((this.w_FLAVAL="A" and this.w_F2LOTT="-") or (this.w_FLAVAL="V" and this.w_F2LOTT="+") ,"T","F")
    endif
    do case
      case (this.w_FLLOTT="+" ) OR (EMPTY(this.w_FLLOTT) AND this.w_F2LOTT="+")
        * --- Carica il Lotto
        vx_exec("..\MADV\EXE\QUERY\GSMD_QLC.VZM",this)
      case this.oParentObject.w_DISLOT$"NC"
        * --- No Disponibilit� Lotto
        vx_exec("..\MADV\EXE\QUERY\GSMD_QLN.VZM",this)
      otherwise
        * --- Scarica il Lotto
        if this.w_FLRISE="+" AND NOT EMPTY(this.w_SERRIF) AND this.w_ROWRIF<>0 
          * --- Carico da Fornitore (effettuo filtro su Questo)
          vx_exec("..\MADV\EXE\QUERY\GSMD_QLR.VZM",this)
        else
          vx_exec("..\MADV\EXE\QUERY\GSMD_QLO.VZM",this)
        endif
    endcase
    if g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S"
      this.w_CODUBI = NVL(this.w_LOCODUBI, SPACE(20))
    endif
    if NOT EMPTY(NVL(this.w_LOCODICE," "))
      do case
        case this.oParentObject.w_FLPRG="M"
          this.oParentObject.w_MMCODLOT = this.w_LOCODICE
          this.oParentObject.w_MMCODUBI = this.w_CODUBI
        case this.oParentObject.w_FLPRG="D"
          this.oParentObject.w_MVCODLOT = this.w_LOCODICE
          this.oParentObject.w_MVCODUBI = this.w_CODUBI
        case this.oParentObject.w_FLPRG="A"
          this.oParentObject.w_SPCODLOT = this.w_LOCODICE
          this.oParentObject.w_SPCODUBI = this.w_CODUBI
        case this.oParentObject.w_FLPRG="P"
          this.oParentObject.w_GPCODLOT = this.w_LOCODICE
          this.oParentObject.w_GPCODUBI = this.w_CODUBI
          THIS.oparentobject.NOTIFYEVENT( "w_GPCODLOT Changed" )
        case this.oParentObject.w_FLPRG="V"
          this.oParentObject.w_MDCODLOT = this.w_LOCODICE
          this.oParentObject.w_MDCODUBI = this.w_CODUBI
        case this.oParentObject.w_FLPRG="L"
          this.oParentObject.w_CLLOTART = this.w_LOCODICE
          this.oParentObject.w_CLCODUBI = this.w_CODUBI
          this.oParentObject.w_QTAESI = this.w_LOQTAESI
          THIS.oparentobject.NOTIFYEVENT( "w_CLLOTART Changed" )
      endcase
      * --- Read from LOTTIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LOTTIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LOFLSTAT"+;
          " from "+i_cTable+" LOTTIART where ";
              +"LOCODICE = "+cp_ToStrODBC(this.w_LOCODICE);
              +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LOFLSTAT;
          from (i_cTable) where;
              LOCODICE = this.w_LOCODICE;
              and LOCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
      this.oParentObject.w_LOTZOOM = .T.
      if NOT EMPTY(NVL(this.w_LODATSCA, cp_CharToDate("  -  -  "))) AND this.w_LODATSCA<this.w_DATREG
        ah_ErrorMsg("Il lotto selezionato � scaduto",,"")
      endif
      * --- Notifica Riga Variata
      SELECT (this.oParentObject.cTrsName)
      if I_SRV=" " AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    else
      * --- Mi dice che il Lotto � stato caricato da Zoom (quindi non devo fare alcuni Check)
      this.oParentObject.w_LOTZOOM = .F.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='CAM_AGAZ'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
