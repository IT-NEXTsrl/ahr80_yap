* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_ktm                                                        *
*              Tracciabilit� matricole                                         *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_194]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-17                                                      *
* Last revis.: 2014-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsmd_ktm
* variabili da passare al report come selezioni
Public L_CodIni,L_CodFin,L_presente,L_lotini,L_LotFin
Public L_MatIni,L_MatFin,L_Cli_ini,L_Cli_Fin
Public L_For_ini,L_For_Fin,L_CodCom,L_CodMag,L_Ubicaz,L_CauMag,L_pos_ini,L_pos_fin
* --- Fine Area Manuale
return(createobject("tgsmd_ktm",oParentObject))

* --- Class definition
define class tgsmd_ktm as StdForm
  Top    = 1
  Left   = 8

  * --- Standard Properties
  Width  = 750
  Height = 423+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-06"
  HelpContextID=185971863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=56

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  LOTTIART_IDX = 0
  MATRICOL_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  CONTI_IDX = 0
  CAM_AGAZ_IDX = 0
  CAN_TIER_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsmd_ktm"
  cComment = "Tracciabilit� matricole"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_KEYSINI = space(20)
  w_KEYSFIN = space(20)
  w_PRESENTE = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_LOTINI = space(20)
  o_LOTINI = space(20)
  w_LOTFIN = space(20)
  w_MATINI_M = space(40)
  o_MATINI_M = space(40)
  w_MATFIN_M = space(40)
  o_MATFIN_M = space(40)
  w_MATINI_A = space(40)
  o_MATINI_A = space(40)
  w_MATFIN_A = space(40)
  o_MATFIN_A = space(40)
  w_CLI_INI = space(15)
  o_CLI_INI = space(15)
  w_NOINT = space(1)
  o_NOINT = space(1)
  w_CLI_FIN = space(15)
  o_CLI_FIN = space(15)
  w_FOR_INI = space(15)
  o_FOR_INI = space(15)
  w_FOR_FIN = space(15)
  o_FOR_FIN = space(15)
  w_CODCOM = space(15)
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_UBICAZ = space(20)
  w_CAUMAG = space(5)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_APPART = space(20)
  w_UBDESCRI = space(40)
  w_FLUBIC = space(1)
  w_ARTINI = space(20)
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_CODART = space(20)
  w_CODMAT = space(40)
  w_DESCLI = space(40)
  w_DESFOR = space(40)
  w_C = space(1)
  w_F = space(1)
  w_DESCLIFIN = space(40)
  w_DESFORFIN = space(40)
  w_PC = space(1)
  w_PF = space(1)
  w_DESCAU = space(35)
  w_TOTCAR = 0
  w_TOTSCA = 0
  w_DESMAG = space(30)
  w_DESCAN = space(30)
  w_DTOBSO = ctod('  /  /  ')
  w_MATINI = space(40)
  w_MATFIN = space(40)
  w_NUMROW = 0
  w_ZCAUMAG = space(5)
  w_CMDESCRI = space(35)
  w_FASE = .F.
  w_POSINI = space(15)
  w_POSFIN = space(15)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_ktmPag1","gsmd_ktm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsmd_ktmPag2","gsmd_ktm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tracciabilit�")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(2).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='MATRICOL'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='CAM_AGAZ'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='ART_ICOL'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_KEYSINI=space(20)
      .w_KEYSFIN=space(20)
      .w_PRESENTE=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_LOTINI=space(20)
      .w_LOTFIN=space(20)
      .w_MATINI_M=space(40)
      .w_MATFIN_M=space(40)
      .w_MATINI_A=space(40)
      .w_MATFIN_A=space(40)
      .w_CLI_INI=space(15)
      .w_NOINT=space(1)
      .w_CLI_FIN=space(15)
      .w_FOR_INI=space(15)
      .w_FOR_FIN=space(15)
      .w_CODCOM=space(15)
      .w_CODMAG=space(5)
      .w_UBICAZ=space(20)
      .w_CAUMAG=space(5)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_APPART=space(20)
      .w_UBDESCRI=space(40)
      .w_FLUBIC=space(1)
      .w_ARTINI=space(20)
      .w_SERIALE=space(10)
      .w_NUMRIF=0
      .w_CODART=space(20)
      .w_CODMAT=space(40)
      .w_DESCLI=space(40)
      .w_DESFOR=space(40)
      .w_C=space(1)
      .w_F=space(1)
      .w_DESCLIFIN=space(40)
      .w_DESFORFIN=space(40)
      .w_PC=space(1)
      .w_PF=space(1)
      .w_DESCAU=space(35)
      .w_TOTCAR=0
      .w_TOTSCA=0
      .w_DESMAG=space(30)
      .w_DESCAN=space(30)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MATINI=space(40)
      .w_MATFIN=space(40)
      .w_NUMROW=0
      .w_ZCAUMAG=space(5)
      .w_CMDESCRI=space(35)
      .w_FASE=.f.
      .w_POSINI=space(15)
      .w_POSFIN=space(15)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_2('Full')
        endif
        .w_KEYSINI = IIF(g_UNIMAT<>'M',.w_CODINI,SPACE(20))
        .w_KEYSFIN = IIF(g_UNIMAT<>'M',.w_CODFIN,SPACE(20))
        .w_PRESENTE = 'T'
          .DoRTCalc(6,7,.f.)
        .w_LOTINI = Space(20)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_LOTINI))
          .link_1_8('Full')
        endif
        .w_LOTFIN = IIF( Not Empty(.w_LOTINI) , .w_LOTINI  ,Space(40))
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_LOTFIN))
          .link_1_9('Full')
        endif
        .w_MATINI_M = Space(40)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MATINI_M))
          .link_1_10('Full')
        endif
        .w_MATFIN_M = IIF( Not Empty(.w_MATINI_M) , .w_MATINI_M  ,Space(40))
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MATFIN_M))
          .link_1_11('Full')
        endif
        .w_MATINI_A = Space(40)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MATINI_A))
          .link_1_12('Full')
        endif
        .w_MATFIN_A = IIF( Not Empty(.w_MATINI_A) , .w_MATINI_A  ,Space(40))
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_MATFIN_A))
          .link_1_13('Full')
        endif
        .w_CLI_INI = SPACE(15)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CLI_INI))
          .link_1_14('Full')
        endif
        .w_NOINT = ' '
        .w_CLI_FIN = SPACE(15)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CLI_FIN))
          .link_1_16('Full')
        endif
        .w_FOR_INI = SPACE(15)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_FOR_INI))
          .link_1_18('Full')
        endif
        .w_FOR_FIN = SPACE(15)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_FOR_FIN))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODCOM))
          .link_1_20('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODMAG))
          .link_1_21('Full')
        endif
        .w_UBICAZ = Space(20)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_UBICAZ))
          .link_1_22('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CAUMAG))
          .link_1_23('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(26,30,.f.)
        .w_SERIALE = .w_Zoom.getVar('SERIAL')
        .w_NUMRIF = .w_Zoom.getVar('NUMRIF')
        .w_CODART = .w_Zoom.getVar('CODART')
      .oPgFrm.Page2.oPag.Zoom.Calculate()
        .w_CODMAT = .w_Zoom.getVar('CODMAT')
          .DoRTCalc(35,36,.f.)
        .w_C = 'C'
        .w_F = 'F'
          .DoRTCalc(39,40,.f.)
        .w_PC = IIF( Empty(.w_CLI_INI+ .w_CLI_FIN) AND Empty( .w_FOR_INI+.w_FOR_FIN ),' ','C' )
        .w_PF = IIF( Empty( .w_FOR_INI+.w_FOR_FIN ) AND Empty(.w_CLI_INI+ .w_CLI_FIN) ,' ','F' )
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .DoRTCalc(43,48,.f.)
        .w_MATINI = Left( Alltrim( .w_MATINI_M+.w_MATINI_A ) + Space ( 40 ) , 40 )
        .w_MATFIN = Left( Alltrim( .w_MATFIN_M+.w_MATFIN_A ) + Space ( 40 ) , 40 )
        .w_NUMROW = .w_Zoom.getVar('MTROWNUM')
        .w_ZCAUMAG = .w_Zoom.getVar('CAUMAG')
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_ZCAUMAG))
          .link_2_19('Full')
        endif
          .DoRTCalc(53,53,.f.)
        .w_FASE = .T.
      .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
    endwith
    this.DoRTCalc(55,56,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_2('Full')
        endif
        if .o_CODINI<>.w_CODINI
            .w_KEYSINI = IIF(g_UNIMAT<>'M',.w_CODINI,SPACE(20))
        endif
        if .o_CODFIN<>.w_CODFIN
            .w_KEYSFIN = IIF(g_UNIMAT<>'M',.w_CODFIN,SPACE(20))
        endif
        .DoRTCalc(5,7,.t.)
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI
            .w_LOTINI = Space(20)
          .link_1_8('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI.or. .o_LOTINI<>.w_LOTINI
            .w_LOTFIN = IIF( Not Empty(.w_LOTINI) , .w_LOTINI  ,Space(40))
          .link_1_9('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI
            .w_MATINI_M = Space(40)
          .link_1_10('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI.or. .o_MATINI_M<>.w_MATINI_M
            .w_MATFIN_M = IIF( Not Empty(.w_MATINI_M) , .w_MATINI_M  ,Space(40))
          .link_1_11('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI
            .w_MATINI_A = Space(40)
          .link_1_12('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI.or. .o_MATINI_A<>.w_MATINI_A
            .w_MATFIN_A = IIF( Not Empty(.w_MATINI_A) , .w_MATINI_A  ,Space(40))
          .link_1_13('Full')
        endif
        if .o_NOINT<>.w_NOINT
            .w_CLI_INI = SPACE(15)
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_NOINT<>.w_NOINT
            .w_CLI_FIN = SPACE(15)
          .link_1_16('Full')
        endif
        if .o_NOINT<>.w_NOINT
            .w_FOR_INI = SPACE(15)
          .link_1_18('Full')
        endif
        if .o_NOINT<>.w_NOINT
            .w_FOR_FIN = SPACE(15)
          .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.t.)
        if .o_CODMAG<>.w_CODMAG
            .w_UBICAZ = Space(20)
          .link_1_22('Full')
        endif
        .DoRTCalc(22,30,.t.)
            .w_SERIALE = .w_Zoom.getVar('SERIAL')
            .w_NUMRIF = .w_Zoom.getVar('NUMRIF')
            .w_CODART = .w_Zoom.getVar('CODART')
        .oPgFrm.Page2.oPag.Zoom.Calculate()
            .w_CODMAT = .w_Zoom.getVar('CODMAT')
        .DoRTCalc(35,40,.t.)
        if .o_CLI_INI<>.w_CLI_INI.or. .o_CLI_FIN<>.w_CLI_FIN.or. .o_FOR_FIN<>.w_FOR_FIN.or. .o_FOR_INI<>.w_FOR_INI
            .w_PC = IIF( Empty(.w_CLI_INI+ .w_CLI_FIN) AND Empty( .w_FOR_INI+.w_FOR_FIN ),' ','C' )
        endif
        if .o_FOR_INI<>.w_FOR_INI.or. .o_FOR_FIN<>.w_FOR_FIN.or. .o_CLI_INI<>.w_CLI_INI.or. .o_CLI_FIN<>.w_CLI_FIN
            .w_PF = IIF( Empty( .w_FOR_INI+.w_FOR_FIN ) AND Empty(.w_CLI_INI+ .w_CLI_FIN) ,' ','F' )
        endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(43,48,.t.)
        if .o_MATINI_A<>.w_MATINI_A.or. .o_MATINI_M<>.w_MATINI_M
            .w_MATINI = Left( Alltrim( .w_MATINI_M+.w_MATINI_A ) + Space ( 40 ) , 40 )
        endif
        if .o_MATFIN_A<>.w_MATFIN_A.or. .o_MATFIN_M<>.w_MATFIN_M
            .w_MATFIN = Left( Alltrim( .w_MATFIN_M+.w_MATFIN_A ) + Space ( 40 ) , 40 )
        endif
            .w_NUMROW = .w_Zoom.getVar('MTROWNUM')
            .w_ZCAUMAG = .w_Zoom.getVar('CAUMAG')
          .link_2_19('Full')
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
        * --- Area Manuale = Calculate
        * --- gsmd_ktm
        * variabili da passare al report come selezioni
        with this
        L_CodIni=.w_CODINI
        L_CodFin=.w_CODFIN
        L_presente=.w_PRESENTE
        L_lotini=.w_LOTINI
        L_LotFin=.w_LOTFIN
        L_MatIni=.w_MATINI
        L_MatFin=.w_MATFIN
        L_Cli_ini=.w_CLI_INI
        L_Cli_Fin=.w_CLI_FIN
        L_For_ini=.w_FOR_INI
        L_For_Fin=.w_FOR_fIN
        L_CodCom=.w_CODCOM
        L_CodMag=.w_CODMAG
        L_Ubicaz=.w_UBICAZ
        L_CauMag=.w_CAUMAG
        L_pos_ini=.w_POSINI
        L_pos_fin=.w_POSFIN
        Endwith
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(53,56,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTINI_1_8.enabled = this.oPgFrm.Page1.oPag.oLOTINI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oLOTFIN_1_9.enabled = this.oPgFrm.Page1.oPag.oLOTFIN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oMATINI_M_1_10.enabled = this.oPgFrm.Page1.oPag.oMATINI_M_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.enabled = this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCLI_INI_1_14.enabled = this.oPgFrm.Page1.oPag.oCLI_INI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCLI_FIN_1_16.enabled = this.oPgFrm.Page1.oPag.oCLI_FIN_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFOR_INI_1_18.enabled = this.oPgFrm.Page1.oPag.oFOR_INI_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFOR_FIN_1_19.enabled = this.oPgFrm.Page1.oPag.oFOR_FIN_1_19.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMATINI_M_1_10.visible=!this.oPgFrm.Page1.oPag.oMATINI_M_1_10.mHide()
    this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.visible=!this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.mHide()
    this.oPgFrm.Page1.oPag.oMATINI_A_1_12.visible=!this.oPgFrm.Page1.oPag.oMATINI_A_1_12.mHide()
    this.oPgFrm.Page1.oPag.oMATFIN_A_1_13.visible=!this.oPgFrm.Page1.oPag.oMATFIN_A_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_17.visible=!this.oPgFrm.Page1.oPag.oBtn_1_17.mHide()
    this.oPgFrm.Page1.oPag.oUBICAZ_1_22.visible=!this.oPgFrm.Page1.oPag.oUBICAZ_1_22.mHide()
    this.oPgFrm.Page1.oPag.oUBDESCRI_1_38.visible=!this.oPgFrm.Page1.oPag.oUBDESCRI_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsmd_ktm
    * cambio pagina se l'evento � l'Esegui
    * sposto il focus sulla pagina dello Zoom
    if cEvent='Esegui'
    This.oPgFrm.ActivePage=2
    This.w_Zoom.SetFocus()
    This.oPgFrm.Click()
    * calcolo il totale degli scarichi
    Select (This.w_Zoom.cCursor)
    Count For Not Empty( MAGCAR ) And Not (FLSCAR='R' Or FLCARI='R') TO This.w_TOTCAR
    Count For Not Empty( MAGSCA ) And Not (FLSCAR='R' Or FLCARI='R') TO This.w_TOTSCA
    Go top
    This.SetControlsValue()
    This.mCalc(.f.)
    This.oPgFrm.Page2.oPag.Zoom.REFRESH()
    Endif
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'GSMA_BZA',"Codici articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKARTI('M',.w_CODINI) AND ((empty(.w_codfin)) OR  (.w_codini <= .w_codfin)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o maggiore di quello finale oppure obsoleto")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_2'),i_cWhere,'GSMA_BZA',"Codici articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKARTI('M',.w_CODFIN) AND ((.w_codini <= .w_codfin) or (empty(.w_codfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o minore di quello iniziale oppure obsoleto")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTINI)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODINI;
                     ,'LOCODICE',trim(this.w_LOTINI))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTINI)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTINI) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTINI_1_8'),i_cWhere,'GSAR_ALO',"Lotti",'LOTTI.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTINI);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODINI;
                       ,'LOCODICE',this.w_LOTINI)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTINI = NVL(_Link_.LOCODICE,space(20))
      this.w_APPART = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTINI = space(20)
      endif
      this.w_APPART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_LOTINI) OR .w_APPART=.w_CODINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTINI = space(20)
        this.w_APPART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTFIN)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODFIN);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODFIN;
                     ,'LOCODICE',trim(this.w_LOTFIN))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTFIN)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTFIN) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTFIN_1_9'),i_cWhere,'GSAR_ALO',"Lotti",'LOTTI.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTFIN);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODFIN;
                       ,'LOCODICE',this.w_LOTFIN)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTFIN = NVL(_Link_.LOCODICE,space(20))
      this.w_APPART = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTFIN = space(20)
      endif
      this.w_APPART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_LOTFIN) OR .w_APPART=.w_CODFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTFIN = space(20)
        this.w_APPART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI_M
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI_M) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI_M)+"%");
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);

          i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMKEYSAL,AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMKEYSAL',this.w_KEYSINI;
                     ,'AMCODICE',trim(this.w_MATINI_M))
          select AMKEYSAL,AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMKEYSAL,AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI_M)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI_M) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(oSource.parent,'oMATINI_M_1_10'),i_cWhere,'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYSINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',oSource.xKey(1);
                       ,'AMCODICE',oSource.xKey(2))
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI_M)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI_M);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_KEYSINI;
                       ,'AMCODICE',this.w_MATINI_M)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI_M = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI_M = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI_M Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN_M
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN_M) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN_M)+"%");
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSFIN);

          i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMKEYSAL,AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMKEYSAL',this.w_KEYSFIN;
                     ,'AMCODICE',trim(this.w_MATFIN_M))
          select AMKEYSAL,AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMKEYSAL,AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN_M)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN_M) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_M_1_11'),i_cWhere,'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYSFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',oSource.xKey(1);
                       ,'AMCODICE',oSource.xKey(2))
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN_M)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN_M);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_KEYSFIN;
                       ,'AMCODICE',this.w_MATFIN_M)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN_M = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN_M = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN_M Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI_A
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI_A)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATINI_A))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI_A)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI_A) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATINI_A_1_12'),i_cWhere,'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATINI_A)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI_A = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI_A = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN_A
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN_A)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATFIN_A))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN_A)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN_A) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_A_1_13'),i_cWhere,'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATFIN_A)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN_A = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN_A = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLI_INI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLI_INI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLI_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_CLI_INI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLI_INI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLI_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLI_INI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLI_INI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLI_INI_1_14'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLI_INI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLI_INI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_CLI_INI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLI_INI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLI_INI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLI_INI = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLI_INI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLI_FIN
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLI_FIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLI_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_CLI_FIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLI_FIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLI_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLI_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLI_FIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLI_FIN_1_16'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLI_FIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLI_FIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_CLI_FIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLI_FIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLIFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLI_FIN = space(15)
      endif
      this.w_DESCLIFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLI_FIN = space(15)
        this.w_DESCLIFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLI_FIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FOR_INI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FOR_INI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FOR_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_F;
                     ,'ANCODICE',trim(this.w_FOR_INI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FOR_INI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FOR_INI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FOR_INI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_F);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FOR_INI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFOR_INI_1_18'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_F<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_F);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FOR_INI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FOR_INI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_F;
                       ,'ANCODICE',this.w_FOR_INI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FOR_INI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FOR_INI = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FOR_INI = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FOR_INI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FOR_FIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FOR_FIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FOR_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_F;
                     ,'ANCODICE',trim(this.w_FOR_FIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FOR_FIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FOR_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FOR_FIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_F);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FOR_FIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFOR_FIN_1_19'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_F<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_F);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FOR_FIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FOR_FIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_F);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_F;
                       ,'ANCODICE',this.w_FOR_FIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FOR_FIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFORFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FOR_FIN = space(15)
      endif
      this.w_DESFORFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FOR_FIN = space(15)
        this.w_DESFORFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FOR_FIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_20'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM = space(15)
        this.w_DESCAN = space(30)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_21'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLUBIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UBICAZ
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UBICAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_UBICAZ)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAG;
                     ,'UBCODICE',trim(this.w_UBICAZ))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UBICAZ)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UBICAZ) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oUBICAZ_1_22'),i_cWhere,'',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UBICAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_UBICAZ);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_UBICAZ)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UBICAZ = NVL(_Link_.UBCODICE,space(20))
      this.w_UBDESCRI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_UBICAZ = space(20)
      endif
      this.w_UBDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UBICAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMAG
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CAUMAG))
          select CMCODICE,CMDESCRI,CMDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_CAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_CAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCAUMAG_1_23'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUMAG)
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMAG = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_CAUMAG = space(5)
        this.w_DESCAU = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCAUMAG
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_ZCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_ZCAUMAG)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_CMDESCRI = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ZCAUMAG = space(5)
      endif
      this.w_CMDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_2.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_2.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRESENTE_1_5.RadioValue()==this.w_PRESENTE)
      this.oPgFrm.Page1.oPag.oPRESENTE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_6.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_6.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_7.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_7.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTINI_1_8.value==this.w_LOTINI)
      this.oPgFrm.Page1.oPag.oLOTINI_1_8.value=this.w_LOTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTFIN_1_9.value==this.w_LOTFIN)
      this.oPgFrm.Page1.oPag.oLOTFIN_1_9.value=this.w_LOTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_M_1_10.value==this.w_MATINI_M)
      this.oPgFrm.Page1.oPag.oMATINI_M_1_10.value=this.w_MATINI_M
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.value==this.w_MATFIN_M)
      this.oPgFrm.Page1.oPag.oMATFIN_M_1_11.value=this.w_MATFIN_M
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_A_1_12.value==this.w_MATINI_A)
      this.oPgFrm.Page1.oPag.oMATINI_A_1_12.value=this.w_MATINI_A
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_A_1_13.value==this.w_MATFIN_A)
      this.oPgFrm.Page1.oPag.oMATFIN_A_1_13.value=this.w_MATFIN_A
    endif
    if not(this.oPgFrm.Page1.oPag.oCLI_INI_1_14.value==this.w_CLI_INI)
      this.oPgFrm.Page1.oPag.oCLI_INI_1_14.value=this.w_CLI_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINT_1_15.RadioValue()==this.w_NOINT)
      this.oPgFrm.Page1.oPag.oNOINT_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLI_FIN_1_16.value==this.w_CLI_FIN)
      this.oPgFrm.Page1.oPag.oCLI_FIN_1_16.value=this.w_CLI_FIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFOR_INI_1_18.value==this.w_FOR_INI)
      this.oPgFrm.Page1.oPag.oFOR_INI_1_18.value=this.w_FOR_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oFOR_FIN_1_19.value==this.w_FOR_FIN)
      this.oPgFrm.Page1.oPag.oFOR_FIN_1_19.value=this.w_FOR_FIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_20.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_20.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_21.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_21.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oUBICAZ_1_22.value==this.w_UBICAZ)
      this.oPgFrm.Page1.oPag.oUBICAZ_1_22.value=this.w_UBICAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMAG_1_23.value==this.w_CAUMAG)
      this.oPgFrm.Page1.oPag.oCAUMAG_1_23.value=this.w_CAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_24.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_24.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_25.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_25.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oUBDESCRI_1_38.value==this.w_UBDESCRI)
      this.oPgFrm.Page1.oPag.oUBDESCRI_1_38.value=this.w_UBDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODART_2_4.value==this.w_CODART)
      this.oPgFrm.Page2.oPag.oCODART_2_4.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAT_2_7.value==this.w_CODMAT)
      this.oPgFrm.Page2.oPag.oCODMAT_2_7.value=this.w_CODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_42.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_42.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_43.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_43.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLIFIN_1_48.value==this.w_DESCLIFIN)
      this.oPgFrm.Page1.oPag.oDESCLIFIN_1_48.value=this.w_DESCLIFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFORFIN_1_50.value==this.w_DESFORFIN)
      this.oPgFrm.Page1.oPag.oDESFORFIN_1_50.value=this.w_DESFORFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_54.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_54.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTCAR_2_13.value==this.w_TOTCAR)
      this.oPgFrm.Page2.oPag.oTOTCAR_2_13.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTSCA_2_14.value==this.w_TOTSCA)
      this.oPgFrm.Page2.oPag.oTOTSCA_2_14.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_59.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_59.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_61.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_61.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oCMDESCRI_2_20.value==this.w_CMDESCRI)
      this.oPgFrm.Page2.oPag.oCMDESCRI_2_20.value=this.w_CMDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKARTI('M',.w_CODINI) AND ((empty(.w_codfin)) OR  (.w_codini <= .w_codfin)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o maggiore di quello finale oppure obsoleto")
          case   not(CHKARTI('M',.w_CODFIN) AND ((.w_codini <= .w_codfin) or (empty(.w_codfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o minore di quello iniziale oppure obsoleto")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_LOTINI) OR .w_APPART=.w_CODINI)  and ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_KEYSINI=.w_KEYSFIN)  and not(empty(.w_LOTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
          case   not(EMPTY(.w_LOTFIN) OR .w_APPART=.w_CODFIN)  and ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_KEYSINI=.w_KEYSFIN)  and not(empty(.w_LOTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOINT<> 'S')  and not(empty(.w_CLI_INI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLI_INI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOINT<> 'S')  and not(empty(.w_CLI_FIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLI_FIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOINT<> 'S')  and not(empty(.w_FOR_INI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOR_INI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_NOINT<> 'S')  and not(empty(.w_FOR_FIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOR_FIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   not(.w_DTOBSO>.w_OBTEST OR EMPTY(.w_DTOBSO))  and not(empty(.w_CODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOM_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CAUMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUMAG_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_LOTINI = this.w_LOTINI
    this.o_MATINI_M = this.w_MATINI_M
    this.o_MATFIN_M = this.w_MATFIN_M
    this.o_MATINI_A = this.w_MATINI_A
    this.o_MATFIN_A = this.w_MATFIN_A
    this.o_CLI_INI = this.w_CLI_INI
    this.o_NOINT = this.w_NOINT
    this.o_CLI_FIN = this.w_CLI_FIN
    this.o_FOR_INI = this.w_FOR_INI
    this.o_FOR_FIN = this.w_FOR_FIN
    this.o_CODMAG = this.w_CODMAG
    return

enddefine

* --- Define pages as container
define class tgsmd_ktmPag1 as StdContainer
  Width  = 746
  height = 425
  stdWidth  = 746
  stdheight = 425
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="BKISJBXBBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice di ricerca di inizio selezione",;
    HelpContextID = 113031130,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_LOTINI)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_2 as StdField with uid="VJUFLTGZQW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o minore di quello iniziale oppure obsoleto",;
    ToolTipText = "Codice di ricerca di fine selezione",;
    HelpContextID = 34584538,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=106, Top=38, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_LOTFIN)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc


  add object oPRESENTE_1_5 as StdCombo with uid="TEOLIVILMF",rtseq=5,rtrep=.f.,left=106,top=66,width=117,height=21;
    , ToolTipText = "Visualizza le matricole caricata (presenti) scaricate (assenti) o tutte";
    , HelpContextID = 37921733;
    , cFormVar="w_PRESENTE",RowSource=""+"Presenti,"+"Assenti,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRESENTE_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPRESENTE_1_5.GetRadio()
    this.Parent.oContained.w_PRESENTE = this.RadioValue()
    return .t.
  endfunc

  func oPRESENTE_1_5.SetRadio()
    this.Parent.oContained.w_PRESENTE=trim(this.Parent.oContained.w_PRESENTE)
    this.value = ;
      iif(this.Parent.oContained.w_PRESENTE=='P',1,;
      iif(this.Parent.oContained.w_PRESENTE=='A',2,;
      iif(this.Parent.oContained.w_PRESENTE=='T',3,;
      0)))
  endfunc

  add object oDATINI_1_6 as StdField with uid="HTBCCMIQFI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio stampa",;
    HelpContextID = 112969162,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=393, Top=66

  func oDATINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_7 as StdField with uid="YNDOCVMZZB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di fine stampa",;
    HelpContextID = 34522570,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=494, Top=66

  func oDATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oLOTINI_1_8 as StdField with uid="YCJDETCCJH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LOTINI", cQueryName = "LOTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 112965450,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=106, Top=95, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTINI"

  func oLOTINI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_KEYSINI=.w_KEYSFIN)
    endwith
   endif
  endfunc

  func oLOTINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALO',"Lotti",'LOTTI.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODINI
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTINI
     i_obj.ecpSave()
  endproc

  add object oLOTFIN_1_9 as StdField with uid="QOWPVQWHIH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LOTFIN", cQueryName = "LOTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 34518858,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=106, Top=123, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODFIN", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTFIN"

  func oLOTFIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_KEYSINI=.w_KEYSFIN)
    endwith
   endif
  endfunc

  func oLOTFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODFIN)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALO',"Lotti",'LOTTI.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODFIN
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTFIN
     i_obj.ecpSave()
  endproc

  add object oMATINI_M_1_10 as StdField with uid="XXLCJORMPW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MATINI_M", cQueryName = "MATINI_M",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 112968941,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=94, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMKEYSAL", oKey_1_2="this.w_KEYSINI", oKey_2_1="AMCODICE", oKey_2_2="this.w_MATINI_M"

  func oMATINI_M_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_KEYSINI))
    endwith
   endif
  endfunc

  func oMATINI_M_1_10.mHide()
    with this.Parent.oContained
      return (g_UNIMAT='M')
    endwith
  endfunc

  func oMATINI_M_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_M_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_M_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.MATRICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStrODBC(this.Parent.oContained.w_KEYSINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStr(this.Parent.oContained.w_KEYSINI)
    endif
    do cp_zoom with 'MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(this.parent,'oMATINI_M_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this.parent.oContained
  endproc
  proc oMATINI_M_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AMKEYSAL=w_KEYSINI
     i_obj.w_AMCODICE=this.parent.oContained.w_MATINI_M
     i_obj.ecpSave()
  endproc

  add object oMATFIN_M_1_11 as StdField with uid="SDTXDBTEZJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MATFIN_M", cQueryName = "MATFIN_M",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 34522349,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=123, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMKEYSAL", oKey_1_2="this.w_KEYSFIN", oKey_2_1="AMCODICE", oKey_2_2="this.w_MATFIN_M"

  func oMATFIN_M_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_KEYSFIN))
    endwith
   endif
  endfunc

  func oMATFIN_M_1_11.mHide()
    with this.Parent.oContained
      return (g_UNIMAT='M')
    endwith
  endfunc

  func oMATFIN_M_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_M_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_M_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.MATRICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStrODBC(this.Parent.oContained.w_KEYSFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStr(this.Parent.oContained.w_KEYSFIN)
    endif
    do cp_zoom with 'MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(this.parent,'oMATFIN_M_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this.parent.oContained
  endproc
  proc oMATFIN_M_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AMKEYSAL=w_KEYSFIN
     i_obj.w_AMCODICE=this.parent.oContained.w_MATFIN_M
     i_obj.ecpSave()
  endproc

  add object oMATINI_A_1_12 as StdField with uid="HMHORZBTBG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MATINI_A", cQueryName = "MATINI_A",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 112968953,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=95, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATINI_A"

  func oMATINI_A_1_12.mHide()
    with this.Parent.oContained
      return (g_UNIMAT<>'M')
    endwith
  endfunc

  func oMATINI_A_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_A_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_A_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATINI_A_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this.parent.oContained
  endproc
  proc oMATINI_A_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AMCODICE=this.parent.oContained.w_MATINI_A
     i_obj.ecpSave()
  endproc

  add object oMATFIN_A_1_13 as StdField with uid="IEOUTLFKLG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MATFIN_A", cQueryName = "MATFIN_A",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 34522361,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=124, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATFIN_A"

  func oMATFIN_A_1_13.mHide()
    with this.Parent.oContained
      return (g_UNIMAT<>'M')
    endwith
  endfunc

  func oMATFIN_A_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_A_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_A_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATFIN_A_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'GSMDZKTM.MATRICOL_VZM',this.parent.oContained
  endproc
  proc oMATFIN_A_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AMCODICE=this.parent.oContained.w_MATFIN_A
     i_obj.ecpSave()
  endproc

  add object oCLI_INI_1_14 as StdField with uid="JQKYAITJMO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CLI_INI", cQueryName = "CLI_INI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente",;
    HelpContextID = 235509030,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=154, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLI_INI"

  func oCLI_INI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOINT<> 'S')
    endwith
   endif
  endfunc

  func oCLI_INI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLI_INI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLI_INI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLI_INI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCLI_INI_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_CLI_INI
     i_obj.ecpSave()
  endproc

  add object oNOINT_1_15 as StdCheck with uid="YWBGYAAATN",rtseq=15,rtrep=.f.,left=549, top=155, caption="No intestatario",;
    ToolTipText = "Se attivo seleziona solo i documenti privi di intestatario",;
    HelpContextID = 11049174,;
    cFormVar="w_NOINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOINT_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNOINT_1_15.GetRadio()
    this.Parent.oContained.w_NOINT = this.RadioValue()
    return .t.
  endfunc

  func oNOINT_1_15.SetRadio()
    this.Parent.oContained.w_NOINT=trim(this.Parent.oContained.w_NOINT)
    this.value = ;
      iif(this.Parent.oContained.w_NOINT=='S',1,;
      0)
  endfunc

  add object oCLI_FIN_1_16 as StdField with uid="OZLDJOCANK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CLI_FIN", cQueryName = "CLI_FIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente",;
    HelpContextID = 148477222,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=182, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLI_FIN"

  func oCLI_FIN_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOINT<> 'S')
    endwith
   endif
  endfunc

  func oCLI_FIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLI_FIN_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLI_FIN_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLI_FIN_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCLI_FIN_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_CLI_FIN
     i_obj.ecpSave()
  endproc


  add object oBtn_1_17 as StdButton with uid="KBSYXIIRQN",left=549, top=182, width=48,height=45,;
    CpPicture="bmp\log_pwd.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire selezioni su clienti negozio";
    , HelpContextID = 45424775;
    , Caption='\<Cliente';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      do gsmd_kcv with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_GPOS<>'S')
     endwith
    endif
  endfunc

  add object oFOR_INI_1_18 as StdField with uid="ETADAQGTDM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FOR_INI", cQueryName = "FOR_INI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore",;
    HelpContextID = 235546710,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=211, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_F", oKey_2_1="ANCODICE", oKey_2_2="this.w_FOR_INI"

  func oFOR_INI_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOINT<> 'S')
    endwith
   endif
  endfunc

  func oFOR_INI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oFOR_INI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFOR_INI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_F)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_F)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFOR_INI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFOR_INI_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_F
     i_obj.w_ANCODICE=this.parent.oContained.w_FOR_INI
     i_obj.ecpSave()
  endproc

  add object oFOR_FIN_1_19 as StdField with uid="MNXFTBZYHF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FOR_FIN", cQueryName = "FOR_FIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore",;
    HelpContextID = 148514902,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=239, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_F", oKey_2_1="ANCODICE", oKey_2_2="this.w_FOR_FIN"

  func oFOR_FIN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOINT<> 'S')
    endwith
   endif
  endfunc

  func oFOR_FIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oFOR_FIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFOR_FIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_F)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_F)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFOR_FIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFOR_FIN_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_F
     i_obj.w_ANCODICE=this.parent.oContained.w_FOR_FIN
     i_obj.ecpSave()
  endproc

  add object oCODCOM_1_20 as StdField with uid="FJUQBNHFOV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 45266906,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=266, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oCODMAG_1_21 as StdField with uid="HFVRYXNEVC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 159954906,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=293, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
      if .not. empty(.w_UBICAZ)
        bRes2=.link_1_22('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oUBICAZ_1_22 as StdField with uid="HPIMFNEXWN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_UBICAZ", cQueryName = "UBICAZ",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'ubicazione",;
    HelpContextID = 110261178,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=106, Top=320, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_UBICAZ"

  func oUBICAZ_1_22.mHide()
    with this.Parent.oContained
      return (.w_FLUBIC<>'S')
    endwith
  endfunc

  func oUBICAZ_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oUBICAZ_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUBICAZ_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oUBICAZ_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Ubicazioni",'',this.parent.oContained
  endproc

  add object oCAUMAG_1_23 as StdField with uid="FLVCEFRTPK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CAUMAG", cQueryName = "CAUMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Causale di magazzino",;
    HelpContextID = 159888858,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=349, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CAUMAG"

  func oCAUMAG_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMAG_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMAG_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCAUMAG_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCAUMAG_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CAUMAG
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_24 as StdField with uid="ZJWJXASAJC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112972234,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=12, InputMask=replicate('X',40)

  add object oDESFIN_1_25 as StdField with uid="TLFKXEQUKI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 34525642,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=394, Top=38, InputMask=replicate('X',40)


  add object oBtn_1_34 as StdButton with uid="QYBPKEREZO",left=679, top=12, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 173976810;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        .NotifyEvent( "Esegui")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oUBDESCRI_1_38 as StdField with uid="TKSSNLBWJB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_UBDESCRI", cQueryName = "UBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208716657,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=320, InputMask=replicate('X',40)

  func oUBDESCRI_1_38.mHide()
    with this.Parent.oContained
      return (.w_FLUBIC<>'S')
    endwith
  endfunc

  add object oDESCLI_1_42 as StdField with uid="NCKSLHAAKO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115462602,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=154, InputMask=replicate('X',40)

  add object oDESFOR_1_43 as StdField with uid="AZUIBDXWIL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229560778,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=211, InputMask=replicate('X',40)

  add object oDESCLIFIN_1_48 as StdField with uid="SSLXBGWTKA",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCLIFIN", cQueryName = "DESCLIFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115461281,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=182, InputMask=replicate('X',40)

  add object oDESFORFIN_1_50 as StdField with uid="BIYPNTERUB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESFORFIN", cQueryName = "DESFORFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38875999,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=239, InputMask=replicate('X',40)

  add object oDESCAU_1_54 as StdField with uid="KEWIMULFIH",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 194105802,;
   bGlobalFont=.t.,;
    Height=21, Width=308, Left=169, Top=349, InputMask=replicate('X',35)


  add object oBtn_1_55 as StdButton with uid="TRVSDHCHGL",left=642, top=376, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 209109466;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_55.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oObj_1_56 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=390, width=393,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 172901402


  add object oBtn_1_58 as StdButton with uid="WFCCDGJTDK",left=693, top=376, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193289286;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAG_1_59 as StdField with uid="VBXMOWNHFP",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159896010,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=169, Top=293, InputMask=replicate('X',30)

  add object oDESCAN_1_61 as StdField with uid="OWCZOIVSRI",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43110858,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=246, Top=266, InputMask=replicate('X',30)

  add object oStr_1_26 as StdString with uid="CHNAEHJHSZ",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da cod. articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ZJNLTJMPGL",Visible=.t., Left=5, Top=38,;
    Alignment=1, Width=95, Height=18,;
    Caption="A cod. articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TJXSHDCNVW",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="PJUNMWIQWM",Visible=.t., Left=294, Top=95,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JQKVBRBQER",Visible=.t., Left=294, Top=124,;
    Alignment=1, Width=94, Height=18,;
    Caption="a matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ELHPTRNTXY",Visible=.t., Left=5, Top=66,;
    Alignment=1, Width=95, Height=18,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="MGMNWKRWKR",Visible=.t., Left=5, Top=123,;
    Alignment=1, Width=95, Height=18,;
    Caption="a codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QLVUXMNNTS",Visible=.t., Left=5, Top=293,;
    Alignment=1, Width=95, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="MUPZSOEIIC",Visible=.t., Left=5, Top=320,;
    Alignment=1, Width=95, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_FLUBIC<>'S')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="JBSZUCWZUO",Visible=.t., Left=5, Top=154,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="RJNDVFJFHH",Visible=.t., Left=5, Top=211,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="XVLRVGIJJF",Visible=.t., Left=5, Top=182,;
    Alignment=1, Width=95, Height=18,;
    Caption="a cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="BSROQKZHAW",Visible=.t., Left=5, Top=239,;
    Alignment=1, Width=95, Height=18,;
    Caption="a fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KKZTZAMNIL",Visible=.t., Left=5, Top=349,;
    Alignment=1, Width=95, Height=18,;
    Caption="Cau.Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="VXVTUJFZJZ",Visible=.t., Left=5, Top=266,;
    Alignment=1, Width=95, Height=17,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="HTCLUTJDMP",Visible=.t., Left=5, Top=390,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="CZUMPRPVYZ",Visible=.t., Left=5, Top=435,;
    Alignment=0, Width=416, Height=18,;
    Caption="I filtri sulle matricole sono sovrapposti (a seconda di g_unimat)"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="HOEVHHMKNU",Visible=.t., Left=344, Top=66,;
    Alignment=1, Width=44, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="GXQGGCQDWL",Visible=.t., Left=472, Top=66,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsmd_ktmPag2 as StdContainer
  Width  = 746
  height = 425
  stdWidth  = 746
  stdheight = 425
  resizeXpos=479
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_3 as StdButton with uid="SXRKBLIJLA",left=7, top=375, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento/movimento di magazzino selezionato";
    , HelpContextID = 144793247;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_SERIALE, .w_NUMRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oCODART_2_4 as StdField with uid="LOZVEEQDTR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo",;
    HelpContextID = 193247194,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=89, Top=368, InputMask=replicate('X',20)


  add object Zoom as cp_zoombox with uid="SFXCXEQWRB",left=-3, top=4, width=753,height=350,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSMD_KTM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="MATRICOL",cMenuFile="",cZoomOnZoom="GSZM_BZM",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=2;
    , HelpContextID = 172901402

  add object oCODMAT_2_7 as StdField with uid="PSFTUCLYSV",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODMAT", cQueryName = "CODMAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Matricola",;
    HelpContextID = 210286554,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=89, Top=403, InputMask=replicate('X',40)

  add object oTOTCAR_2_13 as StdField with uid="KPCQGUHKUR",rtseq=44,rtrep=.f.,;
    cFormVar = "w_TOTCAR", cQueryName = "TOTCAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale carichi",;
    HelpContextID = 244430538,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=490, Top=368, cSayPict='"9999999999"', cGetPict='"9999999999"'

  add object oTOTSCA_2_14 as StdField with uid="FTKPLFVDPK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale scarichi",;
    HelpContextID = 258062026,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=545, Top=368, cSayPict='"9999999999"', cGetPict='"9999999999"'

  add object oCMDESCRI_2_20 as StdField with uid="FVGZDCWNYM",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CMDESCRI", cQueryName = "CMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale di magazzino",;
    HelpContextID = 208714129,;
   bGlobalFont=.t.,;
    Height=21, Width=225, Left=370, Top=403, InputMask=replicate('X',35)


  add object oObj_2_22 as cp_runprogram with uid="GXGAKOQAVM",left=-2, top=435, width=308,height=22,;
    caption='GSAR_BZM(w_SERIALE w_NUMRIF)',;
   bGlobalFont=.t.,;
    prg="GSAR_BZM(w_SERIALE, w_NUMRIF)",;
    cEvent = "w_zoom selected",;
    nPag=2;
    , HelpContextID = 23298399

  add object oStr_2_6 as StdString with uid="FJQWSOTKGF",Visible=.t., Left=89, Top=352,;
    Alignment=0, Width=82, Height=18,;
    Caption="Articolo"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="FOGVPCYTOY",Visible=.t., Left=89, Top=389,;
    Alignment=0, Width=145, Height=18,;
    Caption="Matricola"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="FEGAECMTVP",Visible=.t., Left=607, Top=393,;
    Alignment=0, Width=135, Height=18,;
    Caption="Riservato"    , ForeColor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="GWEJBQCXSD",Visible=.t., Left=607, Top=407,;
    Alignment=0, Width=135, Height=18,;
    Caption="Storno riservato da ev."    , ForeColor=RGB(0,0,255);
  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="TKSPRJIYOH",Visible=.t., Left=607, Top=379,;
    Alignment=0, Width=135, Height=18,;
    Caption="Ultimo movimento"    , BackColor=RGB(0,255,0), BackStyle=1;
  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="ZLLFAFKFNC",Visible=.t., Left=489, Top=351,;
    Alignment=0, Width=50, Height=18,;
    Caption="Carichi"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="MQUINYEMVM",Visible=.t., Left=549, Top=351,;
    Alignment=0, Width=61, Height=18,;
    Caption="Scarichi"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="RFXCPYSOXU",Visible=.t., Left=606, Top=363,;
    Alignment=2, Width=122, Height=17,;
    Caption="Legenda"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="SNTAQBZNNK",Visible=.t., Left=370, Top=388,;
    Alignment=0, Width=163, Height=18,;
    Caption="Causale di magazzino"  ;
  , bGlobalFont=.t.

  add object oBox_2_12 as StdBox with uid="REXWUJTUXV",left=599, top=375, width=143,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_ktm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
