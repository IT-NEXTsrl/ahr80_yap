* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd1bsl                                                        *
*              Scheda lotti                                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-09                                                      *
* Last revis.: 2006-06-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd1bsl",oParentObject)
return(i_retval)

define class tgsmd1bsl as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza schede lotti (lanciato da GSMA_KSL)
    this.w_OBJECT = GSMD_SZL()
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.w_CODART = this.oParentObject.w_CODART
    this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
    this.w_PROG.Check()     
    this.w_OBJECT.mCalc(.t.)     
    this.w_OBJECT.o_CODART = this.oParentObject.w_CODART
    this.w_OBJECT.w_LOTTO = this.oParentObject.w_LOTTOZOOM
    this.w_PROG = this.w_OBJECT.GetcTRL("w_LOTTO")
    this.w_PROG.Check()     
    this.w_OBJECT.mCalc(.t.)     
    this.w_OBJECT.w_CODMAG = this.oParentObject.w_MAGZOOM
    this.w_PROG = this.w_OBJECT.GetcTRL("w_CODMAG")
    this.w_PROG.Check()     
    this.w_OBJECT.mCalc(.t.)     
    this.w_OBJECT.w_CODUBI = this.oParentObject.w_UBIZOOM
    this.w_PROG = this.w_OBJECT.GetcTRL("w_CODUBI")
    this.w_PROG.Check()     
    this.w_OBJECT.mCalc(.t.)     
    this.w_PROG = .NULL.
    GSMD_BSL(this.w_OBJECT, "L")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
