* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bdv                                                        *
*              Crea nuova unita logistica                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_16]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-09                                                      *
* Last revis.: 2004-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pACTION
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bdv",oParentObject,m.pACTION)
return(i_retval)

define class tgsmd_bdv as StdBatch
  * --- Local variables
  pACTION = space(1)
  w_PC = 0
  w_MESS = space(200)
  w_FLCOVA = space(1)
  w_NOCODICE = .f.
  w_CODICE = space(20)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_CODART = space(20)
  w_CODLOT = space(20)
  w_CODCOL = space(5)
  w_MVSERIAL = space(10)
  w_ANFLIMBA = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  TIPICONF_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea nuovo pallet (da GSVE_KDA e GSAC_KDA)
    *     Ricalcola confezioni (da GSVE_MDV o GSAC_MDV)
    * --- pACTION='C' ricalcola confzioni
    *     pACTION non definito crea nuovo pallet
    do case
      case TYPE("pACTION") = "C" AND this.pACTION="C" 
        * --- Ricalcola numero confezioni 
        if this.oParentObject.w_MVQTAMOV=0
          this.w_MESS = "Quantit� movimentata inesistente"
          ah_ErrorMsg(this.w_MESS,,"")
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.w_MVQTAMOV > 0 AND NOT EMPTY(this.oParentObject.w_MVUNIMIS)
          this.w_PC = 0
          do case
            case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS3 AND NOT EMPTY(this.oParentObject.w_UNMIS3)
              * --- UM. del Codice di Ricerca
              * --- Read from KEY_ARTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CAPZCON3"+;
                  " from "+i_cTable+" KEY_ARTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CAPZCON3;
                  from (i_cTable) where;
                      CACODICE = this.oParentObject.w_MVCODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PC = NVL(cp_ToDate(_read_.CAPZCON3),cp_NullValue(_read_.CAPZCON3))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case this.oParentObject.w_MVUNIMIS = this.oParentObject.w_UNMIS2 AND NOT EMPTY(this.oParentObject.w_UNMIS2) AND NOT EMPTY(this.oParentObject.w_MVCODART)
              * --- UM della 2^UM Articolo
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARPESNE2,ARPESLO2,ARPZCON2,ARCOCOL2,ARDESVO2,ARUMVOL2,ARTIPCO2"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARPESNE2,ARPESLO2,ARPZCON2,ARCOCOL2,ARDESVO2,ARUMVOL2,ARTIPCO2;
                  from (i_cTable) where;
                      ARCODART = this.oParentObject.w_MVCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_PN = NVL(cp_ToDate(_read_.ARPESNE2),cp_NullValue(_read_.ARPESNE2))
                w_PL = NVL(cp_ToDate(_read_.ARPESLO2),cp_NullValue(_read_.ARPESLO2))
                this.w_PC = NVL(cp_ToDate(_read_.ARPZCON2),cp_NullValue(_read_.ARPZCON2))
                w_CC = NVL(cp_ToDate(_read_.ARCOCOL2),cp_NullValue(_read_.ARCOCOL2))
                w_DESVO = NVL(cp_ToDate(_read_.ARDESVO2),cp_NullValue(_read_.ARDESVO2))
                w_UMDIM = NVL(cp_ToDate(_read_.ARUMVOL2),cp_NullValue(_read_.ARUMVOL2))
                w_TESTCOL = NVL(cp_ToDate(_read_.ARTIPCO2),cp_NullValue(_read_.ARTIPCO2))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case this.oParentObject.w_MVUNIMIS=this.oParentObject.w_UNMIS1 AND NOT EMPTY(this.oParentObject.w_UNMIS1) AND NOT EMPTY(this.oParentObject.w_MVCODART)
              * --- UM della 1^UM Articolo
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARPESNET,ARPESLOR,ARPZCONF,ARCOCOL1,ARDESVOL,ARUMVOLU,ARTIPCO1"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARPESNET,ARPESLOR,ARPZCONF,ARCOCOL1,ARDESVOL,ARUMVOLU,ARTIPCO1;
                  from (i_cTable) where;
                      ARCODART = this.oParentObject.w_MVCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                w_PN = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
                w_PL = NVL(cp_ToDate(_read_.ARPESLOR),cp_NullValue(_read_.ARPESLOR))
                this.w_PC = NVL(cp_ToDate(_read_.ARPZCONF),cp_NullValue(_read_.ARPZCONF))
                w_CC = NVL(cp_ToDate(_read_.ARCOCOL1),cp_NullValue(_read_.ARCOCOL1))
                w_DESVO = NVL(cp_ToDate(_read_.ARDESVOL),cp_NullValue(_read_.ARDESVOL))
                w_UMDIM = NVL(cp_ToDate(_read_.ARUMVOLU),cp_NullValue(_read_.ARUMVOLU))
                w_TESTCOL = NVL(cp_ToDate(_read_.ARTIPCO1),cp_NullValue(_read_.ARTIPCO1))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
          * --- Verfico se la confezione � a contenuto variabile
          * --- Read from TIPICONF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIPICONF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPICONF_idx,2],.t.,this.TIPICONF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCFLCOVA"+;
              " from "+i_cTable+" TIPICONF where ";
                  +"TCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCONF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCFLCOVA;
              from (i_cTable) where;
                  TCCODICE = this.oParentObject.w_CODCONF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOVA = NVL(cp_ToDate(_read_.TCFLCOVA),cp_NullValue(_read_.TCFLCOVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_MVTIPRIG="R" AND (EMPTY(this.oParentObject.w_CODCONF) OR EMPTY(this.oParentObject.w_MVCODCOL))
            this.w_MESS = "Codice collo o codice confezione mancante%0Impossibile calcolare il numero confezioni"
            ah_ErrorMsg(this.w_MESS,,"")
          else
            * --- Se gi� valorizzato non ricalcolo MVNUMCOL nel caso di confezione a
            *     contenuto variabile
            if this.oParentObject.w_MVNUMCOL=0 OR this.w_FLCOVA<>"S"
              if  this.w_PC<>0 
                this.oParentObject.w_MVNUMCOL = cp_ROUND((this.oParentObject.w_MVQTAMOV / this.w_PC)+ .499, 0)
                if this.oParentObject.w_MVNUMCOL > 30000
                  * --- Controllo per problema dimensione campo
                  ah_ErrorMsg("Superato massimo numero confezioni per riga. I valori verranno azzerati",,"")
                  this.oParentObject.w_MVNUMCOL = 0
                endif
              else
                this.oParentObject.w_MVNUMCOL = 0
              endif
            endif
          endif
        endif
      otherwise
        * --- Crea nuovo pallet
        this.w_CODART = this.oParentObject.oParentObject.w_MVCODART
        this.w_CODLOT = this.oParentObject.oParentObject.w_MVCODLOT
        this.w_CODCOL = this.oParentObject.w_MVCODCOL
        this.w_NOCODICE = .T.
        * --- Leggo l'informazione sul flag imballo del conto
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLIMBA"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLIMBA;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_MVTIPCON;
                and ANCODICE = this.oParentObject.w_MVCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANFLIMBA = NVL(cp_ToDate(_read_.ANFLIMBA),cp_NullValue(_read_.ANFLIMBA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Select from GSMD8KAT
        do vq_exec with 'GSMD8KAT',this,'_Curs_GSMD8KAT','',.f.,.t.
        if used('_Curs_GSMD8KAT')
          select _Curs_GSMD8KAT
          locate for 1=1
          do while not(eof())
          * --- Assegno i codici di ricerca di tipo 
          *     EAN 8, UPC A, EAN 13, EAN 14 se presenti
          this.w_CODICE = CACODICE
          * --- C'� un codice di ricerca adatto (EAN 14, EAN 13, EAN 8, UPC A) per l'articolo 
          *     selezionato
          this.w_NOCODICE = .F.
            select _Curs_GSMD8KAT
            continue
          enddo
          use
        endif
        * --- Lancio l'Anagrafica Pallet
        this.w_OBJECT = GSMD_AUL()
        * --- Controllo se l'apertura della maschera � andata a buon fine
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Dico all'anagrafica che � stata lanciata da batch
        this.w_OBJECT.oParentObject = this
        * --- Entro in caricamento
        this.w_OBJECT.ecpLoad()     
        if this.w_NOCODICE
          * --- Il pallet � mono-prodotto ma non ho un codice di ricerca valido
          ah_ErrorMsg("Non � presente nessun codice di ricerca barcode (EAN 14, EAN 13, UPC A, EAN 8)%0per il prodotto selezionato","!","")
        else
          * --- Assegno il codice di ricerca
          this.w_OBJECT.w_ULCODICE = this.w_CODICE
          * --- Devo far scattare i link
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_ULCODICE")
          this.w_OBJ.bUpd = .t.
          this.w_OBJECT.SetControlsValue()     
          this.w_OBJ.Valid()     
          * --- Assegno il codice lotto
          this.w_OBJECT.w_ULCODLOT = this.w_CODLOT
          * --- Devo far scattare i link
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_ULCODLOT")
          this.w_OBJ.bUpd = .t.
          this.w_OBJECT.SetControlsValue()     
          this.w_OBJ.Valid()     
        endif
        * --- Metto a true bUpdated affinch� possa salvare anche il solo SSCC
        this.w_OBJECT.bUpdated = .T.
        this.w_OBJ = .null.
        this.w_OBJECT = .null.
    endcase
  endproc


  proc Init(oParentObject,pACTION)
    this.pACTION=pACTION
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='TIPICONF'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSMD8KAT')
      use in _Curs_GSMD8KAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pACTION"
endproc
