* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_kat                                                        *
*              Attribuzione unit� logistica                                    *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-21                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_kat",oParentObject))

* --- Class definition
define class tgsmd_kat as StdForm
  Top    = 4
  Left   = 8

  * --- Standard Properties
  Width  = 779
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=132795241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VETTORI_IDX = 0
  MODASPED_IDX = 0
  DES_DIVE_IDX = 0
  UNIT_LOG_IDX = 0
  cPrg = "gsmd_kat"
  cComment = "Attribuzione unit� logistica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODCON = space(15)
  o_CODCON = space(15)
  w_DESCON = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODVET = space(5)
  w_DESVET = space(35)
  w_CODSPE = space(3)
  w_DESSPE = space(35)
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_CODUNLOG = space(18)
  w_MVSERIAL = space(10)
  w_CODDES = space(5)
  w_NOMDES = space(35)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_SERIE1 = space(2)
  w_NUMFIN = 0
  w_SERIE2 = space(2)
  w_FLPACKING = space(1)
  w_FLGEVASI = space(1)
  w_FLGUNLOG = space(1)
  w_FLGHDUN = space(1)
  o_FLGHDUN = space(1)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_NUMRIF = space(20)
  w_CPROWNUM = 0
  w_DDTIPCON = space(1)
  w_DDCODCON = space(15)
  w_ZOOMAST = .NULL.
  w_ZOOMDET = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_katPag1","gsmd_kat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsmd_katPag2","gsmd_kat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMAST = this.oPgFrm.Pages(1).oPag.ZOOMAST
    this.w_ZOOMDET = this.oPgFrm.Pages(1).oPag.ZOOMDET
    DoDefault()
    proc Destroy()
      this.w_ZOOMAST = .NULL.
      this.w_ZOOMDET = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VETTORI'
    this.cWorkTables[3]='MODASPED'
    this.cWorkTables[4]='DES_DIVE'
    this.cWorkTables[5]='UNIT_LOG'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_DESCON=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESVET=space(35)
      .w_DESSPE=space(35)
      .w_SERIAL=space(10)
      .w_SELEZI=space(1)
      .w_CODUNLOG=space(18)
      .w_MVSERIAL=space(10)
      .w_CODDES=space(5)
      .w_NOMDES=space(35)
      .w_FLVEAC=space(1)
      .w_CATDOC=space(2)
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_SERIE1=space(2)
      .w_NUMFIN=0
      .w_SERIE2=space(2)
      .w_FLPACKING=space(1)
      .w_FLGEVASI=space(1)
      .w_FLGUNLOG=space(1)
      .w_FLGHDUN=space(1)
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(15)
      .w_NUMRIF=space(20)
      .w_CPROWNUM=0
      .w_DDTIPCON=space(1)
      .w_DDCODCON=space(15)
        .w_TIPCON = ''
      .oPgFrm.Page1.oPag.ZOOMAST.Calculate()
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODCON))
          .link_2_7('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(5,7,.f.)
        .w_SERIAL = NVL(.w_ZOOMAST.GetVar('MVSERIAL'), SPACE(10))
      .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_SERIAL)
        .w_SELEZI = 'D'
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODUNLOG))
          .link_1_6('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CODDES))
          .link_2_15('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_FLVEAC = NVL(.w_ZOOMAST.GetVar('TDFLVEAC'), SPACE(1))
        .w_CATDOC = NVL(.w_ZOOMAST.GetVar('TDCATDOC'), SPACE(2))
        .w_DATAINI = GOMONTH(i_DATSYS, -1)
          .DoRTCalc(17,17,.f.)
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999
        .w_SERIE2 = ''
        .w_FLPACKING = 'T'
        .w_FLGEVASI = 'N'
        .w_FLGUNLOG = 'N'
        .w_FLGHDUN = 'S'
        .w_MVTIPCON = NVL(.w_ZOOMAST.GetVar('MVTIPCON'), SPACE(1))
        .w_MVCODCON = NVL(.w_ZOOMAST.GetVar('MVCODCON'), SPACE(15))
        .w_NUMRIF = .w_ZOOMDET.GetVar('MVKEYSAL')
        .w_CPROWNUM = .w_ZOOMDET.GetVar('CPROWNUM')
        .w_DDTIPCON = .w_TIPCON
        .w_DDCODCON = .w_CODCON
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMAST.Calculate()
        .DoRTCalc(1,9,.t.)
            .w_SERIAL = NVL(.w_ZOOMAST.GetVar('MVSERIAL'), SPACE(10))
        .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_SERIAL)
        .DoRTCalc(11,15,.t.)
            .w_FLVEAC = NVL(.w_ZOOMAST.GetVar('TDFLVEAC'), SPACE(1))
            .w_CATDOC = NVL(.w_ZOOMAST.GetVar('TDCATDOC'), SPACE(2))
        if .o_SERIAL<>.w_SERIAL
          .Calculate_QEEFGMINIA()
        endif
        if .o_FLGHDUN<>.w_FLGHDUN.or. .o_SERIAL<>.w_SERIAL
          .Calculate_DAXXMYYKKM()
        endif
        if .o_SELEZI<>.w_SELEZI
          .Calculate_NFKOZIMHZB()
        endif
        .DoRTCalc(18,27,.t.)
            .w_MVTIPCON = NVL(.w_ZOOMAST.GetVar('MVTIPCON'), SPACE(1))
            .w_MVCODCON = NVL(.w_ZOOMAST.GetVar('MVCODCON'), SPACE(15))
            .w_NUMRIF = .w_ZOOMDET.GetVar('MVKEYSAL')
            .w_CPROWNUM = .w_ZOOMDET.GetVar('CPROWNUM')
        if .o_TIPCON<>.w_TIPCON
            .w_DDTIPCON = .w_TIPCON
        endif
        if .o_CODCON<>.w_CODCON
            .w_DDCODCON = .w_CODCON
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMAST.Calculate()
        .oPgFrm.Page1.oPag.ZOOMDET.Calculate(.w_SERIAL)
    endwith
  return

  proc Calculate_QEEFGMINIA()
    with this
          * --- Chiude il cursore curtmp
          gsmd_bat(this;
              ,'Q';
             )
    endwith
  endproc
  proc Calculate_DAXXMYYKKM()
    with this
          * --- Visualizza assegnati
          gsmd_bat(this;
              ,'V';
              ,this;
             )
    endwith
  endproc
  proc Calculate_NFKOZIMHZB()
    with this
          * --- Seleziona/deseleziona
          gsmd_bat(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_LTZRKGHYIL()
    with this
          * --- Ricerca
          gsmd_bat(this;
              ,'R';
              ,this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODCON_2_7.enabled = this.oPgFrm.Page2.oPag.oCODCON_2_7.mCond()
    this.oPgFrm.Page2.oPag.oCODDES_2_15.enabled = this.oPgFrm.Page2.oPag.oCODDES_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMAST.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMDET.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_QEEFGMINIA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ricerca")
          .Calculate_LTZRKGHYIL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_7'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVET
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVET))
          select VTCODVET,VTDESVET,VTDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CODVET)+"%");

            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVET_2_11'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET,VTDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVET)
            select VTCODVET,VTDESVET,VTDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VTDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODVET = space(5)
      endif
      this.w_DESVET = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODVET = space(5)
        this.w_DESVET = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSPE
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_CODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_CODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oCODSPE_2_13'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_CODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_CODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUNLOG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUNLOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsmd_aul',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_CODUNLOG)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_CODUNLOG))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUNLOG)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUNLOG) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oCODUNLOG_1_6'),i_cWhere,'gsmd_aul',"Unit� logistiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUNLOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_CODUNLOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_CODUNLOG)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUNLOG = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CODUNLOG = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUNLOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDES
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCON);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPCON;
                     ,'DDCODICE',this.w_CODCON;
                     ,'DDCODDES',trim(this.w_CODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODDES_2_15'),i_cWhere,'',"Destinazioni",'GSVE_BZD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_CODCON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPCON;
                       ,'DDCODICE',this.w_CODCON;
                       ,'DDCODDES',this.w_CODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DDDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODDES = space(5)
      endif
      this.w_NOMDES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oTIPCON_2_1.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page2.oPag.oTIPCON_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_7.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_7.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_8.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_8.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVET_2_11.value==this.w_CODVET)
      this.oPgFrm.Page2.oPag.oCODVET_2_11.value=this.w_CODVET
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_2_12.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_2_12.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page2.oPag.oCODSPE_2_13.value==this.w_CODSPE)
      this.oPgFrm.Page2.oPag.oCODSPE_2_13.value=this.w_CODSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSPE_2_14.value==this.w_DESSPE)
      this.oPgFrm.Page2.oPag.oDESSPE_2_14.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_5.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUNLOG_1_6.value==this.w_CODUNLOG)
      this.oPgFrm.Page1.oPag.oCODUNLOG_1_6.value=this.w_CODUNLOG
    endif
    if not(this.oPgFrm.Page2.oPag.oCODDES_2_15.value==this.w_CODDES)
      this.oPgFrm.Page2.oPag.oCODDES_2_15.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMDES_2_16.value==this.w_NOMDES)
      this.oPgFrm.Page2.oPag.oNOMDES_2_16.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_19.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_19.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_20.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_20.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_21.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_21.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_22.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_22.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_23.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_23.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_24.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_24.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPACKING_1_31.RadioValue()==this.w_FLPACKING)
      this.oPgFrm.Page1.oPag.oFLPACKING_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGEVASI_1_33.RadioValue()==this.w_FLGEVASI)
      this.oPgFrm.Page1.oPag.oFLGEVASI_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGUNLOG_1_35.RadioValue()==this.w_FLGUNLOG)
      this.oPgFrm.Page1.oPag.oFLGUNLOG_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGHDUN_1_37.RadioValue()==this.w_FLGHDUN)
      this.oPgFrm.Page1.oPag.oFLGHDUN_1_37.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.w_TIPCON $ 'CF')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODVET))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODVET_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((empty(.w_DATAFIN)) OR  (.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((empty(.w_DATAINI)) OR  (.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_CODCON = this.w_CODCON
    this.o_SERIAL = this.w_SERIAL
    this.o_SELEZI = this.w_SELEZI
    this.o_FLGHDUN = this.w_FLGHDUN
    return

enddefine

* --- Define pages as container
define class tgsmd_katPag1 as StdContainer
  Width  = 775
  height = 481
  stdWidth  = 775
  stdheight = 481
  resizeXpos=329
  resizeYpos=341
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="SQPNVNDJVE",left=724, top=37, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 44126998;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMAST as cp_zoombox with uid="ALYDYLKRWR",left=12, top=87, width=734,height=146,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSMD_KAT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 45202406


  add object ZOOMDET as cp_szoombox with uid="CGNZVUNDVN",left=12, top=243, width=734,height=185,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSMD_KAT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZM",;
    nPag=1;
    , HelpContextID = 45202406

  add object oSELEZI_1_5 as StdRadio with uid="VNICEXLLTR",rtseq=11,rtrep=.f.,left=5, top=432, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona i documenti";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 151011546
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 151011546
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona i documenti")
      StdRadio::init()
    endproc

  func oSELEZI_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_5.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_5.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oCODUNLOG_1_6 as StdField with uid="UVMCAZGJSF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODUNLOG", cQueryName = "CODUNLOG",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Unit� logistica che sar� assegnata ai documenti selezionati",;
    HelpContextID = 112244627,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=347, Top=439, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", cZoomOnZoom="gsmd_aul", oKey_1_1="UL__SSCC", oKey_1_2="this.w_CODUNLOG"

  func oCODUNLOG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUNLOG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUNLOG_1_6.mZoom
      with this.Parent.oContained
        do GSMD_BZA with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oCODUNLOG_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsmd_aul()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UL__SSCC=this.parent.oContained.w_CODUNLOG
     i_obj.ecpSave()
  endproc


  add object oBtn_1_7 as StdButton with uid="JYUQFWSULT",left=494, top=434, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Crea una nuova unit� logistica sulla base delle righe di dettaglio selezionate";
    , HelpContextID = 8184106;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSMD_BAT(this.Parent.oContained,"N",this.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="DRRNSDZDLV",left=544, top=434, width=48,height=45,;
    CpPicture="bmp\applica.bmp", caption="", nPag=1;
    , ToolTipText = "Assegna unit� logistica ai documenti selezionati";
    , HelpContextID = 217209862;
    , Caption='A\<ssegna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSMD_BAT(this.Parent.oContained,"A",this.Parent.oContained,.w_CODUNLOG)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODUNLOG))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="ZVAOFHWNFV",left=594, top=434, width=48,height=45,;
    CpPicture="bmp\doc1.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento selezionato";
    , HelpContextID = 194945126;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSMD_BAT(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="BGNXKKRDGI",left=724, top=434, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125477818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATAINI_1_19 as StdField with uid="TLSMGPEYWG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento iniziale selezionata",;
    HelpContextID = 85181898,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=77, Top=13

  func oDATAINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAFIN)) OR  (.w_DATAINI<=.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_20 as StdField with uid="SJPNDJOWBM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento finale selezionata",;
    HelpContextID = 172213706,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=77, Top=44

  func oDATAFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAINI)) OR  (.w_DATAINI<=.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_21 as StdField with uid="CGTAYVCLKJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 163324202,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=248, Top=13, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_22 as StdField with uid="IKCHOCFECU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 38527194,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=319, Top=13, InputMask=replicate('X',2)

  func oSERIE1_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_23 as StdField with uid="PPVPLBFEDU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 84877610,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=248, Top=44, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_24 as StdField with uid="SNGKDXOYPG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 21749978,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=319, Top=44, InputMask=replicate('X',2)

  func oSERIE2_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
    endwith
    return bRes
  endfunc


  add object oFLPACKING_1_31 as StdCombo with uid="AWXRNWEFTX",rtseq=24,rtrep=.f.,left=449,top=13,width=135,height=21;
    , ToolTipText = "Documenti con Packing List generate";
    , HelpContextID = 141817324;
    , cFormVar="w_FLPACKING",RowSource=""+"Tutti,"+"Con Packing List,"+"Senza Packing List", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPACKING_1_31.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFLPACKING_1_31.GetRadio()
    this.Parent.oContained.w_FLPACKING = this.RadioValue()
    return .t.
  endfunc

  func oFLPACKING_1_31.SetRadio()
    this.Parent.oContained.w_FLPACKING=trim(this.Parent.oContained.w_FLPACKING)
    this.value = ;
      iif(this.Parent.oContained.w_FLPACKING=='T',1,;
      iif(this.Parent.oContained.w_FLPACKING=='S',2,;
      iif(this.Parent.oContained.w_FLPACKING=='N',3,;
      0)))
  endfunc


  add object oFLGEVASI_1_33 as StdCombo with uid="ARZIHMLQHZ",rtseq=25,rtrep=.f.,left=637,top=13,width=135,height=21;
    , ToolTipText = "Documenti evasi/da evadere/tutti";
    , HelpContextID = 247428511;
    , cFormVar="w_FLGEVASI",RowSource=""+"Tutti,"+"Da evadere,"+"Evasi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGEVASI_1_33.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLGEVASI_1_33.GetRadio()
    this.Parent.oContained.w_FLGEVASI = this.RadioValue()
    return .t.
  endfunc

  func oFLGEVASI_1_33.SetRadio()
    this.Parent.oContained.w_FLGEVASI=trim(this.Parent.oContained.w_FLGEVASI)
    this.value = ;
      iif(this.Parent.oContained.w_FLGEVASI=='T',1,;
      iif(this.Parent.oContained.w_FLGEVASI=='N',2,;
      iif(this.Parent.oContained.w_FLGEVASI=='S',3,;
      0)))
  endfunc


  add object oFLGUNLOG_1_35 as StdCombo with uid="WYJRZDDBVM",rtseq=26,rtrep=.f.,left=449,top=44,width=135,height=21;
    , ToolTipText = "Documenti con unit� logistica/senza unit� logistica/tutti";
    , HelpContextID = 112233059;
    , cFormVar="w_FLGUNLOG",RowSource=""+"Tutti,"+"Con unit� logistica,"+"Senza unit� logistica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGUNLOG_1_35.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFLGUNLOG_1_35.GetRadio()
    this.Parent.oContained.w_FLGUNLOG = this.RadioValue()
    return .t.
  endfunc

  func oFLGUNLOG_1_35.SetRadio()
    this.Parent.oContained.w_FLGUNLOG=trim(this.Parent.oContained.w_FLGUNLOG)
    this.value = ;
      iif(this.Parent.oContained.w_FLGUNLOG=='T',1,;
      iif(this.Parent.oContained.w_FLGUNLOG=='S',2,;
      iif(this.Parent.oContained.w_FLGUNLOG=='N',3,;
      0)))
  endfunc

  add object oFLGHDUN_1_37 as StdCheck with uid="ZOTIOPCAGX",rtseq=27,rtrep=.f.,left=150, top=438, caption="Visual. assegnati",;
    ToolTipText = "Se attivo: visualizza anche le righe con unit� logistica gi� assegnata",;
    HelpContextID = 241011370,;
    cFormVar="w_FLGHDUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGHDUN_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLGHDUN_1_37.GetRadio()
    this.Parent.oContained.w_FLGHDUN = this.RadioValue()
    return .t.
  endfunc

  func oFLGHDUN_1_37.SetRadio()
    this.Parent.oContained.w_FLGHDUN=trim(this.Parent.oContained.w_FLGHDUN)
    this.value = ;
      iif(this.Parent.oContained.w_FLGHDUN=='S',1,;
      0)
  endfunc

  add object oStr_1_11 as StdString with uid="EMPKFVMRUG",Visible=.t., Left=313, Top=439,;
    Alignment=1, Width=32, Height=18,;
    Caption="U.L.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="RIRYCFRNYK",Visible=.t., Left=16, Top=235,;
    Alignment=0, Width=243, Height=17,;
    Caption="Dettaglio documento"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="EEGOMCEKYG",Visible=.t., Left=16, Top=70,;
    Alignment=0, Width=110, Height=15,;
    Caption="Documenti"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="XRPEUUCCWD",Visible=.t., Left=349, Top=235,;
    Alignment=0, Width=190, Height=17,;
    Caption="Rosso: riga senza unit� logistica"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="FOUMODTEAZ",Visible=.t., Left=547, Top=235,;
    Alignment=0, Width=196, Height=17,;
    Caption="Verde: riga con unit� logistica"    , ForeColor=RGB(0,128,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="RWHNHUCUAO",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GFGSYQKGMB",Visible=.t., Left=12, Top=44,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=161, Top=13,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=161, Top=44,;
    Alignment=1, Width=85, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=310, Top=13,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=310, Top=44,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ELDNSZHWRA",Visible=.t., Left=347, Top=13,;
    Alignment=1, Width=100, Height=18,;
    Caption="Packing List:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="VQLLIQLUDX",Visible=.t., Left=590, Top=13,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JGVMTMRRJR",Visible=.t., Left=347, Top=44,;
    Alignment=1, Width=100, Height=18,;
    Caption="Unit� logistica:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsmd_katPag2 as StdContainer
  Width  = 775
  height = 481
  stdWidth  = 775
  stdheight = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_2_1 as StdCombo with uid="IFSUBJFAQF",value=3,rtseq=1,rtrep=.f.,left=129,top=35,width=79,height=21;
    , ToolTipText = "Tipo documenti da selezionare, clienti, fornitori, tutti";
    , HelpContextID = 78773450;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPCON_2_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oTIPCON_2_1.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_2_1.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='',3,;
      0)))
  endfunc

  func oTIPCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_2_7('Full')
      endif
      if .not. empty(.w_CODDES)
        bRes2=.link_2_15('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_2_7 as StdField with uid="NANNYIVVGY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Cliente/fornitore/conto selezionato",;
    HelpContextID = 78821338,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=129, Top=70, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oCODCON_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
      if .not. empty(.w_CODDES)
        bRes2=.link_2_15('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCON_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_8 as StdField with uid="CPGLKVKXTI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78762442,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=267, Top=70, InputMask=replicate('X',40)

  add object oCODVET_2_11 as StdField with uid="OQBCNUOYDK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODVET", cQueryName = "CODVET",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del vettore incaricato del trasporto",;
    HelpContextID = 12601382,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=129, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVET"

  func oCODVET_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVET_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVET_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVET_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVET_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVET
     i_obj.ecpSave()
  endproc

  add object oDESVET_2_12 as StdField with uid="QSQLRZYDUK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 12660278,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=204, Top=146, InputMask=replicate('X',35)

  add object oCODSPE_2_13 as StdField with uid="VNHOFRBQDX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODSPE", cQueryName = "CODSPE",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del mezzo di spedizione",;
    HelpContextID = 227719130,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=129, Top=184, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_CODSPE"

  func oCODSPE_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSPE_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSPE_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oCODSPE_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oCODSPE_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_CODSPE
     i_obj.ecpSave()
  endproc

  add object oDESSPE_2_14 as StdField with uid="OSDMWCFQJR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 227660234,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=204, Top=184, InputMask=replicate('X',35)

  add object oCODDES_2_15 as StdField with uid="NBYNPSWLGL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente",;
    ToolTipText = "Selezione codice destinazione",;
    HelpContextID = 5355482,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODDES"

  func oCODDES_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCON))
    endwith
   endif
  endfunc

  proc oCODDES_2_15.mAfter
    with this.Parent.oContained
      .w_CODDES=chkdesdiv(.w_CODDES, .w_TIPCON, .w_CODCON)
    endwith
  endproc

  func oCODDES_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODDES_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODDES_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCON)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODDES_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni",'GSVE_BZD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oNOMDES_2_16 as StdField with uid="XBDOGUMVUD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 5318442,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=204, Top=108, InputMask=replicate('X',35)

  add object oStr_2_2 as StdString with uid="HISOEUULAE",Visible=.t., Left=41, Top=35,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="CHALHXKUZR",Visible=.t., Left=41, Top=70,;
    Alignment=1, Width=85, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="CWAEOMLODB",Visible=.t., Left=41, Top=108,;
    Alignment=1, Width=85, Height=18,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="GRQMFQRCFZ",Visible=.t., Left=41, Top=184,;
    Alignment=1, Width=85, Height=18,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="NCEXNJGYXQ",Visible=.t., Left=41, Top=146,;
    Alignment=1, Width=85, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_kat','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
