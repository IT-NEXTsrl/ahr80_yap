* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_mat                                                        *
*              Aggiornamento matricole                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_278]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-25                                                      *
* Last revis.: 2015-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsmd_mat
* --- Inizializza la chiave primaria
parameters oParentObject
* --- Fine Area Manuale
return(createobject("tgsmd_mat"))

* --- Class definition
define class tgsmd_mat as MD_MLUClass
  Top    = 3
  Left   = 8

  * --- Standard Properties
  Width  = 782
  Height = 388+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-18"
  HelpContextID=130698089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=62

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AGG_LOTT_IDX = 0
  MAGAZZIN_IDX = 0
  LOTTIART_IDX = 0
  KEY_ARTI_IDX = 0
  DOC_DETT_IDX = 0
  UBICAZIO_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "AGG_LOTT"
  cKeySelect = "MVSERIAL"
  cKeyWhere  = "MVSERIAL=this.w_MVSERIAL"
  cKeyDetail  = "MVSERIAL=this.w_MVSERIAL and MVNUMRIF=this.w_MVNUMRIF"
  cKeyWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cKeyDetailWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and MVNUMRIF="+cp_ToStrODBC(this.w_MVNUMRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"AGG_LOTT.MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'AGG_LOTT.CPROWORD '
  cPrg = "gsmd_mat"
  cComment = "Aggiornamento matricole"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MVSERIAL = space(10)
  o_MVSERIAL = space(10)
  w_CPROWORD = 0
  w_MVCODICE = space(20)
  o_MVCODICE = space(20)
  w_MVCODART = space(20)
  w_ARTLOT = space(20)
  w_MVARTLOT = space(1)
  w_MVFLCASC = space(1)
  w_MVFLRISE = space(1)
  w_MVF2RISE = space(1)
  w_MVF2CASC = space(1)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_CLADOC = space(2)
  w_MVDATREG = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_MVTIPCON = space(1)
  o_MVTIPCON = space(1)
  w_MVCODCON = space(1)
  o_MVCODCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_MVCODLOT = space(20)
  o_MVCODLOT = space(20)
  w_FLSTAT = space(1)
  w_MVNUMRIF = 0
  w_MVFLUBIC = space(1)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_MVQTAUM1 = 0
  w_MVFLUBI2 = space(1)
  w_MVSERRIF = space(10)
  w_MVCODUBI = space(20)
  w_MVROWRIF = 0
  w_FLRRIF = space(1)
  w_CODCON = space(15)
  w_MVCODUB2 = space(20)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_MVCAUDOC = space(5)
  w_DESDOC = space(35)
  w_LOTZOOM = .F.
  w_UBIZOOM = .F.
  w_MVRIFRIG = 0
  w_OLDQTA = 0
  w_FLPRG = space(1)
  w_UNMIS1 = space(3)
  w_FLLOTT = space(1)
  w_TOTMAT = 0
  w_MTCARI = space(1)
  w_MVDESART = space(40)
  w_MVUNIMIS = space(3)
  w_MVQTAMOV = 0
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_DESMAT = space(30)
  w_DESMAG = space(30)
  w_RIGMOVLOT = .F.
  w_FLPRG = space(1)
  w_SCODCON = space(15)
  w_STIPCON = space(10)
  w_DISLOT = space(1)
  w_ARCLAMAT = space(5)
  w_ARSALCOM = space(1)
  w_MVCODCOM = space(15)

  * --- Children pointers
  GSMD_MAM = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'AGG_LOTT','gsmd_mat')
    stdPageFrame::Init()
    *set procedure to GSMD_MAM additive
    with this
      .Pages(1).addobject("oPag","tgsmd_matPag1","gsmd_mat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Aggiornamento matricole")
      .Pages(1).HelpContextID = 61151784
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMD_MAM
    * --- Area Manuale = Init Page Frame
    * --- gsmd_mat
     * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='AGG_LOTT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AGG_LOTT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AGG_LOTT_IDX,3]
  return

  function CreateChildren()
    this.GSMD_MAM = CREATEOBJECT('stdDynamicChild',this,'GSMD_MAM',this.oPgFrm.Page1.oPag.oLinkPC_2_29)
    this.GSMD_MAM.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMD_MAM)
      this.GSMD_MAM.DestroyChildrenChain()
      this.GSMD_MAM=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_29')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMD_MAM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMD_MAM.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMD_MAM.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSMD_MAM.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MVSERIAL,"MTSERIAL";
             ,.w_CPROWNUM,"MTROWNUM";
             ,.w_MVNUMRIF,"MTNUMRIF";
             ,.w_CPROWNUM,"MTROWLOT";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MVSERIAL = NVL(MVSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AGG_LOTT where MVSERIAL=KeySet.MVSERIAL
    *                            and MVNUMRIF=KeySet.MVNUMRIF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2],this.bLoadRecFilter,this.AGG_LOTT_IDX,"gsmd_mat")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AGG_LOTT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AGG_LOTT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AGG_LOTT '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MVSERIAL',this.w_MVSERIAL  )
      select * from (i_cTable) AGG_LOTT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTOBSO = ctod("  /  /  ")
        .w_RIGMOVLOT = .F.
        .w_MVSERIAL = NVL(MVSERIAL,space(10))
        .w_CLADOC = this.oParentObject.oParentObject .w_CLADOC
        .w_MVDATREG = this.oParentObject .w_DATREG
        .w_CODAZI = i_CODAZI
        .w_MVTIPCON = this.oParentObject .w_TIPCON
        .w_MVCODCON = this.oParentObject .w_CODCON
        .w_OBTEST = this.oParentObject .w_DATDOC
        .w_MVNUMDOC = this.oParentObject .w_NUMDOC
        .w_MVALFDOC = this.oParentObject .w_ALFDOC
        .w_MVDATDOC = this.oParentObject .w_DATDOC
        .w_MVCAUDOC = this.oParentObject .w_CAUDOC
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_DESDOC = this.oParentObject .w_DESDOC
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_FLPRG = 'D'
        .w_FLPRG = 'M'
        .w_SCODCON = .w_MVCODCON
        .w_STIPCON = .w_MVTIPCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'AGG_LOTT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ARTLOT = space(20)
          .w_FLSTAT = space(1)
          .w_FLRRIF = space(1)
        .w_LOTZOOM = .T.
        .w_UBIZOOM = .T.
          .w_UNMIS1 = space(3)
          .w_FLLOTT = space(1)
          .w_TOTMAT = 0
          .w_MTCARI = space(1)
          .w_DESMAT = space(30)
          .w_DESMAG = space(30)
          .w_DISLOT = space(1)
          .w_ARCLAMAT = space(5)
          .w_ARSALCOM = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MVCODICE = NVL(MVCODICE,space(20))
          .w_MVCODART = NVL(MVCODART,space(20))
          if link_2_3_joined
            this.w_MVCODART = NVL(ARCODART203,NVL(this.w_MVCODART,space(20)))
            this.w_MVARTLOT = NVL(ARFLLOTT203,space(1))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_FLLOTT = NVL(ARFLLOTT203,space(1))
            this.w_DISLOT = NVL(ARDISLOT203,space(1))
            this.w_ARCLAMAT = NVL(ARCLAMAT203,space(5))
            this.w_ARSALCOM = NVL(ARSALCOM203,space(1))
          else
          .link_2_3('Load')
          endif
          .w_MVARTLOT = NVL(MVARTLOT,space(1))
          .w_MVFLCASC = NVL(MVFLCASC,space(1))
          .w_MVFLRISE = NVL(MVFLRISE,space(1))
          .w_MVF2RISE = NVL(MVF2RISE,space(1))
          .w_MVF2CASC = NVL(MVF2CASC,space(1))
          .w_MVFLLOTT = NVL(MVFLLOTT,space(1))
          .w_MVF2LOTT = NVL(MVF2LOTT,space(1))
          .w_MVCODLOT = NVL(MVCODLOT,space(20))
          .w_MVNUMRIF = NVL(MVNUMRIF,0)
          .w_MVFLUBIC = NVL(MVFLUBIC,space(1))
        .w_FLUBIC = .w_MVFLUBIC
        .w_F2UBIC = .w_MVFLUBI2
          .w_MVQTAUM1 = NVL(MVQTAUM1,0)
          .w_MVFLUBI2 = NVL(MVFLUBI2,space(1))
          .w_MVSERRIF = NVL(MVSERRIF,space(10))
          .w_MVCODUBI = NVL(MVCODUBI,space(20))
          .w_MVROWRIF = NVL(MVROWRIF,0)
          .link_2_22('Load')
        .w_CODCON = IIF(.w_MVTIPCON='F' And Empty(.w_MVCODMAT), .w_MVCODCON, SPACE(15))
          .w_MVCODUB2 = NVL(MVCODUB2,space(20))
          .w_MVRIFRIG = NVL(MVRIFRIG,0)
        .w_OLDQTA = .w_MVQTAUM1
          .w_MVDESART = NVL(MVDESART,space(40))
          .w_MVUNIMIS = NVL(MVUNIMIS,space(3))
          .w_MVQTAMOV = NVL(MVQTAMOV,0)
          .w_MVCODMAG = NVL(MVCODMAG,space(5))
          .w_MVCODMAT = NVL(MVCODMAT,space(5))
          .w_MVCODCOM = NVL(MVCODCOM,space(15))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CLADOC = this.oParentObject.oParentObject .w_CLADOC
        .w_MVDATREG = this.oParentObject .w_DATREG
        .w_CODAZI = i_CODAZI
        .w_MVTIPCON = this.oParentObject .w_TIPCON
        .w_MVCODCON = this.oParentObject .w_CODCON
        .w_OBTEST = this.oParentObject .w_DATDOC
        .w_MVNUMDOC = this.oParentObject .w_NUMDOC
        .w_MVALFDOC = this.oParentObject .w_ALFDOC
        .w_MVDATDOC = this.oParentObject .w_DATDOC
        .w_MVCAUDOC = this.oParentObject .w_CAUDOC
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_DESDOC = this.oParentObject .w_DESDOC
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_FLPRG = 'D'
        .w_FLPRG = 'M'
        .w_SCODCON = .w_MVCODCON
        .w_STIPCON = .w_MVTIPCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MVSERIAL=space(10)
      .w_CPROWORD=10
      .w_MVCODICE=space(20)
      .w_MVCODART=space(20)
      .w_ARTLOT=space(20)
      .w_MVARTLOT=space(1)
      .w_MVFLCASC=space(1)
      .w_MVFLRISE=space(1)
      .w_MVF2RISE=space(1)
      .w_MVF2CASC=space(1)
      .w_MVFLLOTT=space(1)
      .w_MVF2LOTT=space(1)
      .w_CLADOC=space(2)
      .w_MVDATREG=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_MVCODLOT=space(20)
      .w_FLSTAT=space(1)
      .w_MVNUMRIF=0
      .w_MVFLUBIC=space(1)
      .w_FLUBIC=space(1)
      .w_F2UBIC=space(1)
      .w_MVQTAUM1=0
      .w_MVFLUBI2=space(1)
      .w_MVSERRIF=space(10)
      .w_MVCODUBI=space(20)
      .w_MVROWRIF=0
      .w_FLRRIF=space(1)
      .w_CODCON=space(15)
      .w_MVCODUB2=space(20)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCAUDOC=space(5)
      .w_DESDOC=space(35)
      .w_LOTZOOM=.f.
      .w_UBIZOOM=.f.
      .w_MVRIFRIG=0
      .w_OLDQTA=0
      .w_FLPRG=space(1)
      .w_UNMIS1=space(3)
      .w_FLLOTT=space(1)
      .w_TOTMAT=0
      .w_MTCARI=space(1)
      .w_MVDESART=space(40)
      .w_MVUNIMIS=space(3)
      .w_MVQTAMOV=0
      .w_MVCODMAG=space(5)
      .w_MVCODMAT=space(5)
      .w_DESMAT=space(30)
      .w_DESMAG=space(30)
      .w_RIGMOVLOT=.f.
      .w_FLPRG=space(1)
      .w_SCODCON=space(15)
      .w_STIPCON=space(10)
      .w_DISLOT=space(1)
      .w_ARCLAMAT=space(5)
      .w_ARSALCOM=space(1)
      .w_MVCODCOM=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_MVCODART))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,10,.f.)
        .w_MVFLLOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBIC='S'), LEFT(ALLTRIM(.w_MVFLCASC)+IIF(.w_MVFLRISE='+', '-', IIF(.w_MVFLRISE='-', '+', ' ')), 1), ' ')
        .w_MVF2LOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBI2='S'), LEFT(ALLTRIM(.w_MVF2CASC)+IIF(.w_MVF2RISE='+', '-', IIF(.w_MVF2RISE='-', '+', ' ')), 1), ' ')
        .w_CLADOC = this.oParentObject.oParentObject .w_CLADOC
        .w_MVDATREG = this.oParentObject .w_DATREG
        .w_CODAZI = i_CODAZI
        .w_MVTIPCON = this.oParentObject .w_TIPCON
        .w_MVCODCON = this.oParentObject .w_CODCON
        .w_OBTEST = this.oParentObject .w_DATDOC
        .DoRTCalc(19,23,.f.)
        .w_FLUBIC = .w_MVFLUBIC
        .w_F2UBIC = .w_MVFLUBI2
        .DoRTCalc(26,30,.f.)
        if not(empty(.w_MVROWRIF))
         .link_2_22('Full')
        endif
        .DoRTCalc(31,31,.f.)
        .w_CODCON = IIF(.w_MVTIPCON='F' And Empty(.w_MVCODMAT), .w_MVCODCON, SPACE(15))
        .DoRTCalc(33,33,.f.)
        .w_MVNUMDOC = this.oParentObject .w_NUMDOC
        .w_MVALFDOC = this.oParentObject .w_ALFDOC
        .w_MVDATDOC = this.oParentObject .w_DATDOC
        .w_MVCAUDOC = this.oParentObject .w_CAUDOC
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_DESDOC = this.oParentObject .w_DESDOC
        .w_LOTZOOM = .T.
        .w_UBIZOOM = .T.
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(41,41,.f.)
        .w_OLDQTA = .w_MVQTAUM1
        .w_FLPRG = 'D'
        .DoRTCalc(44,54,.f.)
        .w_RIGMOVLOT = .F.
        .w_FLPRG = 'M'
        .w_SCODCON = .w_MVCODCON
        .w_STIPCON = .w_MVTIPCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AGG_LOTT')
    this.DoRTCalc(59,62,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsmd_mat
     * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSMD_MAM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'AGG_LOTT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMD_MAM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVSERIAL,"MVSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
    i_lTable = "AGG_LOTT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.AGG_LOTT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MVCODICE C(20);
      ,CPROWNUM N(10);
      ,t_MVCODART C(20);
      ,t_ARTLOT C(20);
      ,t_MVARTLOT C(1);
      ,t_MVFLCASC C(1);
      ,t_MVFLRISE C(1);
      ,t_MVF2RISE C(1);
      ,t_MVF2CASC C(1);
      ,t_MVFLLOTT C(1);
      ,t_MVF2LOTT C(1);
      ,t_MVCODLOT C(20);
      ,t_FLSTAT C(1);
      ,t_MVNUMRIF N(3);
      ,t_MVFLUBIC C(1);
      ,t_FLUBIC C(1);
      ,t_F2UBIC C(1);
      ,t_MVQTAUM1 N(12,3);
      ,t_MVFLUBI2 C(1);
      ,t_MVSERRIF C(10);
      ,t_MVCODUBI C(20);
      ,t_MVROWRIF N(4);
      ,t_FLRRIF C(1);
      ,t_CODCON C(15);
      ,t_MVCODUB2 C(20);
      ,t_LOTZOOM L(1);
      ,t_UBIZOOM L(1);
      ,t_MVRIFRIG N(4);
      ,t_OLDQTA N(12,3);
      ,t_UNMIS1 C(3);
      ,t_FLLOTT C(1);
      ,t_TOTMAT N(10);
      ,t_MTCARI C(1);
      ,t_MVDESART C(40);
      ,t_MVUNIMIS C(3);
      ,t_MVQTAMOV N(12,3);
      ,t_MVCODMAG C(5);
      ,t_MVCODMAT C(5);
      ,t_DESMAT C(30);
      ,t_DESMAG C(30);
      ,t_DISLOT C(1);
      ,t_ARCLAMAT C(5);
      ,t_ARSALCOM C(1);
      ,t_MVCODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsmd_matbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.controlsource=this.cTrsName+'.t_MVCODICE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
      *
      * insert into AGG_LOTT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AGG_LOTT')
        i_extval=cp_InsertValODBCExtFlds(this,'AGG_LOTT')
        i_cFldBody=" "+;
                  "(MVSERIAL,CPROWORD,MVCODICE,MVCODART,MVARTLOT"+;
                  ",MVFLCASC,MVFLRISE,MVF2RISE,MVF2CASC,MVFLLOTT"+;
                  ",MVF2LOTT,MVCODLOT,MVNUMRIF,MVFLUBIC,MVQTAUM1"+;
                  ",MVFLUBI2,MVSERRIF,MVCODUBI,MVROWRIF,MVCODUB2"+;
                  ",MVRIFRIG,MVDESART,MVUNIMIS,MVQTAMOV,MVCODMAG"+;
                  ",MVCODMAT,MVCODCOM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MVSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MVCODICE)+","+cp_ToStrODBCNull(this.w_MVCODART)+","+cp_ToStrODBC(this.w_MVARTLOT)+;
             ","+cp_ToStrODBC(this.w_MVFLCASC)+","+cp_ToStrODBC(this.w_MVFLRISE)+","+cp_ToStrODBC(this.w_MVF2RISE)+","+cp_ToStrODBC(this.w_MVF2CASC)+","+cp_ToStrODBC(this.w_MVFLLOTT)+;
             ","+cp_ToStrODBC(this.w_MVF2LOTT)+","+cp_ToStrODBC(this.w_MVCODLOT)+","+cp_ToStrODBC(this.w_MVNUMRIF)+","+cp_ToStrODBC(this.w_MVFLUBIC)+","+cp_ToStrODBC(this.w_MVQTAUM1)+;
             ","+cp_ToStrODBC(this.w_MVFLUBI2)+","+cp_ToStrODBC(this.w_MVSERRIF)+","+cp_ToStrODBC(this.w_MVCODUBI)+","+cp_ToStrODBCNull(this.w_MVROWRIF)+","+cp_ToStrODBC(this.w_MVCODUB2)+;
             ","+cp_ToStrODBC(this.w_MVRIFRIG)+","+cp_ToStrODBC(this.w_MVDESART)+","+cp_ToStrODBC(this.w_MVUNIMIS)+","+cp_ToStrODBC(this.w_MVQTAMOV)+","+cp_ToStrODBC(this.w_MVCODMAG)+;
             ","+cp_ToStrODBC(this.w_MVCODMAT)+","+cp_ToStrODBC(this.w_MVCODCOM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AGG_LOTT')
        i_extval=cp_InsertValVFPExtFlds(this,'AGG_LOTT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MVSERIAL',this.w_MVSERIAL,'MVNUMRIF',this.w_MVNUMRIF)
        INSERT INTO (i_cTable) (;
                   MVSERIAL;
                  ,CPROWORD;
                  ,MVCODICE;
                  ,MVCODART;
                  ,MVARTLOT;
                  ,MVFLCASC;
                  ,MVFLRISE;
                  ,MVF2RISE;
                  ,MVF2CASC;
                  ,MVFLLOTT;
                  ,MVF2LOTT;
                  ,MVCODLOT;
                  ,MVNUMRIF;
                  ,MVFLUBIC;
                  ,MVQTAUM1;
                  ,MVFLUBI2;
                  ,MVSERRIF;
                  ,MVCODUBI;
                  ,MVROWRIF;
                  ,MVCODUB2;
                  ,MVRIFRIG;
                  ,MVDESART;
                  ,MVUNIMIS;
                  ,MVQTAMOV;
                  ,MVCODMAG;
                  ,MVCODMAT;
                  ,MVCODCOM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MVSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_MVCODICE;
                  ,this.w_MVCODART;
                  ,this.w_MVARTLOT;
                  ,this.w_MVFLCASC;
                  ,this.w_MVFLRISE;
                  ,this.w_MVF2RISE;
                  ,this.w_MVF2CASC;
                  ,this.w_MVFLLOTT;
                  ,this.w_MVF2LOTT;
                  ,this.w_MVCODLOT;
                  ,this.w_MVNUMRIF;
                  ,this.w_MVFLUBIC;
                  ,this.w_MVQTAUM1;
                  ,this.w_MVFLUBI2;
                  ,this.w_MVSERRIF;
                  ,this.w_MVCODUBI;
                  ,this.w_MVROWRIF;
                  ,this.w_MVCODUB2;
                  ,this.w_MVRIFRIG;
                  ,this.w_MVDESART;
                  ,this.w_MVUNIMIS;
                  ,this.w_MVQTAMOV;
                  ,this.w_MVCODMAG;
                  ,this.w_MVCODMAT;
                  ,this.w_MVCODCOM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_LOTT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_LOTT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSMD_MAM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MVSERIAL,"MTSERIAL";
                     ,this.w_CPROWNUM,"MTROWNUM";
                     ,this.w_MVNUMRIF,"MTNUMRIF";
                     ,this.w_CPROWNUM,"MTROWLOT";
                     )
              this.GSMD_MAM.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AGG_LOTT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_LOTT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MVCODICE="+cp_ToStrODBC(this.w_MVCODICE)+;
                     ",MVCODART="+cp_ToStrODBCNull(this.w_MVCODART)+;
                     ",MVARTLOT="+cp_ToStrODBC(this.w_MVARTLOT)+;
                     ",MVFLCASC="+cp_ToStrODBC(this.w_MVFLCASC)+;
                     ",MVFLRISE="+cp_ToStrODBC(this.w_MVFLRISE)+;
                     ",MVF2RISE="+cp_ToStrODBC(this.w_MVF2RISE)+;
                     ",MVF2CASC="+cp_ToStrODBC(this.w_MVF2CASC)+;
                     ",MVFLLOTT="+cp_ToStrODBC(this.w_MVFLLOTT)+;
                     ",MVF2LOTT="+cp_ToStrODBC(this.w_MVF2LOTT)+;
                     ",MVCODLOT="+cp_ToStrODBC(this.w_MVCODLOT)+;
                     ",MVFLUBIC="+cp_ToStrODBC(this.w_MVFLUBIC)+;
                     ",MVQTAUM1="+cp_ToStrODBC(this.w_MVQTAUM1)+;
                     ",MVFLUBI2="+cp_ToStrODBC(this.w_MVFLUBI2)+;
                     ",MVSERRIF="+cp_ToStrODBC(this.w_MVSERRIF)+;
                     ",MVCODUBI="+cp_ToStrODBC(this.w_MVCODUBI)+;
                     ",MVROWRIF="+cp_ToStrODBCNull(this.w_MVROWRIF)+;
                     ",MVCODUB2="+cp_ToStrODBC(this.w_MVCODUB2)+;
                     ",MVRIFRIG="+cp_ToStrODBC(this.w_MVRIFRIG)+;
                     ",MVDESART="+cp_ToStrODBC(this.w_MVDESART)+;
                     ",MVUNIMIS="+cp_ToStrODBC(this.w_MVUNIMIS)+;
                     ",MVQTAMOV="+cp_ToStrODBC(this.w_MVQTAMOV)+;
                     ",MVCODMAG="+cp_ToStrODBC(this.w_MVCODMAG)+;
                     ",MVCODMAT="+cp_ToStrODBC(this.w_MVCODMAT)+;
                     ",MVCODCOM="+cp_ToStrODBC(this.w_MVCODCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_LOTT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MVCODICE=this.w_MVCODICE;
                     ,MVCODART=this.w_MVCODART;
                     ,MVARTLOT=this.w_MVARTLOT;
                     ,MVFLCASC=this.w_MVFLCASC;
                     ,MVFLRISE=this.w_MVFLRISE;
                     ,MVF2RISE=this.w_MVF2RISE;
                     ,MVF2CASC=this.w_MVF2CASC;
                     ,MVFLLOTT=this.w_MVFLLOTT;
                     ,MVF2LOTT=this.w_MVF2LOTT;
                     ,MVCODLOT=this.w_MVCODLOT;
                     ,MVFLUBIC=this.w_MVFLUBIC;
                     ,MVQTAUM1=this.w_MVQTAUM1;
                     ,MVFLUBI2=this.w_MVFLUBI2;
                     ,MVSERRIF=this.w_MVSERRIF;
                     ,MVCODUBI=this.w_MVCODUBI;
                     ,MVROWRIF=this.w_MVROWRIF;
                     ,MVCODUB2=this.w_MVCODUB2;
                     ,MVRIFRIG=this.w_MVRIFRIG;
                     ,MVDESART=this.w_MVDESART;
                     ,MVUNIMIS=this.w_MVUNIMIS;
                     ,MVQTAMOV=this.w_MVQTAMOV;
                     ,MVCODMAG=this.w_MVCODMAG;
                     ,MVCODMAT=this.w_MVCODMAT;
                     ,MVCODCOM=this.w_MVCODCOM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0)
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSMD_MAM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MVSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MVNUMRIF,"MTNUMRIF";
               ,this.w_CPROWNUM,"MTROWLOT";
               )
          this.GSMD_MAM.mReplace()
          this.GSMD_MAM.bSaveContext=.f.
        endif
      endscan
     this.GSMD_MAM.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsmd_mat
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo dei Documenti
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
        * ---Memorizza l'ultima operazione eseguita
        APPMDV=this.cFunction
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSMD_MAM : Deleting
        this.GSMD_MAM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MVSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MVNUMRIF,"MTNUMRIF";
               ,this.w_CPROWNUM,"MTROWLOT";
               )
        this.GSMD_MAM.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AGG_LOTT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AGG_LOTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_LOTT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_2_3('Full')
        .DoRTCalc(5,10,.t.)
          .w_MVFLLOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBIC='S'), LEFT(ALLTRIM(.w_MVFLCASC)+IIF(.w_MVFLRISE='+', '-', IIF(.w_MVFLRISE='-', '+', ' ')), 1), ' ')
          .w_MVF2LOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBI2='S'), LEFT(ALLTRIM(.w_MVF2CASC)+IIF(.w_MVF2RISE='+', '-', IIF(.w_MVF2RISE='-', '+', ' ')), 1), ' ')
          .w_CLADOC = this.oParentObject.oParentObject .w_CLADOC
          .w_MVDATREG = this.oParentObject .w_DATREG
          .w_CODAZI = i_CODAZI
          .w_MVTIPCON = this.oParentObject .w_TIPCON
          .w_MVCODCON = this.oParentObject .w_CODCON
          .w_OBTEST = this.oParentObject .w_DATDOC
        .DoRTCalc(19,23,.t.)
          .w_FLUBIC = .w_MVFLUBIC
          .w_F2UBIC = .w_MVFLUBI2
        .DoRTCalc(26,29,.t.)
        if .o_MVCODICE<>.w_MVCODICE
          .link_2_22('Full')
        endif
        .DoRTCalc(31,31,.t.)
          .w_CODCON = IIF(.w_MVTIPCON='F' And Empty(.w_MVCODMAT), .w_MVCODCON, SPACE(15))
        .DoRTCalc(33,33,.t.)
          .w_MVNUMDOC = this.oParentObject .w_NUMDOC
          .w_MVALFDOC = this.oParentObject .w_ALFDOC
          .w_MVDATDOC = this.oParentObject .w_DATDOC
          .w_MVCAUDOC = this.oParentObject .w_CAUDOC
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
          .w_DESDOC = this.oParentObject .w_DESDOC
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(39,41,.t.)
        if .o_MVSERIAL<>.w_MVSERIAL
          .w_OLDQTA = .w_MVQTAUM1
        endif
          .w_FLPRG = 'D'
        .DoRTCalc(44,55,.t.)
          .w_FLPRG = 'M'
        if .o_MVCODCON<>.w_MVCODCON
          .w_SCODCON = .w_MVCODCON
        endif
        if .o_MVTIPCON<>.w_MVTIPCON
          .w_STIPCON = .w_MVTIPCON
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(59,62,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MVCODART with this.w_MVCODART
      replace t_ARTLOT with this.w_ARTLOT
      replace t_MVARTLOT with this.w_MVARTLOT
      replace t_MVFLCASC with this.w_MVFLCASC
      replace t_MVFLRISE with this.w_MVFLRISE
      replace t_MVF2RISE with this.w_MVF2RISE
      replace t_MVF2CASC with this.w_MVF2CASC
      replace t_MVFLLOTT with this.w_MVFLLOTT
      replace t_MVF2LOTT with this.w_MVF2LOTT
      replace t_MVCODLOT with this.w_MVCODLOT
      replace t_FLSTAT with this.w_FLSTAT
      replace t_MVNUMRIF with this.w_MVNUMRIF
      replace t_MVFLUBIC with this.w_MVFLUBIC
      replace t_FLUBIC with this.w_FLUBIC
      replace t_F2UBIC with this.w_F2UBIC
      replace t_MVQTAUM1 with this.w_MVQTAUM1
      replace t_MVFLUBI2 with this.w_MVFLUBI2
      replace t_MVSERRIF with this.w_MVSERRIF
      replace t_MVCODUBI with this.w_MVCODUBI
      replace t_MVROWRIF with this.w_MVROWRIF
      replace t_FLRRIF with this.w_FLRRIF
      replace t_CODCON with this.w_CODCON
      replace t_MVCODUB2 with this.w_MVCODUB2
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_UBIZOOM with this.w_UBIZOOM
      replace t_MVRIFRIG with this.w_MVRIFRIG
      replace t_OLDQTA with this.w_OLDQTA
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_FLLOTT with this.w_FLLOTT
      replace t_TOTMAT with this.w_TOTMAT
      replace t_MTCARI with this.w_MTCARI
      replace t_MVDESART with this.w_MVDESART
      replace t_MVUNIMIS with this.w_MVUNIMIS
      replace t_MVQTAMOV with this.w_MVQTAMOV
      replace t_MVCODMAG with this.w_MVCODMAG
      replace t_MVCODMAT with this.w_MVCODMAT
      replace t_DESMAT with this.w_DESMAT
      replace t_DESMAG with this.w_DESMAG
      replace t_DISLOT with this.w_DISLOT
      replace t_ARCLAMAT with this.w_ARCLAMAT
      replace t_ARSALCOM with this.w_ARSALCOM
      replace t_MVCODCOM with this.w_MVCODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARFLLOTT,ARUNMIS1,ARDISLOT,ARCLAMAT,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MVCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MVCODART)
            select ARCODART,ARFLLOTT,ARUNMIS1,ARDISLOT,ARCLAMAT,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODART = NVL(_Link_.ARCODART,space(20))
      this.w_MVARTLOT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_DISLOT = NVL(_Link_.ARDISLOT,space(1))
      this.w_ARCLAMAT = NVL(_Link_.ARCLAMAT,space(5))
      this.w_ARSALCOM = NVL(_Link_.ARSALCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODART = space(20)
      endif
      this.w_MVARTLOT = space(1)
      this.w_UNMIS1 = space(3)
      this.w_FLLOTT = space(1)
      this.w_DISLOT = space(1)
      this.w_ARCLAMAT = space(5)
      this.w_ARSALCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARFLLOTT as ARFLLOTT203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARFLLOTT as ARFLLOTT203"+ ",link_2_3.ARDISLOT as ARDISLOT203"+ ",link_2_3.ARCLAMAT as ARCLAMAT203"+ ",link_2_3.ARSALCOM as ARSALCOM203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on AGG_LOTT.MVCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and AGG_LOTT.MVCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVROWRIF
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_lTable = "DOC_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2], .t., this.DOC_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVROWRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVROWRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,CPROWNUM,MVFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_MVROWRIF);
                   +" and MVSERIAL="+cp_ToStrODBC(this.w_MVSERRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MVSERRIF;
                       ,'CPROWNUM',this.w_MVROWRIF)
            select MVSERIAL,CPROWNUM,MVFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVROWRIF = NVL(_Link_.CPROWNUM,0)
      this.w_FLRRIF = NVL(_Link_.MVFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVROWRIF = 0
      endif
      this.w_FLRRIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.DOC_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVROWRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_11.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_11.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_13.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_13.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDOC_1_14.value==this.w_MVDATDOC)
      this.oPgFrm.Page1.oPag.oMVDATDOC_1_14.value=this.w_MVDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCAUDOC_1_17.value==this.w_MVCAUDOC)
      this.oPgFrm.Page1.oPag.oMVCAUDOC_1_17.value=this.w_MVCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_19.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_19.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value==this.w_MVCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value=this.w_MVCODICE
      replace t_MVCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODICE_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'AGG_LOTT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSMD_MAM.CheckForm()
      if not(Empty(.w_CPROWORD)) AND NOT EMPTY(.w_MVCODICE) AND .w_MVQTAMOV<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVSERIAL = this.w_MVSERIAL
    this.o_MVCODICE = this.w_MVCODICE
    this.o_MVTIPCON = this.w_MVTIPCON
    this.o_MVCODCON = this.w_MVCODCON
    this.o_MVCODLOT = this.w_MVCODLOT
    * --- GSMD_MAM : Depends On
    this.GSMD_MAM.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND NOT EMPTY(t_MVCODICE) AND t_MVQTAMOV<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MVCODICE=space(20)
      .w_MVCODART=space(20)
      .w_ARTLOT=space(20)
      .w_MVARTLOT=space(1)
      .w_MVFLCASC=space(1)
      .w_MVFLRISE=space(1)
      .w_MVF2RISE=space(1)
      .w_MVF2CASC=space(1)
      .w_MVFLLOTT=space(1)
      .w_MVF2LOTT=space(1)
      .w_MVCODLOT=space(20)
      .w_FLSTAT=space(1)
      .w_MVNUMRIF=0
      .w_MVFLUBIC=space(1)
      .w_FLUBIC=space(1)
      .w_F2UBIC=space(1)
      .w_MVQTAUM1=0
      .w_MVFLUBI2=space(1)
      .w_MVSERRIF=space(10)
      .w_MVCODUBI=space(20)
      .w_MVROWRIF=0
      .w_FLRRIF=space(1)
      .w_CODCON=space(15)
      .w_MVCODUB2=space(20)
      .w_LOTZOOM=.f.
      .w_UBIZOOM=.f.
      .w_MVRIFRIG=0
      .w_OLDQTA=0
      .w_UNMIS1=space(3)
      .w_FLLOTT=space(1)
      .w_TOTMAT=0
      .w_MTCARI=space(1)
      .w_MVDESART=space(40)
      .w_MVUNIMIS=space(3)
      .w_MVQTAMOV=0
      .w_MVCODMAG=space(5)
      .w_MVCODMAT=space(5)
      .w_DESMAT=space(30)
      .w_DESMAG=space(30)
      .w_DISLOT=space(1)
      .w_ARCLAMAT=space(5)
      .w_ARSALCOM=space(1)
      .w_MVCODCOM=space(15)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_MVCODART))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,10,.f.)
        .w_MVFLLOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBIC='S'), LEFT(ALLTRIM(.w_MVFLCASC)+IIF(.w_MVFLRISE='+', '-', IIF(.w_MVFLRISE='-', '+', ' ')), 1), ' ')
        .w_MVF2LOTT = IIF((g_PERLOT='S' AND .w_MVARTLOT$ 'SC') OR (g_PERUBI='S' AND .w_MVFLUBI2='S'), LEFT(ALLTRIM(.w_MVF2CASC)+IIF(.w_MVF2RISE='+', '-', IIF(.w_MVF2RISE='-', '+', ' ')), 1), ' ')
      .DoRTCalc(13,23,.f.)
        .w_FLUBIC = .w_MVFLUBIC
        .w_F2UBIC = .w_MVFLUBI2
      .DoRTCalc(26,30,.f.)
      if not(empty(.w_MVROWRIF))
        .link_2_22('Full')
      endif
      .DoRTCalc(31,31,.f.)
        .w_CODCON = IIF(.w_MVTIPCON='F' And Empty(.w_MVCODMAT), .w_MVCODCON, SPACE(15))
      .DoRTCalc(33,38,.f.)
        .w_LOTZOOM = .T.
        .w_UBIZOOM = .T.
      .DoRTCalc(41,41,.f.)
        .w_OLDQTA = .w_MVQTAUM1
    endwith
    this.DoRTCalc(43,62,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_ARTLOT = t_ARTLOT
    this.w_MVARTLOT = t_MVARTLOT
    this.w_MVFLCASC = t_MVFLCASC
    this.w_MVFLRISE = t_MVFLRISE
    this.w_MVF2RISE = t_MVF2RISE
    this.w_MVF2CASC = t_MVF2CASC
    this.w_MVFLLOTT = t_MVFLLOTT
    this.w_MVF2LOTT = t_MVF2LOTT
    this.w_MVCODLOT = t_MVCODLOT
    this.w_FLSTAT = t_FLSTAT
    this.w_MVNUMRIF = t_MVNUMRIF
    this.w_MVFLUBIC = t_MVFLUBIC
    this.w_FLUBIC = t_FLUBIC
    this.w_F2UBIC = t_F2UBIC
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVFLUBI2 = t_MVFLUBI2
    this.w_MVSERRIF = t_MVSERRIF
    this.w_MVCODUBI = t_MVCODUBI
    this.w_MVROWRIF = t_MVROWRIF
    this.w_FLRRIF = t_FLRRIF
    this.w_CODCON = t_CODCON
    this.w_MVCODUB2 = t_MVCODUB2
    this.w_LOTZOOM = t_LOTZOOM
    this.w_UBIZOOM = t_UBIZOOM
    this.w_MVRIFRIG = t_MVRIFRIG
    this.w_OLDQTA = t_OLDQTA
    this.w_UNMIS1 = t_UNMIS1
    this.w_FLLOTT = t_FLLOTT
    this.w_TOTMAT = t_TOTMAT
    this.w_MTCARI = t_MTCARI
    this.w_MVDESART = t_MVDESART
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVCODMAG = t_MVCODMAG
    this.w_MVCODMAT = t_MVCODMAT
    this.w_DESMAT = t_DESMAT
    this.w_DESMAG = t_DESMAG
    this.w_DISLOT = t_DISLOT
    this.w_ARCLAMAT = t_ARCLAMAT
    this.w_ARSALCOM = t_ARSALCOM
    this.w_MVCODCOM = t_MVCODCOM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MVCODICE with this.w_MVCODICE
    replace t_MVCODART with this.w_MVCODART
    replace t_ARTLOT with this.w_ARTLOT
    replace t_MVARTLOT with this.w_MVARTLOT
    replace t_MVFLCASC with this.w_MVFLCASC
    replace t_MVFLRISE with this.w_MVFLRISE
    replace t_MVF2RISE with this.w_MVF2RISE
    replace t_MVF2CASC with this.w_MVF2CASC
    replace t_MVFLLOTT with this.w_MVFLLOTT
    replace t_MVF2LOTT with this.w_MVF2LOTT
    replace t_MVCODLOT with this.w_MVCODLOT
    replace t_FLSTAT with this.w_FLSTAT
    replace t_MVNUMRIF with this.w_MVNUMRIF
    replace t_MVFLUBIC with this.w_MVFLUBIC
    replace t_FLUBIC with this.w_FLUBIC
    replace t_F2UBIC with this.w_F2UBIC
    replace t_MVQTAUM1 with this.w_MVQTAUM1
    replace t_MVFLUBI2 with this.w_MVFLUBI2
    replace t_MVSERRIF with this.w_MVSERRIF
    replace t_MVCODUBI with this.w_MVCODUBI
    replace t_MVROWRIF with this.w_MVROWRIF
    replace t_FLRRIF with this.w_FLRRIF
    replace t_CODCON with this.w_CODCON
    replace t_MVCODUB2 with this.w_MVCODUB2
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_UBIZOOM with this.w_UBIZOOM
    replace t_MVRIFRIG with this.w_MVRIFRIG
    replace t_OLDQTA with this.w_OLDQTA
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_FLLOTT with this.w_FLLOTT
    replace t_TOTMAT with this.w_TOTMAT
    replace t_MTCARI with this.w_MTCARI
    replace t_MVDESART with this.w_MVDESART
    replace t_MVUNIMIS with this.w_MVUNIMIS
    replace t_MVQTAMOV with this.w_MVQTAMOV
    replace t_MVCODMAG with this.w_MVCODMAG
    replace t_MVCODMAT with this.w_MVCODMAT
    replace t_DESMAT with this.w_DESMAT
    replace t_DESMAG with this.w_DESMAG
    replace t_DISLOT with this.w_DISLOT
    replace t_ARCLAMAT with this.w_ARCLAMAT
    replace t_ARSALCOM with this.w_ARSALCOM
    replace t_MVCODCOM with this.w_MVCODCOM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsmd_matPag1 as StdContainer
  Width  = 778
  height = 388
  stdWidth  = 778
  stdheight = 388
  resizeXpos=650
  resizeYpos=225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVNUMDOC_1_11 as StdField with uid="OGHVVNUYGC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 245370871,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=104, Top=38

  add object oMVALFDOC_1_13 as StdField with uid="FKJQCNOITN",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 253353975,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=235, Top=38, InputMask=replicate('X',10)

  add object oMVDATDOC_1_14 as StdField with uid="QDWQNHXVAE",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MVDATDOC", cQueryName = "MVDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 239382519,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=363, Top=38

  add object oMVCAUDOC_1_17 as StdField with uid="WDRTOMBITC",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MVCAUDOC", cQueryName = "MVCAUDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documento",;
    HelpContextID = 238338039,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=9, InputMask=replicate('X',5)


  add object oObj_1_18 as cp_runprogram with uid="XZMXFVYPYA",left=9, top=407, width=258,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMD_BEV('B')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 47299558

  add object oDESDOC_1_19 as StdField with uid="ATTKXKTFRL",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale documento",;
    HelpContextID = 261149130,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=169, Top=9, InputMask=replicate('X',35)


  add object oObj_1_20 as cp_runprogram with uid="UCIJZPMZUV",left=280, top=406, width=258,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSMD_BEV('A')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 47299558


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=121, width=208,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="CPROWORD",Label1="Riga",Field2="MVCODICE",Label2="Articolo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185392762

  add object oStr_1_9 as StdString with uid="QIUTYPWRBD",Visible=.t., Left=6, Top=38,;
    Alignment=1, Width=94, Height=18,;
    Caption="Documento N:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_CLADOC='MM')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="JQYIZLKSQV",Visible=.t., Left=6, Top=38,;
    Alignment=1, Width=94, Height=18,;
    Caption="Movimento N:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_CLADOC<>'MM')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="XECXEQXSDI",Visible=.t., Left=222, Top=38,;
    Alignment=2, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="KVVFVDZNUM",Visible=.t., Left=329, Top=38,;
    Alignment=1, Width=33, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="XSNBWLCNKK",Visible=.t., Left=23, Top=9,;
    Alignment=1, Width=77, Height=18,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsmd_mam",lower(this.oContained.GSMD_MAM.class))=0
        this.oContained.GSMD_MAM.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=140,;
    width=204+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=141,width=203+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_29 as stdDynamicChildContainer with uid="RWJMUQGOGN",bOnScreen=.t.,width=565,height=323,;
   left=210, top=67;


  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsmd_matBodyRow as CPBodyRowCnt
  Width=194
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="HOKJFRCXXG",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 217721194,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMVCODICE_2_2 as StdTrsField with uid="SAXQKXCETF",rtseq=3,rtrep=.t.,;
    cFormVar="w_MVCODICE",value=space(20),enabled=.f.,;
    HelpContextID = 97075211,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=43, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_mat','AGG_LOTT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MVSERIAL=AGG_LOTT.MVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsmd_mat
define class MD_MLUClass as StdTrsForm
* --- devo rivedere l'intero metodo init per attivarmi direttamente in variazione di un form modale
* --- Definisco il puntatore all'oggetto chiamante
oParentObject = .Null.
* --- Testato nella ecpsave, se .f. = conferma chiave in ricerca altrimenti Registra
pFine = .f.

* --- init Modificata rispetto alla standard STDTrsForm a sua volta derivata da STDForm
* --- le modifiche sono tra le righe di commento ZTAM Inizio/Fine
proc Init()
   this.CreateTrs()
   local basename,i,fo
   if not(this.OpenWorkTables())
      ah_Msg(MSG_CANNOT_OPEN_ALL_TABLES,.F.)
   else
     cp_GetSecurity(this,this.cPrg)
     if !(this.bSec1)
        ah_ErrorMsg(MSG_ACCESS_DENIED,'stop',MSG_SECURITY_ADMIN)
        this.CloseAllTables()
        this.release()
        return
     endif
     if type('i_oTreeMenuForm')='O' and i_oTreeMenuForm.left<=0
        this.top=0
        this.left=i_oTreeMenuForm.width+5
     endif
     basename=this.name                        && alternativamente si puo' usare 'sys(2015)'
     public frm&basename
     frm&basename = this
     * --- definisce il nome per il suo cursore
     this.cCursor=sys(2015) &&'cur'+basename
     *this.cSaveCursor=this.cCursor
     this.cKeySet=sys(2015) &&'Key'+basename
     * --- ZTAM Inizio
     this.oParentObject = oParentObject
     this.pFine=.f.
     * --- ZTAM Fine
     * ---
         IF i_Visualtheme=-1	
      	this.MDIForm = i_cViewMode="A" Or i_cViewMode="M"
    ELSE 
	      this.nPrevWndFunc  = GetWindowLong(This.HWnd, -4) && GWL_WNDPROC (costante dichiarata in CP_FORMS
      	IF i_cViewMode <> "S" AND VARTYPE(_screen.TBMDI)<>"U"
           		*--- WM_SYSCOMMAND
      		BINDEVENT(This.Hwnd, 0x0112, _screen.TBMDI, "OnSysCommandForm")
      		*--- WM_NCLBUTTONDBLCLK 0xA3
      		BINDEVENT(This.Hwnd, 0xA3, _screen.TBMDI, "OnNCButtonDblClickForm")      		
      	ENDIF 
    ENDIF
     this.GetFirstControl()
     for i=1 to this.oPgFrm.PageCount-1
        this.oPgFrm.Pages(i).oPag.oContained=this
     next
     * ---
     * --- run-time configuration
     this.oRunTime=createobject('RunTimeConfigurationAgent',this)
     if type("bLoadRuntimeConfig")='U' or bLoadRuntimeConfig
       this.oRunTime.LoadConfig()
     endif
      * ---
     this.NotifyEvent('FormLoad')
     this.CreateChildren()
     *  --- la posizione ?
     this.SetPosition()
    IF i_Visualtheme<>-1
      	IF i_cMenuTab<>"S"
    	 	this.opgfrm.Tabs = .F.
 	      this.opgfrm.borderwidth=0
	      IF i_bGradientBck
			    WITH Thisform.ImgBackGround
				    .Width = Thisform.Width
				    .Height = Thisform.Height
			      .Anchor = 15 && Resize Width and Height
			    	.Picture = i_ThemesManager.GetProp(11)
				    .ZOrder(1)
			      .Visible = .T.
			    ENDWITH 
		    ENDIF
		  Thisform.BackColor = i_ThemesManager.GetProp(7)
		  *--- Rendo trasparenti le pagine	     
		    LOCAL pg, l_i	
		    FOR l_i=1 TO this.oPgFrm.PageCount
		  	  pg = this.oPgFrm.Pages[m.l_i]
	         	pg.backstyle=0
		    NEXT 
		    pg=.null.	
		    this.Addobject("oTabMenu","TabMenu", this, i_cMenuTab="C")	     
	    ENDIF
	  ENDIF
     
     if this.windowtype=0
        this.Show()
     endif
     this.QueryKeySet("1=0","")          && Where inizialmente nulla
     this.ecpQuery()
     * --- ZTAM Inizio
     this.ecpFilter()
     this.w_MVSERIAL = this.oParentObject.w_SERIAL
     this.ecpSave()
     this.pFine=.t.
     this.ecpEdit()
     * --- ZTAM Fine
     this.NotifyEvent('Init')
* --- Zucchetti Aulla Inizio, Activity log
      *--- Tracciatura dell'apertura del form
      if type("g_ACTIVATEPROFILER")='N' AND g_ACTIVATEPROFILER>0
          cp_ActivityLogger(DATETIME(), SECONDS(), SECONDS(), "GSO", "", this, 0, 0, "", this)
      endif 
* --- Zucchetti Aulla Fine, Activity log
     if this.windowtype<>0
        this.Show()
     endif
  endif
  return
 this.cCursorTrs=sys(2015) &&'crt'+this.name
endproc
* --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
proc SetCPToolBar()
     doDefault()
     oCpToolBar.b3.enabled=.f.
     oCpToolBar.b5.enabled=.f.
endproc
* ---- Disattiva i metodi Load e Delete (posso solo variare)
proc ecpLoad()
    * ----
endproc
* --- Esco subito
proc ecpQuit()
     * ----
     this.bUpdated=.t.
     DoDefault()
     if this.cFunction='Query'
        Keyboard "{ESC}"
     endif
endproc
proc ecpSave()
     * ----
     this.bUpdated=.t.
     DoDefault()
     if this.pFine=.t.
        Keyboard "{ESC}"
     endif
endproc
enddefine
* --- Fine Area Manuale
