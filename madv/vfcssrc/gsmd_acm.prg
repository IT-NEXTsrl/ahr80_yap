* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_acm                                                        *
*              Classe matricola                                                *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_108]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-16                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsmd_acm
PARAMETERS pTipCla
* --- Fine Area Manuale
return(createobject("tgsmd_acm"))

* --- Class definition
define class tgsmd_acm as StdForm
  Top    = 4
  Left   = 14

  * --- Standard Properties
  Width  = 613
  Height = 322+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=158708887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  CMT_MAST_IDX = 0
  cFile = "CMT_MAST"
  cKeySelect = "CMCODICE"
  cKeyWhere  = "CMCODICE=this.w_CMCODICE"
  cKeyWhereODBC = '"CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';

  cKeyWhereODBCqualified = '"CMT_MAST.CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';

  cPrg = "gsmd_acm"
  cComment = "Classe matricola"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CMTIPCLA = space(1)
  w_CMCODICE = space(5)
  o_CMCODICE = space(5)
  w_CMDESCRI = space(30)
  w_CMFLAUTO = space(1)
  o_CMFLAUTO = space(1)
  w_CMUNIMAT = space(1)
  w_CMINCREM = 0
  w_ULTPROG = 0
  w_KEYSAL = space(40)
  w_CODPRE = space(5)
  w_TIPCLA = space(1)

  * --- Children pointers
  GSMD_MCM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmd_acm
  pTipCla=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CMT_MAST','gsmd_acm')
    stdPageFrame::Init()
    *set procedure to GSMD_MCM additive
    with this
      .Pages(1).addobject("oPag","tgsmd_acmPag1","gsmd_acm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Classe matricola")
      .Pages(1).HelpContextID = 71315452
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCMCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMD_MCM
    * --- Area Manuale = Init Page Frame
    * --- gsmd_acm
    * --- Imposta il Titolo della Finestra della Classe in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipCla')='L' Or EMPTY(pTipCla)
          .pTipCla = 'M'
       ELSE
          .pTipCla = pTipCla
       ENDIF
    
       DO CASE
             CASE .pTipCla = 'M'
                   .cComment = CP_TRANSLATE('Classi matricole')
                   .cAutoZoom = 'GSMDMACM'
             CASE .pTipCla = 'L'
                   .cComment = CP_TRANSLATE('Classi lotti')
                   .cAutoZoom = 'GSMDLACM'
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CMT_MAST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CMT_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CMT_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSMD_MCM = CREATEOBJECT('stdDynamicChild',this,'GSMD_MCM',this.oPgFrm.Page1.oPag.oLinkPC_1_8)
    this.GSMD_MCM.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMD_MCM)
      this.GSMD_MCM.DestroyChildrenChain()
      this.GSMD_MCM=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_8')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMD_MCM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMD_MCM.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMD_MCM.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMD_MCM.SetKey(;
            .w_CMCODICE,"CMCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMD_MCM.ChangeRow(this.cRowID+'      1',1;
             ,.w_CMCODICE,"CMCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMD_MCM)
        i_f=.GSMD_MCM.BuildFilter()
        if !(i_f==.GSMD_MCM.cQueryFilter)
          i_fnidx=.GSMD_MCM.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMD_MCM.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMD_MCM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMD_MCM.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMD_MCM.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CMCODICE = NVL(CMCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CMT_MAST where CMCODICE=KeySet.CMCODICE
    *
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CMT_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CMT_MAST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CMT_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ULTPROG = 0
        .w_KEYSAL = space(40)
        .w_TIPCLA = space(1)
        .w_CMTIPCLA = NVL(CMTIPCLA,space(1))
        .w_CMCODICE = NVL(CMCODICE,space(5))
        .w_CMDESCRI = NVL(CMDESCRI,space(30))
        .w_CMFLAUTO = NVL(CMFLAUTO,space(1))
        .w_CMUNIMAT = NVL(CMUNIMAT,space(1))
        .w_CMINCREM = NVL(CMINCREM,0)
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(AH_Msgformat(iif(.w_CMTIPCLA="M", "Classe matricola:", "Classe lotto:")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(iiF(Not empty(.w_CMCODICE),AH_Msgformat(iif(.w_CMTIPCLA='M','Lunghezza max codice matricola 40 caratteri','Lunghezza max codice lotto 20 caratteri')),' '))
        .w_CODPRE = .w_CMCODICE
          .link_1_18('Load')
        cp_LoadRecExtFlds(this,'CMT_MAST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsmd_acm
    *Calcola Ultimo Progressivo (asociato alla classe)
    this.NotifyEvent("Carica")
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CMTIPCLA = space(1)
      .w_CMCODICE = space(5)
      .w_CMDESCRI = space(30)
      .w_CMFLAUTO = space(1)
      .w_CMUNIMAT = space(1)
      .w_CMINCREM = 0
      .w_ULTPROG = 0
      .w_KEYSAL = space(40)
      .w_CODPRE = space(5)
      .w_TIPCLA = space(1)
      if .cFunction<>"Filter"
        .w_CMTIPCLA = this.pTipCla
          .DoRTCalc(2,3,.f.)
        .w_CMFLAUTO = 'A'
          .DoRTCalc(5,5,.f.)
        .w_CMINCREM = 0
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(AH_Msgformat(iif(.w_CMTIPCLA="M", "Classe matricola:", "Classe lotto:")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(iiF(Not empty(.w_CMCODICE),AH_Msgformat(iif(.w_CMTIPCLA='M','Lunghezza max codice matricola 40 caratteri','Lunghezza max codice lotto 20 caratteri')),' '))
          .DoRTCalc(7,8,.f.)
        .w_CODPRE = .w_CMCODICE
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CODPRE))
          .link_1_18('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CMT_MAST')
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCMCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oCMDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCMFLAUTO_1_5.enabled = i_bVal
      .Page1.oPag.oCMUNIMAT_1_9.enabled = i_bVal
      .Page1.oPag.oCMINCREM_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_15.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCMCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCMCODICE_1_2.enabled = .t.
        .Page1.oPag.oCMDESCRI_1_3.enabled = .t.
      endif
    endwith
    this.GSMD_MCM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CMT_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMD_MCM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMTIPCLA,"CMTIPCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCODICE,"CMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDESCRI,"CMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLAUTO,"CMFLAUTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMUNIMAT,"CMUNIMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMINCREM,"CMINCREM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsmd_acm
    * --- aggiunge alla Chiave ulteriore filtro su Tipo Classe
    IF NOT EMPTY(i_cWhere)
       IF NOT EMPTY(this.pTipCla)
         i_cWhere=i_cWhere+" and CMTIPCLA='"+this.w_CMTIPCLA+"'"
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    i_lTable = "CMT_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CMT_MAST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMD_SCL with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CMT_MAST_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CMT_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CMT_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CMT_MAST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CMTIPCLA,CMCODICE,CMDESCRI,CMFLAUTO,CMUNIMAT"+;
                  ",CMINCREM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CMTIPCLA)+;
                  ","+cp_ToStrODBC(this.w_CMCODICE)+;
                  ","+cp_ToStrODBC(this.w_CMDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CMFLAUTO)+;
                  ","+cp_ToStrODBC(this.w_CMUNIMAT)+;
                  ","+cp_ToStrODBC(this.w_CMINCREM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CMT_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CMT_MAST')
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE)
        INSERT INTO (i_cTable);
              (CMTIPCLA,CMCODICE,CMDESCRI,CMFLAUTO,CMUNIMAT,CMINCREM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CMTIPCLA;
                  ,this.w_CMCODICE;
                  ,this.w_CMDESCRI;
                  ,this.w_CMFLAUTO;
                  ,this.w_CMUNIMAT;
                  ,this.w_CMINCREM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CMT_MAST_IDX,i_nConn)
      *
      * update CMT_MAST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CMT_MAST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CMTIPCLA="+cp_ToStrODBC(this.w_CMTIPCLA)+;
             ",CMDESCRI="+cp_ToStrODBC(this.w_CMDESCRI)+;
             ",CMFLAUTO="+cp_ToStrODBC(this.w_CMFLAUTO)+;
             ",CMUNIMAT="+cp_ToStrODBC(this.w_CMUNIMAT)+;
             ",CMINCREM="+cp_ToStrODBC(this.w_CMINCREM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CMT_MAST')
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
        UPDATE (i_cTable) SET;
              CMTIPCLA=this.w_CMTIPCLA;
             ,CMDESCRI=this.w_CMDESCRI;
             ,CMFLAUTO=this.w_CMFLAUTO;
             ,CMUNIMAT=this.w_CMUNIMAT;
             ,CMINCREM=this.w_CMINCREM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMD_MCM : Saving
      this.GSMD_MCM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CMCODICE,"CMCODICE";
             )
      this.GSMD_MCM.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsmd_acm
    this.Notifyevent('CHECKPRO')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMD_MCM : Deleting
    this.GSMD_MCM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CMCODICE,"CMCODICE";
           )
    this.GSMD_MCM.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CMT_MAST_IDX,i_nConn)
      *
      * delete CMT_MAST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_CMFLAUTO<>.w_CMFLAUTO
            .w_CMINCREM = 0
        endif
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(AH_Msgformat(iif(.w_CMTIPCLA="M", "Classe matricola:", "Classe lotto:")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(iiF(Not empty(.w_CMCODICE),AH_Msgformat(iif(.w_CMTIPCLA='M','Lunghezza max codice matricola 40 caratteri','Lunghezza max codice lotto 20 caratteri')),' '))
        .DoRTCalc(7,8,.t.)
        if .o_CMCODICE<>.w_CMCODICE
            .w_CODPRE = .w_CMCODICE
          .link_1_18('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(AH_Msgformat(iif(.w_CMTIPCLA="M", "Classe matricola:", "Classe lotto:")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(iiF(Not empty(.w_CMCODICE),AH_Msgformat(iif(.w_CMTIPCLA='M','Lunghezza max codice matricola 40 caratteri','Lunghezza max codice lotto 20 caratteri')),' '))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCMFLAUTO_1_5.enabled = this.oPgFrm.Page1.oPag.oCMFLAUTO_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCMINCREM_1_11.enabled = this.oPgFrm.Page1.oPag.oCMINCREM_1_11.mCond()
    this.GSMD_MCM.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCMUNIMAT_1_9.visible=!this.oPgFrm.Page1.oPag.oCMUNIMAT_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCMINCREM_1_11.visible=!this.oPgFrm.Page1.oPag.oCMINCREM_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPRE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODPRE)
            select CMCODICE,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRE = NVL(_Link_.CMCODICE,space(5))
      this.w_TIPCLA = NVL(_Link_.CMTIPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRE = space(5)
      endif
      this.w_TIPCLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCMCODICE_1_2.value==this.w_CMCODICE)
      this.oPgFrm.Page1.oPag.oCMCODICE_1_2.value=this.w_CMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDESCRI_1_3.value==this.w_CMDESCRI)
      this.oPgFrm.Page1.oPag.oCMDESCRI_1_3.value=this.w_CMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLAUTO_1_5.RadioValue()==this.w_CMFLAUTO)
      this.oPgFrm.Page1.oPag.oCMFLAUTO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMUNIMAT_1_9.RadioValue()==this.w_CMUNIMAT)
      this.oPgFrm.Page1.oPag.oCMUNIMAT_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMINCREM_1_11.value==this.w_CMINCREM)
      this.oPgFrm.Page1.oPag.oCMINCREM_1_11.value=this.w_CMINCREM
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPROG_1_13.value==this.w_ULTPROG)
      this.oPgFrm.Page1.oPag.oULTPROG_1_13.value=this.w_ULTPROG
    endif
    cp_SetControlsValueExtFlds(this,'CMT_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CMCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CMINCREM))  and not(.w_CMFLAUTO<>'A')  and (.w_CMFLAUTO='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMINCREM_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CMINCREM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMD_MCM.CheckForm()
      if i_bres
        i_bres=  .GSMD_MCM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_acm
      with this
        if .bUpdated OR .gsmd_mcm.bUpdated
          do case
            case Not Empty(.w_CODPRE) AND .cFunction='Load'
             i_bnoChk = .f.
             i_bRes = .f.
             i_cErrorMsg =IIF(.w_TIPCLA='M', Ah_MsgFormat("Esiste classe matricola con lo stesso codice"),Ah_MsgFormat("Esiste classe lotto con lo stesso codice"))
            case (.gsmd_mcm.w_TOTLUNGHE > 40 and .w_CMTIPCLA='M')
              i_bnoChk = .f.
              i_bRes = .f.
              i_cErrorMsg = Ah_MsgFormat("Il codice della matricola non deve essere maggiore di 40 caratteri")
            case (.gsmd_mcm.w_TOTLUNGHE > 20 and .w_CMTIPCLA='L')
              i_bnoChk = .f.
              i_bRes = .f.
              i_cErrorMsg = Ah_MsgFormat("Il codice del lotto non deve essere maggiore di 20 caratteri")
          endcase
        endif
      endwith
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CMCODICE = this.w_CMCODICE
    this.o_CMFLAUTO = this.w_CMFLAUTO
    * --- GSMD_MCM : Depends On
    this.GSMD_MCM.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsmd_acmPag1 as StdContainer
  Width  = 609
  height = 323
  stdWidth  = 609
  stdheight = 323
  resizeXpos=346
  resizeYpos=252
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCMCODICE_1_2 as StdField with uid="TACRJIJIKB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CMCODICE", cQueryName = "CMCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice classe",;
    HelpContextID = 150391189,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=117, Top=19, InputMask=replicate('X',5)

  add object oCMDESCRI_1_3 as StdField with uid="MDQVTXOEWP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CMDESCRI", cQueryName = "CMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe",;
    HelpContextID = 235977105,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=117, Top=47, InputMask=replicate('X',30)


  add object oCMFLAUTO_1_5 as StdCombo with uid="CLLPCPROAP",rtseq=4,rtrep=.f.,left=505,top=48,width=90,height=21;
    , ToolTipText = "Flag automatico (S) o manuale";
    , HelpContextID = 220830091;
    , cFormVar="w_CMFLAUTO",RowSource=""+"Automatico,"+"Manuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLAUTO_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oCMFLAUTO_1_5.GetRadio()
    this.Parent.oContained.w_CMFLAUTO = this.RadioValue()
    return .t.
  endfunc

  func oCMFLAUTO_1_5.SetRadio()
    this.Parent.oContained.w_CMFLAUTO=trim(this.Parent.oContained.w_CMFLAUTO)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLAUTO=='A',1,;
      iif(this.Parent.oContained.w_CMFLAUTO=='M',2,;
      0))
  endfunc

  func oCMFLAUTO_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction <> 'Edit')
    endwith
   endif
  endfunc


  add object oLinkPC_1_8 as stdDynamicChildContainer with uid="BQCSXLSDMN",left=2, top=75, width=605, height=218, bOnScreen=.t.;


  func oLinkPC_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CMFLAUTO='A')
      endwith
    endif
  endfunc

  add object oCMUNIMAT_1_9 as StdCheck with uid="OTZBPNFPZU",rtseq=5,rtrep=.f.,left=9, top=298, caption="Matricole univoche",;
    ToolTipText = "Se attivo non permette la presenza di matricole doppie per la classe",;
    HelpContextID = 78031238,;
    cFormVar="w_CMUNIMAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCMUNIMAT_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCMUNIMAT_1_9.GetRadio()
    this.Parent.oContained.w_CMUNIMAT = this.RadioValue()
    return .t.
  endfunc

  func oCMUNIMAT_1_9.SetRadio()
    this.Parent.oContained.w_CMUNIMAT=trim(this.Parent.oContained.w_CMUNIMAT)
    this.value = ;
      iif(this.Parent.oContained.w_CMUNIMAT=='S',1,;
      0)
  endfunc

  func oCMUNIMAT_1_9.mHide()
    with this.Parent.oContained
      return (g_UNIMAT<>'C' or .w_CMTipCla='L')
    endwith
  endfunc

  add object oCMINCREM_1_11 as StdField with uid="FSGTBBXSFT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CMINCREM", cQueryName = "CMINCREM",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Incremento progressivo",;
    HelpContextID = 485773,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=567, Top=301, cSayPict='"9999"', cGetPict='"9999"'

  func oCMINCREM_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CMFLAUTO='A')
    endwith
   endif
  endfunc

  func oCMINCREM_1_11.mHide()
    with this.Parent.oContained
      return (.w_CMFLAUTO<>'A')
    endwith
  endfunc


  add object oObj_1_12 as cp_runprogram with uid="IKTKTRDRQJ",left=450, top=341, width=169,height=24,;
    caption='GSMD_BSZ',;
   bGlobalFont=.t.,;
    prg="GSMD_BSZ('A')",;
    cEvent = "Carica",;
    nPag=1;
    , HelpContextID = 240198464

  add object oULTPROG_1_13 as StdField with uid="OXNYTBTAME",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ULTPROG", cQueryName = "ULTPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo progressivo utilizzato",;
    HelpContextID = 34912698,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=456, Top=19, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'


  add object oObj_1_15 as cp_runprogram with uid="KZTKXFXZBP",left=2, top=337, width=223,height=24,;
    caption='GSMD_BSZ',;
   bGlobalFont=.t.,;
    prg="GSMD_BSZ('P')",;
    cEvent = "CHECKPRO",;
    nPag=1;
    , ToolTipText = "verifica che esista almeno un progressivo";
    , HelpContextID = 240198464


  add object oObj_1_16 as cp_calclbl with uid="AACOZNETBR",left=3, top=20, width=110,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 200164378


  add object oObj_1_17 as cp_calclbl with uid="UWOIDONZWZ",left=151, top=306, width=264,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=0,FontSize=8,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 200164378

  add object oStr_1_4 as StdString with uid="NPTDJABPXL",Visible=.t., Left=3, Top=51,;
    Alignment=1, Width=110, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YVOXARTJAU",Visible=.t., Left=360, Top=51,;
    Alignment=1, Width=141, Height=18,;
    Caption="Tipo aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TZEWHSSQGP",Visible=.t., Left=426, Top=305,;
    Alignment=1, Width=139, Height=18,;
    Caption="Incremento progressivo:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_CMFLAUTO<>'A')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="QOAHOCUUTF",Visible=.t., Left=314, Top=23,;
    Alignment=1, Width=139, Height=18,;
    Caption="Ultimo numero:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_acm','CMT_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CMCODICE=CMT_MAST.CMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
