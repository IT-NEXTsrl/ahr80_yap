* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_ssl                                                        *
*              Stampa schede lotti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_88]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-25                                                      *
* Last revis.: 2008-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_ssl",oParentObject))

* --- Class definition
define class tgsmd_ssl as StdForm
  Top    = 6
  Left   = 6

  * --- Standard Properties
  Width  = 535
  Height = 319
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-11-07"
  HelpContextID=177583255
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  KEY_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  UNIT_LOG_IDX = 0
  cPrg = "gsmd_ssl"
  cComment = "Stampa schede lotti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  w_ARTINI = space(20)
  o_ARTINI = space(20)
  w_ARTFIN = space(20)
  o_ARTFIN = space(20)
  w_LOTTO = space(20)
  o_LOTTO = space(20)
  w_FLARSTO = space(1)
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_CODUBI = space(20)
  w_DATCREIN = ctod('  /  /  ')
  w_FLUBIC = space(1)
  w_DATCREFI = ctod('  /  /  ')
  w_STATO = space(1)
  w_FLUNILOG = space(1)
  o_FLUNILOG = space(1)
  w_UNILOGINI = space(18)
  w_UNILOGFIN = space(18)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_FLLINI = space(1)
  w_APPART = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_sslPag1","gsmd_ssl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='UBICAZIO'
    this.cWorkTables[6]='UNIT_LOG'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_OREP)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_ARTINI=space(20)
      .w_ARTFIN=space(20)
      .w_LOTTO=space(20)
      .w_FLARSTO=space(1)
      .w_CODMAG=space(5)
      .w_CODUBI=space(20)
      .w_DATCREIN=ctod("  /  /  ")
      .w_FLUBIC=space(1)
      .w_DATCREFI=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_FLUNILOG=space(1)
      .w_UNILOGINI=space(18)
      .w_UNILOGFIN=space(18)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_FLLINI=space(1)
      .w_APPART=space(20)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ARTINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ARTFIN))
          .link_1_4('Full')
        endif
        .w_LOTTO = IIF(.w_ARTINI = .w_ARTFIN, .w_LOTTO, SPACE(20) )
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_LOTTO))
          .link_1_5('Full')
        endif
        .w_FLARSTO = 'N'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODMAG))
          .link_1_7('Full')
        endif
        .w_CODUBI = Space(20)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODUBI))
          .link_1_8('Full')
        endif
        .w_DATCREIN = IIF(!EMPTY(.w_LOTTO),cp_CharToDate('  -  -  '),.w_DATCREIN)
          .DoRTCalc(10,10,.f.)
        .w_DATCREFI = IIF(!EMPTY(.w_LOTTO),cp_CharToDate('  -  -  '),.w_DATCREFI)
        .w_STATO = IIF(EMPTY(.w_LOTTO),'D','')
        .w_FLUNILOG = 'T'
        .w_UNILOGINI = space(18)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_UNILOGINI))
          .link_1_14('Full')
        endif
        .w_UNILOGFIN = space(18)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_UNILOGFIN))
          .link_1_15('Full')
        endif
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
    this.DoRTCalc(17,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_2('Full')
        endif
          .link_1_3('Full')
          .link_1_4('Full')
        if .o_ARTFIN<>.w_ARTFIN.or. .o_ARTINI<>.w_ARTINI
            .w_LOTTO = IIF(.w_ARTINI = .w_ARTFIN, .w_LOTTO, SPACE(20) )
          .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.t.)
        if .o_CODMAG<>.w_CODMAG
            .w_CODUBI = Space(20)
          .link_1_8('Full')
        endif
        if .o_LOTTO<>.w_LOTTO
            .w_DATCREIN = IIF(!EMPTY(.w_LOTTO),cp_CharToDate('  -  -  '),.w_DATCREIN)
        endif
        .DoRTCalc(10,10,.t.)
        if .o_LOTTO<>.w_LOTTO
            .w_DATCREFI = IIF(!EMPTY(.w_LOTTO),cp_CharToDate('  -  -  '),.w_DATCREFI)
        endif
        if .o_LOTTO<>.w_LOTTO
            .w_STATO = IIF(EMPTY(.w_LOTTO),'D','')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_FLUNILOG<>.w_FLUNILOG
            .w_UNILOGINI = space(18)
          .link_1_14('Full')
        endif
        if .o_FLUNILOG<>.w_FLUNILOG
            .w_UNILOGFIN = space(18)
          .link_1_15('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOTTO_1_5.enabled = this.oPgFrm.Page1.oPag.oLOTTO_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODUBI_1_8.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_1_12.enabled = this.oPgFrm.Page1.oPag.oSTATO_1_12.mCond()
    this.oPgFrm.Page1.oPag.oUNILOGINI_1_14.enabled = this.oPgFrm.Page1.oPag.oUNILOGINI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oUNILOGFIN_1_15.enabled = this.oPgFrm.Page1.oPag.oUNILOGFIN_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATCREIN_1_9.visible=!this.oPgFrm.Page1.oPag.oDATCREIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDATCREFI_1_11.visible=!this.oPgFrm.Page1.oPag.oDATCREFI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'',"Articoli",'GSMD_SSL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_ARTINI = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_ARTINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODFIN) OR .w_CODINI<= .w_CODFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo non valido oppure obsoleto o maggiore del codice finale")
        endif
        this.w_CODINI = space(20)
        this.w_ARTINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_2'),i_cWhere,'',"Articoli",'GSMD_SSL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_ARTFIN = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_ARTFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODINI) OR .w_CODINI<= .w_CODFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo non valido oppure obsoleto o minore del codice iniziale")
        endif
        this.w_CODFIN = space(20)
        this.w_ARTFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_FLLINI = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_FLLINI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTTO)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_ARTINI);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_ARTINI;
                     ,'LOCODICE',trim(this.w_LOTTO))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTO)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTO) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTTO_1_5'),i_cWhere,'GSMD_ALO',"Lotti",'GSMD_SSL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARTINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_ARTINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTO);
                   +" and LOCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_ARTINI;
                       ,'LOCODICE',this.w_LOTTO)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_APPART = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTO = space(20)
      endif
      this.w_APPART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_LOTTO) OR .w_APPART=.w_ARTINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTTO = space(20)
        this.w_APPART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_7'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_8'),i_cWhere,'GSMD_MUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNILOGINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNILOGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_UNILOGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_UNILOGINI))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNILOGINI)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNILOGINI) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oUNILOGINI_1_14'),i_cWhere,'',"Unit� logistiche",'GSMD_SLU.UNIT_LOG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNILOGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_UNILOGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_UNILOGINI)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNILOGINI = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_UNILOGINI = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endif
        this.w_UNILOGINI = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNILOGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNILOGFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNILOGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_UNILOGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_UNILOGFIN))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNILOGFIN)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNILOGFIN) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oUNILOGFIN_1_15'),i_cWhere,'',"Unit� logistiche",'GSMD_SLU.UNIT_LOG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNILOGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_UNILOGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_UNILOGFIN)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNILOGFIN = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_UNILOGFIN = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endif
        this.w_UNILOGFIN = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNILOGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_2.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_2.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTO_1_5.value==this.w_LOTTO)
      this.oPgFrm.Page1.oPag.oLOTTO_1_5.value=this.w_LOTTO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARSTO_1_6.RadioValue()==this.w_FLARSTO)
      this.oPgFrm.Page1.oPag.oFLARSTO_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_7.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_7.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_8.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_8.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREIN_1_9.value==this.w_DATCREIN)
      this.oPgFrm.Page1.oPag.oDATCREIN_1_9.value=this.w_DATCREIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREFI_1_11.value==this.w_DATCREFI)
      this.oPgFrm.Page1.oPag.oDATCREFI_1_11.value=this.w_DATCREFI
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_12.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLUNILOG_1_13.RadioValue()==this.w_FLUNILOG)
      this.oPgFrm.Page1.oPag.oFLUNILOG_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUNILOGINI_1_14.value==this.w_UNILOGINI)
      this.oPgFrm.Page1.oPag.oUNILOGINI_1_14.value=this.w_UNILOGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oUNILOGFIN_1_15.value==this.w_UNILOGFIN)
      this.oPgFrm.Page1.oPag.oUNILOGFIN_1_15.value=this.w_UNILOGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_23.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_23.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_24.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_24.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_CODFIN) OR .w_CODINI<= .w_CODFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo non valido oppure obsoleto o maggiore del codice finale")
          case   not((empty(.w_CODINI) OR .w_CODINI<= .w_CODFIN) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo non valido oppure obsoleto o minore del codice iniziale")
          case   not(EMPTY(.w_LOTTO) OR .w_APPART=.w_ARTINI)  and ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_ARTINI=.w_ARTFIN)  and not(empty(.w_LOTTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTO_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
          case   not(((empty(.w_DATCREFI)) OR  (.w_DATCREIN<=.w_DATCREFI)))  and not(!EMPTY(.w_LOTTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(((.w_DATCREFI>=.w_DATCREIN) or (empty(.w_DATCREIN))))  and not(!EMPTY(.w_LOTTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREFI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN))  and (.w_FLUNILOG='D')  and not(empty(.w_UNILOGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUNILOGINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
          case   not(.w_UNILOGINI <= .w_UNILOGFIN or empty(.w_UNILOGFIN))  and (.w_FLUNILOG='D')  and not(empty(.w_UNILOGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUNILOGFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_ARTINI = this.w_ARTINI
    this.o_ARTFIN = this.w_ARTFIN
    this.o_LOTTO = this.w_LOTTO
    this.o_CODMAG = this.w_CODMAG
    this.o_FLUNILOG = this.w_FLUNILOG
    return

enddefine

* --- Define pages as container
define class tgsmd_sslPag1 as StdContainer
  Width  = 531
  height = 319
  stdWidth  = 531
  stdheight = 319
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="XCBYWDJYVV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo non valido oppure obsoleto o maggiore del codice finale",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 121419738,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=108, Top=12, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMD_SSL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCODFIN_1_2 as StdField with uid="PXUGAIEXGP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo non valido oppure obsoleto o minore del codice iniziale",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 42973146,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=108, Top=40, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMD_SSL.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oLOTTO_1_5 as StdField with uid="YCJDETCCJH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LOTTO", cQueryName = "LOTTO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 266291382,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=108, Top=68, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_ARTINI", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTTO"

  func oLOTTO_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT EMPTY(.w_CODINI) OR NOT EMPTY(.w_CODFIN)) AND .w_ARTINI=.w_ARTFIN)
    endwith
   endif
  endfunc

  func oLOTTO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_ARTINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_ARTINI)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTTO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'GSMD_SSL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTTO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_ARTINI
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTTO
     i_obj.ecpSave()
  endproc

  add object oFLARSTO_1_6 as StdCheck with uid="GVKODKDZJD",rtseq=6,rtrep=.f.,left=294, top=68, caption="Movimenti storicizzati",;
    ToolTipText = "Se attivo saranno visualizzati anche i movim. di magazzino e i documenti storicizzati",;
    HelpContextID = 199486122,;
    cFormVar="w_FLARSTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLARSTO_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLARSTO_1_6.GetRadio()
    this.Parent.oContained.w_FLARSTO = this.RadioValue()
    return .t.
  endfunc

  func oFLARSTO_1_6.SetRadio()
    this.Parent.oContained.w_FLARSTO=trim(this.Parent.oContained.w_FLARSTO)
    this.value = ;
      iif(this.Parent.oContained.w_FLARSTO=='S',1,;
      0)
  endfunc

  add object oCODMAG_1_7 as StdField with uid="VTQJRAKUEU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 168343514,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=108, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODUBI_1_8 as StdField with uid="HVIFQLPUYT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione (vuota tutte)",;
    HelpContextID = 133216218,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=294, Top=97, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODMAG) and .w_Flubic='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this.parent.oContained
  endproc
  proc oCODUBI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oDATCREIN_1_9 as StdField with uid="ZERROBMDHL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATCREIN", cQueryName = "DATCREIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data creazione iniziale",;
    HelpContextID = 83769988,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=108, Top=124

  func oDATCREIN_1_9.mHide()
    with this.Parent.oContained
      return (!EMPTY(.w_LOTTO))
    endwith
  endfunc

  func oDATCREIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_DATCREFI)) OR  (.w_DATCREIN<=.w_DATCREFI)))
    endwith
    return bRes
  endfunc

  add object oDATCREFI_1_11 as StdField with uid="ZLVEZVUSOH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATCREFI", cQueryName = "DATCREFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data creazione finale",;
    HelpContextID = 184665473,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=294, Top=124

  func oDATCREFI_1_11.mHide()
    with this.Parent.oContained
      return (!EMPTY(.w_LOTTO))
    endwith
  endfunc

  func oDATCREFI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_DATCREFI>=.w_DATCREIN) or (empty(.w_DATCREIN))))
    endwith
    return bRes
  endfunc


  add object oSTATO_1_12 as StdCombo with uid="DXGFSJMRSK",value=4,rtseq=12,rtrep=.f.,left=425,top=124,width=98,height=21;
    , ToolTipText = "Status del lotto";
    , HelpContextID = 266214950;
    , cFormVar="w_STATO",RowSource=""+"Disponibile,"+"Sospeso,"+"Esaurito,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_12.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oSTATO_1_12.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_12.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='D',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='E',3,;
      iif(this.Parent.oContained.w_STATO=='',4,;
      0))))
  endfunc

  func oSTATO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_LOTTO))
    endwith
   endif
  endfunc


  add object oFLUNILOG_1_13 as StdCombo with uid="BMZONYGOGL",rtseq=13,rtrep=.f.,left=108,top=153,width=100,height=21;
    , ToolTipText = "Filtra solo documenti, mov. magaz. o tutti";
    , HelpContextID = 192501149;
    , cFormVar="w_FLUNILOG",RowSource=""+"Documenti,"+"Mov. magaz.,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLUNILOG_1_13.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLUNILOG_1_13.GetRadio()
    this.Parent.oContained.w_FLUNILOG = this.RadioValue()
    return .t.
  endfunc

  func oFLUNILOG_1_13.SetRadio()
    this.Parent.oContained.w_FLUNILOG=trim(this.Parent.oContained.w_FLUNILOG)
    this.value = ;
      iif(this.Parent.oContained.w_FLUNILOG=='D',1,;
      iif(this.Parent.oContained.w_FLUNILOG=='M',2,;
      iif(this.Parent.oContained.w_FLUNILOG=='T',3,;
      0)))
  endfunc

  add object oUNILOGINI_1_14 as StdField with uid="MRFEUARJHA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_UNILOGINI", cQueryName = "UNILOGINI",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione",;
    ToolTipText = "Unit� logistica di inizio selezione",;
    HelpContextID = 114728228,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=108, Top=186, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_UNILOGINI"

  func oUNILOGINI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLUNILOG='D')
    endwith
   endif
  endfunc

  func oUNILOGINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNILOGINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNILOGINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oUNILOGINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'GSMD_SLU.UNIT_LOG_VZM',this.parent.oContained
  endproc

  add object oUNILOGFIN_1_15 as StdField with uid="DEVFSRMLOM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_UNILOGFIN", cQueryName = "UNILOGFIN",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� logistica di inizio selezione maggiore dell'unit� logistica di fine selezione",;
    ToolTipText = "Unit� logistica di fine selezione",;
    HelpContextID = 153707153,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=108, Top=214, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_UNILOGFIN"

  func oUNILOGFIN_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLUNILOG='D')
    endwith
   endif
  endfunc

  func oUNILOGFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNILOGFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNILOGFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oUNILOGFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'GSMD_SLU.UNIT_LOG_VZM',this.parent.oContained
  endproc

  add object oDESINI_1_23 as StdField with uid="GUEMXCCVNR",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121360842,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=265, Top=12, InputMask=replicate('X',40)

  add object oDESFIN_1_24 as StdField with uid="QORTMLGUYO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42914250,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=265, Top=40, InputMask=replicate('X',40)


  add object oObj_1_31 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=242, width=416,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181290010


  add object oBtn_1_32 as StdButton with uid="GHEVIDFOUV",left=425, top=269, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 217498074;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="GNIBZHELYY",left=476, top=269, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184900678;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_18 as StdString with uid="TJXSHDCNVW",Visible=.t., Left=6, Top=68,;
    Alignment=1, Width=101, Height=15,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PQGYQOMLEU",Visible=.t., Left=6, Top=124,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da data creaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LOTTO))
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="FONTFXNVLG",Visible=.t., Left=189, Top=124,;
    Alignment=1, Width=104, Height=18,;
    Caption="A data creaz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (!EMPTY(.w_LOTTO))
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="WLIIABFPSY",Visible=.t., Left=376, Top=124,;
    Alignment=1, Width=44, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TSSRKOVYTD",Visible=.t., Left=6, Top=242,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UVUVQUPFNI",Visible=.t., Left=6, Top=12,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QOSLIITKBL",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=101, Height=18,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UHVMFKZCSX",Visible=.t., Left=6, Top=97,;
    Alignment=1, Width=101, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="UMDUCJAZBD",Visible=.t., Left=212, Top=97,;
    Alignment=1, Width=81, Height=15,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="DQQGOXZULM",Visible=.t., Left=6, Top=186,;
    Alignment=1, Width=101, Height=18,;
    Caption="Da unit� log.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GKWTRTKFBA",Visible=.t., Left=6, Top=214,;
    Alignment=1, Width=101, Height=18,;
    Caption="A unit� log.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VVJSXHFMRW",Visible=.t., Left=6, Top=153,;
    Alignment=1, Width=101, Height=18,;
    Caption="Filtro unit� log.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_ssl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
