* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bcm                                                        *
*              Caricamento codici matricole                                    *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_161]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-14                                                      *
* Last revis.: 2011-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM,pCODICE,pQTAUM1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bcm",oParentObject,m.pPARAM,m.pCODICE,m.pQTAUM1)
return(i_retval)

define class tgsmd_bcm as StdBatch
  * --- Local variables
  pPARAM = space(1)
  pCODICE = space(41)
  pQTAUM1 = 0
  w_KEYSAL = space(40)
  w_CODART = space(20)
  w_CMCODICE = space(5)
  w_INCREM = 0
  w_ULTPROG = 0
  w_NUMERR = 0
  w_LUNGH = 0
  w_CODMATR = space(40)
  w_CODMATR1 = space(40)
  w_CODMATR2 = space(40)
  w_ANNO = 0
  w_SEMEST = 0
  w_QUADRIM = 0
  w_TRIMEST = 0
  w_MESE = 0
  w_GIORNO = 0
  w_FUNZIONE = space(1)
  w_VALORE = space(35)
  w_LPROGR = 0
  w_PROGR = 0
  w_OPROGR = 0
  w_OKPROG = .f.
  w_IND = 0
  w_ERR = .f.
  w_OK = .f.
  w_LANCIATODAFORM = .f.
  w_UNIMAT = space(1)
  w_NOGEN = 0
  w_noloop = 0
  w_OLDMATR = space(40)
  w_TEST = .f.
  w_PROG = 0
  w_LOCODICE = space(20)
  * --- WorkFile variables
  CMT_MAST_idx=0
  CMT_DETT_idx=0
  MATRICOL_idx=0
  PROGMAT_idx=0
  ART_ICOL_idx=0
  LOTTIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pPARAM='M' -> Caricamento Codici Matricole da GSMD_KCM
    *     pPARAM='L' -> Caricamento Codici Matricole da GSMD_KLO
    if TYPE( "this.oParentObject" ) <> "O"
      * --- PRESUMIBILEMTE IL BATCH � STATO UTILIZZATO COME FUNZIONE, per cui
      *     oParentObject non � valorizzato con un oggetto
      this.pQTAUM1 = this.pCODICE
      this.pCODICE = this.pPARAM
      this.pPARAM = oParentObject
      this.oParentObject = .NULL.
    endif
    if TYPE( "this.oParentObject" ) = "O"
      this.w_LANCIATODAFORM = UPPER( this.oParentObject.BaseClass ) = "FORM"
    else
      this.w_LANCIATODAFORM = .F.
    endif
    this.w_KEYSAL = this.pCODICE
    this.w_CODART = this.pCODICE
    if this.pPARAM = "L"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCLALOT"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCLALOT;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CMCODICE = NVL(cp_ToDate(_read_.ARCLALOT),cp_NullValue(_read_.ARCLALOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.pPARAM = "M"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCLAMAT"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCLAMAT;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CMCODICE = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Read from CMT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CMT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMINCREM,CMUNIMAT"+;
        " from "+i_cTable+" CMT_MAST where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CMCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMINCREM,CMUNIMAT;
        from (i_cTable) where;
            CMCODICE = this.w_CMCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INCREM = NVL(cp_ToDate(_read_.CMINCREM),cp_NullValue(_read_.CMINCREM))
      this.w_UNIMAT = NVL(cp_ToDate(_read_.CMUNIMAT),cp_NullValue(_read_.CMUNIMAT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.pPARAM = "L"
      this.w_ULTPROG = 0
    endif
    if this.pPARAM = "M"
      if TYPE("this.oParentObject.w_ULTPROG") <> "U"
        this.w_ULTPROG = this.oParentObject.w_ULTPROG
      else
        this.w_ULTPROG = 0
        * --- Select from PROGMAT
        i_nConn=i_TableProp[this.PROGMAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROGMAT_idx,2],.t.,this.PROGMAT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX( PR_PROGR ) AS PR_PROGR  from "+i_cTable+" PROGMAT ";
              +" where PRCLAMAT = "+cp_ToStrODBC(this.w_CMCODICE)+" AND PRKEYSAL = "+cp_ToStrODBC(this.pCODICE)+"";
              +" group by PRCLAMAT , PRKEYSAL";
               ,"_Curs_PROGMAT")
        else
          select MAX( PR_PROGR ) AS PR_PROGR from (i_cTable);
           where PRCLAMAT = this.w_CMCODICE AND PRKEYSAL = this.pCODICE;
           group by PRCLAMAT , PRKEYSAL;
            into cursor _Curs_PROGMAT
        endif
        if used('_Curs_PROGMAT')
          select _Curs_PROGMAT
          locate for 1=1
          do while not(eof())
          this.w_ULTPROG = NVL( PR_PROGR , 0 )
            select _Curs_PROGMAT
            continue
          enddo
          use
        endif
      endif
    endif
    if this.pQTAUM1=0
      ah_ErrorMsg("Nessun codice matricola generato","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Calcolo Valore in base alla Funzione
    this.w_NUMERR = 0
    this.w_CODMATR1 = ""
    this.w_CODMATR2 = ""
    this.w_ANNO = YEAR(i_DATSYS)
    this.w_MESE = MONTH(i_DATSYS)
    this.w_GIORNO = DAY(i_DATSYS)
    this.w_SEMEST = IIF(this.w_MESE<7, 1, 2)
    this.w_QUADRIM = iif(this.w_MESE<5, 1, iif(this.w_MESE<9, 2, 3 ) )
    this.w_TRIMEST = iif(this.w_MESE<4, 1, iif(this.w_MESE<7, 2, iif(this.w_MESE<10, 3, 4 ) ) )
    * --- Costruisco le due sottostringhe parti fisse del Codice Matricola
    * --- Select from CMT_DETT
    i_nConn=i_TableProp[this.CMT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CMT_DETT_idx,2],.t.,this.CMT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CMT_DETT ";
          +" where CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)+"";
          +" order by CMPOSIZI,CPROWNUM";
           ,"_Curs_CMT_DETT")
    else
      select * from (i_cTable);
       where CMCODICE=this.w_CMCODICE;
       order by CMPOSIZI,CPROWNUM;
        into cursor _Curs_CMT_DETT
    endif
    if used('_Curs_CMT_DETT')
      select _Curs_CMT_DETT
      locate for 1=1
      do while not(eof())
      this.w_LUNGH = _Curs_CMT_DETT.CMLUNGHE
      this.w_FUNZIONE = _Curs_CMT_DETT.CM__FUNC
      do case
        case this.w_FUNZIONE="A"
          this.w_VALORE = STR(this.w_ANNO)
        case this.w_FUNZIONE="S"
          this.w_VALORE = STR(this.w_SEMEST)
        case this.w_FUNZIONE="Q"
          this.w_VALORE = STR(this.w_QUADRIM)
        case this.w_FUNZIONE="T"
          this.w_VALORE = STR(this.w_TRIMEST)
        case this.w_FUNZIONE="M"
          if this.w_LUNGH > 2
            this.w_VALORE = CMONTH(i_DATSYS)
          else
            this.w_VALORE = PADL(ALLTRIM(STR(this.w_MESE)),2,"0")
          endif
        case this.w_FUNZIONE="G"
          this.w_VALORE = PADL(ALLTRIM(STR(this.w_GIORNO)),2,"0")
        case this.w_FUNZIONE="N"
          this.w_VALORE = _Curs_CMT_DETT.CM_START
        case this.w_FUNZIONE="P"
          * --- Progressivo:memorizzo il valore iniziale presente in CMT_DETT. 
          *     w_OKPROG=.t. comporta la fine della prima parte fissa e l'inizio della 
          *     seconda.La parte dinamica � determinata dal progressivo calcolato sotto transazione
          if this.pPARAM="M"
            if g_UNIMAT$"ME" OR (g_UNIMAT="C" And this.w_UNIMAT="S")
              * --- Select from PROGMAT
              i_nConn=i_TableProp[this.PROGMAT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROGMAT_idx,2],.t.,this.PROGMAT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Max( PR_PROGR ) As Massimo  from "+i_cTable+" PROGMAT ";
                    +" where PRCLAMAT="+cp_ToStrODBC(this.w_CMCODICE)+"";
                     ,"_Curs_PROGMAT")
              else
                select Max( PR_PROGR ) As Massimo from (i_cTable);
                 where PRCLAMAT=this.w_CMCODICE;
                  into cursor _Curs_PROGMAT
              endif
              if used('_Curs_PROGMAT')
                select _Curs_PROGMAT
                locate for 1=1
                do while not(eof())
                this.w_OPROGR = Nvl( Massimo , 0 ) + this.w_INCREM
                  select _Curs_PROGMAT
                  continue
                enddo
                use
              endif
              if this.w_OPROGR=this.w_INCREM
                this.w_OPROGR = VAL(_Curs_CMT_DETT.CM_START) 
              endif
            else
              this.w_OPROGR = VAL(_Curs_CMT_DETT.CM_START)
            endif
          else
            this.w_OPROGR = VAL(_Curs_CMT_DETT.CM_START)
          endif
          this.w_LPROGR = _Curs_CMT_DETT.CMLUNGHE
          this.w_OKPROG = .t.
          this.w_VALORE = " "
      endcase
      * --- Sottostringhe Parti Fisse del Codice matricola
      if this.w_OKPROG
        this.w_CODMATR2 = ALLTRIM(this.w_CODMATR2)+ALLTRIM(this.w_VALORE)
      else
        this.w_CODMATR1 = ALLTRIM(this.w_CODMATR1)+ALLTRIM(this.w_VALORE)
      endif
        select _Curs_CMT_DETT
        continue
      enddo
      use
    endif
    if Empty ( this.w_CODMATR1+this.w_CODMATR2 ) and this.w_LPROGR=0
      if this.pPARAM="M"
        ah_ErrorMsg("Nessun codice matricola generato, classe priva del dettaglio","!","")
      endif
      i_retcode = 'stop'
      return
    endif
    do case
      case this.pPARAM="M"
        if this.w_LANCIATODAFORM
          this.oParentObject.w_MSG = ""
        endif
        * --- Generazione Codici Matricola
        * --- Try
        local bErr_032DF318
        bErr_032DF318=bTrsErr
        this.Try_032DF318()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if this.w_ERR
            ah_ErrorMsg(i_ErrMsg,,"")
          else
            ah_ErrorMsg("Impossibile generare codici matricola %1","!","", Message() )
          endif
        endif
        bTrsErr=bTrsErr or bErr_032DF318
        * --- End
      case this.pPARAM="L"
        * --- Generazione Codice Lotto
        this.w_PROGR = CALPRMAT(this.w_KEYSAL,this.pCODICE,this.w_CMCODICE,this.w_OPROGR,this.w_INCREM)
        this.w_PROG = this.w_PROGR
        if TYPE("this.oParentObject.w_PROG") <> "U"
          this.oParentObject.w_PROG = this.w_PROG
        endif
        if len(this.w_CODMATR1 + PADL(ALLTRIM(STR(this.w_PROGR)),this.w_LPROGR,"0")+this.w_CODMATR2) > 20
          ah_ErrorMsg("Il codice lotto ha superato la dimensione di 20 caratteri","!","")
        else
          this.w_LOCODICE = this.w_CODMATR1 + PADL(ALLTRIM(STR(this.w_PROGR)),this.w_LPROGR,"0")+this.w_CODMATR2
          if TYPE("this.oParentObject.w_LOCODICE") <> "U"
            this.oParentObject.w_LOCODICE = this.w_LOCODICE
          endif
          if NOT this.w_LANCIATODAFORM
            * --- Insert into LOTTIART
            i_nConn=i_TableProp[this.LOTTIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOTTIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LOCODICE"+",LOCODART"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_LOCODICE),'LOTTIART','LOCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'LOTTIART','LOCODART');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LOCODICE',this.w_LOCODICE,'LOCODART',this.w_CODART)
              insert into (i_cTable) (LOCODICE,LOCODART &i_ccchkf. );
                 values (;
                   this.w_LOCODICE;
                   ,this.w_CODART;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
    endcase
  endproc
  proc Try_032DF318()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_TEST = .T.
    * --- begin transaction
    cp_BeginTrs()
    this.w_IND = 1
    this.w_ERR = .f.
    this.w_NOGEN = 0
    this.w_noloop = this.pQTAUM1*10
    this.w_OLDMATR = "XXXXXXXXXXXXXXXXXX"
    do while (this.w_IND<=this.pQTAUM1 AND this.w_NOLOOP>0)
      this.w_PROGR = CALPRMAT(this.w_KEYSAL,this.pCODICE,this.w_CMCODICE,this.w_OPROGR,this.w_INCREM)
      if g_UNIMAT="E"
        * --- Aggiorno anche il valore old del progressivo
        this.w_OPROGR = this.w_PROGR + this.w_INCREM
      endif
      this.w_CODMATR = this.w_CODMATR1+PADL(ALLTRIM(STR(this.w_PROGR)),this.w_LPROGR,"0")+this.w_CODMATR2
      if this.w_TEST
        if TYPE("this.oParentObject.w_INIMATR")<>"U"
          this.oParentObject.w_INIMATR = this.w_CODMATR
        endif
        this.w_TEST = .F.
      endif
      if g_UNIMAT <> "A"
        * --- Verifica Univocit� Codice Matricola (g_UNIMAT='M' : per Matricola, g_UNIMAT='C': per Classe, g_UNIMAT='E' : per classe e articolo)
        this.w_OK = UNIVOMAT(this.w_CODMATR, this.w_UNIMAT , this.w_KEYSAL, this.w_CMCODICE)
      endif
      * --- w_OK =.t. indica la presenza di un' altra matricola (o in assoluto o per classe)
      *     e quindi tale codice NON DEVE ESSERE GENERATO
      if Not this.w_OK
        if this.w_LANCIATODAFORM
          AddMsgNL("Generata: %1",this, ALLTRIM(this.w_CODMATR) )
        endif
        this.w_IND = this.w_IND+1
        if len(alltrim(str(this.w_PROGR))) > this.w_LPROGR
          this.w_ERR = .t.
          * --- Raise
          i_Error="La dimensione del progressivo ha superato la lunghezza a lui assegnata%0Impossibile generare codici matricola"
          return
        endif
        * --- Try
        local bErr_032DE658
        bErr_032DE658=bTrsErr
        this.Try_032DE658()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_Msg(MESSAGE(),.T.)
          if this.w_LANCIATODAFORM
            AddMsgNL("Errore generazione: %1 gi� esistente",this, ALLTRIM(this.w_CODMATR) )
          endif
          this.w_NUMERR = this.w_NUMERR + 1
          this.w_IND = this.w_IND- 1
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_032DE658
        * --- End
      else
        if this.w_OLDMATR<>this.w_CODMATR
          if this.w_LANCIATODAFORM
            AddMsgNL("Non generata: %1 non univoca" ,this, ALLTRIM(this.w_CODMATR) )
          endif
        endif
        this.w_NOGEN = this.w_NOGEN+1
      endif
      this.w_OLDMATR = this.w_CODMATR
      * --- Test uscita ciclo While
      this.w_noloop = this.w_noloop-1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    if this.w_LANCIATODAFORM
      if this.w_NUMERR>0
        AddMsgNL("Errori durante la generazione: %1",this, ALLTRIM(STR(this.w_NUMERR)))
      endif
      ah_ErrorMsg("Operazione terminata%0Numero matricole generate: %1","!","", alltrim(str(this.w_IND-1)) )
      if this.w_NOLOOP=0 and this.pQTAUM1>0
        AddMsgNL("Attenzione non tutte le matricole previste sono state generate, verificare struttura classe ",this)
      endif
      * --- Riepio lo Zoom con i Codici Matricola appena generati
      This.oParentObject.NotifyEvent("Esegui")
    endif
    * --- Aggiorno l'Ultimo Progressivo
    this.w_ULTPROG = this.w_PROGR
    if TYPE("this.oParentObject.w_ULTPROG") <> "U"
      this.oParentObject.w_ULTPROG = this.w_ULTPROG
    endif
    return
  proc Try_032DE658()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MATRICOL
    i_nConn=i_TableProp[this.MATRICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MATRICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AMKEYSAL"+",AMCODICE"+",AMCODART"+",AMCLAMAT"+",AMDATCRE"+",AM_PROGR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'MATRICOL','AMKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMATR),'MATRICOL','AMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'MATRICOL','AMCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CMCODICE),'MATRICOL','AMCLAMAT');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MATRICOL','AMDATCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROGR),'MATRICOL','AM_PROGR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AMKEYSAL',this.w_KEYSAL,'AMCODICE',this.w_CODMATR,'AMCODART',this.w_CODART,'AMCLAMAT',this.w_CMCODICE,'AMDATCRE',i_DATSYS,'AM_PROGR',this.w_PROGR)
      insert into (i_cTable) (AMKEYSAL,AMCODICE,AMCODART,AMCLAMAT,AMDATCRE,AM_PROGR &i_ccchkf. );
         values (;
           this.w_KEYSAL;
           ,this.w_CODMATR;
           ,this.w_CODART;
           ,this.w_CMCODICE;
           ,i_DATSYS;
           ,this.w_PROGR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPARAM,pCODICE,pQTAUM1)
    this.pPARAM=pPARAM
    this.pCODICE=pCODICE
    this.pQTAUM1=pQTAUM1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CMT_MAST'
    this.cWorkTables[2]='CMT_DETT'
    this.cWorkTables[3]='MATRICOL'
    this.cWorkTables[4]='PROGMAT'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='LOTTIART'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PROGMAT')
      use in _Curs_PROGMAT
    endif
    if used('_Curs_CMT_DETT')
      use in _Curs_CMT_DETT
    endif
    if used('_Curs_PROGMAT')
      use in _Curs_PROGMAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM,pCODICE,pQTAUM1"
endproc
