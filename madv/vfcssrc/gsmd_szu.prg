* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_szu                                                        *
*              Visualizza schede ubicazioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_78]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-26                                                      *
* Last revis.: 2014-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_szu",oParentObject))

* --- Class definition
define class tgsmd_szu as StdForm
  Top    = 1
  Left   = 1

  * --- Standard Properties
  Width  = 755
  Height = 486
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-31"
  HelpContextID=241847145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  cPrg = "gsmd_szu"
  cComment = "Visualizza schede ubicazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_CODUBI = space(20)
  w_FLARSTO = space(1)
  w_CODART = space(20)
  o_CODART = space(20)
  w_UNIMIS = space(3)
  w_LOTTO = space(20)
  o_LOTTO = space(20)
  w_STATO = space(12)
  w_DATCRE = ctod('  /  /  ')
  w_DESART = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_FLOTT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CODICE = space(20)
  w_TOTCAR = 0
  w_TOTSCA = 0
  w_ZDESCRI = space(40)
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_FLUBIC = space(1)
  w_DESMAG = space(40)
  w_DESUBI = space(40)
  w_DATSCA = ctod('  /  /  ')
  w_NUMCOM = space(20)
  w_RIFFOR = space(20)
  w_ZoomSub = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_szuPag1","gsmd_szu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODMAG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSub = this.oPgFrm.Pages(1).oPag.ZoomSub
    DoDefault()
    proc Destroy()
      this.w_ZoomSub = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='UBICAZIO'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODMAG=space(5)
      .w_CODUBI=space(20)
      .w_FLARSTO=space(1)
      .w_CODART=space(20)
      .w_UNIMIS=space(3)
      .w_LOTTO=space(20)
      .w_STATO=space(12)
      .w_DATCRE=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLOTT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICE=space(20)
      .w_TOTCAR=0
      .w_TOTSCA=0
      .w_ZDESCRI=space(40)
      .w_SERIALE=space(10)
      .w_NUMRIF=0
      .w_FLUBIC=space(1)
      .w_DESMAG=space(40)
      .w_DESUBI=space(40)
      .w_DATSCA=ctod("  /  /  ")
      .w_NUMCOM=space(20)
      .w_RIFFOR=space(20)
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODMAG))
          .link_1_1('Full')
        endif
        .w_CODUBI = Space(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODUBI))
          .link_1_2('Full')
        endif
        .w_FLARSTO = 'N'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODART))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_LOTTO = Space(20)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_LOTTO))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,11,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.ZoomSub.Calculate()
          .DoRTCalc(13,15,.f.)
        .w_ZDESCRI = Nvl(.w_ZoomSub.getVar('ANDESCRI'),Space(40))
        .w_SERIALE = NVL(.w_ZoomSUB.getVar('SERIALE'), SPACE(10))
        .w_NUMRIF = NVL(.w_ZoomSUB.getVar('NUMRIF'), 0)
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
    this.DoRTCalc(19,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODMAG<>.w_CODMAG
            .w_CODUBI = Space(5)
          .link_1_2('Full')
        endif
        .DoRTCalc(3,5,.t.)
        if .o_CODART<>.w_CODART
            .w_LOTTO = Space(20)
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomSub.Calculate()
        .DoRTCalc(7,15,.t.)
            .w_ZDESCRI = Nvl(.w_ZoomSub.getVar('ANDESCRI'),Space(40))
            .w_SERIALE = NVL(.w_ZoomSUB.getVar('SERIALE'), SPACE(10))
            .w_NUMRIF = NVL(.w_ZoomSUB.getVar('NUMRIF'), 0)
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSub.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUBI_1_2.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_2.mCond()
    this.oPgFrm.Page1.oPag.oLOTTO_1_6.enabled = this.oPgFrm.Page1.oPag.oLOTTO_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMCOM_1_35.visible=!this.oPgFrm.Page1.oPag.oNUMCOM_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSub.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGFLUBIC,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_1'),i_cWhere,'GSAR_AMA',"Magazzini",'GSMD_MUB.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGFLUBIC,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGFLUBIC,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGFLUBIC,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_FLUBIC = space(1)
      this.w_DESMAG = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLUBIC='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente, oppure non gestito ad ubicazioni")
        endif
        this.w_CODMAG = space(5)
        this.w_FLUBIC = space(1)
        this.w_DESMAG = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_CODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_2'),i_cWhere,'GSMD_MUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_CODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(20))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(20)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_4'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARUNMIS1,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNIMIS = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_UNIMIS = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLOTT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LOTTO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LOTTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_LOTTO)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_LOTTO))
          select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LOTTO)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LOTTO) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oLOTTO_1_6'),i_cWhere,'GSMD_ALO',"Anagrafica lotti",'GSMD_SPL.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LOTTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_LOTTO);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_LOTTO)
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LONUMCOM,LOLOTFOR,LOFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_NUMCOM = NVL(_Link_.LONUMCOM,space(20))
      this.w_RIFFOR = NVL(_Link_.LOLOTFOR,space(20))
      this.w_STATO = NVL(_Link_.LOFLSTAT,space(12))
      this.w_CODICE = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LOTTO = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
      this.w_NUMCOM = space(20)
      this.w_RIFFOR = space(20)
      this.w_STATO = space(12)
      this.w_CODICE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODART=.w_CODICE) OR EMPTY(.w_CODART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endif
        this.w_LOTTO = space(20)
        this.w_DATCRE = ctod("  /  /  ")
        this.w_DATSCA = ctod("  /  /  ")
        this.w_NUMCOM = space(20)
        this.w_RIFFOR = space(20)
        this.w_STATO = space(12)
        this.w_CODICE = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LOTTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_1.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_1.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_2.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_2.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARSTO_1_3.RadioValue()==this.w_FLARSTO)
      this.oPgFrm.Page1.oPag.oFLARSTO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_4.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_4.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_5.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_5.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oLOTTO_1_6.value==this.w_LOTTO)
      this.oPgFrm.Page1.oPag.oLOTTO_1_6.value=this.w_LOTTO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_7.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCRE_1_8.value==this.w_DATCRE)
      this.oPgFrm.Page1.oPag.oDATCRE_1_8.value=this.w_DATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_9.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_9.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCAR_1_18.value==this.w_TOTCAR)
      this.oPgFrm.Page1.oPag.oTOTCAR_1_18.value=this.w_TOTCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSCA_1_19.value==this.w_TOTSCA)
      this.oPgFrm.Page1.oPag.oTOTSCA_1_19.value=this.w_TOTSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_27.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_27.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_28.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_28.value=this.w_DESUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_33.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_33.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOM_1_35.value==this.w_NUMCOM)
      this.oPgFrm.Page1.oPag.oNUMCOM_1_35.value=this.w_NUMCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFFOR_1_38.value==this.w_RIFFOR)
      this.oPgFrm.Page1.oPag.oRIFFOR_1_38.value=this.w_RIFFOR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODMAG)) or not(.w_FLUBIC='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente, oppure non gestito ad ubicazioni")
          case   (empty(.w_CODUBI))  and (Not Empty(.w_CODMAG) and .w_Flubic='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUBI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODUBI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
          case   not((.w_CODART=.w_CODICE) OR EMPTY(.w_CODART))  and (.w_FLOTT $ "SC" AND NOT EMPTY(.w_CODART))  and not(empty(.w_LOTTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLOTTO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice lotto selezionato non congruente con il codice articolo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODMAG = this.w_CODMAG
    this.o_CODART = this.w_CODART
    this.o_LOTTO = this.w_LOTTO
    return

enddefine

* --- Define pages as container
define class tgsmd_szuPag1 as StdContainer
  Width  = 751
  height = 486
  stdWidth  = 751
  stdheight = 486
  resizeXpos=269
  resizeYpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMAG_1_1 as StdField with uid="GQSKDIYJJB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente, oppure non gestito ad ubicazioni",;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 217532454,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=89, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSMD_MUB.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oCODMAG_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oCODUBI_1_2 as StdField with uid="MHUQLNNIPJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione (vuota tutte)",;
    HelpContextID = 252659750,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=89, Top=38, cSayPict="p_UBI", cGetPict="p_UBI", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_CODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODMAG) and .w_Flubic='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_CODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_CODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'GSMD_SMU.UBICAZIO_VZM',this.parent.oContained
  endproc
  proc oCODUBI_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_CODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oFLARSTO_1_3 as StdCheck with uid="GVKODKDZJD",rtseq=3,rtrep=.f.,left=578, top=67, caption="Movimenti storicizzati",;
    ToolTipText = "Se attivo saranno visualizzati anche i movim. di magazzino e i documenti storicizzati",;
    HelpContextID = 186389846,;
    cFormVar="w_FLARSTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLARSTO_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLARSTO_1_3.GetRadio()
    this.Parent.oContained.w_FLARSTO = this.RadioValue()
    return .t.
  endfunc

  func oFLARSTO_1_3.SetRadio()
    this.Parent.oContained.w_FLARSTO=trim(this.Parent.oContained.w_FLARSTO)
    this.value = ;
      iif(this.Parent.oContained.w_FLARSTO=='S',1,;
      0)
  endfunc

  add object oCODART_1_4 as StdField with uid="HEMJIDMPXP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 184240166,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=89, Top=67, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_LOTTO)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oUNIMIS_1_5 as StdField with uid="TGWYENDKVJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale di riferimento",;
    HelpContextID = 158832710,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=703, Top=96, InputMask=replicate('X',3)

  add object oLOTTO_1_6 as StdField with uid="OTTOSNRIVX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LOTTO", cQueryName = "LOTTO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto selezionato non congruente con il codice articolo",;
    ToolTipText = "Codice lotto selezionato",;
    HelpContextID = 153139018,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=89, Top=96, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_LOTTO"

  func oLOTTO_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLOTT $ "SC" AND NOT EMPTY(.w_CODART))
    endwith
   endif
  endfunc

  func oLOTTO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oLOTTO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLOTTO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_CODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_CODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oLOTTO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Anagrafica lotti",'GSMD_SPL.LOTTIART_VZM',this.parent.oContained
  endproc
  proc oLOTTO_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_LOTTO
     i_obj.ecpSave()
  endproc


  add object oSTATO_1_7 as StdCombo with uid="WPBJLDHFPV",rtseq=7,rtrep=.f.,left=89,top=125,width=104,height=21, enabled=.f.;
    , ToolTipText = "Status del lotto";
    , HelpContextID = 153215450;
    , cFormVar="w_STATO",RowSource=""+"Disponibile,"+"Sospeso,"+"Esaurito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_7.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(12)))))
  endfunc
  func oSTATO_1_7.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_7.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='D',1,;
      iif(this.Parent.oContained.w_STATO=='S',2,;
      iif(this.Parent.oContained.w_STATO=='E',3,;
      0)))
  endfunc

  add object oDATCRE_1_8 as StdField with uid="IHLAXKOSUE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATCRE", cQueryName = "DATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione",;
    HelpContextID = 201210422,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=325, Top=96

  add object oDESART_1_9 as StdField with uid="XMSTCJOJVH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184299062,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=246, Top=67, InputMask=replicate('X',40)


  add object ZoomSub as cp_zoombox with uid="LEQYSQQATC",left=3, top=154, width=741,height=270,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="UBICAZIO",cZoomFile="GSMD_SZU",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSZM_BZM",bRetriveAllRows=.f.,bAdvOptions=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 204585958

  add object oTOTCAR_1_18 as StdField with uid="EEPQFQVVOX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_TOTCAR", cQueryName = "TOTCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale carichi dei movimenti selezionati",;
    HelpContextID = 133056822,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=394, Top=428, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"

  add object oTOTSCA_1_19 as StdField with uid="ZCWEXKZJKJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TOTSCA", cQueryName = "TOTSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale scarichi dei movimenti selezionati",;
    HelpContextID = 119425334,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=499, Top=428, cSayPict="v_PQ(16)", cGetPict="v_GQ(16)"


  add object oBtn_1_20 as StdButton with uid="VIIDLKOSDX",left=690, top=10, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 203510550;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSMD_BSL(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMAG_1_27 as StdField with uid="CULUXCRDOD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 217591350,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=148, Top=10, InputMask=replicate('X',40)

  add object oDESUBI_1_28 as StdField with uid="CJNADXACQR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 252718646,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=274, Top=38, InputMask=replicate('X',40)


  add object oBtn_1_29 as StdButton with uid="FRSHLUKCSC",left=8, top=435, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/mov.magazzino associato alla riga selezionata";
    , HelpContextID = 14590305;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_SERIALE, .w_NUMRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="YEDHIQOSNF",left=690, top=435, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 234529722;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATSCA_1_33 as StdField with uid="EBKPUWTTMT",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza",;
    HelpContextID = 119421494,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=325, Top=125

  add object oNUMCOM_1_35 as StdField with uid="ZHZDPSXGZT",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NUMCOM", cQueryName = "NUMCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 63823574,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=502, Top=96, InputMask=replicate('X',20)

  func oNUMCOM_1_35.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oRIFFOR_1_38 as StdField with uid="VHRBNZJYZZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_RIFFOR", cQueryName = "RIFFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 147874582,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=502, Top=125, InputMask=replicate('X',20)


  add object oObj_1_39 as cp_runprogram with uid="OVKXFMCALY",left=13, top=497, width=335,height=22,;
    caption='GSAR_BZM(w_SERIALE w_NUMRIF)',;
   bGlobalFont=.t.,;
    prg="GSAR_BZM(w_SERIALE, w_NUMRIF)",;
    cEvent = "w_zoomsub selected",;
    nPag=1;
    , HelpContextID = 85753505

  add object oStr_1_12 as StdString with uid="IQOMEUJROW",Visible=.t., Left=6, Top=67,;
    Alignment=1, Width=78, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="PVZWAFKICF",Visible=.t., Left=6, Top=96,;
    Alignment=1, Width=78, Height=18,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="NUEBZHLIMC",Visible=.t., Left=659, Top=96,;
    Alignment=1, Width=40, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="OQIZPBKNEC",Visible=.t., Left=6, Top=10,;
    Alignment=1, Width=78, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KEDEFKDRHD",Visible=.t., Left=6, Top=38,;
    Alignment=1, Width=78, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HTAHWUWJOY",Visible=.t., Left=6, Top=125,;
    Alignment=1, Width=78, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="YTQGKBDMUH",Visible=.t., Left=259, Top=96,;
    Alignment=1, Width=64, Height=15,;
    Caption="Creato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="SORJKYMDOG",Visible=.t., Left=250, Top=125,;
    Alignment=1, Width=73, Height=15,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="XJLWDGZGWI",Visible=.t., Left=398, Top=96,;
    Alignment=1, Width=101, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="VYKYDVHSUC",Visible=.t., Left=398, Top=125,;
    Alignment=1, Width=101, Height=15,;
    Caption="Rif.lotto fornitore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_szu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
