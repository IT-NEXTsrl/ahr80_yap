* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_aul                                                        *
*              Anagrafica unit� logistiche                                     *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_108]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-22                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_aul"))

* --- Class definition
define class tgsmd_aul as StdForm
  Top    = 12
  Left   = 11

  * --- Standard Properties
  Width  = 623
  Height = 220+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=192263319
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  UNIT_LOG_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  LOTTIART_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  cFile = "UNIT_LOG"
  cKeySelect = "UL__SSCC"
  cKeyWhere  = "UL__SSCC=this.w_UL__SSCC"
  cKeyWhereODBC = '"UL__SSCC="+cp_ToStrODBC(this.w_UL__SSCC)';

  cKeyWhereODBCqualified = '"UNIT_LOG.UL__SSCC="+cp_ToStrODBC(this.w_UL__SSCC)';

  cPrg = "gsmd_aul"
  cComment = "Anagrafica unit� logistiche"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_PREEAN = space(2)
  w_CODPRO = space(7)
  w_CODEXT1 = space(1)
  o_CODEXT1 = space(1)
  w_PREEAN1 = space(2)
  o_PREEAN1 = space(2)
  w_CODPRO1 = space(7)
  o_CODPRO1 = space(7)
  w_CODBAR = space(7)
  o_CODBAR = space(7)
  w_CHKSUM = space(1)
  w_ULCODICE = space(20)
  w_ULCODART = space(20)
  o_ULCODART = space(20)
  w_DESCART = space(40)
  w_ULCODLOT = space(20)
  w_DATASCA = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_FORNRIF = space(15)
  w_DESCFOR = space(40)
  w_QTAMOV = 0
  w_UL__SSCC = space(18)
  w_CODEXT = space(1)
  w_ARFLLOTT = space(1)
  w_NUMCOL = 0
  w_UNIMIS = space(3)
  w_TIPBAR = space(3)
  w_ULFLGINT = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CODBAR = this.W_CODBAR
  * --- Area Manuale = Declare Variables
  * --- gsmd_aul
  *--- Se oParentObject<>null l'anagrafica � stata lanciata dal batch GSMD_BAT
  oParentObject = .null.
  w_OLDSSCC = space(18)
  *--- Ridefinisco ecpSave
  proc ecpSave()
     *--- Salvo il vecchio valore di UL__SSCC per poterlo cos� passare al batch
     *--- che esegue l'assegnamento del pallet
     this.w_OLDSSCC = this.w_UL__SSCC
     DoDefault()
     *--- Nel caso in cui l'anagrafica � stata lanciata da batch, chiudo
     *--- la maschera al salvataggio
     if type('this.oParentObject')='O'
       *--- Se la maschera � stata lanciata da GSMD_BAT eseguo anche l'assegnamento
       if Upper(this.oParentObject.Class)='TGSMD_BAT'
         this.ecpQuit()
         this.NotifyEvent("Done")
         this.Hide()
         if ah_YesNo("Assegno l'unit� logistica corrente%0alle righe selezionate?")
           this.NotifyEvent("Assegna")
         endif
         this.Release()
       else
         *--- Nel caso sia lanciata da GSMD_BDV valorizzo anche GSVE_KDA/GSAC_KDA
         this.ecpQuit()
         this.NotifyEvent("Done")
         this.Hide()
           *--- Valorizzo w_MVUNILOG e eseguo la valid per eseguire i check
           this.oParentObject.oParentObject.w_MVUNILOG=this.w_OLDSSCC
           obj=this.oParentObject.oParentObject.GetCtrl('w_MVUNILOG')
           obj.bUpd=.t.
           this.oParentObject.oParentObject.SetControlsValue()
           obj.Valid()
           obj=.null.
         this.Release()
       endif
     endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'UNIT_LOG','gsmd_aul')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_aulPag1","gsmd_aul",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Unit� logistica")
      .Pages(1).HelpContextID = 201566171
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='UNIT_LOG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UNIT_LOG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UNIT_LOG_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_UL__SSCC = NVL(UL__SSCC,space(18))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from UNIT_LOG where UL__SSCC=KeySet.UL__SSCC
    *
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UNIT_LOG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UNIT_LOG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UNIT_LOG '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UL__SSCC',this.w_UL__SSCC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_PREEAN = space(2)
        .w_CODPRO = space(7)
        .w_CODEXT1 = RIGHT(ALLTRIM('0'+.w_CODEXT),1)
        .w_PREEAN1 = RIGHT(ALLTRIM('00'+.w_PREEAN),2)
        .w_CODPRO1 = RIGHT(ALLTRIM('0000000'+.w_CODPRO),7)
        .w_CODBAR = '0000000'
        .w_CHKSUM = '0'
        .w_DESCART = space(40)
        .w_DATASCA = ctod("  /  /  ")
        .w_TIPCON = space(1)
        .w_FORNRIF = space(15)
        .w_DESCFOR = space(40)
        .w_QTAMOV = 0
        .w_CODEXT = space(1)
        .w_ARFLLOTT = space(1)
        .w_NUMCOL = 0
        .w_UNIMIS = space(3)
        .w_TIPBAR = space(3)
          .link_1_1('Load')
        .w_ULCODICE = NVL(ULCODICE,space(20))
          if link_1_9_joined
            this.w_ULCODICE = NVL(CACODICE109,NVL(this.w_ULCODICE,space(20)))
            this.w_ULCODART = NVL(CACODART109,space(20))
            this.w_TIPBAR = NVL(CATIPBAR109,space(3))
          else
          .link_1_9('Load')
          endif
        .w_ULCODART = NVL(ULCODART,space(20))
          if link_1_10_joined
            this.w_ULCODART = NVL(ARCODART110,NVL(this.w_ULCODART,space(20)))
            this.w_DESCART = NVL(ARDESART110,space(40))
            this.w_UNIMIS = NVL(ARUNMIS1110,space(3))
            this.w_ARFLLOTT = NVL(ARFLLOTT110,space(1))
          else
          .link_1_10('Load')
          endif
        .w_ULCODLOT = NVL(ULCODLOT,space(20))
          .link_1_12('Load')
          .link_1_15('Load')
        .w_UL__SSCC = NVL(UL__SSCC,space(18))
        .w_ULFLGINT = NVL(ULFLGINT,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'UNIT_LOG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_PREEAN = space(2)
      .w_CODPRO = space(7)
      .w_CODEXT1 = space(1)
      .w_PREEAN1 = space(2)
      .w_CODPRO1 = space(7)
      .w_CODBAR = space(7)
      .w_CHKSUM = space(1)
      .w_ULCODICE = space(20)
      .w_ULCODART = space(20)
      .w_DESCART = space(40)
      .w_ULCODLOT = space(20)
      .w_DATASCA = ctod("  /  /  ")
      .w_TIPCON = space(1)
      .w_FORNRIF = space(15)
      .w_DESCFOR = space(40)
      .w_QTAMOV = 0
      .w_UL__SSCC = space(18)
      .w_CODEXT = space(1)
      .w_ARFLLOTT = space(1)
      .w_NUMCOL = 0
      .w_UNIMIS = space(3)
      .w_TIPBAR = space(3)
      .w_ULFLGINT = space(1)
      if .cFunction<>"Filter"
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_CODEXT1 = RIGHT(ALLTRIM('0'+.w_CODEXT),1)
        .w_PREEAN1 = RIGHT(ALLTRIM('00'+.w_PREEAN),2)
        .w_CODPRO1 = RIGHT(ALLTRIM('0000000'+.w_CODPRO),7)
        .w_CODBAR = '0000000'
        .w_CHKSUM = '0'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_ULCODICE))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_ULCODART))
          .link_1_10('Full')
          endif
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_ULCODLOT))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,15,.f.)
          if not(empty(.w_FORNRIF))
          .link_1_15('Full')
          endif
          .DoRTCalc(16,23,.f.)
        .w_ULFLGINT = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'UNIT_LOG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SSCCPRG","i_codazi,w_CODBAR")
      .op_codazi = .w_codazi
      .op_CODBAR = .w_CODBAR
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCODEXT1_1_4.enabled = i_bVal
      .Page1.oPag.oPREEAN1_1_5.enabled = i_bVal
      .Page1.oPag.oCODPRO1_1_6.enabled = i_bVal
      .Page1.oPag.oCODBAR_1_7.enabled = i_bVal
      .Page1.oPag.oULCODICE_1_9.enabled = i_bVal
      .Page1.oPag.oULCODLOT_1_12.enabled = i_bVal
      .Page1.oPag.oULFLGINT_1_38.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'UNIT_LOG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ULCODICE,"ULCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ULCODART,"ULCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ULCODLOT,"ULCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UL__SSCC,"UL__SSCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ULFLGINT,"ULFLGINT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    i_lTable = "UNIT_LOG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.UNIT_LOG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMD_SUL with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsmd_aul
    *--- Permette l'inserzione di un record con il solo codice SSCC
    this.bUpdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.UNIT_LOG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SSCCPRG","i_codazi,w_CODBAR")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      this.Calculate_NRVDUGQGTS()
      *
      * insert into UNIT_LOG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UNIT_LOG')
        i_extval=cp_InsertValODBCExtFlds(this,'UNIT_LOG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ULCODICE,ULCODART,ULCODLOT,UL__SSCC,ULFLGINT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ULCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_ULCODART)+;
                  ","+cp_ToStrODBCNull(this.w_ULCODLOT)+;
                  ","+cp_ToStrODBC(this.w_UL__SSCC)+;
                  ","+cp_ToStrODBC(this.w_ULFLGINT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UNIT_LOG')
        i_extval=cp_InsertValVFPExtFlds(this,'UNIT_LOG')
        cp_CheckDeletedKey(i_cTable,0,'UL__SSCC',this.w_UL__SSCC)
        INSERT INTO (i_cTable);
              (ULCODICE,ULCODART,ULCODLOT,UL__SSCC,ULFLGINT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ULCODICE;
                  ,this.w_ULCODART;
                  ,this.w_ULCODLOT;
                  ,this.w_UL__SSCC;
                  ,this.w_ULFLGINT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.UNIT_LOG_IDX,i_nConn)
      *
      * update UNIT_LOG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'UNIT_LOG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ULCODICE="+cp_ToStrODBCNull(this.w_ULCODICE)+;
             ",ULCODART="+cp_ToStrODBCNull(this.w_ULCODART)+;
             ",ULCODLOT="+cp_ToStrODBCNull(this.w_ULCODLOT)+;
             ",ULFLGINT="+cp_ToStrODBC(this.w_ULFLGINT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'UNIT_LOG')
        i_cWhere = cp_PKFox(i_cTable  ,'UL__SSCC',this.w_UL__SSCC  )
        UPDATE (i_cTable) SET;
              ULCODICE=this.w_ULCODICE;
             ,ULCODART=this.w_ULCODART;
             ,ULCODLOT=this.w_ULCODLOT;
             ,ULFLGINT=this.w_ULFLGINT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.UNIT_LOG_IDX,i_nConn)
      *
      * delete UNIT_LOG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'UL__SSCC',this.w_UL__SSCC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,9,.t.)
          .link_1_10('Full')
        .DoRTCalc(11,11,.t.)
        if .o_ULCODART<>.w_ULCODART
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.t.)
          .link_1_15('Full')
        if .o_CODBAR<>.w_CODBAR.or. .o_CODEXT1<>.w_CODEXT1.or. .o_CODPRO1<>.w_CODPRO1.or. .o_PREEAN1<>.w_PREEAN1
          .Calculate_NRVDUGQGTS()
        endif
        if .o_ULCODART<>.w_ULCODART
          .Calculate_IYSQJOCQZW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SSCCPRG","i_codazi,w_CODBAR")
          .op_CODBAR = .w_CODBAR
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(16,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UKEFWCLGUU()
    with this
          * --- Ricavo SSCC da ul__sscc al load
          .w_CODBAR = SUBSTR(.w_UL__SSCC,11,7)
          .w_CHKSUM = RIGHT(.w_UL__SSCC,1)
          .w_CODEXT1 = LEFT(.w_UL__SSCC,1)
          .w_CODPRO1 = SUBSTR(.w_UL__SSCC,4,7)
          .w_PREEAN1 = SUBSTR(.w_UL__SSCC,2,2)
    endwith
  endproc
  proc Calculate_NRVDUGQGTS()
    with this
          * --- Calcolo il checksum di ul__sscc e aggiorno il campo chksum
          .w_UL__SSCC = CalcBar(.w_CODEXT1+RIGHT(ALLTRIM('00'+.w_PREEAN1),2)+RIGHT(ALLTRIM('0000000'+.w_CODPRO1),7)+.w_CODBAR+'0','S',1)
          .w_CHKSUM = RIGHT(.w_UL__SSCC,1)
    endwith
  endproc
  proc Calculate_FUQSSBWZYN()
    with this
          * --- Lancio gsmd_bat per assegnare in automatico il pallet (gsmd_kat)
          gsmd_bat(this;
              ,'U';
              ,this.oParentObject.oParentObject;
              ,.w_OLDSSCC;
             )
    endwith
  endproc
  proc Calculate_IYSQJOCQZW()
    with this
          * --- Calcolo qtamov, numcol
          gsmd_bbk(this;
             )
    endwith
  endproc
  proc Calculate_ZNLBQDZNTS()
    with this
          * --- Controlli finali
          GSMD_BUL(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODEXT1_1_4.enabled = this.oPgFrm.Page1.oPag.oCODEXT1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oPREEAN1_1_5.enabled = this.oPgFrm.Page1.oPag.oPREEAN1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODPRO1_1_6.enabled = this.oPgFrm.Page1.oPag.oCODPRO1_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODBAR_1_7.enabled = this.oPgFrm.Page1.oPag.oCODBAR_1_7.mCond()
    this.oPgFrm.Page1.oPag.oULCODLOT_1_12.enabled = this.oPgFrm.Page1.oPag.oULCODLOT_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oQTAMOV_1_17.visible=!this.oPgFrm.Page1.oPag.oQTAMOV_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oNUMCOL_1_33.visible=!this.oPgFrm.Page1.oPag.oNUMCOL_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS_1_35.visible=!this.oPgFrm.Page1.oPag.oUNIMIS_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_UKEFWCLGUU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_NRVDUGQGTS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Assegna")
          .Calculate_FUQSSBWZYN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_IYSQJOCQZW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Update end")
          .Calculate_ZNLBQDZNTS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCODEXT,AZPREEAN,AZCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZCODEXT,AZPREEAN,AZCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_CODEXT = NVL(_Link_.AZCODEXT,space(1))
      this.w_PREEAN = NVL(_Link_.AZPREEAN,space(2))
      this.w_CODPRO = NVL(_Link_.AZCODPRO,space(7))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_CODEXT = space(1)
      this.w_PREEAN = space(2)
      this.w_CODPRO = space(7)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ULCODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ULCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ULCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CATIPBAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ULCODICE))
          select CACODICE,CACODART,CATIPBAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ULCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ULCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oULCODICE_1_9'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSMD_AUL.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CATIPBAR";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CATIPBAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ULCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CATIPBAR";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ULCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ULCODICE)
            select CACODICE,CACODART,CATIPBAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ULCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_ULCODART = NVL(_Link_.CACODART,space(20))
      this.w_TIPBAR = NVL(_Link_.CATIPBAR,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ULCODICE = space(20)
      endif
      this.w_ULCODART = space(20)
      this.w_TIPBAR = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=INLIST(.w_TIPBAR, '1', '2', '4', 'C')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di ricerca non valido o di tipo diverso da EAN 13, upc A, EAN 8, EAN 14")
        endif
        this.w_ULCODICE = space(20)
        this.w_ULCODART = space(20)
        this.w_TIPBAR = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ULCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.CACODICE as CACODICE109"+ ",link_1_9.CACODART as CACODART109"+ ",link_1_9.CATIPBAR as CATIPBAR109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on UNIT_LOG.ULCODICE=link_1_9.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and UNIT_LOG.ULCODICE=link_1_9.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ULCODART
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ULCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ULCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ULCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ULCODART)
            select ARCODART,ARDESART,ARUNMIS1,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ULCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESCART = NVL(_Link_.ARDESART,space(40))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_ARFLLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ULCODART = space(20)
      endif
      this.w_DESCART = space(40)
      this.w_UNIMIS = space(3)
      this.w_ARFLLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ULCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.ARCODART as ARCODART110"+ ",link_1_10.ARDESART as ARDESART110"+ ",link_1_10.ARUNMIS1 as ARUNMIS1110"+ ",link_1_10.ARFLLOTT as ARFLLOTT110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on UNIT_LOG.ULCODART=link_1_10.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and UNIT_LOG.ULCODART=link_1_10.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ULCODLOT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ULCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_ULCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_ULCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_ULCODART;
                     ,'LOCODICE',trim(this.w_ULCODLOT))
          select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ULCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ULCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oULCODLOT_1_12'),i_cWhere,'GSMD_ALO',"Codici lotto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ULCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Lotto senza data scadenza")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_ULCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ULCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_ULCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_ULCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_ULCODART;
                       ,'LOCODICE',this.w_ULCODLOT)
            select LOCODART,LOCODICE,LODATSCA,LOCODCON,LOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ULCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_DATASCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_FORNRIF = NVL(_Link_.LOCODCON,space(15))
      this.w_TIPCON = NVL(_Link_.LOTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ULCODLOT = space(20)
      endif
      this.w_DATASCA = ctod("  /  /  ")
      this.w_FORNRIF = space(15)
      this.w_TIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_DATASCA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto senza data scadenza")
        endif
        this.w_ULCODLOT = space(20)
        this.w_DATASCA = ctod("  /  /  ")
        this.w_FORNRIF = space(15)
        this.w_TIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ULCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORNRIF
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORNRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORNRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORNRIF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORNRIF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORNRIF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FORNRIF = space(15)
      endif
      this.w_DESCFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORNRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODEXT1_1_4.value==this.w_CODEXT1)
      this.oPgFrm.Page1.oPag.oCODEXT1_1_4.value=this.w_CODEXT1
    endif
    if not(this.oPgFrm.Page1.oPag.oPREEAN1_1_5.value==this.w_PREEAN1)
      this.oPgFrm.Page1.oPag.oPREEAN1_1_5.value=this.w_PREEAN1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRO1_1_6.value==this.w_CODPRO1)
      this.oPgFrm.Page1.oPag.oCODPRO1_1_6.value=this.w_CODPRO1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBAR_1_7.value==this.w_CODBAR)
      this.oPgFrm.Page1.oPag.oCODBAR_1_7.value=this.w_CODBAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKSUM_1_8.value==this.w_CHKSUM)
      this.oPgFrm.Page1.oPag.oCHKSUM_1_8.value=this.w_CHKSUM
    endif
    if not(this.oPgFrm.Page1.oPag.oULCODICE_1_9.value==this.w_ULCODICE)
      this.oPgFrm.Page1.oPag.oULCODICE_1_9.value=this.w_ULCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oULCODART_1_10.value==this.w_ULCODART)
      this.oPgFrm.Page1.oPag.oULCODART_1_10.value=this.w_ULCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCART_1_11.value==this.w_DESCART)
      this.oPgFrm.Page1.oPag.oDESCART_1_11.value=this.w_DESCART
    endif
    if not(this.oPgFrm.Page1.oPag.oULCODLOT_1_12.value==this.w_ULCODLOT)
      this.oPgFrm.Page1.oPag.oULCODLOT_1_12.value=this.w_ULCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATASCA_1_13.value==this.w_DATASCA)
      this.oPgFrm.Page1.oPag.oDATASCA_1_13.value=this.w_DATASCA
    endif
    if not(this.oPgFrm.Page1.oPag.oFORNRIF_1_15.value==this.w_FORNRIF)
      this.oPgFrm.Page1.oPag.oFORNRIF_1_15.value=this.w_FORNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFOR_1_16.value==this.w_DESCFOR)
      this.oPgFrm.Page1.oPag.oDESCFOR_1_16.value=this.w_DESCFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAMOV_1_17.value==this.w_QTAMOV)
      this.oPgFrm.Page1.oPag.oQTAMOV_1_17.value=this.w_QTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOL_1_33.value==this.w_NUMCOL)
      this.oPgFrm.Page1.oPag.oNUMCOL_1_33.value=this.w_NUMCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_35.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_35.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oULFLGINT_1_38.RadioValue()==this.w_ULFLGINT)
      this.oPgFrm.Page1.oPag.oULFLGINT_1_38.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'UNIT_LOG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODEXT1))  and (.cFunction='Load' AND .w_ULFLGINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODEXT1_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODEXT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PREEAN1))  and (.cFunction='Load'  AND .w_ULFLGINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPREEAN1_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PREEAN1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODPRO1))  and (.cFunction='Load'  AND .w_ULFLGINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRO1_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODPRO1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODBAR))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODBAR_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CODBAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(INLIST(.w_TIPBAR, '1', '2', '4', 'C'))  and not(empty(.w_ULCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oULCODICE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di ricerca non valido o di tipo diverso da EAN 13, upc A, EAN 8, EAN 14")
          case   not(NOT EMPTY(.w_DATASCA))  and (NOT EMPTY(.w_ULCODART) AND .w_ARFLLOTT<>'N')  and not(empty(.w_ULCODLOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oULCODLOT_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lotto senza data scadenza")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODEXT1 = this.w_CODEXT1
    this.o_PREEAN1 = this.w_PREEAN1
    this.o_CODPRO1 = this.w_CODPRO1
    this.o_CODBAR = this.w_CODBAR
    this.o_ULCODART = this.w_ULCODART
    return

enddefine

* --- Define pages as container
define class tgsmd_aulPag1 as StdContainer
  Width  = 619
  height = 220
  stdWidth  = 619
  stdheight = 220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODEXT1_1_4 as StdField with uid="MKSVQUIYYR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODEXT1", cQueryName = "CODEXT1",nZero=1,;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Cifra di estensione",;
    HelpContextID = 180402138,;
   bGlobalFont=.t.,;
    Height=21, Width=15, Left=160, Top=14, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  func oCODEXT1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND .w_ULFLGINT='S')
    endwith
   endif
  endfunc

  add object oPREEAN1_1_5 as StdField with uid="AVKUJDMEDC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PREEAN1", cQueryName = "PREEAN1",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso EAN della nazione",;
    HelpContextID = 36742154,;
   bGlobalFont=.t.,;
    Height=21, Width=22, Left=178, Top=14, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPREEAN1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load'  AND .w_ULFLGINT='S')
    endwith
   endif
  endfunc

  add object oCODPRO1_1_6 as StdField with uid="WVPWMGGFMP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODPRO1", cQueryName = "CODPRO1",nZero=7,;
    bObbl = .t. , nPag = 1, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Cod. produttore per barcode assegnato dall'INDICOD al proprietario del marchio",;
    HelpContextID = 1423322,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=203, Top=14, cSayPict="'9999999'", cGetPict="'9999999'", InputMask=replicate('X',7)

  func oCODPRO1_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load'  AND .w_ULFLGINT='S')
    endwith
   endif
  endfunc

  add object oCODBAR_1_7 as StdField with uid="HGTSUTUEBB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODBAR", cQueryName = "CODBAR",nZero=7,;
    bObbl = .t. , nPag = 1, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo dell'unit� logistica",;
    HelpContextID = 238270426,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=264, Top=14, cSayPict="'9999999'", cGetPict="'9999999'", InputMask=replicate('X',7)

  func oCODBAR_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oCHKSUM_1_8 as StdField with uid="REZTKHOKZZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CHKSUM", cQueryName = "CHKSUM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Cifra di controllo",;
    HelpContextID = 31608538,;
   bGlobalFont=.t.,;
    Height=21, Width=15, Left=325, Top=14, cSayPict="'9'", cGetPict="'9'", InputMask=replicate('X',1)

  add object oULCODICE_1_9 as StdField with uid="IDNTZOUTRK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ULCODICE", cQueryName = "ULCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di ricerca non valido o di tipo diverso da EAN 13, upc A, EAN 8, EAN 14",;
    ToolTipText = "Se specificato l'unit� logistica � mono-prodotto",;
    HelpContextID = 116836725,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=45, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_ULCODICE"

  func oULCODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oULCODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oULCODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oULCODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSMD_AUL.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oULCODICE_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ULCODICE
     i_obj.ecpSave()
  endproc

  add object oULCODART_1_10 as StdField with uid="OIHTTVCQLL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ULCODART", cQueryName = "ULCODART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 251054438,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=74, InputMask=replicate('X',20), cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ULCODART"

  func oULCODART_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ULCODLOT)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESCART_1_11 as StdField with uid="TUSAWEPFUZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCART", cQueryName = "DESCART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 238145994,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=74, InputMask=replicate('X',40)

  add object oULCODLOT_1_12 as StdField with uid="MSPCADUXIN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ULCODLOT", cQueryName = "ULCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lotto senza data scadenza",;
    ToolTipText = "Se specificato l'unit� logistica � mono-lotto",;
    HelpContextID = 201930394,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=104, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_ULCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_ULCODLOT"

  func oULCODLOT_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ULCODART) AND .w_ARFLLOTT<>'N')
    endwith
   endif
  endfunc

  func oULCODLOT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oULCODLOT_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oULCODLOT_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_ULCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_ULCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oULCODLOT_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Codici lotto",'',this.parent.oContained
  endproc
  proc oULCODLOT_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_ULCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_ULCODLOT
     i_obj.ecpSave()
  endproc

  add object oDATASCA_1_13 as StdField with uid="NBZWFUMVHW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATASCA", cQueryName = "DATASCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza lotto",;
    HelpContextID = 202622410,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=160, Top=132

  add object oFORNRIF_1_15 as StdField with uid="WLSFJSYOZS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FORNRIF", cQueryName = "FORNRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore di riferimento",;
    HelpContextID = 102160298,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=160, Top=160, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORNRIF"

  func oFORNRIF_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCFOR_1_16 as StdField with uid="RKJVJESGWU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCFOR", cQueryName = "DESCFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 14799306,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=320, Top=160, InputMask=replicate('X',40)

  add object oQTAMOV_1_17 as StdField with uid="MKGTQUFDWV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_QTAMOV", cQueryName = "QTAMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� articolo movimentata",;
    HelpContextID = 155771386,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=320, Top=192

  func oQTAMOV_1_17.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc

  add object oNUMCOL_1_33 as StdField with uid="NQIZONCKNJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NUMCOL", cQueryName = "NUMCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero confezioni",;
    HelpContextID = 55714090,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=160, Top=192

  func oNUMCOL_1_33.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc

  add object oUNIMIS_1_35 as StdField with uid="PUSGKDOABQ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 212363194,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=455, Top=192, InputMask=replicate('X',3)

  func oUNIMIS_1_35.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc

  add object oULFLGINT_1_38 as StdCheck with uid="BPJDWSSYUJ",rtseq=24,rtrep=.f.,left=387, top=17, caption="Provenienza esterna",;
    ToolTipText = "Se attivo unit� logistica esterna",;
    HelpContextID = 154560154,;
    cFormVar="w_ULFLGINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oULFLGINT_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oULFLGINT_1_38.GetRadio()
    this.Parent.oContained.w_ULFLGINT = this.RadioValue()
    return .t.
  endfunc

  func oULFLGINT_1_38.SetRadio()
    this.Parent.oContained.w_ULFLGINT=trim(this.Parent.oContained.w_ULFLGINT)
    this.value = ;
      iif(this.Parent.oContained.w_ULFLGINT=='S',1,;
      0)
  endfunc

  add object oStr_1_18 as StdString with uid="SXZYHTJRHE",Visible=.t., Left=5, Top=14,;
    Alignment=1, Width=152, Height=19,;
    Caption="SSCC unit� logistica:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="XQDUFZONKR",Visible=.t., Left=5, Top=45,;
    Alignment=1, Width=152, Height=18,;
    Caption="Codice di ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PTTBUDUDBK",Visible=.t., Left=5, Top=74,;
    Alignment=1, Width=152, Height=18,;
    Caption="Codice articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="VUZOSWGCPZ",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=152, Height=18,;
    Caption="Codice lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="UCUFTGRSJL",Visible=.t., Left=5, Top=132,;
    Alignment=1, Width=152, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PQHQGLMFMJ",Visible=.t., Left=5, Top=160,;
    Alignment=1, Width=152, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="UPHOAROPKO",Visible=.t., Left=243, Top=192,;
    Alignment=1, Width=75, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="TQBKSMSUOP",Visible=.t., Left=5, Top=192,;
    Alignment=1, Width=152, Height=18,;
    Caption="Num. conf.:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="AKXBDJFOJB",Visible=.t., Left=420, Top=192,;
    Alignment=1, Width=35, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ULCODART))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_aul','UNIT_LOG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UL__SSCC=UNIT_LOG.UL__SSCC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
