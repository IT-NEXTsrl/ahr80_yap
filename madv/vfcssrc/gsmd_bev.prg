* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bev                                                        *
*              Eventi conferma maschera aggiorna lotti                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_94]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-26                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bev",oParentObject,m.pEXEC)
return(i_retval)

define class tgsmd_bev as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CPROWNUM = 0
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_COMAG = space(5)
  w_CODLOT = space(20)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_FLSTAT = space(1)
  w_FLLOTT = space(1)
  w_FLAGLO = space(1)
  w_FLAG2LO = space(1)
  w_QTAESI = 0
  w_OLDQTA = 0
  w_QTAUM1 = 0
  w_CARSCA = space(1)
  w_TESDIS = .f.
  w_NR = space(6)
  w_ND = space(6)
  w_COART = space(20)
  w_MESS = space(100)
  w_TRIG = 0
  w_CFUNC = space(10)
  w_OK = .f.
  w_SRV = space(1)
  w_RECO = 0
  w_CODMAT = space(5)
  w_FLDEL = .f.
  w_TOTQTA = 0
  w_QTAMOV = 0
  w_LFLVEAC = space(1)
  w_LCLADOC = space(2)
  w_TIPDOC = space(5)
  w_FLANAL = space(1)
  w_SUQTRPER = 0
  w_SUQTAPER = 0
  w_DISLOT = space(1)
  w_LMESS = space(200)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_OLDUBI = space(20)
  w_OLDLOT = space(20)
  w_SILOT = space(1)
  * --- WorkFile variables
  LOTTIART_idx=0
  SPL_LOTT_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  SALDILOT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi alla Conferma della Maschera Articoli Composti (da GSMD_MLU)
    * --- Variabili Lotti
    this.w_oMess=createobject("Ah_Message")
    if this.pEXEC="A"
      WITH this.oParentObject.oParentObject
      .w_CONFERMA=.T.
      ENDWITH
    else
      * --- Controllo Disponibilita' Lotti/Ubicazioni
      * --- Controllo se la riga � stampata nel Libro Giornale
      this.w_OK = .T.
      this.w_MESS = ah_Msgformat("Transazione abbandonata")
      this.w_LCLADOC = This.oparentobject.oparentobject.w_LCLADOC
      this.w_LFLVEAC = IIF(this.w_LCLADOC="MM","",This.oparentobject.oparentobject.w_LFLVEAC)
      if this.w_LCLADOC<>"MM"
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVTIPDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVTIPDOC;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_MVSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLANAL"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLANAL;
            from (i_cTable) where;
                TDTIPDOC = this.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if Not Empty(CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_MVDATREG,"B","N"))
        this.w_MESS = CHKCONS(this.w_LFLVEAC+"M"+IIF(this.w_FLANAL="S","C",""),this.oParentObject.w_MVDATREG,"B","N") +"!"
        this.w_OK = .F.
      endif
      if this.w_OK
        this.w_TRIG = 0
        this.w_FLDEL = .F.
        this.w_CFUNC = this.oParentObject.cFunction
        * --- Preparo il cursore per la memorizzazione dei lotti consumati dalle righe
        *     del temporaneo dell'aggiornamento differito lotti
        CREATE CURSOR "TmpConsLot" ( CODART C(20), CODMAG C(5), CODLOT C(20), CODUBI C(20), QTACONS N(12,3) )
        ah_Msg("Controlli finali...",.T.)
        OLDSET = SET("DELETED")
        SET DELETED OFF
        SELECT (this.oParentObject.cTrsName)
        this.w_RECO = RECNO()
        * --- Cicla sulle Righe del Documento
        SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_MVCODICE)
        this.w_CPROWNUM = CPROWNUM
        this.w_NR = ALLTRIM(STR(t_CPROWORD))
        this.w_SRV = i_SRV
        this.w_COART = NVL(t_MVCODART, SPACE(20))
        this.w_FLAGLO = NVL(t_MVFLLOTT," ")
        this.w_FLAG2LO = NVL(t_MVF2LOTT," ")
        this.w_COMAG = NVL(t_MVCODMAG,SPACE(5))
        this.w_CODMAT = NVL(t_MVCODMAT,SPACE(5))
        this.w_OLDQTA = NVL(t_OLDQTA,0)
        this.w_FLUBIC = NVL(t_FLUBIC," ")
        this.w_F2UBIC = NVL(t_MVFLUBI2," ")
        this.w_FLLOTT = NVL(t_MVARTLOT," ")
        this.w_QTAMOV = NVL(t_MVQTAMOV,0)
        this.w_DISLOT = NVL(t_DISLOT, " ")
        * --- Test Se riga Eliminata
        this.w_FLDEL = IIF(this.w_CFUNC="Query" OR DELETED(), .T., .F.)
        this.w_TOTQTA = 0
        if upper(This.oparentobject.Class)="TGSMD_MLU"
          * --- Select from GSMD_QCD
          do vq_exec with 'GSMD_QCD',this,'_Curs_GSMD_QCD','',.f.,.t.
          if used('_Curs_GSMD_QCD')
            select _Curs_GSMD_QCD
            locate for 1=1
            do while not(eof())
            this.w_TRIG = this.w_TRIG + IIF(DELETED(), 0, 1)
            this.w_ND = ALLTRIM(STR(_Curs_GSMD_QCD.CPROWNUM))
            this.w_CODLOT = NVL(_Curs_GSMD_QCD.SPCODLOT,SPACE(20))
            if Not Empty(this.w_CODLOT)
              * --- Read from LOTTIART
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LOTTIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LOFLSTAT"+;
                  " from "+i_cTable+" LOTTIART where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LOFLSTAT;
                  from (i_cTable) where;
                      LOCODICE = this.w_CODLOT;
                      and LOCODART = this.w_COART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.w_CODUBI = NVL(_Curs_GSMD_QCD.SPCODUBI,SPACE(20))
            this.w_CODUB2 = NVL(_Curs_GSMD_QCD.SPCODUB2,SPACE(20))
            this.w_TOTQTA = this.w_TOTQTA + NVL(_Curs_GSMD_QCD.SPQTAMOV,0)
            this.w_QTAUM1 = NVL(_Curs_GSMD_QCD.SPQTAUM1,0)
            if this.w_OK
              if EMPTY(this.w_CODLOT) AND (this.w_FLAGLO $ "+-" OR this.w_FLAG2LO $ "+-") AND g_PERLOT="S" AND this.w_FLLOTT $ "SC" AND this.w_FLDEL=.F.
                this.w_MESS = ah_Msgformat("Riga movimento: %1%0Riga dello split: %2%0Articolo: %3%0Inserire codice lotto", this.w_NR, this.w_ND, ALLTRIM(this.w_COART) )
                this.w_OK = .F.
              endif
              if g_PERUBI="S"
                if EMPTY(this.w_CODUBI) AND NOT EMPTY(this.w_COMAG) AND this.w_FLUBIC="S" AND this.w_FLAGLO $ "+-" AND this.w_OK=.T. AND this.w_FLDEL=.F.
                  this.w_MESS = ah_Msgformat("Riga movimento: %1%0Riga dello split: %2%0Magazzino: %3%0Inserire codice ubicazione", this.w_NR, this.w_ND, alltrim(this.w_COMAG) )
                  this.w_OK = .F.
                endif
                if EMPTY(this.w_CODUB2) AND NOT EMPTY(this.w_CODMAT) AND this.w_F2UBIC="S" AND this.w_FLAG2LO $ "+-" AND this.w_OK=.T. AND this.w_FLDEL=.F.
                  this.w_MESS = ah_Msgformat("Riga movimento: %1%0Riga dello split: %2%0Magazzino: %3%0Inserire codice ubicazione collegata", this.w_NR, this.w_ND, alltrim(this.w_CODMAT) )
                  this.w_OK = .F.
                endif
              endif
              this.w_CARSCA = IIF(((this.w_FLAGLO="-" OR this.w_FLAG2LO="-") OR (this.w_FLAGLO="+" OR this.w_FLAG2LO="+") ),"S"," ")
              if (this.w_CARSCA="S" OR this.w_FLDEL=.T.) AND this.w_OK
                do case
                  case this.w_FLSTAT="E"
                    if !(((this.w_QTAUM1<this.w_OLDQTA OR this.w_FLDEL ) AND (this.w_FLAGLO="-" OR this.w_FLAG2LO="-")) OR ((this.w_FLAGLO="+" OR this.w_FLAG2LO="+"))) 
                      this.w_MESS = ah_Msgformat("Disponibilit� lotto: %1 esaurita" ,ALLTRIM(this.w_CODLOT) )
                      this.w_OK = .F.
                    endif
                  case this.w_FLSTAT="S"
                    this.w_MESS = ah_Msgformat("Lotto: %1 sospeso", ALLTRIM(this.w_CODLOT) )
                    this.w_OK = .F.
                  otherwise
                    this.w_OLDLOT = Nvl(_Curs_GSMD_QCD.MVCODLOT,Space(20))
                    if this.w_FLAGLO $ "-"
                      this.w_QTAESI = 0
                      this.w_SUQTRPER = 0
                      this.w_SUQTAPER = 0
                      this.w_OLDUBI = Nvl(_Curs_GSMD_QCD.MVCODUBI,Space(20))
                      this.Pag3()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                      if (this.w_QTAESI - this.w_QTAUM1)<0 AND this.w_DISLOT<>"N"
                        if this.w_DISLOT="S"
                          this.w_oPart = this.w_oMess.AddMsgPartNL("Riga movimento: %1%0Riga dello split: %2%0Articolo: %3")
                          this.w_oPart.AddParam(this.w_NR)     
                          this.w_oPart.AddParam(this.w_ND)     
                          this.w_oPart.AddParam( ALLTRIM(this.w_COART))     
                          if this.w_FLLOTT = "C" OR this.w_FLLOTT = "S"
                            this.w_oPart = this.w_oMess.AddMsgPartNL("Disponibilit� lotto: %1" )
                            this.w_oPart.AddParam(ALLTRIM(this.w_CODLOT))     
                          endif
                          this.w_oPart = this.w_oMess.AddMsgPartNL("Magazzino: %1")
                          this.w_oPart.AddParam(alltrim(this.w_COMAG))     
                          if this.w_FLUBIC="S"
                            this.w_oPart = this.w_oMess.AddMsgPartNL("Ubicazione: %1")
                            this.w_oPart.AddParam(ALLTRIM(this.w_CODUBI))     
                          endif
                          this.w_oPart = this.w_oMess.AddMsgPartNL("(%1) inferiore alla quantit� da scaricare")
                          this.w_oPart.AddParam(ALLTRIM(STR(this.w_QTAESI)))     
                          this.w_MESS = this.w_oMess.ComposeMessage()
                          this.w_OK = .F.
                        endif
                        if this.w_DISLOT="C"
                          this.w_LMESS = ""
                          this.w_LMESS = ah_MsgFormat("Riga movimento: %1%0Riga dello split: %2%0Articolo: %3",this.w_NR,this.w_ND,alltrim(this.w_COART))
                          if this.w_FLLOTT = "C" OR this.w_FLLOTT = "S"
                            this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Disponibilit� lotto: %1", ALLTRIM(this.w_CODLOT) )
                          endif
                          this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Magazzino: %1",alltrim (this.w_COMAG))
                          if this.w_FLUBIC="S"
                            this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Ubicazione: %1",alltrim (this.w_CODUBI))
                          endif
                          this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0(%1) inferiore alla quantit� da scaricare" , ALLTRIM(STR(this.w_QTAESI)))
                          this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Confermi ugualmente?")
                          if Not ah_YesNo(this.w_LMESS)
                            * --- Bloccante
                            this.w_OK = .F.
                          else
                            this.w_OK = .T.
                          endif
                        endif
                      endif
                    endif
                    if this.w_OK AND this.w_FLAG2LO $ "-" 
                      * --- Controllo Disponibilit� solo nel caso di Scarico
                      this.w_OLDUBI = Nvl(_Curs_GSMD_QCD.MVCODUB2,Space(20))
                      if NOT EMPTY(this.w_CODMAT) AND this.w_F2UBIC="S" 
                        * --- Controllo Disponibilit� Magazzino Collegato
                        if this.w_COMAG<>this.w_CODMAT OR this.w_CODUBI<>this.w_CODUB2
                          * --- Causale collegata  Magazzino collegato diverso
                          this.w_COMAG = this.w_CODMAT
                          this.w_CODUBI = this.w_CODUB2
                          this.w_QTAESI = 0
                          this.w_SUQTRPER = 0
                          this.w_SUQTAPER = 0
                          this.Pag3()
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                          if (this.w_QTAESI - this.w_QTAUM1)<0 AND this.w_DISLOT<>"N"
                            if this.w_DISLOT="S"
                              this.w_oPart = this.w_oMess.AddMsgPartNL("Riga movimento: %1%0Riga dello split: %2%0Articolo: %3")
                              this.w_oPart.AddParam(this.w_NR)     
                              this.w_oPart.AddParam(this.w_ND)     
                              this.w_oPart.AddParam( ALLTRIM(this.w_COART))     
                              if this.w_FLLOTT = "C" OR this.w_FLLOTT = "S"
                                this.w_oPart = this.w_oMess.AddMsgPartNL("Disponibilit� lotto: %1" )
                                this.w_oPart.AddParam(ALLTRIM(this.w_CODLOT))     
                              endif
                              this.w_oPart = this.w_oMess.AddMsgPartNL("Magazzino: %1")
                              this.w_oPart.AddParam(alltrim(this.w_COMAG))     
                              if this.w_F2UBIC="S"
                                this.w_oPart = this.w_oMess.AddMsgPartNL("Ubicazione: %1")
                                this.w_oPart.AddParam(ALLTRIM(this.w_CODUBI))     
                              endif
                              this.w_oPart = this.w_oMess.AddMsgPartNL("(%1) inferiore alla quantit� da scaricare")
                              this.w_oPart.AddParam(ALLTRIM(STR(this.w_QTAESI)))     
                              this.w_MESS = this.w_oMess.ComposeMessage()
                              this.w_OK = .F.
                            endif
                            if this.w_DISLOT="C"
                              this.w_LMESS = ""
                              this.w_LMESS = ah_MsgFormat("Riga movimento: %1%0Riga dello split: %2%0Articolo: %3",this.w_NR,this.w_ND,alltrim(this.w_COART))
                              if this.w_FLLOTT = "C" OR this.w_FLLOTT = "S"
                                this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Disponibilit� lotto: %1", ALLTRIM(this.w_CODLOT) )
                              endif
                              this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Magazzino: %1",alltrim (this.w_COMAG))
                              if this.w_FLUBIC="S"
                                this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Ubicazione: %1",alltrim (this.w_CODUBI))
                              endif
                              this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0(%1) inferiore alla quantit� da scaricare" , ALLTRIM(STR(this.w_QTAESI)))
                              this.w_LMESS = this.w_LMESS + ah_MsgFormat("%0Confermi ugualmente?")
                              if Not ah_YesNo(this.w_LMESS)
                                * --- Bloccante
                                this.w_OK = .F.
                              else
                                this.w_OK = .T.
                              endif
                            endif
                          endif
                        endif
                      endif
                    endif
                endcase
              endif
            endif
              select _Curs_GSMD_QCD
              continue
            enddo
            use
          endif
        else
          this.w_TRIG = this.w_TRIG + IIF(DELETED(), 0, 1)
        endif
        if this.w_OK=.T. AND this.w_QTAMOV <> this.w_TOTQTA AND NOT EMPTY(this.w_ND)
          this.w_MESS = ah_Msgformat("Riga movimento: %1%0Totale quantit� non congruente alla quantit� di origine%0Differenza riscontrata: %2", this.w_NR, alltrim(str(this.w_totqta - this.w_qtamov)) ) 
          this.w_OK = .F.
        endif
        if this.w_OK = .F.
          * --- Errore!
          EXIT
        endif
        ENDSCAN
        if this.w_CFUNC<>"Query" And g_MATR="S" AND this.w_OK=.T.
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_OK=.T. AND this.w_TRIG=0 AND this.w_CFUNC<>"Query"
          this.w_MESS = ah_Msgformat("Registrazione senza righe di dettaglio")
          this.w_OK = .F.
        endif
        SELECT (this.oParentObject.cTrsName)
        if this.w_RECO>0 AND this.w_RECO<=RECCOUNT()
          GOTO this.w_RECO
        endif
        * --- set deleted ripristinata al valore originario al ternmine del Batch
        SET DELETED &OLDSET
        WAIT CLEAR
        USE IN SELECT ("TmpConsLot")
      endif
      if this.w_OK = .F. 
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico la presenza delle matricole!
    if this.w_CFUNC<>"Query" And g_MATR="S" And this.oParentObject.w_MVDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
      if this.w_OK 
        * --- Verifico congruenza numero matricole con qt� di riga (SE NON IN FASE DI CORR. ERORRI LOTTI / UBICAZIONI)
        this.w_SILOT = "N"
        if g_PERLOT="S" OR g_PERUBI="S"
          this.w_SILOT = "S"
        endif
        * --- Select from GSMD_BEV
        do vq_exec with 'GSMD_BEV',this,'_Curs_GSMD_BEV','',.f.,.t.
        if used('_Curs_GSMD_BEV')
          select _Curs_GSMD_BEV
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          if cp_Round(Nvl(qtaum1,0),0)<>Nvl(qtaum1,0)
            this.w_MESS = ah_Msgformat("Riga %1 impossibile movimentare articoli gestiti a matricole per quantit� frazionabili", Alltrim( Str( _Curs_GSMD_BEV.ROWORD ) ) )
          else
            this.w_MESS = ah_Msgformat("Riga %1 con numero matricole incongruente. Qt� riga: %2, matricole movimentate: %3", Alltrim( Str( _Curs_GSMD_BEV.ROWORD ) ), Alltrim( Str( _Curs_GSMD_BEV.QTAUM1 ) ), Alltrim( Str( _Curs_GSMD_BEV.QTAMAT ) ) ) 
          endif
          Exit
            select _Curs_GSMD_BEV
            continue
          enddo
          use
        endif
      endif
      if this.w_OK 
        * --- Verifica se il documento contiene la stessa matricola due volte (cosa vietata)
        * --- Se ci� avviene la variabile caller w_OK viene posta a false
        * --- Select from GSMD_BCM
        do vq_exec with 'GSMD_BCM',this,'_Curs_GSMD_BCM','',.f.,.t.
        if used('_Curs_GSMD_BCM')
          select _Curs_GSMD_BCM
          locate for 1=1
          do while not(eof())
          this.w_OK = ( _Curs_GSMD_BCM.CODMAT<=1 )
          this.w_MESS = ah_Msgformat("Impossibile movimentare pi� volte la stessa matricola")
          Exit
            select _Curs_GSMD_BCM
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    local L_nOldArea
    * --- Read from SALDILOT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDILOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SUQTAPER,SUQTRPER"+;
        " from "+i_cTable+" SALDILOT where ";
            +"SUCODART = "+cp_ToStrODBC(this.w_COART);
            +" and SUCODMAG = "+cp_ToStrODBC(this.w_COMAG);
            +" and SUCODLOT = "+cp_ToStrODBC(this.w_CODLOT);
            +" and SUCODUBI = "+cp_ToStrODBC(this.w_CODUBI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SUQTAPER,SUQTRPER;
        from (i_cTable) where;
            SUCODART = this.w_COART;
            and SUCODMAG = this.w_COMAG;
            and SUCODLOT = this.w_CODLOT;
            and SUCODUBI = this.w_CODUBI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SUQTAPER = NVL(cp_ToDate(_read_.SUQTAPER),cp_NullValue(_read_.SUQTAPER))
      this.w_SUQTRPER = NVL(cp_ToDate(_read_.SUQTRPER),cp_NullValue(_read_.SUQTRPER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_QTAESI = this.w_QTAESI +this.w_SUQTAPER - this.w_SUQTRPER
    L_nOldArea = Select()
    SELECT "TmpConsLot"
    GO TOP
    LOCATE FOR CODART=this.w_COART AND CODMAG=this.w_COMAG AND CODLOT=this.w_CODLOT AND CODUBI=this.w_CODUBI
    if FOUND()
      this.w_QTAESI = this.w_QTAESI - TmpConsLot.QTACONS
      REPLACE QTACONS WITH QTACONS + this.w_QTAUM1
    else
      INSERT INTO "TmpConsLot" ( CODART, CODMAG, CODLOT, CODUBI, QTACONS ) VALUES ( this.w_COART, this.w_COMAG, this.w_CODLOT, this.w_CODUBI, this.w_QTAUM1 )
    endif
    if this.w_OLDUBI=this.w_CODUBI and this.w_OLDLOT=this.w_CODLOT
      this.w_QTAESI = this.w_QTAESI +this.w_QTAUM1
    endif
    select ( L_nOldArea )
    Release L_nOldArea
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='SPL_LOTT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='SALDILOT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSMD_QCD')
      use in _Curs_GSMD_QCD
    endif
    if used('_Curs_GSMD_BEV')
      use in _Curs_GSMD_BEV
    endif
    if used('_Curs_GSMD_BCM')
      use in _Curs_GSMD_BCM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
