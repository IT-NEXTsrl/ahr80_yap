* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bul                                                        *
*              Controlli finali SSCC                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-09                                                      *
* Last revis.: 2004-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bul",oParentObject)
return(i_retval)

define class tgsmd_bul as StdBatch
  * --- Local variables
  w_MESS = space(254)
  w_OK = .f.
  w_MONOLOT = .f.
  w_MONOART = .f.
  w_CODART = space(20)
  w_CODLOT = space(20)
  w_CODCOL = space(5)
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli congruit� tra documenti e unit� logistica (da GSMD_AUL)
    * --- Seleziono i documenti che movimentato il pallet selezionato
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVCODART,MVCODLOT, MAX(MVCODCOL) as MVCODCOL  from "+i_cTable+" DOC_DETT ";
          +" where MVUNILOG="+cp_ToStrODBC(this.oParentObject.w_UL__SSCC)+"";
          +" group by MVCODART, MVCODLOT";
           ,"_Curs_DOC_DETT")
    else
      select MVCODART,MVCODLOT, MAX(MVCODCOL) as MVCODCOL from (i_cTable);
       where MVUNILOG=this.oParentObject.w_UL__SSCC;
       group by MVCODART, MVCODLOT;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      if reccount()=1
        * --- Mono-Prodotto (e Mono-Lotto)
        this.w_MONOART = .T.
        this.w_CODART = MVCODART
        this.w_CODCOL = MVCODCOL
        this.w_CODLOT = MVCODLOT
        * --- Se MVCODLOT � vuoto allora il pallet � solo mono-prodotto
        this.w_MONOLOT = IIF(EMPTY(NVL(MVCODLOT," ")), .F., .T.)
        this.w_OK = .T.
      endif
      exit
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    if NOT this.w_OK
      * --- Verifico se il pallet � mono-prodotto
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVCODART, MAX(MVCODCOL) as MVCODCOL  from "+i_cTable+" DOC_DETT ";
            +" where MVUNILOG="+cp_ToStrODBC(this.oParentObject.w_UL__SSCC)+"";
            +" group by MVCODART";
             ,"_Curs_DOC_DETT")
      else
        select MVCODART, MAX(MVCODCOL) as MVCODCOL from (i_cTable);
         where MVUNILOG=this.oParentObject.w_UL__SSCC;
         group by MVCODART;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        if reccount()=1
          * --- Mono-Prodotto
          this.w_MONOART = .T.
          this.w_MONOLOT = .F.
          this.w_CODART = MVCODART
          this.w_CODCOL = MVCODCOL
        else
          * --- Pluri-Prodotto e Pluri-Lotto
          this.w_MONOART = .F.
          this.w_MONOLOT = .F.
        endif
        * --- Metto a true w_ok se trovo almeno un documento che movimenta il pallet
        this.w_OK = .T.
        exit
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
    endif
    * --- Verifico le eventuali incongruenze tra l'unit� logistica appena modificata e 
    *     i documenti nei quali � stata movimentata
    if this.w_OK
      this.w_MESS = CheckSSCC(, this.oParentObject.w_UL__SSCC, this.w_MONOART, this.w_MONOLOT, this.w_CODART, this.w_CODLOT, this.w_CODCOL)
      if NOT EMPTY(this.w_MESS)
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_Msgformat("L'unit� logistica non pu� essere modificata")
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
