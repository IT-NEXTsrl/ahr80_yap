* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_asl                                                        *
*              Saldi lotti/ubicazioni                                          *
*                                                                              *
*      Author: Zucchetti Spa - AT                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-28                                                      *
* Last revis.: 2014-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_asl"))

* --- Class definition
define class tgsmd_asl as StdForm
  Top    = 3
  Left   = 5

  * --- Standard Properties
  Width  = 563
  Height = 217+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-30"
  HelpContextID=158708887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  SALDILOT_IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  DET_VARI_IDX = 0
  UBICAZIO_IDX = 0
  LOTTIART_IDX = 0
  cFile = "SALDILOT"
  cKeySelect = "SUCODMAG,SUCODUBI,SUCODART,SUCODLOT"
  cKeyWhere  = "SUCODMAG=this.w_SUCODMAG and SUCODUBI=this.w_SUCODUBI and SUCODART=this.w_SUCODART and SUCODLOT=this.w_SUCODLOT"
  cKeyWhereODBC = '"SUCODMAG="+cp_ToStrODBC(this.w_SUCODMAG)';
      +'+" and SUCODUBI="+cp_ToStrODBC(this.w_SUCODUBI)';
      +'+" and SUCODART="+cp_ToStrODBC(this.w_SUCODART)';
      +'+" and SUCODLOT="+cp_ToStrODBC(this.w_SUCODLOT)';

  cKeyWhereODBCqualified = '"SALDILOT.SUCODMAG="+cp_ToStrODBC(this.w_SUCODMAG)';
      +'+" and SALDILOT.SUCODUBI="+cp_ToStrODBC(this.w_SUCODUBI)';
      +'+" and SALDILOT.SUCODART="+cp_ToStrODBC(this.w_SUCODART)';
      +'+" and SALDILOT.SUCODLOT="+cp_ToStrODBC(this.w_SUCODLOT)';

  cPrg = "gsmd_asl"
  cComment = "Saldi lotti/ubicazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(4)
  w_SUCODMAG = space(5)
  o_SUCODMAG = space(5)
  w_SUCODUBI = space(20)
  w_CODUBI = space(10)
  o_CODUBI = space(10)
  w_DESMAG = space(30)
  w_SUCODART = space(20)
  o_SUCODART = space(20)
  w_DESART = space(40)
  w_SUCODLOT = space(20)
  w_CODLOT = space(20)
  o_CODLOT = space(20)
  w_TIPART = space(2)
  w_SUQTAPER = 0
  w_SUQTRPER = 0
  w_DISPOPER = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESUBI = space(40)
  w_FLUBIC = space(1)
  w_FLLOTT = space(1)
  w_SUQTAPRO = 0
  w_SUQTRPRO = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALDILOT','gsmd_asl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_aslPag1","gsmd_asl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 217156342
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSUCODMAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='DET_VARI'
    this.cWorkTables[4]='UBICAZIO'
    this.cWorkTables[5]='LOTTIART'
    this.cWorkTables[6]='SALDILOT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALDILOT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALDILOT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SUCODMAG = NVL(SUCODMAG,space(5))
      .w_SUCODUBI = NVL(SUCODUBI,space(20))
      .w_SUCODART = NVL(SUCODART,space(20))
      .w_SUCODLOT = NVL(SUCODLOT,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SALDILOT where SUCODMAG=KeySet.SUCODMAG
    *                            and SUCODUBI=KeySet.SUCODUBI
    *                            and SUCODART=KeySet.SUCODART
    *                            and SUCODLOT=KeySet.SUCODLOT
    *
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALDILOT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALDILOT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SALDILOT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SUCODMAG',this.w_SUCODMAG  ,'SUCODUBI',this.w_SUCODUBI  ,'SUCODART',this.w_SUCODART  ,'SUCODLOT',this.w_SUCODLOT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESMAG = space(30)
        .w_DESART = space(40)
        .w_TIPART = space(2)
        .w_OBTEST = i_INIDAT
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESUBI = space(40)
        .w_FLUBIC = space(1)
        .w_FLLOTT = space(1)
        .w_TIPOPE = this.cFunction
        .w_SUCODMAG = NVL(SUCODMAG,space(5))
          .link_1_2('Load')
        .w_SUCODUBI = NVL(SUCODUBI,space(20))
        .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODUBI) 
          .link_1_4('Load')
        .w_SUCODART = NVL(SUCODART,space(20))
          .link_1_6('Load')
        .w_SUCODLOT = NVL(SUCODLOT,space(20))
        .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODLOT) 
          .link_1_9('Load')
        .w_SUQTAPER = NVL(SUQTAPER,0)
        .w_SUQTRPER = NVL(SUQTRPER,0)
        .w_DISPOPER = .w_SUQTAPER-.w_SUQTRPER
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_SUQTAPRO = NVL(SUQTAPRO,0)
        .w_SUQTRPRO = NVL(SUQTRPRO,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'SALDILOT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsmd_asl
    this.w_CODUBI=this.w_SUCODUBI
    this.w_CODLOT=this.w_SUCODLOT
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPE = space(4)
      .w_SUCODMAG = space(5)
      .w_SUCODUBI = space(20)
      .w_CODUBI = space(10)
      .w_DESMAG = space(30)
      .w_SUCODART = space(20)
      .w_DESART = space(40)
      .w_SUCODLOT = space(20)
      .w_CODLOT = space(20)
      .w_TIPART = space(2)
      .w_SUQTAPER = 0
      .w_SUQTRPER = 0
      .w_DISPOPER = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESUBI = space(40)
      .w_FLUBIC = space(1)
      .w_FLLOTT = space(1)
      .w_SUQTAPRO = 0
      .w_SUQTRPRO = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_TIPOPE = this.cFunction
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_SUCODMAG))
          .link_1_2('Full')
          endif
        .w_SUCODUBI = IIF (.cFunction<>'Query', .w_CODUBI,.w_SUCODUBI)
        .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODUBI) 
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODUBI))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,6,.f.)
          if not(empty(.w_SUCODART))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_SUCODLOT = IIF (.cFunction<>'Query', .w_CODLOT , .w_SUCODLOT)
        .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODLOT) 
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CODLOT))
          .link_1_9('Full')
          endif
          .DoRTCalc(10,12,.f.)
        .w_DISPOPER = .w_SUQTAPER-.w_SUQTRPER
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALDILOT')
    this.DoRTCalc(15,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSUCODMAG_1_2.enabled = i_bVal
      .Page1.oPag.oSUCODUBI_1_3.enabled = i_bVal
      .Page1.oPag.oCODUBI_1_4.enabled = i_bVal
      .Page1.oPag.oSUCODART_1_6.enabled = i_bVal
      .Page1.oPag.oSUCODLOT_1_8.enabled = i_bVal
      .Page1.oPag.oCODLOT_1_9.enabled = i_bVal
      .Page1.oPag.oSUQTAPER_1_11.enabled = i_bVal
      .Page1.oPag.oSUQTRPER_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSUCODMAG_1_2.enabled = .f.
        .Page1.oPag.oSUCODUBI_1_3.enabled = .f.
        .Page1.oPag.oSUCODART_1_6.enabled = .f.
        .Page1.oPag.oSUCODLOT_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSUCODMAG_1_2.enabled = .t.
        .Page1.oPag.oSUCODUBI_1_3.enabled = .t.
        .Page1.oPag.oSUCODART_1_6.enabled = .t.
        .Page1.oPag.oSUCODLOT_1_8.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALDILOT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUCODMAG,"SUCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUCODUBI,"SUCODUBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUCODART,"SUCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUCODLOT,"SUCODLOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUQTAPER,"SUQTAPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUQTRPER,"SUQTRPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUQTAPRO,"SUQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SUQTRPRO,"SUQTRPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_lTable = "SALDILOT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALDILOT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALDILOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SALDILOT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALDILOT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALDILOT')
        i_extval=cp_InsertValODBCExtFlds(this,'SALDILOT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SUCODMAG,SUCODUBI,SUCODART,SUCODLOT,SUQTAPER"+;
                  ",SUQTRPER,SUQTAPRO,SUQTRPRO,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SUCODMAG)+;
                  ","+cp_ToStrODBC(this.w_SUCODUBI)+;
                  ","+cp_ToStrODBCNull(this.w_SUCODART)+;
                  ","+cp_ToStrODBC(this.w_SUCODLOT)+;
                  ","+cp_ToStrODBC(this.w_SUQTAPER)+;
                  ","+cp_ToStrODBC(this.w_SUQTRPER)+;
                  ","+cp_ToStrODBC(this.w_SUQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_SUQTRPRO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALDILOT')
        i_extval=cp_InsertValVFPExtFlds(this,'SALDILOT')
        cp_CheckDeletedKey(i_cTable,0,'SUCODMAG',this.w_SUCODMAG,'SUCODUBI',this.w_SUCODUBI,'SUCODART',this.w_SUCODART,'SUCODLOT',this.w_SUCODLOT)
        INSERT INTO (i_cTable);
              (SUCODMAG,SUCODUBI,SUCODART,SUCODLOT,SUQTAPER,SUQTRPER,SUQTAPRO,SUQTRPRO,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SUCODMAG;
                  ,this.w_SUCODUBI;
                  ,this.w_SUCODART;
                  ,this.w_SUCODLOT;
                  ,this.w_SUQTAPER;
                  ,this.w_SUQTRPER;
                  ,this.w_SUQTAPRO;
                  ,this.w_SUQTRPRO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SALDILOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SALDILOT_IDX,i_nConn)
      *
      * update SALDILOT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SALDILOT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SUQTAPER="+cp_ToStrODBC(this.w_SUQTAPER)+;
             ",SUQTRPER="+cp_ToStrODBC(this.w_SUQTRPER)+;
             ",SUQTAPRO="+cp_ToStrODBC(this.w_SUQTAPRO)+;
             ",SUQTRPRO="+cp_ToStrODBC(this.w_SUQTRPRO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SALDILOT')
        i_cWhere = cp_PKFox(i_cTable  ,'SUCODMAG',this.w_SUCODMAG  ,'SUCODUBI',this.w_SUCODUBI  ,'SUCODART',this.w_SUCODART  ,'SUCODLOT',this.w_SUCODLOT  )
        UPDATE (i_cTable) SET;
              SUQTAPER=this.w_SUQTAPER;
             ,SUQTRPER=this.w_SUQTRPER;
             ,SUQTAPRO=this.w_SUQTAPRO;
             ,SUQTRPRO=this.w_SUQTRPRO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SALDILOT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SALDILOT_IDX,i_nConn)
      *
      * delete SALDILOT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SUCODMAG',this.w_SUCODMAG  ,'SUCODUBI',this.w_SUCODUBI  ,'SUCODART',this.w_SUCODART  ,'SUCODLOT',this.w_SUCODLOT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    if i_bUpd
      with this
            .w_TIPOPE = this.cFunction
        .DoRTCalc(2,2,.t.)
        if .o_SUCODMAG<>.w_SUCODMAG.or. .o_CODUBI<>.w_CODUBI
            .w_SUCODUBI = IIF (.cFunction<>'Query', .w_CODUBI,.w_SUCODUBI)
        endif
        if .o_SUCODMAG<>.w_SUCODMAG
            .w_CODUBI = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODUBI) 
          .link_1_4('Full')
        endif
        .DoRTCalc(5,7,.t.)
        if .o_SUCODART<>.w_SUCODART.or. .o_CODLOT<>.w_CODLOT
            .w_SUCODLOT = IIF (.cFunction<>'Query', .w_CODLOT , .w_SUCODLOT)
        endif
        if .o_SUCODART<>.w_SUCODART
            .w_CODLOT = IIF (.cFunction<>'Query', SPACE(20),.w_SUCODLOT) 
          .link_1_9('Full')
        endif
        .DoRTCalc(10,12,.t.)
            .w_DISPOPER = .w_SUQTAPER-.w_SUQTRPER
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUBI_1_4.enabled = this.oPgFrm.Page1.oPag.oCODUBI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSUCODART_1_6.enabled = this.oPgFrm.Page1.oPag.oSUCODART_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODLOT_1_9.enabled = this.oPgFrm.Page1.oPag.oCODLOT_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSUCODUBI_1_3.visible=!this.oPgFrm.Page1.oPag.oSUCODUBI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCODUBI_1_4.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oSUCODLOT_1_8.visible=!this.oPgFrm.Page1.oPag.oSUCODLOT_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCODLOT_1_9.visible=!this.oPgFrm.Page1.oPag.oCODLOT_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SUCODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_SUCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_SUCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_SUCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_SUCODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SUCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oSUCODMAG_1_2'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_SUCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_SUCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SUCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLUBIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUBI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_CODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_SUCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_SUCODMAG;
                     ,'UBCODICE',trim(this.w_CODUBI))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oCODUBI_1_4'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SUCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_SUCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_CODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_SUCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_SUCODMAG;
                       ,'UBCODICE',this.w_CODUBI)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUBI = NVL(_Link_.UBCODICE,space(10))
      this.w_DESUBI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUBI = space(10)
      endif
      this.w_DESUBI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUCODART
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SUCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SUCODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_SUCODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_SUCODART)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SUCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSUCODART_1_6'),i_cWhere,'GSMA_AAR',"Articoli",'GSMA1ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SUCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SUCODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARFLLOTT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SUCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLLOTT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND NVL (.w_FLLOTT , '  ')<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endif
        this.w_SUCODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLLOTT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLOT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_CODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_SUCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_SUCODART;
                     ,'LOCODICE',trim(this.w_CODLOT))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oCODLOT_1_9'),i_cWhere,'GSMD_ALO',"Lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SUCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_SUCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_CODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_SUCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_SUCODART;
                       ,'LOCODICE',this.w_CODLOT)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLOT = NVL(_Link_.LOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODLOT = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSUCODMAG_1_2.value==this.w_SUCODMAG)
      this.oPgFrm.Page1.oPag.oSUCODMAG_1_2.value=this.w_SUCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSUCODUBI_1_3.value==this.w_SUCODUBI)
      this.oPgFrm.Page1.oPag.oSUCODUBI_1_3.value=this.w_SUCODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_4.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_4.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_5.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_5.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSUCODART_1_6.value==this.w_SUCODART)
      this.oPgFrm.Page1.oPag.oSUCODART_1_6.value=this.w_SUCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSUCODLOT_1_8.value==this.w_SUCODLOT)
      this.oPgFrm.Page1.oPag.oSUCODLOT_1_8.value=this.w_SUCODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOT_1_9.value==this.w_CODLOT)
      this.oPgFrm.Page1.oPag.oCODLOT_1_9.value=this.w_CODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oSUQTAPER_1_11.value==this.w_SUQTAPER)
      this.oPgFrm.Page1.oPag.oSUQTAPER_1_11.value=this.w_SUQTAPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSUQTRPER_1_12.value==this.w_SUQTRPER)
      this.oPgFrm.Page1.oPag.oSUQTRPER_1_12.value=this.w_SUQTRPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPOPER_1_13.value==this.w_DISPOPER)
      this.oPgFrm.Page1.oPag.oDISPOPER_1_13.value=this.w_DISPOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUBI_1_24.value==this.w_DESUBI)
      this.oPgFrm.Page1.oPag.oDESUBI_1_24.value=this.w_DESUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oSUQTAPRO_1_28.value==this.w_SUQTAPRO)
      this.oPgFrm.Page1.oPag.oSUQTAPRO_1_28.value=this.w_SUQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSUQTRPRO_1_29.value==this.w_SUQTRPRO)
      this.oPgFrm.Page1.oPag.oSUQTRPRO_1_29.value=this.w_SUQTRPRO
    endif
    cp_SetControlsValueExtFlds(this,'SALDILOT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SUCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSUCODMAG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SUCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SUCODART)) or not((.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND NVL (.w_FLLOTT , '  ')<>'N'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSUCODART_1_6.SetFocus()
            i_bnoObbl = !empty(.w_SUCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsmd_asl
      If i_bRes And !(g_PERUBI='S' Or g_PERLOT='S')
       		i_bRes = .f.
       		i_bnoChk = .f.		
          i_cErrorMsg =Ah_MsgFormat("Gestione lotti ed ubicazioni non attivi a livello di azienda")
      Else
          If i_bRes And Empty(.w_SUCODART + .w_SUCODUBI)
       	  	i_bRes = .f.
       	  	i_bnoChk = .f.		
            i_cErrorMsg = Ah_MsgFormat("Codice lotto o codice ubicazione non impostato")
          Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SUCODMAG = this.w_SUCODMAG
    this.o_CODUBI = this.w_CODUBI
    this.o_SUCODART = this.w_SUCODART
    this.o_CODLOT = this.w_CODLOT
    return

enddefine

* --- Define pages as container
define class tgsmd_aslPag1 as StdContainer
  Width  = 559
  height = 217
  stdWidth  = 559
  stdheight = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSUCODMAG_1_2 as StdField with uid="NADSKCYPRP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SUCODMAG", cQueryName = "SUCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 83280019,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=96, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_SUCODMAG"

  func oSUCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
      if .not. empty(.w_CODUBI)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSUCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oSUCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oSUCODMAG_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_SUCODMAG
     i_obj.ecpSave()
  endproc

  add object oSUCODUBI_1_3 as StdField with uid="JBCXRJMLVY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SUCODUBI", cQueryName = "SUCODMAG,SUCODUBI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 217497745,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=96, Top=36, InputMask=replicate('X',20)

  func oSUCODUBI_1_3.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND  .cFunction<>'Filter' )
    endwith
  endfunc

  add object oCODUBI_1_4 as StdField with uid="SXTGAQFOQS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione",;
    HelpContextID = 152090586,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_SUCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_CODUBI"

  func oCODUBI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit' And g_PERUBI = 'S' And .w_FLUBIC ='S')
    endwith
   endif
  endfunc

  func oCODUBI_1_4.mHide()
    with this.Parent.oContained
      return (NOT (.cFunction<>'Query' AND  .cFunction<>'Filter' ))
    endwith
  endfunc

  func oCODUBI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUBI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUBI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_SUCODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_SUCODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oCODUBI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_MUB',"Ubicazioni",'',this.parent.oContained
  endproc
  proc oCODUBI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_SUCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_CODUBI
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_5 as StdField with uid="VTWFBBMJMA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 187158986,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=171, Top=9, InputMask=replicate('X',30)

  add object oSUCODART_1_6 as StdField with uid="DQHWJQQANT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SUCODART", cQueryName = "SUCODMAG,SUCODUBI,SUCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente oppure obsoleto",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 16171142,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=63, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_SUCODART"

  func oSUCODART_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oSUCODART_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_CODLOT)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSUCODART_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUCODART_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSUCODART_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Articoli",'GSMA1ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSUCODART_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SUCODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_7 as StdField with uid="PPOWDSBJGJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220451274,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=63, InputMask=replicate('X',40)

  add object oSUCODLOT_1_8 as StdField with uid="MUMIBQMNQZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SUCODLOT", cQueryName = "SUCODMAG,SUCODUBI,SUCODART,SUCODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 168378234,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=87, InputMask=replicate('X',20)

  func oSUCODLOT_1_8.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Query' AND  .cFunction<>'Filter' )
    endwith
  endfunc

  add object oCODLOT_1_9 as StdField with uid="SQAWMUBGXK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODLOT", cQueryName = "CODLOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ubicazione",;
    HelpContextID = 222935002,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=96, Top=87, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_SUCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_CODLOT"

  func oCODLOT_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit' And g_PERLOT = 'S' And (.w_FLLOTT = 'S'  OR  .w_FLLOTT = 'C' ) )
    endwith
   endif
  endfunc

  func oCODLOT_1_9.mHide()
    with this.Parent.oContained
      return (NOT (.cFunction<>'Query' AND  .cFunction<>'Filter' ))
    endwith
  endfunc

  func oCODLOT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLOT_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLOT_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_SUCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_SUCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oCODLOT_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ALO',"Lotti",'',this.parent.oContained
  endproc
  proc oCODLOT_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_SUCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_CODLOT
     i_obj.ecpSave()
  endproc

  add object oSUQTAPER_1_11 as StdField with uid="HBHFIZDNUI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SUQTAPER", cQueryName = "SUQTAPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza",;
    HelpContextID = 35709064,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=135, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSUQTRPER_1_12 as StdField with uid="WLUPBGAOVV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SUQTRPER", cQueryName = "SUQTRPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata",;
    HelpContextID = 17883272,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=159, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oDISPOPER_1_13 as StdField with uid="UZFQQDECWD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DISPOPER", cQueryName = "DISPOPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza fra l'esistenza e la quantitą riservata",;
    HelpContextID = 21286264,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=96, Top=191, cSayPict="v_PQ(17)", cGetPict="v_GQ(17)"


  add object oObj_1_22 as cp_runprogram with uid="UJOBPIBCFS",left=-3, top=229, width=169,height=19,;
    caption='GSMD_BMC',;
   bGlobalFont=.t.,;
    prg="GSMD_BMC('LOT')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 28236969

  add object oDESUBI_1_24 as StdField with uid="ZEZWMNSFIM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESUBI", cQueryName = "DESUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 152031690,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=36, InputMask=replicate('X',40)

  add object oSUQTAPRO_1_28 as StdField with uid="LVHUPBAFPS",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SUQTAPRO", cQueryName = "SUQTAPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza movimenti fuori linea",;
    HelpContextID = 35709067,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=297, Top=135, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSUQTRPRO_1_29 as StdField with uid="JEJLUSICFE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SUQTRPRO", cQueryName = "SUQTRPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata fuori linea",;
    HelpContextID = 17883275,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=297, Top=159, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oStr_1_14 as StdString with uid="ZYBQELVOXG",Visible=.t., Left=7, Top=63,;
    Alignment=1, Width=83, Height=15,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="GGRLWGCGFS",Visible=.t., Left=7, Top=9,;
    Alignment=1, Width=83, Height=15,;
    Caption="Magazzino:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="MKHWSLYRMX",Visible=.t., Left=5, Top=159,;
    Alignment=1, Width=85, Height=18,;
    Caption="Riservato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PKVTUNPKFC",Visible=.t., Left=5, Top=135,;
    Alignment=1, Width=85, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YRDDOINMWV",Visible=.t., Left=5, Top=191,;
    Alignment=1, Width=85, Height=15,;
    Caption="Disponibilitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="EUHKXZIIXD",Visible=.t., Left=7, Top=36,;
    Alignment=1, Width=83, Height=15,;
    Caption="Ubicazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="EWZCWQUAAA",Visible=.t., Left=13, Top=113,;
    Alignment=0, Width=88, Height=15,;
    Caption="Saldi in linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZFUAPVOXAT",Visible=.t., Left=215, Top=159,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YLIFYMXWWI",Visible=.t., Left=215, Top=135,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="KIMQMNUBME",Visible=.t., Left=261, Top=113,;
    Alignment=0, Width=99, Height=18,;
    Caption="Saldi fuori linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JFJKLBGWZL",Visible=.t., Left=7, Top=86,;
    Alignment=1, Width=83, Height=15,;
    Caption="Lotto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_18 as StdBox with uid="YJGBELFMFO",left=1, top=129, width=535,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_asl','SALDILOT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SUCODMAG=SALDILOT.SUCODMAG";
  +" and "+i_cAliasName2+".SUCODUBI=SALDILOT.SUCODUBI";
  +" and "+i_cAliasName2+".SUCODART=SALDILOT.SUCODART";
  +" and "+i_cAliasName2+".SUCODLOT=SALDILOT.SUCODLOT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
