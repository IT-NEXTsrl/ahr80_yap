* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_kem                                                        *
*              Elimina matricole                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_42]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-07                                                      *
* Last revis.: 2014-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_kem",oParentObject))

* --- Class definition
define class tgsmd_kem as StdForm
  Top    = 6
  Left   = 8

  * --- Standard Properties
  Width  = 714
  Height = 442+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-25"
  HelpContextID=202749079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  CMT_MAST_IDX = 0
  MATRICOL_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsmd_kem"
  cComment = "Elimina matricole"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_MATINI_M = space(40)
  o_MATINI_M = space(40)
  w_MATFIN_M = space(40)
  o_MATFIN_M = space(40)
  w_MATINI_A = space(40)
  o_MATINI_A = space(40)
  w_MATFIN_A = space(40)
  o_MATFIN_A = space(40)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CLAINI = space(10)
  w_CLAFIN = space(10)
  w_SELEZI = space(1)
  w_KEYSINI = space(20)
  w_ARTINI = space(20)
  w_KEYSFIN = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_MATINI = space(40)
  w_MATFIN = space(40)
  w_AMCODART = space(10)
  w_AMCODICE = space(40)
  w_AMKEYSAL = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_Msg = space(0)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_kemPag1","gsmd_kem",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsmd_kemPag2","gsmd_kem",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='CMT_MAST'
    this.cWorkTables[3]='MATRICOL'
    this.cWorkTables[4]='ART_ICOL'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_MATINI_M=space(40)
      .w_MATFIN_M=space(40)
      .w_MATINI_A=space(40)
      .w_MATFIN_A=space(40)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CLAINI=space(10)
      .w_CLAFIN=space(10)
      .w_SELEZI=space(1)
      .w_KEYSINI=space(20)
      .w_ARTINI=space(20)
      .w_KEYSFIN=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_MATINI=space(40)
      .w_MATFIN=space(40)
      .w_AMCODART=space(10)
      .w_AMCODICE=space(40)
      .w_AMKEYSAL=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_Msg=space(0)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_1('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_MATINI_M = Space(40)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_MATINI_M))
          .link_1_7('Full')
        endif
        .w_MATFIN_M = IIF( Not Empty(.w_MATINI_M) , .w_MATINI_M  ,Space(40))
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MATFIN_M))
          .link_1_8('Full')
        endif
        .w_MATINI_A = Space(40)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MATINI_A))
          .link_1_9('Full')
        endif
        .w_MATFIN_A = IIF( Not Empty(.w_MATINI_A) , .w_MATINI_A  ,Space(40))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_MATFIN_A))
          .link_1_10('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_CLAINI))
          .link_1_15('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CLAFIN))
          .link_1_16('Full')
        endif
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_KEYSINI = .w_CODINI
          .DoRTCalc(15,15,.f.)
        .w_KEYSFIN = .w_CODFIN
          .DoRTCalc(17,17,.f.)
        .w_MATINI = Left( Alltrim( .w_MATINI_M+.w_MATINI_A ) + Space ( 40 ) , 40 )
        .w_MATFIN = Left( Alltrim( .w_MATFIN_M+.w_MATFIN_A ) + Space ( 40 ) , 40 )
        .w_AMCODART = .w_Zoom.getVar('AMCODART')
        .w_AMCODICE = .w_Zoom.getVar('AMCODICE')
        .w_AMKEYSAL = .w_Zoom.getVar('AMKEYSAL')
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
    this.DoRTCalc(24,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI
            .w_MATINI_M = Space(40)
          .link_1_7('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI.or. .o_MATINI_M<>.w_MATINI_M
            .w_MATFIN_M = IIF( Not Empty(.w_MATINI_M) , .w_MATINI_M  ,Space(40))
          .link_1_8('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI
            .w_MATINI_A = Space(40)
          .link_1_9('Full')
        endif
        if .o_CODFIN<>.w_CODFIN.or. .o_CODINI<>.w_CODINI.or. .o_MATINI_A<>.w_MATINI_A
            .w_MATFIN_A = IIF( Not Empty(.w_MATINI_A) , .w_MATINI_A  ,Space(40))
          .link_1_10('Full')
        endif
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(9,13,.t.)
        if .o_CODINI<>.w_CODINI
            .w_KEYSINI = .w_CODINI
        endif
        .DoRTCalc(15,15,.t.)
        if .o_CODFIN<>.w_CODFIN
            .w_KEYSFIN = .w_CODFIN
        endif
        .DoRTCalc(17,17,.t.)
        if .o_MATINI_A<>.w_MATINI_A.or. .o_MATINI_M<>.w_MATINI_M
            .w_MATINI = Left( Alltrim( .w_MATINI_M+.w_MATINI_A ) + Space ( 40 ) , 40 )
        endif
        if .o_MATFIN_A<>.w_MATFIN_A.or. .o_MATFIN_M<>.w_MATFIN_M
            .w_MATFIN = Left( Alltrim( .w_MATFIN_M+.w_MATFIN_A ) + Space ( 40 ) , 40 )
        endif
            .w_AMCODART = .w_Zoom.getVar('AMCODART')
            .w_AMCODICE = .w_Zoom.getVar('AMCODICE')
            .w_AMKEYSAL = .w_Zoom.getVar('AMKEYSAL')
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMATINI_M_1_7.visible=!this.oPgFrm.Page1.oPag.oMATINI_M_1_7.mHide()
    this.oPgFrm.Page1.oPag.oMATFIN_M_1_8.visible=!this.oPgFrm.Page1.oPag.oMATFIN_M_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMATINI_A_1_9.visible=!this.oPgFrm.Page1.oPag.oMATINI_A_1_9.mHide()
    this.oPgFrm.Page1.oPag.oMATFIN_A_1_10.visible=!this.oPgFrm.Page1.oPag.oMATFIN_A_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_1'),i_cWhere,'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKARTI('M',.w_CODINI) AND ((empty(.w_codfin)) OR  (.w_codini <= .w_codfin)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o maggiore di quello finale oppure obsoleto")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_2'),i_cWhere,'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKARTI('M',.w_CODFIN) AND ((.w_codini <= .w_codfin) or (empty(.w_codfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o minore di quello iniziale oppure obsoleto")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI_M
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI_M) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI_M)+"%");
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);

          i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMKEYSAL,AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMKEYSAL',this.w_KEYSINI;
                     ,'AMCODICE',trim(this.w_MATINI_M))
          select AMKEYSAL,AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMKEYSAL,AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI_M)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI_M) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(oSource.parent,'oMATINI_M_1_7'),i_cWhere,'GSMD_AMT',"Elenco matricole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYSINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',oSource.xKey(1);
                       ,'AMCODICE',oSource.xKey(2))
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI_M)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI_M);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_KEYSINI;
                       ,'AMCODICE',this.w_MATINI_M)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI_M = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI_M = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI_M Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN_M
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN_M) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN_M)+"%");
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);

          i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMKEYSAL,AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMKEYSAL',this.w_KEYSINI;
                     ,'AMCODICE',trim(this.w_MATFIN_M))
          select AMKEYSAL,AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMKEYSAL,AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN_M)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN_M) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_M_1_8'),i_cWhere,'GSMD_AMT',"Elenco matricole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_KEYSINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',oSource.xKey(1);
                       ,'AMCODICE',oSource.xKey(2))
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN_M)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN_M);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_KEYSINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_KEYSINI;
                       ,'AMCODICE',this.w_MATFIN_M)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN_M = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN_M = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN_M Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATINI_A
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATINI_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATINI_A)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATINI_A))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATINI_A)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATINI_A) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATINI_A_1_9'),i_cWhere,'GSMD_AMT',"Elenco matricole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATINI_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATINI_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATINI_A)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATINI_A = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATINI_A = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATINI_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MATFIN_A
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MATFIN_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_MATFIN_A)+"%");

          i_ret=cp_SQL(i_nConn,"select AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMCODICE',trim(this.w_MATFIN_A))
          select AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MATFIN_A)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MATFIN_A) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMCODICE',cp_AbsName(oSource.parent,'oMATFIN_A_1_10'),i_cWhere,'GSMD_AMT',"Elenco matricole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',oSource.xKey(1))
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MATFIN_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MATFIN_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMCODICE',this.w_MATFIN_A)
            select AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MATFIN_A = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MATFIN_A = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MATFIN_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAINI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ACM',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CLAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CLAINI))
          select CMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAINI)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAINI) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oCLAINI_1_15'),i_cWhere,'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CLAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CLAINI)
            select CMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAINI = NVL(_Link_.CMCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CLAINI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAFIN
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ACM',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CLAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CLAFIN))
          select CMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAFIN)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAFIN) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oCLAFIN_1_16'),i_cWhere,'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CLAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CLAFIN)
            select CMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAFIN = NVL(_Link_.CMCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CLAFIN = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_1.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_1.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_2.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_2.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_3.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_3.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_M_1_7.value==this.w_MATINI_M)
      this.oPgFrm.Page1.oPag.oMATINI_M_1_7.value=this.w_MATINI_M
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_M_1_8.value==this.w_MATFIN_M)
      this.oPgFrm.Page1.oPag.oMATFIN_M_1_8.value=this.w_MATFIN_M
    endif
    if not(this.oPgFrm.Page1.oPag.oMATINI_A_1_9.value==this.w_MATINI_A)
      this.oPgFrm.Page1.oPag.oMATINI_A_1_9.value=this.w_MATINI_A
    endif
    if not(this.oPgFrm.Page1.oPag.oMATFIN_A_1_10.value==this.w_MATFIN_A)
      this.oPgFrm.Page1.oPag.oMATFIN_A_1_10.value=this.w_MATFIN_A
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_13.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_13.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_14.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_14.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAINI_1_15.value==this.w_CLAINI)
      this.oPgFrm.Page1.oPag.oCLAINI_1_15.value=this.w_CLAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAFIN_1_16.value==this.w_CLAFIN)
      this.oPgFrm.Page1.oPag.oCLAFIN_1_16.value=this.w_CLAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_20.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMsg_2_1.value==this.w_Msg)
      this.oPgFrm.Page2.oPag.oMsg_2_1.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKARTI('M',.w_CODINI) AND ((empty(.w_codfin)) OR  (.w_codini <= .w_codfin)) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o maggiore di quello finale oppure obsoleto")
          case   not(CHKARTI('M',.w_CODFIN) AND ((.w_codini <= .w_codfin) or (empty(.w_codfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o minore di quello iniziale oppure obsoleto")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_MATINI_M = this.w_MATINI_M
    this.o_MATFIN_M = this.w_MATFIN_M
    this.o_MATINI_A = this.w_MATINI_A
    this.o_MATFIN_A = this.w_MATFIN_A
    return

enddefine

* --- Define pages as container
define class tgsmd_kemPag1 as StdContainer
  Width  = 710
  height = 443
  stdWidth  = 710
  stdheight = 443
  resizeXpos=386
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_1 as StdField with uid="BKISJBXBBH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice di ricerca di inizio selezione",;
    HelpContextID = 96253914,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=97, Top=9, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_2 as StdField with uid="VJUFLTGZQW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o minore di quello iniziale oppure obsoleto",;
    ToolTipText = "Codice di ricerca di fine selezione",;
    HelpContextID = 17807322,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=97, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_3 as StdField with uid="ZJWJXASAJC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 96195018,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=253, Top=9, InputMask=replicate('X',40)

  add object oDESFIN_1_4 as StdField with uid="TLFKXEQUKI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17748426,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=253, Top=35, InputMask=replicate('X',40)

  add object oMATINI_M_1_7 as StdField with uid="XXLCJORMPW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MATINI_M", cQueryName = "MATINI_M",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 172243731,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=97, Top=62, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMKEYSAL", oKey_1_2="this.w_KEYSINI", oKey_2_1="AMCODICE", oKey_2_2="this.w_MATINI_M"

  func oMATINI_M_1_7.mHide()
    with this.Parent.oContained
      return (g_UNIMAT='M')
    endwith
  endfunc

  func oMATINI_M_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_M_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_M_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.MATRICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStrODBC(this.Parent.oContained.w_KEYSINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStr(this.Parent.oContained.w_KEYSINI)
    endif
    do cp_zoom with 'MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(this.parent,'oMATINI_M_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'',this.parent.oContained
  endproc
  proc oMATINI_M_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AMKEYSAL=w_KEYSINI
     i_obj.w_AMCODICE=this.parent.oContained.w_MATINI_M
     i_obj.ecpSave()
  endproc

  add object oMATFIN_M_1_8 as StdField with uid="SDTXDBTEZJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MATFIN_M", cQueryName = "MATFIN_M",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 250690323,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=97, Top=90, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMKEYSAL", oKey_1_2="this.w_KEYSINI", oKey_2_1="AMCODICE", oKey_2_2="this.w_MATFIN_M"

  func oMATFIN_M_1_8.mHide()
    with this.Parent.oContained
      return (g_UNIMAT='M')
    endwith
  endfunc

  func oMATFIN_M_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_M_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_M_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.MATRICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStrODBC(this.Parent.oContained.w_KEYSINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AMKEYSAL="+cp_ToStr(this.Parent.oContained.w_KEYSINI)
    endif
    do cp_zoom with 'MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(this.parent,'oMATFIN_M_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'',this.parent.oContained
  endproc
  proc oMATFIN_M_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AMKEYSAL=w_KEYSINI
     i_obj.w_AMCODICE=this.parent.oContained.w_MATFIN_M
     i_obj.ecpSave()
  endproc

  add object oMATINI_A_1_9 as StdField with uid="HMHORZBTBG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MATINI_A", cQueryName = "MATINI_A",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 172243719,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=97, Top=62, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATINI_A"

  func oMATINI_A_1_9.mHide()
    with this.Parent.oContained
      return (g_UNIMAT<>'M')
    endwith
  endfunc

  func oMATINI_A_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATINI_A_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATINI_A_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATINI_A_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'',this.parent.oContained
  endproc
  proc oMATINI_A_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AMCODICE=this.parent.oContained.w_MATINI_A
     i_obj.ecpSave()
  endproc

  add object oMATFIN_A_1_10 as StdField with uid="IEOUTLFKLG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MATFIN_A", cQueryName = "MATFIN_A",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Codice matricola",;
    HelpContextID = 250690311,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=97, Top=90, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSMD_AMT", oKey_1_1="AMCODICE", oKey_1_2="this.w_MATFIN_A"

  func oMATFIN_A_1_10.mHide()
    with this.Parent.oContained
      return (g_UNIMAT<>'M')
    endwith
  endfunc

  func oMATFIN_A_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMATFIN_A_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMATFIN_A_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MATRICOL','*','AMCODICE',cp_AbsName(this.parent,'oMATFIN_A_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_AMT',"Elenco matricole",'',this.parent.oContained
  endproc
  proc oMATFIN_A_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSMD_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AMCODICE=this.parent.oContained.w_MATFIN_A
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_13 as StdField with uid="HTBCCMIQFI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data creazione iniziale",;
    HelpContextID = 96191946,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=496, Top=62

  func oDATINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_14 as StdField with uid="YNDOCVMZZB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data creazione finale",;
    HelpContextID = 17745354,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=603, Top=62

  func oDATFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oCLAINI_1_15 as StdField with uid="RMKSCORDSN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CLAINI", cQueryName = "CLAINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Classe matricole iniziale",;
    HelpContextID = 96266970,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=496, Top=92, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CLAINI"

  func oCLAINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAINI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAINI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oCLAINI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this.parent.oContained
  endproc
  proc oCLAINI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CLAINI
     i_obj.ecpSave()
  endproc

  add object oCLAFIN_1_16 as StdField with uid="NFXHNEGSLZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLAFIN", cQueryName = "CLAFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Classe matricole finale",;
    HelpContextID = 17820378,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=603, Top=92, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CLAFIN"

  func oCLAFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAFIN_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAFIN_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oCLAFIN_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_ACM',"Classi matricole",'GSMDMACM.CMT_MAST_VZM',this.parent.oContained
  endproc
  proc oCLAFIN_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CLAFIN
     i_obj.ecpSave()
  endproc


  add object oBtn_1_19 as StdButton with uid="QYBPKEREZO",left=659, top=9, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 53248954;
    , caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        .NotifyEvent( "Esegui")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_20 as StdRadio with uid="IJDSJFZVFN",rtseq=13,rtrep=.f.,left=112, top=400, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 83902682
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 83902682
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe")
      StdRadio::init()
    endproc

  func oSELEZI_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_20.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_20.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_21 as StdButton with uid="CGPSEQDZQQ",left=250, top=396, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'eliminazione delle matricole selezionate";
    , HelpContextID = 249973434;
    , caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSMD_BEM(this.Parent.oContained,"ELIMINA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Zoom as cp_szoombox with uid="CZSVCBIDGT",left=2, top=137, width=705,height=257,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MATRICOL",cZoomFile="GSMD_KEM",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 156124186


  add object oBtn_1_35 as StdButton with uid="FRSAGQFIZA",left=659, top=396, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 210066502;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_37 as cp_runprogram with uid="RBSXKKZRNZ",left=500, top=456, width=207,height=27,;
    caption='GSMD_BEM',;
   bGlobalFont=.t.,;
    prg="GSMD_BEM('SELEZIONA')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 196158285

  add object oStr_1_5 as StdString with uid="CHNAEHJHSZ",Visible=.t., Left=25, Top=9,;
    Alignment=1, Width=67, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ZJNLTJMPGL",Visible=.t., Left=30, Top=35,;
    Alignment=1, Width=62, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="PJUNMWIQWM",Visible=.t., Left=13, Top=62,;
    Alignment=1, Width=79, Height=18,;
    Caption="Da matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JQKVBRBQER",Visible=.t., Left=21, Top=90,;
    Alignment=1, Width=71, Height=18,;
    Caption="a matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HOEVHHMKNU",Visible=.t., Left=394, Top=62,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da data crea.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="GXQGGCQDWL",Visible=.t., Left=576, Top=62,;
    Alignment=1, Width=19, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="XJOUEYAOIW",Visible=.t., Left=421, Top=90,;
    Alignment=1, Width=67, Height=18,;
    Caption="Da classe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ASGTINBSFC",Visible=.t., Left=577, Top=90,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HLJWCPSTKN",Visible=.t., Left=2, Top=450,;
    Alignment=0, Width=251, Height=18,;
    Caption="Variabili per lancio tracciabilit� (gsve_blt)"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine
define class tgsmd_kemPag2 as StdContainer
  Width  = 710
  height = 443
  stdWidth  = 710
  stdheight = 443
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_2_1 as StdMemo with uid="ATBBCHAJSX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 203201734,;
   bGlobalFont=.t.,;
    Height=385, Width=679, Left=10, Top=9, tabstop = .f., readonly = .t.


  add object oBtn_2_2 as StdButton with uid="CRCMRANPGI",left=640, top=398, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 210066502;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_kem','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
