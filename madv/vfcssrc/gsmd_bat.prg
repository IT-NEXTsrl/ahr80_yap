* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bat                                                        *
*              Attrubuzione unita logistica                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_81]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-22                                                      *
* Last revis.: 2004-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pACTION,pPARENT,pUL__SSCC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bat",oParentObject,m.pACTION,m.pPARENT,m.pUL__SSCC)
return(i_retval)

define class tgsmd_bat as StdBatch
  * --- Local variables
  pACTION = space(1)
  pPARENT = .NULL.
  pUL__SSCC = space(18)
  w_OBJECT = .NULL.
  w_CODART = space(20)
  w_CODICE = space(20)
  w_CODLOT = space(20)
  w_CODCOL = space(5)
  w_MONOART = .f.
  w_MONOLOT = .f.
  w_NOCODICE = .f.
  w_UL__SSCC = space(18)
  w_OBJ = .NULL.
  w_SERIALE = space(10)
  w_CPROWNUM = 0
  w_MVSERIAL = space(10)
  w_ANFLIMBA = space(1)
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_PESNET = 0
  w_UNIMIS = space(3)
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_CODART = space(20)
  w_DESART = space(40)
  w_CODICE = space(20)
  w_CDCONF = space(3)
  w_CODCOL = space(5)
  w_QTAMOV = 0
  w_CPROWORD = 0
  w_NUMCOL = 0
  * --- WorkFile variables
  KEY_ARTI_idx=0
  UNIT_LOG_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attribuzione Unit� Logistica (da GSMD_KAT)
    * --- Parametri
    *     pACTION  = 'S' -> Seleziona/Deseleziona
    *     pACTION  = 'A' -> Assegna pallet alle righe selezionate nello zoom di GSMD_KAT
    *     pACTION  = 'U' -> Assegna il nuovo pallet, creato precedentemente dal parametro N, alle righe selezionate, 
    *     pACTION  = 'N' -> Apro anagrafica pallet 
    *     pPARENT = GSMD_KAT (this o this.oParentObject.oParentObject)
    *     pUL__SSCC = Codice pallet
    *     pACTION  = 'D' -> Apro documento selezionato
    do case
      case this.pACTION="S"
        * --- Seleziona/Deseleziona righe dettaglio
        NC = this.oParentObject.w_ZOOMDET.cCursor
        UPDATE (NC) SET XCHK=IIF(this.oParentObject.w_SELEZI="S",1,0)
      case this.pACTION="A" OR this.pACTION="U" 
        * --- Assegna unit� logistica ai documenti selezionati
        if this.pACTION="A"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Nel caso in cui il batch sia stato lanciato da GSMD_AUL non eseguo
          *     il check righe di dettaglio
          NC = this.pPARENT.w_ZOOMDET.cCursor
          SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR APPO
        endif
        this.w_UL__SSCC = this.pUL__SSCC
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Verifico che il pallet selezionato sia congruente
        if CHECKSSCC("L",this.w_UL__SSCC,this.w_MONOART,this.w_MONOLOT,this.w_CODART,this.w_CODLOT, this.w_CODCOL)
          * --- Assegno alle righe selezionate il codice SSCC
          select APPO
          locate for 1=1
          do while not(eof())
          this.w_SERIALE = MVSERIAL
          this.w_CPROWNUM = MVROWNUM
          * --- Aggiorno il campo MVUNILOG delle righe del documento selezionato
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVUNILOG ="+cp_NullLink(cp_ToStrODBC(this.w_UL__SSCC),'DOC_DETT','MVUNILOG');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                MVUNILOG = this.w_UL__SSCC;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIALE;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          select APPO
          continue
          enddo
          * --- Visualizzo un messaggio di notifica
          ah_ErrorMsg("L'unit� logistica � stata assegnata alle righe selezionate",,"")
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Eseguo il refresh del cursore contenente il dettaglio dei documenti
        this.pPARENT.w_ZOOMDET.ReQuery()
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Sbianco il campo del codice SSCC per riportare la maschera
        *     nelle codizioni iniziali
        this.pPARENT.w_CODUNLOG = " "
      case this.pACTION="N"
        * --- Crea e assegna un nuova unit� logistica ai documenti selezionati
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Determino sulla base delle righe selezionate se il pallet � 
        *     mono-articolo e/o mono-lotto
        this.w_NOCODICE = .F.
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Determino il codice di ricerca se il pallet � mono-prodotto
        * --- Leggo il flag ANFLIMBA
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLIMBA"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLIMBA;
            from (i_cTable) where;
                ANCODICE = this.oParentObject.w_MVCODCON;
                and ANTIPCON = this.oParentObject.w_MVTIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANFLIMBA = NVL(cp_ToDate(_read_.ANFLIMBA),cp_NullValue(_read_.ANFLIMBA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_MONOART
          this.w_NOCODICE = .T.
          * --- Select from GSMD8KAT
          do vq_exec with 'GSMD8KAT',this,'_Curs_GSMD8KAT','',.f.,.t.
          if used('_Curs_GSMD8KAT')
            select _Curs_GSMD8KAT
            locate for 1=1
            do while not(eof())
            * --- Assegno i codici di ricerca di tipo 
            *     EAN 8, UPC A, EAN 13, EAN 14 se presenti
            this.w_CODICE = CACODICE
            * --- C'� un codice di ricerca adatto (EAN14, EAN13, EAN8 ,UPC A) per l'articolo 
            *     selezionato
            this.w_NOCODICE = .F.
              select _Curs_GSMD8KAT
              continue
            enddo
            use
          endif
        endif
        * --- Lancio l'Anagrafica Pallet
        this.w_OBJECT = GSMD_AUL()
        * --- Controllo se l'apertura della maschera � andata a buon fine
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Dico all'anagrafica che � stata lanciata da batch
        this.w_OBJECT.oParentObject = this
        * --- Entro in caricamento
        this.w_OBJECT.ecpLoad()     
        if this.w_NOCODICE
          * --- Il pallet � mono-prodotto ma non ho un codice di ricerca valido
          ah_ErrorMsg("Non � presente nessun codice di ricerca barcode (EAN 14, EAN 13, UPC A, EAN 8)%0per il prodotto selezionato","!","")
        else
          * --- Assegno il codice di ricerca
          this.w_OBJECT.w_ULCODICE = this.w_CODICE
          * --- Devo far scattare i link
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_ULCODICE")
          this.w_OBJ.bUpd = .t.
          this.w_OBJECT.SetControlsValue()     
          this.w_OBJ.Valid()     
          * --- Assegno il codice lotto
          this.w_OBJECT.w_ULCODLOT = this.w_CODLOT
          * --- Devo far scattare i link
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_ULCODLOT")
          this.w_OBJ.bUpd = .t.
          this.w_OBJECT.SetControlsValue()     
          this.w_OBJ.Valid()     
        endif
        this.w_OBJ = .null.
        this.w_OBJECT = .null.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pACTION="D"
        * --- Visualizza Documento selezionato
        * --- Discrimino tra Vendite e Acquisti
        if this.oParentObject.w_FLVEAC="V"
          this.w_OBJECT = GSVE_MDV(this.oParentObject.w_CATDOC)
        else
          this.w_OBJECT = GSAC_MDV(this.oParentObject.w_CATDOC)
        endif
        * --- Controllo se l'apertura della maschera � andata a buon fine
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il documento selezionato
        this.w_OBJECT.ecpFilter()     
        this.w_OBJECT.w_MVSERIAL = this.oParentObject.w_SERIAL
        this.w_OBJECT.ecpSave()     
      case this.pACTION="V"
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pACTION="R"
        * --- Avvio la ricerca
        this.oParentObject.NotifyEvent("Aggiorna")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pACTION="Q"
        * --- Chiudo il cursore CURTMP
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se ho selezionato almeno una riga e riverso il contenuto del 
    *     cursore dello zoom in un cursore di appoggio (APPO)
    NC = this.pPARENT.w_ZOOMDET.cCursor
    SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR APPO1
    if RECCOUNT("APPO1") = 0
      * --- Non ho selezionato alcuna riga
      ah_ErrorMsg("Selezionare almeno una riga di dettaglio","stop","")
      if used("APPO1")
        select APPO1
        use
      endif
      i_retcode = 'stop'
      return
    else
      * --- Controllo se sono state selezionate righe senza tipologia collo e/o con numero
      *     confenzioni a zero
      SELECT * FROM APPO1 WHERE EMPTY(NVL(MVCODCOL," ")) INTO CURSOR APPO
      if RECCOUNT("APPO") <> 0
        * --- Ho selezionato righe senza tipologia collo
        ah_ErrorMsg("Sono state selezionate righe di dettaglio senza tipologia collo","stop","")
        if used("APPO")
          select APPO
          use
        endif
        if used("APPO1")
          select APPO1
          use
        endif
        i_retcode = 'stop'
        return
      else
        * --- Verifico la presenza di confezioni a contenuto variabile che hanno numero
        *     confezioni a zero
        SELECT * FROM APPO1 WHERE NVL(MVNUMCOL,0) <= 0 AND TPFLCOVA = "S" INTO CURSOR APPO
        if RECCOUNT("APPO") <> 0
          ah_ErrorMsg("Riscontrati articoli con confezione a contenuto variabile%0e numero confezioni a zero%0� necessario selezionare righe di dettaglio con un numero%0di confezioni maggiore di zero","stop","")
          if used("APPO")
            select APPO
            use
          endif
          if used("APPO1")
            select APPO1
            use
          endif
          i_retcode = 'stop'
          return
        endif
        * --- Verifico la presenza di righe con numero confezioni a zero e a contenuto non variabile
        SELECT * FROM APPO1 WHERE NVL(MVNUMCOL,0) <= 0 INTO CURSOR APPO
        if RECCOUNT("APPO") <> 0
          * --- Ho selezionato righe con un numero confezioni = 0 quindi chiedo
          *     se si vuole calcolarli in automatico ed in caso di risposta negativa 
          *     elimino le righe con numero confezioni = 0 dal cursore
          if ah_YesNo("Riscontrati articoli con numero confezioni a zero: si desidera calcolarle in modo automatico?")
            * --- Ricalcolo il numero di confezioni e aggiorno il documento
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ripopolo il cirsore APPO poich� � stato chiuso dal batch GSVE_BC2
            SELECT * FROM APPO1 WHERE 1=1 INTO CURSOR APPO
          else
            * --- Elimino le righe con MVNUMCOL <= 0
            ah_ErrorMsg("Sar� assegnata l'unit� logistica alle sole righe di dettaglio%0con numero confezioni diverso da zero","!","")
            SELECT * FROM APPO1 WHERE NVL(MVNUMCOL,0) > 0 INTO CURSOR APPO
            if RECCOUNT("APPO") = 0
              * --- Non ho pi� alcuna riga
              ah_ErrorMsg("Tutte le righe di dettaglio selezionate hanno numero confezioni uguale a zero","stop","")
              if used("APPO")
                select APPO
                use
              endif
              if used("APPO1")
                select APPO1
                use
              endif
              i_retcode = 'stop'
              return
            endif
          endif
        else
          * --- Tutto ok
          SELECT * FROM APPO1 WHERE 1=1 INTO CURSOR APPO
        endif
        * --- Chiudo il cursore di appoggio
        if used("APPO1")
          select APPO1
          use
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("APPO")
       
 select APPO 
 use
    endif
    if used("APPO1")
       
 select APPO1 
 use
    endif
    if used("CURTMP")
       
 select CURTMP 
 use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se le righe selezionate possono essere attribuite ad un pallet 
    *     mono-prodotto (e mono-lotto)
    * --- Verifico se il pallet � mono-prodotto e mono-lotto
    select distinct MVCODART, MVCODLOT from APPO into cursor APPO1
    if reccount()=1
      * --- Mono-Prodotto (e Mono-Lotto)
      this.w_MONOART = .T.
      this.w_CODART = MVCODART
      * --- Se MVCODLOT � vuoto allora il pallet � solo mono-prodotto
      this.w_MONOLOT = IIF(EMPTY(NVL(MVCODLOT," ")), .F., .T.)
      this.w_CODLOT = MVCODLOT
      select distinct MVCODART, MVCODLOT, MVCODCOL from APPO into cursor APPO1
      if reccount() > 1
        this.w_CODCOL = SPACE(5)
      else
        this.w_CODCOL = MVCODCOL
      endif
    else
      * --- Verifico se il pallet � mono-prodotto
      select distinct MVCODART from APPO into cursor APPO1
      if reccount()=1
        * --- Mono-Prodotto
        this.w_MONOART = .T.
        this.w_MONOLOT = .F.
        this.w_CODART = MVCODART
        select distinct MVCODART, MVCODCOL from APPO into cursor APPO1
        if reccount() > 1
          this.w_CODCOL = SPACE(5)
        else
          this.w_CODCOL = MVCODCOL
        endif
      else
        * --- Pluri-Prodotto e Pluri-Lotto
        this.w_MONOART = .F.
        this.w_MONOLOT = .F.
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza/Nasconde righe con pallet gi� assegnato
    NC = this.pPARENT.w_ZOOMDET.cCursor
    if not used("CURTMP")
      select * from (NC) into cursor CURTMP
    endif
    if this.pPARENT.w_FLGHDUN="S"
      select * from CURTMP into cursor APPO
    else
      select * from CURTMP where empty(nvl(MVUNILOG," ")) into cursor APPO
    endif
    * --- Aggiorno il cursore dello zoom con i dati contenuti in APPO
    Zap in (NC)
    Select APPO
    Go Top
    Scan
    Scatter Memvar
    Select (NC)
    Append blank
    Gather memvar
    Endscan
    Select APPO
    Use
    Select (NC)
    Go Top
    this.pPARENT.w_ZOOMDET.Refresh()
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili necessarie al batch GSVE_BC2
    * --- Creo il cursore di appoggio per cos� poter chiamare il batch GSVE_BC2
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVUNIMIS C(3), ;
    t_MVQTAMOV N(12,3), ;
    t_MVPESNET N(9,3), ;
    t_UNMIS1 C(3), t_UNMIS2 C(3), t_UNMIS3 C(3), t_CPROWORD N(5), CPROWNUM N(4), t_CODCONF C(3), t_MVNUMCOL N(5), t_MVCODCOL C(5))
    * --- Riempio il cursore con il cursore di appoggio
    SELECT APPO
    GO TOP
    SCAN
    * --- Valorizzo le variabili che andr� ad inserire
    this.w_SERIALE = MVSERIAL
    this.w_CPROWNUM = MVROWNUM
    this.w_PESNET = NVL(MVPESNET,0)
    this.w_UNIMIS = NVL(MVUNIMIS, "   ")
    this.w_UM1 = NVL(UNMIS1,"   ")
    this.w_UM2 = NVL(UNMIS2,"   ")
    this.w_UM3 = NVL(UNMIS3, "   ")
    this.w_CODART = NVL(MVCODART, " ")
    this.w_DESART = NVL(MVDESART,SPACE(40))
    this.w_CODICE = NVL(MVCODICE, "   ")
    this.w_CDCONF = NVL(CODCONF,SPACE(3))
    this.w_CODCOL = NVL(MVCODCOL,SPACE(5))
    this.w_QTAMOV = NVL(MVQTAMOV,0)
    this.w_CPROWORD = CPROWORD
    this.w_NUMCOL = NVL(MVNUMCOL,0)
    * --- Eseguo l'insert
    INSERT INTO GeneApp ;
    (t_MVTIPRIG, t_MVCODICE, t_MVCODART, t_MVDESART, t_MVUNIMIS, t_MVQTAMOV, t_MVPESNET, ;
    t_UNMIS1, t_UNMIS2, t_UNMIS3, t_CPROWORD, CPROWNUM, t_CODCONF, t_MVNUMCOL, t_MVCODCOL) ;
    VALUES ("R", this.w_CODICE, this.w_CODART, this.w_DESART, this.w_UNIMIS, this.w_QTAMOV, this.w_PESNET, ;
    this.w_UM1, this.w_UM2, this.w_UM3, this.w_CPROWORD, this.w_CPROWNUM, this.w_CDCONF, this.w_NUMCOL , this.w_CODCOL)
    SELECT APPO
    ENDSCAN
    * --- Lancio il batch che calcola i colli, il cursore GENEAPP conterr� nella colonna
    *     t_MVNUMCOL il numero di confezioni corretto
    GSVE_BC2(this,"V")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorno la testata del documento con i valori calcolati da GSVE_BC2
    *     (w_SERIAL � stato valorizzato dalla scan precedente)
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVQTACOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTACOL),'DOC_MAST','MVQTACOL');
      +",MVQTAPES ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAPES),'DOC_MAST','MVQTAPES');
      +",MVQTALOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTALOR),'DOC_MAST','MVQTALOR');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVQTACOL = this.w_MVQTACOL;
          ,MVQTAPES = this.w_MVQTAPES;
          ,MVQTALOR = this.w_MVQTALOR;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno il dettaglio del documento con il valore di NUMCOL calcolato
    *      da GSVE_BC2
    SELECT GENEAPP
    GO TOP
    SCAN
    this.w_CPROWNUM = CPROWNUM
    this.w_NUMCOL = NVL(t_MVNUMCOL,0)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVNUMCOL ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCOL),'DOC_DETT','MVNUMCOL');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          MVNUMCOL = this.w_NUMCOL;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_SERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    * --- Chiudo il cursore GENEAPP
    if used("GENEAPP")
      select GENEAPP
      use
    endif
  endproc


  proc Init(oParentObject,pACTION,pPARENT,pUL__SSCC)
    this.pACTION=pACTION
    this.pPARENT=pPARENT
    this.pUL__SSCC=pUL__SSCC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='UNIT_LOG'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='CONTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSMD8KAT')
      use in _Curs_GSMD8KAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pACTION,pPARENT,pUL__SSCC"
endproc
