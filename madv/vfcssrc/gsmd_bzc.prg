* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bzc                                                        *
*              Lancia classi lotti\matricole                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2010-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bzc",oParentObject)
return(i_retval)

define class tgsmd_bzc as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_TIPCLA = space(1)
  w_DXBTN = .f.
  w_CODICE = space(15)
  * --- WorkFile variables
  CMT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Le Classi Lotti\Matricole da GSMA_AAR
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      * --- Read from CMT_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CMT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CMT_MAST_idx,2],.t.,this.CMT_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMTIPCLA"+;
          " from "+i_cTable+" CMT_MAST where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMTIPCLA;
          from (i_cTable) where;
              CMCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPCLA = NVL(cp_ToDate(_read_.CMTIPCLA),cp_NullValue(_read_.CMTIPCLA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(NVL(this.w_TIPCLA,""))
        * --- NEL CASO IN CUI LA VARIABILE E' VUOTA SI USA IL TOOLTIP PER RISALIRE ALLA CLASSE
        do case
          case G_oMENU.oCTRL.ToolTipText = ah_Msgformat("Classe matricola")
            this.w_TIPCLA = "M"
          case G_oMENU.oCTRL.ToolTipText = ah_Msgformat("Classe lotto")
            this.w_TIPCLA = "L"
        endcase
      endif
    else
      * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      this.w_TIPCLA = &cCurs..CMTIPCLA
      if EMPTY(NVL(this.w_TIPCLA,""))
        * --- NEL CASO IN CUI LO ZOOM E' VUOTO SI USA IL TITOLO PER RISALIRE ALLA CLASSE
        do case
          case upper(i_curform.caption) = ah_Msgformat("CLASSI MATRICOLE")
            this.w_TIPCLA = "M"
          case upper(i_curform.caption) = ah_Msgformat("CLASSI LOTTI")
            this.w_TIPCLA = "L"
        endcase
      endif
    endif
    this.w_PROG = GSMD_ACM(this.w_TIPCLA)
    * --- Controllo se ha passato il test di accesso
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_CMCODICE = this.w_CODICE
        this.w_PROG.w_CMTIPCLA = this.w_TIPCLA
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CMT_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
