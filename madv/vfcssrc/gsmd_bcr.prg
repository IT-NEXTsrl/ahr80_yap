* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bcr                                                        *
*              Caricamento rapido matricole                                    *
*                                                                              *
*      Author: Zucchetti S.p.a                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_365]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-23                                                      *
* Last revis.: 2015-12-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bcr",oParentObject,m.pOPER)
return(i_retval)

define class tgsmd_bcr as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_TOTSEL = .f.
  w_PUNPAD = .NULL.
  w_CODMAT = space(40)
  w_MESS = space(10)
  w_RECPOS = 0
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_APPO = space(10)
  w_MTRIFNUM = 0
  w_CHKCODLOT = .f.
  w_CHKCODUBI = .f.
  w_APCONTA = 0
  w_PIUMAT = 0
  w_PRODUZ = .f.
  w_MAGAZ = space(5)
  w_CODCOM = space(15)
  w_TMPCODCOM = space(15)
  w_CRSERIAL = space(10)
  w_CRROWNUM = 0
  w_CRNUMRIF = 0
  w_CRSERRIF = space(10)
  w_CRROWRIF = 0
  w_CRRIFNUM = 0
  w_CRMAGCAR = space(5)
  w_CRMAGSCA = space(5)
  w_CRFLCARI = space(1)
  w_CRFLSCAR = space(1)
  w_CRKEYSAL = space(20)
  w_CRCODUBI = space(20)
  w_CR__FLAG = space(1)
  w_CR_SALDO = 0
  w_CRCODMAT = space(40)
  w_CRCODLOT = space(20)
  w_TMPCODLOT = space(20)
  w_TMPCODUBI = space(20)
  w_TMPCODCOM = space(15)
  w_oMess = .NULL.
  * --- WorkFile variables
  MOVIMATR_idx=0
  MATRICOL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Caricamento Rapido Matricole da GSMD_KCR
    this.w_PUNPAD = this.oParentObject
    NS = this.w_PUNPAD.w_ZoomSel.cCursor
    NC = this.w_PUNPAD.w_ZoomCar.cCursor
    this.w_PRODUZ = not empty(this.oParentObject.w_MAGSCA) and this.oParentObject.w_QTASC1>0
    do case
      case this.pOPER="R"
        * --- Eseguo la Ricerca
        *     La struttura dello Zoom � definita dalla query gsmd_kcr avente come filtro 1=0,
        *     poi a seconda del tipo Movimento (Carico, Scarico) avr� gsmd_zmc, gsmd_zms 
        *     
        *     Se il magazzino di riferimento � quello di carico e non st� trasferendo tra lo stesso magazznio
        if this.oParentObject.w_CODMAG=this.oParentObject.w_MTMAGCAR And this.oParentObject.w_MTMAGCAR<>this.oParentObject.w_MTMAGSCA
          * --- Movimento di Carico
          this.w_PUNPAD.w_ZoomSel.cCpQueryName = "..\MADV\EXE\QUERY\GSMD1ZMC"
          ah_Msg("Lettura matricole...",.T.)
          this.w_PUNPAD.NotifyEvent("Esegui")     
          * --- Movimento di Carico: nel Primo Zoom con Selezione � visibile solo il Codice
          *     della Matricola
          FOR I=1 TO this.w_PUNPAD.w_ZoomSel.grd.ColumnCount
          AA = ALLTRIM(STR(I))
          this.w_APPO = UPPER(this.w_PUNPAD.w_ZoomSel.grd.Column&AA..ControlSource)
          do case
            case this.w_APPO="CODICE"
              this.w_PUNPAD.w_ZoomSel.grd.Column&AA..Width = 700
            case !(this.w_APPO $ "XCHK-CODICE")
              this.w_PUNPAD.w_ZoomSel.grd.Column&AA..Width = 0
          endcase
          ENDFOR
        else
          * --- Movimento di Scarico
          this.w_PUNPAD.w_ZoomSel.cCpQueryName = "..\MADV\EXE\QUERY\GSMDZZMS"
          ah_Msg("Lettura matricole...",.T.)
          this.w_PUNPAD.NotifyEvent("Esegui")     
        endif
        Select (NC)
        if RecCount(NC)>0
          zap
        endif
        this.oParentObject.w_CONTA = 0
        this.oParentObject.w_CONTACAR = 0
        this.oParentObject.w_NPROG = iif(this.w_PRODUZ and this.oParentObject.w_QTACA1>0, this.oParentObject.w_QTACA1, this.oParentObject.w_QTAUM1)
      case this.pOPER="G"
        * --- Chiama la Maschera di Caricamento Matricole
        do GSMD_KCM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Movimento di Carico
        this.w_PUNPAD.w_ZoomSel.cCpQueryName = "..\MADV\EXE\QUERY\GSMD1ZMC"
        this.w_PUNPAD.NotifyEvent("Esegui")     
        * --- Movimento di Carico: nel Primo Zoom con Selezione � visibile solo il Codice
        *     della Matricola
        FOR I=1 TO this.w_PUNPAD.w_ZoomSel.grd.ColumnCount
        AA = ALLTRIM(STR(I))
        this.w_APPO = UPPER(this.w_PUNPAD.w_ZoomSel.grd.Column&AA..ControlSource)
        do case
          case this.w_APPO="CODICE"
            this.w_PUNPAD.w_ZoomSel.grd.Column&AA..Width = 700
          case !(this.w_APPO $ "XCHK-CODICE")
            this.w_PUNPAD.w_ZoomSel.grd.Column&AA..Width = 0
        endcase
        ENDFOR
      case this.pOPER="C"
        * --- Seleziono nel Primo Zoom una Matricola.
        *     Se nel  Cusore del secondo Zoom � presente un numero di record 
        *     uguale alla quantit� movimentata non posso pi� selezionare nel primo zoom
        this.oParentObject.w_CONTA = this.oParentObject.w_CONTA+1
        * --- Controlli dopo aver selezionato la Matricola nel primo Zoom
        this.w_TOTSEL = this.oParentObject.w_CONTA>this.oParentObject.w_QTAUM1
        * --- Controlli Lotto ed Ubicazione: solo se Movimento di Carico
        * --- Lotto
        *     In caso di trasferimento il lotto non � obbligatorio, la procedura usa quello
        *     dello Zoom se non specificato in alto
        *     
        *     Ubicazione
        *     In caso di trasferimento l'ubicazione � obbligatoria se cambia il magazzino.
        *     Altrimenti la procedura prende quella dello zoom se non specificata quella 
        *     in testata
        if this.w_PRODUZ
          * --- Dichiarazioni di Produzione con Magazzino scarti
          this.w_CHKCODLOT = g_PERLOT="S" AND this.oParentObject.w_FLLOTT="S" AND Empty(nvl(this.oParentObject.w_MTCODLOT,space(20))) And Empty( this.oParentObject.w_MTMAGSCA ) 
          if this.oParentObject.w_CONTACAR<this.oParentObject.w_QTACA1
            this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICAR="S" and Empty(nvl(this.oParentObject.w_CODUBI,space(20)))
          else
            this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICA2="S" and Empty(nvl(this.oParentObject.w_CODUBISCA,space(20)))
          endif
        else
          if ( this.oParentObject.w_CODMAG=this.oParentObject.w_MTMAGCAR OR Not Empty(this.oParentObject.w_MTMAGCAR) ) And (this.w_PUNPAD.w_MTFLSCAR="E" Or this.w_PUNPAD.w_MTFLCARI="E")
            this.w_CHKCODLOT = g_PERLOT="S" AND this.oParentObject.w_FLLOTT="S" AND Empty(nvl(this.oParentObject.w_MTCODLOT,space(20))) And Empty( this.oParentObject.w_MTMAGSCA ) 
            this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICAR="S" and Empty(nvl(this.oParentObject.w_CODUBI,space(20))) and this.oParentObject.w_MTMAGSCA<>this.oParentObject.w_MTMAGCAR
          endif
        endif
        if this.w_TOTSEL OR this.w_CHKCODLOT OR this.w_CHKCODUBI
          Select (NS)
          this.w_RECPOS = RECNO()
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          AA=ALLTRIM(STR(this.w_PUNPAD.w_ZoomSel.grd.ColumnCount))
          this.w_PUNPAD.w_ZoomSel.grd.Column&AA..chk.Value = 0
          GOTO this.w_RECPOS
          this.w_PUNPAD.w_ZoomSel.grd.refresh
          this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
        else
          Select (NS)
          this.w_CODMAT = Codice
          this.w_TMPCODCOM = nvl(codcom, Space(15))
          this.w_MTSERRIF = nvl(CHIAVE, Space(10))
          this.w_MTROWRIF = nvl(NUMROW,0)
          this.w_MTRIFNUM = nvl(NUMRIF, 0)
          * --- Se Movimento di Scarico oppure di trasferimento e non specifico il lotto di testata prendo il lotto dal primo Zoom
          *     Prendo dallo zoom anche se movimento il riservato
          if (( this.oParentObject.w_CODMAG <> this.oParentObject.w_MTMAGCAR AND Empty(this.oParentObject.w_MTMAGCAR) )Or ( not Empty(this.oParentObject.w_MTMAGSCA) and not Empty(this.oParentObject.w_MTMAGCAR) And empty( this.oParentObject.w_MTCODLOT ) )) Or (this.w_PUNPAD.w_MTFLSCAR="R" Or this.w_PUNPAD.w_MTFLCARI="R")
            this.w_TMPCODLOT = NVL(CODLOT,Space(20))
          else
            this.w_TMPCODLOT = this.oParentObject.w_MTCODLOT
          endif
          * --- Se Movimento di Scarico oppure di trasferimento tra magazzini uguali e non specifico l'ubicazione di testata prendo l'ubicazione dal primo Zoom
          if (( this.oParentObject.w_CODMAG <> this.oParentObject.w_MTMAGCAR AND Empty(this.oParentObject.w_MTMAGCAR) )Or ( not Empty(this.oParentObject.w_MTMAGSCA) and not Empty(this.oParentObject.w_MTMAGCAR) And this.oParentObject.w_MTMAGSCA=this.oParentObject.w_MTMAGCAR And empty( this.oParentObject.w_CODUBI ) )) Or (this.w_PUNPAD.w_MTFLSCAR="R" Or this.w_PUNPAD.w_MTFLCARI="R")
            this.w_TMPCODUBI = NVL(CODUBI,Space(20))
          else
            this.w_TMPCODUBI = iif(this.w_PRODUZ and this.oParentObject.w_CONTACAR>=this.oParentObject.w_QTACA1, this.oParentObject.w_CODUBISCA, this.oParentObject.w_CODUBI)
          endif
          this.w_MAGAZ = this.oParentObject.w_MTMAGCAR
          if this.w_PRODUZ
            this.w_MAGAZ = iif(this.oParentObject.w_CONTACAR>=this.oParentObject.w_QTACA1, this.oParentObject.w_MAGSCA, this.oParentObject.w_CODMAG)
          endif
          Select (NC)
          Calculate CNT() FOR MTCODMAT=this.w_CODMAT to this.w_PIUMAT
          * --- Verifico se Matricola gi� selezionata 
          if this.w_PIUMAT = 1
            this.w_MESS = "Matricola gi� selezionata"
            ah_ErrorMsg(this.w_MESS,"!","")
            this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
          else
            insert into (NC) values (this.w_CODMAT,this.w_TMPCODLOT,this.w_TMPCODUBI,this.w_MTSERRIF,this.w_MTROWRIF,this.w_MTRIFNUM, this.w_MAGAZ, this.oParentObject.w_MTKEYSAL, this.w_CODMAT,this.w_TMPCODCOM)
            this.oParentObject.w_CONTACAR = this.oParentObject.w_CONTACAR+1
            if this.w_PRODUZ and this.oParentObject.w_CONTACAR=this.oParentObject.w_QTACA1 and this.oParentObject.w_QTACA1>0 and this.oParentObject.w_CONTA<this.oParentObject.w_QTAUM1
              this.w_MESS = "Caricamento matricole su magazzino %1 completato%0Si caricheranno ora le matricole sul magazzino scarti %2"
              ah_ErrorMsg(this.w_MESS,"I","", this.oParentObject.w_CODMAG, this.oParentObject.w_MAGSCA)
            endif
            this.oParentObject.w_CONTACAR = min(this.oParentObject.w_QTACA1, this.oParentObject.w_CONTACAR)
            this.w_PUNPAD.w_ZoomCar.grd.refresh
          endif
        endif
        this.oParentObject.w_NPROG = this.oParentObject.w_QTAUM1- this.oParentObject.w_CONTA
      case this.pOPER="U"
        * --- Deseleziona riga primo Zoom:decremento Contatore Matricole Selezionat
        this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
        * --- Aggiorno direttamente il campo XCHK del cursore (NS), primo zoom, 
        *      in quanto in alcuni casi  con Uncheck non si verifica l'aggiornamento a 0 di tale campo 
        SELECT (NS)
        REPLACE XCHK WITH 0
        * --- Memorizzo Codice Matricola
        this.w_CODMAT = Codice
        * --- Cancello dal Secondo Zoom la Riga Deselezionata nel Primo Zoom
        SELECT (NC)
        LOCATE FOR MTCODMAT=this.w_CODMAT
        if this.w_PRODUZ AND MTMAGCAR=this.oParentObject.w_CODMAG
          if this.w_PRODUZ and this.oParentObject.w_CONTACAR=this.oParentObject.w_QTACA1 and this.oParentObject.w_QTACA1>0
            this.w_MESS = "Le prossime matricole saranno caricare sul magazzino"
            ah_ErrorMsg(this.w_MESS,"I","", this.oParentObject.w_CODMAG)
          endif
          this.oParentObject.w_CONTACAR = max(this.oParentObject.w_CONTACAR-1,0)
        endif
        DELETE
        GO TOP
        * --- Refresh della griglia
        this.w_PUNPAD.w_ZoomCar.grd.refresh
        this.oParentObject.w_NPROG = this.oParentObject.w_QTAUM1- this.oParentObject.w_CONTA
      case this.pOPER="L"
        Select (NC)
        GO TOP
        Select * from (NC) into Cursor Carica Order by mtcodmat NoFilter
        if Upper( this.w_PUNPAD.oParentObject.oParentObject.Class )="TGSMD_BMU"
          * --- Scrivo direttamente sul database
          this.w_CRSERIAL = this.w_PUNPAD.oParentObject.oParentObject.oParentObject.w_SERIALE
          this.w_CRROWNUM = this.w_PUNPAD.oParentObject.oParentObject.oParentObject.w_NUMROW
          this.w_CRNUMRIF = this.w_PUNPAD.oParentObject.oParentObject.oParentObject.w_NUMRIF
          this.w_CRMAGCAR = this.w_PUNPAD.oParentObject.oParentObject.w_MTMAGCAR
          this.w_CRMAGSCA = this.w_PUNPAD.oParentObject.oParentObject.w_MTMAGSCA
          this.w_CRFLCARI = this.w_PUNPAD.oParentObject.oParentObject.w_MTFLCARI
          this.w_CRFLSCAR = this.w_PUNPAD.oParentObject.oParentObject.w_MTFLSCAR
          this.w_CRKEYSAL = this.w_PUNPAD.oParentObject.oParentObject.w_MTKEYSAL
          * --- Try
          local bErr_04182CA8
          bErr_04182CA8=bTrsErr
          this.Try_04182CA8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            if Empty( i_ErrMsg )
              ah_ErrorMsg("Errore aggiornamento dettaglio matricole: %1","!","", message() )
            else
              ah_ErrorMsg("Errore aggiornamento dettaglio matricole: %1","!","", i_ErrMsg)
            endif
          endif
          bTrsErr=bTrsErr or bErr_04182CA8
          * --- End
        else
          * --- Riempio il dettaglio di GSVE_MMT
          this.w_PUNPAD.oParentObject.oParentObject.NotifyEvent("CarMat")
        endif
        if type("this.w_PUNPAD.class")="C" and upper(this.w_PUNPAD.class)="TGSMD_KCR"
          this.w_PUNPAD.EcpQuit()
        endif
      case this.pOPER="S"
        * --- Se Primo Zoom con Seleziono Vuoto non faccio Niente
        if RECCOUNT(NS) >0
          if this.oParentObject.w_NPROG >this.oParentObject.w_QTAUM1-this.oParentObject.w_CONTA
            this.w_MESS = "Il numero di matricole selezionate maggiore del numero matricole disponibili"
            ah_ErrorMsg(this.w_MESS,"!","")
            i_retcode = 'stop'
            return
          endif
          this.w_TOTSEL = this.oParentObject.w_CONTA=this.oParentObject.w_QTAUM1
          * --- Controlli Lotto ed Ubicazione: solo se Movimento di Carico
          * --- Lotto
          *     In caso di trasferimento il lotto non � obbligatorio, la procedura usa quello
          *     dello Zoom se non specificato in alto
          *     
          *     Ubicazione
          *     In caso di trasferimento l'ubicazione � obbligatoria se cambia il magazzino.
          *     Altrimenti la procedura prende quella dello zoom se non specificata quella 
          *     in testata
          if ( this.oParentObject.w_CODMAG=this.oParentObject.w_MTMAGCAR OR Not Empty(this.oParentObject.w_MTMAGCAR) ) And (this.w_PUNPAD.w_MTFLSCAR="E" Or this.w_PUNPAD.w_MTFLCARI="E")
            this.w_CHKCODLOT = g_PERLOT="S" AND this.oParentObject.w_FLLOTT="S" AND Empty(nvl(this.oParentObject.w_MTCODLOT,space(20))) And Empty( this.oParentObject.w_MTMAGSCA ) 
            this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICAR="S" and Empty(nvl(this.oParentObject.w_CODUBI,space(20))) and this.oParentObject.w_MTMAGSCA<>this.oParentObject.w_MTMAGCAR
          endif
          if this.w_TOTSEL OR this.w_CHKCODLOT OR this.w_CHKCODUBI
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          * --- Seleziono fino ad un massimo pari alla quantit�
          this.w_APCONTA = 0
          SELECT (NS)
          GO TOP
          do while not eof(NS)
            if XCHK=0 and this.w_APCONTA < this.oParentObject.w_NPROG
              REPLACE XCHK WITH 1
              this.w_APCONTA = this.w_APCONTA+1
              this.oParentObject.w_CONTA = this.oParentObject.w_CONTA+1
              this.w_CODMAT = Codice
              this.w_MTSERRIF = nvl(CHIAVE, Space(10))
              this.w_MTROWRIF = nvl(NUMROW,0)
              this.w_MTRIFNUM = nvl(NUMRIF, 0)
              * --- Se Movimento di Scarico oppure di trasferimento e non specifico il lotto di testata prendo il lotto dal primo Zoom
              *     Prendo dallo zoom anche se movimento il riservato
              if (( this.oParentObject.w_CODMAG <> this.oParentObject.w_MTMAGCAR AND Empty(this.oParentObject.w_MTMAGCAR) )Or ( not Empty(this.oParentObject.w_MTMAGSCA) and not Empty(this.oParentObject.w_MTMAGCAR) And empty( this.oParentObject.w_MTCODLOT ) )) Or (this.w_PUNPAD.w_MTFLSCAR="R" Or this.w_PUNPAD.w_MTFLCARI="R")
                this.w_TMPCODLOT = NVL(CODLOT,Space(20))
              else
                this.w_TMPCODLOT = this.oParentObject.w_MTCODLOT
              endif
              * --- Se Movimento di Scarico oppure di trasferimento tra magazzini uguali e non specifico l'ubicazione di testata prendo l'ubicazione dal primo Zoom
              if (( this.oParentObject.w_CODMAG <> this.oParentObject.w_MTMAGCAR AND Empty(this.oParentObject.w_MTMAGCAR) )Or ( not Empty(this.oParentObject.w_MTMAGSCA) and not Empty(this.oParentObject.w_MTMAGCAR) And this.oParentObject.w_MTMAGSCA=this.oParentObject.w_MTMAGCAR And empty( this.oParentObject.w_CODUBI ) )) Or (this.w_PUNPAD.w_MTFLSCAR="R" Or this.w_PUNPAD.w_MTFLCARI="R")
                this.w_TMPCODUBI = NVL(CODUBI,Space(20))
              else
                this.w_TMPCODUBI = iif(this.w_PRODUZ and this.oParentObject.w_CONTACAR>=this.oParentObject.w_QTACA1, this.oParentObject.w_CODUBISCA, this.oParentObject.w_CODUBI)
              endif
              this.w_TMPCODCOM = nvl(codcom, Space(15))
              this.w_MAGAZ = this.oParentObject.w_MTMAGCAR
              if this.w_PRODUZ
                this.w_MAGAZ = iif(this.oParentObject.w_CONTACAR>=this.oParentObject.w_QTACA1, this.oParentObject.w_MAGSCA, this.oParentObject.w_CODMAG)
              endif
              * --- Se la Matricola � gi� stata selezionata non viene riportata nel secondo zoom 
              Select (NC)
              Calculate CNT() FOR MTCODMAT=this.w_CODMAT to this.w_PIUMAT
              if this.w_PIUMAT = 1
                this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
                this.w_APCONTA = this.w_APCONTA-1
              else
                if this.w_PRODUZ
                  * --- Dichiarazioni di Produzione con Magazzino scarti
                  if this.oParentObject.w_CONTACAR<this.oParentObject.w_QTACA1
                    this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICAR="S" and Empty(nvl(this.oParentObject.w_CODUBI,space(20)))
                  else
                    this.w_CHKCODUBI = g_PERUBI="S" AND this.oParentObject.w_MGUBICA2="S" and Empty(nvl(this.oParentObject.w_CODUBISCA,space(20)))
                  endif
                endif
                if this.w_CHKCODUBI
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.oParentObject.w_CONTA = this.oParentObject.w_CONTA-1
                  SELECT (NS)
                  REPLACE XCHK WITH 0
                  GO BOTTOM
                else
                  insert into (NC) values (this.w_CODMAT,this.w_TMPCODLOT,this.w_TMPCODUBI,this.w_MTSERRIF,this.w_MTROWRIF,this.w_MTRIFNUM,this.w_MAGAZ,this.oParentObject.w_MTKEYSAL, this.w_CODMAT,this.w_TMPCODCOM)
                  this.oParentObject.w_CONTACAR = this.oParentObject.w_CONTACAR+1
                  if this.w_PRODUZ and this.oParentObject.w_CONTACAR=this.oParentObject.w_QTACA1 and this.oParentObject.w_QTACA1>0 and this.oParentObject.w_CONTA<this.oParentObject.w_QTAUM1
                    this.w_MESS = "Caricamento matricole su magazzino %1 completato%0Si caricheranno ora le matricole sul magazzino scarti %2"
                    ah_ErrorMsg(this.w_MESS,"I","", this.oParentObject.w_CODMAG, this.oParentObject.w_MAGSCA )
                  endif
                  this.oParentObject.w_CONTACAR = min(this.oParentObject.w_QTACA1, this.oParentObject.w_CONTACAR)
                endif
              endif
            endif
            SELECT (NS) 
 Skip
          enddo
          * --- Refresh della griglia
          this.w_PUNPAD.w_ZoomCar.grd.refresh
          * --- Si riposiziona all'inizio
          SELECT (NS)
          GO TOP
          this.w_PUNPAD.w_ZoomSel.grd.refresh
          if this.w_APCONTA=0
            * --- Se l'utente tenta di selezionare matricole e lo zoom in alto � gia tutto selezionato
            ah_ErrorMsg("Per le selezioni impostate non esistono ulteriori matricole selezionabili","!","")
          endif
          this.oParentObject.w_NPROG = this.oParentObject.w_QTAUM1- this.oParentObject.w_CONTA
        endif
      case this.pOPER="D"
        * --- Se Zoom Matricole Selezionate non vuoto -->messaggio di Warning
        if this.oParentObject.w_CONTA>0 
          this.w_MESS = "La matricole selezionate saranno eliminate dalla visualizzazione%0Vuoi confermare?"
          if NOT ah_YesNo(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Deseleziona tutto nel Primo Zoom
        UPDATE (NS) SET XCHK=0 
        * --- Svuoto il Cursore associato al secondo Zoom
        SELECT (NC)
        Zap
        this.w_PUNPAD.w_ZoomCar.grd.refresh
        SELECT (NS)
        GO TOP
        this.w_PUNPAD.w_ZoomSel.grd.refresh
        * --- -Azzero il Contatore
        this.oParentObject.w_CONTA = 0
        this.oParentObject.w_CONTACAR = 0
        this.oParentObject.w_NPROG = iif(this.w_PRODUZ and this.oParentObject.w_QTACA1>0, this.oParentObject.w_QTACA1, this.oParentObject.w_QTAUM1)
    endcase
    if USED("CurMat")
      SELECT CurMat
      USE
    endif
    if USED("Carica")
      SELECT Carica
      USE
    endif
  endproc
  proc Try_04182CA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do while not eof ("Carica")
      Select Carica
      this.w_CRCODMAT = MTCODMAT
      this.w_CRCODLOT = CODLOT
      this.w_CRCODUBI = CODUBI
      this.w_CODCOM = CODCOM
      this.w_CRSERRIF = nvl(CHIAVE, Space(10))
      this.w_CRROWRIF = nvl(NUMROW,0)
      this.w_CRRIFNUM = nvl(NUMRIF, 0)
      this.w_CR__FLAG = IIF( Empty( this.w_CRSERRIF ) , " " , "+" )
      this.w_CR_SALDO = 0
      * --- Quattro Insert perch� non ho Link di analisi tra MOVIMATR e CODUBI e CODLOT.
      *     Questo fa si che il Painter inserisca degli sapzi al posto di Null
      do case
        case Empty(this.w_CRCODUBI ) And Empty( this.w_CRCODLOT )
          * --- Insert into MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTKEYSAL"+",MTCODMAT"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CRSERIAL),'MOVIMATR','MTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWNUM),'MOVIMATR','MTROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRNUMRIF),'MOVIMATR','MTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MOVIMATR','MTMAGCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGSCA),'MOVIMATR','MTMAGSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLCARI),'MOVIMATR','MTFLCARI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLSCAR),'MOVIMATR','MTFLSCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRKEYSAL),'MOVIMATR','MTKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODMAT),'MOVIMATR','MTCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRSERRIF),'MOVIMATR','MTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWRIF),'MOVIMATR','MTROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRRIFNUM),'MOVIMATR','MTRIFNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR__FLAG),'MOVIMATR','MT__FLAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR_SALDO),'MOVIMATR','MT_SALDO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'MOVIMATR','MTCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_CRSERIAL,'MTROWNUM',this.w_CRROWNUM,'MTNUMRIF',this.w_CRNUMRIF,'MTMAGCAR',this.w_CRMAGCAR,'MTMAGSCA',this.w_CRMAGSCA,'MTFLCARI',this.w_CRFLCARI,'MTFLSCAR',this.w_CRFLSCAR,'MTKEYSAL',this.w_CRKEYSAL,'MTCODMAT',this.w_CRCODMAT,'MTSERRIF',this.w_CRSERRIF,'MTROWRIF',this.w_CRROWRIF,'MTRIFNUM',this.w_CRRIFNUM)
            insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTKEYSAL,MTCODMAT,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
               values (;
                 this.w_CRSERIAL;
                 ,this.w_CRROWNUM;
                 ,this.w_CRNUMRIF;
                 ,this.w_CRMAGCAR;
                 ,this.w_CRMAGSCA;
                 ,this.w_CRFLCARI;
                 ,this.w_CRFLSCAR;
                 ,this.w_CRKEYSAL;
                 ,this.w_CRCODMAT;
                 ,this.w_CRSERRIF;
                 ,this.w_CRROWRIF;
                 ,this.w_CRRIFNUM;
                 ,this.w_CR__FLAG;
                 ,this.w_CR_SALDO;
                 ,this.w_CODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        case Empty(this.w_CRCODUBI ) And !Empty( this.w_CRCODLOT )
          * --- Insert into MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTKEYSAL"+",MTCODMAT"+",MTCODLOT"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CRSERIAL),'MOVIMATR','MTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWNUM),'MOVIMATR','MTROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRNUMRIF),'MOVIMATR','MTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MOVIMATR','MTMAGCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGSCA),'MOVIMATR','MTMAGSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLCARI),'MOVIMATR','MTFLCARI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLSCAR),'MOVIMATR','MTFLSCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRKEYSAL),'MOVIMATR','MTKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODMAT),'MOVIMATR','MTCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MOVIMATR','MTCODLOT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRSERRIF),'MOVIMATR','MTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWRIF),'MOVIMATR','MTROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRRIFNUM),'MOVIMATR','MTRIFNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR__FLAG),'MOVIMATR','MT__FLAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR_SALDO),'MOVIMATR','MT_SALDO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'MOVIMATR','MTCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_CRSERIAL,'MTROWNUM',this.w_CRROWNUM,'MTNUMRIF',this.w_CRNUMRIF,'MTMAGCAR',this.w_CRMAGCAR,'MTMAGSCA',this.w_CRMAGSCA,'MTFLCARI',this.w_CRFLCARI,'MTFLSCAR',this.w_CRFLSCAR,'MTKEYSAL',this.w_CRKEYSAL,'MTCODMAT',this.w_CRCODMAT,'MTCODLOT',this.w_CRCODLOT,'MTSERRIF',this.w_CRSERRIF,'MTROWRIF',this.w_CRROWRIF)
            insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTKEYSAL,MTCODMAT,MTCODLOT,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
               values (;
                 this.w_CRSERIAL;
                 ,this.w_CRROWNUM;
                 ,this.w_CRNUMRIF;
                 ,this.w_CRMAGCAR;
                 ,this.w_CRMAGSCA;
                 ,this.w_CRFLCARI;
                 ,this.w_CRFLSCAR;
                 ,this.w_CRKEYSAL;
                 ,this.w_CRCODMAT;
                 ,this.w_CRCODLOT;
                 ,this.w_CRSERRIF;
                 ,this.w_CRROWRIF;
                 ,this.w_CRRIFNUM;
                 ,this.w_CR__FLAG;
                 ,this.w_CR_SALDO;
                 ,this.w_CODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        case Empty( this.w_CRCODLOT ) And !Empty(this.w_CRCODUBI ) 
          * --- Insert into MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTKEYSAL"+",MTCODMAT"+",MTCODUBI"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CRSERIAL),'MOVIMATR','MTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWNUM),'MOVIMATR','MTROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRNUMRIF),'MOVIMATR','MTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MOVIMATR','MTMAGCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGSCA),'MOVIMATR','MTMAGSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLCARI),'MOVIMATR','MTFLCARI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLSCAR),'MOVIMATR','MTFLSCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRKEYSAL),'MOVIMATR','MTKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODMAT),'MOVIMATR','MTCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MOVIMATR','MTCODUBI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRSERRIF),'MOVIMATR','MTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWRIF),'MOVIMATR','MTROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRRIFNUM),'MOVIMATR','MTRIFNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR__FLAG),'MOVIMATR','MT__FLAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR_SALDO),'MOVIMATR','MT_SALDO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'MOVIMATR','MTCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_CRSERIAL,'MTROWNUM',this.w_CRROWNUM,'MTNUMRIF',this.w_CRNUMRIF,'MTMAGCAR',this.w_CRMAGCAR,'MTMAGSCA',this.w_CRMAGSCA,'MTFLCARI',this.w_CRFLCARI,'MTFLSCAR',this.w_CRFLSCAR,'MTKEYSAL',this.w_CRKEYSAL,'MTCODMAT',this.w_CRCODMAT,'MTCODUBI',this.w_CRCODUBI,'MTSERRIF',this.w_CRSERRIF,'MTROWRIF',this.w_CRROWRIF)
            insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTKEYSAL,MTCODMAT,MTCODUBI,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
               values (;
                 this.w_CRSERIAL;
                 ,this.w_CRROWNUM;
                 ,this.w_CRNUMRIF;
                 ,this.w_CRMAGCAR;
                 ,this.w_CRMAGSCA;
                 ,this.w_CRFLCARI;
                 ,this.w_CRFLSCAR;
                 ,this.w_CRKEYSAL;
                 ,this.w_CRCODMAT;
                 ,this.w_CRCODUBI;
                 ,this.w_CRSERRIF;
                 ,this.w_CRROWRIF;
                 ,this.w_CRRIFNUM;
                 ,this.w_CR__FLAG;
                 ,this.w_CR_SALDO;
                 ,this.w_CODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        case !Empty(this.w_CRCODUBI ) And !Empty( this.w_CRCODLOT )
          * --- Insert into MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVIMATR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MTSERIAL"+",MTROWNUM"+",MTNUMRIF"+",MTMAGCAR"+",MTMAGSCA"+",MTFLCARI"+",MTFLSCAR"+",MTKEYSAL"+",MTCODMAT"+",MTCODLOT"+",MTCODUBI"+",MTSERRIF"+",MTROWRIF"+",MTRIFNUM"+",MT__FLAG"+",MT_SALDO"+",MTCODCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CRSERIAL),'MOVIMATR','MTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWNUM),'MOVIMATR','MTROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRNUMRIF),'MOVIMATR','MTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGCAR),'MOVIMATR','MTMAGCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRMAGSCA),'MOVIMATR','MTMAGSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLCARI),'MOVIMATR','MTFLCARI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRFLSCAR),'MOVIMATR','MTFLSCAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRKEYSAL),'MOVIMATR','MTKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODMAT),'MOVIMATR','MTCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODLOT),'MOVIMATR','MTCODLOT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRCODUBI),'MOVIMATR','MTCODUBI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRSERRIF),'MOVIMATR','MTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRROWRIF),'MOVIMATR','MTROWRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CRRIFNUM),'MOVIMATR','MTRIFNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR__FLAG),'MOVIMATR','MT__FLAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CR_SALDO),'MOVIMATR','MT_SALDO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'MOVIMATR','MTCODCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_CRSERIAL,'MTROWNUM',this.w_CRROWNUM,'MTNUMRIF',this.w_CRNUMRIF,'MTMAGCAR',this.w_CRMAGCAR,'MTMAGSCA',this.w_CRMAGSCA,'MTFLCARI',this.w_CRFLCARI,'MTFLSCAR',this.w_CRFLSCAR,'MTKEYSAL',this.w_CRKEYSAL,'MTCODMAT',this.w_CRCODMAT,'MTCODLOT',this.w_CRCODLOT,'MTCODUBI',this.w_CRCODUBI,'MTSERRIF',this.w_CRSERRIF)
            insert into (i_cTable) (MTSERIAL,MTROWNUM,MTNUMRIF,MTMAGCAR,MTMAGSCA,MTFLCARI,MTFLSCAR,MTKEYSAL,MTCODMAT,MTCODLOT,MTCODUBI,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG,MT_SALDO,MTCODCOM &i_ccchkf. );
               values (;
                 this.w_CRSERIAL;
                 ,this.w_CRROWNUM;
                 ,this.w_CRNUMRIF;
                 ,this.w_CRMAGCAR;
                 ,this.w_CRMAGSCA;
                 ,this.w_CRFLCARI;
                 ,this.w_CRFLSCAR;
                 ,this.w_CRKEYSAL;
                 ,this.w_CRCODMAT;
                 ,this.w_CRCODLOT;
                 ,this.w_CRCODUBI;
                 ,this.w_CRSERRIF;
                 ,this.w_CRROWRIF;
                 ,this.w_CRRIFNUM;
                 ,this.w_CR__FLAG;
                 ,this.w_CR_SALDO;
                 ,this.w_CODCOM;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
      endcase
      * --- Chiudo il movimento Matricole che ho evaso
      if Not Empty(this.w_CRSERRIF)
        * --- Write into MOVIMATR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MT_SALDO =MT_SALDO+ "+cp_ToStrODBC(1);
              +i_ccchkf ;
          +" where ";
              +"MTSERIAL = "+cp_ToStrODBC(this.w_CRSERRIF);
              +" and MTROWNUM = "+cp_ToStrODBC(this.w_CRROWRIF);
              +" and MTNUMRIF = "+cp_ToStrODBC(this.w_CRRIFNUM);
              +" and MTKEYSAL = "+cp_ToStrODBC(this.w_CRKEYSAL);
              +" and MTCODMAT = "+cp_ToStrODBC(this.w_CRCODMAT);
              +" and MT_SALDO = "+cp_ToStrODBC(0);
                 )
        else
          update (i_cTable) set;
              MT_SALDO = MT_SALDO + 1;
              &i_ccchkf. ;
           where;
              MTSERIAL = this.w_CRSERRIF;
              and MTROWNUM = this.w_CRROWRIF;
              and MTNUMRIF = this.w_CRRIFNUM;
              and MTKEYSAL = this.w_CRKEYSAL;
              and MTCODMAT = this.w_CRCODMAT;
              and MT_SALDO = 0;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Se non aggiorna significa che MT_SALDO del movimento non � 0 e quindi non posso utilizzarlo!
        if i_Rows=0
          * --- Raise
          i_Error=Ah_Msgformat("Matricola %1 utilizzata da altro utente. Impossibile generare dettaglio matricole", this.w_CRCODMAT)
          return
        endif
      endif
      Select Carica
      if Not Eof("Carica")
        Skip
      endif
    enddo
    * --- Costruisco MOVILOTT
    * --- commit
    cp_EndTrs(.t.)
    * --- Aggiono lo Zoom
    Do GSMA_BVM with This.w_PUNPAD.oParentObject.oParentObject.oParentObject
    ah_ErrorMsg("Dettaglio matricole aggiornato","!","")
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggi di errore in caso di selezione errata
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    if this.w_TOTSEL
      this.w_oMess.AddMsgPartNL("Numero di matricole selezionate maggiore della quantit�")     
    endif
    if this.w_CHKCODLOT
      this.w_oMess.AddMsgPartNL("Codice lotto non impostato")     
    endif
    if this.w_CHKCODUBI
      this.w_oMess.AddMsgPartNL("Codice ubicazione non impostato")     
    endif
    this.w_oMess.AddMsgPartNL("Impossibile confermare la selezione")     
    this.w_oMess.Ah_ErrorMsg()     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MOVIMATR'
    this.cWorkTables[2]='MATRICOL'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
