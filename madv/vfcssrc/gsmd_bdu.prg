* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bdu                                                        *
*              Controllo eliminazione ubicazioni                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-04                                                      *
* Last revis.: 2008-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bdu",oParentObject)
return(i_retval)

define class tgsmd_bdu as StdBatch
  * --- Local variables
  w_CODMAG = space(5)
  w_CODUBI = space(20)
  w_MESS = space(10)
  * --- WorkFile variables
  SALDILOT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se l' ubicazione � utilizzata nell anagrafica saldi lotti allora se ne impedisce la cancellazione
    this.w_CODMAG = this.oparentobject.w_UBCODMAG
    this.w_CODUBI = this.oparentobject.w_UBCODICE
    * --- Select from gsar4bks
    do vq_exec with 'gsar4bks',this,'_Curs_gsar4bks','',.f.,.t.
    if used('_Curs_gsar4bks')
      select _Curs_gsar4bks
      locate for 1=1
      do while not(eof())
      if CONTA>0
        this.w_MESS = "Ubicazione usata nell anagrafica saldi/lotti%0Impossibile eliminare!"
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat(this.w_MESS)
      endif
        select _Curs_gsar4bks
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALDILOT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gsar4bks')
      use in _Curs_gsar4bks
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
