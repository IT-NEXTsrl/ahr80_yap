* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bem                                                        *
*              Eliminazione matricole                                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-07                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bem",oParentObject,m.pTipo)
return(i_retval)

define class tgsmd_bem as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_ELIMINATE = 0
  w_ERRORI = 0
  w_KEYSAL = space(40)
  w_CODMAT = space(40)
  w_CODART = space(40)
  w_OK = .f.
  * --- WorkFile variables
  MATRICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTipo="SELEZIONA"
        * --- Seleziona Deseleziona Tutti
        *     Se il documento � fuori dal'esercizio lo marco sempre
        Update ( this.oParentObject.w_ZOOM.cCursor) Set Xchk= iif( this.oParentObject.w_SELEZI="D",0,1)
      case this.pTipo="ELIMINA"
        * --- Tenta l'eliminazione delle matricole selezionate
        this.w_ELIMINATE = 0
        this.w_ERRORI = 0
        this.oParentObject.w_MSG = ""
        this.oParentobject.oPgFrm.ActivePage=2
        AddMsgNL("Elaborazione iniziata alle: %1",this, Time())
        * --- Try
        local bErr_032DFCA8
        bErr_032DFCA8=bTrsErr
        this.Try_032DFCA8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_032DFCA8
        * --- End
        AddMsgNL("Elaborazione terminata alle: %1",this,Time())
        AddMsgNL("Eliminate %1 matricole con %2 errori", this, Alltrim(Str( this.w_ELIMINATE )), Alltrim(Str( this.w_ERRORI )) )
        * --- Refresho lo Zoom
        This.oParentObject.NotifyEvent("Esegui")
    endcase
  endproc
  proc Try_032DFCA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
    do while Not Eof ( this.oParentObject.w_ZOOM.cCursor )
      * --- Tento di eliminare la matricola
      Select ( this.oParentObject.w_ZOOM.cCursor )
      if Xchk=1
        this.w_OK = .T.
        this.w_KEYSAL = Nvl ( AMKEYSAL , Space(40) )
        this.w_CODMAT = Nvl ( AMCODICE , Space(40) )
        this.w_CODART = Nvl ( AMCODART , Space(40) )
        * --- Try
        local bErr_03419408
        bErr_03419408=bTrsErr
        this.Try_03419408()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_OK = .F.
        endif
        bTrsErr=bTrsErr or bErr_03419408
        * --- End
        if this.w_OK
          AddMsgNL("Eliminata %1 articolo %2",this, ALLTRIM(this.w_CODMAT) , this.w_CODART )
          this.w_ELIMINATE = this.w_ELIMINATE + 1
        else
          AddMsgNL("Impossibile eliminare matricola %1 dell'articolo %2 errore: %3", this, ALLTRIM(this.w_CODMAT), this.w_CODART, Message() )
          this.w_ERRORI = this.w_ERRORI + 1
        endif
      endif
      if Not Eof ( this.oParentObject.w_ZOOM.cCursor )
        Skip
      endif
    enddo
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03419408()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MATRICOL
    i_nConn=i_TableProp[this.MATRICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AMKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and AMCODICE = "+cp_ToStrODBC(this.w_CODMAT);
             )
    else
      delete from (i_cTable) where;
            AMKEYSAL = this.w_KEYSAL;
            and AMCODICE = this.w_CODMAT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MATRICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
