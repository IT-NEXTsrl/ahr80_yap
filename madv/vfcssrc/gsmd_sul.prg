* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_sul                                                        *
*              Stampa etichette unit� logistiche                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_8]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-03                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmd_sul",oParentObject))

* --- Class definition
define class tgsmd_sul as StdForm
  Top    = 11
  Left   = 9

  * --- Standard Properties
  Width  = 463
  Height = 227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=211137687
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  UNIT_LOG_IDX = 0
  cPrg = "gsmd_sul"
  cComment = "Stampa etichette unit� logistiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SSCCINI = space(18)
  w_SSCCFIN = space(18)
  w_NUMCOPY = 0
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_SERIE1 = space(2)
  w_NUMFIN = 0
  w_SERIE2 = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmd_sulPag1","gsmd_sul",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSSCCINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='UNIT_LOG'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSMD_BSU with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SSCCINI=space(18)
      .w_SSCCFIN=space(18)
      .w_NUMCOPY=0
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_SERIE1=space(2)
      .w_NUMFIN=0
      .w_SERIE2=space(2)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SSCCINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SSCCFIN))
          .link_1_2('Full')
        endif
        .w_NUMCOPY = 1
          .DoRTCalc(4,5,.f.)
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999
        .w_SERIE2 = ''
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SSCCINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SSCCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_SSCCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_SSCCINI))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SSCCINI)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SSCCINI) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oSSCCINI_1_1'),i_cWhere,'',"Unit� logistiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SSCCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_SSCCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_SSCCINI)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SSCCINI = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_SSCCINI = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_SSCCFIN)) OR  (.w_SSCCINI<=.w_SSCCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'unit� logistica iniziale � pi� grande di quella finale")
        endif
        this.w_SSCCINI = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SSCCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SSCCFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIT_LOG_IDX,3]
    i_lTable = "UNIT_LOG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2], .t., this.UNIT_LOG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SSCCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIT_LOG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UL__SSCC like "+cp_ToStrODBC(trim(this.w_SSCCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UL__SSCC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UL__SSCC',trim(this.w_SSCCFIN))
          select UL__SSCC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UL__SSCC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SSCCFIN)==trim(_Link_.UL__SSCC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SSCCFIN) and !this.bDontReportError
            deferred_cp_zoom('UNIT_LOG','*','UL__SSCC',cp_AbsName(oSource.parent,'oSSCCFIN_1_2'),i_cWhere,'',"Unit� logistiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                     +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',oSource.xKey(1))
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SSCCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UL__SSCC";
                   +" from "+i_cTable+" "+i_lTable+" where UL__SSCC="+cp_ToStrODBC(this.w_SSCCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UL__SSCC',this.w_SSCCFIN)
            select UL__SSCC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SSCCFIN = NVL(_Link_.UL__SSCC,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_SSCCFIN = space(18)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_SSCCINI)) OR  (.w_SSCCINI<=.w_SSCCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'unit� logistica iniziale � pi� grande di quella finale")
        endif
        this.w_SSCCFIN = space(18)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIT_LOG_IDX,2])+'\'+cp_ToStr(_Link_.UL__SSCC,1)
      cp_ShowWarn(i_cKey,this.UNIT_LOG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SSCCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSSCCINI_1_1.value==this.w_SSCCINI)
      this.oPgFrm.Page1.oPag.oSSCCINI_1_1.value=this.w_SSCCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSSCCFIN_1_2.value==this.w_SSCCFIN)
      this.oPgFrm.Page1.oPag.oSSCCFIN_1_2.value=this.w_SSCCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOPY_1_3.value==this.w_NUMCOPY)
      this.oPgFrm.Page1.oPag.oNUMCOPY_1_3.value=this.w_NUMCOPY
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAINI_1_4.value==this.w_DATAINI)
      this.oPgFrm.Page1.oPag.oDATAINI_1_4.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_5.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_5.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_6.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_6.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE1_1_7.value==this.w_SERIE1)
      this.oPgFrm.Page1.oPag.oSERIE1_1_7.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIE2_1_9.value==this.w_SERIE2)
      this.oPgFrm.Page1.oPag.oSERIE2_1_9.value=this.w_SERIE2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_SSCCFIN)) OR  (.w_SSCCINI<=.w_SSCCFIN))  and not(empty(.w_SSCCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSSCCINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'unit� logistica iniziale � pi� grande di quella finale")
          case   not((empty(.w_SSCCINI)) OR  (.w_SSCCINI<=.w_SSCCFIN))  and not(empty(.w_SSCCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSSCCFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'unit� logistica iniziale � pi� grande di quella finale")
          case   not(.w_NUMCOPY>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMCOPY_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_DATAFIN)) OR  (.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((empty(.w_DATAINI)) OR  (.w_DATAINI<=.w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE1_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERIE2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsmd_sulPag1 as StdContainer
  Width  = 459
  height = 227
  stdWidth  = 459
  stdheight = 227
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSSCCINI_1_1 as StdField with uid="XHFKHIRNWP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SSCCINI", cQueryName = "SSCCINI",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "L'unit� logistica iniziale � pi� grande di quella finale",;
    ToolTipText = "Unit� logistica iniziale selezionata",;
    HelpContextID = 258817318,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=135, Top=11, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_SSCCINI"

  func oSSCCINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSSCCINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSSCCINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oSSCCINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'',this.parent.oContained
  endproc

  add object oSSCCFIN_1_2 as StdField with uid="RGETXCMOJA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SSCCFIN", cQueryName = "SSCCFIN",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    sErrorMsg = "L'unit� logistica iniziale � pi� grande di quella finale",;
    ToolTipText = "Unit� logistica finale selezionata",;
    HelpContextID = 171785510,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=135, Top=39, InputMask=replicate('X',18), bHasZoom = .t. , cLinkFile="UNIT_LOG", oKey_1_1="UL__SSCC", oKey_1_2="this.w_SSCCFIN"

  func oSSCCFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSSCCFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSSCCFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIT_LOG','*','UL__SSCC',cp_AbsName(this.parent,'oSSCCFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� logistiche",'',this.parent.oContained
  endproc

  add object oNUMCOPY_1_3 as StdField with uid="UXNIDCANMO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMCOPY", cQueryName = "NUMCOPY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 30269142,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=392, Top=11, cSayPict='"99999"', cGetPict='"99999"'

  func oNUMCOPY_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMCOPY>0)
    endwith
    return bRes
  endfunc

  add object oDATAINI_1_4 as StdField with uid="TLSMGPEYWG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "DATAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento iniziale selezionata",;
    HelpContextID = 258751030,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=84, Top=75

  func oDATAINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAFIN)) OR  (.w_DATAINI<=.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_5 as StdField with uid="SJPNDJOWBM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento finale selezionata",;
    HelpContextID = 171719222,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=84, Top=106

  func oDATAFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAINI)) OR  (.w_DATAINI<=.w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_6 as StdField with uid="CGTAYVCLKJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 87826730,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=263, Top=75, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oSERIE1_1_7 as StdField with uid="IKCHOCFECU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 36970278,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=334, Top=75, InputMask=replicate('X',2)

  func oSERIE1_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_8 as StdField with uid="PPVPLBFEDU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 9380138,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=263, Top=106, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oSERIE2_1_9 as StdField with uid="SNGKDXOYPG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 53747494,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=334, Top=106, InputMask=replicate('X',2)

  func oSERIE2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_SERIE2>=.w_SERIE1) or (empty(.w_SERIE1)))
    endwith
    return bRes
  endfunc


  add object oObj_1_19 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=148, width=346,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 147735578


  add object oBtn_1_20 as StdButton with uid="GHEVIDFOUV",left=353, top=177, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 183943642;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSMD_BSU with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="GNIBZHELYY",left=404, top=177, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 218455110;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="RWHNHUCUAO",Visible=.t., Left=3, Top=75,;
    Alignment=1, Width=79, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="GFGSYQKGMB",Visible=.t., Left=3, Top=106,;
    Alignment=1, Width=79, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=171, Top=75,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=171, Top=106,;
    Alignment=1, Width=90, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=325, Top=75,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=325, Top=106,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QAUEZYFYDW",Visible=.t., Left=12, Top=11,;
    Alignment=1, Width=121, Height=18,;
    Caption="Da unit� logistica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PPZHFIKLUM",Visible=.t., Left=12, Top=39,;
    Alignment=1, Width=121, Height=18,;
    Caption="A unit� logistica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="EYUCCHJANJ",Visible=.t., Left=8, Top=148,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MXEPXNYCRJ",Visible=.t., Left=279, Top=11,;
    Alignment=1, Width=110, Height=18,;
    Caption="Numero copie:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmd_sul','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
