* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_brl                                                        *
*              RIcostruzoione saldi/lotti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-10                                                      *
* Last revis.: 2018-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIALE,pTIPO,pOPERAZIONE,pRIGA,pBNOTRAN,w_LOG,w_FLDELSLD,w_AGCLOTUBI,w_CODARTIN,w_CODARTFI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_brl",oParentObject,m.pSERIALE,m.pTIPO,m.pOPERAZIONE,m.pRIGA,m.pBNOTRAN,m.w_LOG,m.w_FLDELSLD,m.w_AGCLOTUBI,m.w_CODARTIN,m.w_CODARTFI)
return(i_retval)

define class tgsmd_brl as StdBatch
  * --- Local variables
  pSERIALE = space(10)
  pTIPO = space(1)
  pOPERAZIONE = space(1)
  pRIGA = 0
  pBNOTRAN = .f.
  w_LOG = .NULL.
  w_FLDELSLD = space(1)
  w_AGCLOTUBI = space(1)
  w_CODARTIN = space(20)
  w_CODARTFI = space(20)
  w_CODLOT = space(20)
  w_CODART = space(20)
  w_SALDO = 0
  w_COMMDEFA = space(15)
  w_EMPTYCOM = space(15)
  w_ROWS = 0
  w_ROWSCOM = 0
  * --- WorkFile variables
  SALDILOT_idx=0
  TMPSALDI_idx=0
  LOTTIART_idx=0
  SALOTCOM_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- 
    *     - Se non riceve parametri allora effettua il calcolo massivo (ossia ricalcola e aggiorna j  saldi di tutti i lotti)
    *     altrimenti aggiorna i saldilotti relativamente ad un documento (il document di cui ha ricevuto il seriale 
    *     Parametri :  
    *     �  pSERIALE   -> seriale del movimento magazzino , docuemnto di vendita o moviemnto GPOS
    *     �  pTIPO   ->   indica a quale tipo di moviemntazione si riferisce il seriale : M=movimento magazzino , V=docuemnto , P=moviemnto GPOS
    *     �  pOPERAZIONE  -> '+'  Indica se il documento � stato inserito (quindi si aggiornano i saldi conformamente al segno indicato nei campi flag ex : MVFLSCA) -  '-' indica che il documento verr� cancellato (i saldi vengono aggiornati con segno opposto a quello indicato nei campi flag ex : MVFLSCA)
    *     � pRIGA   ->  indica il numero di riga del documento (CPROWNUM) su cui effettuare l aggioenamento saldi
    *     �pBNOTRAN -> Indifca se effettuaer la ricostruzione sotto transazione
    *     �w_LOG -> Log erroi su cui scrivere i messagi di errore/avviso
    *     � w_FLDELSLD -> Svuota tabella saldi prima della ricostruzione
    *     � w_AGCLOTUBI -> Aggiorna anche i saldi lotto/ubicazione/commessa
    *     � w_CODARTIN -> filtro da articolo
    *     � w_CODARTFI -> filtro a articolo
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Se non vengono passati i parametri  allora li imposto con valori di default
    if VARTYPE (this.pTIPO)="L"
      this.pTIPO = ""
    endif
    if VARTYPE (this.pOPERAZIONE)="L" or this.pOPERAZIONE<>"-"
      this.pOPERAZIONE = "+"
    endif
    if VARTYPE (this.pSERIALE)="L"
      this.pSERIALE = ""
    endif
    if VARTYPE (this.pRIGA)="L"
      this.pRIGA = 0
    endif
    this.w_FLDELSLD = IIF(VARTYPE (this.w_FLDELSLD)<>"C", "N", this.w_FLDELSLD)
    this.w_AGCLOTUBI = IIF(VARTYPE (this.w_AGCLOTUBI)<>"C", "S", this.w_AGCLOTUBI)
    this.w_CODARTIN = IIF(VARTYPE (this.w_CODARTIN)<>"C" , SPACE(20), NVL(this.w_CODARTIN, SPACE(20)))
    this.w_CODARTFI = IIF(VARTYPE (this.w_CODARTFI)<>"C" , SPACE(20), NVL(this.w_CODARTFI, SPACE(20)))
    * --- Try
    local bErr_02CC42A0
    bErr_02CC42A0=bTrsErr
    this.Try_02CC42A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if NOT this.pBNOTRAN
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Aggiornamento saldo lotti terminato con errore : %1", this.w_Log ,Message() , , ,, ,)
      endif
      this.pBNOTRAN = .F.
    endif
    bTrsErr=bTrsErr or bErr_02CC42A0
    * --- End
  endproc
  proc Try_02CC42A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if NOT this.pBNOTRAN
      * --- begin transaction
      cp_BeginTrs()
    endif
    if VarType( this.w_Log ) ="O" 
      AddMsgNL("Inizio ricostruzione saldi lotti ", this.w_Log , , , ,, ,)
    endif
    if EMPTY (this.pSERIALE) OR EMPTY ( this.pTIPO)
      if this.w_FLDELSLD<>"S" And this.w_AGCLOTUBI="S" And !Empty(this.w_COMMDEFA)
        * --- Se la commessa di default non � vuota elimino il saldo in modo da non lasciare dati incongruenti
        this.w_EMPTYCOM = SPACE(15)
        * --- Delete from SALOTCOM
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SMCODCAN = "+cp_ToStrODBC(this.w_EMPTYCOM);
                 )
        else
          delete from (i_cTable) where;
                SMCODCAN = this.w_EMPTYCOM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if this.w_FLDELSLD="S"
        * --- Delete from SALDILOT
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
        
          do vq_exec with 'GSMDDSLD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.w_ROWS = i_rows
        if this.w_AGCLOTUBI="S"
          * --- Delete from SALOTCOM
          i_nConn=i_TableProp[this.SALOTCOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
          
            do vq_exec with 'GSMDDSLDC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_ROWSCOM = i_rows
        endif
      else
        * --- Se si sta facendo la ricostruzione massiva  dei saldi (di tuti i lotti) azzera i saldi 
        * --- Write into SALDILOT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SUCODART"
          do vq_exec with 'GSMDDSLD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPER');
          +",SUQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPER');
          +",SUQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPRO');
          +",SUQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPRO');
              +i_ccchkf;
              +" from "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 set ";
          +"SALDILOT.SUQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPER');
          +",SALDILOT.SUQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPER');
          +",SALDILOT.SUQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPRO');
          +",SALDILOT.SUQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPRO');
              +Iif(Empty(i_ccchkf),"",",SALDILOT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SALDILOT.SUCODART = t2.SUCODART";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set (";
              +"SUQTAPER,";
              +"SUQTRPER,";
              +"SUQTRPRO,";
              +"SUQTAPRO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPER')+",";
              +cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPER')+",";
              +cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPRO')+",";
              +cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPRO')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set ";
          +"SUQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPER');
          +",SUQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPER');
          +",SUQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPRO');
          +",SUQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPRO');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPER');
          +",SUQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPER');
          +",SUQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTRPRO');
          +",SUQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALDILOT','SUQTAPRO');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_ROWS = i_rows
        if this.w_AGCLOTUBI="S"
          * --- Write into SALOTCOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALOTCOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="SMCODART"
            do vq_exec with 'GSMDDSLDC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SMQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER');
            +",SMQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER');
            +",SMQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPRO');
            +",SMQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPRO');
                +i_ccchkf;
                +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
            +"SALOTCOM.SMQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER');
            +",SALOTCOM.SMQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER');
            +",SALOTCOM.SMQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPRO');
            +",SALOTCOM.SMQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPRO');
                +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="SALOTCOM.SMCODART = t2.SMCODART";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set (";
                +"SMQTAPER,";
                +"SMQTRPER,";
                +"SMQTAPRO,";
                +"SMQTRPRO";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER')+",";
                +cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER')+",";
                +cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPRO')+",";
                +cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPRO')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
            +"SMQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER');
            +",SMQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER');
            +",SMQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPRO');
            +",SMQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPRO');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SMQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER');
            +",SMQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER');
            +",SMQTAPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPRO');
            +",SMQTRPRO ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPRO');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_ROWSCOM = i_rows
        endif
      endif
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Azzerata tabella saldi lotti per %1 righe ", this.w_Log , alltrim(str( this.w_rows)), , ,, ,)
        if this.w_AGCLOTUBI="S"
          AddMsgNL("Azzerata tabella saldi lotti/commessa per %1 righe ", this.w_Log , alltrim(str( this.w_rowscom)), , ,, ,)
        endif
      endif
    endif
    * --- Estrae i saldi  dai movimenti di magazzino/documenti/POS
    if VarType( this.w_Log ) ="O" 
      AddMsgNL("Ricalcolo saldi da movimenti di magazzino/documenti/movimenti negozio pos", this.w_Log , , , ,, ,)
    endif
    if g_GPOS="S"
      * --- Create temporary table TMPSALDI
      i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsmd_sl5',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSALDI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPSALDI
      i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsmd_SL6',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSALDI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Insert Dati Non Presenti in SALDILOT
    if VarType( this.w_Log ) ="O" 
      AddMsgNL("Inizio inserimento nuove righe nella tabella saldi lotti ", this.w_Log , , , ,, ,)
    endif
    * --- Insert into SALDILOT
    i_nConn=i_TableProp[this.SALDILOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMD_SL4",this.SALDILOT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ROWS = i_rows
    if this.w_AGCLOTUBI="S"
      * --- Insert into SALOTCOM
      i_nConn=i_TableProp[this.SALOTCOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMD_SL10",this.SALOTCOM_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_ROWSCOM = i_rows
    endif
    if VarType( this.w_Log ) ="O" 
      AddMsgNL("Inserite  %1 nuove righe nella tabella saldi lotti ", this.w_Log , alltrim(str( this.w_rows)), , ,, ,)
      if this.w_AGCLOTUBI="S"
        AddMsgNL("Inserite  %1 nuove righe nella tabella saldi lotti/commessa ", this.w_Log , alltrim(str( this.w_rowscom)), , ,, ,)
      endif
      AddMsgNL("Inizio aggiornamento saldi presenti nell anagrafica saldi lotti", this.w_Log , , , ,, ,)
    endif
    * --- Aggiorna Dati Esistenti in SALDILOT
    *     Se l operazione ='+' viene effettuato l aggiornamento normale mentre se operazione='-'  il saldo viene stornato
    *     Nel caso di un aggiornamento non massivo allora si presume che lo stato dei saldi sia corretto
    if this.pOPERAZIONE="+"
      * --- Write into SALDILOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SUCODART,SUCODMAG,SUCODLOT ,SUCODUBI "
        do vq_exec with 'GSMD_SL3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPER";
            +",SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPER";
            +i_ccchkf;
            +" from "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 set ";
            +"SALDILOT.SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPER";
            +",SALDILOT.SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPER";
            +Iif(Empty(i_ccchkf),"",",SALDILOT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDILOT.SUCODART = t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = t2.SUCODUBI ";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set (";
            +"SUQTAPER,";
            +"SUQTRPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SUQTAPER+t2.SUQTAPER,";
            +"SUQTRPER+t2.SUQTRPER";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set ";
            +"SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPER";
            +",SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPER";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
                +" and "+i_cTable+".SUCODMAG = "+i_cQueryTable+".SUCODMAG";
                +" and "+i_cTable+".SUCODLOT  = "+i_cQueryTable+".SUCODLOT ";
                +" and "+i_cTable+".SUCODUBI  = "+i_cQueryTable+".SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SUQTAPER = (select "+i_cTable+".SUQTAPER+SUQTAPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SUQTRPER = (select "+i_cTable+".SUQTRPER+SUQTRPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_ROWS = i_rows
      if this.w_AGCLOTUBI="S"
        * --- Write into SALOTCOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SMCODART,SMCODMAG,SMCODLOT ,SMCODUBI ,SMCODCAN"
          do vq_exec with 'GSMD_SL2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPER";
              +",SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPER";
              +i_ccchkf;
              +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
              +"SALOTCOM.SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPER";
              +",SALOTCOM.SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPER";
              +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SALOTCOM.SMCODART = t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = t2.SMCODCAN";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set (";
              +"SMQTAPER,";
              +"SMQTRPER";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SMQTAPER+t2.SMQTAPER,";
              +"SMQTRPER+t2.SMQTRPER";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPER";
              +",SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPER";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
                  +" and "+i_cTable+".SMCODMAG = "+i_cQueryTable+".SMCODMAG";
                  +" and "+i_cTable+".SMCODLOT  = "+i_cQueryTable+".SMCODLOT ";
                  +" and "+i_cTable+".SMCODUBI  = "+i_cQueryTable+".SMCODUBI ";
                  +" and "+i_cTable+".SMCODCAN = "+i_cQueryTable+".SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = (select "+i_cTable+".SMQTAPER+SMQTAPER from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SMQTRPER = (select "+i_cTable+".SMQTRPER+SMQTRPER from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_ROWSCOM = i_rows
      endif
    else
      * --- Write into SALDILOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SUCODART,SUCODMAG,SUCODLOT ,SUCODUBI "
        do vq_exec with 'GSMD_SL3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SUQTAPER = SALDILOT.SUQTAPER-_t2.SUQTAPER";
            +",SUQTRPER = SALDILOT.SUQTRPER-_t2.SUQTRPER";
            +i_ccchkf;
            +" from "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 set ";
            +"SALDILOT.SUQTAPER = SALDILOT.SUQTAPER-_t2.SUQTAPER";
            +",SALDILOT.SUQTRPER = SALDILOT.SUQTRPER-_t2.SUQTRPER";
            +Iif(Empty(i_ccchkf),"",",SALDILOT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDILOT.SUCODART = t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = t2.SUCODUBI ";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set (";
            +"SUQTAPER,";
            +"SUQTRPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SUQTAPER-t2.SUQTAPER,";
            +"SUQTRPER-t2.SUQTRPER";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDILOT.SUCODART = _t2.SUCODART";
                +" and "+"SALDILOT.SUCODMAG = _t2.SUCODMAG";
                +" and "+"SALDILOT.SUCODLOT  = _t2.SUCODLOT ";
                +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set ";
            +"SUQTAPER = SALDILOT.SUQTAPER-_t2.SUQTAPER";
            +",SUQTRPER = SALDILOT.SUQTRPER-_t2.SUQTRPER";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
                +" and "+i_cTable+".SUCODMAG = "+i_cQueryTable+".SUCODMAG";
                +" and "+i_cTable+".SUCODLOT  = "+i_cQueryTable+".SUCODLOT ";
                +" and "+i_cTable+".SUCODUBI  = "+i_cQueryTable+".SUCODUBI ";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SUQTAPER = (select "+i_cTable+".SUQTAPER-SUQTAPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SUQTRPER = (select "+i_cTable+".SUQTRPER-SUQTRPER from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_ROWS = i_rows
      if this.w_AGCLOTUBI="S"
        * --- Write into SALOTCOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SMCODART,SMCODMAG,SMCODLOT ,SMCODUBI ,SMCODCAN"
          do vq_exec with 'GSMD_SL2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER-_t2.SMQTAPER";
              +",SMQTRPER = SALOTCOM.SMQTRPER-_t2.SMQTRPER";
              +i_ccchkf;
              +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
              +"SALOTCOM.SMQTAPER = SALOTCOM.SMQTAPER-_t2.SMQTAPER";
              +",SALOTCOM.SMQTRPER = SALOTCOM.SMQTRPER-_t2.SMQTRPER";
              +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SALOTCOM.SMCODART = t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = t2.SMCODCAN";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set (";
              +"SMQTAPER,";
              +"SMQTRPER";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SMQTAPER-t2.SMQTAPER,";
              +"SMQTRPER-t2.SMQTRPER";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODLOT  = _t2.SMCODLOT ";
                  +" and "+"SALOTCOM.SMCODUBI  = _t2.SMCODUBI ";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER-_t2.SMQTAPER";
              +",SMQTRPER = SALOTCOM.SMQTRPER-_t2.SMQTRPER";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
                  +" and "+i_cTable+".SMCODMAG = "+i_cQueryTable+".SMCODMAG";
                  +" and "+i_cTable+".SMCODLOT  = "+i_cQueryTable+".SMCODLOT ";
                  +" and "+i_cTable+".SMCODUBI  = "+i_cQueryTable+".SMCODUBI ";
                  +" and "+i_cTable+".SMCODCAN = "+i_cQueryTable+".SMCODCAN";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = (select "+i_cTable+".SMQTAPER-SMQTAPER from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SMQTRPER = (select "+i_cTable+".SMQTRPER-SMQTRPER from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_ROWSCOM = i_rows
      endif
    endif
    if VarType( this.w_Log ) ="O" 
      AddMsgNL("Aggiornate %1 righe dell' anagrafica dei saldi lotti", this.w_Log , alltrim(str( this.w_rows)), , ,, ,)
      if this.w_AGCLOTUBI="S"
        AddMsgNL("Aggiornate %1 righe dell' anagrafica dei saldi lotti/commessa", this.w_Log , alltrim(str( this.w_rowscom)), , ,, ,)
      endif
    endif
    * --- Aggiorno la Combo Disponibilit� nell'Anagrafica Lotti
    if EMPTY (this.pSERIALE) OR EMPTY ( this.pTIPO)
      * --- Aggiungo le giacenze fuori linea
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Inizio aggiornamento saldi lotti da giacenze fuori linea", this.w_Log , , , ,, ,)
      endif
      * --- Se il lotto � solo fuori linea allova viene inserito in anagrafica saldi lotti
      * --- Try
      local bErr_02CC68B0
      bErr_02CC68B0=bTrsErr
      this.Try_02CC68B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Inserisce valori non presenti in SALDILOT ma presenti in SALLOTUBI
        * --- Insert into SALDILOT
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA9BRS",this.SALDILOT_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into SALDILOT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SUCODMAG,SUCODART,SUCODLOT,SUCODUBI "
          do vq_exec with 'GSMD_SL7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SALDILOT.SUCODMAG = _t2.SUCODMAG";
                  +" and "+"SALDILOT.SUCODART = _t2.SUCODART";
                  +" and "+"SALDILOT.SUCODLOT = _t2.SUCODLOT";
                  +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPRO";
              +",SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPRO";
              +",SUQTAPRO = SALDILOT.SUQTAPRO+_t2.SUQTAPRO";
              +",SUQTRPRO = SALDILOT.SUQTRPRO+_t2.SUQTRPRO";
              +i_ccchkf;
              +" from "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SALDILOT.SUCODMAG = _t2.SUCODMAG";
                  +" and "+"SALDILOT.SUCODART = _t2.SUCODART";
                  +" and "+"SALDILOT.SUCODLOT = _t2.SUCODLOT";
                  +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT, "+i_cQueryTable+" _t2 set ";
              +"SALDILOT.SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPRO";
              +",SALDILOT.SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPRO";
              +",SALDILOT.SUQTAPRO = SALDILOT.SUQTAPRO+_t2.SUQTAPRO";
              +",SALDILOT.SUQTRPRO = SALDILOT.SUQTRPRO+_t2.SUQTRPRO";
              +Iif(Empty(i_ccchkf),"",",SALDILOT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SALDILOT.SUCODMAG = t2.SUCODMAG";
                  +" and "+"SALDILOT.SUCODART = t2.SUCODART";
                  +" and "+"SALDILOT.SUCODLOT = t2.SUCODLOT";
                  +" and "+"SALDILOT.SUCODUBI  = t2.SUCODUBI ";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set (";
              +"SUQTAPER,";
              +"SUQTRPER,";
              +"SUQTAPRO,";
              +"SUQTRPRO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SUQTAPER+t2.SUQTAPRO,";
              +"SUQTRPER+t2.SUQTRPRO,";
              +"SUQTAPRO+t2.SUQTAPRO,";
              +"SUQTRPRO+t2.SUQTRPRO";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SALDILOT.SUCODMAG = _t2.SUCODMAG";
                  +" and "+"SALDILOT.SUCODART = _t2.SUCODART";
                  +" and "+"SALDILOT.SUCODLOT = _t2.SUCODLOT";
                  +" and "+"SALDILOT.SUCODUBI  = _t2.SUCODUBI ";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDILOT set ";
              +"SUQTAPER = SALDILOT.SUQTAPER+_t2.SUQTAPRO";
              +",SUQTRPER = SALDILOT.SUQTRPER+_t2.SUQTRPRO";
              +",SUQTAPRO = SALDILOT.SUQTAPRO+_t2.SUQTAPRO";
              +",SUQTRPRO = SALDILOT.SUQTRPRO+_t2.SUQTRPRO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SUCODMAG = "+i_cQueryTable+".SUCODMAG";
                  +" and "+i_cTable+".SUCODART = "+i_cQueryTable+".SUCODART";
                  +" and "+i_cTable+".SUCODLOT = "+i_cQueryTable+".SUCODLOT";
                  +" and "+i_cTable+".SUCODUBI  = "+i_cQueryTable+".SUCODUBI ";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SUQTAPER = (select "+i_cTable+".SUQTAPER+SUQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SUQTRPER = (select "+i_cTable+".SUQTRPER+SUQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SUQTAPRO = (select "+i_cTable+".SUQTAPRO+SUQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SUQTRPRO = (select "+i_cTable+".SUQTRPRO+SUQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore aggiunta giacenze fuori linea'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_02CC68B0
      * --- End
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Aggiornate giacenze fuori linea di  %1 righe della tabella saldi lotti  ", this.w_Log , alltrim(str( i_rows)), , ,, ,)
        AddMsgNL("Inizio aggiornamento stato lotti ", this.w_Log , , , ,, ,)
      endif
      * --- Write into LOTTIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.LOTTIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="LOCODICE,LOCODART"
        do vq_exec with 'GSVE6BDL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
                +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOFLSTAT = _t2.LOFLSTAT";
            +i_ccchkf;
            +" from "+i_cTable+" LOTTIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
                +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART, "+i_cQueryTable+" _t2 set ";
            +"LOTTIART.LOFLSTAT = _t2.LOFLSTAT";
            +Iif(Empty(i_ccchkf),"",",LOTTIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="LOTTIART.LOCODICE = t2.LOCODICE";
                +" and "+"LOTTIART.LOCODART = t2.LOCODART";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART set (";
            +"LOFLSTAT";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.LOFLSTAT";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="LOTTIART.LOCODICE = _t2.LOCODICE";
                +" and "+"LOTTIART.LOCODART = _t2.LOCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" LOTTIART set ";
            +"LOFLSTAT = _t2.LOFLSTAT";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".LOCODICE = "+i_cQueryTable+".LOCODICE";
                +" and "+i_cTable+".LOCODART = "+i_cQueryTable+".LOCODART";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOFLSTAT = (select LOFLSTAT from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Aggiornato stato di %1 lotti  ", this.w_Log , alltrim(str( i_rows)), , ,, ,)
      endif
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Inizio aggiornamento saldi commesse,lotti,ubicazione da giacenze fuori linea", this.w_Log , , , ,, ,)
      endif
      * --- Se il lotto � solo fuori linea allora viene inserito in anagrafica saldi lotti
      * --- Try
      local bErr_02CC90A0
      bErr_02CC90A0=bTrsErr
      this.Try_02CC90A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Inserisce valori non presenti in SALOTCOM ma presenti in SASLOTCOM
        * --- Insert into SALOTCOM
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA10BRS",this.SALOTCOM_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into SALOTCOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SMCODART,SMCODMAG,SMCODCAN,SMCODLOT,SMCODUBI"
          do vq_exec with 'GSSRMSAS1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                  +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                  +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPRO";
              +",SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPRO";
              +",SMQTAPRO = SALOTCOM.SMQTAPRO+_t2.SMQTAPRO";
              +",SMQTRPRO = SALOTCOM.SMQTRPRO+_t2.SMQTRPRO";
              +i_ccchkf;
              +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                  +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                  +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
              +"SALOTCOM.SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPRO";
              +",SALOTCOM.SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPRO";
              +",SALOTCOM.SMQTAPRO = SALOTCOM.SMQTAPRO+_t2.SMQTAPRO";
              +",SALOTCOM.SMQTRPRO = SALOTCOM.SMQTRPRO+_t2.SMQTRPRO";
              +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SALOTCOM.SMCODART = t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODCAN = t2.SMCODCAN";
                  +" and "+"SALOTCOM.SMCODLOT = t2.SMCODLOT";
                  +" and "+"SALOTCOM.SMCODUBI = t2.SMCODUBI";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set (";
              +"SMQTAPER,";
              +"SMQTRPER,";
              +"SMQTAPRO,";
              +"SMQTRPRO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SMQTAPER+t2.SMQTAPRO,";
              +"SMQTRPER+t2.SMQTRPRO,";
              +"SMQTAPRO+t2.SMQTAPRO,";
              +"SMQTRPRO+t2.SMQTRPRO";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                  +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                  +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                  +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                  +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
              +"SMQTAPER = SALOTCOM.SMQTAPER+_t2.SMQTAPRO";
              +",SMQTRPER = SALOTCOM.SMQTRPER+_t2.SMQTRPRO";
              +",SMQTAPRO = SALOTCOM.SMQTAPRO+_t2.SMQTAPRO";
              +",SMQTRPRO = SALOTCOM.SMQTRPRO+_t2.SMQTRPRO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
                  +" and "+i_cTable+".SMCODMAG = "+i_cQueryTable+".SMCODMAG";
                  +" and "+i_cTable+".SMCODCAN = "+i_cQueryTable+".SMCODCAN";
                  +" and "+i_cTable+".SMCODLOT = "+i_cQueryTable+".SMCODLOT";
                  +" and "+i_cTable+".SMCODUBI = "+i_cQueryTable+".SMCODUBI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER = (select "+i_cTable+".SMQTAPER+SMQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SMQTRPER = (select "+i_cTable+".SMQTRPER+SMQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SMQTAPRO = (select "+i_cTable+".SMQTAPRO+SMQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SMQTRPRO = (select "+i_cTable+".SMQTRPRO+SMQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore aggiunta giacenze fuori linea'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_02CC90A0
      * --- End
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Aggiornate giacenze fuori linea di  %1 righe della tabella saldi commessa,lotti,ubicazioni  ", this.w_Log , alltrim(str( i_rows)), , ,, ,)
      endif
    else
      * --- Effettua l aggiornamento dei saldi lotti solo per i lotti movimentati
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Inizio aggiornamento stato lotti ", this.w_Log , , , ,, ,)
      endif
      * --- Select from gsmd_sss
      do vq_exec with 'gsmd_sss',this,'_Curs_gsmd_sss','',.f.,.t.
      if used('_Curs_gsmd_sss')
        select _Curs_gsmd_sss
        locate for 1=1
        do while not(eof())
        this.w_CODLOT = SUCODLOT
        this.w_CODART = SUCODART 
        if SUQTAPER - SUQTRPER >0 
          * --- Write into LOTTIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOTTIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                +i_ccchkf ;
            +" where ";
                +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                   )
          else
            update (i_cTable) set;
                LOFLSTAT = "D";
                &i_ccchkf. ;
             where;
                LOCODICE = this.w_CODLOT;
                and LOCODART = this.w_CODART;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into LOTTIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LOTTIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                +i_ccchkf ;
            +" where ";
                +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.w_CODART);
                   )
          else
            update (i_cTable) set;
                LOFLSTAT = "E";
                &i_ccchkf. ;
             where;
                LOCODICE = this.w_CODLOT;
                and LOCODART = this.w_CODART;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_gsmd_sss
          continue
        enddo
        use
      endif
      if VarType( this.w_Log ) ="O" 
        AddMsgNL("Aggiornato stato lotti", this.w_Log , , , ,, ,)
      endif
    endif
    * --- Aggiorna Saldi Lotti/Ubicazioni
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    if NOT this.pBNOTRAN
      * --- commit
      cp_EndTrs(.t.)
    endif
    return
  proc Try_02CC68B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDILOT
    i_nConn=i_TableProp[this.SALDILOT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMD_SL7",this.SALDILOT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02CC90A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALOTCOM
    i_nConn=i_TableProp[this.SALOTCOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSRMSAS1",this.SALOTCOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pSERIALE,pTIPO,pOPERAZIONE,pRIGA,pBNOTRAN,w_LOG,w_FLDELSLD,w_AGCLOTUBI,w_CODARTIN,w_CODARTFI)
    this.pSERIALE=pSERIALE
    this.pTIPO=pTIPO
    this.pOPERAZIONE=pOPERAZIONE
    this.pRIGA=pRIGA
    this.pBNOTRAN=pBNOTRAN
    this.w_LOG=w_LOG
    this.w_FLDELSLD=w_FLDELSLD
    this.w_AGCLOTUBI=w_AGCLOTUBI
    this.w_CODARTIN=w_CODARTIN
    this.w_CODARTFI=w_CODARTFI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='SALDILOT'
    this.cWorkTables[2]='*TMPSALDI'
    this.cWorkTables[3]='LOTTIART'
    this.cWorkTables[4]='SALOTCOM'
    this.cWorkTables[5]='SALDICOM'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gsmd_sss')
      use in _Curs_gsmd_sss
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIALE,pTIPO,pOPERAZIONE,pRIGA,pBNOTRAN,w_LOG,w_FLDELSLD,w_AGCLOTUBI,w_CODARTIN,w_CODARTFI"
endproc
