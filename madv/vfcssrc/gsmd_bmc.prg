* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmd_bmc                                                        *
*              Controllo su cancellazione saldi  lotti                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-26                                                      *
* Last revis.: 2013-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmd_bmc",oParentObject,m.pPROV)
return(i_retval)

define class tgsmd_bmc as StdBatch
  * --- Local variables
  pPROV = space(3)
  W_MESS = space(255)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPROV="LOTCOM"
        * --- Saldi per Commessa/Lotti/Ubicazioni
        if g_GPOS="S"
          vq_exec("..\MADV\EXE\QUERY\GSMDCLBPS",this,"RESULT")
        else
          vq_exec("..\MADV\EXE\QUERY\GSMDCLBVS",this,"RESULT")
        endif
        sum RESULT.QUANTI to L_TOTAMOVI
        if L_TOTAMOVI > 0
          this.W_MESS = "Non � possibile eseguire la cancellazione in quanto esistono movimenti dell'articolo e del codice commessa nel magazzino specificato"
          this.W_MESS = AH_MsgFormat(this.W_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.W_MESS
        endif
      case this.pPROV="LOT"
        * --- Saldi Lotti/Ubicazioni
        if g_GPOS="S"
          vq_exec("..\MADV\EXE\QUERY\GSMDLBPS",this,"RESULT")
        else
          vq_exec("..\MADV\EXE\QUERY\GSMDLBVS",this,"RESULT")
        endif
        sum RESULT.QUANTI to L_TOTAMOVI
        if L_TOTAMOVI > 0
          this.W_MESS = "Non � possibile eseguire la cancellazione in quanto esistono movimenti dell'articolo e del codice lotto nel magazzino ed ubicazione specificati"
          this.W_MESS = AH_MsgFormat(this.W_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.W_MESS
        endif
    endcase
    if USED("RESULT")
      SELECT RESULT 
 USE
    endif
  endproc


  proc Init(oParentObject,pPROV)
    this.pPROV=pPROV
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV"
endproc
